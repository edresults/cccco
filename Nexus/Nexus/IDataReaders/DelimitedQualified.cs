﻿using Microsoft.VisualBasic.FileIO;
using Nexus.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace Nexus.IDataReaders
{
    public class DelimitedQualified : IDataReader
    {
        // statics
        private readonly StreamReader _sr;
        private readonly TextFieldParser _tfp;
        private readonly INexus _iNexus;
        private readonly int _errorThreshold = 10;
        // file record
        private string[] _fileFieldValues;
        private long _recordCount = 0;
        private int _errorCount = 0;
        private Dictionary<int, Func<string, object>> _dictionary;
        private bool _checkTooFewCols = false;

        public DelimitedQualified(INexus iNexus)
        {
            _iNexus = iNexus;
            _dictionary = _iNexus.FunctionMap;

            if (_iNexus.Entity.GetType().Name == "OccupationalSurvey")
            {
                _checkTooFewCols = true;
            }


            // open file
            _sr = new StreamReader(_iNexus.FilePath);
            _tfp = new TextFieldParser(_sr);

            _tfp.SetDelimiters(new string[] { _iNexus.Delimiter.ToString() });
            _tfp.HasFieldsEnclosedInQuotes = _iNexus.HasFieldsEnclosedInQuotes;

            if (_iNexus.HasHeader) _tfp.ReadLine();
        }

        public long ErrorCount { get { return _errorCount; } }

        public long RecordCount { get { return _recordCount; } }

        public int FieldCount { get { return _dictionary.Count; } }

        public object GetValue(int i)
        {
            // Get column value
            string fileFieldValue = _fileFieldValues[i];

            // dictionary of properties to functions
            // key: field index
            // value: convert function
            return _dictionary[i](fileFieldValue);
        }

        public bool Read()
        {
            if (IsClosed || _tfp.EndOfData)
            {
                return false;
            }

            _recordCount += 1;

            //if (_recordCount % 1000 == 0)
            //{
            //    Console.WriteLine(_recordCount);
            //}

            bool invalid = true;

            while (invalid)
            {
                // get field value array
                _fileFieldValues = _tfp.ReadFields();

                // process 
                if (_fileFieldValues.Length < FieldCount)
                {
                    Console.WriteLine("Error:      Too few file fields at record number {0}", Convert.ToInt32(_iNexus.HasHeader) + _recordCount);

                    // iterate error count
                    _errorCount += 1;

                    // exit if error threshold met
                    if (_errorCount >= _errorThreshold)
                    {
                        this.Close();
                        // do not read
                        return false;
                    }

                    continue;
                }

                if (_checkTooFewCols && _fileFieldValues.Length > FieldCount)
                {
                    Console.WriteLine("Error:      Too many file fields at record number {0}", Convert.ToInt32(_iNexus.HasHeader) + _recordCount);

                    // iterate error count
                    //_errorCount += 1;

                    // exit if error threshold met
                    if (_errorCount >= _errorThreshold)
                    {
                        this.Close();
                        // do not read
                        return false;
                    }

                    continue;
                }

                invalid = false;

            }

            return true;
        }

        public void Dispose()
        {
            _tfp.Dispose();
            _sr.Dispose();
        }

        public object this[int i]
        {
            get
            {
                // ?????
                return _fileFieldValues[i];
            }
        }

        #region IDataReader Members
        public object this[string name]
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public int Depth
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public bool IsClosed { get; set; }
        public int RecordsAffected
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public void Close()
        {
            throw new NotImplementedException();
        }
        public bool GetBoolean(int i)
        {
            throw new NotImplementedException();
        }
        public byte GetByte(int i)
        {
            throw new NotImplementedException();
        }
        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }
        public char GetChar(int i)
        {
            throw new NotImplementedException();
        }
        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }
        public IDataReader GetData(int i)
        {
            throw new NotImplementedException();
        }
        public string GetDataTypeName(int i)
        {
            throw new NotImplementedException();
        }
        public DateTime GetDateTime(int i)
        {
            throw new NotImplementedException();
        }
        public decimal GetDecimal(int i)
        {
            throw new NotImplementedException();
        }
        public double GetDouble(int i)
        {
            throw new NotImplementedException();
        }
        public Type GetFieldType(int i)
        {
            throw new NotImplementedException();
        }
        public float GetFloat(int i)
        {
            throw new NotImplementedException();
        }
        public Guid GetGuid(int i)
        {
            throw new NotImplementedException();
        }
        public short GetInt16(int i)
        {
            throw new NotImplementedException();
        }
        public int GetInt32(int i)
        {
            throw new NotImplementedException();
        }
        public long GetInt64(int i)
        {
            throw new NotImplementedException();
        }
        public string GetName(int i)
        {
            throw new NotImplementedException();
        }
        public int GetOrdinal(string name)
        {
            throw new NotImplementedException();
        }
        public DataTable GetSchemaTable()
        {
            throw new NotImplementedException();
        }
        public string GetString(int i)
        {
            throw new NotImplementedException();
        }
        public int GetValues(object[] values)
        {
            throw new NotImplementedException();
        }
        public bool IsDBNull(int i)
        {
            return this[i] is DBNull;
        }
        public bool NextResult()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
