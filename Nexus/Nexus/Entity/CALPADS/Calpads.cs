﻿using Microsoft.SqlServer.Server;
using Nexus.CustomAttribute;
using Nexus.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace Nexus.Entity.CALPADS
{
    public class Calpads : FileDetail
    {

        public static void PostProcessing()
        {
            string connString = "Server=PRO-DAT-SQL-03;Database=calpass;Trusted_Connection=true";
            string commString = "dbo.K12Process";

            Console.WriteLine("Calpads:    {0}", commString);

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                using (SqlCommand comm = new SqlCommand(commString, conn))
                {
                    conn.Open();
                    comm.CommandType = CommandType.StoredProcedure;
                    comm.CommandTimeout = 0;
                    comm.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            //commString = "dbo.p_MemberList_Process";

            //try
            //{
            //    using (SqlConnection conn = new SqlConnection(connString))
            //    using (SqlCommand comm = new SqlCommand(commString, conn))
            //    {
            //        conn.Open();
            //        comm.CommandType = CommandType.StoredProcedure;
            //        comm.CommandTimeout = 0;
            //        comm.ExecuteNonQuery();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex);
            //}
        }

        public Calpads(FileDrop fileDrop)
        {
            FilePath = fileDrop.TempFileName;
            HasFieldsEnclosedInQuotes = false;
            HasRecordNumber = true;
            HasValidation = false;
            ConnectionString = "Server=PRO-DAT-SQL-03;Database=calpass;Trusted_Connection=true";
            SchemaName = "calpads";

            SqlCommand etlCommand = null;

            // get first line of file
            string line;

            using (StreamReader sr = new StreamReader(FilePath))
            {
                line = sr.ReadLine() ?? "";
            }

            HasHeader = false;
            Delimiter = '^';

            const int OdsOffest = 2;
            int fieldCount;
            string field;
            // first field
            field = line.Split((char)Delimiter)[0];
            // if field is null
            if (String.IsNullOrWhiteSpace(field))
            {
                fileDrop.Status = "Failed";
                fileDrop.ErrorMessage = "Could not determine CALPADS file type";
                return;
            }
            else
            {
                try
                {
                    // CALPADS file type is the first four characters of the first field
                    TableName = field.Substring(0, 4).ToLower();
                    // hack for Enrollment, not Completion
                    TableName = TableName.Replace("crse", "crsc");
                }
                catch (Exception ex)
                {
                    fileDrop.Status = "Failed";
                    fileDrop.ErrorMessage = ex.Message;
                    return;
                }
            }
            // cound fields
            fieldCount = line.Split((char)Delimiter).Length;
            // if file originates from CALPADS Operational Data Store
            // then offset two fields to account for upload_date and update_date
            if (field.Length == 6 && field.Substring(4, 2) == @"-R") fieldCount -= OdsOffest;
            // process
            if (TableName == "crsc" && fieldCount == 28) Entity = new CrscV30();
            else if (TableName == "crsc" && fieldCount == 36) Entity = new CrscV110();
            else if (TableName == "scsc" && fieldCount == 19) Entity = new ScscV30();
            else if (TableName == "scsc" && fieldCount == 20) Entity = new ScscV40();
            else if (TableName == "scsc" && fieldCount == 21) Entity = new ScscV40();
            else if (TableName == "scte" && fieldCount == 14) Entity = new ScteV40();
            else if (TableName == "sdis" && fieldCount == 22) Entity = new SdisV40();
            else if (TableName == "sdis" && fieldCount == 23) Entity = new SdisV70();
            else if (TableName == "sela" && fieldCount == 15) Entity = new SelaV60();
            else if (TableName == "senr" && fieldCount == 30) Entity = new SenrV30();
            else if (TableName == "senr" && fieldCount == 31) Entity = new SenrV60();
            else if (TableName == "senr" && fieldCount == 33) Entity = new SenrV85();
            else if (TableName == "senr" && fieldCount == 39) Entity = new SenrV102();
            else if (TableName == "senr" && fieldCount == 34) Entity = new SenrV120();
            else if (TableName == "sinf" && fieldCount == 54) Entity = new SinfV30();
            else if (TableName == "sinf" && fieldCount == 42) Entity = new SinfV60();
            else if (TableName == "sinf" && fieldCount == 44) Entity = new SinfV120();
            else if (TableName == "sprg" && fieldCount == 22) Entity = new SprgV30();
            else if (TableName == "sprg" && fieldCount == 26) Entity = new SprgV60();
            else if (TableName == "stas" && fieldCount == 21) Entity = new StasV86();
            else if (TableName == "sinc") Entity = new SincV120();
            else if (TableName == "sirs") Entity = new SirsV120();
            else if (TableName == "soff") Entity = new SoffV120();
            else if (TableName == "sped") Entity = new SpedV120();
            else if (TableName == "psts" && fieldCount == 13) Entity = new PstsV110();
            else if (TableName == "psts" && fieldCount == 15) Entity = new PstsV120();
            else
            {
                fileDrop.Status = "Failed";
                fileDrop.ErrorMessage = "Could not determine CALPADS file type";
                return;
            }

            StageName = TableName.Substring(0, Math.Min(TableName.Length, 107)) + DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
            FullTableName = string.Format("[{0}].[{1}]", SchemaName, TableName);
            FullStageName = string.Format("[{0}].[{1}]", SchemaName, StageName);

            //if (Entity.GetType().Name.IndexOf("Sinc") >= 0 ||
            //    Entity.GetType().Name.IndexOf("Sirs") >= 0 ||
            //    Entity.GetType().Name.IndexOf("Soff") >= 0)
            //{
            //    StageName = TableName;
            //    FullTableName = string.Format("[{0}].[{1}]", SchemaName, TableName);
            //    FullStageName = string.Format("[{0}].[{1}]", SchemaName, TableName);
            //}
            //else
            if (Entity.GetType().Name.IndexOf("Senr") >= 0 ||
                Entity.GetType().Name.IndexOf("Sinf") >= 0 ||
                Entity.GetType().Name.IndexOf("Sela") >= 0 ||
                Entity.GetType().Name.IndexOf("Sprg") >= 0 ||
                Entity.GetType().Name.IndexOf("Sdis") >= 0 ||
                Entity.GetType().Name.IndexOf("Psts") >= 0)
            {
                var comm = "[calpads].[" + Entity.GetType().Name + "Merge]";

                etlCommand = new SqlCommand(
                    comm,
                    new SqlConnection(ConnectionString)
                );

                etlCommand.CommandType = CommandType.StoredProcedure;
                etlCommand.CommandTimeout = 0;
                etlCommand.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "StageSchemaTableName",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 517 * 2,
                        Value = FullStageName
                    }
                );
            }
            else
            {
                etlCommand = new SqlCommand(
                    "[dbo].[GenericMerge]",
                    new SqlConnection(ConnectionString)
                );

                etlCommand.CommandType = CommandType.StoredProcedure;
                etlCommand.CommandTimeout = 0;
                etlCommand.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "SourceSchemaTableName",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 517 * 2,
                        Value = FullTableName
                    }
                );
                etlCommand.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "StageSchemaTableName",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 517 * 2,
                        Value = FullStageName
                    }
                );
            }

            EtlCommand = new List<SqlCommand>();
            EtlCommand.Add(etlCommand);

            // assign properties
            FunctionMap = SqlBulkCopyExt.TextToTypeDictionary(Entity.GetType());
        }
    }
}
