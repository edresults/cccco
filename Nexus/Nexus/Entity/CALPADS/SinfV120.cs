﻿using Nexus.CustomAttribute;
using System.Data;

namespace Nexus.Entity.CALPADS
{
    class SinfV120
    {
        [ColumnMapping(1, 0)]
        [ColumnMetaData("TransactionTypeCode", SqlDbType.VarChar, 1)]
        public string TransactionTypeCode { get; set; }
        [ColumnMapping(3, 1)]
        [ColumnMetaData("EffectiveStartDate", SqlDbType.Char, 8)]
        public string EffectiveStartDate { get; set; }
        [ColumnMapping(4, 2)]
        [ColumnMetaData("EffectiveEndDate", SqlDbType.Char, 8)]
        public string EffectiveEndDate { get; set; }
        [ColumnMapping(5, 3)]
        [ColumnMetaData("DistrictCode", SqlDbType.VarChar, 7)]
        public string DistrictCode { get; set; }
        [ColumnMapping(6, 4)]
        [ColumnMetaData("SchoolCode", SqlDbType.VarChar, 7)]
        public string SchoolCode { get; set; }
        [ColumnMapping(7, 5)]
        [ColumnMetaData("YearCode", SqlDbType.VarChar, 9)]
        public string YearCode { get; set; }
        [ColumnMapping(8, 6)]
        [ColumnMetaData("StudentStateId", SqlDbType.Char, 10)]
        public string StudentStateId { get; set; }
        [ColumnMapping(9, 7)]
        [ColumnMetaData("StudentLocalId", SqlDbType.VarChar, 15)]
        public string StudentLocalId { get; set; }
        [ColumnMapping(10, 8)]
        [ColumnMetaData("NameFirst", SqlDbType.VarChar, 30)]
        public string NameFirst { get; set; }
        [ColumnMapping(11, 9)]
        [ColumnMetaData("NameMiddle", SqlDbType.VarChar, 30)]
        public string NameMiddle { get; set; }
        [ColumnMapping(12, 10)]
        [ColumnMetaData("NameLast", SqlDbType.VarChar, 50)]
        public string NameLast { get; set; }
        [ColumnMapping(13, 11)]
        [ColumnMetaData("NameSuffix", SqlDbType.VarChar, 3)]
        public string NameSuffix { get; set; }
        [ColumnMapping(14, 12)]
        [ColumnMetaData("NameFirstAlias", SqlDbType.VarChar, 30)]
        public string NameFirstAlias { get; set; }
        [ColumnMapping(15, 13)]
        [ColumnMetaData("NameMiddleAlias", SqlDbType.VarChar, 30)]
        public string NameMiddleAlias { get; set; }
        [ColumnMapping(16, 14)]
        [ColumnMetaData("NameLastAlias", SqlDbType.VarChar, 50)]
        public string NameLastAlias { get; set; }
        [ColumnMapping(17, 15)]
        [ColumnMetaData("Birthdate", SqlDbType.Char, 8)]
        public string Birthdate { get; set; }
        [ColumnMapping(18, 16)]
        [ColumnMetaData("Gender", SqlDbType.Char, 1)]
        public string Gender { get; set; }
        [ColumnMapping(19, 17)]
        [ColumnMetaData("BirthCity", SqlDbType.VarChar, 30)]
        public string BirthCity { get; set; }
        [ColumnMapping(20, 18)]
        [ColumnMetaData("BirthStateCode", SqlDbType.VarChar, 6)]
        public string BirthStateCode { get; set; }
        [ColumnMapping(21, 19)]
        [ColumnMetaData("BirthCountryCode", SqlDbType.Char, 2)]
        public string BirthCountryCode { get; set; }
        [ColumnMapping(22, 20)]
        [ColumnMetaData("IsHispanicEthnicity", SqlDbType.Char, 1)]
        public string IsHispanicEthnicity { get; set; }
        [ColumnMapping(23, 21)]
        [ColumnMetaData("IsMissingEthnicity", SqlDbType.Char, 1)]
        public string IsMissingEthnicity { get; set; }
        [ColumnMapping(24, 22)]
        [ColumnMetaData("Race01", SqlDbType.Char, 3)]
        public string Race01 { get; set; }
        [ColumnMapping(25, 23)]
        [ColumnMetaData("Race02", SqlDbType.Char, 3)]
        public string Race02 { get; set; }
        [ColumnMapping(26, 24)]
        [ColumnMetaData("Race03", SqlDbType.Char, 3)]
        public string Race03 { get; set; }
        [ColumnMapping(27, 25)]
        [ColumnMetaData("Race04", SqlDbType.Char, 3)]
        public string Race04 { get; set; }
        [ColumnMapping(28, 26)]
        [ColumnMetaData("Race05", SqlDbType.Char, 3)]
        public string Race05 { get; set; }
        [ColumnMapping(29, 27)]
        [ColumnMetaData("IsMissingRace", SqlDbType.Char, 1)]
        public string IsMissingRace { get; set; }
        [ColumnMapping(30, 28)]
        [ColumnMetaData("AddressLine01", SqlDbType.VarChar, 60)]
        public string AddressLine01 { get; set; }
        [ColumnMapping(31, 29)]
        [ColumnMetaData("AddressLine02", SqlDbType.VarChar, 60)]
        public string AddressLine02 { get; set; }
        [ColumnMapping(32, 30)]
        [ColumnMetaData("AddressCityName", SqlDbType.VarChar, 30)]
        public string AddressCityName { get; set; }
        [ColumnMapping(33, 31)]
        [ColumnMetaData("AddressStateCode", SqlDbType.VarChar, 6)]
        public string AddressStateCode { get; set; }
        [ColumnMapping(34, 32)]
        [ColumnMetaData("AddressZipCode", SqlDbType.VarChar, 10)]
        public string AddressZipCode { get; set; }
        [ColumnMapping(35, 33)]
        [ColumnMetaData("InitialSchoolEnrollDate", SqlDbType.Char, 8)]
        public string InitialSchoolEnrollDate { get; set; }
        [ColumnMapping(36, 34)]
        [ColumnMetaData("IsNotCumulativeThreeYearEnroll", SqlDbType.Char, 1)]
        public string IsNotCumulativeThreeYearEnroll { get; set; }
        [ColumnMapping(37, 35)]
        [ColumnMetaData("ParentHighestEducationLevelCode", SqlDbType.Char, 2)]
        public string ParentHighestEducationLevelCode { get; set; }
        [ColumnMapping(38, 36)]
        [ColumnMetaData("Parent01NameFirst", SqlDbType.VarChar, 30)]
        public string Parent01NameFirst { get; set; }
        [ColumnMapping(39, 37)]
        [ColumnMetaData("Parent01NameLast", SqlDbType.VarChar, 50)]
        public string Parent01NameLast { get; set; }
        [ColumnMapping(40, 38)]
        [ColumnMetaData("Parent02NameFirst", SqlDbType.VarChar, 30)]
        public string Parent02NameFirst { get; set; }
        [ColumnMapping(41, 39)]
        [ColumnMetaData("Parent02NameLast", SqlDbType.VarChar, 50)]
        public string Parent02NameLast { get; set; }
        [ColumnMapping(42, 40)]
        [ColumnMetaData("NameFirstPreferred", SqlDbType.VarChar, 30)]
        public string NameFirstPreferred { get; set; }
        [ColumnMapping(43, 41)]
        [ColumnMetaData("NameLastPreferred", SqlDbType.VarChar, 50)]
        public string NameLastPreferred { get; set; }
    }
}
