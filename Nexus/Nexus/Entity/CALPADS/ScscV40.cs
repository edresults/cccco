﻿using Microsoft.SqlServer.Server;
using Nexus.CustomAttribute;
using System.Data;
using System.Data.SqlClient;

namespace Nexus.Entity.CALPADS
{
    class ScscV40
    {
        [ColumnMapping(1, 0)]
        [ColumnMetaData("TransactionTypeCode", SqlDbType.VarChar, 1)]
        public string TransactionTypeCode { get; set; }
        [ColumnMapping(3, 1)]
        [ColumnMetaData("DistrictCode", SqlDbType.VarChar, 7)]
        public string DistrictCode { get; set; }
        [ColumnMapping(4, 2)]
        [ColumnMetaData("SchoolCode", SqlDbType.VarChar, 7)]
        public string SchoolCode { get; set; }
        [ColumnMapping(5, 3)]
        [ColumnMetaData("YearCode", SqlDbType.VarChar, 9)]
        public string YearCode { get; set; }
        [ColumnMapping(6, 4)]
        [ColumnMetaData("StudentStateId", SqlDbType.Char, 10)]
        public string StudentStateId { get; set; }
        [ColumnMapping(12, 5)]
        [ColumnMetaData("CourseId", SqlDbType.VarChar, 10)]
        public string CourseId { get; set; }
        [ColumnMapping(13, 6)]
        [ColumnMetaData("SectionId", SqlDbType.VarChar, 10)]
        public string SectionId { get; set; }
        [ColumnMapping(14, 7)]
        [ColumnMetaData("TermCode", SqlDbType.Char, 2)]
        public string TermCode { get; set; }
        [ColumnMapping(15, 8)]
        [ColumnMetaData("CreditTry", SqlDbType.Decimal, 4, 2)]
        public decimal? CreditTry { get; set; }
        [ColumnMapping(16, 9)]
        [ColumnMetaData("CreditGet", SqlDbType.Decimal, 4, 2)]
        public decimal? CreditGet { get; set; }
        [ColumnMapping(17, 10)]
        [ColumnMetaData("MarkCode", SqlDbType.VarChar, 3)]
        public string MarkCode { get; set; }
        [ColumnMapping(18, 11)]
        [ColumnMetaData("UniversityCode", SqlDbType.VarChar, 2)]
        public string UniversityCode { get; set; }
        [ColumnMapping(19, 12)]
        [ColumnMetaData("MarkingPeriodCode", SqlDbType.Char, 2)]
        public string MarkingPeriodCode { get; set; }
    }
}
