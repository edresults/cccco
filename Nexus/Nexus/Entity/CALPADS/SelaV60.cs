﻿using Nexus.CustomAttribute;
using System.Data;

namespace Nexus.Entity.CALPADS
{
    class SelaV60
    {
        [ColumnMapping(1, 0)]
        [ColumnMetaData("TransactionTypeCode", SqlDbType.VarChar, 1)]
        public string TransactionTypeCode { get; set; }
        [ColumnMapping(3, 1)]
        [ColumnMetaData("DistrictCode", SqlDbType.VarChar, 7)]
        public string DistrictCode { get; set; }
        [ColumnMapping(4, 2)]
        [ColumnMetaData("SchoolCode", SqlDbType.VarChar, 7)]
        public string SchoolCode { get; set; }
        [ColumnMapping(5, 3)]
        [ColumnMetaData("YearCode", SqlDbType.VarChar, 9)]
        public string YearCode { get; set; }
        [ColumnMapping(6, 4)]
        [ColumnMetaData("StudentStateId", SqlDbType.Char, 10)]
        public string StudentStateId { get; set; }
        [ColumnMapping(12, 5)]
        [ColumnMetaData("ElaStatusCode", SqlDbType.VarChar, 4)]
        public string ElaStatusCode { get; set; }
        [ColumnMapping(13, 6)]
        [ColumnMetaData("ElaStatusStartDate", SqlDbType.Char, 8)]
        public string ElaStatusStartDate { get; set; }
        [ColumnMapping(14, 7)]
        [ColumnMetaData("PrimaryLanguageCode", SqlDbType.Char, 2)]
        public string PrimaryLanguageCode { get; set; }
    }
}
