﻿using Nexus.CustomAttribute;
using System.Data;

namespace Nexus.Entity.CALPADS
{
    class ScteV40
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("RecordTypeCode", SqlDbType.VarChar, 6)]
        public string RecordTypeCode { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("TransactionTypeCode", SqlDbType.VarChar, 1)]
        public string TransactionTypeCode { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("LocalRecordId", SqlDbType.VarChar, 255)]
        public string LocalRecordId { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("DistrictCode", SqlDbType.VarChar, 7)]
        public string DistrictCode { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("SchoolCode", SqlDbType.VarChar, 7)]
        public string SchoolCode { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("YearCode", SqlDbType.VarChar, 9)]
        public string YearCode { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("StudentStateId", SqlDbType.Char, 10)]
        public string StudentStateId { get; set; }
        [ColumnMapping(12, 7)]
        [ColumnMetaData("CtePathwayCode", SqlDbType.Char, 3)]
        public string CtePathwayCode { get; set; }
        [ColumnMapping(13, 8)]
        [ColumnMetaData("CtePathwayCompletionYearCode", SqlDbType.Char, 9)]
        public string CtePathwayCompletionYearCode { get; set; }
    }
}
