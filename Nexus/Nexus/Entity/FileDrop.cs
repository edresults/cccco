﻿using Microsoft.SqlServer.Server;
using Nexus.CustomAttribute;
using Nexus.Entity.CALPADS;
using Nexus.Entity.COMIS;
using Nexus.Entity.Constraint;
using Nexus.Entity.MMAP;
using Nexus.Entity.UNIVERSITY;
using Nexus.Entity.EDDUI;
using Nexus.Entity.FOSTERYOUTH;
using Nexus.IDataReaders;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using Nexus.Entity.CAASPP;
using Nexus.Entity.CCFY;
using Nexus.Entity.StudentAthleteFactory;

namespace Nexus.Entity
{
    public class FileDrop
    {
        public int? SubmissionFileId { get; set; }
        public int OrganizationId { get; set; }
        public Guid BatchId { get; set; }
        public DateTime SubmissionDateTime { get; set; }
        public DateTime? ProcessedDateTime { get; set; }
        public string Status { get; set; }
        public string FileLocation { get; set; }
        public string OriginalFileName { get; set; }
        public string TempFileName { get; set; }
        public string ErrorMessage { get; set; }
        public int? UserId { get; set; }
        public string SubmissionFileSource { get; set; }
        public bool? Removed { get; set; }
        public string SubmissionFileDetails { get; set; }
        public int SubmissionFileDescriptionId { get; set; }
        public Purpose Purpose { get; set; }
        public CmsUser User { get; set; }
        public Pilot Pilot { get; set; }
        public Organization Organization { get; set; }
        public FileDropTag Tag { get; set; }
        public INexus INexus { get; set; }

        private const string connectionString = "Server=PRO-DAT-SQL-01;Database=cms_production_calpass;Trusted_Connection=true";
        private const string cmdTextGet = "dbo.FileDropGet";
        private const string cmdTextPatch = "dbo.FileDropPatch";
        private static string[] allowedExts = new String[] { @".txt", @".csv", @".dat" };
        private readonly SqlMetaData[] sqlMetaData = new SqlMetaData[]
        {
            new SqlMetaData("SubmissionFileId", SqlDbType.Int),
            new SqlMetaData("OrganizationId", SqlDbType.Int),
            new SqlMetaData("BatchId", SqlDbType.UniqueIdentifier),
            new SqlMetaData("SubmissionDateTime", SqlDbType.DateTime),
            new SqlMetaData("ProcessedDateTime", SqlDbType.DateTime),
            new SqlMetaData("Status", SqlDbType.NVarChar, 50),
            new SqlMetaData("FileLocation", SqlDbType.NVarChar, 1000),
            new SqlMetaData("OriginalFileName", SqlDbType.NVarChar, 100),
            new SqlMetaData("TempFileName", SqlDbType.NVarChar, 200),
            new SqlMetaData("ErrorMessage", SqlDbType.NVarChar, 4000),
            new SqlMetaData("UserId", SqlDbType.Int),
            new SqlMetaData("SubmissionFileSource", SqlDbType.NVarChar, 4000),
            new SqlMetaData("Removed", SqlDbType.Bit),
            new SqlMetaData("SubmissionFileDetails", SqlDbType.NVarChar, -1),
            new SqlMetaData("SubmissionFileDescriptionId", SqlDbType.Int)
        };

        public static List<FileDrop> GetNew()
        {
            List<FileDrop> fileDrops = new List<FileDrop>();
            List<FileDrop> entryFileDrops = new List<FileDrop>();
            List<FileDrop> archiveDrops = new List<FileDrop>();
            string status = "New";

            using (SqlConnection conn = new SqlConnection(connectionString))
#if DEBUG
            using (SqlCommand comm = new SqlCommand("dbo.DebugFileDropGet", conn))
#else
            using (SqlCommand comm = new SqlCommand(cmdTextGet, conn))
#endif
            {
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "Status",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 50 * 2,
                        Value = status
                    }
                );
                conn.Open();

                using (IDataReader reader = comm.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        fileDrops.Add(
                            new FileDrop()
                            {
                                SubmissionFileId = reader.GetInt32(0),
                                OrganizationId = reader.GetInt32(1),
                                BatchId = reader.GetGuid(2),
                                SubmissionDateTime = reader.GetDateTime(3),
                                ProcessedDateTime = reader.IsDBNull(4) ? (DateTime?)null : reader.GetDateTime(4),
                                Status = reader.GetString(5),
                                FileLocation = reader.IsDBNull(6) ? (String)null : reader.GetString(6),
                                OriginalFileName = reader.GetString(7),
                                TempFileName = reader.GetString(8),
                                ErrorMessage = (String)null, // start fresh
                                UserId = reader.IsDBNull(10) ? (Int32?)null : reader.GetInt32(10),
                                SubmissionFileSource = reader.GetString(11),
                                Removed = reader.IsDBNull(12) ? (Boolean?)null : reader.GetBoolean(12),
                                SubmissionFileDetails = reader.IsDBNull(13) ? (String)null : reader.GetString(13).Substring(0, Math.Min(reader.GetString(13).Length, 4000)),
                                SubmissionFileDescriptionId = reader.GetInt32(14),
                                Purpose = new Purpose()
                                {
                                    FileDropPurposeId = reader.IsDBNull(14) ? (Int32?)null : reader.GetInt32(14),
                                    FileDropPurpose = reader.IsDBNull(15) ? (String)null : reader.GetString(15)
                                },
                                User = new CmsUser()
                                {
                                    UserId = reader.IsDBNull(10) ? (Int32?)null : reader.GetInt32(10),
                                    FirstName = reader.IsDBNull(16) ? (String)null : reader.GetString(16),
                                    Email = reader.IsDBNull(17) ? (String)null : reader.GetString(17)
                                },
                                Pilot = new Pilot()
                                {
                                    LocalFolder = reader.IsDBNull(18) ? (String)null : reader.GetString(18),
                                    SftpFolder = reader.IsDBNull(19) ? (String)null : reader.GetString(19)
                                },
                                Organization = new Organization()
                                {
                                    OrganizationId = reader.GetInt32(1),
                                    OrganizationName = reader.GetString(20),
                                    OrganizationCode = reader.GetString(21)
                                },
                                Tag = new FileDropTag()
                                {
                                    SubmissionFileId = reader.GetInt32(0)
                                }
                            }
                        );
                    }
                }
            }

            foreach (FileDrop fileDrop in fileDrops)
            {
                // check if zip file
                if (Path.GetExtension(fileDrop.TempFileName).ToLower() == ".zip")
                {
                    Console.WriteLine("Zip File:   {0}", fileDrop.TempFileName);

                    // loop through archive; extract files; add filedrop records
                    string filePath = fileDrop.TempFileName;
                    string fileName = Path.GetFileName(filePath);
                    string fileFolder = Path.GetDirectoryName(filePath);
                    Guid batchId = fileDrop.BatchId;

                    FileStream stream = null;
                    FileInfo file = new FileInfo(filePath);

                    try
                    {
                        stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
                    }
                    catch (IOException ex)
                    {
                        fileDrop.Status = "Failed";
                        fileDrop.ErrorMessage = ex.Message;
                        Console.WriteLine("Error:      {0}", ex.Message);
                        fileDrop.Patch();
                        continue;
                    }
                    finally
                    {
                        if (stream != null)
                        {
                            stream.Close();
                        }
                    }

                    using (ZipArchive archive = ZipFile.OpenRead(filePath))
                    {
                        foreach (ZipArchiveEntry entry in archive.Entries)
                        {
                            try
                            {
                                String entryPath = entry.FullName;
                                String entryName = entry.Name;
                                String entryExt = Path.GetExtension(entryPath).ToLower();

                                // skip Mac resource files
                                if (entryPath.IndexOf(".DS_Store") >= 0 || entryPath.IndexOf("__MACOSX") >= 0)
                                {
                                    Console.WriteLine("            Skipping...File is a Mac resource: {0}", entryPath);
                                    continue;
                                }

                                // skip file with no extension
                                if (String.IsNullOrEmpty(entryExt))
                                {
                                    Console.WriteLine("            Skipping...File has no extension: {0}", entryPath);
                                    continue;
                                }

                                // skip unsupported file
                                if (Array.IndexOf(allowedExts, entryExt) == -1)
                                {
                                    Console.WriteLine("            Skipping...File is unsupported: {0}", entryPath);
                                    continue;
                                }

                                // set new file vars
                                string entryFileName = batchId + @"." + entryName;
                                string entryFilePath = fileFolder + @"\" + entryFileName;

                                // extract file and overwrite
                                entry.ExtractToFile(entryFilePath, true);

                                Console.WriteLine("Extracting: {0}", entryFilePath);

                                // create filedrop record
                                entryFileDrops.Add(
                                    new FileDrop()
                                    {
                                        SubmissionFileId = null,
                                        OrganizationId = fileDrop.OrganizationId,
                                        BatchId = fileDrop.BatchId,
                                        SubmissionDateTime = fileDrop.SubmissionDateTime,
                                        ProcessedDateTime = null,
                                        Status = fileDrop.Status,
                                        FileLocation = Path.GetDirectoryName(fileFolder),
                                        OriginalFileName = entryFileName,
                                        TempFileName = entryFilePath,
                                        ErrorMessage = (string)null, // start fresh
                                        UserId = fileDrop.UserId,
                                        SubmissionFileSource = fileDrop.SubmissionFileSource,
                                        Removed = fileDrop.Removed,
                                        SubmissionFileDetails = fileDrop.SubmissionFileDetails,
                                        SubmissionFileDescriptionId = fileDrop.SubmissionFileDescriptionId,
                                        Purpose = new Purpose()
                                        {
                                            FileDropPurposeId = fileDrop.Purpose.FileDropPurposeId,
                                            FileDropPurpose = fileDrop.Purpose.FileDropPurpose
                                        },
                                        User = new CmsUser()
                                        {
                                            UserId = fileDrop.User.UserId,
                                            FirstName = fileDrop.User.FirstName,
                                            Email = fileDrop.User.Email
                                        },
                                        Pilot = new Pilot()
                                        {
                                            LocalFolder = fileDrop.Pilot.LocalFolder,
                                            SftpFolder = fileDrop.Pilot.SftpFolder
                                        },
                                        Organization = new Organization()
                                        {
                                            OrganizationId = fileDrop.Organization.OrganizationId,
                                            OrganizationName = fileDrop.Organization.OrganizationName,
                                            OrganizationCode = fileDrop.Organization.OrganizationCode
                                        },
                                        Tag = new FileDropTag()
                                        {
                                            SubmissionFileId = null
                                        }
                                    }
                                );
                            }
                            catch (Exception ex)
                            {
                                fileDrop.Status = "Failed";
                                fileDrop.ErrorMessage = ex.Message;
                                Console.WriteLine("Error:      {0}", ex.Message);
                                continue;
                            }
                        }
                    }

                    if (fileDrop.Status != "Failed") fileDrop.Status = "Unzipped";

                    archiveDrops.Add(fileDrop);
                    // process zip file as unzipped
                    fileDrop.Patch();
                }
            }

            // update new entries
            foreach (FileDrop entryFileDrop in entryFileDrops)
            {
                entryFileDrop.Patch();
            }

            // remove zip files
            foreach (FileDrop archiveDrop in archiveDrops)
            {
                fileDrops.Remove(archiveDrop);
            }

            // add lists
            fileDrops.AddRange(entryFileDrops);

            return fileDrops;
        }

        public void Patch()
        {
            // initialize record
            SqlDataRecord sqlDataRecord = new SqlDataRecord(sqlMetaData);
            // populate record
            sqlDataRecord.SetValue(0, SubmissionFileId);
            sqlDataRecord.SetValue(1, OrganizationId);
            sqlDataRecord.SetValue(2, BatchId);
            sqlDataRecord.SetValue(3, SubmissionDateTime);
            sqlDataRecord.SetValue(4, DateTime.Now);
            sqlDataRecord.SetValue(5, Status);
            sqlDataRecord.SetValue(6, FileLocation);
            sqlDataRecord.SetValue(7, OriginalFileName);
            sqlDataRecord.SetValue(8, TempFileName);
            sqlDataRecord.SetValue(9, ErrorMessage);
            sqlDataRecord.SetValue(10, UserId);
            sqlDataRecord.SetValue(11, SubmissionFileSource);
            sqlDataRecord.SetValue(12, Removed);
            sqlDataRecord.SetValue(13, SubmissionFileDetails);
            sqlDataRecord.SetValue(14, SubmissionFileDescriptionId);

            IEnumerable<SqlDataRecord> records = new SqlDataRecord[] { sqlDataRecord };

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand comm = new SqlCommand(cmdTextPatch, conn))
            {
                // create command
                comm.CommandType = CommandType.StoredProcedure;
                // add parameters
                comm.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "FileDrop",
                        SqlDbType = SqlDbType.Structured,
                        Value = records
                    }
                );
                // open connection
                comm.Connection.Open();

                // update SubmissionFileId for inserted records coming from zip files
                using (IDataReader reader = comm.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        SubmissionFileId = reader.GetInt32(0);
                        Tag.SubmissionFileId = SubmissionFileId;
                    }
                }
            }
        }

        public void Process()
        {
            while (true)
            {
                ValidateFile();

                if (Status == "Failed") break;

                Define();

                if (Status == "Failed") break;

                ValidateStructure();

                if (Status == "Failed") break;

                Stage();

                if (Status == "Failed") break;

                Import();

                if (Status == "Failed") break;

                ValidateDataDetail();

                if (Status == "Failed") break;

                Merge();

                if (Status == "Failed") break;

                Export();

                if (Status == "Failed") break;

                Evaluate();

                break;
            }

            if (INexus != null && !string.IsNullOrWhiteSpace(INexus.FullStageName)) INexus.StageTableDrop();

            Patch();

            Email();
        }

        private void ValidateFile()
        {
            string[] altServer = { @"\\10.11.4.21\", @"\\10.11.4.50\Archives\" };

            if (File.Exists(TempFileName))
            {
                Status = "Validated";
            }
            else if (File.Exists(TempFileName.Replace(altServer[0], altServer[1])))
            {
                TempFileName = TempFileName.Replace(altServer[0], altServer[1]);
                FileLocation = FileLocation.Replace(altServer[0], altServer[1]);
                Status = "Validated";
            }
            else
            {
                Status = "Failed";
                ErrorMessage = "File does not exist";
                Console.WriteLine("Error:      {0}", ErrorMessage);
                return;
            }

            string fileExt = Path.GetExtension(TempFileName).ToLower();

            if (Array.IndexOf(allowedExts, fileExt) == -1)
            {
                Status = "Failed";
                ErrorMessage = string.Format("Unsupported file extension {0}", fileExt);
                Console.WriteLine("Error:      {0}", ErrorMessage);
                return;
            }

            long fileLength = new FileInfo(TempFileName).Length;

            if (fileLength == 0)
            {
                Status = "Failed";
                ErrorMessage = "File has no data";
                Console.WriteLine("Error:      {0}", ErrorMessage);
                return;
            }

            Status = "Validated";
            ErrorMessage = string.Empty;
        }

        private void Define()
        {
            // validate entity
            if (SubmissionFileDescriptionId == 2)
            {
                INexus = new Calpads(this);
                // process constructor
                if (Status != "Failed")
                {
                    // Tag Attributes
                    Tag.SubmissionFileId = (int)SubmissionFileId;
                    Tag.Attribute = "Version";
                    Tag.Value = INexus.Entity.GetType().Name.ToString();
                    Tag.Patch();
                }
            }
            else if (SubmissionFileDescriptionId == 4 || SubmissionFileDescriptionId == 14)
            {
                INexus = new Caaspp(this);
            }
            else if (SubmissionFileDescriptionId == 6)
            {
                INexus = new University(this);
            }
            else if (SubmissionFileDescriptionId == 7 || SubmissionFileDescriptionId == 8) // MMAP
            {
                INexus = new Mmap(this);
            }
            else if (SubmissionFileDescriptionId == 11)
            {
                INexus = new Comis(this);
            }
            else if (SubmissionFileDescriptionId == 12)
            {
                INexus = new FosterYouth(this);
            }
            else if (SubmissionFileDescriptionId == 13)
            {
                INexus = new Eddui(this);
            }
            else if (SubmissionFileDescriptionId == 15)
            {
                INexus = new CCFosterYouth(this);
            }
            else if (SubmissionFileDescriptionId == 16)
            {
                INexus = new StudentAthlete(this);
            }

            if (INexus.Entity == null)
            {
                Status = "Failed";
                ErrorMessage = "File Entity could not be determined";
                Console.WriteLine(ErrorMessage);
                return;
            }

            Console.WriteLine("File Type:  {0}", INexus.Entity.GetType().Name);
        }

        private void ValidateStructure()
        {
            string line;
            // validate first record
            using (StreamReader sr = new StreamReader(INexus.FilePath))
            {
                if (INexus.HasHeader) sr.ReadLine();

                line = sr.ReadLine();
            }

            if (string.IsNullOrWhiteSpace(line))
            {
                Status = "Failed";
                ErrorMessage = "Error:      File contains no data";
                Console.WriteLine(ErrorMessage);
                return;
            }

            if (INexus.Delimiter == null) return;

            string[] fields = line.Split((char)INexus.Delimiter);
            List<ColumnMetaData> data = ColumnMetaData.GetColumnMetaData(INexus.Entity.GetType()).ToList();
            List<ColumnMapping> map = (List<ColumnMapping>)ColumnMapping.GetMap(INexus.Entity.GetType()).ToList();

            if (map.Count == 0)
            {
                Status = "Failed";
                ErrorMessage = "Error:      No Column Mapping";
                Console.WriteLine(ErrorMessage);
                return;
            }
            else if (fields.Length < map.Count)
            {
                Status = "Failed";
                ErrorMessage = string.Format("Error:      Record contains fewer fields than Column Mapping; Validate Delimiter [{0}]", INexus.Delimiter);
                Console.WriteLine(ErrorMessage);
                return;
            }
            else if (data.Count == 0)
            {
                Status = "Failed";
                ErrorMessage = "Error:      No Meta Data";
                Console.WriteLine(ErrorMessage);
                return;
            }
            //else if (fields.Length < data.Count)
            //{
            //    Status = "Failed";
            //    ErrorMessage = string.Format("Error:      Record contains fewer fields than Meta Data; Validate Delimiter [{0}]", INexus.Delimiter);
            //    Console.WriteLine(ErrorMessage);
            //    return;
            //}

            // loop over fields, check table column string length vs file field string length
            // ToDo: validate more than strings
            string errorMessage = string.Empty;

            PropertyInfo[] properties = INexus.Entity.GetType().GetProperties(
                BindingFlags.Public |
                BindingFlags.Instance |
                BindingFlags.DeclaredOnly
            );

            for (int propertyIndex = 0, propertyLength = properties.Length; propertyIndex < propertyLength; propertyIndex++)
            {
                PropertyInfo property = properties[propertyIndex];
                Type propertyType = property.PropertyType;

                // check for custom attributes
                object[] attributes = property.GetCustomAttributes(false);
                int sourceColumnOrdinal = -1;
                ColumnMapping columnMapping = null;
                ColumnMetaData columnMetaData = null;

                for (int attributeIndex = 0, attributeLength = attributes.Length; attributeIndex < attributeLength; attributeIndex++)
                {
                    object attribute = attributes[attributeIndex];

                    if (attribute.GetType() == typeof(ColumnMapping))
                    {
                        columnMapping = (ColumnMapping)attribute;
                    }
                    else if (attribute.GetType() == typeof(ColumnMetaData))
                    {
                        columnMetaData = (ColumnMetaData)attribute;
                    }

                }

                if (columnMapping == null)
                {
                    continue;
                }

                // loop through fields
                // match on sourceordinal and field index
                for (int fieldIndex = 0, fieldUbound = fields.Length; fieldIndex < fieldUbound; fieldIndex++)
                {
                    if (fieldIndex == sourceColumnOrdinal)
                    {
                        switch (columnMetaData.ColumnType)
                        {
                            case SqlDbType.Char:
                            case SqlDbType.NChar:
                            case SqlDbType.NText:
                            case SqlDbType.NVarChar:
                            case SqlDbType.Text:
                            case SqlDbType.VarChar:
                                string field = fields[fieldIndex];
                                if (field.Length > columnMetaData.ColumnLength)
                                {
                                    if (!String.IsNullOrWhiteSpace(errorMessage))
                                    {
                                        errorMessage += Environment.NewLine;
                                        errorMessage += string.Format("Error:      Field [{0}] with Value [{1}] of Length [{2}] greater than...", fieldIndex + 1, field, field.Length) + Environment.NewLine;
                                        errorMessage += string.Format("            Column [{0}] of Max Length [{1}]", columnMetaData.ColumnName, columnMetaData.ColumnLength);
                                    }
                                }
                                break;
                        }
                        break;
                    }
                }
            }

            if (!String.IsNullOrEmpty(errorMessage))
            {
                Status = "Failed";
                ErrorMessage = errorMessage;
                Console.WriteLine(ErrorMessage);
                return;
            }
        }

        private void Stage()
        {
            // create stating table if applicable
            try
            {
                INexus.StageTableCreate();
                Console.WriteLine("StageTable: {0}", INexus.FullStageName);
            }
            catch (Exception ex)
            {
                Status = "Failed";
                Console.WriteLine("Error:      {0}", ex.Message);
                return;
            }
        }

        private void Import()
        {
            // prime timer
            Stopwatch sw = Stopwatch.StartNew();
            // try to load files
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(INexus.ConnectionString))
                {
                    sqlConnection.Open();

                    using (SqlBulkCopy sbc = new SqlBulkCopy(
                            sqlConnection,
                            SqlBulkCopyOptions.CheckConstraints |
                            SqlBulkCopyOptions.FireTriggers |
                            SqlBulkCopyOptions.KeepNulls |
                            SqlBulkCopyOptions.TableLock |
                            SqlBulkCopyOptions.UseInternalTransaction,
                            null
                        )
                    )
                    {

                        sbc.DestinationTableName = INexus.FullStageName;
                        sbc.BatchSize = 100000;
                        sbc.BulkCopyTimeout = 0;
                        sbc.EnableStreaming = true;

                        // assign values in dictionary
                        IEnumerable<ColumnMapping> items = ColumnMapping.GetMap(INexus.Entity.GetType());
                        foreach (ColumnMapping item in items)
                        {
                            sbc.ColumnMappings.Add(item.SourceColumnOrdinal, item.DestinationOrdinal);
                        }

                        // use text parser?
                        if (INexus.Delimiter == null)
                        {
                            using (Fixed fileDataReader = new Fixed(INexus))
                            {
                                sbc.WriteToServer(fileDataReader);

                                INexus.RecordCount = fileDataReader.RecordCount;
                                Console.WriteLine("Records:    {0}", INexus.RecordCount);

                                if (fileDataReader.ErrorCount >= 10)
                                {
                                    Status = "Failed";
                                    ErrorMessage = "Too many File Structure errors";
                                    Console.WriteLine(ErrorMessage);
                                }
                            }
                        }
                        else if (INexus.HasFieldsEnclosedInQuotes)
                        {
                            using (DelimitedQualified fileDataReader = new DelimitedQualified(INexus))
                            {
                                sbc.WriteToServer(fileDataReader);

                                INexus.RecordCount = fileDataReader.RecordCount - fileDataReader.ErrorCount;
                                Console.WriteLine("Records:    {0}", INexus.RecordCount);
 
                                if (fileDataReader.ErrorCount >= 10)
                                {
                                    Status = "Failed";
                                    ErrorMessage = "Too many File Structure errors";
                                    Console.WriteLine(ErrorMessage);
                                }
                            }
                        }
                        else if (!INexus.HasFieldsEnclosedInQuotes)
                        {
                            using (Delimited fileDataReader = new Delimited(INexus))
                            {
                                sbc.WriteToServer(fileDataReader);

                                INexus.RecordCount = fileDataReader.RecordCount - fileDataReader.ErrorCount;
                                Console.WriteLine("Records:    {0}", INexus.RecordCount);

                                if (fileDataReader.ErrorCount >= 10)
                                {
                                    Status = "Failed";
                                    ErrorMessage = "Too many File Structure errors";
                                    Console.WriteLine(ErrorMessage);
                                }
                            }
                        }
                    }
                }
                // stop timer
                sw.Stop();
                // output
                Console.WriteLine("Duration:   {0}", sw.Elapsed);
                Console.WriteLine("Record/ms:  {0}", sw.Elapsed.TotalMilliseconds / INexus.RecordCount);
                // update status
                Status = "Loaded";
            }
            catch (Exception ex)
            {
                Status = "Failed";
                ErrorMessage = ex.Message;
                Console.WriteLine("Error:      {0}", ex.Message);
            }
        }

        private void ValidateDataDetail()
        {
            if (!INexus.HasValidation) return;

            string output = string.Empty;
            string errors = string.Empty;

            try
            {
                output += !string.IsNullOrEmpty(errors = new NullConstraint().GetDetail(INexus)) ? errors + Environment.NewLine : string.Empty;
                output += !string.IsNullOrEmpty(errors = new CheckConstraint().GetDetail(INexus)) ? errors + Environment.NewLine : string.Empty;
                output += !string.IsNullOrEmpty(errors = new ForeignKey().GetDetail(INexus)) ? errors + Environment.NewLine : string.Empty;
                output += !string.IsNullOrEmpty(errors = new PrimaryKey().GetDetail(INexus)) ? errors : string.Empty;
            }
            catch (Exception ex)
            {
                Status = "Failed";
                ErrorMessage = ex.Message;
                Console.WriteLine(ErrorMessage);
                return;
            }

            // process errors
            if (!string.IsNullOrEmpty(output))
            {
                Status = "Failed";
                ErrorMessage = output;
                Console.WriteLine(output);
                return;
            }

            Status = "Validated";
        }

        private void ValidateData()
        {
            if (!INexus.HasValidation) return;

            string output = string.Empty;
            string errors = string.Empty;

            try
            {
                output += !string.IsNullOrEmpty(errors = new NullConstraint().GetCount(INexus)) ? errors + Environment.NewLine : string.Empty;
                output += !string.IsNullOrEmpty(errors = new CheckConstraint().GetCount(INexus)) ? errors + Environment.NewLine : string.Empty;
                output += !string.IsNullOrEmpty(errors = new ForeignKey().GetCount(INexus)) ? errors + Environment.NewLine : string.Empty;
                output += !string.IsNullOrEmpty(errors = new PrimaryKey().GetCount(INexus)) ? errors : string.Empty;
            }
            catch (Exception ex)
            {
                Status = "Failed";
                ErrorMessage = ex.Message;
                Console.WriteLine(ErrorMessage);
                return;
            }

            // process errors
            if (!string.IsNullOrEmpty(output))
            {
                Status = "Failed";
                ErrorMessage = output;
                Console.WriteLine(output);
                return;
            }

            Status = "Validated";
        }

        private void Merge()
        {
            if (INexus.EtlCommand.Count == 0) return;
            if (INexus.EtlCommand[0] == null) return;
            // prime timer
            Stopwatch sw = new Stopwatch();

            foreach (SqlCommand sqlComm in INexus.EtlCommand)
            {
                try
                {
                    using (SqlConnection conn = sqlComm.Connection)
                    {
                        sqlComm.Connection.Open();
                        // output ETL procedure
                        Console.WriteLine("Procedure:  {0}", sqlComm.CommandText);
                        // start timer
                        sw.Start();
                        // execute stored procedure
                        sqlComm.ExecuteNonQuery();
                        // stop timer
                        sw.Stop();
                        // output duration
                        Console.WriteLine("Duration:   {0} ", sw.Elapsed);
                        Console.WriteLine("Record/ms:  {0}", sw.Elapsed.TotalMilliseconds / INexus.RecordCount);
                        // update status
                        Status = "Merged";
                    }
                }
                catch (Exception ex)
                {
                    Status = "Failed";
                    ErrorMessage = "File could not be merged";
                    // ErrorMessage = ex.Message;
                    Console.WriteLine(ex);
                    // drop table
                    INexus.StageTableDrop();
                    // break
                    break;
                }
            }
            // drop table
            INexus.StageTableDrop();
        }

        private void Export()
        {
            if (Pilot == null) return;
            if (INexus.ExportCommand == null) return;

            if (String.IsNullOrWhiteSpace(Pilot.LocalFolder))
            {
                Status = "Failed";
                ErrorMessage = "Destination missing";
                Console.WriteLine(ErrorMessage);
                return;
            }

            Directory.CreateDirectory(Pilot.LocalFolder);

            string filePath = Pilot.LocalFolder + OriginalFileName;

            try
            {
                INexus.ExportCommand.Connection.Open();

                using (INexus.ExportCommand)
                using (SqlDataReader sqlDataReader = INexus.ExportCommand.ExecuteReader(CommandBehavior.CloseConnection))
                using (StreamWriter streamWriter = new StreamWriter(filePath))
                {
                    int i = 0;
                    int c = sqlDataReader.FieldCount;
                    // create header
                    for (; i < c; i++)
                    {
                        string columnName = sqlDataReader.GetName(i);
                        // check for delimiter
                        if (columnName.Contains(INexus.Delimiter.ToString()))
                        {
                            columnName = columnName.Replace((char)INexus.Delimiter, '_');
                        }
                        // write column name
                        streamWriter.Write(columnName);
                        // add delimiter?
                        if (i < c - 1)
                        {
                            streamWriter.Write(INexus.Delimiter);
                        }
                        else
                        {
                            streamWriter.WriteLine();
                        }
                    }
                    // loop through columns in rows
                    while (sqlDataReader.Read())
                    {
                        i = 0;
                        for (; i < c; i++)
                        {
                            if (sqlDataReader[i].GetType() == typeof(bool))
                            {
                                streamWriter.Write(Convert.ToInt32(sqlDataReader[i]));
                            }
                            else
                            {
                                streamWriter.Write(sqlDataReader[i].ToString());
                            }
                            // add delimiter or terminator
                            if (i < c - 1)
                            {
                                streamWriter.Write(INexus.Delimiter);
                            }
                            else
                            {
                                streamWriter.WriteLine();
                            }
                        }
                    }
                }
                // update fileUploadItem
                Status = "Exported";
                ErrorMessage = null;
            }
            catch (Exception ex)
            {
                Status = "Failed";
                ErrorMessage = ex.Message;
                Console.WriteLine(ErrorMessage);
            }
            finally
            {
                // close connection
                INexus.EtlCommand[0].Connection.Close();
                INexus.EtlCommand[0].Connection.Dispose();
            }
        }

        private void Evaluate()
        {
            if (INexus.EvaluateCommand == null) return;
            // prime timer
            Stopwatch sw = new Stopwatch();

            SqlCommand sqlComm = INexus.EvaluateCommand;

            try
            {
                using (SqlConnection conn = sqlComm.Connection)
                {
                    sqlComm.Connection.Open();
                    // output ETL procedure
                    Console.WriteLine("Procedure:  {0}", sqlComm.CommandText);
                    // start timer
                    sw.Start();
                    // execute stored procedure
                    sqlComm.ExecuteNonQuery();
                    // stop timer
                    sw.Stop();
                    // output duration
                    Console.WriteLine("Duration:   {0} ", sw.Elapsed);
                    Console.WriteLine("Record/ms:  {0}", sw.Elapsed.TotalMilliseconds / INexus.RecordCount);
                }
            }
            catch (Exception ex)
            {
                //Status = "Failed";
                //ErrorMessage = "File could not be evaluated";
                Console.WriteLine(ex);
            }
        }

        private string EmailBody()
        {
            string emailBody = string.Empty;

            if (Status == "Failed" && !string.IsNullOrEmpty(ErrorMessage))
            {
                emailBody = string.Format(

                    "Hello {0}," + Environment.NewLine +
                    Environment.NewLine +
                    "The file, {1}, was not processed." + Environment.NewLine +
                    Environment.NewLine +
                    "<pre>{2}</pre>" + Environment.NewLine +
                    Environment.NewLine +
                    "Please reupload your file when the necessary corrections have been completed." + Environment.NewLine +
                    Environment.NewLine +
                    "For additional support, please email dlamoree@edresults.org." + Environment.NewLine +
                    Environment.NewLine +
                    Environment.NewLine +
                    "Thank you," + Environment.NewLine +
                    "Cal-PASS Plus Support", User.FirstName, OriginalFileName, ErrorMessage);
            }
            else if (SubmissionFileDescriptionId == 7)
            {
                emailBody = string.Format(
                    "Hello {0}," + Environment.NewLine +
                    Environment.NewLine +
                    "The file, {1}, was processed and has been uploaded to our Secure FTP Server. " +
                    "You may download your prospective file from the Secure FTP server using your login credentials provided for you. " +
                    "If you do not already have your Secure FTP credentials, please email dlamoree@edresults.org." +
                    Environment.NewLine +
                    Environment.NewLine +
                    "Thank you," + Environment.NewLine +
                    "Cal-PASS Plus Support", User.FirstName, OriginalFileName);
            }
            else if (SubmissionFileDescriptionId == 8 || SubmissionFileDescriptionId == 15)
            {
                emailBody = string.Format(
                    "Hello {0}," + Environment.NewLine +
                    Environment.NewLine +
                    "The file, {1}, was processed." +
                    Environment.NewLine +
                    Environment.NewLine +
                    "Thank you," + Environment.NewLine +
                    "Cal-PASS Plus Support", User.FirstName, OriginalFileName);
            }

            if (string.IsNullOrEmpty(emailBody)) return string.Empty;

            return emailBody;
        }

        private void Email()
        {
            // validate
            if (!((SubmissionFileDescriptionId == 7) || (SubmissionFileDescriptionId == 8) || (SubmissionFileDescriptionId == 15))) return;

            // email user
            try
            {
                // process error message
                if (Status == "Failed" && string.IsNullOrEmpty(ErrorMessage)) throw new ArgumentException("File has error");
                // get message
                string message = EmailBody();
                // mail: from, to

#if !DEBUG
                MailMessage mail = new MailMessage("no-reply@calpass.org", User.Email);
                mail.Bcc.Add("dlamoree@edresults.org");

                if (SubmissionFileDescriptionId == 15)
                {
                    mail.Bcc.Add("debbie@jbay.org");
                    mail.Bcc.Add("vmarrero@edresults.org");
                }
#else
                MailMessage mail = new MailMessage("no-reply@calpass.org", "dlamoree@edresults.org");
#endif

                
                mail.Subject = string.Format("File Submission :: {0}", Purpose.FileDropPurpose);
                mail.Body = message.Replace(Environment.NewLine, "<br>").Replace(" ", "&nbsp;");
                mail.IsBodyHtml = true;
                // client
                SmtpClient client = new SmtpClient();
                client.Port = 25;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = "mailin.deltacollege.edu";
                client.Send(mail);
            }
            catch (Exception ex)
            {
                // mail: from, to
                MailMessage mail = new MailMessage("no-reply@calpass.org", "dlamoree@edresults.org");
                mail.Subject = string.Format("File Submission :: {0}", Purpose.FileDropPurpose);
                mail.Body =
                    string.Format("User Name:  {0}", User.FirstName) + System.Environment.NewLine +
                    string.Format("User Email: {0}", User.Email) + System.Environment.NewLine +
                    string.Format("Purpose:    {0}", Purpose.FileDropPurpose) + System.Environment.NewLine +
                    string.Format("Org Name:   {0}", Organization.OrganizationName) + System.Environment.NewLine +
                    string.Format("File Id:    {0}", SubmissionFileId) + System.Environment.NewLine +
                    string.Format("File Date:  {0}", SubmissionDateTime) + System.Environment.NewLine +
                    string.Format("File Path:  {0}", TempFileName) + System.Environment.NewLine +
                    string.Format("File Name:  {0}", OriginalFileName.Substring(37)) + System.Environment.NewLine +
                    string.Format("Error:      {0}", ex.Message);
                mail.IsBodyHtml = true;
                // client
                SmtpClient client = new SmtpClient();
                client.Port = 25;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = "mailin.deltacollege.edu";
                client.Send(mail);
            }
        }
    }
}
