﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class SSSUCCESS
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("student_id", SqlDbType.Char, 9, false)]
        public string student_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("term_id", SqlDbType.Char, 3, false)]
        public string term_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("goals", SqlDbType.Char, 1)]
        public string goals { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("major", SqlDbType.VarChar, 6)]
        public string major { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("orientation_status", SqlDbType.Char, 2)]
        public string orientation_status { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("assessment_status", SqlDbType.Char, 2)]
        public string assessment_status { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("educationplan_status", SqlDbType.Char, 2)]
        public string educationplan_status { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("initial_orientation", SqlDbType.Char, 1)]
        public string initial_orientation { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("initial_assessment", SqlDbType.Char, 4)]
        public string initial_assessment { get; set; }
        [ColumnMapping(10, 10)]
        [ColumnMetaData("counseling", SqlDbType.Char, 1)]
        public string counseling { get; set; }
        [ColumnMapping(11, 11)]
        [ColumnMetaData("education_plan", SqlDbType.Char, 1)]
        public string education_plan { get; set; }
        [ColumnMapping(12, 12)]
        [ColumnMetaData("progress", SqlDbType.Char, 1)]
        public string progress { get; set; }
        [ColumnMapping(13, 13)]
        [ColumnMetaData("success", SqlDbType.Char, 4)]
        public string success { get; set; }
        [ColumnMapping(14, 14)]
        [ColumnMetaData("major_nc", SqlDbType.VarChar, 6)]
        public string major_nc { get; set; }
        [ColumnMapping(15, 15)]
        [ColumnMetaData("orientation_status_nc", SqlDbType.Char, 2)]
        public string orientation_status_nc { get; set; }
        [ColumnMapping(16, 16)]
        [ColumnMetaData("assessment_status_nc", SqlDbType.Char, 2)]
        public string assessment_status_nc { get; set; }
        [ColumnMapping(17, 17)]
        [ColumnMetaData("educationplan_status_nc", SqlDbType.Char, 2)]
        public string educationplan_status_nc { get; set; }
        [ColumnMapping(18, 18)]
        [ColumnMetaData("initial_orientation_nc", SqlDbType.Char, 1)]
        public string initial_orientation_nc { get; set; }
        [ColumnMapping(19, 19)]
        [ColumnMetaData("initial_assessment_nc", SqlDbType.Char, 4)]
        public string initial_assessment_nc { get; set; }
        [ColumnMapping(20, 20)]
        [ColumnMetaData("counseling_nc", SqlDbType.Char, 1)]
        public string counseling_nc { get; set; }
        [ColumnMapping(21, 21)]
        [ColumnMetaData("education_plan_nc", SqlDbType.Char, 1)]
        public string education_plan_nc { get; set; }
        [ColumnMapping(22, 22)]
        [ColumnMetaData("success_nc", SqlDbType.Char, 4)]
        public string success_nc { get; set; }
    }
}
