﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class SVVATEA
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("student_id", SqlDbType.Char, 9, false)]
        public string student_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("term_id", SqlDbType.Char, 3, false)]
        public string term_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("voc_pgm_plan", SqlDbType.Char, 1)]
        public string voc_pgm_plan { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("econ_disadv", SqlDbType.Char, 2)]
        public string econ_disadv { get; set; }
        [ColumnMetaData("econ_disadv_status", SqlDbType.Char, 1)]
        public string econ_disadv_status { get; set; }
        [ColumnMetaData("econ_disadv_source", SqlDbType.Char, 1)]
        public string econ_disadv_source { get; set; }
        [ColumnMapping(5, 7)]
        [ColumnMetaData("single_parent", SqlDbType.Char, 1)]
        public string single_parent { get; set; }
        [ColumnMapping(6, 8)]
        [ColumnMetaData("displ_homemker", SqlDbType.Char, 1)]
        public string displ_homemker { get; set; }
        [ColumnMapping(7, 9)]
        [ColumnMetaData("coop_work_exp", SqlDbType.Char, 1)]
        public string coop_work_exp { get; set; }
        [ColumnMapping(8, 10)]
        [ColumnMetaData("tech_prep", SqlDbType.Char, 1)]
        public string tech_prep { get; set; }
        [ColumnMapping(9, 11)]
        [ColumnMetaData("migrant_worker", SqlDbType.Char, 1)]
        public string migrant_worker { get; set; }
        [ColumnMapping(10, 12)]
        [ColumnMetaData("wia_veteran", SqlDbType.Char, 1)]
        public string wia_veteran { get; set; }
    }
}
