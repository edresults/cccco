﻿using Microsoft.SqlServer.Server;
using Nexus.CustomAttribute;
using Nexus.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Nexus.Entity.COMIS
{
    public class Comis : FileDetail
    {

        public static void PostProcessing()
        {
            string connString = "Server=PRO-DAT-SQL-03;Database=calpass;Trusted_Connection=true";
            string commString = "comis.PostProcessing";

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                using (SqlCommand comm = new SqlCommand(commString, conn))
                {
                    conn.Open();
                    comm.CommandType = CommandType.StoredProcedure;
                    comm.CommandTimeout = 0;
                    comm.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            commString = "comis.Rebuild";

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                using (SqlCommand comm = new SqlCommand(commString, conn))
                {
                    conn.Open();
                    comm.CommandType = CommandType.StoredProcedure;
                    comm.CommandTimeout = 0;
                    comm.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public Comis(FileDrop fileDrop)
        {
            FilePath = fileDrop.TempFileName;
            HasFieldsEnclosedInQuotes = false;
            HasValidation = false;
            ConnectionString = "Server=PRO-DAT-SQL-03;Database=calpass;Trusted_Connection=true";
            SchemaName = "comis";
            Delimiter = '\t';
            HasHeader = true;

            EtlCommand = new List<SqlCommand>();
            SqlCommand etlCommand = null;

            string fileName = Path.GetFileName(FilePath);
            string procName;
            // Match fileName with entity
            if (fileName.ToUpper().IndexOf("CBCRSINV") != -1)
            {
                Entity = new CBCRSINV();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("CWAPPL") != -1)
            {
                Entity = new CWAPPL();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("HF_FIRST") != -1)
            {
                Entity = new HF_FIRST();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("SBSTUDNT") != -1)
            {
                Entity = new SBSTUDNT();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("SDDSPS") != -1)
            {
                Entity = new SDDSPS();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("SEEOPS") != -1)
            {
                Entity = new SEEOPS();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("SFAWARDS") != -1)
            {
                Entity = new SFAWARDS();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("SGPOPS") != -1)
            {
                Entity = new SGPOPS();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("SMMATRIC") != -1)
            {
                Entity = new SMMATRIC();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("SPAWARDS") != -1)
            {
                Entity = new SPAWARDS();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("SSSUCCESS") != -1)
            {
                Entity = new SSSUCCESS();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("STTERM") != -1)
            {
                Entity = new STTERM();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("STUDNTID") != -1)
            {
                Entity = new STUDNTID();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("SVVATEA") != -1)
            {
                Entity = new SVVATEA();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("SXENRLM") != -1)
            {
                Entity = new SXENRLM();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("XBSECTON") != -1)
            {
                Entity = new XBSECTON();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("XFSESION") != -1)
            {
                Entity = new XFSESION();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("XFER_BUCKET") != -1)
            {
                Entity = new XFER_BUCKET();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
            }
            else if (fileName.ToUpper().IndexOf("APPLICATION") != -1)
            {
                Entity = new APPLICATION();
                Delimiter = '|';
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
                HasHeader = false;
            }
            else if (fileName.ToUpper().IndexOf("DAS") != -1)
            {
                Entity = new DAS();
                Delimiter = ',';
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
                HasFieldsEnclosedInQuotes = true;
                HasRecordNumber = true;
            }
            else if (fileName.ToUpper().IndexOf("SAASEMENT") != -1)
            {
                Entity = new SAASEMENT();
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
                Delimiter = ',';
            }
            else if (fileName.IndexOf("OccupationalSurvey") != -1)
            {
                Entity = new OccupationalSurvey();
                Delimiter = ',';
                procName = "dbo.GenericSwitch";
                HasRecordNumber = false;
                HasFieldsEnclosedInQuotes = true;
            }
            else
            {
                fileDrop.Status = "Failed";
                fileDrop.ErrorMessage = "Could not determine " + this.GetType().Name + " file type";
                return;
            }

            if (Entity is APPLICATION)
            {
                TableName = "ApplicationNew";
            }
            else
            {
                TableName = Entity.GetType().Name;
            }
            
            StageName = TableName.Substring(0, Math.Min(TableName.Length, 107)) + DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
            FullTableName = string.Format("[{0}].[{1}]", SchemaName, TableName);
            FullStageName = string.Format("[{0}].[{1}]", SchemaName, StageName);


            if (Entity is DAS)
            {
                etlCommand = new SqlCommand(
                    "TableAssimilatePk",
                    new SqlConnection(ConnectionString));
                etlCommand.CommandType = CommandType.StoredProcedure;
                etlCommand.CommandTimeout = 0;
                etlCommand.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "ProductionName",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 517 * 2,
                        Value = FullTableName
                    }
                );
                etlCommand.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "StageName",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 517 * 2,
                        Value = FullStageName
                    }
                );

                EtlCommand.Add(etlCommand);
            }

            etlCommand = new SqlCommand(
                procName,
                new SqlConnection(ConnectionString));
            etlCommand.CommandType = CommandType.StoredProcedure;
            etlCommand.CommandTimeout = 0;
            etlCommand.Parameters.Add(
                new SqlParameter()
                {
                    ParameterName = "SourceSchemaTableName",
                    Direction = ParameterDirection.Input,
                    SqlDbType = SqlDbType.NVarChar,
                    Size = 517 * 2,
                    Value = FullTableName
                }
            );
            etlCommand.Parameters.Add(
                new SqlParameter()
                {
                    ParameterName = "StageSchemaTableName",
                    Direction = ParameterDirection.Input,
                    SqlDbType = SqlDbType.NVarChar,
                    Size = 517 * 2,
                    Value = FullStageName
                }
            );

            EtlCommand.Add(etlCommand);

            // assign properties
            FunctionMap = SqlBulkCopyExt.TextToTypeDictionary(Entity.GetType());

            // Encrypt Columns
            if (Entity is HF_FIRST)
            {
                FunctionMap[3] = s => SqlBulkCopyExt.Encryption(s.Trim(), Encoding.ASCII);
            }
            else if (Entity is STUDNTID)
            {
                FunctionMap[2] = s => SqlBulkCopyExt.Encryption(s.Trim(), Encoding.ASCII);
            }
            else if (Entity is XFER_BUCKET)
            {
                FunctionMap[0] = s => SqlBulkCopyExt.Encryption(s.Trim(), Encoding.ASCII);
            }
            else if (Entity is DAS)
            {
                FunctionMap[3] = s => SqlBulkCopyExt.Encryption(s.Trim(), Encoding.ASCII);
                FunctionMap[14] = s =>
                    {
                        if (String.IsNullOrWhiteSpace(s))
                        {
                            return new DateTime(2099, 12, 31);
                        }
                        else
                        {
                            return Convert.ToDateTime(s);
                        }
                    };
            }
            else if (Entity is SAASEMENT)
            {
                FunctionMap[1] = s => SqlBulkCopyExt.Encryption(s.Trim(), Encoding.ASCII);
            }
        }
    }
}
