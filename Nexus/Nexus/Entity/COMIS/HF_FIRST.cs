﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class HF_FIRST
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("district_id", SqlDbType.Char, 3, false)]
        public string district_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("student_id", SqlDbType.Char, 9, false)]
        public string student_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("student_ssn", SqlDbType.Binary, 64, false)]
        public byte[] student_ssn { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("id_status", SqlDbType.Char, 1)]
        public string id_status { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("ccc_first_term", SqlDbType.Char, 3)]
        public string ccc_first_term { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("ccc_first_term_value", SqlDbType.Char, 5)]
        public string ccc_first_term_value { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("ccc_first_term_loc", SqlDbType.Char, 1)]
        public string ccc_first_term_loc { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("ccc_first_term_nsa", SqlDbType.Char, 3)]
        public string ccc_first_term_nsa { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("ccc_first_term_nsa_value", SqlDbType.Char, 5)]
        public string ccc_first_term_nsa_value { get; set; }
        [ColumnMapping(10, 10)]
        [ColumnMetaData("ccc_first_term_nsa_loc", SqlDbType.Char, 1)]
        public string ccc_first_term_nsa_loc { get; set; }
        [ColumnMapping(11, 11)]
        [ColumnMetaData("ccc_first_term_cr", SqlDbType.Char, 3)]
        public string ccc_first_term_cr { get; set; }
        [ColumnMapping(12, 12)]
        [ColumnMetaData("ccc_first_term_cr_value", SqlDbType.Char, 5)]
        public string ccc_first_term_cr_value { get; set; }
        [ColumnMapping(13, 13)]
        [ColumnMetaData("ccc_first_term_cr_loc", SqlDbType.Char, 1)]
        public string ccc_first_term_cr_loc { get; set; }
        [ColumnMapping(14, 14)]
        [ColumnMetaData("ccc_first_term_cr_nsa", SqlDbType.Char, 3)]
        public string ccc_first_term_cr_nsa { get; set; }
        [ColumnMapping(15, 15)]
        [ColumnMetaData("ccc_first_term_cr_nsa_value", SqlDbType.Char, 5)]
        public string ccc_first_term_cr_nsa_value { get; set; }
        [ColumnMapping(16, 16)]
        [ColumnMetaData("ccc_first_term_cr_nsa_loc", SqlDbType.Char, 1)]
        public string ccc_first_term_cr_nsa_loc { get; set; }
        [ColumnMapping(17, 17)]
        [ColumnMetaData("ccc_first_term_ncr", SqlDbType.Char, 3)]
        public string ccc_first_term_ncr { get; set; }
        [ColumnMapping(18, 18)]
        [ColumnMetaData("ccc_first_term_ncr_value", SqlDbType.Char, 5)]
        public string ccc_first_term_ncr_value { get; set; }
        [ColumnMapping(19, 19)]
        [ColumnMetaData("ccc_first_term_ncr_loc", SqlDbType.Char, 1)]
        public string ccc_first_term_ncr_loc { get; set; }
        [ColumnMapping(20, 20)]
        [ColumnMetaData("ccc_first_term_ncr_nsa", SqlDbType.Char, 3)]
        public string ccc_first_term_ncr_nsa { get; set; }
        [ColumnMapping(21, 21)]
        [ColumnMetaData("ccc_first_term_ncr_nsa_value", SqlDbType.Char, 5)]
        public string ccc_first_term_ncr_nsa_value { get; set; }
        [ColumnMapping(22, 22)]
        [ColumnMetaData("ccc_first_term_ncr_nsa_loc", SqlDbType.Char, 1)]
        public string ccc_first_term_ncr_nsa_loc { get; set; }
        [ColumnMapping(23, 23)]
        [ColumnMetaData("first_term", SqlDbType.Char, 3)]
        public string first_term { get; set; }
        [ColumnMapping(24, 24)]
        [ColumnMetaData("first_term_value", SqlDbType.Char, 5)]
        public string first_term_value { get; set; }
        [ColumnMapping(25, 25)]
        [ColumnMetaData("first_term_loc", SqlDbType.Char, 1)]
        public string first_term_loc { get; set; }
        [ColumnMapping(26, 26)]
        [ColumnMetaData("first_term_nsa", SqlDbType.Char, 3)]
        public string first_term_nsa { get; set; }
        [ColumnMapping(27, 27)]
        [ColumnMetaData("first_term_nsa_value", SqlDbType.Char, 5)]
        public string first_term_nsa_value { get; set; }
        [ColumnMapping(28, 28)]
        [ColumnMetaData("first_term_nsa_loc", SqlDbType.Char, 1)]
        public string first_term_nsa_loc { get; set; }
        [ColumnMapping(29, 29)]
        [ColumnMetaData("first_term_cr", SqlDbType.Char, 3)]
        public string first_term_cr { get; set; }
        [ColumnMapping(30, 30)]
        [ColumnMetaData("first_term_cr_value", SqlDbType.Char, 5)]
        public string first_term_cr_value { get; set; }
        [ColumnMapping(31, 31)]
        [ColumnMetaData("first_term_cr_loc", SqlDbType.Char, 1)]
        public string first_term_cr_loc { get; set; }
        [ColumnMapping(32, 32)]
        [ColumnMetaData("first_term_cr_nsa", SqlDbType.Char, 3)]
        public string first_term_cr_nsa { get; set; }
        [ColumnMapping(33, 33)]
        [ColumnMetaData("first_term_cr_nsa_value", SqlDbType.Char, 5)]
        public string first_term_cr_nsa_value { get; set; }
        [ColumnMapping(34, 34)]
        [ColumnMetaData("first_term_cr_nsa_loc", SqlDbType.Char, 1)]
        public string first_term_cr_nsa_loc { get; set; }
        [ColumnMapping(35, 35)]
        [ColumnMetaData("directed_year", SqlDbType.Char, 9)]
        public string directed_year { get; set; }
        [ColumnMapping(36, 36)]
        [ColumnMetaData("prepared_year", SqlDbType.Char, 9)]
        public string prepared_year { get; set; }
        [ColumnMapping(37, 37)]
        [ColumnMetaData("first_date_4yr", SqlDbType.Char, 8)]
        public string first_date_4yr { get; set; }
        [ColumnMapping(38, 38)]
        [ColumnMetaData("first_segment_4yr", SqlDbType.Char, 3)]
        public string first_segment_4yr { get; set; }
        [ColumnMapping(39, 39)]
        [ColumnMetaData("last_date_4yr", SqlDbType.Char, 8)]
        public string last_date_4yr { get; set; }
        [ColumnMapping(40, 40)]
        [ColumnMetaData("last_segment_4yr", SqlDbType.Char, 3)]
        public string last_segment_4yr { get; set; }
    }
}
