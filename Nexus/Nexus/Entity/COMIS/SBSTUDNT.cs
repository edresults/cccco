﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class SBSTUDNT
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("student_id", SqlDbType.Char, 9, false)]
        public string student_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("birthdate", SqlDbType.DateTime, 10)]
        public DateTime? birthdate { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("term_first", SqlDbType.Char, 3)]
        public string term_first { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("term_last", SqlDbType.Char, 3)]
        public string term_last { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("lep_flag", SqlDbType.Char, 1)]
        public string lep_flag { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("acad_disad_flag", SqlDbType.Char, 1)]
        public string acad_disad_flag { get; set; }
        [ColumnMetaData("dsps_flag", SqlDbType.Char, 1)]
        public string dsps_flag { get; set; }
        [ColumnMetaData("migrant_worker", SqlDbType.Char, 1)]
        public string migrant_worker { get; set; }
        [ColumnMetaData("birthdate_enc", SqlDbType.VarBinary, 128)]
        public string birthdate_enc { get; set; }
    }
}
