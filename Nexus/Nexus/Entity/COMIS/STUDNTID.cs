﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class STUDNTID
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("student_id", SqlDbType.Char, 9, false)]
        public string student_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("ssn", SqlDbType.Binary, 64)]
        public string ssn { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("student_id_status", SqlDbType.Char, 1)]
        public string student_id_status { get; set; }
        [ColumnMetaData("InterSegmentKey", SqlDbType.Binary, 64)]
        public byte[] InterSegmentKey { get; set; }
        [ColumnMetaData("InterSegmentKeyBug", SqlDbType.Binary, 64)]
        public byte[] InterSegmentKeyBug { get; set; }
    }
}
