﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class XFER_BUCKET
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("ssn", SqlDbType.Binary, 64)]
        public Byte[] ssn { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("fice", SqlDbType.Char, 6)]
        public String fice { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("date_of_xfer", SqlDbType.Date, 10)]
        public DateTime? date_of_xfer { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("source", SqlDbType.Char, 4)]
        public String source { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("tfr_in_school_name", SqlDbType.VarChar, 40)]
        public String tfr_in_school_name { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("tfr_in_inst_type", SqlDbType.Char, 1)]
        public String tfr_in_inst_type { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("sector", SqlDbType.Char, 5)]
        public String sector { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("state", SqlDbType.Char, 2)]
        public String state { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("segment", SqlDbType.VarChar, 10)]
        public String segment { get; set; }
    }
}
