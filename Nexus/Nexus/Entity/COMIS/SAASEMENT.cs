﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class SAASEMENT
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("student_id", SqlDbType.Binary, 64, false)]
        public string student_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("term_id", SqlDbType.Char, 3, false)]
        public string term_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("instrument_id", SqlDbType.Char, 4, false)]
        public string instrument_id { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("accommodation", SqlDbType.Char, 4)]
        public string accommodation { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("purpose", SqlDbType.Char, 2, false)]
        public string purpose { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("date_id", SqlDbType.Date, 10, false)]
        public DateTime date_id { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("educational_function_level", SqlDbType.Char, 2)]
        public string educational_function_level { get; set; }

    }
}
