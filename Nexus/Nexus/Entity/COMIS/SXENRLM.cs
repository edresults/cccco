﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class SXENRLM
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("student_id", SqlDbType.Char, 9, false)]
        public string student_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("term_id", SqlDbType.Char, 3, false)]
        public string term_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("course_id", SqlDbType.VarChar, 12, false)]
        public string course_id { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("section_id", SqlDbType.VarChar, 6, false)]
        public string section_id { get; set; }
        [ColumnMapping(11, 5)]
        [ColumnMetaData("control_number", SqlDbType.VarChar, 12)]
        public string control_number { get; set; }
        [ColumnMetaData("effect_date", SqlDbType.Date, 10)]
        public DateTime? effect_date { get; set; }
        [ColumnMetaData("drop_date", SqlDbType.Date, 10)]
        public DateTime? drop_date { get; set; }
        [ColumnMapping(10, 8)]
        [ColumnMetaData("credit_flag", SqlDbType.Char, 1)]
        public string credit_flag { get; set; }
        [ColumnMapping(5, 9)]
        [ColumnMetaData("units_attempted", SqlDbType.Decimal, 4, 2)]
        public Decimal? units_attempted { get; set; }
        [ColumnMapping(6, 10)]
        [ColumnMetaData("units", SqlDbType.Decimal, 4, 2)]
        public Decimal? units { get; set; }
        [ColumnMapping(7, 11)]
        [ColumnMetaData("grade", SqlDbType.VarChar, 3)]
        public string grade { get; set; }
        [ColumnMapping(8, 12)]
        [ColumnMetaData("attend_hours", SqlDbType.Decimal, 5, 1)]
        public Decimal? attend_hours { get; set; }
        [ColumnMetaData("census_status", SqlDbType.Char, 1)]
        public string census_status { get; set; }
        [ColumnMapping(9, 14)]
        [ColumnMetaData("total_hours", SqlDbType.Decimal, 7, 1)]
        public Decimal? total_hours { get; set; }
        [ColumnMapping(12, 15)]
        [ColumnMetaData("apportionment", SqlDbType.Char, 1)]
        public string apportionment { get; set; }
    }
}
