﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class XFSESION
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("term_id", SqlDbType.Char, 3, false)]
        public string term_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("course_id", SqlDbType.VarChar, 12, false)]
        public string course_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("section_id", SqlDbType.VarChar, 6, false)]
        public string section_id { get; set; }
        [ColumnMapping(6, 4)]
        [ColumnMetaData("control_number", SqlDbType.Char, 12, false)]
        public string control_number { get; set; }
        [ColumnMapping(4, 5)]
        [ColumnMetaData("session_id", SqlDbType.Char, 2, false)]
        public string session_id { get; set; }
        [ColumnMapping(5, 6)]
        [ColumnMetaData("instruction", SqlDbType.Char, 2)]
        public string instruction { get; set; }
        [ColumnMetaData("date_begin", SqlDbType.Char, 8)]
        public string date_begin { get; set; }
        [ColumnMetaData("date_end", SqlDbType.Char, 8)]
        public string date_end { get; set; }
        [ColumnMetaData("days", SqlDbType.Char, 9)]
        public string days { get; set; }
        [ColumnMetaData("time_start", SqlDbType.Char, 4)]
        public string time_start { get; set; }
        [ColumnMetaData("time_end", SqlDbType.Char, 4)]
        public string time_end { get; set; }
        [ColumnMetaData("hours", SqlDbType.Decimal, 5, 1)]
        public Decimal? hours { get; set; }
    }
}
