﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class SPAWARDS
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("student_id", SqlDbType.Char, 9, false)]
        public string student_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("term_id", SqlDbType.Char, 3, false)]
        public string term_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("top_code", SqlDbType.Char, 6, false)]
        public string top_code { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("award", SqlDbType.Char, 1, false)]
        public string award { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("date", SqlDbType.DateTime, 10, false)]
        public DateTime date { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("record_id", SqlDbType.Char, 1, false)]
        public string record_id { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("program_code", SqlDbType.Char, 5, false)]
        public string program_code { get; set; }
    }
}
