﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Nexus.Entity.Constraint
{
    class NullConstraint
    {
        public string ColumnName { get; set; }
        public bool IsNullable { get; set; }

        public static IEnumerable<NullConstraint> Get(INexus iNexus)
        {
            string ckStructure = @"SELECT ColumnName = c.Name, IsNullable = c.is_nullable FROM sys.columns c inner join sys.columns sc on c.name = sc.name WHERE c.object_id = object_id(@SourceSchemaTableName) and sc.object_id = object_id(@StageSchemaTableName) and c.is_nullable = 0 ORDER BY c.column_id;";

            using (SqlConnection sqlConnection = new SqlConnection(iNexus.ConnectionString))
            using (SqlCommand sqlCommand = new SqlCommand(ckStructure, sqlConnection))
            {
                sqlCommand.CommandType = CommandType.Text;
                // add paramter
                sqlCommand.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "SourceSchemaTableName",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 517 * 2,
                        Value = iNexus.FullTableName
                    }
                );
                sqlCommand.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "StageSchemaTableName",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 517 * 2,
                        Value = iNexus.FullStageName
                    }
                );
                // open connection
                sqlConnection.Open();
                // get data
                using (IDataReader reader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        yield return new NullConstraint()
                        {
                            ColumnName = reader.GetString(0),
                            IsNullable = reader.GetBoolean(1)
                        };
                    }
                }
            }
        }

        public string GetCount(INexus iNexus)
        {
            string query = @"SELECT Errors = count(*) FROM {0} WHERE [{1}] is null;";
            int errors = 0;

            IEnumerable<NullConstraint> constraints = Get(iNexus);

            using (SqlConnection sqlConnection = new SqlConnection(iNexus.ConnectionString))
            {
                sqlConnection.Open();

                foreach (NullConstraint constraint in constraints)
                {
                    // format query string
                    string cmdText = string.Format(query, iNexus.FullStageName, constraint.ColumnName);

                    using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.Text;
                        errors += (int)sqlCommand.ExecuteScalar();
                    }
                }
            }

            if (errors == 0) return string.Empty;
            
            return string.Format("Error:      File has {0} Null Constraint Violations", errors);
        }

        public string GetDetail(INexus iNexus)
        {
            string query = @"SELECT convert(varchar, SubmissionFileRecordNumber) FROM {0} WHERE [{1}] is null;";

            IEnumerable<NullConstraint> constraints = Get(iNexus);
            List<TableConstraint> errors = new List<TableConstraint>();

            foreach (NullConstraint constraint in constraints)
            {
            
                using (SqlConnection sqlConnection = new SqlConnection(iNexus.ConnectionString))
                {
                    sqlConnection.Open();

                    // format query string
                    string cmdText = string.Format(query, iNexus.FullStageName, constraint.ColumnName);

                    using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.Text;

                        using (IDataReader reader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            while (reader.Read())
                            {
                                errors.Add(new TableConstraint()
                                    {
                                        ColumnName = constraint.ColumnName,
                                        RecordNumber = reader.GetString(0)
                                    }
                                );
                            }
                        }
                    }
                }
            }

            int errorsCount = errors.Count;

            if (errorsCount == 0) return string.Empty;

            string output = "Null Constraint Violations :: ColumnName | RecordNumber";
            int padLength = 0;

            for (int errorsIndex = 0; errorsIndex < errorsCount; errorsIndex++)
            {
                int columnNameLength = errors[errorsIndex].ColumnName.Length;

                if (columnNameLength > padLength)
                {
                    padLength = columnNameLength;
                }
            }

            foreach (TableConstraint error in errors)
            {
                string padSpace = new string(' ', padLength - error.ColumnName.Length);

                output += Environment.NewLine + string.Format("{0} | {1}", error.ColumnName + padSpace, error.RecordNumber);
            }

            return output;
        }
    }
}
