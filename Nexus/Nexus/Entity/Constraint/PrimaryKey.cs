﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Nexus.Entity.Constraint
{
    public class PrimaryKey
    {
        public string ConstraintName { get; set; }
        public string ColumnName { get; set; }

        public static IEnumerable<PrimaryKey> Get(INexus iNexus)
        {
            string pkStructure = @"SELECT ConstraintName = i.name, ColumnName = c.name FROM sys.indexes i inner join sys.index_columns ic on i.object_id = ic.object_id and i.index_id = ic.index_id inner join sys.columns c on ic.object_id = c.object_id and c.column_id = ic.column_id inner join sys.columns sc on sc.name = c.name WHERE i.object_id = object_id(@SourceSchemaTableName) and i.is_primary_key = 1 and sc.object_id = object_id(@StageSchemaTableName) and c.is_identity = 0;";

            using (SqlConnection sqlConnection = new SqlConnection(iNexus.ConnectionString))
            using (SqlCommand sqlCommand = new SqlCommand(pkStructure, sqlConnection))
            {
                sqlCommand.CommandType = CommandType.Text;
                // add paramter
                sqlCommand.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "SourceSchemaTableName",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 517 * 2,
                        Value = iNexus.FullTableName
                    }
                );
                sqlCommand.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "StageSchemaTableName",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 517 * 2,
                        Value = iNexus.FullStageName
                    }
                );
                // open connection
                sqlConnection.Open();
                // get data
                using (IDataReader reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        yield return new PrimaryKey()
                        {
                            ConstraintName = reader.GetString(0),
                            ColumnName = reader.GetString(1)
                        };
                    }
                }
            }
        }

        public string GetCount(INexus iNexus)
        {
            string query = @"SELECT Errors = count(occurrence) FROM (SELECT occurrence = row_number() over(partition by {0} order by (SELECT 1)) FROM {1}) a WHERE occurrence > 1;";
            string columns = string.Empty;
            int errors = 0;

            // get primary key columns
            IEnumerable<PrimaryKey> constraints = Get(iNexus);
            // loop through key columns
            foreach (PrimaryKey constraint in constraints)
            {
                columns += constraint.ColumnName + ",";
            }

            if (string.IsNullOrWhiteSpace(columns)) { return string.Empty; }

            // trim last comma
            columns = columns.Substring(0, columns.Length - 1);

            // set command
            string cmdText = string.Format(query, columns, iNexus.FullStageName);

            // exe command
            using (SqlConnection sqlConnection = new SqlConnection(iNexus.ConnectionString))
            using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
            {
                sqlConnection.Open();
                sqlCommand.CommandType = CommandType.Text;
                errors = (int)sqlCommand.ExecuteScalar();
            }

            if (errors == 0) return string.Empty;
            
            return string.Format("Error:      File has {0} Primary Key Violations", errors);
        }

        public string GetDetail(INexus iNexus)
        {
            string query = @"SELECT convert(varchar, SubmissionFileRecordNumber) FROM (SELECT SubmissionFileRecordNumber, occurrence = row_number() over(partition by {0} order by SubmissionFileRecordNumber) FROM {1}) a WHERE occurrence > 1;";
            string columns = string.Empty;

            IEnumerable<PrimaryKey> primaryKeys = Get(iNexus);
            // loop through key columns
            foreach (PrimaryKey primaryKey in primaryKeys)
            {
                columns += primaryKey.ColumnName + ",";
            }

            if (string.IsNullOrWhiteSpace(columns)) return string.Empty;

            // trim last comma
            columns = columns.TrimEnd(',');
            string output = string.Empty;

            using (SqlConnection sqlConnection = new SqlConnection(iNexus.ConnectionString))
            {
                sqlConnection.Open();

                string cmdText = string.Format(query, columns, iNexus.FullStageName);

                using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.Text;

                    using (IDataReader reader = sqlCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            output += Environment.NewLine + string.Format("{0}", reader.GetString(0));
                        }
                    }
                }
            }

            if (string.IsNullOrEmpty(output)) return string.Empty;
            
            return "Primary Key Constraint Violations :: RecordNumber" + output;
        }
    }
}
