﻿using Nexus.CustomAttribute;
using System.Data;

namespace Nexus.Entity.MMAP
{
    class ProspectiveCohort
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("CollegeId", SqlDbType.Char, 3)]
        public string CollegeId { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("StudentId", SqlDbType.Char, 9)]
        public string StudentId { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("IdStatus", SqlDbType.Char, 1)]
        public string IdStatus { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("Birthdate", SqlDbType.Char, 8)]
        public string Birthdate { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("Gender", SqlDbType.Char, 1)]
        public string Gender { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("HighSchool", SqlDbType.Char, 6)]
        public string HighSchool { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("NameFirst", SqlDbType.VarChar, 30)]
        public string NameFirst { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("NameLast", SqlDbType.VarChar, 40)]
        public string NameLast { get; set; }
    }
}
