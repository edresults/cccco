﻿using Microsoft.SqlServer.Server;
using Nexus.CustomAttribute;
using Nexus.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace Nexus.Entity.MMAP
{
    public class Mmap : FileDetail
    {
        public Mmap(FileDrop fileDrop)
        {
            FilePath = fileDrop.TempFileName;
            Delimiter = '\t';
            HasFieldsEnclosedInQuotes = false;
            HasHeader = true;
            HasRecordNumber = true;
            HasValidation = true;
            ConnectionString = "Server=PRO-DAT-SQL-03;Database=calpass;Trusted_Connection=true";
            SchemaName = "mmap";

            SqlCommand etlCommand = null;

            if (fileDrop.SubmissionFileDescriptionId == 7)
            {
                Entity = new ProspectiveCohort();
                TableName = "ProspectiveCohort";

                StageName = TableName.Substring(0, Math.Min(TableName.Length, 107)) + DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
                FullTableName = string.Format("[{0}].[{1}]", SchemaName, TableName);
                FullStageName = string.Format("[{0}].[{1}]", SchemaName, StageName);

                etlCommand = new SqlCommand(
                    string.Format("[{0}].[{1}]", SchemaName, "p_ProspectiveCohort_Delsert"),
                    new SqlConnection(ConnectionString)
                );
                etlCommand.CommandType = CommandType.StoredProcedure;
                etlCommand.CommandTimeout = 0;
                etlCommand.Parameters.Add(
                     new SqlParameter()
                     {
                         ParameterName = "SubmissionFileId",
                         Direction = ParameterDirection.Input,
                         SqlDbType = SqlDbType.Int,
                         Value = fileDrop.SubmissionFileId
                     }
                 );
                etlCommand.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "StageSchemaTableName",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 517 * 2,
                        Value = FullStageName
                    }
                );
                ExportCommand = new SqlCommand(
                    string.Format("[{0}].[{1}]", SchemaName, "p_ProspectiveCohort_Export"),
                    new SqlConnection(ConnectionString)
                );
                ExportCommand.CommandType = CommandType.StoredProcedure;
                ExportCommand.CommandTimeout = 0;
                ExportCommand.Parameters.Add(
                     new SqlParameter()
                     {
                         ParameterName = "SubmissionFileId",
                         Direction = ParameterDirection.Input,
                         SqlDbType = SqlDbType.Int,
                         Value = fileDrop.SubmissionFileId
                     }
                 );
                EvaluateCommand = new SqlCommand(
                    string.Format("[{0}].[{1}]", SchemaName, "p_ProspectiveCohort_Eval"),
                    new SqlConnection(ConnectionString)
                );
                EvaluateCommand.CommandType = CommandType.StoredProcedure;
                EvaluateCommand.CommandTimeout = 0;
                EvaluateCommand.Parameters.Add(
                     new SqlParameter()
                     {
                         ParameterName = "SubmissionFileId",
                         Direction = ParameterDirection.Input,
                         SqlDbType = SqlDbType.Int,
                         Value = fileDrop.SubmissionFileId
                     }
                 );

            }
            else if (fileDrop.SubmissionFileDescriptionId == 8)
            {
                Entity = new ValidationAssessment();
                TableName = "ValidationAssessment";

                StageName = TableName.Substring(0, Math.Min(TableName.Length, 107)) + DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
                FullTableName = string.Format("[{0}].[{1}]", SchemaName, TableName);
                FullStageName = string.Format("[{0}].[{1}]", SchemaName, StageName);

                etlCommand = new SqlCommand(
                    string.Format("[{0}].[{1}]", SchemaName, "p_ValidationAssessment_Delsert"),
                    new SqlConnection(ConnectionString)
                );
                etlCommand.CommandType = CommandType.StoredProcedure;
                etlCommand.CommandTimeout = 0;
                etlCommand.Parameters.Add(
                     new SqlParameter()
                     {
                         ParameterName = "SubmissionFileId",
                         Direction = ParameterDirection.Input,
                         SqlDbType = SqlDbType.Int,
                         Value = fileDrop.SubmissionFileId
                     }
                 );
                etlCommand.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "StageSchemaTableName",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 517 * 2,
                        Value = FullStageName
                    }
                );
            }
            else
            {
                fileDrop.Status = "Failed";
                fileDrop.ErrorMessage = "Could not determine MMAP file type";
                return;
            }

            EtlCommand = new List<SqlCommand>();
            EtlCommand.Add(etlCommand);

            // assign properties
            FunctionMap = SqlBulkCopyExt.TextToTypeDictionary(Entity.GetType());
        }
    }
}