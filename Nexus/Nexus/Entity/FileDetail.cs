﻿using Microsoft.SqlServer.Server;
using Nexus.CustomAttribute;
using Nexus.Entity.Constraint;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Nexus.Entity
{
    public class FileDetail : INexus
    {
        public string FilePath { get; set; }
        public char? Delimiter { get; set; }
        public bool HasFieldsEnclosedInQuotes { get; set; }
        public bool HasHeader { get; set; }
        public bool HasRecordNumber { get; set; }
        public bool HasValidation { get; set; }
        public string ConnectionString { get; set; }
        public string SchemaName { get; set; }
        public string TableName { get; set; }
        public string StageName { get; set; }
        public string FullTableName { get; set; }
        public string FullStageName { get; set; }
        public object Entity { get; set; }
        public long RecordCount { get; set; }
        public Dictionary<int, Func<string, object>> FunctionMap { get; set; }
        public List<SqlCommand> EtlCommand { get; set; }
        public SqlCommand ExportCommand { get; set; }
        public SqlCommand EvaluateCommand { get; set; }

        private string StageTableColumnString()
        {
            string columns = string.Empty;
            IEnumerable<ColumnMetaData> items = ColumnMetaData.GetColumnMetaData(Entity.GetType());

            foreach (ColumnMetaData item in items)
            {
                columns += item.ColumnName + " " + item.ColumnType.ToString().ToLower();

                switch (item.ColumnType)
                {
                    case SqlDbType.Char:
                    case SqlDbType.NChar:
                    case SqlDbType.VarChar:
                    case SqlDbType.NVarChar:
                    case SqlDbType.Text:
                    case SqlDbType.NText:
                    case SqlDbType.Binary:
                    case SqlDbType.VarBinary:
                        if (item.ColumnLength == -1)
                        {
                            columns += "(max)";

                        }
                        else
                        {
                            columns += string.Format("({0})", item.ColumnLength);
                        }
                        break;

                    case SqlDbType.Decimal:
                        columns += string.Format("({0},{1})", item.ColumnPrecision, item.ColumnScale);
                        break;

                    case SqlDbType.Float:
                    case SqlDbType.Real:
                        columns += string.Format("({0}", item.ColumnPrecision);
                        break;
                }

                if (item.IsNullable)
                {
                    columns += " null";
                }
                else
                {
                    columns += " not null";
                }

                columns += ",";

            }

            columns.TrimEnd(',');

            return columns;
        }

        public void StageTableCreate()
        {
            string columns = StageTableColumnString();

            string cmdText = string.Format("CREATE TABLE {0} ({1});", FullStageName, columns);

            if (HasRecordNumber)
            {
                cmdText += String.Format(@"ALTER TABLE {0} ADD SubmissionFileRecordNumber int identity({1}, 1);", FullStageName, 1 + Convert.ToInt32(HasHeader));
            }

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConnectionString))
                using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
                {
                    sqlConnection.Open();
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public void StageTableDrop()
        {
            string cmdText = string.Format("IF (object_id('{0}') is not null) DROP TABLE {0};", FullStageName);

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConnectionString))
                using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
                {
                    sqlConnection.Open();
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }
    }
}
