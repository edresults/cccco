﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.UNIVERSITY
{
    class UnivCourseProdv2
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("School", SqlDbType.Char, 6)]
        public String School { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("YrTerm", SqlDbType.Char, 5)]
        public String YrTerm { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("StudentId", SqlDbType.Binary, 64)]
        public String StudentId { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("CourseTitle", SqlDbType.VarChar, 40)]
        public String CourseTitle { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("CourseAbbr", SqlDbType.VarChar, 8)]
        public String CourseAbbr { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("CourseNum", SqlDbType.VarChar, 5)]
        public String CourseNum { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("CourseNumSuffix", SqlDbType.VarChar, 4)]
        public String CourseNumSuffix { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("Units", SqlDbType.VarChar, 6)]
        public String Units { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("Grade", SqlDbType.VarChar, 3)]
        public String Grade { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("Remedial", SqlDbType.Char, 1)]
        public String Remedial { get; set; }
    }
}
