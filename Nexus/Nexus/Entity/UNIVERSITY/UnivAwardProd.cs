﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.UNIVERSITY
{
    class UnivAwardProd
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("School", SqlDbType.Char, 6)]
        public String School { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("YrTerm", SqlDbType.Char, 5)]
        public String YrTerm { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("StudentId", SqlDbType.Binary, 64)]
        public String StudentId { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("DegreeType", SqlDbType.Char, 1)]
        public String DegreeType { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("Major1", SqlDbType.Char, 6)]
        public String Major1 { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("Major2", SqlDbType.Char, 6)]
        public String Major2 { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("Major3", SqlDbType.Char, 6)]
        public String Major3 { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("AwardDate", SqlDbType.Char, 8)]
        public String AwardDate { get; set; }
    }
}
