﻿using Microsoft.SqlServer.Server;
using Nexus.CustomAttribute;
using Nexus.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace Nexus.Entity.UNIVERSITY
{
    public class University : FileDetail
    {

        public static void PostProcessing()
        {
            string connString = "Server=PRO-DAT-SQL-03;Database=calpass;Trusted_Connection=true";
            string commString = "dbo.UnivPostProcessing";

            Console.WriteLine("University: {0}", commString);

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                using (SqlCommand comm = new SqlCommand(commString, conn))
                {
                    conn.Open();
                    comm.CommandType = CommandType.StoredProcedure;
                    comm.CommandTimeout = 0;
                    comm.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public University(FileDrop fileDrop)
        {
            FilePath = fileDrop.TempFileName;
            HasFieldsEnclosedInQuotes = true;
            HasRecordNumber = true;
            HasValidation = false;
            ConnectionString = "Server=PRO-DAT-SQL-03;Database=calpass;Trusted_Connection=true";
            SchemaName = "dbo";

            SqlCommand etlCommand = null;

            // get first line of file
            string line;

            using (StreamReader sr = new StreamReader(FilePath))
            {
                line = sr.ReadLine() ?? "";
            }

            // FileDetail
            Delimiter = ',';
            HasHeader = false;

            int fieldCount;

            // cound fields
            fieldCount = line.QualifySplit((char)Delimiter, '"').Length;

            if (fieldCount == 8 && line.QualifySplit((char)Delimiter, '"')[4].Length == 6)
            {
                Entity = new UnivAwardProd();
                TableName = Entity.GetType().Name;
            }
            else if (fieldCount == 8 && line.QualifySplit((char)Delimiter, '"')[4].Length <= 1)
            {
                Entity = new UnivCharProd();
                TableName = Entity.GetType().Name;
            }
            else if (fieldCount == 9)
            {
                Entity = new UnivCourseProdv1();
                TableName = Entity.GetType().Name;
            }
            else if (fieldCount == 10)
            {
                Entity = new UnivCourseProdv2();
                TableName = Entity.GetType().Name;
            }
            else if (fieldCount == 25)
            {
                Entity = new UnivStudentProdv1();
                TableName = Entity.GetType().Name;
            }
            else if (fieldCount == 27)
            {
                Entity = new UnivStudentProdv2();
                TableName = Entity.GetType().Name;
            }
            else
            {
                fileDrop.Status = "Failed";
                fileDrop.ErrorMessage = "Could not determine University file type";
                return;
            }

            StageName = TableName.Substring(0, Math.Min(TableName.Length, 107)) + DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
            FullTableName = string.Format("[{0}].[{1}]", SchemaName, TableName);
            FullStageName = string.Format("[{0}].[{1}]", SchemaName, StageName);

            etlCommand = new SqlCommand(
                string.Format("[{0}].[{1}]", SchemaName, Entity.GetType().Name + "Merge"),
                new SqlConnection(ConnectionString));
            etlCommand.CommandType = CommandType.StoredProcedure;
            etlCommand.CommandTimeout = 0;
            etlCommand.Parameters.Add(
                new SqlParameter()
                {
                    ParameterName = "StageSchemaTableName",
                    Direction = ParameterDirection.Input,
                    SqlDbType = SqlDbType.NVarChar,
                    Size = 517 * 2,
                    Value = FullStageName
                }
            );

            EtlCommand = new List<SqlCommand>();
            EtlCommand.Add(etlCommand);

            // assign properties
            FunctionMap = SqlBulkCopyExt.TextToTypeDictionary(Entity.GetType());

            // Encrypt Columns
            if (TableName != "UnivCharProd")
            {
                FunctionMap[2] = s => SqlBulkCopyExt.Encryption(s.Trim(), Encoding.ASCII);
            }
        }
    }
}
