﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.UNIVERSITY
{
    class UnivStudentProdv1
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("School", SqlDbType.Char, 6)]
        public String School { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("YrTerm", SqlDbType.Char, 5)]
        public String YrTerm { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("StudentId", SqlDbType.Binary, 64)]
        public String StudentId { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("IdStatus", SqlDbType.Char, 1)]
        public String IdStatus { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("CSISNum", SqlDbType.Char, 10)]
        public String CSISNum { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("Fname", SqlDbType.Char, 30)]
        public String Fname { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("Lname", SqlDbType.Char, 40)]
        public String Lname { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("Gender", SqlDbType.Char, 1)]
        public String Gender { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("Ethnicity", SqlDbType.Char, 3)]
        public String Ethnicity { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("Birthdate", SqlDbType.Char, 8)]
        public String Birthdate { get; set; }
        [ColumnMapping(10, 10)]
        [ColumnMetaData("InstOrigin", SqlDbType.Char, 6)]
        public String InstOrigin { get; set; }
        [ColumnMapping(11, 11)]
        [ColumnMetaData("AdmitBasis", SqlDbType.Char, 2)]
        public String AdmitBasis { get; set; }
        [ColumnMapping(12, 12)]
        [ColumnMetaData("Major", SqlDbType.Char, 6)]
        public String Major { get; set; }
        [ColumnMapping(13, 13)]
        [ColumnMetaData("EnrollStatus", SqlDbType.Char, 1)]
        public String EnrollStatus { get; set; }
        [ColumnMapping(14, 14)]
        [ColumnMetaData("StudentLevel", SqlDbType.Char, 1)]
        public String StudentLevel { get; set; }
        [ColumnMapping(15, 15)]
        [ColumnMetaData("UnitsEarnedTerm", SqlDbType.VarChar, 7)]
        public String UnitsEarnedTerm { get; set; }
        [ColumnMapping(16, 16)]
        [ColumnMetaData("TermGPA", SqlDbType.VarChar, 5)]
        public String TermGPA { get; set; }
        [ColumnMapping(17, 17)]
        [ColumnMetaData("UnitsEarnedLocal", SqlDbType.VarChar, 7)]
        public String UnitsEarnedLocal { get; set; }
        [ColumnMapping(18, 18)]
        [ColumnMetaData("CumeGPA", SqlDbType.VarChar, 5)]
        public String CumeGPA { get; set; }
        [ColumnMapping(19, 19)]
        [ColumnMetaData("TotalUnitsEarned", SqlDbType.VarChar, 7)]
        public String TotalUnitsEarned { get; set; }
        [ColumnMapping(20, 20)]
        [ColumnMetaData("TotalUnitsEnroll", SqlDbType.VarChar, 7)]
        public String TotalUnitsEnroll { get; set; }
        [ColumnMapping(21, 21)]
        [ColumnMetaData("XferGPA", SqlDbType.VarChar, 5)]
        public String XferGPA { get; set; }
        [ColumnMapping(22, 22)]
        [ColumnMetaData("XferUnitsEarned", SqlDbType.VarChar, 7)]
        public String XferUnitsEarned { get; set; }
        [ColumnMapping(23, 23)]
        [ColumnMetaData("HispanicEthnicity", SqlDbType.Char, 1)]
        public String HispanicEthnicity { get; set; }
        [ColumnMapping(24, 24)]
        [ColumnMetaData("MultipleEthnicity", SqlDbType.VarChar, 10)]
        public String MultipleEthnicity { get; set; }
    }
}
