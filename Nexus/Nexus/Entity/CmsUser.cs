﻿using System;

namespace Nexus.Entity
{
    public class CmsUser
    {
        public int? UserId { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
    }
}