﻿using Nexus.CustomAttribute;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus.Entity.StudentAthleteFactory
{
    class StudentAthletes
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("FirstName", SqlDbType.VarChar, 30, false)]
        public string FirstName { get; set; }

        [ColumnMapping(1, 1)]
        [ColumnMetaData("LastName", SqlDbType.VarChar, 40, false)]
        public string LastName { get; set; }

        [ColumnMapping(2, 2)]
        [ColumnMetaData("Birthdate", SqlDbType.Date, 10, false)]
        public DateTime Birthdate { get; set; }

        [ColumnMapping(3, 3)]
        [ColumnMetaData("StudentId", SqlDbType.Binary, 64, false)]
        public string StudentId { get; set; }

        [ColumnMapping(4, 4)]
        [ColumnMetaData("SportYear", SqlDbType.Char, 9, false)]
        public string SportYear { get; set; }

        [ColumnMapping(5, 5)]
        [ColumnMetaData("SportName", SqlDbType.VarChar, 20, false)]
        public string SportName { get; set; }

        [ColumnMapping(6, 6)]
        [ColumnMetaData("SportSex", SqlDbType.VarChar, 7, false)]
        public string SportSex { get; set; }

        [ColumnMapping(7, 7)]
        [ColumnMetaData("CollegeName", SqlDbType.VarChar, 40, false)]
        public string CollegeName { get; set; }
    }
}
