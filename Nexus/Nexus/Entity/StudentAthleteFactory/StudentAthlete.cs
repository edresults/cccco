﻿using Microsoft.SqlServer.Server;
using Nexus.CustomAttribute;
using Nexus.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace Nexus.Entity.StudentAthleteFactory
{
    public class StudentAthlete : FileDetail
    {
        public StudentAthlete(FileDrop fileDrop)
        {
            FilePath = fileDrop.TempFileName;
            Delimiter = ',';
            HasFieldsEnclosedInQuotes = true;
            HasHeader = true;
            HasRecordNumber = false;
            HasValidation = false;
            ConnectionString = "Server=PRO-DAT-SQL-03;Database=calpass;Trusted_Connection=true";
            SchemaName = "dbo";

            SqlCommand etlCommand = null;

            Entity = new StudentAthletes();

            TableName = "StudentAthletes";
            StageName = Entity.GetType().Name.Substring(0, Math.Min(Entity.GetType().Name.Length, 107)) + DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
            FullTableName = string.Format("[{0}].[{1}]", SchemaName, TableName);
            FullStageName = string.Format("[{0}].[{1}]", SchemaName, StageName);

            etlCommand = new SqlCommand(
                "[dbo].[SimpleMerge]",
                new SqlConnection(ConnectionString)
            );
            etlCommand.CommandType = CommandType.StoredProcedure;
            etlCommand.CommandTimeout = 0;
            etlCommand.Parameters.Add(
                new SqlParameter()
                {
                    ParameterName = "TargetName",
                    Direction = ParameterDirection.Input,
                    SqlDbType = SqlDbType.NVarChar,
                    Size = 517 * 2,
                    Value = FullTableName
                }
            );
            etlCommand.Parameters.Add(
                new SqlParameter()
                {
                    ParameterName = "SourceName",
                    Direction = ParameterDirection.Input,
                    SqlDbType = SqlDbType.NVarChar,
                    Size = 517 * 2,
                    Value = FullStageName
                }
            );

            EtlCommand = new List<SqlCommand>();
            EtlCommand.Add(etlCommand);

            // assign properties
            FunctionMap = SqlBulkCopyExt.TextToTypeDictionary(Entity.GetType());

            FunctionMap[3] = s => SqlBulkCopyExt.Encryption(s.Trim(), Encoding.ASCII);
        }
    }
}
