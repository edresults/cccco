﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.CAASPP
{
    class Caaspp20172018
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("col0", SqlDbType.Char, 2)]
        public string col0 { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("col1", SqlDbType.Char, 10)]
        public string col1 { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("col2", SqlDbType.Char, 50)]
        public string col2 { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("col3", SqlDbType.Char, 30)]
        public string col3 { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("col4", SqlDbType.Char, 30)]
        public string col4 { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("col5", SqlDbType.Char, 10)]
        public string col5 { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("col6", SqlDbType.Char, 6)]
        public string col6 { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("col7", SqlDbType.Char, 2)]
        public string col7 { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("col8", SqlDbType.Char, 2)]
        public string col8 { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("col9", SqlDbType.Char, 14)]
        public string col9 { get; set; }
        [ColumnMapping(10, 10)]
        [ColumnMetaData("col10", SqlDbType.Char, 14)]
        public string col10 { get; set; }
        [ColumnMapping(11, 11)]
        [ColumnMetaData("col11", SqlDbType.Char, 4)]
        public string col11 { get; set; }
        [ColumnMapping(12, 12)]
        [ColumnMetaData("col12", SqlDbType.Char, 2)]
        public string col12 { get; set; }
        [ColumnMapping(13, 13)]
        [ColumnMetaData("col13", SqlDbType.Char, 7)]
        public string col13 { get; set; }
        [ColumnMapping(14, 14)]
        [ColumnMetaData("col14", SqlDbType.Char, 3)]
        public string col14 { get; set; }
        [ColumnMapping(15, 15)]
        [ColumnMetaData("col15", SqlDbType.Char, 3)]
        public string col15 { get; set; }
        [ColumnMapping(16, 16)]
        [ColumnMetaData("col16", SqlDbType.Char, 3)]
        public string col16 { get; set; }
        [ColumnMapping(17, 17)]
        [ColumnMetaData("col17", SqlDbType.Char, 3)]
        public string col17 { get; set; }
        [ColumnMapping(18, 18)]
        [ColumnMetaData("col18", SqlDbType.Char, 3)]
        public string col18 { get; set; }
        [ColumnMapping(19, 19)]
        [ColumnMetaData("col19", SqlDbType.Char, 10)]
        public string col19 { get; set; }
        [ColumnMapping(20, 20)]
        [ColumnMetaData("col20", SqlDbType.Char, 10)]
        public string col20 { get; set; }
        [ColumnMapping(21, 21)]
        [ColumnMetaData("col21", SqlDbType.Char, 10)]
        public string col21 { get; set; }
        [ColumnMapping(22, 22)]
        [ColumnMetaData("col22", SqlDbType.Char, 4)]
        public string col22 { get; set; }
        [ColumnMapping(23, 23)]
        [ColumnMetaData("col23", SqlDbType.Char, 3)]
        public string col23 { get; set; }
        [ColumnMapping(24, 24)]
        [ColumnMetaData("col24", SqlDbType.Char, 3)]
        public string col24 { get; set; }
        [ColumnMapping(25, 25)]
        [ColumnMetaData("col25", SqlDbType.Char, 1)]
        public string col25 { get; set; }
        [ColumnMapping(26, 26)]
        [ColumnMetaData("col26", SqlDbType.Char, 3)]
        public string col26 { get; set; }
        [ColumnMapping(27, 27)]
        [ColumnMetaData("col27", SqlDbType.Char, 3)]
        public string col27 { get; set; }
        [ColumnMapping(28, 28)]
        [ColumnMetaData("col28", SqlDbType.Char, 3)]
        public string col28 { get; set; }
        [ColumnMapping(29, 29)]
        [ColumnMetaData("col29", SqlDbType.Char, 3)]
        public string col29 { get; set; }
        [ColumnMapping(30, 30)]
        [ColumnMetaData("col30", SqlDbType.Char, 3)]
        public string col30 { get; set; }
        [ColumnMapping(31, 31)]
        [ColumnMetaData("col31", SqlDbType.Char, 3)]
        public string col31 { get; set; }
        [ColumnMapping(32, 32)]
        [ColumnMetaData("col32", SqlDbType.Char, 3)]
        public string col32 { get; set; }
        [ColumnMapping(33, 33)]
        [ColumnMetaData("col33", SqlDbType.Char, 3)]
        public string col33 { get; set; }
        [ColumnMapping(34, 34)]
        [ColumnMetaData("col34", SqlDbType.Char, 3)]
        public string col34 { get; set; }
        [ColumnMapping(35, 35)]
        [ColumnMetaData("col35", SqlDbType.Char, 2)]
        public string col35 { get; set; }
        [ColumnMapping(36, 36)]
        [ColumnMetaData("col36", SqlDbType.Char, 1)]
        public string col36 { get; set; }
        [ColumnMapping(37, 37)]
        [ColumnMetaData("col37", SqlDbType.Char, 16)]
        public string col37 { get; set; }
        [ColumnMapping(38, 38)]
        [ColumnMetaData("col38", SqlDbType.Char, 1)]
        public string col38 { get; set; }
        [ColumnMapping(39, 39)]
        [ColumnMetaData("col39", SqlDbType.Char, 16)]
        public string col39 { get; set; }
        [ColumnMapping(40, 40)]
        [ColumnMetaData("col40", SqlDbType.Char, 1)]
        public string col40 { get; set; }
        [ColumnMapping(41, 41)]
        [ColumnMetaData("col41", SqlDbType.Char, 16)]
        public string col41 { get; set; }
        [ColumnMapping(42, 42)]
        [ColumnMetaData("col42", SqlDbType.Char, 1)]
        public string col42 { get; set; }
        [ColumnMapping(43, 43)]
        [ColumnMetaData("col43", SqlDbType.Char, 16)]
        public string col43 { get; set; }
        [ColumnMapping(44, 44)]
        [ColumnMetaData("col44", SqlDbType.Char, 100)]
        public string col44 { get; set; }
        [ColumnMapping(45, 45)]
        [ColumnMetaData("col45", SqlDbType.Char, 14)]
        public string col45 { get; set; }
        [ColumnMapping(46, 46)]
        [ColumnMetaData("col46", SqlDbType.Char, 100)]
        public string col46 { get; set; }
        [ColumnMapping(47, 47)]
        [ColumnMetaData("col47", SqlDbType.Char, 14)]
        public string col47 { get; set; }
        [ColumnMapping(48, 48)]
        [ColumnMetaData("col48", SqlDbType.Char, 2)]
        public string col48 { get; set; }
        [ColumnMapping(49, 49)]
        [ColumnMetaData("col49", SqlDbType.Char, 4)]
        public string col49 { get; set; }
        [ColumnMapping(50, 50)]
        [ColumnMetaData("col50", SqlDbType.Char, 1)]
        public string col50 { get; set; }
        [ColumnMapping(51, 51)]
        [ColumnMetaData("col51", SqlDbType.Char, 10)]
        public string col51 { get; set; }
        [ColumnMapping(52, 52)]
        [ColumnMetaData("col52", SqlDbType.Char, 100)]
        public string col52 { get; set; }
        [ColumnMapping(53, 53)]
        [ColumnMetaData("col53", SqlDbType.Char, 14)]
        public string col53 { get; set; }
        [ColumnMapping(54, 54)]
        [ColumnMetaData("col54", SqlDbType.Char, 100)]
        public string col54 { get; set; }
        [ColumnMapping(55, 55)]
        [ColumnMetaData("col55", SqlDbType.Char, 14)]
        public string col55 { get; set; }
        [ColumnMapping(56, 56)]
        [ColumnMetaData("col56", SqlDbType.Char, 2)]
        public string col56 { get; set; }
        [ColumnMapping(57, 57)]
        [ColumnMetaData("col57", SqlDbType.Char, 4)]
        public string col57 { get; set; }
        [ColumnMapping(58, 58)]
        [ColumnMetaData("col58", SqlDbType.Char, 1)]
        public string col58 { get; set; }
        [ColumnMapping(59, 59)]
        [ColumnMetaData("col59", SqlDbType.Char, 100)]
        public string col59 { get; set; }
        [ColumnMapping(60, 60)]
        [ColumnMetaData("col60", SqlDbType.Char, 14)]
        public string col60 { get; set; }
        [ColumnMapping(61, 61)]
        [ColumnMetaData("col61", SqlDbType.Char, 100)]
        public string col61 { get; set; }
        [ColumnMapping(62, 62)]
        [ColumnMetaData("col62", SqlDbType.Char, 14)]
        public string col62 { get; set; }
        [ColumnMapping(63, 63)]
        [ColumnMetaData("col63", SqlDbType.Char, 2)]
        public string col63 { get; set; }
        [ColumnMapping(64, 64)]
        [ColumnMetaData("col64", SqlDbType.Char, 4)]
        public string col64 { get; set; }
        [ColumnMapping(65, 65)]
        [ColumnMetaData("col65", SqlDbType.Char, 1)]
        public string col65 { get; set; }
        [ColumnMapping(66, 66)]
        [ColumnMetaData("col66", SqlDbType.Char, 10)]
        public string col66 { get; set; }
        [ColumnMapping(67, 67)]
        [ColumnMetaData("col67", SqlDbType.Char, 10)]
        public string col67 { get; set; }
        [ColumnMapping(68, 68)]
        [ColumnMetaData("col68", SqlDbType.Char, 10)]
        public string col68 { get; set; }
        [ColumnMapping(69, 69)]
        [ColumnMetaData("col69", SqlDbType.Char, 10)]
        public string col69 { get; set; }
        [ColumnMapping(70, 70)]
        [ColumnMetaData("col70", SqlDbType.Char, 10)]
        public string col70 { get; set; }
        [ColumnMapping(71, 71)]
        [ColumnMetaData("col71", SqlDbType.Char, 10)]
        public string col71 { get; set; }
        [ColumnMapping(72, 72)]
        [ColumnMetaData("col72", SqlDbType.Char, 10)]
        public string col72 { get; set; }
        [ColumnMapping(73, 73)]
        [ColumnMetaData("col73", SqlDbType.Char, 10)]
        public string col73 { get; set; }
        [ColumnMapping(74, 74)]
        [ColumnMetaData("col74", SqlDbType.Char, 10)]
        public string col74 { get; set; }
        [ColumnMapping(75, 75)]
        [ColumnMetaData("col75", SqlDbType.Char, 10)]
        public string col75 { get; set; }
        [ColumnMapping(76, 76)]
        [ColumnMetaData("col76", SqlDbType.Char, 10)]
        public string col76 { get; set; }
        [ColumnMapping(77, 77)]
        [ColumnMetaData("col77", SqlDbType.Char, 10)]
        public string col77 { get; set; }
        [ColumnMapping(78, 78)]
        [ColumnMetaData("col78", SqlDbType.Char, 4)]
        public string col78 { get; set; }
        [ColumnMapping(79, 79)]
        [ColumnMetaData("col79", SqlDbType.Char, 10)]
        public string col79 { get; set; }
        [ColumnMapping(80, 80)]
        [ColumnMetaData("col80", SqlDbType.Char, 10)]
        public string col80 { get; set; }
        [ColumnMapping(81, 81)]
        [ColumnMetaData("col81", SqlDbType.Char, 1)]
        public string col81 { get; set; }
        [ColumnMapping(82, 82)]
        [ColumnMetaData("col82", SqlDbType.Char, 3)]
        public string col82 { get; set; }
        [ColumnMapping(83, 83)]
        [ColumnMetaData("col83", SqlDbType.Char, 1)]
        public string col83 { get; set; }
        [ColumnMapping(84, 84)]
        [ColumnMetaData("col84", SqlDbType.Char, 4)]
        public string col84 { get; set; }
        [ColumnMapping(85, 85)]
        [ColumnMetaData("col85", SqlDbType.Char, 1)]
        public string col85 { get; set; }
        [ColumnMapping(86, 86)]
        [ColumnMetaData("col86", SqlDbType.Char, 1)]
        public string col86 { get; set; }
        [ColumnMapping(87, 87)]
        [ColumnMetaData("col87", SqlDbType.Char, 1)]
        public string col87 { get; set; }
        [ColumnMapping(88, 88)]
        [ColumnMetaData("col88", SqlDbType.Char, 1)]
        public string col88 { get; set; }
        [ColumnMapping(89, 89)]
        [ColumnMetaData("col89", SqlDbType.Char, 1)]
        public string col89 { get; set; }
        [ColumnMapping(90, 90)]
        [ColumnMetaData("col90", SqlDbType.Char, 8)]
        public string col90 { get; set; }
        [ColumnMapping(91, 91)]
        [ColumnMetaData("col91", SqlDbType.Char, 2)]
        public string col91 { get; set; }
        [ColumnMapping(92, 92)]
        [ColumnMetaData("col92", SqlDbType.Char, 4)]
        public string col92 { get; set; }
        [ColumnMapping(93, 93)]
        [ColumnMetaData("col93", SqlDbType.Char, 1)]
        public string col93 { get; set; }
        [ColumnMapping(94, 94)]
        [ColumnMetaData("col94", SqlDbType.Char, 4)]
        public string col94 { get; set; }
        [ColumnMapping(95, 95)]
        [ColumnMetaData("col95", SqlDbType.Char, 1)]
        public string col95 { get; set; }
        [ColumnMapping(96, 96)]
        [ColumnMetaData("col96", SqlDbType.Char, 4)]
        public string col96 { get; set; }
        [ColumnMapping(97, 97)]
        [ColumnMetaData("col97", SqlDbType.Char, 1)]
        public string col97 { get; set; }
        [ColumnMapping(98, 98)]
        [ColumnMetaData("col98", SqlDbType.Char, 4)]
        public string col98 { get; set; }
        [ColumnMapping(99, 99)]
        [ColumnMetaData("col99", SqlDbType.Char, 1)]
        public string col99 { get; set; }
        [ColumnMapping(100, 100)]
        [ColumnMetaData("col100", SqlDbType.Char, 4)]
        public string col100 { get; set; }
        [ColumnMapping(101, 101)]
        [ColumnMetaData("col101", SqlDbType.Char, 4)]
        public string col101 { get; set; }
        [ColumnMapping(102, 102)]
        [ColumnMetaData("col102", SqlDbType.Char, 4)]
        public string col102 { get; set; }
        [ColumnMapping(103, 103)]
        [ColumnMetaData("col103", SqlDbType.Char, 4)]
        public string col103 { get; set; }
        [ColumnMapping(104, 104)]
        [ColumnMetaData("col104", SqlDbType.Char, 1)]
        public string col104 { get; set; }
        [ColumnMapping(105, 105)]
        [ColumnMetaData("col105", SqlDbType.Char, 1)]
        public string col105 { get; set; }
        [ColumnMapping(106, 106)]
        [ColumnMetaData("col106", SqlDbType.Char, 1)]
        public string col106 { get; set; }
        [ColumnMapping(107, 107)]
        [ColumnMetaData("col107", SqlDbType.Char, 3)]
        public string col107 { get; set; }
        [ColumnMapping(108, 108)]
        [ColumnMetaData("col108", SqlDbType.Char, 3)]
        public string col108 { get; set; }
        [ColumnMapping(109, 109)]
        [ColumnMetaData("col109", SqlDbType.Char, 8)]
        public string col109 { get; set; }
        [ColumnMapping(110, 110)]
        [ColumnMetaData("col110", SqlDbType.Char, 16)]
        public string col110 { get; set; }
        [ColumnMapping(111, 111)]
        [ColumnMetaData("col111", SqlDbType.Char, 11)]
        public string col111 { get; set; }
        [ColumnMapping(112, 112)]
        [ColumnMetaData("col112", SqlDbType.Char, 14)]
        public string col112 { get; set; }
        [ColumnMapping(113, 113)]
        [ColumnMetaData("col113", SqlDbType.Char, 8)]
        public string col113 { get; set; }
        [ColumnMapping(114, 114)]
        [ColumnMetaData("col114", SqlDbType.Char, 12)]
        public string col114 { get; set; }
        [ColumnMapping(115, 115)]
        [ColumnMetaData("col115", SqlDbType.Char, 8)]
        public string col115 { get; set; }
        [ColumnMapping(116, 116)]
        [ColumnMetaData("col116", SqlDbType.Char, 11)]
        public string col116 { get; set; }
        [ColumnMapping(117, 117)]
        [ColumnMetaData("col117", SqlDbType.Char, 8)]
        public string col117 { get; set; }
        [ColumnMapping(118, 118)]
        [ColumnMetaData("col118", SqlDbType.Char, 12)]
        public string col118 { get; set; }
        [ColumnMapping(119, 119)]
        [ColumnMetaData("col119", SqlDbType.Char, 10)]
        public string col119 { get; set; }
        [ColumnMapping(120, 120)]
        [ColumnMetaData("col120", SqlDbType.Char, 13)]
        public string col120 { get; set; }
        [ColumnMapping(121, 121)]
        [ColumnMetaData("col121", SqlDbType.Char, 6)]
        public string col121 { get; set; }
        [ColumnMapping(122, 122)]
        [ColumnMetaData("col122", SqlDbType.Char, 8)]
        public string col122 { get; set; }
        [ColumnMapping(123, 123)]
        [ColumnMetaData("col123", SqlDbType.Char, 6)]
        public string col123 { get; set; }
        [ColumnMapping(124, 124)]
        [ColumnMetaData("col124", SqlDbType.Char, 25)]
        public string col124 { get; set; }
        [ColumnMapping(125, 125)]
        [ColumnMetaData("col125", SqlDbType.Char, 14)]
        public string col125 { get; set; }
        [ColumnMapping(126, 126)]
        [ColumnMetaData("col126", SqlDbType.Char, 16)]
        public string col126 { get; set; }
        [ColumnMapping(127, 127)]
        [ColumnMetaData("col127", SqlDbType.Char, 7)]
        public string col127 { get; set; }
        [ColumnMapping(128, 128)]
        [ColumnMetaData("col128", SqlDbType.Char, 10)]
        public string col128 { get; set; }
        [ColumnMapping(129, 129)]
        [ColumnMetaData("col129", SqlDbType.Char, 18)]
        public string col129 { get; set; }
        [ColumnMapping(130, 130)]
        [ColumnMetaData("col130", SqlDbType.Char, 12)]
        public string col130 { get; set; }
        [ColumnMapping(131, 131)]
        [ColumnMetaData("col131", SqlDbType.Char, 10)]
        public string col131 { get; set; }
        [ColumnMapping(132, 132)]
        [ColumnMetaData("col132", SqlDbType.Char, 6)]
        public string col132 { get; set; }
        [ColumnMapping(133, 133)]
        [ColumnMetaData("col133", SqlDbType.Char, 8)]
        public string col133 { get; set; }
        [ColumnMapping(134, 134)]
        [ColumnMetaData("col134", SqlDbType.Char, 6)]
        public string col134 { get; set; }
        [ColumnMapping(135, 135)]
        [ColumnMetaData("col135", SqlDbType.Char, 25)]
        public string col135 { get; set; }
        [ColumnMapping(136, 136)]
        [ColumnMetaData("col136", SqlDbType.Char, 16)]
        public string col136 { get; set; }
        [ColumnMapping(137, 137)]
        [ColumnMetaData("col137", SqlDbType.Char, 7)]
        public string col137 { get; set; }
        [ColumnMapping(138, 138)]
        [ColumnMetaData("col138", SqlDbType.Char, 18)]
        public string col138 { get; set; }
        [ColumnMapping(139, 139)]
        [ColumnMetaData("col139", SqlDbType.Char, 12)]
        public string col139 { get; set; }
        [ColumnMapping(140, 140)]
        [ColumnMetaData("col140", SqlDbType.Char, 10)]
        public string col140 { get; set; }
        [ColumnMapping(141, 141)]
        [ColumnMetaData("col141", SqlDbType.Char, 19)]
        public string col141 { get; set; }
        [ColumnMapping(142, 142)]
        [ColumnMetaData("col142", SqlDbType.Char, 12)]
        public string col142 { get; set; }
        [ColumnMapping(143, 143)]
        [ColumnMetaData("col143", SqlDbType.Char, 16)]
        public string col143 { get; set; }
        [ColumnMapping(144, 144)]
        [ColumnMetaData("col144", SqlDbType.Char, 7)]
        public string col144 { get; set; }
        [ColumnMapping(145, 145)]
        [ColumnMetaData("col145", SqlDbType.Char, 9)]
        public string col145 { get; set; }
        [ColumnMapping(146, 146)]
        [ColumnMetaData("col146", SqlDbType.Char, 3)]
        public string col146 { get; set; }
        [ColumnMapping(147, 147)]
        [ColumnMetaData("col147", SqlDbType.Char, 25)]
        public string col147 { get; set; }
        [ColumnMapping(148, 148)]
        [ColumnMetaData("col148", SqlDbType.Char, 38)]
        public string col148 { get; set; }
        [ColumnMapping(149, 149)]
        [ColumnMetaData("col149", SqlDbType.Char, 3)]
        public string col149 { get; set; }
        [ColumnMapping(150, 150)]
        [ColumnMetaData("col150", SqlDbType.Char, 19)]
        public string col150 { get; set; }
        [ColumnMapping(151, 151)]
        [ColumnMetaData("col151", SqlDbType.Char, 12)]
        public string col151 { get; set; }
        [ColumnMapping(152, 152)]
        [ColumnMetaData("col152", SqlDbType.Char, 7)]
        public string col152 { get; set; }
        [ColumnMapping(153, 153)]
        [ColumnMetaData("col153", SqlDbType.Char, 16)]
        public string col153 { get; set; }
        [ColumnMapping(154, 154)]
        [ColumnMetaData("col154", SqlDbType.Char, 9)]
        public string col154 { get; set; }
        [ColumnMapping(155, 155)]
        [ColumnMetaData("col155", SqlDbType.Char, 3)]
        public string col155 { get; set; }
        [ColumnMapping(156, 156)]
        [ColumnMetaData("col156", SqlDbType.Char, 25)]
        public string col156 { get; set; }
        [ColumnMapping(157, 157)]
        [ColumnMetaData("col157", SqlDbType.Char, 38)]
        public string col157 { get; set; }
        [ColumnMapping(158, 158)]
        [ColumnMetaData("col158", SqlDbType.Char, 3)]
        public string col158 { get; set; }
        [ColumnMapping(159, 159)]
        [ColumnMetaData("col159", SqlDbType.Char, 12)]
        public string col159 { get; set; }
        [ColumnMapping(160, 160)]
        [ColumnMetaData("col160", SqlDbType.Char, 9)]
        public string col160 { get; set; }
        [ColumnMapping(161, 161)]
        [ColumnMetaData("col161", SqlDbType.Char, 7)]
        public string col161 { get; set; }
        [ColumnMapping(162, 162)]
        [ColumnMetaData("col162", SqlDbType.Char, 7)]
        public string col162 { get; set; }
        [ColumnMapping(163, 163)]
        [ColumnMetaData("col163", SqlDbType.Char, 8)]
        public string col163 { get; set; }
        [ColumnMapping(164, 164)]
        [ColumnMetaData("col164", SqlDbType.Char, 7)]
        public string col164 { get; set; }
        [ColumnMapping(165, 165)]
        [ColumnMetaData("col165", SqlDbType.Char, 13)]
        public string col165 { get; set; }
        [ColumnMapping(166, 166)]
        [ColumnMetaData("col166", SqlDbType.Char, 21)]
        public string col166 { get; set; }
        [ColumnMapping(167, 167)]
        [ColumnMetaData("col167", SqlDbType.Char, 25)]
        public string col167 { get; set; }
        [ColumnMapping(168, 168)]
        [ColumnMetaData("col168", SqlDbType.Char, 13)]
        public string col168 { get; set; }
        [ColumnMapping(169, 169)]
        [ColumnMetaData("col169", SqlDbType.Char, 13)]
        public string col169 { get; set; }
        [ColumnMapping(170, 170)]
        [ColumnMetaData("col170", SqlDbType.Char, 7)]
        public string col170 { get; set; }
        [ColumnMapping(171, 171)]
        [ColumnMetaData("col171", SqlDbType.Char, 13)]
        public string col171 { get; set; }
        [ColumnMapping(172, 172)]
        [ColumnMetaData("col172", SqlDbType.Char, 14)]
        public string col172 { get; set; }
        [ColumnMapping(173, 173)]
        [ColumnMetaData("col173", SqlDbType.Char, 11)]
        public string col173 { get; set; }
        [ColumnMapping(174, 174)]
        [ColumnMetaData("col174", SqlDbType.Char, 12)]
        public string col174 { get; set; }
        [ColumnMapping(175, 175)]
        [ColumnMetaData("col175", SqlDbType.Char, 7)]
        public string col175 { get; set; }
        [ColumnMapping(176, 176)]
        [ColumnMetaData("col176", SqlDbType.Char, 7)]
        public string col176 { get; set; }
        [ColumnMapping(177, 177)]
        [ColumnMetaData("col177", SqlDbType.Char, 7)]
        public string col177 { get; set; }
        [ColumnMapping(178, 178)]
        [ColumnMetaData("col178", SqlDbType.Char, 8)]
        public string col178 { get; set; }
        [ColumnMapping(179, 179)]
        [ColumnMetaData("col179", SqlDbType.Char, 13)]
        public string col179 { get; set; }
        [ColumnMapping(180, 180)]
        [ColumnMetaData("col180", SqlDbType.Char, 21)]
        public string col180 { get; set; }
        [ColumnMapping(181, 181)]
        [ColumnMetaData("col181", SqlDbType.Char, 25)]
        public string col181 { get; set; }
        [ColumnMapping(182, 182)]
        [ColumnMetaData("col182", SqlDbType.Char, 13)]
        public string col182 { get; set; }
        [ColumnMapping(183, 183)]
        [ColumnMetaData("col183", SqlDbType.Char, 7)]
        public string col183 { get; set; }
        [ColumnMapping(184, 184)]
        [ColumnMetaData("col184", SqlDbType.Char, 13)]
        public string col184 { get; set; }
        [ColumnMapping(185, 185)]
        [ColumnMetaData("col185", SqlDbType.Char, 14)]
        public string col185 { get; set; }
        [ColumnMapping(186, 186)]
        [ColumnMetaData("col186", SqlDbType.Char, 1)]
        public string col186 { get; set; }
        [ColumnMapping(187, 187)]
        [ColumnMetaData("col187", SqlDbType.Char, 1)]
        public string col187 { get; set; }
        [ColumnMapping(188, 188)]
        [ColumnMetaData("col188", SqlDbType.Char, 1)]
        public string col188 { get; set; }
        [ColumnMapping(189, 189)]
        [ColumnMetaData("col189", SqlDbType.Char, 1)]
        public string col189 { get; set; }
        [ColumnMapping(190, 190)]
        [ColumnMetaData("col190", SqlDbType.Char, 1)]
        public string col190 { get; set; }
        [ColumnMapping(191, 191)]
        [ColumnMetaData("col191", SqlDbType.Char, 1)]
        public string col191 { get; set; }
        [ColumnMapping(192, 192)]
        [ColumnMetaData("col192", SqlDbType.Char, 1)]
        public string col192 { get; set; }
        [ColumnMapping(193, 193)]
        [ColumnMetaData("col193", SqlDbType.Char, 1)]
        public string col193 { get; set; }
        [ColumnMapping(194, 194)]
        [ColumnMetaData("col194", SqlDbType.Char, 1)]
        public string col194 { get; set; }
        [ColumnMapping(195, 195)]
        [ColumnMetaData("col195", SqlDbType.Char, 1)]
        public string col195 { get; set; }
        [ColumnMapping(196, 196)]
        [ColumnMetaData("col196", SqlDbType.Char, 1)]
        public string col196 { get; set; }
        [ColumnMapping(197, 197)]
        [ColumnMetaData("col197", SqlDbType.Char, 1)]
        public string col197 { get; set; }
        [ColumnMapping(198, 198)]
        [ColumnMetaData("col198", SqlDbType.Char, 1)]
        public string col198 { get; set; }
        [ColumnMapping(199, 199)]
        [ColumnMetaData("col199", SqlDbType.Char, 1)]
        public string col199 { get; set; }
        [ColumnMapping(200, 200)]
        [ColumnMetaData("col200", SqlDbType.Char, 1)]
        public string col200 { get; set; }
        [ColumnMapping(201, 201)]
        [ColumnMetaData("col201", SqlDbType.Char, 1)]
        public string col201 { get; set; }
        [ColumnMapping(202, 202)]
        [ColumnMetaData("col202", SqlDbType.Char, 36)]
        public string col202 { get; set; }
        [ColumnMapping(203, 203)]
        [ColumnMetaData("col203", SqlDbType.Char, 1)]
        public string col203 { get; set; }
        [ColumnMapping(204, 204)]
        [ColumnMetaData("col204", SqlDbType.Char, 3)]
        public string col204 { get; set; }
        [ColumnMapping(205, 205)]
        [ColumnMetaData("col205", SqlDbType.Char, 3)]
        public string col205 { get; set; }
        [ColumnMapping(206, 206)]
        [ColumnMetaData("col206", SqlDbType.Char, 2)]
        public string col206 { get; set; }
        [ColumnMapping(207, 207)]
        [ColumnMetaData("col207", SqlDbType.Char, 8)]
        public string col207 { get; set; }
        [ColumnMapping(208, 208)]
        [ColumnMetaData("col208", SqlDbType.Char, 4)]
        public string col208 { get; set; }
        [ColumnMapping(209, 209)]
        [ColumnMetaData("col209", SqlDbType.Char, 4)]
        public string col209 { get; set; }
        [ColumnMapping(210, 210)]
        [ColumnMetaData("col210", SqlDbType.Char, 4)]
        public string col210 { get; set; }
        [ColumnMapping(211, 211)]
        [ColumnMetaData("col211", SqlDbType.Char, 1)]
        public string col211 { get; set; }
        [ColumnMapping(212, 212)]
        [ColumnMetaData("col212", SqlDbType.Char, 1)]
        public string col212 { get; set; }
        [ColumnMapping(213, 213)]
        [ColumnMetaData("col213", SqlDbType.Char, 1)]
        public string col213 { get; set; }
        [ColumnMapping(214, 214)]
        [ColumnMetaData("col214", SqlDbType.Char, 1)]
        public string col214 { get; set; }
        [ColumnMapping(215, 215)]
        [ColumnMetaData("col215", SqlDbType.Char, 1)]
        public string col215 { get; set; }
        [ColumnMapping(216, 216)]
        [ColumnMetaData("col216", SqlDbType.Char, 1)]
        public string col216 { get; set; }
        [ColumnMapping(217, 217)]
        [ColumnMetaData("col217", SqlDbType.Char, 1)]
        public string col217 { get; set; }
        [ColumnMapping(218, 218)]
        [ColumnMetaData("col218", SqlDbType.Char, 13)]
        public string col218 { get; set; }
        [ColumnMapping(219, 219)]
        [ColumnMetaData("col219", SqlDbType.Char, 2)]
        public string col219 { get; set; }
        [ColumnMapping(220, 220)]
        [ColumnMetaData("col220", SqlDbType.Char, 8)]
        public string col220 { get; set; }
        [ColumnMapping(221, 221)]
        [ColumnMetaData("col221", SqlDbType.Char, 4)]
        public string col221 { get; set; }
        [ColumnMapping(222, 222)]
        [ColumnMetaData("col222", SqlDbType.Char, 4)]
        public string col222 { get; set; }
        [ColumnMapping(223, 223)]
        [ColumnMetaData("col223", SqlDbType.Char, 4)]
        public string col223 { get; set; }
        [ColumnMapping(224, 224)]
        [ColumnMetaData("col224", SqlDbType.Char, 1)]
        public string col224 { get; set; }
        [ColumnMapping(225, 225)]
        [ColumnMetaData("col225", SqlDbType.Char, 1)]
        public string col225 { get; set; }
        [ColumnMapping(226, 226)]
        [ColumnMetaData("col226", SqlDbType.Char, 1)]
        public string col226 { get; set; }
        [ColumnMapping(227, 227)]
        [ColumnMetaData("col227", SqlDbType.Char, 1)]
        public string col227 { get; set; }
        [ColumnMapping(228, 228)]
        [ColumnMetaData("col228", SqlDbType.Char, 1)]
        public string col228 { get; set; }
        [ColumnMapping(229, 229)]
        [ColumnMetaData("col229", SqlDbType.Char, 1)]
        public string col229 { get; set; }
        [ColumnMapping(230, 230)]
        [ColumnMetaData("col230", SqlDbType.Char, 1)]
        public string col230 { get; set; }
        [ColumnMapping(231, 231)]
        [ColumnMetaData("col231", SqlDbType.Char, 13)]
        public string col231 { get; set; }
        [ColumnMapping(232, 232)]
        [ColumnMetaData("col232", SqlDbType.Char, 2)]
        public string col232 { get; set; }
        [ColumnMapping(233, 233)]
        [ColumnMetaData("col233", SqlDbType.Char, 8)]
        public string col233 { get; set; }
        [ColumnMapping(234, 234)]
        [ColumnMetaData("col234", SqlDbType.Char, 4)]
        public string col234 { get; set; }
        [ColumnMapping(235, 235)]
        [ColumnMetaData("col235", SqlDbType.Char, 4)]
        public string col235 { get; set; }
        [ColumnMapping(236, 236)]
        [ColumnMetaData("col236", SqlDbType.Char, 4)]
        public string col236 { get; set; }
        [ColumnMapping(237, 237)]
        [ColumnMetaData("col237", SqlDbType.Char, 1)]
        public string col237 { get; set; }
        [ColumnMapping(238, 238)]
        [ColumnMetaData("col238", SqlDbType.Char, 1)]
        public string col238 { get; set; }
        [ColumnMapping(239, 239)]
        [ColumnMetaData("col239", SqlDbType.Char, 1)]
        public string col239 { get; set; }
        [ColumnMapping(240, 240)]
        [ColumnMetaData("col240", SqlDbType.Char, 1)]
        public string col240 { get; set; }
        [ColumnMapping(241, 241)]
        [ColumnMetaData("col241", SqlDbType.Char, 1)]
        public string col241 { get; set; }
        [ColumnMapping(242, 242)]
        [ColumnMetaData("col242", SqlDbType.Char, 1)]
        public string col242 { get; set; }
        [ColumnMapping(243, 243)]
        [ColumnMetaData("col243", SqlDbType.Char, 1)]
        public string col243 { get; set; }
        [ColumnMapping(244, 244)]
        [ColumnMetaData("col244", SqlDbType.Char, 1)]
        public string col244 { get; set; }
        [ColumnMapping(245, 245)]
        [ColumnMetaData("col245", SqlDbType.Char, 28)]
        public string col245 { get; set; }
        [ColumnMapping(246, 246)]
        [ColumnMetaData("col246", SqlDbType.Char, 16)]
        public string col246 { get; set; }
        [ColumnMapping(247, 247)]
        [ColumnMetaData("col247", SqlDbType.Char, 60)]
        public string col247 { get; set; }
        [ColumnMapping(248, 248)]
        [ColumnMetaData("col248", SqlDbType.Char, 2)]
        public string col248 { get; set; }
    }
}