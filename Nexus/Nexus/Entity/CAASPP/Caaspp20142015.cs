﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.CAASPP
{
    class Caaspp20142015
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("record_type", SqlDbType.Char, 2)]
        public string record_type { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("unique_identification_number", SqlDbType.Char, 16)]
        public string unique_identification_number { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("name_full", SqlDbType.VarChar, 110)]
        public string name_full { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("confirmation_code", SqlDbType.VarChar, 30)]
        public string confirmation_code { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("student_state_id", SqlDbType.Char, 10)]
        public string student_state_id { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("birthdate", SqlDbType.Date, 10)]
        public DateTime? birthdate { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("gender", SqlDbType.VarChar, 6)]
        public string gender { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("ela_pt_district_name", SqlDbType.VarChar, 40)]
        public string ela_pt_district_name { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("ela_pt_district_code", SqlDbType.Char, 7)]
        public string ela_pt_district_code { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("ela_pt_school_name", SqlDbType.VarChar, 40)]
        public string ela_pt_school_name { get; set; }
        [ColumnMapping(10, 10)]
        [ColumnMetaData("ela_pt_school_code", SqlDbType.Char, 7)]
        public string ela_pt_school_code { get; set; }
        [ColumnMapping(11, 11)]
        [ColumnMetaData("ela_pt_Charter_school_directly_funded_indicator", SqlDbType.Char, 2)]
        public string ela_pt_charter_school_directly_funded_indicator { get; set; }
        [ColumnMapping(12, 12)]
        [ColumnMetaData("ela_pt_test_completion_date", SqlDbType.Char, 8)]
        public string ela_pt_test_completion_date { get; set; }
        [ColumnMapping(13, 13)]
        [ColumnMetaData("ela_npt_district_name", SqlDbType.VarChar, 40)]
        public string ela_npt_district_name { get; set; }
        [ColumnMapping(14, 14)]
        [ColumnMetaData("ela_npt_district_code", SqlDbType.Char, 7)]
        public string ela_npt_district_code { get; set; }
        [ColumnMapping(15, 15)]
        [ColumnMetaData("ela_npt_school_name", SqlDbType.VarChar, 40)]
        public string ela_npt_school_name { get; set; }
        [ColumnMapping(16, 16)]
        [ColumnMetaData("ela_npt_school_code", SqlDbType.Char, 7)]
        public string ela_npt_school_code { get; set; }
        [ColumnMapping(17, 17)]
        [ColumnMetaData("ela_npt_Charter_school_directly_funded_indicator", SqlDbType.Char, 2)]
        public string ela_npt_charter_school_directly_funded_indicator { get; set; }
        [ColumnMapping(18, 18)]
        [ColumnMetaData("ela_npt_test_completion_date", SqlDbType.Char, 8)]
        public string ela_npt_test_completion_date { get; set; }
        [ColumnMapping(19, 19)]
        [ColumnMetaData("mat_pt_district_name", SqlDbType.VarChar, 40)]
        public string mat_pt_district_name { get; set; }
        [ColumnMapping(20, 20)]
        [ColumnMetaData("mat_pt_district_code", SqlDbType.Char, 7)]
        public string mat_pt_district_code { get; set; }
        [ColumnMapping(21, 21)]
        [ColumnMetaData("mat_pt_school_name", SqlDbType.VarChar, 40)]
        public string mat_pt_school_name { get; set; }
        [ColumnMapping(22, 22)]
        [ColumnMetaData("mat_pt_school_code", SqlDbType.Char, 7)]
        public string mat_pt_school_code { get; set; }
        [ColumnMapping(23, 23)]
        [ColumnMetaData("mat_pt_Charter_school_directly_funded_indicator", SqlDbType.Char, 2)]
        public string mat_pt_charter_school_directly_funded_indicator { get; set; }
        [ColumnMapping(24, 24)]
        [ColumnMetaData("mat_pt_test_completion_date", SqlDbType.Char, 8)]
        public string mat_pt_test_completion_date { get; set; }
        [ColumnMapping(25, 25)]
        [ColumnMetaData("mat_npt_district_name", SqlDbType.VarChar, 40)]
        public string mat_npt_district_name { get; set; }
        [ColumnMapping(26, 26)]
        [ColumnMetaData("mat_npt_district_code", SqlDbType.Char, 7)]
        public string mat_npt_district_code { get; set; }
        [ColumnMapping(27, 27)]
        [ColumnMetaData("mat_npt_school_name", SqlDbType.VarChar, 40)]
        public string mat_npt_school_name { get; set; }
        [ColumnMapping(28, 28)]
        [ColumnMetaData("mat_npt_school_code", SqlDbType.Char, 7)]
        public string mat_npt_school_code { get; set; }
        [ColumnMapping(29, 29)]
        [ColumnMetaData("mat_npt_Charter_school_directly_funded_indicator", SqlDbType.Char, 2)]
        public string mat_npt_charter_school_directly_funded_indicator { get; set; }
        [ColumnMapping(30, 30)]
        [ColumnMetaData("mat_npt_test_completion_date", SqlDbType.Char, 8)]
        public string mat_npt_test_completion_date { get; set; }
        [ColumnMapping(31, 31)]
        [ColumnMetaData("sci_district_name", SqlDbType.VarChar, 40)]
        public string sci_district_name { get; set; }
        [ColumnMapping(32, 32)]
        [ColumnMetaData("sci_district_code", SqlDbType.Char, 7)]
        public string sci_district_code { get; set; }
        [ColumnMapping(33, 33)]
        [ColumnMetaData("sci_school_name", SqlDbType.VarChar, 40)]
        public string sci_school_name { get; set; }
        [ColumnMapping(34, 34)]
        [ColumnMetaData("sci_school_code", SqlDbType.Char, 7)]
        public string sci_school_code { get; set; }
        [ColumnMapping(35, 35)]
        [ColumnMetaData("sci_Charter_school_directly_funded_indicator", SqlDbType.Char, 2)]
        public string sci_charter_school_directly_funded_indicator { get; set; }
        [ColumnMapping(36, 36)]
        [ColumnMetaData("sci_test_completion_date", SqlDbType.Char, 8)]
        public string sci_test_completion_date { get; set; }
        [ColumnMapping(37, 37)]
        [ColumnMetaData("rla_district_name", SqlDbType.VarChar, 40)]
        public string rla_district_name { get; set; }
        [ColumnMapping(38, 38)]
        [ColumnMetaData("rla_district_code", SqlDbType.Char, 7)]
        public string rla_district_code { get; set; }
        [ColumnMapping(39, 39)]
        [ColumnMetaData("rla_school_name", SqlDbType.VarChar, 40)]
        public string rla_school_name { get; set; }
        [ColumnMapping(40, 40)]
        [ColumnMetaData("rla_school_code", SqlDbType.Char, 7)]
        public string rla_school_code { get; set; }
        [ColumnMapping(41, 41)]
        [ColumnMetaData("rla_Charter_school_directly_funded_indicator", SqlDbType.Char, 2)]
        public string rla_charter_school_directly_funded_indicator { get; set; }
        [ColumnMapping(42, 42)]
        [ColumnMetaData("rla_test_completion_date", SqlDbType.Char, 8)]
        public string rla_test_completion_date { get; set; }
        [ColumnMapping(43, 43)]
        [ColumnMetaData("enrolled_grade", SqlDbType.Char, 2)]
        public string enrolled_grade { get; set; }
        [ColumnMapping(44, 44)]
        [ColumnMetaData("ela_grade_tested", SqlDbType.Char, 2)]
        public string ela_grade_tested { get; set; }
        [ColumnMapping(45, 45)]
        [ColumnMetaData("mat_grade_tested", SqlDbType.Char, 2)]
        public string mat_grade_tested { get; set; }
        [ColumnMapping(46, 46)]
        [ColumnMetaData("sci_grade_tested", SqlDbType.Char, 2)]
        public string sci_grade_tested { get; set; }
        [ColumnMapping(47, 47)]
        [ColumnMetaData("rla_grade_tested", SqlDbType.Char, 2)]
        public string rla_grade_tested { get; set; }
        [ColumnMapping(48, 48)]
        [ColumnMetaData("capa_level", SqlDbType.Char, 1)]
        public string capa_level { get; set; }
        [ColumnMapping(49, 49)]
        [ColumnMetaData("district_residence", SqlDbType.Char, 7)]
        public string district_residence { get; set; }
        [ColumnMapping(50, 50)]
        [ColumnMetaData("demographic_information_only", SqlDbType.Char, 1)]
        public string demographic_information_only { get; set; }
        [ColumnMapping(51, 51)]
        [ColumnMetaData("rla_abs", SqlDbType.Char, 1)]
        public string rla_abs { get; set; }
        [ColumnMapping(52, 52)]
        [ColumnMetaData("sci_abs", SqlDbType.Char, 1)]
        public string sci_abs { get; set; }
        [ColumnMapping(53, 53)]
        [ColumnMetaData("xla_c", SqlDbType.Char, 1)]
        public string xla_c { get; set; }
        [ColumnMapping(54, 54)]
        [ColumnMetaData("mat_c", SqlDbType.Char, 1)]
        public string mat_c { get; set; }
        [ColumnMapping(55, 55)]
        [ColumnMetaData("sci_c", SqlDbType.Char, 1)]
        public string sci_c { get; set; }
        [ColumnMapping(56, 56)]
        [ColumnMetaData("ela_nte", SqlDbType.Char, 1)]
        public string ela_nte { get; set; }
        [ColumnMapping(57, 57)]
        [ColumnMetaData("mat_nte", SqlDbType.Char, 1)]
        public string mat_nte { get; set; }
        [ColumnMapping(58, 58)]
        [ColumnMetaData("sci_nte", SqlDbType.Char, 1)]
        public string sci_nte { get; set; }
        [ColumnMapping(59, 59)]
        [ColumnMetaData("ela_usa", SqlDbType.Char, 1)]
        public string ela_usa { get; set; }
        [ColumnMapping(60, 60)]
        [ColumnMetaData("mat_usa", SqlDbType.Char, 1)]
        public string mat_usa { get; set; }
        [ColumnMapping(61, 61)]
        [ColumnMetaData("sci_usa", SqlDbType.Char, 1)]
        public string sci_usa { get; set; }
        [ColumnMapping(62, 62)]
        [ColumnMetaData("ela_pge", SqlDbType.Char, 1)]
        public string ela_pge { get; set; }
        [ColumnMapping(63, 63)]
        [ColumnMetaData("mat_pge", SqlDbType.Char, 1)]
        public string mat_pge { get; set; }
        [ColumnMapping(64, 64)]
        [ColumnMetaData("sci_pge", SqlDbType.Char, 1)]
        public string sci_pge { get; set; }
        [ColumnMapping(65, 65)]
        [ColumnMetaData("ela_t", SqlDbType.Char, 1)]
        public string ela_t { get; set; }
        [ColumnMapping(66, 66)]
        [ColumnMetaData("mat_t", SqlDbType.Char, 1)]
        public string mat_t { get; set; }
        [ColumnMapping(67, 67)]
        [ColumnMetaData("sci_t", SqlDbType.Char, 1)]
        public string sci_t { get; set; }
        [ColumnMapping(68, 68)]
        [ColumnMetaData("rla_inc", SqlDbType.Char, 1)]
        public string rla_inc { get; set; }
        [ColumnMapping(69, 69)]
        [ColumnMetaData("sci_inc", SqlDbType.Char, 1)]
        public string sci_inc { get; set; }
        [ColumnMapping(70, 70)]
        [ColumnMetaData("ela_loss", SqlDbType.Char, 1)]
        public string ela_loss { get; set; }
        [ColumnMapping(71, 71)]
        [ColumnMetaData("mat_loss", SqlDbType.Char, 1)]
        public string mat_loss { get; set; }
        [ColumnMapping(72, 72)]
        [ColumnMetaData("rla_ne", SqlDbType.Char, 1)]
        public string rla_ne { get; set; }
        [ColumnMapping(73, 73)]
        [ColumnMetaData("section_504_status", SqlDbType.VarChar, 3)]
        public string section_504_status { get; set; }
        [ColumnMapping(74, 74)]
        [ColumnMetaData("idea_indicator", SqlDbType.VarChar, 3)]
        public string idea_indicator { get; set; }
        [ColumnMapping(75, 75)]
        [ColumnMetaData("ela_em_american_sign_language", SqlDbType.Char, 1)]
        public string ela_em_american_sign_language { get; set; }
        [ColumnMapping(76, 76)]
        [ColumnMetaData("mat_em_american_sign_language", SqlDbType.Char, 1)]
        public string mat_em_american_sign_language { get; set; }
        [ColumnMapping(77, 77)]
        [ColumnMetaData("ela_em_braille", SqlDbType.Char, 1)]
        public string ela_em_braille { get; set; }
        [ColumnMapping(78, 78)]
        [ColumnMetaData("mat_em_braille", SqlDbType.Char, 1)]
        public string mat_em_braille { get; set; }
        [ColumnMapping(79, 79)]
        [ColumnMetaData("ela_em_closed_captioning", SqlDbType.Char, 1)]
        public string ela_em_closed_captioning { get; set; }
        [ColumnMapping(80, 80)]
        [ColumnMetaData("ela_em_color_contrast", SqlDbType.Char, 2)]
        public string ela_em_color_contrast { get; set; }
        [ColumnMapping(81, 81)]
        [ColumnMetaData("mat_em_color_contrast", SqlDbType.Char, 2)]
        public string mat_em_color_contrast { get; set; }
        [ColumnMapping(82, 82)]
        [ColumnMetaData("ela_em_masking", SqlDbType.Char, 1)]
        public string ela_em_masking { get; set; }
        [ColumnMapping(83, 83)]
        [ColumnMetaData("mat_em_masking", SqlDbType.Char, 1)]
        public string mat_em_masking { get; set; }
        [ColumnMapping(84, 84)]
        [ColumnMetaData("ela_em_permissive_mode", SqlDbType.Char, 1)]
        public string ela_em_permissive_mode { get; set; }
        [ColumnMapping(85, 85)]
        [ColumnMetaData("mat_em_permissive_mode", SqlDbType.Char, 1)]
        public string mat_em_permissive_mode { get; set; }
        [ColumnMapping(86, 86)]
        [ColumnMetaData("ela_em_streamlining", SqlDbType.Char, 1)]
        public string ela_em_streamlining { get; set; }
        [ColumnMapping(87, 87)]
        [ColumnMetaData("mat_em_streamlining", SqlDbType.Char, 1)]
        public string mat_em_streamlining { get; set; }
        [ColumnMapping(88, 88)]
        [ColumnMetaData("ela_em_text_to_speech", SqlDbType.Char, 2)]
        public string ela_em_text_to_speech { get; set; }
        [ColumnMapping(89, 89)]
        [ColumnMetaData("mat_em_text_to_speech", SqlDbType.Char, 2)]
        public string mat_em_text_to_speech { get; set; }
        [ColumnMapping(90, 90)]
        [ColumnMetaData("mat_em_translated_test_directions", SqlDbType.Char, 1)]
        public string mat_em_translated_test_directions { get; set; }
        [ColumnMapping(91, 91)]
        [ColumnMetaData("mat_em_translations_glossary", SqlDbType.Char, 3)]
        public string mat_em_translations_glossary { get; set; }
        [ColumnMapping(92, 92)]
        [ColumnMetaData("mat_em_translations_stacked", SqlDbType.Char, 2)]
        public string mat_em_translations_stacked { get; set; }
        [ColumnMapping(93, 93)]
        [ColumnMetaData("ela_em_turn_off_any_universal_tool", SqlDbType.Char, 1)]
        public string ela_em_turn_off_any_universal_tool { get; set; }
        [ColumnMapping(94, 94)]
        [ColumnMetaData("mat_em_turn_off_any_universal_tool", SqlDbType.Char, 1)]
        public string mat_em_turn_off_any_universal_tool { get; set; }
        [ColumnMapping(95, 95)]
        [ColumnMetaData("ela_nem_beneficial_time_of_day", SqlDbType.Char, 1)]
        public string ela_nem_beneficial_time_of_day { get; set; }
        [ColumnMapping(96, 96)]
        [ColumnMetaData("mat_nem_beneficial_time_of_day", SqlDbType.Char, 1)]
        public string mat_nem_beneficial_time_of_day { get; set; }
        [ColumnMapping(97, 97)]
        [ColumnMetaData("sci_nem_beneficial_time_of_day", SqlDbType.Char, 1)]
        public string sci_nem_beneficial_time_of_day { get; set; }
        [ColumnMapping(98, 98)]
        [ColumnMetaData("rla_nem_beneficial_time_of_day", SqlDbType.Char, 1)]
        public string rla_nem_beneficial_time_of_day { get; set; }
        [ColumnMapping(99, 99)]
        [ColumnMetaData("mat_nem_abacus", SqlDbType.Char, 1)]
        public string mat_nem_abacus { get; set; }
        [ColumnMapping(100, 100)]
        [ColumnMetaData("sci_nem_abacus", SqlDbType.Char, 1)]
        public string sci_nem_abacus { get; set; }
        [ColumnMapping(101, 101)]
        [ColumnMetaData("ela_nem_alternate_response_options", SqlDbType.Char, 1)]
        public string ela_nem_alternate_response_options { get; set; }
        [ColumnMapping(102, 102)]
        [ColumnMetaData("mat_nem_alternate_response_options", SqlDbType.Char, 1)]
        public string mat_nem_alternate_response_options { get; set; }
        [ColumnMapping(103, 103)]
        [ColumnMetaData("ela_nem_american_sign_language", SqlDbType.Char, 1)]
        public string ela_nem_american_sign_language { get; set; }
        [ColumnMapping(104, 104)]
        [ColumnMetaData("mat_nem_american_sign_language", SqlDbType.Char, 1)]
        public string mat_nem_american_sign_language { get; set; }
        [ColumnMapping(105, 105)]
        [ColumnMetaData("sci_nem_american_sign_language", SqlDbType.Char, 1)]
        public string sci_nem_american_sign_language { get; set; }
        [ColumnMapping(106, 106)]
        [ColumnMetaData("rla_nem_american_sign_language", SqlDbType.Char, 1)]
        public string rla_nem_american_sign_language { get; set; }
        [ColumnMapping(107, 107)]
        [ColumnMetaData("ela_nem_bilingual_dictionary", SqlDbType.Char, 1)]
        public string ela_nem_bilingual_dictionary { get; set; }
        [ColumnMapping(108, 108)]
        [ColumnMetaData("mat_nem_bilingual_dictionary", SqlDbType.Char, 1)]
        public string mat_nem_bilingual_dictionary { get; set; }
        [ColumnMapping(109, 109)]
        [ColumnMetaData("sci_nem_bilingual_dictionary", SqlDbType.Char, 1)]
        public string sci_nem_bilingual_dictionary { get; set; }
        [ColumnMapping(110, 110)]
        [ColumnMetaData("rla_nem_bilingual_dictionary", SqlDbType.Char, 1)]
        public string rla_nem_bilingual_dictionary { get; set; }
        [ColumnMapping(111, 111)]
        [ColumnMetaData("ela_nem_braille", SqlDbType.Char, 1)]
        public string ela_nem_braille { get; set; }
        [ColumnMapping(112, 112)]
        [ColumnMetaData("mat_nem_braille", SqlDbType.Char, 1)]
        public string mat_nem_braille { get; set; }
        [ColumnMapping(113, 113)]
        [ColumnMetaData("sci_nem_braille", SqlDbType.Char, 1)]
        public string sci_nem_braille { get; set; }
        [ColumnMapping(114, 114)]
        [ColumnMetaData("rla_nem_braille", SqlDbType.Char, 1)]
        public string rla_nem_braille { get; set; }
        [ColumnMapping(115, 115)]
        [ColumnMetaData("mat_nem_calculator", SqlDbType.Char, 1)]
        public string mat_nem_calculator { get; set; }
        [ColumnMapping(116, 116)]
        [ColumnMetaData("sci_nem_calculator", SqlDbType.Char, 1)]
        public string sci_nem_calculator { get; set; }
        [ColumnMapping(117, 117)]
        [ColumnMetaData("ela_nem_color_contrast", SqlDbType.Char, 1)]
        public string ela_nem_color_contrast { get; set; }
        [ColumnMapping(118, 118)]
        [ColumnMetaData("mat_nem_color_contrast", SqlDbType.Char, 1)]
        public string mat_nem_color_contrast { get; set; }
        [ColumnMapping(119, 119)]
        [ColumnMetaData("ela_nem_color_overlay", SqlDbType.Char, 1)]
        public string ela_nem_color_overlay { get; set; }
        [ColumnMapping(120, 120)]
        [ColumnMetaData("mat_nem_color_overlay", SqlDbType.Char, 1)]
        public string mat_nem_color_overlay { get; set; }
        [ColumnMapping(121, 121)]
        [ColumnMetaData("ela_nem_english_dictionary", SqlDbType.Char, 1)]
        public string ela_nem_english_dictionary { get; set; }
        [ColumnMapping(122, 122)]
        [ColumnMetaData("mat_nem_english_dictionary", SqlDbType.Char, 1)]
        public string mat_nem_english_dictionary { get; set; }
        [ColumnMapping(123, 123)]
        [ColumnMetaData("sci_nem_english_dictionary", SqlDbType.Char, 1)]
        public string sci_nem_english_dictionary { get; set; }
        [ColumnMapping(124, 124)]
        [ColumnMetaData("rla_nem_english_dictionary", SqlDbType.Char, 1)]
        public string rla_nem_english_dictionary { get; set; }
        [ColumnMapping(125, 125)]
        [ColumnMetaData("ela_nem_individualized_aid", SqlDbType.Char, 1)]
        public string ela_nem_individualized_aid { get; set; }
        [ColumnMapping(126, 126)]
        [ColumnMetaData("mat_nem_individualized_aid", SqlDbType.Char, 1)]
        public string mat_nem_individualized_aid { get; set; }
        [ColumnMapping(127, 127)]
        [ColumnMetaData("sci_nem_individualized_aid", SqlDbType.Char, 1)]
        public string sci_nem_individualized_aid { get; set; }
        [ColumnMapping(128, 128)]
        [ColumnMetaData("rla_nem_individualized_aid", SqlDbType.Char, 1)]
        public string rla_nem_individualized_aid { get; set; }
        [ColumnMapping(129, 129)]
        [ColumnMetaData("ela_nem_large_print", SqlDbType.Char, 1)]
        public string ela_nem_large_print { get; set; }
        [ColumnMapping(130, 130)]
        [ColumnMetaData("mat_nem_large_print", SqlDbType.Char, 1)]
        public string mat_nem_large_print { get; set; }
        [ColumnMapping(131, 131)]
        [ColumnMetaData("sci_nem_large_print", SqlDbType.Char, 1)]
        public string sci_nem_large_print { get; set; }
        [ColumnMapping(132, 132)]
        [ColumnMetaData("rla_nem_large_print", SqlDbType.Char, 1)]
        public string rla_nem_large_print { get; set; }
        [ColumnMapping(133, 133)]
        [ColumnMetaData("ela_nem_magnification", SqlDbType.Char, 1)]
        public string ela_nem_magnification { get; set; }
        [ColumnMapping(134, 134)]
        [ColumnMetaData("mat_nem_magnification", SqlDbType.Char, 1)]
        public string mat_nem_magnification { get; set; }
        [ColumnMapping(135, 135)]
        [ColumnMetaData("sci_nem_magnification", SqlDbType.Char, 1)]
        public string sci_nem_magnification { get; set; }
        [ColumnMapping(136, 136)]
        [ColumnMetaData("rla_nem_magnification", SqlDbType.Char, 1)]
        public string rla_nem_magnification { get; set; }
        [ColumnMapping(137, 137)]
        [ColumnMetaData("mat_nem_math_tools", SqlDbType.Char, 1)]
        public string mat_nem_math_tools { get; set; }
        [ColumnMapping(138, 138)]
        [ColumnMetaData("mat_nem_multiplication_table", SqlDbType.Char, 1)]
        public string mat_nem_multiplication_table { get; set; }
        [ColumnMapping(139, 139)]
        [ColumnMetaData("ela_nem_noise_buffers", SqlDbType.Char, 1)]
        public string ela_nem_noise_buffers { get; set; }
        [ColumnMapping(140, 140)]
        [ColumnMetaData("mat_nem_noise_buffers", SqlDbType.Char, 1)]
        public string mat_nem_noise_buffers { get; set; }
        [ColumnMapping(141, 141)]
        [ColumnMetaData("sci_nem_noise_buffers", SqlDbType.Char, 1)]
        public string sci_nem_noise_buffers { get; set; }
        [ColumnMapping(142, 142)]
        [ColumnMetaData("rla_nem_noise_buffers", SqlDbType.Char, 1)]
        public string rla_nem_noise_buffers { get; set; }
        [ColumnMapping(143, 143)]
        [ColumnMetaData("ela_nem_print_on_demand", SqlDbType.Char, 2)]
        public string ela_nem_print_on_demand { get; set; }
        [ColumnMapping(144, 144)]
        [ColumnMetaData("mat_nem_print_on_demand", SqlDbType.Char, 2)]
        public string mat_nem_print_on_demand { get; set; }
        [ColumnMapping(145, 145)]
        [ColumnMetaData("ela_nem_read_aloud", SqlDbType.Char, 1)]
        public string ela_nem_read_aloud { get; set; }
        [ColumnMapping(146, 146)]
        [ColumnMetaData("mat_nem_read_aloud", SqlDbType.Char, 1)]
        public string mat_nem_read_aloud { get; set; }
        [ColumnMapping(147, 147)]
        [ColumnMetaData("sci_nem_read_aloud", SqlDbType.Char, 1)]
        public string sci_nem_read_aloud { get; set; }
        [ColumnMapping(148, 148)]
        [ColumnMetaData("rla_nem_read_aloud", SqlDbType.Char, 1)]
        public string rla_nem_read_aloud { get; set; }
        [ColumnMapping(149, 149)]
        [ColumnMetaData("ela_nem_scibe", SqlDbType.Char, 1)]
        public string ela_nem_scibe { get; set; }
        [ColumnMapping(150, 150)]
        [ColumnMetaData("mat_nem_scibe", SqlDbType.Char, 1)]
        public string mat_nem_scibe { get; set; }
        [ColumnMapping(151, 151)]
        [ColumnMetaData("sci_nem_scibe", SqlDbType.Char, 1)]
        public string sci_nem_scibe { get; set; }
        [ColumnMapping(152, 152)]
        [ColumnMetaData("rla_nem_scibe", SqlDbType.Char, 1)]
        public string rla_nem_scibe { get; set; }
        [ColumnMapping(153, 153)]
        [ColumnMetaData("ela_nem_separate_setting", SqlDbType.Char, 1)]
        public string ela_nem_separate_setting { get; set; }
        [ColumnMapping(154, 154)]
        [ColumnMetaData("mat_nem_separate_setting", SqlDbType.Char, 1)]
        public string mat_nem_separate_setting { get; set; }
        [ColumnMapping(155, 155)]
        [ColumnMetaData("sci_nem_separate_setting", SqlDbType.Char, 1)]
        public string sci_nem_separate_setting { get; set; }
        [ColumnMapping(156, 156)]
        [ColumnMetaData("rla_nem_separate_setting", SqlDbType.Char, 1)]
        public string rla_nem_separate_setting { get; set; }
        [ColumnMapping(157, 157)]
        [ColumnMetaData("ela_nem_special_lighting_or_acoustics", SqlDbType.Char, 1)]
        public string ela_nem_special_lighting_or_acoustics { get; set; }
        [ColumnMapping(158, 158)]
        [ColumnMetaData("mat_nem_special_lighting_or_acoustics", SqlDbType.Char, 1)]
        public string mat_nem_special_lighting_or_acoustics { get; set; }
        [ColumnMapping(159, 159)]
        [ColumnMetaData("sci_nem_special_lighting_or_acoustics", SqlDbType.Char, 1)]
        public string sci_nem_special_lighting_or_acoustics { get; set; }
        [ColumnMapping(160, 160)]
        [ColumnMetaData("rla_nem_special_lighting_or_acoustics", SqlDbType.Char, 1)]
        public string rla_nem_special_lighting_or_acoustics { get; set; }
        [ColumnMapping(161, 161)]
        [ColumnMetaData("ela_nem_speech_to_text", SqlDbType.Char, 1)]
        public string ela_nem_speech_to_text { get; set; }
        [ColumnMapping(162, 162)]
        [ColumnMetaData("mat_nem_speech_to_text", SqlDbType.Char, 1)]
        public string mat_nem_speech_to_text { get; set; }
        [ColumnMapping(163, 163)]
        [ColumnMetaData("sci_nem_speech_to_text", SqlDbType.Char, 1)]
        public string sci_nem_speech_to_text { get; set; }
        [ColumnMapping(164, 164)]
        [ColumnMetaData("rla_nem_speech_to_text", SqlDbType.Char, 1)]
        public string rla_nem_speech_to_text { get; set; }
        [ColumnMapping(165, 165)]
        [ColumnMetaData("ela_nem_thesaurus", SqlDbType.Char, 1)]
        public string ela_nem_thesaurus { get; set; }
        [ColumnMapping(166, 166)]
        [ColumnMetaData("mat_nem_thesaurus", SqlDbType.Char, 1)]
        public string mat_nem_thesaurus { get; set; }
        [ColumnMapping(167, 167)]
        [ColumnMetaData("sci_nem_thesaurus", SqlDbType.Char, 1)]
        public string sci_nem_thesaurus { get; set; }
        [ColumnMapping(168, 168)]
        [ColumnMetaData("rla_nem_thesaurus", SqlDbType.Char, 1)]
        public string rla_nem_thesaurus { get; set; }
        [ColumnMapping(169, 169)]
        [ColumnMetaData("ela_nem_translated_test_directions", SqlDbType.Char, 1)]
        public string ela_nem_translated_test_directions { get; set; }
        [ColumnMapping(170, 170)]
        [ColumnMetaData("mat_nem_translated_test_directions", SqlDbType.Char, 1)]
        public string mat_nem_translated_test_directions { get; set; }
        [ColumnMapping(171, 171)]
        [ColumnMetaData("sci_nem_translated_test_directions", SqlDbType.Char, 1)]
        public string sci_nem_translated_test_directions { get; set; }
        [ColumnMapping(172, 172)]
        [ColumnMetaData("rla_nem_translated_test_directions", SqlDbType.Char, 1)]
        public string rla_nem_translated_test_directions { get; set; }
        [ColumnMapping(173, 173)]
        [ColumnMetaData("ela_nem_translations_glossary", SqlDbType.Char, 3)]
        public string ela_nem_translations_glossary { get; set; }
        [ColumnMapping(174, 174)]
        [ColumnMetaData("mat_nem_translations_glossary", SqlDbType.Char, 3)]
        public string mat_nem_translations_glossary { get; set; }
        [ColumnMapping(175, 175)]
        [ColumnMetaData("sci_nem_translations_glossary", SqlDbType.Char, 3)]
        public string sci_nem_translations_glossary { get; set; }
        [ColumnMapping(176, 176)]
        [ColumnMetaData("rla_nem_translations_glossary", SqlDbType.Char, 3)]
        public string rla_nem_translations_glossary { get; set; }
        [ColumnMapping(177, 177)]
        [ColumnMetaData("ela_adult_testing_irregularities", SqlDbType.Char, 1)]
        public string ela_adult_testing_irregularities { get; set; }
        [ColumnMapping(178, 178)]
        [ColumnMetaData("mat_adult_testing_irregularities", SqlDbType.Char, 1)]
        public string mat_adult_testing_irregularities { get; set; }
        [ColumnMapping(179, 179)]
        [ColumnMetaData("una_adult_testing_irregularities", SqlDbType.Char, 1)]
        public string una_adult_testing_irregularities { get; set; }
        [ColumnMapping(180, 180)]
        [ColumnMetaData("sci_adult_testing_irregularities", SqlDbType.Char, 1)]
        public string sci_adult_testing_irregularities { get; set; }
        [ColumnMapping(181, 181)]
        [ColumnMetaData("ela_inappropriate_test_preparation", SqlDbType.Char, 1)]
        public string ela_inappropriate_test_preparation { get; set; }
        [ColumnMapping(182, 182)]
        [ColumnMetaData("mat_inappropriate_test_preparation", SqlDbType.Char, 1)]
        public string mat_inappropriate_test_preparation { get; set; }
        [ColumnMapping(183, 183)]
        [ColumnMetaData("una_inappropriate_test_preparation", SqlDbType.Char, 1)]
        public string una_inappropriate_test_preparation { get; set; }
        [ColumnMapping(184, 184)]
        [ColumnMetaData("sci_inappropriate_test_preparation", SqlDbType.Char, 1)]
        public string sci_inappropriate_test_preparation { get; set; }
        [ColumnMapping(185, 185)]
        [ColumnMetaData("english_language_proficiency_level", SqlDbType.VarChar, 32)]
        public string english_language_proficiency_level { get; set; }
        [ColumnMapping(186, 186)]
        [ColumnMetaData("migrant_status", SqlDbType.VarChar, 3)]
        public string migrant_status { get; set; }
        [ColumnMapping(187, 187)]
        [ColumnMetaData("lep_status", SqlDbType.VarChar, 3)]
        public string lep_status { get; set; }
        [ColumnMapping(188, 188)]
        [ColumnMetaData("lep_entry_date", SqlDbType.Date, 10)]
        public DateTime? lep_entry_date { get; set; }
        [ColumnMapping(189, 189)]
        [ColumnMetaData("lep_exit_date", SqlDbType.Date, 10)]
        public DateTime? lep_exit_date { get; set; }
        [ColumnMapping(190, 190)]
        [ColumnMetaData("first_date_in_us_school", SqlDbType.Date, 10)]
        public DateTime? first_date_in_us_school { get; set; }
        [ColumnMapping(191, 191)]
        [ColumnMetaData("english_language_acquisition_status", SqlDbType.VarChar, 4)]
        public string english_language_acquisition_status { get; set; }
        [ColumnMapping(192, 192)]
        [ColumnMetaData("language_code", SqlDbType.Char, 3)]
        public string language_code { get; set; }
        [ColumnMapping(193, 193)]
        [ColumnMetaData("economic_disadvantage_status", SqlDbType.VarChar, 3)]
        public string economic_disadvantage_status { get; set; }
        [ColumnMapping(194, 194)]
        [ColumnMetaData("primary_disability_type", SqlDbType.VarChar, 3)]
        public string primary_disability_type { get; set; }
        [ColumnMapping(195, 195)]
        [ColumnMetaData("nonpublic_nonsectarian_school_code", SqlDbType.Char, 7)]
        public string nonpublic_nonsectarian_school_code { get; set; }
        [ColumnMapping(196, 196)]
        [ColumnMetaData("hispanic_or_latino", SqlDbType.VarChar, 3)]
        public string hispanic_or_latino { get; set; }
        [ColumnMapping(197, 197)]
        [ColumnMetaData("american_indian_or_alaska_native", SqlDbType.VarChar, 3)]
        public string american_indian_or_alaska_native { get; set; }
        [ColumnMapping(198, 198)]
        [ColumnMetaData("asian", SqlDbType.VarChar, 3)]
        public string asian { get; set; }
        [ColumnMapping(199, 199)]
        [ColumnMetaData("native_hawaiian_or_other_pacific_islander", SqlDbType.VarChar, 3)]
        public string native_hawaiian_or_other_pacific_islander { get; set; }
        [ColumnMapping(200, 200)]
        [ColumnMetaData("filipino", SqlDbType.VarChar, 3)]
        public string filipino { get; set; }
        [ColumnMapping(201, 201)]
        [ColumnMetaData("black_or_african_american", SqlDbType.VarChar, 3)]
        public string black_or_african_american { get; set; }
        [ColumnMapping(202, 202)]
        [ColumnMetaData("white", SqlDbType.VarChar, 3)]
        public string white { get; set; }
        [ColumnMapping(203, 203)]
        [ColumnMetaData("two_or_more_races", SqlDbType.VarChar, 3)]
        public string two_or_more_races { get; set; }
        [ColumnMapping(204, 204)]
        [ColumnMetaData("reporting_ethnicity", SqlDbType.Char, 3)]
        public string reporting_ethnicity { get; set; }
        [ColumnMapping(205, 205)]
        [ColumnMetaData("parent_education_level", SqlDbType.Char, 2)]
        public string parent_education_level { get; set; }
        [ColumnMapping(206, 206)]
        [ColumnMetaData("ela_pt_attempts", SqlDbType.TinyInt, 2)]
        public byte? ela_pt_attempts { get; set; }
        [ColumnMapping(207, 207)]
        [ColumnMetaData("ela_npt_attempts", SqlDbType.TinyInt, 2)]
        public byte? ela_npt_attempts { get; set; }
        [ColumnMapping(208, 208)]
        [ColumnMetaData("mat_pt_attempts", SqlDbType.TinyInt, 2)]
        public byte? mat_pt_attempts { get; set; }
        [ColumnMapping(209, 209)]
        [ColumnMetaData("mat_npt_attempts", SqlDbType.TinyInt, 2)]
        public byte? mat_npt_attempts { get; set; }
        [ColumnMapping(210, 210)]
        [ColumnMetaData("sci_attempts", SqlDbType.TinyInt, 2)]
        public byte? sci_attempts { get; set; }
        [ColumnMapping(211, 211)]
        [ColumnMetaData("rla_attempts", SqlDbType.TinyInt, 2)]
        public byte? rla_attempts { get; set; }
        [ColumnMapping(212, 212)]
        [ColumnMetaData("ela_attemptedness", SqlDbType.Char, 1)]
        public string ela_attemptedness { get; set; }
        [ColumnMapping(213, 213)]
        [ColumnMetaData("mat_attemptedness", SqlDbType.Char, 1)]
        public string mat_attemptedness { get; set; }
        [ColumnMapping(214, 214)]
        [ColumnMetaData("ela_invalidated_status", SqlDbType.Char, 1)]
        public string ela_invalidated_status { get; set; }
        [ColumnMapping(215, 215)]
        [ColumnMetaData("mat_invalidated_status", SqlDbType.Char, 1)]
        public string mat_invalidated_status { get; set; }
        [ColumnMapping(216, 216)]
        [ColumnMetaData("ela_test_delivery_type", SqlDbType.Char, 1)]
        public string ela_test_delivery_type { get; set; }
        [ColumnMapping(217, 217)]
        [ColumnMetaData("mat_test_delivery_type", SqlDbType.Char, 1)]
        public string mat_test_delivery_type { get; set; }
        [ColumnMapping(218, 218)]
        [ColumnMetaData("sci_test_results_reported", SqlDbType.Char, 1)]
        public string sci_test_results_reported { get; set; }
        [ColumnMapping(219, 219)]
        [ColumnMetaData("ela_rla_aggregate_reporting", SqlDbType.Char, 1)]
        public string ela_rla_aggregate_reporting { get; set; }
        [ColumnMapping(220, 220)]
        [ColumnMetaData("mat_aggregate_reporting", SqlDbType.Char, 1)]
        public string mat_aggregate_reporting { get; set; }
        [ColumnMapping(221, 221)]
        [ColumnMetaData("sci_aggregate_reporting", SqlDbType.Char, 1)]
        public string sci_aggregate_reporting { get; set; }
        [ColumnMapping(222, 222)]
        [ColumnMetaData("filler_00", SqlDbType.Char, 8)]
        public string filler_00 { get; set; }
        [ColumnMapping(223, 223)]
        [ColumnMetaData("filler_01", SqlDbType.Char, 8)]
        public string filler_01 { get; set; }
        [ColumnMapping(224, 224)]
        [ColumnMetaData("sci_raw_score", SqlDbType.TinyInt, 2)]
        public byte? sci_raw_score { get; set; }
        [ColumnMapping(225, 225)]
        [ColumnMetaData("rla_raw_score", SqlDbType.TinyInt, 2)]
        public byte? rla_raw_score { get; set; }
        [ColumnMapping(226, 226)]
        [ColumnMetaData("ela_reading", SqlDbType.Char, 1)]
        public string ela_reading { get; set; }
        [ColumnMapping(227, 227)]
        [ColumnMetaData("ela_writing", SqlDbType.Char, 1)]
        public string ela_writing { get; set; }
        [ColumnMapping(228, 228)]
        [ColumnMetaData("ela_listening", SqlDbType.Char, 1)]
        public string ela_listening { get; set; }
        [ColumnMapping(229, 229)]
        [ColumnMetaData("ela_research_inquiry", SqlDbType.Char, 1)]
        public string ela_research_inquiry { get; set; }
        [ColumnMapping(230, 230)]
        [ColumnMetaData("mat_concepts_procedures", SqlDbType.Char, 1)]
        public string mat_concepts_procedures { get; set; }
        [ColumnMapping(231, 231)]
        [ColumnMetaData("mat_problem_solving_modeling", SqlDbType.Char, 1)]
        public string mat_problem_solving_modeling { get; set; }
        [ColumnMapping(232, 232)]
        [ColumnMetaData("mat_communicating_reasoning", SqlDbType.Char, 1)]
        public string mat_communicating_reasoning { get; set; }
        [ColumnMapping(233, 233)]
        [ColumnMetaData("ela_scaled_score", SqlDbType.VarChar, 4)]
        public string ela_scaled_score { get; set; }
        [ColumnMapping(234, 234)]
        [ColumnMetaData("mat_scaled_score", SqlDbType.VarChar, 4)]
        public string mat_scaled_score { get; set; }
        [ColumnMapping(235, 235)]
        [ColumnMetaData("sci_scaled_score", SqlDbType.VarChar, 3)]
        public string sci_scaled_score { get; set; }
        [ColumnMapping(236, 236)]
        [ColumnMetaData("rla_scaled_score", SqlDbType.VarChar, 3)]
        public string rla_scaled_score { get; set; }
        [ColumnMapping(237, 237)]
        [ColumnMetaData("ela_standard_error_measurement", SqlDbType.Char, 4)]
        public string ela_standard_error_measurement { get; set; }
        [ColumnMapping(238, 238)]
        [ColumnMetaData("ela_error_band_min", SqlDbType.Char, 4)]
        public string ela_error_band_min { get; set; }
        [ColumnMapping(239, 239)]
        [ColumnMetaData("ela_error_band_max", SqlDbType.Char, 4)]
        public string ela_error_band_max { get; set; }
        [ColumnMapping(240, 240)]
        [ColumnMetaData("mat_standard_error_measurement", SqlDbType.Char, 4)]
        public string mat_standard_error_measurement { get; set; }
        [ColumnMapping(241, 241)]
        [ColumnMetaData("mat_error_band_min", SqlDbType.Char, 4)]
        public string mat_error_band_min { get; set; }
        [ColumnMapping(242, 242)]
        [ColumnMetaData("mat_error_band_max", SqlDbType.Char, 4)]
        public string mat_error_band_max { get; set; }
        [ColumnMapping(243, 243)]
        [ColumnMetaData("rla_performance_level", SqlDbType.Char, 1)]
        public string rla_performance_level { get; set; }
        [ColumnMapping(244, 244)]
        [ColumnMetaData("sci_performance_level", SqlDbType.Char, 1)]
        public string sci_performance_level { get; set; }
        [ColumnMapping(245, 245)]
        [ColumnMetaData("ela_achievement_level", SqlDbType.Char, 1)]
        public string ela_achievement_level { get; set; }
        [ColumnMapping(246, 246)]
        [ColumnMetaData("mat_achievement_level", SqlDbType.Char, 1)]
        public string mat_achievement_level { get; set; }
        [ColumnMapping(247, 247)]
        [ColumnMetaData("test_version_taken", SqlDbType.Char, 2)]
        public string test_version_taken { get; set; }
        [ColumnMapping(248, 248)]
        [ColumnMetaData("eap_ela_authorized_release", SqlDbType.Char, 1)]
        public string eap_ela_authorized_release { get; set; }
        [ColumnMapping(249, 249)]
        [ColumnMetaData("eap_mat_authorized_release", SqlDbType.Char, 1)]
        public string eap_mat_authorized_release { get; set; }
        [ColumnMapping(250, 250)]
        [ColumnMetaData("filler_03", SqlDbType.VarChar, 129)]
        public string filler_03 { get; set; }
    }
}
