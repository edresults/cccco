﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.CAASPP
{
    class Caaspp20202021
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("record_type", SqlDbType.Char, 2)]
        public string record_type { get; set; }

        [ColumnMapping(1, 1)]
        [ColumnMetaData("Statewide_Student_Identifier", SqlDbType.Char, 10)]
        public string Statewide_Student_Identifier { get; set; }

        [ColumnMapping(2, 2)]
        [ColumnMetaData("Student_Last_Name", SqlDbType.Char, 50)]
        public string Student_Last_Name { get; set; }

        [ColumnMapping(3, 3)]
        [ColumnMetaData("Student_First_Name", SqlDbType.Char, 30)]
        public string Student_First_Name { get; set; }

        [ColumnMapping(4, 4)]
        [ColumnMetaData("Student_Middle_Name", SqlDbType.Char, 30)]
        public string Student_Middle_Name { get; set; }

        [ColumnMapping(5, 5)]
        [ColumnMetaData("Date_of_Birth", SqlDbType.Char, 10)]
        public string Date_of_Birth { get; set; }

        [ColumnMapping(6, 6)]
        [ColumnMetaData("Gender", SqlDbType.Char, 1)]
        public string Gender { get; set; }

        [ColumnMapping(7, 7)]
        [ColumnMetaData("blank00", SqlDbType.Char, 2)]
        public string blank00 { get; set; }

        [ColumnMapping(8, 8)]
        [ColumnMetaData("blank01", SqlDbType.Char, 3)]
        public string blank01 { get; set; }

        [ColumnMapping(9, 9)]
        [ColumnMetaData("Calpads_Grade", SqlDbType.Char, 2)]
        public string Calpads_Grade { get; set; }

        [ColumnMapping(10, 10)]
        [ColumnMetaData("Grade_Assessed", SqlDbType.Char, 2)]
        public string Grade_Assessed { get; set; }

        [ColumnMapping(11, 11)]
        [ColumnMetaData("Calpads_District_Code", SqlDbType.Char, 14)]
        public string Calpads_District_Code { get; set; }

        [ColumnMapping(12, 12)]
        [ColumnMetaData("Calpads_District_Name", SqlDbType.Char, 100)]
        public string Calpads_District_Name { get; set; }

        [ColumnMapping(13, 13)]
        [ColumnMetaData("Calpads_School_Code", SqlDbType.Char, 14)]
        public string Calpads_School_Code { get; set; }

        [ColumnMapping(14, 14)]
        [ColumnMetaData("Calpads_School_Name", SqlDbType.Char, 100)]
        public string Calpads_School_Name { get; set; }

        [ColumnMapping(15, 15)]
        [ColumnMetaData("Calpads_Charter_Code", SqlDbType.Char, 4)]
        public string Calpads_Charter_Code { get; set; }

        [ColumnMapping(16, 16)]
        [ColumnMetaData("Calpads_Charter_School_Indicator", SqlDbType.Char, 2)]
        public string Calpads_Charter_School_Indicator { get; set; }

        [ColumnMapping(17, 17)]
        [ColumnMetaData("County_District_Code_of_Accountability", SqlDbType.Char, 7)]
        public string County_District_Code_of_Accountability { get; set; }

        [ColumnMapping(18, 18)]
        [ColumnMetaData("Section_504_Status", SqlDbType.Char, 3)]
        public string Section_504_Status { get; set; }

        [ColumnMapping(19, 19)]
        [ColumnMetaData("Primary_Disability_Type", SqlDbType.Char, 3)]
        public string Primary_Disability_Type { get; set; }

        [ColumnMapping(20, 20)]
        [ColumnMetaData("Primary_Disability_Type_Testing", SqlDbType.Char, 3)]
        public string Primary_Disability_Type_Testing { get; set; }

        [ColumnMapping(21, 21)]
        [ColumnMetaData("IDEA_Indicator", SqlDbType.Char, 3)]
        public string IDEA_Indicator { get; set; }

        [ColumnMapping(22, 22)]
        [ColumnMetaData("IDEA_Indicator_Testing", SqlDbType.Char, 3)]
        public string IDEA_Indicator_Testing { get; set; }

        [ColumnMapping(23, 23)]
        [ColumnMetaData("Migrant_Status", SqlDbType.Char, 3)]
        public string Migrant_Status { get; set; }

        [ColumnMapping(24, 24)]
        [ColumnMetaData("EL_Status", SqlDbType.Char, 3)]
        public string EL_Status { get; set; }

        [ColumnMapping(25, 25)]
        [ColumnMetaData("EL_ID_Date", SqlDbType.Char, 10)]
        public string EL_ID_Date { get; set; }

        [ColumnMapping(26, 26)]
        [ColumnMetaData("RFEP_Date", SqlDbType.Char, 10)]
        public string RFEP_Date { get; set; }

        [ColumnMapping(27, 27)]
        [ColumnMetaData("First_Entry_Date_in_US_School", SqlDbType.Char, 10)]
        public string First_Entry_Date_in_US_School { get; set; }

        [ColumnMapping(28, 28)]
        [ColumnMetaData("Enrollment_Effective_Date", SqlDbType.Char, 10)]
        public string Enrollment_Effective_Date { get; set; }

        [ColumnMapping(29, 29)]
        [ColumnMetaData("English_Language_Acquisition_Status", SqlDbType.Char, 4)]
        public string English_Language_Acquisition_Status { get; set; }

        [ColumnMapping(30, 30)]
        [ColumnMetaData("Language_Code", SqlDbType.Char, 3)]
        public string Language_Code { get; set; }

        [ColumnMapping(31, 31)]
        [ColumnMetaData("Primary_Language", SqlDbType.Char, 3)]
        public string Primary_Language { get; set; }

        [ColumnMapping(32, 32)]
        [ColumnMetaData("Military_Status", SqlDbType.Char, 3)]
        public string Military_Status { get; set; }

        [ColumnMapping(33, 33)]
        [ColumnMetaData("Foster_Status", SqlDbType.Char, 3)]
        public string Foster_Status { get; set; }

        [ColumnMapping(34, 34)]
        [ColumnMetaData("Homeless_Status", SqlDbType.Char, 3)]
        public string Homeless_Status { get; set; }

        [ColumnMapping(35, 35)]
        [ColumnMetaData("Calpads_Ecoonomic_Disadvantage_Status", SqlDbType.Char, 3)]
        public string Calpads_Ecoonomic_Disadvantage_Status { get; set; }

        [ColumnMapping(36, 36)]
        [ColumnMetaData("Calpads_Ecoonomic_Disadvantage_Status_Testing", SqlDbType.Char, 3)]
        public string Calpads_Ecoonomic_Disadvantage_Status_Testing { get; set; }

        [ColumnMapping(37, 37)]
        [ColumnMetaData("NPS_School_Flag", SqlDbType.Char, 1)]
        public string NPS_School_Flag { get; set; }

        [ColumnMapping(38, 38)]
        [ColumnMetaData("Hispanic_Latino", SqlDbType.Char, 3)]
        public string Hispanic_Latino { get; set; }

        [ColumnMapping(39, 39)]
        [ColumnMetaData("American_Indian_Alaska_Native", SqlDbType.Char, 3)]
        public string American_Indian_Alaska_Native { get; set; }

        [ColumnMapping(40, 40)]
        [ColumnMetaData("Asian", SqlDbType.Char, 3)]
        public string Asian { get; set; }

        [ColumnMapping(41, 41)]
        [ColumnMetaData("Native_Hawaiian_Pacific_Islander", SqlDbType.Char, 3)]
        public string Native_Hawaiian_Pacific_Islander { get; set; }

        [ColumnMapping(42, 42)]
        [ColumnMetaData("Filipino", SqlDbType.Char, 3)]
        public string Filipino { get; set; }

        [ColumnMapping(43, 43)]
        [ColumnMetaData("Black_African_American", SqlDbType.Char, 3)]
        public string Black_African_American { get; set; }

        [ColumnMapping(44, 44)]
        [ColumnMetaData("White", SqlDbType.Char, 3)]
        public string White { get; set; }

        [ColumnMapping(45, 45)]
        [ColumnMetaData("Two_More_Races", SqlDbType.Char, 3)]
        public string Two_More_Races { get; set; }

        [ColumnMapping(46, 46)]
        [ColumnMetaData("Reporting_Ethinicty", SqlDbType.Char, 3)]
        public string Reporting_Ethinicty { get; set; }

        [ColumnMapping(47, 47)]
        [ColumnMetaData("Parent_Education_Level", SqlDbType.Char, 2)]
        public string Parent_Education_Level { get; set; }

        [ColumnMapping(48, 48)]
        [ColumnMetaData("blank02", SqlDbType.Char, 1)]
        public string blank02 { get; set; }

        [ColumnMapping(49, 49)]
        [ColumnMetaData("Opportunity_ID1", SqlDbType.Char, 16)]
        public string Opportunity_ID1 { get; set; }

        [ColumnMapping(50, 50)]
        [ColumnMetaData("Opportunity_ID1_Testing_Status", SqlDbType.Char, 1)]
        public string Opportunity_ID1_Testing_Status { get; set; }

        [ColumnMapping(51, 51)]
        [ColumnMetaData("Opportunity_ID2", SqlDbType.Char, 16)]
        public string Opportunity_ID2 { get; set; }

        [ColumnMapping(52, 52)]
        [ColumnMetaData("Opportunity_ID2_Testing_Status", SqlDbType.Char, 1)]
        public string Opportunity_ID2_Testing_Status { get; set; }

        [ColumnMapping(53, 53)]
        [ColumnMetaData("Opportunity_ID3", SqlDbType.Char, 16)]
        public string Opportunity_ID3 { get; set; }

        [ColumnMapping(54, 54)]
        [ColumnMetaData("Opportunity_ID3_Testing_Status", SqlDbType.Char, 1)]
        public string Opportunity_ID3_Testing_Status { get; set; }

        [ColumnMapping(55, 55)]
        [ColumnMetaData("Opportunity_ID4", SqlDbType.Char, 16)]
        public string Opportunity_ID4 { get; set; }

        [ColumnMapping(56, 56)]
        [ColumnMetaData("Opportunity_ID4_Testing_Status", SqlDbType.Char, 1)]
        public string Opportunity_ID4_Testing_Status { get; set; }

        [ColumnMapping(57, 57)]
        [ColumnMetaData("Test_Registration_ID", SqlDbType.Char, 16)]
        public string Test_Registration_ID { get; set; }

        [ColumnMapping(58, 58)]
        [ColumnMetaData("Tested_LEA_Name_01", SqlDbType.Char, 100)]
        public string Tested_LEA_Name_01 { get; set; }

        [ColumnMapping(59, 59)]
        [ColumnMetaData("Tested_County_District_Code_01", SqlDbType.Char, 14)]
        public string Tested_County_District_Code_01 { get; set; }

        [ColumnMapping(60, 60)]
        [ColumnMetaData("Tested_School_Name_01", SqlDbType.Char, 100)]
        public string Tested_School_Name_01 { get; set; }

        [ColumnMapping(61, 61)]
        [ColumnMetaData("Tested_School_Code_01", SqlDbType.Char, 14)]
        public string Tested_School_Code_01 { get; set; }

        [ColumnMapping(62, 62)]
        [ColumnMetaData("Tested_Charter_School_Indicator_01", SqlDbType.Char, 2)]
        public string Tested_Charter_School_Indicator_01 { get; set; }

        [ColumnMapping(63, 63)]
        [ColumnMetaData("Tested_Charter_Code_01", SqlDbType.Char, 4)]
        public string Tested_Charter_Code_01 { get; set; }

        [ColumnMapping(64, 64)]
        [ColumnMetaData("Tested_School_NPS_Flag_01", SqlDbType.Char, 1)]
        public string Tested_School_NPS_Flag_01 { get; set; }

        [ColumnMapping(65, 65)]
        [ColumnMetaData("Paper_Pencil_Test_Completion_Date", SqlDbType.Char, 10)]
        public string Paper_Pencil_Test_Completion_Date { get; set; }

        [ColumnMapping(66, 66)]
        [ColumnMetaData("Tested_LEA_Name_02", SqlDbType.Char, 100)]
        public string Tested_LEA_Name_02 { get; set; }

        [ColumnMapping(67, 67)]
        [ColumnMetaData("Tested_County_District_Code_02", SqlDbType.Char, 14)]
        public string Tested_County_District_Code_02 { get; set; }

        [ColumnMapping(68, 68)]
        [ColumnMetaData("Tested_School_Name_02", SqlDbType.Char, 100)]
        public string Tested_School_Name_02 { get; set; }

        [ColumnMapping(69, 69)]
        [ColumnMetaData("Tested_School_Code_02", SqlDbType.Char, 14)]
        public string Tested_School_Code_02 { get; set; }

        [ColumnMapping(70, 70)]
        [ColumnMetaData("Tested_Charter_School_Indicator_02", SqlDbType.Char, 2)]
        public string Tested_Charter_School_Indicator_02 { get; set; }

        [ColumnMapping(71, 71)]
        [ColumnMetaData("Tested_Charter_Code_02", SqlDbType.Char, 4)]
        public string Tested_Charter_Code_02 { get; set; }

        [ColumnMapping(72, 72)]
        [ColumnMetaData("Tested_School_NPS_Flag_02", SqlDbType.Char, 1)]
        public string Tested_School_NPS_Flag_02 { get; set; }

        [ColumnMapping(73, 73)]
        [ColumnMetaData("Tested_LEA_Name_03", SqlDbType.Char, 100)]
        public string Tested_LEA_Name_03 { get; set; }

        [ColumnMapping(74, 74)]
        [ColumnMetaData("Tested_County_District_Code_03", SqlDbType.Char, 14)]
        public string Tested_County_District_Code_03 { get; set; }

        [ColumnMapping(75, 75)]
        [ColumnMetaData("Tested_School_Name_03", SqlDbType.Char, 100)]
        public string Tested_School_Name_03 { get; set; }

        [ColumnMapping(76, 76)]
        [ColumnMetaData("Tested_School_Code_03", SqlDbType.Char, 14)]
        public string Tested_School_Code_03 { get; set; }

        [ColumnMapping(77, 77)]
        [ColumnMetaData("Tested_Charter_School_Indicator_03", SqlDbType.Char, 2)]
        public string Tested_Charter_School_Indicator_03 { get; set; }

        [ColumnMapping(78, 78)]
        [ColumnMetaData("Tested_Charter_Code_03", SqlDbType.Char, 4)]
        public string Tested_Charter_Code_03 { get; set; }

        [ColumnMapping(79, 79)]
        [ColumnMetaData("Tested_School_NPS_Flag_03", SqlDbType.Char, 1)]
        public string Tested_School_NPS_Flag_03 { get; set; }

        [ColumnMapping(80, 80)]
        [ColumnMetaData("Tested_LEA_Name_04", SqlDbType.Char, 100)]
        public string Tested_LEA_Name_04 { get; set; }

        [ColumnMapping(81, 81)]
        [ColumnMetaData("Tested_County_District_Code_04", SqlDbType.Char, 14)]
        public string Tested_County_District_Code_04 { get; set; }

        [ColumnMapping(82, 82)]
        [ColumnMetaData("Tested_School_Name_04", SqlDbType.Char, 100)]
        public string Tested_School_Name_04 { get; set; }

        [ColumnMapping(83, 83)]
        [ColumnMetaData("Tested_School_Code_04", SqlDbType.Char, 14)]
        public string Tested_School_Code_04 { get; set; }

        [ColumnMapping(84, 84)]
        [ColumnMetaData("Tested_Charter_School_Indicator_04", SqlDbType.Char, 2)]
        public string Tested_Charter_School_Indicator_04 { get; set; }

        [ColumnMapping(85, 85)]
        [ColumnMetaData("Tested_Charter_Code_04", SqlDbType.Char, 4)]
        public string Tested_Charter_Code_04 { get; set; }

        [ColumnMapping(86, 86)]
        [ColumnMetaData("Tested_School_NPS_Flag_04", SqlDbType.Char, 1)]
        public string Tested_School_NPS_Flag_04 { get; set; }

        [ColumnMapping(87, 87)]
        [ColumnMetaData("Test_Start_Date_01", SqlDbType.Char, 10)]
        public string Test_Start_Date_01 { get; set; }

        [ColumnMapping(88, 88)]
        [ColumnMetaData("Test_Completion_Date_01", SqlDbType.Char, 10)]
        public string Test_Completion_Date_01 { get; set; }

        [ColumnMapping(89, 89)]
        [ColumnMetaData("Test_Start_Date_02", SqlDbType.Char, 10)]
        public string Test_Start_Date_02 { get; set; }

        [ColumnMapping(90, 90)]
        [ColumnMetaData("Test_Completion_Date_02", SqlDbType.Char, 10)]
        public string Test_Completion_Date_02 { get; set; }

        [ColumnMapping(91, 91)]
        [ColumnMetaData("Test_Start_Date_03", SqlDbType.Char, 10)]
        public string Test_Start_Date_03 { get; set; }

        [ColumnMapping(92, 92)]
        [ColumnMetaData("Test_Completion_Date_03", SqlDbType.Char, 10)]
        public string Test_Completion_Date_03 { get; set; }

        [ColumnMapping(93, 93)]
        [ColumnMetaData("Test_Start_Date_04", SqlDbType.Char, 10)]
        public string Test_Start_Date_04 { get; set; }

        [ColumnMapping(94, 94)]
        [ColumnMetaData("Test_Completion_Date_04", SqlDbType.Char, 10)]
        public string Test_Completion_Date_04 { get; set; }

        [ColumnMapping(95, 95)]
        [ColumnMetaData("Final_Tested_LEA_Name", SqlDbType.Char, 100)]
        public string Final_Tested_LEA_Name { get; set; }

        [ColumnMapping(96, 96)]
        [ColumnMetaData("Final_Tested_County_District_Code", SqlDbType.Char, 14)]
        public string Final_Tested_County_District_Code { get; set; }

        [ColumnMapping(97, 97)]
        [ColumnMetaData("FInal_Tested_School_Name", SqlDbType.Char, 100)]
        public string FInal_Tested_School_Name { get; set; }

        [ColumnMapping(98, 98)]
        [ColumnMetaData("Final_Tested_School_Code", SqlDbType.Char, 14)]
        public string Final_Tested_School_Code { get; set; }

        [ColumnMapping(99, 99)]
        [ColumnMetaData("Final_Tested_Charter_School_Indicator", SqlDbType.Char, 2)]
        public string Final_Tested_Charter_School_Indicator { get; set; }

        [ColumnMapping(100, 100)]
        [ColumnMetaData("Final_Tested_Charter_Code", SqlDbType.Char, 4)]
        public string Final_Tested_Charter_Code { get; set; }

        [ColumnMapping(101, 101)]
        [ColumnMetaData("Final_Tested_School_NPS_Flag", SqlDbType.Char, 1)]
        public string Final_Tested_School_NPS_Flag { get; set; }

        [ColumnMapping(102, 102)]
        [ColumnMetaData("Final_Test_Completed_Date", SqlDbType.Char, 10)]
        public string Final_Test_Completed_Date { get; set; }

        [ColumnMapping(103, 103)]
        [ColumnMetaData("School_Selected_Start_Of_Test_WIndow_01", SqlDbType.Char, 10)]
        public string School_Selected_Start_Of_Test_WIndow_01 { get; set; }

        [ColumnMapping(104, 104)]
        [ColumnMetaData("School_Selected_End_Of_Test_WIndow_01", SqlDbType.Char, 10)]
        public string School_Selected_End_Of_Test_WIndow_01 { get; set; }

        [ColumnMapping(105, 105)]
        [ColumnMetaData("School_Selected_Start_Of_Test_WIndow_02", SqlDbType.Char, 10)]
        public string School_Selected_Start_Of_Test_WIndow_02 { get; set; }

        [ColumnMapping(106, 106)]
        [ColumnMetaData("School_Selected_End_Of_Test_WIndow_02", SqlDbType.Char, 10)]
        public string School_Selected_End_Of_Test_WIndow_02 { get; set; }

        [ColumnMapping(107, 107)]
        [ColumnMetaData("School_Selected_Start_Of_Test_WIndow_03", SqlDbType.Char, 10)]
        public string School_Selected_Start_Of_Test_WIndow_03 { get; set; }

        [ColumnMapping(108, 108)]
        [ColumnMetaData("School_Selected_End_Of_Test_WIndow_03", SqlDbType.Char, 10)]
        public string School_Selected_End_Of_Test_WIndow_03 { get; set; }

        [ColumnMapping(109, 109)]
        [ColumnMetaData("School_Selected_Start_Of_Test_WIndow_04", SqlDbType.Char, 10)]
        public string School_Selected_Start_Of_Test_WIndow_04 { get; set; }

        [ColumnMapping(110, 110)]
        [ColumnMetaData("School_Selected_End_Of_Test_WIndow_04", SqlDbType.Char, 10)]
        public string School_Selected_End_Of_Test_WIndow_04 { get; set; }

        [ColumnMapping(111, 111)]
        [ColumnMetaData("Student_Exit_Code", SqlDbType.Char, 4)]
        public string Student_Exit_Code { get; set; }

        [ColumnMapping(112, 112)]
        [ColumnMetaData("Student_Exit_Withdrawal_Date", SqlDbType.Char, 10)]
        public string Student_Exit_Withdrawal_Date { get; set; }

        [ColumnMapping(113, 113)]
        [ColumnMetaData("Student_Removed_from_Calpads_File_Date", SqlDbType.Char, 10)]
        public string Student_Removed_from_Calpads_File_Date { get; set; }

        [ColumnMapping(114, 114)]
        [ColumnMetaData("Cast_Last_Science_Class_Flag", SqlDbType.Char, 1)]
        public string Cast_Last_Science_Class_Flag { get; set; }

        [ColumnMapping(115, 115)]
        [ColumnMetaData("Condition_Code", SqlDbType.Char, 4)]
        public string Condition_Code { get; set; }

        [ColumnMapping(116, 116)]
        [ColumnMetaData("Attemptedness", SqlDbType.Char, 1)]
        public string Attemptedness { get; set; }

        [ColumnMapping(117, 117)]
        [ColumnMetaData("Score_Status", SqlDbType.Char, 1)]
        public string Score_Status { get; set; }

        [ColumnMapping(118, 118)]
        [ColumnMetaData("Unlisted_Resource_Construct_Change", SqlDbType.Char, 1)]
        public string Unlisted_Resource_Construct_Change { get; set; }

        [ColumnMapping(119, 119)]
        [ColumnMetaData("Test_Mode", SqlDbType.Char, 1)]
        public string Test_Mode { get; set; }

        [ColumnMapping(120, 120)]
        [ColumnMetaData("Include_Indicator", SqlDbType.Char, 1)]
        public string Include_Indicator { get; set; }

        [ColumnMapping(121, 121)]
        [ColumnMetaData("Remote_Tester_01", SqlDbType.Char, 1)]
        public string Remote_Tester_01 { get; set; }

        [ColumnMapping(122, 122)]
        [ColumnMetaData("Remote_Tester_02", SqlDbType.Char, 1)]
        public string Remote_Tester_02 { get; set; }

        [ColumnMapping(123, 123)]
        [ColumnMetaData("Remote_Tester_03", SqlDbType.Char, 1)]
        public string Remote_Tester_03 { get; set; }

        [ColumnMapping(124, 124)]
        [ColumnMetaData("Remote_Tester_04", SqlDbType.Char, 1)]
        public string Remote_Tester_04 { get; set; }

        [ColumnMapping(125, 125)]
        [ColumnMetaData("SSR_Eligible", SqlDbType.Char, 1)]
        public string SSR_Eligible { get; set; }

        [ColumnMapping(126, 126)]
        [ColumnMetaData("Lexile_Quantile_Measure", SqlDbType.Char, 6)]
        public string Lexile_Quantile_Measure { get; set; }

        [ColumnMapping(127, 127)]
        [ColumnMetaData("blank03", SqlDbType.Char, 8)]
        public string blank03 { get; set; }

        [ColumnMapping(128, 128)]
        [ColumnMetaData("blank04", SqlDbType.Char, 8)]
        public string blank04 { get; set; }

        [ColumnMapping(129, 129)]
        [ColumnMetaData("Raw_Score_01", SqlDbType.Char, 2)]
        public string Raw_Score_01 { get; set; }

        [ColumnMapping(130, 130)]
        [ColumnMetaData("Raw_Score_02", SqlDbType.Char, 2)]
        public string Raw_Score_02 { get; set; }

        [ColumnMapping(131, 131)]
        [ColumnMetaData("Raw_Score_03", SqlDbType.Char, 2)]
        public string Raw_Score_03 { get; set; }

        [ColumnMapping(132, 132)]
        [ColumnMetaData("Raw_Score_04", SqlDbType.Char, 2)]
        public string Raw_Score_04 { get; set; }

        [ColumnMapping(133, 133)]
        [ColumnMetaData("blank05", SqlDbType.Char, 19)]
        public string blank05 { get; set; }

        [ColumnMapping(134, 134)]
        [ColumnMetaData("blank06", SqlDbType.Char, 19)]
        public string blank06 { get; set; }

        [ColumnMapping(135, 135)]
        [ColumnMetaData("blank07", SqlDbType.Char, 19)]
        public string blank07 { get; set; }

        [ColumnMapping(136, 136)]
        [ColumnMetaData("blank08", SqlDbType.Char, 19)]
        public string blank08 { get; set; }

        [ColumnMapping(137, 137)]
        [ColumnMetaData("blank09", SqlDbType.Char, 19)]
        public string blank09 { get; set; }

        [ColumnMapping(138, 138)]
        [ColumnMetaData("blank10", SqlDbType.Char, 19)]
        public string blank10 { get; set; }

        [ColumnMapping(139, 139)]
        [ColumnMetaData("blank11", SqlDbType.Char, 19)]
        public string blank11 { get; set; }

        [ColumnMapping(140, 140)]
        [ColumnMetaData("blank12", SqlDbType.Char, 19)]
        public string blank12 { get; set; }

        [ColumnMapping(141, 141)]
        [ColumnMetaData("Scale_Score", SqlDbType.Char, 4)]
        public string Scale_Score { get; set; }

        [ColumnMapping(142, 142)]
        [ColumnMetaData("Standard_Error_Measurement", SqlDbType.Char, 4)]
        public string Standard_Error_Measurement { get; set; }

        [ColumnMapping(143, 143)]
        [ColumnMetaData("Smarter_Scale_Scores_Error_Bands_Min", SqlDbType.Char, 4)]
        public string Smarter_Scale_Scores_Error_Bands_Min { get; set; }

        [ColumnMapping(144, 144)]
        [ColumnMetaData("Smarter_Scale_Scores_Error_Bands_Max", SqlDbType.Char, 4)]
        public string Smarter_Scale_Scores_Error_Bands_Max { get; set; }

        [ColumnMapping(145, 145)]
        [ColumnMetaData("Achievement_Levels", SqlDbType.Char, 1)]
        public string Achievement_Levels { get; set; }

        [ColumnMapping(146, 146)]
        [ColumnMetaData("Domain_01_Level", SqlDbType.Char, 1)]
        public string Domain_01_Level { get; set; }

        [ColumnMapping(147, 147)]
        [ColumnMetaData("Domain_02_Level", SqlDbType.Char, 1)]
        public string Domain_02_Level { get; set; }

        [ColumnMapping(148, 148)]
        [ColumnMetaData("Domain_03_Level", SqlDbType.Char, 1)]
        public string Domain_03_Level { get; set; }

        [ColumnMapping(149, 149)]
        [ColumnMetaData("Genre", SqlDbType.Char, 4)]
        public string Genre { get; set; }

        [ColumnMapping(150, 150)]
        [ColumnMetaData("WER_POR", SqlDbType.Char, 1)]
        public string WER_POR { get; set; }

        [ColumnMapping(151, 151)]
        [ColumnMetaData("WER_DEV_EEL", SqlDbType.Char, 1)]
        public string WER_DEV_EEL { get; set; }

        [ColumnMapping(152, 152)]
        [ColumnMetaData("WER_COV", SqlDbType.Char, 1)]
        public string WER_COV { get; set; }

        [ColumnMapping(153, 153)]
        [ColumnMetaData("EAP", SqlDbType.Char, 1)]
        public string EAP { get; set; }

        [ColumnMapping(154, 154)]
        [ColumnMetaData("Number_of_Items_Attempted_01", SqlDbType.Char, 3)]
        public string Number_of_Items_Attempted_01 { get; set; }

        [ColumnMapping(155, 155)]
        [ColumnMetaData("Number_of_Items_Attempted_02", SqlDbType.Char, 3)]
        public string Number_of_Items_Attempted_02 { get; set; }

        [ColumnMapping(156, 156)]
        [ColumnMetaData("Number_of_Items_Attempted_03", SqlDbType.Char, 3)]
        public string Number_of_Items_Attempted_03 { get; set; }

        [ColumnMapping(157, 157)]
        [ColumnMetaData("Number_of_Items_Attempted_04", SqlDbType.Char, 3)]
        public string Number_of_Items_Attempted_04 { get; set; }

        [ColumnMapping(158, 158)]
        [ColumnMetaData("Accomodations_Available_Indicator", SqlDbType.Char, 3)]
        public string Accomodations_Available_Indicator { get; set; }

        [ColumnMapping(159, 159)]
        [ColumnMetaData("Designated_Support_Available_Indicator", SqlDbType.Char, 3)]
        public string Designated_Support_Available_Indicator { get; set; }

        [ColumnMapping(160, 160)]
        [ColumnMetaData("Embedded_Accomodation_01_American_Sign_Language", SqlDbType.Char, 8)]
        public string Embedded_Accomodation_01_American_Sign_Language { get; set; }

        [ColumnMapping(161, 161)]
        [ColumnMetaData("Embedded_Accomodation_02_American_Sign_Language", SqlDbType.Char, 8)]
        public string Embedded_Accomodation_02_American_Sign_Language { get; set; }

        [ColumnMapping(162, 162)]
        [ColumnMetaData("Embedded_Accomodation_01_Audio_Transcript", SqlDbType.Char, 16)]
        public string Embedded_Accomodation_01_Audio_Transcript { get; set; }

        [ColumnMapping(163, 163)]
        [ColumnMetaData("Embedded_Accomodation_02_Audio_Transcript", SqlDbType.Char, 16)]
        public string Embedded_Accomodation_02_Audio_Transcript { get; set; }

        [ColumnMapping(164, 164)]
        [ColumnMetaData("Embedded_Accomodation_01_Braille", SqlDbType.Char, 11)]
        public string Embedded_Accomodation_01_Braille { get; set; }

        [ColumnMapping(165, 165)]
        [ColumnMetaData("Embedded_Accomodation_02_Braille", SqlDbType.Char, 11)]
        public string Embedded_Accomodation_02_Braille { get; set; }

        [ColumnMapping(166, 166)]
        [ColumnMetaData("Embedded_Accomodation_01_Closed_Captioning", SqlDbType.Char, 14)]
        public string Embedded_Accomodation_01_Closed_Captioning { get; set; }

        [ColumnMapping(167, 167)]
        [ColumnMetaData("Embedded_Accomodation_02_Closed_Captioning", SqlDbType.Char, 14)]
        public string Embedded_Accomodation_02_Closed_Captioning { get; set; }

        [ColumnMapping(168, 168)]
        [ColumnMetaData("Embedded_Accomodation_01_Text_to_Speech", SqlDbType.Char, 12)]
        public string Embedded_Accomodation_01_Text_to_Speech { get; set; }

        [ColumnMapping(169, 169)]
        [ColumnMetaData("Embedded_Accomodation_02_Text_to_Speech", SqlDbType.Char, 12)]
        public string Embedded_Accomodation_02_Text_to_Speech { get; set; }

        [ColumnMapping(170, 170)]
        [ColumnMetaData("Non_Embedded_Accomodation_01_100_Number_Table", SqlDbType.Char, 10)]
        public string Non_Embedded_Accomodation_01_100_Number_Table { get; set; }

        [ColumnMapping(171, 171)]
        [ColumnMetaData("Non_Embedded_Accomodation_02_100_Number_Table", SqlDbType.Char, 10)]
        public string Non_Embedded_Accomodation_02_100_Number_Table { get; set; }

        [ColumnMapping(172, 172)]
        [ColumnMetaData("Non_Embedded_Accomodation_01_Abacus", SqlDbType.Char, 10)]
        public string Non_Embedded_Accomodation_01_Abacus { get; set; }

        [ColumnMapping(173, 173)]
        [ColumnMetaData("Non_Embedded_Accomodation_02_Abacus", SqlDbType.Char, 10)]
        public string Non_Embedded_Accomodation_02_Abacus { get; set; }

        [ColumnMapping(174, 174)]
        [ColumnMetaData("Non_Embedded_Accomodation_03_Abacus", SqlDbType.Char, 10)]
        public string Non_Embedded_Accomodation_03_Abacus { get; set; }

        [ColumnMapping(175, 175)]
        [ColumnMetaData("Non_Embedded_Accomodation_04_Abacus", SqlDbType.Char, 10)]
        public string Non_Embedded_Accomodation_04_Abacus { get; set; }

        [ColumnMapping(176, 176)]
        [ColumnMetaData("Non_Embedded_Accomodation_01_Alternate_Assessments", SqlDbType.Char, 13)]
        public string Non_Embedded_Accomodation_01_Alternate_Assessments { get; set; }

        [ColumnMapping(177, 177)]
        [ColumnMetaData("Non_Embedded_Accomodation_02_Alternate_Assessments", SqlDbType.Char, 13)]
        public string Non_Embedded_Accomodation_02_Alternate_Assessments { get; set; }

        [ColumnMapping(178, 178)]
        [ColumnMetaData("Non_Embedded_Accomodation_03_Alternate_Assessments", SqlDbType.Char, 13)]
        public string Non_Embedded_Accomodation_03_Alternate_Assessments { get; set; }

        [ColumnMapping(179, 179)]
        [ColumnMetaData("Non_Embedded_Accomodation_04_Alternate_Assessments", SqlDbType.Char, 13)]
        public string Non_Embedded_Accomodation_04_Alternate_Assessments { get; set; }

        [ColumnMapping(180, 180)]
        [ColumnMetaData("Non_Embedded_Accomodation_01_Alternate_Response", SqlDbType.Char, 6)]
        public string Non_Embedded_Accomodation_01_Alternate_Response { get; set; }

        [ColumnMapping(181, 181)]
        [ColumnMetaData("Non_Embedded_Accomodation_02_Alternate_Response", SqlDbType.Char, 6)]
        public string Non_Embedded_Accomodation_02_Alternate_Response { get; set; }

        [ColumnMapping(182, 182)]
        [ColumnMetaData("Non_Embedded_Accomodation_03_Alternate_Response", SqlDbType.Char, 6)]
        public string Non_Embedded_Accomodation_03_Alternate_Response { get; set; }

        [ColumnMapping(183, 183)]
        [ColumnMetaData("Non_Embedded_Accomodation_04_Alternate_Response", SqlDbType.Char, 6)]
        public string Non_Embedded_Accomodation_04_Alternate_Response { get; set; }

        [ColumnMapping(184, 184)]
        [ColumnMetaData("Non_Embedded_Accomodation_Braille", SqlDbType.Char, 11)]
        public string Non_Embedded_Accomodation_Braille { get; set; }

        [ColumnMapping(185, 185)]
        [ColumnMetaData("Non_Embedded_Accomodation_01_Calculator", SqlDbType.Char, 8)]
        public string Non_Embedded_Accomodation_01_Calculator { get; set; }

        [ColumnMapping(186, 186)]
        [ColumnMetaData("Non_Embedded_Accomodation_02_Calculator", SqlDbType.Char, 8)]
        public string Non_Embedded_Accomodation_02_Calculator { get; set; }

        [ColumnMapping(187, 187)]
        [ColumnMetaData("Non_Embedded_Accomodation_Large_print", SqlDbType.Char, 6)]
        public string Non_Embedded_Accomodation_Large_print { get; set; }

        [ColumnMapping(188, 188)]
        [ColumnMetaData("Non_Embedded_Accomodation_01_Multiplication_Table", SqlDbType.Char, 6)]
        public string Non_Embedded_Accomodation_01_Multiplication_Table { get; set; }

        [ColumnMapping(189, 189)]
        [ColumnMetaData("Non_Embedded_Accomodation_02_Multiplication_Table", SqlDbType.Char, 6)]
        public string Non_Embedded_Accomodation_02_Multiplication_Table { get; set; }

        [ColumnMapping(190, 190)]
        [ColumnMetaData("Non_Embedded_Accomodation_01_Print_On_Demand", SqlDbType.Char, 25)]
        public string Non_Embedded_Accomodation_01_Print_On_Demand { get; set; }

        [ColumnMapping(191, 191)]
        [ColumnMetaData("Non_Embedded_Accomodation_02_Print_On_Demand", SqlDbType.Char, 25)]
        public string Non_Embedded_Accomodation_02_Print_On_Demand { get; set; }

        [ColumnMapping(192, 192)]
        [ColumnMetaData("Non_Embedded_Accomodation_03_Print_On_Demand", SqlDbType.Char, 25)]
        public string Non_Embedded_Accomodation_03_Print_On_Demand { get; set; }

        [ColumnMapping(193, 193)]
        [ColumnMetaData("Non_Embedded_Accomodation_04_Print_On_Demand", SqlDbType.Char, 25)]
        public string Non_Embedded_Accomodation_04_Print_On_Demand { get; set; }

        [ColumnMapping(194, 194)]
        [ColumnMetaData("Non_Embedded_Accomodation_01_Read_Aloud_Messages", SqlDbType.Char, 14)]
        public string Non_Embedded_Accomodation_01_Read_Aloud_Messages { get; set; }

        [ColumnMapping(195, 195)]
        [ColumnMetaData("Non_Embedded_Accomodation_01_Scribe", SqlDbType.Char, 16)]
        public string Non_Embedded_Accomodation_01_Scribe { get; set; }

        [ColumnMapping(196, 196)]
        [ColumnMetaData("Non_Embedded_Accomodation_02_Scribe", SqlDbType.Char, 16)]
        public string Non_Embedded_Accomodation_02_Scribe { get; set; }

        [ColumnMapping(197, 197)]
        [ColumnMetaData("Non_Embedded_Accomodation_01_Speech_Text", SqlDbType.Char, 7)]
        public string Non_Embedded_Accomodation_01_Speech_Text { get; set; }

        [ColumnMapping(198, 198)]
        [ColumnMetaData("Non_Embedded_Accomodation_02_Speech_Text", SqlDbType.Char, 7)]
        public string Non_Embedded_Accomodation_02_Speech_Text { get; set; }

        [ColumnMapping(199, 199)]
        [ColumnMetaData("Non_Embedded_Accomodation_01_Unlisted_Resources", SqlDbType.Char, 18)]
        public string Non_Embedded_Accomodation_01_Unlisted_Resources { get; set; }

        [ColumnMapping(200, 200)]
        [ColumnMetaData("Non_Embedded_Accomodation_02_Unlisted_Resources", SqlDbType.Char, 18)]
        public string Non_Embedded_Accomodation_02_Unlisted_Resources { get; set; }

        [ColumnMapping(201, 201)]
        [ColumnMetaData("Non_Embedded_Accomodation_03_Unlisted_Resources", SqlDbType.Char, 18)]
        public string Non_Embedded_Accomodation_03_Unlisted_Resources { get; set; }

        [ColumnMapping(202, 202)]
        [ColumnMetaData("Non_Embedded_Accomodation_04_Unlisted_Resources", SqlDbType.Char, 18)]
        public string Non_Embedded_Accomodation_04_Unlisted_Resources { get; set; }

        [ColumnMapping(203, 203)]
        [ColumnMetaData("Non_Embedded_Accomodation_01_Word_Prediction", SqlDbType.Char, 12)]
        public string Non_Embedded_Accomodation_01_Word_Prediction { get; set; }

        [ColumnMapping(204, 204)]
        [ColumnMetaData("Non_Embedded_Accomodation_02_Word_Prediction", SqlDbType.Char, 12)]
        public string Non_Embedded_Accomodation_02_Word_Prediction { get; set; }

        [ColumnMapping(205, 205)]
        [ColumnMetaData("Non_Embedded_Accomodation_03_Word_Prediction", SqlDbType.Char, 12)]
        public string Non_Embedded_Accomodation_03_Word_Prediction { get; set; }

        [ColumnMapping(206, 206)]
        [ColumnMetaData("Non_Embedded_Accomodation_04_Word_Prediction", SqlDbType.Char, 12)]
        public string Non_Embedded_Accomodation_04_Word_Prediction { get; set; }

        [ColumnMapping(207, 207)]
        [ColumnMetaData("Embedded_Designated_Support_01_Color_Contrast", SqlDbType.Char, 19)]
        public string Embedded_Designated_Support_01_Color_Contrast { get; set; }

        [ColumnMapping(208, 208)]
        [ColumnMetaData("Embedded_Designated_Support_02_Color_Contrast", SqlDbType.Char, 19)]
        public string Embedded_Designated_Support_02_Color_Contrast { get; set; }

        [ColumnMapping(209, 209)]
        [ColumnMetaData("Embedded_Designated_Support_03_Color_Contrast", SqlDbType.Char, 19)]
        public string Embedded_Designated_Support_03_Color_Contrast { get; set; }

        [ColumnMapping(210, 210)]
        [ColumnMetaData("Embedded_Designated_Support_04_Color_Contrast", SqlDbType.Char, 19)]
        public string Embedded_Designated_Support_04_Color_Contrast { get; set; }

        [ColumnMapping(211, 211)]
        [ColumnMetaData("Embedded_Designated_Support_01_Masking", SqlDbType.Char, 12)]
        public string Embedded_Designated_Support_01_Masking { get; set; }

        [ColumnMapping(212, 212)]
        [ColumnMetaData("Embedded_Designated_Support_02_Masking", SqlDbType.Char, 12)]
        public string Embedded_Designated_Support_02_Masking { get; set; }

        [ColumnMapping(213, 213)]
        [ColumnMetaData("Embedded_Designated_Support_03_Masking", SqlDbType.Char, 12)]
        public string Embedded_Designated_Support_03_Masking { get; set; }

        [ColumnMapping(214, 214)]
        [ColumnMetaData("Embedded_Designated_Support_04_Masking", SqlDbType.Char, 12)]
        public string Embedded_Designated_Support_04_Masking { get; set; }

        [ColumnMapping(215, 215)]
        [ColumnMetaData("Embedded_Designated_Support_01_Mouse_Pointer", SqlDbType.Char, 16)]
        public string Embedded_Designated_Support_01_Mouse_Pointer { get; set; }

        [ColumnMapping(216, 216)]
        [ColumnMetaData("Embedded_Designated_Support_02_Mouse_Pointer", SqlDbType.Char, 16)]
        public string Embedded_Designated_Support_02_Mouse_Pointer { get; set; }

        [ColumnMapping(217, 217)]
        [ColumnMetaData("Embedded_Designated_Support_03_Mouse_Pointer", SqlDbType.Char, 16)]
        public string Embedded_Designated_Support_03_Mouse_Pointer { get; set; }

        [ColumnMapping(218, 218)]
        [ColumnMetaData("Embedded_Designated_Support_04_Mouse_Pointer", SqlDbType.Char, 16)]
        public string Embedded_Designated_Support_04_Mouse_Pointer { get; set; }

        [ColumnMapping(219, 219)]
        [ColumnMetaData("Embedded_Designated_Support_01_Permissive_Mode", SqlDbType.Char, 7)]
        public string Embedded_Designated_Support_01_Permissive_Mode { get; set; }

        [ColumnMapping(220, 220)]
        [ColumnMetaData("Embedded_Designated_Support_02_Permissive_Mode", SqlDbType.Char, 7)]
        public string Embedded_Designated_Support_02_Permissive_Mode { get; set; }

        [ColumnMapping(221, 221)]
        [ColumnMetaData("Embedded_Designated_Support_03_Permissive_Mode", SqlDbType.Char, 7)]
        public string Embedded_Designated_Support_03_Permissive_Mode { get; set; }

        [ColumnMapping(222, 222)]
        [ColumnMetaData("Embedded_Designated_Support_04_Permissive_Mode", SqlDbType.Char, 7)]
        public string Embedded_Designated_Support_04_Permissive_Mode { get; set; }

        [ColumnMapping(223, 223)]
        [ColumnMetaData("Embedded_Designated_Support_01_Print_Size", SqlDbType.Char, 9)]
        public string Embedded_Designated_Support_01_Print_Size { get; set; }

        [ColumnMapping(224, 224)]
        [ColumnMetaData("Embedded_Designated_Support_02_Print_Size", SqlDbType.Char, 9)]
        public string Embedded_Designated_Support_02_Print_Size { get; set; }

        [ColumnMapping(225, 225)]
        [ColumnMetaData("Embedded_Designated_Support_03_Print_Size", SqlDbType.Char, 9)]
        public string Embedded_Designated_Support_03_Print_Size { get; set; }

        [ColumnMapping(226, 226)]
        [ColumnMetaData("Embedded_Designated_Support_04_Print_Size", SqlDbType.Char, 9)]
        public string Embedded_Designated_Support_04_Print_Size { get; set; }

        [ColumnMapping(227, 227)]
        [ColumnMetaData("Embedded_Designated_Support_01_Stacked_Translations", SqlDbType.Char, 3)]
        public string Embedded_Designated_Support_01_Stacked_Translations { get; set; }

        [ColumnMapping(228, 228)]
        [ColumnMetaData("Embedded_Designated_Support_02_Stacked_Translations", SqlDbType.Char, 3)]
        public string Embedded_Designated_Support_02_Stacked_Translations { get; set; }

        [ColumnMapping(229, 229)]
        [ColumnMetaData("Embedded_Designated_Support_01_Streamline", SqlDbType.Char, 8)]
        public string Embedded_Designated_Support_01_Streamline { get; set; }

        [ColumnMapping(230, 230)]
        [ColumnMetaData("Embedded_Designated_Support_02_Streamline", SqlDbType.Char, 8)]
        public string Embedded_Designated_Support_02_Streamline { get; set; }

        [ColumnMapping(231, 231)]
        [ColumnMetaData("Embedded_Designated_Support_03_Streamline", SqlDbType.Char, 8)]
        public string Embedded_Designated_Support_03_Streamline { get; set; }

        [ColumnMapping(232, 232)]
        [ColumnMetaData("Embedded_Designated_Support_04_Streamline", SqlDbType.Char, 8)]
        public string Embedded_Designated_Support_04_Streamline { get; set; }

        [ColumnMapping(233, 233)]
        [ColumnMetaData("Embedded_Designated_Support_01_Text_Speech", SqlDbType.Char, 25)]
        public string Embedded_Designated_Support_01_Text_Speech { get; set; }

        [ColumnMapping(234, 234)]
        [ColumnMetaData("Embedded_Designated_Support_02_Text_Speech", SqlDbType.Char, 25)]
        public string Embedded_Designated_Support_02_Text_Speech { get; set; }

        [ColumnMapping(235, 235)]
        [ColumnMetaData("Embedded_Designated_Support_01_Translations", SqlDbType.Char, 38)]
        public string Embedded_Designated_Support_01_Translations { get; set; }

        [ColumnMapping(236, 236)]
        [ColumnMetaData("Embedded_Designated_Support_02_Translations", SqlDbType.Char, 38)]
        public string Embedded_Designated_Support_02_Translations { get; set; }

        [ColumnMapping(237, 237)]
        [ColumnMetaData("Embedded_Designated_Support_01_Turn_off_Universal_Tools", SqlDbType.Char, 3)]
        public string Embedded_Designated_Support_01_Turn_off_Universal_Tools { get; set; }

        [ColumnMapping(238, 238)]
        [ColumnMetaData("Embedded_Designated_Support_02_Turn_off_Universal_Tools", SqlDbType.Char, 3)]
        public string Embedded_Designated_Support_02_Turn_off_Universal_Tools { get; set; }

        [ColumnMapping(239, 239)]
        [ColumnMetaData("Embedded_Designated_Support_03_Turn_off_Universal_Tools", SqlDbType.Char, 3)]
        public string Embedded_Designated_Support_03_Turn_off_Universal_Tools { get; set; }

        [ColumnMapping(240, 240)]
        [ColumnMetaData("Embedded_Designated_Support_04_Turn_off_Universal_Tools", SqlDbType.Char, 3)]
        public string Embedded_Designated_Support_04_Turn_off_Universal_Tools { get; set; }

        [ColumnMapping(241, 241)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_100_Number_Table", SqlDbType.Char, 11)]
        public string Non_Embedded_Designated_Support_01_100_Number_Table { get; set; }

        [ColumnMapping(242, 242)]
        [ColumnMetaData("Non_Embedded_Designated_Support_02_100_Number_Table", SqlDbType.Char, 11)]
        public string Non_Embedded_Designated_Support_02_100_Number_Table { get; set; }

        [ColumnMapping(243, 243)]
        [ColumnMetaData("Non_Embedded_Designated_Support_03_100_Number_Table", SqlDbType.Char, 11)]
        public string Non_Embedded_Designated_Support_03_100_Number_Table { get; set; }

        [ColumnMapping(244, 244)]
        [ColumnMetaData("Non_Embedded_Designated_Support_04_100_Number_Table", SqlDbType.Char, 11)]
        public string Non_Embedded_Designated_Support_04_100_Number_Table { get; set; }

        [ColumnMapping(245, 245)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Amplification", SqlDbType.Char, 12)]
        public string Non_Embedded_Designated_Support_01_Amplification { get; set; }

        [ColumnMapping(246, 246)]
        [ColumnMetaData("Non_Embedded_Designated_Support_02_Amplification", SqlDbType.Char, 12)]
        public string Non_Embedded_Designated_Support_02_Amplification { get; set; }

        [ColumnMapping(247, 247)]
        [ColumnMetaData("Non_Embedded_Designated_Support_03_Amplification", SqlDbType.Char, 12)]
        public string Non_Embedded_Designated_Support_03_Amplification { get; set; }

        [ColumnMapping(248, 248)]
        [ColumnMetaData("Non_Embedded_Designated_Support_04_Amplification", SqlDbType.Char, 12)]
        public string Non_Embedded_Designated_Support_04_Amplification { get; set; }

        [ColumnMapping(249, 249)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Bilingual_Dictionary", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_01_Bilingual_Dictionary { get; set; }

        [ColumnMapping(250, 250)]
        [ColumnMetaData("Non_Embedded_Designated_Support_02_Bilingual_Dictionary", SqlDbType.Char, 12)]
        public string Non_Embedded_Designated_Support_02_Bilingual_Dictionary { get; set; }

        [ColumnMapping(251, 251)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Calculator", SqlDbType.Char, 9)]
        public string Non_Embedded_Designated_Support_01_Calculator { get; set; }

        [ColumnMapping(252, 252)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Color_Contrast", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_01_Color_Contrast { get; set; }

        [ColumnMapping(253, 253)]
        [ColumnMetaData("Non_Embedded_Designated_Support_02_Color_Contrast", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_02_Color_Contrast { get; set; }

        [ColumnMapping(254, 254)]
        [ColumnMetaData("Non_Embedded_Designated_Support_03_Color_Contrast", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_03_Color_Contrast { get; set; }

        [ColumnMapping(255, 255)]
        [ColumnMetaData("Non_Embedded_Designated_Support_04_Color_Contrast", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_04_Color_Contrast { get; set; }

        [ColumnMapping(256, 256)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Color_Overlay", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_01_Color_Overlay { get; set; }

        [ColumnMapping(257, 257)]
        [ColumnMetaData("Non_Embedded_Designated_Support_02_Color_Overlay", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_02_Color_Overlay { get; set; }

        [ColumnMapping(258, 258)]
        [ColumnMetaData("Non_Embedded_Designated_Support_03_Color_Overlay", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_03_Color_Overlay { get; set; }

        [ColumnMapping(259, 259)]
        [ColumnMetaData("Non_Embedded_Designated_Support_04_Color_Overlay", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_04_Color_Overlay { get; set; }

        [ColumnMapping(260, 260)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Magnification", SqlDbType.Char, 8)]
        public string Non_Embedded_Designated_Support_01_Magnification { get; set; }

        [ColumnMapping(261, 261)]
        [ColumnMetaData("Non_Embedded_Designated_Support_02_Magnification", SqlDbType.Char, 8)]
        public string Non_Embedded_Designated_Support_02_Magnification { get; set; }

        [ColumnMapping(262, 262)]
        [ColumnMetaData("Non_Embedded_Designated_Support_03_Magnification", SqlDbType.Char, 8)]
        public string Non_Embedded_Designated_Support_03_Magnification { get; set; }

        [ColumnMapping(263, 263)]
        [ColumnMetaData("Non_Embedded_Designated_Support_04_Magnification", SqlDbType.Char, 8)]
        public string Non_Embedded_Designated_Support_04_Magnification { get; set; }

        [ColumnMapping(264, 264)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Medical_Supports", SqlDbType.Char, 11)]
        public string Non_Embedded_Designated_Support_01_Medical_Supports { get; set; }

        [ColumnMapping(265, 265)]
        [ColumnMetaData("Non_Embedded_Designated_Support_02_Medical_Supports", SqlDbType.Char, 11)]
        public string Non_Embedded_Designated_Support_02_Medical_Supports { get; set; }

        [ColumnMapping(266, 266)]
        [ColumnMetaData("Non_Embedded_Designated_Support_03_Medical_Supports", SqlDbType.Char, 11)]
        public string Non_Embedded_Designated_Support_03_Medical_Supports { get; set; }

        [ColumnMapping(267, 267)]
        [ColumnMetaData("Non_Embedded_Designated_Support_04_Medical_Supports", SqlDbType.Char, 11)]
        public string Non_Embedded_Designated_Support_04_Medical_Supports { get; set; }

        [ColumnMapping(268, 268)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Multiplication_Table", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_01_Multiplication_Table { get; set; }

        [ColumnMapping(269, 269)]
        [ColumnMetaData("Non_Embedded_Designated_Support_02_Multiplication_Table", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_02_Multiplication_Table { get; set; }

        [ColumnMapping(270, 270)]
        [ColumnMetaData("Non_Embedded_Designated_Support_03_Multiplication_Table", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_03_Multiplication_Table { get; set; }

        [ColumnMapping(271, 271)]
        [ColumnMetaData("Non_Embedded_Designated_Support_04_Multiplication_Table", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_04_Multiplication_Table { get; set; }

        [ColumnMapping(272, 272)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Noise_Buffers", SqlDbType.Char, 13)]
        public string Non_Embedded_Designated_Support_01_Noise_Buffers { get; set; }

        [ColumnMapping(273, 273)]
        [ColumnMetaData("Non_Embedded_Designated_Support_02_Noise_Buffers", SqlDbType.Char, 13)]
        public string Non_Embedded_Designated_Support_02_Noise_Buffers { get; set; }

        [ColumnMapping(274, 274)]
        [ColumnMetaData("Non_Embedded_Designated_Support_03_Noise_Buffers", SqlDbType.Char, 13)]
        public string Non_Embedded_Designated_Support_03_Noise_Buffers { get; set; }

        [ColumnMapping(275, 275)]
        [ColumnMetaData("Non_Embedded_Designated_Support_04_Noise_Buffers", SqlDbType.Char, 13)]
        public string Non_Embedded_Designated_Support_04_Noise_Buffers { get; set; }

        [ColumnMapping(276, 276)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Read_Aloud_Items", SqlDbType.Char, 21)]
        public string Non_Embedded_Designated_Support_01_Read_Aloud_Items { get; set; }

        [ColumnMapping(277, 277)]
        [ColumnMetaData("Non_Embedded_Designated_Support_02_Read_Aloud_Items", SqlDbType.Char, 21)]
        public string Non_Embedded_Designated_Support_02_Read_Aloud_Items { get; set; }

        [ColumnMapping(278, 278)]
        [ColumnMetaData("Non_Embedded_Designated_Support_03_Read_Aloud_Items", SqlDbType.Char, 21)]
        public string Non_Embedded_Designated_Support_03_Read_Aloud_Items { get; set; }

        [ColumnMapping(279, 279)]
        [ColumnMetaData("Non_Embedded_Designated_Support_04_Read_Aloud_Items", SqlDbType.Char, 21)]
        public string Non_Embedded_Designated_Support_04_Read_Aloud_Items { get; set; }

        [ColumnMapping(280, 280)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Read_Aloud_Spanish", SqlDbType.Char, 25)]
        public string Non_Embedded_Designated_Support_01_Read_Aloud_Spanish { get; set; }

        [ColumnMapping(281, 281)]
        [ColumnMetaData("Non_Embedded_Designated_Support_02_Read_Aloud_Spanish", SqlDbType.Char, 25)]
        public string Non_Embedded_Designated_Support_02_Read_Aloud_Spanish { get; set; }

        [ColumnMapping(282, 282)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Science_Charts", SqlDbType.Char, 13)]
        public string Non_Embedded_Designated_Support_01_Science_Charts { get; set; }

        [ColumnMapping(283, 283)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Scribe_Items", SqlDbType.Char, 13)]
        public string Non_Embedded_Designated_Support_01_Scribe_Items { get; set; }

        [ColumnMapping(284, 284)]
        [ColumnMetaData("Non_Embedded_Designated_Support_02_Scribe_Items", SqlDbType.Char, 13)]
        public string Non_Embedded_Designated_Support_02_Scribe_Items { get; set; }

        [ColumnMapping(285, 285)]
        [ColumnMetaData("Non_Embedded_Designated_Support_03_Scribe_Items", SqlDbType.Char, 13)]
        public string Non_Embedded_Designated_Support_03_Scribe_Items { get; set; }

        [ColumnMapping(286, 286)]
        [ColumnMetaData("Non_Embedded_Designated_Support_04_Scribe_Items", SqlDbType.Char, 13)]
        public string Non_Embedded_Designated_Support_04_Scribe_Items { get; set; }

        [ColumnMapping(287, 287)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Separate_Setting", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_01_Separate_Setting { get; set; }

        [ColumnMapping(288, 288)]
        [ColumnMetaData("Non_Embedded_Designated_Support_02_Separate_Setting", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_02_Separate_Setting { get; set; }

        [ColumnMapping(289, 289)]
        [ColumnMetaData("Non_Embedded_Designated_Support_03_Separate_Setting", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_03_Separate_Setting { get; set; }

        [ColumnMapping(290, 290)]
        [ColumnMetaData("Non_Embedded_Designated_Support_04_Separate_Setting", SqlDbType.Char, 7)]
        public string Non_Embedded_Designated_Support_04_Separate_Setting { get; set; }

        [ColumnMapping(291, 291)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Simplified_Test_Directions", SqlDbType.Char, 13)]
        public string Non_Embedded_Designated_Support_01_Simplified_Test_Directions { get; set; }

        [ColumnMapping(292, 292)]
        [ColumnMetaData("Non_Embedded_Designated_Support_02_Simplified_Test_Directions", SqlDbType.Char, 13)]
        public string Non_Embedded_Designated_Support_02_Simplified_Test_Directions { get; set; }

        [ColumnMapping(293, 293)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Translated_Test_Directions", SqlDbType.Char, 14)]
        public string Non_Embedded_Designated_Support_01_Translated_Test_Directions { get; set; }

        [ColumnMapping(294, 294)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Translated_Test_Directions_Online", SqlDbType.Char, 14)]
        public string Non_Embedded_Designated_Support_01_Translated_Test_Directions_Online { get; set; }

        [ColumnMapping(295, 295)]
        [ColumnMetaData("Non_Embedded_Designated_Support_01_Translated_Test_Directions_Paper", SqlDbType.Char, 38)]
        public string Non_Embedded_Designated_Support_01_Translated_Test_Directions_Paper { get; set; }

        [ColumnMapping(296, 296)]
        [ColumnMetaData("SSR_Eligible_Minus_01", SqlDbType.Char, 1)]
        public string SSR_Eligible_Minus_01 { get; set; }

        [ColumnMapping(297, 297)]
        [ColumnMetaData("Grade_Assessed_Current_year_Minus_01", SqlDbType.Char, 2)]
        public string Grade_Assessed_Current_year_Minus_01 { get; set; }

        [ColumnMapping(298, 298)]
        [ColumnMetaData("blank13", SqlDbType.Char, 8)]
        public string blank13 { get; set; }

        [ColumnMapping(299, 299)]
        [ColumnMetaData("Standard_Error_Measurement_01", SqlDbType.Char, 4)]
        public string Standard_Error_Measurement_01 { get; set; }

        [ColumnMapping(300, 300)]
        [ColumnMetaData("Scale_Score_01", SqlDbType.Char, 4)]
        public string Scale_Score_01 { get; set; }

        [ColumnMapping(301, 301)]
        [ColumnMetaData("Achievement_Levels_01", SqlDbType.Char, 1)]
        public string Achievement_Levels_01 { get; set; }

        [ColumnMapping(302, 302)]
        [ColumnMetaData("Condition_Code_01", SqlDbType.Char, 4)]
        public string Condition_Code_01 { get; set; }

        [ColumnMapping(303, 303)]
        [ColumnMetaData("blank14", SqlDbType.Char, 23)]
        public string blank14 { get; set; }

        [ColumnMapping(304, 304)]
        [ColumnMetaData("blank15", SqlDbType.Char, 23)]
        public string blank15 { get; set; }

        [ColumnMapping(305, 305)]
        [ColumnMetaData("blank16", SqlDbType.Char, 23)]
        public string blank16 { get; set; }

        [ColumnMapping(306, 306)]
        [ColumnMetaData("blank17", SqlDbType.Char, 23)]
        public string blank17 { get; set; }

        [ColumnMapping(307, 307)]
        [ColumnMetaData("blank18", SqlDbType.Char, 23)]
        public string blank18 { get; set; }

        [ColumnMapping(308, 308)]
        [ColumnMetaData("blank19", SqlDbType.Char, 23)]
        public string blank19 { get; set; }

        [ColumnMapping(309, 309)]
        [ColumnMetaData("blank20", SqlDbType.Char, 23)]
        public string blank20 { get; set; }

        [ColumnMapping(310, 310)]
        [ColumnMetaData("blank21", SqlDbType.Char, 23)]
        public string blank21 { get; set; }

        [ColumnMapping(311, 311)]
        [ColumnMetaData("blank22", SqlDbType.Char, 23)]
        public string blank22 { get; set; }

        [ColumnMapping(312, 312)]
        [ColumnMetaData("blank23", SqlDbType.Char, 23)]
        public string blank23 { get; set; }

        [ColumnMapping(313, 313)]
        [ColumnMetaData("blank24", SqlDbType.Char, 23)]
        public string blank24 { get; set; }

        [ColumnMapping(314, 314)]
        [ColumnMetaData("SSR_Eligible_Minus_02", SqlDbType.Char, 1)]
        public string SSR_Eligible_Minus_02 { get; set; }

        [ColumnMapping(315, 315)]
        [ColumnMetaData("Grade_Assessed_Current_year_Minus_02", SqlDbType.Char, 2)]
        public string Grade_Assessed_Current_year_Minus_02 { get; set; }

        [ColumnMapping(316, 316)]
        [ColumnMetaData("blank25", SqlDbType.Char, 8)]
        public string blank25 { get; set; }

        [ColumnMapping(317, 317)]
        [ColumnMetaData("Standard_Error_Measurement_02", SqlDbType.Char, 4)]
        public string Standard_Error_Measurement_02 { get; set; }

        [ColumnMapping(318, 318)]
        [ColumnMetaData("Scale_Score_02", SqlDbType.Char, 4)]
        public string Scale_Score_02 { get; set; }

        [ColumnMapping(319, 319)]
        [ColumnMetaData("Achievement_Levels_02", SqlDbType.Char, 1)]
        public string Achievement_Levels_02 { get; set; }

        [ColumnMapping(320, 320)]
        [ColumnMetaData("Condition_Code_02", SqlDbType.Char, 4)]
        public string Condition_Code_02 { get; set; }

        [ColumnMapping(321, 321)]
        [ColumnMetaData("blank26", SqlDbType.Char, 23)]
        public string blank26 { get; set; }

        [ColumnMapping(322, 322)]
        [ColumnMetaData("blank27", SqlDbType.Char, 23)]
        public string blank27 { get; set; }

        [ColumnMapping(323, 323)]
        [ColumnMetaData("blank28", SqlDbType.Char, 23)]
        public string blank28 { get; set; }

        [ColumnMapping(324, 324)]
        [ColumnMetaData("blank29", SqlDbType.Char, 23)]
        public string blank29 { get; set; }

        [ColumnMapping(325, 325)]
        [ColumnMetaData("blank30", SqlDbType.Char, 23)]
        public string blank30 { get; set; }

        [ColumnMapping(326, 326)]
        [ColumnMetaData("blank31", SqlDbType.Char, 23)]
        public string blank31 { get; set; }

        [ColumnMapping(327, 327)]
        [ColumnMetaData("blank32", SqlDbType.Char, 23)]
        public string blank32 { get; set; }

        [ColumnMapping(328, 328)]
        [ColumnMetaData("blank33", SqlDbType.Char, 23)]
        public string blank33 { get; set; }

        [ColumnMapping(329, 329)]
        [ColumnMetaData("blank34", SqlDbType.Char, 23)]
        public string blank34 { get; set; }

        [ColumnMapping(330, 330)]
        [ColumnMetaData("blank35", SqlDbType.Char, 23)]
        public string blank35 { get; set; }

        [ColumnMapping(331, 331)]
        [ColumnMetaData("blank36", SqlDbType.Char, 23)]
        public string blank36 { get; set; }

        [ColumnMapping(332, 332)]
        [ColumnMetaData("SSR_Eligible_Minus_03", SqlDbType.Char, 1)]
        public string SSR_Eligible_Minus_03 { get; set; }

        [ColumnMapping(333, 333)]
        [ColumnMetaData("Grade_Assessed_Current_year_Minus_03", SqlDbType.Char, 2)]
        public string Grade_Assessed_Current_year_Minus_03 { get; set; }

        [ColumnMapping(334, 334)]
        [ColumnMetaData("blank37", SqlDbType.Char, 8)]
        public string blank37 { get; set; }

        [ColumnMapping(335, 335)]
        [ColumnMetaData("Standard_Error_Measurement_03", SqlDbType.Char, 4)]
        public string Standard_Error_Measurement_03 { get; set; }

        [ColumnMapping(336, 336)]
        [ColumnMetaData("Scale_Score_03", SqlDbType.Char, 4)]
        public string Scale_Score_03 { get; set; }

        [ColumnMapping(337, 337)]
        [ColumnMetaData("Achievement_Levels_03", SqlDbType.Char, 1)]
        public string Achievement_Levels_03 { get; set; }

        [ColumnMapping(338, 338)]
        [ColumnMetaData("Condition_Code_03", SqlDbType.Char, 4)]
        public string Condition_Code_03 { get; set; }

        [ColumnMapping(339, 339)]
        [ColumnMetaData("blank38", SqlDbType.Char, 23)]
        public string blank38 { get; set; }

        [ColumnMapping(340, 340)]
        [ColumnMetaData("blank39", SqlDbType.Char, 23)]
        public string blank39 { get; set; }

        [ColumnMapping(341, 341)]
        [ColumnMetaData("blank40", SqlDbType.Char, 23)]
        public string blank40 { get; set; }

        [ColumnMapping(342, 342)]
        [ColumnMetaData("blank41", SqlDbType.Char, 23)]
        public string blank41 { get; set; }

        [ColumnMapping(343, 343)]
        [ColumnMetaData("blank42", SqlDbType.Char, 23)]
        public string blank42 { get; set; }

        [ColumnMapping(344, 344)]
        [ColumnMetaData("blank43", SqlDbType.Char, 23)]
        public string blank43 { get; set; }

        [ColumnMapping(345, 345)]
        [ColumnMetaData("blank44", SqlDbType.Char, 23)]
        public string blank44 { get; set; }

        [ColumnMapping(346, 346)]
        [ColumnMetaData("blank45", SqlDbType.Char, 23)]
        public string blank45 { get; set; }

        [ColumnMapping(347, 347)]
        [ColumnMetaData("blank46", SqlDbType.Char, 23)]
        public string blank46 { get; set; }

        [ColumnMapping(348, 348)]
        [ColumnMetaData("UIN", SqlDbType.Char, 16)]
        public string UIN { get; set; }

        [ColumnMapping(349, 349)]
        [ColumnMetaData("blank47", SqlDbType.Char, 60)]
        public string blank47 { get; set; }

        [ColumnMapping(350, 350)]
        [ColumnMetaData("End_of_Record", SqlDbType.Char, 2)]
        public string End_of_Record { get; set; }
    }
}
