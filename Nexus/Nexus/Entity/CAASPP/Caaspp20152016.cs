﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.CAASPP
{
    class Caaspp20152016
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("record_type", SqlDbType.Char, 2)]
        public string record_type { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("unique_identification_number", SqlDbType.Char, 16)]
        public string unique_identification_number { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("student_state_id", SqlDbType.Char, 10)]
        public string student_state_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("name_last", SqlDbType.VarChar, 50)]
        public string name_last { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("name_first", SqlDbType.VarChar, 30)]
        public string name_first { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("name_middle", SqlDbType.VarChar, 30)]
        public string name_middle { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("student_local_id", SqlDbType.VarChar, 30)]
        public string student_local_id { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("student_smart_id", SqlDbType.VarChar, 40)]
        public string student_smart_id { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("birthdate", SqlDbType.Date, 10)]
        public DateTime? birthdate { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("gender", SqlDbType.VarChar, 6)]
        public string gender { get; set; }
        [ColumnMapping(10, 10)]
        [ColumnMetaData("grade_level_code", SqlDbType.Char, 2)]
        public string grade_level_code { get; set; }
        [ColumnMapping(11, 11)]
        [ColumnMetaData("district_code", SqlDbType.Char, 14)]
        public string district_code { get; set; }
        [ColumnMapping(12, 12)]
        [ColumnMetaData("school_code", SqlDbType.Char, 14)]
        public string school_code { get; set; }
        [ColumnMapping(13, 13)]
        [ColumnMetaData("blank01", SqlDbType.VarChar, 7)]
        public string blank01 { get; set; }
        [ColumnMapping(14, 14)]
        [ColumnMetaData("section_504_status", SqlDbType.VarChar, 3)]
        public string section_504_status { get; set; }
        [ColumnMapping(15, 15)]
        [ColumnMetaData("idea_indicator", SqlDbType.VarChar, 3)]
        public string idea_indicator { get; set; }
        [ColumnMapping(16, 16)]
        [ColumnMetaData("english_language_proficiency_level", SqlDbType.VarChar, 18)]
        public string english_language_proficiency_level { get; set; }
        [ColumnMapping(17, 17)]
        [ColumnMetaData("migrant_status", SqlDbType.VarChar, 3)]
        public string migrant_status { get; set; }
        [ColumnMapping(18, 18)]
        [ColumnMetaData("lep_status", SqlDbType.VarChar, 3)]
        public string lep_status { get; set; }
        [ColumnMapping(19, 19)]
        [ColumnMetaData("lep_entry_date", SqlDbType.Date, 10)]
        public DateTime? lep_entry_date { get; set; }
        [ColumnMapping(20, 20)]
        [ColumnMetaData("lep_exit_date", SqlDbType.Date, 10)]
        public DateTime? lep_exit_date { get; set; }
        [ColumnMapping(21, 21)]
        [ColumnMetaData("first_entry_us_school_date", SqlDbType.Date, 10)]
        public DateTime? first_entry_us_school_date { get; set; }
        [ColumnMapping(22, 22)]
        [ColumnMetaData("english_language_acquisition_status", SqlDbType.VarChar, 4)]
        public string english_language_acquisition_status { get; set; }
        [ColumnMapping(23, 23)]
        [ColumnMetaData("language_code", SqlDbType.VarChar, 3)]
        public string language_code { get; set; }
        [ColumnMapping(24, 24)]
        [ColumnMetaData("economic_disadvantage_status", SqlDbType.VarChar, 3)]
        public string economic_disadvantage_status { get; set; }
        [ColumnMapping(25, 25)]
        [ColumnMetaData("primary_disability_type", SqlDbType.VarChar, 3)]
        public string primary_disability_type { get; set; }
        [ColumnMapping(26, 26)]
        [ColumnMetaData("nps_school_flag", SqlDbType.Char, 1)]
        public string nps_school_flag { get; set; }
        [ColumnMapping(27, 27)]
        [ColumnMetaData("hispanic", SqlDbType.VarChar, 3)]
        public string hispanic { get; set; }
        [ColumnMapping(28, 28)]
        [ColumnMetaData("american_indian_alaska_native", SqlDbType.VarChar, 3)]
        public string american_indian_alaska_native { get; set; }
        [ColumnMapping(29, 29)]
        [ColumnMetaData("asian", SqlDbType.VarChar, 3)]
        public string asian { get; set; }
        [ColumnMapping(30, 30)]
        [ColumnMetaData("native_hawaiian_pacific_islander", SqlDbType.VarChar, 3)]
        public string native_hawaiian_pacific_islander { get; set; }
        [ColumnMapping(31, 31)]
        [ColumnMetaData("filipino", SqlDbType.VarChar, 3)]
        public string filipino { get; set; }
        [ColumnMapping(32, 32)]
        [ColumnMetaData("black", SqlDbType.VarChar, 3)]
        public string black { get; set; }
        [ColumnMapping(33, 33)]
        [ColumnMetaData("white", SqlDbType.VarChar, 3)]
        public string white { get; set; }
        [ColumnMapping(34, 34)]
        [ColumnMetaData("two_races", SqlDbType.VarChar, 3)]
        public string two_races { get; set; }
        [ColumnMapping(35, 35)]
        [ColumnMetaData("reporting_ethnicity", SqlDbType.VarChar, 3)]
        public string reporting_ethnicity { get; set; }
        [ColumnMapping(36, 36)]
        [ColumnMetaData("parent_education_level", SqlDbType.Char, 2)]
        public string parent_education_level { get; set; }
        [ColumnMapping(37, 37)]
        [ColumnMetaData("opportunity_id1", SqlDbType.VarChar, 16)]
        public string opportunity_id1 { get; set; }
        [ColumnMapping(38, 38)]
        [ColumnMetaData("opportunity_id2", SqlDbType.VarChar, 16)]
        public string opportunity_id2 { get; set; }
        [ColumnMapping(39, 39)]
        [ColumnMetaData("grade_level_code_assessed", SqlDbType.Char, 2)]
        public string grade_level_code_assessed { get; set; }
        [ColumnMapping(40, 40)]
        [ColumnMetaData("capa_level", SqlDbType.TinyInt, 1)]
        public byte? capa_level { get; set; }
        [ColumnMapping(41, 41)]
        [ColumnMetaData("enrollment_id", SqlDbType.VarChar, 16)]
        public string enrollment_id { get; set; }
        [ColumnMapping(42, 42)]
        [ColumnMetaData("tested_lea_name1", SqlDbType.VarChar, 100)]
        public string tested_lea_name1 { get; set; }
        [ColumnMapping(43, 43)]
        [ColumnMetaData("tested_district_code1", SqlDbType.VarChar, 14)]
        public string tested_district_code1 { get; set; }
        [ColumnMapping(44, 44)]
        [ColumnMetaData("tested_school_name1", SqlDbType.VarChar, 100)]
        public string tested_school_name1 { get; set; }
        [ColumnMapping(45, 45)]
        [ColumnMetaData("tested_school_code1", SqlDbType.VarChar, 14)]
        public string tested_school_code1 { get; set; }
        [ColumnMapping(46, 46)]
        [ColumnMetaData("tested_Charter_school_directly_funded_indicator1", SqlDbType.Char, 2)]
        public string tested_charter_school_directly_funded_indicator1 { get; set; }
        [ColumnMapping(47, 47)]
        [ColumnMetaData("paper_pencil_test_completion_date", SqlDbType.Char, 10)]
        public string paper_pencil_test_completion_date { get; set; }
        [ColumnMapping(48, 48)]
        [ColumnMetaData("scanned_date", SqlDbType.Date, 10)]
        public DateTime? scanned_date { get; set; }
        [ColumnMapping(49, 49)]
        [ColumnMetaData("tested_lea_name2", SqlDbType.VarChar, 100)]
        public string tested_lea_name2 { get; set; }
        [ColumnMapping(50, 50)]
        [ColumnMetaData("tested_district_code2", SqlDbType.VarChar, 14)]
        public string tested_district_code2 { get; set; }
        [ColumnMapping(51, 51)]
        [ColumnMetaData("tested_school_name2", SqlDbType.VarChar, 100)]
        public string tested_school_name2 { get; set; }
        [ColumnMapping(52, 52)]
        [ColumnMetaData("tested_school_code2", SqlDbType.VarChar, 14)]
        public string tested_school_code2 { get; set; }
        [ColumnMapping(53, 53)]
        [ColumnMetaData("tested_Charter_school_directly_funded_indicator2", SqlDbType.Char, 2)]
        public string tested_charter_school_directly_funded_indicator2 { get; set; }
        [ColumnMapping(54, 54)]
        [ColumnMetaData("test_start_time1", SqlDbType.DateTime, 18, true, "yyyy-MM-ddHH:mm:ss")]
        public DateTime? test_start_time1 { get; set; }
        [ColumnMapping(55, 55)]
        [ColumnMetaData("test_completion_time1", SqlDbType.DateTime, 18, true, "yyyy-MM-ddHH:mm:ss")]
        public DateTime? test_completion_time1 { get; set; }
        [ColumnMapping(56, 56)]
        [ColumnMetaData("test_start_time2", SqlDbType.DateTime, 18, true, "yyyy-MM-ddHH:mm:ss")]
        public DateTime? test_start_time2 { get; set; }
        [ColumnMapping(57, 57)]
        [ColumnMetaData("test_completion_time2", SqlDbType.DateTime, 18, true, "yyyy-MM-ddHH:mm:ss")]
        public DateTime? test_completion_time2 { get; set; }
        [ColumnMapping(58, 58)]
        [ColumnMetaData("school_selected_start_test_window", SqlDbType.Date, 10)]
        public DateTime? school_selected_start_test_window { get; set; }
        [ColumnMapping(59, 59)]
        [ColumnMetaData("school_selected_end_test_window", SqlDbType.Date, 10)]
        public DateTime? school_selected_end_test_window { get; set; }
        [ColumnMapping(60, 60)]
        [ColumnMetaData("student_exit_date", SqlDbType.Date, 10)]
        public DateTime? student_exit_date { get; set; }
        [ColumnMapping(61, 61)]
        [ColumnMetaData("condition_code", SqlDbType.Char, 4)]
        public string condition_code { get; set; }
        [ColumnMapping(62, 62)]
        [ColumnMetaData("attemptedness", SqlDbType.Char, 1)]
        public string attemptedness { get; set; }
        [ColumnMapping(63, 63)]
        [ColumnMetaData("score_status", SqlDbType.Char, 1)]
        public string score_status { get; set; }
        [ColumnMapping(64, 64)]
        [ColumnMetaData("test_mode", SqlDbType.Char, 1)]
        public string test_mode { get; set; }
        [ColumnMapping(65, 65)]
        [ColumnMetaData("include_indicator", SqlDbType.Char, 1)]
        public string include_indicator { get; set; }
        [ColumnMapping(66, 66)]
        [ColumnMetaData("blank02", SqlDbType.VarChar, 8)]
        public string blank02 { get; set; }
        [ColumnMapping(67, 67)]
        [ColumnMetaData("raw_score", SqlDbType.Char, 2)]
        public string raw_score { get; set; }
        [ColumnMapping(68, 68)]
        [ColumnMetaData("smarter_claim_1_level", SqlDbType.TinyInt, 1)]
        public byte? smarter_claim_1_level { get; set; }
        [ColumnMapping(69, 69)]
        [ColumnMetaData("smarter_claim_2_level", SqlDbType.TinyInt, 1)]
        public byte? smarter_claim_2_level { get; set; }
        [ColumnMapping(70, 70)]
        [ColumnMetaData("smarter_claim_3_level", SqlDbType.TinyInt, 1)]
        public byte? smarter_claim_3_level { get; set; }
        [ColumnMapping(71, 71)]
        [ColumnMetaData("smarter_claim_4_level", SqlDbType.TinyInt, 1)]
        public byte? smarter_claim_4_level { get; set; }
        [ColumnMapping(72, 72)]
        [ColumnMetaData("scale_score", SqlDbType.VarChar, 4)]
        public string scale_score { get; set; }
        [ColumnMapping(73, 73)]
        [ColumnMetaData("smarter_standard_error_measurement", SqlDbType.VarChar, 4)]
        public string smarter_standard_error_measurement { get; set; }
        [ColumnMapping(74, 74)]
        [ColumnMetaData("smarter_scale_scores_error_bands_min", SqlDbType.VarChar, 4)]
        public string smarter_scale_scores_error_bands_min { get; set; }
        [ColumnMapping(75, 75)]
        [ColumnMetaData("smarter_scale_scores_error_bands_max", SqlDbType.VarChar, 4)]
        public string smarter_scale_scores_error_bands_max { get; set; }
        [ColumnMapping(76, 76)]
        [ColumnMetaData("performance_level", SqlDbType.TinyInt, 1)]
        public byte? performance_level { get; set; }
        [ColumnMapping(77, 77)]
        [ColumnMetaData("achievement_level", SqlDbType.TinyInt, 1)]
        public byte? achievement_level { get; set; }
        [ColumnMapping(78, 78)]
        [ColumnMetaData("eap_release", SqlDbType.TinyInt, 1)]
        public byte? eap_release { get; set; }
        [ColumnMapping(79, 79)]
        [ColumnMetaData("accomodation_available_indicator", SqlDbType.VarChar, 3)]
        public string accomodation_available_indicator { get; set; }
        [ColumnMapping(80, 80)]
        [ColumnMetaData("embedded_accommodation1_sign", SqlDbType.VarChar, 8)]
        public string embedded_accommodation1_sign { get; set; }
        [ColumnMapping(81, 81)]
        [ColumnMetaData("embedded_accommodation1_language", SqlDbType.VarChar, 11)]
        public string embedded_accommodation1_language { get; set; }
        [ColumnMapping(82, 82)]
        [ColumnMetaData("embedded_accommodation1_captioning", SqlDbType.VarChar, 14)]
        public string embedded_accommodation1_captioning { get; set; }
        [ColumnMapping(83, 83)]
        [ColumnMetaData("embedded_accommodation1_steamlined", SqlDbType.VarChar, 8)]
        public string embedded_accommodation1_steamlined { get; set; }
        [ColumnMapping(84, 84)]
        [ColumnMetaData("embedded_accommodation1_speech", SqlDbType.VarChar, 25)]
        public string embedded_accommodation1_speech { get; set; }
        [ColumnMapping(85, 85)]
        [ColumnMetaData("embedded_designated1_color", SqlDbType.VarChar, 19)]
        public string embedded_designated1_color { get; set; }
        [ColumnMapping(86, 86)]
        [ColumnMetaData("embedded_designated1_masking", SqlDbType.VarChar, 12)]
        public string embedded_designated1_masking { get; set; }
        [ColumnMapping(87, 87)]
        [ColumnMetaData("embedded_designated1_translation", SqlDbType.VarChar, 38)]
        public string embedded_designated1_translation { get; set; }
        [ColumnMapping(88, 88)]
        [ColumnMetaData("embedded_designated1_size", SqlDbType.VarChar, 9)]
        public string embedded_designated1_size { get; set; }
        [ColumnMapping(89, 89)]
        [ColumnMetaData("nonembedded_accommodation1_print", SqlDbType.VarChar, 25)]
        public string nonembedded_accommodation1_print { get; set; }
        [ColumnMapping(90, 90)]
        [ColumnMetaData("embedded_designated1_permissive", SqlDbType.VarChar, 7)]
        public string embedded_designated1_permissive { get; set; }
        [ColumnMapping(91, 91)]
        [ColumnMetaData("embedded_designated1_universal", SqlDbType.VarChar, 3)]
        public string embedded_designated1_universal { get; set; }
        [ColumnMapping(92, 92)]
        [ColumnMetaData("nonembedded_accomodation1_alternate", SqlDbType.VarChar, 6)]
        public string nonembedded_accomodation1_alternate { get; set; }
        [ColumnMapping(93, 93)]
        [ColumnMetaData("nonembedded_accomodation1_aloud", SqlDbType.VarChar, 14)]
        public string nonembedded_accomodation1_aloud { get; set; }
        [ColumnMapping(94, 94)]
        [ColumnMetaData("nonembedded_accomodation1_unlisted", SqlDbType.VarChar, 18)]
        public string nonembedded_accomodation1_unlisted { get; set; }
        [ColumnMapping(95, 95)]
        [ColumnMetaData("nonembedded_accomodation1_scribe", SqlDbType.VarChar, 16)]
        public string nonembedded_accomodation1_scribe { get; set; }
        [ColumnMapping(96, 96)]
        [ColumnMetaData("nonembedded_accomodation1_speech", SqlDbType.VarChar, 7)]
        public string nonembedded_accomodation1_speech { get; set; }
        [ColumnMapping(97, 97)]
        [ColumnMetaData("nonembedded_accomodation1_alternate_additional", SqlDbType.VarChar, 13)]
        public string nonembedded_accomodation1_alternate_additional { get; set; }
        [ColumnMapping(98, 98)]
        [ColumnMetaData("nonembedded_accomodation1_abacus", SqlDbType.VarChar, 10)]
        public string nonembedded_accomodation1_abacus { get; set; }
        [ColumnMapping(99, 99)]
        [ColumnMetaData("nonembedded_accomodation1_calculator", SqlDbType.VarChar, 8)]
        public string nonembedded_accomodation1_calculator { get; set; }
        [ColumnMapping(100, 100)]
        [ColumnMetaData("nonembedded_accomodation1_multiplication", SqlDbType.VarChar, 6)]
        public string nonembedded_accomodation1_multiplication { get; set; }
        [ColumnMapping(101, 101)]
        [ColumnMetaData("nonembedded_designated1_bilingual", SqlDbType.VarChar, 7)]
        public string nonembedded_designated1_bilingual { get; set; }
        [ColumnMapping(102, 102)]
        [ColumnMetaData("nonembedded_designated1_color_contrast", SqlDbType.VarChar, 7)]
        public string nonembedded_designated1_color_contrast { get; set; }
        [ColumnMapping(103, 103)]
        [ColumnMetaData("nonembedded_designated1_color_overlay", SqlDbType.VarChar, 7)]
        public string nonembedded_designated1_color_overlay { get; set; }
        [ColumnMapping(104, 104)]
        [ColumnMetaData("nonembedded_designated1_magnification", SqlDbType.VarChar, 8)]
        public string nonembedded_designated1_magnification { get; set; }
        [ColumnMapping(105, 105)]
        [ColumnMetaData("nonembedded_designated1_noise", SqlDbType.VarChar, 12)]
        public string nonembedded_designated1_noise { get; set; }
        [ColumnMapping(106, 106)]
        [ColumnMetaData("nonembedded_designated1_read_english", SqlDbType.VarChar, 21)]
        public string nonembedded_designated1_read_english { get; set; }
        [ColumnMapping(107, 107)]
        [ColumnMetaData("nonembedded_designated1_read_spanish", SqlDbType.VarChar, 25)]
        public string nonembedded_designated1_read_spanish { get; set; }
        [ColumnMapping(108, 108)]
        [ColumnMetaData("nonembedded_designated1_scribe", SqlDbType.VarChar, 16)]
        public string nonembedded_designated1_scribe { get; set; }
        [ColumnMapping(109, 109)]
        [ColumnMetaData("nonembedded_designated1_setting", SqlDbType.VarChar, 7)]
        public string nonembedded_designated1_setting { get; set; }
        [ColumnMapping(110, 110)]
        [ColumnMetaData("nonembedded_designated1_translated", SqlDbType.VarChar, 14)]
        public string nonembedded_designated1_translated { get; set; }
        [ColumnMapping(111, 111)]
        [ColumnMetaData("embedded_accommodation2_sign", SqlDbType.VarChar, 8)]
        public string embedded_accommodation2_sign { get; set; }
        [ColumnMapping(112, 112)]
        [ColumnMetaData("embedded_accommodation2_language", SqlDbType.VarChar, 11)]
        public string embedded_accommodation2_language { get; set; }
        [ColumnMapping(113, 113)]
        [ColumnMetaData("embedded_accommodation2_captioning", SqlDbType.VarChar, 14)]
        public string embedded_accommodation2_captioning { get; set; }
        [ColumnMapping(114, 114)]
        [ColumnMetaData("embedded_accommodation2_steamlined", SqlDbType.VarChar, 8)]
        public string embedded_accommodation2_steamlined { get; set; }
        [ColumnMapping(115, 115)]
        [ColumnMetaData("embedded_accommodation2_speech", SqlDbType.VarChar, 25)]
        public string embedded_accommodation2_speech { get; set; }
        [ColumnMapping(116, 116)]
        [ColumnMetaData("embedded_designated2_color", SqlDbType.VarChar, 19)]
        public string embedded_designated2_color { get; set; }
        [ColumnMapping(117, 117)]
        [ColumnMetaData("embedded_designated2_masking", SqlDbType.VarChar, 12)]
        public string embedded_designated2_masking { get; set; }
        [ColumnMapping(118, 118)]
        [ColumnMetaData("embedded_designated2_translation", SqlDbType.VarChar, 38)]
        public string embedded_designated2_translation { get; set; }
        [ColumnMapping(119, 119)]
        [ColumnMetaData("embedded_designated2_size", SqlDbType.VarChar, 9)]
        public string embedded_designated2_size { get; set; }
        [ColumnMapping(120, 120)]
        [ColumnMetaData("nonembedded_accommodation2_print", SqlDbType.VarChar, 25)]
        public string nonembedded_accommodation2_print { get; set; }
        [ColumnMapping(121, 121)]
        [ColumnMetaData("embedded_designated2_permissive", SqlDbType.VarChar, 7)]
        public string embedded_designated2_permissive { get; set; }
        [ColumnMapping(122, 122)]
        [ColumnMetaData("embedded_designated2_universal", SqlDbType.VarChar, 3)]
        public string embedded_designated2_universal { get; set; }
        [ColumnMapping(123, 123)]
        [ColumnMetaData("nonembedded_accomodation2_alternate", SqlDbType.VarChar, 6)]
        public string nonembedded_accomodation2_alternate { get; set; }
        [ColumnMapping(124, 124)]
        [ColumnMetaData("nonembedded_accomodation2_unlisted", SqlDbType.VarChar, 18)]
        public string nonembedded_accomodation2_unlisted { get; set; }
        [ColumnMapping(125, 125)]
        [ColumnMetaData("nonembedded_accomodation2_scribe", SqlDbType.VarChar, 16)]
        public string nonembedded_accomodation2_scribe { get; set; }
        [ColumnMapping(126, 126)]
        [ColumnMetaData("nonembedded_accomodation2_speech", SqlDbType.VarChar, 7)]
        public string nonembedded_accomodation2_speech { get; set; }
        [ColumnMapping(127, 127)]
        [ColumnMetaData("nonembedded_accomodation2_abacus", SqlDbType.VarChar, 10)]
        public string nonembedded_accomodation2_abacus { get; set; }
        [ColumnMapping(128, 128)]
        [ColumnMetaData("nonembedded_accomodation2_calculator", SqlDbType.VarChar, 8)]
        public string nonembedded_accomodation2_calculator { get; set; }
        [ColumnMapping(129, 129)]
        [ColumnMetaData("nonembedded_accomodation2_multiplication", SqlDbType.VarChar, 6)]
        public string nonembedded_accomodation2_multiplication { get; set; }
        [ColumnMapping(130, 130)]
        [ColumnMetaData("nonembedded_designated2_bilingual", SqlDbType.VarChar, 7)]
        public string nonembedded_designated2_bilingual { get; set; }
        [ColumnMapping(131, 131)]
        [ColumnMetaData("nonembedded_designated2_color_contrast", SqlDbType.VarChar, 7)]
        public string nonembedded_designated2_color_contrast { get; set; }
        [ColumnMapping(132, 132)]
        [ColumnMetaData("nonembedded_designated2_color_overlay", SqlDbType.VarChar, 7)]
        public string nonembedded_designated2_color_overlay { get; set; }
        [ColumnMapping(133, 133)]
        [ColumnMetaData("nonembedded_designated2_magnification", SqlDbType.VarChar, 8)]
        public string nonembedded_designated2_magnification { get; set; }
        [ColumnMapping(134, 134)]
        [ColumnMetaData("nonembedded_designated2_noise", SqlDbType.VarChar, 12)]
        public string nonembedded_designated2_noise { get; set; }
        [ColumnMapping(135, 135)]
        [ColumnMetaData("nonembedded_designated2_read_english", SqlDbType.VarChar, 21)]
        public string nonembedded_designated2_read_english { get; set; }
        [ColumnMapping(136, 136)]
        [ColumnMetaData("nonembedded_designated2_read_spanish", SqlDbType.VarChar, 25)]
        public string nonembedded_designated2_read_spanish { get; set; }
        [ColumnMapping(137, 137)]
        [ColumnMetaData("nonembedded_designated2_scribe", SqlDbType.VarChar, 16)]
        public string nonembedded_designated2_scribe { get; set; }
        [ColumnMapping(138, 138)]
        [ColumnMetaData("nonembedded_designated2_setting", SqlDbType.VarChar, 7)]
        public string nonembedded_designated2_setting { get; set; }
        [ColumnMapping(139, 139)]
        [ColumnMetaData("nonembedded_designated2_translated", SqlDbType.VarChar, 14)]
        public string nonembedded_designated2_translated { get; set; }
        [ColumnMapping(140, 140)]
        [ColumnMetaData("nonembedded_accommodation_abacus", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_abacus { get; set; }
        [ColumnMapping(141, 141)]
        [ColumnMetaData("nonembedded_accommodation_alternate", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_alternate { get; set; }
        [ColumnMapping(142, 142)]
        [ColumnMetaData("nonembedded_accommodation_braille", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_braille { get; set; }
        [ColumnMapping(143, 143)]
        [ColumnMetaData("nonembedded_accommodation_calculator", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_calculator { get; set; }
        [ColumnMapping(144, 144)]
        [ColumnMetaData("nonembedded_accommodation_multiplication", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_multiplication { get; set; }
        [ColumnMapping(145, 145)]
        [ColumnMetaData("nonembedded_supports_aloud", SqlDbType.Char, 1)]
        public string nonembedded_supports_aloud { get; set; }
        [ColumnMapping(146, 146)]
        [ColumnMetaData("nonembedded_supports_scibe", SqlDbType.Char, 1)]
        public string nonembedded_supports_scibe { get; set; }
        [ColumnMapping(147, 147)]
        [ColumnMetaData("nonembedded_supports_speech", SqlDbType.Char, 1)]
        public string nonembedded_supports_speech { get; set; }
        [ColumnMapping(148, 148)]
        [ColumnMetaData("nonembedded_supports_setting", SqlDbType.Char, 1)]
        public string nonembedded_supports_setting { get; set; }
        [ColumnMapping(149, 149)]
        [ColumnMetaData("nonembedded_supports_large", SqlDbType.Char, 1)]
        public string nonembedded_supports_large { get; set; }
        [ColumnMapping(150, 150)]
        [ColumnMetaData("nonembedded_supports_bilingual", SqlDbType.Char, 1)]
        public string nonembedded_supports_bilingual { get; set; }
        [ColumnMapping(151, 151)]
        [ColumnMetaData("nonembedded_supports_translated", SqlDbType.Char, 1)]
        public string nonembedded_supports_translated { get; set; }
        [ColumnMapping(152, 152)]
        [ColumnMetaData("nonembedded_supports_translations", SqlDbType.Char, 1)]
        public string nonembedded_supports_translations { get; set; }
        [ColumnMapping(153, 153)]
        [ColumnMetaData("items_attempted1", SqlDbType.SmallInt, 3)]
        public Int16? items_attempted1 { get; set; }
        [ColumnMapping(154, 154)]
        [ColumnMetaData("items_attempted2", SqlDbType.SmallInt, 3)]
        public Int16? items_attempted2 { get; set; }
        [ColumnMapping(155, 155)]
        [ColumnMetaData("blank03", SqlDbType.VarChar, 8)]
        public string blank03 { get; set; }
        [ColumnMapping(156, 156)]
        [ColumnMetaData("smarter_standard_error_measurement_minus1", SqlDbType.VarChar, 4)]
        public string smarter_standard_error_measurement_minus1 { get; set; }
        [ColumnMapping(157, 157)]
        [ColumnMetaData("scaled_scores_minus1", SqlDbType.VarChar, 4)]
        public string scaled_scores_minus1 { get; set; }
        [ColumnMapping(158, 158)]
        [ColumnMetaData("condition_code_minus1", SqlDbType.VarChar, 4)]
        public string condition_code_minus1 { get; set; }
        [ColumnMapping(159, 159)]
        [ColumnMetaData("blank04", SqlDbType.VarChar, 8)]
        public string blank04 { get; set; }
        [ColumnMapping(160, 160)]
        [ColumnMetaData("smarter_standard_error_measurement_minus2", SqlDbType.VarChar, 4)]
        public string smarter_standard_error_measurement_minus2 { get; set; }
        [ColumnMapping(161, 161)]
        [ColumnMetaData("scaled_scores_minus2", SqlDbType.VarChar, 4)]
        public string scaled_scores_minus2 { get; set; }
        [ColumnMapping(162, 162)]
        [ColumnMetaData("condition_code_minus2", SqlDbType.VarChar, 4)]
        public string condition_code_minus2 { get; set; }
        [ColumnMapping(163, 163)]
        [ColumnMetaData("blank05", SqlDbType.VarChar, 16)]
        public string blank05 { get; set; }
        [ColumnMapping(164, 164)]
        [ColumnMetaData("end_of_record", SqlDbType.Char, 2)]
        public string end_of_record { get; set; }
    }
}
