﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace Nexus.CustomAttribute
{
    class ColumnMetaData : Attribute
    {
        // property
        public string ColumnName { get; private set; }
        public SqlDbType ColumnType { get; private set; }
        public int ColumnLength { get; private set; }
        public byte ColumnPrecision { get; private set; }
        public byte ColumnScale { get; private set; }
        public string ColumnFormat { get; private set; }
        public bool IsNullable { get; private set; }

        // constructors
        // numeric
        public ColumnMetaData(string columnName, SqlDbType sqlDbType, bool isNullable = true, string format = null)
        {
            ColumnName = columnName;
            ColumnType = sqlDbType;
            ColumnFormat = format;
            IsNullable = isNullable;
        }
        // string
        public ColumnMetaData(string columnName, SqlDbType sqlDbType, int maxLength, bool isNullable = true, string format = null)
        {
            ColumnName = columnName;
            ColumnType = sqlDbType;
            ColumnLength = maxLength;
            ColumnFormat = format;
            IsNullable = isNullable;
        }
        // decimal
        public ColumnMetaData(string columnName, SqlDbType sqlDbType, byte columnPrecision, byte columnScale, bool isNullable = true)
        {
            ColumnName = columnName;
            ColumnType = sqlDbType;
            ColumnPrecision = columnPrecision;
            ColumnScale = columnScale;
            IsNullable = isNullable;
        }

        public static IEnumerable<Int32> GetLengthMap(Type type)
        {
            PropertyInfo[] properties = type.GetProperties(
                BindingFlags.Public |
                BindingFlags.Instance |
                BindingFlags.DeclaredOnly
            );

            for (int propertyIndex = 0, propertyLength = properties.Length; propertyIndex < propertyLength; propertyIndex++)
            {
                PropertyInfo property = properties[propertyIndex];
                Type propertyType = property.PropertyType;

                // check for custom attributes
                object[] attributes = property.GetCustomAttributes(false);

                for (int attributeIndex = 0, attributeLength = attributes.Length; attributeIndex < attributeLength; attributeIndex++)
                {
                    object attribute = attributes[attributeIndex];

                    if (attribute.GetType() == typeof(ColumnMetaData))
                    {
                        ColumnMetaData columnMetaData = (ColumnMetaData)attribute;

                        yield return columnMetaData.ColumnLength;
                    }
                }
            }
        }

        public static IEnumerable<SqlMetaData> GetSqlMetaData(Type type)
        {
            PropertyInfo[] properties = type.GetProperties(
                BindingFlags.Public |
                BindingFlags.Instance |
                BindingFlags.DeclaredOnly
            );

            for (int propertyIndex = 0, propertyLength = properties.Length; propertyIndex < propertyLength; propertyIndex++)
            {
                PropertyInfo property = properties[propertyIndex];
                Type propertyType = property.PropertyType;

                // check for custom attributes
                object[] attributes = property.GetCustomAttributes(false);

                for (int attributeIndex = 0, attributeLength = attributes.Length; attributeIndex < attributeLength; attributeIndex++)
                {
                    object attribute = attributes[attributeIndex];

                    if (attribute.GetType() == typeof(ColumnMetaData))
                    {
                        ColumnMetaData columnMetaData = (ColumnMetaData)attribute;

                        if (propertyType == typeof(string))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType, columnMetaData.ColumnLength);
                        }
                        else if (propertyType == typeof(int))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType);
                        }
                        else if (propertyType == typeof(int?))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType);
                        }
                        else if (propertyType == typeof(decimal))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType, columnMetaData.ColumnPrecision, columnMetaData.ColumnScale);
                        }
                        else if (propertyType == typeof(decimal?))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType, columnMetaData.ColumnPrecision, columnMetaData.ColumnScale);
                        }
                        else if (propertyType == typeof(long))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType);
                        }
                        else if (propertyType == typeof(long?))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType);
                        }
                        else if (propertyType == typeof(byte[]))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType, columnMetaData.ColumnLength);
                        }
                        else if (propertyType == typeof(byte?[]))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType, columnMetaData.ColumnLength);
                        }
                        else if (propertyType == typeof(bool))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType);
                        }
                        else if (propertyType == typeof(bool?))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType);
                        }
                        else if (propertyType == typeof(DateTime))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType);
                        }
                        else if (propertyType == typeof(DateTime?))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType);
                        }
                        else if (propertyType == typeof(double))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType, columnMetaData.ColumnPrecision, columnMetaData.ColumnScale);
                        }
                        else if (propertyType == typeof(double?))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType, columnMetaData.ColumnPrecision, columnMetaData.ColumnScale);
                        }
                        else if (propertyType == typeof(float))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType, columnMetaData.ColumnPrecision, columnMetaData.ColumnScale);
                        }
                        else if (propertyType == typeof(float?))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType, columnMetaData.ColumnPrecision, columnMetaData.ColumnScale);
                        }
                        else if (propertyType == typeof(short))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType);
                        }
                        else if (propertyType == typeof(short?))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType);
                        }
                        else if (propertyType == typeof(byte))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType);
                        }
                        else if (propertyType == typeof(byte?))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType);
                        }
                        else if (propertyType == typeof(Guid))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType);
                        }
                        else if (propertyType == typeof(Guid?))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType);
                        }
                        else if (propertyType == typeof(object))
                        {
                            yield return new SqlMetaData(columnMetaData.ColumnName, columnMetaData.ColumnType);
                        }
                        else
                        {
                            throw new ArgumentOutOfRangeException("SqlMetaData");
                        }
                    }
                }
            }
        }

        public static IEnumerable<ColumnMetaData> GetColumnMetaData(Type type)
        {
            PropertyInfo[] properties = type.GetProperties(
                BindingFlags.Public |
                BindingFlags.Instance |
                BindingFlags.DeclaredOnly
            );

            for (int propertyIndex = 0, propertyLength = properties.Length; propertyIndex < propertyLength; propertyIndex++)
            {
                PropertyInfo property = properties[propertyIndex];
                Type propertyType = property.PropertyType;

                // check for custom attributes
                object[] attributes = property.GetCustomAttributes(false);

                for (int attributeIndex = 0, attributeLength = attributes.Length; attributeIndex < attributeLength; attributeIndex++)
                {
                    object attribute = attributes[attributeIndex];

                    if (attribute.GetType() == typeof(ColumnMetaData))
                    {
                        ColumnMetaData columnMetaData = (ColumnMetaData)attribute;

                        yield return columnMetaData;
                    }
                }
            }
        }
    }
}
