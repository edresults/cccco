﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Nexus.Entity
{
    public interface INexus
    {
        string FilePath { get; set; }
        char? Delimiter { get; }
        bool HasFieldsEnclosedInQuotes { get; }
        bool HasHeader { get; }
        bool HasRecordNumber { get; set; }
        bool HasValidation { get; set; }
        string ConnectionString { get; }
        string SchemaName { get; }
        string TableName { get; set; }
        string StageName { get; }
        string FullTableName { get; }
        string FullStageName { get; }
        object Entity { get; set; }
        long RecordCount { get; set; }
        Dictionary<int, Func<string, object>> FunctionMap { get; set; }
        List<SqlCommand> EtlCommand { get; set; }
        SqlCommand ExportCommand { get; set; }
        SqlCommand EvaluateCommand { get; set; }

        void StageTableCreate();

        void StageTableDrop();
    }
}
