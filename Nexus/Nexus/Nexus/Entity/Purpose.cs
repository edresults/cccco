﻿using System;

namespace Nexus.Entity
{
    public class Purpose
    {
        public Int32? FileDropPurposeId { get; set; }
        public string FileDropPurpose { get; set; }
    }
}
