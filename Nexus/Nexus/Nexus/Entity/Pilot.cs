﻿using System;

namespace Nexus.Entity
{
    public class Pilot
    {
        public int PilotCode { get; set; }
        public string LocalFolder { get; set; }
        public string SftpFolder { get; set; }
    }
}
