﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.CAASPP
{
    class Caaspp20162017
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("record_type", SqlDbType.Char, 2)]
        public string record_type { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("unique_identification_number", SqlDbType.Char, 16)]
        public string unique_identification_number { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("student_state_id", SqlDbType.Char, 10)]
        public string student_state_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("name_last", SqlDbType.VarChar, 50)]
        public string name_last { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("name_first", SqlDbType.VarChar, 30)]
        public string name_first { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("name_middle", SqlDbType.VarChar, 30)]
        public string name_middle { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("birthdate", SqlDbType.Date, 10)]
        public DateTime? birthdate { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("gender", SqlDbType.VarChar, 6)]
        public string gender { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("grade_level_code", SqlDbType.Char, 2)]
        public string grade_level_code { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("district_code", SqlDbType.Char, 14)]
        public string district_code { get; set; }
        [ColumnMapping(10, 10)]
        [ColumnMetaData("school_code", SqlDbType.Char, 14)]
        public string school_code { get; set; }
        [ColumnMapping(11, 11)]
        [ColumnMetaData("charter_code", SqlDbType.Char, 4)]
        public string charter_code { get; set; }
        [ColumnMapping(12, 12)]
        [ColumnMetaData("accountability_code", SqlDbType.Char, 7)]
        public string accountability_code { get; set; }
        [ColumnMapping(13, 13)]
        [ColumnMetaData("section_504_status", SqlDbType.VarChar, 3)]
        public string section_504_status { get; set; }
        [ColumnMapping(14, 14)]
        [ColumnMetaData("primary_disability_type", SqlDbType.VarChar, 3)]
        public string primary_disability_type { get; set; }
        [ColumnMapping(15, 15)]
        [ColumnMetaData("idea_indicator", SqlDbType.VarChar, 3)]
        public string idea_indicator { get; set; }
        [ColumnMapping(16, 16)]
        [ColumnMetaData("migrant_status", SqlDbType.VarChar, 3)]
        public string migrant_status { get; set; }
        [ColumnMapping(17, 17)]
        [ColumnMetaData("lep_status", SqlDbType.VarChar, 3)]
        public string lep_status { get; set; }
        [ColumnMapping(18, 18)]
        [ColumnMetaData("lep_entry_date", SqlDbType.Date, 10)]
        public DateTime? lep_entry_date { get; set; }
        [ColumnMapping(19, 19)]
        [ColumnMetaData("rfep_date", SqlDbType.Date, 10)]
        public DateTime? rfep_date { get; set; }
        [ColumnMapping(20, 20)]
        [ColumnMetaData("first_entry_us_school_date", SqlDbType.Date, 10)]
        public DateTime? first_entry_us_school_date { get; set; }
        [ColumnMapping(21, 21)]
        [ColumnMetaData("english_language_acquisition_status", SqlDbType.VarChar, 4)]
        public string english_language_acquisition_status { get; set; }
        [ColumnMapping(22, 22)]
        [ColumnMetaData("language_code", SqlDbType.VarChar, 3)]
        public string language_code { get; set; }
        [ColumnMapping(23, 23)]
        [ColumnMetaData("economic_disadvantage_status", SqlDbType.VarChar, 3)]
        public string economic_disadvantage_status { get; set; }
        [ColumnMapping(24, 24)]
        [ColumnMetaData("nps_school_flag", SqlDbType.Char, 1)]
        public string nps_school_flag { get; set; }
        [ColumnMapping(25, 25)]
        [ColumnMetaData("hispanic", SqlDbType.VarChar, 3)]
        public string hispanic { get; set; }
        [ColumnMapping(26, 26)]
        [ColumnMetaData("american_indian_alaska_native", SqlDbType.VarChar, 3)]
        public string american_indian_alaska_native { get; set; }
        [ColumnMapping(27, 27)]
        [ColumnMetaData("asian", SqlDbType.VarChar, 3)]
        public string asian { get; set; }
        [ColumnMapping(28, 28)]
        [ColumnMetaData("native_hawaiian_pacific_islander", SqlDbType.VarChar, 3)]
        public string native_hawaiian_pacific_islander { get; set; }
        [ColumnMapping(29, 29)]
        [ColumnMetaData("filipino", SqlDbType.VarChar, 3)]
        public string filipino { get; set; }
        [ColumnMapping(30, 30)]
        [ColumnMetaData("black", SqlDbType.VarChar, 3)]
        public string black { get; set; }
        [ColumnMapping(31, 31)]
        [ColumnMetaData("white", SqlDbType.VarChar, 3)]
        public string white { get; set; }
        [ColumnMapping(32, 32)]
        [ColumnMetaData("two_races", SqlDbType.VarChar, 3)]
        public string two_races { get; set; }
        [ColumnMapping(33, 33)]
        [ColumnMetaData("reporting_ethnicity", SqlDbType.VarChar, 3)]
        public string reporting_ethnicity { get; set; }
        [ColumnMapping(34, 34)]
        [ColumnMetaData("parent_education_level", SqlDbType.Char, 2)]
        public string parent_education_level { get; set; }
        [ColumnMapping(35, 35)]
        [ColumnMetaData("blank00", SqlDbType.Char, 1)]
        public string blank00 { get; set; }
        [ColumnMapping(36, 36)]
        [ColumnMetaData("opportunity_id1", SqlDbType.VarChar, 16)]
        public string opportunity_id1 { get; set; }
        [ColumnMapping(37, 37)]
        [ColumnMetaData("opportunity_id1_status", SqlDbType.Char, 1)]
        public string opportunity_id1_status { get; set; }
        [ColumnMapping(38, 38)]
        [ColumnMetaData("opportunity_id2", SqlDbType.VarChar, 16)]
        public string opportunity_id2 { get; set; }
        [ColumnMapping(39, 39)]
        [ColumnMetaData("opportunity_id2_status", SqlDbType.Char, 1)]
        public string opportunity_id2_status { get; set; }
        [ColumnMapping(40, 40)]
        [ColumnMetaData("grade_assessed", SqlDbType.Char, 2)]
        public string grade_assessed { get; set; }
        [ColumnMapping(41, 41)]
        [ColumnMetaData("registration_id", SqlDbType.VarChar, 16)]
        public string registration_id { get; set; }
        [ColumnMapping(42, 42)]
        [ColumnMetaData("tested_lea_name1", SqlDbType.VarChar, 100)]
        public string tested_lea_name1 { get; set; }
        [ColumnMapping(43, 43)]
        [ColumnMetaData("tested_district_code1", SqlDbType.VarChar, 14)]
        public string tested_district_code1 { get; set; }
        [ColumnMapping(44, 44)]
        [ColumnMetaData("tested_school_name1", SqlDbType.VarChar, 100)]
        public string tested_school_name1 { get; set; }
        [ColumnMapping(45, 45)]
        [ColumnMetaData("tested_school_code1", SqlDbType.VarChar, 14)]
        public string tested_school_code1 { get; set; }
        [ColumnMapping(46, 46)]
        [ColumnMetaData("tested_charter_school_directly_funded_indicator1", SqlDbType.Char, 2)]
        public string tested_charter_school_directly_funded_indicator1 { get; set; }
        [ColumnMapping(47, 47)]
        [ColumnMetaData("tested_charter_school_code1", SqlDbType.VarChar, 4)]
        public string tested_charter_school_code1 { get; set; }
        [ColumnMapping(48, 48)]
        [ColumnMetaData("paper_pencil_test_completion_date", SqlDbType.Char, 10)]
        public string paper_pencil_test_completion_date { get; set; }
        [ColumnMapping(49, 49)]
        [ColumnMetaData("scanned_date", SqlDbType.Date, 10)]
        public DateTime? scanned_date { get; set; }
        [ColumnMapping(50, 50)]
        [ColumnMetaData("tested_lea_name2", SqlDbType.VarChar, 100)]
        public string tested_lea_name2 { get; set; }
        [ColumnMapping(51, 51)]
        [ColumnMetaData("tested_district_code2", SqlDbType.VarChar, 14)]
        public string tested_district_code2 { get; set; }
        [ColumnMapping(52, 52)]
        [ColumnMetaData("tested_school_name2", SqlDbType.VarChar, 100)]
        public string tested_school_name2 { get; set; }
        [ColumnMapping(53, 53)]
        [ColumnMetaData("tested_school_code2", SqlDbType.VarChar, 14)]
        public string tested_school_code2 { get; set; }
        [ColumnMapping(54, 54)]
        [ColumnMetaData("tested_charter_school_directly_funded_indicator2", SqlDbType.Char, 2)]
        public string tested_charter_school_directly_funded_indicator2 { get; set; }
        [ColumnMapping(55, 55)]
        [ColumnMetaData("tested_charter_school_code2", SqlDbType.VarChar, 4)]
        public string tested_charter_school_code2 { get; set; }
        [ColumnMapping(56, 56)]
        [ColumnMetaData("test_start_time1", SqlDbType.DateTime, 10)]
        public DateTime? test_start_time1 { get; set; }
        [ColumnMapping(57, 57)]
        [ColumnMetaData("test_completion_time1", SqlDbType.DateTime, 10)]
        public DateTime? test_completion_time1 { get; set; }
        [ColumnMapping(58, 58)]
        [ColumnMetaData("test_start_time2", SqlDbType.DateTime, 10)]
        public DateTime? test_start_time2 { get; set; }
        [ColumnMapping(59, 59)]
        [ColumnMetaData("test_completion_time2", SqlDbType.DateTime, 10)]
        public DateTime? test_completion_time2 { get; set; }
        [ColumnMapping(60, 60)]
        [ColumnMetaData("school_selected_start_test_window1", SqlDbType.Date, 10)]
        public DateTime? school_selected_start_test_window1 { get; set; }
        [ColumnMapping(61, 61)]
        [ColumnMetaData("school_selected_end_test_window1", SqlDbType.Date, 10)]
        public DateTime? school_selected_end_test_window1 { get; set; }
        [ColumnMapping(62, 62)]
        [ColumnMetaData("school_selected_start_test_window2", SqlDbType.Date, 10)]
        public DateTime? school_selected_start_test_window2 { get; set; }
        [ColumnMapping(63, 63)]
        [ColumnMetaData("school_selected_end_test_window2", SqlDbType.Date, 10)]
        public DateTime? school_selected_end_test_window2 { get; set; }
        [ColumnMapping(64, 64)]
        [ColumnMetaData("student_exit_code", SqlDbType.VarChar, 4)]
        public string student_exit_code { get; set; }
        [ColumnMapping(65, 65)]
        [ColumnMetaData("student_exit_date", SqlDbType.Date, 10)]
        public DateTime? student_exit_date { get; set; }
        [ColumnMapping(66, 66)]
        [ColumnMetaData("student_removed_date", SqlDbType.Date, 10)]
        public DateTime? student_removed_date { get; set; }
        [ColumnMapping(67, 67)]
        [ColumnMetaData("condition_code", SqlDbType.Char, 4)]
        public string condition_code { get; set; }
        [ColumnMapping(68, 68)]
        [ColumnMetaData("attemptedness", SqlDbType.Char, 1)]
        public string attemptedness { get; set; }
        [ColumnMapping(69, 69)]
        [ColumnMetaData("score_status", SqlDbType.Char, 1)]
        public string score_status { get; set; }
        [ColumnMapping(70, 70)]
        [ColumnMetaData("unlisted_resourceconstruct_change", SqlDbType.Char, 1)]
        public string unlisted_resourceconstruct_change { get; set; }
        [ColumnMapping(71, 71)]
        [ColumnMetaData("test_mode", SqlDbType.Char, 1)]
        public string test_mode { get; set; }
        [ColumnMapping(72, 72)]
        [ColumnMetaData("include_indicator", SqlDbType.Char, 1)]
        public string include_indicator { get; set; }
        [ColumnMapping(73, 73)]
        [ColumnMetaData("blank01", SqlDbType.VarChar, 8)]
        public string blank01 { get; set; }
        [ColumnMapping(74, 74)]
        [ColumnMetaData("raw_score", SqlDbType.Char, 2)]
        public string raw_score { get; set; }
        [ColumnMapping(75, 75)]
        [ColumnMetaData("blank02", SqlDbType.VarChar, 4)]
        public string blank02 { get; set; }
        [ColumnMapping(76, 76)]
        [ColumnMetaData("smarter_claim_1_level", SqlDbType.TinyInt, 1)]
        public byte? smarter_claim_1_level { get; set; }
        [ColumnMapping(77, 77)]
        [ColumnMetaData("blank03", SqlDbType.VarChar, 4)]
        public string blank03 { get; set; }
        [ColumnMapping(78, 78)]
        [ColumnMetaData("smarter_claim_2_level", SqlDbType.TinyInt, 1)]
        public byte? smarter_claim_2_level { get; set; }
        [ColumnMapping(79, 79)]
        [ColumnMetaData("blank04", SqlDbType.VarChar, 4)]
        public string blank04 { get; set; }
        [ColumnMapping(80, 80)]
        [ColumnMetaData("smarter_claim_3_level", SqlDbType.TinyInt, 1)]
        public byte? smarter_claim_3_level { get; set; }
        [ColumnMapping(81, 81)]
        [ColumnMetaData("blank05", SqlDbType.VarChar, 4)]
        public string blank05 { get; set; }
        [ColumnMapping(82, 82)]
        [ColumnMetaData("smarter_claim_4_level", SqlDbType.TinyInt, 1)]
        public byte? smarter_claim_4_level { get; set; }
        [ColumnMapping(83, 83)]
        [ColumnMetaData("scale_score", SqlDbType.VarChar, 4)]
        public string scale_score { get; set; }
        [ColumnMapping(84, 84)]
        [ColumnMetaData("smarter_standard_error_measurement", SqlDbType.VarChar, 4)]
        public string smarter_standard_error_measurement { get; set; }
        [ColumnMapping(85, 85)]
        [ColumnMetaData("smarter_scale_scores_error_bands_min", SqlDbType.VarChar, 4)]
        public string smarter_scale_scores_error_bands_min { get; set; }
        [ColumnMapping(86, 86)]
        [ColumnMetaData("smarter_scale_scores_error_bands_max", SqlDbType.VarChar, 4)]
        public string smarter_scale_scores_error_bands_max { get; set; }
        [ColumnMapping(87, 87)]
        [ColumnMetaData("performance_level", SqlDbType.TinyInt, 1)]
        public byte? performance_level { get; set; }
        [ColumnMapping(88, 88)]
        [ColumnMetaData("achievement_level", SqlDbType.TinyInt, 1)]
        public byte? achievement_level { get; set; }
        [ColumnMapping(89, 89)]
        [ColumnMetaData("eap_release", SqlDbType.TinyInt, 1)]
        public byte? eap_release { get; set; }
        [ColumnMapping(90, 90)]
        [ColumnMetaData("accomodation_available_indicator", SqlDbType.VarChar, 3)]
        public string accomodation_available_indicator { get; set; }
        [ColumnMapping(91, 91)]
        [ColumnMetaData("embedded_accommodation1_sign", SqlDbType.VarChar, 8)]
        public string embedded_accommodation1_sign { get; set; }
        [ColumnMapping(92, 92)]
        [ColumnMetaData("embedded_accommodation1_braille", SqlDbType.VarChar, 11)]
        public string embedded_accommodation1_braille { get; set; }
        [ColumnMapping(93, 93)]
        [ColumnMetaData("embedded_accommodation1_captioning", SqlDbType.VarChar, 14)]
        public string embedded_accommodation1_captioning { get; set; }
        [ColumnMapping(94, 94)]
        [ColumnMetaData("embedded_accommodation1_steamlined", SqlDbType.VarChar, 8)]
        public string embedded_accommodation1_steamlined { get; set; }
        [ColumnMapping(95, 95)]
        [ColumnMetaData("embedded_accommodation1_speech", SqlDbType.VarChar, 12)]
        public string embedded_accommodation1_speech { get; set; }
        [ColumnMapping(96, 96)]
        [ColumnMetaData("embedded_accommodation2_sign", SqlDbType.VarChar, 8)]
        public string embedded_accommodation2_sign { get; set; }
        [ColumnMapping(97, 97)]
        [ColumnMetaData("embedded_accommodation2_braille", SqlDbType.VarChar, 11)]
        public string embedded_accommodation2_braille { get; set; }
        [ColumnMapping(98, 98)]
        [ColumnMetaData("embedded_accommodation2_steamlined", SqlDbType.VarChar, 8)]
        public string embedded_accommodation2_steamlined { get; set; }
        [ColumnMapping(99, 99)]
        [ColumnMetaData("embedded_accommodation2_speech", SqlDbType.VarChar, 12)]
        public string embedded_accommodation2_speech { get; set; }
        [ColumnMapping(100, 100)]
        [ColumnMetaData("nonembedded_accomodation1_abacus", SqlDbType.VarChar, 10)]
        public string nonembedded_accomodation1_abacus { get; set; }
        [ColumnMapping(101, 101)]
        [ColumnMetaData("nonembedded_accomodation1_alternate_additional", SqlDbType.VarChar, 13)]
        public string nonembedded_accomodation1_alternate_additional { get; set; }
        [ColumnMapping(102, 102)]
        [ColumnMetaData("nonembedded_accomodation1_alternate", SqlDbType.VarChar, 6)]
        public string nonembedded_accomodation1_alternate { get; set; }
        [ColumnMapping(103, 103)]
        [ColumnMetaData("nonembedded_accomodation1_calculator", SqlDbType.VarChar, 8)]
        public string nonembedded_accomodation1_calculator { get; set; }
        [ColumnMapping(104, 104)]
        [ColumnMetaData("nonembedded_accomodation1_math_tools", SqlDbType.VarChar, 13)]
        public string nonembedded_accomodation1_math_tools { get; set; }
        [ColumnMapping(105, 105)]
        [ColumnMetaData("nonembedded_accomodation1_multiplication", SqlDbType.VarChar, 6)]
        public string nonembedded_accomodation1_multiplication { get; set; }
        [ColumnMapping(106, 106)]
        [ColumnMetaData("nonembedded_accommodation1_print", SqlDbType.VarChar, 25)]
        public string nonembedded_accommodation1_print { get; set; }
        [ColumnMapping(107, 107)]
        [ColumnMetaData("nonembedded_accomodation1_aloud", SqlDbType.VarChar, 14)]
        public string nonembedded_accomodation1_aloud { get; set; }
        [ColumnMapping(108, 108)]
        [ColumnMetaData("nonembedded_accomodation1_scribe", SqlDbType.VarChar, 16)]
        public string nonembedded_accomodation1_scribe { get; set; }
        [ColumnMapping(109, 109)]
        [ColumnMetaData("nonembedded_accomodation1_speech", SqlDbType.VarChar, 7)]
        public string nonembedded_accomodation1_speech { get; set; }
        [ColumnMapping(110, 110)]
        [ColumnMetaData("nonembedded_accomodation1_numtbl", SqlDbType.VarChar, 10)]
        public string nonembedded_accomodation1_numtbl { get; set; }
        [ColumnMapping(111, 111)]
        [ColumnMetaData("nonembedded_accomodation1_unlisted", SqlDbType.VarChar, 18)]
        public string nonembedded_accomodation1_unlisted { get; set; }
        [ColumnMapping(112, 112)]
        [ColumnMetaData("nonembedded_accomodation2_abacus", SqlDbType.VarChar, 10)]
        public string nonembedded_accomodation2_abacus { get; set; }
        [ColumnMapping(113, 113)]
        [ColumnMetaData("nonembedded_accomodation2_alternate", SqlDbType.VarChar, 6)]
        public string nonembedded_accomodation2_alternate { get; set; }
        [ColumnMapping(114, 114)]
        [ColumnMetaData("nonembedded_accomodation2_calculator", SqlDbType.VarChar, 8)]
        public string nonembedded_accomodation2_calculator { get; set; }
        [ColumnMapping(115, 115)]
        [ColumnMetaData("nonembedded_accomodation2_multiplication", SqlDbType.VarChar, 6)]
        public string nonembedded_accomodation2_multiplication { get; set; }
        [ColumnMapping(116, 116)]
        [ColumnMetaData("nonembedded_accommodation2_print", SqlDbType.VarChar, 25)]
        public string nonembedded_accommodation2_print { get; set; }
        [ColumnMapping(117, 117)]
        [ColumnMetaData("nonembedded_accomodation2_scribe", SqlDbType.VarChar, 16)]
        public string nonembedded_accomodation2_scribe { get; set; }
        [ColumnMapping(118, 118)]
        [ColumnMetaData("nonembedded_accomodation2_speech", SqlDbType.VarChar, 7)]
        public string nonembedded_accomodation2_speech { get; set; }
        [ColumnMapping(119, 119)]
        [ColumnMetaData("nonembedded_accomodation2_unlisted", SqlDbType.VarChar, 18)]
        public string nonembedded_accomodation2_unlisted { get; set; }
        [ColumnMapping(120, 120)]
        [ColumnMetaData("nonembedded_accomodation2_numtbl", SqlDbType.VarChar, 10)]
        public string nonembedded_accomodation2_numtbl { get; set; }
        [ColumnMapping(121, 121)]
        [ColumnMetaData("embedded_designated1_color", SqlDbType.VarChar, 19)]
        public string embedded_designated1_color { get; set; }
        [ColumnMapping(122, 122)]
        [ColumnMetaData("embedded_designated1_masking", SqlDbType.VarChar, 12)]
        public string embedded_designated1_masking { get; set; }
        [ColumnMapping(123, 123)]
        [ColumnMetaData("embedded_designated1_permissive", SqlDbType.VarChar, 7)]
        public string embedded_designated1_permissive { get; set; }
        [ColumnMapping(124, 124)]
        [ColumnMetaData("embedded_designated1_size", SqlDbType.VarChar, 9)]
        public string embedded_designated1_size { get; set; }
        [ColumnMapping(125, 125)]
        [ColumnMetaData("embedded_designated1_translate_directions", SqlDbType.VarChar, 3)]
        public string embedded_designated1_translate_directions { get; set; }
        [ColumnMapping(126, 126)]
        [ColumnMetaData("embedded_designated1_speech", SqlDbType.VarChar, 25)]
        public string embedded_designated1_speech { get; set; }
        [ColumnMapping(127, 127)]
        [ColumnMetaData("embedded_designated1_translations", SqlDbType.VarChar, 38)]
        public string embedded_designated1_translations { get; set; }
        [ColumnMapping(128, 128)]
        [ColumnMetaData("embedded_designated1_universal", SqlDbType.VarChar, 3)]
        public string embedded_designated1_universal { get; set; }
        [ColumnMapping(129, 129)]
        [ColumnMetaData("embedded_designated2_color", SqlDbType.VarChar, 19)]
        public string embedded_designated2_color { get; set; }
        [ColumnMapping(130, 130)]
        [ColumnMetaData("embedded_designated2_masking", SqlDbType.VarChar, 12)]
        public string embedded_designated2_masking { get; set; }
        [ColumnMapping(131, 131)]
        [ColumnMetaData("embedded_designated2_permissive", SqlDbType.VarChar, 7)]
        public string embedded_designated2_permissive { get; set; }
        [ColumnMapping(132, 132)]
        [ColumnMetaData("embedded_designated2_size", SqlDbType.VarChar, 9)]
        public string embedded_designated2_size { get; set; }
        [ColumnMapping(133, 133)]
        [ColumnMetaData("embedded_designated2_translate_directions", SqlDbType.VarChar, 3)]
        public string embedded_designated2_translate_directions { get; set; }
        [ColumnMapping(134, 134)]
        [ColumnMetaData("embedded_designated2_speech", SqlDbType.VarChar, 25)]
        public string embedded_designated2_speech { get; set; }
        [ColumnMapping(135, 135)]
        [ColumnMetaData("embedded_designated2_translations", SqlDbType.VarChar, 38)]
        public string embedded_designated2_translations { get; set; }
        [ColumnMapping(136, 136)]
        [ColumnMetaData("embedded_designated2_universal", SqlDbType.VarChar, 3)]
        public string embedded_designated2_universal { get; set; }
        [ColumnMapping(137, 137)]
        [ColumnMetaData("nonembedded_designated1_color_contrast", SqlDbType.VarChar, 7)]
        public string nonembedded_designated1_color_contrast { get; set; }
        [ColumnMapping(138, 138)]
        [ColumnMetaData("nonembedded_designated1_color_overlay", SqlDbType.VarChar, 7)]
        public string nonembedded_designated1_color_overlay { get; set; }
        [ColumnMapping(139, 139)]
        [ColumnMetaData("nonembedded_designated1_magnification", SqlDbType.VarChar, 8)]
        public string nonembedded_designated1_magnification { get; set; }
        [ColumnMapping(140, 140)]
        [ColumnMetaData("nonembedded_designated1_noise", SqlDbType.VarChar, 13)]
        public string nonembedded_designated1_noise { get; set; }
        [ColumnMapping(141, 141)]
        [ColumnMetaData("nonembedded_designated1_read_english", SqlDbType.VarChar, 21)]
        public string nonembedded_designated1_read_english { get; set; }
        [ColumnMapping(142, 142)]
        [ColumnMetaData("nonembedded_designated1_read_spanish", SqlDbType.VarChar, 25)]
        public string nonembedded_designated1_read_spanish { get; set; }
        [ColumnMapping(143, 143)]
        [ColumnMetaData("nonembedded_designated1_scribe", SqlDbType.VarChar, 16)]
        public string nonembedded_designated1_scribe { get; set; }
        [ColumnMapping(144, 144)]
        [ColumnMetaData("nonembedded_designated1_setting", SqlDbType.VarChar, 7)]
        public string nonembedded_designated1_setting { get; set; }
        [ColumnMapping(145, 145)]
        [ColumnMetaData("nonembedded_designated1_simplified_directions", SqlDbType.VarChar, 13)]
        public string nonembedded_designated1_simplified_directions { get; set; }
        [ColumnMapping(146, 146)]
        [ColumnMetaData("nonembedded_designated1_translated_directions", SqlDbType.VarChar, 14)]
        public string nonembedded_designated1_translated_directions { get; set; }
        [ColumnMapping(147, 147)]
        [ColumnMetaData("nonembedded_designated2_bilingual", SqlDbType.VarChar, 7)]
        public string nonembedded_designated2_bilingual { get; set; }
        [ColumnMapping(148, 148)]
        [ColumnMetaData("nonembedded_designated2_color_contrast", SqlDbType.VarChar, 7)]
        public string nonembedded_designated2_color_contrast { get; set; }
        [ColumnMapping(149, 149)]
        [ColumnMetaData("nonembedded_designated2_color_overlay", SqlDbType.VarChar, 7)]
        public string nonembedded_designated2_color_overlay { get; set; }
        [ColumnMapping(150, 150)]
        [ColumnMetaData("nonembedded_designated2_magnification", SqlDbType.VarChar, 8)]
        public string nonembedded_designated2_magnification { get; set; }
        [ColumnMapping(151, 151)]
        [ColumnMetaData("nonembedded_designated2_noise", SqlDbType.VarChar, 13)]
        public string nonembedded_designated2_noise { get; set; }
        [ColumnMapping(152, 152)]
        [ColumnMetaData("nonembedded_designated2_read_english", SqlDbType.VarChar, 21)]
        public string nonembedded_designated2_read_english { get; set; }
        [ColumnMapping(153, 153)]
        [ColumnMetaData("nonembedded_designated2_read_spanish", SqlDbType.VarChar, 25)]
        public string nonembedded_designated2_read_spanish { get; set; }
        [ColumnMapping(154, 154)]
        [ColumnMetaData("nonembedded_designated2_scribe", SqlDbType.VarChar, 16)]
        public string nonembedded_designated2_scribe { get; set; }
        [ColumnMapping(155, 155)]
        [ColumnMetaData("nonembedded_designated2_setting", SqlDbType.VarChar, 7)]
        public string nonembedded_designated2_setting { get; set; }
        [ColumnMapping(156, 156)]
        [ColumnMetaData("nonembedded_designated2_simplified_directions", SqlDbType.VarChar, 13)]
        public string nonembedded_designated2_simplified_directions { get; set; }
        [ColumnMapping(157, 157)]
        [ColumnMetaData("nonembedded_designated2_translated_directions", SqlDbType.VarChar, 14)]
        public string nonembedded_designated2_translated_directions { get; set; }
        [ColumnMapping(158, 158)]
        [ColumnMetaData("nonembedded_accommodation_abacus", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_abacus { get; set; }
        [ColumnMapping(159, 159)]
        [ColumnMetaData("nonembedded_accommodation_alternate", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_alternate { get; set; }
        [ColumnMapping(160, 160)]
        [ColumnMetaData("nonembedded_accommodation_sign", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_sign { get; set; }
        [ColumnMapping(161, 161)]
        [ColumnMetaData("nonembedded_accommodation_braille", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_braille { get; set; }
        [ColumnMapping(162, 162)]
        [ColumnMetaData("nonembedded_accommodation_calculator", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_calculator { get; set; }
        [ColumnMapping(163, 163)]
        [ColumnMetaData("nonembedded_supports_large", SqlDbType.Char, 1)]
        public string nonembedded_supports_large { get; set; }
        [ColumnMapping(164, 164)]
        [ColumnMetaData("nonembedded_accommodation_multiplication", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_multiplication { get; set; }
        [ColumnMapping(165, 165)]
        [ColumnMetaData("nonembedded_accommodation_aloud", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_aloud { get; set; }
        [ColumnMapping(166, 166)]
        [ColumnMetaData("nonembedded_accommodation_passages", SqlDbType.Char, 1)]
        public string nonembedded_accommodationpassages { get; set; }
        [ColumnMapping(167, 167)]
        [ColumnMetaData("nonembedded_accommodation_scibe", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_scibe { get; set; }
        [ColumnMapping(168, 168)]
        [ColumnMetaData("nonembedded_accommodation_speech", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_speech { get; set; }
        [ColumnMapping(169, 169)]
        [ColumnMetaData("nonembedded_accommodation_multiday", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_multiday { get; set; }
        [ColumnMapping(170, 170)]
        [ColumnMetaData("nonembedded_accommodation_transfer_responses", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_transfer_responses { get; set; }
        [ColumnMapping(171, 171)]
        [ColumnMetaData("nonembedded_accommodation_numtbl", SqlDbType.Char, 1)]
        public string nonembedded_accommodation_numtbl { get; set; }
        [ColumnMapping(172, 172)]
        [ColumnMetaData("nonembedded_supports_bilingual", SqlDbType.Char, 1)]
        public string nonembedded_supports_bilingual { get; set; }
        [ColumnMapping(173, 173)]
        [ColumnMetaData("nonembedded_supports_aloud", SqlDbType.Char, 1)]
        public string nonembedded_supports_aloud { get; set; }
        [ColumnMapping(174, 174)]
        [ColumnMetaData("nonembedded_supports_scibe", SqlDbType.Char, 1)]
        public string nonembedded_supports_scibe { get; set; }
        [ColumnMapping(175, 175)]
        [ColumnMetaData("nonembedded_supports_setting", SqlDbType.Char, 1)]
        public string nonembedded_supports_setting { get; set; }
        [ColumnMapping(176, 176)]
        [ColumnMetaData("nonembedded_supports_simplified_directions", SqlDbType.Char, 1)]
        public string nonembedded_supports_simplified_directions { get; set; }
        [ColumnMapping(177, 177)]
        [ColumnMetaData("nonembedded_supports_stacked_translation", SqlDbType.Char, 1)]
        public string nonembedded_supports_stacked_translation { get; set; }
        [ColumnMapping(178, 178)]
        [ColumnMetaData("nonembedded_supports_translated_directions", SqlDbType.Char, 1)]
        public string nonembedded_supports_translated_directions { get; set; }
        [ColumnMapping(179, 179)]
        [ColumnMetaData("nonembedded_supports_translations", SqlDbType.VarChar, 36)]
        public string nonembedded_supports_translations { get; set; }
        [ColumnMapping(180, 180)]
        [ColumnMetaData("nonembedded_universal_breaks", SqlDbType.Char, 1)]
        public string nonembedded_universal_breaks { get; set; }
        [ColumnMapping(181, 181)]
        [ColumnMetaData("nonembedded_universal_dictionary", SqlDbType.Char, 1)]
        public string nonembedded_universal_dictionary { get; set; }
        [ColumnMapping(182, 182)]
        [ColumnMetaData("blank06", SqlDbType.Char, 1)]
        public string blank06 { get; set; }
        [ColumnMapping(183, 183)]
        [ColumnMetaData("items_attempted1", SqlDbType.SmallInt, 3)]
        public Int16? items_attempted1 { get; set; }
        [ColumnMapping(184, 184)]
        [ColumnMetaData("items_attempted2", SqlDbType.SmallInt, 3)]
        public Int16? items_attempted2 { get; set; }
        [ColumnMapping(185, 185)]
        [ColumnMetaData("grade_assess_current_year_mninus_1", SqlDbType.Char, 2)]
        public Int16? grade_assess_current_year_mninus_1 { get; set; }
        [ColumnMapping(186, 186)]
        [ColumnMetaData("blank07", SqlDbType.Char, 8)]
        public string blank07 { get; set; }
        [ColumnMapping(187, 187)]
        [ColumnMetaData("smarter_standard_error_measurement_current_year_minus_1", SqlDbType.VarChar, 4)]
        public string smarter_standard_error_measurement_current_year_minus_1 { get; set; }
        [ColumnMapping(188, 188)]
        [ColumnMetaData("scaled_scores_current_year_minus_1", SqlDbType.VarChar, 4)]
        public string scaled_scores_current_year_minus_1 { get; set; }
        [ColumnMapping(189, 189)]
        [ColumnMetaData("condition_code_current_year_minus_1", SqlDbType.VarChar, 4)]
        public string condition_code_current_year_minus_1 { get; set; }
        [ColumnMapping(190, 190)]
        [ColumnMetaData("blank08", SqlDbType.Char, 4)]
        public string blank08 { get; set; }
        [ColumnMapping(191, 191)]
        [ColumnMetaData("blank09", SqlDbType.Char, 1)]
        public string blank09 { get; set; }
        [ColumnMapping(192, 192)]
        [ColumnMetaData("blank10", SqlDbType.Char, 4)]
        public string blank10 { get; set; }
        [ColumnMapping(193, 193)]
        [ColumnMetaData("blank11", SqlDbType.Char, 1)]
        public string blank11 { get; set; }
        [ColumnMapping(194, 194)]
        [ColumnMetaData("blank12", SqlDbType.Char, 4)]
        public string blank12 { get; set; }
        [ColumnMapping(195, 195)]
        [ColumnMetaData("blank13", SqlDbType.Char, 1)]
        public string blank13 { get; set; }
        [ColumnMapping(196, 196)]
        [ColumnMetaData("blank14", SqlDbType.Char, 4)]
        public string blank14 { get; set; }
        [ColumnMapping(197, 197)]
        [ColumnMetaData("blank15", SqlDbType.Char, 1)]
        public string blank15 { get; set; }
        [ColumnMapping(198, 198)]
        [ColumnMetaData("grade_assess_current_year_mninus_2", SqlDbType.Char, 2)]
        public Int16? grade_assess_current_year_mninus_2 { get; set; }
        [ColumnMapping(199, 199)]
        [ColumnMetaData("blank16", SqlDbType.Char, 8)]
        public string blank16 { get; set; }
        [ColumnMapping(200, 200)]
        [ColumnMetaData("smarter_standard_error_measurement_current_year_minus_2", SqlDbType.VarChar, 4)]
        public string smarter_standard_error_measurement_current_year_minus_2 { get; set; }
        [ColumnMapping(201, 201)]
        [ColumnMetaData("scaled_scores_current_year_minus_2", SqlDbType.VarChar, 4)]
        public string scaled_scores_current_year_minus_2 { get; set; }
        [ColumnMapping(202, 202)]
        [ColumnMetaData("condition_code_current_year_minus_2", SqlDbType.VarChar, 4)]
        public string condition_code_current_year_minus_2 { get; set; }
        [ColumnMapping(203, 203)]
        [ColumnMetaData("blank17", SqlDbType.Char, 4)]
        public string blank17 { get; set; }
        [ColumnMapping(204, 204)]
        [ColumnMetaData("blank18", SqlDbType.Char, 1)]
        public string blank18 { get; set; }
        [ColumnMapping(205, 205)]
        [ColumnMetaData("blank19", SqlDbType.Char, 4)]
        public string blank19 { get; set; }
        [ColumnMapping(206, 206)]
        [ColumnMetaData("blank20", SqlDbType.Char, 1)]
        public string blank20 { get; set; }
        [ColumnMapping(207, 207)]
        [ColumnMetaData("blank21", SqlDbType.Char, 4)]
        public string blank21 { get; set; }
        [ColumnMapping(208, 208)]
        [ColumnMetaData("blank22", SqlDbType.Char, 1)]
        public string blank22 { get; set; }
        [ColumnMapping(209, 209)]
        [ColumnMetaData("blank23", SqlDbType.Char, 4)]
        public string blank23 { get; set; }
        [ColumnMapping(210, 210)]
        [ColumnMetaData("blank24", SqlDbType.Char, 1)]
        public string blank24 { get; set; }
        [ColumnMapping(211, 211)]
        [ColumnMetaData("grade_assess_current_year_mninus_3", SqlDbType.Char, 2)]
        public Int16? grade_assess_current_year_mninus_3 { get; set; }
        [ColumnMapping(212, 212)]
        [ColumnMetaData("blank25", SqlDbType.Char, 8)]
        public string blank25 { get; set; }
        [ColumnMapping(213, 213)]
        [ColumnMetaData("smarter_standard_error_measurement_current_year_minus_3", SqlDbType.VarChar, 4)]
        public string smarter_standard_error_measurement_current_year_minus_3 { get; set; }
        [ColumnMapping(214, 214)]
        [ColumnMetaData("scaled_scores_current_year_minus_3", SqlDbType.VarChar, 4)]
        public string scaled_scores_current_year_minus_3 { get; set; }
        [ColumnMapping(215, 215)]
        [ColumnMetaData("condition_code_current_year_minus_3", SqlDbType.VarChar, 4)]
        public string condition_code_current_year_minus_3 { get; set; }
        [ColumnMapping(216, 216)]
        [ColumnMetaData("blank26", SqlDbType.Char, 4)]
        public string blank26 { get; set; }
        [ColumnMapping(217, 217)]
        [ColumnMetaData("blank27", SqlDbType.Char, 1)]
        public string blank27 { get; set; }
        [ColumnMapping(218, 218)]
        [ColumnMetaData("blank28", SqlDbType.Char, 4)]
        public string blank28 { get; set; }
        [ColumnMapping(219, 219)]
        [ColumnMetaData("blank29", SqlDbType.Char, 1)]
        public string blank29 { get; set; }
        [ColumnMapping(220, 220)]
        [ColumnMetaData("blank30", SqlDbType.Char, 4)]
        public string blank30 { get; set; }
        [ColumnMapping(221, 221)]
        [ColumnMetaData("blank31", SqlDbType.Char, 1)]
        public string blank31 { get; set; }
        [ColumnMapping(222, 222)]
        [ColumnMetaData("blank32", SqlDbType.Char, 4)]
        public string blank32 { get; set; }
        [ColumnMapping(223, 223)]
        [ColumnMetaData("blank33", SqlDbType.Char, 1)]
        public string blank33 { get; set; }
        [ColumnMapping(224, 224)]
        [ColumnMetaData("blank34", SqlDbType.Char, 16)]
        public string blank34 { get; set; }
        [ColumnMapping(225, 225)]
        [ColumnMetaData("blank35", SqlDbType.Char, 60)]
        public string blank35 { get; set; }
        [ColumnMapping(226, 226)]
        [ColumnMetaData("end_of_record", SqlDbType.Char, 2)]
        public string end_of_record { get; set; }
    }
}
