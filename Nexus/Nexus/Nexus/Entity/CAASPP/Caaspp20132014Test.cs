﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.CAASPP
{
    class Caaspp20132014Test
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("col0", SqlDbType.Char, 2)]
        public string col0 { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("col1", SqlDbType.Char, 20)]
        public string col1 { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("col2", SqlDbType.Char, 21)]
        public string col2 { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("col3", SqlDbType.Char, 10)]
        public string col3 { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("col4", SqlDbType.Char, 10)]
        public string col4 { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("col5", SqlDbType.Char, 2)]
        public string col5 { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("col6", SqlDbType.Char, 2)]
        public string col6 { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("col7", SqlDbType.Char, 4)]
        public string col7 { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("col8", SqlDbType.Char, 1)]
        public string col8 { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("col9", SqlDbType.Char, 40)]
        public string col9 { get; set; }
        [ColumnMapping(10, 10)]
        [ColumnMetaData("col10", SqlDbType.Char, 7)]
        public string col10 { get; set; }
        [ColumnMapping(11, 11)]
        [ColumnMetaData("col11", SqlDbType.Char, 20)]
        public string col11 { get; set; }
        [ColumnMapping(12, 12)]
        [ColumnMetaData("col12", SqlDbType.Char, 20)]
        public string col12 { get; set; }
        [ColumnMapping(13, 13)]
        [ColumnMetaData("col13", SqlDbType.Char, 10)]
        public string col13 { get; set; }
        [ColumnMapping(14, 14)]
        [ColumnMetaData("col14", SqlDbType.Char, 10)]
        public string col14 { get; set; }
        [ColumnMapping(15, 15)]
        [ColumnMetaData("col15", SqlDbType.Char, 40)]
        public string col15 { get; set; }
        [ColumnMapping(16, 16)]
        [ColumnMetaData("col16", SqlDbType.Char, 7)]
        public string col16 { get; set; }
        [ColumnMapping(17, 17)]
        [ColumnMetaData("col17", SqlDbType.Char, 4)]
        public string col17 { get; set; }
        [ColumnMapping(18, 18)]
        [ColumnMetaData("col18", SqlDbType.Char, 1)]
        public string col18 { get; set; }
        [ColumnMapping(19, 19)]
        [ColumnMetaData("col19", SqlDbType.Char, 1)]
        public string col19 { get; set; }
        [ColumnMapping(20, 20)]
        [ColumnMetaData("col20", SqlDbType.Char, 2)]
        public string col20 { get; set; }
        [ColumnMapping(21, 21)]
        [ColumnMetaData("col21", SqlDbType.Char, 1)]
        public string col21 { get; set; }
        [ColumnMapping(22, 22)]
        [ColumnMetaData("col22", SqlDbType.Char, 7)]
        public string col22 { get; set; }
        [ColumnMapping(23, 23)]
        [ColumnMetaData("col23", SqlDbType.Char, 1)]
        public string col23 { get; set; }
        [ColumnMapping(24, 24)]
        [ColumnMetaData("col24", SqlDbType.Char, 1)]
        public string col24 { get; set; }
        [ColumnMapping(25, 25)]
        [ColumnMetaData("col25", SqlDbType.Char, 1)]
        public string col25 { get; set; }
        [ColumnMapping(26, 26)]
        [ColumnMetaData("col26", SqlDbType.Char, 4)]
        public string col26 { get; set; }
        [ColumnMapping(27, 27)]
        [ColumnMetaData("col27", SqlDbType.Char, 1)]
        public string col27 { get; set; }
        [ColumnMapping(28, 28)]
        [ColumnMetaData("col28", SqlDbType.Char, 1)]
        public string col28 { get; set; }
        [ColumnMapping(29, 29)]
        [ColumnMetaData("col29", SqlDbType.Char, 1)]
        public string col29 { get; set; }
        [ColumnMapping(30, 30)]
        [ColumnMetaData("col30", SqlDbType.Char, 4)]
        public string col30 { get; set; }
        [ColumnMapping(31, 31)]
        [ColumnMetaData("col31", SqlDbType.Char, 1)]
        public string col31 { get; set; }
        [ColumnMapping(32, 32)]
        [ColumnMetaData("col32", SqlDbType.Char, 1)]
        public string col32 { get; set; }
        [ColumnMapping(33, 33)]
        [ColumnMetaData("col33", SqlDbType.Char, 1)]
        public string col33 { get; set; }
        [ColumnMapping(34, 34)]
        [ColumnMetaData("col34", SqlDbType.Char, 5)]
        public string col34 { get; set; }
        [ColumnMapping(35, 35)]
        [ColumnMetaData("col35", SqlDbType.Char, 1)]
        public string col35 { get; set; }
        [ColumnMapping(36, 36)]
        [ColumnMetaData("col36", SqlDbType.Char, 1)]
        public string col36 { get; set; }
        [ColumnMapping(37, 37)]
        [ColumnMetaData("col37", SqlDbType.Char, 1)]
        public string col37 { get; set; }
        [ColumnMapping(38, 38)]
        [ColumnMetaData("col38", SqlDbType.Char, 4)]
        public string col38 { get; set; }
        [ColumnMapping(39, 39)]
        [ColumnMetaData("col39", SqlDbType.Char, 1)]
        public string col39 { get; set; }
        [ColumnMapping(40, 40)]
        [ColumnMetaData("col40", SqlDbType.Char, 1)]
        public string col40 { get; set; }
        [ColumnMapping(41, 41)]
        [ColumnMetaData("col41", SqlDbType.Char, 1)]
        public string col41 { get; set; }
        [ColumnMapping(42, 42)]
        [ColumnMetaData("col42", SqlDbType.Char, 4)]
        public string col42 { get; set; }
        [ColumnMapping(43, 43)]
        [ColumnMetaData("col43", SqlDbType.Char, 1)]
        public string col43 { get; set; }
        [ColumnMapping(44, 44)]
        [ColumnMetaData("col44", SqlDbType.Char, 1)]
        public string col44 { get; set; }
        [ColumnMapping(45, 45)]
        [ColumnMetaData("col45", SqlDbType.Char, 1)]
        public string col45 { get; set; }
        [ColumnMapping(46, 46)]
        [ColumnMetaData("col46", SqlDbType.Char, 4)]
        public string col46 { get; set; }
        [ColumnMapping(47, 47)]
        [ColumnMetaData("col47", SqlDbType.Char, 1)]
        public string col47 { get; set; }
        [ColumnMapping(48, 48)]
        [ColumnMetaData("col48", SqlDbType.Char, 1)]
        public string col48 { get; set; }
        [ColumnMapping(49, 49)]
        [ColumnMetaData("col49", SqlDbType.Char, 1)]
        public string col49 { get; set; }
        [ColumnMapping(50, 50)]
        [ColumnMetaData("col50", SqlDbType.Char, 3)]
        public string col50 { get; set; }
        [ColumnMapping(51, 51)]
        [ColumnMetaData("col51", SqlDbType.Char, 1)]
        public string col51 { get; set; }
        [ColumnMapping(52, 52)]
        [ColumnMetaData("col52", SqlDbType.Char, 1)]
        public string col52 { get; set; }
        [ColumnMapping(53, 53)]
        [ColumnMetaData("col53", SqlDbType.Char, 1)]
        public string col53 { get; set; }
        [ColumnMapping(54, 54)]
        [ColumnMetaData("col54", SqlDbType.Char, 4)]
        public string col54 { get; set; }
        [ColumnMapping(55, 55)]
        [ColumnMetaData("col55", SqlDbType.Char, 1)]
        public string col55 { get; set; }
        [ColumnMapping(56, 56)]
        [ColumnMetaData("col56", SqlDbType.Char, 1)]
        public string col56 { get; set; }
        [ColumnMapping(57, 57)]
        [ColumnMetaData("col57", SqlDbType.Char, 1)]
        public string col57 { get; set; }
        [ColumnMapping(58, 58)]
        [ColumnMetaData("col58", SqlDbType.Char, 4)]
        public string col58 { get; set; }
        [ColumnMapping(59, 59)]
        [ColumnMetaData("col59", SqlDbType.Char, 1)]
        public string col59 { get; set; }
        [ColumnMapping(60, 60)]
        [ColumnMetaData("col60", SqlDbType.Char, 1)]
        public string col60 { get; set; }
        [ColumnMapping(61, 61)]
        [ColumnMetaData("col61", SqlDbType.Char, 1)]
        public string col61 { get; set; }
        [ColumnMapping(62, 62)]
        [ColumnMetaData("col62", SqlDbType.Char, 4)]
        public string col62 { get; set; }
        [ColumnMapping(63, 63)]
        [ColumnMetaData("col63", SqlDbType.Char, 1)]
        public string col63 { get; set; }
        [ColumnMapping(64, 64)]
        [ColumnMetaData("col64", SqlDbType.Char, 1)]
        public string col64 { get; set; }
        [ColumnMapping(65, 65)]
        [ColumnMetaData("col65", SqlDbType.Char, 1)]
        public string col65 { get; set; }
        [ColumnMapping(66, 66)]
        [ColumnMetaData("col66", SqlDbType.Char, 4)]
        public string col66 { get; set; }
        [ColumnMapping(67, 67)]
        [ColumnMetaData("col67", SqlDbType.Char, 1)]
        public string col67 { get; set; }
        [ColumnMapping(68, 68)]
        [ColumnMetaData("col68", SqlDbType.Char, 1)]
        public string col68 { get; set; }
        [ColumnMapping(69, 69)]
        [ColumnMetaData("col69", SqlDbType.Char, 1)]
        public string col69 { get; set; }
        [ColumnMapping(70, 70)]
        [ColumnMetaData("col70", SqlDbType.Char, 4)]
        public string col70 { get; set; }
        [ColumnMapping(71, 71)]
        [ColumnMetaData("col71", SqlDbType.Char, 1)]
        public string col71 { get; set; }
        [ColumnMapping(72, 72)]
        [ColumnMetaData("col72", SqlDbType.Char, 1)]
        public string col72 { get; set; }
        [ColumnMapping(73, 73)]
        [ColumnMetaData("col73", SqlDbType.Char, 1)]
        public string col73 { get; set; }
        [ColumnMapping(74, 74)]
        [ColumnMetaData("col74", SqlDbType.Char, 1)]
        public string col74 { get; set; }
        [ColumnMapping(75, 75)]
        [ColumnMetaData("col75", SqlDbType.Char, 1)]
        public string col75 { get; set; }
        [ColumnMapping(76, 76)]
        [ColumnMetaData("col76", SqlDbType.Char, 1)]
        public string col76 { get; set; }
        [ColumnMapping(77, 77)]
        [ColumnMetaData("col77", SqlDbType.Char, 1)]
        public string col77 { get; set; }
        [ColumnMapping(78, 78)]
        [ColumnMetaData("col78", SqlDbType.Char, 1)]
        public string col78 { get; set; }
        [ColumnMapping(79, 79)]
        [ColumnMetaData("col79", SqlDbType.Char, 3)]
        public string col79 { get; set; }
        [ColumnMapping(80, 80)]
        [ColumnMetaData("col80", SqlDbType.Char, 1)]
        public string col80 { get; set; }
        [ColumnMapping(81, 81)]
        [ColumnMetaData("col81", SqlDbType.Char, 1)]
        public string col81 { get; set; }
        [ColumnMapping(82, 82)]
        [ColumnMetaData("col82", SqlDbType.Char, 1)]
        public string col82 { get; set; }
        [ColumnMapping(83, 83)]
        [ColumnMetaData("col83", SqlDbType.Char, 4)]
        public string col83 { get; set; }
        [ColumnMapping(84, 84)]
        [ColumnMetaData("col84", SqlDbType.Char, 1)]
        public string col84 { get; set; }
        [ColumnMapping(85, 85)]
        [ColumnMetaData("col85", SqlDbType.Char, 1)]
        public string col85 { get; set; }
        [ColumnMapping(86, 86)]
        [ColumnMetaData("col86", SqlDbType.Char, 1)]
        public string col86 { get; set; }
        [ColumnMapping(87, 87)]
        [ColumnMetaData("col87", SqlDbType.Char, 4)]
        public string col87 { get; set; }
        [ColumnMapping(88, 88)]
        [ColumnMetaData("col88", SqlDbType.Char, 1)]
        public string col88 { get; set; }
        [ColumnMapping(89, 89)]
        [ColumnMetaData("col89", SqlDbType.Char, 1)]
        public string col89 { get; set; }
        [ColumnMapping(90, 90)]
        [ColumnMetaData("col90", SqlDbType.Char, 1)]
        public string col90 { get; set; }
        [ColumnMapping(91, 91)]
        [ColumnMetaData("col91", SqlDbType.Char, 4)]
        public string col91 { get; set; }
        [ColumnMapping(92, 92)]
        [ColumnMetaData("col92", SqlDbType.Char, 1)]
        public string col92 { get; set; }
        [ColumnMapping(93, 93)]
        [ColumnMetaData("col93", SqlDbType.Char, 1)]
        public string col93 { get; set; }
        [ColumnMapping(94, 94)]
        [ColumnMetaData("col94", SqlDbType.Char, 1)]
        public string col94 { get; set; }
        [ColumnMapping(95, 95)]
        [ColumnMetaData("col95", SqlDbType.Char, 4)]
        public string col95 { get; set; }
        [ColumnMapping(96, 96)]
        [ColumnMetaData("col96", SqlDbType.Char, 1)]
        public string col96 { get; set; }
        [ColumnMapping(97, 97)]
        [ColumnMetaData("col97", SqlDbType.Char, 1)]
        public string col97 { get; set; }
        [ColumnMapping(98, 98)]
        [ColumnMetaData("col98", SqlDbType.Char, 1)]
        public string col98 { get; set; }
        [ColumnMapping(99, 99)]
        [ColumnMetaData("col99", SqlDbType.Char, 4)]
        public string col99 { get; set; }
        [ColumnMapping(100, 100)]
        [ColumnMetaData("col100", SqlDbType.Char, 1)]
        public string col100 { get; set; }
        [ColumnMapping(101, 101)]
        [ColumnMetaData("col101", SqlDbType.Char, 1)]
        public string col101 { get; set; }
        [ColumnMapping(102, 102)]
        [ColumnMetaData("col102", SqlDbType.Char, 1)]
        public string col102 { get; set; }
        [ColumnMapping(103, 103)]
        [ColumnMetaData("col103", SqlDbType.Char, 1)]
        public string col103 { get; set; }
        [ColumnMapping(104, 104)]
        [ColumnMetaData("col104", SqlDbType.Char, 1)]
        public string col104 { get; set; }
        [ColumnMapping(105, 105)]
        [ColumnMetaData("col105", SqlDbType.Char, 1)]
        public string col105 { get; set; }
        [ColumnMapping(106, 106)]
        [ColumnMetaData("col106", SqlDbType.Char, 3)]
        public string col106 { get; set; }
        [ColumnMapping(107, 107)]
        [ColumnMetaData("col107", SqlDbType.Char, 1)]
        public string col107 { get; set; }
        [ColumnMapping(108, 108)]
        [ColumnMetaData("col108", SqlDbType.Char, 1)]
        public string col108 { get; set; }
        [ColumnMapping(109, 109)]
        [ColumnMetaData("col109", SqlDbType.Char, 1)]
        public string col109 { get; set; }
        [ColumnMapping(110, 110)]
        [ColumnMetaData("col110", SqlDbType.Char, 4)]
        public string col110 { get; set; }
        [ColumnMapping(111, 111)]
        [ColumnMetaData("col111", SqlDbType.Char, 1)]
        public string col111 { get; set; }
        [ColumnMapping(112, 112)]
        [ColumnMetaData("col112", SqlDbType.Char, 1)]
        public string col112 { get; set; }
        [ColumnMapping(113, 113)]
        [ColumnMetaData("col113", SqlDbType.Char, 1)]
        public string col113 { get; set; }
        [ColumnMapping(114, 114)]
        [ColumnMetaData("col114", SqlDbType.Char, 4)]
        public string col114 { get; set; }
        [ColumnMapping(115, 115)]
        [ColumnMetaData("col115", SqlDbType.Char, 1)]
        public string col115 { get; set; }
        [ColumnMapping(116, 116)]
        [ColumnMetaData("col116", SqlDbType.Char, 1)]
        public string col116 { get; set; }
        [ColumnMapping(117, 117)]
        [ColumnMetaData("col117", SqlDbType.Char, 1)]
        public string col117 { get; set; }
        [ColumnMapping(118, 118)]
        [ColumnMetaData("col118", SqlDbType.Char, 4)]
        public string col118 { get; set; }
        [ColumnMapping(119, 119)]
        [ColumnMetaData("col119", SqlDbType.Char, 1)]
        public string col119 { get; set; }
        [ColumnMapping(120, 120)]
        [ColumnMetaData("col120", SqlDbType.Char, 1)]
        public string col120 { get; set; }
        [ColumnMapping(121, 121)]
        [ColumnMetaData("col121", SqlDbType.Char, 3)]
        public string col121 { get; set; }
        [ColumnMapping(122, 122)]
        [ColumnMetaData("col122", SqlDbType.Char, 1)]
        public string col122 { get; set; }
        [ColumnMapping(123, 123)]
        [ColumnMetaData("col123", SqlDbType.Char, 1)]
        public string col123 { get; set; }
        [ColumnMapping(124, 124)]
        [ColumnMetaData("col124", SqlDbType.Char, 1)]
        public string col124 { get; set; }
        [ColumnMapping(125, 125)]
        [ColumnMetaData("col125", SqlDbType.Char, 1)]
        public string col125 { get; set; }
        [ColumnMapping(126, 126)]
        [ColumnMetaData("col126", SqlDbType.Char, 1)]
        public string col126 { get; set; }
        [ColumnMapping(127, 127)]
        [ColumnMetaData("col127", SqlDbType.Char, 1)]
        public string col127 { get; set; }
        [ColumnMapping(128, 128)]
        [ColumnMetaData("col128", SqlDbType.Char, 1)]
        public string col128 { get; set; }
        [ColumnMapping(129, 129)]
        [ColumnMetaData("col129", SqlDbType.Char, 1)]
        public string col129 { get; set; }
        [ColumnMapping(130, 130)]
        [ColumnMetaData("col130", SqlDbType.Char, 1)]
        public string col130 { get; set; }
        [ColumnMapping(131, 131)]
        [ColumnMetaData("col131", SqlDbType.Char, 1)]
        public string col131 { get; set; }
        [ColumnMapping(132, 132)]
        [ColumnMetaData("col132", SqlDbType.Char, 1)]
        public string col132 { get; set; }
        [ColumnMapping(133, 133)]
        [ColumnMetaData("col133", SqlDbType.Char, 1)]
        public string col133 { get; set; }
        [ColumnMapping(134, 134)]
        [ColumnMetaData("col134", SqlDbType.Char, 1)]
        public string col134 { get; set; }
        [ColumnMapping(135, 135)]
        [ColumnMetaData("col135", SqlDbType.Char, 1)]
        public string col135 { get; set; }
        [ColumnMapping(136, 136)]
        [ColumnMetaData("col136", SqlDbType.Char, 1)]
        public string col136 { get; set; }
        [ColumnMapping(137, 137)]
        [ColumnMetaData("col137", SqlDbType.Char, 1)]
        public string col137 { get; set; }
        [ColumnMapping(138, 138)]
        [ColumnMetaData("col138", SqlDbType.Char, 1)]
        public string col138 { get; set; }
        [ColumnMapping(139, 139)]
        [ColumnMetaData("col139", SqlDbType.Char, 1)]
        public string col139 { get; set; }
        [ColumnMapping(140, 140)]
        [ColumnMetaData("col140", SqlDbType.Char, 79)]
        public string col140 { get; set; }
    }
}
