﻿using Microsoft.SqlServer.Server;
using Nexus.CustomAttribute;
using Nexus.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace Nexus.Entity.CAASPP
{
    public class Caaspp : FileDetail
    {

        public Caaspp(FileDrop fileDrop)
        {
            FilePath = fileDrop.TempFileName;
            HasFieldsEnclosedInQuotes = false;
            HasRecordNumber = true;
            HasValidation = false;
            ConnectionString = "Server=PRO-DAT-SQL-03;Database=calpass;Trusted_Connection=true";
            SchemaName = "calpads";

            SqlCommand etlCommand = null;

            // get first line of file
            string line;

            using (StreamReader sr = new StreamReader(FilePath))
            {
                line = sr.ReadLine() ?? "";
            }

            if (fileDrop.SubmissionFileDescriptionId == 4 || fileDrop.SubmissionFileDescriptionId == 14)
            {
                // constants
                const int caaspp20132014DemoFixedLength = 410;
                const int caaspp20132014DemoFieldLength = 145;
                const int caaspp20132014TestFixedLength = 504;
                const int caaspp20132014TestFieldLength = 141;
			    const int caaspp20142015FixedLength = 1323;
			    const int caaspp20142015FieldLength = 251;
			    const int caaspp20152016FixedLength = 1893;
			    const int caaspp20152016FieldLength = 165;
                const int caaspp20162017FieldLength = 227;
                const int caaspp20162017FixedLength = 2101;
                const int caaspp20172018FieldLength = 249;
                const int caaspp20172018FixedLength = 2505;
                const int caaspp20182019FieldLength = 292;
                const int caaspp20182019FixedLength = 2873;
                const int caaspp20192020FieldLength = 335;
                const int caaspp20192020FixedLength = 3441;
                const int caaspp20202021FieldLength = 351;
                const int caaspp20202021FixedLength = 3964;

                string fileExtension = Path.GetExtension(fileDrop.TempFileName).ToLower();

                if (fileExtension == ".csv")
                {
                    Delimiter = ',';
                    HasHeader = true;
                    int FieldCount = line.Split((char)Delimiter).Length;

                    if (FieldCount<=10)
                    {
                        Delimiter = '^';
                        FieldCount = line.Split((char)Delimiter).Length;
                    }

                    if (FieldCount == caaspp20132014DemoFieldLength)
                    {
                        TableName = "CAASPP20132014Demo";
                        Entity = new Caaspp20132014Demo();
                    }
                    else if (FieldCount == caaspp20132014TestFieldLength)
                    {
                        TableName = "CAASPP20132014Test";
                        Entity = new Caaspp20132014Test();
                    }
                    else if (FieldCount == caaspp20142015FieldLength)
                    {
                        TableName = "CAASPP20142015";
                        Entity = new Caaspp20142015();
                    }
                    else if (FieldCount == caaspp20152016FieldLength)
                    {
                        TableName = "CAASPP20152016";
                        Entity = new Caaspp20152016();
                    }
                    else if (FieldCount == caaspp20162017FieldLength)
                    {
                        TableName = "CAASPP20162017";
                        Entity = new Caaspp20162017();
                    }
                    else if (FieldCount == caaspp20172018FieldLength)
                    {
                        TableName = "CAASPP20172018";
                        Entity = new Caaspp20172018();
                    }
                    else if (FieldCount == caaspp20182019FieldLength)
                    {
                        TableName = "CAASPP20182019";
                        Entity = new Caaspp20182019();
                    }
                    else if (FieldCount == caaspp20192020FieldLength)
                    {
                        TableName = "CAASPP20192020";
                        Entity = new Caaspp20192020();
                    }
                    else if (FieldCount == caaspp20202021FieldLength)
                    {
                        TableName = "CAASPP20202021";
                        Entity = new Caaspp20202021();
                    }
                }
                else if (fileExtension == ".dat" || fileExtension == ".txt")
                {


                    if (line.Length == caaspp20132014DemoFixedLength)
                    {
                        TableName = "CAASPP20132014Demo";
                        Entity = new Caaspp20132014Demo();
                    } 
                    else if (line.Length == caaspp20132014TestFixedLength)
                    {
                        TableName = "CAASPP20132014Test";
                        Entity = new Caaspp20132014Test();
                    }
                    else if (line.Length == caaspp20142015FixedLength)
                    {
                        TableName = "CAASPP20142015";
                        Entity = new Caaspp20142015();
                    }
                    else if (line.Length == caaspp20152016FixedLength)
                    {
                        TableName = "CAASPP20152016";
                        Entity = new Caaspp20152016();
                    }
                    else if (line.Length == caaspp20162017FixedLength)
                    {
                        TableName = "CAASPP20162017";
                        Entity = new Caaspp20162017();
                    }
                    else if (line.Length == caaspp20172018FixedLength)
                    {
                        TableName = "CAASPP20172018";
                        Entity = new Caaspp20172018();
                    }
                    else if (line.Length == caaspp20182019FixedLength)
                    {
                        TableName = "CAASPP20182019";
                        Entity = new Caaspp20182019();
                    }
                    else if (line.Length == caaspp20192020FixedLength)
                    {
                        TableName = "CAASPP20192020";
                        Entity = new Caaspp20192020();
                    }
                    else if (line.Length == caaspp20202021FixedLength)
                    {
                        TableName = "CAASPP20202021";
                        Entity = new Caaspp20202021();
                    }
                }
            }


            if (String.IsNullOrWhiteSpace(TableName))
            {
                fileDrop.Status = "Failed";
                fileDrop.ErrorMessage = "Could not determine STAR/CAASPP file type";
                return;
            }

            StageName = TableName.Substring(0, Math.Min(TableName.Length, 107)) + DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
            FullTableName = string.Format("[{0}].[{1}]", SchemaName, TableName);
            FullStageName = string.Format("[{0}].[{1}]", SchemaName, StageName);

            etlCommand = new SqlCommand(
                "[dbo].[GenericMerge]",
                new SqlConnection(ConnectionString)
            );
            etlCommand.CommandType = CommandType.StoredProcedure;
            etlCommand.CommandTimeout = 0;
            etlCommand.Parameters.Add(
                new SqlParameter()
                {
                    ParameterName = "SourceSchemaTableName",
                    Direction = ParameterDirection.Input,
                    SqlDbType = SqlDbType.NVarChar,
                    Size = 517 * 2,
                    Value = FullTableName
                }
            );
            etlCommand.Parameters.Add(
                new SqlParameter()
                {
                    ParameterName = "StageSchemaTableName",
                    Direction = ParameterDirection.Input,
                    SqlDbType = SqlDbType.NVarChar,
                    Size = 517 * 2,
                    Value = FullStageName
                }
            );

            EtlCommand = new List<SqlCommand>();
            EtlCommand.Add(etlCommand);

            // assign properties
            FunctionMap = SqlBulkCopyExt.TextToTypeDictionary(Entity.GetType());
        }
    }
}
