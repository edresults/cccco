﻿using Microsoft.SqlServer.Server;
using Nexus.CustomAttribute;
using Nexus.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace Nexus.Entity.CCFY
{
    public class CCFosterYouth : FileDetail
    {
        //public static void PostProcessing()
        //{
        //    string connString = "Server=PRO-DAT-SQL-03;Database=calpass;Trusted_Connection=true";
        //    string commString = "dbo";

        //    Console.WriteLine("CCFosterYouth: {0}", commString);

        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(connString))
        //        using (SqlCommand comm = new SqlCommand(commString, conn))
        //        {
        //            conn.Open();
        //            comm.CommandType = CommandType.StoredProcedure;
        //            comm.CommandTimeout = 0;
        //            comm.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //    }
        //}

        public CCFosterYouth(FileDrop fileDrop)
        {
            FilePath = fileDrop.TempFileName;
            Delimiter = ',';
            HasFieldsEnclosedInQuotes = true;
            HasHeader = false;
            HasRecordNumber = true;
            HasValidation = true;
            ConnectionString = "Server=PRO-DAT-SQL-03;Database=calpass;Trusted_Connection=true";
            SchemaName = "comis";

            SqlCommand etlCommand = null;

            Entity = new CCFosterYouthCohort();

            TableName = "FosterYouthCohort";            
            StageName = TableName.Substring(0, Math.Min(TableName.Length, 107)) + DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
            FullTableName = string.Format("[{0}].[{1}]", SchemaName, TableName);
            FullStageName = string.Format("[{0}].[{1}]", SchemaName, StageName);
            
            etlCommand = new SqlCommand(
                "[dbo].[GenericMerge]",
                new SqlConnection(ConnectionString)
            );
            etlCommand.CommandType = CommandType.StoredProcedure;
            etlCommand.CommandTimeout = 0;
            etlCommand.Parameters.Add(
                new SqlParameter()
                {
                    ParameterName = "SourceSchemaTableName",
                    Direction = ParameterDirection.Input,
                    SqlDbType = SqlDbType.NVarChar,
                    Size = 517 * 2,
                    Value = FullTableName
                }
            );
            etlCommand.Parameters.Add(
                new SqlParameter()
                {
                    ParameterName = "StageSchemaTableName",
                    Direction = ParameterDirection.Input,
                    SqlDbType = SqlDbType.NVarChar,
                    Size = 517 * 2,
                    Value = FullStageName
                }
            );

            EtlCommand = new List<SqlCommand>();
            EtlCommand.Add(etlCommand);

            // assign properties
            FunctionMap = SqlBulkCopyExt.TextToTypeDictionary(Entity.GetType());

            FunctionMap[1] = s => SqlBulkCopyExt.Encryption(s.Trim(), Encoding.ASCII);
        }
    }
}
