﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.CCFY
{
    class CCFosterYouthCohort
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("OrganizationCode", SqlDbType.Char, 6)]
        public string OrganizationCode { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("StudentId", SqlDbType.Binary, 64)]
        public string StudentId { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("TermId", SqlDbType.Char, 3)]
        public string TermId { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("ProgramName", SqlDbType.VarChar, 255)]
        public string ProgramName { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("FirstName", SqlDbType.VarChar, 30)]
        public string FirstName { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("LastName", SqlDbType.VarChar, 40)]
        public string LastName { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("Birthdate", SqlDbType.Char, 8)]
        public string Birthdate { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("Gender", SqlDbType.Char, 1)]
        public string Gender { get; set; }
    }
}
