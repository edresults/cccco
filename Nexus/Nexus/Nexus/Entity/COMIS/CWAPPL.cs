﻿using Nexus.CustomAttribute;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus.Entity.COMIS
{
    class CWAPPL
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("student_id", SqlDbType.Char, 9, false)]
        public string student_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("term_id", SqlDbType.Char, 3, false)]
        public string term_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("eligibility", SqlDbType.Char, 1)]
        public string eligibility { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("case_mgt", SqlDbType.Char, 1)]
        public string case_mgt { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("counseling", SqlDbType.Char, 1)]
        public string counseling { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("referral", SqlDbType.Char, 1)]
        public string referral { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("other_services", SqlDbType.Char, 5)]
        public string other_services { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("cc_on_campus", SqlDbType.SmallInt)]
        public string cc_on_campus { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("cc_off_campus", SqlDbType.SmallInt)]
        public string cc_off_campus { get; set; }
        [ColumnMapping(10, 10)]
        [ColumnMetaData("cc_dependents", SqlDbType.TinyInt)]
        public string cc_dependents { get; set; }
        [ColumnMapping(11, 11)]
        [ColumnMetaData("total_dependents", SqlDbType.TinyInt)]
        public string total_dependents { get; set; }
        [ColumnMapping(12, 12)]
        [ColumnMetaData("family_status", SqlDbType.Char, 1)]
        public string family_status { get; set; }
        [ColumnMapping(13, 13)]
        [ColumnMetaData("employment_asst", SqlDbType.Char, 6)]
        public string employment_asst { get; set; }
        //[ColumnMapping(14, 14)]
        //[ColumnMetaData("eligibility_time_limit", SqlDbType.Char, 1)]
        //public string eligibility_time_limit { get; set; }
    }
}
