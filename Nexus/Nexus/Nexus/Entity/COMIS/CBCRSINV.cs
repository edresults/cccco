﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class CBCRSINV
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("term_id", SqlDbType.Char, 3, false)]
        public string term_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("course_id", SqlDbType.VarChar, 12, false)]
        public string course_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("control_number", SqlDbType.Char, 12, false)]
        public string control_number { get; set; }
        [ColumnMapping(11, 4)]
        [ColumnMetaData("title", SqlDbType.VarChar, 68)]
        public string title { get; set; }
        [ColumnMapping(4, 5)]
        [ColumnMetaData("top_code", SqlDbType.Char, 6)]
        public string top_code { get; set; }
        [ColumnMapping(12, 6)]
        [ColumnMetaData("credit_status", SqlDbType.Char, 1)]
        public string credit_status { get; set; }
        [ColumnMapping(5, 7)]
        [ColumnMetaData("transfer_status", SqlDbType.Char, 1)]
        public string transfer_status { get; set; }
        [ColumnMapping(6, 8)]
        [ColumnMetaData("units_maximum", SqlDbType.Decimal, 4, 2)]
        public Decimal? units_maximum { get; set; }
        [ColumnMetaData("units_minimum", SqlDbType.Decimal, 4, 2)]
        public Decimal? units_minimum { get; set; }
        [ColumnMapping(7, 10)]
        [ColumnMetaData("basic_skills_status", SqlDbType.Char, 1)]
        public string basic_skills_status { get; set; }
        [ColumnMapping(8, 11)]
        [ColumnMetaData("sam_code", SqlDbType.Char, 1)]
        public string sam_code { get; set; }
        [ColumnMapping(13, 12)]
        [ColumnMetaData("coop_ed_status", SqlDbType.Char, 1)]
        public string coop_ed_status { get; set; }
        [ColumnMapping(14, 13)]
        [ColumnMetaData("classification_code", SqlDbType.Char, 1)]
        public string classification_code { get; set; }
        [ColumnMapping(15, 14)]
        [ColumnMetaData("special_class_status", SqlDbType.Char, 1)]
        public string special_class_status { get; set; }
        [ColumnMetaData("can_code", SqlDbType.VarChar, 6)]
        public string can_code { get; set; }
        [ColumnMetaData("can_seq_code", SqlDbType.VarChar, 8)]
        public string can_seq_code { get; set; }
        [ColumnMetaData("crosswalk_crs_name", SqlDbType.VarChar, 7)]
        public string crosswalk_crs_name { get; set; }
        [ColumnMetaData("crosswalk_crs_number", SqlDbType.VarChar, 9)]
        public string crosswalk_crs_number { get; set; }
        [ColumnMapping(9, 19)]
        [ColumnMetaData("prior_to_college", SqlDbType.Char, 1)]
        public string prior_to_college { get; set; }
        [ColumnMapping(10, 20)]
        [ColumnMetaData("noncredit_category", SqlDbType.Char, 1)]
        public string noncredit_category { get; set; }
        [ColumnMapping(16, 21)]
        [ColumnMetaData("funding_category", SqlDbType.Char, 1)]
        public string funding_category { get; set; }
        [ColumnMetaData("crcc_id", SqlDbType.VarChar, 12)]
        public string crcc_id { get; set; }
        [ColumnMapping(17, 23)]
        [ColumnMetaData("program_status", SqlDbType.Char, 1)]
        public string program_status { get; set; }
        [ColumnMapping(18, 24)]
        [ColumnMetaData("general_education_status", SqlDbType.Char, 1)]
        public string general_education_status { get; set; }
        [ColumnMapping(19, 25)]
        [ColumnMetaData("support_course_status", SqlDbType.Char, 1)]
        public string support_course_status { get; set; }
    }
}
