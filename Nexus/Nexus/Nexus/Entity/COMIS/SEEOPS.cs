﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class SEEOPS
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("student_id", SqlDbType.Char, 9, false)]
        public string student_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("term_id", SqlDbType.Char, 3, false)]
        public string term_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("elig_factor", SqlDbType.Char, 1)]
        public string elig_factor { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("term_accept", SqlDbType.Char, 3)]
        public string term_accept { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("end_term_status", SqlDbType.Char, 1)]
        public string end_term_status { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("eops_units_registered", SqlDbType.Decimal, 4, 2)]
        public Decimal? eops_units_registered { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("eops_care_status", SqlDbType.Char, 1)]
        public string eops_care_status { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("care_term_accept", SqlDbType.Char, 3)]
        public string care_term_accept { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("care_marital_status", SqlDbType.Char, 1)]
        public string care_marital_status { get; set; }
        [ColumnMapping(10, 10)]
        [ColumnMetaData("care_number_dependnts", SqlDbType.Char, 1)]
        public string care_number_dependnts { get; set; }
        [ColumnMapping(11, 11)]
        [ColumnMetaData("care_afdc_duration", SqlDbType.Char, 1)]
        public string care_afdc_duration { get; set; }
        [ColumnMapping(12, 12)]
        [ColumnMetaData("care_withdraw_status", SqlDbType.Char, 1)]
        public string care_withdraw_status { get; set; }
    }
}
