﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class SGPOPS
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("student_id", SqlDbType.Char, 9, false)]
        public string student_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("term_id", SqlDbType.Char, 3, false)]
        public string term_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("military", SqlDbType.Char, 4)]
        public string military { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("military_dependent", SqlDbType.Char, 4)]
        public string military_dependent { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("foster_care", SqlDbType.Char, 1)]
        public string foster_care { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("incarcerated", SqlDbType.Char, 1)]
        public string incarcerated { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("mesa", SqlDbType.Char, 1)]
        public string mesa { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("puente", SqlDbType.Char, 1)]
        public string puente { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("mchs", SqlDbType.Char, 1)]
        public string mchs { get; set; }
        [ColumnMapping(10, 10)]
        [ColumnMetaData("umoja", SqlDbType.Char, 1)]
        public string umoja { get; set; }
        [ColumnMapping(11, 11)]
        [ColumnMetaData("gen_first", SqlDbType.Char, 2)]
        public string gen_first { get; set; }
        [ColumnMapping(12, 12)]
        [ColumnMetaData("caa", SqlDbType.Char, 1)]
        public string caa { get; set; }
        [ColumnMapping(13, 13)]
        [ColumnMetaData("cafyes", SqlDbType.Char, 1)]
        public string cafyes { get; set; }
        [ColumnMapping(14, 14)]
        [ColumnMetaData("ba", SqlDbType.Char, 5)]
        public string ba { get; set; }
        [ColumnMapping(15, 15)]
        [ColumnMetaData("ccap", SqlDbType.Char, 1)]
        public string ccap { get; set; }
        [ColumnMapping(16, 16)]
        [ColumnMetaData("economically_disadv", SqlDbType.Char, 2)]
        public string economically_disadv { get; set; }
        [ColumnMapping(17, 17)]
        [ColumnMetaData("ex_offender", SqlDbType.Char, 1)]
        public string ex_offender { get; set; }
        [ColumnMapping(18, 18)]
        [ColumnMetaData("homeless", SqlDbType.Char, 1)]
        public string homeless { get; set; }
        [ColumnMapping(19, 19)]
        [ColumnMetaData("longterm_unemploy", SqlDbType.Char, 1)]
        public string longterm_unemploy { get; set; }
        [ColumnMapping(20, 20)]
        [ColumnMetaData("cultural_barrier", SqlDbType.Char, 1)]
        public string cultural_barrier { get; set; }
        [ColumnMapping(21, 21)]
        [ColumnMetaData("seasonal_farm_work", SqlDbType.Char, 1)]
        public string seasonal_farm_work { get; set; }
        [ColumnMapping(22, 22)]
        [ColumnMetaData("literacy", SqlDbType.Char, 1)]
        public string literacy { get; set; }
        [ColumnMapping(23, 23)]
        [ColumnMetaData("work_based_learning", SqlDbType.Char, 1)]
        public string work_based_learning { get; set; }
    }
}
