﻿using Nexus.CustomAttribute;
using System;
using System.Data;


namespace Nexus.Entity.COMIS
{
    class APPLICATION
    {
        //[ColumnMapping(0, 0)]
        //[ColumnMetaData("app_id", SqlDbType.BigInt, false)]
        //public Int64 app_id { get; set; }
        [ColumnMapping(0, 0)]
        [ColumnMetaData("ccc_id", SqlDbType.VarChar, 255)]
        public string ccc_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("firstname", SqlDbType.VarChar, 255)]
        public string firstname { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("lastname", SqlDbType.VarChar, 255)]
        public string lastname { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("gender", SqlDbType.VarChar, 255)]
        public string gender { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("birthdate", SqlDbType.VarChar, 255)]
        public string birthdate { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("college_id", SqlDbType.VarChar, 255)]
        public string college_id { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("term_id", SqlDbType.VarChar, 255)]
        public string term_id { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("major_id", SqlDbType.VarChar, 255)]
        public string major_id { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("intended_major", SqlDbType.VarChar, 255)]
        public string intended_major { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("edu_goal", SqlDbType.VarChar, 255)]
        public string edu_goal { get; set; }
        [ColumnMapping(10, 10)]
        [ColumnMetaData("highest_edu_level", SqlDbType.VarChar, 255)]
        public string highest_edu_level { get; set; }
        [ColumnMapping(11, 11)]
        [ColumnMetaData("ack_fin_aid", SqlDbType.VarChar, 255)]
        public string ack_fin_aid { get; set; }
        [ColumnMapping(12, 12)]
        [ColumnMetaData("fin_aid_ref", SqlDbType.VarChar, 255)]
        public string fin_aid_ref { get; set; }
        [ColumnMapping(13, 13)]
        [ColumnMetaData("confirmation", SqlDbType.VarChar, 255)]
        public string confirmation { get; set; }
        [ColumnMapping(14, 14)]
        [ColumnMetaData("sup_page_code", SqlDbType.VarChar, 255)]
        public string sup_page_code { get; set; }
        [ColumnMapping(15, 15)]
        [ColumnMetaData("last_page", SqlDbType.VarChar, 255)]
        public string last_page { get; set; }
        [ColumnMapping(16, 16)]
        [ColumnMetaData("enroll_status", SqlDbType.VarChar, 255)]
        public string enroll_status { get; set; }
        [ColumnMapping(17, 17)]
        [ColumnMetaData("hs_edu_level", SqlDbType.VarChar, 255)]
        public string hs_edu_level { get; set; }
        [ColumnMapping(18, 18)]
        [ColumnMetaData("hs_comp_date", SqlDbType.VarChar, 255)]
        public string hs_comp_date { get; set; }
        [ColumnMapping(19, 19)]
        [ColumnMetaData("higher_edu_level", SqlDbType.VarChar, 255)]
        public string higher_edu_level { get; set; }
        [ColumnMapping(20, 20)]
        [ColumnMetaData("higher_comp_date", SqlDbType.VarChar, 255)]
        public string higher_comp_date { get; set; }
        [ColumnMapping(21, 21)]
        [ColumnMetaData("hs_not_attended", SqlDbType.VarChar, 255)]
        public string hs_not_attended { get; set; }
        [ColumnMapping(22, 22)]
        [ColumnMetaData("cahs_graduated", SqlDbType.VarChar, 255)]
        public string cahs_graduated { get; set; }
        [ColumnMapping(23, 23)]
        [ColumnMetaData("cahs_3year", SqlDbType.VarChar, 255)]
        public string cahs_3year { get; set; }
        [ColumnMapping(24, 24)]
        [ColumnMetaData("hs_name", SqlDbType.VarChar, 255)]
        public string hs_name { get; set; }
        [ColumnMapping(25, 25)]
        [ColumnMetaData("hs_city", SqlDbType.VarChar, 255)]
        public string hs_city { get; set; }
        [ColumnMapping(26, 26)]
        [ColumnMetaData("hs_state", SqlDbType.VarChar, 255)]
        public string hs_state { get; set; }
        [ColumnMapping(27, 27)]
        [ColumnMetaData("hs_country", SqlDbType.VarChar, 255)]
        public string hs_country { get; set; }
        [ColumnMapping(28, 28)]
        [ColumnMetaData("hs_cds", SqlDbType.VarChar, 255)]
        public string hs_cds { get; set; }
        [ColumnMapping(29, 29)]
        [ColumnMetaData("hs_ceeb", SqlDbType.VarChar, 255)]
        public string hs_ceeb { get; set; }
        [ColumnMapping(30, 30)]
        [ColumnMetaData("hs_not_listed", SqlDbType.VarChar, 255)]
        public string hs_not_listed { get; set; }
        [ColumnMapping(31, 31)]
        [ColumnMetaData("home_schooled", SqlDbType.VarChar, 255)]
        public string home_schooled { get; set; }
        [ColumnMapping(32, 32)]
        [ColumnMetaData("college_count", SqlDbType.VarChar, 255)]
        public string college_count { get; set; }
        [ColumnMapping(33, 33)]
        [ColumnMetaData("hs_attendance", SqlDbType.VarChar, 255)]
        public string hs_attendance { get; set; }
        [ColumnMapping(34, 34)]
        [ColumnMetaData("coenroll_confirm", SqlDbType.VarChar, 255)]
        public string coenroll_confirm { get; set; }
        [ColumnMapping(35, 35)]
        [ColumnMetaData("dependent_status", SqlDbType.VarChar, 255)]
        public string dependent_status { get; set; }
        [ColumnMapping(36, 36)]
        [ColumnMetaData("race_ethnic", SqlDbType.VarChar, 255)]
        public string race_ethnic { get; set; }
        [ColumnMapping(37, 37)]
        [ColumnMetaData("hispanic", SqlDbType.VarChar, 255)]
        public string hispanic { get; set; }
        [ColumnMapping(38, 38)]
        [ColumnMetaData("race_group", SqlDbType.VarChar, 255)]
        public string race_group { get; set; }
        [ColumnMapping(39, 39)]
        [ColumnMetaData("military_status", SqlDbType.VarChar, 255)]
        public string military_status { get; set; }
        [ColumnMapping(40, 40)]
        [ColumnMetaData("ca_res_2_years", SqlDbType.VarChar, 255)]
        public string ca_res_2_years { get; set; }
        [ColumnMapping(41, 41)]
        [ColumnMetaData("ca_date_current", SqlDbType.VarChar, 255)]
        public string ca_date_current { get; set; }
        [ColumnMapping(42, 42)]
        [ColumnMetaData("ca_not_arrived", SqlDbType.VarChar, 255)]
        public string ca_not_arrived { get; set; }
        [ColumnMapping(43, 43)]
        [ColumnMetaData("ca_college_employee", SqlDbType.VarChar, 255)]
        public string ca_college_employee { get; set; }
        [ColumnMapping(44, 44)]
        [ColumnMetaData("ca_school_employee", SqlDbType.VarChar, 255)]
        public string ca_school_employee { get; set; }
        [ColumnMapping(45, 45)]
        [ColumnMetaData("ca_seasonal_ag", SqlDbType.VarChar, 255)]
        public string ca_seasonal_ag { get; set; }
        [ColumnMapping(46, 46)]
        [ColumnMetaData("ca_foster_youth", SqlDbType.VarChar, 255)]
        public string ca_foster_youth { get; set; }
        [ColumnMapping(47, 47)]
        [ColumnMetaData("experience", SqlDbType.VarChar, 255)]
        public string experience { get; set; }
        [ColumnMapping(48, 48)]
        [ColumnMetaData("recommend", SqlDbType.VarChar, 255)]
        public string recommend { get; set; }
        [ColumnMapping(49, 49)]
        [ColumnMetaData("comments", SqlDbType.VarChar, 255)]
        public string comments { get; set; }
        [ColumnMapping(50, 50)]
        [ColumnMetaData("comfortable_english", SqlDbType.VarChar, 255)]
        public string comfortable_english { get; set; }
        [ColumnMapping(51, 51)]
        [ColumnMetaData("financial_assistance", SqlDbType.VarChar, 255)]
        public string financial_assistance { get; set; }
        [ColumnMapping(52, 52)]
        [ColumnMetaData("tanf_ssi_ga", SqlDbType.VarChar, 255)]
        public string tanf_ssi_ga { get; set; }
        [ColumnMapping(53, 53)]
        [ColumnMetaData("foster_youths", SqlDbType.VarChar, 255)]
        public string foster_youths { get; set; }
        [ColumnMapping(54, 54)]
        [ColumnMetaData("athletic_intercollegiate", SqlDbType.VarChar, 255)]
        public string athletic_intercollegiate { get; set; }
        [ColumnMapping(55, 55)]
        [ColumnMetaData("athletic_intramural", SqlDbType.VarChar, 255)]
        public string athletic_intramural { get; set; }
        [ColumnMapping(56, 56)]
        [ColumnMetaData("athletic_not_interested", SqlDbType.VarChar, 255)]
        public string athletic_not_interested { get; set; }
        [ColumnMapping(57, 57)]
        [ColumnMetaData("academic_counseling", SqlDbType.VarChar, 255)]
        public string academic_counseling { get; set; }
        [ColumnMapping(58, 58)]
        [ColumnMetaData("basic_skills", SqlDbType.VarChar, 255)]
        public string basic_skills { get; set; }
        [ColumnMapping(59, 59)]
        [ColumnMetaData("calworks", SqlDbType.VarChar, 255)]
        public string calworks { get; set; }
        [ColumnMapping(60, 60)]
        [ColumnMetaData("career_planning", SqlDbType.VarChar, 255)]
        public string career_planning { get; set; }
        [ColumnMapping(61, 61)]
        [ColumnMetaData("child_care", SqlDbType.VarChar, 255)]
        public string child_care { get; set; }
        [ColumnMapping(62, 62)]
        [ColumnMetaData("counseling_personal", SqlDbType.VarChar, 255)]
        public string counseling_personal { get; set; }
        [ColumnMapping(63, 63)]
        [ColumnMetaData("dsps", SqlDbType.VarChar, 255)]
        public string dsps { get; set; }
        [ColumnMapping(64, 64)]
        [ColumnMetaData("eops", SqlDbType.VarChar, 255)]
        public string eops { get; set; }
        [ColumnMapping(65, 65)]
        [ColumnMetaData("esl", SqlDbType.VarChar, 255)]
        public string esl { get; set; }
        [ColumnMapping(66, 66)]
        [ColumnMetaData("health_services", SqlDbType.VarChar, 255)]
        public string health_services { get; set; }
        [ColumnMapping(67, 67)]
        [ColumnMetaData("housing_info", SqlDbType.VarChar, 255)]
        public string housing_info { get; set; }
        [ColumnMapping(68, 68)]
        [ColumnMetaData("employment_assistance", SqlDbType.VarChar, 255)]
        public string employment_assistance { get; set; }
        [ColumnMapping(69, 69)]
        [ColumnMetaData("online_classes", SqlDbType.VarChar, 255)]
        public string online_classes { get; set; }
        [ColumnMapping(70, 70)]
        [ColumnMetaData("reentry_program", SqlDbType.VarChar, 255)]
        public string reentry_program { get; set; }
        [ColumnMapping(71, 71)]
        [ColumnMetaData("scholarship_info", SqlDbType.VarChar, 255)]
        public string scholarship_info { get; set; }
        [ColumnMapping(72, 72)]
        [ColumnMetaData("student_government", SqlDbType.VarChar, 255)]
        public string student_government { get; set; }
        [ColumnMapping(73, 73)]
        [ColumnMetaData("testing_assessment", SqlDbType.VarChar, 255)]
        public string testing_assessment { get; set; }
        [ColumnMapping(74, 74)]
        [ColumnMetaData("transfer_info", SqlDbType.VarChar, 255)]
        public string transfer_info { get; set; }
        [ColumnMapping(75, 75)]
        [ColumnMetaData("tutoring_services", SqlDbType.VarChar, 255)]
        public string tutoring_services { get; set; }
        [ColumnMapping(76, 76)]
        [ColumnMetaData("veterans_services", SqlDbType.VarChar, 255)]
        public string veterans_services { get; set; }
        [ColumnMapping(77, 77)]
        [ColumnMetaData("col1_ceeb", SqlDbType.VarChar, 255)]
        public string col1_ceeb { get; set; }
        [ColumnMapping(78, 78)]
        [ColumnMetaData("col1_cds", SqlDbType.VarChar, 255)]
        public string col1_cds { get; set; }
        [ColumnMapping(79, 79)]
        [ColumnMetaData("col1_not_listed", SqlDbType.VarChar, 255)]
        public string col1_not_listed { get; set; }
        [ColumnMapping(80, 80)]
        [ColumnMetaData("col1_name", SqlDbType.VarChar, 255)]
        public string col1_name { get; set; }
        [ColumnMapping(81, 81)]
        [ColumnMetaData("col1_city", SqlDbType.VarChar, 255)]
        public string col1_city { get; set; }
        [ColumnMapping(82, 82)]
        [ColumnMetaData("col1_state", SqlDbType.VarChar, 255)]
        public string col1_state { get; set; }
        [ColumnMapping(83, 83)]
        [ColumnMetaData("col1_country", SqlDbType.VarChar, 255)]
        public string col1_country { get; set; }
        [ColumnMapping(84, 84)]
        [ColumnMetaData("col1_start_date", SqlDbType.VarChar, 255)]
        public string col1_start_date { get; set; }
        [ColumnMapping(85, 85)]
        [ColumnMetaData("col1_end_date", SqlDbType.VarChar, 255)]
        public string col1_end_date { get; set; }
        [ColumnMapping(86, 86)]
        [ColumnMetaData("col1_degree_date", SqlDbType.VarChar, 255)]
        public string col1_degree_date { get; set; }
        [ColumnMapping(87, 87)]
        [ColumnMetaData("col1_degree_obtained", SqlDbType.VarChar, 255)]
        public string col1_degree_obtained { get; set; }
        [ColumnMapping(88, 88)]
        [ColumnMetaData("col2_ceeb", SqlDbType.VarChar, 255)]
        public string col2_ceeb { get; set; }
        [ColumnMapping(89, 89)]
        [ColumnMetaData("col2_cds", SqlDbType.VarChar, 255)]
        public string col2_cds { get; set; }
        [ColumnMapping(90, 90)]
        [ColumnMetaData("col2_not_listed", SqlDbType.VarChar, 255)]
        public string col2_not_listed { get; set; }
        [ColumnMapping(91, 91)]
        [ColumnMetaData("col2_name", SqlDbType.VarChar, 255)]
        public string col2_name { get; set; }
        [ColumnMapping(92, 92)]
        [ColumnMetaData("col2_city", SqlDbType.VarChar, 255)]
        public string col2_city { get; set; }
        [ColumnMapping(93, 93)]
        [ColumnMetaData("col2_state", SqlDbType.VarChar, 255)]
        public string col2_state { get; set; }
        [ColumnMapping(94, 94)]
        [ColumnMetaData("col2_country", SqlDbType.VarChar, 255)]
        public string col2_country { get; set; }
        [ColumnMapping(95, 95)]
        [ColumnMetaData("col2_start_date", SqlDbType.VarChar, 255)]
        public string col2_start_date { get; set; }
        [ColumnMapping(96, 96)]
        [ColumnMetaData("col2_end_date", SqlDbType.VarChar, 255)]
        public string col2_end_date { get; set; }
        [ColumnMapping(97, 97)]
        [ColumnMetaData("col2_degree_date", SqlDbType.VarChar, 255)]
        public string col2_degree_date { get; set; }
        [ColumnMapping(98, 98)]
        [ColumnMetaData("col2_degree_obtained", SqlDbType.VarChar, 255)]
        public string col2_degree_obtained { get; set; }
        [ColumnMapping(99, 99)]
        [ColumnMetaData("col3_ceeb", SqlDbType.VarChar, 255)]
        public string col3_ceeb { get; set; }
        [ColumnMapping(100, 100)]
        [ColumnMetaData("col3_cds", SqlDbType.VarChar, 255)]
        public string col3_cds { get; set; }
        [ColumnMapping(101, 101)]
        [ColumnMetaData("col3_not_listed", SqlDbType.VarChar, 255)]
        public string col3_not_listed { get; set; }
        [ColumnMapping(102, 102)]
        [ColumnMetaData("col3_name", SqlDbType.VarChar, 255)]
        public string col3_name { get; set; }
        [ColumnMapping(103, 103)]
        [ColumnMetaData("col3_city", SqlDbType.VarChar, 255)]
        public string col3_city { get; set; }
        [ColumnMapping(104, 104)]
        [ColumnMetaData("col3_state", SqlDbType.VarChar, 255)]
        public string col3_state { get; set; }
        [ColumnMapping(105, 105)]
        [ColumnMetaData("col3_country", SqlDbType.VarChar, 255)]
        public string col3_country { get; set; }
        [ColumnMapping(106, 106)]
        [ColumnMetaData("col3_start_date", SqlDbType.VarChar, 255)]
        public string col3_start_date { get; set; }
        [ColumnMapping(107, 107)]
        [ColumnMetaData("col3_end_date", SqlDbType.VarChar, 255)]
        public string col3_end_date { get; set; }
        [ColumnMapping(108, 108)]
        [ColumnMetaData("col3_degree_date", SqlDbType.VarChar, 255)]
        public string col3_degree_date { get; set; }
        [ColumnMapping(109, 109)]
        [ColumnMetaData("col3_degree_obtained", SqlDbType.VarChar, 255)]
        public string col3_degree_obtained { get; set; }
        [ColumnMapping(110, 110)]
        [ColumnMetaData("col4_ceeb", SqlDbType.VarChar, 255)]
        public string col4_ceeb { get; set; }
        [ColumnMapping(111, 111)]
        [ColumnMetaData("col4_cds", SqlDbType.VarChar, 255)]
        public string col4_cds { get; set; }
        [ColumnMapping(112, 112)]
        [ColumnMetaData("col4_not_listed", SqlDbType.VarChar, 255)]
        public string col4_not_listed { get; set; }
        [ColumnMapping(113, 113)]
        [ColumnMetaData("col4_name", SqlDbType.VarChar, 255)]
        public string col4_name { get; set; }
        [ColumnMapping(114, 114)]
        [ColumnMetaData("col4_city", SqlDbType.VarChar, 255)]
        public string col4_city { get; set; }
        [ColumnMapping(115, 115)]
        [ColumnMetaData("col4_state", SqlDbType.VarChar, 255)]
        public string col4_state { get; set; }
        [ColumnMapping(116, 116)]
        [ColumnMetaData("col4_country", SqlDbType.VarChar, 255)]
        public string col4_country { get; set; }
        [ColumnMapping(117, 117)]
        [ColumnMetaData("col4_start_date", SqlDbType.VarChar, 255)]
        public string col4_start_date { get; set; }
        [ColumnMapping(118, 118)]
        [ColumnMetaData("col4_end_date", SqlDbType.VarChar, 255)]
        public string col4_end_date { get; set; }
        [ColumnMapping(119, 119)]
        [ColumnMetaData("col4_degree_date", SqlDbType.VarChar, 255)]
        public string col4_degree_date { get; set; }
        [ColumnMapping(120, 120)]
        [ColumnMetaData("col4_degree_obtained", SqlDbType.VarChar, 255)]
        public string col4_degree_obtained { get; set; }
        [ColumnMapping(121, 121)]
        [ColumnMetaData("college_name", SqlDbType.VarChar, 255)]
        public string college_name { get; set; }
        [ColumnMapping(122, 122)]
        [ColumnMetaData("district_name", SqlDbType.VarChar, 255)]
        public string district_name { get; set; }
        [ColumnMapping(123, 123)]
        [ColumnMetaData("term_code", SqlDbType.VarChar, 255)]
        public string term_code { get; set; }
        [ColumnMapping(124, 124)]
        [ColumnMetaData("term_description", SqlDbType.VarChar, 255)]
        public string term_description { get; set; }
        [ColumnMapping(125, 125)]
        [ColumnMetaData("major_code", SqlDbType.VarChar, 255)]
        public string major_code { get; set; }
        [ColumnMapping(126, 126)]
        [ColumnMetaData("major_description", SqlDbType.VarChar, 255)]
        public string major_description { get; set; }
        [ColumnMapping(127, 127)]
        [ColumnMetaData("foster_youth_status", SqlDbType.VarChar, 255)]
        public string foster_youth_status { get; set; }
        [ColumnMapping(128, 128)]
        [ColumnMetaData("foster_youth_preference", SqlDbType.VarChar, 255)]
        public string foster_youth_preference { get; set; }
        [ColumnMapping(129, 129)]
        [ColumnMetaData("foster_youth_mis", SqlDbType.VarChar, 255)]
        public string foster_youth_mis { get; set; }
        [ColumnMapping(130, 130)]
        [ColumnMetaData("foster_youth_priority", SqlDbType.VarChar, 255)]
        public string foster_youth_priority { get; set; }
        [ColumnMapping(131, 131)]
        [ColumnMetaData("completed_eleventh_grade", SqlDbType.VarChar, 255)]
        public string completed_eleventh_grade { get; set; }
        [ColumnMapping(132, 132)]
        [ColumnMetaData("grade_point_average", SqlDbType.VarChar, 255)]
        public string grade_point_average { get; set; }
        [ColumnMapping(133, 133)]
        [ColumnMetaData("highest_english_course", SqlDbType.VarChar, 255)]
        public string highest_english_course { get; set; }
        [ColumnMapping(134, 134)]
        [ColumnMetaData("highest_english_grade", SqlDbType.VarChar, 255)]
        public string highest_english_grade { get; set; }
        [ColumnMapping(135, 135)]
        [ColumnMetaData("highest_math_course_taken", SqlDbType.VarChar, 255)]
        public string highest_math_course_taken { get; set; }
        [ColumnMapping(136, 136)]
        [ColumnMetaData("highest_math_taken_grade", SqlDbType.VarChar, 255)]
        public string highest_math_taken_grade { get; set; }
        [ColumnMapping(137, 137)]
        [ColumnMetaData("highest_math_course_passed", SqlDbType.VarChar, 255)]
        public string highest_math_course_passed { get; set; }
        [ColumnMapping(138, 138)]
        [ColumnMetaData("highest_math_passed_grade", SqlDbType.VarChar, 255)]
        public string highest_math_passed_grade { get; set; }
        [ColumnMetaData("InterSegmentKey", SqlDbType.Binary, 64)]
        public byte[] InterSegmentKey { get; set; }
        //[ColumnMapping(0, 0)]
        //[ColumnMetaData("app_id", SqlDbType.BigInt, false)]
        //public Int64 app_id { get; set; }
        //[ColumnMapping(1, 1)]
        //[ColumnMetaData("ccc_id", SqlDbType.VarChar, 8, false)]
        //public string ccc_id { get; set; }
        //[ColumnMapping(2, 2)]
        //[ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        //public string college_id { get; set; }
        //[ColumnMapping(3, 3)]
        //[ColumnMetaData("term_id", SqlDbType.Int, false)]
        //public int? term_id { get; set; }
        //[ColumnMapping(4, 4)]
        //[ColumnMetaData("major_id", SqlDbType.Int)]
        //public int? major_id { get; set; }
        //[ColumnMapping(5, 5)]
        //[ColumnMetaData("edu_goal", SqlDbType.Char, 1)]
        //public string edu_goal { get; set; }
        //[ColumnMapping(6, 6)]
        //[ColumnMetaData("highest_edu_level", SqlDbType.Char, 5)]
        //public string highest_edu_level { get; set; }
        //[ColumnMapping(7, 7)]
        //[ColumnMetaData("enroll_status", SqlDbType.Char, 1)]
        //public string enroll_status { get; set; }
        //[ColumnMapping(8, 8)]
        //[ColumnMetaData("gender", SqlDbType.Char, 1)]
        //public string gender { get; set; }
        //[ColumnMapping(9, 9)]
        //[ColumnMetaData("pg_edu_mis", SqlDbType.Char, 2)]
        //public string pg_edu_mis { get; set; }
        //[ColumnMapping(10, 10)]
        //[ColumnMetaData("race_ethnic", SqlDbType.VarChar, 22)]
        //public string race_ethnic { get; set; }
        //[ColumnMapping(11, 11)]
        //[ColumnMetaData("hispanic", SqlDbType.Bit)]
        //public bool? hispanic { get; set; }
        //[ColumnMapping(12, 12)]
        //[ColumnMetaData("race_group", SqlDbType.VarChar, 255)]
        //public string race_group { get; set; }
        //[ColumnMapping(13, 13)]
        //[ColumnMetaData("ssn", SqlDbType.Binary, 64)]
        //public byte?[] ssn { get; set; }
        //[ColumnMapping(14, 14)]
        //[ColumnMetaData("birthdate", SqlDbType.Date)]
        //public DateTime? birthdate { get; set; }
        //[ColumnMapping(15, 15)]
        //[ColumnMetaData("firstname", SqlDbType.VarChar, 50)]
        //public string firstname { get; set; }
        //[ColumnMapping(16, 16)]
        //[ColumnMetaData("lastname", SqlDbType.VarChar, 50)]
        //public string lastname { get; set; }
        //[ColumnMapping(17, 17)]
        //[ColumnMetaData("citizenship_status", SqlDbType.Char, 1)]
        //public string citizenship_status { get; set; }
        //[ColumnMapping(18, 18)]
        //[ColumnMetaData("military_status", SqlDbType.Char, 1)]
        //public string military_status { get; set; }
        //[ColumnMapping(19, 19)]
        //[ColumnMetaData("ca_foster_youth", SqlDbType.Bit)]
        //public bool? ca_foster_youth { get; set; }
        //[ColumnMapping(20, 20)]
        //[ColumnMetaData("comfortable_english", SqlDbType.Bit)]
        //public bool? comfortable_english { get; set; }
        //[ColumnMapping(21, 21)]
        //[ColumnMetaData("tanf_ssi_ga", SqlDbType.Bit)]
        //public bool? tanf_ssi_ga { get; set; }
        //[ColumnMapping(22, 22)]
        //[ColumnMetaData("athletic_intercollegiate", SqlDbType.Bit)]
        //public bool? athletic_intercollegiate { get; set; }
        //[ColumnMapping(23, 23)]
        //[ColumnMetaData("basic_skills", SqlDbType.Bit)]
        //public bool? basic_skills { get; set; }
        //[ColumnMapping(24, 24)]
        //[ColumnMetaData("calworks", SqlDbType.Bit)]
        //public bool? calworks { get; set; }
        //[ColumnMapping(25, 25)]
        //[ColumnMetaData("career_planning", SqlDbType.Bit)]
        //public bool? career_planning { get; set; }
        //[ColumnMapping(26, 26)]
        //[ColumnMetaData("child_care", SqlDbType.Bit)]
        //public bool? child_care { get; set; }
        //[ColumnMapping(27, 27)]
        //[ColumnMetaData("counseling_personal", SqlDbType.Bit)]
        //public bool? counseling_personal { get; set; }
        //[ColumnMapping(28, 28)]
        //[ColumnMetaData("dsps", SqlDbType.Bit)]
        //public bool? dsps { get; set; }
        //[ColumnMapping(29, 29)]
        //[ColumnMetaData("eops", SqlDbType.Bit)]
        //public bool? eops { get; set; }
        //[ColumnMapping(30, 30)]
        //[ColumnMetaData("esl", SqlDbType.Bit)]
        //public bool? esl { get; set; }
        //[ColumnMapping(31, 31)]
        //[ColumnMetaData("health_services", SqlDbType.Bit)]
        //public bool? health_services { get; set; }
        //[ColumnMapping(32, 32)]
        //[ColumnMetaData("housing_info", SqlDbType.Bit)]
        //public bool? housing_info { get; set; }
        //[ColumnMapping(33, 33)]
        //[ColumnMetaData("employment_assistance", SqlDbType.Bit)]
        //public bool? employment_assistance { get; set; }
        //[ColumnMapping(34, 34)]
        //[ColumnMetaData("reentry_program", SqlDbType.Bit)]
        //public bool? reentry_program { get; set; }
        //[ColumnMapping(35, 35)]
        //[ColumnMetaData("scholarship_info", SqlDbType.Bit)]
        //public bool? scholarship_info { get; set; }
        //[ColumnMapping(36, 36)]
        //[ColumnMetaData("student_government", SqlDbType.Bit)]
        //public bool? student_government { get; set; }
        //[ColumnMapping(37, 37)]
        //[ColumnMetaData("testing_assessment", SqlDbType.Bit)]
        //public bool? testing_assessment { get; set; }
        //[ColumnMapping(38, 38)]
        //[ColumnMetaData("transfer_info", SqlDbType.Bit)]
        //public bool? transfer_info { get; set; }
        //[ColumnMapping(39, 39)]
        //[ColumnMetaData("tutoring_services", SqlDbType.Bit)]
        //public bool? tutoring_services { get; set; }
        //[ColumnMapping(40, 40)]
        //[ColumnMetaData("veterans_services", SqlDbType.Bit)]
        //public bool? veterans_services { get; set; }
        //[ColumnMapping(41, 41)]
        //[ColumnMetaData("term_code", SqlDbType.VarChar, 15)]
        //public string term_code { get; set; }
        //[ColumnMapping(42, 42)]
        //[ColumnMetaData("term_description", SqlDbType.VarChar, 100)]
        //public string term_description { get; set; }
        //[ColumnMapping(43, 43)]
        //[ColumnMetaData("major_code", SqlDbType.VarChar, 30)]
        //public string major_code { get; set; }
        //[ColumnMapping(44, 44)]
        //[ColumnMetaData("major_description", SqlDbType.VarChar, 100)]
        //public string major_description { get; set; }
        //[ColumnMapping(45, 45)]
        //[ColumnMetaData("foster_youth_status", SqlDbType.Char, 1)]
        //public string foster_youth_status { get; set; }
        //[ColumnMapping(46, 46)]
        //[ColumnMetaData("foster_youth_mis", SqlDbType.Bit)]
        //public bool? foster_youth_mis { get; set; }
        //[ColumnMapping(47, 47)]
        //[ColumnMetaData("transgender", SqlDbType.Char, 1)]
        //public string transgender { get; set; }
        //[ColumnMapping(48, 48)]
        //[ColumnMetaData("orientation", SqlDbType.Char, 1)]
        //public string orientation { get; set; }
        //[ColumnMapping(49, 49)]
        //[ColumnMetaData("highest_english_course", SqlDbType.VarChar, 2)]
        //public string highest_english_course { get; set; }
        //[ColumnMapping(50, 50)]
        //[ColumnMetaData("highest_english_grade", SqlDbType.VarChar, 2)]
        //public string highest_english_grade { get; set; }
        //[ColumnMapping(51, 51)]
        //[ColumnMetaData("highest_math_course_taken", SqlDbType.VarChar, 2)]
        //public string highest_math_course_taken { get; set; }
        //[ColumnMapping(52, 52)]
        //[ColumnMetaData("highest_math_taken_grade", SqlDbType.VarChar, 2)]
        //public string highest_math_taken_grade { get; set; }
        //[ColumnMapping(53, 53)]
        //[ColumnMetaData("hs_cds_full", SqlDbType.Char, 14)]
        //public string hs_cds_full { get; set; }
        //[ColumnMapping(54, 54)]
        //[ColumnMetaData("no_perm_address_homeless", SqlDbType.Bit)]
        //public bool? no_perm_address_homeless { get; set; }
        //[ColumnMapping(55, 55)]
        //[ColumnMetaData("no_mailing_address_homeless", SqlDbType.Bit)]
        //public bool? no_mailing_address_homeless { get; set; }
        //[ColumnMapping(56, 56)]
        //[ColumnMetaData("homeless_youth", SqlDbType.Bit)]
        //public bool? homeless_youth { get; set; }
        //[ColumnMapping(57, 57)]
        //[ColumnMetaData("cip_code", SqlDbType.VarChar, 6)]
        //public string cip_code { get; set; }
        //[ColumnMapping(58, 58)]
        //[ColumnMetaData("major_category", SqlDbType.VarChar, 100)]
        //public string major_category { get; set; }
        //[ColumnMapping(59, 59)]
        //[ColumnMetaData("race_ethnic_full", SqlDbType.VarChar, 805)]
        //public string race_ethnic_full { get; set; }
        //[ColumnMetaData("InterSegmentKey", SqlDbType.Binary, 64)]
        //public byte[] InterSegmentKey { get; set; }
        //[ColumnMetaData("TermCode", SqlDbType.Char, 3)]
        //public string TermCode { get; set; }
    }
}
