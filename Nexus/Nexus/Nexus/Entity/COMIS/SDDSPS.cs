﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class SDDSPS
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("student_id", SqlDbType.Char, 9, false)]
        public string student_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("term_id", SqlDbType.Char, 3, false)]
        public string term_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("primary_disability", SqlDbType.Char, 1)]
        public string primary_disability { get; set; }
        [ColumnMetaData("pri_disab_srvc_contacts", SqlDbType.SmallInt, 5)]
        public Int16? pri_disab_srvc_contacts { get; set; }
        [ColumnMetaData("secondary_disability", SqlDbType.Char, 1)]
        public string secondary_disability { get; set; }
        [ColumnMetaData("sec_disab_srvc_contacts", SqlDbType.SmallInt, 5)]
        public Int16? sec_disab_srvc_contacts { get; set; }
        [ColumnMetaData("dept_rehab", SqlDbType.Char, 1)]
        public string dept_rehab { get; set; }
    }
}
