﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class XBSECTON
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("term_id", SqlDbType.Char, 3, false)]
        public string term_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("course_id", SqlDbType.VarChar, 12, false)]
        public string course_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("section_id", SqlDbType.VarChar, 6, false)]
        public string section_id { get; set; }
        [ColumnMapping(6, 4)]
        [ColumnMetaData("control_number", SqlDbType.Char, 12, false)]
        public string control_number { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("accounting", SqlDbType.Char, 1)]
        public string accounting { get; set; }
        [ColumnMetaData("first_census", SqlDbType.Char, 8)]
        public string first_census { get; set; }
        [ColumnMetaData("contract_ed", SqlDbType.Char, 1)]
        public string contract_ed { get; set; }
        [ColumnMetaData("units_max", SqlDbType.Decimal, 4, 2)]
        public Decimal? units_max { get; set; }
        [ColumnMetaData("units_min", SqlDbType.Decimal, 4, 2)]
        public Decimal? units_min { get; set; }
        [ColumnMetaData("dsps_status", SqlDbType.Char, 1)]
        public string dsps_status { get; set; }
        [ColumnMetaData("work_based", SqlDbType.Char, 1)]
        public string work_based { get; set; }
        [ColumnMetaData("cvu_cvc", SqlDbType.Char, 1)]
        public string cvu_cvc { get; set; }
        [ColumnMapping(7, 13)]
        [ColumnMetaData("wsch", SqlDbType.Decimal, 6, 2)]
        public Decimal? wsch { get; set; }
        [ColumnMetaData("census_count", SqlDbType.Int, 10)]
        public int? census_count { get; set; }
        [ColumnMapping(4, 15)]
        [ColumnMetaData("day_evening_code", SqlDbType.Char, 1)]
        public string day_evening_code { get; set; }
        [ColumnMetaData("total_hours", SqlDbType.Decimal, 7, 1)]
        public Decimal? total_hours { get; set; }
        [ColumnMetaData("census_status", SqlDbType.Char, 1)]
        public string census_status { get; set; }
    }
}
