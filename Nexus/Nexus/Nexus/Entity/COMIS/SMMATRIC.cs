﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class SMMATRIC
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("student_id", SqlDbType.Char, 9, false)]
        public string student_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("term_id", SqlDbType.Char, 3, false)]
        public string term_id { get; set; }
        [ColumnMapping(4, 3)]
        [ColumnMetaData("goals", SqlDbType.Char, 4)]
        public string goals { get; set; }
        [ColumnMetaData("goals_primary", SqlDbType.Char, 1)]
        public string goals_primary { get; set; }
        [ColumnMetaData("goals_secondary", SqlDbType.Char, 1)]
        public string goals_secondary { get; set; }
        [ColumnMetaData("goals_tertiary", SqlDbType.Char, 1)]
        public string goals_tertiary { get; set; }
        [ColumnMetaData("goals_placeholder", SqlDbType.Char, 1)]
        public string goals_placeholder { get; set; }
        [ColumnMapping(3, 8)]
        [ColumnMetaData("major", SqlDbType.Char, 6)]
        public string major { get; set; }
        [ColumnMetaData("special_needs", SqlDbType.Char, 14)]
        public string special_needs { get; set; }
        [ColumnMetaData("special_needs_01_financial_aid", SqlDbType.Char, 1)]
        public string special_needs_01_financial_aid { get; set; }
        [ColumnMetaData("special_needs_02_child_care", SqlDbType.Char, 1)]
        public string special_needs_02_child_care { get; set; }
        [ColumnMetaData("special_needs_03_disabled_services", SqlDbType.Char, 1)]
        public string special_needs_03_disabled_services { get; set; }
        [ColumnMetaData("special_needs_04_transfer_services", SqlDbType.Char, 1)]
        public string special_needs_04_transfer_services { get; set; }
        [ColumnMetaData("special_needs_05_employment_assistance", SqlDbType.Char, 1)]
        public string special_needs_05_employment_assistance { get; set; }
        [ColumnMetaData("special_needs_06_basic_skills", SqlDbType.Char, 1)]
        public string special_needs_06_basic_skills { get; set; }
        [ColumnMetaData("special_needs_07_tutoring", SqlDbType.Char, 1)]
        public string special_needs_07_tutoring { get; set; }
        [ColumnMetaData("special_needs_08_esl", SqlDbType.Char, 1)]
        public string special_needs_08_esl { get; set; }
        [ColumnMetaData("special_needs_09_eops", SqlDbType.Char, 1)]
        public string special_needs_09_eops { get; set; }
        [ColumnMetaData("special_needs_10_calworks", SqlDbType.Char, 1)]
        public string special_needs_10_calworks { get; set; }
        [ColumnMetaData("special_needs_11_placeholder", SqlDbType.Char, 1)]
        public string special_needs_11_placeholder { get; set; }
        [ColumnMetaData("special_needs_12_placeholder", SqlDbType.Char, 1)]
        public string special_needs_12_placeholder { get; set; }
        [ColumnMetaData("special_needs_13_placeholder", SqlDbType.Char, 1)]
        public string special_needs_13_placeholder { get; set; }
        [ColumnMetaData("special_needs_14_placeholder", SqlDbType.Char, 1)]
        public string special_needs_14_placeholder { get; set; }
        [ColumnMetaData("orientation", SqlDbType.Char, 4)]
        public string orientation { get; set; }
        [ColumnMetaData("orientation_01", SqlDbType.Char, 1)]
        public string orientation_01 { get; set; }
        [ColumnMetaData("orientation_02", SqlDbType.Char, 1)]
        public string orientation_02 { get; set; }
        [ColumnMetaData("orientation_03", SqlDbType.Char, 1)]
        public string orientation_03 { get; set; }
        [ColumnMetaData("orientation_04", SqlDbType.Char, 1)]
        public string orientation_04 { get; set; }
        [ColumnMetaData("assessment", SqlDbType.Char, 4)]
        public string assessment { get; set; }
        [ColumnMetaData("assessment_01", SqlDbType.Char, 1)]
        public string assessment_01 { get; set; }
        [ColumnMetaData("assessment_02", SqlDbType.Char, 1)]
        public string assessment_02 { get; set; }
        [ColumnMetaData("assessment_03", SqlDbType.Char, 1)]
        public string assessment_03 { get; set; }
        [ColumnMetaData("assessment_04", SqlDbType.Char, 1)]
        public string assessment_04 { get; set; }
        [ColumnMetaData("advisement", SqlDbType.Char, 4)]
        public string advisement { get; set; }
        [ColumnMetaData("advisement_01", SqlDbType.Char, 1)]
        public string advisement_01 { get; set; }
        [ColumnMetaData("advisement_02", SqlDbType.Char, 1)]
        public string advisement_02 { get; set; }
        [ColumnMetaData("advisement_03", SqlDbType.Char, 1)]
        public string advisement_03 { get; set; }
        [ColumnMetaData("advisement_04", SqlDbType.Char, 1)]
        public string advisement_04 { get; set; }
        [ColumnMapping(5, 39)]
        [ColumnMetaData("orientation_services", SqlDbType.Char, 1)]
        public string orientation_services { get; set; }
        [ColumnMapping(6, 40)]
        [ColumnMetaData("assessment_services_place", SqlDbType.Char, 1)]
        public string assessment_services_place { get; set; }
        [ColumnMapping(7, 41)]
        [ColumnMetaData("assessment_services_other", SqlDbType.Char, 3)]
        public string assessment_services_other { get; set; }
        [ColumnMetaData("assessment_services_other_01", SqlDbType.Char, 1)]
        public string assessment_services_other_01 { get; set; }
        [ColumnMetaData("assessment_services_other_02", SqlDbType.Char, 1)]
        public string assessment_services_other_02 { get; set; }
        [ColumnMetaData("assessment_services_other_03", SqlDbType.Char, 1)]
        public string assessment_services_other_03 { get; set; }
        [ColumnMapping(8, 45)]
        [ColumnMetaData("advisement_services", SqlDbType.Char, 1)]
        public string advisement_services { get; set; }
        [ColumnMapping(9, 46)]
        [ColumnMetaData("follow_up_services", SqlDbType.Char, 1)]
        public string follow_up_services { get; set; }
    }
}
