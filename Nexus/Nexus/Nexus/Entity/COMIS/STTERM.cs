﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class STTERM
    {
        [ColumnMapping(18, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(19, 1)]
        [ColumnMetaData("student_id", SqlDbType.Char, 9, false)]
        public string student_id { get; set; }
        [ColumnMapping(20, 2)]
        [ColumnMetaData("term_id", SqlDbType.Char, 3, false)]
        public string term_id { get; set; }
        [ColumnMapping(21, 3)]
        [ColumnMetaData("age_at_term", SqlDbType.Int, 10)]
        public int? age_at_term { get; set; }
        [ColumnMapping(5, 4)]
        [ColumnMetaData("zip", SqlDbType.Char, 9)]
        public string zip { get; set; }
        [ColumnMapping(32, 5)]
        [ColumnMetaData("residency", SqlDbType.Char, 5)]
        public string residency { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("education", SqlDbType.Char, 5)]
        public string education { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("high_school", SqlDbType.Char, 6)]
        public string high_school { get; set; }
        [ColumnMapping(9, 8)]
        [ColumnMetaData("enrollment", SqlDbType.Char, 1)]
        public string enrollment { get; set; }
        [ColumnMapping(10, 9)]
        [ColumnMetaData("u_earned_loc", SqlDbType.Decimal, 6, 2)]
        public Decimal? u_earned_loc { get; set; }
        [ColumnMapping(11, 10)]
        [ColumnMetaData("u_earned_trn", SqlDbType.Decimal, 6, 2)]
        public Decimal? u_earned_trn { get; set; }
        [ColumnMapping(12, 11)]
        [ColumnMetaData("u_attmpt_loc", SqlDbType.Decimal, 6, 2)]
        public Decimal? u_attmpt_loc { get; set; }
        [ColumnMapping(13, 12)]
        [ColumnMetaData("u_attmpt_trn", SqlDbType.Decimal, 6, 2)]
        public Decimal? u_attmpt_trn { get; set; }
        [ColumnMapping(14, 13)]
        [ColumnMetaData("g_points_loc", SqlDbType.Decimal, 6, 2)]
        public Decimal? g_points_loc { get; set; }
        [ColumnMapping(15, 14)]
        [ColumnMetaData("g_points_trn", SqlDbType.Decimal, 6, 2)]
        public Decimal? g_points_trn { get; set; }
        [ColumnMapping(33, 15)]
        [ColumnMetaData("gpa_loc", SqlDbType.Decimal, 3, 2)]
        public Decimal? gpa_loc { get; set; }
        [ColumnMapping(34, 16)]
        [ColumnMetaData("gpa_tot", SqlDbType.Decimal, 3, 2)]
        public Decimal? gpa_tot { get; set; }
        [ColumnMapping(16, 17)]
        [ColumnMetaData("aca_standing", SqlDbType.Char, 1)]
        public string aca_standing { get; set; }
        [ColumnMapping(28, 18)]
        [ColumnMetaData("apprentice", SqlDbType.Char, 1)]
        public string apprentice { get; set; }
        [ColumnMapping(29, 19)]
        [ColumnMetaData("transfer_ctr", SqlDbType.Char, 1)]
        public string transfer_ctr { get; set; }
        [ColumnMapping(30, 20)]
        [ColumnMetaData("gain_status", SqlDbType.Char, 1)]
        public string gain_status { get; set; }
        [ColumnMapping(8, 21)]
        [ColumnMetaData("goal", SqlDbType.Char, 1)]
        public string goal { get; set; }
        [ColumnMapping(0, 22)]
        [ColumnMetaData("name_first", SqlDbType.VarChar, 30)]
        public string name_first { get; set; }
        [ColumnMapping(1, 23)]
        [ColumnMetaData("name_last", SqlDbType.VarChar, 40)]
        public string name_last { get; set; }
        [ColumnMapping(2, 24)]
        [ColumnMetaData("gender", SqlDbType.Char, 1)]
        public string gender { get; set; }
        [ColumnMapping(4, 25)]
        [ColumnMetaData("citizenship", SqlDbType.Char, 1)]
        public string citizenship { get; set; }
        [ColumnMapping(31, 26)]
        [ColumnMetaData("jtpa_status", SqlDbType.Char, 1)]
        public string jtpa_status { get; set; }
        [ColumnMapping(27, 27)]
        [ColumnMetaData("census_crload", SqlDbType.Decimal, 4, 2)]
        public Decimal? census_crload { get; set; }
        [ColumnMapping(22, 28)]
        [ColumnMetaData("day_evening_code", SqlDbType.Char, 1)]
        public string day_evening_code { get; set; }
        [ColumnMapping(23, 29)]
        [ColumnMetaData("day_evening_code2", SqlDbType.Char, 1)]
        public string day_evening_code2 { get; set; }
        [ColumnMapping(17, 30)]
        [ColumnMetaData("academic_level", SqlDbType.Char, 1)]
        public string academic_level { get; set; }
        [ColumnMapping(24, 31)]
        [ColumnMetaData("deg_appl_units_earned", SqlDbType.Decimal, 4, 2)]
        public Decimal? deg_appl_units_earned { get; set; }
        [ColumnMapping(25, 32)]
        [ColumnMetaData("headcount_status", SqlDbType.Char, 1)]
        public string headcount_status { get; set; }
        [ColumnMapping(26, 33)]
        [ColumnMetaData("calworks_status", SqlDbType.Char, 1)]
        public string calworks_status { get; set; }
        [ColumnMapping(36, 34)]
        [ColumnMetaData("multi_race", SqlDbType.Char, 21)]
        public string multi_race { get; set; }
        [ColumnMapping(35, 35)]
        [ColumnMetaData("ipeds_race", SqlDbType.Char, 1)]
        public string ipeds_race { get; set; }
        [ColumnMapping(37, 36)]
        [ColumnMetaData("bs_waiver", SqlDbType.Char, 1)]
        public string bs_waiver { get; set; }
        [ColumnMapping(3, 37)]
        [ColumnMetaData("race", SqlDbType.Char, 2)]
        public string race { get; set; }
        [ColumnMapping(38, 38)]
        [ColumnMetaData("parent_ed", SqlDbType.Char, 2)]
        public string parent_ed { get; set; }
        [ColumnMapping(39, 39)]
        [ColumnMetaData("transgender", SqlDbType.Char, 1)]
        public string transgender { get; set; }
        [ColumnMapping(40, 40)]
        [ColumnMetaData("sexual_orientation", SqlDbType.Char, 1)]
        public string sexual_orientation { get; set; }
    }
}
