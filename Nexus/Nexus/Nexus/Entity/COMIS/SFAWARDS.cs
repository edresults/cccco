﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class SFAWARDS
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("college_id", SqlDbType.Char, 3, false)]
        public string college_id { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("student_id", SqlDbType.Char, 9, false)]
        public string student_id { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("term_id", SqlDbType.Char, 3, false)]
        public string term_id { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("term_recd", SqlDbType.Char, 3, false)]
        public string term_recd { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("type_id", SqlDbType.Char, 2, false)]
        public string type_id { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("amount", SqlDbType.Int, 10)]
        public int? amount { get; set; }
    }
}
