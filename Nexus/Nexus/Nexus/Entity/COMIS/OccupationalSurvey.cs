﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class OccupationalSurvey
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("Unique_Survey_ID", SqlDbType.NVarChar, 4000, false)]
        public string Unique_Survey_ID { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("Survey_ID", SqlDbType.VarChar, 255, false)]
        public string Survey_ID { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("Student_ID", SqlDbType.NVarChar, 4000)]
        public string Student_ID { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("Modality", SqlDbType.NVarChar, 4000)]
        public string Modality { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("COLLEGE_ID", SqlDbType.NVarChar, 4000)]
        public string COLLEGE_ID { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("year", SqlDbType.Char, 4, false)]
        public string year { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("SP01", SqlDbType.NVarChar, 4000)]
        public string SP01 { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("SP02", SqlDbType.NVarChar, 4000)]
        public string SP02 { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("SP03", SqlDbType.NVarChar, 4000)]
        public string SP03 { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("SP04", SqlDbType.NVarChar, 4000)]
        public string SP04 { get; set; }
        [ColumnMapping(10, 10)]
        [ColumnMetaData("SP_TITLE", SqlDbType.NVarChar, 4000)]
        public string SP_TITLE { get; set; }
        [ColumnMapping(11, 11)]
        [ColumnMetaData("GE_FLAG", SqlDbType.NVarChar, 4000)]
        public string GE_FLAG { get; set; }
        [ColumnMapping(12, 12)]
        [ColumnMetaData("SB04", SqlDbType.NVarChar, 4000)]
        public string SB04 { get; set; }
        [ColumnMapping(13, 13)]
        [ColumnMetaData("SB14", SqlDbType.NVarChar, 4000)]
        public string SB14 { get; set; }
        [ColumnMapping(14, 14)]
        [ColumnMetaData("SS01", SqlDbType.NVarChar, 4000)]
        public string SS01 { get; set; }
        [ColumnMapping(15, 15)]
        [ColumnMetaData("SS02", SqlDbType.NVarChar, 4000)]
        public string SS02 { get; set; }
        [ColumnMapping(16, 16)]
        [ColumnMetaData("SB16", SqlDbType.NVarChar, 4000)]
        public string SB16 { get; set; }
        [ColumnMapping(17, 17)]
        [ColumnMetaData("SB17", SqlDbType.NVarChar, 4000)]
        public string SB17 { get; set; }
        [ColumnMapping(18, 18)]
        [ColumnMetaData("SB23", SqlDbType.NVarChar, 4000)]
        public string SB23 { get; set; }
        [ColumnMapping(19, 19)]
        [ColumnMetaData("STD1", SqlDbType.NVarChar, 4000)]
        public string STD1 { get; set; }
        [ColumnMapping(20, 20)]
        [ColumnMetaData("STD4", SqlDbType.NVarChar, 4000)]
        public string STD4 { get; set; }
        [ColumnMapping(21, 21)]
        [ColumnMetaData("STD8", SqlDbType.NVarChar, 4000)]
        public string STD8 { get; set; }
        [ColumnMapping(22, 22)]
        [ColumnMetaData("STD9", SqlDbType.NVarChar, 4000)]
        public string STD9 { get; set; }
        [ColumnMapping(23, 23)]
        [ColumnMetaData("STD10", SqlDbType.NVarChar, 4000)]
        public string STD10 { get; set; }
        [ColumnMapping(24, 24)]
        [ColumnMetaData("SCD1", SqlDbType.NVarChar, 4000)]
        public string SCD1 { get; set; }
        [ColumnMapping(25, 25)]
        [ColumnMetaData("SCD2", SqlDbType.NVarChar, 4000)]
        public string SCD2 { get; set; }
        [ColumnMapping(26, 26)]
        [ColumnMetaData("SCD3", SqlDbType.NVarChar, 4000)]
        public string SCD3 { get; set; }
        [ColumnMapping(27, 27)]
        [ColumnMetaData("SCD4", SqlDbType.NVarChar, 4000)]
        public string SCD4 { get; set; }
        [ColumnMapping(28, 28)]
        [ColumnMetaData("SCD5", SqlDbType.NVarChar, 4000)]
        public string SCD5 { get; set; }
        [ColumnMapping(29, 29)]
        [ColumnMetaData("SCD6", SqlDbType.NVarChar, 4000)]
        public string SCD6 { get; set; }
        [ColumnMapping(30, 30)]
        [ColumnMetaData("SG01", SqlDbType.NVarChar, 4000)]
        public string SG01 { get; set; }
        [ColumnMapping(31, 31)]
        [ColumnMetaData("SG03", SqlDbType.NVarChar, 4000)]
        public string SG03 { get; set; }
        [ColumnMapping(32, 32)]
        [ColumnMetaData("SG04", SqlDbType.NVarChar, 4000)]
        public string SG04 { get; set; }
        [ColumnMapping(33, 33)]
        [ColumnMetaData("SG09", SqlDbType.NVarChar, 4000)]
        public string SG09 { get; set; }
        [ColumnMapping(34, 34)]
        [ColumnMetaData("ATTEND", SqlDbType.NVarChar, 4000)]
        public string ATTEND { get; set; }
        [ColumnMapping(35, 35)]
        [ColumnMetaData("Satisfaction", SqlDbType.NVarChar, 4000)]
        public string Satisfaction { get; set; }
        [ColumnMapping(36, 36)]
        [ColumnMetaData("Satisfaction_TEXT", SqlDbType.NVarChar, 4000)]
        public string Satisfaction_TEXT { get; set; }
        [ColumnMapping(37, 37)]
        [ColumnMetaData("Primary_Reason_Study_2", SqlDbType.NVarChar, 4000)]
        public string Primary_Reason_Study_2 { get; set; }
        [ColumnMapping(38, 38)]
        [ColumnMetaData("Primary_Reason_Study_TE", SqlDbType.NVarChar, 4000)]
        public string Primary_Reason_Study_TEXT { get; set; }
        [ColumnMapping(39, 39)]
        [ColumnMetaData("POS", SqlDbType.NVarChar, 4000)]
        public string POS { get; set; }
        [ColumnMapping(40, 40)]
        [ColumnMetaData("POS_TEXT", SqlDbType.NVarChar, 4000)]
        public string POS_TEXT { get; set; }
        [ColumnMapping(41, 41)]
        [ColumnMetaData("Fewer_Goals", SqlDbType.NVarChar, 4000)]
        public string Fewer_Goals { get; set; }
        [ColumnMapping(42, 42)]
        [ColumnMetaData("Fewer_Completed", SqlDbType.NVarChar, 4000)]
        public string Fewer_Completed { get; set; }
        [ColumnMapping(43, 43)]
        [ColumnMetaData("Fewer_Time", SqlDbType.NVarChar, 4000)]
        public string Fewer_Time { get; set; }
        [ColumnMapping(44, 44)]
        [ColumnMetaData("Fewer_Got_Job", SqlDbType.NVarChar, 4000)]
        public string Fewer_Got_Job { get; set; }
        [ColumnMapping(45, 45)]
        [ColumnMetaData("Fewer_Change_Job", SqlDbType.NVarChar, 4000)]
        public string Fewer_Change_Job { get; set; }
        [ColumnMapping(46, 46)]
        [ColumnMetaData("Fewer_Relocation", SqlDbType.NVarChar, 4000)]
        public string Fewer_Relocation { get; set; }
        [ColumnMapping(47, 47)]
        [ColumnMetaData("Fewer_Class", SqlDbType.NVarChar, 4000)]
        public string Fewer_Class { get; set; }
        [ColumnMapping(48, 48)]
        [ColumnMetaData("Fewer_Personal", SqlDbType.NVarChar, 4000)]
        public string Fewer_Personal { get; set; }
        [ColumnMapping(49, 49)]
        [ColumnMetaData("Fewer_Transfer", SqlDbType.NVarChar, 4000)]
        public string Fewer_Transfer { get; set; }
        [ColumnMapping(50, 50)]
        [ColumnMetaData("Fewer_Financial", SqlDbType.NVarChar, 4000)]
        public string Fewer_Financial { get; set; }
        [ColumnMapping(51, 51)]
        [ColumnMetaData("Fewer_Return", SqlDbType.NVarChar, 4000)]
        public string Fewer_Return { get; set; }
        [ColumnMapping(52, 52)]
        [ColumnMetaData("Fewer_Other", SqlDbType.NVarChar, 4000)]
        public string Fewer_Other { get; set; }
        [ColumnMapping(53, 53)]
        [ColumnMetaData("Fewer_TEXT", SqlDbType.NVarChar, 4000)]
        public string Fewer_TEXT { get; set; }
        [ColumnMapping(54, 54)]
        [ColumnMetaData("Career_Services", SqlDbType.NVarChar, 4000)]
        public string Career_Services { get; set; }
        [ColumnMapping(55, 55)]
        [ColumnMetaData("Internship", SqlDbType.NVarChar, 4000)]
        public string Internship { get; set; }
        [ColumnMapping(56, 56)]
        [ColumnMetaData("Apprenticeship", SqlDbType.NVarChar, 4000)]
        public string Apprenticeship { get; set; }
        [ColumnMapping(57, 57)]
        [ColumnMetaData("Apprenticeship_TEXT", SqlDbType.NVarChar, 4000)]
        public string Apprenticeship_TEXT { get; set; }
        [ColumnMapping(58, 58)]
        [ColumnMetaData("Transfer", SqlDbType.NVarChar, 4000)]
        public string Transfer { get; set; }
        [ColumnMapping(59, 59)]
        [ColumnMetaData("Transfer_Name_TEXT", SqlDbType.NVarChar, 4000)]
        public string Transfer_Name_TEXT { get; set; }
        [ColumnMapping(60, 60)]
        [ColumnMetaData("Transfer_Two_Four", SqlDbType.NVarChar, 4000)]
        public string Transfer_Two_Four { get; set; }
        [ColumnMapping(61, 61)]
        [ColumnMetaData("Transfer_OTHER_TEXT", SqlDbType.NVarChar, 4000)]
        public string Transfer_OTHER_TEXT { get; set; }
        [ColumnMapping(62, 62)]
        [ColumnMetaData("Certification", SqlDbType.NVarChar, 4000)]
        public string Certification { get; set; }
        [ColumnMapping(63, 63)]
        [ColumnMetaData("Certification_TEXT", SqlDbType.NVarChar, 4000)]
        public string Certification_TEXT { get; set; }
        [ColumnMapping(64, 64)]
        [ColumnMetaData("Journey", SqlDbType.NVarChar, 4000)]
        public string Journey { get; set; }
        [ColumnMapping(65, 65)]
        [ColumnMetaData("Journey_TEXT", SqlDbType.NVarChar, 4000)]
        public string Journey_TEXT { get; set; }
        [ColumnMapping(66, 66)]
        [ColumnMetaData("Impact_Employment", SqlDbType.NVarChar, 4000)]
        public string Impact_Employment { get; set; }
        [ColumnMapping(67, 67)]
        [ColumnMetaData("Impact_Employment_TEXT", SqlDbType.NVarChar, 4000)]
        public string Impact_Employment_TEXT { get; set; }
        [ColumnMapping(68, 68)]
        [ColumnMetaData("Current_Employment_Stat", SqlDbType.NVarChar, 4000)]
        public string Current_Employment_Status { get; set; }
        [ColumnMapping(69, 69)]
        [ColumnMetaData("Job_Title_TEXT", SqlDbType.NVarChar, 4000)]
        public string Job_Title_TEXT { get; set; }
        [ColumnMapping(70, 70)]
        [ColumnMetaData("Former_Job_Search", SqlDbType.NVarChar, 4000)]
        public string Former_Job_Search { get; set; }
        [ColumnMapping(71, 71)]
        [ColumnMetaData("Former_Search_Length", SqlDbType.NVarChar, 4000)]
        public string Former_Search_Length { get; set; }
        [ColumnMapping(72, 72)]
        [ColumnMetaData("Former_Employment", SqlDbType.NVarChar, 4000)]
        public string Former_Employment { get; set; }
        [ColumnMapping(73, 73)]
        [ColumnMetaData("Former_Hour_Status", SqlDbType.NVarChar, 4000)]
        public string Former_Hour_Status { get; set; }
        [ColumnMapping(74, 74)]
        [ColumnMetaData("Former_Permanence_Statu", SqlDbType.NVarChar, 4000)]
        public string Former_Permanence_Status { get; set; }
        [ColumnMapping(75, 75)]
        [ColumnMetaData("Industry", SqlDbType.NVarChar, 4000)]
        public string Industry { get; set; }
        [ColumnMapping(76, 76)]
        [ColumnMetaData("Industry_TEXT", SqlDbType.NVarChar, 4000)]
        public string Industry_TEXT { get; set; }
        [ColumnMapping(77, 77)]
        [ColumnMetaData("Job_Similarity", SqlDbType.NVarChar, 4000)]
        public string Job_Similarity { get; set; }
        [ColumnMapping(78, 78)]
        [ColumnMetaData("Current_Employment_Hour", SqlDbType.NVarChar, 4000)]
        public string Current_Employment_Hours { get; set; }
        [ColumnMapping(79, 79)]
        [ColumnMetaData("Current_Employment_Perm", SqlDbType.NVarChar, 4000)]
        public string Current_Employment_Permanence { get; set; }
        [ColumnMapping(80, 80)]
        [ColumnMetaData("Former_Wages", SqlDbType.NVarChar, 4000)]
        public string Former_Wages { get; set; }
        [ColumnMapping(81, 81)]
        [ColumnMetaData("Current_Wages", SqlDbType.NVarChar, 4000)]
        public string Current_Wages { get; set; }
        [ColumnMapping(82, 82)]
        [ColumnMetaData("Zip", SqlDbType.NVarChar, 4000)]
        public string Zip { get; set; }
        [ColumnMapping(83, 83)]
        [ColumnMetaData("Current_Employment_Mont", SqlDbType.NVarChar, 4000)]
        public string Current_Employment_Months { get; set; }
        [ColumnMapping(84, 84)]
        [ColumnMetaData("Current_Job_Search", SqlDbType.NVarChar, 4000)]
        public string Current_Job_Search { get; set; }
        [ColumnMapping(85, 85)]
        [ColumnMetaData("Occupation", SqlDbType.NVarChar, 4000)]
        public string Occupation { get; set; }
        [ColumnMapping(86, 86)]
        [ColumnMetaData("CleanTitle", SqlDbType.NVarChar, 4000)]
        public string CleanTitle { get; set; }
        [ColumnMapping(87, 87)]
        [ColumnMetaData("StandardTitle", SqlDbType.NVarChar, 4000)]
        public string StandardTitle { get; set; }
        [ColumnMapping(88, 88)]
        [ColumnMetaData("ONET_1", SqlDbType.NVarChar, 4000)]
        public string ONET_1 { get; set; }
        [ColumnMapping(89, 89)]
        [ColumnMetaData("ONET_2", SqlDbType.NVarChar, 4000)]
        public string ONET_2 { get; set; }
        [ColumnMapping(90, 90)]
        [ColumnMetaData("ONET_3", SqlDbType.NVarChar, 4000)]
        public string ONET_3 { get; set; }
        [ColumnMapping(91, 91)]
        [ColumnMetaData("SOC_1", SqlDbType.NVarChar, 4000)]
        public string SOC_1 { get; set; }
        [ColumnMapping(92, 92)]
        [ColumnMetaData("SOC_2", SqlDbType.NVarChar, 4000)]
        public string SOC_2 { get; set; }
        [ColumnMapping(93, 93)]
        [ColumnMetaData("SOC_3", SqlDbType.NVarChar, 4000)]
        public string SOC_3 { get; set; }
        [ColumnMapping(94, 94)]
        [ColumnMetaData("TransferFlag", SqlDbType.NVarChar, 4000)]
        public string TransferFlag { get; set; }
        [ColumnMapping(95, 95)]
        [ColumnMetaData("CompleterFlag", SqlDbType.NVarChar, 4000)]
        public string CompleterFlag { get; set; }
        [ColumnMapping(96, 96)]
        [ColumnMetaData("NonCreditFlag", SqlDbType.NVarChar, 4000)]
        public string NonCreditFlag { get; set; }
        [ColumnMapping(97, 97)]
        [ColumnMetaData("AUL", SqlDbType.NVarChar, 4000)]
        public string AUL { get; set; }
        [ColumnMapping(98, 98)]
        [ColumnMetaData("SAMAD", SqlDbType.NVarChar, 4000)]
        public string SAMAD { get; set; }
        [ColumnMapping(99, 99)]
        [ColumnMetaData("CRS_PATRN_TOP", SqlDbType.NVarChar, 4000)]
        public string CRS_PATRN_TOP { get; set; }
        [ColumnMapping(100, 100)]
        [ColumnMetaData("CRS_PATRN_TITLE", SqlDbType.NVarChar, 4000)]
        public string CRS_PATRN_TITLE { get; set; }
    }
}
