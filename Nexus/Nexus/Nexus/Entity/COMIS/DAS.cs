﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.COMIS
{
    class DAS
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("INDENTIFIER", SqlDbType.VarChar, 10)]
        public string INDENTIFIER { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("FIRST_NAME", SqlDbType.VarChar, 30)]
        public string FIRST_NAME { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("LAST_NAME", SqlDbType.VarChar, 40)]
        public string LAST_NAME { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("SSN", SqlDbType.Binary, 64, false)]
        public byte[] SSN { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("LMID_REGION", SqlDbType.VarChar, 22)]
        public string LMID_REGION { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("GENDER", SqlDbType.Char, 1)]
        public string GENDER { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("ETHNICITY", SqlDbType.VarChar, 33)]
        public string ETHNICITY { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("VETERAN_FLAG", SqlDbType.Char, 1)]
        public string VETERAN_FLAG { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("APPRENTICE_TRAINEE_FLAG", SqlDbType.Char, 1)]
        public string APPRENTICE_TRAINEE_FLAG { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("OCCUPATION", SqlDbType.VarChar, 73, false)]
        public string OCCUPATION { get; set; }
        [ColumnMapping(10, 10)]
        [ColumnMetaData("DOT_CODE", SqlDbType.VarChar, 255)]
        public string DOT_CODE { get; set; }
        [ColumnMapping(11, 11)]
        [ColumnMetaData("DEGREESCERTIFICATES", SqlDbType.VarChar, 17)]
        public string DEGREESCERTIFICATES { get; set; }
        [ColumnMapping(12, 12)]
        [ColumnMetaData("START_DATE", SqlDbType.Date, false)]
        public DateTime START_DATE { get; set; }
        [ColumnMapping(13, 13)]
        [ColumnMetaData("CURRENT_STATUS", SqlDbType.VarChar, 12)]
        public string CURRENT_STATUS { get; set; }
        [ColumnMapping(14, 14)]
        [ColumnMetaData("STATUS_DATE", SqlDbType.Date, false)]
        public DateTime STATUS_DATE { get; set; }
    }
}
