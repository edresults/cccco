﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.MMAP
{
    class ValidationAssessment
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("CollegeId", SqlDbType.Char, 3)]
        public string CollegeId { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("StudentId", SqlDbType.VarChar, 10)]
        public string StudentId { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("IdStatusCode", SqlDbType.Char, 1)]
        public string IdStatusCode { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("Birthdate", SqlDbType.Char, 8)]
        public string Birthdate { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("Gender", SqlDbType.Char, 1)]
        public string Gender { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("NameFirst", SqlDbType.VarChar, 30)]
        public string NameFirst { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("NameLast", SqlDbType.VarChar, 40)]
        public string NameLast { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("TermCode", SqlDbType.Char, 3)]
        public string TermCode { get; set; }
        [ColumnMapping(8, 8)]
        [ColumnMetaData("PlacementTypeId", SqlDbType.TinyInt)]
        public byte? PlacementTypeId { get; set; }
        [ColumnMapping(9, 9)]
        [ColumnMetaData("MMMethodId", SqlDbType.TinyInt)]
        public byte? MMMethodId { get; set; }
        [ColumnMapping(10, 10)]
        [ColumnMetaData("MMEligibilityId", SqlDbType.TinyInt)]
        public byte? MMEligibilityId { get; set; }
        [ColumnMapping(11, 11)]
        [ColumnMetaData("MMBasisId", SqlDbType.TinyInt)]
        public byte? MMBasisId { get; set; }
        [ColumnMapping(12, 12)]
        [ColumnMetaData("TestSubjectCode", SqlDbType.VarChar, 4)]
        public string TestSubjectCode { get; set; }
        [ColumnMapping(13, 13)]
        [ColumnMetaData("TestDescription", SqlDbType.VarChar, 255)]
        public string TestDescription { get; set; }
        [ColumnMapping(14, 14)]
        [ColumnMetaData("TestDate", SqlDbType.Char, 8)]
        public string TestDate { get; set; }
        [ColumnMapping(15, 15)]
        [ColumnMetaData("TestScoreDescriptionId", SqlDbType.TinyInt)]
        public byte? TestScoreDescriptionId { get; set; }
        [ColumnMapping(16, 16)]
        [ColumnMetaData("TestScore", SqlDbType.Decimal, 6, 2)]
        public decimal? TestScore { get; set; }
        [ColumnMapping(17, 17)]
        [ColumnMetaData("TestScoreCompensatory", SqlDbType.Decimal, 6, 2)]
        public decimal? TestScoreCompensatory { get; set; }
        [ColumnMapping(18, 18)]
        [ColumnMetaData("TestPlacementLevelDescription", SqlDbType.VarChar, 8000)]
        public string TestPlacementLevelDescription { get; set; }
        [ColumnMapping(19, 19)]
        [ColumnMetaData("TestPlacementLevelCode", SqlDbType.Char, 1)]
        public string TestPlacementLevelCode { get; set; }
        [ColumnMapping(20, 20)]
        [ColumnMetaData("TestPlacementMathCourseTransferArray", SqlDbType.Char, 9)]
        public string TestPlacementMathCourseTransferArray { get; set; }
        [ColumnMapping(21, 21)]
        [ColumnMetaData("MMPlacementLevelDescription", SqlDbType.VarChar, 8000)]
        public string MMPlacementLevelDescription { get; set; }
        [ColumnMapping(22, 22)]
        [ColumnMetaData("MMPlacementLevelCode", SqlDbType.Char, 1)]
        public string MMPlacementLevelCode { get; set; }
        [ColumnMapping(23, 23)]
        [ColumnMetaData("MMPlacementMathCourseTransferArray", SqlDbType.Char, 9)]
        public string MMPlacementMathCourseTransferArray { get; set; }
        [ColumnMapping(24, 24)]
        [ColumnMetaData("FinalPlacementLevelDescription", SqlDbType.VarChar, 8000)]
        public string FinalPlacementLevelDescription { get; set; }
        [ColumnMapping(25, 25)]
        [ColumnMetaData("FinalPlacementLevelCode", SqlDbType.Char, 1)]
        public string FinalPlacementLevelCode { get; set; }
        [ColumnMapping(26, 26)]
        [ColumnMetaData("FinalPlacementMathCourseTransferArray", SqlDbType.Char, 9)]
        public string FinalPlacementMathCourseTransferArray { get; set; }
    }
}
