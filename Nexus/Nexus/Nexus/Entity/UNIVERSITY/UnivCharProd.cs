﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.UNIVERSITY
{
    class UnivCharProd
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("School", SqlDbType.Char, 6)]
        public String School { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("YrTerm", SqlDbType.Char, 5)]
        public String YrTerm { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("CourseTitle", SqlDbType.VarChar, 40)]
        public String CourseTitle { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("Discipline", SqlDbType.Char, 2)]
        public String Discipline { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("Subdiscipline", SqlDbType.Char, 1)]
        public String Subdiscipline { get; set; }
        [ColumnMapping(5, 5)]
        [ColumnMetaData("RemedialCode", SqlDbType.Char, 1)]
        public String RemedialCode { get; set; }
        [ColumnMapping(6, 6)]
        [ColumnMetaData("GatewayCode", SqlDbType.Char, 1)]
        public String GatewayCode { get; set; }
        [ColumnMapping(7, 7)]
        [ColumnMetaData("CourseLevel", SqlDbType.Char, 1)]
        public String CourseLevel { get; set; }
    }
}
