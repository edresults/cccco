﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Nexus.Entity.Constraint
{
    public class CheckConstraint
    {
        public string ConstraintName { get; set; }
        public string ColumnName { get; set; }
        public string ColumnDefinition { get; set; }

        public static IEnumerable<CheckConstraint> Get(INexus iNexus)
        {
            string ckStructure = @"SELECT ConstraintName = ck.name, ColumnName = c.name, ColumnDefinition = ck.definition FROM sys.columns c inner join sys.check_constraints ck on ck.parent_object_id = c.object_id and ck.parent_column_id = c.column_id inner join sys.columns sc on sc.name = c.name WHERE c.object_id = object_id(@SourceSchemaTableName) and sc.object_id = object_id(@StageSchemaTableName) ORDER BY c.column_id;";

            using (SqlConnection sqlConnection = new SqlConnection(iNexus.ConnectionString))
            using (SqlCommand sqlCommand = new SqlCommand(ckStructure, sqlConnection))
            {
                sqlCommand.CommandType = CommandType.Text;
                // add paramter
                sqlCommand.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "SourceSchemaTableName",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 517 * 2,
                        Value = iNexus.FullTableName
                    }
                );
                sqlCommand.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "StageSchemaTableName",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 517 * 2,
                        Value = iNexus.FullStageName
                    }
                );
                // open connection
                sqlConnection.Open();
                // get data
                using (IDataReader reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        yield return new CheckConstraint()
                        {
                            ConstraintName = reader.GetString(0),
                            ColumnName = reader.GetString(1),
                            ColumnDefinition = reader.GetString(2)
                        };
                    }
                }
            }
        }

        public string GetCount(INexus iNexus)
        {
            string query = @"SELECT Errors = count(*) FROM {0} WHERE [{1}] is not null and not {2};";
            int errors = 0;

            IEnumerable<CheckConstraint> constraints = Get(iNexus);

            using (SqlConnection sqlConnection = new SqlConnection(iNexus.ConnectionString))
            {
                sqlConnection.Open();

                foreach (CheckConstraint constraint in constraints)
                {
                    // format query string
                    string cmdText = string.Format(query, iNexus.FullStageName, constraint.ColumnName, constraint.ColumnDefinition);

                    using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.Text;
                        errors += (int)sqlCommand.ExecuteScalar();
                    }
                }
            }

            if (errors > 0)
            {
                return string.Format("Error:      File has {0} Check Constraint Violations", errors);
            }

            return string.Empty;
        }

        public string GetDetail(INexus iNexus)
        {
            string query = @"SELECT convert(varchar, SubmissionFileRecordNumber) + replicate(' ', max(len(SubmissionFileRecordNumber)) over() - len(SubmissionFileRecordNumber)), [{1}] FROM {0} WHERE [{1}] is not null and not {2};";

            IEnumerable<CheckConstraint> constraints = Get(iNexus);
            List<TableConstraint> errors = new List<TableConstraint>();

            using (SqlConnection sqlConnection = new SqlConnection(iNexus.ConnectionString))
            {
                sqlConnection.Open();

                foreach (CheckConstraint constraint in constraints)
                {
                    // format query string
                    string cmdText = string.Format(query, iNexus.FullStageName, constraint.ColumnName, constraint.ColumnDefinition);

                    using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.Text;

                        using (IDataReader reader = sqlCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                errors.Add(new TableConstraint() { ColumnName = constraint.ColumnName, RecordNumber = reader.GetString(0), ColumnValue = reader[1] });
                            }
                        }
                    }
                }
            }

            int errorsCount = errors.Count;

            if (errorsCount == 0) return string.Empty;

            string output = "Check Constraint Violations :: ColumnName | RecordNumber | ColumnValue";
            int padLength = 0;

            for (int errorsIndex = 0; errorsIndex < errorsCount; errorsIndex++)
            {
                int columnNameLength = errors[errorsIndex].ColumnName.Length;

                if (columnNameLength > padLength)
                {
                    padLength = columnNameLength;
                }
            }

            foreach (TableConstraint error in errors)
            {
                string padSpace = new string(' ', padLength - error.ColumnName.Length);

                output += Environment.NewLine + string.Format("{0} | {1} | {2}", error.ColumnName + padSpace, error.RecordNumber, error.ColumnValue);
            }

            return output;
        }
    }
}
