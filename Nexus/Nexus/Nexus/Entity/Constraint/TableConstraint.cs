﻿namespace Nexus.Entity.Constraint
{
    public class TableConstraint
    {
        public string RecordNumber { get; set; }
        public string ColumnName { get; set; }
        public object ColumnValue { get; set; }
    }
}
