﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Nexus.Entity.Constraint
{
    public class ForeignKey
    {
        public string ConstraintName { get; set; }
        public string TargetTableColumnName { get; set; }
        public bool TargetTableColumnIsNullable { get; set; }
        public string ReferenceSchemaName { get; set; }
        public string ReferenceTableName { get; set; }
        public string ReferenceTableColumnName { get; set; }
        public string ReferenceTableColumnType { get; set; }

        public static IEnumerable<ForeignKey> Get(INexus iNexus)
        {
            string fkStructure = @"SELECT ConstraintName = fk.name, TargetTableColumnName = c.name, TargetTableColumnIsNullable = c.is_nullable, ReferenceSchemaName = rs.name, ReferenceTableName = rt.name, ReferenceTableColumnName = rc.name, ReferenceTableColumnType = ct.name FROM sys.tables t inner join sys.columns c on c.object_id = t.object_id inner join sys.foreign_keys fk on fk.parent_object_id = t.object_id inner join sys.foreign_key_columns fkc on fkc.constraint_object_id = fk.object_id and fkc.parent_column_id = c.column_id inner join sys.tables rt on rt.object_id = fkc.referenced_object_id inner join sys.columns rc on rc.object_id = rt.object_id and rc.column_id = fkc.referenced_column_id inner join sys.schemas rs on rs.schema_id = rt.schema_id inner join sys.types ct on ct.system_type_id = rc.system_type_id and ct.user_type_id = rc.user_type_id inner join sys.columns sc on sc.name = c.name WHERE t.object_id = object_id(@SourceSchemaTableName) and sc.object_id = object_id(@StageSchemaTableName) ORDER BY c.column_id;";

            using (SqlConnection sqlConnection = new SqlConnection(iNexus.ConnectionString))
            using (SqlCommand sqlCommand = new SqlCommand(fkStructure, sqlConnection))
            {
                sqlCommand.CommandType = CommandType.Text;
                // add paramter
                sqlCommand.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "SourceSchemaTableName",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 517 * 2,
                        Value = iNexus.FullTableName
                    }
                );
                sqlCommand.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "StageSchemaTableName",
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.NVarChar,
                        Size = 517 * 2,
                        Value = iNexus.FullStageName
                    }
                );
                // open connection
                sqlConnection.Open();
                // get data
                using (IDataReader reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        yield return new ForeignKey()
                        {
                            ConstraintName = reader.GetString(0),
                            TargetTableColumnName = reader.GetString(1),
                            TargetTableColumnIsNullable = reader.GetBoolean(2),
                            ReferenceSchemaName = reader.GetString(3),
                            ReferenceTableName = reader.GetString(4),
                            ReferenceTableColumnName = reader.GetString(5),
                            ReferenceTableColumnType = reader.GetString(6)
                        };
                    }
                }
            }
        }

        public string GetCount(INexus iNexus)
        {
            string query = @"SELECT Errors = count(*) FROM {0} target WHERE target.[{1}] is not null and not exists (SELECT 0 FROM [{2}].[{3}] reference WHERE target.[{1}] = reference.[{4}]);";
            int errors = 0;

            IEnumerable<ForeignKey> constraints = Get(iNexus);

            using (SqlConnection sqlConnection = new SqlConnection(iNexus.ConnectionString))
            {
                sqlConnection.Open();

                foreach (ForeignKey constraint in constraints)
                {
                    // format query string
                    string cmdText = string.Format(
                        query,
                        iNexus.FullStageName,
                        constraint.TargetTableColumnName,
                        constraint.ReferenceSchemaName,
                        constraint.ReferenceTableName,
                        constraint.ReferenceTableColumnName
                    );

                    using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.Text;
                        errors += (int)sqlCommand.ExecuteScalar();
                    }
                }
            }

            if (errors > 0)
            {
                return string.Format("Error:      File has {0} Foreign Key Constraint Violations", errors);
            }

            return string.Empty;
        }

        public string GetDetail(INexus iNexus)
        {
            string query = @"SELECT convert(varchar, SubmissionFileRecordNumber) + replicate(' ', max(len(SubmissionFileRecordNumber)) over() - len(SubmissionFileRecordNumber)), [{1}] FROM {0} target WHERE target.[{1}] is not null and not exists (SELECT 0 FROM [{2}].[{3}] reference WHERE target.[{1}] = reference.[{4}]);";

            IEnumerable<ForeignKey> constraints = Get(iNexus);
            List<TableConstraint> errors = new List<TableConstraint>();

            using (SqlConnection sqlConnection = new SqlConnection(iNexus.ConnectionString))
            {
                sqlConnection.Open();

                foreach (ForeignKey constraint in constraints)
                {
                    // format query string
                    string cmdText = string.Format(
                        query,
                        iNexus.FullStageName,
                        constraint.TargetTableColumnName,
                        constraint.ReferenceSchemaName,
                        constraint.ReferenceTableName,
                        constraint.ReferenceTableColumnName
                    );

                    using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.Text;

                        using (IDataReader reader = sqlCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                errors.Add(new TableConstraint() { ColumnName = constraint.TargetTableColumnName, RecordNumber = reader.GetString(0), ColumnValue = reader[1] });
                            }
                        }
                    }
                }
            }

            int errorsCount = errors.Count;

            if (errorsCount == 0) return string.Empty;

            string output = "Foreign Key Constraint Violations :: ColumnName | RecordNumber | ColumnValue";
            int padLength = 0;

            for (int errorsIndex = 0; errorsIndex < errorsCount; errorsIndex++)
            {
                int columnNameLength = errors[errorsIndex].ColumnName.Length;

                if (columnNameLength > padLength)
                {
                    padLength = columnNameLength;
                }
            }

            foreach (TableConstraint error in errors)
            {
                string padSpace = new string(' ', padLength - error.ColumnName.Length);

                output += Environment.NewLine + string.Format("{0} | {1} | {2}", error.ColumnName + padSpace, error.RecordNumber, error.ColumnValue);
            }

            return output;
        }
    }
}
