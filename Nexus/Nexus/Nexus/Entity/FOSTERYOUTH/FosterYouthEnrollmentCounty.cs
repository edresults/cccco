﻿using Nexus.CustomAttribute;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus.Entity.FOSTERYOUTH
{
    class FosterYouthEnrollmentCounty
    {
        [ColumnMapping(4, 0)]
        [ColumnMetaData("SchoolCode", SqlDbType.Char, 7, false)]
        public string SchoolCode { get; set; }

        [ColumnMapping(5, 1)]
        [ColumnMetaData("SchoolName", SqlDbType.VarChar, 90, true)]
        public string SchoolName { get; set; }

        [ColumnMapping(6, 2)]
        [ColumnMetaData("StateId", SqlDbType.Char, 10, false)]
        public string StateId { get; set; }

        [ColumnMapping(7, 3)]
        [ColumnMetaData("StudentName", SqlDbType.VarChar, 72, true)]
        public string StudentName { get; set; }

        [ColumnMapping(8, 4)]
        [ColumnMetaData("LocalId", SqlDbType.VarChar, 15, false)]
        public string LocalId { get; set; }

        [ColumnMapping(9, 5)]
        [ColumnMetaData("BirthDate", SqlDbType.Date, true)]
        public DateTime? BirthDate { get; set; }

        [ColumnMapping(10, 6)]
        [ColumnMetaData("Gender", SqlDbType.Char, 1, true)]
        public string Gender { get; set; }

        [ColumnMapping(11, 7)]
        [ColumnMetaData("GradeLevel", SqlDbType.Char, 2, true)]
        public string GradeLevel { get; set; }

        [ColumnMapping(12, 8)]
        [ColumnMetaData("EnrollmentStartDate", SqlDbType.Date, true, "MM/dd/yyyy")]
        public DateTime? EnrollmentStartDate { get; set; }

        [ColumnMapping(13, 9)]
        [ColumnMetaData("EnrollmentStatus", SqlDbType.VarChar, 30, true)]
        public string EnrollmentStatus { get; set; }

        [ColumnMapping(14, 10)]
        [ColumnMetaData("LocalMatch", SqlDbType.Char, 1, true)]
        public string LocalMatch { get; set; }

        [ColumnMapping(15, 11)]
        [ColumnMetaData("StateMatch", SqlDbType.Char, 1, true)]
        public string StateMatch { get; set; }

        [ColumnMapping(0, 12)]
        [ColumnMetaData("CountyOfJurisdiction", SqlDbType.VarChar, 15, true)]
        public string CountyOfJurisdiction { get; set; }

        [ColumnMapping(16, 13)]
        [ColumnMetaData("ServiceType", SqlDbType.VarChar, 2, true)]
        public string ServiceType { get; set; }

        [ColumnMapping(17, 14)]
        [ColumnMetaData("ClientId", SqlDbType.Char, 10, false)]
        public string ClientId { get; set; }

        [ColumnMapping(18, 15)]
        [ColumnMetaData("CaseId", SqlDbType.VarChar, 19, true)]
        public string CaseId { get; set; }

        [ColumnMapping(19, 16)]
        [ColumnMetaData("CaseStartDate", SqlDbType.Date, true, "MM/dd/yyyy")]
        public DateTime? CaseStartDate { get; set; }

        [ColumnMapping(20, 17)]
        [ColumnMetaData("EpisodeStartDate", SqlDbType.Date, true, "MM/dd/yyyy")]
        public DateTime? EpisodeStartDate { get; set; }

        [ColumnMapping(21, 18)]
        [ColumnMetaData("EpisodeEndDate", SqlDbType.Date, true, "MM/dd/yyyy")]
        public DateTime? EpisodeEndDate { get; set; }

        [ColumnMapping(22, 19)]
        [ColumnMetaData("SocialWorkerName", SqlDbType.VarChar, 72, true)]
        public string SocialWorkerName { get; set; }

        [ColumnMapping(23, 20)]
        [ColumnMetaData("SocialWorkerPhoneNumber", SqlDbType.Char, 14, true)]
        public string SocialWorkerPhoneNumber { get; set; }

        [ColumnMapping(24, 21)]
        [ColumnMetaData("ResponsibleAgency", SqlDbType.Char, 1, true)]
        public string ResponsibleAgency { get; set; }

        [ColumnMapping(25, 22)]
        [ColumnMetaData("ParentalRightLimited", SqlDbType.Char, 1, true)]
        public string ParentalRightLimited { get; set; }

        [ColumnMapping(26, 23)]
        [ColumnMetaData("CourtAppointedName", SqlDbType.VarChar, 72, true)]
        public string CourtAppointedName { get; set; }

        [ColumnMapping(27, 24)]
        [ColumnMetaData("CourtAppointedPhoneNumber", SqlDbType.Char, 14, true)]
        public string CourtAppointedPhoneNumber { get; set; }

        [ColumnMapping(28, 25)]
        [ColumnMetaData("UpdateDate", SqlDbType.Date, true, "MM/dd/yyyy")]
        public DateTime? UpdateDate { get; set; }
    }
}
