﻿using Microsoft.SqlServer.Server;
using Nexus.CustomAttribute;
using Nexus.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace Nexus.Entity.FOSTERYOUTH
{
    public class FosterYouth : FileDetail
    {
        public FosterYouth(FileDrop fileDrop)
        {
            FilePath = fileDrop.TempFileName;
            Delimiter = ',';
            HasFieldsEnclosedInQuotes = true;
            HasHeader = true;
            HasRecordNumber = true;
            HasValidation = true;
            ConnectionString = "Server=PRO-DAT-SQL-03;Database=calpass;Trusted_Connection=true";
            SchemaName = "dbo";

            SqlCommand etlCommand = null;


            // get first line of file
            string line;

            using (StreamReader sr = new StreamReader(FilePath))
            {
                line = sr.ReadLine() ?? "";
            }

            char qualifier = '"';

            var strings = line.QualifySplit((char)Delimiter, qualifier);

            if (strings.Length == 29)
            {
                Entity = new FosterYouthEnrollmentCounty();
            }
            else if (strings.Length == 27)
            {
                Entity = new FosterYouthEnrollmentCountyShort();
            }
            else if (strings.Length == 26)
            {
                Entity = new FosterYouthEnrollmentSchool();
            }
            else if (strings.Length == 24)
            {
                Entity = new FosterYouthEnrollmentSchoolShort();
            }
            else
            {
                fileDrop.Status = "Failed";
                fileDrop.ErrorMessage = "Could not determine Foster Youth file type";
                return;
            }

            TableName = "FosterYouthEnrollments";
            StageName = Entity.GetType().Name.Substring(0, Math.Min(Entity.GetType().Name.Length, 107)) + DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
            FullTableName = string.Format("[{0}].[{1}]", SchemaName, TableName);
            FullStageName = string.Format("[{0}].[{1}]", SchemaName, StageName);

            etlCommand = new SqlCommand(
                "[dbo].[SimpleMerge]",
                new SqlConnection(ConnectionString)
            );
            etlCommand.CommandType = CommandType.StoredProcedure;
            etlCommand.CommandTimeout = 0;
            etlCommand.Parameters.Add(
                new SqlParameter()
                {
                    ParameterName = "TargetName",
                    Direction = ParameterDirection.Input,
                    SqlDbType = SqlDbType.NVarChar,
                    Size = 517 * 2,
                    Value = FullTableName
                }
            );
            etlCommand.Parameters.Add(
                new SqlParameter()
                {
                    ParameterName = "SourceName",
                    Direction = ParameterDirection.Input,
                    SqlDbType = SqlDbType.NVarChar,
                    Size = 517 * 2,
                    Value = FullStageName
                }
            );

            EtlCommand = new List<SqlCommand>();
            EtlCommand.Add(etlCommand);

            // assign properties
            FunctionMap = SqlBulkCopyExt.TextToTypeDictionary(Entity.GetType());
        }
    }
}
