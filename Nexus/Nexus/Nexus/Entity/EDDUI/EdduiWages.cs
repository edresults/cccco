﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.EDDUI
{
    class EdduiWages
    {
        [ColumnMapping(0, 0)]
        [ColumnMetaData("StudentIdSsnEnc", SqlDbType.Binary, 64, false)]
        public string StudentIdSsnEnc { get; set; }
        [ColumnMapping(1, 1)]
        [ColumnMetaData("YearId", SqlDbType.SmallInt, false)]
        public string YearId { get; set; }
        [ColumnMapping(2, 2)]
        [ColumnMetaData("QtrId", SqlDbType.TinyInt, false)]
        public string QtrId { get; set; }
        [ColumnMapping(3, 3)]
        [ColumnMetaData("Wages", SqlDbType.Int, true)]
        public string Wages { get; set; }
        [ColumnMapping(4, 4)]
        [ColumnMetaData("Naics", SqlDbType.Char, 6, true)]
        public string Naics { get; set; }
    }
}
