﻿using Microsoft.SqlServer.Server;
using Nexus.CustomAttribute;
using Nexus.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace Nexus.Entity.EDDUI
{
    public class Eddui : FileDetail
    {
        public Eddui(FileDrop fileDrop)
        {
            FilePath = fileDrop.TempFileName;
            Delimiter = '\t';
            HasFieldsEnclosedInQuotes = false;
            HasHeader = true;
            HasRecordNumber = false;
            HasValidation = false;
            ConnectionString = "Server=PRO-DAT-SQL-03;Database=calpass;Trusted_Connection=true";
            SchemaName = "dbo";

            SqlCommand etlCommand = null;

            Entity = new EdduiWages();
            TableName = "Eddui";

            StageName = TableName.Substring(0, Math.Min(TableName.Length, 107)) + DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
            FullTableName = string.Format("[{0}].[{1}]", SchemaName, TableName);
            FullStageName = string.Format("[{0}].[{1}]", SchemaName, StageName);

            etlCommand = new SqlCommand(
                string.Format("[{0}].[{1}]", SchemaName, "GenericSwitch"),
                new SqlConnection(ConnectionString)
            );
            etlCommand.CommandType = CommandType.StoredProcedure;
            etlCommand.CommandTimeout = 0;
            etlCommand.Parameters.Add(
                new SqlParameter()
                {
                    ParameterName = "SourceSchemaTableName",
                    Direction = ParameterDirection.Input,
                    SqlDbType = SqlDbType.NVarChar,
                    Size = 517 * 2,
                    Value = FullTableName
                }
            );
            etlCommand.Parameters.Add(
                new SqlParameter()
                {
                    ParameterName = "StageSchemaTableName",
                    Direction = ParameterDirection.Input,
                    SqlDbType = SqlDbType.NVarChar,
                    Size = 517 * 2,
                    Value = FullStageName
                }
            );

            EtlCommand = new List<SqlCommand>();
            EtlCommand.Add(etlCommand);

            // assign properties
            FunctionMap = SqlBulkCopyExt.TextToTypeDictionary(Entity.GetType());

            // Encrypt Columns
            if (Entity is EdduiWages)
            {
                FunctionMap[0] = s => SqlBulkCopyExt.Encryption(s.Trim(), Encoding.ASCII);
            }
        }
    }
}