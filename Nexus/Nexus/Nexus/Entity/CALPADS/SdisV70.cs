﻿using Nexus.CustomAttribute;
using System.Data;

namespace Nexus.Entity.CALPADS
{
    class SdisV70
    {
        [ColumnMapping(1, 0)]
        [ColumnMetaData("TransactionTypeCode", SqlDbType.VarChar, 1)]
        public string TransactionTypeCode { get; set; }
        [ColumnMapping(3, 1)]
        [ColumnMetaData("DistrictCode", SqlDbType.VarChar, 7)]
        public string DistrictCode { get; set; }
        [ColumnMapping(4, 2)]
        [ColumnMetaData("SchoolCode", SqlDbType.VarChar, 7)]
        public string SchoolCode { get; set; }
        [ColumnMapping(5, 3)]
        [ColumnMetaData("YearCode", SqlDbType.VarChar, 9)]
        public string YearCode { get; set; }
        [ColumnMapping(6, 4)]
        [ColumnMetaData("StudentStateId", SqlDbType.Char, 10)]
        public string StudentStateId { get; set; }
        [ColumnMapping(12, 5)]
        [ColumnMetaData("IncidentId", SqlDbType.VarChar, 20)]
        public string IncidentId { get; set; }
        [ColumnMapping(13, 6)]
        [ColumnMetaData("IncidentDate", SqlDbType.Char, 8)]
        public string IncidentDate { get; set; }
        [ColumnMapping(14, 7)]
        [ColumnMetaData("OffenseCode", SqlDbType.Char, 3)]
        public string OffenseCode { get; set; }
        [ColumnMapping(15, 8)]
        [ColumnMetaData("SevereOffenseCode", SqlDbType.Char, 3)]
        public string SevereOffenseCode { get; set; }
        [ColumnMapping(16, 9)]
        [ColumnMetaData("WeaponCode", SqlDbType.Char, 2)]
        public string WeaponCode { get; set; }
        [ColumnMapping(17, 10)]
        [ColumnMetaData("ActionTakenCode", SqlDbType.Char, 3)]
        public string ActionTakenCode { get; set; }
        [ColumnMapping(18, 11)]
        [ColumnMetaData("ActionAuthorityCode", SqlDbType.Char, 2)]
        public string ActionAuthorityCode { get; set; }
        [ColumnMapping(19, 12)]
        [ColumnMetaData("ActionDurationDays", SqlDbType.Decimal, 5, 2)]
        public decimal? ActionDurationDays { get; set; }
        [ColumnMapping(20, 13)]
        [ColumnMetaData("InstructionalSupport", SqlDbType.Char, 1)]
        public string InstructionalSupport { get; set; }
        [ColumnMapping(21, 14)]
        [ColumnMetaData("ModificationCategoryCode", SqlDbType.Char, 3)]
        public string ModificationCategoryCode { get; set; }
        [ColumnMapping(22, 15)]
        [ColumnMetaData("RemovalCode", SqlDbType.Char, 1)]
        public string RemovalCode { get; set; }
    }
}
