﻿using Nexus.CustomAttribute;
using System.Data;

namespace Nexus.Entity.CALPADS
{
    class SenrV30
    {
        [ColumnMapping(1, 0)]
        [ColumnMetaData("TransactionTypeCode", SqlDbType.VarChar, 1)]
        public string TransactionTypeCode { get; set; }
        [ColumnMapping(3, 1)]
        [ColumnMetaData("DistrictCode", SqlDbType.VarChar, 7)]
        public string DistrictCode { get; set; }
        [ColumnMapping(4, 2)]
        [ColumnMetaData("SchoolCode", SqlDbType.VarChar, 7)]
        public string SchoolCode { get; set; }
        [ColumnMapping(5, 3)]
        [ColumnMetaData("SchoolNpsCode", SqlDbType.VarChar, 7)]
        public string SchoolNpsCode { get; set; }
        [ColumnMapping(6, 4)]
        [ColumnMetaData("YearCode", SqlDbType.VarChar, 9)]
        public string YearCode { get; set; }
        [ColumnMapping(7, 5)]
        [ColumnMetaData("StudentStateId", SqlDbType.Char, 10)]
        public string StudentStateId { get; set; }
        [ColumnMapping(22, 6)]
        [ColumnMetaData("EnrollStartDate", SqlDbType.Char, 8)]
        public string EnrollStartDate { get; set; }
        [ColumnMapping(23, 7)]
        [ColumnMetaData("EnrollStatusCode", SqlDbType.Char, 2)]
        public string EnrollStatusCode { get; set; }
        [ColumnMapping(24, 8)]
        [ColumnMetaData("GradeCode", SqlDbType.Char, 2)]
        public string GradeCode { get; set; }
        [ColumnMapping(25, 9)]
        [ColumnMetaData("EnrollExitDate", SqlDbType.Char, 8)]
        public string EnrollExitDate { get; set; }
        [ColumnMapping(26, 10)]
        [ColumnMetaData("EnrollExitCode", SqlDbType.Char, 4)]
        public string EnrollExitCode { get; set; }
        [ColumnMapping(27, 11)]
        [ColumnMetaData("CompletionStatusCode", SqlDbType.Char, 3)]
        public string CompletionStatusCode { get; set; }
        [ColumnMapping(28, 12)]
        [ColumnMetaData("ExpectedReceiver", SqlDbType.Char, 7)]
        public string ExpectedReceiver { get; set; }
        [ColumnMapping(29, 13)]
        [ColumnMetaData("IsUniversityReady", SqlDbType.Char, 1)]
        public string IsUniversityReady { get; set; }
    }
}
