﻿using Nexus.CustomAttribute;
using System.Data;

namespace Nexus.Entity.CALPADS
{
    public class CrscV30
    {
        [ColumnMapping(1, 0)]
        [ColumnMetaData("TransactionTypeCode", SqlDbType.Char, 1)]
        public string TransactionTypeCode { get; set; }
        [ColumnMapping(3, 1)]
        [ColumnMetaData("DistrictCode", SqlDbType.Char, 7)]
        public string DistrictCode { get; set; }
        [ColumnMapping(4, 2)]
        [ColumnMetaData("SchoolCode", SqlDbType.Char, 7)]
        public string SchoolCode { get; set; }
        [ColumnMapping(5, 3)]
        [ColumnMetaData("YearCode", SqlDbType.Char, 9)]
        public string YearCode { get; set; }
        [ColumnMapping(6, 4)]
        [ColumnMetaData("CourseCode", SqlDbType.Char, 4)]
        public string CourseCode { get; set; }
        [ColumnMapping(7, 5)]
        [ColumnMetaData("CourseId", SqlDbType.VarChar, 10)]
        public string CourseId { get; set; }
        [ColumnMapping(8, 6)]
        [ColumnMetaData("CourseName", SqlDbType.VarChar, 50)]
        public string CourseName { get; set; }
        [ColumnMapping(9, 7)]
        [ColumnMetaData("CourseContentCode", SqlDbType.Char, 3)]
        public string CourseContentCode { get; set; }
        [ColumnMapping(10, 8)]
        [ColumnMetaData("CourseLevelCode", SqlDbType.Char, 1)]
        public string CourseLevelCode { get; set; }
        [ColumnMapping(11, 9)]
        [ColumnMetaData("IsCte", SqlDbType.Char, 1)]
        public string IsCte { get; set; }
        [ColumnMapping(12, 10)]
        [ColumnMetaData("IsUniversityApproved", SqlDbType.Char, 1)]
        public string IsUniversityApproved { get; set; }
        [ColumnMapping(13, 11)]
        [ColumnMetaData("SectionId", SqlDbType.VarChar, 10)]
        public string SectionId { get; set; }
        [ColumnMapping(14, 12)]
        [ColumnMetaData("TermCode", SqlDbType.Char, 2)]
        public string TermCode { get; set; }
        [ColumnMapping(15, 13)]
        [ColumnMetaData("EmployeeStateId", SqlDbType.Char, 10)]
        public string EmployeeStateId { get; set; }
        [ColumnMapping(16, 14)]
        [ColumnMetaData("EmployeeLocalId", SqlDbType.VarChar, 10)]
        public string EmployeeLocalId { get; set; }
        [ColumnMapping(17, 15)]
        [ColumnMetaData("ClassId", SqlDbType.VarChar, 20)]
        public string ClassId { get; set; }
        [ColumnMapping(18, 16)]
        [ColumnMetaData("SectionLevelCode", SqlDbType.Char, 2)]
        public string SectionLevelCode { get; set; }
        [ColumnMapping(19, 17)]
        [ColumnMetaData("ServiceCode", SqlDbType.Char, 1)]
        public string ServiceCode { get; set; }
        [ColumnMapping(20, 18)]
        [ColumnMetaData("InstructionLanguage", SqlDbType.Char, 2)]
        public string InstructionLanguage { get; set; }
        [ColumnMapping(21, 19)]
        [ColumnMetaData("StrategyCode", SqlDbType.Char, 3)]
        public string StrategyCode { get; set; }
        [ColumnMapping(22, 20)]
        [ColumnMetaData("IsIndependentStudy", SqlDbType.Char, 1)]
        public string IsIndependentStudy { get; set; }
        [ColumnMapping(23, 21)]
        [ColumnMetaData("IsDistanceLearning", SqlDbType.Char, 1)]
        public string IsDistanceLearning { get; set; }
        [ColumnMapping(24, 22)]
        [ColumnMetaData("MultipleTeacherCode", SqlDbType.Char, 1)]
        public string MultipleTeacherCode { get; set; }
        [ColumnMapping(25, 23)]
        [ColumnMetaData("ProgramFundingSource", SqlDbType.Char, 3)]
        public string ProgramFundingSource { get; set; }
        [ColumnMapping(26, 24)]
        [ColumnMetaData("CteCourseSectionProviderCode", SqlDbType.Char, 1)]
        public string CteCourseSectionProviderCode { get; set; }
        [ColumnMapping(27, 25)]
        [ColumnMetaData("CompetencyCode", SqlDbType.Char, 1)]
        public string CompetencyCode { get; set; }
    }
}
