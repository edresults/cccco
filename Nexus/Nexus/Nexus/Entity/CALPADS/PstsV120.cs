﻿using Nexus.CustomAttribute;
using System.Data;

namespace Nexus.Entity.CALPADS
{
    class PstsV120
    {
        // [ColumnMapping(0,)]
        // [ColumnMetaData("RecordTypeCode", SqlDbType.Char, 4, false)]
        // public string RecordTypeCode { get; set; }
        // [ColumnMapping(1,)]
        // [ColumnMetaData("TransactionTypeCode", SqlDbType.Char, 1, false)]
        // public string TransactionTypeCode { get; set; }
        // [ColumnMapping(2,)]
        // [ColumnMetaData("LocalRecordID", SqlDbType.Char, 255, false)]
        // public string LocalRecordID { get; set; }
        [ColumnMapping(3, 0)]
        [ColumnMetaData("ReportingLEA", SqlDbType.Char, 7, true)] // FALSE! FALSE! FALSE!
        public string ReportingLEA { get; set; }
        [ColumnMapping(4, 1)]
        [ColumnMetaData("SchoolofAttendance", SqlDbType.Char, 7, true)]
        public string SchoolofAttendance { get; set; }
        [ColumnMapping(5, 2)]
        [ColumnMetaData("SchoolofAttendanceNPS", SqlDbType.Char, 7, true)]
        public string SchoolofAttendanceNPS { get; set; }
        [ColumnMapping(6, 3)]
        [ColumnMetaData("AcademicYearID", SqlDbType.Char, 9, true)]
        public string AcademicYearID { get; set; }
        [ColumnMapping(7, 4)]
        [ColumnMetaData("SSID", SqlDbType.Char, 10, true)] // FALSE! FALSE! FALSE!
        public string SSID { get; set; }
        //[ColumnMapping(8,)]
        //[ColumnMetaData("LocalSpecialEducationStudentID", SqlDbType.VarChar, 16, false)]
        //public string LocalStudentID { get; set; }
        [ColumnMapping(9, 5)]
        [ColumnMetaData("ReportingSELPA", SqlDbType.Char, 4, true)] // FALSE! FALSE! FALSE!
        public string ReportingSELPA { get; set; }
        [ColumnMapping(10, 6)]
        [ColumnMetaData("EducationProgramParticipationTypeCode", SqlDbType.Char, 2, true)]
        public string EducationProgramParticipationTypeCode { get; set; }
        [ColumnMapping(11, 7)]
        [ColumnMetaData("PostsecondaryStatusCode", SqlDbType.Char, 3, true)]
        public string PostsecondaryStatusCode { get; set; }
        [ColumnMapping(12, 8)]
        [ColumnMetaData("EducationalInstitutionType", SqlDbType.Char, 2, true)]
        public string EducationalInstitutionType { get; set; }
        [ColumnMapping(13, 9)]
        [ColumnMetaData("IndustryField", SqlDbType.Char, 3, true)]
        public string IndustryField { get; set; }
        [ColumnMapping(14, 10)]
        [ColumnMetaData("PostHighSchoolCredential", SqlDbType.Char, 2, true)]
        public string PostHighSchoolCredential { get; set; }
    }
}
