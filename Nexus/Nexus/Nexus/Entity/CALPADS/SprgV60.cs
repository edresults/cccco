﻿using Nexus.CustomAttribute;
using System.Data;

namespace Nexus.Entity.CALPADS
{
    class SprgV60
    {
        [ColumnMapping(1, 0)]
        [ColumnMetaData("TransactionTypeCode", SqlDbType.VarChar, 1)]
        public string TransactionTypeCode { get; set; }
        [ColumnMapping(3, 1)]
        [ColumnMetaData("DistrictCode", SqlDbType.VarChar, 7)]
        public string DistrictCode { get; set; }
        [ColumnMapping(4, 2)]
        [ColumnMetaData("SchoolCode", SqlDbType.VarChar, 7)]
        public string SchoolCode { get; set; }
        [ColumnMapping(5, 3)]
        [ColumnMetaData("YearCode", SqlDbType.VarChar, 9)]
        public string YearCode { get; set; }
        [ColumnMapping(6, 4)]
        [ColumnMetaData("StudentStateId", SqlDbType.Char, 10)]
        public string StudentStateId { get; set; }
        [ColumnMapping(12, 5)]
        [ColumnMetaData("ProgramCode", SqlDbType.Char, 3)]
        public string ProgramCode { get; set; }
        [ColumnMapping(13, 6)]
        [ColumnMetaData("MembershipCode", SqlDbType.Char, 1)]
        public string MembershipCode { get; set; }
        [ColumnMapping(14, 7)]
        [ColumnMetaData("MembershipStartDate", SqlDbType.Char, 8)]
        public string MembershipStartDate { get; set; }
        [ColumnMapping(15, 8)]
        [ColumnMetaData("MembershipEndDate", SqlDbType.Char, 8)]
        public string MembershipEndDate { get; set; }
        [ColumnMapping(16, 9)]
        [ColumnMetaData("ServiceYearCode", SqlDbType.Char, 9)]
        public string ServiceYearCode { get; set; }
        [ColumnMapping(17, 10)]
        [ColumnMetaData("ServiceCode", SqlDbType.Char, 2)]
        public string ServiceCode { get; set; }
        [ColumnMapping(18, 11)]
        [ColumnMetaData("AcademyId", SqlDbType.Char, 5)]
        public string AcademyId { get; set; }
        [ColumnMapping(19, 12)]
        [ColumnMetaData("MigrantId", SqlDbType.Char, 11)]
        public string MigrantId { get; set; }
        [ColumnMapping(20, 13)]
        [ColumnMetaData("DisabilityCode", SqlDbType.Char, 3)]
        public string DisabilityCode { get; set; }
        [ColumnMapping(21, 14)]
        [ColumnMetaData("AccountabilityDistrictCode", SqlDbType.Char, 7)]
        public string AccountabilityDistrictCode { get; set; }
        [ColumnMapping(22, 15)]
        [ColumnMetaData("DwellingCode", SqlDbType.Char, 3)]
        public string DwellingCode { get; set; }
        [ColumnMapping(23, 16)]
        [ColumnMetaData("IsHomeless", SqlDbType.Char, 1)]
        public string IsHomeless { get; set; }
        [ColumnMapping(24, 17)]
        [ColumnMetaData("IsRunaway", SqlDbType.Char, 1)]
        public string IsRunaway { get; set; }
    }
}
