﻿using Nexus.CustomAttribute;
using System.Data;

namespace Nexus.Entity.CALPADS
{
    class SincV120
    {
        // [ColumnMapping(0,)]
        // [ColumnMetaData("RecordTypeCode", SqlDbType.Char, 4, false)]
        // public string RecordTypeCode { get; set; }
        // [ColumnMapping(1,)]
        // [ColumnMetaData("TransactionTypeCode", SqlDbType.Char, 1, false)]
        // public string TransactionTypeCode { get; set; }
        // [ColumnMapping(2,)]
        // [ColumnMetaData("LocalRecordID", SqlDbType.Char, 255, false)]
        // public string LocalRecordID { get; set; }
        [ColumnMapping(3, 0)]
        [ColumnMetaData("ReportingLEA", SqlDbType.Char, 7, false)]
        public string ReportingLEA { get; set; }
        [ColumnMapping(4, 1)]
        [ColumnMetaData("SchoolofAttendance", SqlDbType.Char, 7, false)]
        public string SchoolofAttendance { get; set; }
        [ColumnMapping(5, 2)]
        [ColumnMetaData("AcademicYearID", SqlDbType.Char, 9, false)]
        public string AcademicYearID { get; set; }
        [ColumnMapping(6, 3)]
        [ColumnMetaData("SSID", SqlDbType.Char, 10, false)]
        public string SSID { get; set; }
        // [ColumnMapping(7,)]
        // [ColumnMetaData("LocalStudentID", SqlDbType.Char, 15, false)]
        // public string LocalStudentID { get; set; }
        // [ColumnMapping(8,)]
        // [ColumnMetaData("StudentLegalFirstName", SqlDbType.Char, 30, false)]
        // public string StudentLegalFirstName { get; set; }
        // [ColumnMapping(9,)]
        // [ColumnMetaData("StudentLegalLastName", SqlDbType.Char, 50, false)]
        // public string StudentLegalLastName { get; set; }
        // [ColumnMapping(10,)]
        // [ColumnMetaData("StudentBirthDate", SqlDbType.Char, 8, false)]
        // public string StudentBirthDate { get; set; }
        // [ColumnMapping(11,)]
        // [ColumnMetaData("StudentGenderCode", SqlDbType.Char, 1, false)]
        // public string StudentGenderCode { get; set; }
        [ColumnMapping(12, 4)]
        [ColumnMetaData("IncidentIDLocal", SqlDbType.VarChar, 20, false)]
        public string IncidentIDLocal { get; set; }
        [ColumnMapping(13, 5)]
        [ColumnMetaData("IncidentOccurrenceDate", SqlDbType.Char, 8, false)]
        public string IncidentOccurrenceDate { get; set; }
        [ColumnMapping(14, 6)]
        [ColumnMetaData("StatutoryOffenseIndicator", SqlDbType.Char, 1, false)]
        public string StatutoryOffenseIndicator { get; set; }
        [ColumnMapping(15, 7)]
        [ColumnMetaData("StudentInstructionalSupportIndicator", SqlDbType.Char, 1, true)]
        public string StudentInstructionalSupportIndicator { get; set; }
        [ColumnMapping(16, 8)]
        [ColumnMetaData("RemovaltoInterimAlternativeSettingReasonCode", SqlDbType.Char, 1, true)]
        public string RemovaltoInterimAlternativeSettingReasonCode { get; set; }
    }
}
