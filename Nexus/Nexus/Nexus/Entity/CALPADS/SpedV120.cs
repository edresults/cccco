﻿using Nexus.CustomAttribute;
using System;
using System.Data;

namespace Nexus.Entity.CALPADS
{
    class SpedV120
    {
        // [ColumnMapping(0,)]
        // [ColumnMetaData("RecordTypeCode", SqlDbType.Char, 4, false)]
        // public string RecordTypeCode { get; set; }
        // [ColumnMapping(1,)]
        // [ColumnMetaData("TransactionTypeCode", SqlDbType.Char, 1, false)]
        // public string TransactionTypeCode { get; set; }
        // [ColumnMapping(2,)]
        // [ColumnMetaData("LocalRecordID", SqlDbType.Char, 255, false)]
        // public string LocalRecordID { get; set; }
        [ColumnMapping(3, 0)]
        [ColumnMetaData("ReportingLEA", SqlDbType.Char, 7, false)]
        public string ReportingLEA { get; set; }
        [ColumnMapping(4, 1)]
        [ColumnMetaData("SchoolofAttendance", SqlDbType.Char, 7, true)]
        public string SchoolofAttendance { get; set; }
        [ColumnMapping(5, 2)]
        [ColumnMetaData("SchoolofAttendanceNPS", SqlDbType.Char, 7, true)]
        public string SchoolofAttendanceNPS { get; set; }
        [ColumnMapping(6, 3)]
        [ColumnMetaData("AcademicYearID", SqlDbType.Char, 9, true)]
        public string AcademicYearID { get; set; }
        [ColumnMapping(7, 4)]
        [ColumnMetaData("SSID", SqlDbType.Char, 10, false)]
        public string SSID { get; set; }
        // [ColumnMapping(8,)]
        // [ColumnMetaData("LocalStudentID", SqlDbType.Char, 15, false)]
        // public string LocalStudentID { get; set; }
         //[ColumnMapping(9,)]
         //[ColumnMetaData("LocalSpecialEducationStudentID", SqlDbType.VarChar, 16, false)]
         //public string LocalStudentID { get; set; }
        // [ColumnMapping(10,)]
        // [ColumnMetaData("StudentLegalFirstName", SqlDbType.Char, 30, false)]
        // public string StudentLegalFirstName { get; set; }
        // [ColumnMapping(11,)]
        // [ColumnMetaData("StudentLegalLastName", SqlDbType.Char, 50, false)]
        // public string StudentLegalLastName { get; set; }
        // [ColumnMapping(12,)]
        // [ColumnMetaData("StudentBirthDate", SqlDbType.Char, 8, false)]
        // public string StudentBirthDate { get; set; }
        // [ColumnMapping(13,)]
        // [ColumnMetaData("StudentGenderCode", SqlDbType.Char, 1, false)]
        // public string StudentGenderCode { get; set; }
        [ColumnMapping(14, 5)]
        [ColumnMetaData("ReportingSELPA", SqlDbType.Char, 4, false)]
        public string ReportingSELPA { get; set; }
        [ColumnMapping(15, 6)]
        [ColumnMetaData("DistrictOfSpecialEducationAccountability", SqlDbType.Char, 7, false)]
        public string DistrictOfSpecialEducationAccountability { get; set; }
        [ColumnMapping(16, 7)]
        [ColumnMetaData("SpecialEducationReferralDate", SqlDbType.Date, 8, true, "yyyyMMdd")]
        public DateTime? SpecialEducationReferralDate { get; set; }
        [ColumnMapping(17, 8)]
        [ColumnMetaData("ReferringPartyCode", SqlDbType.Char, 2, true)]
        public string ReferringPartyCode { get; set; }
        [ColumnMapping(18, 9)]
        [ColumnMetaData("InitialEvaluationParentalConsentDate", SqlDbType.Date, 8, true, "yyyyMMdd")]
        public DateTime? InitialEvaluationParentalConsentDate { get; set; }
        [ColumnMapping(19, 10)]
        [ColumnMetaData("SpecialEducationMeetingTypeCode", SqlDbType.Char, 2, true)]
        public string SpecialEducationMeetingTypeCode { get; set; }
        [ColumnMapping(20, 11)]
        [ColumnMetaData("SpecialEducationMeetingDate", SqlDbType.Date, 8, true, "yyyyMMdd")]
        public DateTime? SpecialEducationMeetingDate { get; set; }
        [ColumnMapping(21, 12)]
        [ColumnMetaData("StudentSpecialEducationMeetingOrAmendmentIdentifier", SqlDbType.VarChar, 28, true)]
        public string StudentSpecialEducationMeetingOrAmendmentIdentifier { get; set; }
        [ColumnMapping(22, 13)]
        [ColumnMetaData("MeetingDelayCode", SqlDbType.Char, 2, true)]
        public string MeetingDelayCode { get; set; }
        [ColumnMapping(23, 14)]
        [ColumnMetaData("EducationPlanTypeCode", SqlDbType.Char, 3, false)]
        public string EducationPlanTypeCode { get; set; }
        [ColumnMapping(24, 15)]
        [ColumnMetaData("EducationPlanAmendmentDate", SqlDbType.Date, 8, true, "yyyyMMdd")]
        public DateTime? EducationPlanAmendmentDate { get; set; }
        [ColumnMapping(25, 16)]
        [ColumnMetaData("PrimaryResidenceCode", SqlDbType.Char, 3, true)]
        public string PrimaryResidenceCode { get; set; }
        [ColumnMapping(26, 17)]
        [ColumnMetaData("SpecialEducationInitialEntryStartDate", SqlDbType.Date, 8, true, "yyyyMMdd")]
        public DateTime? SpecialEducationInitialEntryStartDate { get; set; }
        [ColumnMapping(27, 18)]
        [ColumnMetaData("Disability1Code", SqlDbType.Char, 3, true)]
        public string Disability1Code { get; set; }
        [ColumnMapping(28, 19)]
        [ColumnMetaData("Disability2Code", SqlDbType.Char, 3, true)]
        public string Disability2Code { get; set; }
        [ColumnMapping(29, 20)]
        [ColumnMetaData("InfantRegionalCenterServicesEligibilityIndicator", SqlDbType.Char, 1, true)]
        public string InfantRegionalCenterServicesEligibilityIndicator { get; set; }
        [ColumnMapping(30, 21)]
        [ColumnMetaData("SpecialEducationProgramSettingCode", SqlDbType.Char, 3, true)]
        public string SpecialEducationProgramSettingCode { get; set; }
        [ColumnMapping(31, 22)]
        [ColumnMetaData("PreschoolProgramSettingServiceLocationCode", SqlDbType.Char, 1, true)]
        public string PreschoolProgramSettingServiceLocationCode { get; set; }
        [ColumnMapping(32, 23)]
        [ColumnMetaData("TenOrMoreWeeklyHoursInSettingIndicator", SqlDbType.Char, 1, true)]
        public string TenOrMoreWeeklyHoursInSettingIndicator { get; set; }
        [ColumnMapping(33, 24)]
        [ColumnMetaData("GeneralEducationParticipationPercentageRangeCode", SqlDbType.Char, 1, true)]
        public string GeneralEducationParticipationPercentageRangeCode { get; set; }
        [ColumnMapping(34, 25)]
        [ColumnMetaData("SpecialEducationProgramTypeCode", SqlDbType.Char, 3, true)]
        public string SpecialEducationProgramTypeCode { get; set; }
        [ColumnMapping(35, 26)]
        [ColumnMetaData("IEPIncludesPostsecondaryGoalsIndicator", SqlDbType.Char, 1, true)]
        public string IEPIncludesPostsecondaryGoalsIndicator { get; set; }
        [ColumnMapping(36, 27)]
        [ColumnMetaData("PostsecondaryGoalsUpdatedAnnuallyIndicator", SqlDbType.Char, 1, true)]
        public string PostsecondaryGoalsUpdatedAnnuallyIndicator { get; set; }
        [ColumnMapping(37, 28)]
        [ColumnMetaData("PostsecondaryGoalsAgeAppropriateTransitionAssessmentIndicator", SqlDbType.Char, 1, true)]
        public string PostsecondaryGoalsAgeAppropriateTransitionAssessmentIndicator { get; set; }
        [ColumnMapping(38, 29)]
        [ColumnMetaData("TransitionServicesInIEPIndicator", SqlDbType.Char, 1, true)]
        public string TransitionServicesInIEPIndicator { get; set; }
        [ColumnMapping(39, 30)]
        [ColumnMetaData("SupportiveServicesIndicator", SqlDbType.Char, 1, true)]
        public string SupportiveServicesIndicator { get; set; }
        [ColumnMapping(40, 31)]
        [ColumnMetaData("TransitionServicesGoalsInIEPIndicator", SqlDbType.Char, 1, true)]
        public string TransitionServicesGoalsInIEPIndicator { get; set; }
        [ColumnMapping(41, 32)]
        [ColumnMetaData("StudentIEPParticipationIndicator", SqlDbType.Char, 1, true)]
        public string StudentIEPParticipationIndicator { get; set; }
        [ColumnMapping(42, 33)]
        [ColumnMetaData("AgencyRepresentativeIEPParticipationCode", SqlDbType.Char, 2, true)]
        public string AgencyRepresentativeIEPParticipationCode { get; set; }
        [ColumnMapping(43, 34)]
        [ColumnMetaData("SpecialTransportationIndicator", SqlDbType.Char, 1, true)]
        public string SpecialTransportationIndicator { get; set; }
        [ColumnMapping(44, 35)]
        [ColumnMetaData("ParentalInvolvementFacilitationCode", SqlDbType.Char, 2, true)]
        public string ParentalInvolvementFacilitationCode { get; set; }
        [ColumnMapping(45, 36)]
        [ColumnMetaData("SpecialEducationProgramExitDate", SqlDbType.Date, 8, true, "yyyyMMdd")]
        public DateTime? SpecialEducationProgramExitDate { get; set; }
        //[ColumnMapping(46)]
        //[ColumnMetaData("Filler", SqlDbType.Char, 1, false)]
        //public DateTime? Filler { get; set; }
        //[ColumnMapping(47)]
        //[ColumnMetaData("Filler", SqlDbType.Char, 1, false)]
        //public DateTime? Filler { get; set; }
        [ColumnMapping(48, 37)]
        [ColumnMetaData("SpecialEducationProgramExitReasonCode", SqlDbType.Char, 2, true)]
        public string SpecialEducationProgramExitReasonCode { get; set; }
    }
}
