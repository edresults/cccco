﻿using Nexus.CustomAttribute;
using System.Data;

namespace Nexus.Entity.CALPADS
{
    class StasV86
    {
        [ColumnMapping(1, 0)]
        [ColumnMetaData("TransactionTypeCode", SqlDbType.Char, 1)]
        public string TransactionTypeCode { get; set; }
        [ColumnMapping(3, 1)]
        [ColumnMetaData("DistrictCode", SqlDbType.Char, 7)]
        public string DistrictCode { get; set; }
        [ColumnMapping(4, 2)]
        [ColumnMetaData("SchoolCode", SqlDbType.Char, 7)]
        public string SchoolCode { get; set; }
        [ColumnMapping(5, 3)]
        [ColumnMetaData("YearCode", SqlDbType.Char, 9)]
        public string YearCode { get; set; }
        [ColumnMapping(6, 4)]
        [ColumnMetaData("StudentStateId", SqlDbType.VarChar, 10)]
        public string StudentStateId { get; set; }
        [ColumnMapping(12, 5)]
        [ColumnMetaData("IsExempt", SqlDbType.Char, 1)]
        public string IsExempt { get; set; }
        [ColumnMapping(13, 6)]
        [ColumnMetaData("IsHourly", SqlDbType.Char, 1)]
        public string IsHourly { get; set; }
        [ColumnMapping(14, 7)]
        [ColumnMetaData("ExpectedAttenanceDays", SqlDbType.Decimal, 5, 2)]
        public decimal? ExpectedAttenanceDays { get; set; }
        [ColumnMapping(15, 8)]
        [ColumnMetaData("AttendedDays", SqlDbType.Decimal, 5, 2)]
        public decimal? AttendedDays { get; set; }
        [ColumnMapping(16, 9)]
        [ColumnMetaData("SuspendedDays", SqlDbType.Decimal, 5, 2)]
        public decimal? SuspendedDays { get; set; }
        [ColumnMapping(17, 10)]
        [ColumnMetaData("ExclusionDays", SqlDbType.Decimal, 5, 2)]
        public decimal? ExclusionDays { get; set; }
        [ColumnMapping(18, 11)]
        [ColumnMetaData("ExcusedDays", SqlDbType.Decimal, 5, 2)]
        public decimal? ExcusedDays { get; set; }
        [ColumnMapping(19, 12)]
        [ColumnMetaData("UnexcusedDays", SqlDbType.Decimal, 5, 2)]
        public decimal? UnexcusedDays { get; set; }
        [ColumnMapping(20, 13)]
        [ColumnMetaData("IncompleteDays", SqlDbType.Decimal, 5, 2)]
        public decimal? IncompleteDays { get; set; }
    }
}
