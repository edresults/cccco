﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Nexus.Entity
{
    public class FileDropTag
    {
        public int? SubmissionFileId { get; set; }
        public string Attribute { get; set; }
        public object Value { get; set; }
        public DateTime? CreateDateTime { get; set; }
        public DateTime? ModifyDateTime { get; set; }

        private const string connectionString = "Server=PRO-DAT-SQL-01;Database=cms_production_calpass;Trusted_Connection=true";
        private const string cmdTextPatch = "dbo.FileDropTagPatch";
        private readonly SqlMetaData[] sqlMetaData = new SqlMetaData[]
        {
            new SqlMetaData("SubmissionFileId", SqlDbType.Int),
            new SqlMetaData("Attribute", SqlDbType.NVarChar, 255),
            new SqlMetaData("Value", SqlDbType.Variant),
            new SqlMetaData("CreateDateTime", SqlDbType.DateTime),
            new SqlMetaData("ModifyDateTime", SqlDbType.DateTime)
        };

        public void Patch()
        {
            // initialize record
            SqlDataRecord sqlDataRecord = new SqlDataRecord(sqlMetaData);
            // populate record
            sqlDataRecord.SetValue(0, SubmissionFileId);
            sqlDataRecord.SetValue(1, Attribute);
            sqlDataRecord.SetValue(2, Value);
            sqlDataRecord.SetValue(3, CreateDateTime);
            sqlDataRecord.SetValue(4, ModifyDateTime);

            IEnumerable<SqlDataRecord> records = new SqlDataRecord[] { sqlDataRecord };

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand comm = new SqlCommand(cmdTextPatch, conn))
            {
                // create command
                comm.CommandType = CommandType.StoredProcedure;
                // add parameters
                comm.Parameters.Add(
                    new SqlParameter()
                    {
                        ParameterName = "FileDropTag",
                        SqlDbType = SqlDbType.Structured,
                        Value = records
                    }
                );
                // open connection
                comm.Connection.Open();
                // execute command
                comm.ExecuteNonQuery();
            }
        }
    }
}