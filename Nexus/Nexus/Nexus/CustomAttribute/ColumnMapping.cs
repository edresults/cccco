﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Nexus.CustomAttribute
{
    class ColumnMapping : Attribute
    {
        // property
        public int SourceColumnOrdinal { get; private set; }
        public int DestinationOrdinal { get; private set; }

        // constructor
        public ColumnMapping(int sourceColumnOrdinal, int destinationOrdinal)
        {
            SourceColumnOrdinal = sourceColumnOrdinal;
            DestinationOrdinal = destinationOrdinal;
        }

        public static IEnumerable<ColumnMapping> GetMap(Type type)
        {
            PropertyInfo[] properties = type.GetProperties(
                BindingFlags.Public |
                BindingFlags.Instance |
                BindingFlags.DeclaredOnly
            );

            for (int propertyIndex = 0, propertyLength = properties.Length; propertyIndex < propertyLength; propertyIndex++)
            {
                PropertyInfo property = properties[propertyIndex];

                // check for custom attributes
                object[] attributes = property.GetCustomAttributes(false);

                for (int attributeIndex = 0, attributeLength = attributes.Length; attributeIndex < attributeLength; attributeIndex++)
                {
                    object attribute = attributes[attributeIndex];

                    if (attribute.GetType() == typeof(ColumnMapping))
                    {
                        ColumnMapping columnMapping = (ColumnMapping)attribute;
                        yield return new ColumnMapping(columnMapping.SourceColumnOrdinal, columnMapping.DestinationOrdinal);
                    }
                }
            }
        }
    }
}
