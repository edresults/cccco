﻿using Nexus.Entity;
using Nexus.Entity.CALPADS;
using Nexus.Entity.CCFY;
using Nexus.Entity.COMIS;
using Nexus.Entity.UNIVERSITY;
using Nexus.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Nexus
{
    class Program
    {
        static void Main(string[] args)
        {
            // initialize program stopwatch
            Stopwatch psw = Stopwatch.StartNew();
            // get all new files
            List<FileDrop> files = FileDrop.GetNew();
            // log details of file
            bool comis = false;
            bool calpads = false;
            bool university = false;
            bool ccFosterYouth = false;

            int filesCount = files.Count;
            for (int filesIndex = 0; filesIndex < filesCount; filesIndex++)
            {
                FileDrop file = files[filesIndex];

                Console.WriteLine("File Index: {0} of {1}", filesIndex + 1, filesCount);
                Console.WriteLine("User Name:  {0}", file.User.FirstName);
                Console.WriteLine("User Email: {0}", file.User.Email);
                Console.WriteLine("Purpose:    {0}", file.Purpose.FileDropPurpose);
                Console.WriteLine("Org Name:   {0}", file.Organization.OrganizationName);
                Console.WriteLine("Org Code:   {0}", file.Organization.OrganizationCode);
                Console.WriteLine("File Id:    {0}", file.SubmissionFileId);
                Console.WriteLine("File Date:  {0}", file.SubmissionDateTime);
                Console.WriteLine("File Path:  {0}", file.TempFileName);
                Console.WriteLine("File Name:  {0}", file.OriginalFileName.Substring(37));
                file.Process();
                Console.WriteLine();

                if (file.SubmissionFileDescriptionId == 2) calpads = true;
                if (file.SubmissionFileDescriptionId == 11) comis = true;
                if (file.SubmissionFileDescriptionId == 6) university = true;
                if (file.SubmissionFileDescriptionId == 15) ccFosterYouth = true;
            }


#if !DEBUG

            if (calpads)
            {
                Calpads.PostProcessing();
            }

            if (university)
            {
                University.PostProcessing();
            }

            //if (ccFosterYouth)
            //{
            //    CCFosterYouth.PostProcessing();
            //}

            //if (comis)
            //{
            //    Comis.PostProcessing();
            //}
#endif

            // stop timer
            psw.Stop();
            // output total time
            Console.WriteLine();
            Console.WriteLine("Duration:   {0}", psw.Elapsed);
            Console.WriteLine("File/s:     {0}", psw.Elapsed.TotalSeconds / filesCount);
#if DEBUG
            Console.WriteLine("Press 'Enter' to quit");
            Console.ReadLine();
#endif
        }
    }
}
