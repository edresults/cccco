﻿using Nexus.CustomAttribute;
using Nexus.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace Nexus.IDataReaders
{
    class Fixed : IDataReader
    {
        // statics
        private readonly StreamReader _sr;
        private readonly INexus _iNexus;
        // file record
        private string[] _fileFieldValues;
        private long _recordCount = 0;
        private int _errorCount = 0;
        private Dictionary<int, Func<string, object>> _dictionary;
        private int[] _lengthMap = new int[] { };

        public Fixed(INexus iNexus)
        {
            _iNexus = iNexus;
            _dictionary = _iNexus.FunctionMap;
            _lengthMap = ColumnMetaData.GetLengthMap(_iNexus.Entity.GetType()).ToArray();

            // open file
            _sr = new StreamReader(_iNexus.FilePath);

            if (_iNexus.HasHeader) _sr.ReadLine();
        }

        public long ErrorCount { get { return _errorCount; } }

        public long RecordCount { get { return _recordCount; } }

        public int FieldCount { get { return _dictionary.Count; } }

        public object GetValue(int i)
        {
            // Get column value
            string fileFieldValue = _fileFieldValues[i];
            // dictionary of properties to functions
            // key: field index
            // value: convert function
            return _dictionary[i](fileFieldValue);
        }

        public bool Read()
        {
            if (IsClosed || _sr.EndOfStream) return false;

            // add to total record count
            _recordCount += 1;
            // get line
            string line = _sr.ReadLine();
            int startIndex = 0;
            int length;
            int i = 0;
            int c = FieldCount;
            // initialize container
            _fileFieldValues = new string[c];

            // populate container
            for (; i < c; i++)
            {
                length = _lengthMap[i];

                _fileFieldValues[i] = line.Substring(startIndex, length);

                startIndex += length;
            }

            return true;
        }

        public void Dispose()
        {
            _sr.Dispose();
        }

        public object this[int i]
        {
            get
            {
                return _fileFieldValues[i];
            }
        }

        #region IDataReader Members
        public object this[string name]
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public int Depth
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public bool IsClosed { get; set; }

        public int RecordsAffected
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public void Close()
        {
            Array.Clear(_fileFieldValues, 0, _fileFieldValues.Length);
            _sr.Close();
            _sr.Dispose();
            IsClosed = true;
        }
        public bool GetBoolean(int i)
        {
            throw new NotImplementedException();
        }
        public byte GetByte(int i)
        {
            throw new NotImplementedException();
        }
        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }
        public char GetChar(int i)
        {
            throw new NotImplementedException();
        }
        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }
        public IDataReader GetData(int i)
        {
            throw new NotImplementedException();
        }
        public string GetDataTypeName(int i)
        {
            throw new NotImplementedException();
        }
        public DateTime GetDateTime(int i)
        {
            throw new NotImplementedException();
        }
        public decimal GetDecimal(int i)
        {
            throw new NotImplementedException();
        }
        public double GetDouble(int i)
        {
            throw new NotImplementedException();
        }
        public Type GetFieldType(int i)
        {
            throw new NotImplementedException();
        }
        public float GetFloat(int i)
        {
            throw new NotImplementedException();
        }
        public Guid GetGuid(int i)
        {
            throw new NotImplementedException();
        }
        public short GetInt16(int i)
        {
            throw new NotImplementedException();
        }
        public int GetInt32(int i)
        {
            throw new NotImplementedException();
        }
        public long GetInt64(int i)
        {
            throw new NotImplementedException();
        }
        public string GetName(int i)
        {
            throw new NotImplementedException();
        }
        public int GetOrdinal(string name)
        {
            throw new NotImplementedException();
        }
        public DataTable GetSchemaTable()
        {
            throw new NotImplementedException();
        }
        public string GetString(int i)
        {
            throw new NotImplementedException();
        }
        public int GetValues(object[] values)
        {
            throw new NotImplementedException();
        }
        public bool IsDBNull(int i)
        {
            return this[i] is DBNull;
        }
        public bool NextResult()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}