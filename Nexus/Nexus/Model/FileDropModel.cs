﻿using System;

namespace Nexus.Model
{
    public class FileDropModel
    {
        public int SubmissionFileId { get; set; }
        public int OrganizationId { get; set; }
        public Guid BatchId { get; set; }
        public DateTime SubmissionDateTime { get; set; }
        public DateTime? ProcessedDateTime { get; set; }
        public string Status { get; set; }
        public string FileLocation { get; set; }
        public string OriginalFileName { get; set; }
        public string TempFileName { get; set; }
        public string ErrorMessage { get; set; }
        public int? UserId { get; set; }
        public string SubmissionFileSource { get; set; }
        public bool? Removed { get; set; }
        public string SubmissionFileDetails { get; set; }
        public int SubmissionFileDescriptionId { get; set; }
        public string Purpose { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string OrganizationName { get; set; }
    }
}
