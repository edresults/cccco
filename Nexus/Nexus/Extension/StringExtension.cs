﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nexus.Extension
{
    public static class StringExtension
    {
        public static String[] QualifySplit(this String text, char delimiter, char qualifier)
        {
            List<string> tokens = new List<string>();

            var stringBuilder = new StringBuilder();
            bool escaped = false;

            foreach (char c in text)
            {
                if (c.Equals(delimiter) && !escaped)
                {
                    tokens.Add(stringBuilder.ToString().Trim());
                    stringBuilder.Clear();
                }
                else if (c.Equals(delimiter) && escaped)
                {
                    stringBuilder.Append(c);
                }
                else if (c.Equals(qualifier))
                {
                    escaped = !escaped;
                    // stringBuilder.Append(c);
                }
                else
                {
                    stringBuilder.Append(c);
                }
            }

            tokens.Add(stringBuilder.ToString().Trim());

            return tokens.ToArray();
        }
    }
}