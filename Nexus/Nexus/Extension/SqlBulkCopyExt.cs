﻿using Nexus.CustomAttribute;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace Nexus.Extension
{
    public class SqlBulkCopyExt
    {
        private static string[] _format = new string[] { "yyyyMMdd", "MM-dd-yyyy", "M-dd-yyyy", "MM-d-yyyy", "M-d-yyyy", "MM/dd/yyyy", "M/dd/yyyy", "MM/d/yyyy", "M/d/yyyy" };

        public static Dictionary<int, Func<string, object>> TextToTypeDictionary(Type type)
        {
            Dictionary<int, Func<string, object>> dictionary = new Dictionary<int, Func<string, object>>();
            Func<string, object> function = null;

            PropertyInfo[] properties = type.GetProperties(
                BindingFlags.Public |
                BindingFlags.Instance |
                BindingFlags.DeclaredOnly
            );

            for (int propertyIndex = 0, propertyLength = properties.Length; propertyIndex < propertyLength; propertyIndex++)
            {
                PropertyInfo property = properties[propertyIndex];
                Type propertyType = property.PropertyType;

                // check for custom attributes
                object[] attributes = property.GetCustomAttributes(false);
                int sourceColumnOrdinal = -1;
                string format = null;

                for (int attributeIndex = 0, attributeLength = attributes.Length; attributeIndex < attributeLength; attributeIndex++)
                {
                    object attribute = attributes[attributeIndex];

                    if (attribute.GetType() == typeof(ColumnMapping))
                    {
                        ColumnMapping columnMapping = (ColumnMapping)attribute;
                        sourceColumnOrdinal = columnMapping.SourceColumnOrdinal;
                    }
                    else if (attribute.GetType() == typeof(ColumnMetaData))
                    {
                        ColumnMetaData columnMetaData = (ColumnMetaData)attribute;
                        format = columnMetaData.ColumnFormat;
                    }

                }

                if (sourceColumnOrdinal == -1)
                {
                    continue;
                }

                function = TextToTypeFunction(propertyType, format);
                dictionary.Add(sourceColumnOrdinal, function);
            }

            return dictionary;
        }

        public static Func<string, object> TextToTypeFunction(Type type, string format)
        {
            if (type == typeof(string))
            {
                return s => String.IsNullOrWhiteSpace(s) ? (object)DBNull.Value : s.Trim();
            }
            else if (type == typeof(int))
            {
                return s => Convert.ToInt32(s);
            }
            else if (type == typeof(int?))
            {
                return s => String.IsNullOrWhiteSpace(s) ? (object)DBNull.Value : Convert.ToInt32(s);
            }
            else if (type == typeof(decimal))
            {
                return s => Convert.ToDecimal(s);
            }
            else if (type == typeof(decimal?))
            {
                return s => String.IsNullOrWhiteSpace(s) ? (object)DBNull.Value : Convert.ToDecimal(s);
            }
            else if (type == typeof(long))
            {
                return s => Convert.ToInt64(s);
            }
            else if (type == typeof(long?))
            {
                return s => String.IsNullOrWhiteSpace(s) ? (object)DBNull.Value : Convert.ToInt64(s);
            }
            else if (type == typeof(byte[]))
            {
                return s => Convert.ToByte(s);
            }
            else if (type == typeof(byte?[]))
            {
                return s => String.IsNullOrWhiteSpace(s) ? (object)DBNull.Value : Convert.ToByte(s);
            }
            else if (type == typeof(bool))
            {
                return s =>
                {
                    if (s == "1")
                    {
                        return true;
                    }

                    if (s == "0")
                    {
                        return false;
                    }

                    return Convert.ToBoolean(s);
                };
            }
            else if (type == typeof(bool?))
            {
                return s =>
                {
                    if (s == "1")
                    {
                        return true;
                    }

                    if (s == "0")
                    {
                        return false;
                    }

                    return String.IsNullOrWhiteSpace(s) ? (object)DBNull.Value : Convert.ToBoolean(s);
                };
            }
            else if (type == typeof(DateTime))
            {
                return (s) =>
                {
                    if (format != null)
                    {
                        return DateTime.ParseExact(s, format, CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        return Convert.ToDateTime(s);
                    }
                };
            }
            else if (type == typeof(DateTime?))
            {
                return (s) =>
                {
                    if (String.IsNullOrWhiteSpace(s))
                    {
                        return (object)DBNull.Value;
                    }
                    else if (format != null)
                    {
                        return DateTime.ParseExact(s, format, CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        return Convert.ToDateTime(s);
                    }
                };
            }
            else if (type == typeof(double))
            {
                return s => Convert.ToDouble(s);
            }
            else if (type == typeof(double?))
            {
                return s => String.IsNullOrWhiteSpace(s) ? (object)DBNull.Value : Convert.ToDouble(s);
            }
            else if (type == typeof(float))
            {
                return s => Convert.ToSingle(s);
            }
            else if (type == typeof(float?))
            {
                return s => String.IsNullOrWhiteSpace(s) ? (object)DBNull.Value : Convert.ToSingle(s);
            }
            else if (type == typeof(short))
            {
                return s => Convert.ToInt16(s);
            }
            else if (type == typeof(short?))
            {
                return s => String.IsNullOrWhiteSpace(s) ? (object)DBNull.Value : Convert.ToInt16(s);
            }
            else if (type == typeof(byte))
            {
                return s => Convert.ToByte(s);
            }
            else if (type == typeof(byte?))
            {
                return s => String.IsNullOrWhiteSpace(s) ? (object)DBNull.Value : Convert.ToByte(s);
            }
            else if (type == typeof(Guid))
            {
                return s => s;
            }
            else if (type == typeof(Guid?))
            {
                return s => String.IsNullOrWhiteSpace(s) ? (object)DBNull.Value : s;
            }
            else if (type == typeof(object))
            {
                return s => Convert.ChangeType(s, typeof(object));
            }
            else
            {
                throw new ArgumentOutOfRangeException("SqlDbType");
            }
        }

        public static byte[] Encryption(string payload, Encoding encoding)
        {
            byte[] hash;

            using (SHA512 shaM = new SHA512Managed())
            {
                hash = shaM.ComputeHash(encoding.GetBytes(payload));
            }

            return hash;
        }

        private static readonly uint[] _lookup32 = CreateLookup32();

        private static uint[] CreateLookup32()
        {
            var result = new uint[256];
            for (int i = 0; i < 256; i++)
            {
                string s = i.ToString("X2");
                result[i] = ((uint)s[0]) + ((uint)s[1] << 16);
            }
            return result;
        }

        public static string ByteArrayToHexViaLookup32(byte[] bytes)
        {
            var lookup32 = _lookup32;
            var result = new char[bytes.Length * 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                var val = lookup32[bytes[i]];
                result[2 * i] = (char)val;
                result[2 * i + 1] = (char)(val >> 16);
            }
            return new string(result);
        }
    }
}
