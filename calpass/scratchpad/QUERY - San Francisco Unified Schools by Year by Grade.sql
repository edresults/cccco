SELECT
	d.organizationId,
	d.OrganizationName,
	d.OrganizationCode,
	o.OrganizationName,
	o.OrganizationCode,
	y.year_code,
	c.GSoffered,
	s.GradeLevel,
	count(s.GradeLevel) as records
FROM
	calpass.dbo.cdepubschools c
	inner join
	calpass.dbo.organization o
		on c.cdsCode = o.OrganizationCode
	inner join
	calpass.dbo.organization d
		on d.OrganizationId = o.ParentId
	cross join
	(
		VALUES
		('0809','2008-2009'),
		('0910','2009-2010'),
		('1011','2010-2011'),
		('1112','2011-2012'),
		('1213','2012-2013'),
		('1314','2013-2014'),
		('1415','2014-2015')
	) y (year_code, year_description)
	inner join
	(
		VALUES
		('9-12','09'),
		('9-12','10'),
		('9-12','11'),
		('9-12','12')
	) g (grade_spread, grade_level_code)
		on g.grade_spread = c.GSoffered
	left outer join
	calpass.dbo.k12studentprod s
		on s.School = o.organizationCode
		and s.AcYear = y.year_code
		and s.GradeLevel = g.grade_level_code
WHERE
	district = 'San Francisco Unified'
	and EdOpsCode = 'TRAD'
	and EILCode = 'HS'
	and StatusType = 'Active'
GROUP BY
	d.organizationId,
	d.OrganizationName,
	d.OrganizationCode,
	o.OrganizationName,
	o.OrganizationCode,
	c.GSoffered,
	y.year_code,
	s.GradeLevel
ORDER BY
	o.OrganizationCode,
	y.year_code,
	s.GradeLevel