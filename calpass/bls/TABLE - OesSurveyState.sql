USE erp;

GO

IF (object_id('bls.OesSurveyState') is not null)
	BEGIN
		DROP TABLE bls.OesSurveyState;
	END;

GO

CREATE TABLE
	bls.OesSurveyState
	(
		year_code char(4),
		area varchar(7),
		st varchar(2),
		state varchar(255),
		occ_code char(7),
		occ_title varchar(255),
		occ_group varchar(8),
		tot_emp varchar(12),
		emp_prse varchar(12),
		jobs_1000 varchar(12),
		loc_q varchar(12),
		h_mean varchar(12),
		a_mean varchar(12),
		mean_prse varchar(12),
		h_pct10 varchar(12),
		h_pct25 varchar(12),
		h_median varchar(12),
		h_pct75 varchar(12),
		h_pct90 varchar(12),
		a_pct10 varchar(12),
		a_pct25 varchar(12),
		a_median varchar(12),
		a_pct75 varchar(12),
		a_pct90 varchar(12),
		annual varchar(5),
		hourly varchar(5)
	);

GO

CREATE CLUSTERED INDEX
	ixc_OesSurveyState__year_code__area__occ_code
ON
	bls.OesSurveyState
	(
		year_code,
		area,
		occ_code
	);