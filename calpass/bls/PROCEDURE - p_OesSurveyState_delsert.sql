USE erp;

IF (object_id('bls.p_OesSurveyState_delsert') is not null)
	BEGIN
		DROP PROCEDURE bls.p_OesSurveyState_delsert;
	END;

GO

CREATE PROCEDURE
	bls.p_OesSurveyState_delsert
	(
		@StageSchemaTableName nvarchar(517),
		@YearCode char(4)
	)
AS

DECLARE
	@message nvarchar(2048),
	@validate bit = 0,
	@sql nvarchar(max),
	@tab_needle varchar(255) = char(13) + char(10) + char(9) + char(9),
	@tab_replace varchar(255) = char(13) + char(10),
	@Query nvarchar(max),
	@ColumnArray nvarchar(max) = N'';

BEGIN
	-- validate inputs
	SELECT
		@validate = 1
	FROM
		sys.schemas s
		inner join
		sys.tables t
			on s.schema_id = t.schema_id
	WHERE
		s.name = parsename(@StageSchemaTableName, 2)
		and t.name = parsename(@StageSchemaTableName, 1);
	-- process validation
	IF (@validate = 0)
		BEGIN
			SET @message = N'Table not found';
			THROW 70099, @message, 1;
		END;
	-- set query string
	SET @Query = N'SELECT * FROM ' + @StageSchemaTableName + ';';
	-- delete data from production using staging
	SET @sql = replace(N'
		DELETE
			t
		FROM
			bls.OesSurveyState t with (tablockx)
		WHERE
			t.year_code = @YearCode
			and exists (
				SELECT
					1
				FROM
					' + @StageSchemaTableName + ' s
				WHERE
					t.area = s.area
					and t.occ_code = s.occ_code
			);', @tab_needle, @tab_replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@sql,
		N'@YearCode char(4)',
		@YearCode = @YearCode;
	-- set columns
	SET @sql = replace(N'
		SELECT
			@ColumnArray += name + case when count(*) over() = row_number() over(order by (select 1)) then '''' else '','' end
		FROM
			sys.dm_exec_describe_first_result_set(@query, null, 0);', @tab_needle, @tab_replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@sql,
		N'@query nvarchar(max), @ColumnArray nvarchar(max) out',
		@query = @query,
		@ColumnArray = @ColumnArray out;
	-- set dynamic sql
	SET @sql = replace(N'
		INSERT INTO
			bls.OesSurveyState with (tablockx)
			(
				year_code,
				' + @ColumnArray + '
			)
		SELECT
			@YearCode,
			' + @ColumnArray + '
		FROM
			' + @StageSchemaTableName + ';', @tab_needle, @tab_replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@sql,
		N'@YearCode char(4)',
		@YearCode = @YearCode;
END;