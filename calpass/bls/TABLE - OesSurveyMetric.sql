USE erp;

GO

IF (object_id('bls.OesSurveyMetro') is not null)
	BEGIN
		DROP TABLE bls.OesSurveyMetro;
	END;

GO

CREATE TABLE
	bls.OesSurveyMetro
	(
		year_code char(4),
		prim_state char(2),
		area varchar(7),
		area_name varchar(255),
		occ_code char(7),
		occ_title varchar(255),
		occ_group varchar(8),
		tot_emp varchar(12),
		emp_prse varchar(12),
		jobs_1000 varchar(12),
		loc_quotient varchar(12),
		h_mean varchar(12),
		a_mean varchar(12),
		mean_prse varchar(12),
		h_pct10 varchar(12),
		h_pct25 varchar(12),
		h_median varchar(12),
		h_pct75 varchar(12),
		h_pct90 varchar(12),
		a_pct10 varchar(12),
		a_pct25 varchar(12),
		a_median varchar(12),
		a_pct75 varchar(12),
		a_pct90 varchar(12),
		annual varchar(12),
		hourly varchar(12)
	);

GO

CREATE CLUSTERED INDEX
	ixc_OesSurveyMetro__year_code__prim_state__area__occ_code
ON
	bls.OesSurveyMetro
	(
		year_code,
		prim_state,
		area,
		occ_code
	);