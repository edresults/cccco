USE calpass;

GO

IF (object_id('NCES.SchoolCharacteristic') is not null)
	BEGIN
		DROP TABLE NCES.SchoolCharacteristic;
	END;

GO

CREATE TABLE
	NCES.SchoolCharacteristic
	(
		SURVYEAR varchar(9) not null,
		FIPST varchar(2),
		STABR varchar(2),
		STATENAME varchar(44),
		SEANAME varchar(59),
		LEAID varchar(7),
		ST_LEAID varchar(14),
		LEA_NAME varchar(60),
		SCHID varchar(5),
		ST_SCHID varchar(20),
		NCESSCH varchar(12) not null,
		SCH_NAME varchar(60),
		TITLEI_TEXT varchar(63),
		TITLEI_STATUS varchar(1),
		TITLEI varchar(1),
		STITLEI varchar(1),
		SHARED_TIME varchar(7),
		MAGNET_TEXT varchar(14),
		NSLPSTATUS_TEXT varchar(57),
		NSLPSTATUS_CODE varchar(3)
	);

GO

ALTER TABLE
	NCES.SchoolCharacteristic
ADD CONSTRAINT
	pk_SchoolCharacteristic
PRIMARY KEY CLUSTERED
	(
		SURVYEAR,
		NCESSCH
	);