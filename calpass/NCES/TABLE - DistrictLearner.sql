USE calpass;

GO

IF (object_id('NCES.DistrictLearner') is not null)
	BEGIN
		DROP TABLE NCES.DistrictLearner;
	END;

GO

CREATE TABLE
	NCES.DistrictLearner
	(
		SURVYEAR varchar(9) not null,
		FIPST varchar(2),
		STABR varchar(2),
		STATENAME varchar(44),
		SEANAME varchar(59),
		LEAID varchar(7) not null,
		ST_LEAID varchar(14),
		LEA_NAME varchar(60),
		ELL int,
		IAELL varchar(14)
	);

GO

ALTER TABLE
	NCES.DistrictLearner
ADD CONSTRAINT
	pk_DistrictLearner
PRIMARY KEY CLUSTERED
	(
		SURVYEAR,
		LEAID
	);