USE calpass;

GO

IF (object_id('NCES.DistrictDirectory') is not null)
	BEGIN
		DROP TABLE NCES.DistrictDirectory;
	END;

GO

CREATE TABLE
	NCES.DistrictDirectory
	(
		SURVYEAR varchar(9) not null,
		FIPST varchar(2),
		STABR varchar(2),
		STATENAME varchar(44),
		SEANAME varchar(59),
		LEAID varchar(7) not null,
		ST_LEAID varchar(14),
		LEA_NAME varchar(60),
		MSTREET1 varchar(30),
		MSTREET2 varchar(30),
		MSTREET3 varchar(30),
		MCITY varchar(26),
		MSTATE varchar(23),
		MZIP varchar(5),
		MZIP4 varchar(4),
		PHONE varchar(10),
		LSTREET1 varchar(30),
		LSTREET2 varchar(30),
		LSTREET3 varchar(30),
		LCITY varchar(26),
		LSTATE varchar(23),
		LZIP varchar(5),
		LZIP4 varchar(4),
		LEA_TYPE_TEXT varchar(100),
		LEA_TYPE varchar(1),
		[UNION] varchar(3),
		GSLO varchar(2),
		GSHI varchar(2),
		BIEA varchar(1),
		SY_STATUS_TEXT varchar(16),
		SY_STATUS varchar(1),
		UPDATED_STATUS_TEXT varchar(16),
		UPDATED_STATUS varchar(1),
		EFFECTIVE_DATE varchar(20),
		CHARTER_LEA_TEXT varchar(72),
		AGCHRT varchar(1),
		SCH int,
		PKOFFERED varchar(1),
		KGOFFERED varchar(1),
		G1OFFERED varchar(1),
		G2OFFERED varchar(1),
		G3OFFERED varchar(1),
		G4OFFERED varchar(1),
		G5OFFERED varchar(1),
		G6OFFERED varchar(1),
		G7OFFERED varchar(1),
		G8OFFERED varchar(1),
		G9OFFERED varchar(1),
		G10OFFERED varchar(1),
		G11OFFERED varchar(1),
		G12OFFERED varchar(1),
		G13OFFERED varchar(1),
		AEOFFERED varchar(1),
		UGOFFERED varchar(1),
		NOGRADES varchar(1),
		IGOFFERED varchar(1)
	);

GO

ALTER TABLE
	NCES.DistrictDirectory
ADD CONSTRAINT
	pk_DistrictDirectory
PRIMARY KEY CLUSTERED
	(
		SURVYEAR,
		LEAID
	);