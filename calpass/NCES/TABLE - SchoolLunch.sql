USE calpass;

GO

IF (object_id('NCES.SchoolLunch') is not null)
	BEGIN
		DROP TABLE NCES.SchoolLunch;
	END;

GO

CREATE TABLE
	NCES.SchoolLunch
	(
		SURVYEAR varchar(9) not null,
		FIPST varchar(2),
		STABR varchar(2),
		STATENAME varchar(44),
		SEANAME varchar(59),
		LEAID varchar(7),
		ST_LEAID varchar(14),
		LEA_NAME varchar(60),
		SCHID varchar(5),
		ST_SCHID varchar(20),
		NCESSCH varchar(12) not null,
		SCH_NAME varchar(60),
		TOTFRL int,
		FRELCH int,
		REDLCH int,
		ISFLE varchar(14)
	);

GO

ALTER TABLE
	NCES.SchoolLunch
ADD CONSTRAINT
	pk_SchoolLunch
PRIMARY KEY CLUSTERED
	(
		SURVYEAR,
		NCESSCH
	);