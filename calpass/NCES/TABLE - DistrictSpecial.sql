USE calpass;

GO

IF (object_id('NCES.DistrictSpecial') is not null)
	BEGIN
		DROP TABLE NCES.DistrictSpecial;
	END;

GO

CREATE TABLE
	NCES.DistrictSpecial
	(
		SURVYEAR varchar(9) not null,
		FIPST varchar(2),
		STABR varchar(2),
		STATENAME varchar(44),
		SEANAME varchar(59),
		LEAID varchar(7) not null,
		ST_LEAID varchar(14),
		LEA_NAME varchar(60),
		SPECED int,
		IASPECED varchar(12)
	);

GO

ALTER TABLE
	NCES.DistrictSpecial
ADD CONSTRAINT
	pk_DistrictSpecial
PRIMARY KEY CLUSTERED
	(
		SURVYEAR,
		LEAID
	);