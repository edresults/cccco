USE calpass;

GO

IF (object_id('NCES.DistrictStaff') is not null)
	BEGIN
		DROP TABLE NCES.DistrictStaff;
	END;

GO

CREATE TABLE
	NCES.DistrictStaff
	(
		SURVYEAR varchar(9) not null,
		FIPST varchar(2),
		STABR varchar(2),
		STATENAME varchar(44),
		SEANAME varchar(59),
		LEAID varchar(7) not null,
		ST_LEAID varchar(14),
		LEA_NAME varchar(60),
		SCHSUP int,
		SECTCH int,
		STAFF int,
		ELMGUI int,
		KGTCH int,
		LEASUP int,
		PARA int,
		SECGUI int,
		CORSUP int,
		ELMTCH int,
		LIBSUP int,
		SCHADM int,
		STUSUP int,
		GUI int,
		LEAADM int,
		LIBSPE int,
		OTHSUP int,
		PKTCH int,
		UGTCH int,
		TOTTCH int,
		TOTGUI int,
		IAPARCORSUP varchar(12),
		IAFTEPUP varchar(12),
		IAGUID varchar(12),
		IALEAADM varchar(12),
		IALIBSTF varchar(12),
		IASCHADM varchar(12),
		IASUPSTF varchar(12)
	);

GO

ALTER TABLE
	NCES.DistrictStaff
ADD CONSTRAINT
	pk_DistrictStaff
PRIMARY KEY CLUSTERED
	(
		SURVYEAR,
		LEAID
	);