USE calpass;

GO

IF (object_id('NCES.SchoolProgram') is not null)
	BEGIN
		DROP TABLE NCES.SchoolProgram;
	END;

GO

CREATE TABLE
	NCES.SchoolProgram
	(
		SURVYEAR varchar(9) not null,
		FIPST varchar(2),
		STABR varchar(2),
		STATENAME varchar(44),
		SEANAME varchar(59),
		LEAID varchar(7),
		ST_LEAID varchar(14),
		LEA_NAME varchar(60),
		SCHID varchar(5),
		ST_SCHID varchar(20),
		NCESSCH varchar(12) not null,
		SCH_NAME varchar(60),
		MSTREET1 varchar(30),
		MSTREET2 varchar(30),
		MSTREET3 varchar(23),
		MCITY varchar(26),
		MSTATE varchar(23),
		MZIP varchar(5),
		MZIP4 varchar(4),
		PHONE varchar(10),
		LSTREET1 varchar(30),
		LSTREET2 varchar(30),
		LSTREET3 varchar(28),
		LCITY varchar(26),
		LSTATE varchar(23),
		LZIP varchar(5),
		LZIP4 varchar(4),
		[UNION] varchar(3),
		OUT_OF_STATE_FLAG varchar(1),
		SCH_TYPE_TEXT varchar(28),
		SCH_TYPE varchar(1),
		RECON_STATUS varchar(3),
		GSLO varchar(2),
		GSHI varchar(2),
		[LEVEL] varchar(1),
		VIRTUAL varchar(14),
		BIES varchar(1),
		SY_STATUS_TEXT varchar(14),
		SY_STATUS varchar(1),
		UPDATED_STATUS_TEXT varchar(14),
		UPDATED_STATUS varchar(1),
		EFFECTIVE_DATE varchar(20),
		CHARTER_TEXT varchar(14),
		PKOFFERED varchar(1),
		KGOFFERED varchar(1),
		G1OFFERED varchar(1),
		G2OFFERED varchar(1),
		G3OFFERED varchar(1),
		G4OFFERED varchar(1),
		G5OFFERED varchar(1),
		G6OFFERED varchar(1),
		G7OFFERED varchar(1),
		G8OFFERED varchar(1),
		G9OFFERED varchar(1),
		G10OFFERED varchar(1),
		G11OFFERED varchar(1),
		G12OFFERED varchar(1),
		G13OFFERED varchar(1),
		AEOFFERED varchar(1),
		UGOFFERED varchar(1),
		NOGRADES varchar(1),
		CHARTAUTH1 varchar(60),
		CHARTAUTHN1 varchar(14),
		CHARTAUTH2 varchar(14),
		CHARTAUTHN2 varchar(14),
		IGOFFERED varchar(1)
	);

GO

ALTER TABLE
	NCES.SchoolProgram
ADD CONSTRAINT
	pk_SchoolProgram
PRIMARY KEY CLUSTERED
	(
		SURVYEAR,
		NCESSCH
	);