﻿using System;
using System.Data.SqlClient;

namespace AB705.Helpers
{
    public class SqlExceptions
    {
        public static bool PrimaryKeyViolation(Exception e)
        {
            SqlException ex = (SqlException)e.InnerException;

            if (ex.Number == 2601 || ex.Number == 2627)
            {
                return true;
            }

            return false;
        }
    }
}
