﻿using AB705.Entities;
using AB705.Helpers;
using AB705.ViewModels;
using AutoMapper;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AB705.Data
{
    public class AB705Repository : IAB705Repository
    {
        private readonly AB705Context _context;
        private readonly ILogger<AB705Repository> _logger;
        private readonly IMapper _mapper;

        public AB705Repository(AB705Context context, ILogger<AB705Repository> logger, IMapper mapper)
        {
            _context = context;
            _logger = logger;
            _mapper = mapper;
        }

        public Placement GetPlacement (string statewideStudentId, string interSegmentKey, string californiaCommunityCollegeId)
        {
            try
            {
                TranscriptBySsid ssid = null;
                TranscriptByIsk isk = null;
                TranscriptByCccid cccid = null;
                byte[] binaryArray = null;

                int dataSource = 1;
                ssid = _context.TranscriptBySsids.Where(s => s.StatewideStudentId == statewideStudentId && s.DataSource == dataSource).FirstOrDefault();

                if (ssid != null)
                {
                    return _mapper.Map<TranscriptBySsid, Placement>(ssid);
                }

                dataSource = 4;
                ssid = _context.TranscriptBySsids.Where(s => s.StatewideStudentId == statewideStudentId && s.DataSource == dataSource).FirstOrDefault();

                if (ssid != null)
                {
                    return _mapper.Map<TranscriptBySsid, Placement>(ssid);
                }

                binaryArray = HexHelper.StringToByteArray(interSegmentKey);

                isk = _context.TranscriptByIsks.Where(s => s.InterSegmentKey == binaryArray && s.DataSource == dataSource).FirstOrDefault();

                if (isk != null)
                {
                    return _mapper.Map<TranscriptByIsk, Placement>(isk);
                }

                dataSource = 2;
                ssid = _context.TranscriptBySsids.Where(s => s.StatewideStudentId == statewideStudentId && s.DataSource == dataSource).FirstOrDefault();

                if (ssid != null)
                {
                    return _mapper.Map<TranscriptBySsid, Placement>(ssid);
                }

                binaryArray = HexHelper.StringToByteArray(interSegmentKey);

                isk = _context.TranscriptByIsks.Where(s => s.InterSegmentKey == binaryArray && s.DataSource == dataSource).FirstOrDefault();

                if (isk != null)
                {
                    return _mapper.Map<TranscriptByIsk, Placement>(isk);
                }

                dataSource = 3;
                cccid = _context.TranscriptByCccids.Where(s => s.CaliforniaCommunityCollegeId == californiaCommunityCollegeId && s.DataSource == dataSource).FirstOrDefault();

                if (cccid != null)
                {
                    return _mapper.Map<TranscriptByCccid, Placement>(cccid);
                }

                return null;
            }
            catch (Exception ex)
            {

                _logger.LogError($"GetPlacement failed: {ex}");

                return null;
            }
        }

        public void AddEntity(object model)
        {
            _context.Add(model);
        }

        public bool SaveAll()
        {
            return _context.SaveChanges() > 0;
        }
    }
}
