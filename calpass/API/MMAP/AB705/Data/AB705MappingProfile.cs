﻿using AB705.Helpers;
using AB705.Entities;
using AB705.ViewModels;
using AutoMapper;

namespace AB705.Data
{
    public class AB705MappingProfile : Profile
    {
        public AB705MappingProfile()
        {
            CreateMap<Placement, PlacementViewModel>()
                .ReverseMap();

            CreateMap<TranscriptBySsid, Placement>();

            CreateMap<TranscriptByIsk, Placement>();

            CreateMap<TranscriptByCccid, Placement>();

            CreateMap<TranscriptStage, TranscriptViewModel>()
                .ForMember(vm => vm.InterSegmentKey, j => j.MapFrom(m => HexHelper.ByteArrayToHexViaLookup32(m.InterSegmentKey)));

            CreateMap<TranscriptViewModel, TranscriptStage>()
                .ForMember(m => m.InterSegmentKey, j => j.MapFrom(vm => HexHelper.StringToByteArray(vm.InterSegmentKey)));
        }
    }
}
