﻿using AB705.Entities;
using AB705.ViewModels;

namespace AB705.Data
{
    public interface IAB705Repository
    {
        Placement GetPlacement(string statewideStudentId, string interSegmentKey, string californiaCommunityCollegeId);

        void AddEntity(object model);

        bool SaveAll();
    }
}