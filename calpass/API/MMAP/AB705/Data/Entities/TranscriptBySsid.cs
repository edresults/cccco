﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AB705.Entities
{
    [Table("TranscriptBySsid", Schema = "AB705")]
    public class TranscriptBySsid
    {
        [Key]
        [Column(TypeName = "char(10)")]
        public string StatewideStudentId { get; set; }

        public byte DataSource { get; set; }

        public bool? CompletedEleventhGrade { get; set; }

        [Column(TypeName = "decimal(3,2)")]
        public decimal CumulativeGradePointAverage { get; set; }

        public byte? EnglishCompletedCourseId { get; set; }

        [Column(TypeName = "varchar(2)")]
        public string EnglishCompletedCourseGrade { get; set; }

        public byte? MathematicsCompletedCourseId { get; set; }

        [Column(TypeName = "varchar(2)")]
        public string MathematicsCompletedCourseGrade { get; set; }

        public byte? MathematicsPassedCourseId { get; set; }

        [Column(TypeName = "varchar(2)")]
        public string MathematicsPassedCourseGrade { get; set; }

        public byte? English { get; set; }

        public byte? Slam { get; set; }

        public byte? Stem { get; set; }

        public bool? IsAlgI { get; set; }

        public bool? IsAlgII { get; set; }

        public bool? Trigonometry { get; set; }

        public bool? PreCalculus { get; set; }

        public bool? Calculus { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }
    }
}