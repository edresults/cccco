﻿using System.ComponentModel.DataAnnotations;

namespace AB705.ViewModels
{
    /// <summary>
    /// Methodology:
    /// <ul>
    ///     <li>AB705</li>
    ///         <ul>
    ///             <li>English</li>
    ///                 <ul>
    ///                     <li>No additional academic or concurrent support required</li>
    ///                         <ol>
    ///                             <li>Cumulative GPA of 2.6</li>
    ///                         </ol>
    ///                     <li>Additional academic or concurrent support recommended</li>
    ///                         <ol>
    ///                             <li>Cumulative GPA of 1.9</li>
    ///                         </ol>
    ///                     <li>Additional academic or concurrent support strongly recommended</li>
    ///                         <ol>
    ///                             <li>Cumulative GPA less than 1.9</li>
    ///                         </ol>
    ///                 </ul>
    ///             <li>SLAM</li>
    ///                 <ul>
    ///                     <li>No additional academic or concurrent support required</li>
    ///                         <ol>
    ///                             <li>Cumulative GPA of 3.0</li>
    ///                         </ol>
    ///                     <li>Additional academic or concurrent support recommended</li>
    ///                         <ol>
    ///                             <li>Cumulative GPA of 2.3</li>
    ///                         </ol>
    ///                     <li>Additional academic or concurrent support strongly recommended</li>
    ///                         <ol>
    ///                             <li>Cumulative GPA less than 2.3</li>
    ///                         </ol>
    ///                 </ul>
    ///             <li>STEM</li>
    ///                 <ul>
    ///                     <li>No additional academic or concurrent support required</li>
    ///                         <ol>
    ///                             <li>Cumulative GPA of 3.4</li>
    ///                         </ol>
    ///                         OR
    ///                         <ol>
    ///                             <li>Cumulative GPA of 2.6</li>
    ///                             <li>Enrolled in Calculus</li>
    ///                         </ol>
    ///                     <li>Additional academic or concurrent support recommended</li>
    ///                         <ol>
    ///                             <li>Cumulative GPA of 2.6</li>
    ///                             <li>Enrolled in PreCalculus</li>
    ///                         </ol>
    ///                     <li>Additional academic or concurrent support strongly recommended</li>
    ///                         <ol>
    ///                             <li>Cumulative GPA less than 2.6</li>
    ///                         </ol>
    ///                 </ul>
    ///         </ul>
    ///     <li>Higher Level Math</li>
    ///         <ul>
    ///             <li>Calculus</li>
    ///                 <ul>
    ///                     <li>Prerequisite</li>
    ///                         <ul>
    ///                             <li>Pre-Caluclus or Trigonometry course grade of C</li>
    ///                         </ul>
    ///                     <li>Rules</li>
    ///                         <ol>
    ///                             <li>Cumulative GPA of 3.6</li>
    ///                         </ol>
    ///                         OR
    ///                         <ol>
    ///                             <li>Cumulative GPA of 3.2</li>
    ///                             <li>Pre-Calculus course grade of C</li>
    ///                         </ol>
    ///                 </ul>
    ///             <li>Pre-Calculus</li>
    ///                 <ul>
    ///                     <li>Prerequisite</li>
    ///                         <ul>
    ///                             <li>Algebra II course grade of C</li>
    ///                         </ul>
    ///                     <li>Rules</li>
    ///                         <ol>
    ///                             <li>Cumulative GPA of 3.4</li>
    ///                         </ol>
    ///                         OR
    ///                         <ol>
    ///                             <li>Cumulative GPA of 2.6</li>
    ///                             <li>Enrolled in Calculus</li>
    ///                         </ol>
    ///                 </ul>
    ///             <li>Trigonometry</li>
    ///                 <ul>
    ///                     <li>Prerequisite</li>
    ///                         <ol>
    ///                             <li>Algebra II course grade of C</li>
    ///                         </ol>
    ///                     <li>Rules</li>
    ///                         <ol>
    ///                             <li>Cumulative GPA of 3.4</li>
    ///                         </ol>
    ///                         OR
    ///                         <ol>
    ///                             <li>Cumulative GPA of 3.0</li>
    ///                             <li>Pre-Calculus course grade of C+</li>
    ///                         </ol>
    ///                         OR
    ///                         <ol>
    ///                             <li>Cumulative GPA of 3.0</li>
    ///                             <li>Algebra II course grade of B</li>
    ///                         </ol>
    ///                 </ul>
    ///             <li>College Algebra</li>
    ///                 <ul>
    ///                     <li>Prerequisite</li>
    ///                         <ol>
    ///                             <li>Algebra II course grade of C</li>
    ///                         </ol>
    ///                     <li>Rules</li>
    ///                         <ol>
    ///                             <li>Cumulative GPA of 3.2</li>
    ///                         </ol>
    ///                         OR
    ///                         <ol>
    ///                             <li>Cumulative GPA of 2.9</li>
    ///                             <li>Pre-Calculus course grade of C</li>
    ///                         </ol>
    ///                 </ul>
    ///         </ul>
    /// </ul>
    /// 
    /// Data Source Hierarchy:
    /// <ol>
    ///     <li>CCGI</li>
    ///     <li>CPP and APP</li>
    ///     <li>CPP</li>
    ///     <li>APP</li>
    /// </ol>
    /// 
    /// Notes:
    /// <ul>
    ///     <li>Transcripts must have at least one year of data from either 11th or 12th grade</li>
    ///     <li>For data originating from CCGI, "InProgress" coursework will be considered for the Completed course variables.</li>
    ///     <li>If a condition requires success or enrollment in a specific course, enrollment in the next course in the sequence assumes the student passed the course requiement in question. For example, a student enrolled in Calculus is assumed to have successfully passed Pre-Calculus.</li>
    ///     <li>For mathematics coursework, the highest mathematics course is applied between Cal-PASS Plus and CCC Apply.</li>
    ///     <li>Students may receive a SLAM or STEM value of "No Support" despite not having completed Algebra II. Colleges may demote the recommendation as appropriate.</li>
    /// </ul>
    /// 
    /// </summary>
    public class PlacementViewModel
    {
        /// <summary>
        /// Source Of Placement Data 
        /// 1 = California College Guidance Initiative (CCGI)
        /// 2 = Cal-PASS Plus (CPP)
        /// 3 = CCC Apply (APP)
        /// 4 = Cal-PASS Plus and CCC Apply (CCC Apply data only applied to mathematics variables and related placements if higher than CPP data)
        /// </summary>
        [Required]
        [Range(1,4)]
        public byte DataSource { get; set; }

        /// <summary>
        /// English Support Recommendation
        /// 1 = Support Not Recommended
        /// 2 = Support Recommended
        /// 3 = Support Strongly Recommended
        /// </summary>
        [Range(1, 3)]
        public byte? English { get; set; }

        /// <summary>
        /// SLAM Support Recommendation
        /// 1 = Support Not Recommended
        /// 2 = Support Recommended
        /// 3 = Support Strongly Recommended
        /// </summary>
        [Range(1, 3)]
        public byte? Slam { get; set; }

        /// <summary>
        /// STEM Support Recommendation
        /// 1 = Support Not Recommended
        /// 2 = Support Recommended
        /// 3 = Support Strongly Recommended
        /// </summary>
        [Range(1, 3)]
        public byte? Stem { get; set; }

        /// <summary>
        /// Successfully Completed Algebra I
        /// </summary>
        public bool? IsAlgI { get; set; }

        /// <summary>
        /// Successfully Completed Algebra II
        /// </summary>
        public bool? IsAlgII { get; set; }

        /// <summary>
        /// Trigonometry Recommendation
        /// True = Recommended
        /// False = Not Recommended
        /// </summary>
        public bool? Trigonometry { get; set; }

        /// <summary>
        /// PreCalculus Recommendation
        /// True = Recommended
        /// False = Not Recommended
        /// </summary>
        public bool? PreCalculus { get; set; }

        /// <summary>
        /// Calculus Recommendation
        /// True = Recommended
        /// False = Not Recommended
        /// </summary>
        public bool? Calculus { get; set; }

        /// <summary>
        /// Completed Eleventh Grade
        /// </summary>
        public bool? CompletedEleventhGrade { get; set; }

        /// <summary>
        /// Cumulative Grade Point Average
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        public decimal CumulativeGradePointAverage { get; set; }

        /// <summary>
        /// Highest English Course Completed
        /// 0 = Unknown
        /// 1 = 12th grade Advanced Placement(AP) English Composition or Literature
        /// 2 = 12th grade Honors English Composition or Literature
        /// 3 = 12th grade English Composition or Literature
        /// 4 = 11th grade Advanced Placement(AP) English Composition or Literature
        /// 5 = 11th grade Honors English Composition or Literature
        /// 6 = 11th grade English Composition or Literature
        /// 7 = 10th grade(or lower) English Composition or Literature
        /// </summary>
        [Range(0, 7)]
        public byte? EnglishCompletedCourseId { get; set; }

        /// <summary>
        /// Grade of Highest English Course Completed
        /// A
        /// A-
        /// B+
        /// B
        /// B-
        /// C+
        /// C
        /// C-
        /// D
        /// F
        /// P
        /// NP
        /// ON
        /// X
        /// </summary>
        [RegularExpression(@"^(?:A|B|C){1}(?:-|\+)?$|^(?:D|F|NP|P|ON|X)$")]
        public string EnglishCompletedCourseGrade { get; set; }

        /// <summary>
        /// Highest Mathematics Course Completed
        /// 0  = Unknown
        /// 1  = Pre-algebra or lower
        /// 2  = Algebra 1
        /// 3  = Integrated Math 1
        /// 4  = Integrated Math 2
        /// 5  = Geometry
        /// 6  = Algebra 2
        /// 7  = Integrated Math 3
        /// 8  = Statistics
        /// 9  = Integrated Math 4
        /// 10 = Trigonometry
        /// 11 = Pre-calculus
        /// 12 = Calculus or higher
        /// 13 = Math Analysis
        /// </summary>
        [Range(0, 13)]
        public byte? MathematicsCompletedCourseId { get; set; }

        /// <summary>
        /// Grade of Highest Mathematics Course Completed
        /// A
        /// A-
        /// B+
        /// B
        /// B-
        /// C+
        /// C
        /// C-
        /// D
        /// F
        /// P
        /// NP
        /// ON
        /// X
        /// </summary>
        [RegularExpression(@"^(?:A|B|C){1}(?:-|\+)?$|^(?:D|F|NP|P|ON|X)$")]
        public string MathematicsCompletedCourseGrade { get; set; }

        /// <summary>
        /// Highest Mathematics Passed
        /// 0  = Unknown
        /// 1  = Pre-algebra or lower
        /// 2  = Algebra 1
        /// 3  = Integrated Math 1
        /// 4  = Integrated Math 2
        /// 5  = Geometry
        /// 6  = Algebra 2
        /// 7  = Integrated Math 3
        /// 8  = Statistics
        /// 9  = Integrated Math 4
        /// 10 = Trigonometry
        /// 11 = Pre-calculus
        /// 12 = Calculus or higher
        /// 13 = Math Analysis
        /// </summary>
        [Range(0, 13)]
        public byte? MathematicsPassedCourseId { get; set; }

        /// <summary>
        /// Grade of Highest Mathematics Course Passed
        /// A
        /// A-
        /// B+
        /// B
        /// B-
        /// C+
        /// C
        /// C-
        /// D
        /// F
        /// P
        /// NP
        /// ON
        /// X
        /// </summary>
        [RegularExpression(@"^(?:A|B|C){1}(?:-|\+)?$|^(?:D|F|NP|P|ON|X)$")]
        public string MathematicsPassedCourseGrade { get; set; }
    }
}