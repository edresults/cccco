﻿using System.ComponentModel.DataAnnotations;

namespace AB705.ViewModels
{
    public class TranscriptViewModel
    {
        /// <summary>
        /// Statewide Student Identifier
        /// </summary>
        [StringLength(10, MinimumLength = 10)]
        public string StatewideStudentId { get; set; }

        /// <summary>
        /// Hexadecimal Literal of Hashed Demographic Payload
        /// </summary>
        [StringLength(128, MinimumLength = 128)]
        public string InterSegmentKey { get; set; }

        /// <summary>
        /// California Community College Identifier
        /// </summary>
        [StringLength(8, MinimumLength = 7)]
        public string CaliforniaCommunityCollegeId { get; set; }

        /// <summary>
        /// Source Of Placement Data 
        /// 1 = California College Guidance Initiative (CCGI)
        /// 2 = Cal-PASS Plus (CPP)
        /// 3 = CCC Apply
        /// </summary>
        [Range(1, 3)]
        public byte? DataSource { get; set; }

        /// <summary>
        /// Completed Eleventh Grade
        /// </summary>
        public bool? CompletedEleventhGrade { get; set; }

        /// <summary>
        /// Cumulative Grade Point Average
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        public decimal CumulativeGradePointAverage { get; set; }

        /// <summary>
        /// Highest English Course Completed
        /// 0 = Unknown
        /// 1 = 12th grade Advanced Placement(AP) English Composition or Literature
        /// 2 = 12th grade Honors English Composition or Literature
        /// 3 = 12th grade English Composition or Literature
        /// 4 = 11th grade Advanced Placement(AP) English Composition or Literature
        /// 5 = 11th grade Honors English Composition or Literature
        /// 6 = 11th grade English Composition or Literature
        /// 7 = 10th grade(or lower) English Composition or Literature
        /// </summary>
        [Range(0, 7)]
        public byte? EnglishCompletedCourseId { get; set; }

        /// <summary>
        /// Grade of Highest English Course Completed
        /// A
        /// A-
        /// B+
        /// B
        /// B-
        /// C+
        /// C
        /// C-
        /// D
        /// F
        /// P
        /// NP
        /// ON
        /// X
        /// </summary>
        [RegularExpression(@"^(?:A|B|C){1}(?:-|\+)?$|^(?:D|F|NP|P|ON|X)$")]
        public string EnglishCompletedCourseGrade { get; set; }

        /// <summary>
        /// Highest Mathematics Course Completed
        /// 0  = Unknown
        /// 1  = Pre-algebra or lower
        /// 2  = Algebra 1
        /// 3  = Integrated Math 1
        /// 4  = Integrated Math 2
        /// 5  = Geometry
        /// 6  = Algebra 2
        /// 7  = Integrated Math 3
        /// 8  = Statistics
        /// 9  = Integrated Math 4
        /// 10 = Trigonometry
        /// 11 = Pre-calculus
        /// 12 = Calculus or higher
        /// 13 = Math Analysis
        /// </summary>
        [Range(0, 13)]
        public byte? MathematicsCompletedCourseId { get; set; }

        /// <summary>
        /// Grade of Highest Mathematics Course Completed
        /// A
        /// A-
        /// B+
        /// B
        /// B-
        /// C+
        /// C
        /// C-
        /// D
        /// F
        /// P
        /// NP
        /// ON
        /// X
        /// </summary>
        [RegularExpression(@"^(?:A|B|C){1}(?:-|\+)?$|^(?:D|F|NP|P|ON|X)$")]
        public string MathematicsCompletedCourseGrade { get; set; }

        /// <summary>
        /// Highest Mathematics Passed
        /// 0  = Unknown
        /// 1  = Pre-algebra or lower
        /// 2  = Algebra 1
        /// 3  = Integrated Math 1
        /// 4  = Integrated Math 2
        /// 5  = Geometry
        /// 6  = Algebra 2
        /// 7  = Integrated Math 3
        /// 8  = Statistics
        /// 9  = Integrated Math 4
        /// 10 = Trigonometry
        /// 11 = Pre-calculus
        /// 12 = Calculus or higher
        /// 13 = Math Analysis
        /// </summary>
        [Range(0, 13)]
        public byte? MathematicsPassedCourseId { get; set; }

        /// <summary>
        /// Grade of Highest Mathematics Course Passed
        /// A
        /// A-
        /// B+
        /// B
        /// B-
        /// C+
        /// C
        /// C-
        /// D
        /// F
        /// P
        /// NP
        /// ON
        /// X
        /// </summary>
        [RegularExpression(@"^(?:A|B|C){1}(?:-|\+)?$|^(?:D|F|NP|P|ON|X)$")]
        public string MathematicsPassedCourseGrade { get; set; }
    }
}