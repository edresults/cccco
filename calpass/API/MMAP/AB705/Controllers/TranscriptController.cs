﻿using AB705.Data;
using AB705.Entities;
using AB705.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace AB705.Controllers
{
    /// <summary>
    /// AB705 Controller
    /// </summary>
    [ApiController]
    [Route("api/[Controller]")]
    [Produces("application/json")]
    [Authorize(Policy = "PlacementUsers")]
    public class TranscriptController : ControllerBase
    {
        private readonly IAB705Repository _repository;
        private readonly ILogger<TranscriptController> _logger;
        private readonly IMapper _mapper;

        public TranscriptController(IAB705Repository repository, ILogger<TranscriptController> logger, IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Create AB705 Transcript
        /// </summary>
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        public ActionResult<TranscriptViewModel> Post([FromBody]TranscriptViewModel model)
        {
            try
            {
                if (model.InterSegmentKey == null && model.StatewideStudentId == null && model.CaliforniaCommunityCollegeId == null)
                {
                    return BadRequest("At least one identifier must not be null");
                }

                if (!model.CompletedEleventhGrade.HasValue || !(bool)model.CompletedEleventhGrade)
                {
                    return BadRequest("At least 11th or 12th grade must be completed");
                }

                if (ModelState.IsValid)
                {
                    var newEntity = _mapper.Map<TranscriptViewModel, TranscriptStage>(model);

                    newEntity.DataSource = 3;

                    _repository.AddEntity(newEntity);

                    if (_repository.SaveAll())
                    {
                        return StatusCode(201);
                    }
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to create a new transcript: {ex}");
            }
            return BadRequest("Ya done did a goof");
        }
    }
}
