﻿using AB705.Data;
using AB705.Entities;
using AB705.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace AB705.Controllers
{
    /// <summary>
    /// AB705 Controllers
    /// </summary>
    [ApiController]
    [Route("api/[Controller]")]
    [Produces("application/json")]
    [Authorize(Policy = "PlacementUsers")]
    public class PlacementController : ControllerBase
    {

        private readonly IAB705Repository _repository;
        private readonly ILogger<PlacementViewModel> _logger;
        private readonly IMapper _mapper;

        public PlacementController(IAB705Repository repository, ILogger<PlacementViewModel> logger, IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>Read AB705 Placement</summary>
        /// <returns>Instance of AB705PlacementViewModel</returns>
        /// <param name="studentStatewideId"><a href='https://www.cde.ca.gov/ds/sp/cl/ssid.asp'>Statewide Student Identifier</a></param>
        /// <param name="interSegmentKey">Hashed Demographic Payload</param>
        /// <param name="californiaCommunityCollegeId"><a href='https://cccnext.jira.com/wiki/spaces/CSF/pages/150667353/CCCID+The+Use+and+Significance+of+the+CCCID+in+the+CCC+SSO+Initiative'>California Community College Identifier</a></param>
        /// <response code="204">InterSegmentKey Collision</response>
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(PlacementViewModel))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public ActionResult<PlacementViewModel> Get([FromQuery]
            string studentStatewideId,
            string interSegmentKey,
            string californiaCommunityCollegeId
        )
        {
            try
            {
                if (String.IsNullOrWhiteSpace(studentStatewideId) && String.IsNullOrWhiteSpace(interSegmentKey) && String.IsNullOrWhiteSpace(californiaCommunityCollegeId))
                {
                    return BadRequest("At least one identifier must not be null");
                }

                var placement = _repository.GetPlacement(studentStatewideId, interSegmentKey, californiaCommunityCollegeId);

                if (placement == null)
                {
                    return NotFound();
                }
                else if (placement.IsCollision)
                {
                    return NoContent();
                }

                if (placement == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(_mapper.Map<Placement, PlacementViewModel>(placement));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get Placement: {ex}");
                return BadRequest("Ya done did a goof");
            }
        }
    }
}
