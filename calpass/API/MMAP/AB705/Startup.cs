﻿using AB705.Data;
using AB705.Swagger;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Reflection;

namespace AB705
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddCors();

            services.AddAuthentication("Bearer").AddIdentityServerAuthentication(action =>
                {
                    action.Authority = _configuration["AppSettings:Authority"];
                    action.RequireHttpsMetadata = false;
                    action.ApiName = "api";
                }
             );

            services.AddMvcCore().AddAuthorization(action =>
                {
                    action.AddPolicy("PlacementUsers", policy =>
                        policy.RequireAssertion(context =>
                            context.User.HasClaim(claim =>
                                (claim.Type == "Admin" && claim.Value == "True") ||
                                (claim.Type == "MmapUser" && claim.Value == "True") ||
                                (claim.Type == "MmppUser" && claim.Value == "True") ||
                                (claim.Type == "TranscriptUser" && claim.Value == "True")
                            )
                        )
                    );
                    action.AddPolicy("TranscriptUsers", policy =>
                        policy.RequireAssertion(context =>
                            context.User.HasClaim(claim =>
                                (claim.Type == "Admin" && claim.Value == "True") ||
                                (claim.Type == "TranscriptUser" && claim.Value == "True")
                            )
                        )
                    );

                    action.AddPolicy("MmppUsers", policy =>
                        policy.RequireAssertion(context =>
                            context.User.HasClaim(claim =>
                                (claim.Type == "Admin" && claim.Value == "True") ||
                                (claim.Type == "MmppUser" && claim.Value == "True")
                            )
                        )
                    );
                }
            );

            services.AddMvcCore().AddApiExplorer();
            services.AddMvcCore().AddJsonFormatters();
            services.AddMvcCore().AddRazorViewEngine();

            services.AddDbContext<AB705Context>(action =>
                {
                    action.UseSqlServer(_configuration.GetConnectionString("DefaultConnection"));
                }
            );

            services.AddAutoMapper();

            services.AddScoped<IAB705Repository, AB705Repository>();

            services.AddSwaggerGen(action =>
            {
                action.SchemaFilter<Examples>();
                action.SwaggerDoc("v1",
                    new Info
                    {
                        Title = "AB705 API",
                        Version = "v1",
                        Description = "Click <a href='/Documentation/Authentication'>here</a> for information regarding Authorization.",
                        TermsOfService = ""
                    }
                 );

                action.DocumentFilter<DocumentFilters>();
                action.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
                action.DescribeAllEnumsAsStrings();
            });

            //services.AddApiVersioning(
            //    o =>
            //    {
            //        o.AssumeDefaultVersionWhenUnspecified = true;
            //        o.DefaultApiVersion = new ApiVersion(1, 0);
            //    }
            //);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseAuthentication();

            app.UseMvc(action =>
            {
                action.MapRoute(
                    name: "default",
                    template: "{controller=Documentation}/{action=Authentication}/{id?}");
            }
            );

            app.UseSwagger();

            app.UseSwaggerUI(action =>
            {
                action.SwaggerEndpoint("/swagger/v1/swagger.json", "AB705 API V1");
                action.RoutePrefix = "docs";
            }
            );
        }
    }
}
