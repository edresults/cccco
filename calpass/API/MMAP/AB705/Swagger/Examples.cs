﻿using AB705.Helpers;
using AB705.ViewModels;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Security.Cryptography;
using System.Text;

namespace AB705.Swagger
{
    public class Examples : ISchemaFilter
    {
        public void Apply(Schema model, SchemaFilterContext context)
        {
            if (context.SystemType == typeof(PlacementViewModel))
            {
                model.Example = new PlacementViewModel()
                {
                    DataSource = 2,
                    English = 1,
                    Slam = 2,
                    Stem = 3,
                    IsAlgI = true,
                    IsAlgII = false,
                    Trigonometry = false,
                    PreCalculus = false,
                    Calculus = false,
                    CompletedEleventhGrade = true,
                    CumulativeGradePointAverage = 3.89M,
                    EnglishCompletedCourseId = 3,
                    EnglishCompletedCourseGrade = "A",
                    MathematicsCompletedCourseId = 10,
                    MathematicsCompletedCourseGrade = "D",
                    MathematicsPassedCourseId = 9,
                    MathematicsPassedCourseGrade = "C"
                };
            }

            if (context.SystemType == typeof(TranscriptViewModel))
            {
                model.Example = new TranscriptViewModel()
                {
                    StatewideStudentId = "1234567890",
                    InterSegmentKey = HexHelper.ByteArrayToHexViaLookup32(new SHA512Managed().ComputeHash(Encoding.Unicode.GetBytes("DANLAMM19440606"))),
                    CaliforniaCommunityCollegeId = "ZZZ99999",
                    DataSource = 3,
                    CompletedEleventhGrade = true,
                    CumulativeGradePointAverage = 3.89M,
                    EnglishCompletedCourseId = 3,
                    EnglishCompletedCourseGrade = "A",
                    MathematicsCompletedCourseId = 10,
                    MathematicsCompletedCourseGrade = "D",
                    MathematicsPassedCourseId = 9,
                    MathematicsPassedCourseGrade = "C"
                };
            }
        }
    }
}