﻿using UserUtility.Data;
using UserUtility.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;

namespace UserUtility
{
    class Program
    {
        public static IConfigurationRoot configuration;

        static void Main(string[] args)
        {
            var services = new ServiceCollection();

            // Set up configuration sources.
            var builder = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(Directory.GetCurrentDirectory()))
                .AddJsonFile($"appsettings.json", optional: true)
                .AddJsonFile($"appsettings.Production.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.Development.json", optional: false, reloadOnChange: true);

            configuration = builder.Build();

            services.AddDbContext<ApplicationDbContext>(action =>
                {
                    action.UseSqlServer("Server=CPPDELSQLP01;Database=CPP_Identity;User Id=api_sa;Password=96mN5JYfN86m;MultipleActiveResultSets=true");
                }
            );

            // Authentification
            services.AddIdentity<ApplicationUser, IdentityRole>(opt =>
            {
                // Configure identity options
                opt.Password.RequireDigit = false;
                opt.Password.RequireLowercase = false;
                opt.Password.RequireUppercase = false;
                opt.Password.RequireNonAlphanumeric = false;
                opt.Password.RequiredLength = 6;
                opt.User.RequireUniqueEmail = false;
            })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddScoped<IUserCreationService, UserCreationService>();

            // Build the IoC from the service collection
            var provider = services.BuildServiceProvider();

            var userService = provider.GetService<IUserCreationService>();

            Console.WriteLine("Choose Method:");
            Console.WriteLine("  [D]elete");
            Console.WriteLine("  [C]reate");
            var method = Console.ReadLine();

            if (method.ToUpper() == "C" || method.ToUpper() == "CREATE")
            {
                Console.Write("Enter a username -> ");
                var username = Console.ReadLine();

                Console.Write("Enter a password -> ");
                var password = Console.ReadLine();

                Console.Write("Enter a claim -> ");
                var claim = Console.ReadLine();

                userService.CreateUser(username, password, claim)
                                    .GetAwaiter()
                                    .GetResult();
            }
            else if (method.ToUpper() == "D" || method.ToUpper() == "DELETE")
            {
                Console.Write("Enter a username -> ");
                var username = Console.ReadLine();

                userService.DeleteUser(username);
            }
            else
            {
                Console.WriteLine("You dun did a goof");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Hit any key to close this window.");
            Console.ReadKey();
        }
    }

    interface IUserCreationService
    {
        Task CreateUser(string username, string password, string claim);

        Task DeleteUser(string username);

        void RegenerateUsers();
    }

    class UserCreationService : IUserCreationService
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public UserCreationService(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task DeleteUser(string username)
        {
            Console.WriteLine(String.Format("Deleting User: {0}", username));

            var user = await _userManager.FindByNameAsync(username);

            if (user == null)
            {
                Console.WriteLine(String.Format("User ({0}) not found!", username));
                return;
            }
            else
            {
                Console.WriteLine(String.Format("User ({0}) found!", username));
            }

            var result = await _userManager.DeleteAsync(user);

            if (result.Succeeded == false)
            {
                foreach (var error in result.Errors)
                {
                    Console.WriteLine(error.Description);
                }
            }
            else
            {
                Console.WriteLine(String.Format("User ({0}) successfully deleted!", username));
            }
        }

        public async Task CreateUser(string username, string password, string claim)
        {
            var user = new ApplicationUser { UserName = username };

            var result = await _userManager.CreateAsync(user, password);

            if (result.Succeeded == false)
            {
                foreach (var error in result.Errors)
                {
                    Console.WriteLine(error.Description);
                }
            }
            else
            {
                Console.WriteLine(String.Format("User ({0}) successfully created!", username));
            }

            if (!String.IsNullOrWhiteSpace(claim))
            {
                Claim userClaim = new Claim(claim, "True");

                result = await _userManager.AddClaimAsync(user, userClaim);

                if (result.Succeeded == false)
                {
                    foreach (var error in result.Errors)
                    {
                        Console.WriteLine(error.Description);
                    }
                }
                else
                {
                    Console.WriteLine(String.Format("User ({0}) has claim ({1}) added!", username, claim));
                }
            }
        }

        private void DeleteUsers()
        {
            DeleteUser("000").Wait();
            DeleteUser("StcMmap").Wait();
            DeleteUser("pesc_client").Wait();
            DeleteUser("mmpp_client").Wait();
            DeleteUser("NOVA").Wait();
            DeleteUser("031").Wait();
            DeleteUser("061").Wait();
            DeleteUser("161").Wait();
            DeleteUser("231").Wait();
            DeleteUser("232").Wait();
            DeleteUser("233").Wait();
            DeleteUser("234").Wait();
            DeleteUser("261").Wait();
            DeleteUser("271").Wait();
            DeleteUser("311").Wait();
            DeleteUser("361").Wait();
            DeleteUser("371").Wait();
            DeleteUser("411").Wait();
            DeleteUser("421").Wait();
            DeleteUser("431").Wait();
            DeleteUser("451").Wait();
            DeleteUser("461").Wait();
            DeleteUser("470").Wait();
            DeleteUser("481").Wait();
            DeleteUser("490").Wait();
            DeleteUser("523").Wait();
            DeleteUser("551").Wait();
            DeleteUser("561").Wait();
            DeleteUser("571").Wait();
            DeleteUser("580").Wait();
            DeleteUser("590").Wait();
            DeleteUser("611").Wait();
            DeleteUser("651").Wait();
            DeleteUser("731").Wait();
            DeleteUser("781").Wait();
            DeleteUser("811").Wait();
            DeleteUser("821").Wait();
            DeleteUser("831").Wait();
            DeleteUser("832").Wait();
            DeleteUser("833").Wait();
            DeleteUser("841").Wait();
            DeleteUser("860").Wait();
            DeleteUser("861").Wait();
            DeleteUser("862").Wait();
            DeleteUser("871").Wait();
            DeleteUser("873").Wait();
            DeleteUser("881").Wait();
            DeleteUser("890").Wait();
            DeleteUser("891").Wait();
            DeleteUser("892").Wait();
            DeleteUser("931").Wait();
            DeleteUser("962").Wait();
            DeleteUser("963").Wait();
        }

        private void CreateUsers()
        {

        }

        public void RegenerateUsers()
        {
            DeleteUsers();
            CreateUsers();
        }
    }
}
