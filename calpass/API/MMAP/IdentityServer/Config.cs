﻿using IdentityModel;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace IdentityServer
{
    public class Config
    {

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                //new ApiResource("api", "Cal-PASS Plus API"),
                new ApiResource
                {
                    Name = "api",
                    DisplayName = "Cal-PASS Plus API",
                    Scopes =
                        {
                            new Scope
                            {
                                Name = "api",
                                DisplayName = "California Multiple Measures",
                                Required = true
                            }
                        },
                    UserClaims =
                        {
                            "MmapUser", // CCs
                            "MmppUser", // Unicon folks
                            "TranscriptUser", // 
                            "Admin"
                        }
                },

                new ApiResource("txmm", "EdResults API"),
                new ApiResource("mmpp", "Multiple Measures Placement Platform"),
                //new ApiResource
                //{
                //    Name = "txmm",
                //    DisplayName = "South Texas College Multiple Measures",
                //    Scopes =
                //    {
                //        new Scope
                //        {
                //            Name = "api",
                //            DisplayName = "Texas Multiple Measures",
                //            Description = "Student Placement Using Multiple Measures",
                //            Required = true,
                //            //UserClaims =
                //            //{
                //            //    JwtClaimTypes.Role = "txapi"
                //            //}
                //        }
                //    }
                //}
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "remote",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    //AlwaysSendClientClaims = true,
                    //AlwaysIncludeUserClaimsInIdToken = true,
                    RequireConsent = false,
                    RequireClientSecret = false,
                    AllowedScopes = { "api" }
                },
                new Client
                {
                    ClientId = "STC",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    //AlwaysSendClientClaims = true,
                    //AlwaysIncludeUserClaimsInIdToken = true,
                    RequireConsent = false,
                    RequireClientSecret = false,
                    AllowedScopes = { "txmm" }
                },
                new Client
                {
                    ClientId = "mmpp_client",
                    ClientSecrets =
                    {
                        new Secret("L@WIfgI*tOsA".Sha256())
                    },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    //AlwaysSendClientClaims = true,
                    //AlwaysIncludeUserClaimsInIdToken = true,
                    RequireConsent = false,
                    RequireClientSecret = false,
                    AllowedScopes = { "mmpp" }
                }
            };
        }
    }
}
