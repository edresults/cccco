﻿namespace Launchboard.Api.Models
{
    public class MetricResultModel
    {
        public string MetricId { get; set; }
        public string DisplayId { get; set; }
        public string BriefDesc { get; set; }
        public string FullDesc { get; set; }
        public string TableHoverText { get; set; }
        public string ChartHoverText { get; set; }
        public string ChartTitleText { get; set; }
        public string YAxisText { get; set; }
        public string XAxisText { get; set; }
        public string MissingText { get; set; }
        public string FerpaText { get; set; }
        public string DelayText { get; set; }
        public string AcademicYear { get; set; }
        public string MacroRegionName { get; set; }
        public string MicroReagionName { get; set; }
        public string CCIpedsCode { get; set; }
        public string SectorName { get; set; }
        public string ProgramCode { get; set; }
        public string Disagg { get; set; }
        public string Subgroup { get; set; }
        public float? MetricValue { get; set; }
        public int? MetricDenom { get; set; }
        public float? MetricPerc { get; set; }
        public int? FerpaMask { get; set; }
        public int? DisaggOrder { get; set; }
        public int? SubgroupOrder { get; set; }
    }
}
