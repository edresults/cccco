﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Launchboard.Api.Models
{
    public class MetricQueryModel
    {
        [BindRequired]
        public int SelectedYear { get; set; }
        [BindRequired]
        public string Locale { get; set; }
        [BindRequired]
        public string Program { get; set; }
        [BindRequired]
        public string MetricId { get; set; }
    }
}