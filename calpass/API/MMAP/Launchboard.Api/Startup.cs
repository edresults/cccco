﻿using Launchboard.Api.Interfaces;
using Launchboard.Api.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Launchboard.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvcCore()
                .AddJsonFormatters()
                .AddAuthorization();
            
            string connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddSingleton<ILaunchboardService>(new LaunchboardService(connectionString));
            
            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = Configuration["AppSettings:Authority"];
                    options.RequireHttpsMetadata = false;
                    options.ApiName = "api";
                }
            );

            services.AddAuthorization(c =>
            {
                c.AddPolicy("NovaUser", p => p.RequireClaim("NovaUser", "True"));
            }
            );
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
