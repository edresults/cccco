﻿using Launchboard.Api.Models;
using System.Collections.Generic;

namespace Launchboard.Api.Interfaces
{
    public interface ILaunchboardService
    {
        IEnumerable<MetricResultModel> GetMetric(MetricQueryModel queryModel);
    }
}
