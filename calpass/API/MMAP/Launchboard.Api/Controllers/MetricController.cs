﻿using Launchboard.Api.Interfaces;
using Launchboard.Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Launchboard.Api.Controllers
{
    /// <summary>
    /// Metric Controller
    /// </summary>
    [Authorize(Policy = "NovaUser")]
    [Route("api/metric")]
    public class MetricController : Controller
    {

        private readonly ILaunchboardService _launchboardService;

        public MetricController(ILaunchboardService launchboardService)
        {
            _launchboardService = launchboardService;
        }

        /// <summary>
        /// Get Metric Output
        /// </summary>
        /// <returns>Instance of MetricModel</returns>
        /// <param name="selectedYear">Year</param>
        /// <param name="locale">Locale</param>
        /// <param name="program">Program</param>
        /// <param name="metricId">Metric</param>
        [HttpGet]
        public IActionResult Get([FromQuery] MetricQueryModel metricQueryModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                if (metricQueryModel.SelectedYear == 0)
                {
                    return BadRequest();
                }
                else if (String.IsNullOrWhiteSpace(metricQueryModel.Locale))
                {
                    return BadRequest();
                }
                else if (String.IsNullOrWhiteSpace(metricQueryModel.Program))
                {
                    return BadRequest();
                }
                else if (String.IsNullOrWhiteSpace(metricQueryModel.MetricId))
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }


            List<MetricResultModel> metricResultModel = new List<MetricResultModel>();

            try
            {
                metricResultModel = _launchboardService.GetMetric(metricQueryModel).ToList();
            }
            catch
            {
                return StatusCode(500);
            }

            if (metricResultModel == null)
            {
                return NotFound("Metric was not found");
            }

            return Ok(metricResultModel);
        }
    }
}
