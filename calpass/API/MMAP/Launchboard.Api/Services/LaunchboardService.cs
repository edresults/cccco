﻿using System.Data;
using System.Data.SqlClient;
using Launchboard.Api.Interfaces;
using Launchboard.Api.Models;
using System;
using System.Collections.Generic;

namespace Launchboard.Api.Services
{
    public class LaunchboardService : ILaunchboardService
    {
        private readonly string _connectionString;

        public LaunchboardService(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<MetricResultModel> GetMetric(MetricQueryModel metricQueryModel)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                string cmdText = "ReturnMetric";

                using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] sqlParamters = new SqlParameter[] {
                        new SqlParameter()
                        {
                            ParameterName = "@selected_year",
                            SqlDbType = SqlDbType.Int,
                            Direction = ParameterDirection.Input,
                            Value = metricQueryModel.SelectedYear
                        },
                        new SqlParameter()
                        {
                            ParameterName = "@locale",
                            SqlDbType = SqlDbType.VarChar,
                            Size = 100,
                            Direction = ParameterDirection.Input,
                            Value = metricQueryModel.Locale
                        },
                        new SqlParameter()
                        {
                            ParameterName = "@program",
                            SqlDbType = SqlDbType.VarChar,
                            Size = 255,
                            Direction = ParameterDirection.Input,
                            Value = metricQueryModel.Program
                        },
                        new SqlParameter()
                        {
                            ParameterName = "@metric_id",
                            SqlDbType = SqlDbType.VarChar,
                            Size = 6,
                            Direction = ParameterDirection.Input,
                            Value = metricQueryModel.MetricId
                        }
                    };

                    sqlCommand.Parameters.AddRange(sqlParamters);

                    sqlConnection.Open();

                    using (IDataReader reader = sqlCommand.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            yield return new MetricResultModel()
                            {
                                MetricId = reader["metric_id"].ToString(),
                                DisplayId = reader["display_id"].ToString(),
                                BriefDesc = reader["brief_desc"].ToString(),
                                FullDesc = reader["full_desc"].ToString(),
                                TableHoverText = reader["table_hover_text"].ToString(),
                                ChartHoverText = reader["chart_hover_text"].ToString(),
                                ChartTitleText = reader["chart_title_text"].ToString(),
                                YAxisText = reader["y_axis_text"].ToString(),
                                XAxisText = reader["x_axis_text"].ToString(),
                                MissingText = reader["missing_text"].ToString(),
                                FerpaText = reader["ferpa_text"].ToString(),
                                DelayText = reader["delay_text"].ToString(),
                                AcademicYear = reader["academic_year"].ToString(),
                                MacroRegionName = reader["macroregion_name"].ToString(),
                                MicroReagionName = reader["microregion_name"].ToString(),
                                CCIpedsCode = reader["cc_ipeds_code"].ToString(),
                                SectorName = reader["sector_name"].ToString(),
                                ProgramCode = reader["program_code"].ToString(),
                                Disagg = reader["disagg"].ToString(),
                                Subgroup = reader["subgroup"].ToString(),
                                MetricValue = String.IsNullOrWhiteSpace(reader["metric_value"].ToString()) ? (float?)null : float.Parse(reader["metric_value"].ToString()),
                                MetricDenom = Convert.ToInt32(reader["metric_denom"].ToString()),
                                MetricPerc = String.IsNullOrWhiteSpace(reader["metric_perc"].ToString()) ? (float?)null : float.Parse(reader["metric_perc"].ToString()),
                                FerpaMask = Convert.ToInt32(reader["ferpa_mask"].ToString()),
                                DisaggOrder = Convert.ToInt32(reader["disagg_order"].ToString()),
                                SubgroupOrder = Convert.ToInt32(reader["subgroup_order"].ToString())
                            };
                        }
                    }
                }
            }
        }
    }
}