﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MultipleMeasures.Data;
using MultipleMeasures.Entities;
using MultipleMeasures.ViewModels;
using System;

namespace MultipleMeasures.Controllers
{
    /// <summary>
    /// MMAP Placement Controller
    /// </summary>
    [Authorize(Policy = "PlacementUsers")]
    [Route("api/[controller]")]
    public class PlacementController : ControllerBase
    {

        private readonly IMmapRepository _repository;
        private readonly ILogger<Placement> _logger;
        private readonly IMapper _mapper;

        public PlacementController(IMmapRepository repository, ILogger<Placement> logger, IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Read MMAP Placement
        /// </summary>
        /// <returns>Instance of PlacementModel</returns>
        /// <param name="studentId">Pass either <a href='https://www.cde.ca.gov/ds/sp/cl/ssid.asp'>Statewide Student Identifier</a>, <a href='https://cccnext.jira.com/wiki/spaces/CSF/pages/150667353/CCCID+The+Use+and+Significance+of+the+CCCID+in+the+CCC+SSO+Initiative'>CCCID</a>, or InterSegmentKey</param>
        [HttpGet("{studentId}")]
        [ProducesResponseType(200, Type = typeof(PlacementViewModel))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        public ActionResult<PlacementViewModel> Get(string studentId)
        {
            try
            {
                var result = _repository.GetPlacement(studentId);

                if (result == null)
                {
                    return NoContent();
                }
                else
                {
                    return _mapper.Map<Placement, PlacementViewModel>(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get Placement: {ex}");
                return BadRequest("Ya done did a goof");
            }
        }
    }
}
