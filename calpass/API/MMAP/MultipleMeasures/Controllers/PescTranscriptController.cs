﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MultipleMeasures.Data;
using MultipleMeasures.Entities;
using MultipleMeasures.Helpers;
using MultipleMeasures.ViewModels;
using System;

namespace MultipleMeasures.Controllers
{
    /// <summary>
    /// PESC Transcript Controller
    /// </summary>
    [Route("api/[Controller]")]
    [Produces("application/json")]
    [Authorize(Policy = "MmppUsers")]
    public class PescTranscriptController : ControllerBase
    {
        private readonly IMmapRepository _repository;
        private readonly ILogger<PescTranscriptController> _logger;
        private readonly IMapper _mapper;

        public PescTranscriptController(IMmapRepository repository, ILogger<PescTranscriptController> logger, IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Create PESC Transcript
        /// </summary>
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        public ActionResult<PescTranscriptViewModel> Post([FromBody]PescTranscriptViewModel[] model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var newEntity = _mapper.Map<PescTranscriptViewModel[], PescTranscript[]>(model);

                    try
                    {
                        _repository.AddEntities(newEntity);

                        if (_repository.SaveAll())
                        {
                            return StatusCode(201);
                        }

                        return BadRequest($"Failed to create a new transcript");
                    }
                    catch(Exception ex)
                    {
                        _logger.LogError($"Failed to create a new transcript: {ex}");
                        return BadRequest($"Failed to create a new transcript: {ex.Message}");
                    }
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to create a new transcript: {ex}");
                return BadRequest($"Failed to create a new transcript: {ex.Message}");
            }
        }
    }
}
