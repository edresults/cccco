﻿using System.ComponentModel.DataAnnotations;

namespace MultipleMeasures.ViewModels
{
    /// <summary>
    /// Primary Key Columns
    /// <ol>
    ///     <li>SchoolAssignedPersonID</li>
    ///     <li>LocalOrganizationIDCode</li>
    ///     <li>SessionSchoolYear</li>
    ///     <li>SessionTermCode</li>
    ///     <li>OriginalCourseId</li>
    /// </ol>
    /// </summary>
    public class PescTranscriptViewModel
    {
        [Required]
        [StringLength(20, ErrorMessage = "Cannot exceed 20 characters")]
        public string SchoolAssignedPersonID { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Cannot exceed 20 characters")]
        public string RecipientAssignedID { get; set; }

        [Required]
        [RegularExpression(@"^(?:19|20)\d{2}-\d{2}-\d{2}$")]
        public string BirthDate { get; set; }

        [Required]
        [StringLength(35, ErrorMessage = "Cannot exceed 35 characters")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(35, ErrorMessage = "Cannot exceed 35 characters")]
        public string LastName { get; set; }

        [Required]
        [StringLength(6, MinimumLength = 6, ErrorMessage = "Must be exactly 6 characters")]
        public string CEEBACT { get; set; }

        [Required]
        [StringLength(35, ErrorMessage = "Cannot exceed 35 characters")]
        public string LocalOrganizationIDCode { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "Cannot exceed 40 characters")]
        public string StudentLevelCode { get; set; }

        [Required]
        [StringLength(60, ErrorMessage = "Cannot exceed 60 characters")]
        public string OrganizationName { get; set; }

        [RegularExpression(@"^(?:19|20)\d{2}-\d{2}-\d{2}$")]
        public string AcademicCompletionDate { get; set; }

        [Required]
        [RegularExpression(@"^[0-4]{1}(?:\.\d{0,5})?$|^5\.?0{0,5}$")]
        public string GradePointAverage { get; set; }

        [Required]
        [StringLength(19, ErrorMessage = "Cannot exceed 19 characters")]
        public string AcademicSummaryType { get; set; }

        [Required]
        [RegularExpression(@"^(?:19|20)\d{2}-(?:19|20)\d{2}$")]
        public string SessionSchoolYear { get; set; }

        [Required]
        [RegularExpression(@"^(?:F|S1|S2|T1|T2|T3|Q1|Q2|Q3|Q4|SS|SS1|SS2|Unknown)$")]
        public string SessionTermCode { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Cannot exceed 30 characters")]
        public string OriginalCourseID { get; set; }

        [Required]
        [RegularExpression(@"^\d{4}$")]
        public string AgencyCourseID { get; set; }

        [StringLength(30, ErrorMessage = "Cannot exceed 30 characters")]
        public string CourseSectionNumber { get; set; }

        [Required]
        [StringLength(60, ErrorMessage = "Cannot exceed 60 characters")]
        public string CourseTitle { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Cannot exceed 30 characters")]
        public string CourseCreditBasis { get; set; }

        [Required]
        [StringLength(25, ErrorMessage = "Cannot exceed 25 characters")]
        public string CourseCreditUnits { get; set; }

        [Required]
        [RegularExpression(@"^\d{1,2}?\.?\d{0,5}?$")]
        public string CourseCreditValue { get; set; }

        [Required]
        [RegularExpression(@"^\d{1,2}?\.?\d{0,5}?$")]
        public string CourseCreditEarned { get; set; }

        [StringLength(10, ErrorMessage = "Cannot exceed 10 characters")]
        public string CourseAcademicGrade { get; set; }

        [StringLength(25, ErrorMessage = "Cannot exceed 25 characters")]
        public string CourseAcademicGradeStatusCode { get; set; }

        [StringLength(12, ErrorMessage = "Cannot exceed 12 characters")]
        public string CourseHonorsCodeType { get; set; }
    }
}

