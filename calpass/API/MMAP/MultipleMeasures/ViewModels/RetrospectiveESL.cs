﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MultipleMeasures.ViewModels
{
    /// <summary>
    /// Model of Retrospective ESL data
    /// 
    /// Notes:
    /// 
    /// <ul>
    ///     <li>Cohort Definition</li>
    ///         <ol>
    ///             <li>All students having attended a California Community College</li>
    ///             <li>Enrolled in one or more credit ESL courses, non-credit ESL courses, credit Reading courses, or credit English courses</li>
    ///             <li>ESL courses defined by the following Taxonomy of Program codes</li>
    ///                 <ul>
    ///                     <li>493084</li>
    ///                     <li>493085</li>
    ///                     <li>493086</li>
    ///                     <li>493087</li>
    ///                     <li>493090</li>
    ///                     <li>493100</li>
    ///                 </ul>
    ///             <li>English courses defined by the following Taxonomy of Program code</li>
    ///                 <ul>
    ///                     <li>150100</li>
    ///                 </ul>
    ///             <li>Reading courses defined by the following Taxonomy of Program code</li>
    ///                 <ul>
    ///                     <li>152000</li>
    ///                 </ul>
    ///         </ol>
    /// 	<li>Variable Identifiers</li>
    /// 		<ul>
    /// 			<li>Origin of record identified by the first and second characters</li>
    /// 					<ul>
    /// 							<li>HS = High School</li>
    /// 							<li>CC = Community College</li>
    /// 							<li>ST = California Standardized Testing and Reporitng (STAR)</li>
    /// 							<li>AP = California Community Colleges Application (CCCApply)</li>
    /// 					</ul>
    /// 			<li>High school grade of student identified by the third and fourth characters</li>
    /// 					<ul>
    /// 							<li>09 = 9th Grade</li>
    /// 							<li>10 = 10th Grade</li>
    /// 							<li>11 = 11th Grade</li>
    /// 							<li>12 = 12th Grade</li>
    /// 							<li>LG = Last Grade Enrolled</li>
    /// 					</ul>
    /// 			<li>Community college subject identified by the third and fourth characters</li>
    ///                 <ul>
    ///                 	<li>EA = English as a Second Language: All</li>
    ///                 		<ul>
    ///                 			<li>Hierarchy</li>
    ///                 			<ol>
    ///                 				<li>Integrated</li>
    ///                 				<li>Writing</li>
    ///                 				<li>Reading</li>
    ///                 				<li>Speaking</li>
    ///                 				<li>Vocational</li>
    ///                 				<li>Citizenship</li>
    ///                 			</ol>
    ///                 		</ul>
    ///                 	<li>EW = English as a Second Language: Writing</li>
    ///                 	<li>ER = English as a Second Language: Reading</li>
    ///                 	<li>ES = English as a Second Language: Speaking</li>
    ///                 	<li>EI = English as a Second Language: Integrated</li>
    ///                 	<li>EC = English as a Second Language: Citizenship</li>
    ///                 	<li>EV = English as a Second Language: Vocational</li>
    ///                 	<li>EN = English</li>
    ///                 	<li>RD = Reading</li>
    ///                 </ul>
    /// 			<li>Community college subject identified by the fifth and sixth characters</li>
    /// 					<ul>
    /// 							<li>CN = Credit or Non-Credit</li>
    /// 							    <ul>
    /// 							        <li>Credit Has Priority</li>
    /// 							    </ul>
    /// 							<li>CR = Credit</li>
    /// 							<li>NC = Non-Credit</li>
    /// 					</ul>
    /// 			<li>Community college course level identified by the seventh and eighth characters</li>
    /// 					<ul>
    /// 							<li>FL = First Level Enrolled in System</li>
    /// 							<li>00 = College-Level</li>
    /// 							<li>01 = One Level Below College-Level</li>
    /// 							<li>02 = Two Levels Below College-Level</li>
    /// 							<li>03 = Three Levels Below College-Level</li>
    /// 							<li>04 = Four Levels Below College-Level</li>
    /// 							<li>05 = Five Levels Below College-Level</li>
    /// 							<li>06 = Six Levels Below College-Level</li>
    /// 							<li>07 = Seven Levels Below College-Level</li>
    /// 							<li>08 = Eight Levels Below College-Level</li>
    /// 					</ul>
    /// 			<li>Community college transfer-level attempt identified by the ninth and tenth characters</li>
    /// 					<ul>
    /// 							<li>2N = Second Attempt</li>
    /// 					</ul>
    /// 		</ul>
    /// 	<li>Record Selection Criteria</li>
    /// 		<ul>
    /// 			<li>When two or more records exist for a high school student in a grade, the represented record is hierarchically selected using:</li>
    /// 					<ul>
    /// 							<li>Most Recent Year</li>
    /// 							<li>Most Recent Term</li>
    /// 							<li>Most Rigorous Course</li>
    /// 							<li>Best Mark Letter</li>
    /// 					</ul>
    /// 			<li>When two or more records exist for a college student in a level, the represented record is hierarchically selected using:</li>
    /// 					<ul>
    /// 							<li>Least Recent Year</li>
    /// 							<li>Least Recent Term</li>
    /// 							<li>Most Rigorous Course</li>
    /// 							<li>Best Mark Letter</li>
    /// 					</ul>
    /// 		</ul>
    /// </ul>
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(DefaultNamingStrategy))]
    public class RetrospectiveESL
    {
        /// <summary>
        /// Internally Generated Student Id
        /// </summary>
        [Key]
        [Column(TypeName = "integer")]
        public int Id { get; set; }

        #region CDE :: 09
        /// <summary>
        /// 09 = 9th Grade
        /// 10 = 10th Grade
        /// 11 = 11th Grade
        /// 12 = 12th Grade
        /// </summary>
        [RegularExpression(@"^(09|10|11|12)$")]
        [Column(TypeName = "char(2)")]
        public string HS09GradeCode { get; set; }

        /// <summary>
        /// Overall Grade Point Average
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS09OverallGradePointAverage { get; set; }

        /// <summary>
        /// Cumulative Overall Grade Point Average
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS09OverallCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// English Grade Point Average
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS09EnglishGradePointAverage { get; set; }

        /// <summary>
        /// Cumulative English Grade Point Average
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS09EnglishCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/si/ds/pubschls.asp'>High School CDS Code</a>
        /// </summary>
        [StringLength(14)]
        [Column(TypeName = "char(14)")]
        public string HS09SchoolCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int HS09YearTermCode { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/sp/cl/systemdocs.asp'>CALPADS Course Code (formerly CBEDS Course Code)</a>
        /// </summary>
        [StringLength(4, MinimumLength = 4)]
        [Column(TypeName = "char(4)")]
        public string HS09CourseCode { get; set; }

        /// <summary>
        /// <a href='https://docs.google.com/spreadsheets/d/1Kk64_gTfpDBuCWe_R3x-aQ1GM-TXNR4HXIwkLqRbjy8/edit?usp=sharing'>Course Ranking of Rigor</a>
        /// </summary>
        [Range(1, 9)]
        [Column(TypeName = "tinyint")]
        public byte? HS09CourseContentRank { get; set; }

        /// <summary>
        /// Course Title
        /// </summary>
        [StringLength(40)]
        [Column(TypeName = "varchar(40)")]
        public string HS09CourseTitle { get; set; }

        /// <summary>
        /// Course Mark Letter
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string HS09CourseMarkLetter { get; set; }

        /// <summary>
        /// Course Mark Points
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? HS09CourseMarkPoints { get; set; }

        /// <summary>
        /// University of California and California State University A-G Code
        /// </summary>
        [StringLength(2)]
        [Column(TypeName = "varchar(2)")]
        public string HS09CourseUniversityAdmissionRequirementCode { get; set; }

        /// <summary>
        /// 30 = Advanced Placement
        /// 31 = Basic
        /// 32 = General
        /// 33 = Gifted and talented
        /// 34 = Other honors
        /// 35 = Remedial
        /// 36 = Special Education
        /// 37 = International Baccalaureate(IB) – higher level
        /// 38 = International Baccalaureate(IB) – standard level
        /// 39 = UC certified honors
        /// 40 = College level
        /// XX = Unknown
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS09CourseLevelCode { get; set; }

        /// <summary>
        /// 30 = ROP or ROC
        /// 31 = Career Technical Education
        /// 32 = Technical Prep
        /// 33 = Technical Prep, and ROP or ROC
        /// XX = None of the Above
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS09CourseTypeCode { get; set; }
        #endregion

        #region CDE :: 10
        /// <summary>
        /// 09 = 9th Grade
        /// 10 = 10th Grade
        /// 11 = 11th Grade
        /// 12 = 12th Grade
        /// </summary>
        [RegularExpression(@"^(09|10|11|12)$")]
        [Column(TypeName = "char(2)")]
        public string HS10GradeCode { get; set; }

        /// <summary>
        /// Overall Grade Point Average of 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS10OverallGradePointAverage { get; set; }

        /// <summary>
        /// Overall Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS10OverallCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// English Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS10EnglishGradePointAverage { get; set; }

        /// <summary>
        /// English Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS10EnglishCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/si/ds/pubschls.asp'>High School CDS Code</a>
        /// </summary>
        [StringLength(14)]
        [Column(TypeName = "char(14)")]
        public string HS10SchoolCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int HS10YearTermCode { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/sp/cl/systemdocs.asp'>CALPADS Course Code (formerly CBEDS Course Code)</a>
        /// </summary>
        [StringLength(4, MinimumLength = 4)]
        [Column(TypeName = "char(4)")]
        public string HS10CourseCode { get; set; }

        /// <summary>
        /// <a href='https://docs.google.com/spreadsheets/d/1Kk64_gTfpDBuCWe_R3x-aQ1GM-TXNR4HXIwkLqRbjy8/edit?usp=sharing'>Course Ranking of Rigor</a>
        /// </summary>
        [Range(1, 9)]
        [Column(TypeName = "tinyint")]
        public byte? HS10CourseContentRank { get; set; }

        /// <summary>
        /// Course Title
        /// </summary>
        [StringLength(40)]
        [Column(TypeName = "varchar(40)")]
        public string HS10CourseTitle { get; set; }

        /// <summary>
        /// Course Mark Letter
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string HS10CourseMarkLetter { get; set; }

        /// <summary>
        /// Course Mark Points
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? HS10CourseMarkPoints { get; set; }

        /// <summary>
        /// University of California and California State University A-G Code
        /// </summary>
        [StringLength(2)]
        [Column(TypeName = "varchar(2)")]
        public string HS10CourseUniversityAdmissionRequirementCode { get; set; }

        /// <summary>
        /// 30 = Advanced Placement
        /// 31 = Basic
        /// 32 = General
        /// 33 = Gifted and talented
        /// 34 = Other honors
        /// 35 = Remedial
        /// 36 = Special Education
        /// 37 = International Baccalaureate(IB) – higher level
        /// 38 = International Baccalaureate(IB) – standard level
        /// 39 = UC certified honors
        /// 40 = College level
        /// XX = Unknown
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS10CourseLevelCode { get; set; }

        /// <summary>
        /// 30 = ROP or ROC
        /// 31 = Career Technical Education
        /// 32 = Technical Prep
        /// 33 = Technical Prep, and ROP or ROC
        /// XX = None of the Above
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS10CourseTypeCode { get; set; }
        #endregion

        #region CDE :: 11
        /// <summary>
        /// 09 = 9th Grade
        /// 10 = 10th Grade
        /// 11 = 11th Grade
        /// 12 = 12th Grade
        /// </summary>
        [RegularExpression(@"^(09|10|11|12)$")]
        [Column(TypeName = "char(2)")]
        public string HS11GradeCode { get; set; }

        /// <summary>
        /// Overall Grade Point Average of 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS11OverallGradePointAverage { get; set; }

        /// <summary>
        /// Overall Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS11OverallCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// English Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS11EnglishGradePointAverage { get; set; }

        /// <summary>
        /// English Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS11EnglishCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/si/ds/pubschls.asp'>High School CDS Code</a>
        /// </summary>
        [StringLength(14)]
        [Column(TypeName = "char(14)")]
        public string HS11SchoolCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int HS11YearTermCode { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/sp/cl/systemdocs.asp'>CALPADS Course Code (formerly CBEDS Course Code)</a>
        /// </summary>
        [StringLength(4, MinimumLength = 4)]
        [Column(TypeName = "char(4)")]
        public string HS11CourseCode { get; set; }

        /// <summary>
        /// <a href='https://docs.google.com/spreadsheets/d/1Kk64_gTfpDBuCWe_R3x-aQ1GM-TXNR4HXIwkLqRbjy8/edit?usp=sharing'>Course Ranking of Rigor</a>
        /// </summary>
        [Range(1, 9)]
        [Column(TypeName = "tinyint")]
        public byte? HS11CourseContentRank { get; set; }

        /// <summary>
        /// Course Title
        /// </summary>
        [StringLength(40)]
        [Column(TypeName = "varchar(40)")]
        public string HS11CourseTitle { get; set; }

        /// <summary>
        /// Course Mark Letter
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string HS11CourseMarkLetter { get; set; }

        /// <summary>
        /// Course Mark Points
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? HS11CourseMarkPoints { get; set; }

        /// <summary>
        /// University of California and California State University A-G Code
        /// </summary>
        [StringLength(2)]
        [Column(TypeName = "varchar(2)")]
        public string HS11CourseUniversityAdmissionRequirementCode { get; set; }

        /// <summary>
        /// 30 = Advanced Placement
        /// 31 = Basic
        /// 32 = General
        /// 33 = Gifted and talented
        /// 34 = Other honors
        /// 35 = Remedial
        /// 36 = Special Education
        /// 37 = International Baccalaureate(IB) – higher level
        /// 38 = International Baccalaureate(IB) – standard level
        /// 39 = UC certified honors
        /// 40 = College level
        /// XX = Unknown
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS11CourseLevelCode { get; set; }

        /// <summary>
        /// 30 = ROP or ROC
        /// 31 = Career Technical Education
        /// 32 = Technical Prep
        /// 33 = Technical Prep, and ROP or ROC
        /// XX = None of the Above
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS11CourseTypeCode { get; set; }
        #endregion

        #region CDE :: 12
        /// <summary>
        /// 09 = 9th Grade
        /// 10 = 10th Grade
        /// 11 = 11th Grade
        /// 12 = 12th Grade
        /// </summary>
        [RegularExpression(@"^(09|10|11|12)$")]
        [Column(TypeName = "char(2)")]
        public string HS12GradeCode { get; set; }

        /// <summary>
        /// Overall Grade Point Average of 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS12OverallGradePointAverage { get; set; }

        /// <summary>
        /// Overall Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS12OverallCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// English Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS12EnglishGradePointAverage { get; set; }

        /// <summary>
        /// English Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS12EnglishCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/si/ds/pubschls.asp'>High School CDS Code</a>
        /// </summary>
        [StringLength(14)]
        [Column(TypeName = "char(14)")]
        public string HS12SchoolCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int HS12YearTermCode { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/sp/cl/systemdocs.asp'>CALPADS Course Code (formerly CBEDS Course Code)</a>
        /// </summary>
        [StringLength(4, MinimumLength = 4)]
        [Column(TypeName = "char(4)")]
        public string HS12CourseCode { get; set; }

        /// <summary>
        /// <a href='https://docs.google.com/spreadsheets/d/1Kk64_gTfpDBuCWe_R3x-aQ1GM-TXNR4HXIwkLqRbjy8/edit?usp=sharing'>Course Ranking of Rigor</a>
        /// </summary>
        [Range(1, 9)]
        [Column(TypeName = "tinyint")]
        public byte? HS12CourseContentRank { get; set; }

        /// <summary>
        /// Course Title
        /// </summary>
        [StringLength(40)]
        [Column(TypeName = "varchar(40)")]
        public string HS12CourseTitle { get; set; }

        /// <summary>
        /// Course Mark Letter
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string HS12CourseMarkLetter { get; set; }

        /// <summary>
        /// Course Mark Points
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? HS12CourseMarkPoints { get; set; }

        /// <summary>
        /// University of California and California State University A-G Code
        /// </summary>
        [StringLength(2)]
        [Column(TypeName = "varchar(2)")]
        public string HS12CourseUniversityAdmissionRequirementCode { get; set; }

        /// <summary>
        /// 30 = Advanced Placement
        /// 31 = Basic
        /// 32 = General
        /// 33 = Gifted and talented
        /// 34 = Other honors
        /// 35 = Remedial
        /// 36 = Special Education
        /// 37 = International Baccalaureate(IB) – higher level
        /// 38 = International Baccalaureate(IB) – standard level
        /// 39 = UC certified honors
        /// 40 = College level
        /// XX = Unknown
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS12CourseLevelCode { get; set; }

        /// <summary>
        /// 30 = ROP or ROC
        /// 31 = Career Technical Education
        /// 32 = Technical Prep
        /// 33 = Technical Prep, and ROP or ROC
        /// XX = None of the Above
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS12CourseTypeCode { get; set; }
        #endregion

        #region CDE :: LG
        /// <summary>
        /// 09 = 9th Grade
        /// 10 = 10th Grade
        /// 11 = 11th Grade
        /// 12 = 12th Grade
        /// </summary>
        [RegularExpression(@"^(09|10|11|12)$")]
        [Column(TypeName = "char(2)")]
        public string HSLGGradeCode { get; set; }

        /// <summary>
        /// Overall Grade Point Average of 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HSLGOverallGradePointAverage { get; set; }

        /// <summary>
        /// Overall Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HSLGOverallCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// English Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HSLGEnglishGradePointAverage { get; set; }

        /// <summary>
        /// English Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HSLGEnglishCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/si/ds/pubschls.asp'>High School CDS Code</a>
        /// </summary>
        [StringLength(14)]
        [Column(TypeName = "char(14)")]
        public string HSLGSchoolCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int HSLGYearTermCode { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/sp/cl/systemdocs.asp'>CALPADS Course Code (formerly CBEDS Course Code)</a>
        /// </summary>
        [StringLength(4, MinimumLength = 4)]
        [Column(TypeName = "char(4)")]
        public string HSLGCourseCode { get; set; }

        /// <summary>
        /// <a href='https://docs.google.com/spreadsheets/d/1Kk64_gTfpDBuCWe_R3x-aQ1GM-TXNR4HXIwkLqRbjy8/edit?usp=sharing'>Course Ranking of Rigor</a>
        /// </summary>
        [Range(1, 9)]
        [Column(TypeName = "tinyint")]
        public byte? HSLGCourseContentRank { get; set; }

        /// <summary>
        /// Course Title
        /// </summary>
        [StringLength(40)]
        [Column(TypeName = "varchar(40)")]
        public string HSLGCourseTitle { get; set; }

        /// <summary>
        /// Course Mark Letter
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string HSLGCourseMarkLetter { get; set; }

        /// <summary>
        /// Course Mark Points
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? HSLGCourseMarkPoints { get; set; }

        /// <summary>
        /// University of California and California State University A-G Code
        /// </summary>
        [StringLength(2)]
        [Column(TypeName = "varchar(2)")]
        public string HSLGCourseUniversityAdmissionRequirementCode { get; set; }

        /// <summary>
        /// 30 = Advanced Placement
        /// 31 = Basic
        /// 32 = General
        /// 33 = Gifted and talented
        /// 34 = Other honors
        /// 35 = Remedial
        /// 36 = Special Education
        /// 37 = International Baccalaureate(IB) – higher level
        /// 38 = International Baccalaureate(IB) – standard level
        /// 39 = UC certified honors
        /// 40 = College level
        /// XX = Unknown
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HSLGCourseLevelCode { get; set; }

        /// <summary>
        /// 30 = ROP or ROC
        /// 31 = Career Technical Education
        /// 32 = Technical Prep
        /// 33 = Technical Prep, and ROP or ROC
        /// XX = None of the Above
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HSLGCourseTypeCode { get; set; }
        #endregion

        #region COMIS :: ESL :: ALL :: CN :: FL
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEACNFLCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEACNFLYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEACNFLCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEACNFLCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACNFLCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEACNFLCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEACNFLCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEACNFLCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEACNFLCourseLevelCode { get; set; }

        /// <summary>
        /// 'CR' = Credit
        /// 'NC' = Non-Credit
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string CCEACNFLCourseCreditCode { get; set; }

        /// <summary>
        /// Summary of Enrollment in ESL Taxonomy of Program Codes for the Given Term
        /// Each Position identifies a specific Program:
        /// Position 1 = Integrated
        /// Position 2 = Writing
        /// Position 3 = Reading
        /// Position 4 = Speaking
        /// Position 5 = Vocational
        /// Position 6 = Citizenship
        /// </summary>[StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACNFLSummary { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACNFLEslUnits { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACNFLEnglUnits { get; set; }
        #endregion

        #region COMIS :: ESL :: ALL :: CN :: 00
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEACN00CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEACN00YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEACN00CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEACN00CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN00CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEACN00CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEACN00CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEACN00CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEACN00CourseLevelCode { get; set; }

        /// <summary>
        /// 'CR' = Credit
        /// 'NC' = Non-Credit
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string CCEACN00CourseCreditCode { get; set; }

        /// <summary>
        /// Summary of Enrollment in ESL Taxonomy of Program Codes for the Given Term
        /// Each Position identifies a specific Program:
        /// Position 1 = Integrated
        /// Position 2 = Writing
        /// Position 3 = Reading
        /// Position 4 = Speaking
        /// Position 5 = Vocational
        /// Position 6 = Civics
        /// </summary>[StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN00Summary { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN00EslUnits { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN00EnglUnits { get; set; }
        #endregion

        #region COMIS :: ESL :: ALL :: CN :: 01
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEACN01CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEACN01YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEACN01CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEACN01CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN01CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEACN01CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEACN01CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEACN01CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEACN01CourseLevelCode { get; set; }

        /// <summary>
        /// 'CR' = Credit
        /// 'NC' = Non-Credit
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string CCEACN01CourseCreditCode { get; set; }

        /// <summary>
        /// Summary of Enrollment in ESL Taxonomy of Program Codes for the Given Term
        /// Each Position identifies a specific Program:
        /// Position 1 = Integrated
        /// Position 2 = Writing
        /// Position 3 = Reading
        /// Position 4 = Speaking
        /// Position 5 = Vocational
        /// Position 6 = Civics
        /// </summary>[StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN01Summary { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN01EslUnits { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN01EnglUnits { get; set; }
        #endregion

        #region COMIS :: ESL :: ALL :: CN :: 02
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi02.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEACN02CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEACN02YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEACN02CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEACN02CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN02CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEACN02CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEACN02CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEACN02CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEACN02CourseLevelCode { get; set; }

        /// <summary>
        /// 'CR' = Credit
        /// 'NC' = Non-Credit
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string CCEACN02CourseCreditCode { get; set; }

        /// <summary>
        /// Summary of Enrollment in ESL Taxonomy of Program Codes for the Given Term
        /// Each Position identifies a specific Program:
        /// Position 1 = Integrated
        /// Position 2 = Writing
        /// Position 3 = Reading
        /// Position 4 = Speaking
        /// Position 5 = Vocational
        /// Position 6 = Civics
        /// </summary>[StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN02Summary { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN02EslUnits { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN02EnglUnits { get; set; }
        #endregion

        #region COMIS :: ESL :: ALL :: CN :: 03
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEACN03CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEACN03YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEACN03CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEACN03CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN03CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEACN03CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEACN03CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEACN03CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEACN03CourseLevelCode { get; set; }

        /// <summary>
        /// 'CR' = Credit
        /// 'NC' = Non-Credit
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string CCEACN03CourseCreditCode { get; set; }

        /// <summary>
        /// Summary of Enrollment in ESL Taxonomy of Program Codes for the Given Term
        /// Each Position identifies a specific Program:
        /// Position 1 = Integrated
        /// Position 2 = Writing
        /// Position 3 = Reading
        /// Position 4 = Speaking
        /// Position 5 = Vocational
        /// Position 6 = Civics
        /// </summary>[StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN03Summary { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN03EslUnits { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN03EnglUnits { get; set; }
        #endregion

        #region COMIS :: ESL :: ALL :: CN :: 04
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEACN04CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEACN04YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEACN04CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEACN04CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN04CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEACN04CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEACN04CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEACN04CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEACN04CourseLevelCode { get; set; }

        /// <summary>
        /// 'CR' = Credit
        /// 'NC' = Non-Credit
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string CCEACN04CourseCreditCode { get; set; }

        /// <summary>
        /// Summary of Enrollment in ESL Taxonomy of Program Codes for the Given Term
        /// Each Position identifies a specific Program:
        /// Position 1 = Integrated
        /// Position 2 = Writing
        /// Position 3 = Reading
        /// Position 4 = Speaking
        /// Position 5 = Vocational
        /// Position 6 = Civics
        /// </summary>[StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN04Summary { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN04EslUnits { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN04EnglUnits { get; set; }
        #endregion

        #region COMIS :: ESL :: ALL :: CN :: 05
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEACN05CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEACN05YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEACN05CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEACN05CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN05CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEACN05CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEACN05CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEACN05CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEACN05CourseLevelCode { get; set; }

        /// <summary>
        /// 'CR' = Credit
        /// 'NC' = Non-Credit
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string CCEACN05CourseCreditCode { get; set; }

        /// <summary>
        /// Summary of Enrollment in ESL Taxonomy of Program Codes for the Given Term
        /// Each Position identifies a specific Program:
        /// Position 1 = Integrated
        /// Position 2 = Writing
        /// Position 3 = Reading
        /// Position 4 = Speaking
        /// Position 5 = Vocational
        /// Position 6 = Civics
        /// </summary>[StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN05Summary { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN05EslUnits { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN05EnglUnits { get; set; }
        #endregion

        #region COMIS :: ESL :: ALL :: CN :: 06
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEACN06CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEACN06YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEACN06CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEACN06CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN06CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEACN06CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEACN06CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEACN06CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEACN06CourseLevelCode { get; set; }

        /// <summary>
        /// 'CR' = Credit
        /// 'NC' = Non-Credit
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string CCEACN06CourseCreditCode { get; set; }

        /// <summary>
        /// Summary of Enrollment in ESL Taxonomy of Program Codes for the Given Term
        /// Each Position identifies a specific Program:
        /// Position 1 = Integrated
        /// Position 2 = Writing
        /// Position 3 = Reading
        /// Position 4 = Speaking
        /// Position 5 = Vocational
        /// Position 6 = Civics
        /// </summary>[StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN06Summary { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN06EslUnits { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN06EnglUnits { get; set; }
        #endregion

        #region COMIS :: ESL :: ALL :: CN :: 07
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEACN07CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEACN07YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEACN07CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEACN07CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN07CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEACN07CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEACN07CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEACN07CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEACN07CourseLevelCode { get; set; }

        /// <summary>
        /// 'CR' = Credit
        /// 'NC' = Non-Credit
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string CCEACN07CourseCreditCode { get; set; }

        /// <summary>
        /// Summary of Enrollment in ESL Taxonomy of Program Codes for the Given Term
        /// Each Position identifies a specific Program:
        /// Position 1 = Integrated
        /// Position 2 = Writing
        /// Position 3 = Reading
        /// Position 4 = Speaking
        /// Position 5 = Vocational
        /// Position 6 = Civics
        /// </summary>[StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN07Summary { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN07EslUnits { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN07EnglUnits { get; set; }
        #endregion

        #region COMIS :: ESL :: ALL :: CN :: 08
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEACN08CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEACN08YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEACN08CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEACN08CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN08CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEACN08CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEACN08CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEACN08CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEACN08CourseLevelCode { get; set; }

        /// <summary>
        /// 'CR' = Credit
        /// 'NC' = Non-Credit
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string CCEACN08CourseCreditCode { get; set; }

        /// <summary>
        /// Summary of Enrollment in ESL Taxonomy of Program Codes for the Given Term
        /// Each Position identifies a specific Program:
        /// Position 1 = Integrated
        /// Position 2 = Writing
        /// Position 3 = Reading
        /// Position 4 = Speaking
        /// Position 5 = Vocational
        /// Position 6 = Civics
        /// </summary>[StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEACN08Summary { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN08EslUnits { get; set; }

        /// <summary>
        /// Total ESL Units
        /// </summary>
        [Range(0, 9999)]
        [Column(TypeName = "decimal(6,2)")]
        public decimal? CCEACN08EnglUnits { get; set; }
        #endregion

        #region COMIS :: ESL :: WRITING :: CR :: FL
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEWCRFLCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEWCRFLYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEWCRFLCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEWCRFLCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEWCRFLCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEWCRFLCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEWCRFLCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEWCRFLCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEWCRFLCourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: WRITING :: CR :: 00
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEWCR00CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEWCR00YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEWCR00CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEWCR00CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEWCR00CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEWCR00CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEWCR00CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEWCR00CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEWCR00CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: WRITING :: CR :: 01
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEWCR01CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEWCR01YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEWCR01CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEWCR01CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEWCR01CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEWCR01CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEWCR01CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEWCR01CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEWCR01CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: WRITING :: CR :: 02
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi02.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEWCR02CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEWCR02YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEWCR02CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEWCR02CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEWCR02CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEWCR02CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEWCR02CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEWCR02CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEWCR02CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: WRITING :: CR :: 03
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEWCR03CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEWCR03YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEWCR03CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEWCR03CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEWCR03CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEWCR03CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEWCR03CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEWCR03CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEWCR03CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: WRITING :: CR :: 04
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEWCR04CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEWCR04YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEWCR04CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEWCR04CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEWCR04CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEWCR04CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEWCR04CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEWCR04CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEWCR04CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: WRITING :: CR :: 05
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEWCR05CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEWCR05YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEWCR05CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEWCR05CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEWCR05CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEWCR05CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEWCR05CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEWCR05CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEWCR05CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: WRITING :: CR :: 06
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEWCR06CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEWCR06YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEWCR06CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEWCR06CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEWCR06CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEWCR06CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEWCR06CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEWCR06CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEWCR06CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: READING :: CR :: FL
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCERCRFLCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCERCRFLYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCERCRFLCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCERCRFLCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCERCRFLCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCERCRFLCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCERCRFLCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCERCRFLCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCERCRFLCourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: READING :: CR :: 00
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCERCR00CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCERCR00YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCERCR00CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCERCR00CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCERCR00CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCERCR00CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCERCR00CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCERCR00CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCERCR00CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: READING :: CR :: 01
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCERCR01CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCERCR01YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCERCR01CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCERCR01CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCERCR01CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCERCR01CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCERCR01CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCERCR01CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCERCR01CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: READING :: CR :: 02
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi02.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCERCR02CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCERCR02YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCERCR02CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCERCR02CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCERCR02CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCERCR02CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCERCR02CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCERCR02CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCERCR02CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: READING :: CR :: 03
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCERCR03CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCERCR03YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCERCR03CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCERCR03CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCERCR03CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCERCR03CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCERCR03CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCERCR03CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCERCR03CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: READING :: CR :: 04
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCERCR04CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCERCR04YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCERCR04CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCERCR04CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCERCR04CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCERCR04CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCERCR04CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCERCR04CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCERCR04CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: READING :: CR :: 05
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCERCR05CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCERCR05YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCERCR05CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCERCR05CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCERCR05CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCERCR05CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCERCR05CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCERCR05CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCERCR05CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: READING :: CR :: 06
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCERCR06CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCERCR06YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCERCR06CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCERCR06CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCERCR06CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCERCR06CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCERCR06CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCERCR06CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCERCR06CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: SPEAKING :: CR :: FL
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCESCRFLCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCESCRFLYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCESCRFLCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCESCRFLCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCESCRFLCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCESCRFLCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCESCRFLCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCESCRFLCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCESCRFLCourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: SPEAKING :: CR :: 00
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCESCR00CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCESCR00YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCESCR00CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCESCR00CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCESCR00CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCESCR00CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCESCR00CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCESCR00CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCESCR00CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: SPEAKING :: CR :: 01
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCESCR01CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCESCR01YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCESCR01CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCESCR01CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCESCR01CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCESCR01CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCESCR01CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCESCR01CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCESCR01CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: SPEAKING :: CR :: 02
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi02.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCESCR02CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCESCR02YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCESCR02CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCESCR02CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCESCR02CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCESCR02CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCESCR02CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCESCR02CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCESCR02CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: SPEAKING :: CR :: 03
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCESCR03CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCESCR03YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCESCR03CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCESCR03CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCESCR03CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCESCR03CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCESCR03CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCESCR03CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCESCR03CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: SPEAKING :: CR :: 04
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCESCR04CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCESCR04YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCESCR04CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCESCR04CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCESCR04CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCESCR04CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCESCR04CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCESCR04CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCESCR04CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: SPEAKING :: CR :: 05
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCESCR05CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCESCR05YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCESCR05CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCESCR05CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCESCR05CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCESCR05CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCESCR05CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCESCR05CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCESCR05CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: SPEAKING :: CR :: 06
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCESCR06CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCESCR06YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCESCR06CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCESCR06CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCESCR06CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCESCR06CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCESCR06CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCESCR06CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCESCR06CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: CR :: FL
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEICRFLCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEICRFLYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEICRFLCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEICRFLCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEICRFLCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEICRFLCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEICRFLCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEICRFLCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEICRFLCourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: CR :: 00
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEICR00CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEICR00YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEICR00CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEICR00CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEICR00CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEICR00CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEICR00CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEICR00CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEICR00CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: CR :: 01
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEICR01CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEICR01YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEICR01CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEICR01CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEICR01CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEICR01CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEICR01CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEICR01CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEICR01CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: CR :: 02
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi02.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEICR02CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEICR02YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEICR02CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEICR02CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEICR02CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEICR02CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEICR02CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEICR02CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEICR02CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: CR :: 03
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEICR03CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEICR03YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEICR03CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEICR03CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEICR03CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEICR03CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEICR03CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEICR03CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEICR03CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: CR :: 04
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEICR04CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEICR04YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEICR04CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEICR04CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEICR04CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEICR04CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEICR04CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEICR04CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEICR04CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: CR :: 05
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEICR05CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEICR05YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEICR05CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEICR05CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEICR05CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEICR05CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEICR05CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEICR05CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEICR05CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: CR :: 06
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEICR06CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEICR06YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEICR06CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEICR06CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEICR06CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEICR06CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEICR06CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEICR06CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEICR06CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: WRITING :: NC :: FL
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEWNCFLCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEWNCFLYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEWNCFLCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEWNCFLCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEWNCFLCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEWNCFLCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEWNCFLCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEWNCFLCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEWNCFLCourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: WRITING :: NC :: 01
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEWNC01CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEWNC01YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEWNC01CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEWNC01CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEWNC01CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEWNC01CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEWNC01CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEWNC01CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEWNC01CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: WRITING :: NC :: 02
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi02.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEWNC02CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEWNC02YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEWNC02CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEWNC02CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEWNC02CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEWNC02CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEWNC02CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEWNC02CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEWNC02CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: WRITING :: NC :: 03
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEWNC03CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEWNC03YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEWNC03CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEWNC03CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEWNC03CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEWNC03CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEWNC03CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEWNC03CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEWNC03CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: WRITING :: NC :: 04
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEWNC04CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEWNC04YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEWNC04CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEWNC04CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEWNC04CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEWNC04CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEWNC04CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEWNC04CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEWNC04CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: WRITING :: NC :: 05
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEWNC05CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEWNC05YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEWNC05CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEWNC05CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEWNC05CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEWNC05CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEWNC05CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEWNC05CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEWNC05CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: WRITING :: NC :: 06
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEWNC06CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEWNC06YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEWNC06CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEWNC06CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEWNC06CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEWNC06CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEWNC06CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEWNC06CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEWNC06CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: READING :: NC :: FL
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCERNCFLCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCERNCFLYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCERNCFLCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCERNCFLCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCERNCFLCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCERNCFLCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCERNCFLCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCERNCFLCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCERNCFLCourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: READING :: NC :: 01
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCERNC01CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCERNC01YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCERNC01CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCERNC01CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCERNC01CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCERNC01CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCERNC01CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCERNC01CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCERNC01CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: READING :: NC :: 02
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi02.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCERNC02CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCERNC02YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCERNC02CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCERNC02CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCERNC02CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCERNC02CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCERNC02CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCERNC02CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCERNC02CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: READING :: NC :: 03
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCERNC03CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCERNC03YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCERNC03CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCERNC03CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCERNC03CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCERNC03CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCERNC03CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCERNC03CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCERNC03CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: READING :: NC :: 04
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCERNC04CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCERNC04YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCERNC04CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCERNC04CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCERNC04CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCERNC04CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCERNC04CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCERNC04CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCERNC04CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: READING :: NC :: 05
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCERNC05CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCERNC05YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCERNC05CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCERNC05CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCERNC05CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCERNC05CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCERNC05CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCERNC05CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCERNC05CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: READING :: NC :: 06
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCERNC06CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCERNC06YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCERNC06CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCERNC06CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCERNC06CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCERNC06CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCERNC06CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCERNC06CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCERNC06CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: SPEAKING :: NC :: FL
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCESNCFLCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCESNCFLYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCESNCFLCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCESNCFLCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCESNCFLCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCESNCFLCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCESNCFLCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCESNCFLCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCESNCFLCourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: SPEAKING :: NC :: 01
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCESNC01CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCESNC01YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCESNC01CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCESNC01CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCESNC01CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCESNC01CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCESNC01CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCESNC01CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCESNC01CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: SPEAKING :: NC :: 02
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi02.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCESNC02CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCESNC02YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCESNC02CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCESNC02CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCESNC02CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCESNC02CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCESNC02CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCESNC02CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCESNC02CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: SPEAKING :: NC :: 03
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCESNC03CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCESNC03YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCESNC03CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCESNC03CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCESNC03CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCESNC03CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCESNC03CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCESNC03CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCESNC03CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: SPEAKING :: NC :: 04
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCESNC04CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCESNC04YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCESNC04CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCESNC04CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCESNC04CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCESNC04CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCESNC04CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCESNC04CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCESNC04CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: SPEAKING :: NC :: 05
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCESNC05CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCESNC05YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCESNC05CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCESNC05CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCESNC05CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCESNC05CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCESNC05CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCESNC05CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCESNC05CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: SPEAKING :: NC :: 06
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCESNC06CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCESNC06YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCESNC06CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCESNC06CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCESNC06CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCESNC06CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCESNC06CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCESNC06CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCESNC06CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: NC :: FL
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEINCFLCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEINCFLYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEINCFLCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEINCFLCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEINCFLCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEINCFLCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEINCFLCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEINCFLCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEINCFLCourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: NC :: 01
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEINC01CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEINC01YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEINC01CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEINC01CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEINC01CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEINC01CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEINC01CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEINC01CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEINC01CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: NC :: 02
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi02.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEINC02CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEINC02YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEINC02CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEINC02CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEINC02CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEINC02CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEINC02CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEINC02CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEINC02CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: NC :: 03
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEINC03CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEINC03YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEINC03CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEINC03CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEINC03CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEINC03CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEINC03CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEINC03CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEINC03CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: NC :: 04
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEINC04CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEINC04YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEINC04CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEINC04CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEINC04CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEINC04CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEINC04CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEINC04CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEINC04CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: NC :: 05
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEINC05CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEINC05YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEINC05CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEINC05CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEINC05CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEINC05CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEINC05CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEINC05CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEINC05CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: NC :: 06
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEINC06CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEINC06YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEINC06CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEINC06CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEINC06CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEINC06CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEINC06CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEINC06CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEINC06CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: NC :: 07
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEINC07CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEINC07YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEINC07CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEINC07CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEINC07CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEINC07CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEINC07CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEINC07CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEINC07CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: INTEGRATED :: NC :: 08
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEINC08CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEINC08YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEINC08CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEINC08CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEINC08CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEINC08CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEINC08CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEINC08CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEINC08CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: CITIZENSHIP :: NC :: FL
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCECNCFLCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCECNCFLYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCECNCFLCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCECNCFLCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCECNCFLCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCECNCFLCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCECNCFLCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCECNCFLCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCECNCFLCourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: CITIZENSHIP :: NC :: 00
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCECNC00CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCECNC00YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCECNC00CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCECNC00CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCECNC00CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCECNC00CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCECNC00CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCECNC00CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCECNC00CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: CITIZENSHIP :: NC :: 01
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCECNC01CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCECNC01YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCECNC01CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCECNC01CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCECNC01CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCECNC01CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCECNC01CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCECNC01CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCECNC01CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: CITIZENSHIP :: NC :: 02
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi02.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCECNC02CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCECNC02YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCECNC02CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCECNC02CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCECNC02CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCECNC02CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCECNC02CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCECNC02CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCECNC02CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: CITIZENSHIP :: NC :: 03
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCECNC03CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCECNC03YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCECNC03CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCECNC03CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCECNC03CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCECNC03CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCECNC03CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCECNC03CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCECNC03CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: CITIZENSHIP :: NC :: 04
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCECNC04CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCECNC04YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCECNC04CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCECNC04CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCECNC04CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCECNC04CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCECNC04CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCECNC04CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCECNC04CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: CITIZENSHIP :: NC :: 05
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCECNC05CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCECNC05YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCECNC05CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCECNC05CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCECNC05CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCECNC05CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCECNC05CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCECNC05CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCECNC05CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: CITIZENSHIP :: NC :: 06
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCECNC06CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCECNC06YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCECNC06CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCECNC06CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCECNC06CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCECNC06CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCECNC06CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCECNC06CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCECNC06CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: CITIZENSHIP :: NC :: 07
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCECNC07CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCECNC07YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCECNC07CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCECNC07CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCECNC07CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCECNC07CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCECNC07CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCECNC07CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCECNC07CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: CITIZENSHIP :: NC :: 08
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCECNC08CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCECNC08YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCECNC08CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCECNC08CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCECNC08CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCECNC08CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCECNC08CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCECNC08CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCECNC08CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: VOCATIONAL :: NC :: FL
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEVNCFLCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEVNCFLYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEVNCFLCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEVNCFLCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEVNCFLCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEVNCFLCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEVNCFLCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEVNCFLCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEVNCFLCourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: VOCATIONAL :: NC :: 00
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEVNC00CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEVNC00YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEVNC00CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEVNC00CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEVNC00CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEVNC00CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEVNC00CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEVNC00CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEVNC00CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: VOCATIONAL :: NC :: 01
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEVNC01CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEVNC01YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEVNC01CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEVNC01CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEVNC01CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEVNC01CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEVNC01CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEVNC01CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEVNC01CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: VOCATIONAL :: NC :: 02
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi02.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEVNC02CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEVNC02YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEVNC02CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEVNC02CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEVNC02CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEVNC02CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEVNC02CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEVNC02CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEVNC02CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: VOCATIONAL :: NC :: 03
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEVNC03CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEVNC03YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEVNC03CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEVNC03CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEVNC03CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEVNC03CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEVNC03CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEVNC03CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEVNC03CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: VOCATIONAL :: NC :: 04
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEVNC04CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEVNC04YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEVNC04CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEVNC04CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEVNC04CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEVNC04CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEVNC04CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEVNC04CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEVNC04CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: VOCATIONAL :: NC :: 05
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEVNC05CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi05.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEVNC05YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEVNC05CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEVNC05CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEVNC05CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEVNC05CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEVNC05CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb05.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEVNC05CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEVNC05CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ESL :: VOCATIONAL :: NC :: 06
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCEVNC06CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi06.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCEVNC06YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCEVNC06CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCEVNC06CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCEVNC06CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCEVNC06CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCEVNC06CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb06.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCEVNC06CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCEVNC06CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ENGL :: CR :: FL
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCENCRFLCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCENCRFLYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCENCRFLCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCENCRFLCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCENCRFLCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCENCRFLCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCENCRFLCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCENCRFLCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCENCRFLCourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ENGL :: CR :: 00
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCENCR00CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCENCR00YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCENCR00CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCENCR00CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCENCR00CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCENCR00CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCENCR00CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCENCR00CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCENCR00CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ENGL :: CR :: 00 :: SECOND ATTEMPT
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCENCR002NCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCENCR002NYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCENCR002NCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCENCR002NCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCENCR002NCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCENCR002NCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCENCR002NCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCENCR002NCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCENCR002NCourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ENGL :: CR :: 01
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCENCR01CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCENCR01YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCENCR01CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCENCR01CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCENCR01CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCENCR01CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCENCR01CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCENCR01CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCENCR01CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ENGL :: CR :: 02
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi02.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCENCR02CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCENCR02YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCENCR02CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCENCR02CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCENCR02CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCENCR02CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCENCR02CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCENCR02CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCENCR02CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ENGL :: CR :: 03
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCENCR03CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCENCR03YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCENCR03CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCENCR03CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCENCR03CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCENCR03CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCENCR03CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCENCR03CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCENCR03CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: ENGL :: CR :: 04
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCENCR04CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCENCR04YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCENCR04CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCENCR04CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCENCR04CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCENCR04CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCENCR04CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCENCR04CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCENCR04CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: READ :: CR :: FL
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCRDCRFLCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCRDCRFLYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCRDCRFLCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCRDCRFLCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCRDCRFLCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCRDCRFLCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCRDCRFLCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCRDCRFLCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCRDCRFLCourseLevelCode { get; set; }
        #endregion

        #region COMIS :: READ :: CR :: 00
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCRDCR00CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCRDCR00YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCRDCR00CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCRDCR00CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCRDCR00CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCRDCR00CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCRDCR00CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCRDCR00CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCRDCR00CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: READ :: CR :: 01
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCRDCR01CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCRDCR01YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCRDCR01CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCRDCR01CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCRDCR01CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCRDCR01CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCRDCR01CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCRDCR01CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCRDCR01CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: READ :: CR :: 02
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi02.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCRDCR02CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCRDCR02YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCRDCR02CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCRDCR02CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCRDCR02CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCRDCR02CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCRDCR02CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCRDCR02CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCRDCR02CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: READ :: CR :: 03
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCRDCR03CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCRDCR03YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCRDCR03CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCRDCR03CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCRDCR03CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCRDCR03CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCRDCR03CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCRDCR03CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCRDCR03CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: READ :: CR :: 04
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 5)]
        [Column(TypeName = "char(3)")]
        public string CCRDCR04CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCRDCR04YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCRDCR04CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCRDCR04CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCRDCR04CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCRDCR04CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCRDCR04CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCRDCR04CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCRDCR04CourseLevelCode { get; set; }
        #endregion

        #region STAR
        /// <summary>
        /// True = Ready for California State University (CSU) or Participating California Community Colleges (CCC) College-Level English Courses
        /// False = Not Ready
        /// </summary>
        public bool? STIsEnglishEap { get; set; }

        /// <summary>
        /// English Scaled Score
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STEnglishScaledScore { get; set; }

        /// <summary>
        /// English Cluster 1 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STEnglishCluster1 { get; set; }

        /// <summary>
        /// English Cluster 1 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STEnglishCluster2 { get; set; }

        /// <summary>
        /// English Cluster 1 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STEnglishCluster3 { get; set; }

        /// <summary>
        /// English Cluster 1 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STEnglishCluster4 { get; set; }

        /// <summary>
        /// English Cluster 1 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STEnglishCluster5 { get; set; }

        /// <summary>
        /// English Cluster 1 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STEnglishCluster6 { get; set; }

        /// <summary>
        /// 0 = Grade-level mathematics (grade 7); default if unknown
        /// 1 = General Mathematics (grades 8 and 9)
        /// 2 = Summative High School Mathematics (grades 9–11)
        /// 3 = Algebra I
        /// 4 = Integrated Mathematics 1
        /// 5 = Geometry
        /// 6 = Integrated Mathematics 2
        /// 7 = Algebra II
        /// 8 = Integrated Mathematics 3
        /// 9 = Unknown (mathematics test in grades 8–11 was taken)
        /// </summary>
        [Range(0, 9)]
        [Column(TypeName = "tinyint")]
        public byte? STMathematicsSubject { get; set; }

        /// <summary>
        /// True = Ready for California State University (CSU) or Participating California Community Colleges (CCC) College-Level Mathematics Courses
        /// False = Not Ready
        /// </summary>
        public bool? STIsMathematicsEap { get; set; }

        /// <summary>
        /// Mathematics Scaled Score
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STMathematicsScaledScore { get; set; }

        /// <summary>
        /// Mathematics Cluster 6 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STMathematicsCluster1 { get; set; }

        /// <summary>
        /// Mathematics Cluster 6 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STMathematicsCluster2 { get; set; }

        /// <summary>
        /// Mathematics Cluster 6 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STMathematicsCluster3 { get; set; }

        /// <summary>
        /// Mathematics Cluster 6 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STMathematicsCluster4 { get; set; }

        /// <summary>
        /// Mathematics Cluster 6 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STMathematicsCluster5 { get; set; }

        /// <summary>
        /// Mathematics Cluster 6 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STMathematicsCluster6 { get; set; }

        /// <summary>
        /// English Only
        /// </summary>
        public bool? STIsEnglishOnly { get; set; }

        /// <summary>
        /// English Learner
        /// </summary>
        public bool? STIsEnglishLearner { get; set; }

        /// <summary>
        /// Initial Fluent English Proficient
        /// </summary>
        public bool? STIsFluentInitially { get; set; }

        /// <summary>
        /// Reclassified Fluent English Proficient
        /// </summary>
        public bool? STIsFluentReclassified { get; set; }
        #endregion

        #region CDE :: Demograhpic
        /// <summary>
        /// Enrolled in a Course Code of 2100 at any time during high school
        /// </summary>
        public bool HSIsRemedial { get; set; }

        /// <summary>
        /// Date (YYYYMMDD) of High School Graduation
        /// </summary>
        [StringLength(8, MinimumLength = 8)]
        [Column(TypeName = "char(8)")]
        public string HSGraduationDate { get; set; }
        #endregion

        #region COMIS :: Demographic
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/STD/STD1.pdf'>Age</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public byte CCAge { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SB/SB04.pdf'>Gender</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CCGender { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SB/SB06.pdf'>Citizenship</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CCCitzenship { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SB/SB09.pdf'>Residency</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [StringLength(5, MinimumLength = 5)]
        [Column(TypeName = "char(5)")]
        public string CCResidency { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SB/SB11.pdf'>Education Status</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [StringLength(5, MinimumLength = 5)]
        [Column(TypeName = "char(5)")]
        public string CCEducationStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SB/SB15.pdf'>Enrollment Status</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CCEnrollmentStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SB/SB29.pdf'>Ethnicities</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [StringLength(21, MinimumLength = 21)]
        [Column(TypeName = "char(21)")]
        public string CCEthnicities { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SB/SB33.pdf'>Parent Education Level</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [StringLength(21, MinimumLength = 21)]
        [Column(TypeName = "char(21)")]
        public string CCParentEducationLevel { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/STD/STD10.pdf'>Ethnicity</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public byte CCEthnicity { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG01.pdf'>Military Status</a>
        /// </summary>
        [StringLength(4, MinimumLength = 4)]
        [Column(TypeName = "char(4)")]
        public string MilitaryStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG02.pdf'>Military Dependent Status</a>
        /// </summary>
        [StringLength(4, MinimumLength = 4)]
        [Column(TypeName = "char(4)")]
        public string MilitaryDependentStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG03.pdf'>Foster Youth Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string FosterYouthStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG04.pdf'>Incarcerated Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string IncarceratedStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG05.pdf'>MESA ASEM Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string MESAASEMStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG06.pdf'>Puente Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string PuenteStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG07.pdf'>MCHS ECHS Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string MCHSECHSStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG08.pdf'>UMOJA Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string UMOJAStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG10.pdf'>CAA Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CAAStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG11.pdf'>CAFYES Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CAFYESStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG12.pdf'>Baccalaureate Program</a>
        /// </summary>
        [StringLength(5, MinimumLength = 5)]
        [Column(TypeName = "char(5)")]
        public string BaccalaureateProgram { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG13.pdf'>CCAP Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CCAPStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG14.pdf'>Economically Disadv Status</a>
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string EconomicallyDisadvStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG15.pdf'>Ex Offender Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string ExOffenderStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG16.pdf'>Homeless Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string HomelessStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG17.pdf'>Longterm Unemploy Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string LongtermUnemployStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG18.pdf'>Cultural Barrier Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CulturalBarrierStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG19.pdf'>Seasonal Farm Work Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string SeasonalFarmWorkStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG20.pdf'>Literacy Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string LiteracyStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG21.pdf'>Work Based Learning Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string WorkBasedLearningStatus { get; set; }

        /// <summary>
        /// Student enrolled in multiple colleges
        /// </summary>
        public bool CCIsMultiCollege { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SD/sd01.pdf'>Primary Disability</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCPrimaryDisability { get; set; }

        /// <summary>
        /// Union of <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SM/SM01.pdf'>Matriculation Educational Goal</a> (first position) and <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SS/SS01.pdf'>Success Educational Goal</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CCEducationalGoal { get; set; }

        /// <summary>
        /// Union of <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SM/SM02.pdf'>Matriculation Major</a> and <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SS/SS02.pdf'>Credit Course of Study</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCMajor { get; set; }

        /// <summary>
        /// Union of <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SM/SM05.pdf'>Matriculation Assessment Status</a> (first two positions) and <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SS/SS04.pdf'>Credit Initial Assessment Status</a>
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string CCAssessmentStatus { get; set; }

        /// <summary>
        /// Union of <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SM/SM06.pdf'>Matriculation Advisement Status</a> (first two positions) and <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SS/SS05.pdf'>Credit Education Plan Status</a>
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string CCAdvisementStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD2.pdf'>Last Term of Enrollment in the California Community College System</a>
        /// </summary>
        [StringLength(3, MinimumLength = 3)]
        [Column(TypeName = "char(3)")]
        public string CCEnrollmentLastTerm { get; set; }

        /// <summary>
        /// Cumulative Total of Transferable Units Attempted
        /// </summary>
        [RegularExpression(@"^\d{4}\.\d{2}$")]
        [Column(TypeName = "decimal(6,2)")]
        public string CCTransferableUnitsAttempted { get; set; }

        /// <summary>
        /// Cumulative Total of Transferable Units Earned
        /// </summary>
        [RegularExpression(@"^\d{4}\.\d{2}$")]
        [Column(TypeName = "decimal(6,2)")]
        public string CCTransferableUnitsEarned { get; set; }
        #endregion Demographic

        #region CCC Apply
        /// <summary>
        /// Last High School Attended: Country
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APHSCountry { get; set; }

        /// <summary>
        /// Last College or University Attended: Country (first)
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APUN01Country { get; set; }

        /// <summary>
        /// Last College or University Attended: Award (first)
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string APUN01Award { get; set; }

        /// <summary>
        /// Last College or University Attended: Country (second)
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APUN02Country { get; set; }

        /// <summary>
        /// Last College or University Attended: Award (second)
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string APUN02Award { get; set; }

        /// <summary>
        /// Last College or University Attended: Country (third)
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APUN03Country { get; set; }

        /// <summary>
        /// Last College or University Attended: Award (third)
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string APUN03Award { get; set; }

        /// <summary>
        /// Last College or University Attended: Country (fourth)
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APUN04Country { get; set; }

        /// <summary>
        /// Last College or University Attended: Award (fourth)
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string APUN04Award { get; set; }

        /// <summary>
        /// Visa Type
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APVisaType { get; set; }

        /// <summary>
        /// Eligible for AB540 Waiver
        /// </summary>
        public bool? APAB540Waiver { get; set; }

        /// <summary>
        /// Comfortable with English
        /// </summary>
        public bool? APComfortableEnglish { get; set; }

        /// <summary>
        /// Language of Application
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APApplicationLanguage { get; set; }

        /// <summary>
        /// Interest in English as a Second Language Programs
        /// </summary>
        public bool? APEsl { get; set; }

        /// <summary>
        /// Interest in Assistance with Reading, Wriging or Math
        /// </summary>
        public bool? APBasicSkills { get; set; }

        /// <summary>
        /// Interest in Employment Assistance
        /// </summary>
        public bool? APEmploymentAssistance { get; set; }

        /// <summary>
        /// Student Employed as Seasonal Agreicultural Worker
        /// </summary>
        public bool? APSeasonalAG { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APCountry { get; set; }

        /// <summary>
        /// Permanent Country
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APPermanentCountry { get; set; }

        /// <summary>
        /// Permanent Country (International Application)
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APPermanentCountryInternational { get; set; }

        /// <summary>
        /// CCC Apply Self-Report: Completed Eleventh Grade
        /// </summary>
        public bool? APCompletedEleventhGrade { get; set; }

        /// <summary>
        /// CCC Apply Self-Report:Cumulative Grade Point Average
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? APCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// CCC Apply Self-Report: Highest English Course Completed
        /// 0 = Unknown
        /// 1 = 12th grade Advanced Placement(AP) English Composition or Literature
        /// 2 = 12th grade Honors English Composition or Literature
        /// 3 = 12th grade English Composition or Literature
        /// 4 = 11th grade Advanced Placement(AP) English Composition or Literature
        /// 5 = 11th grade Honors English Composition or Literature
        /// 6 = 11th grade English Composition or Literature
        /// 7 = 10th grade(or lower) English Composition or Literature
        /// </summary>
        [Range(0, 7)]
        [Column(TypeName = "tinyint")]
        public byte? APEnglishCompletedCourseId { get; set; }

        /// <summary>
        /// CCC Apply Self-Report: Grade of Highest English Course Completed
        /// A
        /// A-
        /// B+
        /// B
        /// B-
        /// C+
        /// C
        /// C-
        /// D
        /// F
        /// P
        /// NP
        /// ON
        /// X
        /// </summary>
        [RegularExpression(@"^(?:A|B|C){1}(?:-|\+)?$|^(?:D|F|NP|P|ON|X)$")]
        [Column(TypeName = "varchar(3)")]
        public string APEnglishCompletedCourseGrade { get; set; }

        /// <summary>
        /// CCC Apply Self-Report: Highest Mathematics Course Completed
        /// 0  = Unknown
        /// 1  = Pre-algebra or lower
        /// 2  = Algebra 1
        /// 3  = Integrated Math 1
        /// 4  = Integrated Math 2
        /// 5  = Geometry
        /// 6  = Algebra 2
        /// 7  = Integrated Math 3
        /// 8  = Statistics
        /// 9  = Integrated Math 4
        /// 10 = Trigonometry
        /// 11 = Pre-calculus
        /// 12 = Calculus or higher
        /// 13 = Math Analysis
        /// </summary>
        [Range(0, 13)]
        [Column(TypeName = "tinyint")]
        public byte? APMathematicsCompletedCourseId { get; set; }

        /// <summary>
        /// CCC Apply Self-Report: Grade of Highest Mathematics Course Completed
        /// A
        /// A-
        /// B+
        /// B
        /// B-
        /// C+
        /// C
        /// C-
        /// D
        /// F
        /// P
        /// NP
        /// ON
        /// X
        /// </summary>
        [RegularExpression(@"^(?:A|B|C){1}(?:-|\+)?$|^(?:D|F|NP|P|ON|X)$")]
        [Column(TypeName = "varchar(3)")]
        public string APMathematicsCompletedCourseGrade { get; set; }

        /// <summary>
        /// CCC Apply Self-Report: Highest Mathematics Passed
        /// 0  = Unknown
        /// 1  = Pre-algebra or lower
        /// 2  = Algebra 1
        /// 3  = Integrated Math 1
        /// 4  = Integrated Math 2
        /// 5  = Geometry
        /// 6  = Algebra 2
        /// 7  = Integrated Math 3
        /// 8  = Statistics
        /// 9  = Integrated Math 4
        /// 10 = Trigonometry
        /// 11 = Pre-calculus
        /// 12 = Calculus or higher
        /// 13 = Math Analysis
        /// </summary>
        [Range(0, 13)]
        [Column(TypeName = "tinyint")]
        public byte? APMathematicsPassedCourseId { get; set; }

        /// <summary>
        /// CCC Apply Self-Report: Grade of Highest Mathematics Course Passed
        /// A
        /// A-
        /// B+
        /// B
        /// B-
        /// C+
        /// C
        /// C-
        /// D
        /// F
        /// P
        /// NP
        /// ON
        /// X
        /// </summary>
        [RegularExpression(@"^(?:A|B|C){1}(?:-|\+)?$|^(?:D|F|NP|P|ON|X)$")]
        [Column(TypeName = "varchar(3)")]
        public string APMathematicsPassedCourseGrade { get; set; }
        #endregion
    }
}