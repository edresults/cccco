﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MultipleMeasures.ViewModels
{
    public class PlacementViewModel
    {
        /// <summary>
        /// Source Of Transcript Data 
        /// 1 = California College Guidance Initiative (CCGI)
        /// 2 = Cal-PASS Plus (CPP)
        /// 3 = CCC Apply
        /// </summary>
        [Required]
        [Range(1, 3)]
        public byte DataSource { get; set; }

        /// <summary>
        /// College Level
        /// </summary>
        [Required]
        public bool EnglishY { get; set; }

        /// <summary>
        /// One Level Below College Level
        /// </summary>
        [Required]
        public bool EnglishA { get; set; }

        /// <summary>
        /// Two Levels Below College Level
        /// </summary>
        [Required]
        public bool EnglishB { get; set; }

        /// <summary>
        /// Three Levels Below College Level
        /// </summary>
        [Required]
        public bool EnglishC { get; set; }

        /// <summary>
        /// Preparatory Algebra
        /// </summary>
        [Required]
        public bool PreAlgebra { get; set; }

        /// <summary>
        /// Beginning Algebra
        /// </summary>
        [Required]
        public bool AlgebraI { get; set; }

        /// <summary>
        /// Intermediate Algebra
        /// </summary>
        [Required]
        public bool AlgebraII { get; set; }

        /// <summary>
        /// Intermediate Algebra
        /// </summary>
        [Required]
        public bool MathGE { get; set; }

        /// <summary>
        /// Intermediate Algebra
        /// </summary>
        [Required]
        public bool Statistics { get; set; }

        /// <summary>
        /// College Algebra
        /// </summary>
        [Required]
        public bool CollegeAlgebra { get; set; }

        /// <summary>
        /// Trigonometry
        /// </summary>
        [Required]
        public bool Trigonometry { get; set; }

        /// <summary>
        /// Preparatory Calculus
        /// </summary>
        [Required]
        public bool PreCalculus { get; set; }

        /// <summary>
        /// Calculus
        /// </summary>
        [Required]
        public bool CalculusI { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is College Level
        /// Met College Reading Requirement
        /// </summary>
        [Required]
        public bool ReadingM_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is College Level
        /// College Level Reading
        /// </summary>
        [Required]
        public bool ReadingY_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is College Level
        /// One Level Below College Level
        /// </summary>
        [Required]
        public bool ReadingA_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is College Level
        /// Two Level Below College Level
        /// </summary>
        [Required]
        public bool ReadingB_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is College Level
        /// Three Levels Below College Level
        /// </summary>
        [Required]
        public bool ReadingC_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is One Level Below College Level
        /// Met College Reading Requirement
        /// </summary>
        [Required]
        public bool ReadingM_UboundA { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is One Level Below College Level
        /// One Level Below College Level
        /// </summary>
        [Required]
        public bool ReadingA_UboundA { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is One Level Below College Level
        /// Two Levels Below College Level
        /// </summary>
        [Required]
        public bool ReadingB_UboundA { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is One Level Below College Level
        /// Three Levels Below College Level
        /// </summary>
        [Required]
        public bool ReadingC_UboundA { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is Two Levels Below College Level
        /// Met College Reading Requirement
        /// </summary>
        [Required]
        public bool ReadingM_UboundB { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is Two Levels Below College Level
        /// Two Levels Below College Level
        /// </summary>
        [Required]
        public bool ReadingB_UboundB { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is Two Levels Below College Level
        /// Three Levels Below College Level
        /// </summary>
        [Required]
        public bool ReadingC_UboundB { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is College Level
        /// College Level
        /// </summary>
        [Required]
        public bool EslY_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is College Level
        /// One Level Below College Level
        /// </summary>
        [Required]
        public bool EslA_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is College Level
        /// Two Levels Below College Level
        /// </summary>
        [Required]
        public bool EslB_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is One Level Below College Level
        /// One Level Below College Level
        /// </summary>
        [Required]
        public bool EslA_UboundA { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is One Level Below College Level
        /// Two Levels Below College Level
        /// </summary> 
        [Required]
        public bool EslB_UboundA { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is One Level Below College Level
        /// Three Levels Below College Level
        /// </summary>
        [Required]
        public bool EslC_UboundA { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is Two Levels Below College Level
        /// Two Levels Below College Level
        /// </summary>
        [Required]
        public bool EslB_UboundB { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is Two Levels Below College Level
        /// Three Levels Below College Level
        /// </summary>
        [Required]
        public bool EslC_UboundB { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is Two Levels Below College Level
        /// Four Levels Below College Level
        /// </summary>
        [Required]
        public bool EslD_UboundB { get; set; }

        /// <summary>
        /// Record Creation Date
        /// </summary>
        [Required]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Record Modification Date
        /// </summary>
        [Required]
        public DateTime ModifyDate { get; set; }
    }
}