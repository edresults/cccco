﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MultipleMeasures.ViewModels
{
    public class TranscriptViewModel
    {
        /// <summary>
        /// Source Of Transcript Data 
        /// 1 = California College Guidance Initiative (CCGI)
        /// 2 = Cal-PASS Plus (CPP)
        /// 3 = CCC Apply
        /// </summary>
        [Required]
        [Range(1, 3)]
        public byte DataSource { get; set; }

        /// <summary>
        /// Student Enrolled in 9th Grade Coursework
        /// </summary>
        [Required]
        public bool IsHighSchoolGrade09 { get; set; }

        /// <summary>
        /// Student Enrolled in 10th Grade Coursework
        /// </summary>
        [Required]
        public bool IsHighSchoolGrade10 { get; set; }

        /// <summary>
        /// Student Enrolled in 11th Grade Coursework
        /// </summary>
        [Required]
        public bool IsHighSchoolGrade11 { get; set; }

        /// <summary>
        /// Student Enrolled in 12th Grade Coursework
        /// </summary>
        [Required]
        public bool IsHighSchoolGrade12 { get; set; }

        /// <summary>
        /// Unweighted Cumulative Grade Point Average
        /// </summary>
        [Range(0.00, 4.00)]
        public decimal? CumulativeGradePointAverage { get; set; }

        /// <summary>
        /// Highest English Course Grade Points
        /// </summary>
        [Range(0.0, 4.0)]
        public decimal? English { get; set; }

        /// <summary>
        /// Preparatory Algebra Grade Points
        /// </summary>
        [Range(0.0, 4.0)]
        public decimal? PreAlgebra { get; set; }

        /// <summary>
        /// Beginning Algebra Grade Points
        /// </summary>
        [Range(0.0, 4.0)]
        public decimal? AlgebraI { get; set; }

        /// <summary>
        /// Geometry Grade Points
        /// </summary>
        [Range(0.0, 4.0)]
        public decimal? Geometry { get; set; }

        /// <summary>
        /// Intermediate Algebra Grade Points
        /// </summary>
        [Range(0.0, 4.0)]
        public decimal? AlgebraII { get; set; }

        /// <summary>
        /// Trigonometry Grade Points
        /// </summary>
        [Range(0.0, 4.0)]
        public decimal? Trigonometry { get; set; }

        /// <summary>
        /// Preparatory Calculus Grade Points
        /// </summary>
        [Range(0.0, 4.0)]
        public decimal? PreCalculus { get; set; }

        /// <summary>
        /// Statistics Grade Points
        /// </summary>
        [Range(0.0, 4.0)]
        public decimal? Statistics { get; set; }

        /// <summary>
        /// Calculus Grade Points
        /// </summary>
        [Range(0.0, 4.0)]
        public decimal? Calculus { get; set; }

        /// <summary>
        /// Record Creation Date
        /// </summary>
        [Required]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Record Modification Date
        /// </summary>
        [Required]
        public DateTime ModifyDate { get; set; }
    }
}