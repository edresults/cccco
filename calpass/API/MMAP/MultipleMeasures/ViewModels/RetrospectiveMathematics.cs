﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MultipleMeasures.ViewModels
{
    /// <summary>
    /// Model of Retrospective Mathematics data
    /// 
    /// Notes:
    /// 
    /// <ul>
    ///     <li>Cohort Definition</li>
    ///         <ol>
    ///             <li>All students having attended a California Community College</li>
    ///             <li>Enrolled in one or more credit Mathematics courses or statistics course from outside Mathematics</li>
    ///             <li>Mathematics courses defined by the following Taxonomy of Program code</li>
    ///                 <ul>
    ///                     <li>170100</li>
    ///                 </ul>
    ///         </ol>
    /// 	<li>Variable Identifiers</li>
    /// 		<ul>
    /// 			<li>Origin of record identified by the first and second characters</li>
    /// 					<ul>
    /// 							<li>HS = High School</li>
    /// 							<li>CC = Community College</li>
    /// 							<li>ST = California Standardized Testing and Reporitng (STAR)</li>
    /// 							<li>AP = California Community Colleges Application (CCCApply)</li>
    /// 					</ul>
    /// 			<li>High school grade of student identified by the third and fourth characters</li>
    /// 					<ul>
    /// 							<li>09 = 9th Grade</li>
    /// 							<li>10 = 10th Grade</li>
    /// 							<li>11 = 11th Grade</li>
    /// 							<li>12 = 12th Grade</li>
    /// 							<li>LG = Last Grade Enrolled</li>
    /// 					</ul>
    /// 			<li>Community college subject identified by the third and fourth characters</li>
    ///                 <ul>
    ///                 	<li>MA = Mathematics</li>
    ///                 </ul>
    /// 			<li>Community college subject identified by the fifth and sixth characters</li>
    /// 					<ul>
    /// 						<li>CR = Credit</li>
    /// 					</ul>
    /// 			<li>Community college course level identified by the seventh and eighth characters</li>
    /// 					<ul>
    /// 							<li>FL = First Level Enrolled in System</li>
    /// 							<li>00 = College-Level</li>
    /// 							<li>01 = One Level Below College-Level</li>
    /// 							<li>02 = Two Levels Below College-Level</li>
    /// 							<li>03 = Three Levels Below College-Level</li>
    /// 							<li>04 = Four Levels Below College-Level</li>
    /// 					</ul>
    /// 			<li>Community college transfer-level attempt identified by the ninth and tenth characters</li>
    /// 					<ul>
    /// 							<li>2N = Second Attempt</li>
    /// 					</ul>
    /// 		</ul>
    /// 	<li>Record Selection Criteria</li>
    /// 		<ul>
    /// 			<li>When two or more records exist for a high school student in a grade, the represented record is hierarchically selected using:</li>
    /// 					<ul>
    /// 							<li>Most Recent Year</li>
    /// 							<li>Most Recent Term</li>
    /// 							<li>Most Rigorous Course</li>
    /// 							<li>Best Mark Letter</li>
    /// 					</ul>
    /// 			<li>When two or more records exist for a college student in a level, the represented record is hierarchically selected using:</li>
    /// 					<ul>
    /// 							<li>Least Recent Year</li>
    /// 							<li>Least Recent Term</li>
    /// 							<li>Most Rigorous Course</li>
    /// 							<li>Best Mark Letter</li>
    /// 					</ul>
    /// 		</ul>
    /// </ul>
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(DefaultNamingStrategy))]
    public class RetrospectiveMathematics
    {
        /// <summary>
        /// Internally Generated Student Id
        /// </summary>
        [Key]
        [Column(TypeName = "integer")]
        public int Id { get; set; }

        #region CDE :: 09
        /// <summary>
        /// 09 = 9th Grade
        /// 10 = 10th Grade
        /// 11 = 11th Grade
        /// 12 = 12th Grade
        /// </summary>
        [RegularExpression(@"^(09|10|11|12)$")]
        [Column(TypeName = "char(2)")]
        public string HS09GradeCode { get; set; }

        /// <summary>
        /// Overall Grade Point Average
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS09OverallGradePointAverage { get; set; }

        /// <summary>
        /// Cumulative Overall Grade Point Average
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS09OverallCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// Mathematics Grade Point Average
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS09MathematicsGradePointAverage { get; set; }

        /// <summary>
        /// Cumulative Mathematics Grade Point Average
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS09MathematicsCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/si/ds/pubschls.asp'>High School CDS Code</a>
        /// </summary>
        [StringLength(14)]
        [Column(TypeName = "char(14)")]
        public string HS09SchoolCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int HS09YearTermCode { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/sp/cl/systemdocs.asp'>CALPADS Course Code (formerly CBEDS Course Code)</a>
        /// </summary>
        [StringLength(4, MinimumLength = 4)]
        [Column(TypeName = "char(4)")]
        public string HS09CourseCode { get; set; }

        /// <summary>
        /// <a href='https://docs.google.com/spreadsheets/d/1Kk64_gTfpDBuCWe_R3x-aQ1GM-TXNR4HXIwkLqRbjy8/edit?usp=sharing'>Course Ranking of Rigor</a>
        /// </summary>
        [Range(1, 9)]
        [Column(TypeName = "tinyint")]
        public byte? HS09CourseContentRank { get; set; }

        /// <summary>
        /// Course Title
        /// </summary>
        [StringLength(40)]
        [Column(TypeName = "varchar(40)")]
        public string HS09CourseTitle { get; set; }

        /// <summary>
        /// Course Mark Letter
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string HS09CourseMarkLetter { get; set; }

        /// <summary>
        /// Course Mark Points
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? HS09CourseMarkPoints { get; set; }

        /// <summary>
        /// University of California and California State University A-G Code
        /// </summary>
        [StringLength(2)]
        [Column(TypeName = "varchar(2)")]
        public string HS09CourseUniversityAdmissionRequirementCode { get; set; }

        /// <summary>
        /// 30 = Advanced Placement
        /// 31 = Basic
        /// 32 = General
        /// 33 = Gifted and talented
        /// 34 = Other honors
        /// 35 = Remedial
        /// 36 = Special Education
        /// 37 = International Baccalaureate(IB) – higher level
        /// 38 = International Baccalaureate(IB) – standard level
        /// 39 = UC certified honors
        /// 40 = College level
        /// XX = Unknown
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS09CourseLevelCode { get; set; }

        /// <summary>
        /// 30 = ROP or ROC
        /// 31 = Career Technical Education
        /// 32 = Technical Prep
        /// 33 = Technical Prep, and ROP or ROC
        /// XX = None of the Above
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS09CourseTypeCode { get; set; }
        #endregion

        #region CDE :: 10
        /// <summary>
        /// 09 = 9th Grade
        /// 10 = 10th Grade
        /// 11 = 11th Grade
        /// 12 = 12th Grade
        /// </summary>
        [RegularExpression(@"^(09|10|11|12)$")]
        [Column(TypeName = "char(2)")]
        public string HS10GradeCode { get; set; }

        /// <summary>
        /// Overall Grade Point Average of 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS10OverallGradePointAverage { get; set; }

        /// <summary>
        /// Overall Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS10OverallCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// Mathematics Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS10MathematicsGradePointAverage { get; set; }

        /// <summary>
        /// Mathematics Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS10MathematicsCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/si/ds/pubschls.asp'>High School CDS Code</a>
        /// </summary>
        [StringLength(14)]
        [Column(TypeName = "char(14)")]
        public string HS10SchoolCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int HS10YearTermCode { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/sp/cl/systemdocs.asp'>CALPADS Course Code (formerly CBEDS Course Code)</a>
        /// </summary>
        [StringLength(4, MinimumLength = 4)]
        [Column(TypeName = "char(4)")]
        public string HS10CourseCode { get; set; }

        /// <summary>
        /// <a href='https://docs.google.com/spreadsheets/d/1Kk64_gTfpDBuCWe_R3x-aQ1GM-TXNR4HXIwkLqRbjy8/edit?usp=sharing'>Course Ranking of Rigor</a>
        /// </summary>
        [Range(1, 9)]
        [Column(TypeName = "tinyint")]
        public byte? HS10CourseContentRank { get; set; }

        /// <summary>
        /// Course Title
        /// </summary>
        [StringLength(40)]
        [Column(TypeName = "varchar(40)")]
        public string HS10CourseTitle { get; set; }

        /// <summary>
        /// Course Mark Letter
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string HS10CourseMarkLetter { get; set; }

        /// <summary>
        /// Course Mark Points
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? HS10CourseMarkPoints { get; set; }

        /// <summary>
        /// University of California and California State University A-G Code
        /// </summary>
        [StringLength(2)]
        [Column(TypeName = "varchar(2)")]
        public string HS10CourseUniversityAdmissionRequirementCode { get; set; }

        /// <summary>
        /// 30 = Advanced Placement
        /// 31 = Basic
        /// 32 = General
        /// 33 = Gifted and talented
        /// 34 = Other honors
        /// 35 = Remedial
        /// 36 = Special Education
        /// 37 = International Baccalaureate(IB) – higher level
        /// 38 = International Baccalaureate(IB) – standard level
        /// 39 = UC certified honors
        /// 40 = College level
        /// XX = Unknown
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS10CourseLevelCode { get; set; }

        /// <summary>
        /// 30 = ROP or ROC
        /// 31 = Career Technical Education
        /// 32 = Technical Prep
        /// 33 = Technical Prep, and ROP or ROC
        /// XX = None of the Above
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS10CourseTypeCode { get; set; }
        #endregion

        #region CDE :: 11
        /// <summary>
        /// 09 = 9th Grade
        /// 10 = 10th Grade
        /// 11 = 11th Grade
        /// 12 = 12th Grade
        /// </summary>
        [RegularExpression(@"^(09|10|11|12)$")]
        [Column(TypeName = "char(2)")]
        public string HS11GradeCode { get; set; }

        /// <summary>
        /// Overall Grade Point Average of 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS11OverallGradePointAverage { get; set; }

        /// <summary>
        /// Overall Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS11OverallCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// Mathematics Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS11MathematicsGradePointAverage { get; set; }

        /// <summary>
        /// Mathematics Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS11MathematicsCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/si/ds/pubschls.asp'>High School CDS Code</a>
        /// </summary>
        [StringLength(14)]
        [Column(TypeName = "char(14)")]
        public string HS11SchoolCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int HS11YearTermCode { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/sp/cl/systemdocs.asp'>CALPADS Course Code (formerly CBEDS Course Code)</a>
        /// </summary>
        [StringLength(4, MinimumLength = 4)]
        [Column(TypeName = "char(4)")]
        public string HS11CourseCode { get; set; }

        /// <summary>
        /// <a href='https://docs.google.com/spreadsheets/d/1Kk64_gTfpDBuCWe_R3x-aQ1GM-TXNR4HXIwkLqRbjy8/edit?usp=sharing'>Course Ranking of Rigor</a>
        /// </summary>
        [Range(1, 9)]
        [Column(TypeName = "tinyint")]
        public byte? HS11CourseContentRank { get; set; }

        /// <summary>
        /// Course Title
        /// </summary>
        [StringLength(40)]
        [Column(TypeName = "varchar(40)")]
        public string HS11CourseTitle { get; set; }

        /// <summary>
        /// Course Mark Letter
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string HS11CourseMarkLetter { get; set; }

        /// <summary>
        /// Course Mark Points
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? HS11CourseMarkPoints { get; set; }

        /// <summary>
        /// University of California and California State University A-G Code
        /// </summary>
        [StringLength(2)]
        [Column(TypeName = "varchar(2)")]
        public string HS11CourseUniversityAdmissionRequirementCode { get; set; }

        /// <summary>
        /// 30 = Advanced Placement
        /// 31 = Basic
        /// 32 = General
        /// 33 = Gifted and talented
        /// 34 = Other honors
        /// 35 = Remedial
        /// 36 = Special Education
        /// 37 = International Baccalaureate(IB) – higher level
        /// 38 = International Baccalaureate(IB) – standard level
        /// 39 = UC certified honors
        /// 40 = College level
        /// XX = Unknown
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS11CourseLevelCode { get; set; }

        /// <summary>
        /// 30 = ROP or ROC
        /// 31 = Career Technical Education
        /// 32 = Technical Prep
        /// 33 = Technical Prep, and ROP or ROC
        /// XX = None of the Above
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS11CourseTypeCode { get; set; }
        #endregion

        #region CDE :: 12
        /// <summary>
        /// 09 = 9th Grade
        /// 10 = 10th Grade
        /// 11 = 11th Grade
        /// 12 = 12th Grade
        /// </summary>
        [RegularExpression(@"^(09|10|11|12)$")]
        [Column(TypeName = "char(2)")]
        public string HS12GradeCode { get; set; }

        /// <summary>
        /// Overall Grade Point Average of 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS12OverallGradePointAverage { get; set; }

        /// <summary>
        /// Overall Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS12OverallCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// Mathematics Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS12MathematicsGradePointAverage { get; set; }

        /// <summary>
        /// Mathematics Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HS12MathematicsCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/si/ds/pubschls.asp'>High School CDS Code</a>
        /// </summary>
        [StringLength(14)]
        [Column(TypeName = "char(14)")]
        public string HS12SchoolCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int HS12YearTermCode { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/sp/cl/systemdocs.asp'>CALPADS Course Code (formerly CBEDS Course Code)</a>
        /// </summary>
        [StringLength(4, MinimumLength = 4)]
        [Column(TypeName = "char(4)")]
        public string HS12CourseCode { get; set; }

        /// <summary>
        /// <a href='https://docs.google.com/spreadsheets/d/1Kk64_gTfpDBuCWe_R3x-aQ1GM-TXNR4HXIwkLqRbjy8/edit?usp=sharing'>Course Ranking of Rigor</a>
        /// </summary>
        [Range(1, 9)]
        [Column(TypeName = "tinyint")]
        public byte? HS12CourseContentRank { get; set; }

        /// <summary>
        /// Course Title
        /// </summary>
        [StringLength(40)]
        [Column(TypeName = "varchar(40)")]
        public string HS12CourseTitle { get; set; }

        /// <summary>
        /// Course Mark Letter
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string HS12CourseMarkLetter { get; set; }

        /// <summary>
        /// Course Mark Points
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? HS12CourseMarkPoints { get; set; }

        /// <summary>
        /// University of California and California State University A-G Code
        /// </summary>
        [StringLength(2)]
        [Column(TypeName = "varchar(2)")]
        public string HS12CourseUniversityAdmissionRequirementCode { get; set; }

        /// <summary>
        /// 30 = Advanced Placement
        /// 31 = Basic
        /// 32 = General
        /// 33 = Gifted and talented
        /// 34 = Other honors
        /// 35 = Remedial
        /// 36 = Special Education
        /// 37 = International Baccalaureate(IB) – higher level
        /// 38 = International Baccalaureate(IB) – standard level
        /// 39 = UC certified honors
        /// 40 = College level
        /// XX = Unknown
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS12CourseLevelCode { get; set; }

        /// <summary>
        /// 30 = ROP or ROC
        /// 31 = Career Technical Education
        /// 32 = Technical Prep
        /// 33 = Technical Prep, and ROP or ROC
        /// XX = None of the Above
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HS12CourseTypeCode { get; set; }
        #endregion

        #region CDE :: LG
        /// <summary>
        /// 09 = 9th Grade
        /// 10 = 10th Grade
        /// 11 = 11th Grade
        /// 12 = 12th Grade
        /// </summary>
        [RegularExpression(@"^(09|10|11|12)$")]
        [Column(TypeName = "char(2)")]
        public string HSLGGradeCode { get; set; }

        /// <summary>
        /// Overall Grade Point Average of 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HSLGOverallGradePointAverage { get; set; }

        /// <summary>
        /// Overall Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HSLGOverallCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// Mathematics Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HSLGMathematicsGradePointAverage { get; set; }

        /// <summary>
        /// Mathematics Cumulative Grade Point Average Through 9th Grade
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? HSLGMathematicsCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/si/ds/pubschls.asp'>High School CDS Code</a>
        /// </summary>
        [StringLength(14)]
        [Column(TypeName = "char(14)")]
        public string HSLGSchoolCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int HSLGYearTermCode { get; set; }

        /// <summary>
        /// <a href='https://www.cde.ca.gov/ds/sp/cl/systemdocs.asp'>CALPADS Course Code (formerly CBEDS Course Code)</a>
        /// </summary>
        [StringLength(4, MinimumLength = 4)]
        [Column(TypeName = "char(4)")]
        public string HSLGCourseCode { get; set; }

        /// <summary>
        /// <a href='https://docs.google.com/spreadsheets/d/1Kk64_gTfpDBuCWe_R3x-aQ1GM-TXNR4HXIwkLqRbjy8/edit?usp=sharing'>Course Ranking of Rigor</a>
        /// </summary>
        [Range(1, 9)]
        [Column(TypeName = "tinyint")]
        public byte? HSLGCourseContentRank { get; set; }

        /// <summary>
        /// Course Title
        /// </summary>
        [StringLength(40)]
        [Column(TypeName = "varchar(40)")]
        public string HSLGCourseTitle { get; set; }

        /// <summary>
        /// Course Mark Letter
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string HSLGCourseMarkLetter { get; set; }

        /// <summary>
        /// Course Mark Points
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? HSLGCourseMarkPoints { get; set; }

        /// <summary>
        /// University of California and California State University A-G Code
        /// </summary>
        [StringLength(2)]
        [Column(TypeName = "varchar(2)")]
        public string HSLGCourseUniversityAdmissionRequirementCode { get; set; }

        /// <summary>
        /// 30 = Advanced Placement
        /// 31 = Basic
        /// 32 = General
        /// 33 = Gifted and talented
        /// 34 = Other honors
        /// 35 = Remedial
        /// 36 = Special Education
        /// 37 = International Baccalaureate(IB) – higher level
        /// 38 = International Baccalaureate(IB) – standard level
        /// 39 = UC certified honors
        /// 40 = College level
        /// XX = Unknown
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HSLGCourseLevelCode { get; set; }

        /// <summary>
        /// 30 = ROP or ROC
        /// 31 = Career Technical Education
        /// 32 = Technical Prep
        /// 33 = Technical Prep, and ROP or ROC
        /// XX = None of the Above
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string HSLGCourseTypeCode { get; set; }
        #endregion

        #region COMIS :: MATH :: CR :: FL
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 3)]
        [Column(TypeName = "char(3)")]
        public string CCENCRFLCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCENCRFLYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCENCRFLCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/XB/xb00.pdf'>Section Id</a>
        /// </summary>
        [StringLength(6)]
        [Column(TypeName = "varchar(6)")]
        public string CCENCRFLSectionId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCENCRFLCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCENCRFLCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCENCRFLCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCENCRFLCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCENCRFLCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCENCRFLCourseLevelCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SXD/SXD3.pdf'>Sum of credit Mathematics units attempted this term</a>
        /// </summary>
        [RegularExpression(@"^\d{1,2}\.\d{2}$")]
        [Column(TypeName = "decimal(4,2)")]
        public decimal? CCENCRFLUnitsAttempt { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SXD/SXD3.pdf'>Sum of credit Mathematics units earned this term</a>
        /// </summary>
        [RegularExpression(@"^\d{1,2}\.\d{2}$")]
        [Column(TypeName = "decimal(4,2)")]
        public decimal? CCENCRFLUnitsSuccess { get; set; }

        #endregion

        #region COMIS :: MATH :: CR :: 00
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 3)]
        [Column(TypeName = "char(3)")]
        public string CCENCR00CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCENCR00YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCENCR00CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/XB/xb00.pdf'>Section Id</a>
        /// </summary>
        [StringLength(6)]
        [Column(TypeName = "varchar(6)")]
        public string CCENCR00SectionId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCENCR00CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCENCR00CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCENCR00CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCENCR00CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCENCR00CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCENCR00CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: MATH :: CR :: 00 :: SECOND ATTEMPT
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 3)]
        [Column(TypeName = "char(3)")]
        public string CCENCR002NCollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCENCR002NYearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCENCR002NCourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/XB/xb00.pdf'>Section Id</a>
        /// </summary>
        [StringLength(6)]
        [Column(TypeName = "varchar(6)")]
        public string CCENCR002NSectionId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb00.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCENCR002NCourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCENCR002NCourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCENCR002NCourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCENCR002NCourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCENCR002NCourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCENCR002NCourseLevelCode { get; set; }
        #endregion

        #region COMIS :: MATH :: CR :: 01
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi01.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 3)]
        [Column(TypeName = "char(3)")]
        public string CCENCR01CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCENCR01YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCENCR01CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/XB/xb00.pdf'>Section Id</a>
        /// </summary>
        [StringLength(6)]
        [Column(TypeName = "varchar(6)")]
        public string CCENCR01SectionId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb01.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCENCR01CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCENCR01CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCENCR01CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCENCR01CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCENCR01CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCENCR01CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: MATH :: CR :: 02
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi02.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 3)]
        [Column(TypeName = "char(3)")]
        public string CCENCR02CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCENCR02YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCENCR02CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/XB/xb00.pdf'>Section Id</a>
        /// </summary>
        [StringLength(6)]
        [Column(TypeName = "varchar(6)")]
        public string CCENCR02SectionId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCENCR02CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCENCR02CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCENCR02CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCENCR02CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb02.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCENCR02CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCENCR02CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: MATH :: CR :: 03
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 3)]
        [Column(TypeName = "char(3)")]
        public string CCENCR03CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi03.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCENCR03YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCENCR03CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/XB/xb00.pdf'>Section Id</a>
        /// </summary>
        [StringLength(6)]
        [Column(TypeName = "varchar(6)")]
        public string CCENCR03SectionId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCENCR03CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCENCR03CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCENCR03CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCENCR03CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb03.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCENCR03CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCENCR03CourseLevelCode { get; set; }
        #endregion

        #region COMIS :: MATH :: CR :: 04
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>College Code</a>
        /// </summary>
        [StringLength(3, MinimumLength = 3)]
        [Column(TypeName = "char(3)")]
        public string CCENCR04CollegeCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/GI/gi04.pdf'>Expanded Year and Term Code</a>
        /// </summary>
        [Column(TypeName = "integer")]
        public int CCENCR04YearTermCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Id</a>
        /// </summary>
        [StringLength(12)]
        [Column(TypeName = "varchar(12)")]
        public string CCENCR04CourseId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/XB/xb00.pdf'>Section Id</a>
        /// </summary>
        [StringLength(6)]
        [Column(TypeName = "varchar(6)")]
        public string CCENCR04SectionId { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Control Number</a>
        /// </summary>
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "char(12)")]
        public string CCENCR04CourseControlNumber { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Taxonomy of Program Code</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCENCR04CourseTopCode { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Title</a>
        /// </summary>
        [StringLength(68)]
        [Column(TypeName = "varchar(68)")]
        public string CCENCR04CourseTitle { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Letter</a>
        /// </summary>
        [StringLength(3)]
        [Column(TypeName = "varchar(3)")]
        public string CCENCR04CourseMarkLetter { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb04.pdf'>Course Mark Points</a>
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{1}$|^4.0$")]
        [Column(TypeName = "decimal(2,1)")]
        public decimal? CCENCR04CourseMarkPoints { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/CB/cb21.pdf'>Course Level Code</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCENCR04CourseLevelCode { get; set; }
        #endregion

        #region STAR
        /// <summary>
        /// True = Ready for California State University (CSU) or Participating California Community Colleges (CCC) College-Level Mathematics Courses
        /// False = Not Ready
        /// </summary>
        public bool? STIsEnglishEap { get; set; }

        /// <summary>
        /// Mathematics Scaled Score
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STEnglishScaledScore { get; set; }

        /// <summary>
        /// Mathematics Cluster 1 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STEnglishCluster1 { get; set; }

        /// <summary>
        /// Mathematics Cluster 1 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STEnglishCluster2 { get; set; }

        /// <summary>
        /// Mathematics Cluster 1 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STEnglishCluster3 { get; set; }

        /// <summary>
        /// Mathematics Cluster 1 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STEnglishCluster4 { get; set; }

        /// <summary>
        /// Mathematics Cluster 1 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STEnglishCluster5 { get; set; }

        /// <summary>
        /// Mathematics Cluster 1 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STEnglishCluster6 { get; set; }

        /// <summary>
        /// 0 = Grade-level mathematics (grade 7); default if unknown
        /// 1 = General Mathematics (grades 8 and 9)
        /// 2 = Summative High School Mathematics (grades 9–11)
        /// 3 = Algebra I
        /// 4 = Integrated Mathematics 1
        /// 5 = Geometry
        /// 6 = Integrated Mathematics 2
        /// 7 = Algebra II
        /// 8 = Integrated Mathematics 3
        /// 9 = Unknown (mathematics test in grades 8–11 was taken)
        /// </summary>
        [Range(0, 9)]
        [Column(TypeName = "tinyint")]
        public byte? STMathematicsSubject { get; set; }

        /// <summary>
        /// True = Ready for California State University (CSU) or Participating California Community Colleges (CCC) College-Level Mathematics Courses
        /// False = Not Ready
        /// </summary>
        public bool? STIsMathematicsEap { get; set; }

        /// <summary>
        /// Mathematics Scaled Score
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STMathematicsScaledScore { get; set; }

        /// <summary>
        /// Mathematics Cluster 6 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STMathematicsCluster1 { get; set; }

        /// <summary>
        /// Mathematics Cluster 6 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STMathematicsCluster2 { get; set; }

        /// <summary>
        /// Mathematics Cluster 6 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STMathematicsCluster3 { get; set; }

        /// <summary>
        /// Mathematics Cluster 6 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STMathematicsCluster4 { get; set; }

        /// <summary>
        /// Mathematics Cluster 6 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STMathematicsCluster5 { get; set; }

        /// <summary>
        /// Mathematics Cluster 6 Raw Subscore
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public int? STMathematicsCluster6 { get; set; }

        /// <summary>
        /// Mathematics Only
        /// </summary>
        public bool? STIsEnglishOnly { get; set; }

        /// <summary>
        /// Mathematics Learner
        /// </summary>
        public bool? STIsEnglishLearner { get; set; }

        /// <summary>
        /// Initial Fluent Mathematics Proficient
        /// </summary>
        public bool? STIsFluentInitially { get; set; }

        /// <summary>
        /// Reclassified Fluent Mathematics Proficient
        /// </summary>
        public bool? STIsFluentReclassified { get; set; }
        #endregion

        #region CDE :: Demograhpic
        /// <summary>
        /// Enrolled in a Course Code of 2100 at any time during high school
        /// </summary>
        public bool HSIsRemedial { get; set; }

        /// <summary>
        /// Date (YYYYMMDD) of High School Graduation
        /// </summary>
        [StringLength(8, MinimumLength = 8)]
        [Column(TypeName = "char(8)")]
        public string HSGraduationDate { get; set; }
        #endregion

        #region COMIS :: Demographic
        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/STD/STD1.pdf'>Age</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public byte CCAge { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SB/SB04.pdf'>Gender</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CCGender { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SB/SB06.pdf'>Citizenship</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CCCitzenship { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SB/SB09.pdf'>Residency</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [StringLength(5, MinimumLength = 5)]
        [Column(TypeName = "char(5)")]
        public string CCResidency { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SB/SB11.pdf'>Education Status</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [StringLength(5, MinimumLength = 5)]
        [Column(TypeName = "char(5)")]
        public string CCEducationStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SB/SB15.pdf'>Enrollment Status</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CCEnrollmentStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SB/SB29.pdf'>Ethnicities</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [StringLength(21, MinimumLength = 21)]
        [Column(TypeName = "char(21)")]
        public string CCEthnicities { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SB/SB33.pdf'>Parent Education Level</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [StringLength(21, MinimumLength = 21)]
        [Column(TypeName = "char(21)")]
        public string CCParentEducationLevel { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/STD/STD10.pdf'>Ethnicity</a> at <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD1.pdf'>Entry</a> Into System
        /// </summary>
        [Range(0, 999)]
        [Column(TypeName = "smallint")]
        public byte CCEthnicity { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG01.pdf'>Military Status</a>
        /// </summary>
        [StringLength(4, MinimumLength = 4)]
        [Column(TypeName = "char(4)")]
        public string MilitaryStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG02.pdf'>Military Dependent Status</a>
        /// </summary>
        [StringLength(4, MinimumLength = 4)]
        [Column(TypeName = "char(4)")]
        public string MilitaryDependentStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG03.pdf'>Foster Youth Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string FosterYouthStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG04.pdf'>Incarcerated Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string IncarceratedStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG05.pdf'>MESA ASEM Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string MESAASEMStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG06.pdf'>Puente Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string PuenteStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG07.pdf'>MCHS ECHS Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string MCHSECHSStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG08.pdf'>UMOJA Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string UMOJAStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG10.pdf'>CAA Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CAAStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG11.pdf'>CAFYES Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CAFYESStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG12.pdf'>Baccalaureate Program</a>
        /// </summary>
        [StringLength(5, MinimumLength = 5)]
        [Column(TypeName = "char(5)")]
        public string BaccalaureateProgram { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG13.pdf'>CCAP Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CCAPStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG14.pdf'>Economically Disadv Status</a>
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string EconomicallyDisadvStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG15.pdf'>Ex Offender Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string ExOffenderStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG16.pdf'>Homeless Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string HomelessStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG17.pdf'>Longterm Unemploy Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string LongtermUnemployStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG18.pdf'>Cultural Barrier Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CulturalBarrierStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG19.pdf'>Seasonal Farm Work Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string SeasonalFarmWorkStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG20.pdf'>Literacy Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string LiteracyStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SG/SG21.pdf'>Work Based Learning Status</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string WorkBasedLearningStatus { get; set; }

        /// <summary>
        /// Student enrolled in multiple colleges
        /// </summary>
        public bool CCIsMultiCollege { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SD/sd01.pdf'>Primary Disability</a>
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        [Column(TypeName = "char(1)")]
        public string CCPrimaryDisability { get; set; }

        /// <summary>
        /// Union of <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SM/SM01.pdf'>Matriculation Educational Goal</a> (first position) and <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SS/SS01.pdf'>Success Educational Goal</a>
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string CCEducationalGoal { get; set; }

        /// <summary>
        /// Union of <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SM/SM02.pdf'>Matriculation Major</a> and <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SS/SS02.pdf'>Credit Course of Study</a>
        /// </summary>
        [StringLength(6, MinimumLength = 6)]
        [Column(TypeName = "char(6)")]
        public string CCMajor { get; set; }

        /// <summary>
        /// Union of <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SM/SM05.pdf'>Matriculation Assessment Status</a> (first two positions) and <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SS/SS04.pdf'>Credit Initial Assessment Status</a>
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string CCAssessmentStatus { get; set; }

        /// <summary>
        /// Union of <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SM/SM06.pdf'>Matriculation Advisement Status</a> (first two positions) and <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Data_Elements/SS/SS05.pdf'>Credit Education Plan Status</a>
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string CCAdvisementStatus { get; set; }

        /// <summary>
        /// <a href='http://extranet.cccco.edu/Portals/1/TRIS/MIS/Left_Nav/DED/Derived_Elements/SCD/SCD2.pdf'>Last Term of Enrollment in the California Community College System</a>
        /// </summary>
        [StringLength(3, MinimumLength = 3)]
        [Column(TypeName = "char(3)")]
        public string CCEnrollmentLastTerm { get; set; }

        /// <summary>
        /// Cumulative Total of Transferable Units Attempted
        /// </summary>
        [RegularExpression(@"^\d{4}\.\d{2}$")]
        [Column(TypeName = "decimal(6,2)")]
        public string CCTransferableUnitsAttempted { get; set; }

        /// <summary>
        /// Cumulative Total of Transferable Units Earned
        /// </summary>
        [RegularExpression(@"^\d{4}\.\d{2}$")]
        [Column(TypeName = "decimal(6,2)")]
        public string CCTransferableUnitsEarned { get; set; }
        #endregion Demographic

        #region CCC Apply
        /// <summary>
        /// Last High School Attended: Country
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APHSCountry { get; set; }

        /// <summary>
        /// Last College or University Attended: Country (first)
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APUN01Country { get; set; }

        /// <summary>
        /// Last College or University Attended: Award (first)
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string APUN01Award { get; set; }

        /// <summary>
        /// Last College or University Attended: Country (second)
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APUN02Country { get; set; }

        /// <summary>
        /// Last College or University Attended: Award (second)
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string APUN02Award { get; set; }

        /// <summary>
        /// Last College or University Attended: Country (third)
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APUN03Country { get; set; }

        /// <summary>
        /// Last College or University Attended: Award (third)
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string APUN03Award { get; set; }

        /// <summary>
        /// Last College or University Attended: Country (fourth)
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APUN04Country { get; set; }

        /// <summary>
        /// Last College or University Attended: Award (fourth)
        /// </summary>
        [StringLength(1)]
        [Column(TypeName = "char(1)")]
        public string APUN04Award { get; set; }

        /// <summary>
        /// Visa Type
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APVisaType { get; set; }

        /// <summary>
        /// Eligible for AB540 Waiver
        /// </summary>
        public bool? APAB540Waiver { get; set; }

        /// <summary>
        /// Comfortable with Mathematics
        /// </summary>
        public bool? APComfortableEnglish { get; set; }

        /// <summary>
        /// Language of Application
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APApplicationLanguage { get; set; }

        /// <summary>
        /// Interest in Mathematics as a Second Language Programs
        /// </summary>
        public bool? APEsl { get; set; }

        /// <summary>
        /// Interest in Assistance with Reading, Wriging or Math
        /// </summary>
        public bool? APBasicSkills { get; set; }

        /// <summary>
        /// Interest in Employment Assistance
        /// </summary>
        public bool? APEmploymentAssistance { get; set; }

        /// <summary>
        /// Student Employed as Seasonal Agreicultural Worker
        /// </summary>
        public bool? APSeasonalAG { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APCountry { get; set; }

        /// <summary>
        /// Permanent Country
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APPermanentCountry { get; set; }

        /// <summary>
        /// Permanent Country (International Application)
        /// </summary>
        [StringLength(2, MinimumLength = 2)]
        [Column(TypeName = "char(2)")]
        public string APPermanentCountryInternational { get; set; }

        /// <summary>
        /// CCC Apply Self-Report: Completed Eleventh Grade
        /// </summary>
        public bool? APCompletedEleventhGrade { get; set; }

        /// <summary>
        /// CCC Apply Self-Report:Cumulative Grade Point Average
        /// </summary>
        [RegularExpression(@"^[0-3]\.\d{2}$|^4.00$")]
        [Column(TypeName = "decimal(3,2)")]
        public decimal? APCumulativeGradePointAverage { get; set; }

        /// <summary>
        /// CCC Apply Self-Report: Highest Mathematics Course Completed
        /// 0 = Unknown
        /// 1 = 12th grade Advanced Placement(AP) Mathematics Composition or Literature
        /// 2 = 12th grade Honors Mathematics Composition or Literature
        /// 3 = 12th grade Mathematics Composition or Literature
        /// 4 = 11th grade Advanced Placement(AP) Mathematics Composition or Literature
        /// 5 = 11th grade Honors Mathematics Composition or Literature
        /// 6 = 11th grade Mathematics Composition or Literature
        /// 7 = 10th grade(or lower) Mathematics Composition or Literature
        /// </summary>
        [Range(0, 7)]
        [Column(TypeName = "tinyint")]
        public byte? APEnglishCompletedCourseId { get; set; }

        /// <summary>
        /// CCC Apply Self-Report: Grade of Highest Mathematics Course Completed
        /// A
        /// A-
        /// B+
        /// B
        /// B-
        /// C+
        /// C
        /// C-
        /// D
        /// F
        /// P
        /// NP
        /// ON
        /// X
        /// </summary>
        [RegularExpression(@"^(?:A|B|C){1}(?:-|\+)?$|^(?:D|F|NP|P|ON|X)$")]
        [Column(TypeName = "varchar(3)")]
        public string APEnglishCompletedCourseGrade { get; set; }

        /// <summary>
        /// CCC Apply Self-Report: Highest Mathematics Course Completed
        /// 0  = Unknown
        /// 1  = Pre-algebra or lower
        /// 2  = Algebra 1
        /// 3  = Integrated Math 1
        /// 4  = Integrated Math 2
        /// 5  = Geometry
        /// 6  = Algebra 2
        /// 7  = Integrated Math 3
        /// 8  = Statistics
        /// 9  = Integrated Math 4
        /// 10 = Trigonometry
        /// 11 = Pre-calculus
        /// 12 = Calculus or higher
        /// 13 = Math Analysis
        /// </summary>
        [Range(0, 13)]
        [Column(TypeName = "tinyint")]
        public byte? APMathematicsCompletedCourseId { get; set; }

        /// <summary>
        /// CCC Apply Self-Report: Grade of Highest Mathematics Course Completed
        /// A
        /// A-
        /// B+
        /// B
        /// B-
        /// C+
        /// C
        /// C-
        /// D
        /// F
        /// P
        /// NP
        /// ON
        /// X
        /// </summary>
        [RegularExpression(@"^(?:A|B|C){1}(?:-|\+)?$|^(?:D|F|NP|P|ON|X)$")]
        [Column(TypeName = "varchar(3)")]
        public string APMathematicsCompletedCourseGrade { get; set; }

        /// <summary>
        /// CCC Apply Self-Report: Highest Mathematics Passed
        /// 0  = Unknown
        /// 1  = Pre-algebra or lower
        /// 2  = Algebra 1
        /// 3  = Integrated Math 1
        /// 4  = Integrated Math 2
        /// 5  = Geometry
        /// 6  = Algebra 2
        /// 7  = Integrated Math 3
        /// 8  = Statistics
        /// 9  = Integrated Math 4
        /// 10 = Trigonometry
        /// 11 = Pre-calculus
        /// 12 = Calculus or higher
        /// 13 = Math Analysis
        /// </summary>
        [Range(0, 13)]
        [Column(TypeName = "tinyint")]
        public byte? APMathematicsPassedCourseId { get; set; }

        /// <summary>
        /// CCC Apply Self-Report: Grade of Highest Mathematics Course Passed
        /// A
        /// A-
        /// B+
        /// B
        /// B-
        /// C+
        /// C
        /// C-
        /// D
        /// F
        /// P
        /// NP
        /// ON
        /// X
        /// </summary>
        [RegularExpression(@"^(?:A|B|C){1}(?:-|\+)?$|^(?:D|F|NP|P|ON|X)$")]
        [Column(TypeName = "varchar(3)")]
        public string APMathematicsPassedCourseGrade { get; set; }
        #endregion
    }
}