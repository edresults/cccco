﻿using MultipleMeasures.Helpers;
using MultipleMeasures.Entities;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Data.SqlClient;
using System.Linq;

namespace MultipleMeasures.Data
{
    public class MmapRepository : IMmapRepository
    {
        private readonly MmapContext _context;
        private readonly ILogger<MmapRepository> _logger;
        private readonly IMapper _mapper;

        public MmapRepository(MmapContext context, ILogger<MmapRepository> logger, IMapper mapper)
        {
            _context = context;
            _logger = logger;
            _mapper = mapper;
        }

        public Placement GetPlacement(string studentId)
        {
            var binaryArray = HexHelper.StringToByteArray(studentId);

            try
            {
                _logger.LogInformation("GetPlacement called");

                return _context.Placements
                    .Where(p => p.StudentId == binaryArray).FirstOrDefault();
            }
            catch (Exception ex)
            {

                _logger.LogError($"GetPlacement failed: {ex}");

                return null;
            }
        }

        public Transcript GetTranscript(string studentId)
        {
            var binaryArray = HexHelper.StringToByteArray(studentId);

            try
            {
                _logger.LogInformation("GetTranscript called");

                return _context.Transcripts
                    .Where(p => p.StudentId == binaryArray).FirstOrDefault();
            }
            catch (Exception ex)
            {

                _logger.LogError($"GetTranscript failed: {ex}");

                return null;
            }
        }

        public void AddEntity(object model)
        {
            _context.Add(model);
        }

        public void AddEntities(object[] model)
        {
            _context.AddRange(model);
        }

        public bool SaveAll()
        {
            return _context.SaveChanges() > 0;
        }
    }
}
