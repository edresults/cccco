﻿using MultipleMeasures.Helpers;
using MultipleMeasures.Entities;
using MultipleMeasures.ViewModels;
using AutoMapper;

namespace AB705.Data
{
    public class MmapMappingProfile : Profile
    {
        public MmapMappingProfile()
        {
            CreateMap<Placement, PlacementViewModel>()
                .ReverseMap();

            CreateMap<Transcript, TranscriptViewModel>()
                .ReverseMap();

            CreateMap<PescTranscript, PescTranscriptViewModel>()
                .ReverseMap();
        }
    }
}
