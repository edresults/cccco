﻿using MultipleMeasures.Entities;

namespace MultipleMeasures.Data
{
    public interface IMmapRepository
    {
        Placement GetPlacement(string studentId);
        Transcript GetTranscript(string studentId);

        void AddEntity(object model);

        void AddEntities(object[] model);

        bool SaveAll();
    }
}