﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MultipleMeasures.Entities
{
    [Table("Transcript", Schema = "mmap")]
    public class Transcript
    {
        /// <summary>
        /// Hashed Student Identifier
        /// </summary>
        [Key]
        [Column(TypeName = "varbinary(64)")]
        public byte[] StudentId { get; set; }

        /// <summary>
        /// Source Of Transcript Data 
        /// 1 = California College Guidance Initiative (CCGI)
        /// 2 = Cal-PASS Plus (CPP)
        /// 3 = CCC Apply
        /// </summary>
        public byte DataSource { get; set; }

        /// <summary>
        /// Student Enrolled in 9th Grade Coursework
        /// </summary>
        public bool IsHighSchoolGrade09 { get; set; }

        /// <summary>
        /// Student Enrolled in 10th Grade Coursework
        /// </summary>
        public bool IsHighSchoolGrade10 { get; set; }

        /// <summary>
        /// Student Enrolled in 11th Grade Coursework
        /// </summary>
        public bool IsHighSchoolGrade11 { get; set; }

        /// <summary>
        /// Student Enrolled in 12th Grade Coursework
        /// </summary>
        public bool IsHighSchoolGrade12 { get; set; }

        /// <summary>
        /// Unweighted Cumulative Grade Point Average
        /// </summary>
        [Column(TypeName = "decimal(3,2)")]
        public decimal? CumulativeGradePointAverage { get; set; }

        /// <summary>
        /// Highest English Course Grade Points
        /// </summary>
        [Column(TypeName = "decimal(2,1)")]
        public decimal? English { get; set; }

        /// <summary>
        /// Preparatory Algebra Grade Points
        /// </summary>
        [Column(TypeName = "decimal(2,1)")]
        public decimal? PreAlgebra { get; set; }

        /// <summary>
        /// Beginning Algebra Grade Points
        /// </summary>
        [Column(TypeName = "decimal(2,1)")]
        public decimal? AlgebraI { get; set; }

        /// <summary>
        /// Geometry Grade Points
        /// </summary>
        [Column(TypeName = "decimal(2,1)")]
        public decimal? Geometry { get; set; }

        /// <summary>
        /// Intermediate Algebra Grade Points
        /// </summary>
        [Column(TypeName = "decimal(2,1)")]
        public decimal? AlgebraII { get; set; }

        /// <summary>
        /// Trigonometry Grade Points
        /// </summary>
        [Column(TypeName = "decimal(2,1)")]
        public decimal? Trigonometry { get; set; }

        /// <summary>
        /// Preparatory Calculus Grade Points
        /// </summary>
        [Column(TypeName = "decimal(2,1)")]
        public decimal? PreCalculus { get; set; }

        /// <summary>
        /// Statistics Grade Points
        /// </summary>
        [Column(TypeName = "decimal(2,1)")]
        public decimal? Statistics { get; set; }

        /// <summary>
        /// Calculus Grade Points
        /// </summary>
        [Column(TypeName = "decimal(2,1)")]
        public decimal? Calculus { get; set; }

        /// <summary>
        /// Record Creation Date
        /// </summary>
        public DateTime? CreateDate { get; set; }

        /// <summary>
        /// Record Modification Date
        /// </summary>
        public DateTime? ModifyDate { get; set; }
    }
}