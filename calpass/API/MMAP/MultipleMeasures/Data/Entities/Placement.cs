﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MultipleMeasures.Entities
{
    [Table("Placement", Schema = "mmap")]
    public class Placement
    {
        /// <summary>
        /// Hashed Student Identifier
        /// </summary>
        [Key]
        [Column(TypeName = "varbinary(64)")]
        public byte[] StudentId { get; set; }

        /// <summary>
        /// Source Of Transcript Data 
        /// 1 = California College Guidance Initiative (CCGI)
        /// 2 = Cal-PASS Plus (CPP)
        /// 3 = CCC Apply
        /// </summary>
        public byte DataSource { get; set; }

        /// <summary>
        /// College Level
        /// </summary>        
        public bool EnglishY { get; set; }

        /// <summary>
        /// One Level Below College Level
        /// </summary>        
        public bool EnglishA { get; set; }

        /// <summary>
        /// Two Levels Below College Level
        /// </summary>        
        public bool EnglishB { get; set; }

        /// <summary>
        /// Three Levels Below College Level
        /// </summary>        
        public bool EnglishC { get; set; }

        /// <summary>
        /// Preparatory Algebra
        /// </summary>        
        public bool PreAlgebra { get; set; }

        /// <summary>
        /// Beginning Algebra
        /// </summary>        
        public bool AlgebraI { get; set; }

        /// <summary>
        /// Intermediate Algebra
        /// </summary>        
        public bool AlgebraII { get; set; }

        /// <summary>
        /// Intermediate Algebra
        /// </summary>        
        public bool MathGE { get; set; }

        /// <summary>
        /// Intermediate Algebra
        /// </summary>        
        public bool Statistics { get; set; }

        /// <summary>
        /// College Algebra
        /// </summary>        
        public bool CollegeAlgebra { get; set; }

        /// <summary>
        /// Trigonometry
        /// </summary>
        
        public bool Trigonometry { get; set; }

        /// <summary>
        /// Preparatory Calculus
        /// </summary>        
        public bool PreCalculus { get; set; }

        /// <summary>
        /// Calculus
        /// </summary>        
        public bool CalculusI { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is College Level
        /// Met College Reading Requirement
        /// </summary>        
        public bool ReadingM_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is College Level
        /// College Level Reading
        /// </summary>        
        public bool ReadingY_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is College Level
        /// One Level Below College Level
        /// </summary>        
        public bool ReadingA_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is College Level
        /// Two Level Below College Level
        /// </summary>        
        public bool ReadingB_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is College Level
        /// Three Levels Below College Level
        /// </summary>        
        public bool ReadingC_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is One Level Below College Level
        /// Met College Reading Requirement
        /// </summary>        
        public bool ReadingM_UboundA { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is One Level Below College Level
        /// One Level Below College Level
        /// </summary>        
        public bool ReadingA_UboundA { get; set; }
        
        /// <summary>
        /// Terminal Level in Reading Sequence Is One Level Below College Level
        /// Two Levels Below College Level
        /// </summary>
        public bool ReadingB_UboundA { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is One Level Below College Level
        /// Three Levels Below College Level
        /// </summary>        
        public bool ReadingC_UboundA { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is Two Levels Below College Level
        /// Met College Reading Requirement
        /// </summary>        
        public bool ReadingM_UboundB { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is Two Levels Below College Level
        /// Two Levels Below College Level
        /// </summary>        
        public bool ReadingB_UboundB { get; set; }

        /// <summary>
        /// Terminal Level in Reading Sequence Is Two Levels Below College Level
        /// Three Levels Below College Level
        /// </summary>        
        public bool ReadingC_UboundB { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is College Level
        /// College Level
        /// </summary>        
        public bool EslY_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is College Level
        /// One Level Below College Level
        /// </summary>        
        public bool EslA_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is College Level
        /// Two Levels Below College Level
        /// </summary>        
        public bool EslB_UboundY { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is One Level Below College Level
        /// One Level Below College Level
        /// </summary>        
        public bool EslA_UboundA { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is One Level Below College Level
        /// Two Levels Below College Level
        /// </summary>        
        public bool EslB_UboundA { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is One Level Below College Level
        /// Three Levels Below College Level
        /// </summary>        
        public bool EslC_UboundA { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is Two Levels Below College Level
        /// Two Levels Below College Level
        /// </summary>        
        public bool EslB_UboundB { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is Two Levels Below College Level
        /// Three Levels Below College Level
        /// </summary>        
        public bool EslC_UboundB { get; set; }

        /// <summary>
        /// Terminal Level in Credit ESL Sequence Is Two Levels Below College Level
        /// Four Levels Below College Level
        /// </summary>
        public bool EslD_UboundB { get; set; }

        /// <summary>
        /// Record Creation Date
        /// </summary>
        public DateTime? CreateDate { get; set; }

        /// <summary>
        /// Record Modification Date
        /// </summary>
        public DateTime? ModifyDate { get; set; }
    }
}