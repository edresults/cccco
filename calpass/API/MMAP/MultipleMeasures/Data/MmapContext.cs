﻿using Microsoft.EntityFrameworkCore;
using MultipleMeasures.Entities;

namespace MultipleMeasures.Data
{
    public class MmapContext : DbContext
    {
        public DbSet<Placement> Placements { get; set; }
        public DbSet<Transcript> Transcripts { get; set; }
        public DbSet<PescTranscript> PescTranscripts { get; set; }

        public MmapContext(DbContextOptions<MmapContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PescTranscript>()
                .HasKey(pk => 
                    new
                    {
                        pk.SchoolAssignedPersonID,
                        pk.LocalOrganizationIDCode,
                        pk.SessionSchoolYear,
                        pk.SessionTermCode,
                        pk.OriginalCourseID
                    }
                );

            ////
            //// Support: Form
            ////
            //modelBuilder.Entity<Support>()
            //    .HasIndex(c => c.Description)
            //    .IsUnique();

            ////
            //// Support: Seed
            ////
            //modelBuilder.Entity<Support>()
            //    .HasData(
            //        new Support { Id = 1, Description = "Support Not Recommended" },
            //        new Support { Id = 2, Description = "Support Recommended" },
            //        new Support { Id = 3, Description = "Support Strongly Recommended" }
            //    );

            ////
            //// Support: Relationships
            ////
            //modelBuilder.Entity<Support>()
            //    .HasMany(c => c.EnglishPlacements)
            //    .WithOne(p => p.English)
            //    .HasForeignKey(p => p.EnglishId)
            //    .OnDelete(DeleteBehavior.SetNull);

            //modelBuilder.Entity<Support>()
            //    .HasMany(c => c.SlamPlacements)
            //    .WithOne(p => p.Slam)
            //    .HasForeignKey(p => p.SlamId)
            //    .OnDelete(DeleteBehavior.SetNull);

            //modelBuilder.Entity<Support>()
            //    .HasMany(c => c.StemPlacements)
            //    .WithOne(p => p.Stem)
            //    .HasForeignKey(p => p.StemId)
            //    .OnDelete(DeleteBehavior.SetNull);

            ////
            //// CourseGrade: Form
            ////
            //modelBuilder.Entity<CourseGrade>()
            //    .HasIndex(c => c.Code)
            //    .IsUnique();
            //modelBuilder.Entity<CourseGrade>()
            //    .HasIndex(c => c.Description)
            //    .IsUnique();

            ////
            //// CourseGrade: Seed
            ////
            //modelBuilder.Entity<CourseGrade>()
            //    .HasData(
            //        //new CourseGrade { Id = 1, Code = "A", Description = "Course Grade of 'A'" },
            //        //new CourseGrade { Id = 2, Code = "A-", Description = "Course Grade of 'A-'" },
            //        //new CourseGrade { Id = 3, Code = "B+", Description = "Course Grade of 'B+'" },
            //        //new CourseGrade { Id = 4, Code = "B", Description = "Course Grade of 'B'" },
            //        //new CourseGrade { Id = 5, Code = "B-", Description = "Course Grade of 'B-'" },
            //        //new CourseGrade { Id = 6, Code = "C+", Description = "Course Grade of 'C+'" },
            //        //new CourseGrade { Id = 7, Code = "C", Description = "Course Grade of 'C'" },
            //        //new CourseGrade { Id = 8, Code = "C-", Description = "Course Grade of 'C-'" },
            //        //new CourseGrade { Id = 9, Code = "D+", Description = "Course Grade of 'D+'" },
            //        //new CourseGrade { Id = 10, Code = "D", Description = "Course Grade of 'D'" },
            //        //new CourseGrade { Id = 11, Code = "D-", Description = "Course Grade of 'D-'" },
            //        //new CourseGrade { Id = 12, Code = "F", Description = "Course Grade of 'F'" },
            //        //new CourseGrade { Id = 13, Code = "P", Description = "Course Grade of 'P'" },
            //        //new CourseGrade { Id = 14, Code = "NP", Description = "Course Grade of 'NP'" },
            //        //new CourseGrade { Id = 15, Code = "O", Description = "Other Non-Passing Grade" }
            //        new CourseGrade { Code = "A", Description = "Course Grade of 'A'" },
            //        new CourseGrade { Code = "A-", Description = "Course Grade of 'A-'" },
            //        new CourseGrade { Code = "B+", Description = "Course Grade of 'B+'" },
            //        new CourseGrade { Code = "B", Description = "Course Grade of 'B'" },
            //        new CourseGrade { Code = "B-", Description = "Course Grade of 'B-'" },
            //        new CourseGrade { Code = "C+", Description = "Course Grade of 'C+'" },
            //        new CourseGrade { Code = "C", Description = "Course Grade of 'C'" },
            //        new CourseGrade { Code = "C-", Description = "Course Grade of 'C-'" },
            //        new CourseGrade { Code = "D+", Description = "Course Grade of 'D+'" },
            //        new CourseGrade { Code = "D", Description = "Course Grade of 'D'" },
            //        new CourseGrade { Code = "D-", Description = "Course Grade of 'D-'" },
            //        new CourseGrade { Code = "F", Description = "Course Grade of 'F'" },
            //        new CourseGrade { Code = "P", Description = "Course Grade of 'P'" },
            //        new CourseGrade { Code = "NP", Description = "Course Grade of 'NP'" },
            //        new CourseGrade { Code = "O", Description = "Other Non-Passing Grade" }
            //    );

            ////
            //// CourseGrade: Relationships
            ////
            //modelBuilder.Entity<CourseGrade>()
            //    .HasMany(c => c.EnglishCompleteGrades)
            //    .WithOne(p => p.EnglishCompleteGrade)
            //    .HasForeignKey(p => p.EnglishCompleteGradeCode);

            //modelBuilder.Entity<CourseGrade>()
            //    .HasMany(c => c.MathematicsAttemptGrades)
            //    .WithOne(p => p.MathematicsAttemptGrade)
            //    .HasForeignKey(p => p.MathematicsAttemptGradeCode);

            //modelBuilder.Entity<CourseGrade>()
            //    .HasMany(c => c.MathematicsCompleteGrades)
            //    .WithOne(p => p.MathematicsCompleteGrade)
            //    .HasForeignKey(p => p.MathematicsCompleteGradeCode);

            ////
            //// EnglishCourse: Form
            ////
            //modelBuilder.Entity<EnglishCourse>()
            //    .HasIndex(c => c.Description)
            //    .IsUnique();

            ////
            //// EnglishCourse: Seed
            ////
            //modelBuilder.Entity<EnglishCourse>()
            //    .HasData(
            //        new EnglishCourse { Id = 1, Description = "12th grade Advanced Placement (AP) English Composition or Literature" },
            //        new EnglishCourse { Id = 2, Description = "12th grade Honors English Composition or Literature" },
            //        new EnglishCourse { Id = 3, Description = "12th grade English Composition or Literature" },
            //        new EnglishCourse { Id = 4, Description = "11th grade Advanced Placement (AP) English Composition or Literature" },
            //        new EnglishCourse { Id = 5, Description = "11th grade Honors English Composition or Literature" },
            //        new EnglishCourse { Id = 6, Description = "11th grade English Composition or Literature" },
            //        new EnglishCourse { Id = 7, Description = "10th grade (or lower) English Composition or Literature" }
            //    );

            ////
            //// EnglishCourse: Relationships
            ////
            //modelBuilder.Entity<EnglishCourse>()
            //    .HasMany(c => c.EnglishCompleteCourses)
            //    .WithOne(p => p.EnglishCompleteCourse)
            //    .HasForeignKey(p => p.EnglishCompleteCourseId)
            //    .OnDelete(DeleteBehavior.SetNull);

            ////
            //// MathematicsCourse: Form
            ////
            //modelBuilder.Entity<MathematicsCourse>()
            //    .HasIndex(c => c.Description)
            //    .IsUnique();

            ////
            //// MathematicsCourse: Seed
            ////
            //modelBuilder.Entity<MathematicsCourse>()
            //    .HasData(
            //        new MathematicsCourse { Id = 1, Description = "Pre-algebra or lower" },
            //        new MathematicsCourse { Id = 2, Description = "Algebra 1" },
            //        new MathematicsCourse { Id = 3, Description = "Integrated Math 1" },
            //        new MathematicsCourse { Id = 4, Description = "Integrated Math 2" },
            //        new MathematicsCourse { Id = 5, Description = "Algebra 2" },
            //        new MathematicsCourse { Id = 6, Description = "Integrated Math 3" },
            //        new MathematicsCourse { Id = 7, Description = "Statistics" },
            //        new MathematicsCourse { Id = 8, Description = "Integrated Math 4" },
            //        new MathematicsCourse { Id = 9, Description = "Trigonometry" },
            //        new MathematicsCourse { Id = 10, Description = "Math Analysis" },
            //        new MathematicsCourse { Id = 11, Description = "Pre-calculus" },
            //        new MathematicsCourse { Id = 12, Description = "Calculus or higher" }
            //    );

            ////
            //// MathematicsCourse: Relationships
            ////
            //modelBuilder.Entity<MathematicsCourse>()
            //    .HasMany(c => c.MathematicsCompleteCourses)
            //    .WithOne(p => p.MathematicsCompleteCourse)
            //    .HasForeignKey(p => p.MathematicsCompleteCourseId);

            //modelBuilder.Entity<MathematicsCourse>()
            //    .HasMany(c => c.MathematicsAttemptCourses)
            //    .WithOne(p => p.MathematicsAttemptCourse)
            //    .HasForeignKey(p => p.MathematicsAttemptCourseId);

            ////
            //// Placement: Seed
            ////
            //modelBuilder.Entity<Placement>()
            //    .HasData(
            //        new Placement[]
            //        {
            //            new Placement
            //            {
            //                StudentId = "0123456789",
            //                EnglishId = 1,
            //                SlamId = 2,
            //                StemId = 3,
            //                CreateDate = DateTime.Now,
            //                ModifyDate = DateTime.Now
            //            },
            //            new Placement
            //            {
            //                StudentId = "1111111111",
            //                EnglishId = 2,
            //                SlamId = 2,
            //                StemId = 2,
            //                CreateDate = DateTime.Now,
            //                ModifyDate = DateTime.Now
            //            },
            //        }
            //    );

            ////
            //// Transcript: Seed
            ////
            //modelBuilder.Entity<Transcript>()
            //    .HasData(
            //        new Transcript[]
            //        {
            //            new Transcript {
            //                StudentId = "0123456789",
            //                CumulativeGradePointAverage = 3.63M,
            //                EnglishCompleteCourseId = 1,
            //                EnglishCompleteGradeCode = "A",
            //                MathematicsAttemptCourseId = 9,
            //                MathematicsAttemptGradeCode = "D+",
            //                MathematicsCompleteCourseId = 7,
            //                MathematicsCompleteGradeCode = "C",
            //                CreateDate = DateTime.Now,
            //                ModifyDate = DateTime.Now
            //            },
            //            new Transcript {
            //                StudentId = "1111111111",
            //                CumulativeGradePointAverage = 3.33M,
            //                EnglishCompleteCourseId = 3,
            //                EnglishCompleteGradeCode = "B",
            //                MathematicsAttemptCourseId = 6,
            //                MathematicsAttemptGradeCode = "B-",
            //                MathematicsCompleteCourseId = 6,
            //                MathematicsCompleteGradeCode = "B-",
            //                CreateDate = DateTime.Now,
            //                ModifyDate = DateTime.Now
            //            },
            //        }
            //    );
        }
    }
}
