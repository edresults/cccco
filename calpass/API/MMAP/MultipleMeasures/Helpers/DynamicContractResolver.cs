﻿using MultipleMeasures.ViewModels;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;

namespace MultipleMeasures.Helpers
{
    public class DynamicContractResolver : DefaultContractResolver
    {
        public new static readonly DynamicContractResolver Instance = new DynamicContractResolver();

        protected override JsonContract CreateContract(Type objectType)
        {
            JsonContract contract = base.CreateContract(objectType);

            if (objectType == typeof(RetrospectiveESL))
            {
                this.NamingStrategy = new DefaultNamingStrategy();
            }
            else
            {
                this.NamingStrategy = new CamelCaseNamingStrategy();
            }

            return contract;
        }
    }
}


//protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
//{
//    JsonProperty property = base.CreateProperty(member, memberSerialization);

//    if (property.DeclaringType == typeof(RetrospectiveESL))
//    {
//        property.PropertyName = property.PropertyName;
//    }
//    else
//    {
//        property.PropertyName = property.PropertyName.
//            }

//    return property;
//}