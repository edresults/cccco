﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MultipleMeasures.Data;
using MultipleMeasures.Helpers;
using MultipleMeasures.Swagger;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Reflection;

namespace MultipleMeasures
{
    public class Startup
    {
        public IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddCors();

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(action =>
                {
                    action.Authority = _configuration["AppSettings:Authority"];
                    action.RequireHttpsMetadata = false;
                    action.ApiName = "api";
                }
             );

            services.AddMvcCore().AddAuthorization(action =>
                {
                    action.AddPolicy("PlacementUsers", policy =>
                        policy.RequireAssertion(context =>
                            context.User.HasClaim(claim =>
                                (claim.Type == "Admin" && claim.Value == "True") ||
                                (claim.Type == "MmapUser" && claim.Value == "True") ||
                                (claim.Type == "MmppUser" && claim.Value == "True") ||
                                (claim.Type == "TranscriptUser" && claim.Value == "True")
                            )
                        )
                    );
                    action.AddPolicy("TranscriptUsers", policy =>
                        policy.RequireAssertion(context =>
                            context.User.HasClaim(claim =>
                                (claim.Type == "Admin" && claim.Value == "True") ||
                                (claim.Type == "TranscriptUser" && claim.Value == "True")
                            )
                        )
                    );

                    action.AddPolicy("MmppUsers", policy =>
                        policy.RequireAssertion(context =>
                            context.User.HasClaim(claim =>
                                (claim.Type == "Admin" && claim.Value == "True") ||
                                (claim.Type == "MmppUser" && claim.Value == "True")
                            )
                        )
                    );
                }
            );

            services.AddMvcCore().AddApiExplorer();
            services.AddMvcCore().AddJsonFormatters();
            //services.AddMvcCore().AddJsonFormatters(options =>
            //{
            //    options.ContractResolver = new DynamicContractResolver();
            //});
            services.AddMvcCore().AddRazorViewEngine();

            services.AddAutoMapper();

            services.AddDbContext<MmapContext>(action =>
                {
                    action.UseSqlServer(_configuration.GetConnectionString("DefaultConnection"));
                }
            );

            services.AddScoped<IMmapRepository, MmapRepository>();

            services.AddSwaggerGen(action =>
                {
                    action.SchemaFilter<Examples>();
                    action.SwaggerDoc("v1",
                        new Info
                        {
                            Title = "MMAP API",
                            Version = "v1",
                            Description = "Click <a href='/Documentation/Authentication'>here</a> for information regarding Authorization.",
                            TermsOfService = ""
                        }
                     );

                    action.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
                    action.DescribeAllEnumsAsStrings();
                    action.DocumentFilter<DocumentFilters>();
                }
            );            
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseAuthentication();

            app.UseMvc(action =>
                {
                    action.MapRoute(
                        name: "default",
                        template: "{controller=Documentation}/{action=Authentication}/{id?}");
                }
            );

            app.UseSwagger();

            app.UseSwaggerUI(action =>
                {
                    action.SwaggerEndpoint("/swagger/v1/swagger.json", "MMAP API V1");
                    action.RoutePrefix = "docs";
                }
            );
        }
    }
}
