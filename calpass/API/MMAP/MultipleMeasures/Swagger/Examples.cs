﻿using MultipleMeasures.ViewModels;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;

namespace MultipleMeasures.Swagger
{
    public class Examples : ISchemaFilter
    {
        public void Apply(Schema model, SchemaFilterContext context)
        {
            if (context.SystemType == typeof(PescTranscriptViewModel[]))
            {
                model.Example = new PescTranscriptViewModel[]
                {
                    new PescTranscriptViewModel()
                    {
                        SchoolAssignedPersonID = "1234567890",
                        RecipientAssignedID = "1234567890",
                        BirthDate = "1944-06-06",
                        FirstName = "Jane",
                        LastName = "Doe",
                        CEEBACT = "123456",
                        LocalOrganizationIDCode = "12345678901234",
                        StudentLevelCode = "TwelfthGrade",
                        OrganizationName = "Test High School",
                        AcademicCompletionDate = null,
                        GradePointAverage = "3.93",
                        AcademicSummaryType = "Weighted",
                        SessionSchoolYear = "2015-2016",
                        SessionTermCode = "S1",
                        OriginalCourseID = "ENGL12",
                        AgencyCourseID = "2133",
                        CourseSectionNumber = "1234567",
                        CourseTitle = "Test Course Title",
                        CourseCreditBasis = "HighSchoolCreditOnly",
                        CourseCreditUnits = "Semester",
                        CourseCreditValue = "5.00",
                        CourseCreditEarned = "5.00",
                        CourseAcademicGrade = "A+",
                        CourseAcademicGradeStatusCode = null,
                        CourseHonorsCodeType = null
                    }
                };
            }

            if (context.SystemType == typeof(PlacementViewModel))
            {
                model.Example = new PlacementViewModel()
                {
                    DataSource = 1,
                    EnglishY = true,
                    EnglishA = true,
                    EnglishB = true,
                    EnglishC = true,
                    PreAlgebra = true,
                    AlgebraI = true,
                    AlgebraII = true,
                    MathGE = true,
                    Statistics = true,
                    CollegeAlgebra = true,
                    Trigonometry = true,
                    PreCalculus = true,
                    CalculusI = true,
                    ReadingM_UboundY = true,
                    ReadingY_UboundY = true,
                    ReadingA_UboundY = true,
                    ReadingB_UboundY = true,
                    ReadingC_UboundY = true,
                    ReadingM_UboundA = true,
                    ReadingA_UboundA = true,
                    ReadingB_UboundA = true,
                    ReadingC_UboundA = true,
                    ReadingM_UboundB = true,
                    ReadingB_UboundB = true,
                    ReadingC_UboundB = true,
                    EslY_UboundY = true,
                    EslA_UboundY = true,
                    EslB_UboundY = true,
                    EslA_UboundA = true,
                    EslB_UboundA = true,
                    EslC_UboundA = true,
                    EslB_UboundB = true,
                    EslC_UboundB = true,
                    EslD_UboundB = true,
                    CreateDate = DateTime.Now,
                    ModifyDate = DateTime.Now
                };
            }

            if (context.SystemType == typeof(TranscriptViewModel))
            {
                model.Example = new TranscriptViewModel()
                {
                    DataSource = 1,
                    IsHighSchoolGrade09 = true,
                    IsHighSchoolGrade10 = true,
                    IsHighSchoolGrade11 = true,
                    IsHighSchoolGrade12 = false,
                    CumulativeGradePointAverage = 3.16m,
                    English = 3.7m,
                    PreAlgebra = 3.7m,
                    AlgebraI = 3.3m,
                    Geometry = 3.0m,
                    AlgebraII = 3.0m,
                    Trigonometry = 3.3m,
                    PreCalculus = 3.3m,
                    Statistics = 2.3m,
                    Calculus = 3.7m,
                    CreateDate = DateTime.Now,
                    ModifyDate = DateTime.Now
                };
            }
        }
    }
}