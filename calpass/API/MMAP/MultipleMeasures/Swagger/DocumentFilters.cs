﻿using MultipleMeasures.ViewModels;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace MultipleMeasures.Swagger
{
    public class DocumentFilters : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            context.SchemaRegistry.GetOrRegister(typeof(RetrospectiveESL));
            context.SchemaRegistry.GetOrRegister(typeof(RetrospectiveEnglish));
            context.SchemaRegistry.GetOrRegister(typeof(RetrospectiveMathematics));
        }
    }
}
