-- a. gender
-- b. race/ethnicity (IPEDS)
-- c. age
-- d. educational goal
-- e. BOG flag
-- f. Pell flag
-- g. EOPS flag
-- h. DSPS flag
-- i. Rank/CB21 of first English course
-- j. Rank/CB21 of first math course
-- k. Academic year of first CC enrollment

USE calpass;

GO

IF (object_id('comis.Aggregates') is not null)
	BEGIN
		DROP TABLE comis.Aggregates;
	END;

GO

CREATE TABLE
	comis.Aggregates
	(
		InterSegmentKey   binary(64)  not null,
		YearTermCodeFirst char(5)         null,
		GenderCode        char(1)         null,
		Race              char(1)         null,
		AgeCode           varchar(25)     null,
		EdGoal            char(1)         null,
		IsBog             bit             null,
		IsPell            bit             null,
		IsEops            bit             null,
		IsDsps            bit             null,
		EnglLevelCode     char(1)         null,
		MathLevelCode     char(1)         null
	);

GO

ALTER TABLE
	comis.Aggregates
ADD CONSTRAINT
	PK_Aggregates
PRIMARY KEY
	(
		InterSegmentKey
	);

GO

INSERT INTO
	comis.Aggregates
	(
		InterSegmentKey,
		EnglLevelCode,
		MathLevelCode
	)
SELECT
	ccc.InterSegmentKey,
	EnglLevelCode = max(case when ccc.TopCode = '150100' then CourseLevel end),
	MathLevelCode = max(case when ccc.TopCode = '170100' then CourseLevel end)
FROM
	dbo.Student s
	inner join
	mmap.CollegeCourseContent ccc
		on ccc.InterSegmentKey = s.InterSegmentKey
WHERE
	ccc.LevelSelector = 1
	and ccc.FirstSelector = 1
	and s.IsGrade11SectionInclusive = 1
	and s.IsHighSchoolCollision = 0
GROUP BY
	ccc.InterSegmentKey;

UPDATE
	t
SET
	t.YearTermCodeFirst = s.YearTermCodeFirst
FROM
	comis.Aggregates t
	inner join
	(
		SELECT
			a.InterSegmentKey,
			YearTermCodeFirst = min(t.YearTermCode)
		FROM
			comis.Aggregates a
			inner join
			comis.studntid id
				on id.InterSegmentKey = a.InterSegmentKey
			inner join
			comis.stterm st
				on st.college_id = id.college_id
				and st.student_id = id.student_id
			inner join
			comis.sxenrlm sx
				on sx.college_id = st.college_id
				and sx.student_id = st.student_id
				and sx.term_id = st.term_id
			inner join
			comis.Mark m
				on m.MarkCode = sx.Grade
			inner join
			comis.Term t
				on t.TermCode = st.term_id
		WHERE
			m.IsEnroll = 1
		GROUP BY
			a.InterSegmentKey
	) s
		on s.InterSegmentKey = t.InterSegmentKey;

UPDATE
	t
SET
	t.GenderCode = s.GenderCode,
	t.Race       = s.Race,
	t.AgeCode    = s.AgeCode,
	t.EdGoal     = s.EdGoal
FROM
	comis.Aggregates t
	inner join
	(
		SELECT
			*
		FROM
			(
				SELECT
					id.InterSegmentKey,
					GenderCode = st.gender,
					Race       = st.ipeds_race,
					AgeCode    = g.Label,
					EdGoal     = st.goal,
					Selector   = row_number() over(partition by a.InterSegmentKey order by (select 1))
				FROM
					comis.studntid id

				WHERE
					exists (
						SELECT
							1
						FROM
							comis.Aggregates a
						WHERE
							id.InterSegmentKey = a.InterSegmentKey
							and t.YearTermCode = a.YearTermCodeFirst
					)
			)
		WHERE

	) s
		on s.InterSegmentKey = t.InterSegmentKey;