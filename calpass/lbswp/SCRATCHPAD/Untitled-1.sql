/*

use Launchboard4Top6;

exec StudentWageTablesLoops 'Overall', 'CC';
 

if object_id('StudentWageTablesLoops', 'P') is not null drop procedure StudentWageTablesLoops;

*/


create procedure StudentWageTablesLoops
	@disagg as nvarchar(100),
	@version as varchar(2),
	@start_year int,
	@end_year int
as

declare @SQLStatement1 nvarchar(4000);
declare @SQLStatement2 nvarchar(4000);

declare @metric_disagg nvarchar(100) = @disagg;
declare @metric_subgroup_var nvarchar(100);
declare @metric_subgroup_set nvarchar(100);
declare @equity_table nvarchar(200);
declare @equity_table_statewide nvarchar(200);
declare @equity_table_macroregion nvarchar(200);
declare @equity_table_microregion nvarchar(200);
declare @equity_table_final nvarchar(200);

declare @cohort_table nvarchar(200);

declare @group_list nvarchar(200);
declare @metric_suffix nvarchar(1) = 'f';

create table #Metrics (
	rownum int,
	metric_id nvarchar(3),
	metric_name nvarchar(100)
);

insert into #Metrics
values (1, '503', 'Pre1QWage'),
	(2, '504', 'Pre2QWage'),
	(3, '505', 'Pre3QWage'),
	(4, '506', 'Pre4QWage'),
	(5, '507', 'Post1QWage'),
	(6, '508', 'Post2QWage'),
	(7, '509', 'Post3QWage'),
	(8, '510', 'Post4QWage'),
	(9, '511', 'PostWage'),
	(10, '512', 'WageDiff')
;

create table #Groups (
	rownum int,
	metric_label nvarchar(1),
	sql_group nvarchar(200),
	sql_source nvarchar(200)
);


if @disagg = 'Overall'
	begin
		set @metric_subgroup_var = quotename('None', '''');
		set @metric_subgroup_set = '';
		set @equity_table = 'Launchboard4Top6.dbo.FCStudentWageTableOverall';
		set @equity_table_statewide = 'Launchboard4Top6.dbo.FCStatewideWageTableOverall';
		set @equity_table_macroregion = 'Launchboard4Top6.dbo.FCMacroregionWageTableOverall';
		set @equity_table_microregion = 'Launchboard4Top6.dbo.FCMicroregionWageTableOverall';
		set @equity_table_final = 'Launchboard4Top6.dbo.FCWageTableCombinedOverall';
	end
else if @disagg = 'Gender'
	begin
		set @metric_subgroup_var = 'student_gender';
		set @metric_subgroup_set = ', student_gender';
		set @equity_table = 'Launchboard4Top6.dbo.FCStudentWageTableGender';
		set @equity_table_statewide = 'Launchboard4Top6.dbo.FCStatewideWageTableGender';
		set @equity_table_macroregion = 'Launchboard4Top6.dbo.FCMacroregionWageTableGender';
		set @equity_table_microregion = 'Launchboard4Top6.dbo.FCMicroregionWageTableGender';
		set @equity_table_final = 'Launchboard4Top6.dbo.FCWageTableCombinedGender';
	end
else if @disagg = 'Race/Ethnicity'
	begin
		set @metric_subgroup_var = 'student_ethnicity';
		set @metric_subgroup_set = ', student_ethnicity';
		set @equity_table = 'Launchboard4Top6.dbo.FCStudentWageTableEthnicity';
		set @equity_table_statewide = 'Launchboard4Top6.dbo.FCStatewideWageTableEthnicity';
		set @equity_table_macroregion = 'Launchboard4Top6.dbo.FCMacroregionWageTableEthnicity';
		set @equity_table_microregion = 'Launchboard4Top6.dbo.FCMicroregionWageTableEthnicity';
		set @equity_table_final = 'Launchboard4Top6.dbo.FCWageTableCombinedEthnicity';
	end
else if @disagg = 'Age Group'
	begin
		set @metric_subgroup_var = 'student_age';
		set @metric_subgroup_set = ', student_age';
		set @equity_table = 'Launchboard4Top6.dbo.FCStudentWageTableAge';
		set @equity_table_statewide = 'Launchboard4Top6.dbo.FCStatewideWageTableAge';
		set @equity_table_macroregion = 'Launchboard4Top6.dbo.FCMacroregionWageTableAge';
		set @equity_table_microregion = 'Launchboard4Top6.dbo.FCMicroregionWageTableAge';
		set @equity_table_final = 'Launchboard4Top6.dbo.FCWageTableCombinedAge';
	end
else if @disagg = 'Economically Disadvantaged'
	begin
		set @metric_subgroup_var = 'EconDis';
		set @metric_subgroup_set = ', EconDis';
		set @equity_table = 'Launchboard4Top6.dbo.FCStudentWageTableEconDis';
		set @equity_table_statewide = 'Launchboard4Top6.dbo.FCStatewideWageTableEconDis';
		set @equity_table_macroregion = 'Launchboard4Top6.dbo.FCMacroregionWageTableEconDis';
		set @equity_table_microregion = 'Launchboard4Top6.dbo.FCMicroregionWageTableEconDis';
		set @equity_table_final = 'Launchboard4Top6.dbo.FCWageTableCombinedEconDis';
	end

if @version = 'FC'
	begin

		set @cohort_table = 'Launchboard4Top6.dbo.FCStudentCohortTableMultiYr';

		insert into #Groups
		values (1, 'a', 'COCertFC = 1 and for_credit = 1 and Exiter = 1', 'CCCompleterCohortTableMultiYr'),
			(2, 'c', '(COCertFC = 1 or ADegreeFC = 1 or LocalCertFC = 1 or BDegreeFC = 1) and for_credit = 1 and Exiter = 1', 'CCCompleterCohortTableMultiYr'),
			(3, 'd', 'ADegreeFC = 1 and for_credit = 1 and Exiter = 1', 'CCCompleterCohortTableMultiYr'),
			(4, 'e', 'Exiter = 1', 'CohortWorkingTable'),
			(5, 'j', 'BDegreeFC = 1 and for_credit = 1 and Exiter = 1', 'CCCompleterCohortTableMultiYr'),
			(6, 'k', 'SkillsBuilder = 1', 'FCStudentCohortTableMultiYr'),
			(7, 'l', 'LocalCertFC = 1 and for_credit = 1 and Exiter = 1', 'CCCompleterCohortTableMultiYr'),
			(8, 't', 'Transfer = 1', 'FCStudentCohortTableMultiYr')
		;

	end
else if @version = 'NC'
	begin

		set @cohort_table = 'Launchboard4Top6.dbo.NCStudentCohortTableMultiYr';

		set @equity_table = replace(@equity_table, 'FC', 'NC'); -- set as correct label
		set @equity_table_statewide = replace(@equity_table_statewide, 'FC', 'NC');
		set @equity_table_macroregion = replace(@equity_table_macroregion, 'FC', 'NC');
		set @equity_table_microregion = replace(@equity_table_microregion, 'FC', 'NC');
		set @equity_table_final = replace(@equity_table_final, 'FC', 'NC');
		set @metric_suffix = 'n'

		insert into #Groups
		values (1, 'a', 'COCertNC = 1 and non_credit = 1 and Exiter = 1', 'CCCompleterCohortTableMultiYr'),
			(2, 'c', '(LocalCertNC = 1 or COCertNC = 1) and non_credit = 1 and Exiter = 1', 'CCCompleterCohortTableMultiYr'),
			(3, 'd', 'ADegreeNC = 1 and non_credit = 1 and Exiter = 1', 'CCCompleterCohortTableMultiYr'),
			(4, 'e', 'Exiter = 1', 'CohortWorkingTable'),
			(5, 'j', 'BDegreeNC = 1 and non_credit = 1 and Exiter = 1', 'CCCompleterCohortTableMultiYr'),
			(6, 'l', 'LocalCertNC = 1 and for_credit = 1 and Exiter = 1', 'CCCompleterCohortTableMultiYr'),
			(7, 't', 'Transfer = 1', 'NCStudentCohortTableMultiYr')
		;

	end
else if @version = 'CC'
	begin

		set @cohort_table = 'Launchboard4Top6.dbo.CCStudentCohortTableMultiYr';

		set @equity_table = replace(@equity_table, 'FC', 'CC'); -- set as correct label
		set @equity_table_statewide = replace(@equity_table_statewide, 'FC', 'CC');
		set @equity_table_macroregion = replace(@equity_table_macroregion, 'FC', 'CC');
		set @equity_table_microregion = replace(@equity_table_microregion, 'FC', 'CC');
		set @equity_table_final = replace(@equity_table_final, 'FC', 'CC');
		set @metric_suffix = 'b'

		insert into #Groups
		values (1, 'a', 'COCertCC = 1 and Exiter = 1', 'CCCompleterCohortTableMultiYr'),
			(2, 'c', '(for_credit = 1 or non_credit = 1) and Exiter = 1', 'CCCompleterCohortTableMultiYr'),
			(3, 'd', 'ADegreeCC = 1 and Exiter = 1', 'CCCompleterCohortTableMultiYr'),
			(4, 'e', 'Exiter = 1', 'CohortWorkingTable'),
			(5, 'j', 'BDegreeCC = 1 and Exiter = 1', 'CCCompleterCohortTableMultiYr'),
			(6, 'k', 'SkillsBuilder = 1', 'CCStudentCohortTableMultiYr'),
			(7, 'l', 'LocalCertCC = 1 and Exiter = 1', 'CCCompleterCohortTableMultiYr'),
			(8, 't', 'Transfer = 1', 'CCStudentCohortTableMultiYr')
		;

	end

if object_id('Launchboard4Top6.dbo.BuildStudentWageTableDisaggCube') is not null -- temp table for inserts
	drop table Launchboard4Top6.dbo.BuildStudentWageTableDisaggCube;


create table Launchboard4Top6.dbo.BuildStudentWageTableDisaggCube ( -- temp table for calcs
	metric_id varchar(6),
	academic_year varchar(4),
	macroregion_name varchar(100),
	microregion_name varchar(100),
	cc_ipeds_code varchar(6),
	cte_program varchar(4),
	sector_name varchar(100),
	program_code varchar(6),
	disagg varchar(50),
	subgroup varchar(50),
	metric_value float,
	metric_denom int
);

declare @metric_id_str nvarchar(10);
declare @metric_label nvarchar(1);
declare @metric_name nvarchar(100);
declare @where_str nvarchar(200);
declare @source_str nvarchar(200);

declare @i int = @start_year;
declare @LastYear int = @end_year;

declare @j int = 1;
declare @LastMetric int = (select count(*) from #Metrics);

declare @k int = 1;
declare @LastGroup int = (select count(*) from #Groups);



while @i <= @LastYear
	begin

		while @j <= @LastMetric
			begin

				while @k <= @LastGroup
					begin

						if object_id('Launchboard4Top6.dbo.StudentWageWorkingCube') is not null
							drop table Launchboard4Top6.dbo.StudentWageWorkingCube;

						set @metric_id_str = (select #Metrics.metric_id from #Metrics where #Metrics.rownum = @j);
						set @metric_label = (select #Groups.metric_label from #Groups where #Groups.rownum = @k);
						set @metric_name = (select #Metrics.metric_name from #Metrics where #Metrics.rownum = @j);
						set @where_str = (select #Groups.sql_group from #Groups where #Groups.rownum = @k);
						set @source_str = (select #Groups.sql_source from #Groups where #Groups.rownum = @k);

						set @metric_id_str = concat(@metric_id_str, @metric_suffix, @metric_label);

						if (select metric_label from #Groups where #Groups.rownum = @k) = 'e' -- use distinct of student and completer cohort tables
							begin

								if object_id('Launchboard4Top6.dbo.CohortWorkingTable') is not null
									drop table Launchboard4Top6.dbo.CohortWorkingTable;

								set @SQLStatement2 =
								
									'select distinct *
									into Launchboard4Top6.dbo.CohortWorkingTable
									from (

										select academic_year, macroregion_name, microregion_name, cc_ipeds_code,
											cte_program, sector_name, program_code, student_id, Exiter,
											student_gender, student_ethnicity, student_age, EconDis,
											Pre1QWage, Pre2QWage, Pre3QWage, Pre4QWage,
											Post1QWage, Post2QWage, Post3QWage, Post4QWage,
											PostWage, WageDiff
										from ' + @cohort_table + ' 
										where Exiter = 1
											and academic_year = ' + cast(@i as nvarchar) + '
											
										union all

										select academic_year, macroregion_name, microregion_name, cc_ipeds_code,
											cte_program, sector_name, program_code, student_id, Exiter,
											student_gender, student_ethnicity, student_age, EconDis,
											Pre1QWage, Pre2QWage, Pre3QWage, Pre4QWage,
											Post1QWage, Post2QWage, Post3QWage, Post4QWage,
											PostWage, WageDiff
										from Launchboard4Top6.dbo.CCCompleterCohortTableMultiYr
										where Exiter = 1
											and academic_year = ' + cast(@i as nvarchar) + '

									) as #Cohort;'

								--print @SQLStatement2;
								exec sp_executesql @SQLStatement2;

								
								if object_id('Launchboard4Top6.dbo.CohortWorkingTable') is not null
								create nonclustered index CohortWorkingTableIndex on
									Launchboard4Top6.dbo.CohortWorkingTable
										(academic_year, macroregion_name, microregion_name, cc_ipeds_code,
											cte_program, sector_name, program_code, student_id,
											student_gender, student_ethnicity, student_age, EconDis)
									include (Pre1QWage, Pre2QWage, Pre3QWage, Pre4QWage, Post1QWage, Post2QWage, Post3QWage, Post4QWage,
											PostWage, WageDiff
									)
								;

							end

						set @SQLStatement1 = 

							'select ' + quotename(@metric_id_str, '''') + ' as metric_id, ' +
								cast(@i as varchar) + ' as academic_year, macroregion_name, microregion_name, cc_ipeds_code, ' +
								'cte_program, sector_name, program_code, ' +
								quotename(@metric_disagg, '''') + ' as disagg, ' +  @metric_subgroup_var + ' as subgroup, ' +
								'percentile_cont(.5) within group (order by ' + @metric_name + ') over' +
								'(partition by concat(cc_ipeds_code, cte_program, sector_name, program_code' + @metric_subgroup_set + ')) as metric_value, ' +
								'count(distinct student_id) as metric_denom ' +
							'into Launchboard4Top6.dbo.StudentWageWorkingCube ' +
							'from Launchboard4Top6.dbo.' + @source_str + ' ' +
							'where ' + @metric_name + ' is not null and ' + 'academic_year = ' + cast(@i as nvarchar) + ' and ' + @where_str + ' ' +
							'group by macroregion_name, microregion_name, cc_ipeds_code, cte_program, sector_name, program_code' + @metric_subgroup_set + ', ' +
							@metric_name + ' with cube;'

						--print @metric_id_str;
						--print current_timestamp;
						--print @SQLStatement1;
						exec sp_executesql @SQLStatement1;

						set @SQLStatement2 = 

						'insert into Launchboard4Top6.dbo.BuildStudentWageTableDisaggCube ' +
						'select distinct metric_id, academic_year, ' + 
						'macroregion_name, microregion_name, cc_ipeds_code, ' +
						'cte_program, sector_name, program_code, ' +
						'disagg, subgroup, ' +
						'metric_value, max(metric_denom) as metric_denom ' +
						'from Launchboard4Top6.dbo.StudentWageWorkingCube ' +
						'group by metric_id, academic_year, macroregion_name, microregion_name, cc_ipeds_code, cte_program, sector_name, program_code, disagg, subgroup, metric_value;'
						;

						--print @SQLStatement2;
						exec sp_executesql @SQLStatement2;

						set @k = @k + 1;

					end

				set @k = 1;
				set @j = @j + 1;

			end

		set @j = 1;
		set @i = @i + 1

	end
	set @i = 1;
;


-------------------

set @SQLStatement1 = 

	'if object_id(' + quotename(@equity_table, '''') + ') is not null 
		drop table ' + @equity_table + ';'

--print @SQLStatement1;
exec sp_executesql @SQLStatement1;

set @SQLStatement1 = 

	'select *
	into ' + @equity_table + ' 
	from Launchboard4Top6.dbo.BuildStudentWageTableDisaggCube
	;

	delete from ' + @equity_table + ' 
	where academic_year is null
	;

	update ' + @equity_table + ' 
	set macroregion_name = ' + quotename('All', '''') + ' 
	where macroregion_name is null
	;

	update ' + @equity_table + ' 
	set microregion_name = ' + quotename('All', '''') + ' 
	where microregion_name is null
	;

	update ' + @equity_table + ' 
	set cc_ipeds_code = ' + quotename('All', '''') + ' 
	where cc_ipeds_code is null
	;

	alter table ' + @equity_table + ' 
	alter column cte_program varchar(4)
	;

	update ' + @equity_table + ' 
	set cte_program = ' + quotename('All', '''') + ' 
	where cte_program is null
	;

	update ' + @equity_table + ' 
	set cte_program = ' + quotename('Yes', '''') + ' 
	where cte_program = ' + quotename('1', '''') + ' 
	;

	update ' + @equity_table + ' 
	set cte_program = ' + quotename('No', '''') + ' 
	where cte_program = ' + quotename('0', '''') + ' 
	;

	update ' + @equity_table + ' 
	set sector_name = ' + quotename('All', '''') + ' 
	where sector_name is null
	;

	update ' + @equity_table + ' 
	set program_code = ' + quotename('All', '''') + ' 
	where program_code is null
	;

	update ' + @equity_table + ' 
	set disagg = ' + quotename('All', '''') + ' 
	where disagg is null
	;

	alter table ' + @equity_table + ' 
	alter column subgroup varchar(100)
	;

	update ' + @equity_table + ' 
	set subgroup = ' + quotename('All', '''') + ' 
	where subgroup is null
	;

	update ' + @equity_table + ' 
	set subgroup = ' + quotename('Yes', '''') + ' 
	where subgroup = ' + quotename('1', '''') + ' 
	;

	update ' + @equity_table + ' 
	set subgroup = ' + quotename('No', '''') + ' 
	where subgroup = ' + quotename('0', '''') + ' 
	;'



--print @SQLStatement1;
exec sp_executesql @SQLStatement1;

set @SQLStatement2 = 

	'create nonclustered index WageTableIndex on ' +
	@equity_table + ' (metric_id, academic_year, macroregion_name, microregion_name, cc_ipeds_code, cte_program, sector_name, program_code, disagg, subgroup)
	include (metric_value, metric_denom)'
;

--print @SQLStatement2;
exec sp_executesql @SQLStatement2;


-- create regional medians

-- statewide first

if object_id('Launchboard4Top6.dbo.MedianCalcListSetup') is not null
	drop table Launchboard4Top6.dbo.MedianCalcListSetup;

if object_id('Launchboard4Top6.dbo.MedianCalcList') is not null
	drop table Launchboard4Top6.dbo.MedianCalcList;

set @SQLStatement1 = 

	'select distinct 
		metric_id, academic_year, cte_program, sector_name, program_code, disagg, subgroup
	into Launchboard4Top6.dbo.MedianCalcListSetup
	from ' + @equity_table + ' 
	where macroregion_name <> ' + quotename('All', '''') + ' 
		and microregion_name <> ' + quotename('All', '''') + '
		and cc_ipeds_code <> ' + quotename('All', '''') + '
	;
	

	select dense_rank()
		over(order by metric_id, academic_year, cte_program, sector_name, program_code, disagg, subgroup) as RowNum,
		*
	into Launchboard4Top6.dbo.MedianCalcList
	from Launchboard4Top6.dbo.MedianCalcListSetup
	;'
;

--print @SQLStatement1;
exec sp_executesql @SQLStatement1;

set @SQLStatement2 = 

	'create nonclustered index WageTableIndex on ' +
	'Launchboard4Top6.dbo.MedianCalcList (metric_id);'
;

--print @SQLStatement2;
exec sp_executesql @SQLStatement2;

declare @Calcs int =
	(select count(*) from Launchboard4Top6.dbo.MedianCalcList)
;

if object_id('Launchboard4Top6.dbo.StatewideWageCalcs') is not null
	drop table Launchboard4Top6.dbo.StatewideWageCalcs;

create table Launchboard4Top6.dbo.StatewideWageCalcs (
	metric_id varchar(6),
	academic_year varchar(4),
	cte_program	varchar(4),
	sector_name	varchar(100),
	program_code varchar(6),
	disagg varchar(50),
	subgroup varchar(100),
	metric_value float,
	median_value float,
	median_denom int
);

declare @metric_id varchar(6);
declare @academic_year varchar(4);
declare @macroregion_name varchar(100);
declare @microregion_name varchar(100);
declare @cte_program varchar(4);
declare @sector_name varchar(100);
declare @program_code varchar(6);
--declare @disagg varchar(100);
declare @subgroup varchar(100);

set @i = 1;

set nocount on;

while @i <= @Calcs
	begin

		set @metric_id = (select metric_id from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @academic_year = (select academic_year from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @cte_program = (select cte_program from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @sector_name = (select sector_name from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @program_code = (select program_code from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @disagg = (select disagg from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @subgroup = (select subgroup from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);

		set @SQLStatement1 = '
					
			insert into Launchboard4Top6.dbo.StatewideWageCalcs
			select metric_id, academic_year,
				cte_program, sector_name, program_code, disagg, subgroup, metric_value,
				percentile_cont(.5) within group (order by metric_value) over() as median_value,
				count(metric_value) as median_denom
			from ' + @equity_table + ' 
			where metric_id = ' + quotename(@metric_id, '''') + ' 
				and academic_year = ' + quotename(@academic_year, '''') + ' 
				and macroregion_name <> ' + quotename('All', '''') + '
				and microregion_name <> ' + quotename('All', '''') + '
				and cc_ipeds_code <> ' + quotename('All', '''') + '
				and cte_program = ' + quotename(@cte_program, '''') + ' 
				and sector_name = ' + quotename(@sector_name, '''') + '
				and program_code = ' + quotename(@program_code, '''') + '
				and disagg = ' + quotename(@disagg, '''') + ' 
				and subgroup = ' + quotename(@subgroup, '''') + '
			group by metric_id, academic_year, macroregion_name,
				cte_program, sector_name, program_code, disagg, subgroup, metric_value
			;'
		;

		--print @SQLStatement1;
		exec sp_executesql @SQLStatement1;

		set @i = @i + 1;
	end

set nocount off;

set @SQLStatement1 = 

	'if object_id(' + quotename(@equity_table_statewide, '''') + ') is not null 
		drop table ' + @equity_table_statewide + ';'
;

-- print @SQLStatement1;
exec sp_executesql @SQLStatement1;

set @SQLStatement2 = 

	'select metric_id, academic_year, ' +
		quotename('All', '''') + ' as macroregion_name, ' + quotename('All', '''') + ' as microregion_name, ' + quotename('All', '''') + ' as cc_ipeds_code,
		cte_program, sector_name, program_code,
		disagg, subgroup,
		max(median_value) as metric_value,
		count(median_value) as metric_denom
	into ' + @equity_table_statewide + ' 
	from Launchboard4Top6.dbo.StatewideWageCalcs
	group by metric_id, academic_year,
		cte_program, sector_name, program_code,
		disagg, subgroup
	;'
;

-- print @SQLStatement2;
exec sp_executesql @SQLStatement2;


-- macroregion second

if object_id('Launchboard4Top6.dbo.MedianCalcListSetup') is not null
	drop table Launchboard4Top6.dbo.MedianCalcListSetup;

if object_id('Launchboard4Top6.dbo.MedianCalcList') is not null
	drop table Launchboard4Top6.dbo.MedianCalcList;

set @SQLStatement1 = 

	'select distinct 
		metric_id, academic_year, macroregion_name, cte_program, sector_name, program_code, disagg, subgroup
	into Launchboard4Top6.dbo.MedianCalcListSetup
	from ' + @equity_table + ' 
	where macroregion_name <> ' + quotename('All', '''') + ' 
		and microregion_name <> ' + quotename('All', '''') + '
		and cc_ipeds_code <> ' + quotename('All', '''') + '
	;
	

	select dense_rank()
		over(order by metric_id, academic_year, macroregion_name, cte_program, sector_name, program_code, disagg, subgroup) as RowNum,
		*
	into Launchboard4Top6.dbo.MedianCalcList
	from Launchboard4Top6.dbo.MedianCalcListSetup
	;'
;

--print @SQLStatement1;
exec sp_executesql @SQLStatement1;

set @SQLStatement2 = 

	'create nonclustered index WageTableIndex on ' +
	'Launchboard4Top6.dbo.MedianCalcList (metric_id);'
;

--print @SQLStatement2;
exec sp_executesql @SQLStatement2;


set @Calcs =
	(select count(*) from Launchboard4Top6.dbo.MedianCalcList)
;

if object_id('Launchboard4Top6.dbo.MacromedianWageCalcs') is not null
	drop table Launchboard4Top6.dbo.MacromedianWageCalcs;

create table Launchboard4Top6.dbo.MacromedianWageCalcs (
	metric_id varchar(6),
	academic_year varchar(4),
	macroregion_name varchar(50),
	cte_program	varchar(4),
	sector_name	varchar(100),
	program_code varchar(6),
	disagg varchar(50),
	subgroup varchar(50),
	metric_value float,
	median_value float,
	median_denom int
);

set @i = 1;

set nocount on;

while @i <= @Calcs
	begin

		set @metric_id = (select metric_id from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @academic_year = (select academic_year from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @macroregion_name = (select macroregion_name from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @cte_program = (select cte_program from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @sector_name = (select sector_name from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @program_code = (select program_code from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @disagg = (select disagg from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @subgroup = (select subgroup from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);

		set @SQLStatement1 = '
					
			insert into Launchboard4Top6.dbo.MacromedianWageCalcs
			select metric_id, academic_year, macroregion_name,
				cte_program, sector_name, program_code, disagg, subgroup, metric_value,
				percentile_cont(.5) within group (order by metric_value) over() as median_value,
				count(metric_value) as median_denom
			from ' + @equity_table + ' 
			where metric_id = ' + quotename(@metric_id, '''') + ' 
				and academic_year = ' + quotename(@academic_year, '''') + ' 
				and macroregion_name = ' + quotename(@macroregion_name, '''') + ' 
				and microregion_name <> ' + quotename('All', '''') + '
				and cc_ipeds_code <> ' + quotename('All', '''') + '
				and cte_program = ' + quotename(@cte_program, '''') + ' 
				and sector_name = ' + quotename(@sector_name, '''') + '
				and program_code = ' + quotename(@program_code, '''') + '
				and disagg = ' + quotename(@disagg, '''') + ' 
				and subgroup = ' + quotename(@subgroup, '''') + '
			group by metric_id, academic_year, macroregion_name,
				cte_program, sector_name, program_code, disagg, subgroup, metric_value
			;'
		;

		--print @SQLStatement1;
		exec sp_executesql @SQLStatement1;

		set @i = @i + 1;
	end

set nocount off;

set @SQLStatement1 = 

	'if object_id(' + quotename(@equity_table_macroregion, '''') + ') is not null 
		drop table ' + @equity_table_macroregion + ';'
;

-- print @SQLStatement1;
exec sp_executesql @SQLStatement1;

set @SQLStatement2 = 

	'select metric_id, academic_year,
		macroregion_name, ' + quotename('All', '''') + ' as microregion_name, ' + quotename('All', '''') + ' as cc_ipeds_code,
		cte_program, sector_name, program_code,
		disagg, subgroup,
		max(median_value) as metric_value,
		count(median_value) as metric_denom
	into ' + @equity_table_macroregion + ' 
	from Launchboard4Top6.dbo.MacromedianWageCalcs
	group by metric_id, academic_year, macroregion_name,
		cte_program, sector_name, program_code,
		disagg, subgroup
	;'
;

-- print @SQLStatement2;
exec sp_executesql @SQLStatement2;




-- calc microregion wages

if object_id('Launchboard4Top6.dbo.MedianCalcListSetup') is not null
	drop table Launchboard4Top6.dbo.MedianCalcListSetup;

if object_id('Launchboard4Top6.dbo.MedianCalcList') is not null
	drop table Launchboard4Top6.dbo.MedianCalcList;

set @SQLStatement1 = 

	'select distinct 
		metric_id, academic_year, macroregion_name, microregion_name, cte_program, sector_name, program_code, disagg, subgroup
	into Launchboard4Top6.dbo.MedianCalcListSetup
	from ' + @equity_table + ' 
	where macroregion_name <> ' + quotename('All', '''') + ' 
		and microregion_name <> ' + quotename('All', '''') + '
		and cc_ipeds_code <> ' + quotename('All', '''') + '
	;
	

	select dense_rank()
		over(order by metric_id, academic_year, macroregion_name, microregion_name, cte_program, sector_name, program_code, disagg, subgroup) as RowNum,
		*
	into Launchboard4Top6.dbo.MedianCalcList
	from Launchboard4Top6.dbo.MedianCalcListSetup
	;'
;

-- print @SQLStatement1;
exec sp_executesql @SQLStatement1;

set @SQLStatement2 = 

	'create nonclustered index WageTableIndex on ' +
	'Launchboard4Top6.dbo.MedianCalcList (metric_id);'
;

--print @SQLStatement2;
exec sp_executesql @SQLStatement2;

set @Calcs =
	(select count(*) from Launchboard4Top6.dbo.MedianCalcList)
;

if object_id('Launchboard4Top6.dbo.MicromedianWageCalcs') is not null
	drop table Launchboard4Top6.dbo.MicromedianWageCalcs;

create table Launchboard4Top6.dbo.MicromedianWageCalcs (
	metric_id varchar(6),
	academic_year varchar(4),
	macroregion_name varchar(50),
	microregion_name varchar(50),
	cte_program	varchar(4),
	sector_name	varchar(100),
	program_code varchar(6),
	disagg varchar(50),
	subgroup varchar(50),
	metric_value float,
	median_value float,
	median_denom int
);

set @i = 1;

set nocount on;

while @i <= @Calcs
	begin

		set @metric_id = (select metric_id from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @academic_year = (select academic_year from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @macroregion_name = (select macroregion_name from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @microregion_name = (select microregion_name from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @cte_program = (select cte_program from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @sector_name = (select sector_name from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @program_code = (select program_code from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @disagg = (select disagg from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);
		set @subgroup = (select subgroup from Launchboard4Top6.dbo.MedianCalcList where @i = RowNum);

		set @SQLStatement1 = '
					
			insert into Launchboard4Top6.dbo.MicromedianWageCalcs
			select metric_id, academic_year, macroregion_name, microregion_name,
				cte_program, sector_name, program_code, disagg, subgroup, metric_value,
				percentile_cont(.5) within group (order by metric_value) over() as median_value,
				count(metric_value) as median_denom
			from ' + @equity_table + ' 
			where metric_id = ' + quotename(@metric_id, '''') + ' 
				and academic_year = ' + quotename(@academic_year, '''') + ' 
				and macroregion_name = ' + quotename(@macroregion_name, '''') + ' 
				and microregion_name = ' + quotename(@microregion_name, '''') + ' 
				and cc_ipeds_code <> ' + quotename('All', '''') + '
				and cte_program = ' + quotename(@cte_program, '''') + ' 
				and sector_name = ' + quotename(@sector_name, '''') + '
				and program_code = ' + quotename(@program_code, '''') + ' 
				and disagg = ' + quotename(@disagg, '''') + ' 
				and subgroup = ' + quotename(@subgroup, '''') + ' 
			group by metric_id, academic_year, macroregion_name, microregion_name,
				cte_program, sector_name, program_code, disagg, subgroup, metric_value
			;'
		;

		-- print @SQLStatement1;
		exec sp_executesql @SQLStatement1;

		set @i = @i + 1;
	end

set nocount off;

set @SQLStatement1 = 

	'if object_id(' + quotename(@equity_table_microregion, '''') + ') is not null 
		drop table ' + @equity_table_microregion + ';'

;

-- print @SQLStatement1;
exec sp_executesql @SQLStatement1;


set @SQLStatement2 = 

	'select metric_id, academic_year,
		macroregion_name, microregion_name, ' + quotename('All', '''') + ' as cc_ipeds_code,
		cte_program, sector_name, program_code,
		disagg, subgroup,
		max(median_value) as metric_value,
		count(median_value) as metric_denom
	into ' + @equity_table_microregion + ' 
	from Launchboard4Top6.dbo.MicromedianWageCalcs
	group by metric_id, academic_year, macroregion_name, microregion_name,
		cte_program, sector_name, program_code,
		disagg, subgroup
	;'
;

-- print @SQLStatement2;
exec sp_executesql @SQLStatement2;
----


set @SQLStatement1 = 

	'if object_id(' + quotename(@equity_table_final, '''') + ') is not null 
		drop table ' + @equity_table_final + ';'

;

-- print @SQLStatement1;
exec sp_executesql @SQLStatement1;


set @SQLStatement2 = 

	'select *
	into ' + @equity_table_final + ' 
	from (

	select *
	from ' + @equity_table + ' 
	where cc_ipeds_code <> ' + quotename('All', '''') + '

	union all

	select *
	from ' + @equity_table_macroregion + ' 

	union all

	select *
	from ' + @equity_table_microregion + '

	) as #Combined;'
;

-- print @SQLStatement2;
exec sp_executesql @SQLStatement2;