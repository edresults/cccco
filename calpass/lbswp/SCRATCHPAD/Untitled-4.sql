			SELECT
				CollegeCode,
				StudentId,
				YearCode,
				IsSkillsBuilder = 
					case
						when
							sum(NonIntroUnits) >= 0.5
							and
							IsAwardAny = 0
							and
							IsVocationalSuccessAll = 1
							and
							IsCCEnrolledYearCodeNext = 0
							and
							IsUNEnrolledYearCodeNext = 0 then 1
						else 0
					end,
				IsTransfer = 
					case
						when
							(
								sum(NonIntroUnits) > 0
								or
								IsAwardAny = 1
							)
							and
							IsCCEnrolledYearCodeNext = 0
							and
							IsUNEnrolledYearCodeNextFirst = 1 then 1
						else 0
					end,
				IsExiter = 
					case
						when
							(
								sum(NonIntroUnits) > 0
								or
								IsAwardAny = 1
							)
							and
							IsCCEnrolledYearCodeNext = 0
							and
							IsUNEnrolledYearCodeNext = 0 then 1
						else 0
					end,
				IsCompleter = max(convert(tinyint, IsAwardProgramRigorous))
			FROM
				lbswp.GoldenRecord
			WHERE
				YearCode = '150'
				and ProgramCode = '050200'
				and CollegeCode = '641'
			GROUP BY
				CollegeCode,
				StudentId,
				YearCode,
				IsAwardAny,
				IsVocationalSuccessAll,
				IsCCEnrolledYearCodeNext,
				IsUNEnrolledYearCodeNext,
				IsUNEnrolledYearCodeNextFirst