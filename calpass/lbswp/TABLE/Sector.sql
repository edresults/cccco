USE calpass;

GO

IF (object_id('lbswp.Sector') is not null)
	BEGIN
		DROP TABLE lbswp.Sector;
	END;

GO

CREATE TABLE
	lbswp.Sector
	(
		SectorId  smallint identity(0,1) not null,
		ContentId smallint               not null,
		Label     varchar(65)            not null
	)

GO

ALTER TABLE
	lbswp.Sector
ADD CONSTRAINT
	PK_Sector
PRIMARY KEY CLUSTERED
	(
		SectorId
	);

ALTER TABLE
	lbswp.Sector
ADD CONSTRAINT
	UQ_Sector__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	lbswp.Sector
ADD CONSTRAINT
	FK_Sector__ContentId
FOREIGN KEY
	(
		ContentId
	)
REFERENCES
	lbswp.Content
	(
		ContentId
	);

CREATE NONCLUSTERED INDEX
	IX_Sector__ContentId
ON
	lbswp.Sector
	(
		ContentId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	lbswp.Sector
	(
		ContentId,
		Label
	)
VALUES
	(2,'Advanced Manufacturing'),
	(2,'Advanced Transportation and Logistics'),
	(2,'Agriculture, Water and Environmental Technologies'),
	(2,'Business and Entrepreneurship'),
	(2,'Education and Human Development'),
	(2,'Energy, Construction and Utilities'),
	(2,'Global Trade'),
	(2,'Health'),
	(2,'Information and Communication Technologies (ICT) - Digital Media'),
	(2,'Life Sciences - Biotechnology'),
	(2,'Public Safety'),
	(2,'Retail, Hospitality and Tourism'),
	(2,'Unassigned');