USE calpass;

GO

IF (object_id('lbswp.Occupation') is not null)
	BEGIN
		DROP TABLE lbswp.Occupation
	END;

GO

CREATE TABLE
	lbswp.Occupation
	(
		OccupationId   int identity(0,1) not null,
		OccupationCode char(1)           not null,
		Title          varchar(25)       not null,
		Description    varchar(8000)     not null,
		IsIntroductory bit               not null
	)

GO

ALTER TABLE
	lbswp.Occupation
ADD CONSTRAINT
	PK_Occupation
PRIMARY KEY CLUSTERED
	(
		OccupationId
	);

ALTER TABLE
	lbswp.Occupation
ADD CONSTRAINT
	UQ_Occupation__OccupationCode
UNIQUE
	(
		OccupationCode
	);

GO

INSERT
	lbswp.Occupation
	(
		OccupationCode,
		Title,
		IsIntroductory,
		Description
	)
VALUES
	('A','Apprenticeship',       0,'The course is designed for an apprentice and must have the approval of the State of California, Department of Industrial Relations, Division of Apprenticeship Standards. Some examples of apprenticeship courses are: Carpentry, Plumbing and Electrician.'),
	('B','Advanced Occupational',0,'Courses are those taken by students in the advanced stages of their occupational programs. A “B” course is offered in one specific occupational area only and clearly labels its taker as a major in this area. The course may be a “capstone course” that is taken as the last requirement for a career technical education program. Priority letter “B” should be assigned sparingly; in most cases no more than two courses in any one program should be labeled “B”. Each “B” level course must have a “C” level prerequisite in the same program area. Some examples of “B” level courses are: Dental Pathology, Advanced Radiology Technology, Fire Hydraulics, Livestock and Dairy Selections, Real Estate Finance, Cost Accounting.'),
	('C','Clearly Occupational', 0,'Courses will generally be taken by students in the middle stages of their rograms and should be of difficulty level sufficient to detract “drop-ins”. A “C” level course may be offered in several occupational programs within a broad area such as business or agriculture. The “C” priority, however, should also be used for courses within a specific program area when the criteria for “B” classification are not met. A “C” level course should provide the student with entry-level job skills. Some examples of “C” level courses are: Soils, Principles of Advertising, Air Transportation, Clinical Techniques, Principles of Patient Care, Food and Nutrition, Sanitation/Safety, Small Business Management, Advanced Keyboarding, Technical Engineering.'),
	('D','Possibly Occupational',1,'“D” courses are those taken by students in the beginning stages of their occupational programs. The “D” priority can also be used for service (or survey) courses for other occupational Programs. Some examples of “D” level courses are: Technical Mathematics, Graphic Communications, Elementary Mechanical Principles, Fundamentals of Electronics, Keyboarding (Beginning or Intermediate), Accounting (Beginning).'),
	('E','Non-Occupational',     1,'These courses are non-occupational.');