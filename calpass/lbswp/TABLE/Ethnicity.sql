USE calpass;

GO

IF (object_id('lbswp.Ethnicity') is not null)
	BEGIN
		DROP TABLE lbswp.Ethnicity;
	END;

GO

CREATE TABLE
	lbswp.Ethnicity
	(
		EthnicityId   tinyint identity(0,1) not null,
		EthnicityCode char(2)               not null,
		Label         varchar(35)           not null,
		ParseStart    tinyint               not null,
		ParseLength   bit                   not null
	)

GO

ALTER TABLE
	lbswp.Ethnicity
ADD CONSTRAINT
	PK_Ethnicity
PRIMARY KEY CLUSTERED
	(
		EthnicityId
	);

ALTER TABLE
	lbswp.Ethnicity
ADD CONSTRAINT
	UQ_Ethnicity__EthnicityCode
UNIQUE
	(
		EthnicityCode
	);

ALTER TABLE
	lbswp.Ethnicity
ADD CONSTRAINT
	UQ_Ethnicity__ParseStart
UNIQUE
	(
		ParseStart
	);

ALTER TABLE
	lbswp.Ethnicity
ADD CONSTRAINT
	CK_Ethnicity__ParseLength
CHECK
	(
		ParseLength = 1
	);

GO

INSERT
	lbswp.Ethnicity
	(
		ParseStart,
		ParseLength,
		EthnicityCode,
		Label
	)
VALUES
	(1, 1,'HL','Hispanic, Latino,'),
	(2, 1,'HM','Mexican, Mexican-American, Chicano'),
	(3, 1,'HC','Central American'),
	(4, 1,'HS','South American'),
	(5, 1,'HO','Hispanic Other'),
	(6, 1,'AI','Asian Indian'),
	(7, 1,'AN','Asian Chinese'),
	(8, 1,'AJ','Asian Japanese'),
	(9, 1,'AK','Asian Korean'),
	(10,1,'AL','Asian Laotian'),
	(11,1,'AC','Asian Cambodian'),
	(12,1,'AV','Asian Vietnamese'),
	(13,1,'AF','Filipino'),
	(14,1,'AO','Asian Other'),
	(15,1,'BA','Black or African American'),
	(16,1,'NA','American Indian / Alaskan Native'),
	(17,1,'PG','Pacific Islander Guamanian'),
	(18,1,'PH','Pacific Islander Hawaiian'),
	(19,1,'PS','Pacific Islander Samoan'),
	(20,1,'PO','Pacific Islander Other'),
	(21,1,'WH','White');