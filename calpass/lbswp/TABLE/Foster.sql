USE calpass;

GO

IF (object_id('lbswp.Foster') is not null)
	BEGIN
		DROP TABLE lbswp.Foster;
	END;

GO

CREATE TABLE
	lbswp.Foster
	(
		FosterId   tinyint identity(0,1) not null,
		FosterCode char(1)               not null,
		IsFoster   bit                   not null,
		Label      varchar(45)           not null
	)

GO

ALTER TABLE
	lbswp.Foster
ADD CONSTRAINT
	PK_Foster
PRIMARY KEY CLUSTERED
	(
		FosterId
	);

ALTER TABLE
	lbswp.Foster
ADD CONSTRAINT
	UQ_Foster__FosterCode
UNIQUE
	(
		FosterCode
	);

GO

INSERT
	lbswp.Foster
	(
		FosterCode,
		IsFoster,
		Label
	)
VALUES
	('0',0,'No'),
	('1',1,'Yes'),
	('X',0,'Unknown/Unreported');