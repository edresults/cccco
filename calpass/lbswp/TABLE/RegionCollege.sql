USE calpass;

GO

IF (object_id('lbswp.RegionCollege') is not null)
	BEGIN
		DROP TABLE lbswp.RegionCollege;
	END;

GO

CREATE TABLE
	lbswp.RegionCollege
	(
		RegionCollegeId tinyint identity(0,1) not null,
		RegionId        tinyint               not null,
		RegionMicroId   tinyint               not null,
		CollegeCode     char(3)               not null,
		Label           varchar(40)           not null
	);

GO

ALTER TABLE
	lbswp.RegionCollege
ADD CONSTRAINT
	PK_RegionCollege
PRIMARY KEY CLUSTERED
	(
		RegionCollegeId
	);

ALTER TABLE
	lbswp.RegionCollege
ADD CONSTRAINT
	UQ_RegionCollege__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	lbswp.RegionCollege
ADD CONSTRAINT
	UQ_RegionCollege__CollegeCode
UNIQUE
	(
		CollegeCode
	);

ALTER TABLE
	lbswp.RegionCollege
ADD CONSTRAINT
	FK_RegionCollege__RegionMicroId
FOREIGN KEY
	(
		RegionMicroId
	)
REFERENCES
	lbswp.RegionMicro
	(
		RegionMicroId
	);

ALTER TABLE
	lbswp.RegionCollege
ADD CONSTRAINT
	FK_RegionCollege__RegionId
FOREIGN KEY
	(
		RegionId
	)
REFERENCES
	lbswp.Region
	(
		RegionId
	);

CREATE NONCLUSTERED INDEX
	IX_RegionCollege__RegionId
ON
	lbswp.RegionCollege
	(
		RegionId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	lbswp.RegionCollege
	(
		RegionMicroId,
		RegionId,
		CollegeCode,
		Label
	)
VALUES
	(0,3,'345','Berkeley City College'),
	(0,3,'482','Chabot College'),
	(0,3,'341','College of Alameda'),
	(0,3,'311','Contra Costa College'),
	(0,3,'312','Diablo Valley College'),
	(0,3,'343','Laney College'),
	(0,3,'481','Las Positas College'),
	(0,3,'313','Los Medanos College'),
	(0,3,'344','Merritt College'),
	(0,3,'431','Ohlone College'),
	(1,3,'334','College of Marin'),
	(1,3,'241','Napa Valley College'),
	(1,3,'261','Santa Rosa Junior College'),
	(1,3,'281','Solano Community College'),
	(1,3,'335','Marin Continuing'),
	(2,3,'421','De Anza College'),
	(2,3,'471','Evergreen Valley College'),
	(2,3,'422','Foothill College'),
	(2,3,'441','Gavilan College'),
	(2,3,'492','Mission College'),
	(2,3,'472','San Jose City College'),
	(2,3,'493','West Valley College'),
	(3,3,'411','Cabrillo College'),
	(3,3,'451','Hartnell College'),
	(3,3,'461','Monterey Peninsula College'),
	(4,3,'371','Cañada College'),
	(4,3,'361','City College of San Francisco'),
	(4,3,'372','College of San Mateo'),
	(4,3,'363','San Francisco Community College Centers'),
	(4,3,'373','Skyline College'),
	(5,3,'591','Columbia College'),
	(5,3,'531','Merced College'),
	(5,3,'592','Modesto Junior College'),
	(5,3,'551','San Joaquin Delta College'),
	(6,3,'521','Bakersfield College'),
	(6,3,'522','Cerro Coso Community College'),
	(6,3,'576','Clovis College'),
	(6,3,'561','College of the Sequoias'),
	(6,3,'571','Fresno City College'),
	(6,3,'523','Porterville College'),
	(6,3,'572','Reedley College'),
	(6,3,'691','Taft College'),
	(6,3,'581','West Hills Coalinga'),
	(6,3,'582','West Hills Lemoore'),
	(7,3,'911','Barstow College'),
	(7,3,'921','Chaffey College'),
	(7,3,'931','College of the Desert'),
	(7,3,'971','Copper Mountain Community College'),
	(7,3,'981','Crafton Hills College'),
	(7,3,'962','Moreno Valley College'),
	(7,3,'941','Mt. San Jacinto College'),
	(7,3,'963','Norco College'),
	(7,3,'951','Palo Verde College'),
	(7,3,'961','Riverside City College'),
	(7,3,'982','San Bernardino Valley College'),
	(7,3,'991','Victor Valley Community College'),
	(8,3,'811','Cerritos College'),
	(8,3,'821','Citrus College'),
	(8,3,'748','East Los Angeles College'),
	(8,3,'721','El Camino College'),
	(8,3,'711','El Camino College Compton Center'),
	(8,3,'731','Glendale College'),
	(8,3,'841','Long Beach City College'),
	(8,3,'741','Los Angeles City College'),
	(8,3,'742','Los Angeles Harbor College'),
	(8,3,'743','Los Angeles Mission College'),
	(8,3,'744','Los Angeles Pierce College'),
	(8,3,'745','Los Angeles Southwest College'),
	(8,3,'746','Los Angeles Trade-Technical College'),
	(8,3,'747','Los Angeles Valley College'),
	(8,3,'851','Mt. San Antonio College'),
	(8,3,'771','Pasadena City College'),
	(8,3,'881','Rio Hondo College'),
	(8,3,'781','Santa Monica College'),
	(8,3,'749','West Los Angeles College'),
	(8,3,'74A','Los Angeles Mission College ITV'),
	(9,3,'831','Coastline Community College'),
	(9,3,'861','Cypress College'),
	(9,3,'862','Fullerton College'),
	(9,3,'832','Golden West College'),
	(9,3,'892','Irvine Valley College'),
	(9,3,'863','North Orange Adult Division'),
	(9,3,'833','Orange Coast College'),
	(9,3,'891','Saddleback College'),
	(9,3,'871','Santa Ana College'),
	(9,3,'873','Santiago Canyon College'),
	(9,3,'872','Rancho Santiago CED'),
	(10,3,'231','American River College'),
	(10,3,'232','Cosumnes River College'),
	(10,3,'234','Folsom Lake College'),
	(10,3,'221','Lake Tahoe Community College'),
	(10,3,'233','Sacramento City College'),
	(10,3,'271','Sierra College'),
	(10,3,'292','Woodland College'),
	(10,3,'291','Yuba College'),
	(11,3,'111','Butte College'),
	(11,3,'181','College of the Siskiyous'),
	(11,3,'121','Feather River College'),
	(11,3,'131','Lassen College'),
	(11,3,'171','Shasta College'),
	(12,3,'161','College of the Redwoods'),
	(12,3,'141','Mendocino College'),
	(13,3,'021','Cuyamaca College'),
	(13,3,'022','Grossmont College'),
	(13,3,'031','Imperial Valley College'),
	(13,3,'051','MiraCosta College'),
	(13,3,'061','Palomar College'),
	(13,3,'071','San Diego City College'),
	(13,3,'076','San Diego Continuing Education'),
	(13,3,'072','San Diego Mesa College'),
	(13,3,'073','San Diego Miramar College'),
	(13,3,'091','Southwestern College'),
	(14,3,'611','Allan Hancock College'),
	(14,3,'621','Antelope Valley College'),
	(14,3,'661','College of the Canyons'),
	(14,3,'641','Cuesta College'),
	(14,3,'681','Moorpark College'),
	(14,3,'682','Oxnard College'),
	(14,3,'651','Santa Barbara City College'),
	(14,3,'683','Ventura College'),
	(14,3,'652','Santa Barbara Continuing');