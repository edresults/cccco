USE calpass;

GO

IF (object_id('lbswp.Fiscal') is not null)
	BEGIN
		DROP TABLE lbswp.Fiscal;
	END;

GO

CREATE TABLE
	lbswp.Fiscal
	(
		FiscalId        int identity(0,1) not null,
		YearCode        char(3)           not null,
		FiscalYear      smallint          not null,
		FiscalQuarter   tinyint           not null,
		IsQuarterSecond bit               not null,
		IsQuarterFourth bit               not null
	)

GO

ALTER TABLE
	lbswp.Fiscal
ADD CONSTRAINT
	PK_Fiscal
PRIMARY KEY CLUSTERED
	(
		FiscalId
	);

GO

ALTER TABLE
	lbswp.Fiscal
ADD CONSTRAINT
	FK_Fiscal__YearCode
FOREIGN KEY
	(
		YearCode
	)
REFERENCES
	lbswp.Year
	(
		YearCode
	);

CREATE NONCLUSTERED INDEX
	IX_Fiscal__YearCode
ON
	lbswp.Fiscal
	(
		YearCode
	);

ALTER TABLE
	lbswp.Fiscal
ADD CONSTRAINT
	UQ_Fiscal__FiscalYear__FiscalQuarter
UNIQUE
	(
		FiscalYear,
		FiscalQuarter
	);

GO

INSERT INTO
	lbswp.Fiscal
	(
		YearCode,
		FiscalYear,
		FiscalQuarter,
		IsQuarterSecond,
		IsQuarterFourth
	)
VALUES
	('120',2011,3,0,0),
	('120',2011,4,1,0),
	('120',2012,1,0,0),
	('120',2012,2,0,1),
	('130',2012,3,0,0),
	('130',2012,4,1,0),
	('130',2013,1,0,0),
	('130',2013,2,0,1),
	('140',2013,3,0,0),
	('140',2013,4,1,0),
	('140',2014,1,0,0),
	('140',2014,2,0,1),
	('150',2014,3,0,0),
	('150',2014,4,1,0),
	('150',2015,1,0,0),
	('150',2015,2,0,1),
	('160',2015,3,0,0),
	('160',2015,4,1,0),
	('160',2016,1,0,0),
	('160',2016,2,0,1),
	('170',2016,3,0,0),
	('170',2016,4,1,0),
	('170',2017,1,0,0),
	('170',2017,2,0,1);