USE calpass;

GO

IF (object_id('lbswp.AidCategory') is not null)
	BEGIN
		DROP TABLE lbswp.AidCategory;
	END;

GO

CREATE TABLE
	lbswp.AidCategory
	(
		AidCategoryId    tinyint identity(0,1) not null,
		AidCategoryCode  varchar(40)           not null
	);

GO

ALTER TABLE
	lbswp.AidCategory
ADD CONSTRAINT
	PK_AidCategory
PRIMARY KEY CLUSTERED
	(
		AidCategoryId
	);

ALTER TABLE
	lbswp.AidCategory
ADD CONSTRAINT
	UQ_AidCategory__AidCategoryCode
UNIQUE
	(
		AidCategoryCode
	);

GO

INSERT
	lbswp.AidCategory
	(
		AidCategoryCode
	)
VALUES
	('Board of Governors Enrollment Fee Waiver'),
	('Coding Work Study'),
	('Grant'),
	('Loan'),
	('Scholarship');