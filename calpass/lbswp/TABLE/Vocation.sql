USE calpass;

GO

IF (object_id('lbswp.Vocation') is not null)
	BEGIN
		DROP TABLE lbswp.Vocation;
	END;

GO

CREATE TABLE
	lbswp.Vocation
	(
		VocationId smallint identity(0,1) not null,
		ContentId  smallint               not null,
		Label      varchar(15)            not null
	);

GO

ALTER TABLE
	lbswp.Vocation
ADD CONSTRAINT
	PK_Vocation
PRIMARY KEY CLUSTERED
	(
		VocationId
	);

ALTER TABLE
	lbswp.Vocation
ADD CONSTRAINT
	UQ_Vocation__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	lbswp.Vocation
ADD CONSTRAINT
	FK_Vocation__ContentId
FOREIGN KEY
	(
		ContentId
	)
REFERENCES
	lbswp.Content
	(
		ContentId
	);

CREATE NONCLUSTERED INDEX
	IX_Vocation__ContentId
ON
	lbswp.Vocation
	(
		ContentId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	lbswp.Vocation
	(
		ContentId,
		Label
	)
VALUES
	(1,'Vocational'),
	(1,'Non-Vocational');