IF (object_id('lbswp.Metric') is not null)
	BEGIN
		DROP TABLE lbswp.Metric;
	END;

GO

CREATE TABLE
	lbswp.Metric
	(
		MetricId    tinyint identity(0,1) not null,
		MetricCode  smallint              not null,
		Label       varchar(60)           not null,
		Description varchar(255)          not null
	);

GO

ALTER TABLE
	lbswp.Metric
ADD CONSTRAINT
	PK_Metric
PRIMARY KEY
	(
		MetricId
	);

ALTER TABLE
	lbswp.Metric
ADD CONSTRAINT
	UQ_Metric__MetricCode
UNIQUE
	(
		MetricCode
	);

GO

INSERT
	lbswp.Metric
	(
		MetricCode,
		Label,
		Description
	)
VALUES
	(101,'NUMBER OF COURSE ENROLLMENTS: ','The number of enrollments in courses'),
	(408,'NUMBER OF STUDENTS WHO GOT A DEGREE OR CERTIFICATE: ','Number of distinct students who earned a locally-issued certificate, Chancellor’s Office approved certificate, associate degree, and/or applied baccalaureate degree'),
	(410,'NUMBER OF STUDENTS WHO TRANSFERRED: ','Students who took non-introductory courses or completed a CCCCO award who subsequently enrolled for the first time in a four-year institution the following year'),
	(500,'EMPLOYED IN THE SECOND FISCAL QUARTER AFTER EXIT: ','Among all exiters, the percentage who were employed two quarters after exiting post-secondary education'),
	(501,'EMPLOYED IN THE FOURTH FISCAL QUARTER AFTER EXIT: ','Among exiting students, the percentage who were employed four quarters after exiting post-secondary education'),
	(502,'JOB CLOSELY RELATED TO FIELD OF STUDY: ','Among students who responded to the CTE Outcomes Survey, the percentage who reported they were employed in the same or similar field as their program of study'),
	(508,'MEDIAN EARNINGS IN THE SECOND FISCAL QUARTER AFTER EXIT: ','Among exiting students, the median second quarter earnings one year after the year in which they exited post-secondary education'),
	(512,'MEDIAN CHANGE IN EARNINGS: ','Among exiting students, the percentage change in earnings one year before and one year after exiting post-secondary education'),
	(513,'ATTAINED A LIVING WAGE: ','Among completers and skills-builders who exited, the proportion of students who attained a living wage');