USE calpass;

GO

IF (object_id('lbswp.GuardianEducation') is not null)
	BEGIN
		DROP TABLE lbswp.GuardianEducation;
	END;

GO

CREATE TABLE
	lbswp.GuardianEducation
	(
		GuardianEducationId   tinyint identity(0,1) not null,
		GuardianEducationCode char(1)               not null,
		Label                 varchar(50)           not null,
		IsFirstGeneration     bit                   not null
	)

GO

ALTER TABLE
	lbswp.GuardianEducation
ADD CONSTRAINT
	PK_GuardianEducation
PRIMARY KEY CLUSTERED
	(
		GuardianEducationId
	);

ALTER TABLE
	lbswp.GuardianEducation
ADD CONSTRAINT
	UQ_GuardianEducation__GuardianEducationCode
UNIQUE
	(
		GuardianEducationCode
	);

GO

INSERT
	lbswp.GuardianEducation
	(
		GuardianEducationCode,
		IsFirstGeneration,
		Label
	)
VALUES
	('1',1,'Grade 9 or less'),
	('2',1,'Grade 10, 11, or 12 but did not graduate'),
	('3',1,'High school graduate'),
	('4',0,'Some college but no degree'),
	('5',0,'AA/AS degree'),
	('6',0,'BA/BS degree'),
	('7',0,'Graduate or professional degree beyond a BA/BS'),
	('Y',0,'Not applicable, no first parent/guardian'),
	('X',0,'Unknown / Unreported');