USE calpass;

GO

IF (object_id('lbswp.College') is not null)
	BEGIN
		DROP TABLE lbswp.College;
	END;

GO

CREATE TABLE
	lbswp.College
	(
		CollegeId    tinyint      not null identity(1,1),
		CollegeCode  char(3)      not null,
		Name         varchar(255) not null,
		CONSTRAINT
			PK_CollegeId
		PRIMARY KEY CLUSTERED
			(
				CollegeId
			),
		INDEX
			IQ_College__CollegeCode
		UNIQUE
			(
				CollegeCode
			),
		INDEX
			IQ_College__Name
		UNIQUE
			(
				Name
			)
	);

GO

INSERT INTO
	lbswp.College
	(
		CollegeCode,
		Name
	)
VALUES
	('021','Cuyamaca'),
	('022','Grossmont'),
	('031','Imperial'),
	('051','MiraCosta'),
	('061','Palomar'),
	('071','San Diego City'),
	('072','San Diego Mesa'),
	('073','San Diego Miramar'),
	('076','San Diego Adult'),
	('091','Southwestern'),
	('111','Butte'),
	('121','Feather River'),
	('131','Lassen'),
	('141','Mendocino'),
	('161','Redwoods'),
	('171','Shasta'),
	('181','Siskiyous'),
	('221','Lake Tahoe'),
	('231','American River'),
	('232','Cosumnes River'),
	('233','Sacramento City'),
	('234','Folsom Lake'),
	('241','Napa'),
	('261','Santa Rosa'),
	('271','Sierra'),
	('281','Solano'),
	('291','Yuba'),
	('292','Woodland'),
	('311','Contra Costa'),
	('312','Diablo Valley'),
	('313','Los Medanos'),
	('334','Marin'),
	('335','Marin Continuing'),
	('341','Alameda'),
	('343','Laney'),
	('344','Merritt'),
	('345','Berkeley City'),
	('361','San Francisco'),
	('363','San Francisco Ctrs'),
	('371','Canada'),
	('372','San Mateo'),
	('373','Skyline'),
	('411','Cabrillo'),
	('421','Deanza'),
	('422','Foothill'),
	('431','Ohlone'),
	('441','Gavilan'),
	('451','Hartnell'),
	('461','Monterey'),
	('471','Evergreen Valley'),
	('472','San Jose City'),
	('481','Las Positas'),
	('482','Chabot Hayward'),
	('492','Mission'),
	('493','West Valley'),
	('521','Bakersfield'),
	('522','Cerro Coso'),
	('523','Porterville'),
	('531','Merced'),
	('551','San Joaquin Delta'),
	('561','Sequoias'),
	('571','Fresno City'),
	('572','Reedley College'),
	('581','West Hills Coalinga'),
	('582','West Hills Lemoore'),
	('591','Columbia'),
	('592','Modesto'),
	('611','Allan Hancock'),
	('621','Antelope Valley'),
	('641','Cuesta'),
	('651','Santa Barbara'),
	('652','Santa Barbara Cont'),
	('661','Canyons'),
	('681','Moorpark'),
	('682','Oxnard'),
	('683','Ventura'),
	('691','Taft'),
	('711','Compton'),
	('721','El Camino'),
	('731','Glendale'),
	('741','LA City'),
	('742','LA Harbor'),
	('743','LA Mission'),
	('744','LA Pierce'),
	('745','LA Swest'),
	('746','LA Trade'),
	('747','LA Valley'),
	('748','East LA'),
	('749','West LA'),
	('74A','LA ITV'),
	('771','Pasadena'),
	('781','Santa Monica'),
	('811','Cerritos'),
	('821','Citrus'),
	('831','Coastline'),
	('832','Golden West'),
	('833','Orange Coast'),
	('841','Long Beach'),
	('851','Mt. San Antonio'),
	('861','Cypress'),
	('862','Fullerton'),
	('863','North Orange Adult'),
	('871','Santa Ana'),
	('872','Rancho Santiago CED'),
	('873','Santiago Canyon'),
	('881','Rio Hondo'),
	('891','Saddleback'),
	('892','Irvine'),
	('911','Barstow'),
	('921','Chaffey'),
	('931','Desert'),
	('941','Mt. San Jacinto'),
	('951','Palo Verde'),
	('961','Riverside'),
	('962','Moreno Valley'),
	('963','Norco College'),
	('971','Copper Mountain'),
	('981','Crafton Hills'),
	('982','San Bernardino'),
	('991','Victor Valley');