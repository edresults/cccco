USE calpass;

GO

IF (object_id('lbswp.Lens') is not null)
	BEGIN
		DROP TABLE lbswp.Lens;
	END;

GO

CREATE TABLE
	lbswp.Lens
	(
		LensId    tinyint identity(0,1) not null,
		Label     varchar(25)          not null
	);

GO

ALTER TABLE
	lbswp.Lens
ADD CONSTRAINT
	PK_Lens
PRIMARY KEY CLUSTERED
	(
		LensId
	);
GO

INSERT
	lbswp.Lens
	(
		Label
	)
VALUES
	('Field'),
	('Subdiscipline');