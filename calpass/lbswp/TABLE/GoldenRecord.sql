USE calpass;

GO

IF (object_id('lbswp.GoldenRecord') is not null)
	BEGIN
		DROP TABLE lbswp.GoldenRecord;
	END;

GO

CREATE TABLE
	lbswp.GoldenRecord
	(
		-- primary key
		CollegeCode                           char(3)      not null,
		StudentId                             char(9)      not null,
		YearCode                              char(3)      not null,
		ProgramCode                           char(6)      not null,
		-- content
		ContentId                             smallint         null,
		VocationId                            smallint     not null,
		SectorId                              smallint     not null,
		SubdisciplineId                       smallint     not null,
		ProgramId                             smallint     not null,
		-- demograhpic
		DemographicId                         tinyint          null,
		RaceId                                tinyint      not null,
		AgeId                                 tinyint      not null,
		GenderId                              tinyint      not null,
		-- attributes
		IsWorkforce                           bit          not null,
		IsFoster                              bit          not null,
		IsVeteran                             bit          not null,
		IsFirstGeneration                     bit          not null,
		IsEops                                bit          not null,
		IsDisability                          bit          not null,
		IsDisadvantaged                       bit          not null,
		IsFinancialAid                        bit          not null,
		IsCredit                              bit          not null,
		IsNonCredit                           bit          not null,
		IsSkillsBuilder                       bit              null,
		IsTransfer                            bit              null,
		IsExiter                              bit              null,
		IsCompleter                           bit              null,
		EnrolledSections                      tinyint      not null,
		NonIntroUnits                         decimal(5,2) not null,
		IsVocationalSuccess                   bit          not null,
		IsVocationalSuccessAll                bit              null,
		IsAwardProgram                        bit          not null,
		IsAwardProgramRigorous                bit          not null,
		IsAwardAny                            bit              null,
		IsCCEnrolledYearCodeNext              bit              null,
		IsUNEnrolledYearCodeCurr              bit              null,
		IsUNEnrolledYearCodeNext              bit              null,
		IsUNEnrolledYearCodeNextFirst         bit              null,
		IsEmploymentYearCodeNextSecondQuarter bit              null,
		IsEmploymentYearCodeNextFourthQuarter bit              null,
		IsEmploymentYearCodeNextRelated       bit              null,
		EarningsYearCodeNextSecondQuarter     int              null,
		EarningsYearCodeNextAnnual            int              null,
		EarningsYearCodePrevAnnual            int              null
	);

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	PK_GoldenRecord
PRIMARY KEY CLUSTERED
	(
		CollegeCode,
		StudentId,
		YearCode,
		ProgramCode
	);

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__ContentId
DEFAULT
	(
		0
	)
FOR
	ContentId;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__DemographicId
DEFAULT
	(
		0
	)
FOR
	DemographicId;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__IsVocationalSuccessAll
DEFAULT
	(
		0
	)
FOR
	IsVocationalSuccessAll;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__IsAwardAny
DEFAULT
	(
		0
	)
FOR
	IsAwardAny;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__IsSkillsBuilder
DEFAULT
	(
		0
	)
FOR
	IsSkillsBuilder;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__IsTransfer
DEFAULT
	(
		0
	)
FOR
	IsTransfer;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__IsExiter
DEFAULT
	(
		0
	)
FOR
	IsExiter;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__IsCompleter
DEFAULT
	(
		0
	)
FOR
	IsCompleter;


ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__IsCCEnrolledYearCodeNext
DEFAULT
	(
		0
	)
FOR
	IsCCEnrolledYearCodeNext;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__IsUNEnrolledYearCodeCurr
DEFAULT
	(
		0
	)
FOR
	IsUNEnrolledYearCodeCurr;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__IsUNEnrolledYearCodeNext
DEFAULT
	(
		0
	)
FOR
	IsUNEnrolledYearCodeNext;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__IsUNEnrolledYearCodeNextFirst
DEFAULT
	(
		0
	)
FOR
	IsUNEnrolledYearCodeNextFirst;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__IsEmploymentYearCodeNextSecondQuarter
DEFAULT
	(
		0
	)
FOR
	IsEmploymentYearCodeNextSecondQuarter;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__IsEmploymentYearCodeNextFourthQuarter
DEFAULT
	(
		0
	)
FOR
	IsEmploymentYearCodeNextFourthQuarter;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__IsEmploymentYearCodeNextRelated
DEFAULT
	(
		0
	)
FOR
	IsEmploymentYearCodeNextRelated;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__EarningsYearCodeNextSecondQuarter
DEFAULT
	(
		0
	)
FOR
	EarningsYearCodeNextSecondQuarter;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__EarningsYearCodeNextAnnual
DEFAULT
	(
		0
	)
FOR
	EarningsYearCodeNextAnnual;

ALTER TABLE
	lbswp.GoldenRecord
ADD CONSTRAINT
	DF_GoldenRecord__EarningsYearCodePrevAnnual
DEFAULT
	(
		0
	)
FOR
	EarningsYearCodePrevAnnual;