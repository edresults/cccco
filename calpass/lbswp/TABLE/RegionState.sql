USE calpass;

GO

IF (object_id('lbswp.RegionState') is not null)
	BEGIN
		DROP TABLE lbswp.RegionState;
	END;

GO

CREATE TABLE
	lbswp.RegionState
	(
		RegionStateId tinyint     not null identity(0,1),
		RegionId      tinyint     not null,
		RegionCode    char(2)     not null,
		Label         varchar(15) not null
	);

GO

ALTER TABLE
	lbswp.RegionState
ADD CONSTRAINT
	PK_RegionState
PRIMARY KEY CLUSTERED
	(
		RegionStateId
	);

ALTER TABLE
	lbswp.RegionState
ADD CONSTRAINT
	UQ_RegionState__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	lbswp.RegionState
ADD CONSTRAINT
	UQ_RegionState__RegionCode
UNIQUE
	(
		RegionCode
	);

ALTER TABLE
	lbswp.RegionState
ADD CONSTRAINT
	FK_RegionState__RegionId
FOREIGN KEY
	(
		RegionId
	)
REFERENCES
	lbswp.Region
	(
		RegionId
	);

CREATE NONCLUSTERED INDEX
	IX_RegionState__RegionId
ON
	lbswp.RegionState
	(
		RegionId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	lbswp.RegionState
	(
		RegionId,
		RegionCode,
		Label
	)
VALUES
	(0,'CA','California');