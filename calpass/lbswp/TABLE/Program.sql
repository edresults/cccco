USE calpass;

GO

IF (object_id('lbswp.Program') is not null)
	BEGIN
		DROP TABLE lbswp.Program;
	END;

GO

CREATE TABLE
	lbswp.Program
	(
		ProgramId         smallint identity(0,1) not null,
		ContentId         smallint               not null,
		SectorId          smallint                   null,
		ProgramCode       char(6)                not null,
		SubdisciplineId   smallint               not null,
		Label             varchar(75)            not null,
		Description       varchar(610)               null,
		IsVocational      smallint               not null
	);

GO

ALTER TABLE
	lbswp.Program
ADD CONSTRAINT
	PK_Program
PRIMARY KEY
	(
		ProgramCode
	);

ALTER TABLE
	lbswp.Program
ADD CONSTRAINT
	UQ_Program__ProgramId
UNIQUE
	(
		ProgramId
	);

ALTER TABLE
	lbswp.Program
ADD CONSTRAINT
	FK_Program__ContentId
FOREIGN KEY
	(
		ContentId
	)
REFERENCES
	lbswp.Content
	(
		ContentId
	);

CREATE NONCLUSTERED INDEX
	IX_Program__ContentId
ON
	lbswp.Program
	(
		ContentId
	)
INCLUDE
	(
		Label
	);

ALTER TABLE
	lbswp.Program
ADD CONSTRAINT
	FK_Program__SectorId
FOREIGN KEY
	(
		SectorId
	)
REFERENCES
	lbswp.Sector
	(
		SectorId
	);

CREATE NONCLUSTERED INDEX
	IX_Program__SectorId
ON
	lbswp.Program
	(
		SectorId
	)
INCLUDE
	(
		Label
	);

ALTER TABLE
	lbswp.Program
ADD CONSTRAINT
	FK_Program__SubdisciplineId
FOREIGN KEY
	(
		SubdisciplineId
	)
REFERENCES
	lbswp.Subdiscipline
	(
		SubdisciplineId
	);

CREATE NONCLUSTERED INDEX
	IX_Program__SubdisciplineId
ON
	lbswp.Program
	(
		SubdisciplineId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	lbswp.Program
	(
		ContentId,
		SectorId,
		ProgramCode,
		SubdisciplineId,
		Label,
		Description,
		IsVocational
	)
SELECT
	ContentId       = c.ContentId,
	SectorId        = isnull(s.SectorId, 12),
	ProgramCode     = p.ProgramCode,
	SubdisciplineId = sd.SubdisciplineId,
	Label           = p.Name,
	Description     = p.Description,
	IsVocational    = isnull(p.IsVocational, 0)
FROM
	lbswp.Content c
	cross join
	comis.Program p
	inner join
	lbswp.Subdiscipline sd
		on left(sd.SubdisciplineCode, 4) = left(p.ProgramCode, 4)
	left outer join
	(
		VALUES
		('010100','Agriculture Technology and Sciences, General','Agriculture, Water and Environmental Technologies'),
		('010200','Animal Science','Agriculture, Water and Environmental Technologies'),
		('010210','Veterinary Technician (Licensed)','Agriculture, Water and Environmental Technologies'),
		('010220','Artificial Inseminator','Agriculture, Water and Environmental Technologies'),
		('010230','Dairy Science','Agriculture, Water and Environmental Technologies'),
		('010240','Equine Science','Agriculture, Water and Environmental Technologies'),
		('010300','Plant Science','Agriculture, Water and Environmental Technologies'),
		('010310','Agricultural Pest Control Advisor and Operator','Agriculture, Water and Environmental Technologies'),
		('010400','Viticulture, Enology, and Wine Business','Agriculture, Water and Environmental Technologies'),
		('010900','Horticulture','Agriculture, Water and Environmental Technologies'),
		('010910','Landscape Design and Maintenance','Agriculture, Water and Environmental Technologies'),
		('010920','Floriculture -Floristry','Agriculture, Water and Environmental Technologies'),
		('010930','Nursery Technology','Agriculture, Water and Environmental Technologies'),
		('010940','Turfgrass Technology','Agriculture, Water and Environmental Technologies'),
		('011200','Agriculture Business, Sales and Service','Agriculture, Water and Environmental Technologies'),
		('011300','Food Processing and Related Technologies','Agriculture, Water and Environmental Technologies'),
		('011400','Forestry','Agriculture, Water and Environmental Technologies'),
		('011500','Natural Resources','Agriculture, Water and Environmental Technologies'),
		('011510','Parks and Outdoor Recreation','Agriculture, Water and Environmental Technologies'),
		('011520','Wildlife and Fisheries','Agriculture, Water and Environmental Technologies'),
		('011600','Agricultural Power Equipment Technology','Agriculture, Water and Environmental Technologies'),
		('019900','Other Agriculture and Natural Resources','Agriculture, Water and Environmental Technologies'),
		('020100','Architecture and Architectural Technology','Energy, Construction and Utilities'),
		('029900','Other Architecture and Environmental Design','Energy, Construction and Utilities'),
		('030300','Environmental Technology','Agriculture, Water and Environmental Technologies'),
		('043000','Biotechnology and Biomedical Technology','Life Sciences - Biotechnology'),
		('050100','Business and Commerce, General','Business and Entrepreneurship'),
		('050200','Accounting','Business and Entrepreneurship'),
		('050210','Tax Studies','Business and Entrepreneurship'),
		('050400','Banking and Finance','Business and Entrepreneurship'),
		('050500','Business Administration','Business and Entrepreneurship'),
		('050600','Business Management','Business and Entrepreneurship'),
		('050630','Management Development and Supervision','Business and Entrepreneurship'),
		('050640','Small Business and Entrepreneurship','Business and Entrepreneurship'),
		('050650','Retail Store Operations and Management','Retail, Hospitality and Tourism'),
		('050800','International Business and Trade','Global Trade'),
		('050900','Marketing and Distribution','Business and Entrepreneurship'),
		('050910','Advertising','Business and Entrepreneurship'),
		('050920','Purchasing','Business and Entrepreneurship'),
		('050940','Sales and Salesmanship','Business and Entrepreneurship'),
		('050960','Display','Retail, Hospitality and Tourism'),
		('050970','e-commerce (business emphasis)','Business and Entrepreneurship'),
		('051000','Logistics and Materials Transportation','Advanced Transportation and Logistics'),
		('051100','Real Estate','Business and Entrepreneurship'),
		('051110','Escrow','Business and Entrepreneurship'),
		('051200','Insurance','Business and Entrepreneurship'),
		('051400','Office Technology-Office Computer Applications','Information and Communication Technologies (ICT) - Digital Media'),
		('051410','Legal Office Technology','Business and Entrepreneurship'),
		('051420','Medical Office Technology','Health'),
		('051430','Court Reporting','Unassigned'),
		('051440','Office Management','Retail, Hospitality and Tourism'),
		('051600','Labor and Industrial Relations','Retail, Hospitality and Tourism'),
		('051800','Customer Service','Business and Entrepreneurship'),
		('059900','Other Business and Management','Business and Entrepreneurship'),
		('060200','Journalism','Information and Communication Technologies (ICT) - Digital Media'),
		('060400','Radio and Television','Information and Communication Technologies (ICT) - Digital Media'),
		('060410','Radio','Information and Communication Technologies (ICT) - Digital Media'),
		('060420','Television (including combined TV-film-video)','Information and Communication Technologies (ICT) - Digital Media'),
		('060430','Broadcast Journalism','Information and Communication Technologies (ICT) - Digital Media'),
		('060600','Public Relations','Retail, Hospitality and Tourism'),
		('060700','Technical Communication','Information and Communication Technologies (ICT) - Digital Media'),
		('061000','Mass Communications','Information and Communication Technologies (ICT) - Digital Media'),
		('061220','Film Production','Information and Communication Technologies (ICT) - Digital Media'),
		('061400','Digital Media','Information and Communication Technologies (ICT) - Digital Media'),
		('061410','Multimedia','Information and Communication Technologies (ICT) - Digital Media'),
		('061420','Electronic Game Design','Information and Communication Technologies (ICT) - Digital Media'),
		('061430','Website Design and Development','Information and Communication Technologies (ICT) - Digital Media'),
		('061440','Animation','Information and Communication Technologies (ICT) - Digital Media'),
		('061450','Desktop Publishing','Information and Communication Technologies (ICT) - Digital Media'),
		('061460','Computer Graphics and Digital Imagery','Information and Communication Technologies (ICT) - Digital Media'),
		('069900','Other Media and Communications','Information and Communication Technologies (ICT) - Digital Media'),
		('070100','Information Technology, General','Information and Communication Technologies (ICT) - Digital Media'),
		('070200','Computer Information Systems','Information and Communication Technologies (ICT) - Digital Media'),
		('070210','Software Applications','Information and Communication Technologies (ICT) - Digital Media'),
		('070700','Computer Software Development','Information and Communication Technologies (ICT) - Digital Media'),
		('070710','Computer Programming','Information and Communication Technologies (ICT) - Digital Media'),
		('070720','Database Design and Administration','Information and Communication Technologies (ICT) - Digital Media'),
		('070730','Computer Systems Analysis','Information and Communication Technologies (ICT) - Digital Media'),
		('070800','Computer Infrastructure and Support','Information and Communication Technologies (ICT) - Digital Media'),
		('070810','Computer Networking','Information and Communication Technologies (ICT) - Digital Media'),
		('070820','Computer Support','Information and Communication Technologies (ICT) - Digital Media'),
		('070900','World Wide Web Administration','Information and Communication Technologies (ICT) - Digital Media'),
		('070910','E-Commerce (technology emphasis)','Information and Communication Technologies (ICT) - Digital Media'),
		('079900','Other Information Technology','Information and Communication Technologies (ICT) - Digital Media'),
		('080200','Educational Aide (Teacher Assistant)','Education and Human Development'),
		('080210','Educational Aide (Teacher Assistant), Bilingual','Education and Human Development'),
		('080900','Special Education','Education and Human Development'),
		('083520','Fitness Trainer','Unassigned'),
		('083560','Coaching','Education and Human Development'),
		('083570','Aquatics and Lifesaving','Retail, Hospitality and Tourism'),
		('083600','Recreation','Education and Human Development'),
		('083610','Recreation Assistant','Education and Human Development'),
		('085010','Sign Language Interpreting','Education and Human Development'),
		('086000','Educational Technology','Education and Human Development'),
		('089900','Other Education','Education and Human Development'),
		('092400','Engineering Technology, General','Advanced Manufacturing'),
		('093400','Electronics and Electric Technology','Advanced Manufacturing'),
		('093410','Computer Electronics','Advanced Manufacturing'),
		('093420','Industrial Electronics','Advanced Manufacturing'),
		('093430','Telecommunications Technology','Information and Communication Technologies (ICT) - Digital Media'),
		('093440','Electrical Systems and Power Transmission','Energy, Construction and Utilities'),
		('093460','Biomedical Instrumentation','Life Sciences - Biotechnology'),
		('093470','Electron Microscopy','Life Sciences - Biotechnology'),
		('093480','Laser and Optical Technology','Advanced Manufacturing'),
		('093500','Electro-Mechanical Technology','Energy, Construction and Utilities'),
		('093510','Appliance Repair','Advanced Manufacturing'),
		('093600','Printing and Lithography','Advanced Manufacturing'),
		('094300','Instrumentation Technology','Advanced Manufacturing'),
		('094330','Vacuum Technology','Advanced Manufacturing'),
		('094500','Industrial Systems Technology and Maintenance','Advanced Manufacturing'),
		('094600','Environmental Control Technology','Energy, Construction and Utilities'),
		('094610','Energy Systems Technology','Energy, Construction and Utilities'),
		('094700','Diesel Technology','Advanced Transportation and Logistics'),
		('094720','Heavy Equipment Maintenance','Advanced Transportation and Logistics'),
		('094730','Heavy Equipment Operation','Advanced Transportation and Logistics'),
		('094740','Railroad and Light Rail Operations','Advanced Transportation and Logistics'),
		('094750','Truck and Bus Driving','Advanced Transportation and Logistics'),
		('094800','Automotive Technology','Advanced Transportation and Logistics'),
		('094830','Motorcycle, Outboard and Small Engine Repair','Advanced Transportation and Logistics'),
		('094840','Alternative Fuels and Advanced Transportation Technology','Advanced Transportation and Logistics'),
		('094850','Recreational Vehicle Service','Advanced Transportation and Logistics'),
		('094900','Automotive Collision Repair','Advanced Transportation and Logistics'),
		('094910','Upholstery Repair - Automotive','Advanced Transportation and Logistics'),
		('095000','Aeronautical and Aviation Technology','Advanced Manufacturing'),
		('095010','Aviation Airframe Mechanics','Advanced Transportation and Logistics'),
		('095020','Aviation Powerplant Mechanics','Advanced Transportation and Logistics'),
		('095040','Aircraft Electronics (Avionics)','Advanced Manufacturing'),
		('095050','Aircraft Fabrication','Advanced Manufacturing'),
		('095200','Construction Crafts Technology','Energy, Construction and Utilities'),
		('095210','Carpentry','Energy, Construction and Utilities'),
		('095220','Electrical','Energy, Construction and Utilities'),
		('095230','Plumbing, Pipefitting and Steamfitting','Energy, Construction and Utilities'),
		('095240','Glazing','Energy, Construction and Utilities'),
		('095250','Mill and Cabinet Work','Energy, Construction and Utilities'),
		('095260','Masonry, Tile, Cement, Lath and Plaster','Energy, Construction and Utilities'),
		('095270','Painting, Decorating, and Flooring','Energy, Construction and Utilities'),
		('095280','Drywall and Insulation','Energy, Construction and Utilities'),
		('095290','Roofing','Energy, Construction and Utilities'),
		('095300','Drafting Technology','Energy, Construction and Utilities'),
		('095310','Architectural Drafting','Energy, Construction and Utilities'),
		('095320','Civil Drafting','Energy, Construction and Utilities'),
		('095330','Electrical, Electronic, and Electro-Mechanical Drafting','Advanced Manufacturing'),
		('095340','Mechanical Drafting','Advanced Manufacturing'),
		('095360','Technical Illustration','Advanced Manufacturing'),
		('095400','Chemical Technology','Life Sciences - Biotechnology'),
		('095420','Plastics and Composites','Advanced Manufacturing'),
		('095430','Petroleum Technology','Advanced Manufacturing'),
		('095500','Laboratory Science Technology','Life Sciences - Biotechnology'),
		('095600','Manufacturing and Industrial Technology','Advanced Manufacturing'),
		('095630','Machining and Machine Tools','Advanced Manufacturing'),
		('095640','Sheet Metal and Structural Metal','Energy, Construction and Utilities'),
		('095650','Welding Technology','Advanced Manufacturing'),
		('095670','Industrial and Occupational Safety and Health','Advanced Manufacturing'),
		('095680','Industrial Quality Control','Advanced Manufacturing'),
		('095700','Civil and Construction Management Technology','Energy, Construction and Utilities'),
		('095720','Construction Inspection','Energy, Construction and Utilities'),
		('095730','Surveying','Advanced Manufacturing'),
		('095800','Water and Wastewater Technology','Energy, Construction and Utilities'),
		('095900','Marine Technology','Advanced Transportation and Logistics'),
		('095910','Diving and Underwater Safety','Retail, Hospitality and Tourism'),
		('096100','Optics','Advanced Manufacturing'),
		('096200','Musical Instrument Repair','Unassigned'),
		('099900','Other Engineering and Related Industrial Technologies','Unassigned'),
		('100500','Commercial Music','Information and Communication Technologies (ICT) - Digital Media'),
		('100600','Technical Theater','Unassigned'),
		('100810','Commercial Dance','Unassigned'),
		('100900','Applied Design','Unassigned'),
		('101200','Applied Photography','Information and Communication Technologies (ICT) - Digital Media'),
		('101300','Commercial Art','Information and Communication Technologies (ICT) - Digital Media'),
		('103000','Graphic Art and Design','Information and Communication Technologies (ICT) - Digital Media'),
		('109900','Other Fine and Applied Arts','Unassigned'),
		('120100','Health Occupations, General','Health'),
		('120200','Hospital and Health Care Administration','Health'),
		('120500','Medical Laboratory Technology','Health'),
		('120510','Phlebotomy','Health'),
		('120600','Physicians Assistant','Health'),
		('120800','Medical Assisting','Health'),
		('120810','Clinical Medical Assisting','Health'),
		('120820','Administrative Medical Assisting','Health'),
		('120830','Health Facility Unit Coordinator','Health'),
		('120900','Hospital Central Service Technician','Health'),
		('121000','Respiratory Care-Therapy','Health'),
		('121100','Polysomnography','Health'),
		('121200','Electro-Neurodiagnostic Technology','Health'),
		('121300','Cardiovascular Technician','Health'),
		('121400','Orthopedic Assistant','Health'),
		('121500','Electrocardiography','Health'),
		('121700','Surgical Technician','Health'),
		('121800','Occupational Therapy Technology','Health'),
		('121900','Optical Technology','Health'),
		('122000','Speech-Language Pathology and Audiology','Health'),
		('122100','Pharmacy Technology','Health'),
		('122200','Physical Therapist Assistant','Health'),
		('122300','Health Information Technology','Health'),
		('122310','Health Information Coding','Health'),
		('122400','School Health Clerk','Health'),
		('122500','Radiologic Technology','Health'),
		('122600','Radiation Therapy Technician','Health'),
		('122700','Diagnostic Medical Sonography','Health'),
		('122800','Athletic Training and Sports Medicine','Health'),
		('123000','Nursing','Health'),
		('123010','Registered Nursing','Health'),
		('123020','Licensed Vocational Nursing','Health'),
		('123030','Certified Nurse Assistant','Health'),
		('123080','Home Health Aide','Health'),
		('123900','Psychiatric Technician','Health'),
		('124000','Dental Occupations','Health'),
		('124010','Dental Assistant','Health'),
		('124020','Dental Hygienist','Health'),
		('124030','Dental Laboratory Technician','Health'),
		('125000','Emergency Medical Services','Health'),
		('125100','Paramedic','Health'),
		('125500','Mortuary Science','Business and Entrepreneurship'),
		('126100','Community Health Care Worker','Health'),
		('126200','Massage Therapy','Business and Entrepreneurship'),
		('129900','Other Health Occupations','Health'),
		('130100','Family and Consumer Sciences, General','Retail, Hospitality and Tourism'),
		('130110','Consumer Services','Retail, Hospitality and Tourism'),
		('130200','Interior Design and Merchandising','Retail, Hospitality and Tourism'),
		('130300','Fashion','Retail, Hospitality and Tourism'),
		('130310','Fashion Design','Retail, Hospitality and Tourism'),
		('130320','Fashion Merchandising','Retail, Hospitality and Tourism'),
		('130330','Fashion Production','Advanced Manufacturing'),
		('130500','Child Development-Early Care and Education','Education and Human Development'),
		('130520','Children with Special Needs','Education and Human Development'),
		('130540','Preschool Age Child','Education and Human Development'),
		('130550','The School Age Child','Education and Human Development'),
		('130560','Parenting and Family Education','Education and Human Development'),
		('130570','Foster and Kinship Care','Education and Human Development'),
		('130580','Child Development Administration and Management','Education and Human Development'),
		('130590','Infants and Toddlers','Education and Human Development'),
		('130600','Nutrition, Foods, and Culinary Arts','Health'),
		('130620','Dietetic Services and Management','Health'),
		('130630','Culinary Arts','Retail, Hospitality and Tourism'),
		('130660','Dietetic Technology','Health'),
		('130700','Hospitality','Retail, Hospitality and Tourism'),
		('130710','Restaurant and Food Services and Management','Retail, Hospitality and Tourism'),
		('130720','Lodging Management','Retail, Hospitality and Tourism'),
		('130730','Resort and Club Management','Retail, Hospitality and Tourism'),
		('130800','Family Studies','Education and Human Development'),
		('130900','Gerontology','Health'),
		('139900','Other Family and Consumer Sciences','Unassigned'),
		('140200','Paralegal','Unassigned'),
		('160200','Library Technician (Aide)','Unassigned'),
		('192000','Ocean Technology','Advanced Manufacturing'),
		('210200','Public Administration','Unassigned'),
		('210210','Public Works','Energy, Construction and Utilities'),
		('210400','Human Services','Unassigned'),
		('210440','Alcohol and Controlled Substances','Health'),
		('210450','Disability Services','Unassigned'),
		('210500','Administration of Justice','Public Safety'),
		('210510','Corrections','Public Safety'),
		('210520','Probation and Parole','Public Safety'),
		('210530','Industrial and Transportation Security','Public Safety'),
		('210540','Forensics, Evidence, and Investigation','Public Safety'),
		('210550','Police Academy','Public Safety'),
		('213300','Fire Technology','Public Safety'),
		('213310','Wildland Fire Technology','Public Safety'),
		('213350','Fire Academy','Public Safety'),
		('214000','Legal and Community Interpretation','Unassigned'),
		('219900','Other Public and Protective Services','Public Safety'),
		('220610','Geographic Information Systems','Information and Communication Technologies (ICT) - Digital Media'),
		('300500','Custodial Services','Business and Entrepreneurship'),
		('300700','Cosmetology and Barbering','Business and Entrepreneurship'),
		('300800','Dry Cleaning','Retail, Hospitality and Tourism'),
		('300900','Travel Services and Tourism','Retail, Hospitality and Tourism'),
		('302000','Aviation and Airport Management and Services','Advanced Transportation and Logistics'),
		('302010','Aviation and Airport Management','Advanced Transportation and Logistics'),
		('302020','Piloting','Advanced Transportation and Logistics'),
		('302030','Air Traffic Control','Advanced Transportation and Logistics'),
		('302040','Flight Attendant','Retail, Hospitality and Tourism'),
		('309900','Other Commercial Services','Unassigned'),
		('493100','Vocational ESL','Unassigned'),
		('493200','General Work Experience','Unassigned')
	) z (ProgramCode, ProgramTitle, SectorName)
		on p.ProgramCode = z.ProgramCode
	left outer join
	lbswp.Sector s
		on s.Label = z.SectorName
WHERE
	c.Label = 'Program'
	and sd.SubdisciplineId is not null;