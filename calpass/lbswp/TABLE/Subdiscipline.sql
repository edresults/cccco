USE calpass;

GO

IF (object_id('lbswp.Subdiscipline') is not null)
	BEGIN
		DROP TABLE lbswp.Subdiscipline;
	END;

GO

CREATE TABLE
	lbswp.Subdiscipline
	(
		SubdisciplineId   smallint identity(0,1) not null,
		ContentId         smallint               not null,
		SubdisciplineCode char(6)                not null,
		Label             varchar(75)            not null
	);

GO

ALTER TABLE
	lbswp.Subdiscipline
ADD CONSTRAINT
	PK_Subdiscipline
PRIMARY KEY CLUSTERED
	(
		SubdisciplineId
	);

ALTER TABLE
	lbswp.Subdiscipline
ADD CONSTRAINT
	UQ_Subdiscipline__SubdisciplineCode
UNIQUE
	(
		SubdisciplineCode
	);

ALTER TABLE
	lbswp.Subdiscipline
ADD CONSTRAINT
	FK_Subdiscipline__ContentId
FOREIGN KEY
	(
		ContentId
	)
REFERENCES
	lbswp.Content
	(
		ContentId
	);

CREATE NONCLUSTERED INDEX
	IX_Subdiscipline__ContentId
ON
	lbswp.Subdiscipline
	(
		ContentId
	);

GO

INSERT
	lbswp.Subdiscipline
	(
		SubdisciplineCode,
		ContentId,
		Label
	)
SELECT
	SubdisciplineCode = ProgramCode,
	ContentId         = c.ContentId,
	Label             = p.Name
FROM
	lbswp.Content c
	cross join
	comis.Program p
	left outer join
	lbswp.Subdiscipline sd
		on left(sd.SubdisciplineCode, 4) = left(p.ProgramCode, 4)
WHERE
	c.Label = 'Subdiscipline'
	and right(p.ProgramCode, 2) = '00';