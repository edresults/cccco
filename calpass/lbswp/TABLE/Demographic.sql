USE calpass;

GO

IF (object_id('lbswp.Demographic') is not null)
	BEGIN
		DROP TABLE lbswp.Demographic;
	END;

GO

CREATE TABLE
	lbswp.Demographic
	(
		DemographicId tinyint     not null identity(1,1),
		Label         varchar(25) not null,
		Identifier    sysname     not null
	);

GO

ALTER TABLE
	lbswp.Demographic
ADD CONSTRAINT
	PK_Demographic
PRIMARY KEY CLUSTERED
	(
		DemographicId
	);

ALTER TABLE
	lbswp.Demographic
ADD CONSTRAINT
	UQ_Demographic__Label
UNIQUE
	(
		Label
	);

GO

INSERT
	lbswp.Demographic
	(
		Label,
		Identifier
	)
VALUES
	('All', 'DemographicId'),
	('Gender','GenderId'),
	('Age','AgeId'),
	('Race','RaceId');