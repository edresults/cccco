USE calpass;

GO

IF (object_id('lbswp.RegionMicro') is not null)
	BEGIN
		DROP TABLE lbswp.RegionMicro;
	END;

GO

CREATE TABLE
	lbswp.RegionMicro
	(
		RegionMicroId tinyint identity(0,1) not null,
		RegionId      tinyint               not null,
		RegionMacroId tinyint               not null,
		Label         varchar(35)           not null
	);

GO

ALTER TABLE
	lbswp.RegionMicro
ADD CONSTRAINT
	PK_RegionMicro
PRIMARY KEY CLUSTERED
	(
		RegionMicroId
	);

ALTER TABLE
	lbswp.RegionMicro
ADD CONSTRAINT
	UQ_RegionMicro__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	lbswp.RegionMicro
ADD CONSTRAINT
	FK_RegionMicro__RegionMacroId
FOREIGN KEY
	(
		RegionMacroId
	)
REFERENCES
	lbswp.RegionMacro
	(
		RegionMacroId
	);

ALTER TABLE
	lbswp.RegionMicro
ADD CONSTRAINT
	FK_RegionMicro__RegionId
FOREIGN KEY
	(
		RegionId
	)
REFERENCES
	lbswp.Region
	(
		RegionId
	);

CREATE NONCLUSTERED INDEX
	IX_RegionMicro__RegionId
ON
	lbswp.RegionMicro
	(
		RegionId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	lbswp.RegionMicro
	(
		RegionMacroId,
		RegionId,
		Label
	)
VALUES
	(0,2,'East Bay'),
	(0,2,'North Bay'),
	(0,2,'Silicon Valley'),
	(0,2,'Santa Cruz-Monterey'),
	(0,2,'Mid-Peninsula'),
	(1,2,'Northern Central Valley-Mother Lode'),
	(1,2,'Southern Central Valley-Mother Lode'),
	(2,2,'Inland Empire'),
	(3,2,'Los Angeles'),
	(3,2,'Orange County'),
	(4,2,'Greater Sacramento'),
	(4,2,'Northern Inland'),
	(4,2,'Northern Coastal'),
	(5,2,'San Diego-Imperial'),
	(6,2,'South Central Coast');