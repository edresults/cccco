USE calpass;

GO

IF (object_id('lbswp.Mark') is not null)
	BEGIN
		DROP TABLE lbswp.Mark;
	END;

GO

CREATE TABLE
	lbswp.Mark
	(
		MarkCode varchar(5) not null,
		Points decimal(3,2) not null,
		Category smallint not null,
		IsEnroll bit not null,
		IsComplete bit not null,
		IsCompleteDenominator bit not null,
		IsSuccess bit not null,
		IsSuccessDenominator bit not null,
		IsPromoted bit not null,
		IsPromotedDenominator bit not null,
		IsGpa bit not null,
		Rank bit not null,
		Abbreviation varchar(6) not null,
		Description varchar(255) not null
	);

GO

ALTER TABLE
	lbswp.Mark
ADD CONSTRAINT
	PK_Mark
PRIMARY KEY CLUSTERED
	(
		MarkCode
	);

GO

INSERT INTO
	lbswp.Mark
	(
		MarkCode,
		Points,
		Category,
		IsEnroll,
		IsComplete,
		IsCompleteDenominator,
		IsSuccess,
		IsSuccessDenominator,
		IsPromoted,
		IsPromotedDenominator,
		IsGpa,
		Rank,
		Abbreviation,
		Description
	)
SELECT
	MarkCode,
	Points,
	Category,
	IsEnroll,
	IsComplete,
	IsCompleteDenominator,
	IsSuccess,
	IsSuccessDenominator,
	IsPromoted,
	IsPromotedDenominator,
	IsGpa,
	Rank = row_number() over(order by (select 1)),
	Abbreviation,
	Description
FROM
	(
		VALUES
		('A+'  ,4.0 ,4   ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,'A' ,'Highest level of academic achievement where students demonstrate mastery of a subject'),
		('A'   ,4.0 ,4   ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,'A' ,'Highest level of academic achievement where students demonstrate mastery of a subject'),
		('A-'  ,3.7 ,4   ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,'A' ,'Highest level of academic achievement where students demonstrate mastery of a subject'),
		('B+'  ,3.3 ,3   ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,'B' ,'Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
		('B'   ,3.0, 3   ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,'B' ,'Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
		('B-'  ,2.7 ,3   ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,'B' ,'Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
		('C+'  ,2.3 ,2   ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,'C' ,'Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
		('C'   ,2.0 ,2   ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,'C' ,'Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
		('C-'  ,1.7 ,2   ,1 ,1 ,1 ,1 ,1 ,0 ,1 ,1 ,'C' ,'Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
		('CR'  ,0.0 ,1   ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,0 ,'CR','Credit'),
		('P'   ,0.0 ,1   ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,0 ,'CR','Pass'),
		('S'   ,0.0 ,1   ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,0 ,'S' ,'Satisfactory Progress'),
		('SP'  ,0.0 ,1   ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,0 ,'S' ,'Satisfactory Progress'),
		('D+'  ,1.3 ,1   ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,1 ,'D' ,'Fourth highest level of academic achievement where students demonstrate an inadequate understanding of a subject'),
		('D'   ,1.0 ,1   ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,1 ,'D' ,'Fourth highest level of academic achievement where students demonstrate an inadequate understanding of a subject'),
		('D-'  ,0.7 ,1   ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,1 ,'D' ,'Fourth highest level of academic achievement where students demonstrate an inadequate understanding of a subject'),
		('F+'  ,0.3 ,0   ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,1 ,'F' ,'Lowest level of academic achievement where students demonstrate a substandard understanding of a subject'),
		('F'   ,0.0 ,0   ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,1 ,'F' ,'Lowest level of academic achievement where students demonstrate a substandard understanding of a subject'),
		('NC'  ,0.0 ,0   ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'NC','No Credit'),
		('NP'  ,0.0 ,0   ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'NC','No Pass'),
		('NS'  ,0.0 ,0   ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'S' ,'No Satisfactory Progress'),
		('IA+' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete'),
		('IA'  ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete'),
		('IA-' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete'),
		('IB+' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete'),
		('IB'  ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete'),
		('IB-' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete'),
		('IC+' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete'),
		('IC'  ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete'),
		('IC-' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete'),
		('ID+' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete'),
		('ID'  ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete'),
		('ID-' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete'),
		('IF+' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete'),
		('IF'  ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete'),
		('I'   ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete'),
		('IP'  ,0.0 ,-99 ,1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,'I' ,'Incomplete'),
		('ICR' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete Credit'),
		('IPP' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete Pass'),
		('INC' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete Pass'),
		('INP' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,1 ,0 ,1 ,0 ,'I' ,'Incomplete No Pass'),
		('W'   ,0.0 ,-98 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,'W' ,'Withdrawl'),
		('FW'  ,0.0 ,-99 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,'W' ,'Withdrawl without permission and without having achieved a final passing Mark'),
		('MW'  ,0.0 ,-99 ,1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,'O' ,'Military withdraw'),
		('RD'  ,0.0 ,-99 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,'O' ,'Report delayed'),
		('RD0' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,'O' ,'Report delayed'),
		('RD1' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,'O' ,'Report delayed'),
		('RD2' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,'O' ,'Report delayed'),
		('RD3' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,'O' ,'Report delayed'),
		('RD4' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,'O' ,'Report delayed'),
		('RD5' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,'O' ,'Report delayed'),
		('RD6' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,'O' ,'Report delayed'),
		('RD7' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,'O' ,'Report delayed'),
		('RD8' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,'O' ,'Report delayed'),
		('RD9' ,0.0 ,-99 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,'O' ,'Report delayed'),
		('NG'  ,0.0 ,-99 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,'O' ,'Not Marked'),
		('UG'  ,0.0 ,-99 ,1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,'O' ,'UnMarked'),
		('UD'  ,0.0 ,-99 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,'O' ,'UnMarked Dependent'),
		('DR'  ,0.0 ,-99 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,'DR','Dropped section ON OR AFTER first census date and before withdraw period'),
		('XX'  ,0.0 ,-99 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,'XX','None of the above or unknown')
	) t
	(
		MarkCode,
		Points,
		Category,
		IsEnroll,
		IsComplete,
		IsCompleteDenominator,
		IsSuccess,
		IsSuccessDenominator,
		IsPromoted,
		IsPromotedDenominator,
		IsGpa,
		Abbreviation,
		Description
	);

-- Grade   Records
-- A       59902967
-- A+      282523
-- A-      506792
-- B       40103256
-- B+      413051
-- B-      330428
-- C       27921006
-- C+      254651
-- D       9090502
-- D+      53571
-- D-      30178
-- DR      1621227
-- F       16780776
-- F+      335
-- FW      447709
-- I       7
-- IA      2441
-- IA-     171
-- IB      14331
-- IB+     100
-- IB-     54
-- IC      46679
-- IC+     74
-- ID      65641
-- ID+     100
-- ID-     57
-- IF      393705
-- INP     19700
-- IP      1081087
-- IPP     1146
-- IX      743323
-- MW      6836
-- NP      4101058
-- P       15719700
-- RD      1482982
-- RD0     35
-- RD1     520
-- RD2     1
-- RD3     1
-- RD4     251
-- RD5     80
-- RD6     7
-- RD8     26
-- SP      382909
-- UD      7626444
-- UG      40137372
-- W       32512136
-- XX      5161616