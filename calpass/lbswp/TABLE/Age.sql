USE calpass;

GO

IF (object_id('lbswp.Age') is not null)
	BEGIN
		DROP TABLE lbswp.Age;
	END;

GO

CREATE TABLE
	lbswp.Age
	(
		AgeId         tinyint identity(0,1) not null,
		DemographicId tinyint               not null,
		Label         varchar(25)           not null,
		LowerBound    smallint              not null,
		UpperBound    smallint              not null
	);

GO

ALTER TABLE
	lbswp.Age
ADD CONSTRAINT
	PK_Age
PRIMARY KEY CLUSTERED
	(
		AgeId
	);

ALTER TABLE
	lbswp.Age
ADD CONSTRAINT
	UQ_Age__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	lbswp.Age
ADD CONSTRAINT
	UQ_Age__LowerBound
UNIQUE
	(
		LowerBound
	);

ALTER TABLE
	lbswp.Age
ADD CONSTRAINT
	UQ_Age__UpperBound
UNIQUE
	(
		UpperBound
	);

ALTER TABLE
	lbswp.Age
ADD CONSTRAINT
	FK_Age__DemographicId
FOREIGN KEY
	(
		DemographicId
	)
REFERENCES
	lbswp.Demographic
	(
		DemographicId
	);

CREATE NONCLUSTERED INDEX
	IX_Age__DemographicId
ON
	lbswp.Age
	(
		DemographicId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	lbswp.Age
	(
		DemographicId,
		LowerBound,
		UpperBound,
		Label
	)
VALUES
	(2,0,19,'19 or younger'),
	(2,20,24,'20 to 24'),
	(2,25,29,'25 to 29'),
	(2,30,34,'30 to 34'),
	(2,35,39,'35 to 39'),
	(2,40,999,'40 and older');