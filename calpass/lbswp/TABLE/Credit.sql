USE calpass;

GO

IF (object_id('lbswp.Credit') is not null)
	BEGIN
		DROP TABLE lbswp.Credit
	END;

GO

CREATE TABLE
	lbswp.Credit
	(
		CreditId    int identity(0,1) not null,
		CreditCode  char(1)           not null,
		Description varchar(25)       not null,
		IsCredit    bit               not null
	)

GO

ALTER TABLE
	lbswp.Credit
ADD CONSTRAINT
	PK_Credit
PRIMARY KEY CLUSTERED
	(
		CreditId
	);

ALTER TABLE
	lbswp.Credit
ADD CONSTRAINT
	UQ_Credit__CreditCode
UNIQUE
	(
		CreditCode
	);

GO

INSERT
	lbswp.Credit
	(
		CreditCode,
		IsCredit,
		Description
	)
VALUES
	('D',1,'Degree Applicable'),
	('C',1,'Not Degree Applicable'),
	('N',0,'Noncredit');