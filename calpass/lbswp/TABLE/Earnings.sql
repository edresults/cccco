USE calpass;

GO

IF (object_id('lbswp.Earnings') is not null)
	BEGIN
		DROP TABLE lbswp.Earnings;
	END;

GO

CREATE TABLE
	lbswp.Earnings
	(
		CollegeId       tinyint  not null,
		YearId          tinyint  not null,
		StudentId       char(9)  not null,
		ContentId       smallint not null,
		VocationId      smallint not null,
		SectorId        smallint not null,
		SubdisciplineId smallint not null,
		ProgramId       smallint not null,
		DemographicId   tinyint  not null,
		AgeId           tinyint  not null,
		GenderId        tinyint  not null,
		RaceId          tinyint  not null,
		Earnings        int      not null
	);

GO

ALTER TABLE
	lbswp.Earnings
ADD CONSTRAINT
	PK_Earnings
PRIMARY KEY
	(
		CollegeId,
		YearId,
		ProgramId,
		StudentId
	);