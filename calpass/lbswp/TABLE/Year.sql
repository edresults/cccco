USE calpass;

GO

IF (object_id('lbswp.Year') is not null)
	BEGIN
		DROP TABLE lbswp.Year
	END;

GO

CREATE TABLE
	lbswp.Year
	(
		YearId       tinyint identity(0,1) not null,
		YearCode     char(3)               not null,
		YearCodeNext char(3)               not null,
		AcademicYear char(9)               not null
	)

GO

ALTER TABLE
	lbswp.Year
ADD CONSTRAINT
	PK_Year
PRIMARY KEY CLUSTERED
	(
		YearId
	);

GO

ALTER TABLE
	lbswp.Year
ADD CONSTRAINT
	UQ_Year__YearCode
UNIQUE
	(
		YearCode
	);

GO

INSERT INTO
	lbswp.Year
	(
		YearCode,
		YearCodeNext,
		AcademicYear
	)
VALUES
	('120','130','2011-2012'),
	('130','140','2012-2013'),
	('140','150','2013-2014'),
	('150','160','2014-2015'),
	('160','170','2015-2016'),
	('170','180','2016-2017');

ALTER TABLE
	lbswp.Year
ADD
	BeginDate date;

ALTER TABLE
	lbswp.Year
ADD
	EndDate date;

GO

UPDATE
	y
SET
	BeginDate = convert(date, substring(AcademicYear, 1, 4) + '-07-01'),
	EndDate   = convert(date, substring(AcademicYear, 6, 4) + '-06-30')
FROM
	lbswp.Year y

GO

ALTER TABLE
	lbswp.Year
ALTER COLUMN
	BeginDate date not null;

ALTER TABLE
	lbswp.Year
ALTER COLUMN
	EndDate date not null;