USE calpass;

GO

IF (object_id('lbswp.Gender') is not null)
	BEGIN
		DROP TABLE lbswp.Gender;
	END;

GO

CREATE TABLE
	lbswp.Gender
	(
		GenderId      tinyint identity(0,1) not null,
		DemographicId tinyint               not null,
		GenderCode    char(1)               not null,
		Label         varchar(25)           not null
	)

GO

ALTER TABLE
	lbswp.Gender
ADD CONSTRAINT
	PK_Gender
PRIMARY KEY CLUSTERED
	(
		GenderId
	);

ALTER TABLE
	lbswp.Gender
ADD CONSTRAINT
	UQ_Gender__GenderCode
UNIQUE
	(
		GenderCode
	);

ALTER TABLE
	lbswp.Gender
ADD CONSTRAINT
	UQ_Gender__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	lbswp.Gender
ADD CONSTRAINT
	FK_Gender__DemographicId
FOREIGN KEY
	(
		DemographicId
	)
REFERENCES
	lbswp.Demographic
	(
		DemographicId
	);

CREATE NONCLUSTERED INDEX
	IX_Gender__DemographicId
ON
	lbswp.Gender
	(
		DemographicId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	lbswp.Gender
	(
		DemographicId,
		GenderCode,
		Label
	)
VALUES
	(1,'F','Female'),
	(1,'M','Male'),
	(1,'X','Unknown or Unreported');