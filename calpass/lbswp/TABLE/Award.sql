USE calpass;

GO

IF (object_id('lbswp.Award') is not null)
	BEGIN
		DROP TABLE lbswp.Award;
	END;

GO

CREATE TABLE
	lbswp.Award
	(
		AwardId       int          not null identity(0,1),
		AwardCode     char(6)      not null,
		Description   varchar(255) not null,
		IsCredit      bit          not null,
		IsRigorous    bit          not null,
		IsDegree      bit          not null,
		IsCertificate bit          not null
	)

GO

ALTER TABLE
	lbswp.Award
ADD CONSTRAINT
	PK_Award
PRIMARY KEY CLUSTERED
	(
		AwardId
	);

ALTER TABLE
	lbswp.Award
ADD CONSTRAINT
	UQ_Award__AwardCode
UNIQUE
	(
		AwardCode
	);

GO

INSERT
	lbswp.Award
	(
		AwardCode,
		IsCredit,
		IsRigorous,
		IsDegree,
		IsCertificate,
		Description
	)
VALUES
	('A',1,1,1,0,'Associate of Arts (A.A.) degree'),
	('S',1,1,1,0,'Associate of Science (A.S.) degree'),
	('Y',1,1,1,0,'Baccalaureate of Arts (B.A.) degree'),
	('Z',1,1,1,0,'Baccalaureate of Science (B.S.) degree'),
	('E',1,1,0,1,'Certificate requiring 6 to fewer than 18 semester units (not approved by Chancellor’s Office)'),
	('B',1,1,0,1,'Certificate requiring 12 to fewer than 18 semester units (approved by Chancellor’s Office)'),
	('L',1,1,0,1,'Certificate requiring 18 to fewer than 30 semester units'),
	('T',1,1,0,1,'Certificate requiring 30 to fewer than 60 semester units'),
	('F',1,1,0,1,'Certificate requiring 60 or more semester units'),
	('O',1,0,0,1,'Other Credit Award, under 6 semester units'),
	('G',0,0,0,1,'Noncredit award requiring fewer than 48 hours'),
	('H',0,1,0,1,'Noncredit award requiring from 48 to fewer than 96 hours'),
	('I',0,1,0,1,'Noncredit award requiring from 96 to fewer than 144 hours'),
	('J',0,1,0,1,'Noncredit award requiring from 144 to fewer than 192 hours'),
	('K',0,1,0,1,'Noncredit award requiring from 192 to fewer than 288 hours'),
	('P',0,1,0,1,'Noncredit award requiring from 288 to fewer than 480 hours'),
	('Q',0,1,0,1,'Noncredit award requiring from 480 to fewer than 960 hours'),
	('R',0,1,0,1,'Noncredit award requiring 960 hours or more');