USE calpass;

GO

IF (object_id('lbswp.Region') is not null)
	BEGIN
		DROP TABLE lbswp.Region;
	END;

GO

CREATE TABLE
	lbswp.Region
	(
		RegionId         tinyint     not null identity(0,1),
		Label            varchar(15) not null,
		RegionIdentifier sysname     not null
	);

GO

ALTER TABLE
	lbswp.Region
ADD CONSTRAINT
	PK_Region
PRIMARY KEY CLUSTERED
	(
		RegionId
	);

ALTER TABLE
	lbswp.Region
ADD CONSTRAINT
	UQ_Region__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	lbswp.Region
ADD CONSTRAINT
	UQ_Region__RegionIdentifier
UNIQUE
	(
		RegionIdentifier
	);

GO

INSERT
	lbswp.Region
	(
		Label,
		RegionIdentifier
	)
VALUES
	('State','RegionStateId'),
	('Macroregion','RegionMacroId'),
	('Microregion','RegionMicroId'),
	('College','RegionCollegeId');