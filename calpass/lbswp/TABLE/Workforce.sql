USE calpass;

GO

IF (object_id('lbswp.Workforce') is not null)
	BEGIN
		DROP TABLE lbswp.Workforce;
	END;

GO

CREATE TABLE
	lbswp.Workforce
	(
		WorkforceId   tinyint identity(0,1) not null,
		WorkforceCode char(1)               not null,
		Label         varchar(50)           not null,
		IsWorkforce   bit                   not null
	)

GO

ALTER TABLE
	lbswp.Workforce
ADD CONSTRAINT
	PK_Workforce
PRIMARY KEY CLUSTERED
	(
		WorkforceId
	);

ALTER TABLE
	lbswp.Workforce
ADD CONSTRAINT
	UQ_Workforce__WorkforceCode
UNIQUE
	(
		WorkforceCode
	);

GO

INSERT
	lbswp.Workforce
	(
		WorkforceCode,
		IsWorkforce,
		Label
	)
VALUES
	('J',1,'Student is a participant in the WIA program'),
	('N',0,'Student is not a participant in the WIA program');