USE calpass;

GO

IF (object_id('lbswp.RegionMacro') is not null)
	BEGIN
		DROP TABLE lbswp.RegionMacro;
	END;

GO

CREATE TABLE
	lbswp.RegionMacro
	(
		RegionMacroId tinyint identity(0,1) not null,
		RegionId      tinyint               not null,
		RegionStateId tinyint               not null,
		Label         varchar(35)           not null
	);

GO

ALTER TABLE
	lbswp.RegionMacro
ADD CONSTRAINT
	PK_RegionMacro
PRIMARY KEY CLUSTERED
	(
		RegionMacroId
	);

ALTER TABLE
	lbswp.RegionMacro
ADD CONSTRAINT
	UQ_RegionMacro__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	lbswp.RegionMacro
ADD CONSTRAINT
	FK_RegionMacro__RegionStateId
FOREIGN KEY
	(
		RegionStateId
	)
REFERENCES
	lbswp.RegionState
	(
		RegionStateId
	);

ALTER TABLE
	lbswp.RegionMacro
ADD CONSTRAINT
	FK_RegionMacro__RegionId
FOREIGN KEY
	(
		RegionId
	)
REFERENCES
	lbswp.Region
	(
		RegionId
	);

CREATE NONCLUSTERED INDEX
	IX_RegionMacro__RegionId
ON
	lbswp.RegionMacro
	(
		RegionId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	lbswp.RegionMacro
	(
		RegionStateId,
		RegionId,
		Label
	)
VALUES
	(0,1,'Bay Area'),
	(0,1,'Central Valley-Mother Lode'),
	(0,1,'Inland Empire-Desert'),
	(0,1,'Los Angeles-Orange County'),
	(0,1,'North-Far North'),
	(0,1,'San Diego-Imperial'),
	(0,1,'South Central Coast');