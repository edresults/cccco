USE calpass;

GO

IF (object_id('lbswp.Aid') is not null)
	BEGIN
		DROP TABLE lbswp.Aid;
	END;

GO

CREATE TABLE
	lbswp.Aid
	(
		AidId         tinyint identity(0,1) not null,
		AidCode       char(2)               not null,
		AidCategoryId tinyint               not null,
		Label         varchar(255)          not null,
		IsFederal     bit                   not null,
		IsCalifornia  bit                   not null,
		IsLaunchBoard bit                   not null
	);

GO

ALTER TABLE
	lbswp.Aid
ADD CONSTRAINT
	PK_Aid
PRIMARY KEY CLUSTERED
	(
		AidId
	);

ALTER TABLE
	lbswp.Aid
ADD CONSTRAINT
	UQ_Aid__AidCode
UNIQUE
	(
		AidCode
	);

ALTER TABLE
	lbswp.Aid
ADD CONSTRAINT
	FK_Aid__AidCategoryId
FOREIGN KEY
	(
		AidCategoryId
	)
REFERENCES
	lbswp.AidCategory
	(
		AidCategoryId
	);

GO

INSERT
	lbswp.Aid
	(
		AidCode,
		AidCategoryId,
		IsFederal,
		IsCalifornia,
		IsLaunchBoard,
		Label
	)
VALUES
	('BA',0,0,1,1,'BOGW - Method A-? (unknown base)'),
	('B1',0,0,1,1,'BOGW - Method A-1 based on TANF recipient status'),
	('B2',0,0,1,1,'BOGW - Method A-2 based on SSI recipient status'),
	('B3',0,0,1,1,'BOGW - Method A-3 based on general assistance recipient status'),
	('BB',0,0,1,1,'BOGW - Method B based on income standards'),
	('BC',0,0,1,1,'BOGW - Method C based on financial need'),
	('BD',0,0,1,1,'BOGW - Method D based on Homeless Youth determination'),
	('F1',0,1,0,1,'Fee Waiver – Dependent (children) of Deceased Law Enforcement/Fire Suppression (Subject to Group C edits)'),
	('F2',0,1,0,1,'Fee Waiver – Dependent (surviving spouse and children) of deceased or disabled member of CA National Guard (Subject to Group C edits)'),
	('F3',0,1,0,1,'Fee Waiver – Dependent of (children) deceased or disabled Veteran (Subject to Group C edits)'),
	('F4',0,1,0,1,'Fee Waiver – Dependent of (children) of Congressional Medal of Honor recipient (CMH) or CMH recipient (Subject to Group C edits)'),
	('F5',0,1,0,1,'Fee Waiver – Dependent of (surviving spouse and children) of deceased victims of September 11, 2001 terrorist attack. (Subject of Group C edits)'),
	('WC',1,0,1,0,'California State Work Study (SWS)'),
	('WE',1,0,1,0,'EOPS Work Study'),
	('WF',1,1,0,1,'Federal Work Study (FWS) (Federal share)'),
	('WK',1,0,1,0,'CalWORKs Work Study'),
	('WY',1,0,1,0,'CAFYES Work Study'),
	('WU',1,0,0,0,'Other Work Study and matching funds'),
	('GA',2,1,0,0,'Academic Competitiveness Grant'),
	('GB',2,0,1,1,'Cal Grant B'),
	('GC',2,0,1,1,'Cal Grant C'),
	('GD',2,0,1,0,'Full-time Student Success Grant'),
	('GE',2,0,1,0,'EOPS Grant'),
	('GF',2,0,1,0,'CARE Grant'),
	('GN',2,0,1,0,'CSAC CNG EAAP (California Student Aid Commission California National Guard Education Assistance Award Program)'),
	('GP',2,1,0,1,'Pell Grant'),
	('GS',2,1,0,1,'SEOG (Supplemental Educational Opportunity Grant)'),
	('GU',2,0,0,0,'Other grant: institutional source'),
	('GV',2,0,0,0,'Other grant: non-institutional source'),
	('GW',2,1,0,1,'Bureau of Indian Affairs (BIA) Grant'),
	('GG',2,0,1,1,'Chafee Grant'),
	('GK',2,0,1,0,'CalWORKs Grant'),
	('GY',2,0,1,0,'CAFYES Grant'),
	('LD',3,1,0,0,'Perkins Loan'),
	('LE',3,0,1,0,'EOPS Loan'),
	('LG',3,1,0,0,'Stafford Loan, subsidized (valid through 2009-2010 data)'),
	('LH',3,1,0,0,'Stafford Loan, unsubsidized (valid through 2009-2010 data)'),
	('LI',3,0,0,0,'Other loan, institutional source'),
	('LN',3,0,0,0,'Other loan, non-institutional source'),
	('LP',3,0,0,0,'PLUS loan, parent loan for undergraduate student'),
	('LS',3,1,0,0,'Federal Direct Student Loan - subsidized'),
	('LL',3,1,0,0,'Federal Direct Student Loan - unsubsidized'),
	('LY',3,0,1,0,'CAFYES Loan'),
	('SO',4,0,0,0,'Scholarship: Osher'),
	('SU',4,0,0,0,'Scholarship: institutional source'),
	('SV',4,0,0,0,'Scholarship: non-institutional source'),
	('SX',4,0,0,0,'Scholarship: source unknown');