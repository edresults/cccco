USE calpass;

GO

IF (object_id('lbswp.Race') is not null)
	BEGIN
		DROP TABLE lbswp.Race;
	END;

GO

CREATE TABLE
	lbswp.Race
	(
		RaceId        tinyint identity(0,1) not null,
		DemographicId tinyint               not null,
		RaceCode      char(1)               not null,
		Label         varchar(45)           not null
	)

GO

ALTER TABLE
	lbswp.Race
ADD CONSTRAINT
	PK_Race
PRIMARY KEY CLUSTERED
	(
		RaceId
	);

ALTER TABLE
	lbswp.Race
ADD CONSTRAINT
	UQ_Race__RaceCode
UNIQUE
	(
		RaceCode
	);

ALTER TABLE
	lbswp.Race
ADD CONSTRAINT
	FK_Race__DemographicId
FOREIGN KEY
	(
		DemographicId
	)
REFERENCES
	lbswp.Demographic
	(
		DemographicId
	);

CREATE NONCLUSTERED INDEX
	IX_Race__DemographicId
ON
	lbswp.Race
	(
		DemographicId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	lbswp.Race
	(
		DemographicId,
		RaceCode,
		Label
	)
VALUES
	(3,'A','Asian'),
	(3,'B','Black or African American'),
	(3,'F','Filipino'),
	(3,'H','Hispanic or Latino'),
	(3,'N','American Indian or Alaska Native'),
	(3,'P','Native Hawaiian or Other Pacific Islander'),
	(3,'W','White'),
	(3,'T','Two or More Races'),
	(3,'X','Unknown or Unreported');