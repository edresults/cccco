USE calpass;

GO

IF (object_id('lbswp.Disability') is not null)
	BEGIN
		DROP TABLE lbswp.Disability;
	END;

GO

CREATE TABLE
	lbswp.Disability
	(
		DisabilityId   tinyint identity(0,1) not null,
		DisabilityCode char(1)               not null,
		Label          varchar(50)           not null
	)

GO

ALTER TABLE
	lbswp.Disability
ADD CONSTRAINT
	PK_Disability
PRIMARY KEY CLUSTERED
	(
		DisabilityId
	);

ALTER TABLE
	lbswp.Disability
ADD CONSTRAINT
	UQ_Disability__DisabilityCode
UNIQUE
	(
		DisabilityCode
	);

GO

INSERT
	lbswp.Disability
	(
		DisabilityCode,
		Label
	)
VALUES
	('M','Physical Disability'),
	('V','Blind and Low Vision'),
	('H','Deaf and Hard of Hearing (DHH)'),
	('S','Speech/Language Impaired '),
	('D','Intellectual Disability (ID)'),
	('B','Acquired Brain Injury (ABI)'),
	('L','Learning Disability'),
	('P','Mental Health Disability'),
	('A','Attention Deficit Hyperactivity Disorder (ADHD)'),
	('U','Autism Spectrum'),
	('O','Other Health Conditions and Disabilities');