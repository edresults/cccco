IF (object_id('lbswp.Base') is not null)
	BEGIN
		DROP TABLE lbswp.Base;
	END;

GO

CREATE TABLE
	lbswp.Base
	(
		CollegeId      tinyint     not null,
		YearId         tinyint     not null,
		ContentId      tinyint     not null,
		ContentKey     smallint    not null,
		DemographicId  tinyint     not null,
		DemographicKey tinyint     not null,
		Metric101      int             null,
		Metric101State int             null,
		Metric101Macro int             null,
		Metric101Micro int             null,
		Metric101Top   int             null,
		Metric408      int             null,
		Metric408State int             null,
		Metric408Macro int             null,
		Metric408Micro int             null,
		Metric408Top   int             null,
		Metric410      int             null,
		Metric410State int             null,
		Metric410Macro int             null,
		Metric410Micro int             null,
		Metric410Top   int             null,
		Metric500      int             null,
		Metric500State int             null,
		Metric500Macro int             null,
		Metric500Micro int             null,
		Metric500Top   int             null,
		Metric501      int             null,
		Metric501State int             null,
		Metric501Macro int             null,
		Metric501Micro int             null,
		Metric501Top   int             null,
		Metric502      int             null,
		Metric502State int             null,
		Metric502Macro int             null,
		Metric502Micro int             null,
		Metric502Top   int             null,
		Metric508      int             null,
		Metric508State int             null,
		Metric508Macro int             null,
		Metric508Micro int             null,
		Metric508Top   int             null,
		Metric512      int             null,
		Metric512State int             null,
		Metric512Macro int             null,
		Metric512Micro int             null,
		Metric512Top   int             null,
		Metric513      int             null,
		Metric513State int             null,
		Metric513Macro int             null,
		Metric513Micro int             null,
		Metric513Top   int             null
	);

GO

ALTER TABLE
	lbswp.Base
ADD CONSTRAINT
	PK_Base
PRIMARY KEY
	(
		CollegeId,
		YearId,
		ContentId,
		ContentKey,
		DemographicId,
		DemographicKey
	);