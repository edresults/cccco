USE calpass;

GO

IF (object_id('lbswp.Content') is not null)
	BEGIN
		DROP TABLE lbswp.Content;
	END;

GO

CREATE TABLE
	lbswp.Content
	(
		ContentId  smallint identity(0,1) not null,
		Label      varchar(15)           not null,
		Identifier sysname               not null
	);

GO

ALTER TABLE
	lbswp.Content
ADD CONSTRAINT
	PK_Content
PRIMARY KEY CLUSTERED
	(
		ContentId
	);

ALTER TABLE
	lbswp.Content
ADD CONSTRAINT
	UQ_Content__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	lbswp.Content
ADD CONSTRAINT
	UQ_Content__Identifier
UNIQUE
	(
		Identifier
	);

GO

INSERT
	lbswp.Content
	(
		Label,
		Identifier
	)
VALUES
	('All', 'ContentId'),
	('Vocation', 'VocationId'),
	('Sector', 'SectorId'),
	('Subdiscipline', 'SubdisciplineId'),
	('Program', 'ProgramId');