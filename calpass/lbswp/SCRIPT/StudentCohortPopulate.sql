SET NOCOUNT ON;

DECLARE
	@Message   nvarchar(2048),
	@Time      datetime,
	@Seconds   int,
	@Severity  tinyint = 0,
	@State     tinyint = 1,
	@Error     varchar(2048),
	@Iterator   int = 0,
	@Increment int = 100000,
	@Iterations    int;

-- INSERT
-- 	lbswp.Student
-- 	(
-- 		InterSegmentKey
-- 	)
-- SELECT
-- 	InterSegmentKey
-- FROM
-- 	comis.studntid id
-- WHERE
-- 	id.InterSegmentKey is not null
-- 	and exists (
-- 		SELECT
-- 			1
-- 		FROM
-- 			comis.stterm st
-- 			inner join
-- 			comis.Term t
-- 				on t.TermCode = st.term_id
-- 			inner join
-- 			lbswp.Year y
-- 				on y.YearCode = t.YearCode
-- 		WHERE
-- 			st.college_id = id.college_id
-- 			and st.student_id = id.student_id
-- 	)
-- UNION
-- SELECT
-- 	InterSegmentKey
-- FROM
-- 	comis.studntid id
-- WHERE
-- 	id.InterSegmentKey is not null
-- 	and exists (
-- 		SELECT
-- 			1
-- 		FROM
-- 			comis.spawards sp
-- 			inner join
-- 			lbswp.Year y
-- 				on y.YearCode = sp.term_id
-- 		WHERE
-- 			sp.college_id = id.college_id
-- 			and sp.student_id = id.student_id
-- 	);

SELECT
	@Iterations = convert(varchar, ceiling(convert(decimal, count(*)) / @Increment))
FROM
	lbswp.Student;

BEGIN

	WHILE (@Iterator < @Iterations)
	BEGIN

		SET @Error = convert(varchar, @Iterator + 1) + ' of ' + convert(varchar, @Iterations);

		RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

		SET @Time = getdate();

		MERGE
			lbswp.StudentCohort
		WITH
			(
				TABLOCKX
			) t
		USING
			(
				SELECT
					s.InterSegmentKey,
					c.CollegeCode,
					c.YearCode,
					c.ProgramCode,
					c.IsCredit,
					c.IsNonCredit,
					c.IsBuilder,
					c.IsTransfer,
					c.IsExiter,
					c.IsCompleter
				FROM
					lbswp.Student s
					cross apply
					lbswp.BehaviorCohort(s.InterSegmentKey) c
				WHERE
					Iterator >= @Increment * @Iterator
					and Iterator < @Increment * (@Iterator + 1)
			) s
		ON
			(
				s.InterSegmentKey = t.InterSegmentKey
				and s.CollegeCode = t.CollegeCode
				and s.YearCode = t.YearCode
				and s.ProgramCode = t.ProgramCode
			)
		WHEN MATCHED THEN
			UPDATE SET
				t.InterSegmentKey = s.InterSegmentKey,
				t.CollegeCode     = s.CollegeCode,
				t.YearCode        = s.YearCode,
				t.ProgramCode     = s.ProgramCode,
				t.IsCredit        = s.IsCredit,
				t.IsNonCredit     = s.IsNonCredit,
				t.IsBuilder       = s.IsBuilder,
				t.IsTransfer      = s.IsTransfer,
				t.IsExiter        = s.IsExiter,
				t.IsCompleter     = s.IsCompleter
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					InterSegmentKey,
					CollegeCode,
					YearCode,
					ProgramCode,
					IsCredit,
					IsNonCredit,
					IsBuilder,
					IsTransfer,
					IsExiter,
					IsCompleter
				)
			VALUES
				(
					InterSegmentKey,
					CollegeCode,
					YearCode,
					ProgramCode,
					IsCredit,
					IsNonCredit,
					IsBuilder,
					IsTransfer,
					IsExiter,
					IsCompleter
				);

		-- get time elapsed
		SET @Seconds = datediff(second, @Time, getdate());
		-- set msg
		SET @Error = 'ITERATION: ' + 
			convert(nvarchar, @Seconds / 86400) + ':' +
			convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
		-- output to user
		RAISERROR(@Error, 0, 1) WITH NOWAIT;

		SET @Iterator += 1;

	END;
END;