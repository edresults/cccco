WITH
	UnitaryData
AS
	(
		SELECT
			RegionCollegeId,
			YearId,
			ProgramCode,
			RaceId,
			Earnings    = Earnings,
			RecordCount = count(*),
			RecordIndex = row_number() over (partition by RegionCollegeId, YearId, ProgramCode, RaceId ORDER BY Earnings)
		FROM
			lbswp.Earnings
		GROUP BY
			RegionCollegeId,
			YearId,
			ProgramCode,
			RaceId,
			Earnings
	),
	UnitaryPointer
AS
	(
		SELECT
			RegionCollegeId,
			YearId,
			ProgramCode,
			RaceId,
			RecordCount,
			LBound = ((RecordCount + 1) / 2),
			UBound = 
				case
					when RecordCount % 2 = 0 then (1 + (RecordCount / 2))
					else (RecordCount + 1) / 2
				end
		FROM
			(
				SELECT DISTINCT
					RegionCollegeId,
					YearId,
					ProgramCode,
					RaceId,
					RecordCount
				FROM
					UnitaryData
			) ud
	)
SELECT
	ud.RegionCollegeId,
	ud.YearId,
	ud.ProgramCode,
	ud.RaceId,
	Median = avg(ud.Earnings)
FROM
	UnitaryPointer up
	inner join
	UnitaryData ud
		on  ud.RegionCollegeId  = up.RegionCollegeId
		and ud.YearId     = up.YearId
		and ud.ProgramCode  = up.ProgramCode
		and ud.RaceId       = up.RaceId
		and ud.RecordIndex >= up.LBound
		and ud.RecordIndex <= up.UBound
GROUP BY
	ud.RegionCollegeId,
	ud.YearId,
	ud.ProgramCode,
	ud.RaceId;