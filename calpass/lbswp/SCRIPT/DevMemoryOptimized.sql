IF (object_id('lbswp.Earnings') is not null)
	BEGIN
		DROP TABLE lbswp.Earnings;
	END;

GO

CREATE TABLE
	lbswp.Earnings
	(
		RegionStateId   tinyint  not null,
		RegionMacroId   tinyint  not null,
		RegionMicroId   tinyint  not null,
		RegionCollegeId tinyint  not null,
		YearId          tinyint  not null,
		StudentId       char(9)  not null,
		ContentId       smallint not null,
		VocationId      smallint not null,
		SectorId        smallint     null,
		SubdisciplineId smallint not null,
		ProgramId       smallint not null,
		DemographicId   smallint not null,
		AgeId           smallint not null,
		GenderId        smallint not null,
		RaceId          smallint not null,
		Earnings        int      not null
	);

ALTER TABLE
	lbswp.Earnings
ADD CONSTRAINT
 PK_Earnings
PRIMARY KEY
	(
		RegionCollegeId,
		YearId,
		ProgramId,
		StudentId
	);

INSERT INTO
	lbswp.Earnings
	(
		RegionStateId,
		RegionMacroId,
		RegionMicroId,
		RegionCollegeId,
		YearId,
		StudentId,
		ContentId,
		VocationId,
		SectorId,
		SubdisciplineId,
		ProgramId,
		DemographicId,
		AgeId,
		GenderId,
		RaceId,
		Earnings
	)
SELECT
	rs.RegionStateId,
	rma.RegionMacroId,
	rmi.RegionMicroId,
	rc.RegionCollegeId,
	y.YearId,
	gr.StudentId,
	ContentId = 0,
	p.IsVocational,
	p.SectorId,
	s.SubdisciplineId,
	p.ProgramId,
	DemographicId = 0,
	gr.AgeId,
	gr.GenderId,
	gr.RaceId,
	Earnings = EarningsYearCodeNextSecondQuarter
FROM
	lbswp.GoldenRecord gr
	inner join
	lbswp.Year y
		on y.YearCode = gr.YearCode
	-- Region
	inner join
	lbswp.RegionCollege rc
		on rc.CollegeCode = gr.CollegeCode
	inner join
	lbswp.RegionMicro rmi
		on rmi.RegionMicroId = rc.RegionMicroId
	inner join
	lbswp.RegionMacro rma
		on rma.RegionMacroId = rmi.RegionMacroId
	inner join
	lbswp.RegionState rs
		on rs.RegionStateId = rma.RegionStateId
	inner join
	-- Content
	lbswp.Program p
		on p.ProgramCode = gr.Programcode
	inner join
	lbswp.Subdiscipline s
		on s.SubdisciplineCode = p.SubdisciplineCode
WHERE
	EarningsYearCodeNextSecondQuarter <> 0
	and IsExiter = 1;

