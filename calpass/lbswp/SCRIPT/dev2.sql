USE calpass;

GO

IF (object_id('lbswp.RegionHierarchy') is not null)
	BEGIN
		DROP TABLE lbswp.RegionHierarchy;
	END;

GO

CREATE TABLE
	lbswp.RegionHierarchy
	(
		RegionHierarchyId hierarchyid not null,
		Label             varchar(25) not null,
		Level             as RegionHierarchyId.GetLevel()
	);

GO

-- ALTER TABLE
-- 	lbswp.RegionHierarchy
-- ADD CONSTRAINT
-- 	PK_RegionHierarchy
-- PRIMARY KEY CLUSTERED
-- 	(
-- 		RegionHierarchyId
-- 	);

-- ALTER TABLE
-- 	lbswp.RegionHierarchy
-- ADD CONSTRAINT
-- 	UQ_RegionHierarchy__RegionHierarchyVal
-- UNIQUE
-- 	(
-- 		RegionHierarchyVal
-- 	);

GO

INSERT
	lbswp.RegionHierarchy
	(
		RegionHierarchyId,
		Label
	)
SELECT
	RegionHierarchyId = 
		case
			when rs.Label  is not null then '/' + convert(varchar, rs.RegionId + 1) + '/'
			when rma.Label is not null then '/' + convert(varchar, rs.RegionId + 1) + '/' + convert(varchar, rma.RegionMacroId + 1) + '/'
			when rmi.Label is not null then '/' + convert(varchar, rs.RegionId + 1) + '/' + convert(varchar, rma.RegionMacroId + 1) + '/' + convert(varchar, rmi.RegionMicroId + 1)
			when rc.Label  is not null then '/' + convert(varchar, rs.RegionId + 1) + '/' + convert(varchar, rma.RegionMacroId + 1) + '/' + convert(varchar, rmi.RegionMicroId + 1) + '/' + convert(varchar, rc.RegionMicroId + 1) + '/'
		end,
	Label = coalesce(rs.Label, rma.Label, rmi.Label, rc.Label)
FROM
	lbswp.Region r
	left outer join
	lbswp.RegionState rs
		on rs.RegionId = r.RegionId
	left outer join
	lbswp.RegionMacro rma
		on rma.RegionId = r.RegionId
	left outer join
	lbswp.RegionMicro rmi
		on rmi.RegionId = r.RegionId
	left outer join
	lbswp.RegionCollege rc
		on rc.RegionId = r.RegionId