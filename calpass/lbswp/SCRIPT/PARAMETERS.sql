SELECT
	RegionId,
	RegionKey,
	RegionValue,
	ContentId,
	ContentKey,
	ContentValue,
	DemographicId,
	DemographicKey,
	DemographicValue
FROM
	lbswp.RegionHierarchy rh
	cross join
	lbswp.ContentHierarchy ch
	cross join
	lbswp.DemographicHierarchy dh
ORDER BY
	RegionId,
	RegionKey,
	ContentId,
	ContentKey,
	DemographicId,
	DemographicKey;