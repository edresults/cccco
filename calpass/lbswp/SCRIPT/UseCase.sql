DECLARE
	@InterSegment dbo.InterSegment;

INSERT INTO
	@InterSegment
VALUES
	-- NIL AWARDS
	(0x5E2D8C973E90170DA396677E9B56DCF6F1DE631E53A45FCC34AAE3BE366035B05A8FD0B43C709B4EC9B3230905BDFD52AFB7BCA02F029CB60EC46B4E8A9A5505),
	-- HAS AWARDS
	(0xDC6AFA46C28C1CA43518C11CD2D1D67B11FE5AA5B469322F7F2DAECDE777F2CC69C41D72FBE63F559E77211E63E2437306955F988CF97E6A9795DB0E4E00BF89),
	-- HAS UNIV
	(0x9B02A58D33B890CCB4326362375F55B1AFDF3CCD8C77A12F5E572A739DC137EF8C1D5CBF316BAB1B44FE7665645DED61917A9DDDD27230D04B1881D4638C9006);

SELECT
	InterSegmentKey,
	YearCode,
	ProgramCode,
	IsCredit,
	IsNonCredit,
	IsBuilder,
	IsTransfer,
	IsExiter,
	IsCompleter
FROM
	@InterSegment i
	cross apply
	lbswp.BehaviorCohort(i.InterSegmentKey);