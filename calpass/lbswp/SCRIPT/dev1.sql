DECLARE
	@GeoCode  char(3) = '641',
	@YearCode char(3) = '150';

SELECT
	rs.RegionStateId,
	rma.RegionMacroId,
	rmi.RegionMicroId,
	rc.RegionCollegeId,
	gr.CollegeCode,
	gr.YearCode,
	Sections        = sum(convert(tinyint, gr.EnrolledSections)),
	Awards          = count(distinct case when gr.IsAwardAny = 1 then StudentId end),
	Transfers       = count(distinct case when gr.IsTransfer = 1 then StudentId end),
	IsEmpFQ2ndNum   = count(distinct case when gr.IsExiter = 1 and gr.IsEmploymentYearCodeNextSecondQuarter = 1 then StudentId end),
	IsEmpFQ2ndDem   = count(distinct case when gr.IsExiter = 1 then StudentId end),
	IsEmpFQ2ndPct   = 
		convert(
			decimal(5,2),
			100.00
			*
			count(distinct case when gr.IsExiter = 1 and gr.IsEmploymentYearCodeNextSecondQuarter = 1 then StudentId end)
			/
			count(distinct case when gr.IsExiter = 1 then StudentId end)
		),
	IsEmpFQ4thNum   = count(distinct case when gr.IsExiter = 1 and gr.IsEmploymentYearCodeNextFourthQuarter = 1 then StudentId end),
	IsEmpFQ4thDem   = count(distinct case when gr.IsExiter = 1 then StudentId end),
	IsEmpFQ4thPct   = 
		convert(
			decimal(5,2),
			100.00
			*
			count(distinct case when gr.IsExiter = 1 and gr.IsEmploymentYearCodeNextFourthQuarter = 1 then StudentId end)
			/
			count(distinct case when gr.IsExiter = 1 then StudentId end)
		)
	WagesFQ2nd      = case when gr.IsExiter = 1 then EarningsYearCodeNextSecondQuarter end
FROM
	lbswp.GoldenRecord gr
	inner join
	lbswp.RegionCollege rc
		on rc.CollegeCode = gr.CollegeCode
	inner join
	lbswp.RegionMicro rmi
		on rmi.RegionMicroId = rc.RegionMicroId
	inner join
	lbswp.RegionMacro rma
		on rma.RegionMacroId = rmi.RegionMacroId
	inner join
	lbswp.RegionState rs
		on rs.RegionStateId = rma.RegionStateId
WHERE
	gr.CollegeCode = @GeoCode
	and gr.YearCode = @YearCode
GROUP BY
	rs.RegionStateId,
	rma.RegionMacroId,
	rmi.RegionMicroId,
	rc.RegionCollegeId,
	gr.CollegeCode,
	gr.YearCode
ORDER BY
	rc.RegionCollegeId,
	rmi.RegionMicroId,
	rma.RegionMacroId,
	rs.RegionStateId;


DECLARE
	@rows int = (
		SELECT
			count(distinct StudentId)
		FROM
			lbswp.GoldenRecord
		WHERE
			CollegeCode = '641'
			and YearCode = '150'
			and EarningsYearCodeNextSecondQuarter <> 0
	);
 
SELECT
	avg(EarningsYearCodeNextSecondQuarter)
FROM
	(
		SELECT DISTINCT
			StudentId,
			EarningsYearCodeNextSecondQuarter
		FROM
			lbswp.GoldenRecord
		WHERE
			CollegeCode = '641'
			and YearCode = '150'
			and EarningsYearCodeNextSecondQuarter <> 0
		ORDER BY
			EarningsYearCodeNextSecondQuarter
		OFFSET
			((@rows - 1) / 2) ROWS
		FETCH NEXT
			1 + (1 - @rows % 2) ROWS ONLY
	) median;