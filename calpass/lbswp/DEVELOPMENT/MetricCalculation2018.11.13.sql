SELECT
	*
FROM
	(
		SELECT
			a.YearId,
			a.RegionId,
			a.RegionKey,
			a.ContentId,
			a.ContentKey,
			a.DemographicId,
			a.DemographicKey,
			Metric103b   = isnull(round(convert(decimal(5,2), 100.00 * a.Metric103b   / nullif(d.Metric103b,   0)), 2), 100.00),
			Metric200bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric200bN  / nullif(d.Metric200bN,  0)), 2), 100.00),
			Metric200bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric200bD  / nullif(d.Metric200bD,  0)), 2), 100.00),
			Metric201bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric201bN  / nullif(d.Metric201bN,  0)), 2), 100.00),
			Metric201bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric201bD  / nullif(d.Metric201bD,  0)), 2), 100.00),
			Metric202bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric202bN  / nullif(d.Metric202bN,  0)), 2), 100.00),
			Metric202bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric202bD  / nullif(d.Metric202bD,  0)), 2), 100.00),
			Metric206bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric206bN  / nullif(d.Metric206bN,  0)), 2), 100.00),
			Metric206bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric206bD  / nullif(d.Metric206bD,  0)), 2), 100.00),
			Metric207bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric207bN  / nullif(d.Metric207bN,  0)), 2), 100.00),
			Metric207bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric207bD  / nullif(d.Metric207bD,  0)), 2), 100.00),
			Metric208bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric208bN  / nullif(d.Metric208bN,  0)), 2), 100.00),
			Metric208bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric208bD  / nullif(d.Metric208bD,  0)), 2), 100.00),
			Metric209bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric209bN  / nullif(d.Metric209bN,  0)), 2), 100.00),
			Metric209bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric209bD  / nullif(d.Metric209bD,  0)), 2), 100.00),
			Metric210bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric210bN  / nullif(d.Metric210bN,  0)), 2), 100.00),
			Metric210bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric210bD  / nullif(d.Metric210bD,  0)), 2), 100.00),
			Metric211bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric211bN  / nullif(d.Metric211bN,  0)), 2), 100.00),
			Metric211bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric211bD  / nullif(d.Metric211bD,  0)), 2), 100.00),
			Metric212bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric212bN  / nullif(d.Metric212bN,  0)), 2), 100.00),
			Metric212bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric212bD  / nullif(d.Metric212bD,  0)), 2), 100.00),
			Metric213bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric213bN  / nullif(d.Metric213bN,  0)), 2), 100.00),
			Metric213bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric213bD  / nullif(d.Metric213bD,  0)), 2), 100.00),
			Metric214bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric214bN  / nullif(d.Metric214bN,  0)), 2), 100.00),
			Metric214bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric214bD  / nullif(d.Metric214bD,  0)), 2), 100.00),
			Metric215bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric215bN  / nullif(d.Metric215bN,  0)), 2), 100.00),
			Metric215bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric215bD  / nullif(d.Metric215bD,  0)), 2), 100.00),
			Metric216bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric216bN  / nullif(d.Metric216bN,  0)), 2), 100.00),
			Metric216bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric216bD  / nullif(d.Metric216bD,  0)), 2), 100.00),
			Metric218bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric218bN  / nullif(d.Metric218bN,  0)), 2), 100.00),
			Metric218bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric218bD  / nullif(d.Metric218bD,  0)), 2), 100.00),
			Metric219bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric219bN  / nullif(d.Metric219bN,  0)), 2), 100.00),
			Metric219bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric219bD  / nullif(d.Metric219bD,  0)), 2), 100.00),
			Metric220bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric220bN  / nullif(d.Metric220bN,  0)), 2), 100.00),
			Metric220bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric220bD  / nullif(d.Metric220bD,  0)), 2), 100.00),
			Metric221bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric221bN  / nullif(d.Metric221bN,  0)), 2), 100.00),
			Metric221bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric221bD  / nullif(d.Metric221bD,  0)), 2), 100.00),
			Metric222bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric222bN  / nullif(d.Metric222bN,  0)), 2), 100.00),
			Metric222bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric222bD  / nullif(d.Metric222bD,  0)), 2), 100.00),
			Metric223bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric223bN  / nullif(d.Metric223bN,  0)), 2), 100.00),
			Metric223bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric223bD  / nullif(d.Metric223bD,  0)), 2), 100.00),
			Metric224bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric224bN  / nullif(d.Metric224bN,  0)), 2), 100.00),
			Metric224bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric224bD  / nullif(d.Metric224bD,  0)), 2), 100.00),
			Metric225bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric225bN  / nullif(d.Metric225bN,  0)), 2), 100.00),
			Metric225bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric225bD  / nullif(d.Metric225bD,  0)), 2), 100.00),
			Metric302bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric302bN  / nullif(d.Metric302bN,  0)), 2), 100.00),
			Metric302bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric302bD  / nullif(d.Metric302bD,  0)), 2), 100.00),
			Metric303bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric303bN  / nullif(d.Metric303bN,  0)), 2), 100.00),
			Metric303bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric303bD  / nullif(d.Metric303bD,  0)), 2), 100.00),
			Metric304bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric304bN  / nullif(d.Metric304bN,  0)), 2), 100.00),
			Metric304bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric304bD  / nullif(d.Metric304bD,  0)), 2), 100.00),
			Metric305bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric305bN  / nullif(d.Metric305bN,  0)), 2), 100.00),
			Metric305bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric305bD  / nullif(d.Metric305bD,  0)), 2), 100.00),
			Metric306bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric306bN  / nullif(d.Metric306bN,  0)), 2), 100.00),
			Metric306bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric306bD  / nullif(d.Metric306bD,  0)), 2), 100.00),
			Metric307bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric307bN  / nullif(d.Metric307bN,  0)), 2), 100.00),
			Metric307bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric307bD  / nullif(d.Metric307bD,  0)), 2), 100.00),
			Metric308bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric308bN  / nullif(d.Metric308bN,  0)), 2), 100.00),
			Metric308bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric308bD  / nullif(d.Metric308bD,  0)), 2), 100.00),
			Metric309bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric309bN  / nullif(d.Metric309bN,  0)), 2), 100.00),
			Metric309bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric309bD  / nullif(d.Metric309bD,  0)), 2), 100.00),
			Metric310bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric310bN  / nullif(d.Metric310bN,  0)), 2), 100.00),
			Metric310bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric310bD  / nullif(d.Metric310bD,  0)), 2), 100.00),
			Metric410bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric410bN  / nullif(d.Metric410bN,  0)), 2), 100.00),
			Metric410bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric410bD  / nullif(d.Metric410bD,  0)), 2), 100.00),
			Metric411bN  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric411bN  / nullif(d.Metric411bN,  0)), 2), 100.00),
			Metric411bD  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric411bD  / nullif(d.Metric411bD,  0)), 2), 100.00),
			Metric500btN = isnull(round(convert(decimal(5,2), 100.00 * a.Metric500btN / nullif(d.Metric500btN, 0)), 2), 100.00),
			Metric500btD = isnull(round(convert(decimal(5,2), 100.00 * a.Metric500btD / nullif(d.Metric500btD, 0)), 2), 100.00),
			Metric500bkN = isnull(round(convert(decimal(5,2), 100.00 * a.Metric500bkN / nullif(d.Metric500bkN, 0)), 2), 100.00),
			Metric500bkD = isnull(round(convert(decimal(5,2), 100.00 * a.Metric500bkD / nullif(d.Metric500bkD, 0)), 2), 100.00),
			Metric501btN = isnull(round(convert(decimal(5,2), 100.00 * a.Metric501btN / nullif(d.Metric501btN, 0)), 2), 100.00),
			Metric501btD = isnull(round(convert(decimal(5,2), 100.00 * a.Metric501btD / nullif(d.Metric501btD, 0)), 2), 100.00),
			Metric501bkN = isnull(round(convert(decimal(5,2), 100.00 * a.Metric501bkN / nullif(d.Metric501bkN, 0)), 2), 100.00),
			Metric501bkD = isnull(round(convert(decimal(5,2), 100.00 * a.Metric501bkD / nullif(d.Metric501bkD, 0)), 2), 100.00)
		FROM
			(
				SELECT
					YearId,
					RegionId = 
						case
							when rc.RegionCollegeId is not null then rc.RegionId
							when ra.RegionMacroId   is not null then ra.RegionId
							when ri.RegionMicroId   is not null then ri.RegionId
							else 1
						end,
					RegionKey = 
						case
							when rc.RegionCollegeId is not null then rc.RegionCollegeId
							when ra.RegionMacroId   is not null then ra.RegionMacroId
							when ri.RegionMicroId   is not null then ri.RegionMicroId
							else 1
						end,
					ContentId = 
						case
							when p.ProgramId  is not null then p.ContentId
							when s.SectorId   is not null then s.ContentId
							when v.VocationId is not null then v.ContentId
							else 1
						end,
					ContentKey = 
						case
							when p.ProgramId  is not null then p.ProgramId
							when s.SectorId   is not null then s.SectorId
							when v.VocationId is not null then v.VocationId
							else 1
						end,
					DemographicId = 
						case
							when a.AgeId is not null then a.DemographicId
							else 1
						end,
					DemographicKey = 
						case
							when a.AgeId is not null then a.AgeId
							else 1
						end,
					Metric103b   = max(case when metric_id = '103b'   then metric_value end),
					Metric200bN  = max(case when metric_id = '200bN'  then metric_value end),
					Metric200bD  = max(case when metric_id = '200bD'  then metric_value end),
					Metric201bN  = max(case when metric_id = '201bN'  then metric_value end),
					Metric201bD  = max(case when metric_id = '201bD'  then metric_value end),
					Metric202bN  = max(case when metric_id = '202bN'  then metric_value end),
					Metric202bD  = max(case when metric_id = '202bD'  then metric_value end),
					Metric206bN  = max(case when metric_id = '206bN'  then metric_value end),
					Metric206bD  = max(case when metric_id = '206bD'  then metric_value end),
					Metric207bN  = max(case when metric_id = '207bN'  then metric_value end),
					Metric207bD  = max(case when metric_id = '207bD'  then metric_value end),
					Metric208bN  = max(case when metric_id = '208bN'  then metric_value end),
					Metric208bD  = max(case when metric_id = '208bD'  then metric_value end),
					Metric209bN  = max(case when metric_id = '209bN'  then metric_value end),
					Metric209bD  = max(case when metric_id = '209bD'  then metric_value end),
					Metric210bN  = max(case when metric_id = '210bN'  then metric_value end),
					Metric210bD  = max(case when metric_id = '210bD'  then metric_value end),
					Metric211bN  = max(case when metric_id = '211bN'  then metric_value end),
					Metric211bD  = max(case when metric_id = '211bD'  then metric_value end),
					Metric212bN  = max(case when metric_id = '212bN'  then metric_value end),
					Metric212bD  = max(case when metric_id = '212bD'  then metric_value end),
					Metric213bN  = max(case when metric_id = '213bN'  then metric_value end),
					Metric213bD  = max(case when metric_id = '213bD'  then metric_value end),
					Metric214bN  = max(case when metric_id = '214bN'  then metric_value end),
					Metric214bD  = max(case when metric_id = '214bD'  then metric_value end),
					Metric215bN  = max(case when metric_id = '215bN'  then metric_value end),
					Metric215bD  = max(case when metric_id = '215bD'  then metric_value end),
					Metric216bN  = max(case when metric_id = '216bN'  then metric_value end),
					Metric216bD  = max(case when metric_id = '216bD'  then metric_value end),
					Metric218bN  = max(case when metric_id = '218bN'  then metric_value end),
					Metric218bD  = max(case when metric_id = '218bD'  then metric_value end),
					Metric219bN  = max(case when metric_id = '219bN'  then metric_value end),
					Metric219bD  = max(case when metric_id = '219bD'  then metric_value end),
					Metric220bN  = max(case when metric_id = '220bN'  then metric_value end),
					Metric220bD  = max(case when metric_id = '220bD'  then metric_value end),
					Metric221bN  = max(case when metric_id = '221bN'  then metric_value end),
					Metric221bD  = max(case when metric_id = '221bD'  then metric_value end),
					Metric222bN  = max(case when metric_id = '222bN'  then metric_value end),
					Metric222bD  = max(case when metric_id = '222bD'  then metric_value end),
					Metric223bN  = max(case when metric_id = '223bN'  then metric_value end),
					Metric223bD  = max(case when metric_id = '223bD'  then metric_value end),
					Metric224bN  = max(case when metric_id = '224bN'  then metric_value end),
					Metric224bD  = max(case when metric_id = '224bD'  then metric_value end),
					Metric225bN  = max(case when metric_id = '225bN'  then metric_value end),
					Metric225bD  = max(case when metric_id = '225bD'  then metric_value end),
					Metric302bN  = max(case when metric_id = '302bN'  then metric_value end),
					Metric302bD  = max(case when metric_id = '302bD'  then metric_value end),
					Metric303bN  = max(case when metric_id = '303bN'  then metric_value end),
					Metric303bD  = max(case when metric_id = '303bD'  then metric_value end),
					Metric304bN  = max(case when metric_id = '304bN'  then metric_value end),
					Metric304bD  = max(case when metric_id = '304bD'  then metric_value end),
					Metric305bN  = max(case when metric_id = '305bN'  then metric_value end),
					Metric305bD  = max(case when metric_id = '305bD'  then metric_value end),
					Metric306bN  = max(case when metric_id = '306bN'  then metric_value end),
					Metric306bD  = max(case when metric_id = '306bD'  then metric_value end),
					Metric307bN  = max(case when metric_id = '307bN'  then metric_value end),
					Metric307bD  = max(case when metric_id = '307bD'  then metric_value end),
					Metric308bN  = max(case when metric_id = '308bN'  then metric_value end),
					Metric308bD  = max(case when metric_id = '308bD'  then metric_value end),
					Metric309bN  = max(case when metric_id = '309bN'  then metric_value end),
					Metric309bD  = max(case when metric_id = '309bD'  then metric_value end),
					Metric310bN  = max(case when metric_id = '310bN'  then metric_value end),
					Metric310bD  = max(case when metric_id = '310bD'  then metric_value end),
					Metric410bN  = max(case when metric_id = '410bN'  then metric_value end),
					Metric410bD  = max(case when metric_id = '410bD'  then metric_value end),
					Metric411bN  = max(case when metric_id = '411bN'  then metric_value end),
					Metric411bD  = max(case when metric_id = '411bD'  then metric_value end),
					Metric500btN = max(case when metric_id = '500btN' then metric_value end),
					Metric500btD = max(case when metric_id = '500btD' then metric_value end),
					Metric500bkN = max(case when metric_id = '500bkN' then metric_value end),
					Metric500bkD = max(case when metric_id = '500bkD' then metric_value end),
					Metric501btN = max(case when metric_id = '501btN' then metric_value end),
					Metric501btD = max(case when metric_id = '501btD' then metric_value end),
					Metric501bkN = max(case when metric_id = '501bkN' then metric_value end),
					Metric501bkD = max(case when metric_id = '501bkD' then metric_value end)
				FROM
					Launchboard4Top6.dbo.CCStudentEquityTableAgeOldScript t
					inner join
					Year y
						on y.YearEnd = t.academic_year
					left outer join
					RegionMacro ra
						on ra.Label = t.macroregion_name
					left outer join
					RegionMicro ri
						on ri.Label = t.microregion_name
					left outer join
					RegionCollege rc
						on rc.IpedsCode = t.cc_ipeds_code
					left outer join
					Program p
						on p.ProgramCode = t.program_code
					left outer join
					Sector s
						on s.Label = t.sector_name
					left outer join
					Vocation v
						on v.VocationId = case when t.cte_program = 'Yes' then 1 when t.cte_program = 'No' then 2 end
					left outer join
					Age a
						on a.Label = t.subgroup
				WHERE
					academic_year = '2012'
				GROUP BY
					YearId,
					case
						when rc.RegionCollegeId is not null then rc.RegionId
						when ra.RegionMacroId   is not null then ra.RegionId
						when ri.RegionMicroId   is not null then ri.RegionId
						else 1
					end,
					case
						when rc.RegionCollegeId is not null then rc.RegionCollegeId
						when ra.RegionMacroId   is not null then ra.RegionMacroId
						when ri.RegionMicroId   is not null then ri.RegionMicroId
						else 1
					end,
					case
						when p.ProgramId  is not null then p.ContentId
						when s.SectorId   is not null then s.ContentId
						when v.VocationId is not null then v.ContentId
						else 1
					end,
					case
						when p.ProgramId  is not null then p.ProgramId
						when s.SectorId   is not null then s.SectorId
						when v.VocationId is not null then v.VocationId
						else 1
					end,
					case
						when a.AgeId is not null then a.DemographicId
						else 1
					end,
					case
						when a.AgeId is not null then a.AgeId
						else 1
					end
			) a
			inner join
			MetricOutput d
				on  a.YearId         = d.YearId
				and a.RegionId       = d.RegionId
				and a.RegionKey      = d.RegionKey
				and a.ContentId      = d.ContentId
				and a.ContentKey     = d.ContentKey
				and a.DemographicId  = d.DemographicId
				and a.DemographicKey = d.DemographicKey
	) a
WHERE
	Metric103b   < 95.00
	or Metric103b   > 105.00
	or Metric200bN  < 95.00
	or Metric200bN  > 105.00
	or Metric200bD  < 95.00
	or Metric200bD  > 105.00
	or Metric201bN  < 95.00
	or Metric201bN  > 105.00
	or Metric201bD  < 95.00
	or Metric201bD  > 105.00
	or Metric202bN  < 95.00
	or Metric202bN  > 105.00
	or Metric202bD  < 95.00
	or Metric202bD  > 105.00
	or Metric206bN  < 95.00
	or Metric206bN  > 105.00
	or Metric206bD  < 95.00
	or Metric206bD  > 105.00
	or Metric207bN  < 95.00
	or Metric207bN  > 105.00
	or Metric207bD  < 95.00
	or Metric207bD  > 105.00
	or Metric208bN  < 95.00
	or Metric208bN  > 105.00
	or Metric208bD  < 95.00
	or Metric208bD  > 105.00
	or Metric209bN  < 95.00
	or Metric209bN  > 105.00
	or Metric209bD  < 95.00
	or Metric209bD  > 105.00
	or Metric210bN  < 95.00
	or Metric210bN  > 105.00
	or Metric210bD  < 95.00
	or Metric210bD  > 105.00
	or Metric211bN  < 95.00
	or Metric211bN  > 105.00
	or Metric211bD  < 95.00
	or Metric211bD  > 105.00
	or Metric212bN  < 95.00
	or Metric212bN  > 105.00
	or Metric212bD  < 95.00
	or Metric212bD  > 105.00
	or Metric213bN  < 95.00
	or Metric213bN  > 105.00
	or Metric213bD  < 95.00
	or Metric213bD  > 105.00
	or Metric214bN  < 95.00
	or Metric214bN  > 105.00
	or Metric214bD  < 95.00
	or Metric214bD  > 105.00
	or Metric215bN  < 95.00
	or Metric215bN  > 105.00
	or Metric215bD  < 95.00
	or Metric215bD  > 105.00
	or Metric216bN  < 95.00
	or Metric216bN  > 105.00
	or Metric216bD  < 95.00
	or Metric216bD  > 105.00
	or Metric218bN  < 95.00
	or Metric218bN  > 105.00
	or Metric218bD  < 95.00
	or Metric218bD  > 105.00
	or Metric219bN  < 95.00
	or Metric219bN  > 105.00
	or Metric219bD  < 95.00
	or Metric219bD  > 105.00
	or Metric220bN  < 95.00
	or Metric220bN  > 105.00
	or Metric220bD  < 95.00
	or Metric220bD  > 105.00
	or Metric221bN  < 95.00
	or Metric221bN  > 105.00
	or Metric221bD  < 95.00
	or Metric221bD  > 105.00
	or Metric222bN  < 95.00
	or Metric222bN  > 105.00
	or Metric222bD  < 95.00
	or Metric222bD  > 105.00
	or Metric223bN  < 95.00
	or Metric223bN  > 105.00
	or Metric223bD  < 95.00
	or Metric223bD  > 105.00
	or Metric224bN  < 95.00
	or Metric224bN  > 105.00
	or Metric224bD  < 95.00
	or Metric224bD  > 105.00
	or Metric225bN  < 95.00
	or Metric225bN  > 105.00
	or Metric225bD  < 95.00
	or Metric225bD  > 105.00
	or Metric302bN  < 95.00
	or Metric302bN  > 105.00
	or Metric302bD  < 95.00
	or Metric302bD  > 105.00
	or Metric303bN  < 95.00
	or Metric303bN  > 105.00
	or Metric303bD  < 95.00
	or Metric303bD  > 105.00
	or Metric304bN  < 95.00
	or Metric304bN  > 105.00
	or Metric304bD  < 95.00
	or Metric304bD  > 105.00
	or Metric305bN  < 95.00
	or Metric305bN  > 105.00
	or Metric305bD  < 95.00
	or Metric305bD  > 105.00
	or Metric306bN  < 95.00
	or Metric306bN  > 105.00
	or Metric306bD  < 95.00
	or Metric306bD  > 105.00
	or Metric307bN  < 95.00
	or Metric307bN  > 105.00
	or Metric307bD  < 95.00
	or Metric307bD  > 105.00
	or Metric308bN  < 95.00
	or Metric308bN  > 105.00
	or Metric308bD  < 95.00
	or Metric308bD  > 105.00
	or Metric309bN  < 95.00
	or Metric309bN  > 105.00
	or Metric309bD  < 95.00
	or Metric309bD  > 105.00
	or Metric310bN  < 95.00
	or Metric310bN  > 105.00
	or Metric310bD  < 95.00
	or Metric310bD  > 105.00
	or Metric410bN  < 95.00
	or Metric410bN  > 105.00
	or Metric410bD  < 95.00
	or Metric410bD  > 105.00
	or Metric411bN  < 95.00
	or Metric411bN  > 105.00
	or Metric411bD  < 95.00
	or Metric411bD  > 105.00
	or Metric500btN < 95.00
	or Metric500btN > 105.00
	or Metric500btD < 95.00
	or Metric500btD > 105.00
	or Metric500bkN < 95.00
	or Metric500bkN > 105.00
	or Metric500bkD < 95.00
	or Metric500bkD > 105.00
	or Metric501btN < 95.00
	or Metric501btN > 105.00
	or Metric501btD < 95.00
	or Metric501btD > 105.00
	or Metric501bkN < 95.00
	or Metric501bkN > 105.00
	or Metric501bkD < 95.00
	or Metric501bkD > 105.00
ORDER BY
	YearId,
	RegionId,
	RegionKey,
	ContentId,
	ContentKey,
	DemographicId,
	DemographicKey;


-- SELECT
-- 	DemographicKey,
-- 	Metric103B
-- FROM
-- 	metricoutput
-- WHERE
-- 	YearId = 1
-- 	and RegionId = 1
-- 	and RegionKey = 1
-- 	and ContentId = 3
-- 	and ContentKey = 13
-- 	and DemographicId = 3

-- SELECT
-- 	*
-- FROM
-- 	Launchboard4Top6.dbo.CCStudentEquityTableAgeOldScript
-- WHERE
-- 	metric_id = '103b'
-- 	and academic_year = '2012'
-- 	and macroregion_name = 'All'
-- 	and microregion_name = 'All'
-- 	and sector = 'Unassigned'
-- ORDER BY
-- 	metric_id,
-- 	academic_year,
-- 	subgroup;