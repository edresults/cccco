DECLARE
	@CollegeCode   char(3) = '641',
	@YearCode      char(3) = '160',
	@IsExiter      bit     = 1,
	@ProgramCode   char(6) = '050200';

SELECT
	count(*)
FROM
	lbswp.GoldenRecord gr
	inner join
	lbswp.Year y1
		on y1.YearCode = gr.YearCode
WHERE
	gr.CollegeCode = @CollegeCode
	and gr.YearCode = @YearCode
	and gr.ProgramCode = @ProgramCode
	-- MUST CHECK CC SYSTEM; NOT SINGULAR COLLEGE
	and not exists (
		SELECT
			1
		FROM
			comis.studntid id1
			inner join
			comis.studntid id2
				on id2.ssn = id1.ssn
			inner join
			comis.stterm st
				on st.college_id = id2.college_id
				and st.student_id = id2.student_id
			inner join
			comis.Term t
				on t.TermCode = st.term_id
			inner join
			lbswp.Year y2
				on y2.YearCode = t.YearCode
		WHERE
			id1.college_id = gr.CollegeCode
			and id1.student_id = gr.StudentId
			and y1.YearCode = y2.YearCodeNext
	)
	and not exists (
		SELECT
			1
		FROM
			comis.studntid id
			inner join
			comis.Transfer tr
				on id.ssn = tr.StudentSsn
			inner join
			comis.Term t
				on t.TermCode = gr.YearCode
			inner join
			comis.Term t1
				on t1.TermCode = t.YearCodeNext
		WHERE
			tr.InstitutionType = '4'
			and id.college_id = gr.CollegeCode
			and id.student_id = gr.StudentId
		GROUP BY
			t1.BeginDate,
			t1.EndDate
		HAVING
			t1.BeginDate <= min(tr.TransferDate)
			and t1.EndDate >= min(tr.TransferDate)
	)