IF (object_id('Student') is not null)
	BEGIN
		DROP TABLE Student;
	END;

IF (object_id('dbo.StudentMetrics') is not null)
	BEGIN
		DROP TABLE dbo.StudentMetrics;
	END;

GO

IF (object_id('dbo.Year') is not null)
	BEGIN
		DROP TABLE dbo.Year
	END;

GO

IF (object_id('dbo.Gender') is not null)
	BEGIN
		DROP TABLE dbo.Gender;
	END;

GO

IF (object_id('dbo.Age') is not null)
	BEGIN
		DROP TABLE dbo.Age;
	END;

GO

IF (object_id('dbo.Ethnicity') is not null)
	BEGIN
		DROP TABLE dbo.Ethnicity;
	END;

GO

IF (object_id('dbo.Poverty') is not null)
	BEGIN
		DROP TABLE dbo.Poverty;
	END;

GO

IF (object_id('dbo.Demographic') is not null)
	BEGIN
		DROP TABLE dbo.Demographic;
	END;

GO

IF (object_id('dbo.RegionCollege') is not null)
	BEGIN
		DROP TABLE dbo.RegionCollege;
	END;

GO

IF (object_id('dbo.RegionDistrict') is not null)
	BEGIN
		DROP TABLE dbo.RegionDistrict;
	END;

GO

IF (object_id('dbo.RegionMicro') is not null)
	BEGIN
		DROP TABLE dbo.RegionMicro;
	END;

GO

IF (object_id('dbo.RegionMacro') is not null)
	BEGIN
		DROP TABLE dbo.RegionMacro;
	END;

GO

IF (object_id('dbo.RegionState') is not null)
	BEGIN
		DROP TABLE dbo.RegionState;
	END;

GO

IF (object_id('dbo.Region') is not null)
	BEGIN
		DROP TABLE dbo.Region;
	END;

GO

IF (object_id('dbo.ComisProgram') is not null)
	BEGIN
		DROP TABLE dbo.ComisProgram;
	END;

GO

IF (object_id('dbo.Program') is not null)
	BEGIN
		DROP TABLE dbo.Program;
	END;

GO

IF (object_id('dbo.Subdiscipline') is not null)
	BEGIN
		DROP TABLE dbo.Subdiscipline;
	END;

GO

IF (object_id('dbo.Sector') is not null)
	BEGIN
		DROP TABLE dbo.Sector;
	END;

GO

IF (object_id('dbo.Vocation') is not null)
	BEGIN
		DROP TABLE dbo.Vocation;
	END;

GO

IF (object_id('dbo.Content') is not null)
	BEGIN
		DROP TABLE dbo.Content;
	END;

GO

IF (object_id('MetricOutput') is not null)
	BEGIN
		DROP TABLE MetricOutput;
	END;

GO

IF (object_id('StudentSwirl') is not null)
	BEGIN
		DROP TABLE StudentSwirl;
	END;

GO

IF (object_id('StudentSwirlData') is not null)
	BEGIN
		DROP TABLE StudentSwirlData;
	END;

GO

IF (object_id('StudentSwirlSummary') is not null)
	BEGIN
		DROP TABLE StudentSwirlSummary;
	END;

GO

IF (object_id('MetricSwirlOutput') is not null)
	BEGIN
		DROP TABLE MetricSwirlOutput;
	END;

GO

IF (object_id('RegionCohort') is not null)
	BEGIN
		DROP TABLE RegionCohort;
	END;

GO

IF (object_id('Credit') is not null)
	BEGIN
		DROP TABLE Credit;
	END;

GO

IF (object_id('Attribute') is not null)
	BEGIN
		DROP TABLE Attribute;
	END;

GO

CREATE TABLE
	RegionCohort
	(
		RegionId  tinyint,
		RegionKey tinyint,
		CollegeId tinyint CONSTRAINT PK_RegionCohort primary key
	);

GO

CREATE TABLE
	StudentSwirl
	(
		StudentId int primary key
	);

GO

CREATE TABLE
	StudentSwirlData
	(
		YearId                tinyint  not null,
		StudentId             integer  not null,
		RegionId              tinyint  not null,
		RegionKey             tinyint  not null,
		ContentId             smallint not null,
		ContentKey            smallint not null,
		DemographicId         tinyint  not null,
		DemographicKey        tinyint  not null,
		CTE12Units            tinyint  not null,
		Disabled              tinyint      null,
		EconDis               tinyint  not null,
		Employed2Qtrs         tinyint      null,
		Employed4Qtrs         tinyint      null,
		EOPS                  tinyint      null,
		ESL                   tinyint  not null,
		Exiter                tinyint  not null,
		FinAid                tinyint      null,
		FirstGen              tinyint      null,
		FirstTime             tinyint  not null,
		Foster                tinyint      null,
		FullTime              tinyint      null,
		MathEngBS             tinyint  not null,
		MultiSchool           tinyint  not null,
		NC48Hours             tinyint  not null,
		NCTransAnyToAny       tinyint      null,
		NCTransAnyToSelect    tinyint      null,
		NCTransSelectToSelect tinyint      null,
		NonIntro              tinyint  not null,
		NoPrevHEAward         tinyint  not null,
		PartTime              tinyint      null,
		Persist               tinyint      null,
		PrevBach              tinyint  not null,
		PrevCCAward           tinyint  not null,
		PrevCTE               tinyint  not null,
		PrevHEAward           tinyint  not null,
		RegPersist            tinyint      null,
		SBWageGain            tinyint      null,
		SkillsBuilder         tinyint  not null,
		TermRegRet            tinyint      null,
		TermRet               tinyint      null,
		Transfer              tinyint  not null,
		Veteran               tinyint      null,
		CONSTRAINT
			PK_StudentSwirlData
		PRIMARY KEY CLUSTERED
			(
				YearId,
				StudentId,
				RegionId,
				ContentId,
				ContentKey,
				DemographicId,
				DemographicKey,
				RegionKey
			)
	);

GO

CREATE TABLE
	StudentSwirlSummary
	(
		YearId                 tinyint  not null,
		StudentId              integer  not null,
		ContentId              smallint not null,
		ContentKey             smallint not null,
		DemographicId          tinyint  not null,
		DemographicKey         tinyint  not null,
		CTE12UnitsN            tinyint  not null,
		CTE12UnitsD            tinyint  not null,
		DisabledN              tinyint  not null,
		DisabledD              tinyint  not null,
		EconDisN               tinyint  not null,
		EconDisD               tinyint  not null,
		EconAdvN               tinyint  not null,
		EconAdvD               tinyint  not null,
		Employed2QtrsTranN     tinyint  not null,
		Employed2QtrsTranD     tinyint  not null,
		Employed2QtrsSBN       tinyint  not null,
		Employed2QtrsSBD       tinyint  not null,
		Employed4QtrsTranN     tinyint  not null,
		Employed4QtrsTranD     tinyint  not null,
		Employed4QtrsSBN       tinyint  not null,
		Employed4QtrsSBD       tinyint  not null,
		EOPSN                  tinyint  not null,
		EOPSD                  tinyint  not null,
		ESLN                   tinyint  not null,
		ESLD                   tinyint  not null,
		ExiterN                tinyint  not null,
		ExiterD                tinyint  not null,
		FinAidN                tinyint  not null,
		FinAidD                tinyint  not null,
		FirstGenN              tinyint  not null,
		FirstGenD              tinyint  not null,
		FirstTimeN             tinyint  not null,
		FirstTimeD             tinyint  not null,
		FosterN                tinyint  not null,
		FosterD                tinyint  not null,
		FullTimeN              tinyint  not null,
		FullTimeD              tinyint  not null,
		MathEngBSN             tinyint  not null,
		MathEngBSD             tinyint  not null,
		MultiSchoolN           tinyint  not null,
		MultiSchoolD           tinyint  not null,
		NC48HoursN             tinyint  not null,
		NC48HoursD             tinyint  not null,
		NCTransAnyToAnyN       tinyint  not null,
		NCTransAnyToAnyD       tinyint  not null,
		NCTransAnyToSelectN    tinyint  not null,
		NCTransAnyToSelectD    tinyint  not null,
		NCTransSelectToSelectN tinyint  not null,
		NCTransSelectToSelectD tinyint  not null,
		NonIntroN              tinyint  not null,
		NonIntroD              tinyint  not null,
		NoPrevHEAwardN         tinyint  not null,
		NoPrevHEAwardD         tinyint  not null,
		PartTimeN              tinyint  not null,
		PartTimeD              tinyint  not null,
		PersistN               tinyint  not null,
		PersistD               tinyint  not null,
		PrevBachN              tinyint  not null,
		PrevBachD              tinyint  not null,
		PrevCCAwardN           tinyint  not null,
		PrevCCAwardD           tinyint  not null,
		PrevCTEN               tinyint  not null,
		PrevCTED               tinyint  not null,
		PrevHEAwardN           tinyint  not null,
		PrevHEAwardD           tinyint  not null,
		RegPersistN            tinyint  not null,
		RegPersistD            tinyint  not null,
		SBWageGainN            tinyint  not null,
		SBWageGainD            tinyint  not null,
		SkillsBuilderN         tinyint  not null,
		SkillsBuilderD         tinyint  not null,
		TermRegRetN            tinyint  not null,
		TermRegRetD            tinyint  not null,
		TermRetN               tinyint  not null,
		TermRetD               tinyint  not null,
		TransferN              tinyint  not null,
		TransferD              tinyint  not null,
		VeteranN               tinyint  not null,
		VeteranD               tinyint  not null,
		Generic                tinyint  not null,
		CONSTRAINT
			PK_StudentSwirlSummary
		PRIMARY KEY CLUSTERED
			(
				YearId,
				StudentId,
				ContentId,
				ContentKey,
				DemographicId,
				DemographicKey
			)
	);

GO

CREATE TABLE
	MetricSwirlOutput
	(
		YearId         tinyint  not null,
		RegionId       tinyint  not null,
		RegionKey      tinyint  not null,
		ContentId      smallint not null,
		ContentKey     smallint not null,
		DemographicId  tinyint  not null,
		DemographicKey tinyint  not null,
		Metric103b     integer  not null,
		Metric200bN    integer  not null,
		Metric200bD    integer  not null,
		Metric201bN    integer  not null,
		Metric201bD    integer  not null,
		Metric202bN    integer  not null,
		Metric202bD    integer  not null,
		Metric206bN    integer  not null,
		Metric206bD    integer  not null,
		Metric207bN    integer  not null,
		Metric207bD    integer  not null,
		Metric208bN    integer  not null,
		Metric208bD    integer  not null,
		Metric209bN    integer  not null,
		Metric209bD    integer  not null,
		Metric210bN    integer  not null,
		Metric210bD    integer  not null,
		Metric211bN    integer  not null,
		Metric211bD    integer  not null,
		Metric212bN    integer  not null,
		Metric212bD    integer  not null,
		Metric213bN    integer  not null,
		Metric213bD    integer  not null,
		Metric214bN    integer  not null,
		Metric214bD    integer  not null,
		Metric215bN    integer  not null,
		Metric215bD    integer  not null,
		Metric216bN    integer  not null,
		Metric216bD    integer  not null,
		Metric218bN    integer  not null,
		Metric218bD    integer  not null,
		Metric219bN    integer  not null,
		Metric219bD    integer  not null,
		Metric220bN    integer  not null,
		Metric220bD    integer  not null,
		Metric221bN    integer  not null,
		Metric221bD    integer  not null,
		Metric222bN    integer  not null,
		Metric222bD    integer  not null,
		Metric223bN    integer  not null,
		Metric223bD    integer  not null,
		Metric224bN    integer  not null,
		Metric224bD    integer  not null,
		Metric225bN    integer  not null,
		Metric225bD    integer  not null,
		Metric302bN    integer  not null,
		Metric302bD    integer  not null,
		Metric303bN    integer  not null,
		Metric303bD    integer  not null,
		Metric304bN    integer  not null,
		Metric304bD    integer  not null,
		Metric305bN    integer  not null,
		Metric305bD    integer  not null,
		Metric306bN    integer  not null,
		Metric306bD    integer  not null,
		Metric307bN    integer  not null,
		Metric307bD    integer  not null,
		Metric308bN    integer  not null,
		Metric308bD    integer  not null,
		Metric309bN    integer  not null,
		Metric309bD    integer  not null,
		Metric310bN    integer  not null,
		Metric310bD    integer  not null,
		Metric410bN    integer  not null,
		Metric410bD    integer  not null,
		Metric411bN    integer  not null,
		Metric411bD    integer  not null,
		Metric500btN   integer  not null,
		Metric500btD   integer  not null,
		Metric500bkN   integer  not null,
		Metric500bkD   integer  not null,
		Metric501btN   integer  not null,
		Metric501btD   integer  not null,
		Metric501bkN   integer  not null,
		Metric501bkD   integer  not null
		CONSTRAINT
			PK_MetricSwirlOutput
		PRIMARY KEY CLUSTERED
			(
				YearId,
				RegionId,
				RegionKey,
				ContentId,
				ContentKey,
				DemographicId,
				DemographicKey
			)
	);

GO

CREATE TABLE
	dbo.MetricOutput
	(
		YearId         tinyint  not null,
		RegionId       tinyint  not null,
		RegionKey      tinyint  not null,
		ContentId      smallint not null,
		ContentKey     smallint not null,
		DemographicId  tinyint  not null,
		DemographicKey tinyint  not null,
		Metric103b     integer  not null,
		Metric200bN    integer  not null,
		Metric200bD    integer  not null,
		Metric201bN    integer  not null,
		Metric201bD    integer  not null,
		Metric202bN    integer  not null,
		Metric202bD    integer  not null,
		Metric206bN    integer  not null,
		Metric206bD    integer  not null,
		Metric207bN    integer  not null,
		Metric207bD    integer  not null,
		Metric208bN    integer  not null,
		Metric208bD    integer  not null,
		Metric209bN    integer  not null,
		Metric209bD    integer  not null,
		Metric210bN    integer  not null,
		Metric210bD    integer  not null,
		Metric211bN    integer  not null,
		Metric211bD    integer  not null,
		Metric212bN    integer  not null,
		Metric212bD    integer  not null,
		Metric213bN    integer  not null,
		Metric213bD    integer  not null,
		Metric214bN    integer  not null,
		Metric214bD    integer  not null,
		Metric215bN    integer  not null,
		Metric215bD    integer  not null,
		Metric216bN    integer  not null,
		Metric216bD    integer  not null,
		Metric218bN    integer  not null,
		Metric218bD    integer  not null,
		Metric219bN    integer  not null,
		Metric219bD    integer  not null,
		Metric220bN    integer  not null,
		Metric220bD    integer  not null,
		Metric221bN    integer  not null,
		Metric221bD    integer  not null,
		Metric222bN    integer  not null,
		Metric222bD    integer  not null,
		Metric223bN    integer  not null,
		Metric223bD    integer  not null,
		Metric224bN    integer  not null,
		Metric224bD    integer  not null,
		Metric225bN    integer  not null,
		Metric225bD    integer  not null,
		Metric302bN    integer  not null,
		Metric302bD    integer  not null,
		Metric303bN    integer  not null,
		Metric303bD    integer  not null,
		Metric304bN    integer  not null,
		Metric304bD    integer  not null,
		Metric305bN    integer  not null,
		Metric305bD    integer  not null,
		Metric306bN    integer  not null,
		Metric306bD    integer  not null,
		Metric307bN    integer  not null,
		Metric307bD    integer  not null,
		Metric308bN    integer  not null,
		Metric308bD    integer  not null,
		Metric309bN    integer  not null,
		Metric309bD    integer  not null,
		Metric310bN    integer  not null,
		Metric310bD    integer  not null,
		Metric410bN    integer  not null,
		Metric410bD    integer  not null,
		Metric411bN    integer  not null,
		Metric411bD    integer  not null,
		Metric500btN   integer  not null,
		Metric500btD   integer  not null,
		Metric500bkN   integer  not null,
		Metric500bkD   integer  not null,
		Metric501btN   integer  not null,
		Metric501btD   integer  not null,
		Metric501bkN   integer  not null,
		Metric501bkD   integer  not null
		CONSTRAINT
			PK_MetricOutput
		PRIMARY KEY CLUSTERED
			(
				YearId,
				RegionId,
				RegionKey,
				ContentId,
				ContentKey,
				DemographicId,
				DemographicKey
			)
	);

GO

-- CREATE TABLE
-- 	dbo.StudentMetrics
-- 	(
-- 		YearId                  tinyint       not null,
-- 		StudentId               integer       not null,
-- 		RegionIdState           tinyint       not null,
-- 		RegionIdMacro           tinyint       not null,
-- 		RegionIdMicro           tinyint       not null,
-- 		RegionIdCollege         tinyint       not null,
-- 		ContentId               smallint      not null,
-- 		VocationId              smallint      not null,
-- 		SectorId                smallint      not null,
-- 		SubdisciplineId         smallint      not null,
-- 		ProgramId               smallint      not null,
-- 		DemographicId           tinyint       not null,
-- 		AgeId                   tinyint       not null,
-- 		EthnicityId             tinyint       not null,
-- 		GenderId                tinyint       not null,
-- 		PovertyId               tinyint       not null,
-- 		NonIntro                bit           not null,
-- 		SkillsBuilder           bit           not null,
-- 		Exiter                  bit           not null,
-- 		Foster                  bit           not null,
-- 		Veteran                 bit           not null,
-- 		Disabled                bit           not null,
-- 		FirstGen                bit           not null,
-- 		EconDis                 bit           not null,
-- 		FinAid                  bit           not null,
-- 		EOPS                    bit           not null,
-- 		MathEngBS               bit           not null,
-- 		ESL                     bit           not null,
-- 		FullTime                bit               null,
-- 		PartTime                bit               null,
-- 		FirstTime               bit           not null,
-- 		MultiSchool             bit           not null,
-- 		PrevHEAward             bit           not null,
-- 		NoPrevHEAward           bit           not null,
-- 		PrevCCAward             bit           not null,
-- 		PrevBach                bit           not null,
-- 		PrevCTE                 bit           not null,
-- 		TermRet                 bit               null,
-- 		TermRegRet              bit               null,
-- 		Persist                 bit               null,
-- 		RegPersist              bit               null,
-- 		NCTransAnyToSelect      bit               null,
-- 		NCTransAnyToAny         bit               null,
-- 		NCTransSelectToSelect   bit               null,
-- 		CTE12Units              bit           not null,
-- 		NC48Hours               bit           not null,
-- 		Transfer                bit           not null,
-- 		SBWageGain              bit               null,
-- 		Employed2Qtrs           bit               null,
-- 		Employed4Qtrs           bit               null,
-- 		Pre1QWage               decimal(19,2)     null,
-- 		Pre2QWage               decimal(19,2)     null,
-- 		Pre3QWage               decimal(19,2)     null,
-- 		Pre4QWage               decimal(19,2)     null,
-- 		Post1QWage              decimal(19,2)     null,
-- 		Post2QWage              decimal(19,2)     null,
-- 		Post3QWage              decimal(19,2)     null,
-- 		Post4QWage              decimal(19,2)     null,
-- 		PreWage                 decimal(19,2)     null,
-- 		PostWage                decimal(19,2)     null,
-- 		WageDiff                decimal(19,2)     null,
-- 		AboveSingleMicro        bit               null,
-- 		AboveSingleMacro        bit               null,
-- 		CONSTRAINT
-- 			PK_StudentMetrics
-- 		PRIMARY KEY NONCLUSTERED
-- 			(
-- 				YearId,
-- 				RegionIdCollege,
-- 				StudentId,
-- 				ProgramId
-- 			)
-- 	)
-- WITH
-- 	(
-- 		MEMORY_OPTIMIZED = ON
-- 	);

CREATE TABLE
	dbo.StudentMetrics
	(
		YearId                  tinyint       not null,
		StudentId               integer       not null,
		RegionIdState           tinyint       not null,
		RegionIdMacro           tinyint       not null,
		RegionIdMicro           tinyint       not null,
		RegionIdDistrict        tinyint       not null,
		RegionIdCollege         tinyint       not null,
		ContentId               smallint      not null,
		VocationId              smallint      not null,
		SectorId                smallint      not null,
		SubdisciplineId         smallint      not null,
		ProgramId               smallint      not null,
		AttributeId             tinyint       not null,
		CreditId                tinyint       not null,
		DemographicId           tinyint       not null,
		AgeIdCollege            tinyint       not null,
		AgeIdDistrict           tinyint       not null,
		AgeIdMacro              tinyint       not null,
		AgeIdMicro              tinyint       not null,
		AgeIdState              tinyint       not null,
		EthnicityIdCollege      tinyint       not null,
		EthnicityIdDistrict     tinyint       not null,
		EthnicityIdMacro        tinyint       not null,
		EthnicityIdMicro        tinyint       not null,
		EthnicityIdState        tinyint       not null,
		GenderIdCollege         tinyint       not null,
		GenderIdDistrict        tinyint       not null,
		GenderIdMacro           tinyint       not null,
		GenderIdMicro           tinyint       not null,
		GenderIdState           tinyint       not null,
		PovertyId               tinyint       not null,
		SkillsBuilder           bit           not null,
		Exiter                  bit           not null,
		EconDis                 bit           not null,
		Transfer                bit           not null,
		CTE9UnitsDist           bit           not null,
		CTENonCredit            bit           not null,
		LivingWage              bit               null,
		CONSTRAINT
			PK_StudentMetrics
		PRIMARY KEY NONCLUSTERED
			(
				YearId,
				RegionIdCollege,
				StudentId,
				ProgramId,
				CreditId
			),
		CONSTRAINT
			AK_StudentMetrics
		UNIQUE
			(
				StudentId,
				YearId,
				RegionIdCollege,
				ProgramId,
				CreditId
			)
	)
WITH
	(
		MEMORY_OPTIMIZED = ON
	);

GO

CREATE TABLE
	dbo.Student
	(
		StudentId       INT        NOT NULL IDENTITY(1,1),
		student_id      BINARY(64) NOT NULL
		CONSTRAINT
			PK_Student
		PRIMARY KEY CLUSTERED
			(
				StudentId),
		CONSTRAINT
			AK_Student_student_id
		UNIQUE
			(
				student_id
			)
	);

GO

CREATE TABLE
	dbo.Year
	(
		YearId       tinyint not null identity(1,1),
		YearCode     char(3) not null,
		YearCodeNext char(3) not null,
		AcademicYear char(9) not null,
		YearStart    char(4) not null,
		YearEnd      char(4) not null
	);

GO

ALTER TABLE
	dbo.Year
ADD CONSTRAINT
	PK_Year
PRIMARY KEY CLUSTERED
	(
		YearId
	);

GO

ALTER TABLE
	dbo.Year
ADD CONSTRAINT
	UQ_Year__YearCode
UNIQUE
	(
		YearCode
	);

GO

INSERT INTO
	dbo.Year
	(
		YearCode,
		YearCodeNext,
		AcademicYear,
		YearStart,
		YearEnd
	)
VALUES
	('120','130','2011-2012','2011','2012'),
	('130','140','2012-2013','2012','2013'),
	('140','150','2013-2014','2013','2014'),
	('150','160','2014-2015','2014','2015'),
	('160','170','2015-2016','2015','2016'),
	('170','180','2016-2017','2016','2017'),
	('180','190','2017-2018','2017','2018');

ALTER TABLE
	dbo.Year
ADD
	BeginDate date;

ALTER TABLE
	dbo.Year
ADD
	EndDate date;

GO

UPDATE
	y
SET
	BeginDate = convert(date, substring(AcademicYear, 1, 4) + '-07-01'),
	EndDate   = convert(date, substring(AcademicYear, 6, 4) + '-06-30')
FROM
	dbo.Year y

GO

ALTER TABLE
	dbo.Year
ALTER COLUMN
	BeginDate date not null;

ALTER TABLE
	dbo.Year
ALTER COLUMN
	EndDate date not null;

GO

CREATE TABLE
	dbo.Demographic
	(
		DemographicId tinyint     not null identity(1,1),
		Label         varchar(25) not null,
		Identifier    sysname     not null
	);

GO

ALTER TABLE
	dbo.Demographic
ADD CONSTRAINT
	PK_Demographic
PRIMARY KEY CLUSTERED
	(
		DemographicId
	);

ALTER TABLE
	dbo.Demographic
ADD CONSTRAINT
	UQ_Demographic__Label
UNIQUE
	(
		Label
	);

GO

INSERT
	dbo.Demographic
	(
		Label,
		Identifier
	)
VALUES
	('All', 'DemographicId'),
	('Gender','GenderId'),
	('Age','AgeId'),
	('Ethnicity','EthnicityId'),
	('Poverty', 'PovertyId');

GO
CREATE TABLE
	dbo.Gender
	(
		GenderId      tinyint     not null identity(1,1),
		DemographicId tinyint     not null,
		GenderCode    char(1)     not null,
		Label         varchar(25) not null
	)

GO

ALTER TABLE
	dbo.Gender
ADD CONSTRAINT
	PK_Gender
PRIMARY KEY CLUSTERED
	(
		GenderId
	);

ALTER TABLE
	dbo.Gender
ADD CONSTRAINT
	UQ_Gender__GenderCode
UNIQUE
	(
		GenderCode
	);

ALTER TABLE
	dbo.Gender
ADD CONSTRAINT
	UQ_Gender__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	dbo.Gender
ADD CONSTRAINT
	FK_Gender__DemographicId
FOREIGN KEY
	(
		DemographicId
	)
REFERENCES
	dbo.Demographic
	(
		DemographicId
	);

CREATE NONCLUSTERED INDEX
	IX_Gender__DemographicId
ON
	dbo.Gender
	(
		DemographicId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	dbo.Gender
	(
		DemographicId,
		GenderCode,
		Label
	)
VALUES
	(2,'F','Female'),
	(2,'M','Male'),
	(2,'X','Unknown/Non-Respondent'),
	(2,'U','Multiple Values Reported');

GO

CREATE TABLE
	dbo.Age
	(
		AgeId         tinyint     not null identity(1,1),
		DemographicId tinyint     not null,
		Label         varchar(25) not null,
		LowerBound    tinyint         null,
		UpperBound    tinyint         null
	);

GO

ALTER TABLE
	dbo.Age
ADD CONSTRAINT
	PK_Age
PRIMARY KEY CLUSTERED
	(
		AgeId
	);

ALTER TABLE
	dbo.Age
ADD CONSTRAINT
	UQ_Age__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	dbo.Age
ADD CONSTRAINT
	UQ_Age__LowerBound
UNIQUE
	(
		LowerBound
	);

ALTER TABLE
	dbo.Age
ADD CONSTRAINT
	UQ_Age__UpperBound
UNIQUE
	(
		UpperBound
	);

ALTER TABLE
	dbo.Age
ADD CONSTRAINT
	FK_Age__DemographicId
FOREIGN KEY
	(
		DemographicId
	)
REFERENCES
	dbo.Demographic
	(
		DemographicId
	);

CREATE NONCLUSTERED INDEX
	IX_Age__DemographicId
ON
	dbo.Age
	(
		DemographicId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	dbo.Age
	(
		DemographicId,
		LowerBound,
		UpperBound,
		Label
	)
VALUES
	(3,0,   19,  'Less than 20'),
	(3,20,  24,  '20-24'),
	(3,25,  39,  '25-39'),
	(3,40,  54,  '40-54'),
	(3,55,  255, '55 and Older'),
	(3,null,null,'Multiple Values Reported');

GO

CREATE TABLE
	dbo.Ethnicity
	(
		EthnicityId   tinyint     not null identity(1,1),
		DemographicId tinyint     not null,
		EthnicityCode char(1)     not null,
		Label         varchar(45) not null
	)

GO

ALTER TABLE
	dbo.Ethnicity
ADD CONSTRAINT
	PK_Ethnicity
PRIMARY KEY CLUSTERED
	(
		EthnicityId
	);

ALTER TABLE
	dbo.Ethnicity
ADD CONSTRAINT
	UQ_Ethnicity__EthnicityCode
UNIQUE
	(
		EthnicityCode
	);

ALTER TABLE
	dbo.Ethnicity
ADD CONSTRAINT
	FK_Ethnicity__DemographicId
FOREIGN KEY
	(
		DemographicId
	)
REFERENCES
	dbo.Demographic
	(
		DemographicId
	);

CREATE NONCLUSTERED INDEX
	IX_Ethnicity__DemographicId
ON
	dbo.Ethnicity
	(
		DemographicId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	dbo.Ethnicity
	(
		DemographicId,
		EthnicityCode,
		Label
	)
VALUES
	(4,'A','Asian'),
	(4,'B','Black or African American'),
	(4,'F','Filipino'),
	(4,'H','Hispanic'),
	(4,'N','American Indian/Alaska Native'),
	(4,'P','Native Hawaiian or Other Pacific Islander'),
	(4,'W','White'),
	(4,'T','Two or More Races'),
	(4,'X','Unknown/Non-Respondent'),
	(4,'U','Multiple Values Reported');


GO

CREATE TABLE
	dbo.Poverty
	(
		PovertyId       tinyint     not null identity(1,1),
		DemographicId   tinyint     not null,
		Label           varchar(30) not null,
		IsDisadvantaged bit         not null
	)

GO

ALTER TABLE
	dbo.Poverty
ADD CONSTRAINT
	PK_Poverty
PRIMARY KEY CLUSTERED
	(
		PovertyId
	);

ALTER TABLE
	dbo.Poverty
ADD CONSTRAINT
	FK_Poverty__DemographicId
FOREIGN KEY
	(
		DemographicId
	)
REFERENCES
	dbo.Demographic
	(
		DemographicId
	);

CREATE NONCLUSTERED INDEX
	IX_Poverty__DemographicId
ON
	dbo.Poverty
	(
		DemographicId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	dbo.Poverty
	(
		DemographicId,
		Label,
		IsDisadvantaged
	)
VALUES
	(5,'Economically Disadvantaged', 1),
	(5,'Not Economically Disadvantaged',0);

GO

CREATE TABLE
	dbo.Content
	(
		ContentId  smallint    not null identity(1,1),
		Label      varchar(15) not null,
		Identifier sysname     not null
	);

GO

ALTER TABLE
	dbo.Content
ADD CONSTRAINT
	PK_Content
PRIMARY KEY CLUSTERED
	(
		ContentId
	);

ALTER TABLE
	dbo.Content
ADD CONSTRAINT
	UQ_Content__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	dbo.Content
ADD CONSTRAINT
	UQ_Content__Identifier
UNIQUE
	(
		Identifier
	);

GO

INSERT
	dbo.Content
	(
		Label,
		Identifier
	)
VALUES
	('All', 'ContentId'),
	('Vocation', 'VocationId'),
	('Sector', 'SectorId'),
	('Subdiscipline', 'SubdisciplineId'),
	('Program', 'ProgramId');

GO

CREATE TABLE
	dbo.Vocation
	(
		VocationId smallint    not null identity(1,1),
		ContentId  smallint    not null,
		Label      varchar(15) not null
	);

GO

ALTER TABLE
	dbo.Vocation
ADD CONSTRAINT
	PK_Vocation
PRIMARY KEY CLUSTERED
	(
		VocationId
	);

ALTER TABLE
	dbo.Vocation
ADD CONSTRAINT
	UQ_Vocation__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	dbo.Vocation
ADD CONSTRAINT
	FK_Vocation__ContentId
FOREIGN KEY
	(
		ContentId
	)
REFERENCES
	dbo.Content
	(
		ContentId
	);

CREATE NONCLUSTERED INDEX
	IX_Vocation__ContentId
ON
	dbo.Vocation
	(
		ContentId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	dbo.Vocation
	(
		ContentId,
		Label
	)
VALUES
	(2,'Vocational'),
	(2,'Non-Vocational');

GO

CREATE TABLE
	dbo.Sector
	(
		SectorId  smallint    not null identity(1,1),
		ContentId smallint    not null,
		Label     varchar(65) not null
	)

GO

ALTER TABLE
	dbo.Sector
ADD CONSTRAINT
	PK_Sector
PRIMARY KEY CLUSTERED
	(
		SectorId
	);

ALTER TABLE
	dbo.Sector
ADD CONSTRAINT
	UQ_Sector__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	dbo.Sector
ADD CONSTRAINT
	FK_Sector__ContentId
FOREIGN KEY
	(
		ContentId
	)
REFERENCES
	dbo.Content
	(
		ContentId
	);

CREATE NONCLUSTERED INDEX
	IX_Sector__ContentId
ON
	dbo.Sector
	(
		ContentId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	dbo.Sector
	(
		ContentId,
		Label
	)
VALUES
	(3,'Advanced Manufacturing'),
	(3,'Advanced Transportation and Logistics'),
	(3,'Agriculture, Water and Environmental Technologies'),
	(3,'Business and Entrepreneurship'),
	(3,'Education and Human Development'),
	(3,'Energy, Construction and Utilities'),
	(3,'Global Trade'),
	(3,'Health'),
	(3,'Information and Communication Technologies (ICT) - Digital Media'),
	(3,'Life Sciences - Biotechnology'),
	(3,'Public Safety'),
	(3,'Retail, Hospitality and Tourism'),
	(3,'Unassigned'),
	(3,'None');

GO

CREATE TABLE
	dbo.ComisProgram
	(
		ProgramCode       char(6),
		DisciplineCode    char(2),
		SubdisciplineCode char(2),
		FieldCode         char(2),
		Name              varchar(255),
		Description       varchar(1024),
		IsVocational      bit,
		SubjectCode       varchar(4),
		CONSTRAINT
			pk_Program__ProgramCode
		PRIMARY KEY CLUSTERED
			(
				ProgramCode
			),
		INDEX
			ix_Program__DisciplineCode
		NONCLUSTERED
			(
				DisciplineCode
			),
		INDEX
			ix_Program__SubdisciplineCode
		NONCLUSTERED
			(
				DisciplineCode
			),
		INDEX
			ix_Program__IsVocational
		NONCLUSTERED
			(
				IsVocational
			),
		INDEX
			ix_Program__SubjectCode
		NONCLUSTERED
			(
				SubjectCode
			)
	);

INSERT INTO
	dbo.ComisProgram
	(
		ProgramCode,
		DisciplineCode,
		SubdisciplineCode,
		FieldCode,
		Name,
		Description,
		IsVocational,
		SubjectCode
	)
SELECT
	a.ProgramCode,
	a.DisciplineCode,
	a.SubdisciplineCode,
	a.FieldCode,
	a.Name,
	a.Description,
	IsVocational = case when b.ProgramCode is not null then 1 else 0 end,
	a.SubjectCode
FROM
	(
		VALUES
		('010000','01','00','00','Agriculture and Natural Resources','Instructional programs that prepare individuals to apply scientific knowledge andmethods, and technical skills, to support agribusiness and agricultural activities, such as management, production and propagation, supplies and services, mechanics,marketing, and horticulture.',0,null),
		('010100','01','01','00','Agriculture Technology and Sciences, General','Economic and business principles used in the organization, operation, and management of farm and agricultural businesses',1,null),
		('010200','01','02','00','Animal Science','Operation of animal production enterprises by developing competencies in the selection, breeding, physiology, nutrition, health, housing, feeding, and marketing of animals.',1,null),
		('010300','01','03','00','Plant Science','Theories, principles, and practices involved in the production and management of plants for food, feed, fiber, and soil conservation.',1,null),
		('010400','01','04','00','Viticulture, Enology, and Wine Business','Programs relating to cultivation of wine grapes, and making and marketing of wine.',1,null),
		('010900','01','09','00','Horticulture','Production of flowers, foliage, and related plant materials in fields and greenhouses for garden and ornamental purposes.',1,null),
		('011200','01','12','00','Agriculture Business, Sales, and Service','Principles and application of technical skills that apply to purchasing, storing, inspecting, marketing, and selling products from agricultural suppliers.',1,null),
		('011300','01','13','00','Food Processing and Related Technologies','Application of chemical, physical, and engineering principles to the manufacturing, packaging, storage and distribution of food products.',1,null),
		('011400','01','14','00','Forestry','Science, art, and practices of managing, using, and protecting forest lands and public and private timberlands.',1,null),
		('011500','01','15','00','Natural Resources','Theories, principles, laws and regulations, and the application of skills to the use, management, and conservation of renewable natural resources, including wildlife and fisheries.',1,null),
		('011600','01','16','00','Agricultural Power Equipment Technology','Theory and technical skills that apply to the selection, operation, servicing, maintenance, and repair of a variety of agricultural and forestry machinery and equipment.',1,null),
		('019900','01','99','00','Other Agriculture and Natural Resources','Specify (includes all emerging technologies).',1,null),
		('010210','01','02','10','Veterinary Technician (Licensed)','Laws and regulations, principles and practices, and licensure requirements that prepare individuals to assist the veterinarian, biological researcher or other scientist in applying knowledge of veterinary medical assisting procedures and techniques.',1,null),
		('010220','01','02','20','Artificial Inseminator (Licensed)','Laws and regulations, principles and practices, and licensure requirements ofartificial inseminators.',1,null),
		('010230','01','02','30','Dairy Science','Care of dairy animals and associated dairy farm and processing facilities and operations.',1,null),
		('010240','01','02','40','Equine Science','Programs related to horse breeding, management, training, and riding.',1,null),
		('010310','01','03','10','Agricultural Pest Control Advisor and Operator (Licensed)','Laws and regulations, principles and practices, and licensure requirements pertaining to pest control advisors and operators.',1,null),
		('010910','01','09','10','Landscape Design and Maintenance','Design, maintenance, and management of landscape areas, including residential, commercial, parks, and golf courses.',1,null),
		('010920','01','09','20','Floriculture/Floristry','Prepares individuals to produce flowers and related foliage, and to design floral arrangements and decorations. Includes instruction in selecting appropriate containers, flowers, and accessories for special occasions and events.',1,null),
		('010930','01','09','30','Nursery Technology','Programs related to plant nursery operations and management.',1,null),
		('010940','01','09','40','Turfgrass Technology','Programs related to installation, cultivation, and maintenance of turfgrass for golf courses and other purposes.',1,null),
		('011510','01','15','10','Parks and Outdoor Recreation','Describes the significance and operation of recreational entities and facilities, such as parks and outdoor activities.',1,null),
		('011520','01','15','20','Wildlife and Fisheries','Conservation and management of wilderness fauna and wildlife reservations, and of fish and shellfish resources for recreational or commercial purposes.',1,null),
		('020000','02','00','00','Architecture and Related Technologies','Instructional programs that prepare individuals to assist in architectural tasks, including the creation, adoption, alteration, preservation, and control of physical and social surroundings.',0,null),
		('020100','02','01','00','Architecture and Architectural Technology','Planning, organization, and enclosure of space for functional and esthetic purposes, including the design of structures, testing of materials, estimating, environmental impact studies, and dealing with contracts and specifications.',1,null),
		('029900','02','99','00','Other Architecture and Environmental Design','Specify (includes all emerging occupations)',1,null),
		('020110','02','01','10','Landscape Architecture (transfer)',null,0,null),
		('030000','03','00','00','Environmental Sciences and Technologies','Instructional programs that study biological and physical aspects of the environment and environment-related issues, including methods of controlling environmental pollution; and programs that prepare individuals to monitor and maintain environmental pollution control systems and to manage hazardous wastes.',0,null),
		('030100','03','01','00','Environmental Science','Study of the biological, chemical, and physical aspects of the environment, including methods of preventing environmental pollution and damage, and preventing animal and plant extinction.',0,null),
		('030200','03','02','00','Environmental Studies','Study of environment-related issues, including policy and legislation, social, legal, and economic aspects, and scientific principles of the ecosystem and environmental conservation.',0,null),
		('030300','03','03','00','Environmental Technology','Environmental management, monitoring, assessment, and restoration, including environmental pollution control systems and the management of hazardous materials and hazardous waste, and related government regulations.',1,null),
		('039900','03','99','00','Environmental Sciences and Technologies, Other','Specify (includes all emerging occupations).',0,null),
		('040000','04','00','00','Biological Sciences','Instructional programs that study life or living matter in all its forms and phenomena, especially with regard to the origin, function, growth, reproduction, heredity, and structure of life forms.',0,null),
		('040100','04','01','00','Biology, General','A generalized study of life, including the structure, function, reproduction, growth, heredity, evolution, behavior, and distribution of living organisms.',0,null),
		('040200','04','02','00','Botany, General','Plant life, structure, function, reproduction, growth, heredity, evolution, and distribution.',0,null),
		('040300','04','03','00','Microbiology','Study of microorganisms, including their morphology, physiology, metabolism, growth, and effects upon substances and other organisms.',0,null),
		('040700','04','07','00','Zoology, General','Animals, including their structure, function, reproduction, growth, heredity, evolution, behavior, and distribution.',0,null),
		('040800','04','08','00','Natural History','A broad survey of life forms and natural systems, and their evolutionary and environmental relationships.',0,null),
		('041000','04','10','00','Anatomy and Physiology','The cellular mechanisms underlying the life processes, functions of various parts of living organisms, and of integrated physiological response to the environments in which they live.',0,null),
		('043000','04','30','00','Biotechnology and Biomedical Technology','Theories, operations, and technical skills used to assist researchers and engineers engaged in developing or manufacturing biological, biotechnical, or medical systems or products.',1,null),
		('049900','04','99','00','Other Biological Sciences','Specify.',0,null),
		('050000','05','00','00','Business and Management','Instructional programs that prepare individuals for a variety of activities in planning, organizing, directing, and managing all business office systems and procedures.',0,null),
		('050100','05','01','00','Business and Commerce, General','Processes, principles, and procedures of purchasing, selling, producing, and interchanging goods, commodities, and services to prepare a person for a position of responsibility, management, and/or ownership.',1,null),
		('050200','05','02','00','Accounting','Procedures to systematize information about transactions and activities into accounts and quantitative reports to verify accuracy of data by applying accounting, internal reporting, and decision making principles. Includes accounting and financial reporting that assists in making internal management decisions',1,null),
		('050400','05','04','00','Banking and Finance','Financial sectors of the general economy to prepare individuals to engage in financial or banking services. Includes bank management, investments, and loan analysis and management.',1,null),
		('050500','05','05','00','Business Administration','Programs designed to give a broad, balanced introduction to professional careers in business, usually including business law, economics, mathematics, managerial accounting and computer systems. Includes transfer programs.',1,null),
		('050600','05','06','00','Business Management','Planning, organizing, directing, and controlling business operations. Includes various theories, tools, and practical applications used to maintain business sustainability through the management of capital, financial, and human resources',1,null),
		('050800','05','08','00','International Business and Trade','Principles of managing a business in an international context, and of exporting or importing of industrial or consumer goods in world markets. Includes trade regulations and controls, foreign trade operations, locating markets, negotiation practices, monetary issues, and international law and public relations.',1,null),
		('050900','05','09','00','Marketing and Distribution','Marketing functions and tasks that facilitate the flow of goods and services to customers and/or ultimate consumers.',1,null),
		('051000','05','10','00','Logistics and Materials Transportation','Theory, principles, functions, and procedures for the orderly and economic receiving, manufacturing, shipping, and servicing of products or services.',1,null),
		('051100','05','11','00','Real Estate','Theory and techniques of buying, selling, appraising, renting, managing, and leasing real property. Includes marketing, financing government regulations, and legal aspects of real estate and land economics.',1,null),
		('051200','05','12','00','Insurance','Risk analysis and personal and/or business insurance and their application in such things as life, disability, property, liability, and fiduciary trust and annuity underwriting.',1,null),
		('051400','05','14','00','Office Technology/Office Computer Applications','Recording and disseminating of information, by manual and/or electronic means, including administrative office practices (keyboarding, computer literacy/applications, internet usage, e-mailing, scheduling, etc.), global concepts, and office management skills (problem solving, critical thinking, and interpersonal relations).',1,null),
		('051600','05','16','00','Labor and Industrial Relations','Describes the history and development of the labor movement, including the analysis and interpretation of federal and state regulations, union contracts, labor negotiations, conciliation, arbitration, and grievance procedures.',1,null),
		('051800','05','18','00','Customer Service','Preparation for customer service representative, call center technician, and related occupations.',1,null),
		('059900','05','99','00','Other Business and Management','Specify (includes all emerging occupations).',1,null),
		('050210','05','02','10','Tax Studies','Tax preparation and tax management services for individuals and businesses, including individual and business income tax preparation, and tax planning.',1,null),
		('050630','05','06','30','Management Development and Supervision','Supervising employees; budgeting, analysis, and coordinating clerical activities; evaluating, organizing, and revising office operations; design of facilities to provide maximum production; evaluating employee records; and coordinating activities of clerical departments and workers, dispute resolution, and mediation.',1,null),
		('050640','05','06','40','Small Business and Entrepreneurship','Principles, practices, and strategies of small business wholesale, retail, or service operations for owners/managers, and marketing principles and methods applicable to developing businesses.',1,null),
		('050650','05','06','50','Retail Store Operations and Management','Principles and methods of retail store operations and management, including department stores and supermarkets.',1,null),
		('050910','05','09','10','Advertising','Describes the creation, execution, transmission, and evaluation of commercial messages concerned with the promotion and sale of products and services.',1,null),
		('050920','05','09','20','Purchasing','The purchase of machinery, raw materials, and product components for manufacturing firms; office supplies, furniture, and business machines for a place of business; or the supplies and equipment needed to conduct a retail or service business.',1,null),
		('050940','05','09','40','Sales and Salesmanship','Sales functions and tasks generally applicable to any marketing environment, including retailing, sales, and customer service.',1,null),
		('050960','05','09','60','Display','Creation of products or institutional displays and exhibits for the purpose of stimulating sales and goodwill.',1,null),
		('050970','05','09','70','E-Commerce (business emphasis)','Programs that combine marketing and management principles with technical applications of the Internet and World Wide Web, with main emphasis on business principles.',1,null),
		('051110','05','11','10','Escrow','Principles and methods of real estate escrow and title company operations.',1,null),
		('051410','05','14','10','Legal Office Technology','Preparation of legal papers and correspondence by manual and/or electronic means. Includes legal terminology, procedures, and documents.',1,null),
		('051420','05','14','20','Medical Office Technology','Prepares individuals to perform medical office administrative duties by manual and/or electronic means. Includes a knowledge of medical terminology, as well as hospital, clinic, or laboratory procedures, and compiling and maintaining medical records.',1,null),
		('051430','05','14','30','Court Reporting','Prepares individuals to record court testimony or other proceedings by machine shorthand. Prepares also for closed captioning and scoping.',1,null),
		('051440','05','14','40','Office Management','Preparation to supervise and manage operations and personnel of business offices, including supervision, budgeting, scheduling, office systems and records.',1,null),
		('060000','06','00','00','Media and Communications','Instructional programs that study the theory, principles and methods of creation,transmission, reception and evaluation of various media.',0,null),
		('060100','06','01','00','Media and Communications, General','Programs including combinations or overviews of the media arts and technologies categories included in this discipline. ',0,null),
		('060200','06','02','00','Journalism','The gathering, processing, evaluation, and dissemination of information concerning current events and issues through the mass media. Origination and preparation of materials is practiced.',1,null),
		('060300','06','03','00','Radio and Television','History, theories, principles, techniques, functions, technologies, and creative processes of radio and television (including combined television/film/video programs) in reaching mass audiences',1,null),
		('060400','06','04','00','Radio and Television','History, theories, principles, techniques, functions, technologies, and creative processes of radio and television (including combined television/film/video programs) in reaching mass audiences',1,null),
		('060310','06','03','10','Radio','History, theories, principles, techniques, functions, technologies, and creative processes of radio.',1,null),
		('060410','06','04','10','Radio','History, theories, principles, techniques, functions, technologies, and creative processes of radio.',1,null),
		('060320','06','03','20','Television (including combined TV/film/video)','History, theories, principles, techniques, functions, technologies and creative processes of television and video. Includes programs that combine television with film and/or video.',1,null),
		('060420','06','04','20','Television (including combined TV/film/video)','History, theories, principles, techniques, functions, technologies and creative processes of television and video. Includes programs that combine television with film and/or video.',1,null),
		('060330','06','03','30','Broadcast Journalism','Theory and techniques of gathering and reporting news specifically for electronic media such as radio and television.',1,null),
		('060430','06','04','30','Broadcast Journalism','Theory and techniques of gathering and reporting news specifically for electronic media such as radio and television.',1,null),
		('060600','06','06','00','Public Relations','Surveying and research in PR, issues PR and news media collaboration, writing for PR, media in PR, speech writing and PR campaigns, data collection and analysis, logic and critical thinking in PR, psychology in critical thinking, public speaking and presentation skills, legal issues in PR, and ethics.',1,null),
		('060700','06','07','00','Technical Communication','Theory, methods, and skills for writing and producing scientific, technical, and business communications and documentation',1,null),
		('061000','06','10','00','Mass Communications','Study of the media by which entertainment and information messages are delivered, techniques used in such media, and social effects of such messages.',1,null),
		('061200','06','12','00','Film Studies (including combined film/video)','History, development, theory, and criticism of the film/video arts, as well as principles of film making and film production.',0,null),
		('061400','06','14','00','Digital Media','A broad range of programs that combine computer and other electronic technologies with skills and techniques from various fine arts and communications disciplines.',0,null),
		('061210','06','12','10','Film History and Criticism','History, development, theory, and criticism of the film/video arts.',0,null),
		('061220','06','12','20','Film Production','Communication of dramatic information, ideas, moods, and feelings through films and videos. Includes film technology and equipment, directing, editing, planning and management of film/video operations.',1,null),
		('061410','06','14','10','Multimedia','Principles and techniques of using computers to bring together text, sounds, animation, graphic art, and video to create interactive products to inform, educate, or entertain.',1,null),
		('061420','06','14','20','Electronic Game Design',null,1,null),
		('061430','06','14','30','Website Design and Development','Principles of design, user interface/navigation, graphics applications and other authoring tools to design, edit and publish web pages, documents, images, graphics, sound and multimedia products for the Internet.',1,null),
		('061440','06','14','40','Animation','Principles and techniques for creating the visual illusion of motion through sequenced images. Includes animation using digital technology',1,null),
		('061450','06','14','50','Desktop Publishing','Methods of preparing text and images for presentation to readers, using computerized electronic page layout and publication programs.',1,null),
		('061460','06','14','60','Computer Graphics and Digital Imagery','Theories, principles, and uses of computer graphics vector- and raster-based software programs for consumer, commercial, and industrial applications.',1,null),
		('069900','06','99','00','Other Media and Communications','Specify (includes all emerging occupations).',1,null),
		('070000','07','00','00','Information Technology','Instructional programs in the theories, principles, and methods of design, development and application of computer capabilities to data storage and manipulation.',0,null),
		('070100','07','01','00','Information Technology, General','Information technology concepts, theories, principles, methods and related computer capabilities and applications related to business, technical, and scientific problems',1,null),
		('070200','07','02','00','Computer Information Systems','General programs in data and information storage and processing, including hardware, software, basic design principles, and user requirements.',1,null),
		('070420','07','04','20','Unknown','Unknown',0,null),
		('070600','07','06','00','Computer Science (transfer)','Scientific and mathematical principles used in designing and building computers and computing systems, including transfer-oriented programs.',0,null),
		('070700','07','07','00','Computer Software Development','Design and development of computer-based applications. Includes systems analysis, design, specification, programming, database analysis and design, user interface development, maintenance, and testing.',1,null),
		('070800','07','08','00','Computer Infrastructure and Support','Network and operation systems design and administration, including certification preparation.',1,null),
		('070900','07','09','00','World Wide Web Administration','Methods to develop and maintain web servers and hosted web pages, and to function as Webmaster. Includes computer systems and networks, servers, web design and editing, information resources management, web policies and security.',1,null),
		('079900','07','99','00','Other Information Technology','Specify (includes all emerging occupations).',1,null),
		('070210','07','02','10','Software Applications','Computer application software used in a business or home environment ranging from an end user skill level to corporate level for the management of information. Concepts, theory, application of software for the design and development, distribution, publishing, presentation, and analysis of text, numeric, and graphic data. Procedures, information, and application of software including a variety of methods for inputting and retrieving data, records, and information. Application of software and computer-related tools such as e-mail and speech/voice recognition for the global dissemination of information.',1,null),
		('070710','07','07','10','Computer Programming','Entry-level programming, including methods, procedures, symbols and rules used in planning and writing instructions in computer language for the solution of a problem. Includes programming for the World Wide Web.',1,null),
		('070720','07','07','20','Database Design and Administration','Development of database applications in a business or organizational environment, including database architecture, programming languages, proprietary database software, and related skills and techniques.',1,null),
		('070730','07','07','30','Computer Systems Analysis','Systems analysis and design, including the recognition, definition, and improvement of processes through the use of computer technology and methodologies.',1,null),
		('070810','07','08','10','Computer Networking','Principles of local, metropolitan, and wide area computer networking design, installation, maintenance and troubleshooting.',1,null),
		('070820','07','08','20','Computer Support','Preparation to provide technical assistance to computer system users. May include use of computer hardware and software, printing, installation, word processing, electronic mail, and operating systems.',1,null),
		('070910','07','09','10','E-Commerce (technology emphasis)','Programs that combine marketing and management principles with technical applications of the Internet and World Wide Web, with main emphasis on applications of technology.',1,null),
		('080000','08','00','00','Education','Instructional programs that describe the science and art of importing knowledge, developing the powers of reasoning and judgment, and preparing others intellectually for a more mature and rewarding life.',0,null),
		('080100','08','01','00','Education, General (Pre-Professional) (Transfer)','Theory and method related to elementary, secondary and postsecondary education at the lower-division level.',0,null),
		('080200','08','02','00','Educational Aide (Teacher Assistant)','Practices and techniques necessary for preparing individuals to provide services to students and parents under the direction of professional staff.',1,null),
		('080900','08','09','00','Special Education','Theories and methods used in working with physically or mentally disabled persons, and assisting special education teachers.',1,null),
		('083500','08','35','00','Physical Education','Professional preparation skills in fitness, physical activity, and intercollegiate athletics. Includes courses designed to meet the general education requirement for instruction in lifelong healthful living through appropriate physical activity and theory courses, as well as transfer and physical education teacher preparation.',0,null),
		('083600','08','36','00','Recreation','Leadership principles and skills for recreational and leisure activities, and practices for providing indoor and outdoor recreational facilities and services for the general public.',0,null),
		('083700','08','37','00','Health Education','Physical and mental health, including disease prevention and control, and the social and economic significance of good health.',0,null),
		('083900','08','39','00','Industrial Arts (Transfer)','Meets the lower-division (transfer) requirements for industrial arts majors, including theories, methods, tools, materials, processes related to productive capacity of industry.',0,null),
		('085000','08','50','00','Sign Language','Programs and courses in American Sign Language or other sign language for the deaf.',0,null),
		('086000','08','60','00','Educational Technology','Principles and techniques for use of technology to improve learning. Includes computer software and resources, multimedia enhancements, audio and video skills, and other technology strategies that assist teachers to enhance the delivery of curriculum.',1,null),
		('089900','08','99','00','Other Education','Specify (includes all emerging occupations).',1,null),
		('080210','08','02','10','Educational Aide (Teacher Assistant), Bilingual','Preparation for individuals to provide classroom and school services to children and parents whose native language is other than English, under the direction of professional staff.',1,null),
		('083510','08','35','10','Physical Fitness and Body Movement','Programs and courses that encourage personal health and longevity through exercise activity, especially noncredit courses in this category.',0,null),
		('083520','08','35','20','Fitness Trainer','Preparation for training occupations in fitness and health, such as fitness specialists, aerobics or movement instructor, and human performance technician.',1,null),
		('083550','08','35','50','Intercollegiate Athletics',null,0,null),
		('083560','08','35','60','Coaching','Preparation for occupational coaching in schools or clubs.',1,null),
		('083570','08','35','70','Aquatics and Lifesaving','Preparation for occupational competency in swimming instruction and lifesaving as recommended by the State of California and the American Red Cross.',1,null),
		('083580','08','35','80','Adapted Physical Education','Programs and courses in physical education for persons with disabilities.',0,null),
		('083610','08','36','10','Recreation Assistant','Principles and techniques for preparing individuals to work in recreational occupations, including arts and crafts, sports, and hobbies.',1,null),
		('085010','08','50','10','Sign Language Interpreting','Programs to prepare individuals to interpret oral speech for the hearing impaired, including sign language, orientation to deaf culture, and interpreting from signing to voice and from voice to signing.',1,null),
		('090000','09','00','00','Engineering and Industrial Technologies','Instructional programs in the mathematical and natural sciences utilizing the materials and forces of nature for the benefit of human beings. Instructional programs in technology that require the application of scientific and engineering knowledge, methods, and technical skills in support of engineers and other professionals.',0,null),
		('090100','09','01','00','Engineering, General (requires Calculus) (Transfer)','Properties of matter and the sources of energy in nature that are made economically useful to humans.',0,null),
		('092400','09','24','00','Engineering Technology, General (requires Trigonometry)','Technical support of engineering, including the use of civil and mechanical engineering principles, physical sciences, basic physics, mathematics, surveying, materials testing, hydraulics and pneumatics, and the preparation of plans, specifications, and engineering reports.',1,null),
		('093400','09','34','00','Electronics and Electric Technology','Theory and application of electric and electronic systems and components, including circuits, electro-magnetic fields, energy sources, communications devices, radio, and television circuits, computers, and other electric and electronic components and devices.',1,null),
		('093500','09','35','00','Electro-Mechanical Technology','Design, development, testing, and maintenance of electro-mechanical and servomechanical devices and systems.',1,null),
		('093600','09','36','00','Printing and Lithography','Printing or reproduction of materials, including forms, newspapers, publications, and brochures. Computerized pre-print applications, press operations, camera and stripping, and bindery and finish work are included.',1,null),
		('094300','09','43','00','Instrumentation Technology','Design, manufacture and use of display devices and systems for detection, observation, measurement, control, computation, communication, or data processing.',1,null),
		('094500','09','45','00','Industrial Systems Technology and Maintenance','Design, construction, maintenance, and operation of mechanical, hydraulic, pneumatic, and electrical equipment and related systems, such as production machinery. Includes building and plant maintenance.',1,null),
		('094600','09','46','00','Environmental Control Technology (HVAC)','Assembly, installation, operation, maintenance, and repair of air conditioning, heating, and refrigeration systems.',1,null),
		('094700','09','47','00','Diesel Technology','Repair and maintenance of diesel engines in vehicles, ships, locomotives, and construction equipment, as well as stationary diesel engines in electrical generators and related equipment.',1,null),
		('094800','09','48','00','Automotive Technology','The servicing, maintenance, and diagnosis of malfunctions, and repair and overhaul of components and systems in automotive vehicles.',1,null),
		('094900','09','49','00','Automotive Collision Repair','Repair and refinishing of automotive vehicle panels and bodies, straightening of vehicle frames and unibodies, and replacement of damaged vehicle glass.',1,null),
		('095000','09','50','00','Aeronautical and Aviation Technology','Theory of flight and the design, construction, operation, and maintenance of aircraft, aircraft propulsion units, and aerospace vehicles. Includes combined airframe and powerplant mechanics programs.',1,null),
		('095200','09','52','00','Construction Crafts Technology','Lay out, fabrication, erection, installation, and repair of buildings, highways, airports, and other structures and fixtures, including framing, construction materials, estimating, blueprint reading, and use of tools.',1,null),
		('095300','09','53','00','Drafting Technology','Planning, preparation, and interpretation of various engineering sketches for design and drafting duties, for circuits, machines, structures, weldments, or architectural plans. Includes the application of advanced computer software and hardware (Computer Assisted Drafting and Computer Assisted Design) to the creation of graphic representations and simulations in support of engineering projects.',1,null),
		('095400','09','54','00','Chemical Technology','Chemical processes, including heat transfer, treatment of liquid and gases, and physical-chemical operations used in industrial processes and the chemical industry.',1,null),
		('095500','09','55','00','Laboratory Science Technology','Practical analytical applications of inorganic chemistry, organic chemistry biochemistry, and other physical and biological sciences in laboratory, testing, and quality control settings in industry and science.',1,null),
		('095600','09','56','00','Manufacturing and Industrial Technology','Engineering principles and technical skills for the manufacture of products and related industrial processes. Includes shaping and forming operations, materials handling, instrumentation and controls, and quality control. Includes Computer Aided Manufacturing and robotics. Also includes optimization theory, industrial and manufacturing planning, and related management skills.',1,null),
		('095700','09','57','00','Civil and Construction Management Technology','Application of procedures and techniques related to civil and construction management, including estimating and bidding, scheduling and control, inspection, building systems, construction practices, quality control, labor and safety practices. Includes public works management.',1,null),
		('095800','09','58','00','Water and Wastewater Technology','Principles, technical skills and equipment used to process, purify, store and distribute potable water, and dispose of waste water. Design, construction, operation, and maintenance of equipment for water or waste water treatment systems.',1,null),
		('095900','09','59','00','Marine Technology','Operation and maintenance of ships systems and marine equipment',1,null),
		('096100','09','61','00','Optics','Grinding of lenses from optical glass or plastic according to engineering specifications or optometrist prescriptions.',1,null),
		('096200','09','62','00','Musical Instrument Repair','Maintenance, repair, and tuning of acoustic and electric musical instruments',1,null),
		('099900','09','99','00','Other Engineering and Related Industrial Technologies','Specify (includes all emerging occupations).',1,null),
		('093410','09','34','10','Computer Electronics','Principles of computer design and circuitry, systems and network architecture and maintenance, components and peripherals, problem diagnosis and repair.',1,null),
		('093420','09','34','20','Industrial Electronics','Assembly, installation, operation, maintenance, and repair of electronic equipment used in industry and manufacturing. Includes fabrication and assembly of electronic and related components.',1,null),
		('093430','09','34','30','Telecommunications Technology','Application of engineering principles and technical skills to design and implementation of telecommunications systems. Includes communication protocols, data networking, digital compression and signal processing, satellite and microwave technology.',1,null),
		('093440','09','34','40','Electrical Systems and Power Transmission','Installation, operation, maintenance, and repair of electrical systems and the power lines that transmit electricity. Includes assembly, installation, maintenance and repair of motors, generators, transformers, and related equipment.',1,null),
		('093460','09','34','60','Biomedical Instrumentation','Operation, maintenance, and installation of devices associated with biomedical measurements and medical life support.',1,null),
		('093470','09','34','70','Electron Microscopy','Principles, procedures, and techniques associated with electron microscopes.',1,null),
		('093480','09','34','80','Laser and Optical Technology','Assembly, installation, testing, adjustment, and operation of various types of lasers and other optical equipment, including fiber optic equipment and systems.',1,null),
		('093510','09','35','10','Appliance Repair','Repair and servicing of consumer appliances, such as ranges, refrigerators, dryers, water heaters, washers, and dishwashers.',1,null),
		('094330','09','43','30','Vacuum Technology','Assembly, installation, maintenance, and repair of various vacuum actuated systems and devices.',1,null),
		('094610','09','46','10','Energy Systems Technology','Theory and methods of energy conservation applied to heating, cooling, and related systems, including the measurement and assessment of energy consumption, diagnosis and prescription. Includes alternative energy systems.',1,null),
		('094720','09','47','20','Heavy Equipment Maintenance','Maintenance, repair and overhaul of heavy equipment.',1,null),
		('094730','09','47','30','Heavy Equipment Operation','Operation of heavy equipment, including earth moving, demolition, and construction equipment.',1,null),
		('094740','09','47','40','Railroad and Light Rail Operations','Operation and maintenance of trains and railroad equipment, including light rail, heavy rail, passenger rail, and freight rail.',1,null),
		('094750','09','47','50','Truck and Bus Driving','Operation of trucks and buses with diesel, gasoline, or alternative power engines.',1,null),
		('094830','09','48','30','Motorcycle, Outboard, and Small Engine Repair','Repair, overhaul, service, and maintenance of motorcycles, outboard motors, and small engines.',1,null),
		('094840','09','48','40','Alternative Fuels and Advanced Transportation Technology','Conversion to, installation of, and maintenance of electric vehicles, liquefied petroleum gas, compressed natural gas, hybrid fuel technologies, and related systems.',1,null),
		('094850','09','48','50','Recreational Vehicle Service','Maintenance of hydraulic, electrical, air conditioning and other systems in recreational vehicles.',1,null),
		('094910','09','49','10','Upholstery Repair–Automotive','Repair and replacement of automotive interiors.',1,null),
		('095010','09','50','10','Aviation Airframe Mechanics','Inspection, repair, service, maintenance, and overhaul of airframes and aircraft systems. The program is designed to meet the Federal Aviation Administration (FAA) requirements for licensing as an airframe mechanic.',1,null),
		('095020','09','50','20','Aviation Powerplant Mechanics','Inspection, repair, service, maintenance, and overhaul of aircraft engines and engine systems. The program is designed to meet the Federal Aviation Administration (FAA) requirements for licensing as a powerplant mechanic.',1,null),
		('095040','09','50','40','Aircraft Electronics (Avionics)','Electronic theory, applications and equipment used in aircraft, including installation, maintenance, and repair of aircraft electronic and other operating and control systems.',1,null),
		('095050','09','50','50','Aircraft Fabrication','Principles and techniques of aircraft structural and composites fabrication and assembly.',1,null),
		('095210','09','52','10','Carpentry','Layout, fabrication, erection, and installation of structures using common systems of framing, construction materials, estimating, and blueprint reading.',1,null),
		('095220','09','52','20','Electrical','Installation, operation, maintenance and repair of electrical systems in buildings, including residential, commercial, and industrial electric power wiring and motors, controls, and electrical-distribution panels.',1,null),
		('095230','09','52','30','Plumbing, Pipefitting, and Steamfitting','Theories, principles, methods, technical skills and use of equipment in plumbing, pipefitting, and steamfitting.',1,null),
		('095240','09','52','40','Glazing','Theories, principles, methods, technical skills, and use of equipment in glazing.',1,null),
		('095250','09','52','50','Mill and Cabinet Work','Cutting, shaping, assembly, and finishing of wood and related materials according to designs and specifications.',1,null),
		('095260','09','52','60','Masonry, Tile, Cement, and Lath and Plaster','Theories, principles, methods, technical skills, and use of equipment in brick, block and stonemasonry, tile laying and finishing, cement finishing, and lathe and plaster construction.',1,null),
		('095270','09','52','70','Painting, Decorating, and Flooring','Theories, principles, methods, technical skills and use of equipment in painting, decorating, paperhanging and flooring.',1,null),
		('095280','09','52','80','Drywall and Insulation','Theories, principles, methods, technical skills, and use of equipment in drywall installation, taping, and insulation.',1,null),
		('095290','09','52','90','Roofing','Theories, principles, methods, technical skills and use of equipment in hot tar and/or the shingle roofing.',1,null),
		('095310','09','53','10','Architectural Drafting','Preparation of working drawings and electronic simulations for architectural and related construction projects.',1,null),
		('095320','09','53','20','Civil Drafting','Preparation of working drawings and electronic simulations in support of civil engineers, geologic engineers, and related professionals.',1,null),
		('095330','09','53','30','Electrical, Electronic, and Electro-Mechanical Drafting','Development of working schematics and representations in support of electrical/electronic engineers, computer engineers, electro-mechanical engineers, and related professionals.',1,null),
		('095340','09','53','40','Mechanical Drafting','Development of working drawings and electronic simulations in support of mechanical and industrial engineers and related professionals.',1,null),
		('095360','09','53','60','Technical Illustration','Principles, methods, and techniques used in the design and layout of consumer, commercial, and industrial illustrations.',1,null),
		('095420','09','54','20','Plastics and Composites','Principles, processes, technical skills, and equipment used in fabricating products from plastics, polymers, and composites.',1,null),
		('095430','09','54','30','Petroleum Technology','Petroleum production, exploration, testing, drilling, analyzing, and logging drilling cores and transporting gas and oil.',1,null),
		('095630','09','56','30','Machining and Machine Tools','Fabrication, assembly and repair of parts and components or systems on machines, such as lathes, grinders, drill presses, milling machines, and shaping machines. Includes Computer Numerical Control and tool design.',1,null),
		('095640','09','56','40','Sheet Metal and Structural Metal','Theories, principles, methods, technical skills, and equipment used in sheet metal occupations and ironworking occupations.',1,null),
		('095650','09','56','50','Welding Technology','Welding techniques, processes, and equipment applied in accordance with diagrams, blueprints, or other specifications.',1,null),
		('095670','09','56','70','Industrial and Occupational Safety and Health','Safety engineering principles and practices, as well as related federal, state and local regulations concerned with workplace safety.',1,null),
		('095680','09','56','80','Industrial Quality Control','Inspection, testing and evaluation of parts, products, equipment, and processes for adherence to specifications. Includes nondestructive testing.',1,null),
		('095720','09','57','20','Construction Inspection','Inspection of new or remodeled structures to determine their soundness and compliance to specifications, building codes and other regulations.',1,null),
		('095730','09','57','30','Surveying','Surveying and mapping of angles, elevations, points and contours used for construction, map-making, urban planning or other purposes. Includes related applications of Global Positioning Systems (GPS) and Geographic Information Systems (GIS).',1,null),
		('095910','09','59','10','Diving and Underwater Safety','Professional diving, diving instructors or diving support personnel.',1,null),
		('100000','10','00','00','Fine and Applied Arts','Instructional programs that study the perception, appreciation and creation of diverse modes of communicating ideas and emotion by means of visual and nonvisual representations and symbols; subject to esthetic criteria and related functions.',0,null),
		('100100','10','01','00','Fine Arts, General','Appreciation and production of works of art that express artistic intention',0,null),
		('100200','10','02','00','Art (Painting, Drawing, and Sculpture)','Two and three dimensional techniques in the areas of painting, drawing, and sculpture.',0,null),
		('100400','10','04','00','Music','Art and technique of combining sounds of various timbres in harmonic, rhythmic and/or melodic forms, which are artistically expressive.',0,null),
		('100500','10','05','00','Commercial Music','Performance of music in public venues under contract and related business skills and services.',1,null),
		('100600','10','06','00','Technical Theater','Techniques for communicating information, ideas, moods, and feelings through set design and construction and costuming with attention to stagecraft, function, and esthetics.',1,null),
		('100700','10','07','00','Dramatic Arts','Drama, theater and interpretation.',0,null),
		('100800','10','08','00','Dance','Techniques, composition, and choreography of dance.',0,null),
		('100900','10','09','00','Applied Design','Theory and studio work in the application of esthetic principles to the design of useful and decorative objects and spaces.',1,null),
		('101100','10','11','00','Photography','Historic development, esthetic qualities, creative processes, and practical applications of photographic imagery as a means of artistic expression.',0,null),
		('101200','10','12','00','Applied Photography','Application of esthetic principles and technical processes to the exposure, development, and marketing of photographs and photographic services.',1,null),
		('101300','10','13','00','Commercial Art','Design and execution of layouts and illustrations for advertising displays and instructional manuals, including the preparation of copy, lettering, poster, package and product design, fashion illustration, silk screening, air brushing, inks, color dynamics, and computer pre-print applications.',1,null),
		('103000','10','30','00','Graphic Art and Design','Application of artistic techniques to the effective communication of information and ideas, for commercial or technical purposes. Includes design principles, color theory, typography, concepts sketching, imaging, communication skills.',1,null),
		('103020','10','30','20','Computer Graphics',null,0,null),
		('109900','10','99','00','Other Fine and Applied Arts','Specify (includes all emerging occupations).',1,null),
		('100210','10','02','10','Painting and Drawing',null,0,null),
		('100220','10','02','20','Sculpture',null,0,null),
		('100230','10','02','30','Ceramics',null,0,null),
		('100810','10','08','10','Commercial Dance','Dance performance techniques specifically for occupational applications.',1,null),
		('100910','10','09','10','Jewlery','Application of design principles and fabrication techniques to the design, creation, and manufacture of jewelry.',0,null),
		('110000','11','00','00','Foreign Language','Instructional programs that study a language other than English, or related to the study of a foreign culture through exploration of the literature of that culture as expressed in the language of that culture.',0,null),
		('110100','11','01','00','Foreign Languages, General','Concentrated on more than one foreign language without giving full major emphasis to any one language, e.g., a minimum of 12 units in one foreign language and 6 units in another.',0,null),
		('110200','11','02','00','French','Language, literature, and culture of French-speaking people.',0,null),
		('110300','11','03','00','German','Language, literature, and culture of German-speaking people.',0,null),
		('110400','11','04','00','Italian','Language, literature, and culture of Italian-speaking people.',0,null),
		('110500','11','05','00','Spanish','Language, literature, and culture of Spanish-speaking people.',0,null),
		('110600','11','06','00','Russian','Language, literature, and culture of Russian-speaking people.',0,null),
		('110700','11','07','00','Chinese','Language, literature, and culture of Chinese-speaking people.',0,null),
		('110800','11','08','00','Japanese','Language, literature, and culture of Japanese-speaking people.',0,null),
		('110900','11','09','00','Latin','Language, literature, and culture of Latin-speaking people.',0,null),
		('111000','11','10','00','Greek','Language, literature, and culture of modern or classical Greek-speaking people.',0,null),
		('111100','11','11','00','Hebrew','Language, literature, and culture of Hebrew -speaking people.',0,null),
		('111200','11','12','00','Arabic','Language, literature, and culture of Arabic-speaking people.',0,null),
		('111600','11','16','00','African Languages (Non-Semitic)','Language, literature, and culture of the African peoples, such as Coptic, Berber, Igbo, Yoruba, Amharic, Hausa, Egyptian, and Swahili.',0,null),
		('111700','11','17','00','Asian, South Asian, and Pacific Islands (Chinese and Japanese excluded)','Language, literature, and culture of Asian-speaking people other than Chinese andJapanese.',0,null),
		('111900','11','19','00','Portuguese','Language, literature, and culture of Portuguese-speaking people.',0,null),
		('119900','11','99','00','Other Foreign Languages','Specify',0,null),
		('111710','11','17','10','Filipino (Tagalog)','Language, literature, and culture of Filipino-speaking people.',0,null),
		('111720','11','17','20','Vietnamese',null,0,null),
		('111730','11','17','30','Korean',null,0,null),
		('120000','12','00','00','Health','Instructional programs that study the theories and techniques for the restoration or preservation of mental and physical health through the use of drugs, surgical procedures, manipulations, or other curative or remedial methods.',0,null),
		('120100','12','01','00','Health Occupations, General','General and introductory occupational programs in the health occupations. Does not include transfer preparation programs (see 1260.00).',1,null),
		('120200','12','02','00','Hospital and Health Care Administration','Planning, organizing, and administering a hospital or other health care facility',1,null),
		('120500','12','05','00','Medical Laboratory Technology','Application of chemical, physical science, engineering, and technological concepts,principles, and practices to human and other living systems.',1,null),
		('120600','12','06','00','Physicians Assistant','Physician–delegated functions in the areas of general practice, family medicine, internal medicine, obstetrics, emergency medicine, or in other specific areas of patient care.',1,null),
		('120800','12','08','00','Medical Assisting','Administrative, clerical or technical support services for a licensed physician, podiatrist, or health care services plan, and related support functions in a health care facility.',1,null),
		('120900','12','09','00','Hospital Central Service Technician','Services and techniques used in support of patient care, such as receipt, evaluation, storage and distribution of supplies, equipment, and instruments, including sterile processing.',1,null),
		('121000','12','10','00','Respiratory Care/Therapy','Clinical assistance in the diagnosis, treatment, and rehabilitation of acute and chronic respiratory disease, including pulmonary function, drug administration, mechanical ventilation, medical gas therapy, airway management, patient assessment, and assistance to the physician in carrying out special procedures, and as part of emergency and life-support team.',1,null),
		('121100','12','11','00','Polysomnography','Diagnosis and treatment of disorders of sleep and daytime alertness, and evaluation of various treatment methods.',1,null),
		('121200','12','12','00','Electro-Neurodiagnostic Technology','Techniques used to perform non-invasive diagnostic procedures on the nervous system, including electroencephalography.',1,null),
		('121300','12','13','00','Cardiovascular Technician','Principles and techniques used in performing invasive, noninvasive, and peripheral examinations of the cardiovascular system to aid in diagnoses and therapeutic treatments.',1,null),
		('121400','12','14','00','Orthopedic Assistant','Principles and techniques used in assisting a physician to apply and remove casts, assemble traction apparatus, fit straps and splints, assemble exercise frames, and adjust crutches and canes.',1,null),
		('121500','12','15','00','Electrocardiography','Prepares individuals in the operation and maintenance of electrocardiograph machines.',1,null),
		('121700','12','17','00','Surgical Technician','Procedures, skills, and use of equipment to assist in surgery.',1,null),
		('121800','12','18','00','Occupational Therapy Technology','Techniques of rehabilitation, patient evaluation, and treatment planning, under the supervision of an occupational therapist or other healthcare provider.',1,null),
		('121900','12','19','00','Optical Technology','Theory of vision and lenses and techniques for the application of those principles to assisting in vision care.',1,null),
		('122000','12','20','00','Speech-Language Pathology and Audiology','Principles and procedures used to assist in the diagnosis, treatment, and prevention of hearing and speech disorders under the supervision of a licensed speech-language pathologist or audiologist.',1,null),
		('122100','12','21','00','Pharmacy Technology','Principles and procedures used to assist in maintaining and dispensing pharmaceutical supplies and medications.',1,null),
		('122200','12','22','00','Physical Therapist Assistant','Principles and procedures used to assist a physical therapist in implementing a prescribed plan of therapy for a patient, including patient instruction and administration of treatments.',1,null),
		('122300','12','23','00','Health Information Technology','Compilation and maintenance of medical records, development of statistical reports, coding of diseases and operations, maintenance of indexes, and handling of requests for medical records information.',1,null),
		('122400','12','24','00','School Health Clerk','Provision of non-invasive health support services to children in the education system, under supervision of a school nurse or school administration.',1,null),
		('122500','12','25','00','Radiologic Technology','Principles and techniques used in diagnostic radiography. Including radiographic techniques, radiation protection, equipment maintenance, and film processing and darkroom techniques.',1,null),
		('122600','12','26','00','Radiation Therapy Technician','Application of radioactive substances to patients for diagnostic and therapeutic purposes.',1,null),
		('122700','12','27','00','Diagnostic Medical Sonography','Techniques used to assist physicians in diagnosing disease or injury by means of high-frequency sound waves (ultrasound) that create body images showing the shape and composition of body tissues.',1,null),
		('122800','12','28','00','Athletic Training and Sports Medicine','Programs to prepare for employment in athletic training, injury treatment, rehabilitation, fitness assessment, and related fields.',1,null),
		('123000','12','30','00','Nursing','Principles and techniques for assisting the individual, sick or well, in the performance of those activities contributing to health or to recovery.',1,null),
		('123900','12','39','00','Psychiatric Technician','Principles and techniques used in the care of mentally disordered, developmentally disabled and emotionally disturbed patients, in a variety of health care settings specific to the licensure examination.',1,null),
		('124000','12','40','00','Dental Occupations','Programs related to dental services.',1,null),
		('125000','12','50','00','Emergency Medical Services','Pre-hospital, emergency medical diagnostic procedure, treatment, and comprehensive care in medical crises, including emergency vehicle operation and patient transportation procedures, including training specific to the certification standards for the EMT-1 or EMT-2 certifications.',1,null),
		('125100','12','51','00','Paramedic','Training specific to the certification standards for EMT-P (Paramedic).',1,null),
		('125500','12','55','00','Mortuary Science','Preparation for burial, embalming, cremating, and other methods of disposition in conformity with legal requirements, and the conduct of funerals.',1,null),
		('126000','12','60','00','Health Professions, Transfer Core Curriculum','Studies that satisfy the academic transfer requirements of the core curriculum for health professions.',0,null),
		('126100','12','61','00','Community Health Care Worker','Preparation for facilitators, advocates, and referral workers linking health care and related social services with affected recipient communities.',1,null),
		('126200','12','62','00','Massage Therapy','Preparation for certified practice of various styles of massage, such as Swedish, sports, and deep tissue, for therapeutic and relaxation purposes, including business aspects of massage practice.',1,null),
		('127000','12','70','00','Kinesiology','Provides preparation for programs that focus on the anatomy, physiology, biochemistry, and biophysics of human movement, and applications to exercise and therapeutic rehabilitation.',0,null),
		('129900','12','99','00','Other Health Occupations','Specify (includes all emerging occupations).',1,null),
		('120510','12','05','10','Phlebotomy','Principles and practices for puncturing the skin to give injections, and puncturing veins or arteries to obtain blood samples for analysis and diagnosis.',1,null),
		('120810','12','08','10','Clinical Medical Assisting','Principles and techniques to assist with patient care and examination in health care offices and clinics. Includes taking vital signs, preparing patient, assisting with physical exams, collecting specimens, performing basic laboratory tests, and maintaining examination rooms. May include administration of oral or injectable medications under direction of a nurse or physician.',1,null),
		('120820','12','08','20','Administrative Medical Assisting','Principles and techniques to assist with administration in health care offices and clinics. Includes receptionist duties, maintaining patient records, billing, insurance, bookkeeping, appointments, and scheduling.',1,null),
		('120830','12','08','30','Health Facility Unit Coordinator','Procedures and techniques for the administrative and clerical support of a nursing unit in a hospital or other health care facility.',1,null),
		('122310','12','23','10','Health Information Coding','Training to assign numerical codes to diagnoses, symptoms, and procedures using standard international classifications and terminology.',1,null),
		('123010','12','30','10','Registered Nursing','Programs leading to licensure by the Board of Registered Nursing. Includes nursing care in specialty areas.',1,null),
		('123020','12','30','20','Licensed Vocational Nursing','Programs leading to vocational nurse licensure by the Board of Vocational Nursing and Psychiatric Technicians. Technical and manual nursing skills, practiced under the direction of a registered nurse, physician, or other medical staff, specific to the scope of practice of the licensed vocational nurse.',1,null),
		('123030','12','30','30','Certified Nurse Assistant','Routine nursing services of patients in hospitals or long term care facilities practiced under the direction of nursing or medical staff, and specific to the nurse assistant certification examination. Includes Acute Care Nurse Assistant.',1,null),
		('123080','12','30','80','Home Health Aide','Principles and techniques used to assist elderly, convalescent, or disabled patients in their homes by providing for their health care needs.',1,null),
		('124010','12','40','10','Dental Assistant','Techniques used in the clinical chairside procedures of dentistry, including preparation of the patient, radiographic exposures, the mixing of dental materials, dental office management specific to the licensure examination.',1,null),
		('124020','12','40','20','Dental Hygienist','Oral health education, control of oral disease, preventive, educational, and therapeutic techniques used in dental treatments specific to the licensure examination.',1,null),
		('124030','12','40','30','Dental Laboratory Technician','Construction and repair of dental appliances; operations and procedures used in support of a dentist or orthodontist.',1,null),
		('130000','13','00','00','Family and Consumer Sciences','Instructional programs that study the relationship between the physical, social, emotional and intellectual environment in and of the home and family and the development of individuals, including programs in child development, family studies, gerontology, fashion, interior design and merchandising, consumer services, foods and nutrition,culinary arts, and hospitality.',0,null),
		('130100','13','01','00','Family and Consumer Sciences, General','General programs in family and consumer sciences, including life management, how individuals develop and function in family, work, and community settings, and how they relate to their physical, social, emotional, and intellectual environments. Prepares individuals to balance personal, family and work responsibilities throughout the life cycle. May include introductory courses in fashion, nutrition and foods, interiors, family studies, child development. Programs may lead to transfer or to practical and occupational outcomes.',1,null),
		('130200','13','02','00','Interior Design and Merchandising','Design and its functional application to the environment, housing, furnishings, accessories, and equipment to provide commercial and residential environments that meet the psychological, sociological, emotional, and physical needs of the users and protect the health, safety, and welfare of the general public.',1,null),
		('130300','13','03','00','Fashion','Fashion and its influence on individuals and society, including fashion’s principles and concepts as related to design, construction, merchandising and selection; and the study of textiles involving the design, production, finishing, characteristics, selection, use, and care of fibers and fabrics.',1,null),
		('130500','13','05','00','Child Development/Early Care and Education','Intellectual, psychosocial, and physical development of children. Principles and practices in the care and education of children.',1,null),
		('130600','13','06','00','Nutrition, Foods, and Culinary Arts','Principles and techniques of food preparation, food management, food production services and related technologies, and the fundamentals of nutrition, nutrition education, and nutrition care affecting human growth and health maintenance.',1,null),
		('130700','13','07','00','Hospitality','Organization and administration of hospitality services, management, and training of personnel, including restaurant and food service management, hotel/motel or resort management, and convention and special events planning. Includes combined hotel/restaurant management programs.',1,null),
		('130800','13','08','00','Family Studies','Basic human developmental and behavioral characteristics of the individual within the context of the family, and over the lifespan. Includes human growth and development, the family as a social unit, and relationships.',1,null),
		('130900','13','09','00','Gerontology','Principles and practices of working with older adults. Includes physical, social, economic, emotional, health care and support services needs and concerns, related legislation, and community resources.',1,null),
		('139900','13','99','00','Other Family and Consumer Sciences','Specify (includes all emerging occupations).',1,null),
		('130110','13','01','10','Consumer Services','Programs intended to prepare directly for employment related to consumer communication and services in the public sector or businesses, such as finance, retail, utilities, and telecommunications. Includes responding to consumer problems and trends and providing information. Generally includes courses in business and well as courses in family and consumer sciences.',1,null),
		('130310','13','03','10','Fashion Design','Design and construction of garments.',1,null),
		('130320','13','03','20','Fashion Merchandising','Merchandising of fashion and related articles in retail and wholesale establishments.',1,null),
		('130330','13','03','30','Fashion Production','Construction, alteration, and finishing of garments to industry or customer specifications.',1,null),
		('130520','13','05','20','Children with Special Needs','Principles and practices necessary for the care and education of children with special needs and their families.',1,null),
		('130540','13','05','40','Preschool Age Children','Principles and practices necessary for working with children ages three through five.',1,null),
		('130550','13','05','50','The School Age Child','Principles and practices necessary for working with school age children and youth in out-of-school settings, such as before and after school programs, recreational activities, youth centers, and summer camps.',1,null),
		('130560','13','05','60','Parenting and Family Education','Principles and practices of working with individuals in all family forms to positively affect their relationships and dynamics.',1,null),
		('130570','13','05','70','Foster and Kinship Care','Education and training for foster parents and kinship parents to meet the educational, emotional, behavioral and developmental needs of children and youth in the foster care system.',1,null),
		('130580','13','05','80','Child Development Administration and Management','Principles and practices of managing all programs in child development, and early care and education.',1,null),
		('130590','13','05','90','Infants and Toddlers','Principles and practices necessary for working with children younger than three years old.',1,null),
		('130620','13','06','20','Dietetic Services and Management','Programs providing training in institutional food services and the management and supervision of such services, as Dietary Managers, Dietary Service Supervisors, and similar positions. Includes food services in schools, hospitals, nursing facilities, and other noncommercial settings',1,null),
		('130630','13','06','30','Culinary Arts','Selection, storage, preparation, and service of food in quantity, including the culinary techniques used by chefs, institutional cooks, bakers, and catering services.',1,null),
		('130660','13','06','60','Dietetic Technology','Programs leading to national certification as a dietetic technician by the American Dietetic Association.',1,null),
		('130710','13','07','10','Restaurant and Food Services and Management','Management and supervision of food and beverage service operations. Includes server and bartender training.',1,null),
		('130720','13','07','20','Lodging Management','Preparation to manage places of lodging such as hotels, motels and inns. Includes supplies purchasing and control, facilities design and planning, hospitality law, personnel and financial management, marketing, events management, and front desk operations.',1,null),
		('130730','13','07','30','Resort and Club Management','Preparation to plan, manage, and market comprehensive vacation facilities, golf courses, casinos, time-share resorts and the like.',1,null),
		('140000','14','00','00','Law','Instructional programs that study the principles and procedures developed and enforced by institutions of government for the social order in the form of legislation, decisions, regulations, and orders.',0,null),
		('140100','14','01','00','Law, General','Legal customs, practices, and rules of society and the states. Includes pre-law programs.',0,null),
		('140200','14','02','00','Paralegal','Legal terminology, forms and procedures; general legal concepts; principles and techniques of legal research, including analysis of legal issues, documentation of appropriate legal precedents, and presentation of research findings useable by attorneys, judges, and others.',1,null),
		('149900','14','99','00','Other Law','Specify.',0,null),
		('150000','15','00','00','Humanities (Letters)','Instructional programs that study language and literature, philosophy and value systems related to ancient and modern cultures and the expression of those systems in creative works and cultural artifacts.',0,null),
		('150100','15','01','00','English','Written expression and the writing process which include critical reading, critical writing, research practices, literature, and literary criticism.',0,'ENGL'),
		('150200','15','02','00','Language Arts','Programs that combine English, speech communication, and/or foreign language and other humanistic or communications-related disciplines.',0,null),
		('150300','15','03','00','Comparative Literature','Comparison of literature of different cultures or eras, based on such dimensions as genre, theme, literary period, and language source.',0,null),
		('150400','15','04','00','Classics','Historical works of literature, religious thought, intellectual and social history and related art forms accepted as seminal in a culture or discipline.',0,null),
		('150600','15','06','00','Speech Communication','Theory and methods of group communication and public address, including the development and use of language and the analysis of speech and other communication behaviors and actions.',0,null),
		('150700','15','07','00','Creative Writing','Short story, poetry, and novel, including the detailed study of published models with emphasis on the creative process.',0,null),
		('150900','15','09','00','Philosophy','Critical examination of the categories for describing reality, the nature and contexts of human experience, the methodology of rational inquiry, and criteria of practice, including ethics, esthetics, logic, and the history of ideas.',0,null),
		('151000','15','10','00','Religious Studies (Theological professions excluded)','Nature, function, origin, history, and tenets of the various religions.',0,null),
		('152000','15','20','00','Reading','Theory and methods of reading including vocabulary, comprehension, fluency, critical and analytical reading.',0,'READ'),
		('159900','15','99','00','Other Humanities','Specify.',0,null),
		('150110','15','01','10','Linguistics','Study of language, language development, and relationships among languages from a humanistic and/or scientific perspective.',0,null),
		('160000','16','00','00','Library Science','Pre-professional transfer and paraprofessional programs that prepare individuals to assist professional librarians in acquiring and organizing media collections, and assisting in research and other services related to those resources.',0,null),
		('160100','16','01','00','Library Science, General','Introduction to the structures and procedures of acquiring and organizing collections of resource materials in print and other media and conducting research using those resources.',0,null),
		('160200','16','02','00','Library Technician (Aide)','Techniques necessary to assist librarians or to conduct library activities under the direction of library staff.',1,null),
		('169900','16','99','00','Other Library Science','Specify.',0,null),
		('170000','17','00','00','Mathematics','Instructional programs in the science of numbers, space configurations, and their operations, computations, measurements, relationships, applications and abstractions. Mathematics is a broad field of study that examines properties and interactions using numbers, graphing, equations, reasoning and logic for both real utilitarian problems and abstract concepts.',0,null),
		('170100','17','01','00','Mathematics, General','Science of numbers and space configurations and their operations, measurements, computations, relationships, applications and abstractions. Theoretical topics in computer science, statistics, astronomy, or other sciences may be included when treated as mathematical constructs or used as examples for the application of mathematical concepts and operations.',0,'MATH'),
		('170200','17','02','00','Mathematics Skills','Designed to clarify and develop specific supplementary skills to help students succeed in particular concepts fundamental to mathematics, such as factoring, math anxiety, word problems, scientific and graphing calculators, etc.',0,null),
		('179900','17','99','00','Other Mathematics','Specify.',0,null),
		('180000','18','00','00','Military Studies','Instructional programs in the techniques and skills unique to the pursuit of a professional career as a military officer.',0,null),
		('180100','18','01','00','Military Science','History, strategy, logistics, and techniques of organized warfare and its conduct via air, land, and sea forces.',0,null),
		('189900','18','99','00','Other Military Studies','Specify.',0,null),
		('190000','19','00','00','Physical Sciences','Instructional programs in the basic nature of matter, energy, systems, and associated phenomena.',0,null),
		('190100','19','01','00','Physical Sciences, General','Matter, energy, chemical and other systems, and associated phenomena.',0,null),
		('190200','19','02','00','Physics, General','Physical properties and interactions of matter and energy, including equilibrium, power, wave phenomena, mechanics, heat, electricity, magnetism, sound, light, special relativity, and the particle nature of matter.',0,null),
		('190500','19','05','00','Chemistry, General','Subatomic particles, elements, compounds, and other forms of matter: their detection, occurrence, composition, structure, properties, determination, interactions, transformations, changes of state, energy relationship; and the governing laws.',0,null),
		('191100','19','11','00','Astronomy','Matter and energy in the universe, i.e., the solar system, stars, galaxies and nebula, their history and dynamics, and related theories and cases.',0,null),
		('191400','19','14','00','Geology','The structure, composition, origin, history, distribution and modification of materials upon and within the earth and other celestial bodies.',0,null),
		('191900','19','19','00','Oceanography','The physical and chemical properties of water, the topography and composition of the ocean bottom, waves, currents, tides, the formation of islands, and related subjects.',0,null),
		('192000','19','20','00','Ocean Technology','Procedures and techniques used to measure and analyze ocean currents, seas, and other major bodies of water and ocean life, including the operation and/or maintenance and repair of related equipment and instruments. Includes aquarium technology and aquaculture.',1,null),
		('193000','19','30','00','Earth Science','Interdisciplinary study of the natural phenomena of the Earth as they affect human interactions in the environment. May include elements from geology, physical geography, meteorology and climatology, environmental resources, soil science, physical oceanography, and other disciplines.',0,null),
		('199900','19','99','00','Other Physical Sciences','Specify.',0,null),
		('200000','20','00','00','Psychology','Instructional programs in the objective behavior and subjective experience of the individual organism. It is also the associated professional discipline which applies its findings in service to the individual, industry, and government.',0,null),
		('200100','20','01','00','Psychology, General','The objective behavior and the subjective experience of human beings.',0,null),
		('200300','20','03','00','Behavioral Science','Programs that combine psychology with coursework from one or more social sciences.',0,null),
		('209900','20','99','00','Other Psychology','Specify.',0,null),
		('210000','21','00','00','Public and Protective Services','Instructional programs in the theories and techniques of the formulation, implementation and evaluation of public policies and services, including public safety occupations. Includes developing and improving competencies in the management and operation of government agencies.',0,null),
		('210200','21','02','00','Public Administration','Policy development and implementation in management of public services, public works, and public utilities.',1,null),
		('210400','21','04','00','Human Services','Theory and practice in providing human and social services to individuals and communities. Preparation for work in public and private human services organizations.',1,null),
		('210500','21','05','00','Administration of Justice','Theories, principles, and techniques of law enforcement agencies, juvenile justice, and corrections.',1,null),
		('213300','21','33','00','Fire Technology','Principles and techniques of preventing, controlling and extinguishing fires, including firefighter operations, maintenance of fire fighting equipment, fire rescue procedures, and applicable laws and regulations.',1,null),
		('214000','21','40','00','Legal and Community Interpretation','Training in providing English-foreign language interpreting and translating services to aid clients in the legal and social services systems and in other governmental or community contexts.',1,null),
		('219900','21','99','00','Other Public and Protective Services','Specify (includes all emerging occupations).',1,null),
		('210210','21','02','10','Public Works','Practical instruction in street construction and maintenance and other public infrastructure services.',1,null),
		('210440','21','04','40','Alcohol and Controlled Substances','Prepares individuals with an integrated theoretical and practical experience to develop skills necessary to work in the field of alcohol/drug abuse, as well as with families or employers of chemically dependent persons.',1,null),
		('210450','21','04','50','Disability Services','Principles and techniques of providing counseling, supervision, and supportive services for persons with developmental and other disabilities.',1,null),
		('210510','21','05','10','Corrections','Theories, principles and techniques of providing services to the incarcerated.',1,null),
		('210520','21','05','20','Probation and Parole','Principles and techniques governing the probation and parole of legal offenders.',1,null),
		('210530','21','05','30','Industrial and Transportation Security','Techniques involved in providing security services to institutions, government entities, and the general public. Includes airport and airline security.',1,null),
		('210540','21','05','40','Forensics, Evidence, and Investigation','Theories, principles, and techniques of forensic science and investigation in the justice system.',1,null),
		('210550','21','05','50','Police Academy','Principles and techniques of law enforcement specific to the requirements of the California Commission on Peace Officer Standards and Training (POST).',1,null),
		('213310','21','33','10','Wildland Fire Technology','Principles and techniques of wildland firefighting including weather, topography, fuels and fuel moisture as it affects wildland fire behavior. Wildland fire prevention and specific training in logistics, investigation, and management of large wildland fires.',1,null),
		('213350','21','33','50','Fire Academy','Studies specific to local and state training requirements for employment and post-employment advancement.',1,null),
		('220000','22','00','00','Social Sciences','Instructional programs in all aspects of the past and present activities, conduct, interactions and organizations of humans.',0,null),
		('220100','22','01','00','Social Sciences, General','Programs that combine coursework from various different disciplines within the social sciences.',0,null),
		('220200','22','02','00','Anthropology','The origins, physical and cultural development, technologies, social customs, and beliefs of mankind.',0,null),
		('220300','22','03','00','Ethnic Studies','Programs that focus on the culture and history of ethnic and minority groups.',0,null),
		('220400','22','04','00','Economics','Study of limited resources, their use in producing goods and services, and their allocation and consumption; and related theories, principles, and techniques.',0,null),
		('220500','22','05','00','History','The past, including the recording, gathering, criticizing, synthesizing, and interpreting evidence about past events.',0,null),
		('220600','22','06','00','Geography','Earth and its life in land, sea, and air, including the distribution of plant and animal life, human beings, human settlements and industries, and the interaction of these factors.',0,null),
		('220700','22','07','00','Political Science','Description and analysis of political institutions and processes, including the origin, development, geographical units, forms, sources of authority, powers, purposes, functions and operations of government and related theories of social benefit.',0,null),
		('220800','22','08','00','Sociology','Human society, social institutions, and social relationships, including such things as the development, purposes, structures, functions, and interactions of human groups.',0,null),
		('221000','22','10','00','International Studies','Programs that focus on global and international issues from the perspective of the social sciences and related fields.',0,null),
		('229900','22','99','00','Other Social Sciences','Specify.',0,null),
		('220110','22','01','10','Women’s Studies','History, sociology, culture of women, and development of modern feminism in relation to historical roles played by women.',0,null),
		('220120','22','01','20','American Studies','History, society, politics, culture, and economics of the United States and preColumbian predecessors.',0,null),
		('220220','22','02','20','Archaeology','Study of extinct societies, and the past of living societies, via interpretation of their remains.',0,null),
		('220610','22','06','10','Geographic Information Systems','Computer-based tools for acquiring, editing, storing, analyzing, and visualizing geographically referenced information, with applications in research, education, management, and planning. Includes Global Positioning System (GPS).',1,null),
		('220710','22','07','10','Student Government',null,0,null),
		('221010','22','10','10','Area Studies','Programs that focus on defined regions or countries of the world.',0,null),
		('300000','30','00','00','Commercial Services','Instructional programs that include those subject field designations associated with the development of skills required for the field of commerce.',0,null),
		('300500','30','05','00','Custodial Services','Care of buildings, fixtures, furnishings, and floor surfaces, including the use and care of tools, chemicals, clean procedures and the scheduling of work, and the purchasing of custodial supplies.',1,null),
		('300700','30','07','00','Cosmetology and Barbering','Care and styling of hair, and care of complexion, hands and feet, including hygiene, customer relations, and salon management.',1,null),
		('300800','30','08','00','Dry Cleaning','Operation and management of dry cleaning and laundering plants.',1,null),
		('300900','30','09','00','Travel Services and Tourism','Promotion of tourism, marketing, booking, and other services provided by travel agents and travel-related companies.',1,null),
		('302000','30','20','00','Aviation and Airport Management and Services','Programs related to air transportation systems and services.',1,null),
		('309900','30','99','00','Other Commercial Services','Specify (includes all emerging occupations).',1,null),
		('302010','30','20','10','Aviation and Airport Management','Training in airport and aviation operations, ground support and ground traffic direction, passenger and cargo operations, flight and terminal safety and security, aviation regulations, and business aspects of aviation and airports.',1,null),
		('302020','30','20','20','Piloting','Operation of commercial and private aircraft, including piloting, navigation, and passenger services.',1,null),
		('302030','30','20','30','Air Traffic Control','Air traffic management and flight control, including radar plotting, radio communications, aviation weather, instrumentation, and maintenance of flight control logs.',1,null),
		('302040','30','20','40','Flight Attendant','Knowledge and skills used in service of the safety and comfort of airline passengers.',1,null),
		('490000','49','00','00','Interdisciplinary Studies','Instructional programs that include those subject field designations which involve more than one major discipline without primary concentration in any one area. Also, courses in guidance, student success, and other categories, which do not fall into any other discipline, may be coded in the 6-digit 4930.xx series or 4931.00 through 4999.00.',0,null),
		('490100','49','01','00','Liberal Arts and Sciences, General','Provides for a wide distribution of courses that contribute to a balance of intellectual interests in the disciplines of this category.',0,null),
		('490200','49','02','00','Biological and Physical Sciences (and Mathematics)','Provides for a wide distribution of courses that contribute to a balance of intellectual interests in the disciplines of this category.',0,null),
		('490300','49','03','00','Humanities','Provides for a wide distribution of courses that contribute to a balance of intellectual interests in the disciplines of this category.',0,null),
		('493000','49','30','00','General Studies','This code can not be used for courses.',0,null),
		('493080','49','30','80','English as a Second Language-General',null,0,null),
		('493081','49','30','81','English as a Second Language–College Level',null,0,null),
		('493082','49','30','82','English as a Second Language–Survival Level',null,0,null),
		('493100','49','31','00','Vocational ESL',null,1,null),
		('493200','49','32','00','General Work Experience','Supervised employment intended to assist students in acquiring desirable work habits, attitudes, and career awareness.',1,null),
		('499900','49','99','00','Other Interdisciplinary Studies','Specify.',0,null),
		('490110','49','01','10','Transfer Preparation','Includes Certificates of Achievement based on the Intersegmental General Education Transfer Curriculum (IGETC), the California State University General Education-Breadth requirements or transfer patterns established by accredited public postsecondary institutions in adjacent states that award the baccalaureate degree.',0,null),
		('490120','49','01','20','Liberal Studies (teaching preparation)','Programs specifically designed to meet requirements for a Liberal Studies university major, for students intending to pursue a Multiple Subject teaching credential.',0,null),
		('490310','49','03','10','Humanities and Fine Arts',null,0,null),
		('490330','49','03','30','Humanities and Social Sciences',null,0,null),
		('493009','49','30','09','Supervised Tutoring',null,0,null),
		('493010','49','30','10','Career Guidance and Orientation',null,0,null),
		('493011','49','30','11','Interpersonal Skills',null,0,null),
		('493012','49','30','12','Job Seeking/Changing Skills',null,0,null),
		('493013','49','30','13','Academic Guidance',null,0,null),
		('493014','49','30','14','Study Skills',null,0,null),
		('493030','49','30','30','Learning Skills, Disabled',null,0,null),
		('493031','49','30','31','Living Skills, Disabled',null,0,null),
		('493032','49','30','32','Learning Skills, Learning Disabled',null,0,null),
		('493033','49','30','33','Learning Skills, Speech Impaired',null,0,null),
		('493060','49','30','60','Elementary Education (Grades 1-8)',null,0,null),
		('493062','49','30','62','Secondary Education (Grades 9-12) and G.E.D.',null,0,null),
		('493072','49','30','72','Leadership Skills Development',null,0,null),
		('493084','49','30','84','English as a Second Language–Writing',null,0,'ESL'),
		('493085','49','30','85','English as a Second Language–Reading',null,0,'ESL'),
		('493086','49','30','86','English as a Second Language–Speaking/Listening',null,0,'ESL'),
		('493087','49','30','87','English as a Second Language–Integrated',null,0,'ESL'),
		('493090','49','30','90','Citizenship (and ESL Civics)',null,0,null),
		('999999','99','99','99','Unknown',null,0,null)
	) a
	(
		ProgramCode,
		DisciplineCode,
		SubdisciplineCode,
		FieldCode,
		Name,
		Description,
		IsVocational,
		SubjectCode
	)
	left outer join
	(
		VALUES
		('010100'),
		('010200'),
		('010210'),
		('010220'),
		('010230'),
		('010240'),
		('010300'),
		('010310'),
		('010400'),
		('010900'),
		('010910'),
		('010920'),
		('010930'),
		('010940'),
		('011200'),
		('011300'),
		('011400'),
		('011500'),
		('011510'),
		('011520'),
		('011600'),
		('019900'),
		('020100'),
		('029900'),
		('030300'),
		('043000'),
		('050100'),
		('050200'),
		('050210'),
		('050400'),
		('050500'),
		('050600'),
		('050630'),
		('050640'),
		('050650'),
		('050800'),
		('050900'),
		('050910'),
		('050920'),
		('050940'),
		('050960'),
		('050970'),
		('051000'),
		('051100'),
		('051110'),
		('051200'),
		('051400'),
		('051410'),
		('051420'),
		('051430'),
		('051440'),
		('051600'),
		('051800'),
		('059900'),
		('060200'),
		('060400'),
		('060410'),
		('060420'),
		('060430'),
		('060600'),
		('060700'),
		('061000'),
		('061220'),
		('061400'),
		('061410'),
		('061420'),
		('061430'),
		('061440'),
		('061450'),
		('061460'),
		('069900'),
		('070100'),
		('070200'),
		('070210'),
		('070700'),
		('070710'),
		('070720'),
		('070730'),
		('070800'),
		('070810'),
		('070820'),
		('070900'),
		('070910'),
		('079900'),
		('080200'),
		('080210'),
		('080900'),
		('083520'),
		('083560'),
		('083570'),
		('083600'),
		('083610'),
		('085010'),
		('086000'),
		('089900'),
		('092400'),
		('093400'),
		('093410'),
		('093420'),
		('093430'),
		('093440'),
		('093460'),
		('093470'),
		('093480'),
		('093500'),
		('093510'),
		('093600'),
		('094300'),
		('094330'),
		('094500'),
		('094600'),
		('094610'),
		('094700'),
		('094720'),
		('094730'),
		('094740'),
		('094750'),
		('094800'),
		('094830'),
		('094840'),
		('094850'),
		('094900'),
		('094910'),
		('095000'),
		('095010'),
		('095020'),
		('095040'),
		('095050'),
		('095200'),
		('095210'),
		('095220'),
		('095230'),
		('095240'),
		('095250'),
		('095260'),
		('095270'),
		('095280'),
		('095290'),
		('095300'),
		('095310'),
		('095320'),
		('095330'),
		('095340'),
		('095360'),
		('095400'),
		('095420'),
		('095430'),
		('095500'),
		('095600'),
		('095630'),
		('095640'),
		('095650'),
		('095670'),
		('095680'),
		('095700'),
		('095720'),
		('095730'),
		('095800'),
		('095900'),
		('095910'),
		('096100'),
		('096200'),
		('099900'),
		('100500'),
		('100600'),
		('100810'),
		('100900'),
		('101200'),
		('101300'),
		('103000'),
		('109900'),
		('120100'),
		('120200'),
		('120500'),
		('120510'),
		('120600'),
		('120800'),
		('120810'),
		('120820'),
		('120830'),
		('120900'),
		('121000'),
		('121100'),
		('121200'),
		('121300'),
		('121400'),
		('121500'),
		('121700'),
		('121800'),
		('121900'),
		('122000'),
		('122100'),
		('122200'),
		('122300'),
		('122310'),
		('122400'),
		('122500'),
		('122600'),
		('122700'),
		('122800'),
		('123000'),
		('123010'),
		('123020'),
		('123030'),
		('123080'),
		('123900'),
		('124000'),
		('124010'),
		('124020'),
		('124030'),
		('125000'),
		('125100'),
		('125500'),
		('126100'),
		('126200'),
		('129900'),
		('130100'),
		('130110'),
		('130200'),
		('130300'),
		('130310'),
		('130320'),
		('130330'),
		('130500'),
		('130520'),
		('130540'),
		('130550'),
		('130560'),
		('130570'),
		('130580'),
		('130590'),
		('130600'),
		('130620'),
		('130630'),
		('130660'),
		('130700'),
		('130710'),
		('130720'),
		('130730'),
		('130800'),
		('130900'),
		('139900'),
		('140200'),
		('160200'),
		('192000'),
		('210200'),
		('210210'),
		('210400'),
		('210440'),
		('210450'),
		('210500'),
		('210510'),
		('210520'),
		('210530'),
		('210540'),
		('210550'),
		('213300'),
		('213310'),
		('213350'),
		('214000'),
		('219900'),
		('220610'),
		('300500'),
		('300700'),
		('300800'),
		('300900'),
		('302000'),
		('302010'),
		('302020'),
		('302030'),
		('302040'),
		('309900'),
		('493100'),
		('493200')
	) b
	(
		ProgramCode
	)
		on a.ProgramCode = b.ProgramCode

INSERT INTO
	dbo.ComisProgram
	(
		ProgramCode,
		DisciplineCode,
		SubdisciplineCode,
		FieldCode,
		Name,
		Description,
		IsVocational
	)
SELECT
	ProgramCode = code,
	DisciplineCode = substring(code, 1, 2),
	SubdisciplineCode = substring(code, 3, 2),
	FieldCode = substring(code, 5, 2),
	Name = title,
	null,
	null
FROM
	(
		VALUES
		('010000','Agriculture and Natural Resources                                          '),
		('010100','Agriculture Technology and Sciences, General                               '),
		('010110','General Agriculture                                                        '),
		('010200','Animal Science                                                             '),
		('010210','Veterinary Technician (Licensed)                                           '),
		('010220','Artificial Inseminator (Licensed)                                          '),
		('010230','Dairy Science                                                              '),
		('010240','Equine Science                                                             '),
		('010250','Farm Management                                                            '),
		('010300','Plant Science                                                              '),
		('010310','Agricultural Pest Control Adviser and Operator (Licensed)                  '),
		('010400','Viticulture, Enology, and Wine Business                                    '),
		('010900','Horticulture                                                               '),
		('010910','Landscape Design and Maintenance                                           '),
		('010920','Floriculture / Floristry                                                   '),
		('010930','Nursery Technology                                                         '),
		('010940','Turfgrass Technology                                                       '),
		('011200','Agriculture Business, Sales and Service                                    '),
		('011210','Agri-Business (Sales and Service)                                          '),
		('011230','Agricultural Pest Control Advisor                                          '),
		('011240','Animal Health Technician (Licensed)                                        '),
		('011250','Artificial Inseminator (Licensed)                                          '),
		('011260','Farrier                                                                    '),
		('011270','Animal Grooming and Training                                               '),
		('011280','Food Processing                                                            '),
		('011290','Pack Station and Stable Operations                                         '),
		('011300','Food Processing and Related Technologies                                   '),
		('011400','Forestry                                                                   '),
		('011410','Timber Management                                                          '),
		('011420','Forest Protection                                                          '),
		('011500','Natural Resources                                                          '),
		('011510','Parks and Outdoor Recreation                                               '),
		('011520','Wildlife and Fisheries                                                     '),
		('011600','Agricultural Power Equipment Technology                                    '),
		('011610','Equipment and Machinery, General                                           '),
		('011630','Farm Mechanics                                                             '),
		('019900','Other Agriculture and Natural Resources                                    '),
		('020000','Architecture and Environmental Design                                      '),
		('020100','Architecture and Architectural Technology                                  '),
		('020110','Landscape Architecture (Transfer)                                          '),
		('020120','Urban Planning Technology                                                  '),
		('020130','Architectural Model Building                                               '),
		('020300','Interior Design                                                            '),
		('020310','Interior Design Technology                                                 '),
		('029900','Other Architecture and Environmental Design                                '),
		('030100','Environmental Science                                                      '),
		('030200','Environmental Studies                                                      '),
		('030300','Environmental Technology                                                   '),
		('039900','Environmental Sciences and Technologies, Other                             '),
		('040000','Biological Sciences                                                        '),
		('040100','Biology, General                                                           '),
		('040200','Botany, General                                                            '),
		('040300','MicroBiology                                                               '),
		('040700','Zoology, General                                                           '),
		('040800','Natural History                                                            '),
		('041000','Anatomy and Physiology                                                     '),
		('043000','Biotechnology and Biomedical Technology                                    '),
		('049900','Other Biological Sciences                                                  '),
		('050000','Business and Management                                                    '),
		('050100','Business and Commerce, General                                             '),
		('050200','Accounting                                                                 '),
		('050210','Tax Studies                                                                '),
		('050220','Bookkeeping                                                                '),
		('050230','Tax Studies                                                                '),
		('050400','Banking and Finance                                                        '),
		('050410','Banking (Management)                                                       '),
		('050420','Investments and Securities                                                 '),
		('050430','Credit Management                                                          '),
		('050440','Cashiering, Bank Tellering                                                 '),
		('050500','Business Administration                                                    '),
		('050600','Business Management                                                        '),
		('050610','Small Business                                                             '),
		('050620','Hotel/Motel Management                                                     '),
		('050630','Management Development and Supervision                                     '),
		('050640','Small Business and Entrepreneurship                                        '),
		('050650','Retail Store Operations and Management                                     '),
		('050800','International Business and Trade                                           '),
		('050900','Marketing and Distribution                                                 '),
		('050910','Advertising                                                                '),
		('050920','Purchasing                                                                 '),
		('050930','Apparel and Accessories                                                    '),
		('050940','Sales and Salesmanship                                                     '),
		('050960','Display                                                                    '),
		('050970','E-Commerce (Business emphasis)                                             '),
		('050980','Gemology                                                                   '),
		('051000','Logistics and Materials Transportation                                     '),
		('051010','International Trade                                                        '),
		('051030','Traffic Management                                                         '),
		('051070','Air Traffic Control                                                        '),
		('051100','Real Estate                                                                '),
		('051110','Escrow                                                                     '),
		('051200','Insurance                                                                  '),
		('051400','Office Technology/Office Computer Applications                             '),
		('051410','Legal Office Technology                                                    '),
		('051420','Medical Office Technology                                                  '),
		('051430','Court Reporting                                                            '),
		('051440','Office Management                                                          '),
		('051450','Clerical (Including Office Practice)                                       '),
		('051460','Typing                                                                     '),
		('051470','Word Processing                                                            '),
		('051600','Labor and Industrial Relations                                             '),
		('051800','Customer Service                                                           '),
		('059900','Other Business and Management                                              '),
		('060000','Communications                                                             '),
		('060100','Media and Communications, General                                          '),
		('060110','Public Relations                                                           '),
		('060120','Technical Writing                                                          '),
		('060130','Mass Communications                                                        '),
		('060200','Journalism                                                                 '),
		('060300','Radio, Motion Picture and Television                                       '),
		('060310','Television Technician                                                      '),
		('060320','Audio/Visual Technician                                                    '),
		('060400','Radio and Television                                                       '),
		('060410','Radio                                                                      '),
		('060420','Television (including combined TV/Film/Video)                              '),
		('060430','Broadcast Journalism                                                       '),
		('060500','Audio/Visual Technician                                                    '),
		('060600','Public Relations                                                           '),
		('060700','Technical Communication                                                    '),
		('061000','Mass Communications                                                        '),
		('061200','Film Studies                                                               '),
		('061210','Film History and Criticism                                                 '),
		('061220','Film Production                                                            '),
		('061400','Digital Media                                                              '),
		('061410','Multimedia                                                                 '),
		('061420','Electronic Game Design                                                     '),
		('061430','Website Design and Development                                             '),
		('061440','Animation                                                                  '),
		('061450','Desktop Publishing                                                         '),
		('061460','Computer Graphics and Digital Imagery                                      '),
		('069900','Other Media and Communications                                             '),
		('070000','Computer and Information Science                                           '),
		('070100','Information Technology, General                                            '),
		('070110','Maintenance Technician                                                     '),
		('070200','Computer Information Systems                                               '),
		('070210','Software Applications                                                      '),
		('070410','Computer Programming, Business                                             '),
		('070420','Computer Programming, Scientific                                           '),
		('070510','Systems Analysis, Business                                                 '),
		('070520','Systems Analysis, Scientific                                               '),
		('070600','Computer Science (Transfer)                                                '),
		('070700','Computer Software Development                                              '),
		('070710','Computer Programming                                                       '),
		('070720','Database Design and Administration                                         '),
		('070730','Computer Systems Analysis                                                  '),
		('070800','Computer Infrastructure and Support                                        '),
		('070810','Computer Networking                                                        '),
		('070820','Computer Support                                                           '),
		('070900','World Wide Web Administration                                              '),
		('070910','E-Commerce (Technology emphasis)                                           '),
		('079900','Other Information Technology                                               '),
		('080000','Education                                                                  '),
		('080100','Education, General                                                         '),
		('080200','Educational Aide (Teacher Assistant)                                       '),
		('080210','Educational Aide (Teacher Assistant), Bilingual                            '),
		('080820','Special Education - Serv/Aide                                              '),
		('080900','Special Education                                                          '),
		('083500','Physical Education                                                         '),
		('083510','Physical Fitness and Body Movement                                         '),
		('083520','Fitness Trainer                                                            '),
		('083550','Intercollegiate Athletics                                                  '),
		('083560','Coaching                                                                   '),
		('083570','Aquatics and Lifesaving                                                    '),
		('083580','Adapted Physical Education                                                 '),
		('083600','Recreation                                                                 '),
		('083610','Recreation Assistant                                                       '),
		('083700','Health Education                                                           '),
		('083900','Industrial Arts (Transfer)                                                 '),
		('085000','Sign Language                                                              '),
		('085010','Sign Language Interpreting                                                 '),
		('086000','Educational Technology                                                     '),
		('089900','Other Education                                                            '),
		('090000','Engineering & Related Industrial Technology                                '),
		('090100','Engineering, General (requires Calculus) (Transfer)                        '),
		('092400','Engineering Technology, General (requires Trigonometry)                    '),
		('092510','Civil                                                                      '),
		('092520','Design/Drafting                                                            '),
		('092530','Electrical                                                                 '),
		('092540','Electronics                                                                '),
		('092550','Mechanical                                                                 '),
		('092560','ElectroMechanical Technology                                               '),
		('093000','Technologies and Occupational Curricula                                    '),
		('093300','Radiation Technology (Non-Medical)                                         '),
		('093400','Electronics and Electric Technology                                        '),
		('093410','Computer Electronics                                                       '),
		('093420','Industrial Electronics                                                     '),
		('093430','Telecommunications Technology                                              '),
		('093440','Electrical Systems and Power Transmission                                  '),
		('093460','Biomedical Instrumentation                                                 '),
		('093470','Electron Microscopy                                                        '),
		('093480','Laser and Optical Technology                                               '),
		('093500','Electro-Mechanical Technology                                              '),
		('093510','Appliance Repair                                                           '),
		('093520','Business Machine Maintenance                                               '),
		('093530','Vending Machine Repair                                                     '),
		('093540','Industrial Electrical/Mechanical                                           '),
		('093550','Robotics                                                                   '),
		('093600','Printing and Lithography                                                   '),
		('093610','Typesetting and Copy Preparation                                           '),
		('093620','Camera and Stripping                                                       '),
		('093640','Press Operator (Offset and Letter)                                         '),
		('093650','Bindery and Finish Work                                                    '),
		('093710','Tool and Machine Design                                                    '),
		('094300','Instrumentation Technology                                                 '),
		('094310','Instrument Maintenance                                                     '),
		('094330','Vacuum Technology                                                          '),
		('094500','Industrial Systems Technology and Maintenance                              '),
		('094520','Fluid Power/Hydraulic Technology                                           '),
		('094530','Stationary Engineering (Only)                                              '),
		('094540','Refrigeration Systems                                                      '),
		('094600','Environmental Control Technology                                           '),
		('094610','Energy Systems Technology                                                  '),
		('094700','Diesel Technology                                                          '),
		('094710','Diesel Mechanic                                                            '),
		('094720','Heavy Equipment Maintenance                                                '),
		('094730','Heavy Equipment Operation                                                  '),
		('094740','Railroad and Light Rail Operations                                         '),
		('094750','Truck and Bus Driving                                                      '),
		('094800','Automotive Technology                                                      '),
		('094810','Auto Mechanics                                                             '),
		('094830','Motorcycle, Outboard and Small Engine Repair                               '),
		('094840','Alternative Fuels and Advanced Transportation Technology                   '),
		('094850','Recreational Vehicle Service                                               '),
		('094900','Automotive Collision Repair                                                '),
		('094910','Upholstery Repair - Automotive                                             '),
		('095000','Aeronautical and Aviation Technology                                       '),
		('095010','Aviation Airframe Mechanics                                                '),
		('095020','Aviation Powerplant Mechanics                                              '),
		('095040','Aircraft Electronics (Avionics)                                            '),
		('095050','Aircraft Fabrication                                                       '),
		('095200','Construction Crafts Technology                                             '),
		('095210','Carpentry                                                                  '),
		('095220','Electrical                                                                 '),
		('095230','Plumbing, Pipefitting and Steamfitting                                     '),
		('095240','Glazing                                                                    '),
		('095250','Mill and Cabinet Work                                                      '),
		('095260','Masonry, Tile, Cement, Lath and Plaster                                    '),
		('095270','Painting, Decorating, and Flooring                                         '),
		('095280','Drywall and Insulation                                                     '),
		('095290','Roofing                                                                    '),
		('095300','Drafting Technology                                                        '),
		('095310','Architectural Drafting                                                     '),
		('095320','Civil Drafting                                                             '),
		('095330','Electrical, Electronic, and Electro-Mechanical Drafting                    '),
		('095340','Mechanical Drafting                                                        '),
		('095350','Industrial Design                                                          '),
		('095360','Technical Illustration                                                     '),
		('095400','Chemical Technology                                                        '),
		('095420','Plastics and Composites                                                    '),
		('095430','Petroleum Technology                                                       '),
		('095500','Laboratory Science Technology                                              '),
		('095600','Manufacturing and Industrial Technology                                    '),
		('095610','Metallurgical Technology                                                   '),
		('095620','Metalworking                                                               '),
		('095630','Machining and Machine Tools                                                '),
		('095640','Sheet Metal and Structural Metal                                           '),
		('095650','Welding Technology                                                         '),
		('095660','Woodworking                                                                '),
		('095670','Industrial and Occupational Safety and Health                              '),
		('095680','Industrial Quality Control                                                 '),
		('095690','Musical Instrument Repair                                                  '),
		('095700','Civil and Construction Management Technology                               '),
		('095710','Construction Management                                                    '),
		('095720','Construction Inspection                                                    '),
		('095730','Surveying                                                                  '),
		('095800','Water and Wastewater Technology                                            '),
		('095810','Water and Wastewater                                                       '),
		('095840','Solid Waste Management                                                     '),
		('095900','Marine Technology                                                          '),
		('095910','Diving and Underwater Safety                                               '),
		('096100','Optics                                                                     '),
		('096200','Musical Instrument Repair                                                  '),
		('099900','Other Engineering and Related Industrial Technologies                      '),
		('100000','Fine and Applied Arts                                                      '),
		('100100','Fine Arts, General                                                         '),
		('100200','Art                                                                        '),
		('100210','Painting and Drawing                                                       '),
		('100220','Sculpture                                                                  '),
		('100230','Ceramics                                                                   '),
		('100400','Music                                                                      '),
		('100430','Commercial Music                                                           '),
		('100500','Commercial Music                                                           '),
		('100600','Technical Theater                                                          '),
		('100700','Dramatic Arts                                                              '),
		('100710','Technical Theater                                                          '),
		('100800','Dance                                                                      '),
		('100810','Commercial Dance                                                           '),
		('100900','Applied Design                                                             '),
		('100910','Jewelry                                                                    '),
		('100920','Commercial Art                                                             '),
		('101000','Cinematography                                                             '),
		('101100','Photography                                                                '),
		('101110','Photo Lab Technician                                                       '),
		('101130','Biological Photography Technician                                          '),
		('101140','Commercial Photography                                                     '),
		('101200','Applied Photography                                                        '),
		('101300','Commercial Art                                                             '),
		('103000','Graphic Art and Design                                                     '),
		('103010','Technical Illustration                                                     '),
		('103020','Computer Graphics                                                          '),
		('109900','Other Fine and Applied Arts                                                '),
		('110000','Foreign Language                                                           '),
		('110100','Foreign Languages, General                                                 '),
		('110200','French                                                                     '),
		('110300','German                                                                     '),
		('110400','Italian                                                                    '),
		('110500','Spanish                                                                    '),
		('110600','Russian                                                                    '),
		('110700','Chinese                                                                    '),
		('110800','Japanese                                                                   '),
		('110900','Latin                                                                      '),
		('111000','Greek                                                                      '),
		('111100','Hebrew and Semitic                                                         '),
		('111200','Arabic                                                                     '),
		('111600','African Languages (Non-Semitic)                                            '),
		('111700','Asian, South Asian, and Pacific Islands (Chinese and Japanese excluded)    '),
		('111710','Filipino (Tagalog)                                                         '),
		('111720','Vietnamese                                                                 '),
		('111730','Korean                                                                     '),
		('111900','Portuguese                                                                 '),
		('119900','Other Foreign Languages                                                    '),
		('120000','Health                                                                     '),
		('120100','Health Occupations, General                                                '),
		('120110','Core Curriculum for all Health                                             '),
		('120120','Medicine, General                                                          '),
		('120200','Hospital and Health Care Administration                                    '),
		('120210','Institutional Management Technician                                        '),
		('120220','Hospital Staff Development                                                 '),
		('120310','Nursing, RN                                                                '),
		('120340','Obstetrical Technology                                                     '),
		('120430','Dental Technician                                                          '),
		('120500','Medical Laboratory Technology                                              '),
		('120510','Phlebotomy                                                                 '),
		('120520','Medical Equipment Technician                                               '),
		('120530','Medical Lab Assistant                                                      '),
		('120600','Physicians Assistant                                                       '),
		('120610','Physicians Assistant, General                                              '),
		('120630','Physicians Assistant, Primary Care                                         '),
		('120700','Electro-Diagnostic Technology                                              '),
		('120710','Electro-Diagnostic Technology                                              '),
		('120730','Respiratory Therapy                                                        '),
		('120740','Cardio-Pulmonary Technician                                                '),
		('120750','Orthopedic Assistant                                                       '),
		('120770','Surgical Technician/O.R. Nursing                                           '),
		('120780','Dialysis Technician                                                        '),
		('120800','Medical Assisting                                                          '),
		('120810','Clinical Medical Assisting                                                 '),
		('120820','Administrative Medical Assisting                                           '),
		('120830','Health Facility Unit Coordinator                                           '),
		('120900','Hospital Central Service Technician                                        '),
		('120910','Optical Technician                                                         '),
		('121000','Respiratory Care/Therapy                                                   '),
		('121100','Polysomnography                                                            '),
		('121110','Pharmacy Technician                                                        '),
		('121200','Electro-Neurodiagnostic Technology                                         '),
		('121210','Athletic Trainer                                                           '),
		('121220','Physical Therapy Assistant                                                 '),
		('121300','Cardiovascular Technician                                                  '),
		('121400','Orthopedic Assistant                                                       '),
		('121500','Electrocardiography                                                        '),
		('121510','Medical Record Technician                                                  '),
		('121600','Podiatry                                                                   '),
		('121700','Surgical Technician                                                        '),
		('121800','Occupational Therapy Technology                                            '),
		('121900','Optical Technology                                                         '),
		('122000','Speech/Language Pathology and Audiology                                    '),
		('122100','Pharmacy Technology                                                        '),
		('122200','Physical Therapist Assistant                                               '),
		('122230','Recreation Therapy                                                         '),
		('122300','Health Information Technology                                              '),
		('122310','Health Information Coding                                                  '),
		('122400','School Health Clerk                                                        '),
		('122500','Radiologic Technology                                                      '),
		('122520','X-Ray Technician                                                           '),
		('122600','Radiation Therapy Technician                                               '),
		('122700','Diagnostic Medical Sonography                                              '),
		('122800','Athletic Training and Sports Medicine                                      '),
		('123000','Nursing                                                                    '),
		('123010','Registered Nursing                                                         '),
		('123020','Licensed Vocational Nursing                                                '),
		('123030','Certified Nurse Assistant                                                  '),
		('123080','Home Health Aide                                                           '),
		('123900','Psychiatric Technician                                                     '),
		('123920','Mental Health Aide                                                         '),
		('123930','Residential Services Provider                                              '),
		('124000','Dental Occupations                                                         '),
		('124010','Dental Assistant                                                           '),
		('124020','Dental Hygienist                                                           '),
		('124030','Dental Laboratory Technician                                               '),
		('124600','Recreation Therapy                                                         '),
		('125000','Emergency Medical Services                                                 '),
		('125010','Paramedical Technician                                                     '),
		('125020','EMT                                                                        '),
		('125100','Paramedic                                                                  '),
		('125500','Mortuary Science                                                           '),
		('126000','Health Professions, Transfer Core Curriculum                               '),
		('126100','Community Health Care Worker                                               '),
		('126200','Massage Therapy                                                            '),
		('127000','Kinesiology                                                                '),
		('129900','Other Health Occupations                                                   '),
		('130000','Consumer Education & Home Economics                                        '),
		('130100','Family and Consumer Sciences, General                                      '),
		('130110','Consumer Services                                                          '),
		('130200','Interior Design and Merchandising                                          '),
		('130210','Occupational Home Furnishing                                               '),
		('130220','Floristry                                                                  '),
		('130300','Fashion                                                                    '),
		('130310','Fashion Design                                                             '),
		('130320','Fashion Merchandising                                                      '),
		('130330','Fashion Production                                                         '),
		('130340','Industrial Sewing                                                          '),
		('130410','Consumer and Homemaking HM EC                                              '),
		('130420','Child Development Care and Guidance                                        '),
		('130430','Clothing and Textiles                                                      '),
		('130440','Consumer Education                                                         '),
		('130450','Family/Individual Health                                                   '),
		('130460','Family Living and Parenthood                                               '),
		('130470','Food and Nutrition                                                         '),
		('130480','Home Management                                                            '),
		('130490','Housing, Home Furnishings and Equipment                                    '),
		('130500','Child Development/Early Care and Education                                 '),
		('130510','Child and Adolescent Development                                           '),
		('130520','Children with Special Needs                                                '),
		('130540','Preshool Age Children                                                      '),
		('130550','The School Age Child                                                       '),
		('130560','Parenting and Family Education                                             '),
		('130570','Foster and Kinship Care                                                    '),
		('130580','Child Development Administration and Management                            '),
		('130590','Infants and Toddlers                                                       '),
		('130600','Nutrition, Foods, and Culinary Arts                                        '),
		('130620','Dietetic Services and Management                                           '),
		('130630','Culinary Arts                                                              '),
		('130640','Nutrition, Health and Fitness                                              '),
		('130650','Food and Equipment Demonstration                                           '),
		('130660','Dietetic Technology                                                        '),
		('130700','Hospitality                                                                '),
		('130710','Restaurant and Food Services and Management                                '),
		('130720','Lodging Management                                                         '),
		('130730','Resort and Club Management                                                 '),
		('130800','Family Studies                                                             '),
		('130900','Gerontology                                                                '),
		('139900','Other Family and Consumer Sciences                                         '),
		('140000','Law                                                                        '),
		('140100','Law, General                                                               '),
		('140110','Legal Assistant(Judicial Aide)                                             '),
		('140200','Paralegal                                                                  '),
		('149900','Other Law                                                                  '),
		('150000','Humanities                                                                 '),
		('150100','English                                                                    '),
		('150110','Linguistics                                                                '),
		('150200','Language Arts                                                              '),
		('150300','Comparative Literature                                                     '),
		('150400','Classics                                                                   '),
		('150600','Speech Communication                                                       '),
		('150700','Creative Writing                                                           '),
		('150900','Philosophy                                                                 '),
		('151000','Religious Studies                                                          '),
		('152000','Reading                                                                    '),
		('159900','Other Humanities                                                           '),
		('160000','Library Science                                                            '),
		('160100','Library Science, General                                                   '),
		('160110','Library Technician OR Aide                                                 '),
		('160200','Library Technician (Aide)                                                  '),
		('169900','Other Library Science                                                      '),
		('170000','Mathematics                                                                '),
		('170100','Mathematics, General                                                       '),
		('170110','Mathematics, General (Non-majors)                                          '),
		('170170','Technical Math                                                             '),
		('170200','Mathematics Skills                                                         '),
		('179900','Other Mathematics                                                          '),
		('180000','Military Studies                                                           '),
		('180100','Military Science                                                           '),
		('189900','Other Military Studies                                                     '),
		('190000','Physical Sciences                                                          '),
		('190100','Physical Sciences, General                                                 '),
		('190200','Physics, General                                                           '),
		('190500','Chemistry, General                                                         '),
		('191100','Astronomy                                                                  '),
		('191400','Geology                                                                    '),
		('191410','Geologic Technician                                                        '),
		('191900','Oceangraphy                                                                '),
		('191910','Oceangraphy Technician                                                     '),
		('192000','Ocean Technology                                                           '),
		('193000','Earth Science                                                              '),
		('199900','Other Physical Sciences                                                    '),
		('200000','Psychology                                                                 '),
		('200100','Psychology, General                                                        '),
		('200300','Behavioral Science                                                         '),
		('209900','Other Psychology                                                           '),
		('210000','Public Affairs & Services                                                  '),
		('210100','Community Services, General                                                '),
		('210130','Diving and Underwater Safety                                               '),
		('210150','Alcohol and Controlled Substances                                          '),
		('210200','Public Administration                                                      '),
		('210210','Public Works                                                               '),
		('210220','Ski Patrol Technician                                                      '),
		('210240','Public Works and Utilities                                                 '),
		('210250','Street Maintenance                                                         '),
		('210260','Search and Rescue                                                          '),
		('210300','Parks and Recreation Management                                            '),
		('210400','Human Services                                                             '),
		('210410','Social Work Aide                                                           '),
		('210430','Community Health Worker                                                    '),
		('210440','Alcohol and Controlled Substances                                          '),
		('210450','Disability Services                                                        '),
		('210500','Administration of Justice                                                  '),
		('210510','Corrections                                                                '),
		('210520','Probation and Parole                                                       '),
		('210530','Industrial and Transportation Security                                     '),
		('210540','Forensics, Evidence, and Investigation                                     '),
		('210550','Police Academy                                                             '),
		('210560','Investigators                                                              '),
		('210700','Human Services                                                             '),
		('210710','Early Childhood Education Aide                                             '),
		('210720','Child Development                                                          '),
		('210730','Parent Education                                                           '),
		('210740','Gerontology Aide                                                           '),
		('210770','Counselor Aide                                                             '),
		('213300','Fire Technology                                                            '),
		('213310','Wildland Fire Technology                                                   '),
		('213320','Hazardous Materials Handling                                               '),
		('213350','Fire Academy                                                               '),
		('214000','Legal and Community Interpretation                                         '),
		('219900','Other Public and Protective Services                                       '),
		('219910','Environmental Enforcement                                                  '),
		('220000','Social Sciences                                                            '),
		('220100','Social Sciences, General                                                   '),
		('220110','Women’s Studies                                                            '),
		('220120','American Studies                                                           '),
		('220130','Social Justice Studies                                                     '),
		('220140','Gay, Lesbian, Bisexual and Transgender Studies                             '),
		('220200','Anthropology                                                               '),
		('220220','Archaeology                                                                '),
		('220300','Ethnic Studies                                                             '),
		('220400','Economics                                                                  '),
		('220500','History                                                                    '),
		('220600','Geography                                                                  '),
		('220610','Geographic Information Systems                                             '),
		('220700','Political Science                                                          '),
		('220710','Student Government                                                         '),
		('220800','Sociology                                                                  '),
		('221000','International Studies                                                      '),
		('221010','Area Studies                                                               '),
		('221020','Global Studies                                                             '),
		('229900','Other Social Sciences                                                      '),
		('300000','Commercial Services                                                        '),
		('300100','Commercial Services, General                                               '),
		('300200','Food Service Technology                                                    '),
		('300210','Chef Training                                                              '),
		('300220','Institutional Cook                                                         '),
		('300230','Waiter/Waitress                                                            '),
		('300240','Catering                                                                   '),
		('300250','Restaurant/Food Service Management                                         '),
		('300260','Baking                                                                     '),
		('300400','Institutional Housekeeping                                                 '),
		('300500','Custodial Services                                                         '),
		('300700','Cosmetology and Barbering                                                  '),
		('300800','Dry Cleaning                                                               '),
		('300900','Travel Services and Tourism                                                '),
		('300930','Travel Agency Operations                                                   '),
		('300940','Recreation and Tourism                                                     '),
		('302000','Aviation and Airport Management and Services                               '),
		('302010','Aviation and Airport Management                                            '),
		('302020','Piloting                                                                   '),
		('302030','Air Traffic Control                                                        '),
		('302040','Flight Attendant                                                           '),
		('309900','Other Commercial Services                                                  '),
		('490000','Interdisciplinary Studies                                                  '),
		('490100','Liberal Arts and Sciences, General                                         '),
		('490110','Transfer Studies                                                           '),
		('490120','Liberal Studies                                                            '),
		('490200','Biological and Physical Sciences (and Mathematics)                         '),
		('490300','Humanities                                                                 '),
		('490310','Humanities and Fine Arts                                                   '),
		('490330','Humanities and Social Sciences                                             '),
		('490400','Engineering Technology and Liberal Arts                                    '),
		('493000','General Studies                                                            '),
		('493009','Supervised Tutoring                                                        '),
		('493010','Career Guidance and Orientation                                            '),
		('493011','Interpersonal Skills                                                       '),
		('493012','Job Seeking/Changing Skills                                                '),
		('493013','Academic Guidance                                                          '),
		('493014','Study Skills                                                               '),
		('493020','Communication Skills                                                       '),
		('493021','Writing                                                                    '),
		('493022','Speech                                                                     '),
		('493030','Learning Skills, Handicapped                                               '),
		('493031','Living Skills, Handicapped                                                 '),
		('493032','Learning Skills, Learning Disabled                                         '),
		('493033','Learning Skills, Speech Impaired                                           '),
		('493040','Career Technical Computational Skills                                      '),
		('493041','Pre-Algebra (Basic Math/Arithmetic)                                        '),
		('493042','Elementary Algebra (not degree applicable)                                 '),
		('493060','Elementary Education (Grades 1-8)                                          '),
		('493062','Secondary Education (Grades 9-12) and G.E.D.                               '),
		('493070','Reading Skills Development                                                 '),
		('493071','Reading Skills, College Level (including Speed Reading)                    '),
		('493072','Leadership Skills Development                                              '),
		('493080','English as a Second Language - Intermediate                                '),
		('493081','English as a Second Language - Advanced (not degree applicable)            '),
		('493082','English as a Second Language - Elementary                                  '),
		('493083','English as a Second Language - Degree Applicable                           '),
		('493084','English as a Second Language - Writing                                     '),
		('493085','English as a Second Language - Reading                                     '),
		('493086','English as a Second Language - Listening and Speaking                      '),
		('493087','English as a Second Language - Integrated                                  '),
		('493090','Citizenship / ESL Civics                                                   '),
		('493091','ESL Civics                                                                 '),
		('493100','Vocational ESL                                                             '),
		('493200','General Work Experience                                                    '),
		('499900','Other Interdisciplinary Studies                                            '),
		('999999','Unknown                                                                    ')
	) t (code, title)
WHERE
	not exists (
		SELECT
			0
		FROM
			dbo.ComisProgram p
		WHERE
			p.ProgramCode = t.code
	);

GO

CREATE TABLE
	dbo.Subdiscipline
	(
		SubdisciplineId   smallint    not null identity(1,1),
		ContentId         smallint    not null,
		SubdisciplineCode char(6)     not null,
		Label             varchar(75) not null
	);

GO

ALTER TABLE
	dbo.Subdiscipline
ADD CONSTRAINT
	PK_Subdiscipline
PRIMARY KEY CLUSTERED
	(
		SubdisciplineId
	);

ALTER TABLE
	dbo.Subdiscipline
ADD CONSTRAINT
	UQ_Subdiscipline__SubdisciplineCode
UNIQUE
	(
		SubdisciplineCode
	);

ALTER TABLE
	dbo.Subdiscipline
ADD CONSTRAINT
	FK_Subdiscipline__ContentId
FOREIGN KEY
	(
		ContentId
	)
REFERENCES
	dbo.Content
	(
		ContentId
	);

CREATE NONCLUSTERED INDEX
	IX_Subdiscipline__ContentId
ON
	dbo.Subdiscipline
	(
		ContentId
	);

GO

INSERT
	dbo.Subdiscipline
	(
		SubdisciplineCode,
		ContentId,
		Label
	)
SELECT
	SubdisciplineCode = ProgramCode,
	ContentId         = c.ContentId,
	Label             = p.Name
FROM
	dbo.Content c
	cross join
	dbo.ComisProgram p
	left outer join
	dbo.Subdiscipline sd
		on left(sd.SubdisciplineCode, 4) = left(p.ProgramCode, 4)
WHERE
	c.Label = 'Subdiscipline'
	and right(p.ProgramCode, 2) = '00';

GO

CREATE TABLE
	dbo.Program
	(
		ProgramId         smallint     not null identity(1,1),
		ContentId         smallint     not null,
		ProgramCode       char(6)      not null,
		SectorId          smallint         null,
		SubdisciplineId   smallint     not null,
		VocationId        smallint     not null,
		Label             varchar(75)  not null,
		Description       varchar(610)     null
	);

GO

ALTER TABLE
	dbo.Program
ADD CONSTRAINT
	PK_Program
PRIMARY KEY
	(
		ProgramCode
	);

ALTER TABLE
	dbo.Program
ADD CONSTRAINT
	UQ_Program__ProgramId
UNIQUE
	(
		ProgramId
	);

ALTER TABLE
	dbo.Program
ADD CONSTRAINT
	FK_Program__ContentId
FOREIGN KEY
	(
		ContentId
	)
REFERENCES
	dbo.Content
	(
		ContentId
	);

CREATE NONCLUSTERED INDEX
	IX_Program__ContentId
ON
	dbo.Program
	(
		ContentId
	)
INCLUDE
	(
		Label
	);

ALTER TABLE
	dbo.Program
ADD CONSTRAINT
	FK_Program__SectorId
FOREIGN KEY
	(
		SectorId
	)
REFERENCES
	dbo.Sector
	(
		SectorId
	);

CREATE NONCLUSTERED INDEX
	IX_Program__SectorId
ON
	dbo.Program
	(
		SectorId
	)
INCLUDE
	(
		Label
	);

ALTER TABLE
	dbo.Program
ADD CONSTRAINT
	FK_Program__SubdisciplineId
FOREIGN KEY
	(
		SubdisciplineId
	)
REFERENCES
	dbo.Subdiscipline
	(
		SubdisciplineId
	);

CREATE NONCLUSTERED INDEX
	IX_Program__SubdisciplineId
ON
	dbo.Program
	(
		SubdisciplineId
	)
INCLUDE
	(
		Label
	);

ALTER TABLE
	dbo.Program
ADD CONSTRAINT
	FK_Program__VocationId
FOREIGN KEY
	(
		VocationId
	)
REFERENCES
	dbo.Vocation
	(
		VocationId
	);

CREATE NONCLUSTERED INDEX
	IX_Program__VocationId
ON
	dbo.Program
	(
		VocationId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	dbo.Program
	(
		ContentId,
		ProgramCode,
		SubdisciplineId,
		SectorId,
		VocationId,
		Label,
		Description
	)
SELECT
	ContentId       = c.ContentId,
	ProgramCode     = p.ProgramCode,
	SubdisciplineId = sd.SubdisciplineId,
	SectorId        = isnull(s.SectorId, 14),
	VocationId      = case when p.IsVocational = 1 then 1 else 2 end,
	Label           = p.Name,
	Description     = p.Description
FROM
	dbo.Content c
	cross join
	dbo.ComisProgram p
	inner join
	dbo.Subdiscipline sd
		on left(sd.SubdisciplineCode, 4) = left(p.ProgramCode, 4)
	left outer join
	(
		VALUES
		('010100','Agriculture Technology and Sciences, General','Agriculture, Water and Environmental Technologies'),
		('010200','Animal Science','Agriculture, Water and Environmental Technologies'),
		('010210','Veterinary Technician (Licensed)','Agriculture, Water and Environmental Technologies'),
		('010220','Artificial Inseminator','Agriculture, Water and Environmental Technologies'),
		('010230','Dairy Science','Agriculture, Water and Environmental Technologies'),
		('010240','Equine Science','Agriculture, Water and Environmental Technologies'),
		('010300','Plant Science','Agriculture, Water and Environmental Technologies'),
		('010310','Agricultural Pest Control Advisor and Operator','Agriculture, Water and Environmental Technologies'),
		('010400','Viticulture, Enology, and Wine Business','Agriculture, Water and Environmental Technologies'),
		('010900','Horticulture','Agriculture, Water and Environmental Technologies'),
		('010910','Landscape Design and Maintenance','Agriculture, Water and Environmental Technologies'),
		('010920','Floriculture -Floristry','Agriculture, Water and Environmental Technologies'),
		('010930','Nursery Technology','Agriculture, Water and Environmental Technologies'),
		('010940','Turfgrass Technology','Agriculture, Water and Environmental Technologies'),
		('011200','Agriculture Business, Sales and Service','Agriculture, Water and Environmental Technologies'),
		('011300','Food Processing and Related Technologies','Agriculture, Water and Environmental Technologies'),
		('011400','Forestry','Agriculture, Water and Environmental Technologies'),
		('011500','Natural Resources','Agriculture, Water and Environmental Technologies'),
		('011510','Parks and Outdoor Recreation','Agriculture, Water and Environmental Technologies'),
		('011520','Wildlife and Fisheries','Agriculture, Water and Environmental Technologies'),
		('011600','Agricultural Power Equipment Technology','Agriculture, Water and Environmental Technologies'),
		('019900','Other Agriculture and Natural Resources','Agriculture, Water and Environmental Technologies'),
		('020100','Architecture and Architectural Technology','Energy, Construction and Utilities'),
		('029900','Other Architecture and Environmental Design','Energy, Construction and Utilities'),
		('030300','Environmental Technology','Agriculture, Water and Environmental Technologies'),
		('043000','Biotechnology and Biomedical Technology','Life Sciences - Biotechnology'),
		('050100','Business and Commerce, General','Business and Entrepreneurship'),
		('050200','Accounting','Business and Entrepreneurship'),
		('050210','Tax Studies','Business and Entrepreneurship'),
		('050400','Banking and Finance','Business and Entrepreneurship'),
		('050500','Business Administration','Business and Entrepreneurship'),
		('050600','Business Management','Business and Entrepreneurship'),
		('050630','Management Development and Supervision','Business and Entrepreneurship'),
		('050640','Small Business and Entrepreneurship','Business and Entrepreneurship'),
		('050650','Retail Store Operations and Management','Retail, Hospitality and Tourism'),
		('050800','International Business and Trade','Global Trade'),
		('050900','Marketing and Distribution','Business and Entrepreneurship'),
		('050910','Advertising','Business and Entrepreneurship'),
		('050920','Purchasing','Business and Entrepreneurship'),
		('050940','Sales and Salesmanship','Business and Entrepreneurship'),
		('050960','Display','Retail, Hospitality and Tourism'),
		('050970','e-commerce (business emphasis)','Business and Entrepreneurship'),
		('051000','Logistics and Materials Transportation','Advanced Transportation and Logistics'),
		('051100','Real Estate','Business and Entrepreneurship'),
		('051110','Escrow','Business and Entrepreneurship'),
		('051200','Insurance','Business and Entrepreneurship'),
		('051400','Office Technology-Office Computer Applications','Information and Communication Technologies (ICT) - Digital Media'),
		('051410','Legal Office Technology','Business and Entrepreneurship'),
		('051420','Medical Office Technology','Health'),
		('051430','Court Reporting','Unassigned'),
		('051440','Office Management','Retail, Hospitality and Tourism'),
		('051600','Labor and Industrial Relations','Retail, Hospitality and Tourism'),
		('051800','Customer Service','Business and Entrepreneurship'),
		('059900','Other Business and Management','Business and Entrepreneurship'),
		('060200','Journalism','Information and Communication Technologies (ICT) - Digital Media'),
		('060400','Radio and Television','Information and Communication Technologies (ICT) - Digital Media'),
		('060410','Radio','Information and Communication Technologies (ICT) - Digital Media'),
		('060420','Television (including combined TV-film-video)','Information and Communication Technologies (ICT) - Digital Media'),
		('060430','Broadcast Journalism','Information and Communication Technologies (ICT) - Digital Media'),
		('060600','Public Relations','Retail, Hospitality and Tourism'),
		('060700','Technical Communication','Information and Communication Technologies (ICT) - Digital Media'),
		('061000','Mass Communications','Information and Communication Technologies (ICT) - Digital Media'),
		('061220','Film Production','Information and Communication Technologies (ICT) - Digital Media'),
		('061400','Digital Media','Information and Communication Technologies (ICT) - Digital Media'),
		('061410','Multimedia','Information and Communication Technologies (ICT) - Digital Media'),
		('061420','Electronic Game Design','Information and Communication Technologies (ICT) - Digital Media'),
		('061430','Website Design and Development','Information and Communication Technologies (ICT) - Digital Media'),
		('061440','Animation','Information and Communication Technologies (ICT) - Digital Media'),
		('061450','Desktop Publishing','Information and Communication Technologies (ICT) - Digital Media'),
		('061460','Computer Graphics and Digital Imagery','Information and Communication Technologies (ICT) - Digital Media'),
		('069900','Other Media and Communications','Information and Communication Technologies (ICT) - Digital Media'),
		('070100','Information Technology, General','Information and Communication Technologies (ICT) - Digital Media'),
		('070200','Computer Information Systems','Information and Communication Technologies (ICT) - Digital Media'),
		('070210','Software Applications','Information and Communication Technologies (ICT) - Digital Media'),
		('070700','Computer Software Development','Information and Communication Technologies (ICT) - Digital Media'),
		('070710','Computer Programming','Information and Communication Technologies (ICT) - Digital Media'),
		('070720','Database Design and Administration','Information and Communication Technologies (ICT) - Digital Media'),
		('070730','Computer Systems Analysis','Information and Communication Technologies (ICT) - Digital Media'),
		('070800','Computer Infrastructure and Support','Information and Communication Technologies (ICT) - Digital Media'),
		('070810','Computer Networking','Information and Communication Technologies (ICT) - Digital Media'),
		('070820','Computer Support','Information and Communication Technologies (ICT) - Digital Media'),
		('070900','World Wide Web Administration','Information and Communication Technologies (ICT) - Digital Media'),
		('070910','E-Commerce (technology emphasis)','Information and Communication Technologies (ICT) - Digital Media'),
		('079900','Other Information Technology','Information and Communication Technologies (ICT) - Digital Media'),
		('080200','Educational Aide (Teacher Assistant)','Education and Human Development'),
		('080210','Educational Aide (Teacher Assistant), Bilingual','Education and Human Development'),
		('080900','Special Education','Education and Human Development'),
		('083520','Fitness Trainer','Unassigned'),
		('083560','Coaching','Education and Human Development'),
		('083570','Aquatics and Lifesaving','Retail, Hospitality and Tourism'),
		('083600','Recreation','Education and Human Development'),
		('083610','Recreation Assistant','Education and Human Development'),
		('085010','Sign Language Interpreting','Education and Human Development'),
		('086000','Educational Technology','Education and Human Development'),
		('089900','Other Education','Education and Human Development'),
		('092400','Engineering Technology, General','Advanced Manufacturing'),
		('093400','Electronics and Electric Technology','Advanced Manufacturing'),
		('093410','Computer Electronics','Advanced Manufacturing'),
		('093420','Industrial Electronics','Advanced Manufacturing'),
		('093430','Telecommunications Technology','Information and Communication Technologies (ICT) - Digital Media'),
		('093440','Electrical Systems and Power Transmission','Energy, Construction and Utilities'),
		('093460','Biomedical Instrumentation','Life Sciences - Biotechnology'),
		('093470','Electron Microscopy','Life Sciences - Biotechnology'),
		('093480','Laser and Optical Technology','Advanced Manufacturing'),
		('093500','Electro-Mechanical Technology','Energy, Construction and Utilities'),
		('093510','Appliance Repair','Advanced Manufacturing'),
		('093600','Printing and Lithography','Advanced Manufacturing'),
		('094300','Instrumentation Technology','Advanced Manufacturing'),
		('094330','Vacuum Technology','Advanced Manufacturing'),
		('094500','Industrial Systems Technology and Maintenance','Advanced Manufacturing'),
		('094600','Environmental Control Technology','Energy, Construction and Utilities'),
		('094610','Energy Systems Technology','Energy, Construction and Utilities'),
		('094700','Diesel Technology','Advanced Transportation and Logistics'),
		('094720','Heavy Equipment Maintenance','Advanced Transportation and Logistics'),
		('094730','Heavy Equipment Operation','Advanced Transportation and Logistics'),
		('094740','Railroad and Light Rail Operations','Advanced Transportation and Logistics'),
		('094750','Truck and Bus Driving','Advanced Transportation and Logistics'),
		('094800','Automotive Technology','Advanced Transportation and Logistics'),
		('094830','Motorcycle, Outboard and Small Engine Repair','Advanced Transportation and Logistics'),
		('094840','Alternative Fuels and Advanced Transportation Technology','Advanced Transportation and Logistics'),
		('094850','Recreational Vehicle Service','Advanced Transportation and Logistics'),
		('094900','Automotive Collision Repair','Advanced Transportation and Logistics'),
		('094910','Upholstery Repair - Automotive','Advanced Transportation and Logistics'),
		('095000','Aeronautical and Aviation Technology','Advanced Manufacturing'),
		('095010','Aviation Airframe Mechanics','Advanced Transportation and Logistics'),
		('095020','Aviation Powerplant Mechanics','Advanced Transportation and Logistics'),
		('095040','Aircraft Electronics (Avionics)','Advanced Manufacturing'),
		('095050','Aircraft Fabrication','Advanced Manufacturing'),
		('095200','Construction Crafts Technology','Energy, Construction and Utilities'),
		('095210','Carpentry','Energy, Construction and Utilities'),
		('095220','Electrical','Energy, Construction and Utilities'),
		('095230','Plumbing, Pipefitting and Steamfitting','Energy, Construction and Utilities'),
		('095240','Glazing','Energy, Construction and Utilities'),
		('095250','Mill and Cabinet Work','Energy, Construction and Utilities'),
		('095260','Masonry, Tile, Cement, Lath and Plaster','Energy, Construction and Utilities'),
		('095270','Painting, Decorating, and Flooring','Energy, Construction and Utilities'),
		('095280','Drywall and Insulation','Energy, Construction and Utilities'),
		('095290','Roofing','Energy, Construction and Utilities'),
		('095300','Drafting Technology','Energy, Construction and Utilities'),
		('095310','Architectural Drafting','Energy, Construction and Utilities'),
		('095320','Civil Drafting','Energy, Construction and Utilities'),
		('095330','Electrical, Electronic, and Electro-Mechanical Drafting','Advanced Manufacturing'),
		('095340','Mechanical Drafting','Advanced Manufacturing'),
		('095360','Technical Illustration','Advanced Manufacturing'),
		('095400','Chemical Technology','Life Sciences - Biotechnology'),
		('095420','Plastics and Composites','Advanced Manufacturing'),
		('095430','Petroleum Technology','Advanced Manufacturing'),
		('095500','Laboratory Science Technology','Life Sciences - Biotechnology'),
		('095600','Manufacturing and Industrial Technology','Advanced Manufacturing'),
		('095630','Machining and Machine Tools','Advanced Manufacturing'),
		('095640','Sheet Metal and Structural Metal','Energy, Construction and Utilities'),
		('095650','Welding Technology','Advanced Manufacturing'),
		('095670','Industrial and Occupational Safety and Health','Advanced Manufacturing'),
		('095680','Industrial Quality Control','Advanced Manufacturing'),
		('095700','Civil and Construction Management Technology','Energy, Construction and Utilities'),
		('095720','Construction Inspection','Energy, Construction and Utilities'),
		('095730','Surveying','Advanced Manufacturing'),
		('095800','Water and Wastewater Technology','Energy, Construction and Utilities'),
		('095900','Marine Technology','Advanced Transportation and Logistics'),
		('095910','Diving and Underwater Safety','Retail, Hospitality and Tourism'),
		('096100','Optics','Advanced Manufacturing'),
		('096200','Musical Instrument Repair','Unassigned'),
		('099900','Other Engineering and Related Industrial Technologies','Unassigned'),
		('100500','Commercial Music','Information and Communication Technologies (ICT) - Digital Media'),
		('100600','Technical Theater','Unassigned'),
		('100810','Commercial Dance','Unassigned'),
		('100900','Applied Design','Unassigned'),
		('101200','Applied Photography','Information and Communication Technologies (ICT) - Digital Media'),
		('101300','Commercial Art','Information and Communication Technologies (ICT) - Digital Media'),
		('103000','Graphic Art and Design','Information and Communication Technologies (ICT) - Digital Media'),
		('109900','Other Fine and Applied Arts','Unassigned'),
		('120100','Health Occupations, General','Health'),
		('120200','Hospital and Health Care Administration','Health'),
		('120500','Medical Laboratory Technology','Health'),
		('120510','Phlebotomy','Health'),
		('120600','Physicians Assistant','Health'),
		('120800','Medical Assisting','Health'),
		('120810','Clinical Medical Assisting','Health'),
		('120820','Administrative Medical Assisting','Health'),
		('120830','Health Facility Unit Coordinator','Health'),
		('120900','Hospital Central Service Technician','Health'),
		('121000','Respiratory Care-Therapy','Health'),
		('121100','Polysomnography','Health'),
		('121200','Electro-Neurodiagnostic Technology','Health'),
		('121300','Cardiovascular Technician','Health'),
		('121400','Orthopedic Assistant','Health'),
		('121500','Electrocardiography','Health'),
		('121700','Surgical Technician','Health'),
		('121800','Occupational Therapy Technology','Health'),
		('121900','Optical Technology','Health'),
		('122000','Speech-Language Pathology and Audiology','Health'),
		('122100','Pharmacy Technology','Health'),
		('122200','Physical Therapist Assistant','Health'),
		('122300','Health Information Technology','Health'),
		('122310','Health Information Coding','Health'),
		('122400','School Health Clerk','Health'),
		('122500','Radiologic Technology','Health'),
		('122600','Radiation Therapy Technician','Health'),
		('122700','Diagnostic Medical Sonography','Health'),
		('122800','Athletic Training and Sports Medicine','Health'),
		('123000','Nursing','Health'),
		('123010','Registered Nursing','Health'),
		('123020','Licensed Vocational Nursing','Health'),
		('123030','Certified Nurse Assistant','Health'),
		('123080','Home Health Aide','Health'),
		('123900','Psychiatric Technician','Health'),
		('124000','Dental Occupations','Health'),
		('124010','Dental Assistant','Health'),
		('124020','Dental Hygienist','Health'),
		('124030','Dental Laboratory Technician','Health'),
		('125000','Emergency Medical Services','Health'),
		('125100','Paramedic','Health'),
		('125500','Mortuary Science','Business and Entrepreneurship'),
		('126100','Community Health Care Worker','Health'),
		('126200','Massage Therapy','Business and Entrepreneurship'),
		('129900','Other Health Occupations','Health'),
		('130100','Family and Consumer Sciences, General','Retail, Hospitality and Tourism'),
		('130110','Consumer Services','Retail, Hospitality and Tourism'),
		('130200','Interior Design and Merchandising','Retail, Hospitality and Tourism'),
		('130300','Fashion','Retail, Hospitality and Tourism'),
		('130310','Fashion Design','Retail, Hospitality and Tourism'),
		('130320','Fashion Merchandising','Retail, Hospitality and Tourism'),
		('130330','Fashion Production','Advanced Manufacturing'),
		('130500','Child Development-Early Care and Education','Education and Human Development'),
		('130520','Children with Special Needs','Education and Human Development'),
		('130540','Preschool Age Child','Education and Human Development'),
		('130550','The School Age Child','Education and Human Development'),
		('130560','Parenting and Family Education','Education and Human Development'),
		('130570','Foster and Kinship Care','Education and Human Development'),
		('130580','Child Development Administration and Management','Education and Human Development'),
		('130590','Infants and Toddlers','Education and Human Development'),
		('130600','Nutrition, Foods, and Culinary Arts','Health'),
		('130620','Dietetic Services and Management','Health'),
		('130630','Culinary Arts','Retail, Hospitality and Tourism'),
		('130660','Dietetic Technology','Health'),
		('130700','Hospitality','Retail, Hospitality and Tourism'),
		('130710','Restaurant and Food Services and Management','Retail, Hospitality and Tourism'),
		('130720','Lodging Management','Retail, Hospitality and Tourism'),
		('130730','Resort and Club Management','Retail, Hospitality and Tourism'),
		('130800','Family Studies','Education and Human Development'),
		('130900','Gerontology','Health'),
		('139900','Other Family and Consumer Sciences','Unassigned'),
		('140200','Paralegal','Unassigned'),
		('160200','Library Technician (Aide)','Unassigned'),
		('192000','Ocean Technology','Advanced Manufacturing'),
		('210200','Public Administration','Unassigned'),
		('210210','Public Works','Energy, Construction and Utilities'),
		('210400','Human Services','Unassigned'),
		('210440','Alcohol and Controlled Substances','Health'),
		('210450','Disability Services','Unassigned'),
		('210500','Administration of Justice','Public Safety'),
		('210510','Corrections','Public Safety'),
		('210520','Probation and Parole','Public Safety'),
		('210530','Industrial and Transportation Security','Public Safety'),
		('210540','Forensics, Evidence, and Investigation','Public Safety'),
		('210550','Police Academy','Public Safety'),
		('213300','Fire Technology','Public Safety'),
		('213310','Wildland Fire Technology','Public Safety'),
		('213350','Fire Academy','Public Safety'),
		('214000','Legal and Community Interpretation','Unassigned'),
		('219900','Other Public and Protective Services','Public Safety'),
		('220610','Geographic Information Systems','Information and Communication Technologies (ICT) - Digital Media'),
		('300500','Custodial Services','Business and Entrepreneurship'),
		('300700','Cosmetology and Barbering','Business and Entrepreneurship'),
		('300800','Dry Cleaning','Retail, Hospitality and Tourism'),
		('300900','Travel Services and Tourism','Retail, Hospitality and Tourism'),
		('302000','Aviation and Airport Management and Services','Advanced Transportation and Logistics'),
		('302010','Aviation and Airport Management','Advanced Transportation and Logistics'),
		('302020','Piloting','Advanced Transportation and Logistics'),
		('302030','Air Traffic Control','Advanced Transportation and Logistics'),
		('302040','Flight Attendant','Retail, Hospitality and Tourism'),
		('309900','Other Commercial Services','Unassigned'),
		('493100','Vocational ESL','Unassigned'),
		('493200','General Work Experience','Unassigned')
	) z (ProgramCode, ProgramTitle, SectorName)
		on p.ProgramCode = z.ProgramCode
	left outer join
	dbo.Sector s
		on s.Label = z.SectorName
WHERE
	c.Label = 'Program'
	and sd.SubdisciplineId is not null;

GO

CREATE TABLE
	dbo.Region
	(
		RegionId   tinyint     not null identity(1,1),
		Label      varchar(15) not null,
		Identifier sysname     not null
	);

GO

ALTER TABLE
	dbo.Region
ADD CONSTRAINT
	PK_Region
PRIMARY KEY CLUSTERED
	(
		RegionId
	);

ALTER TABLE
	dbo.Region
ADD CONSTRAINT
	UQ_Region__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	dbo.Region
ADD CONSTRAINT
	UQ_Region__Identifier
UNIQUE
	(
		Identifier
	);

GO

INSERT
	dbo.Region
	(
		Label,
		Identifier
	)
VALUES
	('State','RegionIdState'),
	('Macroregion','RegionIdMacro'),
	('Microregion','RegionIdMicro'),
	('District', 'RegionIdDistrict'),
	('College','RegionIdCollege');

CREATE TABLE
	dbo.RegionState
	(
		RegionIdState tinyint     not null identity(1,1),
		RegionId      tinyint     not null,
		RegionCode    char(2)     not null,
		Label         varchar(15) not null
	);

GO

ALTER TABLE
	dbo.RegionState
ADD CONSTRAINT
	PK_RegionState
PRIMARY KEY CLUSTERED
	(
		RegionIdState
	);

ALTER TABLE
	dbo.RegionState
ADD CONSTRAINT
	UQ_RegionState__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	dbo.RegionState
ADD CONSTRAINT
	UQ_RegionState__RegionCode
UNIQUE
	(
		RegionCode
	);

ALTER TABLE
	dbo.RegionState
ADD CONSTRAINT
	FK_RegionState__RegionId
FOREIGN KEY
	(
		RegionId
	)
REFERENCES
	dbo.Region
	(
		RegionId
	);

CREATE NONCLUSTERED INDEX
	IX_RegionState__RegionId
ON
	dbo.RegionState
	(
		RegionId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	dbo.RegionState
	(
		RegionId,
		RegionCode,
		Label
	)
VALUES
	(1,'CA','California');

GO

CREATE TABLE
	dbo.RegionMacro
	(
		RegionIdMacro tinyint     not null identity(1,1),
		RegionId      tinyint     not null,
		RegionIdState tinyint     not null,
		Label         varchar(35) not null
	);

GO

ALTER TABLE
	dbo.RegionMacro
ADD CONSTRAINT
	PK_RegionMacro
PRIMARY KEY CLUSTERED
	(
		RegionIdMacro
	);

ALTER TABLE
	dbo.RegionMacro
ADD CONSTRAINT
	UQ_RegionMacro__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	dbo.RegionMacro
ADD CONSTRAINT
	FK_RegionMacro__RegionIdState
FOREIGN KEY
	(
		RegionIdState
	)
REFERENCES
	dbo.RegionState
	(
		RegionIdState
	);

ALTER TABLE
	dbo.RegionMacro
ADD CONSTRAINT
	FK_RegionMacro__RegionId
FOREIGN KEY
	(
		RegionId
	)
REFERENCES
	dbo.Region
	(
		RegionId
	);

CREATE NONCLUSTERED INDEX
	IX_RegionMacro__RegionId
ON
	dbo.RegionMacro
	(
		RegionId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	dbo.RegionMacro
	(
		RegionIdState,
		RegionId,
		Label
	)
VALUES
	(1,2,'Bay Area'),
	(1,2,'Central Valley/Mother Lode'),
	(1,2,'Inland Empire/Desert'),
	(1,2,'Los Angeles/Orange County'),
	(1,2,'North/Far North'),
	(1,2,'San Diego/Imperial Counties'),
	(1,2,'South Central Coast');

GO

CREATE TABLE
	dbo.RegionMicro
	(
		RegionIdMicro tinyint     not null identity(1,1),
		RegionId      tinyint     not null,
		RegionIdMacro tinyint     not null,
		Label         varchar(35) not null
	);

GO

ALTER TABLE
	dbo.RegionMicro
ADD CONSTRAINT
	PK_RegionMicro
PRIMARY KEY CLUSTERED
	(
		RegionIdMicro
	);

ALTER TABLE
	dbo.RegionMicro
ADD CONSTRAINT
	UQ_RegionMicro__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	dbo.RegionMicro
ADD CONSTRAINT
	FK_RegionMicro__RegionIdMacro
FOREIGN KEY
	(
		RegionIdMacro
	)
REFERENCES
	dbo.RegionMacro
	(
		RegionIdMacro
	);

ALTER TABLE
	dbo.RegionMicro
ADD CONSTRAINT
	FK_RegionMicro__RegionId
FOREIGN KEY
	(
		RegionId
	)
REFERENCES
	dbo.Region
	(
		RegionId
	);

CREATE NONCLUSTERED INDEX
	IX_RegionMicro__RegionId
ON
	dbo.RegionMicro
	(
		RegionId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	dbo.RegionMicro
	(
		RegionIdMacro,
		RegionId,
		Label
	)
VALUES
	(1,3,'East Bay'),
	(1,3,'North Bay'),
	(1,3,'Silicon Valley'),
	(1,3,'Santa Cruz-Monterey'),
	(1,3,'Mid-Peninsula'),
	(2,3,'Northern Central Valley-Mother Lode'),
	(2,3,'Southern Central Valley-Mother Lode'),
	(3,3,'Inland Empire'),
	(4,3,'Los Angeles'),
	(4,3,'Orange County'),
	(5,3,'Greater Sacramento'),
	(5,3,'Northern Inland'),
	(5,3,'Northern Coastal'),
	(6,3,'San Diego-Imperial'),
	(7,3,'South Central Coast');

GO

CREATE TABLE
	RegionDistrict
	(
		RegionIdDistrict TINYINT     NOT NULL IDENTITY(1,1),
		RegionId         TINYINT     NOT NULL,
		RegionIdMicro    TINYINT     NOT NULL,
		DistrictCode     CHAR(3)     NOT NULL,
		Label            VARCHAR(40) NOT NULL
	);

GO

ALTER TABLE
	RegionDistrict
ADD CONSTRAINT
	PK_RegionDistrict
PRIMARY KEY CLUSTERED
	(
		RegionIdDistrict
	);

ALTER TABLE
	RegionDistrict
ADD CONSTRAINT
	UQ_RegionDistrict__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	RegionDistrict
ADD CONSTRAINT
	UQ_RegionDistrict__DistrictCode
UNIQUE
	(
		DistrictCode
	);

ALTER TABLE
	RegionDistrict
ADD CONSTRAINT
	FK_RegionDistrict__RegionIdMicro
FOREIGN KEY
	(
		RegionIdMicro
	)
REFERENCES
	RegionMicro
	(
		RegionIdMicro
	);

ALTER TABLE
	RegionDistrict
ADD CONSTRAINT
	FK_RegionDistrict__RegionId
FOREIGN KEY
	(
		RegionId
	)
REFERENCES
	Region
	(
		RegionId
	);

CREATE NONCLUSTERED INDEX
	IX_RegionDistrict__RegionId
ON
	RegionDistrict
	(
		RegionId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	RegionDistrict
	(
		RegionIdMicro,
		RegionId,
		DistrictCode,
		Label
	)
SELECT DISTINCT
	c.RegionIdMicro,
	4,
	d.DistrictCode,
	d.Name
FROM
	(
		VALUES
		('311','112826','Contra Costa College',1,1,1,5),
		('312','113634','Diablo Valley College',1,1,1,5),
		('313','117894','Los Medanos College',1,1,1,5),
		('341','108667','College of Alameda',1,1,1,5),
		('343','117247','Laney College',1,1,1,5),
		('344','118772','Merritt College',1,1,1,5),
		('345','125170','Berkeley City College',1,1,1,5),
		('431','120290','Ohlone College',1,1,1,5),
		('481','366401','Las Positas College',1,1,1,5),
		('482','111920','Chabot College',1,1,1,5),
		('241','119331','Napa Valley College',2,1,1,5),
		('261','123013','Santa Rosa Junior College',2,1,1,5),
		('281','123563','Solano Community College',2,1,1,5),
		('334','118347','College of Marin',2,1,1,5),
		('335','998347','Marin Continuing',2,1,1,5),
		('421','113333','De Anza College',3,1,1,5),
		('422','114716','Foothill College',3,1,1,5),
		('441','114938','Gavilan College',3,1,1,5),
		('471','114266','Evergreen Valley College',3,1,1,5),
		('472','122746','San Jose City College',3,1,1,5),
		('492','118930','Mission College',3,1,1,5),
		('493','125499','West Valley College',3,1,1,5),
		('411','110334','Cabrillo College',4,1,1,5),
		('451','115393','Hartnell College',4,1,1,5),
		('461','119067','Monterey Peninsula College',4,1,1,5),
		('361','112190','City College of San Francisco',5,1,1,5),
		('363','992190','San Francisco Community College Centers',5,1,1,5),
		('371','111434','Cañada College',5,1,1,5),
		('372','122791','College of San Mateo',5,1,1,5),
		('373','123509','Skyline College',5,1,1,5),
		('531','118718','Merced College',6,2,1,5),
		('551','122658','San Joaquin Delta College',6,2,1,5),
		('591','112561','Columbia College',6,2,1,5),
		('592','118976','Modesto Junior College',6,2,1,5),
		('521','109819','Bakersfield College',7,2,1,5),
		('522','111896','Cerro Coso Community College',7,2,1,5),
		('523','121363','Porterville College',7,2,1,5),
		('561','123217','College of the Sequoias',7,2,1,5),
		('571','114789','Fresno City College',7,2,1,5),
		('572','117052','Reedley College',7,2,1,5),
		('576','999576','Clovis College',7,2,1,5),
		('581','125462','West Hills Coalinga',7,2,1,5),
		('582','448594','West Hills Lemoore',7,2,1,5),
		('691','124113','Taft College',7,2,1,5),
		('911','109907','Barstow College',8,3,1,5),
		('921','111939','Chaffey College',8,3,1,5),
		('931','113573','College of the Desert',8,3,1,5),
		('941','119216','Mt. San Jacinto College',8,3,1,5),
		('951','120953','Palo Verde College',8,3,1,5),
		('961','121901','Riverside City College',8,3,1,5),
		('962','460394','Moreno Valley College',8,3,1,5),
		('963','460464','Norco College',8,3,1,5),
		('971','395362','Copper Mountain Community College',8,3,1,5),
		('981','113111','Crafton Hills College',8,3,1,5),
		('982','123527','San Bernardino Valley College',8,3,1,5),
		('991','125091','Victor Valley Community College',8,3,1,5),
		('711','112686','El Camino College Compton Center',9,4,1,5),
		('721','113980','El Camino College',9,4,1,5),
		('731','115001','Glendale College',9,4,1,5),
		('741','117788','Los Angeles City College',9,4,1,5),
		('742','117690','Los Angeles Harbor College',9,4,1,5),
		('743','117867','Los Angeles Mission College',9,4,1,5),
		('744','117706','Los Angeles Pierce College',9,4,1,5),
		('745','117715','Los Angeles Southwest College',9,4,1,5),
		('746','117724','Los Angeles Trade-Technical College',9,4,1,5),
		('747','117733','Los Angeles Valley College',9,4,1,5),
		('748','113856','East Los Angeles College',9,4,1,5),
		('749','125471','West Los Angeles College',9,4,1,5),
		('74A','997788','Los Angeles Mission College ITV',9,4,1,5),
		('771','121044','Pasadena City College',9,4,1,5),
		('781','122977','Santa Monica College',9,4,1,5),
		('811','111887','Cerritos College',9,4,1,5),
		('821','112172','Citrus College',9,4,1,5),
		('841','117645','Long Beach City College',9,4,1,5),
		('851','119164','Mt. San Antonio College',9,4,1,5),
		('881','121886','Rio Hondo College',9,4,1,5),
		('831','112385','Coastline Community College',10,4,1,5),
		('832','115126','Golden West College',10,4,1,5),
		('833','120342','Orange Coast College',10,4,1,5),
		('861','113236','Cypress College',10,4,1,5),
		('862','114859','Fullerton College',10,4,1,5),
		('863','993236','North Orange Adult Division',10,4,1,5),
		('871','121619','Santa Ana College',10,4,1,5),
		('872','991619','Rancho Santiago CED',10,4,1,5),
		('873','399212','Santiago Canyon College',10,4,1,5),
		('891','122205','Saddleback College',10,4,1,5),
		('892','116439','Irvine Valley College',10,4,1,5),
		('221','117195','Lake Tahoe Community College',11,5,1,5),
		('231','109208','American River College',11,5,1,5),
		('232','113096','Cosumnes River College',11,5,1,5),
		('233','122180','Sacramento City College',11,5,1,5),
		('234','444219','Folsom Lake College',11,5,1,5),
		('271','123341','Sierra College',11,5,1,5),
		('291','126119','Yuba College',11,5,1,5),
		('292','455512','Woodland College',11,5,1,5),
		('111','110246','Butte College',12,5,1,5),
		('121','114433','Feather River College',12,5,1,5),
		('131','117274','Lassen College',12,5,1,5),
		('171','123299','Shasta College',12,5,1,5),
		('181','123484','College of the Siskiyous',12,5,1,5),
		('141','118684','Mendocino College',13,5,1,5),
		('161','121707','College of the Redwoods',13,5,1,5),
		('021','113218','Cuyamaca College',14,6,1,5),
		('022','115296','Grossmont College',14,6,1,5),
		('031','115861','Imperial Valley College',14,6,1,5),
		('051','118912','MiraCosta College',14,6,1,5),
		('061','120971','Palomar College',14,6,1,5),
		('071','122339','San Diego City College',14,6,1,5),
		('072','122375','San Diego Mesa College',14,6,1,5),
		('073','122384','San Diego Miramar College',14,6,1,5),
		('076','113953','San Diego Continuing Education',14,6,1,5),
		('091','123800','Southwestern College',14,6,1,5),
		('611','108807','Allan Hancock College',15,7,1,5),
		('621','109350','Antelope Valley College',15,7,1,5),
		('641','113193','Cuesta College',15,7,1,5),
		('651','122889','Santa Barbara City College',15,7,1,5),
		('652','992889','Santa Barbara Continuing',15,7,1,5),
		('661','111461','College of the Canyons',15,7,1,5),
		('681','119137','Moorpark College',15,7,1,5),
		('682','120421','Oxnard College',15,7,1,5),
		('683','125028','Ventura College',15,7,1,5)
	) c (
		CollegeCode,
		IpedsCode,
		Label,
		RegionIdMicro,
		RegionIdMacro,
		RegionIdState,
		RegionId
	)
	inner join
	(
		VALUES
		('020','115287','170768','GROSSMONT CCD'),
		('030',NULL,NULL,'IMPERIAL CCD'),
		('050',NULL,NULL,'MIRA COSTA CCD'),
		('060',NULL,NULL,'PALOMAR CCD'),
		('070','122320','170821','SAN DIEGO CCD'),
		('090',NULL,NULL,'SOUTHWESTERN CCD'),
		('110',NULL,NULL,'BUTTE CCD'),
		('120',NULL,NULL,'FEATHER RIVER CCD'),
		('130',NULL,NULL,'LASSEN CCD'),
		('140',NULL,NULL,'MENDOCINO CCD'),
		('160',NULL,NULL,'REDWOODS CCD'),
		('170',NULL,NULL,'SHASTA TEHAMA CCD'),
		('180',NULL,NULL,'SISKIYOUS CCD'),
		('220',NULL,NULL,'LAKE TAHOE CCD'),
		('230','117900','170788','LOS RIOS CCD'),
		('240',NULL,NULL,'NAPA CCD'),
		('260',NULL,NULL,'SONOMA CCD'),
		('270',NULL,NULL,'SIERRA CCD'),
		('280','123563','170837','SOLANO CCD'),
		('290','126119','172784','YUBA CCD'),
		('310','112817','170746','CONTRA COSTA CCD'),
		('330',NULL,NULL,'MARIN CCD'),
		('340','121178','170808','PERALTA CCD'),
		('360',NULL,'170824','SAN FRANCISCO CCD'),
		('370','122782','170828','SAN MATEO CCD'),
		('410',NULL,NULL,'CABRILLO CCD'),
		('420','114831','170761','FOOTHILL CCD'),
		('430',NULL,NULL,'OHLONE CCD'),
		('440',NULL,NULL,'GAVILAN CCD'),
		('450',NULL,NULL,'HARTNELL CCD'),
		('460',NULL,NULL,'MONTEREY CCD'),
		('470','122737','170827','SAN JOSE CCD'),
		('480',NULL,'170728','CHABOT-LAS POSITAS CCD'),
		('490','125222','170849','WEST VALLEY CCD'),
		('520',NULL,'170772','KERN CCD'),
		('530',NULL,NULL,'MERCED CCD'),
		('550',NULL,NULL,'SAN JOAQUIN DELTA CCD'),
		('560',NULL,NULL,'SEQUOIAS CCD'),
		('570','123925','170840','STATE CENTER CCD'),
		('580','448637','172264','WEST HILLS CCD'),
		('590','126100','170850','YOSEMITE CCD'),
		('610',NULL,NULL,'ALLAN HANCOCK CCD'),
		('620',NULL,NULL,'ANTELOPE CCD'),
		('640',NULL,NULL,'SAN LUIS OBISPO CCD'),
		('650',NULL,NULL,'SANTA BARBARA CCD'),
		('660',NULL,NULL,'SANTA CLARITA CCD'),
		('680','125019','170843','VENTURA CCD'),
		('690',NULL,NULL,'WEST KERN CCD'),
		('710',NULL,NULL,'COMPTON CCD'),
		('720',NULL,'172781','EL CAMINO CCD'),
		('730',NULL,NULL,'GLENDALE CCD'),
		('740','117681','170779','LOS ANGELES CCD'),
		('770',NULL,NULL,'PASADENA CCD'),
		('780',NULL,NULL,'SANTA MONICA CCD'),
		('810',NULL,NULL,'CERRITOS CCD'),
		('820',NULL,NULL,'CITRUS CCD'),
		('830','112376','170733','COAST CCD'),
		('840',NULL,NULL,'LONG BEACH CCD'),
		('850',NULL,NULL,'MT. SAN ANTONIO CCD'),
		('860','120023','170801','NORTH ORANGE CCD'),
		('870','438665','170811','RANCHO SANTIAGO CCD'),
		('880',NULL,NULL,'RIO HONDO CCD'),
		('890','432144','170838','SOUTH ORANGE COUNTY CCD'),
		('910',NULL,NULL,'BARSTOW CCD'),
		('920',NULL,NULL,'CHAFFEY CCD'),
		('930',NULL,NULL,'DESERT CCD'),
		('940',NULL,NULL,'MT. SAN JACINTO CCD'),
		('950',NULL,NULL,'PALO VERDE CCD'),
		('960',NULL,'175134','RIVERSIDE CCD'),
		('970',NULL,NULL,'COPPER MOUNTAIN'),
		('980','428426','170817','SAN BERNARDINO CCD'),
		('990',NULL,NULL,'VICTOR VALLEY CCD')
	) d (DistrictCode, IpedsCode, LegacyCode, Name)
		on left(d.DistrictCode, 2) = left(c.CollegeCode, 2)
ORDER BY
	c.RegionIdMicro,
	d.DistrictCode;

GO

CREATE TABLE
	dbo.RegionCollege
	(
		RegionIdCollege  tinyint     not null identity(1,1),
		CollegeCode      char(3)     not null,
		IpedsCode        char(6)         null,
		Label            varchar(40) not null,
		RegionIdDistrict tinyint     not null,
		RegionIdMicro    tinyint     not null,
		RegionIdMacro    tinyint     not null,
		RegionIdState    tinyint     not null,
		RegionId         tinyint     not null
	);

GO

ALTER TABLE
	dbo.RegionCollege
ADD CONSTRAINT
	PK_RegionCollege
PRIMARY KEY CLUSTERED
	(
		RegionIdCollege
	);

ALTER TABLE
	dbo.RegionCollege
ADD CONSTRAINT
	UQ_RegionCollege__Label
UNIQUE
	(
		Label
	);

ALTER TABLE
	dbo.RegionCollege
ADD CONSTRAINT
	UQ_RegionCollege__CollegeCode
UNIQUE
	(
		CollegeCode
	);

ALTER TABLE
	dbo.RegionCollege
ADD CONSTRAINT
	FK_RegionCollege__RegionIdDistrict
FOREIGN KEY
	(
		RegionIdDistrict
	)
REFERENCES
	dbo.RegionDistrict
	(
		RegionIdDistrict
	);

ALTER TABLE
	dbo.RegionCollege
ADD CONSTRAINT
	FK_RegionCollege__RegionIdMacro
FOREIGN KEY
	(
		RegionIdMacro
	)
REFERENCES
	dbo.RegionMacro
	(
		RegionIdMacro
	);

ALTER TABLE
	dbo.RegionCollege
ADD CONSTRAINT
	FK_RegionCollege__RegionIdState
FOREIGN KEY
	(
		RegionIdState
	)
REFERENCES
	dbo.RegionState
	(
		RegionIdState
	);

ALTER TABLE
	dbo.RegionCollege
ADD CONSTRAINT
	FK_RegionCollege__RegionIdMicro
FOREIGN KEY
	(
		RegionIdMicro
	)
REFERENCES
	dbo.RegionMicro
	(
		RegionIdMicro
	);

ALTER TABLE
	dbo.RegionCollege
ADD CONSTRAINT
	FK_RegionCollege__RegionId
FOREIGN KEY
	(
		RegionId
	)
REFERENCES
	dbo.Region
	(
		RegionId
	);

CREATE NONCLUSTERED INDEX
	IX_RegionCollege__RegionIdMicro
ON
	dbo.RegionCollege
	(
		RegionIdMicro
	)
INCLUDE
	(
		Label
	);

CREATE NONCLUSTERED INDEX
	IX_RegionCollege__RegionIdMacro
ON
	dbo.RegionCollege
	(
		RegionIdMacro
	)
INCLUDE
	(
		Label
	);

CREATE NONCLUSTERED INDEX
	IX_RegionCollege__RegionIdState
ON
	dbo.RegionCollege
	(
		RegionIdState
	)
INCLUDE
	(
		Label
	);

CREATE NONCLUSTERED INDEX
	IX_RegionCollege__RegionId
ON
	dbo.RegionCollege
	(
		RegionId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	dbo.RegionCollege
	(
		CollegeCode,
		IpedsCode,
		Label,
		RegionIdDistrict,
		RegionIdMicro,
		RegionIdMacro,
		RegionIdState,
		RegionId
	)
SELECT
	c.CollegeCode,
	c.IpedsCode,
	c.Label,
	RegionIdDistrict = dense_rank() over(order by c.RegionIdMicro, d.DistrictCode),
	c.RegionIdMicro,
	c.RegionIdMacro,
	c.RegionIdState,
	c.RegionId
FROM
	(
		VALUES
		('311','112826','Contra Costa College',1,1,1,5),
		('312','113634','Diablo Valley College',1,1,1,5),
		('313','117894','Los Medanos College',1,1,1,5),
		('341','108667','College of Alameda',1,1,1,5),
		('343','117247','Laney College',1,1,1,5),
		('344','118772','Merritt College',1,1,1,5),
		('345','125170','Berkeley City College',1,1,1,5),
		('431','120290','Ohlone College',1,1,1,5),
		('481','366401','Las Positas College',1,1,1,5),
		('482','111920','Chabot College',1,1,1,5),
		('241','119331','Napa Valley College',2,1,1,5),
		('261','123013','Santa Rosa Junior College',2,1,1,5),
		('281','123563','Solano Community College',2,1,1,5),
		('334','118347','College of Marin',2,1,1,5),
		('335','998347','Marin Continuing',2,1,1,5),
		('421','113333','De Anza College',3,1,1,5),
		('422','114716','Foothill College',3,1,1,5),
		('441','114938','Gavilan College',3,1,1,5),
		('471','114266','Evergreen Valley College',3,1,1,5),
		('472','122746','San Jose City College',3,1,1,5),
		('492','118930','Mission College',3,1,1,5),
		('493','125499','West Valley College',3,1,1,5),
		('411','110334','Cabrillo College',4,1,1,5),
		('451','115393','Hartnell College',4,1,1,5),
		('461','119067','Monterey Peninsula College',4,1,1,5),
		('361','112190','City College of San Francisco',5,1,1,5),
		('363','992190','San Francisco Community College Centers',5,1,1,5),
		('371','111434','Cañada College',5,1,1,5),
		('372','122791','College of San Mateo',5,1,1,5),
		('373','123509','Skyline College',5,1,1,5),
		('531','118718','Merced College',6,2,1,5),
		('551','122658','San Joaquin Delta College',6,2,1,5),
		('591','112561','Columbia College',6,2,1,5),
		('592','118976','Modesto Junior College',6,2,1,5),
		('521','109819','Bakersfield College',7,2,1,5),
		('522','111896','Cerro Coso Community College',7,2,1,5),
		('523','121363','Porterville College',7,2,1,5),
		('561','123217','College of the Sequoias',7,2,1,5),
		('571','114789','Fresno City College',7,2,1,5),
		('572','117052','Reedley College',7,2,1,5),
		('576','999576','Clovis College',7,2,1,5),
		('581','125462','West Hills Coalinga',7,2,1,5),
		('582','448594','West Hills Lemoore',7,2,1,5),
		('691','124113','Taft College',7,2,1,5),
		('911','109907','Barstow College',8,3,1,5),
		('921','111939','Chaffey College',8,3,1,5),
		('931','113573','College of the Desert',8,3,1,5),
		('941','119216','Mt. San Jacinto College',8,3,1,5),
		('951','120953','Palo Verde College',8,3,1,5),
		('961','121901','Riverside City College',8,3,1,5),
		('962','460394','Moreno Valley College',8,3,1,5),
		('963','460464','Norco College',8,3,1,5),
		('971','395362','Copper Mountain Community College',8,3,1,5),
		('981','113111','Crafton Hills College',8,3,1,5),
		('982','123527','San Bernardino Valley College',8,3,1,5),
		('991','125091','Victor Valley Community College',8,3,1,5),
		('711','112686','El Camino College Compton Center',9,4,1,5),
		('721','113980','El Camino College',9,4,1,5),
		('731','115001','Glendale College',9,4,1,5),
		('741','117788','Los Angeles City College',9,4,1,5),
		('742','117690','Los Angeles Harbor College',9,4,1,5),
		('743','117867','Los Angeles Mission College',9,4,1,5),
		('744','117706','Los Angeles Pierce College',9,4,1,5),
		('745','117715','Los Angeles Southwest College',9,4,1,5),
		('746','117724','Los Angeles Trade-Technical College',9,4,1,5),
		('747','117733','Los Angeles Valley College',9,4,1,5),
		('748','113856','East Los Angeles College',9,4,1,5),
		('749','125471','West Los Angeles College',9,4,1,5),
		('74A','997788','Los Angeles Mission College ITV',9,4,1,5),
		('771','121044','Pasadena City College',9,4,1,5),
		('781','122977','Santa Monica College',9,4,1,5),
		('811','111887','Cerritos College',9,4,1,5),
		('821','112172','Citrus College',9,4,1,5),
		('841','117645','Long Beach City College',9,4,1,5),
		('851','119164','Mt. San Antonio College',9,4,1,5),
		('881','121886','Rio Hondo College',9,4,1,5),
		('831','112385','Coastline Community College',10,4,1,5),
		('832','115126','Golden West College',10,4,1,5),
		('833','120342','Orange Coast College',10,4,1,5),
		('861','113236','Cypress College',10,4,1,5),
		('862','114859','Fullerton College',10,4,1,5),
		('863','993236','North Orange Adult Division',10,4,1,5),
		('871','121619','Santa Ana College',10,4,1,5),
		('872','991619','Rancho Santiago CED',10,4,1,5),
		('873','399212','Santiago Canyon College',10,4,1,5),
		('891','122205','Saddleback College',10,4,1,5),
		('892','116439','Irvine Valley College',10,4,1,5),
		('221','117195','Lake Tahoe Community College',11,5,1,5),
		('231','109208','American River College',11,5,1,5),
		('232','113096','Cosumnes River College',11,5,1,5),
		('233','122180','Sacramento City College',11,5,1,5),
		('234','444219','Folsom Lake College',11,5,1,5),
		('271','123341','Sierra College',11,5,1,5),
		('291','126119','Yuba College',11,5,1,5),
		('292','455512','Woodland College',11,5,1,5),
		('111','110246','Butte College',12,5,1,5),
		('121','114433','Feather River College',12,5,1,5),
		('131','117274','Lassen College',12,5,1,5),
		('171','123299','Shasta College',12,5,1,5),
		('181','123484','College of the Siskiyous',12,5,1,5),
		('141','118684','Mendocino College',13,5,1,5),
		('161','121707','College of the Redwoods',13,5,1,5),
		('021','113218','Cuyamaca College',14,6,1,5),
		('022','115296','Grossmont College',14,6,1,5),
		('031','115861','Imperial Valley College',14,6,1,5),
		('051','118912','MiraCosta College',14,6,1,5),
		('061','120971','Palomar College',14,6,1,5),
		('071','122339','San Diego City College',14,6,1,5),
		('072','122375','San Diego Mesa College',14,6,1,5),
		('073','122384','San Diego Miramar College',14,6,1,5),
		('076','113953','San Diego Continuing Education',14,6,1,5),
		('091','123800','Southwestern College',14,6,1,5),
		('611','108807','Allan Hancock College',15,7,1,5),
		('621','109350','Antelope Valley College',15,7,1,5),
		('641','113193','Cuesta College',15,7,1,5),
		('651','122889','Santa Barbara City College',15,7,1,5),
		('652','992889','Santa Barbara Continuing',15,7,1,5),
		('661','111461','College of the Canyons',15,7,1,5),
		('681','119137','Moorpark College',15,7,1,5),
		('682','120421','Oxnard College',15,7,1,5),
		('683','125028','Ventura College',15,7,1,5)
	) c (
		CollegeCode,
		IpedsCode,
		Label,
		RegionIdMicro,
		RegionIdMacro,
		RegionIdState,
		RegionId
	)
	inner join
	(
		VALUES
		('020','115287','170768','GROSSMONT CCD'),
		('030',NULL,NULL,'IMPERIAL CCD'),
		('050',NULL,NULL,'MIRA COSTA CCD'),
		('060',NULL,NULL,'PALOMAR CCD'),
		('070','122320','170821','SAN DIEGO CCD'),
		('090',NULL,NULL,'SOUTHWESTERN CCD'),
		('110',NULL,NULL,'BUTTE CCD'),
		('120',NULL,NULL,'FEATHER RIVER CCD'),
		('130',NULL,NULL,'LASSEN CCD'),
		('140',NULL,NULL,'MENDOCINO CCD'),
		('160',NULL,NULL,'REDWOODS CCD'),
		('170',NULL,NULL,'SHASTA TEHAMA CCD'),
		('180',NULL,NULL,'SISKIYOUS CCD'),
		('220',NULL,NULL,'LAKE TAHOE CCD'),
		('230','117900','170788','LOS RIOS CCD'),
		('240',NULL,NULL,'NAPA CCD'),
		('260',NULL,NULL,'SONOMA CCD'),
		('270',NULL,NULL,'SIERRA CCD'),
		('280','123563','170837','SOLANO CCD'),
		('290','126119','172784','YUBA CCD'),
		('310','112817','170746','CONTRA COSTA CCD'),
		('330',NULL,NULL,'MARIN CCD'),
		('340','121178','170808','PERALTA CCD'),
		('360',NULL,'170824','SAN FRANCISCO CCD'),
		('370','122782','170828','SAN MATEO CCD'),
		('410',NULL,NULL,'CABRILLO CCD'),
		('420','114831','170761','FOOTHILL CCD'),
		('430',NULL,NULL,'OHLONE CCD'),
		('440',NULL,NULL,'GAVILAN CCD'),
		('450',NULL,NULL,'HARTNELL CCD'),
		('460',NULL,NULL,'MONTEREY CCD'),
		('470','122737','170827','SAN JOSE CCD'),
		('480',NULL,'170728','CHABOT-LAS POSITAS CCD'),
		('490','125222','170849','WEST VALLEY CCD'),
		('520',NULL,'170772','KERN CCD'),
		('530',NULL,NULL,'MERCED CCD'),
		('550',NULL,NULL,'SAN JOAQUIN DELTA CCD'),
		('560',NULL,NULL,'SEQUOIAS CCD'),
		('570','123925','170840','STATE CENTER CCD'),
		('580','448637','172264','WEST HILLS CCD'),
		('590','126100','170850','YOSEMITE CCD'),
		('610',NULL,NULL,'ALLAN HANCOCK CCD'),
		('620',NULL,NULL,'ANTELOPE CCD'),
		('640',NULL,NULL,'SAN LUIS OBISPO CCD'),
		('650',NULL,NULL,'SANTA BARBARA CCD'),
		('660',NULL,NULL,'SANTA CLARITA CCD'),
		('680','125019','170843','VENTURA CCD'),
		('690',NULL,NULL,'WEST KERN CCD'),
		('710',NULL,NULL,'COMPTON CCD'),
		('720',NULL,'172781','EL CAMINO CCD'),
		('730',NULL,NULL,'GLENDALE CCD'),
		('740','117681','170779','LOS ANGELES CCD'),
		('770',NULL,NULL,'PASADENA CCD'),
		('780',NULL,NULL,'SANTA MONICA CCD'),
		('810',NULL,NULL,'CERRITOS CCD'),
		('820',NULL,NULL,'CITRUS CCD'),
		('830','112376','170733','COAST CCD'),
		('840',NULL,NULL,'LONG BEACH CCD'),
		('850',NULL,NULL,'MT. SAN ANTONIO CCD'),
		('860','120023','170801','NORTH ORANGE CCD'),
		('870','438665','170811','RANCHO SANTIAGO CCD'),
		('880',NULL,NULL,'RIO HONDO CCD'),
		('890','432144','170838','SOUTH ORANGE COUNTY CCD'),
		('910',NULL,NULL,'BARSTOW CCD'),
		('920',NULL,NULL,'CHAFFEY CCD'),
		('930',NULL,NULL,'DESERT CCD'),
		('940',NULL,NULL,'MT. SAN JACINTO CCD'),
		('950',NULL,NULL,'PALO VERDE CCD'),
		('960',NULL,'175134','RIVERSIDE CCD'),
		('970',NULL,NULL,'COPPER MOUNTAIN'),
		('980','428426','170817','SAN BERNARDINO CCD'),
		('990',NULL,NULL,'VICTOR VALLEY CCD')
	) d (DistrictCode, IpedsCode, LegacyCode, Name)
		on left(d.DistrictCode, 2) = left(c.CollegeCode, 2);

GO

CREATE TABLE
	dbo.Attribute
	(
		AttributeId TINYINT     NOT NULL identity(1,1),
		Label       VARCHAR(25) NOT NULL,
		Identifier  SYSNAME     NOT NULL
	);

GO

ALTER TABLE
	dbo.Attribute
ADD CONSTRAINT
	PK_Attribute
PRIMARY KEY CLUSTERED
	(
		AttributeId
	);

ALTER TABLE
	dbo.Attribute
ADD CONSTRAINT
	UQ_Attribute__Label
UNIQUE
	(
		Label
	);

GO

INSERT
	dbo.Attribute
	(
		Label,
		Identifier
	)
VALUES
	('All', 'AttributeId'),
	('Credit', 'CreditId');

GO

CREATE TABLE
	dbo.Credit
	(
		CreditId    TINYINT     NOT NULL IDENTITY(1,1),
		AttributeId TINYINT     NOT NULL,
		CreditCode  BIT         NOT NULL,
		Label       VARCHAR(10) NOT NULL
	)

GO

ALTER TABLE
	dbo.Credit
ADD CONSTRAINT
	PK_Credit
PRIMARY KEY CLUSTERED
	(
		CreditId
	);

ALTER TABLE
	dbo.Credit
ADD CONSTRAINT
	UQ_Credit__CreditCode
UNIQUE
	(
		CreditCode
	);

ALTER TABLE
	dbo.Credit
ADD CONSTRAINT
	FK_Credit__AttributeId
FOREIGN KEY
	(
		AttributeId
	)
REFERENCES
	dbo.Attribute
	(
		AttributeId
	);

CREATE NONCLUSTERED INDEX
	IX_Credit__AttributeId
ON
	dbo.Credit
	(
		AttributeId
	)
INCLUDE
	(
		Label
	);

GO

INSERT
	dbo.Credit
	(
		AttributeId,
		CreditCode,
		Label
	)
VALUES
	(2,1,'ForCredit'),
	(2,0,'NonCredit');

GO

-- GO

-- DELETE
-- 	z
-- FROM
-- 	StudentMetrics a
-- 	inner join
-- 	dbo.Student s
-- 		on s.StudentId = a.StudentId
-- 	inner join
-- 	dbo.Year y
-- 		on y.YearId = a.YearId
-- 	inner join
-- 	dbo.RegionCollege c
-- 		on c.RegionIdCollege = a.RegionIdCollege
-- 	inner join
-- 	dbo.Program p
-- 		on p.ProgramId = a.ProgramId
-- 	inner join
-- 	CCStudentCohortTableMultiYr z
-- 		on z.student_id = s.student_id
-- 		and z.academic_year = y.YearEnd
-- 		and z.cc_ipeds_code = c.IpedsCode
-- 		and z.program_code = p.ProgramCode
-- WHERE
-- 	exists (
-- 		SELECT
-- 			1
-- 		FROM
-- 			dbo.StudentMetrics b
-- 		WHERE
-- 			b.YearId               = a.YearId
-- 			and b.StudentId        = a.StudentId
-- 			and b.RegionIdCollege  = a.RegionIdCollege
-- 			and b.ProgramId        = a.ProgramId
-- 			and b.DuplicateCounter > 1
-- 	);

GO

INSERT INTO
	Student
	(
		student_id
	)
SELECT
	student_id
FROM
	CCStudentCohortTableMultiYr
GROUP BY
	student_id;

INSERT INTO
	dbo.StudentMetrics
	(
		YearId,
		StudentId,
		RegionIdState,
		RegionIdMacro,
		RegionIdMicro,
		RegionIdDistrict,
		RegionIdCollege,
		ContentId,
		VocationId,
		SectorId,
		SubdisciplineId,
		ProgramId,
		DemographicId,
		AgeIdCollege,
		AgeIdDistrict,
		AgeIdMacro,
		AgeIdMicro,
		AgeIdState,
		EthnicityIdCollege,
		EthnicityIdDistrict,
		EthnicityIdMacro,
		EthnicityIdMicro,
		EthnicityIdState,
		GenderIdCollege,
		GenderIdDistrict,
		GenderIdMacro,
		GenderIdMicro,
		GenderIdState,
		PovertyId,
		AttributeId,
		CreditId,
		SkillsBuilder,
		Exiter,
		EconDis,
		Transfer,
		CTE9UnitsDist,
		CTENonCredit,
		LivingWage
	)
SELECT
	YearId                = b.YearId,
	StudentId             = c.StudentId,
	RegionIdState         = f.RegionIdState,
	RegionIdMacro         = f.RegionIdMacro,
	RegionIdMicro         = f.RegionIdMicro,
	RegionIdDistrict      = f.RegionIdDistrict,
	RegionIdCollege       = f.RegionIdCollege,
	ContentId             = 1,
	VocationId            = i.VocationId,
	SectorId              = i.SectorId,
	SubdisciplineId       = i.SubdisciplineId,
	ProgramId             = i.ProgramId,
	DemographicId         = 1,
	AgeIdCollege          = a1.AgeId,
	AgeIdDistrict         = a2.AgeId,
	AgeIdMacro            = a3.AgeId,
	AgeIdMicro            = a4.AgeId,
	AgeIdState            = a5.AgeId,
	EthnicityIdCollege    = e1.EthnicityId,
	EthnicityIdDistrict   = e2.EthnicityId,
	EthnicityIdMacro      = e3.EthnicityId,
	EthnicityIdMicro      = e4.EthnicityId,
	EthnicityIdState      = e5.EthnicityId,
	GenderIdCollege       = g1.GenderId,
	GenderIdDistrict      = g2.GenderId,
	GenderIdMacro         = g3.GenderId,
	GenderIdMicro         = g4.GenderId,
	GenderIdState         = g5.GenderId,
	PovertyId             = PovertyId,
	AttributeId           = 1,
	CreditId              = c1.CreditId,
	SkillsBuilder         = CONVERT(BIT, SB),
	Exiter                = CONVERT(BIT, Exiter),
	EconDis               = CONVERT(BIT, p.IsDisadvantaged),
	Transfer              = CONVERT(BIT, Transfer),
	CTE9UnitsDist         = CONVERT(BIT, CTE9UnitsDist),
	CTENonCredit          = CONVERT(BIT, CTENonCredit),
	LivingWage            = CONVERT(BIT, LivingWage)
FROM
	dbo.CCStudentCohortTableMultiYr a
	inner join
	dbo.Year b
		on b.YearEnd = a.academic_year
	inner join
	dbo.Student c
		on c.student_id = a.student_id
	inner join
	dbo.RegionCollege f
		on f.IpedsCode = a.cc_ipeds_code
	inner join
	dbo.Program i
		on i.ProgramCode = a.program_code
	inner join
	dbo.Poverty p
		on p.Label = a.student_econdis
	inner join
	dbo.Age a1
		on a1.Label = a.age_col
	inner join
	dbo.Age a2
		on a2.Label = a.age_dist
	inner join
	dbo.Age a3
		on a3.Label = a.age_macro
	inner join
	dbo.Age a4
		on a4.Label = a.age_micro
	inner join
	dbo.Age a5
		on a5.Label = a.age_state
	inner join
	dbo.Ethnicity e1
		on e1.Label = a.Ethnicity_col
	inner join
	dbo.Ethnicity e2
		on e2.Label = a.Ethnicity_dist
	inner join
	dbo.Ethnicity e3
		on e3.Label = a.Ethnicity_macro
	inner join
	dbo.Ethnicity e4
		on e4.Label = a.Ethnicity_micro
	inner join
	dbo.Ethnicity e5
		on e5.Label = a.Ethnicity_state
	inner join
	dbo.Gender g1
		on g1.Label = a.Gender_col
	inner join
	dbo.Gender g2
		on g2.Label = a.Gender_dist
	inner join
	dbo.Gender g3
		on g3.Label = a.Gender_macro
	inner join
	dbo.Gender g4
		on g4.Label = a.Gender_micro
	inner join
	dbo.Gender g5
		on g5.Label = a.Gender_state
	inner join
	dbo.Credit c1
		on c1.CreditCode = a.for_credit;