SET NOCOUNT ON;
SET ANSI_WARNINGS OFF;

DECLARE
	@Severity       tinyint       = 0,
	@State          tinyint       = 1,
	@Error          varchar(2048) = '',
	@YearCounter    tinyint       = 2,
	@YearMax        tinyint       = 0,
	@CollegeCounter tinyint       = 1,
	@CollegeMax     tinyint       = 0,
	@RegionId       tinyint       = 4;

SELECT
	@YearMax = max(YearId)
FROM
	dbo.Year;

SELECT
	@CollegeMax = max(RegionCollegeId)
FROM
	dbo.RegionCollege;

WHILE (@YearCounter <= @YearMax)
BEGIN

	SET @Error = 'Year: ' + convert(varchar, @YearCounter) + ' of ' + convert(varchar, @YearMax);
	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

	WHILE (@CollegeCounter <= @CollegeMax)
	BEGIN

		SET @Error = 'College: ' + convert(varchar, @CollegeCounter) + ' of ' + convert(varchar, @CollegeMax);
		RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

		MERGE
			dbo.MetricOutput t
		USING
			(
				SELECT
					YearId,
					RegionId,
					RegionKey,
					ContentId,
					ContentKey,
					DemographicId,
					DemographicKey,
					Metric103b  = count(StudentId),
					Metric200bN = count(case when NonIntro = 1 then StudentId end),
					Metric200bD = count(StudentId),
					Metric201bN = count(case when SkillsBuilder = 1 then StudentId end),
					Metric201bD = count(StudentId),
					Metric202bN = count(case when Exiter = 1 then StudentId end),
					Metric202bD = count(StudentId),
					Metric224bN = count(case when EconDis = 1 then StudentId end),
					Metric224bD = count(StudentId),
					Metric302bN = count(case when TermRet = 1 then StudentId end),
					Metric302bD = count(case when TermRet is not null then StudentId end),
					Metric303bN = count(case when TermRegRet = 1 then StudentId end),
					Metric303bD = count(case when TermRegRet is not null then StudentId end),
					Metric304bN = count(case when Persist = 1 then StudentId end),
					Metric304bD = count(case when Persist is not null then StudentId end),
					Metric305bN = count(case when RegPersist = 1 then StudentId end),
					Metric305bD = count(case when RegPersist is not null then StudentId end),
					Metric410bN = count(case when Transfer = 1 then StudentId end),
					Metric410bD = count(StudentId),
					Metric411bN = count(case when SBWageGain = 1 then StudentId end),
					Metric411bD = count(StudentId)
				FROM
					(
						SELECT
							YearId         = @YearCounter,
							StudentId      = StudentId,
							RegionId       = @RegionId,
							RegionKey      = @CollegeCounter,
							ContentId      = ContentId,
							ContentKey     = ContentKey,
							DemographicId  = DemographicId,
							DemographicKey = DemographicKey,
							NonIntro       = max(convert(tinyint, NonIntro)),
							SkillsBuilder  = max(convert(tinyint, SkillsBuilder)),
							Exiter         = max(convert(tinyint, Exiter)),
							EconDis        = max(convert(tinyint, EconDis)),
							TermRet        = max(convert(tinyint, TermRet)),
							TermRegRet     = max(convert(tinyint, TermRegRet)),
							Persist        = max(convert(tinyint, Persist)),
							RegPersist     = max(convert(tinyint, RegPersist)),
							Transfer       = max(convert(tinyint, Transfer)),
							SBWageGain     = max(convert(tinyint, SBWageGain))
						FROM
							(
								SELECT
									StudentId,
									ContentId,
									VocationId,
									SectorId,
									SubdisciplineId,
									ProgramId,
									DemographicId,
									GenderId,
									AgeId,
									EthnicityId,
									PovertyId,
									NonIntro,
									SkillsBuilder,
									Exiter,
									EconDis,
									TermRet,
									TermRegRet,
									Persist,
									RegPersist,
									Transfer,
									SBWageGain
								FROM
									dbo.StudentMetrics
								WHERE
									YearId = @YearCounter
									and RegionCollegeId = @CollegeCounter
							) a
						UNPIVOT
							(
								DemographicKey for DemographicIdentifier in (DemographicId, GenderId, AgeId, EthnicityId, PovertyId)
							) u
						UNPIVOT
							(
								ContentKey for ContentIdentifier in (ContentId, VocationId, SectorId, SubdisciplineId, ProgramId)
							) u
							inner join
							dbo.Demographic d
								on d.Identifier = u.DemographicIdentifier
							inner join
							dbo.Content c
								on c.Identifier = u.ContentIdentifier
						GROUP BY
							StudentId,
							ContentId,
							ContentKey,
							DemographicId,
							DemographicKey
					) a
				GROUP BY
					YearId,
					RegionId,
					RegionKey,
					ContentId,
					ContentKey,
					DemographicId,
					DemographicKey
			) s
		ON
			(
				s.YearId             = t.YearId
				and s.RegionId       = t.RegionId
				and s.RegionKey      = t.RegionKey
				and s.ContentId      = t.ContentId
				and s.ContentKey     = t.ContentKey
				and s.DemographicId  = t.DemographicId
				and s.DemographicKey = t.DemographicKey
			)
		WHEN MATCHED THEN
			UPDATE SET
				t.Metric103b  = s.Metric103b,
				t.Metric200bN = s.Metric200bN,
				t.Metric200bD = s.Metric200bD,
				t.Metric201bN = s.Metric201bN,
				t.Metric201bD = s.Metric201bD,
				t.Metric202bN = s.Metric202bN,
				t.Metric202bD = s.Metric202bD,
				t.Metric224bN = s.Metric224bN,
				t.Metric224bD = s.Metric224bD,
				t.Metric302bN = s.Metric302bN,
				t.Metric302bD = s.Metric302bD,
				t.Metric303bN = s.Metric303bN,
				t.Metric303bD = s.Metric303bD,
				t.Metric304bN = s.Metric304bN,
				t.Metric304bD = s.Metric304bD,
				t.Metric305bN = s.Metric305bN,
				t.Metric305bD = s.Metric305bD,
				t.Metric410bN = s.Metric410bN,
				t.Metric410bD = s.Metric410bD,
				t.Metric411bN = s.Metric411bN,
				t.Metric411bD = s.Metric411bD
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					YearId,
					RegionId,
					RegionKey,
					ContentId,
					ContentKey,
					DemographicId,
					DemographicKey,
					Metric103b,
					Metric200bN,
					Metric200bD,
					Metric201bN,
					Metric201bD,
					Metric202bN,
					Metric202bD,
					Metric224bN,
					Metric224bD,
					Metric302bN,
					Metric302bD,
					Metric303bN,
					Metric303bD,
					Metric304bN,
					Metric304bD,
					Metric305bN,
					Metric305bD,
					Metric410bN,
					Metric410bD,
					Metric411bN,
					Metric411bD
			)
		VALUES
			(
				s.YearId,
				s.RegionId,
				s.RegionKey,
				s.ContentId,
				s.ContentKey,
				s.DemographicId,
				s.DemographicKey,
				s.Metric103b,
				s.Metric200bN,
				s.Metric200bD,
				s.Metric201bN,
				s.Metric201bD,
				s.Metric202bN,
				s.Metric202bD,
				s.Metric224bN,
				s.Metric224bD,
				s.Metric302bN,
				s.Metric302bD,
				s.Metric303bN,
				s.Metric303bD,
				s.Metric304bN,
				s.Metric304bD,
				s.Metric305bN,
				s.Metric305bD,
				s.Metric410bN,
				s.Metric410bD,
				s.Metric411bN,
				s.Metric411bD
			);

		SET @CollegeCounter += 1;
	END;

	SET @YearCounter += 1;
	SET @CollegeCounter = 1;

END;