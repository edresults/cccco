SELECT
	-- count(distinct student_id)
	*
FROM
	(
		SELECT
			sx.college_id,
			sx.student_id,
			cb.top_code,
			CCNext = 
				isnull(
					(
						SELECT DISTINCT
							1
						FROM
							comis.studntid id1
							inner join
							comis.sxenrlm sx1
								on sx1.college_id = id1.college_id
								and sx1.student_id = id1.student_id
							inner join
							comis.cbcrsinv cb1
								on  cb1.college_id     = sx1.college_id
								and cb1.course_id      = sx1.course_id
								and cb1.control_number = sx1.control_number
								and cb1.term_id        = sx1.term_id
							inner join
							comis.Term t1
								on  t1.TermCode = sx1.term_id
							inner join
							lbswp.Year y1
								on y1.YearCode = t1.YearCode
							inner join
							lbswp.Mark m1
								on  m1.MarkCode = sx1.Grade
							inner join
							lbswp.Program p1
								on  p1.ProgramCode = cb1.top_code
							inner join
							lbswp.Occupation o1
								on  o1.OccupationCode = cb1.sam_code
							inner join
							lbswp.Credit c1
								on  c1.CreditCode = cb1.credit_status
							inner join
							lbswp.College i1
								on i1.CollegeCode = sx1.college_id
						WHERE
							y1.YearCode = y.YearCodeNext
							and (
								id1.ssn = id.ssn
								or
								id1.InterSegmentKey = id.InterSegmentKey
							)
					),
				0),
			UNNext =
				isnull(
					(
						SELECT DISTINCT
							1
						FROM
							comis.Transfer tr
							inner join
							comis.Term t2
								on t2.BeginDate <= tr.TransferDate
								and t2.EndDate >= tr.TransferDate
							inner join
							lbswp.Year y2
								on y2.YearCode = t2.YearCodeNext
						WHERE
							tr.InstitutionType = '4'
							and id.ssn = tr.StudentSsn
							and y2.YearCode = y.YearCode
				 ),
				0)
		FROM
			comis.studntid id
			inner join
			comis.sxenrlm sx
				on sx.college_id = id.college_id
				and sx.student_id = id.student_id
			inner join
			comis.cbcrsinv cb
				on  cb.college_id     = sx.college_id
				and cb.course_id      = sx.course_id
				and cb.control_number = sx.control_number
				and cb.term_id        = sx.term_id
			inner join
			comis.Term t
				on  t.TermCode = sx.term_id
			inner join
			lbswp.Year y
				on y.YearCode = t.YearCode
			inner join
			lbswp.Mark m
				on  m.MarkCode = sx.Grade
			inner join
			lbswp.Program p
				on  p.ProgramCode = cb.top_code
			inner join
			lbswp.Occupation o
				on  o.OccupationCode = cb.sam_code
			inner join
			lbswp.Credit c
				on  c.CreditCode = cb.credit_status
			inner join
			lbswp.College i
				on i.CollegeCode = sx.college_id
		WHERE
			sx.college_id = '641'
			and y.YearCode = '160'
			and cb.top_code = '050200'
	) a
-- WHERE
-- 	CCNext = 0;