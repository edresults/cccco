--SELECT
--	*
--FROM
--	Launchboard4Top6.dbo.CCStudentCohortTableMultiYr
--WHERE
--	cc_ipeds_code = '113193'
--	and program_code = '050200'
--	and Exiter = 1
--	and academic_year = 2016
--	and student_id = 0xA62DC59702BD46CD2E430A179B29830986EC68E2698F8AA68AF828C2138BC6C807DE9942267DCBCB33E4C0855DB01AC77FFE6535A79478E412FA0A9918D5921A;

SELECT
	*
FROM
	calpass.comis.studntid id
	inner join
	calpass.comis.stterm st
		on st.college_id = id.college_id
		and st.student_id = id.student_id
	inner join
	calpass.comis.Term t
		on t.TermCode = st.term_id
	inner join
	calpass.lbswp.GoldenRecord gr
		on gr.CollegeCode = st.college_id
		and gr.StudentId = st.student_id
		and gr.YearCode = t.YearCode
	inner join
	calpass.comis.sxenrlm sx
		on sx.college_id = st.college_id
		and sx.student_id = st.student_id
		and sx.term_id = st.term_id
WHERE
	ssn = 0xA62DC59702BD46CD2E430A179B29830986EC68E2698F8AA68AF828C2138BC6C807DE9942267DCBCB33E4C0855DB01AC77FFE6535A79478E412FA0A9918D5921A
	and t.YearCode = '170';