DECLARE
	@CollegeCode   char(3)     = '641',
	@YearCode      char(3)     = '160',
	@ContentId     varchar(30) = 'ProgramId',
	@DemographicId varchar(30) = 'DemographicId',
	@ProgramCode   char(6)     = '050200';

SELECT
	CollegeCode           = u.CollegeCode,
	YearCode              = u.YearCode,
	ContentIdentifier     = u.ContentIdentifier,
	ContentKey            = u.ContentKey,
	c.Label,
	d.Label,
	DemographicId         = d.DemographicId,
	DemographicIdentifier = u.DemographicIdentifier,
	DemographicKey        = u.DemographicKey,
	Enrollments           = sum(u.EnrolledSections),
	FullLoadUnits         = sum(case when NonIntroUnits >= 12 then 1 else 0 end),
	-- FullLoadHours         = sum(case when NonIntroHours >= 48 then 1 else 0 end),
	Awards                = sum(convert(tinyint, u.IsAwardProgram)),
	Transfers             = sum(convert(tinyint, u.IsUNEnrolledYearCodeNextFirst))
FROM
	lbswp.GoldenRecord gr
UNPIVOT
	(
		DemographicKey for DemographicIdentifier in (gr.DemographicId, gr.AgeId, gr.GenderId, gr.RaceId)
	) uu
UNPIVOT
	(
		ContentKey for ContentIdentifier in (ContentId, VocationId, SectorId, SubdisciplineId, ProgramId)
	) u
	inner join
	lbswp.Demographic d
		on d.Identifier = u.DemographicIdentifier
	inner join
	lbswp.Content c
		on c.Identifier = u.ContentIdentifier
WHERE
	u.CollegeCode = @CollegeCode
	and u.YearCode = @YearCode
	and u.ContentIdentifier = @ContentId
	and u.DemographicIdentifier = @DemographicId
	and u.ContentKey = (SELECT p.ProgramId FROM lbswp.Program p WHERE p.ProgramCode = @ProgramCode)
	and u.DemographicKey = 0
GROUP BY
	u.CollegeCode,
	u.YearCode,
	u.ContentIdentifier,
	u.ContentKey,
	u.DemographicIdentifier,
	u.DemographicKey,
	c.Label,
	d.Label,
	d.DemoGraphicId
ORDER BY
	1,2,3,4,5,6,7,8