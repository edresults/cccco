SELECT
	*
FROM
	(
		SELECT
			a.YearId,
			a.RegionId,
			a.RegionKey,
			a.ContentId,
			a.ContentKey,
			a.DemographicId,
			a.DemographicKey,
			Metric103b  = isnull(round(convert(decimal(5,2), 100.00 * a.Metric103b  / nullif(d.Metric103b,  0)), 2), 100.00),
			Metric200bN = isnull(round(convert(decimal(5,2), 100.00 * a.Metric200bN / nullif(d.Metric200bN, 0)), 2), 100.00),
			Metric200bD = isnull(round(convert(decimal(5,2), 100.00 * a.Metric200bD / nullif(d.Metric200bD, 0)), 2), 100.00),
			Metric201bN = isnull(round(convert(decimal(5,2), 100.00 * a.Metric201bN / nullif(d.Metric201bN, 0)), 2), 100.00),
			Metric201bD = isnull(round(convert(decimal(5,2), 100.00 * a.Metric201bD / nullif(d.Metric201bD, 0)), 2), 100.00),
			Metric202bN = isnull(round(convert(decimal(5,2), 100.00 * a.Metric202bN / nullif(d.Metric202bN, 0)), 2), 100.00),
			Metric202bD = isnull(round(convert(decimal(5,2), 100.00 * a.Metric202bD / nullif(d.Metric202bD, 0)), 2), 100.00),
			Metric224bN = isnull(round(convert(decimal(5,2), 100.00 * a.Metric224bN / nullif(d.Metric224bN, 0)), 2), 100.00),
			Metric224bD = isnull(round(convert(decimal(5,2), 100.00 * a.Metric224bD / nullif(d.Metric224bD, 0)), 2), 100.00),
			Metric302bN = isnull(round(convert(decimal(5,2), 100.00 * a.Metric302bN / nullif(d.Metric302bN, 0)), 2), 100.00),
			Metric302bD = isnull(round(convert(decimal(5,2), 100.00 * a.Metric302bD / nullif(d.Metric302bD, 0)), 2), 100.00),
			Metric303bN = isnull(round(convert(decimal(5,2), 100.00 * a.Metric303bN / nullif(d.Metric303bN, 0)), 2), 100.00),
			Metric303bD = isnull(round(convert(decimal(5,2), 100.00 * a.Metric303bD / nullif(d.Metric303bD, 0)), 2), 100.00),
			Metric304bN = isnull(round(convert(decimal(5,2), 100.00 * a.Metric304bN / nullif(d.Metric304bN, 0)), 2), 100.00),
			Metric304bD = isnull(round(convert(decimal(5,2), 100.00 * a.Metric304bD / nullif(d.Metric304bD, 0)), 2), 100.00),
			Metric305bN = isnull(round(convert(decimal(5,2), 100.00 * a.Metric305bN / nullif(d.Metric305bN, 0)), 2), 100.00),
			Metric305bD = isnull(round(convert(decimal(5,2), 100.00 * a.Metric305bD / nullif(d.Metric305bD, 0)), 2), 100.00),
			Metric410bN = isnull(round(convert(decimal(5,2), 100.00 * a.Metric410bN / nullif(d.Metric410bN, 0)), 2), 100.00),
			Metric410bD = isnull(round(convert(decimal(5,2), 100.00 * a.Metric410bD / nullif(d.Metric410bD, 0)), 2), 100.00),
			Metric411bN = isnull(round(convert(decimal(5,2), 100.00 * a.Metric411bN / nullif(d.Metric411bN, 0)), 2), 100.00),
			Metric411bD = isnull(round(convert(decimal(5,2), 100.00 * a.Metric411bD / nullif(d.Metric411bD, 0)), 2), 100.00)
		FROM
			(
				SELECT
					YearId,
					RegionId = 
						case
							when rc.RegionCollegeId is not null then rc.RegionId
							when ra.RegionMacroId   is not null then ra.RegionId
							when ri.RegionMicroId   is not null then ri.RegionId
							else 1
						end,
					RegionKey = 
						case
							when rc.RegionCollegeId is not null then rc.RegionCollegeId
							when ra.RegionMacroId   is not null then ra.RegionMacroId
							when ri.RegionMicroId   is not null then ri.RegionMicroId
							else 1
						end,
					ContentId = 
						case
							when p.ProgramId  is not null then p.ContentId
							when s.SectorId   is not null then s.ContentId
							when v.VocationId is not null then v.ContentId
							else 1
						end,
					ContentKey = 
						case
							when p.ProgramId  is not null then p.ProgramId
							when s.SectorId   is not null then s.SectorId
							when v.VocationId is not null then v.VocationId
							else 1
						end,
					DemographicId = 
						case
							when a.AgeId is not null then a.DemographicId
							else 1
						end,
					DemographicKey = 
						case
							when a.AgeId is not null then a.AgeId
							else 1
						end,
					Metric103b      = max(case when metric_id = '103b'   then metric_value end),
					Metric200bN     = max(case when metric_id = '200b'   then metric_value end),
					Metric200bD     = max(case when metric_id = '200b'   then metric_denom end),
					Metric201bN     = max(case when metric_id = '201b'   then metric_value end),
					Metric201bD     = max(case when metric_id = '201b'   then metric_denom end),
					Metric202bN     = max(case when metric_id = '202b'   then metric_value end),
					Metric202bD     = max(case when metric_id = '202b'   then metric_denom end),
					Metric224bN     = max(case when metric_id = '224b'   then metric_value end),
					Metric224bD     = max(case when metric_id = '224b'   then metric_denom end),
					Metric302bN     = max(case when metric_id = '302b'   then metric_value end),
					Metric302bD     = max(case when metric_id = '302b'   then metric_denom end),
					Metric303bN     = max(case when metric_id = '303b'   then metric_value end),
					Metric303bD     = max(case when metric_id = '303b'   then metric_denom end),
					Metric304bN     = max(case when metric_id = '304b'   then metric_value end),
					Metric304bD     = max(case when metric_id = '304b'   then metric_denom end),
					Metric305bN     = max(case when metric_id = '305b'   then metric_value end),
					Metric305bD     = max(case when metric_id = '305b'   then metric_denom end),
					Metric402bN     = max(case when metric_id = '402b'   then metric_value end),
					Metric402bD     = max(case when metric_id = '402b'   then metric_denom end),
					Metric404bN     = max(case when metric_id = '404b'   then metric_value end),
					Metric404bD     = max(case when metric_id = '404b'   then metric_denom end),
					Metric406bN     = max(case when metric_id = '406b'   then metric_value end),
					Metric406bD     = max(case when metric_id = '406b'   then metric_denom end),
					Metric408bN     = max(case when metric_id = '408b'   then metric_value end),
					Metric408bD     = max(case when metric_id = '408b'   then metric_denom end),
					Metric410bN     = max(case when metric_id = '410b'   then metric_value end),
					Metric410bD     = max(case when metric_id = '410b'   then metric_denom end),
					Metric411bN     = max(case when metric_id = '411b'   then metric_value end),
					Metric411bD     = max(case when metric_id = '411b'   then metric_denom end),
					Metric412bN     = max(case when metric_id = '412b'   then metric_value end),
					Metric412bD     = max(case when metric_id = '412b'   then metric_denom end),
					Metric413bN     = max(case when metric_id = '413b'   then metric_value end),
					Metric413bD     = max(case when metric_id = '413b'   then metric_denom end),
					Metric414bN     = max(case when metric_id = '414b'   then metric_value end),
					Metric414bD     = max(case when metric_id = '414b'   then metric_denom end),
					Metric500beN    = max(case when metric_id = '500be'  then metric_value end),
					Metric500beD    = max(case when metric_id = '500be'  then metric_denom end),
					Metric501beN    = max(case when metric_id = '501be'  then metric_value end),
					Metric501beD    = max(case when metric_id = '501be'  then metric_denom end),
					Metric513bckN   = max(case when metric_id = '513bck' then metric_value end),
					Metric513bckD   = max(case when metric_id = '513bck' then metric_denom end)
				FROM
					Launchboard4Top6.dbo.CCStudentEquityTableAgeOldScript t
					inner join
					Year y
						on y.YearEnd = t.academic_year
					left outer join
					RegionMacro ra
						on ra.Label = t.macroregion_name
					left outer join
					RegionMicro ri
						on ri.Label = t.microregion_name
					left outer join
					RegionCollege rc
						on rc.IpedsCode = t.cc_ipeds_code
					left outer join
					Program p
						on p.ProgramCode = t.program_code
					left outer join
					Sector s
						on s.Label = t.sector_name
					left outer join
					Vocation v
						on v.VocationId = case when t.cte_program = 'Yes' then 1 when t.cte_program = 'No' then 2 end
					left outer join
					Age a
						on a.Label = t.subgroup
				WHERE
					academic_year = '2012'
				GROUP BY
					YearId,
					case
						when rc.RegionCollegeId is not null then rc.RegionId
						when ra.RegionMacroId   is not null then ra.RegionId
						when ri.RegionMicroId   is not null then ri.RegionId
						else 1
					end,
					case
						when rc.RegionCollegeId is not null then rc.RegionCollegeId
						when ra.RegionMacroId   is not null then ra.RegionMacroId
						when ri.RegionMicroId   is not null then ri.RegionMicroId
						else 1
					end,
					case
						when p.ProgramId  is not null then p.ContentId
						when s.SectorId   is not null then s.ContentId
						when v.VocationId is not null then v.ContentId
						else 1
					end,
					case
						when p.ProgramId  is not null then p.ProgramId
						when s.SectorId   is not null then s.SectorId
						when v.VocationId is not null then v.VocationId
						else 1
					end,
					case
						when a.AgeId is not null then a.DemographicId
						else 1
					end,
					case
						when a.AgeId is not null then a.AgeId
						else 1
					end
			) a
			inner join
			MetricOutput d
				on  a.YearId         = d.YearId
				and a.RegionId       = d.RegionId
				and a.RegionKey      = d.RegionKey
				and a.ContentId      = d.ContentId
				and a.ContentKey     = d.ContentKey
				and a.DemographicId  = d.DemographicId
				and a.DemographicKey = d.DemographicKey
	) a
WHERE
	Metric103b < 95.00
	or Metric103b > 105.00
	or Metric200bN < 95.00
	or Metric200bN > 105.00
	or Metric200bD < 95.00
	or Metric200bD > 105.00
	or Metric201bN < 95.00
	or Metric201bN > 105.00
	or Metric201bD < 95.00
	or Metric201bD > 105.00
	or Metric202bN < 95.00
	or Metric202bN > 105.00
	or Metric202bD < 95.00
	or Metric202bD > 105.00
	or Metric224bN < 95.00
	or Metric224bN > 105.00
	or Metric224bD < 95.00
	or Metric224bD > 105.00
	or Metric302bN < 95.00
	or Metric302bN > 105.00
	or Metric302bD < 95.00
	or Metric302bD > 105.00
	or Metric303bN < 95.00
	or Metric303bN > 105.00
	or Metric303bD < 95.00
	or Metric303bD > 105.00
	or Metric304bN < 95.00
	or Metric304bN > 105.00
	or Metric304bD < 95.00
	or Metric304bD > 105.00
	or Metric305bN < 95.00
	or Metric305bN > 105.00
	or Metric305bD < 95.00
	or Metric305bD > 105.00
	or Metric410bN < 95.00
	or Metric410bN > 105.00
	or Metric410bD < 95.00
	or Metric410bD > 105.00
ORDER BY
	YearId,
	RegionId,
	RegionKey,
	ContentId,
	ContentKey,
	DemographicId,
	DemographicKey;


-- SELECT
-- 	DemographicKey,
-- 	Metric103B
-- FROM
-- 	metricoutput
-- WHERE
-- 	YearId = 1
-- 	and RegionId = 1
-- 	and RegionKey = 1
-- 	and ContentId = 3
-- 	and ContentKey = 13
-- 	and DemographicId = 3

-- SELECT
-- 	*
-- FROM
-- 	Launchboard4Top6.dbo.CCStudentEquityTableAgeOldScript
-- WHERE
-- 	metric_id = '103b'
-- 	and academic_year = '2012'
-- 	and macroregion_name = 'All'
-- 	and microregion_name = 'All'
-- 	and sector = 'Unassigned'
-- ORDER BY
-- 	metric_id,
-- 	academic_year,
-- 	subgroup;