SET NOCOUNT ON;
SET ANSI_WARNINGS OFF;

DECLARE
	@Severity      tinyint       = 0,
	@State         tinyint       = 1,
	@Error         varchar(2048) = '',
	@YearCounter   tinyint       = 2,
	@YearMax       tinyint       = 0,
	@RegionCounter tinyint       = 1,
	@RegionMax     tinyint       = 0,
	@RegionId      tinyint       = 3;

SELECT
	@YearMax = max(YearId)
FROM
	dbo.Year;

SELECT
	@RegionMax = max(RegionMicroId)
FROM
	dbo.RegionMicro;

WHILE (@YearCounter <= @YearMax)
BEGIN

	SET @Error = 'Year: ' + convert(varchar, @YearCounter) + ' of ' + convert(varchar, @YearMax);
	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

	WHILE (@RegionCounter <= @RegionMax)
	BEGIN

		SET @Error = 'Micro Region: ' + convert(varchar, @RegionCounter) + ' of ' + convert(varchar, @RegionMax);
		RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

		TRUNCATE TABLE StudentSwirl;
		TRUNCATE TABLE StudentSwirlData;
		TRUNCATE TABLE StudentSwirlSummary;
		TRUNCATE TABLE MetricSwirlOutput;

		INSERT INTO
			StudentSwirl
		SELECT
			sm.StudentId
		FROM
			StudentMetrics sm
			inner join
			RegionCollege rc
				on rc.RegionCollegeId = sm.RegionCollegeId
			inner join
			RegionMicro rm
				on rm.RegionMicroId = rc.RegionMicroId
		WHERE
			sm.YearId = @YearCounter
			and rm.RegionMicroId = @RegionCounter
		GROUP BY
			sm.StudentId
		HAVING
			count(distinct sm.RegionCollegeId) > 1;

		INSERT INTO
			StudentSwirlData
		SELECT
			YearId         = YearId,
			StudentId      = StudentId,
			RegionId       = RegionId,
			RegionKey      = RegionKey,
			ContentId      = ContentId,
			ContentKey     = ContentKey,
			DemographicId  = DemographicId,
			DemographicKey = DemographicKey,
			NonIntro       = max(convert(tinyint, NonIntro)),
			SkillsBuilder  = max(convert(tinyint, SkillsBuilder)),
			Exiter         = max(convert(tinyint, Exiter)),
			EconDis        = max(convert(tinyint, EconDis)),
			TermRet        = max(convert(tinyint, TermRet)),
			TermRegRet     = max(convert(tinyint, TermRegRet)),
			Persist        = max(convert(tinyint, Persist)),
			RegPersist     = max(convert(tinyint, RegPersist)),
			Transfer       = max(convert(tinyint, Transfer)),
			SBWageGain     = max(convert(tinyint, SBWageGain))
		FROM
			(
				SELECT
					sm.YearId,
					sm.RegionCollegeId,
					sm.StudentId,
					sm.ContentId,
					sm.VocationId,
					sm.SectorId,
					sm.SubdisciplineId,
					sm.ProgramId,
					sm.DemographicId,
					sm.GenderId,
					sm.AgeId,
					sm.EthnicityId,
					sm.PovertyId,
					sm.NonIntro,
					sm.SkillsBuilder,
					sm.Exiter,
					sm.EconDis,
					sm.TermRet,
					sm.TermRegRet,
					sm.Persist,
					sm.RegPersist,
					sm.Transfer,
					sm.SBWageGain
				FROM
					StudentSwirl sc
					inner join
					StudentMetrics sm
						on sc.StudentId = sm.StudentId
					inner join
					RegionCollege rc
						on rc.RegionCollegeId = sm.RegionCollegeId
					inner join
					RegionMicro rm
						on rm.RegionMicroId = rc.RegionMicroId
				WHERE
					sm.YearId = @YearCounter
					and rm.RegionMicroId = @RegionCounter
			) a
		UNPIVOT
			(
				DemographicKey for DemographicIdentifier in (DemographicId, GenderId, AgeId, EthnicityId, PovertyId)
			) u
		UNPIVOT
			(
				ContentKey for ContentIdentifier in (ContentId, VocationId, SectorId, SubdisciplineId, ProgramId)
			) u
		UNPIVOT
			(
				RegionKey for RegionIdentifier in (RegionCollegeId)
			) u
			inner join
			Demographic d
				on d.Identifier = u.DemographicIdentifier
			inner join
			Content c
				on c.Identifier = u.ContentIdentifier
			inner join
			Region r
				on r.Identifier = u.RegionIdentifier
		GROUP BY
			YearId,
			StudentId,
			RegionId,
			RegionKey,
			ContentId,
			ContentKey,
			DemographicId,
			DemographicKey;

		INSERT
			StudentSwirlSummary
		SELECT
			YearId             = a.YearId,
			StudentId          = a.StudentId,
			ContentId          = a.ContentId,
			ContentKey         = a.ContentKey,
			DemographicId      = a.DemographicId,
			DemographicKey     = a.DemographicKey,
			NonIntro           = max(a.NonIntro),
			SkillsBuilder      = max(a.SkillsBuilder),
			Exiter             = max(a.Exiter),
			EconDis            = max(a.EconDis),
			TermRet            = max(a.TermRet),
			TermRegRet         = max(a.TermRegRet),
			Persist            = max(a.Persist),
			RegPersist         = max(a.RegPersist),
			Transfer           = max(a.Transfer),
			SBWageGain         = max(a.SBWageGain),
			Duplicates         = -(count(distinct a.RegionKey) - 1)
		FROM
			StudentSwirlData a
			inner join
			StudentSwirlData b
				on  a.YearId          = b.YearId
				and a.StudentId       = b.StudentId
				and a.RegionId        = b.RegionId
				and a.ContentId       = b.ContentId
				and a.ContentKey      = b.ContentKey
				and a.DemographicId   = b.DemographicId
				and a.DemographicKey  = b.DemographicKey
				and a.RegionKey      <> b.RegionKey
		GROUP BY
			a.YearId,
			a.StudentId,
			a.ContentId,
			a.ContentKey,
			a.DemographicId,
			a.DemographicKey;

		INSERT
			MetricSwirlOutput
		SELECT
			YearId         = YearId,
			RegionId       = RegionId,
			RegionKey      = RegionKey,
			ContentId      = ContentId,
			ContentKey     = ContentKey,
			DemographicId  = DemographicId,
			DemographicKey = DemographicKey,
			Metric103b     = sum(Metric103b),
			Metric200bN    = sum(Metric200bN),
			Metric200bD    = sum(Metric200bD),
			Metric201bN    = sum(Metric201bN),
			Metric201bD    = sum(Metric201bD),
			Metric202bN    = sum(Metric202bN),
			Metric202bD    = sum(Metric202bD),
			Metric224bN    = sum(Metric224bN),
			Metric224bD    = sum(Metric224bD),
			Metric302bN    = sum(Metric302bN),
			Metric302bD    = sum(Metric302bD),
			Metric303bN    = sum(Metric303bN),
			Metric303bD    = sum(Metric303bD),
			Metric304bN    = sum(Metric304bN),
			Metric304bD    = sum(Metric304bD),
			Metric305bN    = sum(Metric305bN),
			Metric305bD    = sum(Metric305bD),
			Metric410bN    = sum(Metric410bN),
			Metric410bD    = sum(Metric410bD),
			Metric411bN    = sum(Metric411bN),
			Metric411bD    = sum(Metric411bD)
		FROM
			(
				SELECT
					YearId         = YearId,
					RegionId       = @RegionId,
					RegionKey      = @RegionCounter,
					ContentId      = ContentId,
					ContentKey     = ContentKey,
					DemographicId  = DemographicId,
					DemographicKey = DemographicKey,
					Metric103b     = sum(isnull(Duplicates, 0)),
					Metric200bN    = sum(isnull(case when NonIntro = 1 then Duplicates end, 0)),
					Metric200bD    = sum(isnull(Duplicates, 0)),
					Metric201bN    = sum(isnull(case when SkillsBuilder = 1 then Duplicates end, 0)),
					Metric201bD    = sum(isnull(Duplicates, 0)),
					Metric202bN    = sum(isnull(case when Exiter = 1 then Duplicates end, 0)),
					Metric202bD    = sum(isnull(Duplicates, 0)),
					Metric224bN    = sum(isnull(case when EconDis = 1 then Duplicates end, 0)),
					Metric224bD    = sum(isnull(Duplicates, 0)),
					Metric302bN    = sum(isnull(case when TermRet = 1 then Duplicates end, 0)),
					Metric302bD    = sum(isnull(case when TermRet is not null then Duplicates end, 0)),
					Metric303bN    = sum(isnull(case when TermRegRet = 1 then Duplicates end, 0)),
					Metric303bD    = sum(isnull(case when TermRegRet is not null then Duplicates end, 0)),
					Metric304bN    = sum(isnull(case when Persist = 1 then Duplicates end, 0)),
					Metric304bD    = sum(isnull(case when Persist is not null then Duplicates end, 0)),
					Metric305bN    = sum(isnull(case when RegPersist = 1 then Duplicates end, 0)),
					Metric305bD    = sum(isnull(case when RegPersist is not null then Duplicates end, 0)),
					Metric410bN    = sum(isnull(case when Transfer = 1 then Duplicates end, 0)),
					Metric410bD    = sum(isnull(Duplicates, 0)),
					Metric411bN    = sum(isnull(case when SBWageGain = 1 then Duplicates end, 0)),
					Metric411bD    = sum(isnull(Duplicates, 0))
				FROM
					StudentSwirlSummary
				GROUP BY
					YearId,
					ContentId,
					ContentKey,
					DemographicId,
					DemographicKey
				UNION
				SELECT
					YearId         = mo.YearId,
					RegionId       = rm.RegionId,
					RegionKey      = rm.RegionMicroId,
					ContentId      = mo.ContentId,
					ContentKey     = mo.ContentKey,
					DemographicId  = mo.DemographicId,
					DemographicKey = mo.DemographicKey,
					Metric103b     = sum(mo.Metric103b),
					Metric200bN    = sum(mo.Metric200bN),
					Metric200bD    = sum(mo.Metric200bD),
					Metric201bN    = sum(mo.Metric201bN),
					Metric201bD    = sum(mo.Metric201bD),
					Metric202bN    = sum(mo.Metric202bN),
					Metric202bD    = sum(mo.Metric202bD),
					Metric224bN    = sum(mo.Metric224bN),
					Metric224bD    = sum(mo.Metric224bD),
					Metric302bN    = sum(mo.Metric302bN),
					Metric302bD    = sum(mo.Metric302bD),
					Metric303bN    = sum(mo.Metric303bN),
					Metric303bD    = sum(mo.Metric303bD),
					Metric304bN    = sum(mo.Metric304bN),
					Metric304bD    = sum(mo.Metric304bD),
					Metric305bN    = sum(mo.Metric305bN),
					Metric305bD    = sum(mo.Metric305bD),
					Metric410bN    = sum(mo.Metric410bN),
					Metric410bD    = sum(mo.Metric410bD),
					Metric411bN    = sum(mo.Metric411bN),
					Metric411bD    = sum(mo.Metric411bD)
				FROM
					MetricOutput mo
					inner join
					RegionCollege rc
						on rc.RegionCollegeId = mo.RegionKey
					inner join
					RegionMicro rm
						on rm.RegionMicroId = rc.RegionMicroId
				WHERE
					mo.YearId = @YearCounter
					and rm.RegionMicroId = @RegionCounter
				GROUP BY
					mo.YearId,
					rm.RegionId,
					rm.RegionMicroId,
					mo.ContentId,
					mo.ContentKey,
					mo.DemographicId,
					mo.DemographicKey
			) a
		GROUP BY
			YearId,
			RegionId,
			RegionKey,
			ContentId,
			ContentKey,
			DemographicId,
			DemographicKey;

		MERGE
			MetricOutput t
		USING
			MetricSwirlOutput s
		ON
			(
				s.YearId             = t.YearId
				and s.RegionId       = t.RegionId
				and s.RegionKey      = t.RegionKey
				and s.ContentId      = t.ContentId
				and s.ContentKey     = t.ContentKey
				and s.DemographicId  = t.DemographicId
				and s.DemographicKey = t.DemographicKey
			)
		WHEN MATCHED THEN
			UPDATE SET
				t.Metric103b  = s.Metric103b,
				t.Metric200bN = s.Metric200bN,
				t.Metric200bD = s.Metric200bD,
				t.Metric201bN = s.Metric201bN,
				t.Metric201bD = s.Metric201bD,
				t.Metric202bN = s.Metric202bN,
				t.Metric202bD = s.Metric202bD,
				t.Metric224bN = s.Metric224bN,
				t.Metric224bD = s.Metric224bD,
				t.Metric302bN = s.Metric302bN,
				t.Metric302bD = s.Metric302bD,
				t.Metric303bN = s.Metric303bN,
				t.Metric303bD = s.Metric303bD,
				t.Metric304bN = s.Metric304bN,
				t.Metric304bD = s.Metric304bD,
				t.Metric305bN = s.Metric305bN,
				t.Metric305bD = s.Metric305bD,
				t.Metric410bN = s.Metric410bN,
				t.Metric410bD = s.Metric410bD,
				t.Metric411bN = s.Metric411bN,
				t.Metric411bD = s.Metric411bD
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					YearId,
					RegionId,
					RegionKey,
					ContentId,
					ContentKey,
					DemographicId,
					DemographicKey,
					Metric103b,
					Metric200bN,
					Metric200bD,
					Metric201bN,
					Metric201bD,
					Metric202bN,
					Metric202bD,
					Metric224bN,
					Metric224bD,
					Metric302bN,
					Metric302bD,
					Metric303bN,
					Metric303bD,
					Metric304bN,
					Metric304bD,
					Metric305bN,
					Metric305bD,
					Metric410bN,
					Metric410bD,
					Metric411bN,
					Metric411bD
			)
		VALUES
			(
				s.YearId,
				s.RegionId,
				s.RegionKey,
				s.ContentId,
				s.ContentKey,
				s.DemographicId,
				s.DemographicKey,
				s.Metric103b,
				s.Metric200bN,
				s.Metric200bD,
				s.Metric201bN,
				s.Metric201bD,
				s.Metric202bN,
				s.Metric202bD,
				s.Metric224bN,
				s.Metric224bD,
				s.Metric302bN,
				s.Metric302bD,
				s.Metric303bN,
				s.Metric303bD,
				s.Metric304bN,
				s.Metric304bD,
				s.Metric305bN,
				s.Metric305bD,
				s.Metric410bN,
				s.Metric410bD,
				s.Metric411bN,
				s.Metric411bD
			);

		SET @RegionCounter += 1;
	END;

	SET @RegionCounter = 1;
	SET @YearCounter += 1;
END;