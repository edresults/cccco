
SET NOCOUNT ON;
SET ANSI_WARNINGS OFF;

DECLARE
	@Severity      tinyint       = 0,
	@State         tinyint       = 1,
	@Message       varchar(2048) = '';

DECLARE
	@YearIterator        tinyint       = 0,
	@YearUBound          tinyint       = 0,
	@RegionIterator      tinyint       = 0,
	@RegionLBound        tinyint       = 0,
	@RegionUBound        tinyint       = 0,
	@RegionChildIterator tinyint       = 0,
	@RegionChildUBound   tinyint       = 0;

SELECT
	@YearIterator = min(YearId),
	@YearUBound   = max(YearId)
FROM
	dbo.Year;

SELECT
	@RegionIterator = max(RegionId),
	@RegionLBound   = min(RegionId),
	@RegionUBound   = max(RegionId)
FROM
	dbo.Region;

WHILE (@YearIterator <= @YearUBound)
BEGIN

	SET @Message = 'Year: ' + convert(varchar, @YearIterator) + ' of ' + convert(varchar, @YearUBound);
	RAISERROR(@Message, @Severity, @State) WITH NOWAIT;
	SELECT @RegionIterator = max(RegionId) FROM dbo.Region;

	WHILE (@RegionIterator >= @RegionLBound)
	BEGIN
		
		SET @Message = '  ' + 'Region: ' + convert(varchar, @RegionIterator) + ' of ' + convert(varchar, @RegionUBound);
		RAISERROR(@Message, @Severity, @State) WITH NOWAIT;
		SELECT
			@RegionChildIterator = min(coalesce(rs.RegionStateId, ra.RegionMacroId, ri.RegionMicroId, rc.RegionCollegeId)),
			@RegionChildUBound   = max(coalesce(rs.RegionStateId, ra.RegionMacroId, ri.RegionMicroId, rc.RegionCollegeId))
		FROM
			dbo.Region r
			left outer join
			dbo.RegionState rs
				on rs.RegionId = r.RegionId
			left outer join
			dbo.RegionMacro ra
				on ra.RegionId = r.RegionId
			left outer join
			dbo.RegionMicro ri
				on ri.RegionId = r.RegionId
			left outer join
			dbo.RegionCollege rc
				on rc.RegionId = r.RegionId
		WHERE
			r.RegionId = @RegionIterator;
			

		WHILE (@RegionChildIterator <= @RegionChildUBound)
		BEGIN

			SELECT
				@Message = '    ' + r.Label + ': ' + convert(varchar, @RegionChildIterator) + ' of ' + convert(varchar, @RegionChildUBound)
			FROM
				dbo.Region r
				left outer join
				dbo.RegionState rs
					on rs.RegionId = r.RegionId
					and rs.RegionStateId = @RegionChildIterator
				left outer join
				dbo.RegionMacro ra
					on ra.RegionId = r.RegionId
					and ra.RegionMacroId = @RegionChildIterator
				left outer join
				dbo.RegionMicro ri
					on ri.RegionId = r.RegionId
					and ri.RegionMicroId = @RegionChildIterator
				left outer join
				dbo.RegionCollege rc
					on rc.RegionId = r.RegionId
					and rc.RegionCollegeId = @RegionChildIterator
			WHERE
				r.RegionId = @RegionIterator;
			RAISERROR(@Message, @Severity, @State) WITH NOWAIT;

			TRUNCATE TABLE StudentSwirl;
			TRUNCATE TABLE StudentSwirlData;
			TRUNCATE TABLE StudentSwirlSummary;
			TRUNCATE TABLE MetricSwirlOutput;
			TRUNCATE TABLE RegionCohort;

			IF (@RegionIterator = 4)
				BEGIN
					INSERT INTO
						dbo.MetricSwirlOutput
					SELECT
						YearId,
						RegionId,
						RegionKey,
						ContentId,
						ContentKey,
						DemographicId,
						DemographicKey,
						Metric103b   = count(StudentId),
						Metric200bN  = count(case when NonIntro = 1                                    then StudentId end),
						Metric200bD  = count(StudentId),
						Metric201bN  = count(case when SkillsBuilder = 1                               then StudentId end),
						Metric201bD  = count(StudentId),
						Metric202bN  = count(case when Exiter = 1                                      then StudentId end),
						Metric202bD  = count(StudentId),
						Metric206bN  = count(case when Foster = 1                                      then StudentId end),
						Metric206bD  = count(case when Foster is not null                              then StudentId end),
						Metric207bN  = count(case when Veteran = 1                                     then StudentId end),
						Metric207bD  = count(case when Veteran is not null                             then StudentId end),
						Metric208bN  = count(case when Disabled = 1                                    then StudentId end),
						Metric208bD  = count(case when Disabled is not null                            then StudentId end),
						Metric209bN  = count(case when FirstGen = 1                                    then StudentId end),
						Metric209bD  = count(case when FirstGen is not null                            then StudentId end),
						Metric210bN  = count(case when FinAid = 1                                      then StudentId end),
						Metric210bD  = count(case when FinAid is not null                              then StudentId end),
						Metric211bN  = count(case when EOPS = 1                                        then StudentId end),
						Metric211bD  = count(case when EOPS is not null                                then StudentId end),
						Metric212bN  = count(case when MathEngBS = 1                                   then StudentId end),
						Metric212bD  = count(StudentId),
						Metric213bN  = count(case when ESL = 1                                         then StudentId end),
						Metric213bD  = count(StudentId),
						Metric214bN  = count(case when FullTime = 1                                    then StudentId end),
						Metric214bD  = count(case when FullTime is not null                            then StudentId end),
						Metric215bN  = count(case when PartTime = 1                                    then StudentId end),
						Metric215bD  = count(case when PartTime is not null                            then StudentId end),
						Metric216bN  = count(case when FirstTime = 1                                   then StudentId end),
						Metric216bD  = count(StudentId),
						Metric218bN  = count(case when MultiSchool = 1                                 then StudentId end),
						Metric218bD  = count(StudentId),
						Metric219bN  = count(case when PrevHEAward = 1                                 then StudentId end),
						Metric219bD  = count(StudentId),
						Metric220bN  = count(case when NoPrevHEAward = 1                               then StudentId end),
						Metric220bD  = count(StudentId),
						Metric221bN  = count(case when PrevCCAward = 1                                 then StudentId end),
						Metric221bD  = count(StudentId),
						Metric222bN  = count(case when PrevBach = 1                                    then StudentId end),
						Metric222bD  = count(StudentId),
						Metric223bN  = count(case when PrevCTE = 1                                     then StudentId end),
						Metric223bD  = count(StudentId),
						Metric224bN  = count(case when EconDis = 1                                     then StudentId end),
						Metric224bD  = count(StudentId),
						Metric225bN  = count(case when EconDis = 0                                     then StudentId end),
						Metric225bD  = count(StudentId),
						Metric302bN  = count(case when TermRet = 1                                     then StudentId end),
						Metric302bD  = count(case when TermRet is not null                             then StudentId end),
						Metric303bN  = count(case when TermRegRet = 1                                  then StudentId end),
						Metric303bD  = count(case when TermRegRet is not null                          then StudentId end),
						Metric304bN  = count(case when Persist = 1                                     then StudentId end),
						Metric304bD  = count(case when Persist is not null                             then StudentId end),
						Metric305bN  = count(case when RegPersist = 1                                  then StudentId end),
						Metric305bD  = count(case when RegPersist is not null                          then StudentId end),
						Metric306bN  = count(case when NCTransAnyToSelect = 1                          then StudentId end),
						Metric306bD  = count(StudentId),
						Metric307bN  = count(case when NCTransAnyToAny = 1                             then StudentId end),
						Metric307bD  = count(case when NCTransAnyToAny is not null                     then StudentId end),
						Metric308bN  = count(case when NCTransSelectToSelect = 1                       then StudentId end),
						Metric308bD  = count(case when NCTransSelectToSelect is not null               then StudentId end),
						Metric309bN  = count(case when CTE12Units = 1                                  then StudentId end),
						Metric309bD  = count(StudentId),
						Metric310bN  = count(case when NC48Hours = 1                                   then StudentId end),
						Metric310bD  = count(StudentId),
						Metric410bN  = count(case when Transfer = 1                                    then StudentId end),
						Metric410bD  = count(StudentId),
						Metric411bN  = count(case when SBWageGain = 1                                  then StudentId end),
						Metric411bD  = count(case when SBWageGain is not null                          then StudentId end),
						Metric500btN = count(case when Employed2Qtrs = 1                               then StudentId end),
						Metric500btD = count(case when Employed2Qtrs is not null and Transfer = 1      then StudentId end),
						Metric500bkN = count(case when Employed2Qtrs = 1                               then StudentId end),
						Metric500bkD = count(case when Employed2Qtrs is not null and SkillsBuilder = 1 then StudentId end),
						Metric501btN = count(case when Employed4Qtrs = 1                               then StudentId end),
						Metric501btD = count(case when Employed4Qtrs is not null and Transfer = 1      then StudentId end),
						Metric501bkN = count(case when Employed4Qtrs = 1                               then StudentId end),
						Metric501bkD = count(case when Employed4Qtrs is not null and SkillsBuilder = 1 then StudentId end)
					FROM
						(
							SELECT
								YearId                = YearId,
								StudentId             = StudentId,
								RegionId              = RegionId,
								RegionKey             = RegionKey,
								ContentId             = ContentId,
								ContentKey            = ContentKey,
								DemographicId         = DemographicId,
								DemographicKey        = DemographicKey,
								CTE12Units            = max(convert(tinyint, CTE12Units)),
								Disabled              = max(convert(tinyint, Disabled)),
								EconDis               = max(convert(tinyint, EconDis)),
								Employed2Qtrs         = max(convert(tinyint, Employed2Qtrs)),
								Employed4Qtrs         = max(convert(tinyint, Employed4Qtrs)),
								EOPS                  = max(convert(tinyint, EOPS)),
								ESL                   = max(convert(tinyint, ESL)),
								Exiter                = max(convert(tinyint, Exiter)),
								FinAid                = max(convert(tinyint, FinAid)),
								FirstGen              = max(convert(tinyint, FirstGen)),
								FirstTime             = max(convert(tinyint, FirstTime)),
								Foster                = max(convert(tinyint, Foster)),
								FullTime              = max(convert(tinyint, FullTime)),
								MathEngBS             = max(convert(tinyint, MathEngBS)),
								MultiSchool           = max(convert(tinyint, MultiSchool)),
								NC48Hours             = max(convert(tinyint, NC48Hours)),
								NCTransAnyToAny       = max(convert(tinyint, NCTransAnyToAny)),
								NCTransAnyToSelect    = max(convert(tinyint, NCTransAnyToSelect)),
								NCTransSelectToSelect = max(convert(tinyint, NCTransSelectToSelect)),
								NonIntro              = max(convert(tinyint, NonIntro)),
								NoPrevHEAward         = max(convert(tinyint, NoPrevHEAward)),
								PartTime              = max(convert(tinyint, PartTime)),
								Persist               = max(convert(tinyint, Persist)),
								PrevBach              = max(convert(tinyint, PrevBach)),
								PrevCCAward           = max(convert(tinyint, PrevCCAward)),
								PrevCTE               = max(convert(tinyint, PrevCTE)),
								PrevHEAward           = max(convert(tinyint, PrevHEAward)),
								RegPersist            = max(convert(tinyint, RegPersist)),
								SBWageGain            = max(convert(tinyint, SBWageGain)),
								SkillsBuilder         = max(convert(tinyint, SkillsBuilder)),
								TermRegRet            = max(convert(tinyint, TermRegRet)),
								TermRet               = max(convert(tinyint, TermRet)),
								Transfer              = max(convert(tinyint, Transfer)),
								Veteran               = max(convert(tinyint, Veteran))
							FROM
								(
									SELECT
										YearId,
										RegionCollegeId,
										StudentId,
										ContentId,
										VocationId,
										SectorId,
										SubdisciplineId,
										ProgramId,
										DemographicId,
										GenderId,
										AgeId,
										EthnicityId,
										PovertyId,
										CTE12Units,
										Disabled,
										EconDis,
										Employed2Qtrs,
										Employed4Qtrs,
										EOPS,
										ESL,
										Exiter,
										FinAid,
										FirstGen,
										FirstTime,
										Foster,
										FullTime,
										MathEngBS,
										MultiSchool,
										NC48Hours,
										NCTransAnyToAny,
										NCTransAnyToSelect,
										NCTransSelectToSelect,
										NonIntro,
										NoPrevHEAward,
										PartTime,
										Persist,
										PrevBach,
										PrevCCAward,
										PrevCTE,
										PrevHEAward,
										RegPersist,
										SBWageGain,
										SkillsBuilder,
										TermRegRet,
										TermRet,
										Transfer,
										Veteran
									FROM
										dbo.StudentMetrics
									WHERE
										YearId = @YearIterator
										and RegionCollegeId = @RegionChildIterator
								) a
							UNPIVOT
								(
									DemographicKey for DemographicIdentifier in (DemographicId, GenderId, AgeId, EthnicityId, PovertyId)
								) u
							UNPIVOT
								(
									ContentKey for ContentIdentifier in (ContentId, VocationId, SectorId, SubdisciplineId, ProgramId)
								) u
							UNPIVOT
								(
									RegionKey for RegionIdentifier in (RegionCollegeId)
								) u
								inner join
								Demographic d
									on d.Identifier = u.DemographicIdentifier
								inner join
								Content c
									on c.Identifier = u.ContentIdentifier
								inner join
								Region r
									on r.Identifier = u.RegionIdentifier
							GROUP BY
								YearId,
								StudentId,
								RegionId,
								RegionKey,
								ContentId,
								ContentKey,
								DemographicId,
								DemographicKey
						) a
					GROUP BY
						YearId,
						RegionId,
						RegionKey,
						ContentId,
						ContentKey,
						DemographicId,
						DemographicKey;
				END;
			ELSE
				BEGIN
					INSERT INTO
						RegionCohort
						(
							RegionId,
							RegionKey,
							CollegeId
						)
					SELECT
						RegionId  = r.RegionId,
						RegionKey = coalesce(rs.RegionStateId, ra.RegionMacroId, ri.RegionMicroId, rc.RegionCollegeId),
						CollegeId = coalesce(rc1.RegionCollegeId, rc2.RegionCollegeId, rc3.RegionCollegeId, rc.RegionCollegeId)
					FROM
						Region r
						left outer join
						RegionState rs
							on rs.RegionId = r.RegionId
						left outer join
						RegionMacro ra
							on ra.RegionId = r.RegionId
						left outer join
						RegionMicro ri
							on ri.RegionId = r.RegionId
						left outer join
						RegionCollege rc
							on rc.RegionId = r.RegionId
						left outer join
						RegionCollege rc1
							on rc1.RegionMicroId = ri.RegionMicroId
						left outer join
						RegionCollege rc2
							on rc2.RegionMacroId = ra.RegionMacroId
						left outer join
						RegionCollege rc3
							on rc3.RegionStateId = rs.RegionStateId
					WHERE
						r.RegionId = @RegionIterator
						and coalesce(rs.RegionStateId, ra.RegionMacroId, ri.RegionMicroId, rc.RegionCollegeId) = @RegionChildIterator;

					INSERT INTO
						StudentSwirl
					SELECT DISTINCT
						sm.StudentId
					FROM
						StudentMetrics sm
						inner join
						RegionCohort rc
							on rc.CollegeId = sm.RegionCollegeId
					WHERE
						sm.YearId = @YearIterator
					GROUP BY
						sm.StudentId
					HAVING
						count(distinct sm.RegionCollegeId) > 1;

					INSERT INTO
						StudentSwirlData
						(
							YearId,
							StudentId,
							RegionId,
							RegionKey,
							ContentId,
							ContentKey,
							DemographicId,
							DemographicKey,
							CTE12Units,
							Disabled,
							EconDis,
							Employed2Qtrs,
							Employed4Qtrs,
							EOPS,
							ESL,
							Exiter,
							FinAid,
							FirstGen,
							FirstTime,
							Foster,
							FullTime,
							MathEngBS,
							MultiSchool,
							NC48Hours,
							NCTransAnyToAny,
							NCTransAnyToSelect,
							NCTransSelectToSelect,
							NonIntro,
							NoPrevHEAward,
							PartTime,
							Persist,
							PrevBach,
							PrevCCAward,
							PrevCTE,
							PrevHEAward,
							RegPersist,
							SBWageGain,
							SkillsBuilder,
							TermRegRet,
							TermRet,
							Transfer,
							Veteran
						)
					SELECT
						YearId                = YearId,
						StudentId             = StudentId,
						RegionId              = RegionId,
						RegionKey             = RegionKey,
						ContentId             = ContentId,
						ContentKey            = ContentKey,
						DemographicId         = DemographicId,
						DemographicKey        = DemographicKey,
						CTE12Units            = max(convert(tinyint, CTE12Units)),
						Disabled              = max(convert(tinyint, Disabled)),
						EconDis               = max(convert(tinyint, EconDis)),
						Employed2Qtrs         = max(convert(tinyint, Employed2Qtrs)),
						Employed4Qtrs         = max(convert(tinyint, Employed4Qtrs)),
						EOPS                  = max(convert(tinyint, EOPS)),
						ESL                   = max(convert(tinyint, ESL)),
						Exiter                = max(convert(tinyint, Exiter)),
						FinAid                = max(convert(tinyint, FinAid)),
						FirstGen              = max(convert(tinyint, FirstGen)),
						FirstTime             = max(convert(tinyint, FirstTime)),
						Foster                = max(convert(tinyint, Foster)),
						FullTime              = max(convert(tinyint, FullTime)),
						MathEngBS             = max(convert(tinyint, MathEngBS)),
						MultiSchool           = max(convert(tinyint, MultiSchool)),
						NC48Hours             = max(convert(tinyint, NC48Hours)),
						NCTransAnyToAny       = max(convert(tinyint, NCTransAnyToAny)),
						NCTransAnyToSelect    = max(convert(tinyint, NCTransAnyToSelect)),
						NCTransSelectToSelect = max(convert(tinyint, NCTransSelectToSelect)),
						NonIntro              = max(convert(tinyint, NonIntro)),
						NoPrevHEAward         = max(convert(tinyint, NoPrevHEAward)),
						PartTime              = max(convert(tinyint, PartTime)),
						Persist               = max(convert(tinyint, Persist)),
						PrevBach              = max(convert(tinyint, PrevBach)),
						PrevCCAward           = max(convert(tinyint, PrevCCAward)),
						PrevCTE               = max(convert(tinyint, PrevCTE)),
						PrevHEAward           = max(convert(tinyint, PrevHEAward)),
						RegPersist            = max(convert(tinyint, RegPersist)),
						SBWageGain            = max(convert(tinyint, SBWageGain)),
						SkillsBuilder         = max(convert(tinyint, SkillsBuilder)),
						TermRegRet            = max(convert(tinyint, TermRegRet)),
						TermRet               = max(convert(tinyint, TermRet)),
						Transfer              = max(convert(tinyint, Transfer)),
						Veteran               = max(convert(tinyint, Veteran))
					FROM
						(
							SELECT
								sm.YearId,
								sm.RegionCollegeId,
								sm.StudentId,
								sm.ContentId,
								sm.VocationId,
								sm.SectorId,
								sm.SubdisciplineId,
								sm.ProgramId,
								sm.DemographicId,
								sm.GenderId,
								sm.AgeId,
								sm.EthnicityId,
								sm.PovertyId,
								sm.CTE12Units,
								sm.Disabled,
								sm.EconDis,
								sm.Employed2Qtrs,
								sm.Employed4Qtrs,
								sm.EOPS,
								sm.ESL,
								sm.Exiter,
								sm.FinAid,
								sm.FirstGen,
								sm.FirstTime,
								sm.Foster,
								sm.FullTime,
								sm.MathEngBS,
								sm.MultiSchool,
								sm.NC48Hours,
								sm.NCTransAnyToAny,
								sm.NCTransAnyToSelect,
								sm.NCTransSelectToSelect,
								sm.NonIntro,
								sm.NoPrevHEAward,
								sm.PartTime,
								sm.Persist,
								sm.PrevBach,
								sm.PrevCCAward,
								sm.PrevCTE,
								sm.PrevHEAward,
								sm.RegPersist,
								sm.SBWageGain,
								sm.SkillsBuilder,
								sm.TermRegRet,
								sm.TermRet,
								sm.Transfer,
								sm.Veteran
							FROM
								StudentSwirl sc
								inner join
								StudentMetrics sm
									on sc.StudentId = sm.StudentId
								inner join
								RegionCohort rc
									on rc.CollegeId = sm.RegionCollegeId
							WHERE
								sm.YearId = @YearIterator
						) a
					UNPIVOT
						(
							DemographicKey for DemographicIdentifier in (DemographicId, GenderId, AgeId, EthnicityId, PovertyId)
						) u
					UNPIVOT
						(
							ContentKey for ContentIdentifier in (ContentId, VocationId, SectorId, SubdisciplineId, ProgramId)
						) u
					UNPIVOT
						(
							RegionKey for RegionIdentifier in (RegionCollegeId)
						) u
						inner join
						Demographic d
							on d.Identifier = u.DemographicIdentifier
						inner join
						Content c
							on c.Identifier = u.ContentIdentifier
						inner join
						Region r
							on r.Identifier = u.RegionIdentifier
					GROUP BY
						YearId,
						StudentId,
						RegionId,
						RegionKey,
						ContentId,
						ContentKey,
						DemographicId,
						DemographicKey;

					INSERT
						StudentSwirlSummary
						(
							YearId,
							StudentId,
							ContentId,
							ContentKey,
							DemographicId,
							DemographicKey,
							CTE12UnitsN,
							CTE12UnitsD,
							DisabledN,
							DisabledD,
							EconDisN,
							EconDisD,
							EconAdvN,
							EconAdvD,
							Employed2QtrsTranN,
							Employed2QtrsTranD,
							Employed2QtrsSBN,
							Employed2QtrsSBD,
							Employed4QtrsTranN,
							Employed4QtrsTranD,
							Employed4QtrsSBN,
							Employed4QtrsSBD,
							EOPSN,
							EOPSD,
							ESLN,
							ESLD,
							ExiterN,
							ExiterD,
							FinAidN,
							FinAidD,
							FirstGenN,
							FirstGenD,
							FirstTimeN,
							FirstTimeD,
							FosterN,
							FosterD,
							FullTimeN,
							FullTimeD,
							MathEngBSN,
							MathEngBSD,
							MultiSchoolN,
							MultiSchoolD,
							NC48HoursN,
							NC48HoursD,
							NCTransAnyToAnyN,
							NCTransAnyToAnyD,
							NCTransAnyToSelectN,
							NCTransAnyToSelectD,
							NCTransSelectToSelectN,
							NCTransSelectToSelectD,
							NonIntroN,
							NonIntroD,
							NoPrevHEAwardN,
							NoPrevHEAwardD,
							PartTimeN,
							PartTimeD,
							PersistN,
							PersistD,
							PrevBachN,
							PrevBachD,
							PrevCCAwardN,
							PrevCCAwardD,
							PrevCTEN,
							PrevCTED,
							PrevHEAwardN,
							PrevHEAwardD,
							RegPersistN,
							RegPersistD,
							SBWageGainN,
							SBWageGainD,
							SkillsBuilderN,
							SkillsBuilderD,
							TermRegRetN,
							TermRegRetD,
							TermRetN,
							TermRetD,
							TransferN,
							TransferD,
							VeteranN,
							VeteranD,
							Generic
						)
					SELECT
						YearId                 = a.YearId,
						StudentId              = a.StudentId,
						ContentId              = a.ContentId,
						ContentKey             = a.ContentKey,
						DemographicId          = a.DemographicId,
						DemographicKey         = a.DemographicKey,
						CTE12UnitsN            = count(case when a.CTE12Units = 1 then 1 end),
						CTE12UnitsD            = count(a.CTE12Units),
						DisabledN              = count(case when a.Disabled = 1 then 1 end),
						DisabledD              = count(a.Disabled),
						EconDisN               = count(case when a.EconDis = 1 then 1 end),
						EconDisD               = count(a.EconDis),
						EconAdvN               = count(case when a.EconDis = 0 then 1 end),
						EconAdvD               = count(a.EconDis),
						Employed2QtrsTranN     = count(case when a.Employed2Qtrs = 1 and Transfer = 1 then 1 end),
						Employed2QtrsTranD     = count(a.Employed2Qtrs),
						Employed2QtrsSBN       = count(case when a.Employed2Qtrs = 1 and SkillsBuilder = 1 then 1 end),
						Employed2QtrsSBD       = count(a.Employed2Qtrs),
						Employed4QtrsTranN     = count(case when a.Employed4Qtrs = 1 and Transfer = 1 then 1 end),
						Employed4QtrsTranD     = count(a.Employed4Qtrs),
						Employed4QtrsSBN       = count(case when a.Employed4Qtrs = 1 and SkillsBuilder = 1 then 1 end),
						Employed4QtrsSBD       = count(a.Employed4Qtrs),
						EOPSN                  = count(case when a.EOPS = 1 then 1 end),
						EOPSD                  = count(a.EOPS),
						ESLN                   = count(case when a.ESL = 1 then 1 end),
						ESLD                   = count(a.ESL),
						ExiterN                = count(case when a.Exiter = 1 then 1 end),
						ExiterD                = count(a.Exiter),
						FinAidN                = count(case when a.FinAid = 1 then 1 end),
						FinAidD                = count(a.FinAid),
						FirstGenN              = count(case when a.FirstGen = 1 then 1 end),
						FirstGenD              = count(a.FirstGen),
						FirstTimeN             = count(case when a.FirstTime = 1 then 1 end),
						FirstTimeD             = count(a.FirstTime),
						FosterN                = count(case when a.Foster = 1 then 1 end),
						FosterD                = count(a.Foster),
						FullTimeN              = count(case when a.FullTime = 1 then 1 end),
						FullTimeD              = count(a.FullTime),
						MathEngBSN             = count(case when a.MathEngBS = 1 then 1 end),
						MathEngBSD             = count(a.MathEngBS),
						MultiSchoolN           = count(case when a.MultiSchool = 1 then 1 end),
						MultiSchoolD           = count(a.MultiSchool),
						NC48HoursN             = count(case when a.NC48Hours = 1 then 1 end),
						NC48HoursD             = count(a.NC48Hours),
						NCTransAnyToAnyN       = count(case when a.NCTransAnyToAny = 1 then 1 end),
						NCTransAnyToAnyD       = count(a.NCTransAnyToAny),
						NCTransAnyToSelectN    = count(case when a.NCTransAnyToSelect = 1 then 1 end),
						NCTransAnyToSelectD    = count(a.NCTransAnyToSelect),
						NCTransSelectToSelectN = count(case when a.NCTransSelectToSelect = 1 then 1 end),
						NCTransSelectToSelectD = count(a.NCTransSelectToSelect),
						NonIntroN              = count(case when a.NonIntro = 1 then 1 end),
						NonIntroD              = count(a.NonIntro),
						NoPrevHEAwardN         = count(case when a.NoPrevHEAward = 1 then 1 end),
						NoPrevHEAwardD         = count(a.NoPrevHEAward),
						PartTimeN              = count(case when a.PartTime = 1 then 1 end),
						PartTimeD              = count(a.PartTime),
						PersistN               = count(case when a.Persist = 1 then 1 end),
						PersistD               = count(a.Persist),
						PrevBachN              = count(case when a.PrevBach = 1 then 1 end),
						PrevBachD              = count(a.PrevBach),
						PrevCCAwardN           = count(case when a.PrevCCAward = 1 then 1 end),
						PrevCCAwardD           = count(a.PrevCCAward),
						PrevCTEN               = count(case when a.PrevCTE = 1 then 1 end),
						PrevCTED               = count(a.PrevCTE),
						PrevHEAwardN           = count(case when a.PrevHEAward = 1 then 1 end),
						PrevHEAwardD           = count(a.PrevHEAward),
						RegPersistN            = count(case when a.RegPersist = 1 then 1 end),
						RegPersistD            = count(a.RegPersist),
						SBWageGainN            = count(case when a.SBWageGain = 1 then 1 end),
						SBWageGainD            = count(a.SBWageGain),
						SkillsBuilderN         = count(case when a.SkillsBuilder = 1 then 1 end),
						SkillsBuilderD         = count(a.SkillsBuilder),
						TermRegRetN            = count(case when a.TermRegRet = 1 then 1 end),
						TermRegRetD            = count(a.TermRegRet),
						TermRetN               = count(case when a.TermRet = 1 then 1 end),
						TermRetD               = count(a.TermRet),
						TransferN              = count(case when a.Transfer = 1 then 1 end),
						TransferD              = count(a.Transfer),
						VeteranN               = count(case when a.Veteran = 1 then 1 end),
						VeteranD               = count(a.Veteran),
						Generic                = count(*)
					FROM
						StudentSwirlData a
					GROUP BY
						a.YearId,
						a.StudentId,
						a.ContentId,
						a.ContentKey,
						a.DemographicId,
						a.DemographicKey
					ORDER BY
						a.YearId,
						a.StudentId,
						a.ContentId,
						a.ContentKey,
						a.DemographicId,
						a.DemographicKey;

					INSERT
						MetricSwirlOutput
					SELECT
						YearId         = YearId,
						RegionId       = RegionId,
						RegionKey      = RegionKey,
						ContentId      = ContentId,
						ContentKey     = ContentKey,
						DemographicId  = DemographicId,
						DemographicKey = DemographicKey,
						Metric103b     = sum(Metric103b),
						Metric200bN    = sum(Metric200bN),
						Metric200bD    = sum(Metric200bD),
						Metric201bN    = sum(Metric201bN),
						Metric201bD    = sum(Metric201bD),
						Metric202bN    = sum(Metric202bN),
						Metric202bD    = sum(Metric202bD),
						Metric206bN    = sum(Metric206bN),
						Metric206bD    = sum(Metric206bD),
						Metric207bN    = sum(Metric207bN),
						Metric207bD    = sum(Metric207bD),
						Metric208bN    = sum(Metric208bN),
						Metric208bD    = sum(Metric208bD),
						Metric209bN    = sum(Metric209bN),
						Metric209bD    = sum(Metric209bD),
						Metric210bN    = sum(Metric210bN),
						Metric210bD    = sum(Metric210bD),
						Metric211bN    = sum(Metric211bN),
						Metric211bD    = sum(Metric211bD),
						Metric212bN    = sum(Metric212bN),
						Metric212bD    = sum(Metric212bD),
						Metric213bN    = sum(Metric213bN),
						Metric213bD    = sum(Metric213bD),
						Metric214bN    = sum(Metric214bN),
						Metric214bD    = sum(Metric214bD),
						Metric215bN    = sum(Metric215bN),
						Metric215bD    = sum(Metric215bD),
						Metric216bN    = sum(Metric216bN),
						Metric216bD    = sum(Metric216bD),
						Metric218bN    = sum(Metric218bN),
						Metric218bD    = sum(Metric218bD),
						Metric219bN    = sum(Metric219bN),
						Metric219bD    = sum(Metric219bD),
						Metric220bN    = sum(Metric220bN),
						Metric220bD    = sum(Metric220bD),
						Metric221bN    = sum(Metric221bN),
						Metric221bD    = sum(Metric221bD),
						Metric222bN    = sum(Metric222bN),
						Metric222bD    = sum(Metric222bD),
						Metric223bN    = sum(Metric223bN),
						Metric223bD    = sum(Metric223bD),
						Metric224bN    = sum(Metric224bN),
						Metric224bD    = sum(Metric224bD),
						Metric225bN    = sum(Metric225bN),
						Metric225bD    = sum(Metric225bD),
						Metric302bN    = sum(Metric302bN),
						Metric302bD    = sum(Metric302bD),
						Metric303bN    = sum(Metric303bN),
						Metric303bD    = sum(Metric303bD),
						Metric304bN    = sum(Metric304bN),
						Metric304bD    = sum(Metric304bD),
						Metric305bN    = sum(Metric305bN),
						Metric305bD    = sum(Metric305bD),
						Metric306bN    = sum(Metric306bN),
						Metric306bD    = sum(Metric306bD),
						Metric307bN    = sum(Metric307bN),
						Metric307bD    = sum(Metric307bD),
						Metric308bN    = sum(Metric308bN),
						Metric308bD    = sum(Metric308bD),
						Metric309bN    = sum(Metric309bN),
						Metric309bD    = sum(Metric309bD),
						Metric310bN    = sum(Metric310bN),
						Metric310bD    = sum(Metric310bD),
						Metric410bN    = sum(Metric410bN),
						Metric410bD    = sum(Metric410bD),
						Metric411bN    = sum(Metric411bN),
						Metric411bD    = sum(Metric411bD),
						Metric500btN   = sum(Metric500btN),
						Metric500btD   = sum(Metric500btD),
						Metric500bkN   = sum(Metric500bkN),
						Metric500bkD   = sum(Metric500bkD),
						Metric501btN   = sum(Metric501btN),
						Metric501btD   = sum(Metric501btD),
						Metric501bkN   = sum(Metric501bkN),
						Metric501bkD   = sum(Metric501bkD)
					FROM
						(
							SELECT
								YearId         = YearId,
								RegionId       = @RegionIterator,
								RegionKey      = @RegionChildIterator,
								ContentId      = ContentId,
								ContentKey     = ContentKey,
								DemographicId  = DemographicId,
								DemographicKey = DemographicKey,
								Metric103b     = -sum(case when Generic > 0             then Generic - 1             else isnull(Generic,             0) end),
								Metric200bN    = -sum(case when NonIntroN > 0           then NonIntroN - 1           else isnull(NonIntroN,           0) end),
								Metric200bD    = -sum(case when NonIntroD > 0           then NonIntroD - 1           else isnull(NonIntroD,           0) end),
								Metric201bN    = -sum(case when SkillsBuilderN > 0      then SkillsBuilderN - 1      else isnull(SkillsBuilderN,      0) end),
								Metric201bD    = -sum(case when SkillsBuilderD > 0      then SkillsBuilderD - 1      else isnull(SkillsBuilderD,      0) end),
								Metric202bN    = -sum(case when ExiterN > 0             then ExiterN - 1             else isnull(ExiterN,             0) end),
								Metric202bD    = -sum(case when ExiterD > 0             then ExiterD - 1             else isnull(ExiterD,             0) end),
								Metric206bN    = -sum(case when FosterN > 0             then FosterN - 1             else isnull(FosterN,             0) end),
								Metric206bD    = -sum(case when FosterD > 0             then FosterD - 1             else isnull(FosterD,             0) end),
								Metric207bN    = -sum(case when VeteranN > 0            then VeteranN - 1            else isnull(VeteranN,            0) end),
								Metric207bD    = -sum(case when VeteranD > 0            then VeteranD - 1            else isnull(VeteranD,            0) end),
								Metric208bN    = -sum(case when DisabledN > 0           then DisabledN - 1           else isnull(DisabledN,           0) end),
								Metric208bD    = -sum(case when DisabledD > 0           then DisabledD - 1           else isnull(DisabledD,           0) end),
								Metric209bN    = -sum(case when FirstGenN > 0           then FirstGenN - 1           else isnull(FirstGenN,           0) end),
								Metric209bD    = -sum(case when FirstGenD > 0           then FirstGenD - 1           else isnull(FirstGenD,           0) end),
								Metric210bN    = -sum(case when FinAidN > 0             then FinAidN - 1             else isnull(FinAidN,             0) end),
								Metric210bD    = -sum(case when FinAidD > 0             then FinAidD - 1             else isnull(FinAidD,             0) end),
								Metric211bN    = -sum(case when EOPSN > 0               then EOPSN - 1               else isnull(EOPSN,               0) end),
								Metric211bD    = -sum(case when EOPSD > 0               then EOPSD - 1               else isnull(EOPSD,               0) end),
								Metric212bN    = -sum(case when MathEngBSN > 0          then MathEngBSN - 1          else isnull(MathEngBSN,          0) end),
								Metric212bD    = -sum(case when MathEngBSD > 0          then MathEngBSD - 1          else isnull(MathEngBSD,          0) end),
								Metric213bN    = -sum(case when ESLN > 0                then ESLN - 1                else isnull(ESLN,                0) end),
								Metric213bD    = -sum(case when ESLD > 0                then ESLD - 1                else isnull(ESLD,                0) end),
								Metric214bN    = -sum(case when FullTimeN > 0           then FullTimeN - 1           else isnull(FullTimeN,           0) end),
								Metric214bD    = -sum(case when FullTimeD > 0           then FullTimeD - 1           else isnull(FullTimeD,           0) end),
								Metric215bN    = -sum(case when PartTimeN > 0           then PartTimeN - 1           else isnull(PartTimeN,           0) end),
								Metric215bD    = -sum(case when PartTimeD > 0           then PartTimeD - 1           else isnull(PartTimeD,           0) end),
								Metric216bN    = -sum(case when FirstTimeN > 0          then FirstTimeN - 1          else isnull(FirstTimeN,          0) end),
								Metric216bD    = -sum(case when FirstTimeD > 0          then FirstTimeD - 1          else isnull(FirstTimeD,          0) end),
								Metric218bN    = -sum(case when MultiSchoolN > 0        then MultiSchoolN - 1        else isnull(MultiSchoolN,        0) end),
								Metric218bD    = -sum(case when MultiSchoolD > 0        then MultiSchoolD - 1        else isnull(MultiSchoolD,        0) end),
								Metric219bN    = -sum(case when PrevHEAwardN > 0        then PrevHEAwardN - 1        else isnull(PrevHEAwardN,        0) end),
								Metric219bD    = -sum(case when PrevHEAwardD > 0        then PrevHEAwardD - 1        else isnull(PrevHEAwardD,        0) end),
								Metric220bN    = -sum(case when NoPrevHEAwardN > 0      then NoPrevHEAwardN - 1      else isnull(NoPrevHEAwardN,      0) end),
								Metric220bD    = -sum(case when NoPrevHEAwardD > 0      then NoPrevHEAwardD - 1      else isnull(NoPrevHEAwardD,      0) end),
								Metric221bN    = -sum(case when PrevCCAwardN > 0        then PrevCCAwardN - 1        else isnull(PrevCCAwardN,        0) end),
								Metric221bD    = -sum(case when PrevCCAwardD > 0        then PrevCCAwardD - 1        else isnull(PrevCCAwardD,        0) end),
								Metric222bN    = -sum(case when PrevBachN > 0           then PrevBachN - 1           else isnull(PrevBachN,           0) end),
								Metric222bD    = -sum(case when PrevBachD > 0           then PrevBachD - 1           else isnull(PrevBachD,           0) end),
								Metric223bN    = -sum(case when PrevCTEN > 0            then PrevCTEN - 1            else isnull(PrevCTEN,            0) end),
								Metric223bD    = -sum(case when PrevCTED > 0            then PrevCTED - 1            else isnull(PrevCTED,            0) end),
								Metric224bN    = -sum(case when EconDisN > 0            then EconDisN - 1            else isnull(EconDisN,            0) end),
								Metric224bD    = -sum(case when EconDisD > 0            then EconDisD - 1            else isnull(EconDisD,            0) end),
								Metric225bN    = -sum(case when EconAdvN > 0            then EconAdvN - 1            else isnull(EconAdvN,            0) end),
								Metric225bD    = -sum(case when EconAdvD > 0            then EconAdvD - 1            else isnull(EconAdvD,            0) end),
								Metric302bN    = -sum(case when TermRetN > 0            then TermRetN - 1            else isnull(TermRetN,            0) end),
								Metric302bD    = -sum(case when TermRetD > 0            then TermRetD - 1            else isnull(TermRetD,            0) end),
								Metric303bN    = -sum(case when TermRegRetN > 0         then TermRegRetN - 1         else isnull(TermRegRetN,         0) end),
								Metric303bD    = -sum(case when TermRegRetD > 0         then TermRegRetD - 1         else isnull(TermRegRetD,         0) end),
								Metric304bN    = -sum(case when PersistN > 0            then PersistN - 1            else isnull(PersistN,            0) end),
								Metric304bD    = -sum(case when PersistD > 0            then PersistD - 1            else isnull(PersistD,            0) end),
								Metric305bN    = -sum(case when RegPersistN > 0         then RegPersistN - 1         else isnull(RegPersistN,         0) end),
								Metric305bD    = -sum(case when RegPersistD > 0         then RegPersistD - 1         else isnull(RegPersistD,         0) end),
								Metric306bN    = -sum(case when NCTransAnyToSelectN > 0 then NCTransAnyToSelectN - 1 else isnull(NCTransAnyToSelectN, 0) end),
								Metric306bD    = -sum(case when NCTransAnyToSelectD > 0 then NCTransAnyToSelectD - 1 else isnull(NCTransAnyToSelectD, 0) end),
								Metric307bN    = -sum(case when NCTransAnyToAnyN > 0    then NCTransAnyToAnyN - 1    else isnull(NCTransAnyToAnyN,    0) end),
								Metric307bD    = -sum(case when NCTransAnyToAnyD > 0    then NCTransAnyToAnyD - 1    else isnull(NCTransAnyToAnyD,    0) end),
								Metric308bN    = -sum(case when NCTransAnyToSelectN > 0 then NCTransAnyToSelectN - 1 else isnull(NCTransAnyToSelectN, 0) end),
								Metric308bD    = -sum(case when NCTransAnyToSelectD > 0 then NCTransAnyToSelectD - 1 else isnull(NCTransAnyToSelectD, 0) end),
								Metric309bN    = -sum(case when CTE12UnitsN > 0         then CTE12UnitsN - 1         else isnull(CTE12UnitsN,         0) end),
								Metric309bD    = -sum(case when CTE12UnitsD > 0         then CTE12UnitsD - 1         else isnull(CTE12UnitsD,         0) end),
								Metric310bN    = -sum(case when NC48HoursN > 0          then NC48HoursN - 1          else isnull(NC48HoursN,          0) end),
								Metric310bD    = -sum(case when NC48HoursD > 0          then NC48HoursD - 1          else isnull(NC48HoursD,          0) end),
								Metric410bN    = -sum(case when TransferN > 0           then TransferN - 1           else isnull(TransferN,           0) end),
								Metric410bD    = -sum(case when TransferD > 0           then TransferD - 1           else isnull(TransferD,           0) end),
								Metric411bN    = -sum(case when SBWageGainN > 0         then SBWageGainN - 1         else isnull(SBWageGainN,         0) end),
								Metric411bD    = -sum(case when SBWageGainD > 0         then SBWageGainD - 1         else isnull(SBWageGainD,         0) end),
								Metric500btN   = -sum(case when Employed2QtrsTranN > 0  then Employed2QtrsTranN - 1  else isnull(Employed2QtrsTranN,  0) end),
								Metric500btD   = -sum(case when Employed2QtrsTranD > 0  then Employed2QtrsTranD - 1  else isnull(Employed2QtrsTranD,  0) end),
								Metric500bkN   = -sum(case when Employed2QtrsSBN > 0    then Employed2QtrsSBN - 1    else isnull(Employed2QtrsSBN,    0) end),
								Metric500bkD   = -sum(case when Employed2QtrsSBD > 0    then Employed2QtrsSBD - 1    else isnull(Employed2QtrsSBD,    0) end),
								Metric501btN   = -sum(case when Employed4QtrsTranN > 0  then Employed4QtrsTranN - 1  else isnull(Employed4QtrsTranN,  0) end),
								Metric501btD   = -sum(case when Employed4QtrsTranD > 0  then Employed4QtrsTranD - 1  else isnull(Employed4QtrsTranD,  0) end),
								Metric501bkN   = -sum(case when Employed4QtrsSBN > 0    then Employed4QtrsSBN - 1    else isnull(Employed4QtrsSBN,    0) end),
								Metric501bkD   = -sum(case when Employed4QtrsSBD > 0    then Employed4QtrsSBD - 1    else isnull(Employed4QtrsSBD,    0) end)
							FROM
								StudentSwirlSummary
							GROUP BY
								YearId,
								ContentId,
								ContentKey,
								DemographicId,
								DemographicKey
							UNION
							SELECT
								YearId         = mo.YearId,
								RegionId       = rc.RegionId,
								RegionKey      = rc.RegionKey,
								ContentId      = mo.ContentId,
								ContentKey     = mo.ContentKey,
								DemographicId  = mo.DemographicId,
								DemographicKey = mo.DemographicKey,
								Metric103b     = sum(Metric103b),
								Metric200bN    = sum(Metric200bN),
								Metric200bD    = sum(Metric200bD),
								Metric201bN    = sum(Metric201bN),
								Metric201bD    = sum(Metric201bD),
								Metric202bN    = sum(Metric202bN),
								Metric202bD    = sum(Metric202bD),
								Metric206bN    = sum(Metric206bN),
								Metric206bD    = sum(Metric206bD),
								Metric207bN    = sum(Metric207bN),
								Metric207bD    = sum(Metric207bD),
								Metric208bN    = sum(Metric208bN),
								Metric208bD    = sum(Metric208bD),
								Metric209bN    = sum(Metric209bN),
								Metric209bD    = sum(Metric209bD),
								Metric210bN    = sum(Metric210bN),
								Metric210bD    = sum(Metric210bD),
								Metric211bN    = sum(Metric211bN),
								Metric211bD    = sum(Metric211bD),
								Metric212bN    = sum(Metric212bN),
								Metric212bD    = sum(Metric212bD),
								Metric213bN    = sum(Metric213bN),
								Metric213bD    = sum(Metric213bD),
								Metric214bN    = sum(Metric214bN),
								Metric214bD    = sum(Metric214bD),
								Metric215bN    = sum(Metric215bN),
								Metric215bD    = sum(Metric215bD),
								Metric216bN    = sum(Metric216bN),
								Metric216bD    = sum(Metric216bD),
								Metric218bN    = sum(Metric218bN),
								Metric218bD    = sum(Metric218bD),
								Metric219bN    = sum(Metric219bN),
								Metric219bD    = sum(Metric219bD),
								Metric220bN    = sum(Metric220bN),
								Metric220bD    = sum(Metric220bD),
								Metric221bN    = sum(Metric221bN),
								Metric221bD    = sum(Metric221bD),
								Metric222bN    = sum(Metric222bN),
								Metric222bD    = sum(Metric222bD),
								Metric223bN    = sum(Metric223bN),
								Metric223bD    = sum(Metric223bD),
								Metric224bN    = sum(Metric224bN),
								Metric224bD    = sum(Metric224bD),
								Metric225bN    = sum(Metric225bN),
								Metric225bD    = sum(Metric225bD),
								Metric302bN    = sum(Metric302bN),
								Metric302bD    = sum(Metric302bD),
								Metric303bN    = sum(Metric303bN),
								Metric303bD    = sum(Metric303bD),
								Metric304bN    = sum(Metric304bN),
								Metric304bD    = sum(Metric304bD),
								Metric305bN    = sum(Metric305bN),
								Metric305bD    = sum(Metric305bD),
								Metric306bN    = sum(Metric306bN),
								Metric306bD    = sum(Metric306bD),
								Metric307bN    = sum(Metric307bN),
								Metric307bD    = sum(Metric307bD),
								Metric308bN    = sum(Metric308bN),
								Metric308bD    = sum(Metric308bD),
								Metric309bN    = sum(Metric309bN),
								Metric309bD    = sum(Metric309bD),
								Metric310bN    = sum(Metric310bN),
								Metric310bD    = sum(Metric310bD),
								Metric410bN    = sum(Metric410bN),
								Metric410bD    = sum(Metric410bD),
								Metric411bN    = sum(Metric411bN),
								Metric411bD    = sum(Metric411bD),
								Metric500btN   = sum(Metric500btN),
								Metric500btD   = sum(Metric500btD),
								Metric500bkN   = sum(Metric500bkN),
								Metric500bkD   = sum(Metric500bkD),
								Metric501btN   = sum(Metric501btN),
								Metric501btD   = sum(Metric501btD),
								Metric501bkN   = sum(Metric501bkN),
								Metric501bkD   = sum(Metric501bkD)
							FROM
								MetricOutput mo
								inner join
								RegionCohort rc
									on rc.CollegeId = mo.RegionKey
							WHERE
								mo.YearId = @YearIterator
								and mo.RegionId = 4
							GROUP BY
								mo.YearId,
								rc.RegionId,
								rc.RegionKey,
								mo.ContentId,
								mo.ContentKey,
								mo.DemographicId,
								mo.DemographicKey
						) a
					GROUP BY
						YearId,
						RegionId,
						RegionKey,
						ContentId,
						ContentKey,
						DemographicId,
						DemographicKey;
			END;

			MERGE
				MetricOutput t
			USING
				MetricSwirlOutput s
			ON
				(
					s.YearId             = t.YearId
					and s.RegionId       = t.RegionId
					and s.RegionKey      = t.RegionKey
					and s.ContentId      = t.ContentId
					and s.ContentKey     = t.ContentKey
					and s.DemographicId  = t.DemographicId
					and s.DemographicKey = t.DemographicKey
				)
			WHEN MATCHED THEN
				UPDATE SET
					t.Metric103b   = s.Metric103b,
					t.Metric200bN  = s.Metric200bN,
					t.Metric200bD  = s.Metric200bD,
					t.Metric201bN  = s.Metric201bN,
					t.Metric201bD  = s.Metric201bD,
					t.Metric202bN  = s.Metric202bN,
					t.Metric202bD  = s.Metric202bD,
					t.Metric206bN  = s.Metric206bN,
					t.Metric206bD  = s.Metric206bD,
					t.Metric207bN  = s.Metric207bN,
					t.Metric207bD  = s.Metric207bD,
					t.Metric208bN  = s.Metric208bN,
					t.Metric208bD  = s.Metric208bD,
					t.Metric209bN  = s.Metric209bN,
					t.Metric209bD  = s.Metric209bD,
					t.Metric210bN  = s.Metric210bN,
					t.Metric210bD  = s.Metric210bD,
					t.Metric211bN  = s.Metric211bN,
					t.Metric211bD  = s.Metric211bD,
					t.Metric212bN  = s.Metric212bN,
					t.Metric212bD  = s.Metric212bD,
					t.Metric213bN  = s.Metric213bN,
					t.Metric213bD  = s.Metric213bD,
					t.Metric214bN  = s.Metric214bN,
					t.Metric214bD  = s.Metric214bD,
					t.Metric215bN  = s.Metric215bN,
					t.Metric215bD  = s.Metric215bD,
					t.Metric216bN  = s.Metric216bN,
					t.Metric216bD  = s.Metric216bD,
					t.Metric218bN  = s.Metric218bN,
					t.Metric218bD  = s.Metric218bD,
					t.Metric219bN  = s.Metric219bN,
					t.Metric219bD  = s.Metric219bD,
					t.Metric220bN  = s.Metric220bN,
					t.Metric220bD  = s.Metric220bD,
					t.Metric221bN  = s.Metric221bN,
					t.Metric221bD  = s.Metric221bD,
					t.Metric222bN  = s.Metric222bN,
					t.Metric222bD  = s.Metric222bD,
					t.Metric223bN  = s.Metric223bN,
					t.Metric223bD  = s.Metric223bD,
					t.Metric224bN  = s.Metric224bN,
					t.Metric224bD  = s.Metric224bD,
					t.Metric225bN  = s.Metric225bN,
					t.Metric225bD  = s.Metric225bD,
					t.Metric302bN  = s.Metric302bN,
					t.Metric302bD  = s.Metric302bD,
					t.Metric303bN  = s.Metric303bN,
					t.Metric303bD  = s.Metric303bD,
					t.Metric304bN  = s.Metric304bN,
					t.Metric304bD  = s.Metric304bD,
					t.Metric305bN  = s.Metric305bN,
					t.Metric305bD  = s.Metric305bD,
					t.Metric306bN  = s.Metric306bN,
					t.Metric306bD  = s.Metric306bD,
					t.Metric307bN  = s.Metric307bN,
					t.Metric307bD  = s.Metric307bD,
					t.Metric308bN  = s.Metric308bN,
					t.Metric308bD  = s.Metric308bD,
					t.Metric309bN  = s.Metric309bN,
					t.Metric309bD  = s.Metric309bD,
					t.Metric310bN  = s.Metric310bN,
					t.Metric310bD  = s.Metric310bD,
					t.Metric410bN  = s.Metric410bN,
					t.Metric410bD  = s.Metric410bD,
					t.Metric411bN  = s.Metric411bN,
					t.Metric411bD  = s.Metric411bD,
					t.Metric500btN = s.Metric500btN,
					t.Metric500btD = s.Metric500btD,
					t.Metric500bkN = s.Metric500bkN,
					t.Metric500bkD = s.Metric500bkD,
					t.Metric501btN = s.Metric501btN,
					t.Metric501btD = s.Metric501btD,
					t.Metric501bkN = s.Metric501bkN,
					t.Metric501bkD = s.Metric501bkD
			WHEN NOT MATCHED BY TARGET THEN
				INSERT
					(
						YearId,
						RegionId,
						RegionKey,
						ContentId,
						ContentKey,
						DemographicId,
						DemographicKey,
						Metric103b,
						Metric200bN,
						Metric200bD,
						Metric201bN,
						Metric201bD,
						Metric202bN,
						Metric202bD,
						Metric206bN,
						Metric206bD,
						Metric207bN,
						Metric207bD,
						Metric208bN,
						Metric208bD,
						Metric209bN,
						Metric209bD,
						Metric210bN,
						Metric210bD,
						Metric211bN,
						Metric211bD,
						Metric212bN,
						Metric212bD,
						Metric213bN,
						Metric213bD,
						Metric214bN,
						Metric214bD,
						Metric215bN,
						Metric215bD,
						Metric216bN,
						Metric216bD,
						Metric218bN,
						Metric218bD,
						Metric219bN,
						Metric219bD,
						Metric220bN,
						Metric220bD,
						Metric221bN,
						Metric221bD,
						Metric222bN,
						Metric222bD,
						Metric223bN,
						Metric223bD,
						Metric224bN,
						Metric224bD,
						Metric225bN,
						Metric225bD,
						Metric302bN,
						Metric302bD,
						Metric303bN,
						Metric303bD,
						Metric304bN,
						Metric304bD,
						Metric305bN,
						Metric305bD,
						Metric306bN,
						Metric306bD,
						Metric307bN,
						Metric307bD,
						Metric308bN,
						Metric308bD,
						Metric309bN,
						Metric309bD,
						Metric310bN,
						Metric310bD,
						Metric410bN,
						Metric410bD,
						Metric411bN,
						Metric411bD,
						Metric500btN,
						Metric500btD,
						Metric500bkN,
						Metric500bkD,
						Metric501btN,
						Metric501btD,
						Metric501bkN,
						Metric501bkD 
				)
			VALUES
				(
					s.YearId,
					s.RegionId,
					s.RegionKey,
					s.ContentId,
					s.ContentKey,
					s.DemographicId,
					s.DemographicKey,
					s.Metric103b,
					s.Metric200bN,
					s.Metric200bD,
					s.Metric201bN,
					s.Metric201bD,
					s.Metric202bN,
					s.Metric202bD,
					s.Metric206bN,
					s.Metric206bD,
					s.Metric207bN,
					s.Metric207bD,
					s.Metric208bN,
					s.Metric208bD,
					s.Metric209bN,
					s.Metric209bD,
					s.Metric210bN,
					s.Metric210bD,
					s.Metric211bN,
					s.Metric211bD,
					s.Metric212bN,
					s.Metric212bD,
					s.Metric213bN,
					s.Metric213bD,
					s.Metric214bN,
					s.Metric214bD,
					s.Metric215bN,
					s.Metric215bD,
					s.Metric216bN,
					s.Metric216bD,
					s.Metric218bN,
					s.Metric218bD,
					s.Metric219bN,
					s.Metric219bD,
					s.Metric220bN,
					s.Metric220bD,
					s.Metric221bN,
					s.Metric221bD,
					s.Metric222bN,
					s.Metric222bD,
					s.Metric223bN,
					s.Metric223bD,
					s.Metric224bN,
					s.Metric224bD,
					s.Metric225bN,
					s.Metric225bD,
					s.Metric302bN,
					s.Metric302bD,
					s.Metric303bN,
					s.Metric303bD,
					s.Metric304bN,
					s.Metric304bD,
					s.Metric305bN,
					s.Metric305bD,
					s.Metric306bN,
					s.Metric306bD,
					s.Metric307bN,
					s.Metric307bD,
					s.Metric308bN,
					s.Metric308bD,
					s.Metric309bN,
					s.Metric309bD,
					s.Metric310bN,
					s.Metric310bD,
					s.Metric410bN,
					s.Metric410bD,
					s.Metric411bN,
					s.Metric411bD,
					s.Metric500btN,
					s.Metric500btD,
					s.Metric500bkN,
					s.Metric500bkD,
					s.Metric501btN,
					s.Metric501btD,
					s.Metric501bkN,
					s.Metric501bkD
				);

			SET @RegionChildIterator += 1;
		END;
		
		SET @RegionChildIterator = 1
		SET @RegionIterator -= 1;
	END;

	SELECT @RegionIterator = max(RegionId) FROM dbo.Region;
	SET @YearIterator += 1;
END;