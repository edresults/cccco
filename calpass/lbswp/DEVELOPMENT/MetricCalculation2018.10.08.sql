SELECT
	YearId            = u.YearId,
	-- RegionId          = u.RegionIdentifier,
	-- RegionKey         = u.RegionKey,
	ContentId         = u.ContentIdentifier,
	ContentKey        = u.ContentKey,
	DemograhpicId     = u.DemographicIdentifier,
	DemographicKey    = u.DemographicKey,
	MetricNumerator   = count(distinct case when SkillsBuilder = 1 then StudentId end),
	MetricDenominator = count(distinct StudentId),
	MetricResult      = round(convert(decimal(5,2), 100.00 * count(distinct case when SkillsBuilder = 1 then StudentId end) / count(distinct StudentId)), 2)
FROM
	(
		SELECT
			YearId,
			StudentId,
			RegionStateId,
			RegionMacroId,
			RegionMicroId,
			RegionCollegeId,
			ContentId,
			VocationId,
			SectorId,
			SubdisciplineId,
			ProgramId,
			DemographicId,
			GenderId,
			EthnicityId,
			AgeId,
			SkillsBuilder
		FROM
			dbo.StudentMetrics
		WHERE
			YearId = 1
			and RegionCollegeId = 115
	) s
UNPIVOT
	(
		DemographicKey for DemographicIdentifier in (DemographicId, GenderId, EthnicityId, AgeId)
	) u
UNPIVOT
	(
		ContentKey for ContentIdentifier in (ContentId, VocationId, SectorId, ProgramId)
	) u
-- UNPIVOT
-- 	(
-- 		RegionKey for RegionIdentifier in (RegionStateId, RegionMacroId, RegionMicroId, RegionCollegeId)
-- 	) u
	inner join
	dbo.Demographic d
		on d.Identifier = u.DemographicIdentifier
	inner join
	dbo.Content c
		on c.Identifier = u.ContentIdentifier
	-- inner join
	-- dbo.Region r
	-- 	on r.Identifier = u.RegionIdentifier
GROUP BY
	u.YearId,
	-- u.RegionIdentifier,
	-- u.RegionKey,
	u.ContentIdentifier,
	u.ContentKey,
	u.DemographicIdentifier,
	u.DemographicKey;