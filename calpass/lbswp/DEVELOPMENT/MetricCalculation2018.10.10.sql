SET NOCOUNT ON;
SET ANSI_WARNINGS OFF;

DECLARE
	@Severity       tinyint       = 0,
	@State          tinyint       = 1,
	@Error          varchar(2048) = '',
	@YearCounter    tinyint       = 1,
	@YearMax        tinyint       = 0,
	@CollegeCounter tinyint       = 1,
	@CollegeMax     tinyint       = 0,
	@RegionCounter  tinyint       = 1,
	@RegionMax      tinyint       = 0;

SELECT
	@YearMax = max(YearId)
FROM
	dbo.Year;

SELECT
	@CollegeMax = max(RegionCollegeId)
FROM
	dbo.RegionCollege;

SELECT
	@RegionMax = max(RegionId)
FROM
	dbo.Region
WHERE
	Label <> 'College';

WHILE (@YearCounter <= @YearMax)
BEGIN

	SET @Error = 'Year: ' + convert(varchar, @YearCounter) + ' of ' + convert(varchar, @YearMax);
	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

	WHILE (@CollegeCounter <= @CollegeMax)
	BEGIN

		SET @Error = 'College: ' + convert(varchar, @CollegeCounter) + ' of ' + convert(varchar, @CollegeMax);
		RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

		MERGE
			dbo.MetricOutput t
		USING
			(
				SELECT
					YearId         = u.YearId,
					RegionId       = r.RegionId,
					RegionKey      = u.RegionKey,
					ContentId      = c.Contentid,
					ContentKey     = u.ContentKey,
					DemographicId  = d.DemographicId,
					DemographicKey = u.DemographicKey,
					Metric103b     = count(distinct StudentId),
					Metric200b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when NonIntro = 1      then StudentId end) / nullif(count(distinct StudentId), 0), 0.00)), 2),
					Metric201b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when SkillsBuilder = 1 then StudentId end) / nullif(count(distinct StudentId), 0), 0.00)), 2),
					Metric202b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when Exiter = 1        then StudentId end) / nullif(count(distinct StudentId), 0), 0.00)), 2),
					Metric224b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when EconDis = 1       then StudentId end) / nullif(count(distinct StudentId), 0), 0.00)), 2),
					Metric302b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when TermRet = 1       then StudentId end) / nullif(count(distinct case when TermRet    is not null then StudentId end), 0), 0)), 2),
					Metric303b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when TermRegRet = 1    then StudentId end) / nullif(count(distinct case when TermRegRet is not null then StudentId end), 0), 0.00)), 2),
					Metric304b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when Persist = 1       then StudentId end) / nullif(count(distinct case when Persist    is not null then StudentId end), 0), 0.00)), 2),
					Metric305b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when RegPersist = 1    then StudentId end) / nullif(count(distinct case when RegPersist is not null then StudentId end), 0), 0.00)), 2),
					Metric410b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when Transfer = 1      then StudentId end) / nullif(count(distinct StudentId), 0), 0.00)), 2),
					Metric411b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when SBWageGain = 1    then StudentId end) / nullif(count(distinct StudentId), 0), 0.00)), 2)
				FROM
					(
						SELECT
							YearId,
							StudentId,
							RegionCollegeId,
							ContentId,
							VocationId,
							SectorId,
							SubdisciplineId,
							ProgramId,
							DemographicId,
							GenderId,
							EthnicityId,
							AgeId,
							NonIntro,
							SkillsBuilder,
							Exiter,
							EconDis,
							TermRet,
							TermRegRet,
							Persist,
							RegPersist,
							Transfer,
							SBWageGain
						FROM
							dbo.StudentMetrics
						WHERE
							YearId = @YearCounter
							and RegionCollegeId = @CollegeCounter
					) s
				UNPIVOT
					(
						DemographicKey for DemographicIdentifier in (DemographicId, GenderId, EthnicityId, AgeId)
					) u
				UNPIVOT
					(
						ContentKey for ContentIdentifier in (ContentId, VocationId, SectorId, ProgramId)
					) u
				UNPIVOT
					(
						RegionKey for RegionIdentifier in (RegionCollegeId)
					) u
					inner join
					dbo.Demographic d
						on d.Identifier = u.DemographicIdentifier
					inner join
					dbo.Content c
						on c.Identifier = u.ContentIdentifier
					inner join
					dbo.Region r
						on r.Identifier = u.RegionIdentifier
				GROUP BY
					u.YearId,
					r.RegionId,
					u.RegionKey,
					c.ContentId,
					u.ContentKey,
					d.DemographicId,
					u.DemographicKey
			) s
		ON
			(
				s.YearId             = t.YearId
				and s.RegionId       = t.RegionId
				and s.RegionKey      = t.RegionKey
				and s.ContentId      = t.ContentId
				and s.ContentKey     = t.ContentKey
				and s.DemographicId  = t.DemographicId
				and s.DemographicKey = t.DemographicKey
			)
		WHEN MATCHED THEN
			UPDATE SET
				t.Metric103b = s.Metric103b,
				t.Metric200b = s.Metric200b,
				t.Metric201b = s.Metric201b,
				t.Metric202b = s.Metric202b,
				t.Metric224b = s.Metric224b,
				t.Metric302b = s.Metric302b,
				t.Metric303b = s.Metric303b,
				t.Metric304b = s.Metric304b,
				t.Metric305b = s.Metric305b,
				t.Metric410b = s.Metric410b,
				t.Metric411b = s.Metric411b
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					YearId,
					RegionId,
					RegionKey,
					ContentId,
					ContentKey,
					DemographicId,
					DemographicKey,
					Metric103b,
					Metric200b,
					Metric201b,
					Metric202b,
					Metric224b,
					Metric302b,
					Metric303b,
					Metric304b,
					Metric305b,
					Metric410b,
					Metric411b
			)
		VALUES
			(
				YearId,
				RegionId,
				RegionKey,
				ContentId,
				ContentKey,
				DemographicId,
				DemographicKey,
				Metric103b,
				Metric200b,
				Metric201b,
				Metric202b,
				Metric224b,
				Metric302b,
				Metric303b,
				Metric304b,
				Metric305b,
				Metric410b,
				Metric411b
			);

		SET @CollegeCounter += 1;
	END;

	SET @Error = 'Region: ' + convert(varchar, @RegionCounter) + ' of ' + convert(varchar, @RegionMax);
	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;
			
	MERGE
		dbo.MetricOutput t
	USING
		(
			SELECT
				YearId         = u.YearId,
				RegionId       = r.RegionId,
				RegionKey      = u.RegionKey,
				ContentId      = c.Contentid,
				ContentKey     = u.ContentKey,
				DemographicId  = d.DemographicId,
				DemographicKey = u.DemographicKey,
				Metric103b     = count(distinct StudentId),
				Metric200b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when NonIntro = 1      then StudentId end) / nullif(count(distinct StudentId), 0), 0.00)), 2),
				Metric201b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when SkillsBuilder = 1 then StudentId end) / nullif(count(distinct StudentId), 0), 0.00)), 2),
				Metric202b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when Exiter = 1        then StudentId end) / nullif(count(distinct StudentId), 0), 0.00)), 2),
				Metric224b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when EconDis = 1       then StudentId end) / nullif(count(distinct StudentId), 0), 0.00)), 2),
				Metric302b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when TermRet = 1       then StudentId end) / nullif(count(distinct case when TermRet    is not null then StudentId end), 0), 0)), 2),
				Metric303b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when TermRegRet = 1    then StudentId end) / nullif(count(distinct case when TermRegRet is not null then StudentId end), 0), 0.00)), 2),
				Metric304b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when Persist = 1       then StudentId end) / nullif(count(distinct case when Persist    is not null then StudentId end), 0), 0.00)), 2),
				Metric305b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when RegPersist = 1    then StudentId end) / nullif(count(distinct case when RegPersist is not null then StudentId end), 0), 0.00)), 2),
				Metric410b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when Transfer = 1      then StudentId end) / nullif(count(distinct StudentId), 0), 0.00)), 2),
				Metric411b     = round(convert(decimal(5,2), isnull(100.00 * count(distinct case when SBWageGain = 1    then StudentId end) / nullif(count(distinct StudentId), 0), 0.00)), 2)
			FROM
				(
					SELECT
						YearId,
						StudentId,
						RegionStateId,
						RegionMacroId,
						RegionMicroId,
						ContentId,
						VocationId,
						SectorId,
						SubdisciplineId,
						ProgramId,
						DemographicId,
						GenderId,
						EthnicityId,
						AgeId,
						NonIntro,
						SkillsBuilder,
						Exiter,
						EconDis,
						TermRet,
						TermRegRet,
						Persist,
						RegPersist,
						Transfer,
						SBWageGain
					FROM
						dbo.StudentMetrics
					WHERE
						YearId = @YearCounter
				) s
			UNPIVOT
				(
					DemographicKey for DemographicIdentifier in (DemographicId, GenderId, EthnicityId, AgeId)
				) u
			UNPIVOT
				(
					ContentKey for ContentIdentifier in (ContentId, VocationId, SectorId, ProgramId)
				) u
			UNPIVOT
				(
					RegionKey for RegionIdentifier in (RegionStateId, RegionMacroId, RegionMicroId)
				) u
				inner join
				dbo.Demographic d
					on d.Identifier = u.DemographicIdentifier
				inner join
				dbo.Content c
					on c.Identifier = u.ContentIdentifier
				inner join
				dbo.Region r
					on r.Identifier = u.RegionIdentifier
			GROUP BY
				u.YearId,
				r.RegionId,
				u.RegionKey,
				c.ContentId,
				u.ContentKey,
				d.DemographicId,
				u.DemographicKey
		) s
	ON
		(
			s.YearId             = t.YearId
			and s.RegionId       = t.RegionId
			and s.RegionKey      = t.RegionKey
			and s.ContentId      = t.ContentId
			and s.ContentKey     = t.ContentKey
			and s.DemographicId  = t.DemographicId
			and s.DemographicKey = t.DemographicKey
		)
	WHEN MATCHED THEN
		UPDATE SET
			t.Metric103b = s.Metric103b,
			t.Metric200b = s.Metric200b,
			t.Metric201b = s.Metric201b,
			t.Metric202b = s.Metric202b,
			t.Metric224b = s.Metric224b,
			t.Metric302b = s.Metric302b,
			t.Metric303b = s.Metric303b,
			t.Metric304b = s.Metric304b,
			t.Metric305b = s.Metric305b,
			t.Metric410b = s.Metric410b,
			t.Metric411b = s.Metric411b
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				YearId,
				RegionId,
				RegionKey,
				ContentId,
				ContentKey,
				DemographicId,
				DemographicKey,
				Metric103b,
				Metric200b,
				Metric201b,
				Metric202b,
				Metric224b,
				Metric302b,
				Metric303b,
				Metric304b,
				Metric305b,
				Metric410b,
				Metric411b
		)
	VALUES
		(
			YearId,
			RegionId,
			RegionKey,
			ContentId,
			ContentKey,
			DemographicId,
			DemographicKey,
			Metric103b,
			Metric200b,
			Metric201b,
			Metric202b,
			Metric224b,
			Metric302b,
			Metric303b,
			Metric304b,
			Metric305b,
			Metric410b,
			Metric411b
		);

		SET @RegionCounter += 1;
END;

SET @YearCounter += 1;
SET @CollegeCounter = 1;
SET @RegionCounter = 1;