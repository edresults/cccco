-- base
SELECT
	DemographicKey = mo.DemographicKey,
	Metric304bN    = sum(mo.Metric304bN),
	Metric304bD    = sum(mo.Metric304bD)
FROM
	MetricOutput mo
WHERE
	mo.YearId = 1
	and mo.RegionId = 4
	and mo.ContentId = 1
	and mo.ContentKey = 1
	and mo.DemographicId = 3
GROUP BY
	mo.DemographicKey
ORDER BY
	mo.DemographicKey;

-- duplicates
SELECT
	DemographicKey = DemographicKey,
	Metric304bN    = sum(isnull(case when Persist = 1 then Duplicates end, 0)),
	Metric304bD    = sum(isnull(case when Persist is not null then Duplicates end, 0))
FROM
	StudentSwirlSummary
WHERE
	YearId = 1
	and ContentId = 1
	and ContentKey = 1
	and DemographicId = 3
GROUP BY
	YearId,
	ContentId,
	ContentKey,
	DemographicId,
	DemographicKey
ORDER BY
	DemographicKey;

-- union
SELECT
	DemographicKey,
	Metric304bN    = sum(Metric304bN),
	Metric304bD    = sum(Metric304bD)
FROM
	(
		SELECT
			DemographicKey = mo.DemographicKey,
			Metric304bN    = sum(mo.Metric304bN),
			Metric304bD    = sum(mo.Metric304bD)
		FROM
			MetricOutput mo
		WHERE
			mo.YearId = 1
			and mo.RegionId = 4
			and mo.ContentId = 1
			and mo.ContentKey = 1
			and mo.DemographicId = 3
		GROUP BY
			mo.DemographicKey
		UNION
		SELECT
			DemographicKey = DemographicKey,
			Metric304bN    = sum(isnull(case when Persist = 1 then Duplicates end, 0)),
			Metric304bD    = sum(isnull(case when Persist is not null then Duplicates end, 0))
		FROM
			StudentSwirlSummary
		WHERE
			YearId = 1
			and ContentId = 1
			and ContentKey = 1
			and DemographicId = 3
		GROUP BY
			YearId,
			ContentId,
			ContentKey,
			DemographicId,
			DemographicKey
	) a
GROUP BY
	DemographicKey
ORDER BY
	DemographicKey;

select metric_value, metric_denom
from Launchboard4Top6.dbo.CCStudentEquityTableAgeOldScript
where metric_id = '304b'
    and macroregion_name = 'All'
    and cte_program = 'All'
	and academic_year = '2012'
order by academic_year, subgroup
;

-- What happens when a student reports multiple different demographic codes in different colleges? How does count distinct work?
-- A student has Two or More Races in one college and Hispanic in another college. Does the student count as 1 for each category?
SELECT
	YearId              = a.YearId,
	StudentId           = a.StudentId,
	ContentId           = a.ContentId,
	ContentKey          = a.ContentKey,
	DemographicId       = a.DemographicId,
	DemographicKey      = a.DemographicKey,
	NonIntroCntNo       = case when count(case when a.NonIntro = 1 then 1 end) > 1 then -1 else 0 end + count(case when a.NonIntro = 1 then 1 end),
	NonIntroCntYes      = case when count(a.NonIntro) > 1 then -1 else 0 end + count(a.NonIntro),
	SkillsBuilderCntNo  = case when count(case when a.SkillsBuilder = 1 then 1 end) > 1 then -1 else 0 end + count(case when a.SkillsBuilder = 1 then 1 end),
	SkillsBuilderCntYes = case when count(a.SkillsBuilder) > 1 then -1 else 0 end + count(a.SkillsBuilder),
	ExiterCntNo         = case when count(case when a.Exiter = 1 then 1 end) > 1 then -1 else 0 end + count(case when a.Exiter = 1 then 1 end),
	ExiterCntYes        = case when count(a.Exiter) > 1 then -1 else 0 end + count(a.Exiter),
	EconDisCntNo        = case when count(case when a.EconDis = 1 then 1 end) > 1 then -1 else 0 end + count(case when a.EconDis = 1 then 1 end),
	EconDisCntYes       = case when count(a.EconDis) > 1 then -1 else 0 end + count(a.EconDis),
	TermRetCntNo        = case when count(case when a.TermRet = 1 then 1 end) > 1 then -1 else 0 end + count(case when a.TermRet = 1 then 1 end),
	TermRetCntYes       = case when count(case when a.TermRet is not null then 1 end) > 1 then -1 else 0 end + count(case when a.TermRet is not null then 1 end),
	TermRegRetCntNo     = case when count(case when a.TermRegRet = 1 then 1 end) > 1 then -1 else 0 end + count(case when a.TermRegRet = 1 then 1 end),
	TermRegRetCntYes    = case when count(case when a.TermRegRet is not null then 1 end) > 1 then -1 else 0 end + count(case when a.TermRegRet is not null then 1 end),
	PersistCntNo        = case when count(case when a.Persist = 1 then 1 end) > 1 then -1 else 0 end + count(case when a.Persist = 1 then 1 end),
	PersistCntYes       = case when count(case when a.Persist is not null then 1 end) > 1 then -1 else 0 end + count(case when a.Persist is not null then 1 end),
	RegPersistCntNo     = case when count(case when a.RegPersist = 1 then 1 end) > 1 then -1 else 0 end + count(case when a.RegPersist = 1 then 1 end),
	RegPersistCntYes    = case when count(case when a.RegPersist is not null then 1 end) > 1 then -1 else 0 end + count(case when a.RegPersist is not null then 1 end),
	TransferCntNo       = case when count(case when a.Transfer = 1 then 1 end) > 1 then -1 else 0 end + count(case when a.Transfer = 1 then 1 end),
	TransferCntYes      = case when count(case when a.Transfer is not null then 1 end) > 1 then -1 else 0 end + count(case when a.Transfer is not null then 1 end),
	SBWageGainCntNo     = case when count(case when a.SBWageGain = 1 then 1 end) > 1 then -1 else 0 end + count(case when a.SBWageGain = 1 then 1 end),
	SBWageGainCntYes    = case when count(case when a.SBWageGain is not null then 1 end) > 1 then -1 else 0 end + count(case when a.SBWageGain is not null then 1 end)
FROM
	StudentSwirlData a
WHERE
	a.YearId = 1
	and a.StudentId = 3358939
GROUP BY
	a.YearId,
	a.StudentId,
	a.ContentId,
	a.ContentKey,
	a.DemographicId,
	a.DemographicKey
ORDER BY
	a.YearId,
	a.StudentId,
	a.ContentId,
	a.ContentKey,
	a.DemographicId,
	a.DemographicKey;