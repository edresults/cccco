DECLARE
	@CollegeId     int = 70,
	@ContentId     int = 4,
	@ContentKey    int = 61,
	@YearId        int = 4,
	@DemographicId int = 0;

SELECT
	*
FROM
	lbswp.base
WHERE
	CollegeId = @CollegeId
	and YearId = @YearId
	and ContentId = @ContentId
	and ContentKey = @ContentKey
	and DemographicId = @DemographicId;