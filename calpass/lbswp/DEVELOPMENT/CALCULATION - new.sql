SET NOCOUNT ON;
SET ANSI_WARNINGS OFF;

-- IF (object_id('MetricData') is not null)
-- 	BEGIN
-- 		DROP TABLE MetricData;
-- 	END;

-- GO

-- CREATE TABLE
-- 	dbo.MetricData
-- 	(
-- 		YearId         TINYINT  NOT NULL,
-- 		RegionKey      TINYINT  NOT NULL,
-- 		RegionVal      TINYINT  NOT NULL,
-- 		ContentKey     TINYINT  NOT NULL,
-- 		ContentVal     SMALLINT NOT NULL,
-- 		DemographicKey TINYINT  NOT NULL,
-- 		DemographicVal TINYINT  NOT NULL,
-- 		AttributeKey   TINYINT  NOT NULL,
-- 		AttributeVal   TINYINT  NOT NULL,
-- 		CP122N         INT      NOT NULL,
-- 		CP202N         INT      NOT NULL,
-- 		CP402N         INT      NOT NULL,
-- 		CP403N         INT      NOT NULL,
-- 		CP403D         INT      NOT NULL,
-- 		CP620N         INT      NOT NULL,
-- 		CP802N         INT      NOT NULL,
-- 		CP802D         INT      NOT NULL,
-- 		CONSTRAINT
-- 			PK_MetricData
-- 		PRIMARY KEY CLUSTERED
-- 			(
-- 				YearId,
-- 				RegionKey,
-- 				RegionVal,
-- 				ContentKey,
-- 				ContentVal,
-- 				DemographicKey,
-- 				DemographicVal,
-- 				AttributeKey,
-- 				AttributeVal
-- 			)
-- 	);

DECLARE
	@Severity            TINYINT       = 0,
	@State               TINYINT       = 1,
	@Message             VARCHAR(2048) = '',
	@YearIterator        TINYINT       = 0,
	@YearUBound          TINYINT       = 0,
	@RegionIterator      TINYINT       = 0,
	@RegionLBound        TINYINT       = 0,
	@RegionUBound        TINYINT       = 0,
	@RegionChildIterator TINYINT       = 0,
	@RegionChildUBound   TINYINT       = 0;

SELECT
	@YearUBound   = max(YearId)
FROM
	dbo.Year;

SELECT
	@RegionIterator = max(RegionId) - 1,
	@RegionLBound   = min(RegionId),
	@RegionUBound   = max(RegionId) - 1
FROM
	dbo.Region;

WHILE (@RegionIterator >= @RegionLBound)
BEGIN

	SET @Message = 'RegionIterator: ' + convert(varchar, @RegionIterator) + ' of ' + convert(varchar, @RegionUBound);
	RAISERROR(@Message, @Severity, @State) WITH NOWAIT;

	SELECT @YearIterator = min(YearID) FROM dbo.Year;

	WHILE (@YearIterator <= @YearUBound)
	BEGIN

		SET @Message = CHAR(9) + 'YearIterator: ' + convert(varchar, @YearIterator) + ' of ' + convert(varchar, @YearUBound);
		RAISERROR(@Message, @Severity, @State) WITH NOWAIT;

		SELECT
			@RegionChildIterator = 
				min(
					case
						when @RegionIterator = 5 then RegionIdCollege
						when @RegionIterator = 4 then RegionIdDistrict
						when @RegionIterator = 3 then RegionIdMicro
						when @RegionIterator = 2 then RegionIdMacro
						when @RegionIterator = 1 then RegionIdState
					end
				),
			@RegionChildUbound   = 
				max(
					case
						when @RegionIterator = 5 then RegionIdCollege
						when @RegionIterator = 4 then RegionIdDistrict
						when @RegionIterator = 3 then RegionIdMicro
						when @RegionIterator = 2 then RegionIdMacro
						when @RegionIterator = 1 then RegionIdState
					end
				)
		FROM
			dbo.RegionCollege;

		WHILE (@RegionChildIterator <= @RegionChildUBound)
		BEGIN

			SET @Message = CHAR(9) + CHAR(9) + 'RegionKey: ' + convert(varchar, @RegionChildIterator) + ' of ' + convert(varchar, @RegionChildUBound);
			RAISERROR(@Message, @Severity, @State) WITH NOWAIT;
		
			IF (@RegionIterator = 5)
				BEGIN
					INSERT INTO
						MetricData
						(
							YearId,
							RegionKey,
							RegionVal,
							ContentKey,
							ContentVal,
							DemographicKey,
							DemographicVal,
							AttributeKey,
							AttributeVal,
							CP122N,
							CP202N,
							CP402N,
							CP403N,
							CP403D,
							CP620N,
							CP802N,
							CP802D
						)
					SELECT
						YearId,
						RegionKey,
						RegionVal,
						ContentKey,
						ContentVal,
						DemographicKey,
						DemographicVal,
						AttributeKey,
						AttributeVal,
						CP122N = count(*),
						CP202N = count(case when Exiter = 1 then StudentId end),
						CP402N = count(case when CTE9UnitsDist = 1 then StudentId end),
						CP403N = count(case when CTENonCredit = 1 and ((AttributeVal = 1 and AttributeKey = 2) or AttributeKey = 1) then StudentId end),
						CP403D = count(case when ((AttributeVal = 1 and AttributeKey = 2) or AttributeKey = 1) then StudentId end),
						CP620N = count(case when Transfer = 1 then StudentId end),
						CP802N = count(case when LivingWage = 1 then StudentId end),
						CP802D = count(case when LivingWage is not null then StudentId end)
					FROM
						(
							SELECT DISTINCT
								YearId,
								RegionKey      = @RegionIterator,
								RegionVal      = u.RegionId,
								ContentKey     = c.ContentId,
								ContentVal     = u.ContentVal,
								DemographicKey = d.DemographicId,
								DemographicVal = u.DemographicVal,
								AttributeKey   = a.AttributeId,
								AttributeVal   = u.AttributeVal,
								StudentId,
								Exiter,
								CTE9UnitsDist,
								CTENonCredit,
								Transfer,
								LivingWage
							FROM
								(
									SELECT
										YearId,
										StudentId,
										-- <REGION>
										RegionId = 
											case
												when @RegionIterator = 5 then RegionIdCollege
												when @RegionIterator = 4 then RegionIdDistrict
												when @RegionIterator = 3 then RegionIdMicro
												when @RegionIterator = 2 then RegionIdMacro
												when @RegionIterator = 1 then RegionIdState
											end,
										-- </REGION>
										-- <CONTENT>
										ContentId,
										VocationId,
										SectorId,
										SubdisciplineId,
										ProgramId,
										-- </CONTENT>
										-- <DEMOGRAPHIC>
										DemographicId,
										GenderId = 
											case
												when @RegionIterator = 5 then GenderIdCollege
												when @RegionIterator = 4 then GenderIdDistrict
												when @RegionIterator = 3 then GenderIdMicro
												when @RegionIterator = 2 then GenderIdMacro
												when @RegionIterator = 1 then GenderIdState
											end,
										AgeId = 
											case
												when @RegionIterator = 5 then AgeIdCollege
												when @RegionIterator = 4 then AgeIdDistrict
												when @RegionIterator = 3 then AgeIdMicro
												when @RegionIterator = 2 then AgeIdMacro
												when @RegionIterator = 1 then AgeIdState
											end,
										EthnicityId = 
											case
												when @RegionIterator = 5 then EthnicityIdCollege
												when @RegionIterator = 4 then EthnicityIdDistrict
												when @RegionIterator = 3 then EthnicityIdMicro
												when @RegionIterator = 2 then EthnicityIdMacro
												when @RegionIterator = 1 then EthnicityIdState
											end,
										PovertyId,
										-- </DEMOGRAPHIC>
										-- <ATTRIBUTE>
										AttributeId,
										CreditId,
										-- </ATTRIBUTE>
										Exiter,
										CTE9UnitsDist,
										CTENonCredit,
										Transfer,
										LivingWage
									FROM
										dbo.StudentMetrics
									WHERE
										YearId = @YearIterator
										and case
											when @RegionIterator = 5 then RegionIdCollege
											when @RegionIterator = 4 then RegionIdDistrict
											when @RegionIterator = 3 then RegionIdMicro
											when @RegionIterator = 2 then RegionIdMacro
											when @RegionIterator = 1 then RegionIdState
										end = @RegionChildIterator
								) a
							UNPIVOT
								(
									-- DemographicVal for DemographicIdentifier in (DemographicId, GenderId, AgeId, EthnicityId, PovertyId)
									DemographicVal for DemographicIdentifier in (GenderId)
								) u
							UNPIVOT
								(
									ContentVal for ContentIdentifier in (ContentId, VocationId, SectorId, SubdisciplineId, ProgramId)
								) u
							UNPIVOT
								(
									-- AttributeVal for AttributeIdentifier in (AttributeId, CreditId)
									AttributeVal for AttributeIdentifier in (AttributeId)
								) u
								inner join
								Demographic d
									on d.Identifier = u.DemographicIdentifier
								inner join
								Content c
									on c.Identifier = u.ContentIdentifier
								inner join
								Attribute a
									on a.Identifier = u.AttributeIdentifier
						) z
					GROUP BY
						YearId,
						RegionKey,
						RegionVal,
						ContentKey,
						ContentVal,
						DemographicKey,
						DemographicVal,
						AttributeKey,
						AttributeVal;
				END;
			ELSE
				BEGIN
					INSERT INTO
						MetricData
						(
							YearId,
							RegionKey,
							RegionVal,
							ContentKey,
							ContentVal,
							DemographicKey,
							DemographicVal,
							AttributeKey,
							AttributeVal,
							CP122N,
							CP202N,
							CP402N,
							CP403N,
							CP403D,
							CP620N,
							CP802N,
							CP802D
						)
					SELECT
						YearId,
						RegionKey,
						RegionVal,
						ContentKey,
						ContentVal,
						DemographicKey,
						DemographicVal,
						AttributeKey,
						AttributeVal,
						CP122N = sum(CP122N),
						CP202N = sum(CP202N),
						CP402N = sum(CP402N),
						CP403N = sum(CP403N),
						CP403D = sum(CP403D),
						CP620N = sum(CP620N),
						CP802N = sum(CP802N),
						CP802D = sum(CP802D)
					FROM
						(
							SELECT
								YearId,
								RegionKey = @RegionIterator,
								RegionVal = @RegionChildIterator,
								ContentKey,
								ContentVal,
								DemographicKey,
								DemographicVal,
								AttributeKey,
								AttributeVal,
								CP122N,
								CP202N,
								CP402N,
								CP403N,
								CP403D,
								CP620N,
								CP802N,
								CP802D
							FROM
								dbo.MetricData
							WHERE
								YearId = @YearIterator
								and RegionKey = 5
								and RegionVal in (
									SELECT
										RegionIdCollege
									FROM
										dbo.RegionCollege
									WHERE
										case
											when @RegionIterator = 4 then RegionIdDistrict
											when @RegionIterator = 3 then RegionIdMicro
											when @RegionIterator = 2 then RegionIdMacro
											when @RegionIterator = 1 then RegionIdState
										end = @RegionChildIterator
								)
							UNION
							SELECT
								YearId,
								RegionKey,
								RegionVal,
								ContentKey,
								ContentVal,
								DemographicKey,
								DemographicVal,
								AttributeKey,
								AttributeVal,
								CP122N = 
									sum(
										case
											when DemographicKey = 2 and GenderDelta    = 1 then case when CP122N > 0 then 1 else 0 end
											when DemographicKey = 3 and AgeDelta       = 1 then case when CP122N > 0 then 1 else 0 end
											when DemographicKey = 4 and EthnicityDelta = 1 then case when CP122N > 0 then 1 else 0 end
											else case when CP122N > 0 then -(CP122N - 1) else CP122N end
										end
									),
								CP202N = 
									sum(
										case
											when DemographicKey = 2 and GenderDelta    = 1 then case when CP202N > 0 then 1 else 0 end
											when DemographicKey = 3 and AgeDelta       = 1 then case when CP202N > 0 then 1 else 0 end
											when DemographicKey = 4 and EthnicityDelta = 1 then case when CP202N > 0 then 1 else 0 end
											else case when CP202N > 0 then -(CP202N - 1) else CP202N end
										end
									),
								CP402N = 
									sum(
										case
											when DemographicKey = 2 and GenderDelta    = 1 then case when CP402N > 0 then 1 else 0 end
											when DemographicKey = 3 and AgeDelta       = 1 then case when CP402N > 0 then 1 else 0 end
											when DemographicKey = 4 and EthnicityDelta = 1 then case when CP402N > 0 then 1 else 0 end
											else case when CP402N > 0 then -(CP402N - 1) else CP402N end
										end
									),
								CP403N = 
									sum(
										case
											when DemographicKey = 2 and GenderDelta    = 1 then case when CP403N > 0 then 1 else 0 end
											when DemographicKey = 3 and AgeDelta       = 1 then case when CP403N > 0 then 1 else 0 end
											when DemographicKey = 4 and EthnicityDelta = 1 then case when CP403N > 0 then 1 else 0 end
											else case when CP403N > 0 then -(CP403N - 1) else CP403N end
										end
									),
								CP403D = 
									sum(
										case
											when DemographicKey = 2 and GenderDelta    = 1 then case when CP403D > 0 then 1 else 0 end
											when DemographicKey = 3 and AgeDelta       = 1 then case when CP403D > 0 then 1 else 0 end
											when DemographicKey = 4 and EthnicityDelta = 1 then case when CP403D > 0 then 1 else 0 end
											else case when CP403D > 0 then -(CP403D - 1) else CP403D end
										end
									),
								CP620N = 
									sum(
										case
											when DemographicKey = 2 and GenderDelta    = 1 then case when CP620N > 0 then 1 else 0 end
											when DemographicKey = 3 and AgeDelta       = 1 then case when CP620N > 0 then 1 else 0 end
											when DemographicKey = 4 and EthnicityDelta = 1 then case when CP620N > 0 then 1 else 0 end
											else case when CP620N > 0 then -(CP620N - 1) else CP620N end
										end
									),
								CP802N = 
									sum(
										case
											when DemographicKey = 2 and GenderDelta    = 1 then case when CP802N > 0 then 1 else 0 end
											when DemographicKey = 3 and AgeDelta       = 1 then case when CP802N > 0 then 1 else 0 end
											when DemographicKey = 4 and EthnicityDelta = 1 then case when CP802N > 0 then 1 else 0 end
											else case when CP802N > 0 then -(CP802N - 1) else CP802N end
										end
									),
								CP802D = 
									sum(
										case
											when DemographicKey = 2 and GenderDelta    = 1 then case when CP802D > 0 then 1 else 0 end
											when DemographicKey = 3 and AgeDelta       = 1 then case when CP802D > 0 then 1 else 0 end
											when DemographicKey = 4 and EthnicityDelta = 1 then case when CP802D > 0 then 1 else 0 end
											else case when CP802D > 0 then -(CP802D - 1) else CP802D end
										end
									)
							FROM
								(
									SELECT
										YearId,
										RegionKey = @RegionIterator,
										RegionVal = @RegionChildIterator,
										ContentKey,
										ContentVal,
										DemographicKey,
										DemographicVal,
										AttributeKey,
										AttributeVal,
										StudentId,
										GenderDelta,
										AgeDelta,
										EthnicityDelta,
										CP122N = count(*),
										CP202N = count(case when Exiter = 1 then StudentId end),
										CP402N = count(case when CTE9UnitsDist = 1 then StudentId end),
										CP403N = count(case when CTENonCredit = 1 and ((AttributeVal = 1 and AttributeKey = 2) or AttributeKey = 1) then StudentId end),
										CP403D = count(case when ((AttributeVal = 1 and AttributeKey = 2) or AttributeKey = 1) then StudentId end),
										CP620N = count(case when Transfer = 1 then StudentId end),
										CP802N = count(case when LivingWage = 1 then StudentId end),
										CP802D = count(case when LivingWage is not null then StudentId end)
									FROM
										(
											SELECT DISTINCT
												YearId,
												RegionKey      = 5,
												RegionVal      = u.RegionId,
												ContentKey     = c.ContentId,
												ContentVal     = u.ContentVal,
												DemographicKey = d.DemographicId,
												DemographicVal = u.DemographicVal,
												AttributeKey   = a.AttributeId,
												AttributeVal   = u.AttributeVal,
												GenderDelta,
												AgeDelta,
												EthnicityDelta,
												StudentId,
												Exiter,
												CTE9UnitsDist,
												CTENonCredit,
												Transfer,
												LivingWage
											FROM
												(
													SELECT
														YearId,
														sm.StudentId,
														-- -- <REGION>
														RegionId = RegionIdCollege,
														-- -- </REGION>
														-- <CONTENT>
														ContentId,
														VocationId,
														SectorId,
														SubdisciplineId,
														ProgramId,
														-- </CONTENT>
														-- <DEMOGRAPHIC>
														DemographicId,
														GenderId = 
															case
																when @RegionIterator = 4 then GenderIdDistrict
																when @RegionIterator = 3 then GenderIdMicro
																when @RegionIterator = 2 then GenderIdMacro
																when @RegionIterator = 1 then GenderIdState
															end,
														GenderDelta = 
															case
																when 
																	case
																		when @RegionIterator = 4 then GenderIdDistrict
																		when @RegionIterator = 3 then GenderIdMicro
																		when @RegionIterator = 2 then GenderIdMacro
																		when @RegionIterator = 1 then GenderIdState
																	end <> GenderIdCollege then 1
																else 0
															end,
														AgeId = 
															case
																when @RegionIterator = 4 then AgeIdDistrict
																when @RegionIterator = 3 then AgeIdMicro
																when @RegionIterator = 2 then AgeIdMacro
																when @RegionIterator = 1 then AgeIdState
															end,
														AgeDelta = 
															case
																when 
																	case
																		when @RegionIterator = 4 then AgeIdDistrict
																		when @RegionIterator = 3 then AgeIdMicro
																		when @RegionIterator = 2 then AgeIdMacro
																		when @RegionIterator = 1 then AgeIdState
																	end <> AgeIdCollege then 1
																else 0
															end,
														EthnicityId = 
															case
																when @RegionIterator = 4 then EthnicityIdDistrict
																when @RegionIterator = 3 then EthnicityIdMicro
																when @RegionIterator = 2 then EthnicityIdMacro
																when @RegionIterator = 1 then EthnicityIdState
															end,
														EthnicityDelta = 
															case
																when 
																	case
																		when @RegionIterator = 4 then EthnicityIdDistrict
																		when @RegionIterator = 3 then EthnicityIdMicro
																		when @RegionIterator = 2 then EthnicityIdMacro
																		when @RegionIterator = 1 then EthnicityIdState
																	end <> EthnicityIdCollege then 1
																else 0
															end,
														PovertyId,
														-- </DEMOGRAPHIC>
														-- <ATTRIBUTE>
														AttributeId,
														CreditId,
														-- </ATTRIBUTE>
														Exiter,
														CTE9UnitsDist,
														CTENonCredit,
														Transfer,
														LivingWage
													FROM
														dbo.StudentMetrics sm
													WHERE
														YearId = @YearIterator
														and sm.RegionIdCollege in (
															SELECT
																RegionIdCollege
															FROM
																dbo.RegionCollege
															WHERE
																case
																	when @RegionIterator = 4 then RegionIdDistrict
																	when @RegionIterator = 3 then RegionIdMicro
																	when @RegionIterator = 2 then RegionIdMacro
																	when @RegionIterator = 1 then RegionIdState
																end = @RegionChildIterator
														)
														and exists (
															SELECT
																NULL
															FROM
																dbo.StudentMetrics sm1
															WHERE
																sm1.YearId = sm.YearId
																and sm1.StudentId = sm.StudentId
																and case
																	when @RegionIterator = 4 then sm.RegionIdDistrict
																	when @RegionIterator = 3 then sm.RegionIdMicro
																	when @RegionIterator = 2 then sm.RegionIdMacro
																	when @RegionIterator = 1 then sm.RegionIdState
																end = case
																	when @RegionIterator = 4 then sm1.RegionIdDistrict
																	when @RegionIterator = 3 then sm1.RegionIdMicro
																	when @RegionIterator = 2 then sm1.RegionIdMacro
																	when @RegionIterator = 1 then sm1.RegionIdState
																end
																and sm1.ProgramId = sm.ProgramId
																and sm1.CreditId = sm.CreditId
															HAVING
																count(distinct sm1.RegionIdCollege) > 1
														)
												) a
											UNPIVOT
												(
													-- DemographicVal for DemographicIdentifier in (DemographicId, GenderId, AgeId, EthnicityId, PovertyId)
													DemographicVal for DemographicIdentifier in (GenderId)
												) u
											UNPIVOT
												(
													ContentVal for ContentIdentifier in (/*ContentId, VocationId, SectorId, SubdisciplineId, */ProgramId)
												) u
											UNPIVOT
												(
													-- AttributeVal for AttributeIdentifier in (AttributeId, CreditId)
													AttributeVal for AttributeIdentifier in (AttributeId)
												) u
												inner join
												Demographic d
													on d.Identifier = u.DemographicIdentifier
												inner join
												Content c
													on c.Identifier = u.ContentIdentifier
												inner join
												Attribute a
													on a.Identifier = u.AttributeIdentifier
										) z
									GROUP BY
										YearId,
										ContentKey,
										ContentVal,
										DemographicKey,
										DemographicVal,
										AttributeKey,
										AttributeVal,
										StudentId,
										GenderDelta,
										AgeDelta,
										EthnicityDelta
								) a
							GROUP BY
								YearId,
								RegionKey,
								RegionVal,
								ContentKey,
								ContentVal,
								DemographicKey,
								DemographicVal,
								AttributeKey,
								AttributeVal
						) a
					GROUP BY
						YearId,
						RegionKey,
						RegionVal,
						ContentKey,
						ContentVal,
						DemographicKey,
						DemographicVal,
						AttributeKey,
						AttributeVal;
				END;

			SET @RegionChildIterator += 1;
		END;

		SET @YearIterator += 1;
	END;

	SET @RegionIterator -= 1;
END;