SET NOCOUNT ON;
SET ANSI_WARNINGS OFF;

DECLARE
	@Severity            TINYINT       = 0,
	@State               TINYINT       = 1,
	@Message             VARCHAR(2048) = '',
	@YearIterator        TINYINT       = 0,
	@YearUBound          TINYINT       = 0,
	@RegionIterator      TINYINT       = 0,
	@RegionLBound        TINYINT       = 0,
	@RegionUBound        TINYINT       = 0,
	@RegionChildIterator TINYINT       = 0,
	@RegionChildUBound   TINYINT       = 0;

SELECT
	@YearIterator = min(YearId),
	@YearUBound   = max(YearId)
FROM
	dbo.Year;

SELECT
	@RegionIterator = max(RegionId),
	@RegionLBound   = min(RegionId),
	@RegionUBound   = max(RegionId)
FROM
	dbo.Region;

WHILE (@YearIterator <= @YearUBound)
BEGIN

	SET @Message = 'Year: ' + convert(varchar, @YearIterator) + ' of ' + convert(varchar, @YearUBound);
	RAISERROR(@Message, @Severity, @State) WITH NOWAIT;
	SELECT @RegionIterator = max(RegionId) FROM dbo.Region;

	WHILE (@RegionIterator >= @RegionLBound)
	BEGIN
		
		SET @Message = '  ' + 'Region: ' + convert(varchar, @RegionIterator) + ' of ' + convert(varchar, @RegionUBound);
		RAISERROR(@Message, @Severity, @State) WITH NOWAIT;
		SELECT
			@RegionChildIterator = min(coalesce(rs.RegionStateId, ra.RegionMacroId, ri.RegionMicroId, rc.RegionCollegeId)),
			@RegionChildUBound   = max(coalesce(rs.RegionStateId, ra.RegionMacroId, ri.RegionMicroId, rc.RegionCollegeId))
		FROM
			dbo.Region r
			left outer join
			dbo.RegionState rs
				on rs.RegionId = r.RegionId
			left outer join
			dbo.RegionMacro ra
				on ra.RegionId = r.RegionId
			left outer join
			dbo.RegionMicro ri
				on ri.RegionId = r.RegionId
			left outer join
			dbo.RegionCollege rc
				on rc.RegionId = r.RegionId
		WHERE
			r.RegionId = @RegionIterator;
			

		WHILE (@RegionChildIterator <= @RegionChildUBound)
		BEGIN

			SELECT
				@Message = '    ' + r.Label + ': ' + convert(varchar, @RegionChildIterator) + ' of ' + convert(varchar, @RegionChildUBound)
			FROM
				dbo.Region r
				left outer join
				dbo.RegionState rs
					on rs.RegionId = r.RegionId
					and rs.RegionStateId = @RegionChildIterator
				left outer join
				dbo.RegionMacro ra
					on ra.RegionId = r.RegionId
					and ra.RegionMacroId = @RegionChildIterator
				left outer join
				dbo.RegionMicro ri
					on ri.RegionId = r.RegionId
					and ri.RegionMicroId = @RegionChildIterator
				left outer join
				dbo.RegionCollege rc
					on rc.RegionId = r.RegionId
					and rc.RegionCollegeId = @RegionChildIterator
			WHERE
				r.RegionId = @RegionIterator;
			RAISERROR(@Message, @Severity, @State) WITH NOWAIT;

			TRUNCATE TABLE StudentSwirl;
			TRUNCATE TABLE StudentSwirlData;
			TRUNCATE TABLE StudentSwirlSummary;
			TRUNCATE TABLE MetricSwirlOutput;
			TRUNCATE TABLE RegionCohort;

			IF (@RegionIterator = 4)
				BEGIN
					INSERT INTO
						dbo.MetricSwirlOutput
					SELECT
						YearId,
						RegionId,
						RegionKey,
						ContentId,
						ContentKey,
						DemographicId,
						DemographicKey,
						Metric103b  = count(StudentId),
						Metric200bN = count(case when NonIntro = 1 then StudentId end),
						Metric200bD = count(StudentId),
						Metric201bN = count(case when SkillsBuilder = 1 then StudentId end),
						Metric201bD = count(StudentId),
						Metric202bN = count(case when Exiter = 1 then StudentId end),
						Metric202bD = count(StudentId),
						Metric224bN = count(case when EconDis = 1 then StudentId end),
						Metric224bD = count(StudentId),
						Metric302bN = count(case when TermRet = 1 then StudentId end),
						Metric302bD = count(case when TermRet is not null then StudentId end),
						Metric303bN = count(case when TermRegRet = 1 then StudentId end),
						Metric303bD = count(case when TermRegRet is not null then StudentId end),
						Metric304bN = count(case when Persist = 1 then StudentId end),
						Metric304bD = count(case when Persist is not null then StudentId end),
						Metric305bN = count(case when RegPersist = 1 then StudentId end),
						Metric305bD = count(case when RegPersist is not null then StudentId end),
						Metric410bN = count(case when Transfer = 1 then StudentId end),
						Metric410bD = count(StudentId),
						Metric411bN = count(case when SBWageGain = 1 then StudentId end),
						Metric411bD = count(case when SBWageGain is not null then StudentId end)
					FROM
						(
							SELECT
								YearId         = YearId,
								StudentId      = StudentId,
								RegionId       = RegionId,
								RegionKey      = RegionKey,
								ContentId      = ContentId,
								ContentKey     = ContentKey,
								DemographicId  = DemographicId,
								DemographicKey = DemographicKey,
								NonIntro       = max(convert(tinyint, NonIntro)),
								SkillsBuilder  = max(convert(tinyint, SkillsBuilder)),
								Exiter         = max(convert(tinyint, Exiter)),
								EconDis        = max(convert(tinyint, EconDis)),
								TermRet        = max(convert(tinyint, TermRet)),
								TermRegRet     = max(convert(tinyint, TermRegRet)),
								Persist        = max(convert(tinyint, Persist)),
								RegPersist     = max(convert(tinyint, RegPersist)),
								Transfer       = max(convert(tinyint, Transfer)),
								SBWageGain     = max(convert(tinyint, SBWageGain))
							FROM
								(
									SELECT
										YearId,
										RegionCollegeId,
										StudentId,
										ContentId,
										VocationId,
										SectorId,
										SubdisciplineId,
										ProgramId,
										DemographicId,
										GenderId,
										AgeId,
										EthnicityId,
										PovertyId,
										NonIntro,
										SkillsBuilder,
										Exiter,
										EconDis,
										TermRet,
										TermRegRet,
										Persist,
										RegPersist,
										Transfer,
										SBWageGain
									FROM
										dbo.StudentMetrics
									WHERE
										YearId = @YearIterator
										and RegionCollegeId = @RegionChildIterator
								) a
							UNPIVOT
								(
									DemographicKey for DemographicIdentifier in (DemographicId, GenderId, AgeId, EthnicityId, PovertyId)
								) u
							UNPIVOT
								(
									ContentKey for ContentIdentifier in (ContentId, VocationId, SectorId, SubdisciplineId, ProgramId)
								) u
							UNPIVOT
								(
									RegionKey for RegionIdentifier in (RegionCollegeId)
								) u
								inner join
								Demographic d
									on d.Identifier = u.DemographicIdentifier
								inner join
								Content c
									on c.Identifier = u.ContentIdentifier
								inner join
								Region r
									on r.Identifier = u.RegionIdentifier
							GROUP BY
								YearId,
								StudentId,
								RegionId,
								RegionKey,
								ContentId,
								ContentKey,
								DemographicId,
								DemographicKey
						) a
					GROUP BY
						YearId,
						RegionId,
						RegionKey,
						ContentId,
						ContentKey,
						DemographicId,
						DemographicKey;
				END;
			ELSE
				BEGIN
					INSERT INTO
						RegionCohort
						(
							RegionId,
							RegionKey,
							CollegeId
						)
					SELECT
						RegionId  = r.RegionId,
						RegionKey = coalesce(rs.RegionStateId, ra.RegionMacroId, ri.RegionMicroId, rc.RegionCollegeId),
						CollegeId = coalesce(rc1.RegionCollegeId, rc2.RegionCollegeId, rc3.RegionCollegeId, rc.RegionCollegeId)
					FROM
						Region r
						left outer join
						RegionState rs
							on rs.RegionId = r.RegionId
						left outer join
						RegionMacro ra
							on ra.RegionId = r.RegionId
						left outer join
						RegionMicro ri
							on ri.RegionId = r.RegionId
						left outer join
						RegionCollege rc
							on rc.RegionId = r.RegionId
						left outer join
						RegionCollege rc1
							on rc1.RegionMicroId = ri.RegionMicroId
						left outer join
						RegionCollege rc2
							on rc2.RegionMacroId = ra.RegionMacroId
						left outer join
						RegionCollege rc3
							on rc3.RegionStateId = rs.RegionStateId
					WHERE
						r.RegionId = @RegionIterator
						and coalesce(rs.RegionStateId, ra.RegionMacroId, ri.RegionMicroId, rc.RegionCollegeId) = @RegionChildIterator;

					INSERT INTO
						StudentSwirl
					SELECT DISTINCT
						sm.StudentId
					FROM
						StudentMetrics sm
						inner join
						RegionCohort rc
							on rc.CollegeId = sm.RegionCollegeId
					WHERE
						sm.YearId = @YearIterator
					GROUP BY
						sm.StudentId
					HAVING
						count(distinct sm.RegionCollegeId) > 1;

					INSERT INTO
						StudentSwirlData
					SELECT
						YearId         = YearId,
						StudentId      = StudentId,
						RegionId       = RegionId,
						RegionKey      = RegionKey,
						ContentId      = ContentId,
						ContentKey     = ContentKey,
						DemographicId  = DemographicId,
						DemographicKey = DemographicKey,
						NonIntro       = max(convert(tinyint, NonIntro)),
						SkillsBuilder  = max(convert(tinyint, SkillsBuilder)),
						Exiter         = max(convert(tinyint, Exiter)),
						EconDis        = max(convert(tinyint, EconDis)),
						TermRet        = max(convert(tinyint, TermRet)),
						TermRegRet     = max(convert(tinyint, TermRegRet)),
						Persist        = max(convert(tinyint, Persist)),
						RegPersist     = max(convert(tinyint, RegPersist)),
						Transfer       = max(convert(tinyint, Transfer)),
						SBWageGain     = max(convert(tinyint, SBWageGain))
					FROM
						(
							SELECT
								sm.YearId,
								sm.RegionCollegeId,
								sm.StudentId,
								sm.ContentId,
								sm.VocationId,
								sm.SectorId,
								sm.SubdisciplineId,
								sm.ProgramId,
								sm.DemographicId,
								sm.GenderId,
								sm.AgeId,
								sm.EthnicityId,
								sm.PovertyId,
								sm.NonIntro,
								sm.SkillsBuilder,
								sm.Exiter,
								sm.EconDis,
								sm.TermRet,
								sm.TermRegRet,
								sm.Persist,
								sm.RegPersist,
								sm.Transfer,
								sm.SBWageGain
							FROM
								StudentSwirl sc
								inner join
								StudentMetrics sm
									on sc.StudentId = sm.StudentId
								inner join
								RegionCohort rc
									on rc.CollegeId = sm.RegionCollegeId
							WHERE
								sm.YearId = @YearIterator
						) a
					UNPIVOT
						(
							DemographicKey for DemographicIdentifier in (DemographicId, GenderId, AgeId, EthnicityId, PovertyId)
						) u
					UNPIVOT
						(
							ContentKey for ContentIdentifier in (ContentId, VocationId, SectorId, SubdisciplineId, ProgramId)
						) u
					UNPIVOT
						(
							RegionKey for RegionIdentifier in (RegionCollegeId)
						) u
						inner join
						Demographic d
							on d.Identifier = u.DemographicIdentifier
						inner join
						Content c
							on c.Identifier = u.ContentIdentifier
						inner join
						Region r
							on r.Identifier = u.RegionIdentifier
					GROUP BY
						YearId,
						StudentId,
						RegionId,
						RegionKey,
						ContentId,
						ContentKey,
						DemographicId,
						DemographicKey;

					INSERT
						StudentSwirlSummary
					SELECT
						YearId           = a.YearId,
						StudentId        = a.StudentId,
						ContentId        = a.ContentId,
						ContentKey       = a.ContentKey,
						DemographicId    = a.DemographicId,
						DemographicKey   = a.DemographicKey,
						NonIntroN        = count(case when a.NonIntro = 1 then 1 end),
						NonIntroD        = count(a.NonIntro),
						SkillsBuilderN   = count(case when a.SkillsBuilder = 1 then 1 end),
						SkillsBuilderD   = count(a.SkillsBuilder),
						ExiterN          = count(case when a.Exiter = 1 then 1 end),
						ExiterD          = count(a.Exiter),
						EconDisN         = count(case when a.EconDis = 1 then 1 end),
						EconDisD         = count(a.EconDis),
						TermRetN         = count(case when a.TermRet = 1 then 1 end),
						TermRetD         = count(a.TermRet),
						TermRegRetN      = count(case when a.TermRegRet = 1 then 1 end),
						TermRegRetD      = count(a.TermRegRet),
						PersistN         = count(case when a.Persist = 1 then 1 end),
						PersistD         = count(a.Persist),
						RegPersistN      = count(case when a.RegPersist = 1 then 1 end),
						RegPersistD      = count(a.RegPersist),
						TransferN        = count(case when a.Transfer = 1 then 1 end),
						TransferD        = count(a.Transfer),
						SBWageGainN      = count(case when a.SBWageGain = 1 then 1 end),
						SBWageGainD      = count(a.SBWageGain),
						Generic          = count(*)
					FROM
						StudentSwirlData a
					GROUP BY
						a.YearId,
						a.StudentId,
						a.ContentId,
						a.ContentKey,
						a.DemographicId,
						a.DemographicKey
					ORDER BY
						a.YearId,
						a.StudentId,
						a.ContentId,
						a.ContentKey,
						a.DemographicId,
						a.DemographicKey;

					INSERT
						MetricSwirlOutput
					SELECT
						YearId         = YearId,
						RegionId       = RegionId,
						RegionKey      = RegionKey,
						ContentId      = ContentId,
						ContentKey     = ContentKey,
						DemographicId  = DemographicId,
						DemographicKey = DemographicKey,
						Metric103b     = sum(Metric103b),
						Metric200bN    = sum(Metric200bN),
						Metric200bD    = sum(Metric200bD),
						Metric201bN    = sum(Metric201bN),
						Metric201bD    = sum(Metric201bD),
						Metric202bN    = sum(Metric202bN),
						Metric202bD    = sum(Metric202bD),
						Metric224bN    = sum(Metric224bN),
						Metric224bD    = sum(Metric224bD),
						Metric302bN    = sum(Metric302bN),
						Metric302bD    = sum(Metric302bD),
						Metric303bN    = sum(Metric303bN),
						Metric303bD    = sum(Metric303bD),
						Metric304bN    = sum(Metric304bN),
						Metric304bD    = sum(Metric304bD),
						Metric305bN    = sum(Metric305bN),
						Metric305bD    = sum(Metric305bD),
						Metric410bN    = sum(Metric410bN),
						Metric410bD    = sum(Metric410bD),
						Metric411bN    = sum(Metric411bN),
						Metric411bD    = sum(Metric411bD)
					FROM
						(
							SELECT
								YearId         = YearId,
								RegionId       = @RegionIterator,
								RegionKey      = @RegionChildIterator,
								ContentId      = ContentId,
								ContentKey     = ContentKey,
								DemographicId  = DemographicId,
								DemographicKey = DemographicKey,
								Metric103b     = -sum(case when Generic > 0 then Generic - 1 else isnull(Generic, 0) end),
								Metric200bN    = -sum(case when NonIntroN > 0 then NonIntroN - 1 else isnull(NonIntroN, 0) end),
								Metric200bD    = -sum(case when NonIntroD > 0 then NonIntroD - 1 else isnull(NonIntroD, 0) end),
								Metric201bN    = -sum(case when SkillsBuilderN > 0 then SkillsBuilderN - 1 else isnull(SkillsBuilderN, 0) end),
								Metric201bD    = -sum(case when SkillsBuilderD > 0 then SkillsBuilderD - 1 else isnull(SkillsBuilderD, 0) end),
								Metric202bN    = -sum(case when ExiterN > 0 then ExiterN - 1 else isnull(ExiterN, 0) end),
								Metric202bD    = -sum(case when ExiterD > 0 then ExiterD - 1 else isnull(ExiterD, 0) end),
								Metric224bN    = -sum(case when EconDisN > 0 then EconDisN - 1 else isnull(EconDisN, 0) end),
								Metric224bD    = -sum(case when EconDisD > 0 then EconDisD - 1 else isnull(EconDisD, 0) end),
								Metric302bN    = -sum(case when TermRetN > 0 then TermRetN - 1 else isnull(TermRetN, 0) end),
								Metric302bD    = -sum(case when TermRetD > 0 then TermRetD - 1 else isnull(TermRetD, 0) end),
								Metric303bN    = -sum(case when TermRegRetN > 0 then TermRegRetN - 1 else isnull(TermRegRetN, 0) end),
								Metric303bD    = -sum(case when TermRegRetD > 0 then TermRegRetD - 1 else isnull(TermRegRetD, 0) end),
								Metric304bN    = -sum(case when PersistN > 0 then PersistN - 1 else isnull(PersistN, 0) end),
								Metric304bD    = -sum(case when PersistD > 0 then PersistD - 1 else isnull(PersistD, 0) end),
								Metric305bN    = -sum(case when RegPersistN > 0 then RegPersistN - 1 else isnull(RegPersistN, 0) end),
								Metric305bD    = -sum(case when RegPersistD > 0 then RegPersistD - 1 else isnull(RegPersistD, 0) end),
								Metric410bN    = -sum(case when TransferN > 0 then TransferN - 1 else isnull(TransferN, 0) end),
								Metric410bD    = -sum(case when TransferD > 0 then TransferD - 1 else isnull(TransferD, 0) end),
								Metric411bN    = -sum(case when SBWageGainN > 0 then SBWageGainN - 1 else isnull(SBWageGainN, 0) end),
								Metric411bD    = -sum(case when SBWageGainD > 0 then SBWageGainD - 1 else isnull(SBWageGainD, 0) end)
							FROM
								StudentSwirlSummary
							GROUP BY
								YearId,
								ContentId,
								ContentKey,
								DemographicId,
								DemographicKey
							UNION
							SELECT
								YearId         = mo.YearId,
								RegionId       = rc.RegionId,
								RegionKey      = rc.RegionKey,
								ContentId      = mo.ContentId,
								ContentKey     = mo.ContentKey,
								DemographicId  = mo.DemographicId,
								DemographicKey = mo.DemographicKey,
								Metric103b     = sum(mo.Metric103b),
								Metric200bN    = sum(mo.Metric200bN),
								Metric200bD    = sum(mo.Metric200bD),
								Metric201bN    = sum(mo.Metric201bN),
								Metric201bD    = sum(mo.Metric201bD),
								Metric202bN    = sum(mo.Metric202bN),
								Metric202bD    = sum(mo.Metric202bD),
								Metric224bN    = sum(mo.Metric224bN),
								Metric224bD    = sum(mo.Metric224bD),
								Metric302bN    = sum(mo.Metric302bN),
								Metric302bD    = sum(mo.Metric302bD),
								Metric303bN    = sum(mo.Metric303bN),
								Metric303bD    = sum(mo.Metric303bD),
								Metric304bN    = sum(mo.Metric304bN),
								Metric304bD    = sum(mo.Metric304bD),
								Metric305bN    = sum(mo.Metric305bN),
								Metric305bD    = sum(mo.Metric305bD),
								Metric410bN    = sum(mo.Metric410bN),
								Metric410bD    = sum(mo.Metric410bD),
								Metric411bN    = sum(mo.Metric411bN),
								Metric411bD    = sum(mo.Metric411bD)
							FROM
								MetricOutput mo
								inner join
								RegionCohort rc
									on rc.CollegeId = mo.RegionKey
							WHERE
								mo.YearId = @YearIterator
								and mo.RegionId = 4
							GROUP BY
								mo.YearId,
								rc.RegionId,
								rc.RegionKey,
								mo.ContentId,
								mo.ContentKey,
								mo.DemographicId,
								mo.DemographicKey
						) a
					GROUP BY
						YearId,
						RegionId,
						RegionKey,
						ContentId,
						ContentKey,
						DemographicId,
						DemographicKey;
			END;

			MERGE
				MetricOutput t
			USING
				MetricSwirlOutput s
			ON
				(
					s.YearId             = t.YearId
					and s.RegionId       = t.RegionId
					and s.RegionKey      = t.RegionKey
					and s.ContentId      = t.ContentId
					and s.ContentKey     = t.ContentKey
					and s.DemographicId  = t.DemographicId
					and s.DemographicKey = t.DemographicKey
				)
			WHEN MATCHED THEN
				UPDATE SET
					t.Metric103b  = s.Metric103b,
					t.Metric200bN = s.Metric200bN,
					t.Metric200bD = s.Metric200bD,
					t.Metric201bN = s.Metric201bN,
					t.Metric201bD = s.Metric201bD,
					t.Metric202bN = s.Metric202bN,
					t.Metric202bD = s.Metric202bD,
					t.Metric224bN = s.Metric224bN,
					t.Metric224bD = s.Metric224bD,
					t.Metric302bN = s.Metric302bN,
					t.Metric302bD = s.Metric302bD,
					t.Metric303bN = s.Metric303bN,
					t.Metric303bD = s.Metric303bD,
					t.Metric304bN = s.Metric304bN,
					t.Metric304bD = s.Metric304bD,
					t.Metric305bN = s.Metric305bN,
					t.Metric305bD = s.Metric305bD,
					t.Metric410bN = s.Metric410bN,
					t.Metric410bD = s.Metric410bD,
					t.Metric411bN = s.Metric411bN,
					t.Metric411bD = s.Metric411bD
			WHEN NOT MATCHED BY TARGET THEN
				INSERT
					(
						YearId,
						RegionId,
						RegionKey,
						ContentId,
						ContentKey,
						DemographicId,
						DemographicKey,
						Metric103b,
						Metric200bN,
						Metric200bD,
						Metric201bN,
						Metric201bD,
						Metric202bN,
						Metric202bD,
						Metric224bN,
						Metric224bD,
						Metric302bN,
						Metric302bD,
						Metric303bN,
						Metric303bD,
						Metric304bN,
						Metric304bD,
						Metric305bN,
						Metric305bD,
						Metric410bN,
						Metric410bD,
						Metric411bN,
						Metric411bD
				)
			VALUES
				(
					s.YearId,
					s.RegionId,
					s.RegionKey,
					s.ContentId,
					s.ContentKey,
					s.DemographicId,
					s.DemographicKey,
					s.Metric103b,
					s.Metric200bN,
					s.Metric200bD,
					s.Metric201bN,
					s.Metric201bD,
					s.Metric202bN,
					s.Metric202bD,
					s.Metric224bN,
					s.Metric224bD,
					s.Metric302bN,
					s.Metric302bD,
					s.Metric303bN,
					s.Metric303bD,
					s.Metric304bN,
					s.Metric304bD,
					s.Metric305bN,
					s.Metric305bD,
					s.Metric410bN,
					s.Metric410bD,
					s.Metric411bN,
					s.Metric411bD
				);

			SET @RegionChildIterator += 1;
		END;
		
		SET @RegionChildIterator = 1
		SET @RegionIterator -= 1;
	END;

	SELECT @RegionIterator = max(RegionId) FROM dbo.Region;
	SET @YearIterator += 1;
END;