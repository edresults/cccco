USE calpass;

GO

IF (object_id('lbswp.AcademicHistory', 'V') is not null)
	BEGIN
		DROP VIEW lbswp.AcademicHistory;
	END;

GO

CREATE VIEW
	lbswp.AcademicHistory
AS
	(
		SELECT
			CollegeId         = i.CollegeId,
			StudentId         = sx.student_id,
			YearCode          = y.YearId,
			ProgramId         = p.ProgramId,
			SubdisciplineId   = p.SubdisciplineId,
			SectorId          = p.SectorId,
			IsVocational      = p.IsVocational,

			-- Section Dimension: Credit and NonCredit
			SectionsRegistered                   = convert(tinyint, sum(1)),
			SectionsEnrolled                     = convert(tinyint, sum(case when                    m.IsEnroll   = 1 then 1 else 0 end)),
			SectionsCompleted                    = convert(tinyint, sum(case when                    m.IsComplete = 1 then 1 else 0 end)),
			SectionsPassed                       = convert(tinyint, sum(case when                    m.IsSuccess  = 1 then 1 else 0 end)),

			-- Section Dimension: Credit
			SectionsCreditRegistered             = convert(tinyint, sum(case when c.IsCredit = 1                      then 1 else 0 end)),
			SectionsCreditEnrolled               = convert(tinyint, sum(case when c.IsCredit = 1 and m.IsEnroll   = 1 then 1 else 0 end)),
			SectionsCreditCompleted              = convert(tinyint, sum(case when c.IsCredit = 1 and m.IsComplete = 1 then 1 else 0 end)),
			SectionsCreditPassed                 = convert(tinyint, sum(case when c.IsCredit = 1 and m.IsSuccess  = 1 then 1 else 0 end)),

			-- Section Dimension: Credit, Introductory
			SectionsCreditIntroductoryRegistered = convert(tinyint, sum(case when c.IsCredit = 1 and o.IsIntroductory = 1                      then 1 else 0 end)),
			SectionsCreditIntroductoryEnrolled   = convert(tinyint, sum(case when c.IsCredit = 1 and o.IsIntroductory = 1 and m.IsEnroll   = 1 then 1 else 0 end)),
			SectionsCreditIntroductoryCompleted  = convert(tinyint, sum(case when c.IsCredit = 1 and o.IsIntroductory = 1 and m.IsComplete = 1 then 1 else 0 end)),
			SectionsCreditIntroductoryPassed     = convert(tinyint, sum(case when c.IsCredit = 1 and o.IsIntroductory = 1 and m.IsSuccess  = 1 then 1 else 0 end)),

			-- Section Dimension: NonCredit
			SectionsCreditRegistered             = convert(tinyint, sum(case when c.IsCredit = 0                      then 1 else 0 end)),
			SectionsCreditEnrolled               = convert(tinyint, sum(case when c.IsCredit = 0 and m.IsEnroll   = 1 then 1 else 0 end)),
			SectionsCreditCompleted              = convert(tinyint, sum(case when c.IsCredit = 0 and m.IsComplete = 1 then 1 else 0 end)),
			SectionsCreditPassed                 = convert(tinyint, sum(case when c.IsCredit = 0 and m.IsSuccess  = 1 then 1 else 0 end)),

			-- Section Dimension: NonCredit, Introductory
			SectionsCreditIntroductoryRegistered = convert(tinyint, sum(case when c.IsCredit = 0 and o.IsIntroductory = 1                      then 1 else 0 end)),
			SectionsCreditIntroductoryEnrolled   = convert(tinyint, sum(case when c.IsCredit = 0 and o.IsIntroductory = 1 and m.IsEnroll   = 1 then 1 else 0 end)),
			SectionsCreditIntroductoryCompleted  = convert(tinyint, sum(case when c.IsCredit = 0 and o.IsIntroductory = 1 and m.IsComplete = 1 then 1 else 0 end)),
			SectionsCreditIntroductoryPassed     = convert(tinyint, sum(case when c.IsCredit = 0 and o.IsIntroductory = 1 and m.IsSuccess  = 1 then 1 else 0 end)),

			-- Hours Dimension: Credit and NonCredit
			HoursRegistered             = convert(decimal(5,1), sum(case when sx.attend_hours not in (8888.8, 9999.9)                                         then sx.attend_hours else 0 end)),
			HoursEnrolled               = convert(decimal(5,1), sum(case when sx.attend_hours not in (8888.8, 9999.9) and m.IsEnroll   = 1                    then sx.attend_hours else 0 end)),
			HoursCompleted              = convert(decimal(5,1), sum(case when sx.attend_hours not in (8888.8, 9999.9) and m.IsComplete = 1                    then sx.attend_hours else 0 end)),
			HoursPassed                 = convert(decimal(5,1), sum(case when sx.attend_hours not in (8888.8, 9999.9) and m.IsSuccess  = 1                    then sx.attend_hours else 0 end)),
			-- Hours Dimension: Credit
			HoursRegisteredCredit       = convert(decimal(5,1), sum(case when c.IsCredit = 1 and sx.attend_hours not in (8888.8, 9999.9)                      then sx.attend_hours else 0 end)),
			HoursEnrolledCredit         = convert(decimal(5,1), sum(case when c.IsCredit = 1 and sx.attend_hours not in (8888.8, 9999.9) and m.IsEnroll   = 1 then sx.attend_hours else 0 end)),
			HoursCompletedCredit        = convert(decimal(5,1), sum(case when c.IsCredit = 1 and sx.attend_hours not in (8888.8, 9999.9) and m.IsComplete = 1 then sx.attend_hours else 0 end)),
			HoursPassedCredit           = convert(decimal(5,1), sum(case when c.IsCredit = 1 and sx.attend_hours not in (8888.8, 9999.9) and m.IsSuccess  = 1 then sx.attend_hours else 0 end)),
			-- Hours Dimension: NonCredit
			HoursRegisteredNonCredit    = convert(decimal(5,1), sum(case when c.IsCredit = 0 and sx.attend_hours not in (8888.8, 9999.9)                      then sx.attend_hours else 0 end)),
			HoursEnrolledNonCredit      = convert(decimal(5,1), sum(case when c.IsCredit = 0 and sx.attend_hours not in (8888.8, 9999.9) and m.IsEnroll   = 1 then sx.attend_hours else 0 end)),
			HoursCompletedNonCredit     = convert(decimal(5,1), sum(case when c.IsCredit = 0 and sx.attend_hours not in (8888.8, 9999.9) and m.IsComplete = 1 then sx.attend_hours else 0 end)),
			HoursPassedNonCredit        = convert(decimal(5,1), sum(case when c.IsCredit = 0 and sx.attend_hours not in (8888.8, 9999.9) and m.IsSuccess  = 1 then sx.attend_hours else 0 end)),

			-- Units Dimension: Credit and NonCredit
			UnitsRegistered             = convert(decimal(4,2), sum(case when sx.units_attempted not in (88.88, 99.99)                                         then sx.units_attempted else 0 end)),
			UnitsEnrolled               = convert(decimal(4,2), sum(case when sx.units_attempted not in (88.88, 99.99) and m.IsEnroll   = 1                    then sx.units_attempted else 0 end)),
			UnitsCompleted              = convert(decimal(4,2), sum(case when sx.units_attempted not in (88.88, 99.99) and m.IsComplete = 1                    then sx.units_attempted else 0 end)),
			UnitsPassed                 = convert(decimal(4,2), sum(case when sx.units_attempted not in (88.88, 99.99) and m.IsSuccess  = 1                    then sx.units_attempted else 0 end)),
			-- Units Dimension: Credit
			UnitsRegisteredCredit       = convert(decimal(4,2), sum(case when c.IsCredit = 1 and sx.units_attempted not in (88.88, 99.99)                      then sx.units_attempted else 0 end)),
			UnitsEnrolledCredit         = convert(decimal(4,2), sum(case when c.IsCredit = 1 and sx.units_attempted not in (88.88, 99.99) and m.IsEnroll   = 1 then sx.units_attempted else 0 end)),
			UnitsCompletedCredit        = convert(decimal(4,2), sum(case when c.IsCredit = 1 and sx.units_attempted not in (88.88, 99.99) and m.IsComplete = 1 then sx.units_attempted else 0 end)),
			UnitsPassedCredit           = convert(decimal(4,2), sum(case when c.IsCredit = 1 and sx.units_attempted not in (88.88, 99.99) and m.IsSuccess  = 1 then sx.units_attempted else 0 end)),
			-- Units Dimension: NonCredit
			UnitsRegisteredNonCredit    = convert(decimal(4,2), sum(case when c.IsCredit = 0 and sx.units_attempted not in (88.88, 99.99)                      then sx.units_attempted else 0 end)),
			UnitsEnrolledNonCredit      = convert(decimal(4,2), sum(case when c.IsCredit = 0 and sx.units_attempted not in (88.88, 99.99) and m.IsEnroll   = 1 then sx.units_attempted else 0 end)),
			UnitsCompletedNonCredit     = convert(decimal(4,2), sum(case when c.IsCredit = 0 and sx.units_attempted not in (88.88, 99.99) and m.IsComplete = 1 then sx.units_attempted else 0 end)),
			UnitsPassedNonCredit        = convert(decimal(4,2), sum(case when c.IsCredit = 0 and sx.units_attempted not in (88.88, 99.99) and m.IsSuccess  = 1 then sx.units_attempted else 0 end))
		FROM
			comis.sxenrlm sx
			inner join
			comis.cbcrsinv cb
				on  cb.college_id     = sx.college_id
				and cb.course_id      = sx.course_id
				and cb.control_number = sx.control_number
				and cb.term_id        = sx.term_id
			inner join
			comis.Term t
				on  t.TermCode = sx.term_id
			inner join
			lbswp.Year y
				on y.YearCode = t.YearCode
			inner join
			lbswp.Mark m
				on  m.MarkCode = sx.Grade
			inner join
			lbswp.Program p
				on  p.ProgramCode = cb.top_code
			inner join
			lbswp.Occupation o
				on  o.OccupationCode = cb.sam_code
			inner join
			lbswp.Credit c
				on  c.CreditCode = cb.credit_status
			inner join
			lbswp.College i
				on i.CollegeCode = sx.college_id
		GROUP BY
			i.CollegeId,
			sx.student_id,
			y.YearId,
			p.ProgramId,
			p.SectorId,
			p.SubdisciplineId,
			p.IsVocational
	);