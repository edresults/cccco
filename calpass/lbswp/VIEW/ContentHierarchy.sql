USE calpass;

GO

IF (object_id('lbswp.ContentHierarchy', 'V') is not null)
	BEGIN
		DROP VIEW lbswp.ContentHierarchy;
	END;

GO

CREATE VIEW
	lbswp.ContentHierarchy
AS
	SELECT
		ContentId    = c.ContentId,
		ContentKey   = coalesce(v.VocationId, s.SectorId, d.SubdisciplineId, p.ProgramId, c.ContentId),
		ContentValue = coalesce(v.Label, s.Label, d.Label, p.Label, c.Label)
	FROM
		lbswp.Content c
		left outer join
		lbswp.Vocation v
			on v.ContentId = c.ContentId
		left outer join
		lbswp.Sector s
			on s.ContentId = c.ContentId
		left outer join
		lbswp.Subdiscipline d
			on d.ContentId = c.ContentId
		left outer join
		lbswp.Program p
			on p.ContentId = c.ContentId