USE calpass;

GO

IF (object_id('lbswp.RegionHierarchy', 'V') is not null)
	BEGIN
		DROP VIEW lbswp.RegionHierarchy;
	END;

GO

CREATE VIEW
	lbswp.RegionHierarchy
AS
	SELECT
		RegionId    = r.RegionId,
		RegionKey   = coalesce(rs.RegionStateId, rma.RegionMacroId, rmi.RegionMicroId, rc.RegionCollegeId),
		RegionValue = coalesce(rs.Label, rma.Label, rmi.Label, rc.Label)
	FROM
		lbswp.Region r
		left outer join
		lbswp.RegionState rs
			on rs.RegionId = r.RegionId
		left outer join
		lbswp.RegionMacro rma
			on rma.RegionId = r.RegionId
		left outer join
		lbswp.RegionMicro rmi
			on rmi.RegionId = r.RegionId
		left outer join
		lbswp.RegionCollege rc
			on rc.RegionId = r.RegionId;