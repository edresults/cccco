USE calpass;

GO

IF (object_id('lbswp.DemographicHierarchy', 'V') is not null)
	BEGIN
		DROP VIEW lbswp.DemographicHierarchy;
	END;

GO

CREATE VIEW
	lbswp.DemographicHierarchy
AS
	SELECT
		DemographicId    = d.DemographicId,
		DemographicKey   = coalesce(g.GenderId, a.AgeId, r.RaceId, d.DemographicId),
		DemographicValue = coalesce(g.Label, a.Label, r.Label, d.Label)
	FROM
		lbswp.Demographic d
		left outer join
		lbswp.Gender g
			on g.DemographicId = d.DemographicId
		left outer join
		lbswp.Age a
			on a.DemographicId = d.DemographicId
		left outer join
		lbswp.Race r
			on r.DemographicId = d.DemographicId;