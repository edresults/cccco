USE calpass;

GO

IF (object_id('lbswp.GoldenRecordProcess') is not null)
	BEGIN
		DROP PROCEDURE lbswp.GoldenRecordProcess;
	END;

GO

CREATE PROCEDURE
	lbswp.GoldenRecordProcess
	(
		@CollegeCode char(3) = null
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	SET ANSI_WARNINGS OFF;

DECLARE
	@Message     nvarchar(2048),
	@Time        datetime,
	@Seconds     int,
	@Severity    tinyint = 0,
	@State       tinyint = 1,
	@Error       varchar(2048),
	@Iterator    int = 0,
	@Iterations  int,
	@College     comis.College;

BEGIN

	TRUNCATE TABLE lbswp.GoldenRecord;

	INSERT
		@College
		(
			CollegeCode
		)
	SELECT
		CollegeCode
	FROM
		comis.College
	WHERE
		(
			@CollegeCode is null
			or
			CollegeCode = @CollegeCode
		)
	ORDER BY
		CollegeCode;

	SELECT
		@Iterations = count(*)
	FROM
		@College;

	BEGIN

		WHILE (@Iterator < @Iterations)
		BEGIN

			SET @Error = convert(varchar, @Iterator + 1) + ' of ' + convert(varchar, @Iterations);

			RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

			SET @Time = getdate();
			SELECT
				@CollegeCode = CollegeCode
			FROM
				@College
			WHERE
				Iterator = @Iterator;

			INSERT
				lbswp.GoldenRecord
				(
					CollegeCode,
					StudentId,
					YearCode,
					ProgramCode,
					VocationId,
					SectorId,
					SubdisciplineId,
					ProgramId,
					RaceId,
					AgeId,
					GenderId,
					IsWorkforce,
					IsFoster,
					IsVeteran,
					IsFirstGeneration,
					IsEops,
					IsDisability,
					IsDisadvantaged,
					IsFinancialAid,
					IsCredit,
					IsNonCredit,
					EnrolledSections,
					NonIntroUnits,
					IsVocationalSuccess,
					IsAwardProgram,
					IsAwardProgramRigorous
				)
			SELECT
				CollegeCode,
				StudentId,
				YearCode,
				ProgramCode,
				VocationId,
				SectorId,
				SubdisciplineId,
				ProgramId,
				RaceId,
				AgeId,
				GenderId,
				IsWorkforce,
				IsFoster,
				IsVeteran,
				IsFirstGeneration,
				IsEops,
				IsDisability,
				IsDisadvantaged,
				IsFinancialAid,
				IsCredit,
				IsNonCredit,
				EnrolledSections,
				NonIntroUnits,
				IsVocationalSuccess,
				IsAwardProgram,
				IsAwardProgramRigorous
			FROM
				lbswp.GoldenRecordGet(@CollegeCode);

			-- get time elapsed
			SET @Seconds = datediff(second, @Time, getdate());
			-- set msg
			SET @Error = 'ITERATION: ' + 
				convert(nvarchar, @Seconds / 86400) + ':' +
				convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
			-- output to user
			RAISERROR(@Error, 0, 1) WITH NOWAIT;

			SET @Iterator += 1;
		END;
	END;

	UPDATE
		gr
	SET
		gr.IsCCEnrolledYearCodeNext = 1
	FROM
		lbswp.GoldenRecord gr
		inner join
		lbswp.Year y
			on y.YearCode = gr.YearCode
	WHERE
		exists (
			SELECT
				1
			FROM
				comis.studntid id1
				inner join
				comis.studntid id2
					on id2.ssn = id1.ssn
				inner join
				comis.stterm st
					on st.college_id = id2.college_id
					and st.student_id = id2.student_id
				inner join
				comis.Term t
					on t.TermCode = st.term_id
			WHERE
				id1.college_id = gr.CollegeCode
				and id1.student_id = gr.StudentId
				and t.YearCode = y.YearCodeNext
		);

	UPDATE
		gr
	SET
		gr.IsUNEnrolledYearCodeCurr = 1
	FROM
		lbswp.GoldenRecord gr
		inner join
		comis.studntid id
			on id.college_id = gr.CollegeCode
			and id.student_id = gr.StudentId
		inner join
		comis.Transfer tr
			on id.ssn = tr.StudentSsn
		inner join
		comis.Term t
			on t.TermCode = gr.YearCode
			and t.BeginDate <= tr.TransferDate
			and t.EndDate >= tr.TransferDate
	WHERE
		tr.InstitutionType = '4';

	UPDATE
		gr
	SET
		gr.IsUNEnrolledYearCodeNext = 1
	FROM
		lbswp.GoldenRecord gr
		inner join
		comis.studntid id
			on id.college_id = gr.CollegeCode
			and id.student_id = gr.StudentId
		inner join
		comis.Transfer tr
			on id.ssn = tr.StudentSsn
		inner join
		comis.Term t
			on t.TermCode = gr.YearCode
		inner join
		comis.Term t1
			on t1.TermCode = t.YearCodeNext
			and t1.BeginDate <= tr.TransferDate
			and t1.EndDate >= tr.TransferDate
	WHERE
		tr.InstitutionType = '4';

	UPDATE
		t
	SET
		t.IsUNEnrolledYearCodeNextFirst = 1
	FROM
		lbswp.GoldenRecord t
		inner join
		(
			SELECT
				gr.CollegeCode,
				gr.StudentId,
				gr.YearCode
			FROM
				lbswp.GoldenRecord gr
				inner join
				comis.studntid id
					on id.college_id = gr.CollegeCode
					and id.student_id = gr.StudentId
				inner join
				comis.Transfer tr
					on id.ssn = tr.StudentSsn
				inner join
				comis.Term t
					on t.TermCode = gr.YearCode
				inner join
				comis.Term t1
					on t1.TermCode = t.YearCodeNext
			WHERE
				tr.InstitutionType = '4'
			GROUP BY
				gr.CollegeCode,
				gr.StudentId,
				gr.YearCode,
				t1.BeginDate,
				t1.EndDate
			HAVING
				t1.BeginDate <= min(tr.TransferDate)
				and t1.EndDate >= min(tr.TransferDate)
		) s
			on t.CollegeCode = s.CollegeCode
			and t.StudentId  = s.StudentId
			and t.YearCode   = s.YearCode;

	UPDATE
		t
	SET
		t.IsEmploymentYearCodeNextSecondQuarter = s.IsEmploymentYearCodeNextSecondQuarter,
		t.IsEmploymentYearCodeNextFourthQuarter = s.IsEmploymentYearCodeNextFourthQuarter,
		t.EarningsYearCodeNextSecondQuarter     = s.EarningsYearCodeNextSecondQuarter,
		t.EarningsYearCodeNextAnnual            = s.EarningsYearCodeNextAnnual
	FROM
		lbswp.GoldenRecord t
		inner join
		(
			SELECT
				gr.CollegeCode,
				gr.StudentId,
				gr.YearCode,
				IsEmploymentYearCodeNextSecondQuarter = max(convert(tinyint, IsQuarterSecond)),
				IsEmploymentYearCodeNextFourthQuarter = max(convert(tinyint, IsQuarterFourth)),
				EarningsYearCodeNextSecondQuarter     = max(case when IsQuarterSecond = 1 then Wages else 0 end),
				EarningsYearCodeNextAnnual            = sum(wages)
			FROM
				(
					SELECT DISTINCT
						CollegeCode     = gr.CollegeCode,
						StudentId       = gr.StudentId,
						YearCode        = gr.YearCode,
						StudentIdSocial = id.ssn
					FROM
						lbswp.GoldenRecord gr
						inner join
						comis.studntid id
							on id.college_id = gr.CollegeCode
							and id.student_id = gr.StudentId
					WHERE
						id.student_id_status = 'S'
				) gr
				inner join
				comis.Term t
					on t.TermCode = gr.YearCode
				inner join
				lbswp.Fiscal f
					on f.YearCode = t.YearCodeNext
				inner join
				dbo.eddui ed
					on ed.StudentIdSsnEnc = gr.StudentIdSocial
					and ed.YearId = f.FiscalYear
					and ed.QtrId = f.FiscalQuarter
			GROUP BY
				gr.CollegeCode,
				gr.StudentId,
				gr.YearCode
		) s
			on t.CollegeCode = s.CollegeCode
			and t.StudentId  = s.StudentId
			and t.YearCode   = s.YearCode;

	UPDATE
		t
	SET
		t.IsAwardAny             = s.IsAwardAny,
		t.IsVocationalSuccessAll = s.IsVocationalSuccessAll
	FROM
		lbswp.GoldenRecord t
		inner join
		(
			SELECT
				CollegeCode,
				StudentId,
				YearCode,
				IsAwardAny              = max(convert(tinyint, IsAwardProgram)),
				IsVocationalSuccessAll  = 
					min(
						case
							when IsVocationalSuccess = 1 then 1
							when IsAwardProgram = 1 then 1
							else 0
						end
		 			)
			FROM
				lbswp.GoldenRecord
			GROUP BY
				CollegeCode,
				StudentId,
				YearCode
		) s
			on t.CollegeCode = s.CollegeCode
			and t.StudentId  = s.StudentId
			and t.YearCode   = s.YearCode;

	UPDATE
		t
	SET
		t.IsSkillsBuilder = s.IsSkillsBuilder,
		t.IsTransfer      = s.IsTransfer,
		t.IsExiter        = s.IsExiter,
		t.IsCompleter     = s.IsCompleter
	FROM
		lbswp.GoldenRecord t
		inner join
		(
			SELECT
				CollegeCode,
				StudentId,
				YearCode,
				ProgramCode,
				IsSkillsBuilder = 
					case
						when
							sum(NonIntroUnits) >= 0.5
							and
							IsAwardAny = 0
							and
							IsVocationalSuccessAll = 1
							and
							IsCCEnrolledYearCodeNext = 0
							and
							IsUNEnrolledYearCodeNext = 0 then 1
						else 0
					end,
				IsTransfer = 
					case
						when
							(
								sum(NonIntroUnits) > 0
								or
								IsAwardAny = 1
							)
							and
							IsCCEnrolledYearCodeNext = 0
							and
							IsUNEnrolledYearCodeNextFirst = 1 then 1
						else 0
					end,
				IsExiter = 
					case
						when
							(
								sum(NonIntroUnits) > 0
								or
								IsAwardAny = 1
							)
							and
							IsCCEnrolledYearCodeNext = 0
							and
							IsUNEnrolledYearCodeNext = 0 then 1
						else 0
					end,
				IsCompleter = max(convert(tinyint, IsAwardProgramRigorous))
			FROM
				lbswp.GoldenRecord
			GROUP BY
				CollegeCode,
				StudentId,
				YearCode,
				ProgramCode,
				IsAwardAny,
				IsVocationalSuccessAll,
				IsCCEnrolledYearCodeNext,
				IsUNEnrolledYearCodeNext,
				IsUNEnrolledYearCodeNextFirst
		) s
			on t.CollegeCode  = s.CollegeCode
			and t.StudentId   = s.StudentId
			and t.YearCode    = s.YearCode
			and t.ProgramCode = s.ProgramCode;

END;