USE calpass;

GO

IF (object_id('lbswp.Metric508Process') is not null)
	BEGIN
		DROP PROCEDURE lbswp.Metric508Process;
	END;

GO

CREATE PROCEDURE
	lbswp.Metric508Process
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;


DECLARE
	@Message     nvarchar(2048),
	@Time        datetime,
	@Seconds     int,
	@Severity    tinyint = 0,
	@State       tinyint = 1,
	@Error       varchar(2048),
	@Iterator    int = 0,
	@Iterations  int,
	@CollegeId   tinyint;
	
DECLARE
	@College
AS TABLE
	(
		Iterator  tinyint identity(0,1) not null,
		CollegeId tinyint               not null
	);

BEGIN
	-- fresh table
	TRUNCATE TABLE lbswp.Earnings;
	-- popualte
	INSERT INTO
		lbswp.Earnings
		(
			CollegeId,
			YearId,
			StudentId,
			ContentId,
			VocationId,
			SectorId,
			SubdisciplineId,
			ProgramId,
			DemographicId,
			AgeId,
			GenderId,
			RaceId,
			Earnings
		)
	SELECT
		rc.RegionCollegeId,
		y.YearId,
		gr.StudentId,
		gr.ContentId,
		gr.VocationId,
		gr.SectorId,
		gr.SubdisciplineId,
		gr.ProgramId,
		gr.DemographicId,
		gr.AgeId,
		gr.GenderId,
		gr.RaceId,
		Earnings = EarningsYearCodeNextSecondQuarter
	FROM
		lbswp.GoldenRecord gr
		inner join
		lbswp.Year y
			on y.YearCode = gr.YearCode
		inner join
		lbswp.RegionCollege rc
			on rc.CollegeCode = gr.CollegeCode
	WHERE
		EarningsYearCodeNextSecondQuarter <> 0
		and IsExiter = 1;
	-- populate
	INSERT
		@College
		(
			CollegeId
		)
	SELECT
		RegionCollegeId
	FROM
		lbswp.RegionCollege;
	-- get limit
	SELECT
		@Iterations = count(*)
	FROM
		@College;
	-- loop
	WHILE (@Iterator < @Iterations)
	BEGIN

		SET @Error = convert(varchar, @Iterator + 1) + ' of ' + convert(varchar, @Iterations);

		RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

		SET @Time = getdate();

		SELECT
			@CollegeId = CollegeId
		FROM
			@College
		WHERE
			Iterator = @Iterator;

		-- merge
		MERGE
				lbswp.base
		WITH
			(
				TABLOCKX
			) t
		USING
			(
				SELECT
					CollegeId,
					YearId,
					c.ContentId,
					ContentKey,
					d.DemographicId,
					DemographicKey,
					Metric508 = EarningsMedian
				FROM
					(
						SELECT
							c.CollegeId,
							c.YearId,
							c.ContentIdentifier,
							c.ContentKey,
							c.DemographicIdentifier,
							c.DemographicKey,
							RecordCount = count(*)
						FROM
							lbswp.Earnings m
						UNPIVOT
							(
								DemographicKey for DemographicIdentifier in (DemographicId, AgeId, GenderId, RaceId)
							) d
						UNPIVOT
							(
								ContentKey for ContentIdentifier in (ContentId, VocationId, SectorId, SubdisciplineId, ProgramId)
							) c
						WHERE
							c.CollegeId = @CollegeId
						GROUP BY
							c.CollegeId,
							c.YearId,
							c.ContentIdentifier,
							c.ContentKey,
							c.DemographicIdentifier,
							c.DemographicKey
					) ud
					cross apply
					(
						SELECT
							Metric508 = avg(up.Earnings)
						FROM
							(
								SELECT
									c.Earnings
								FROM
									(
										SELECT
											c.CollegeId,
											c.YearId,
											c.ContentIdentifier,
											c.ContentKey,
											c.DemographicIdentifier,
											c.DemographicKey,
											c.Earnings
										FROM
											lbswp.Earnings m
										UNPIVOT
											(
												DemographicKey for DemographicIdentifier in (DemographicId, AgeId, GenderId, RaceId)
											) d
										UNPIVOT
											(
												ContentKey for ContentIdentifier in (ContentId, VocationId, SectorId, SubdisciplineId, ProgramId)
											) c
									) c
								WHERE
									c.CollegeId                 = ud.CollegeId
									and c.YearId                = ud.YearId
									and c.ContentIdentifier     = ud.ContentIdentifier
									and c.ContentKey            = ud.ContentKey
									and c.DemographicIdentifier = ud.DemographicIdentifier
									and c.DemographicKey        = ud.DemographicKey
								ORDER BY
									c.Earnings
								OFFSET
									((ud.RecordCount - 1) / 2) ROWS
								FETCH NEXT
									(2 - (ud.RecordCount % 2)) ROWS ONLY
							) up
					) up (EarningsMedian)
					inner join
					lbswp.Content c
						on c.Identifier = ud.ContentIdentifier
					inner join
					lbswp.Demographic d
						on d.Identifier = ud.DemographicIdentifier
			) s
		ON
			(
				s.CollegeId          = t.CollegeId
				and s.YearId         = t.YearId
				and s.ContentId      = t.ContentId
				and s.ContentKey     = t.ContentKey
				and s.DemographicId  = t.DemographicId
				and s.DemographicKey = t.DemographicKey
			)
		WHEN MATCHED THEN 
			UPDATE SET
				t.Metric508 = s.Metric508
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					CollegeId,
					YearId,
					ContentId,
					ContentKey,
					DemographicId,
					DemographicKey,
					Metric508
				)
			VALUES
				(
					s.CollegeId,
					s.YearId,
					s.ContentId,
					s.ContentKey,
					s.DemographicId,
					s.DemographicKey,
					s.Metric508
				);

		-- get time elapsed
		SET @Seconds = datediff(second, @Time, getdate());
		-- set msg
		SET @Error = 'ITERATION: ' + 
			convert(nvarchar, @Seconds / 86400) + ':' +
			convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
		-- output to user
		RAISERROR(@Error, 0, 1) WITH NOWAIT;

		SET @Iterator += 1;
	END;
	-- clean up
	TRUNCATE TABLE lbswp.Earnings;
	-- regional metrics
	SELECT
		ud.YearId,
		r.RegionId,
		ud.RegionKey,
		ud.ContentId,
		ud.ContentKey,
		ud.DemographicId,
		ud.DemographicKey,
		Metric508 = up.Metric508
	FROM
		(
			SELECT
				r.YearId,
				r.RegionIdentifier,
				r.RegionKey,
				r.ContentId,
				r.ContentKey,
				r.DemographicId,
				r.DemographicKey,
				RecordCount = count(*)
			FROM
				lbswp.Base m
				inner join
				(
					SELECT
						RegionCollegeId,
						rs.RegionStateId,
						rma.RegionMacroId,
						rmi.RegionMicroId
					FROM
						lbswp.RegionCollege rc
						inner join
						lbswp.RegionMicro rmi
							on rmi.RegionMicroId = rc.RegionMicroId
						inner join
						lbswp.RegionMacro rma
							on rma.RegionMacroId = rmi.RegionMacroId
						inner join
						lbswp.RegionState rs
							on rs.RegionStateId = rma.RegionStateId
				) rc
					on rc.RegionCollegeId = m.CollegeId
			UNPIVOT
				(
					RegionKey for RegionIdentifier in (RegionStateId, RegionMacroId, RegionMicroId)
				) r
			GROUP BY
				r.YearId,
				r.RegionIdentifier,
				r.RegionKey,
				r.ContentId,
				r.ContentKey,
				r.DemographicId,
				r.DemographicKey
		) ud
		cross apply
		(
			SELECT
				Metric508 = avg(up.Metric508)
			FROM
				(
					SELECT
						r.Metric508
					FROM
						(
							SELECT
								r.Metric508
							FROM
								lbswp.Base m
								inner join
								(
									SELECT
										RegionCollegeId,
										rs.RegionStateId,
										rma.RegionMacroId,
										rmi.RegionMicroId
									FROM
										lbswp.RegionCollege rc
										inner join
										lbswp.RegionMicro rmi
											on rmi.RegionMicroId = rc.RegionMicroId
										inner join
										lbswp.RegionMacro rma
											on rma.RegionMacroId = rmi.RegionMacroId
										inner join
										lbswp.RegionState rs
											on rs.RegionStateId = rma.RegionStateId
								) rc
									on rc.RegionCollegeId = m.CollegeId
							UNPIVOT
								(
									RegionKey for RegionIdentifier in (RegionStateId, RegionMacroId, RegionMicroId)
								) r
						WHERE
							r.YearId               = ud.YearId
							and r.RegionIdentifier = ud.RegionIdentifier
							and r.RegionKey        = ud.RegionKey
							and r.ContentId        = ud.ContentId
							and r.ContentKey       = ud.ContentKey
							and r.DemographicId    = ud.DemographicId
							and r.DemographicKey   = ud.DemographicKey
						ORDER BY
							r.Metric508
						OFFSET
							((ud.RecordCount - 1) / 2) ROWS
						FETCH NEXT
							(2 - (ud.RecordCount % 2)) ROWS ONLY
						) r
				) up
		) up (Metric508)
		inner join
		lbswp.Region r
			on r.Identifier = ud.RegionIdentifier;
END;