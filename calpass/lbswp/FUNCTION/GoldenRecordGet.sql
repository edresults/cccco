USE calpass;

GO

IF (object_id('lbswp.GoldenRecordGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION lbswp.GoldenRecordGet;
	END;

GO

CREATE FUNCTION
	lbswp.GoldenRecordGet
	(
		@CollegeCode char(3)
	)
RETURNS
	TABLE
AS
	RETURN (
	SELECT
		CollegeCode            = u.CollegeCode,
		StudentId              = u.StudentId,
		YearCode               = u.YearCode,
		ProgramCode            = u.ProgramCode,
		VocationId             = u.VocationId,
		SectorId               = u.SectorId,
		SubdisciplineId        = u.SubdisciplineId,
		ProgramId              = u.ProgramId,
		RaceId                 = u.RaceId,
		AgeId                  = u.AgeId,
		GenderId               = u.GenderId,
		IsWorkforce            = u.IsWorkforce,
		IsFoster               = u.IsFoster,
		IsVeteran              = u.IsVeteran,
		IsFirstGeneration      = u.IsFirstGeneration,
		IsEops                 = u.IsEops,
		IsDisability           = u.IsDisability,
		IsDisadvantaged        = u.IsDisadvantaged,
		IsFinancialAid         = u.IsFinancialAid,
		IsCredit               = max(u.IsCredit),
		IsNonCredit            = max(u.IsNonCredit),
		EnrolledSections       = max(u.EnrolledSections),
		NonIntroUnits          = max(u.NonIntroUnits),
		IsVocationalSuccess    = max(u.IsVocationalSuccess),
		IsAwardProgram         = max(u.IsAwardProgram),
		IsAwardProgramRigorous = max(u.IsAwardProgramRigorous)
	FROM
		(
			SELECT
				CollegeCode,
				StudentId,
				YearCode,
				ProgramCode,
				VocationId,
				SectorId,
				SubdisciplineId,
				ProgramId,
				RaceId,
				AgeId,
				GenderId,
				IsWorkforce,
				IsFoster,
				IsVeteran,
				IsFirstGeneration,
				IsEops,
				IsDisability,
				IsDisadvantaged,
				IsFinancialAid,
				IsCredit,
				IsNonCredit,
				EnrolledSections,
				NonIntroUnits,
				IsVocationalSuccess,
				IsAwardProgram = 0,
				IsAwardProgramRigorous = 0
			FROM
				lbswp.CollegeEnrollGet(@CollegeCode)
			UNION
			SELECT
				CollegeCode,
				StudentId,
				YearCode,
				ProgramCode,
				VocationId,
				SectorId,
				SubdisciplineId,
				ProgramId,
				RaceId,
				AgeId,
				GenderId,
				IsWorkforce,
				IsFoster,
				IsVeteran,
				IsFirstGeneration,
				IsEops,
				IsDisability,
				IsDisadvantaged,
				IsFinancialAid,
				IsCredit = 0,
				IsNonCredit = 0,
				EnrolledSections = 0,
				NonIntroUnits = 0,
				IsVocationalSuccess = 0,
				IsAwardProgram,
				IsAwardProgramRigorous
			FROM
				lbswp.CollegeAwardGet(@CollegeCode)
		) u
	GROUP BY
		u.CollegeCode,
		u.StudentId,
		u.YearCode,
		u.ProgramCode,
		u.VocationId,
		u.SectorId,
		u.SubdisciplineId,
		u.ProgramId,
		u.RaceId,
		u.AgeId,
		u.GenderId,
		u.IsWorkforce,
		u.IsFoster,
		u.IsVeteran,
		u.IsFirstGeneration,
		u.IsEops,
		u.IsDisability,
		u.IsDisadvantaged,
		u.IsFinancialAid
	);