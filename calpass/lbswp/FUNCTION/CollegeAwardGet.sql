USE calpass;

GO

IF (object_id('lbswp.CollegeAwardGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION lbswp.CollegeAwardGet;
	END;

GO

CREATE FUNCTION
	lbswp.CollegeAwardGet
	(
		@CollegeCode char(3)
	)
RETURNS
	TABLE
AS
	RETURN (
	SELECT
		CollegeCode            = u.CollegeCode,
		StudentId              = u.StudentId,
		YearCode               = u.YearCode,
		ProgramCode            = u.ProgramCode,
		VocationId             = u.VocationId,
		SectorId               = u.SectorId,
		SubdisciplineId        = u.SubdisciplineId,
		ProgramId              = u.ProgramId,
		RaceId                 = st.YearRaceId,
		AgeId                  = st.YearAgeCategoryId,
		GenderId               = st.YearGenderId,
		IsWorkforce            = max(convert(tinyint, w.IsWorkforce)),
		IsFoster               = max(convert(tinyint, isnull(sg.IsFoster, 0))),
		IsVeteran              = max(convert(tinyint, isnull(sg.IsVeteran, 0))),
		IsFirstGeneration      = max(convert(tinyint, isnull(sg.IsFirstGeneration, 0))),
		IsEops                 = max(case when se.student_id is not null then 1 else 0 end),
		IsDisability           = max(case when sd.student_id is not null then 1 else 0 end),
		IsDisadvantaged        = max(convert(tinyint, isnull(sv.IsDisadvantaged, 0))),
		IsFinancialAid         = max(convert(tinyint, isnull(sf.IsLaunchBoard, 0))),
		IsAwardProgram         = u.IsAwardProgram,
		IsAwardProgramRigorous = u.IsAwardProgramRigorous
	FROM
		(
			SELECT
				CollegeCode            = sp.college_id,
				StudentId              = sp.student_id,
				YearCode               = sp.term_id,
				ProgramCode            = sp.top_code,
				VocationId             = p.IsVocational,
				SectorId               = p.SectorId,
				SubdisciplineId        = p.SubdisciplineId,
				ProgramId              = p.ProgramId,
				IsAwardProgram         = 1,
				IsAwardProgramRigorous = max(convert(tinyint, a.IsRigorous))
			FROM
				comis.spawards sp
				inner join
				lbswp.Award a
					on  a.AwardCode = sp.award
				inner join
				lbswp.Year y
					on y.YearCode = sp.term_id
				inner join
				lbswp.Program p
					on p.ProgramCode = sp.top_code
			WHERE
				sp.college_id = @CollegeCode
			GROUP BY
				sp.college_id,
				sp.student_id,
				sp.term_id,
				sp.top_code,
				p.IsVocational,
				p.SectorId,
				p.SubdisciplineId,
				p.ProgramId
		) u
		inner join
		comis.Term tu
			on tu.TermCode = u.YearCode
		inner join
		comis.stterm st
			on st.college_id = u.CollegeCode
			and st.student_id = u.StudentId
		inner join
		comis.Term tst
			on tst.TermCode = st.term_id
		inner join
		lbswp.Workforce w
			on w.WorkforceCode = st.jtpa_status
		left outer join
		comis.sgpops sg
			on sg.college_id = st.college_id
			and sg.student_id = st.student_id
			and sg.term_id = st.term_id
		left outer join
		comis.seeops se
			on se.college_id = st.college_id
			and se.student_id = st.student_id
			and se.term_id = st.term_id
		left outer join
		comis.sddsps sd
			on sd.college_id = st.college_id
			and sd.student_id = st.student_id
			and sd.term_id = st.term_id
		left outer join
		comis.svvatea sv
			on sv.college_id = st.college_id
			and sv.student_id = st.student_id
			and sv.term_id = st.term_id
		left outer join
		comis.sfawards sf
			on sf.college_id = st.college_id
			and sf.student_id = st.student_id
			and sf.term_id = tst.YearCode
	WHERE
		tst.YearCodeFull = (
			SELECT
				max(tst1.YearCodeFull)
			FROM
				comis.stterm st1
				inner join
				comis.Term tst1
					on tst1.TermCode = st1.term_id
			WHERE
				st1.college_id = st.college_id
				and st1.student_id = st.student_id
				and tst1.YearCodeFull <= tu.YearCodeFull
		)
	GROUP BY
		u.CollegeCode,
		u.StudentId,
		u.YearCode,
		u.ProgramCode,
		u.VocationId,
		u.SectorId,
		u.SubdisciplineId,
		u.ProgramId,
		st.YearRaceId,
		st.YearAgeCategoryId,
		st.YearGenderId,
		u.IsAwardProgram,
		u.IsAwardProgramRigorous
);