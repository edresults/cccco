USE calpass;

GO

IF (object_id('lbswp.BehaviorCohort', 'IF') is not null)
	BEGIN
		DROP FUNCTION lbswp.BehaviorCohort;
	END;

GO

CREATE FUNCTION
	lbswp.BehaviorCohort
	(
		@InterSegmentKey binary(64)
	)
RETURNS
	TABLE
AS
	RETURN (
	SELECT
		CollegeCode     = cohort.CollegeCode,
		YearCode        = cohort.YearCode,
		ProgramCode     = cohort.ProgramCode,
		IsCredit = 
			case
				when cohort.ProgramEnrolledUnits > 0.5 then 1
				else 0
			end,
		IsNonCredit = 
			case 
				when cohort.ProgramAttendedHours >= 12 then 1
				else 0
			end,
		IsBuilder = 
			case
				when cohort.ProgramNonIntroUnits >= 0.5 and cohort.YearIsVocationalSuccess = 1 and min(cohort.IsAwardProgram) over(partition by cohort.YearCode) = 0 and cohort.YearIsYearCodeNext = 0 and un.YearCode is null then 1
				else 0
			end,
		IsTransfer = 
			case
				when cohort.ProgramNonIntroUnits >= 0.5 and cohort.YearIsYearCodeNext = 0 and un.YearCodeFirst = y.YearCodeNext then 1
				when cohort.IsAwardProgram = 1 and cohort.YearIsYearCodeNext = 0 and un.YearCodeFirst = y.YearCodeNext then 1
				else 0
			end,
		IsExiter = 
			case
				when cohort.ProgramNonIntroUnits >= 0.5 and cohort.YearIsYearCodeNext = 0 and un.YearCode is null then 1
				when cohort.IsAwardProgram = 1 and cohort.YearIsYearCodeNext = 0 and un.YearCode is null then 1
				else 0
			end,
		IsCompleter = 
			case
				when cohort.IsAwardProgramRigorous = 1 then 1
				else 0
			end
	FROM
		(
			SELECT
				CollegeCode                   = derived.CollegeCode,
				YearCode                      = derived.YearCode,
				ProgramCode                   = derived.ProgramCode,
				ProgramEnrolledUnits          = max(derived.ProgramEnrolledUnits),
				ProgramEnrolledSections       = max(derived.ProgramEnrolledSections),
				ProgramAttendedHours          = max(derived.ProgramAttendedHours),
				ProgramNonIntroUnits          = max(derived.ProgramNonIntroUnits),
				YearIsVocationalSuccess       = max(derived.YearIsVocationalSuccess),
				ProgramIsYearCodeNext         = max(derived.ProgramIsYearCodeNext),
				YearIsYearCodeNext            = max(derived.YearIsYearCodeNext),
				StudentIsNonIntroYearCodeLast = max(derived.StudentIsNonIntroYearCodeLast),
				IsAwardProgram                = max(derived.IsAwardProgram),
				IsAwardProgramRigorous        = max(derived.IsAwardProgramRigorous)
			FROM
				(
					SELECT
						CollegeCode                   = c.CollegeCode,
						YearCode                      = c.YearCode,
						ProgramCode                   = c.ProgramCode,
						ProgramEnrolledUnits          = c.ProgramEnrolledUnits,
						ProgramEnrolledSections       = c.ProgramEnrolledSections,
						ProgramAttendedHours          = c.ProgramAttendedHours,
						ProgramNonIntroUnits          = c.ProgramNonIntroUnits,
						YearIsVocationalSuccess       = c.YearIsVocationalSuccess,
						ProgramIsYearCodeNext         = c.ProgramIsYearCodeNext,
						YearIsYearCodeNext            = c.YearIsYearCodeNext,
						StudentIsNonIntroYearCodeLast = c.StudentIsNonIntroYearCodeLast,
						IsAwardProgram                = null,
						IsAwardProgramRigorous        = null
					FROM
						lbswp.BehaviorCourse(@InterSegmentKey) c
					UNION
					SELECT
						CollegeCode                   = a.CollegeCode,
						YearCode                      = a.YearCode,
						ProgramCode                   = a.ProgramCode,
						ProgramEnrolledUnits          = null,
						ProgramEnrolledSections       = null,
						ProgramAttendedHours          = null,
						ProgramNonIntroUnits          = null,
						YearIsVocationalSuccess       = null,
						ProgramIsYearCodeNext         = null,
						YearIsYearCodeNext            = null,
						StudentIsNonIntroYearCodeLast = null,
						IsAwardProgram                = a.IsAwardProgram,
						IsAwardProgramRigorous        = a.IsAwardProgramRigorous
					FROM
						lbswp.BehaviorAward(@InterSegmentKey) a
				) derived
			GROUP BY
				derived.CollegeCode,
				derived.YearCode,
				derived.ProgramCode
		) cohort
		inner join
		lbswp.Year y
			on cohort.YearCode = y.YearCode
		left outer join
		(
			SELECT DISTINCT
				InterSegmentKey = u.Derkey1,
				YearCode        = ut.YearCode,
				YearCodeFirst   = min(ut.YearCode) over()
			FROM
				dbo.UnivStudentProd u
				inner join
				dbo.UnivTerm ut
					on ut.TermCode = u.YrTerm
			WHERE
				u.Derkey1 = @InterSegmentKey
		) un
			on y.YearCodeNext = un.YearCode
);