USE calpass;

GO

IF (object_id('lbswp.CollegeEnrollGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION lbswp.CollegeEnrollGet;
	END;

GO

CREATE FUNCTION
	lbswp.CollegeEnrollGet
	(
		@CollegeCode char(3)
	)
RETURNS
	TABLE
AS
	RETURN (
	SELECT
		-- primary key
		CollegeCode         = u.CollegeCode,
		StudentId           = u.StudentId,
		YearCode            = u.YearCode,
		ProgramCode         = u.ProgramCode,
		-- content
		VocationId          = u.VocationId,
		SectorId            = u.SectorId,
		SubdisciplineId     = u.SubdisciplineId,
		ProgramId           = u.ProgramId,
		-- demograhpic
		RaceId              = st.YearRaceId,
		AgeId               = st.YearAgeCategoryId,
		GenderId            = st.YearGenderId,
		-- attribtues
		IsWorkforce         = max(convert(tinyint, w.IsWorkforce)),
		IsFoster            = max(convert(tinyint, isnull(sg.IsFoster, 0))),
		IsVeteran           = max(convert(tinyint, isnull(sg.IsVeteran, 0))),
		IsFirstGeneration   = max(convert(tinyint, isnull(sg.IsFirstGeneration, 0))),
		IsEops              = max(case when se.student_id is not null then 1 else 0 end),
		IsDisability        = max(case when sd.student_id is not null then 1 else 0 end),
		IsDisadvantaged     = max(convert(tinyint, isnull(sv.IsDisadvantaged, 0))),
		IsFinancialAid      = max(convert(tinyint, isnull(sf.IsLaunchBoard, 0))),
		IsCredit            = u.IsCredit,
		IsNonCredit         = u.IsNonCredit,
		EnrolledSections    = u.EnrolledSections,
		NonIntroUnits       = u.NonIntroUnits,
		IsVocationalSuccess = u.IsVocationalSuccess
	FROM
		(
			SELECT
				CollegeCode       = sx.college_id,
				StudentId         = sx.student_id,
				YearCode          = t.YearCode,
				ProgramCode       = p.ProgramCode,
				VocationId        = p.IsVocational,
				SectorId          = p.SectorId,
				SubdisciplineId   = p.SubdisciplineId,
				ProgramId         = p.ProgramId,
				IsCredit = 
					case
						when 
							sum(
								case
									when m.IsEnroll = 1 then sx.units_attempted
									else 0
								end
							) >= 0.5 then 1
						else 0
					end,
				IsNonCredit = 
					case
						when
							sum(
								case
									when c.IsCredit = 0 and sx.attend_hours not in (8888.8, 9999.9) then sx.attend_hours
									when c.IsCredit = 1 then 0
									else 0
								end
							) >= 12 then 1
						else 0
					end,
				EnrolledSections = 
					sum(
						case
							when m.IsEnroll = 1 then 1
							else 0
						end
					),
				NonIntroUnits = 
					sum(
						case
							when m.IsEnroll = 1 and o.IsIntroductory = 0 then sx.units_attempted
							else 0
						end
					),
				IsVocationalSuccess = 
					min(
						case
							-- disregard non-vocational courses
							when p.IsVocational = 0 then 0
							when p.IsVocational = 1 and m.IsSuccess = 1 then 1
							when p.IsVocational = 1 and m.IsSuccess = 0 then 0
							else 0
						end
					)
			FROM
				comis.sxenrlm sx
				inner join
				comis.cbcrsinv cb
					on  cb.college_id     = sx.college_id
					and cb.course_id      = sx.course_id
					and cb.control_number = sx.control_number
					and cb.term_id        = sx.term_id
				inner join
				comis.Term t
					on  t.TermCode = sx.term_id
				inner join
				lbswp.Year y
					on y.YearCode = t.YearCode
				inner join
				lbswp.Mark m
					on  m.MarkCode = sx.Grade
				inner join
				lbswp.Program p
					on  p.ProgramCode = cb.top_code
				inner join
				lbswp.Occupation o
					on  o.OccupationCode = cb.sam_code
				inner join
				lbswp.Credit c
					on  c.CreditCode = cb.credit_status
			WHERE
				sx.college_id = @CollegeCode
			GROUP BY
				sx.college_id,
				sx.student_id,
				t.YearCode,
				p.ProgramCode,
				p.IsVocational,
				p.SectorId,
				p.SubdisciplineId,
				p.ProgramId
		) u
		inner join
		comis.Term t
			on t.YearCode = u.YearCode
		inner join
		comis.stterm st
			on st.college_id = u.CollegeCode
			and st.student_id = u.StudentId
			and st.term_id = t.TermCode
		inner join
		lbswp.Workforce w
			on w.WorkforceCode = st.jtpa_status
		left outer join
		comis.sgpops sg
			on sg.college_id = st.college_id
			and sg.student_id = st.student_id
			and sg.term_id = st.term_id
		left outer join
		comis.seeops se
			on se.college_id = st.college_id
			and se.student_id = st.student_id
			and se.term_id = st.term_id
		left outer join
		comis.sddsps sd
			on sd.college_id = st.college_id
			and sd.student_id = st.student_id
			and sd.term_id = st.term_id
		left outer join
		comis.svvatea sv
			on sv.college_id = st.college_id
			and sv.student_id = st.student_id
			and sv.term_id = st.term_id
		left outer join
		comis.sfawards sf
			on sf.college_id = u.CollegeCode
			and sf.student_id = u.StudentId
			and sf.term_id = u.YearCode
	GROUP BY
		u.CollegeCode,
		u.StudentId,
		u.YearCode,
		u.ProgramCode,
		u.VocationId,
		u.SectorId,
		u.SubdisciplineId,
		u.ProgramId,
		st.YearRaceId,
		st.YearAgeCategoryId,
		st.YearGenderId,
		u.IsCredit,
		u.IsNonCredit,
		u.EnrolledSections,
		u.NonIntroUnits,
		u.IsVocationalSuccess
);