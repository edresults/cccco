USE calpass;

GO

IF (object_id('lbswp.BehaviorUN', 'IF') is not null)
	BEGIN
		DROP FUNCTION lbswp.BehaviorUN;
	END;

GO

CREATE FUNCTION
	lbswp.BehaviorUN
	(
		@InterSegmentKey binary(64),
		@YearCodeNext    char(3)
	)
RETURNS
	TABLE
AS
	RETURN (
	SELECT DISTINCT 
		YearCode      = ut.YearCode,
		YearCodeFirst = min(ut.YearCode) over()
	FROM
		dbo.UnivStudentProd u
		inner join
		dbo.UnivTerm ut
			on ut.TermCode = u.YrTerm
	WHERE
		u.Derkey1 = @InterSegmentKey
);