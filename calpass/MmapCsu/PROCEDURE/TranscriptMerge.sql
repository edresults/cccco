USE calpass;

GO

IF (object_id('MmapCsu.TranscriptMerge') is not null)
	BEGIN
		DROP PROCEDURE MmapCsu.TranscriptMerge;
	END;

GO

CREATE PROCEDURE
	MmapCsu.TranscriptMerge
	(
		@UniversityCode char(6) = null
	)
AS

	SET NOCOUNT ON;
	
DECLARE
	@YearTermRankThreshold tinyint = 2;

BEGIN
	IF (@UniversityCode is not null)
		BEGIN
			DELETE
			FROM
				MmapCsu.Transcript
			WHERE
				UniversityCode != @UniversityCode;
		END;

	MERGE
		MmapCsu.Transcript t
	USING
		(
			SELECT
				UniversityCode,
				Derkey1,
				ContentAreaCode,
				YearTermCode,
				CourseRowNumber,
				YearTermRank,
				LevelBelow,
				ContentAreaFirstYearTermCode,
				CourseId,
				CourseTitle,
				UnitsTry,
				Mark,
				Points,
				Category,
				IsSuccess
			FROM
				(
					SELECT
						crt.UniversityCode,
						stu.Derkey1,
						ContentAreaCode = isnull(ca.ContentAreaCode, ''),
						t.YearTermCode,
						LevelBelow = isnull(rem.LevelBelow, 0),
						CourseId = 
							case
								when crs.School = '110608' then
									case
										when rtrim(crs.CourseAbbr) = 'ENGL' and rtrim(crs.CourseNum) in ('113', '114', '115') then coalesce(ltrim(rtrim(uce.CourseAbbrEquivalency)) + ltrim(rtrim(uce.CourseNumEquivalency)), ltrim(rtrim(crs.CourseAbbr)) + ltrim(rtrim(crs.CourseNum)) + ltrim(rtrim(crs.CourseNumSuffix)))
										when rtrim(crs.CourseAbbr) = 'MATH' and rtrim(crs.CourseNum) = '150' then coalesce(ltrim(rtrim(uce.CourseAbbrEquivalency)) + ltrim(rtrim(uce.CourseNumEquivalency)), ltrim(rtrim(crs.CourseAbbr)) + ltrim(rtrim(crs.CourseNum)) + ltrim(rtrim(crs.CourseNumSuffix)))
										else coalesce(ltrim(rtrim(uce.CourseAbbrEquivalency)) + ltrim(rtrim(uce.CourseNumEquivalency)), ltrim(rtrim(crs.CourseAbbr)) + ltrim(rtrim(crs.CourseNum)))
									end
								else coalesce(ltrim(rtrim(uce.CourseAbbrEquivalency)) + ltrim(rtrim(uce.CourseNumEquivalency)), ltrim(rtrim(crs.CourseAbbr)) + ltrim(rtrim(crs.CourseNum)))
							end,
						crs.CourseTitle,
						UnitsTry = crs.Units,
						Mark = m.MarkCode,
						m.Points,
						m.Category,
						m.IsSuccess,
						ContentAreaFirstYearTermCode = 
							min(t.YearTermCode) over(
								partition by
									crt.UniversityCode,
									stu.Derkey1,
									isnull(ca.ContentAreaCode, '')
							),
						CourseRowNumber = 
							row_number() over(
								partition by
									crt.UniversityCode,
									stu.Derkey1,
									isnull(ca.ContentAreaCode, '')
								order by
									t.YearTermCode asc,
									m.Rank asc
							),
						YearTermRank = 
							dense_rank() over(
								partition by
									crt.UniversityCode,
									stu.Derkey1,
									isnull(ca.ContentAreaCode, '')
								order by
									t.YearTermCode asc
							)
					FROM
						MmapCsu.Cohort crt
						inner join
						dbo.UnivStudentProd stu with(index(pk_UnivStudentProd))
							on crt.UniversityCode = stu.school
							and crt.Derkey1 = stu.Derkey1
						inner join
						dbo.UnivCourseProd crs
							on stu.School = crs.School
							and stu.StudentId = crs.StudentId
							and stu.YrTerm = crs.YrTerm
						inner join
						dbo.UnivTerm t
							on t.TermCode = stu.YrTerm
						inner join
						calpads.Mark m
							on m.MarkCode = crs.Grade
						left outer join
						MmapCsu.ContentArea ca
							on ca.UniversityCode = crs.School
							and ca.CourseAbbr = crs.CourseAbbr
						left outer join
						MmapCsu.UnivRemedial rem
							on rem.UniversityCode = crs.school
							and rem.CourseAbbr = rtrim(crs.CourseAbbr)
							and rem.CourseNum = 
								case
									when crs.School = '110608' and rtrim(crs.CourseAbbr) = 'ENGL' and rtrim(crs.CourseNum) in ('113', '114', '115') then rtrim(crs.CourseNum) + rtrim(crs.CourseNumSuffix)
									else rtrim(crs.CourseNum)
								end
						left outer join
						MmapCsu.UnivCourseEquivalency uce
							on uce.UniversityCode = crs.School
							and uce.CourseAbbr = rtrim(crs.CourseAbbr)
							and uce.CourseNum = rtrim(crs.CourseNum)
					WHERE
						(
							@UniversityCode is null
							or
							stu.School = @UniversityCode
						)
						and
						(
							stu.School != '110617'
							or
							(
								stu.School = '110617'
								and
								(
									crs.CourseAbbr = 'LS'
									and
									crs.CourseNum = '015'
								)
								or
								(
									crs.CourseAbbr != 'LS'
								)
							)
						)
				) a
			WHERE
				a.YearTermRank <= @YearTermRankThreshold
		) s
	ON
		s.UniversityCode = t.UniversityCode
		and s.Derkey1 = t.Derkey1
		and s.ContentAreaCode = t.ContentAreaCode
		and s.YearTermCode = t.YearTermCode
		and s.CourseRowNumber = t.CourseRowNumber
	WHEN MATCHED THEN
		UPDATE SET
			t.YearTermRank = s.YearTermRank,
			t.ContentAreaFirstYearTermCode = s.ContentAreaFirstYearTermCode,
			t.LevelBelow = s.LevelBelow,
			t.CourseId = s.CourseId,
			t.CourseTitle = s.CourseTitle,
			t.UnitsTry = s.UnitsTry,
			t.Mark = s.Mark,
			t.Points = s.Points,
			t.Category = s.Category,
			t.IsSuccess = s.IsSuccess
	WHEN NOT MATCHED THEN
		INSERT
			(
				UniversityCode,
				Derkey1,
				ContentAreaCode,
				YearTermCode,
				CourseRowNumber,
				YearTermRank,
				ContentAreaFirstYearTermCode,
				LevelBelow,
				CourseId,
				CourseTitle,
				UnitsTry,
				Mark,
				Points,
				Category,
				IsSuccess
			)
		VALUES
			(
				s.UniversityCode,
				s.Derkey1,
				s.ContentAreaCode,
				s.YearTermCode,
				s.CourseRowNumber,
				s.YearTermRank,
				s.ContentAreaFirstYearTermCode,
				s.LevelBelow,
				s.CourseId,
				s.CourseTitle,
				s.UnitsTry,
				s.Mark,
				s.Points,
				s.Category,
				s.IsSuccess
			);
END;