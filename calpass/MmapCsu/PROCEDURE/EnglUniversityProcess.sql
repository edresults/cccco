USE calpass;

GO

IF (object_id('MmapCsu.EnglUniversityProcess') is not null)
	BEGIN
		DROP PROCEDURE MmapCsu.EnglUniversityProcess;
	END;

GO

CREATE PROCEDURE
	MmapCsu.EnglUniversityProcess
AS

	SET NOCOUNT ON;

DECLARE
	@UniversityCode char(6) = null,
	@UniversityName varchar(200) = null,
	@ContentAreaIndex int = 0,
	@ContentAreaCount int = 0,
	@UniversityIndex int = 0,
	@UniversityCount int = 0,
	@ContentAreaCode varchar(4) = 'ENGL',
	@FirstTermPivot nvarchar(max),
	@FirstTermAlias nvarchar(max),
	@SecondTermPivot nvarchar(max),
	@SecondTermAlias nvarchar(max),
	@Threshold int = 20,
	@Sql nvarchar(max) = N'';
	
BEGIN

	IF (object_id('tempdb.dbo.#Remedial') is not null)
		BEGIN
			DROP TABLE #Remedial;
		END;
	
	CREATE TABLE
		#Remedial
		(
			UniversityCode char(6),
			CourseId varchar(13),
			Cnt int,
			Ttl int,
			Pct decimal(5,2),
			Rnk int,
			IsLocked bit,
			PRIMARY KEY CLUSTERED
				(
					UniversityCode,
					CourseId
				)
		);

	IF (object_id('tempdb.dbo.#University') is not null)
		BEGIN
			DROP TABLE #University;
		END;
	
	CREATE TABLE
		#University
		(
			UniversityIndex int identity(0,1),
			UniversityCode char(6),
			UniversityName varchar(200),
		);

	INSERT INTO
		#University
		(
			UniversityCode,
			UniversityName
		)
	VALUES
		('110510','CSUSB'),
		('110574','CSUEB'),
		('110608','CSUN'),
		('110617','CSUS'),
		('409698','CSUMB');

	IF (@UniversityCode is not null)
		BEGIN
			DELETE
			FROM
				#University
			WHERE
				UniversityCode != @UniversityCode;
		END;
	
	SELECT
		@UniversityCount = count(*)
	FROM
		#University;

	WHILE (@UniversityIndex < @UniversityCount)
	BEGIN

		SELECT
			@UniversityCode = UniversityCode,
			@UniversityName = UniversityName
		FROM
			#University
		WHERE
			UniversityIndex = @UniversityIndex;

		TRUNCATE TABLE #Remedial;

		INSERT INTO
			#Remedial
			(
				UniversityCode,
				CourseId,
				Cnt,
				Ttl,
				Rnk,
				IsLocked
			)
		SELECT
			c.UniversityCode,
			c.CourseId,
			Cnt = sum(case when r.CourseNum is null then 1 else 0 end),
			Ttl = sum(sum(case when r.CourseNum is null then 1 else 0 end)) over(),
			Rnk = case when r.CourseNum is not null then 1 end,
			IsLocked = case when r.CourseNum is not null then 1 else 0 end
		FROM
			MmapCsu.Transcript c
			left outer join
			MmapCsu.UnivRemedial r
				on r.UniversityCode = c.UniversityCode
				and r.CourseAbbr + r.CourseNum = c.CourseId
		WHERE
			c.UniversityCode = @UniversityCode
			and c.ContentAreaCode = @ContentAreaCode
		GROUP BY
			c.UniversityCode,
			c.CourseId,
			r.CourseNum;

		UPDATE
			#Remedial
		SET
			Pct = round(convert(decimal(5,2), 100.00 * Cnt / Ttl), 2);

		UPDATE
			t
		SET
			t.Rnk = t.rank_order
		FROM
			(
				SELECT
					Rnk,
					rank_order = 1 + rank() over(order by Cnt desc)
				FROM
					#Remedial
				WHERE
					IsLocked = 0
			) t
			
		DELETE
			r
		FROM
			(
				SELECT
					Rnk,
					Num = 1 + @Threshold
				FROM
					#Remedial
			) r
		WHERE
			Rnk > Num;

		WITH
			qry
			(
				CourseId
			)
		AS
			(
				SELECT
					CourseId = CourseId
				FROM
					#Remedial
			),
			base
			(
				CourseId,
				Seq,
				Ttl
			)
		AS
			(
				SELECT
					CourseId,
					row_number() over(order by CourseId),
					count(*) over()
				FROM
					qry
			),
			flat
			(
				FirstTermPivot,
				FirstTermAlias,
				SecondTermPivot,
				SecondTermAlias,
				Seq,
				Ttl
			)
		AS
			(
				SELECT
					convert(nvarchar(max), 'FirstTerm' + CourseId + N'=isnull(max(case when t.YearTermRank = 1 and t.CourseId = ''' + CourseId + ''' then t.Category end), -99)'),
					convert(nvarchar(max), 'FirstTerm' + CourseId),
					convert(nvarchar(max), 'SecondTerm' + CourseId + N'=isnull(max(case when t.YearTermRank = 2 and t.CourseId = ''' + CourseId + ''' then t.Category end), -99)'),
					convert(nvarchar(max), 'SecondTerm' + CourseId),
					Seq,
					Ttl
				FROM
					base
				WHERE
					Seq = 1
				UNION ALL
				SELECT
					f.FirstTermPivot + N',' + convert(nvarchar(max), 'FirstTerm' + CourseId + N'=isnull(max(case when t.YearTermRank = 1 and t.CourseId = ''' + CourseId + ''' then t.Category end), -99)'),
					f.FirstTermAlias + N',' + convert(nvarchar(max), 'FirstTerm' + CourseId),
					f.SecondTermPivot + N',' + convert(nvarchar(max), 'SecondTerm' + CourseId + N'=isnull(max(case when t.YearTermRank = 2 and t.CourseId = ''' + CourseId + ''' then t.Category end), -99)'),
					f.SecondTermAlias + N',' + convert(nvarchar(max), 'SecondTerm' + CourseId),
					b.Seq,
					b.Ttl
				FROM
					base b
					inner join
					flat f
						on b.Seq = f.Seq + 1
			)
		SELECT
			@FirstTermPivot = FirstTermPivot,
			@FirstTermAlias = FirstTermAlias,
			@SecondTermPivot = SecondTermPivot,
			@SecondTermAlias = SecondTermAlias
		FROM
			flat
		WHERE
			Seq = Ttl;
		
		SET @Sql = N'
			IF (object_id(''MmapCsu.' + @ContentAreaCode + @UniversityName + N''') is not null)
				BEGIN
					DROP TABLE MmapCsu.' + @ContentAreaCode + @UniversityName + N';
				END;';

		EXECUTE sp_executesql
			@Sql,
			N'@UniversityCode char(6),
				@ContentAreaCode varchar(4)',
			@UniversityCode = @UniversityCode,
			@ContentAreaCode = @ContentAreaCode;

		SET @Sql = N'
			SELECT
				c.Derkey1,
				c.MajorCode,
				c.IsUrm,
				c.IsHispanic,
				c.IsBlack,
				c.IsNativeAmerican,
				c.IsAsian,
				c.IsPacificIslander,
				c.IsWhite,
				c.Gender,
				c.FinancialAidCode,
				gpa.OverallCumulativeGradePointAverage,
				gpa.SubjectOverallCumulativeGradePointAverage,
				gpa.WithoutSubjectOverallCumulativeGradePointAverage,
				IsFrsl = null,
				HS12Rank = null,
				HSLastYearTermCode = null,
				HSContentAreaLastYearTermCode  = hs.LastYearTermCode,
				UNFirstYearTermCode            = c.FirstYearTermCode,
				UNContentAreaFirstYearTermCode = t.ContentAreaFirstYearTermCode,
				Remedial_1_MarkCategory        = isnull(hs.Remedial_1_MarkCategory, -99),
				Remedial_2_MarkCategory        = isnull(hs.Remedial_2_MarkCategory, -99),
				English09_1_MarkCategory       = isnull(hs.English09_1_MarkCategory, -99),
				English09_2_MarkCategory       = isnull(hs.English09_2_MarkCategory, -99),
				English10_1_MarkCategory       = isnull(hs.English10_1_MarkCategory, -99),
				English10_2_MarkCategory       = isnull(hs.English10_2_MarkCategory, -99),
				English11_1_MarkCategory       = isnull(hs.English11_1_MarkCategory, -99),
				English11_2_MarkCategory       = isnull(hs.English11_2_MarkCategory, -99),
				English12_1_MarkCategory       = isnull(hs.English12_1_MarkCategory, -99),
				English12_2_MarkCategory       = isnull(hs.English12_2_MarkCategory, -99),
				Expository_1_MarkCategory      = isnull(hs.Expository_1_MarkCategory, -99),
				Expository_2_MarkCategory      = isnull(hs.Expository_2_MarkCategory, -99),
				LanguageAP_1_MarkCateogory     = isnull(hs.LanguageAP_1_MarkCateogory, -99),
				LanguageAP_2_MarkCateogory     = isnull(hs.LanguageAP_2_MarkCateogory, -99),
				LiteratureAP_1_MarkCategory    = isnull(hs.LiteratureAP_1_MarkCategory, -99),
				LiteratureAP_2_MarkCategory    = isnull(hs.LiteratureAP_2_MarkCategory, -99),
				Rhetoric_1_MarkCategory        = isnull(hs.Rhetoric_1_MarkCategory, -99),
				Rhetoric_2_MarkCategory        = isnull(hs.Rhetoric_1_MarkCategory, -99),
				' + @FirstTermAlias + N',
				' + @SecondTermAlias + N'
			INTO
				MmapCsu.' + @ContentAreaCode + @UniversityName + N'
			FROM
				MmapCsu.Cohort c
				inner join
				(
					SELECT
						t.UniversityCode,
						t.Derkey1,
						t.ContentAreaFirstYearTermCode,
						' + @FirstTermPivot + N',
						' + @SecondTermPivot + N'
					FROM
						MmapCsu.Transcript t
					WHERE
						t.UniversityCode = @UniversityCode
						and t.ContentAreaCode in ('''', @ContentAreaCode)
					GROUP BY
						t.UniversityCode,
						t.Derkey1,
						t.ContentAreaFirstYearTermCode
				) t
					on c.UniversityCode = t.UniversityCode
					and c.Derkey1 = t.Derkey1
				inner join
				(
					SELECT
						Derkey1                     = rcc.InterSegmentKey,
						LastYearTermCode            = max(rcc.YearTermCode),
						Remedial_1_MarkCategory     = isnull(max(case when ContentCode = ''Remedial''    and ContentSelector = 1 then MarkCategory end), -99),
						Remedial_2_MarkCategory     = isnull(max(case when ContentCode = ''Remedial''    and ContentSelector = 2 then MarkCategory end), -99),
						English09_1_MarkCategory    = isnull(max(case when ContentCode = ''English09''   and ContentSelector = 1 then MarkCategory end), -99),
						English09_2_MarkCategory    = isnull(max(case when ContentCode = ''English09''   and ContentSelector = 2 then MarkCategory end), -99),
						English10_1_MarkCategory    = isnull(max(case when ContentCode = ''English10''   and ContentSelector = 1 then MarkCategory end), -99),
						English10_2_MarkCategory    = isnull(max(case when ContentCode = ''English10''   and ContentSelector = 2 then MarkCategory end), -99),
						English11_1_MarkCategory    = isnull(max(case when ContentCode = ''English11''   and ContentSelector = 1 then MarkCategory end), -99),
						English11_2_MarkCategory    = isnull(max(case when ContentCode = ''English11''   and ContentSelector = 2 then MarkCategory end), -99),
						English12_1_MarkCategory    = isnull(max(case when ContentCode = ''English12''   and ContentSelector = 1 then MarkCategory end), -99),
						English12_2_MarkCategory    = isnull(max(case when ContentCode = ''English12''   and ContentSelector = 2 then MarkCategory end), -99),
						Expository_1_MarkCategory   = isnull(max(case when ContentCode = ''Composition'' and ContentSelector = 1 then MarkCategory end), -99),
						Expository_2_MarkCategory   = isnull(max(case when ContentCode = ''Composition'' and ContentSelector = 2 then MarkCategory end), -99),
						LanguageAP_1_MarkCateogory  = isnull(max(case when ContentCode = ''EnglishAP''   and CourseCode = ''2170'' and ContentSelector = 1 then MarkCategory end), -99),
						LanguageAP_2_MarkCateogory  = isnull(max(case when ContentCode = ''EnglishAP''   and CourseCode = ''2170'' and ContentSelector = 2 then MarkCategory end), -99),
						LiteratureAP_1_MarkCategory = isnull(max(case when ContentCode = ''EnglishAP''   and CourseCode = ''2171'' and ContentSelector = 1 then MarkCategory end), -99),
						LiteratureAP_2_MarkCategory = isnull(max(case when ContentCode = ''EnglishAP''   and CourseCode = ''2171'' and ContentSelector = 2 then MarkCategory end), -99),
						Rhetoric_1_MarkCategory     = isnull(max(case when ContentCode = ''Rhetoric'' and ContentSelector = 1 then MarkCategory end), -99),
						Rhetoric_2_MarkCategory     = isnull(max(case when ContentCode = ''Rhetoric'' and ContentSelector = 2 then MarkCategory end), -99)
					FROM
						mmap.RetrospectiveCourseContent rcc
					WHERE
						rcc.DepartmentCode = 14
					GROUP BY
						rcc.InterSegmentKey
				) hs
					on hs.Derkey1 = c.Derkey1
				inner join
				(
					SELECT
						Derkey1 = InterSegmentKey,
						OverallCumulativeGradePointAverage = max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeGradePointAverage end),
						SubjectOverallCumulativeGradePointAverage = max(case when DepartmentCode = 14 and IsLast = 1 then CumulativeGradePointAverage end),
						WithoutSubjectOverallCumulativeGradePointAverage = 
							convert(
								decimal(9,3),
								isnull(
									case
										when max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeQualityPoints end) - max(case when DepartmentCode = 14 and IsLast = 1 then CumulativeQualityPoints end) <= 0 then 0
										else max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeQualityPoints end) - max(case when DepartmentCode = 14 and IsLast = 1 then CumulativeQualityPoints end)
									end
									/
									nullif(
										case
											when max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 14 and IsLast = 1 then CumulativeCreditAttempted end) <= 0 then 0
											else max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 14 and IsLast = 1 then CumulativeCreditAttempted end)
										end,
										0
									)
								,0)
							)
					FROM
						mmap.RetrospectivePerformance
					GROUP BY
						InterSegmentKey
				) gpa
					on gpa.Derkey1 = c.Derkey1
			WHERE
				hs.LastYearTermCode < c.FirstYearTermCode;'
				
			-- UPDATE
			-- 	target
			-- SET
			-- 	target.IsFrsl = source.IsFrsl
			-- FROM
			-- 	MmapCsu.' + @ContentAreaName + @UniversityName + N' target
			-- 	inner join
			-- 	(
			-- 		SELECT
			-- 			t.Derkey1,
			-- 			IsFrsl = max(case when p.ProgramCode in (''175'', ''181'', ''182'') then 1 else 0 end)
			-- 		FROM
			-- 			MmapCsu.' + @ContentAreaName + @UniversityName + N' t
			-- 			inner join
			-- 			K12PrgProd p
			-- 				on t.Derkey1 = p.Derkey1
			-- 		WHERE
			-- 			exists (
			-- 				SELECT
			-- 					1
			-- 				FROM
			-- 					K12StudentProd s
			-- 				WHERE
			-- 					s.School = p.School
			-- 					and s.LocStudentId = p.LocStudentId
			-- 					and s.AcYear = p.AcYear
			-- 					and s.GradeLevel in (''09'', ''10'', ''11'', ''12'')
			-- 			)
			-- 		GROUP BY
			-- 			t.Derkey1
			-- 	) source
			-- 		on target.Derkey1 = source.Derkey1;
				
			-- UPDATE
			-- 	target
			-- SET
			-- 	target.HS12Rank = source.HS12Rank
			-- FROM
			-- 	MmapCsu.' + @ContentAreaName + @UniversityName + N' target
			-- 	inner join
			-- 	(
			-- 		SELECT
			-- 			Derkey1,
			-- 			HS12Rank = max(c.IntraAreaRank)
			-- 		FROM
			-- 			MmapCsu.' + @ContentAreaName + @UniversityName + N' t
			-- 			cross apply
			-- 			dbo.HSTranscriptGet(t.Derkey1) hs
			-- 			inner join
			-- 			calpads.Course c
			-- 				on c.Code = hs.CourseCode
			-- 			inner join
			-- 			calpads.CourseContent cc
			-- 				on cc.CourseCode = c.Code
			-- 			inner join
			-- 			calpads.Content ccc
			-- 				on ccc.Code = cc.ContentCode
			-- 		WHERE
			-- 			hs.GradeCode = ''12''
			-- 			and cc.IsPrimary = 1
			-- 			and c.DepartmentCode = 14
			-- 		GROUP BY
			-- 			Derkey1
			-- 	) source
			-- 		on target.Derkey1 = source.Derkey1;

			EXECUTE sp_executesql
				@Sql,
				N'@UniversityCode char(6),
					@ContentAreaCode varchar(4)',
				@UniversityCode = @UniversityCode,
				@ContentAreaCode = @ContentAreaCode;
		
		SET @UniversityIndex += 1;
	END;
END;