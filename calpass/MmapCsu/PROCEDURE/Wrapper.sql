USE calpass;

GO

IF (object_id('MmapCsu.Wrapper') is not null)
	BEGIN
		DROP PROCEDURE MmapCsu.Wrapper;
	END;

GO

CREATE PROCEDURE
	MmapCsu.Wrapper
AS

	SET NOCOUNT ON;
	
BEGIN
	EXECUTE MmapCsu.CohortMerge;
	EXECUTE MmapCsu.TranscriptMerge;
	EXECUTE MmapCsu.EnglUniversityProcess;
	EXECUTE MmapCsu.MathUniversityProcess;
END;