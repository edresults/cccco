USE calpass;

GO

IF (object_id('MmapCsu.CohortMerge') is not null)
	BEGIN
		DROP PROCEDURE MmapCsu.CohortMerge;
	END;

GO

CREATE PROCEDURE
	MmapCsu.CohortMerge
	(
		@UniversityCode char(6) = null
	)
AS

	SET NOCOUNT ON;

BEGIN

	MERGE
		MmapCsu.Cohort t
	USING
		(
			SELECT DISTINCT
				s.Derkey1,
				UniversityCode = s.school,
				MajorCode = Major,
				IsUrm = 
					case
						when EthnicityHispanic = 'Y' then 1
						when EthnicityBlack = 'Y' then 1
						when EthnicityNativeAmerican = 'Y' then 1
						when EthnicityPacificIslander = 'Y' then 1
						else 0
					end,
				IsHispanic = 
					case
						when EthnicityHispanic = 'Y' then 1
						when EthnicityHispanic = 'N' then 0
					end,
				IsBlack = 
					case
						when EthnicityBlack = 'Y' then 1
						when EthnicityBlack = 'N' then 0
					end,
				IsNativeAmerican = 
					case
						when EthnicityNativeAmerican = 'Y' then 1
						when EthnicityNativeAmerican = 'N' then 0
					end,
				IsAsian = 
					case
						when EthnicityAsian = 'Y' then 1
						when EthnicityAsian = 'N' then 0
					end,
				IsPacificIslander = 
					case
						when EthnicityPacificIslander = 'Y' then 1
						when EthnicityPacificIslander = 'N' then 0
					end,
				IsWhite = 
					case
						when EthnicityWhite = 'Y' then 1
						when EthnicityWhite = 'N' then 0
					end,
				Gender,
				FinancialAidCode = FinancialAid,
				FirstYearTermCode = t.YearTermCode
			FROM
				dbo.UnivStudentProd s with(index(pk_UnivStudentProd))
				inner join
				dbo.UnivTerm t
					on t.TermCode = s.YrTerm
			WHERE
				-- from san bernardino, sacramento, east bay, northridge, monterey
				(
					(
						@UniversityCode is null
						and
						s.School in ('110510', '110617', '110574', '110608', '409698')
					)
					or
					s.school = @UniversityCode
				)
				-- first term of enrollment
				and t.YearTermCode = (
					SELECT
						min(t1.YearTermCode)
					FROM
						dbo.UnivStudentProd s1 with(index(pk_UnivStudentProd))
						inner join
						dbo.UnivTerm t1
							on t1.TermCode = s1.YrTerm
					WHERE
						s1.School = s.School
						and s1.StudentId = s.StudentId
				)
				-- must have course enrollment
				and exists (
					SELECT
						1
					FROM
						dbo.UnivCourseProd c with(index(pk_UnivCourseProd))
					WHERE
						c.School = s.School
						and c.StudentId = s.StudentId
						and c.YrTerm = s.YrTerm
				)
				-- no duplicate derkeys given studentId
				and not exists (
					SELECT
						0
					FROM
						dbo.UnivStudentProd s2 with(index(ix_UnivStudentProd__Derkey1))
					WHERE
						s2.Derkey1 = s.Derkey1
					GROUP BY
						s2.Derkey1
					HAVING
						count(distinct s2.StudentId) > 1
				)
				-- no students having enrolled in two or more universities
				and not exists (
					SELECT
						0
					FROM
						dbo.UnivStudentProd s3 with(index(ix_UnivStudentProd__Derkey1))
					WHERE
						s3.Derkey1 = s.Derkey1
					GROUP BY
						s3.Derkey1
					HAVING
						count(distinct s3.School) > 1
				)
		) s
	ON
		s.Derkey1 = t.Derkey1
	WHEN MATCHED THEN
		UPDATE SET
			t.UniversityCode = s.UniversityCode,
			t.IsUrm = s.IsUrm,
			t.MajorCode = s.MajorCode,
			t.IsHispanic = s.IsHispanic,
			t.IsBlack = s.IsBlack,
			t.IsNativeAmerican = s.IsNativeAmerican,
			t.IsAsian = s.IsAsian,
			t.IsPacificIslander = s.IsPacificIslander,
			t.IsWhite = s.IsWhite,
			t.Gender = s.Gender,
			t.FinancialAidCode = s.FinancialAidCode,
			t.FirstYearTermCode = s.FirstYearTermCode
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				Derkey1,
				UniversityCode,
				MajorCode,
				IsUrm,
				IsHispanic,
				IsBlack,
				IsNativeAmerican,
				IsAsian,
				IsPacificIslander,
				IsWhite,
				Gender,
				FinancialAidCode,
				FirstYearTermCode
			)
		VALUES
			(
				s.Derkey1,
				s.UniversityCode,
				s.MajorCode,
				s.IsUrm,
				s.IsHispanic,
				s.IsBlack,
				s.IsNativeAmerican,
				s.IsAsian,
				s.IsPacificIslander,
				s.IsWhite,
				s.Gender,
				s.FinancialAidCode,
				s.FirstYearTermCode
			);
END;