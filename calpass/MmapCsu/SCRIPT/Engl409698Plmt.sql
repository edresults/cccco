SELECT
	c.*,
	gpa.OverallCumulativeGradePointAverage,
	gpa.SubjectOverallCumulativeGradePointAverage,
	gpa.WithoutSubjectOverallCumulativeGradePointAverage,
	IsFrsl = null,
	HS12Rank = null,
	HSLastYearTermCode = null,
	HSContentAreaLastYearTermCode = hs.LastYearTermCode,
	Remedial_1_MarkCategory        = isnull(hs.Remedial_1_MarkCategory, -99),
	Remedial_2_MarkCategory        = isnull(hs.Remedial_2_MarkCategory, -99),
	English09_1_MarkCategory       = isnull(hs.English09_1_MarkCategory, -99),
	English09_2_MarkCategory       = isnull(hs.English09_2_MarkCategory, -99),
	English10_1_MarkCategory       = isnull(hs.English10_1_MarkCategory, -99),
	English10_2_MarkCategory       = isnull(hs.English10_2_MarkCategory, -99),
	English11_1_MarkCategory       = isnull(hs.English11_1_MarkCategory, -99),
	English11_2_MarkCategory       = isnull(hs.English11_2_MarkCategory, -99),
	English12_1_MarkCategory       = isnull(hs.English12_1_MarkCategory, -99),
	English12_2_MarkCategory       = isnull(hs.English12_2_MarkCategory, -99),
	Expository_1_MarkCategory      = isnull(hs.Expository_1_MarkCategory, -99),
	Expository_2_MarkCategory      = isnull(hs.Expository_2_MarkCategory, -99),
	LanguageAP_1_MarkCateogory     = isnull(hs.LanguageAP_1_MarkCateogory, -99),
	LanguageAP_2_MarkCateogory     = isnull(hs.LanguageAP_2_MarkCateogory, -99),
	LiteratureAP_1_MarkCategory    = isnull(hs.LiteratureAP_1_MarkCategory, -99),
	LiteratureAP_2_MarkCategory    = isnull(hs.LiteratureAP_2_MarkCategory, -99),
	Rhetoric_1_MarkCategory        = isnull(hs.Rhetoric_1_MarkCategory, -99),
	Rhetoric_2_MarkCategory        = isnull(hs.Rhetoric_1_MarkCategory, -99)
INTO
	MmapCsu.Engl409698Plmt
FROM
	MmapCsu.MontereyBayPlacement c
	inner join
	(
		SELECT
			Derkey1                     = rcc.InterSegmentKey,
			LastYearTermCode            = max(rcc.YearTermCode),
			Remedial_1_MarkCategory     = isnull(max(case when ContentCode = 'Remedial'    and ContentSelector = 1 then MarkCategory end), -99),
			Remedial_2_MarkCategory     = isnull(max(case when ContentCode = 'Remedial'    and ContentSelector = 2 then MarkCategory end), -99),
			English09_1_MarkCategory    = isnull(max(case when ContentCode = 'English09'   and ContentSelector = 1 then MarkCategory end), -99),
			English09_2_MarkCategory    = isnull(max(case when ContentCode = 'English09'   and ContentSelector = 2 then MarkCategory end), -99),
			English10_1_MarkCategory    = isnull(max(case when ContentCode = 'English10'   and ContentSelector = 1 then MarkCategory end), -99),
			English10_2_MarkCategory    = isnull(max(case when ContentCode = 'English10'   and ContentSelector = 2 then MarkCategory end), -99),
			English11_1_MarkCategory    = isnull(max(case when ContentCode = 'English11'   and ContentSelector = 1 then MarkCategory end), -99),
			English11_2_MarkCategory    = isnull(max(case when ContentCode = 'English11'   and ContentSelector = 2 then MarkCategory end), -99),
			English12_1_MarkCategory    = isnull(max(case when ContentCode = 'English12'   and ContentSelector = 1 then MarkCategory end), -99),
			English12_2_MarkCategory    = isnull(max(case when ContentCode = 'English12'   and ContentSelector = 2 then MarkCategory end), -99),
			Expository_1_MarkCategory   = isnull(max(case when ContentCode = 'Composition' and ContentSelector = 1 then MarkCategory end), -99),
			Expository_2_MarkCategory   = isnull(max(case when ContentCode = 'Composition' and ContentSelector = 2 then MarkCategory end), -99),
			LanguageAP_1_MarkCateogory  = isnull(max(case when ContentCode = 'EnglishAP'   and CourseCode = '2170' and ContentSelector = 1 then MarkCategory end), -99),
			LanguageAP_2_MarkCateogory  = isnull(max(case when ContentCode = 'EnglishAP'   and CourseCode = '2170' and ContentSelector = 2 then MarkCategory end), -99),
			LiteratureAP_1_MarkCategory = isnull(max(case when ContentCode = 'EnglishAP'   and CourseCode = '2171' and ContentSelector = 1 then MarkCategory end), -99),
			LiteratureAP_2_MarkCategory = isnull(max(case when ContentCode = 'EnglishAP'   and CourseCode = '2171' and ContentSelector = 2 then MarkCategory end), -99),
			Rhetoric_1_MarkCategory     = isnull(max(case when ContentCode = 'Rhetoric' and ContentSelector = 1 then MarkCategory end), -99),
			Rhetoric_2_MarkCategory     = isnull(max(case when ContentCode = 'Rhetoric' and ContentSelector = 2 then MarkCategory end), -99)
		FROM
			mmap.RetrospectiveCourseContent rcc
		WHERE
			rcc.DepartmentCode = 14
		GROUP BY
			rcc.InterSegmentKey
	) hs
		on hs.Derkey1 = c.InterSegmentKey
	inner join
	(
		SELECT
			Derkey1 = InterSegmentKey,
			OverallCumulativeGradePointAverage = max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeGradePointAverage end),
			SubjectOverallCumulativeGradePointAverage = max(case when DepartmentCode = 14 and IsLast = 1 then CumulativeGradePointAverage end),
			WithoutSubjectOverallCumulativeGradePointAverage = 
				convert(
					decimal(9,3),
					isnull(
						case
							when max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeQualityPoints end) - max(case when DepartmentCode = 14 and IsLast = 1 then CumulativeQualityPoints end) <= 0 then 0
							else max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeQualityPoints end) - max(case when DepartmentCode = 14 and IsLast = 1 then CumulativeQualityPoints end)
						end
						/
						nullif(
							case
								when max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 14 and IsLast = 1 then CumulativeCreditAttempted end) <= 0 then 0
								else max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 14 and IsLast = 1 then CumulativeCreditAttempted end)
							end,
							0
						)
					,0)
				)
		FROM
			mmap.RetrospectivePerformance
		GROUP BY
			InterSegmentKey
	) gpa
		on gpa.Derkey1 = c.InterSegmentKey;