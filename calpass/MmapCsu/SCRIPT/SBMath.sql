USE calpass;

GO

IF (object_id('MmapCsu.SBMath') is not null)
	BEGIN
		DROP TABLE MmapCsu.SBMath;
	END;

GO

SELECT
	r.derkey1,
	MajorCode,
	HSSubjLastYearTermCode = h.SubjLastYearTermCode,
	HSLastYearTermCode = h.LastYearTermCode,
	CCFirstYearTermCode = c.FirstYearTermCode,
	CCSubjFirstYearTermCode = c.SubjFirstYearTermCode,
	UNFirstYearTermCode = u.year_term_code,
	HSArithmetic = isnull(h.ArithmeticMarkCategory, -99),
	HSPreAlgebra = isnull(h.PreAlgebraMarkCategory, -99),
	HSAlgebraI = isnull(h.AlgebraIMarkCategory, -99),
	HSGeometry = isnull(h.GeometryMarkCategory, -99),
	HSAlgebraII = isnull(h.AlgebraIIMarkCategory, -99),
	HSStatistics = isnull(h.StatisticsMarkCategory, -99),
	HSTrigonometry = isnull(h.TrigonometryMarkCategory, -99),
	HSPreCalculus = isnull(h.PreCalculusMarkCategory, -99),
	HSCalculusI = isnull(h.CalculusIMarkCategory, -99),
	HSCalculusII = isnull(h.CalculusIIMarkCategory, -99),
	CCArithmetic = isnull(c.ArithmeticMarkCategory, -99),
	CCPreAlgebra = isnull(c.PreAlgebraMarkCategory, -99),
	CCAlgebraI = isnull(c.AlgebraIMarkCategory, -99),
	CCGeometry = isnull(c.GeometryMarkCategory, -99),
	CCAlgebraII = isnull(c.AlgebraIIMarkCategory, -99),
	CCCollegeAlgebra = isnull(c.CollegeAlgebraMarkCategory, -99),
	CCGEMath = isnull(c.GEMathMarkCategory, -99),
	CCStatistics = isnull(c.StatisticsMarkCategory, -99),
	CCPreCalculus = isnull(c.PreCalculusMarkCategory, -99),
	CCCalculusI = isnull(c.CalculusIMarkCategory, -99),
	CCCalculusII = isnull(c.CalculusIIMarkCategory, -99),
	MATH110 = isnull(u.MATH110, -99),
	MATH115 = isnull(u.MATH115, -99),
	MATH120 = isnull(u.MATH120, -99),
	MATH165 = isnull(u.MATH165, -99),
	MATH180 = isnull(u.MATH180, -99),
	MATH192 = isnull(u.MATH192, -99),
	MATH211 = isnull(u.MATH211, -99),
	MATH212 = isnull(u.MATH212, -99),
	MATH213 = isnull(u.MATH213, -99),
	MATH251 = isnull(u.MATH251, -99),
	MATH262 = isnull(u.MATH262, -99),
	MATH272 = isnull(u.MATH272, -99),
	MATH301 = isnull(u.MATH301, -99),
	MATH301A = isnull(u.MATH301A, -99),
	MATH301B = isnull(u.MATH301B, -99),
	MATH301C = isnull(u.MATH301C, -99),
	MATH302 = isnull(u.MATH302, -99),
	MATH305 = isnull(u.MATH305, -99),
	MATH308 = isnull(u.MATH308, -99),
	MATH75 = isnull(u.MATH75, -99),
	MATH75A = isnull(u.MATH75A, -99),
	MATH75B = isnull(u.MATH75B, -99),
	MATH75C = isnull(u.MATH75C, -99),
	MATH80 = isnull(u.MATH80, -99),
	MATH90 = isnull(u.MATH90, -99)
INTO
	MmapCsu.SBMath
FROM
	MmapCsu.Cohort r
	inner join
	MmapCsu.RetroMathCourse110510 u
	on r.Derkey1 = u.Derkey1
	cross apply
	dbo.HSMathematicsGet(u.Derkey1) h
	outer apply
	dbo.CCMathematicsGetFirst(u.Derkey1) c
WHERE
	h.LastYearTermCode < u.year_term_code
	and (
		c.FirstYearTermCode is null
		or
		c.FirstYearTermCode < u.year_term_code
	);