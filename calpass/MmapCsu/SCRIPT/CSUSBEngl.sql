USE MmapCsu;

GO

IF (object_id('dbo.CSUSBEngl') is not null)
	BEGIN
		DROP TABLE dbo.CSUSBEngl;
	END;

GO

SELECT
	derkey1,
	hs_engl09 = isnull(m1.Category,-99),
	hs_engl10 = isnull(m2.Category,-99),
	hs_engl11 = isnull(m3.Category,-99),
	hs_engl12 = isnull(m4.Category,-99),
	hs_langap = isnull(m5.Category,-99),
	hs_litap = isnull(m6.Category,-99),
	hs_rem = isnull(m7.Category,-99),
	hs_esl = isnull(m8.Category,-99),
	hs_expo = isnull(m9.Category,-99),
	cc_engl00 = isnull(m10.Category,-99),
	cc_engl01 = isnull(m11.Category,-99),
	cc_engl02 = isnull(m12.Category,-99),
	cc_engl03 = isnull(m13.Category,-99),
	cc_engl04 = isnull(m14.Category,-99),
	cc_engl05 = isnull(m15.Category,-99),
	ENG085A = isnull(ENG085A, -99),
	ENG101 = isnull(ENG101, -99),
	ENG102 = isnull(ENG102, -99),
	ENG103A = isnull(ENG103A, -99),
	ENG104A = isnull(ENG104A, -99),
	ENG105 = isnull(ENG105, -99),
	ENG107 = isnull(ENG107, -99),
	ENG110 = isnull(ENG110, -99),
	ENG111 = isnull(ENG111, -99),
	ENG170 = isnull(ENG170, -99),
	ENG301 = isnull(ENG301, -99),
	ENG301B = isnull(ENG301B, -99),
	ENG302 = isnull(ENG302, -99),
	ENG303 = isnull(ENG303, -99),
	ENG303B = isnull(ENG303B, -99),
	ENG306 = isnull(ENG306, -99),
	ENG311 = isnull(ENG311, -99),
	ENG314 = isnull(ENG314, -99),
	ENG318 = isnull(ENG318, -99),
	ENG320 = isnull(ENG320, -99),
	ENG327 = isnull(ENG327, -99),
	ENG95 = isnull(ENG95, -99)
INTO
	CSUSBEngl
FROM
	(
		SELECT
			a.derkey1,
			ENG085A,
			ENG101,
			ENG102,
			ENG103A,
			ENG104A,
			ENG105,
			ENG107,
			ENG110,
			ENG111,
			ENG170,
			ENG301,
			ENG301B,
			ENG302,
			ENG303,
			ENG303B,
			ENG306,
			ENG311,
			ENG314,
			ENG318,
			ENG320,
			ENG327,
			ENG95,
			hs_engl09 = 
				case
					when hs_09_course_id = '2130' then hs_09_course_grade
					when hs_10_course_id = '2130' then hs_10_course_grade
					when hs_11_course_id = '2130' then hs_11_course_grade
					when hs_12_course_id = '2130' then hs_12_course_grade
				end,
			hs_engl10 = 
				case
					when hs_09_course_id = '2131' then hs_09_course_grade
					when hs_10_course_id = '2131' then hs_10_course_grade
					when hs_11_course_id = '2131' then hs_11_course_grade
					when hs_12_course_id = '2131' then hs_12_course_grade
				end,
			hs_engl11 = 
				case
					when hs_09_course_id = '2132' then hs_09_course_grade
					when hs_10_course_id = '2132' then hs_10_course_grade
					when hs_11_course_id = '2132' then hs_11_course_grade
					when hs_12_course_id = '2132' then hs_12_course_grade
				end,
			hs_engl12 = 
				case
					when hs_09_course_id = '2133' then hs_09_course_grade
					when hs_10_course_id = '2133' then hs_10_course_grade
					when hs_11_course_id = '2133' then hs_11_course_grade
					when hs_12_course_id = '2133' then hs_12_course_grade
				end,
			hs_LangAP = 
				case
					when hs_09_course_id = '2170' then hs_09_course_grade
					when hs_10_course_id = '2170' then hs_10_course_grade
					when hs_11_course_id = '2170' then hs_11_course_grade
					when hs_12_course_id = '2170' then hs_12_course_grade
				end,
			hs_LitAP = 
				case
					when hs_09_course_id in ('2171','2172') then hs_09_course_grade
					when hs_10_course_id in ('2171','2172') then hs_10_course_grade
					when hs_11_course_id in ('2171','2172') then hs_11_course_grade
					when hs_12_course_id in ('2171','2172') then hs_12_course_grade
				end,
			hs_rem = 
				case
					when hs_09_course_id = '2100' then hs_09_course_grade
					when hs_10_course_id = '2100' then hs_10_course_grade
					when hs_11_course_id = '2100' then hs_11_course_grade
					when hs_12_course_id = '2100' then hs_12_course_grade
				end,
			hs_esl = 
				case
					when hs_09_course_id = '2110' then hs_09_course_grade
					when hs_10_course_id = '2110' then hs_10_course_grade
					when hs_11_course_id = '2110' then hs_11_course_grade
					when hs_12_course_id = '2110' then hs_12_course_grade
				end,
			hs_expo = 
				case
					when hs_09_course_id in ('2118','2114','2113') then hs_09_course_grade
					when hs_10_course_id in ('2118','2114','2113') then hs_10_course_grade
					when hs_11_course_id in ('2118','2114','2113') then hs_11_course_grade
					when hs_12_course_id in ('2118','2114','2113') then hs_12_course_grade
				end,
			cc_engl00 = case when cc_first_course_level_id = 'Y' then cc_first_course_grade_id end,
			cc_engl01 = case when cc_first_course_level_id = 'A' then cc_first_course_grade_id end,
			cc_engl02 = case when cc_first_course_level_id = 'B' then cc_first_course_grade_id end,
			cc_engl03 = case when cc_first_course_level_id = 'C' then cc_first_course_grade_id end,
			cc_engl04 = case when cc_first_course_level_id = 'D' then cc_first_course_grade_id end,
			cc_engl05 = case when cc_first_course_level_id = 'E' then cc_first_course_grade_id end
		FROM
			[pro-dat-sql-03].calpass.mmapcsu.RetroEnglCourse110510 a
			left outer join
			[pro-dat-sql-03].mmap.dbo.organization_engl c
				on c.derkey1 = a.derkey1
				and c.organization_code = '000'
			left outer join
			[pro-dat-sql-03].calpass.comis.Term t
				on t.TermCode = c.cc_first_term_id
				and t.TermCodeFull < 
					case
						when substring(a.year_term_code, 5, 1) = '1' then '7'
						when substring(a.year_term_code, 5, 1) = '2' then '1'
						when substring(a.year_term_code, 5, 1) = '3' then '3'
						when substring(a.year_term_code, 5, 1) = '4' then '5'
					end
	) a
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m1
		on hs_engl09 = m1.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m2
		on hs_engl10 = m2.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m3
		on hs_engl11 = m3.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m4
		on hs_engl12 = m4.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m5
		on hs_LangAP = m5.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m6
		on hs_LitAP = m6.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m7
		on hs_rem = m7.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m8
		on hs_esl = m8.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m9
		on hs_expo = m9.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m10
		on cc_engl00 = m10.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m11
		on cc_engl01 = m11.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m12
		on cc_engl02 = m12.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m13
		on cc_engl03 = m13.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m14
		on cc_engl04 = m14.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m15
		on cc_engl05 = m15.MarkCode