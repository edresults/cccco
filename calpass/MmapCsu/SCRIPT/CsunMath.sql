IF (object_id('mmapcsu.CsunMath') is not null)
	BEGIN
		DROP TABLE mmapcsu.CsunMath;
	END;

GO

CREATE TABLE
	mmapcsu.CsunMath
	(
		InterSegmentKey                                  binary(64)   not null,
		OverallCumulativeGradePointAverage               decimal(4,3)     null,
		SubjectCumulativeGradePointAverage               decimal(4,3)     null,
		WithoutSubjectOverallCumulativeGradePointAverage decimal(4,3)     null,
		AlgebraI_1_MarkCategory                          smallint         null,
		AlgebraI_2_MarkCategory                          smallint         null,
		AlgebraII_1_MarkCategory                         smallint         null,
		AlgebraII_2_MarkCategory                         smallint         null,
		Arithmetic_1_MarkCategory                        smallint         null,
		Arithmetic_2_MarkCategory                        smallint         null,
		Calculus_1_MarkCategory                          smallint         null,
		Calculus_2_MarkCategory                          smallint         null,
		CalculusAB_1_MarkCategory                        smallint         null,
		CalculusAB_2_MarkCategory                        smallint         null,
		CalculusBC_1_MarkCategory                        smallint         null,
		CalculusBC_2_MarkCategory                        smallint         null,
		Geometry_1_MarkCategory                          smallint         null,
		Geometry_2_MarkCategory                          smallint         null,
		PreAlgebra_1_MarkCategory                        smallint         null,
		PreAlgebra_2_MarkCategory                        smallint         null,
		PreCalculus_1_MarkCategory                       smallint         null,
		PreCalculus_2_MarkCategory                       smallint         null,
		Statistics_1_MarkCategory                        smallint         null,
		Statistics_2_MarkCategory                        smallint         null,
		StatisticsAP_1_MarkCategory                      smallint         null,
		StatisticsAP_2_MarkCategory                      smallint         null,
		Trigonometry_1_MarkCategory                      smallint         null,
		Trigonometry_2_MarkCategory                      smallint         null
	);

ALTER TABLE
	mmapcsu.CsunMath
ADD CONSTRAINT
	PK_CsunMath
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey
	);

GO

INSERT
	mmapcsu.CsunMath
	(
		InterSegmentKey,
		OverallCumulativeGradePointAverage,
		SubjectCumulativeGradePointAverage,
		WithoutSubjectOverallCumulativeGradePointAverage,
		AlgebraI_1_MarkCategory,
		AlgebraI_2_MarkCategory,
		AlgebraII_1_MarkCategory,
		AlgebraII_2_MarkCategory,
		Arithmetic_1_MarkCategory,
		Arithmetic_2_MarkCategory,
		Calculus_1_MarkCategory,
		Calculus_2_MarkCategory,
		CalculusAB_1_MarkCategory,
		CalculusAB_2_MarkCategory,
		CalculusBC_1_MarkCategory,
		CalculusBC_2_MarkCategory,
		Geometry_1_MarkCategory,
		Geometry_2_MarkCategory,
		PreAlgebra_1_MarkCategory,
		PreAlgebra_2_MarkCategory,
		PreCalculus_1_MarkCategory,
		PreCalculus_2_MarkCategory,
		Statistics_1_MarkCategory,
		Statistics_2_MarkCategory,
		StatisticsAP_1_MarkCategory,
		StatisticsAP_2_MarkCategory,
		Trigonometry_1_MarkCategory,
		Trigonometry_2_MarkCategory
	)
SELECT
	b.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage,
	-- high school
	AlgebraI_1_MarkCategory     = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'AlgebraI' then rcc.MarkCategory end), -99),
	AlgebraI_2_MarkCategory     = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'AlgebraI' then rcc.MarkCategory end), -99),
	AlgebraII_1_MarkCategory    = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'AlgebraII' then rcc.MarkCategory end), -99),
	AlgebraII_2_MarkCategory    = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'AlgebraII' then rcc.MarkCategory end), -99),
	Arithmetic_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Arithmetic' then rcc.MarkCategory end), -99),
	Arithmetic_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Arithmetic' then rcc.MarkCategory end), -99),
	Calculus_1_MarkCategory     = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Calculus' and rcc.CourseCode not in ('2480', '2481') then rcc.MarkCategory end), -99),
	Calculus_2_MarkCategory     = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Calculus' and rcc.CourseCode not in ('2480', '2481') then rcc.MarkCategory end), -99),
	CalculusAB_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2480' then rcc.MarkCategory end), -99),
	CalculusAB_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2480' then rcc.MarkCategory end), -99),
	CalculusBC_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2481' then rcc.MarkCategory end), -99),
	CalculusBC_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2481' then rcc.MarkCategory end), -99),
	Geometry_1_MarkCategory     = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Geometry' then rcc.MarkCategory end), -99),
	Geometry_2_MarkCategory     = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Geometry' then rcc.MarkCategory end), -99),
	PreAlgebra_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'PreAlgebra' then rcc.MarkCategory end), -99),
	PreAlgebra_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'PreAlgebra' then rcc.MarkCategory end), -99),
	PreCalculus_1_MarkCategory  = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'PreCalculus' then rcc.MarkCategory end), -99),
	PreCalculus_2_MarkCategory  = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'PreCalculus' then rcc.MarkCategory end), -99),
	Statistics_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Statistics' and rcc.CourseCode != '2483' then rcc.MarkCategory end), -99),
	Statistics_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Statistics' and rcc.CourseCode != '2483' then rcc.MarkCategory end), -99),
	StatisticsAP_1_MarkCategory = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Statistics' and rcc.CourseCode  = '2483' then rcc.MarkCategory end), -99),
	StatisticsAP_2_MarkCategory = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Statistics' and rcc.CourseCode  = '2483' then rcc.MarkCategory end), -99),
	Trigonometry_1_MarkCategory = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Trigonometry' then rcc.MarkCategory end), -99),
	Trigonometry_2_MarkCategory = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Trigonometry' then rcc.MarkCategory end), -99)
FROM
	(
		SELECT
			p.InterSegmentKey,
			OverallCumulativeGradePointAverage = max(case when DepartmentCode = '00' then CumulativeGradePointAverage end),
			SubjectCumulativeGradePointAverage = max(case when DepartmentCode = '18' then CumulativeGradePointAverage end),
			WithoutSubjectOverallCumulativeGradePointAverage = 
				convert(
					decimal(9,3),
					isnull(
						case
							when max(case when DepartmentCode = '00' then CumulativeQualityPoints end) - max(case when DepartmentCode = '18' then CumulativeQualityPoints end) <= 0 then 0
							else max(case when DepartmentCode = '00' then CumulativeQualityPoints end) - max(case when DepartmentCode = '18' then CumulativeQualityPoints end)
						end
						/
						nullif(
							case
								when max(case when DepartmentCode = '00' then CumulativeCreditAttempted end) - max(case when DepartmentCode = '18' then CumulativeCreditAttempted end) <= 0 then 0
								else max(case when DepartmentCode = '00' then CumulativeCreditAttempted end) - max(case when DepartmentCode = '18' then CumulativeCreditAttempted end)
							end,
							0
						)
					,0)
				)
		FROM
			mmapcsu.Prospective p
			inner join
			mmap.RetrospectivePerformance rp
				on  rp.InterSegmentKey = p.InterSegmentKey
		WHERE
			rp.DepartmentCode in ('00', '18')
			and rp.IsLast = 1
		GROUP BY
			p.InterSegmentKey
	) b
	inner join
	mmap.RetrospectiveCourseContent rcc
		on rcc.InterSegmentKey = b.InterSegmentKey
	inner join
	dbo.Student s
		on s.InterSegmentKey = b.InterSegmentKey
WHERE
	rcc.DepartmentCode = '18'
	and rcc.ContentSelector in (1,2)
	and s.IsGrade11SectionInclusive = 1
	and s.IsHighSchoolCollision = 0
GROUP BY
	b.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage;