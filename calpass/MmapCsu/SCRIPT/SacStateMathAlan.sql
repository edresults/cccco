USE MmapCsu;

GO

IF (object_id('dbo.SacStateMath') is not null)
	BEGIN
		DROP TABLE dbo.SacStateMath;
	END;

GO

SELECT
	derkey1,
	hs_stats_ap = isnull(m1.Category,-99),
	hs_alg1 = isnull(m2.Category,-99),
	hs_alg2 = isnull(m3.Category,-99),
	hs_trig = isnull(m4.Category,-99),
	hs_geom = isnull(m5.Category,-99),
	hs_prealg = isnull(m6.Category,-99),
	hs_precalc = isnull(m7.Category,-99),
	hs_calc = isnull(m8.Category,-99),
	hs_calc_ab = isnull(m9.Category,-99),
	hs_calc_bc = isnull(m10.Category,-99),
	cc_ge_math = isnull(m11.Category,-99),
	cc_statistic = isnull(m12.Category,-99),
	cc_coll_alg = isnull(m13.Category,-99),
	cc_pre_calc = isnull(m14.Category,-99),
	cc_calc_i = isnull(m15.Category,-99),
	cc_calc_ii = isnull(m16.Category,-99),
	cc_bus_math = isnull(m17.Category,-99),
	cc_diff_eq = isnull(m18.Category,-99),
	math001p = isnull(MATH001,-99),
	math009p = isnull(MATH009,-99),
	math011p = isnull(MATH011,-99),
	math015hp = isnull(MATH015H,-99),
	math017p = isnull(MATH017,-99),
	math024p = isnull(MATH024,-99),
	math026ap = isnull(MATH026A,-99),
	math026bp = isnull(MATH026B,-99),
	math029p = isnull(MATH029,-99),
	math029ap = isnull(MATH029A,-99),
	math030p = isnull(MATH030,-99),
	math031p = isnull(MATH031,-99),
	math032p = isnull(MATH032,-99),
	math035p = isnull(MATH035,-99),
	math045p = isnull(MATH045,-99),
	math101p = isnull(MATH101,-99),
	math107ap = isnull(MATH107A,-99),
	math107bp = isnull(MATH107B,-99),
	math108p = isnull(MATH108,-99),
	stat001p = isnull(STAT001,-99),
	stat050p = isnull(STAT050,-99),
	stat096ap = isnull(STAT096A,-99)
INTO
	SacStateMath
FROM
	(
		SELECT
			a.derkey1,
			MATH001,
			MATH009,
			MATH011,
			MATH015H,
			MATH017,
			MATH024,
			MATH026A,
			MATH026B,
			MATH029,
			MATH029A,
			MATH030,
			MATH031,
			MATH032,
			MATH035,
			MATH045,
			MATH101,
			MATH107A,
			MATH107B,
			MATH108,
			STAT001,
			STAT050,
			STAT096A,
			hs_stats_ap = 
				case
					when hs_09_course_id = '2483' then hs_09_course_grade
					when hs_10_course_id = '2483' then hs_10_course_grade
					when hs_11_course_id = '2483' then hs_11_course_grade
					when hs_12_course_id = '2483' then hs_12_course_grade
				end,
			hs_alg1 = 
				case
					when hs_09_course_id = '2403' then hs_09_course_grade
					when hs_10_course_id = '2403' then hs_10_course_grade
					when hs_11_course_id = '2403' then hs_11_course_grade
					when hs_12_course_id = '2403' then hs_12_course_grade
				end,
			hs_alg2 = 
				case
					when hs_09_course_id = '2404' then hs_09_course_grade
					when hs_10_course_id = '2404' then hs_10_course_grade
					when hs_11_course_id = '2404' then hs_11_course_grade
					when hs_12_course_id = '2404' then hs_12_course_grade
				end,
			hs_trig = 
				case
					when hs_09_course_id = '2407' then hs_09_course_grade
					when hs_10_course_id = '2407' then hs_10_course_grade
					when hs_11_course_id = '2407' then hs_11_course_grade
					when hs_12_course_id = '2407' then hs_12_course_grade
				end,
			hs_geom = 
				case
					when hs_09_course_id = '2413' then hs_09_course_grade
					when hs_10_course_id = '2413' then hs_10_course_grade
					when hs_11_course_id = '2413' then hs_11_course_grade
					when hs_12_course_id = '2413' then hs_12_course_grade
				end,
			hs_prealg = 
				case
					when hs_09_course_id = '2424' then hs_09_course_grade
					when hs_10_course_id = '2424' then hs_10_course_grade
					when hs_11_course_id = '2424' then hs_11_course_grade
					when hs_12_course_id = '2424' then hs_12_course_grade
				end,
			hs_precalc = 
				case
					when hs_09_course_id = '2414' then hs_09_course_grade
					when hs_10_course_id = '2414' then hs_10_course_grade
					when hs_11_course_id = '2414' then hs_11_course_grade
					when hs_12_course_id = '2414' then hs_12_course_grade
				end,
			hs_calc = 
				case
					when hs_09_course_id = '2415' then hs_09_course_grade
					when hs_10_course_id = '2415' then hs_10_course_grade
					when hs_11_course_id = '2415' then hs_11_course_grade
					when hs_12_course_id = '2415' then hs_12_course_grade
				end,
			hs_calc_ab =
				case
					when hs_09_course_id = '2480' then hs_09_course_grade
					when hs_10_course_id = '2480' then hs_10_course_grade
					when hs_11_course_id = '2480' then hs_11_course_grade
					when hs_12_course_id = '2480' then hs_12_course_grade
				end,
			hs_calc_bc = 
				case
					when hs_09_course_id = '2481' then hs_09_course_grade
					when hs_10_course_id = '2481' then hs_10_course_grade
					when hs_11_course_id = '2481' then hs_11_course_grade
					when hs_12_course_id = '2481' then hs_12_course_grade
				end,
			cc_ge_math = case when cc_ge_math = 1 then cc_first_course_grade_id end,
			cc_statistic = case when cc_statistics = 1 then cc_first_course_grade_id end,
			cc_coll_alg = case when cc_coll_alg = 1 then cc_first_course_grade_id end,
			cc_pre_calc = case when cc_pre_calc = 1 then cc_first_course_grade_id end,
			cc_calc_i = case when cc_calc_i = 1 then cc_first_course_grade_id end,
			cc_calc_ii = case when cc_calc_ii = 1 then cc_first_course_grade_id end,
			cc_bus_math = case when cc_bus_math = 1 then cc_first_course_grade_id end,
			cc_diff_eq = case when cc_diff_eq = 1 then cc_first_course_grade_id end
		FROM
			[pro-dat-sql-03].calpass.mmapcsu.RetroMathCourse110617 a
			left outer join
			[pro-dat-sql-03].mmap.dbo.organization_math c
				on c.derkey1 = a.derkey1
				and c.organization_code = '000'
			left outer join
			[pro-dat-sql-03].calpass.comis.Term t
				on t.TermCode = c.cc_first_term_id
				and t.TermCodeFull < 
					case
						when substring(a.year_term_code, 5, 1) = '1' then '7'
						when substring(a.year_term_code, 5, 1) = '2' then '1'
						when substring(a.year_term_code, 5, 1) = '3' then '3'
						when substring(a.year_term_code, 5, 1) = '4' then '5'
					end
	) a
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m1
		on hs_stats_ap = m1.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m2
		on hs_alg1 = m2.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m3
		on hs_alg2 = m3.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m4
		on hs_trig = m4.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m5
		on hs_geom = m5.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m6
		on hs_prealg = m6.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m7
		on hs_precalc = m7.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m8
		on hs_calc = m8.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m9
		on hs_calc_ab = m9.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m10
		on hs_calc_bc = m10.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m11
		on cc_ge_math = m11.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m12
		on cc_statistic = m12.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m13
		on cc_coll_alg = m13.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m14
		on cc_pre_calc = m14.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m15
		on cc_calc_i = m15.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m16
		on cc_calc_ii = m16.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m17
		on cc_bus_math = m17.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m18
		on cc_diff_eq = m18.MarkCode