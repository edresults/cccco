DECLARE
	@Algorithm char(8) = 'SHA2_512';

MERGE
	MmapCsu.Prospective t
USING
	(
		SELECT
			InterSegmentKey,
			NameFirst,
			NameLast,
			BirthDate,
			GenderCode,
			HighSchool,
			MajorCode,
			PlacementLocalEnglId,
			PlacementLocalMathId
		FROM
			(
				SELECT
					InterSegmentKey,
					NameFirst,
					NameLast,
					BirthDate,
					GenderCode,
					HighSchool,
					MajorCode,
					PlacementLocalEnglId,
					PlacementLocalMathId,
					Selector = row_number() over(partition by InterSegmentKey, HighSchool order by (select 1))
				FROM
					(
						SELECT
							InterSegmentKey = 
								HASHBYTES(
									@Algorithm,
									upper(convert(nchar(3), NameFirst)) + 
									upper(convert(nchar(3), NameLast)) + 
									upper(convert(nchar(1), GenderCode)) + 
									convert(nchar(8), Birthdate)
								),
							NameFirst,
							NameLast,
							BirthDate,
							GenderCode,
							HighSchool = o.CDS_CODE_14,
							MajorCode,
							PlacementLocalEnglId,
							PlacementLocalMathId
						FROM
							OPENROWSET(
								BULK
								N'C:\Data\CSU\DATA\cor janet calpass itr file.txt',
								FORMATFILE = 'C:\Data\CSU\FORMAT\Prospective.fmt',
								FIRSTROW = 2
							) a
							left outer join
							MmapCsu.Organization o
								on o.ORG_ID = a.HighSchool
					) b
			) c
		WHERE
			Selector = 1
	) s
ON
	(
		t.InterSegmentKey = s.InterSegmentKey
	)
WHEN MATCHED THEN
	UPDATE SET
		t.HighSchool           = s.HighSchool,
		t.MajorCode            = s.MajorCode,
		t.PlacementLocalEnglId = s.PlacementLocalEnglId,
		t.PlacementLocalMathId = s.PlacementLocalMathId
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			InterSegmentKey,
			NameFirst,
			NameLast,
			BirthDate,
			GenderCode,
			HighSchool,
			MajorCode,
			PlacementLocalEnglId,
			PlacementLocalMathId
		)
	VALUES
		(
			InterSegmentKey,
			NameFirst,
			NameLast,
			BirthDate,
			GenderCode,
			HighSchool,
			MajorCode,
			PlacementLocalEnglId,
			PlacementLocalMathId
		);