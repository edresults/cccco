USE calpass;

GO

IF (object_id('MmapCsu.SSMath') is not null)
	BEGIN
		DROP TABLE MmapCsu.SSMath;
	END;

GO

SELECT
	r.derkey1,
	MajorCode,
	IsUrm,
	IsHispanic,
	IsBlack,
	IsNativeAmerican,
	IsAsian,
	IsPacificIslander,
	IsWhite,
	Gender,
	FinancialAidCode,
	FRSL = null,
	HSSubjLastYearTermCode = h.SubjLastYearTermCode,
	HSLastYearTermCode = h.LastYearTermCode,
	CCFirstYearTermCode = c.FirstYearTermCode,
	CCSubjFirstYearTermCode = c.SubjFirstYearTermCode,
	UNFirstYearTermCode = u.year_term_code,
	HSArithmetic = isnull(h.ArithmeticMarkCategory, -99),
	HSPreAlgebra = isnull(h.PreAlgebraMarkCategory, -99),
	HSAlgebraI = isnull(h.AlgebraIMarkCategory, -99),
	HSGeometry = isnull(h.GeometryMarkCategory, -99),
	HSAlgebraII = isnull(h.AlgebraIIMarkCategory, -99),
	HSStatistics = isnull(h.StatisticsMarkCategory, -99),
	HSTrigonometry = isnull(h.TrigonometryMarkCategory, -99),
	HSPreCalculus = isnull(h.PreCalculusMarkCategory, -99),
	HSCalculusI = isnull(h.CalculusIMarkCategory, -99),
	HSCalculusII = isnull(h.CalculusIIMarkCategory, -99),
	CCArithmetic = isnull(c.ArithmeticMarkCategory, -99),
	CCPreAlgebra = isnull(c.PreAlgebraMarkCategory, -99),
	CCAlgebraI = isnull(c.AlgebraIMarkCategory, -99),
	CCGeometry = isnull(c.GeometryMarkCategory, -99),
	CCAlgebraII = isnull(c.AlgebraIIMarkCategory, -99),
	CCCollegeAlgebra = isnull(c.CollegeAlgebraMarkCategory, -99),
	CCGEMath = isnull(c.GEMathMarkCategory, -99),
	CCStatistics = isnull(c.StatisticsMarkCategory, -99),
	CCPreCalculus = isnull(c.PreCalculusMarkCategory, -99),
	CCCalculusI = isnull(c.CalculusIMarkCategory, -99),
	CCCalculusII = isnull(c.CalculusIIMarkCategory, -99),
	MATH001 = isnull(MATH001, -99),
	MATH009 = isnull(MATH009, -99),
	MATH011 = isnull(MATH011, -99),
	MATH015H = isnull(MATH015H, -99),
	MATH017 = isnull(MATH017, -99),
	MATH024 = isnull(MATH024, -99),
	MATH026A = isnull(MATH026A, -99),
	MATH026B = isnull(MATH026B, -99),
	MATH029 = isnull(MATH029, -99),
	MATH029A = isnull(MATH029A, -99),
	MATH030 = isnull(MATH030, -99),
	MATH031 = isnull(MATH031, -99),
	MATH032 = isnull(MATH032, -99),
	MATH035 = isnull(MATH035, -99),
	MATH045 = isnull(MATH045, -99),
	MATH101 = isnull(MATH101, -99),
	MATH107A = isnull(MATH107A, -99),
	MATH107B = isnull(MATH107B, -99),
	MATH108 = isnull(MATH108, -99),
	STAT001 = isnull(STAT001, -99),
	STAT050 = isnull(STAT050, -99),
	STAT096A = isnull(STAT096A, -99)
INTO
	MmapCsu.SSMath
FROM
	MmapCsu.Cohort r
	inner join
	MmapCsu.RetroMathCourse110617 u
	on r.Derkey1 = u.Derkey1
	cross apply
	dbo.HSMathematicsGet(u.Derkey1) h
	outer apply
	dbo.CCMathematicsGetFirst(u.Derkey1) c
WHERE
	h.LastYearTermCode < u.year_term_code
	and (
		c.FirstYearTermCode is null
		or
		c.FirstYearTermCode < u.year_term_code
	);