USE calpass;

GO

IF (object_id('tempdb..#TeacherTermEval') is not null)
	BEGIN
		DROP TABLE #TeacherTermEval;
	END;

GO

CREATE TABLE
	#TeacherTermEval
	(
		Ssid varchar(10),
		Semester1 varchar(10),
		Semester2 varchar(10)
	);

GO

INSERT INTO
	#TeacherTermEval
	(
		Ssid,
		Semester1,
		Semester2
	)
SELECT
	s.ssid,
	semester1 = max(case when marking_period_code = 'S1' then c.seid end),
	semester2 = max(case when marking_period_code = 'S2' then c.seid end)
FROM
	calpads.scsc s
	inner join
	calpads.crsc c
		on s.district_code = c.district_code
		and s.school_code = c.school_code
		and s.year_code = c.year_code
		and s.course_id = c.course_id
		and s.section_id = c.section_id
WHERE
	c.course_code = '2404' -- algebgra II
	and s.marking_period_code in ('S1', 'S2')
GROUP BY
	s.ssid;

SELECT
	sameFrq = same,
	samePct = round(convert(decimal(5,2), 100.00 * same / (same + diff)), 2),
	diffFrq = diff,
	diffPct = round(convert(decimal(5,2), 100.00 * diff / (same + diff)), 2),
	total = same + diff
FROM
	(
		SELECT
			same = sum(case when Semester1 = Semester2 then 1 else 0 end),
			diff = sum(case when Semester1 != Semester2 then 1 else 0 end)
		FROM
			#TeacherTermEval
		WHERE
			Semester1 is not null
			and Semester2 is not null
	) a;