USE calpass;

GO

IF (object_id('MmapCsu.SBEngl') is not null)
	BEGIN
		DROP TABLE MmapCsu.SBEngl;
	END;

GO

SELECT
	r.derkey1,
	MajorCode,
	HSSubjLastYearTermCode = h.SubjLastYearTermCode,
	HSLastYearTermCode = h.LastYearTermCode,
	CCFirstYearTermCode = c.FirstYearTermCode,
	CCSubjFirstYearTermCode = c.SubjFirstYearTermCode,
	UNFirstYearTermCode = u.year_term_code,
	RemedialMarkCategory = isnull(RemedialMarkCategory, -99),
	English09MarkCategory = isnull(English09MarkCategory, -99),
	English10MarkCategory = isnull(English10MarkCategory, -99),
	English11MarkCategory = isnull(English11MarkCategory, -99),
	English12MarkCategory = isnull(English12MarkCategory, -99),
	ExpositoryMarkCategory = isnull(ExpositoryMarkCategory, -99),
	LiteratureAPMarkCategory = isnull(LiteratureAPMarkCategory, -99),
	EnglishAPMarkCategory = isnull(EnglishAPMarkCategory, -99),
	Level0MarkCategory = isnull(Level0MarkCategory, -99),
	Level1MarkCategory = isnull(Level1MarkCategory, -99),
	Level2MarkCategory = isnull(Level2MarkCategory, -99),
	Level3MarkCategory = isnull(Level3MarkCategory, -99),
	Level4MarkCategory = isnull(Level4MarkCategory, -99),
	Level5MarkCategory = isnull(Level5MarkCategory, -99),
	ENG085A = isnull(ENG085A, -99),
	ENG100 = isnull(ENG100, -99),
	ENG101 = isnull(ENG101, -99),
	ENG102 = isnull(ENG102, -99),
	ENG103 = isnull(ENG103, -99),
	ENG103A = isnull(ENG103A, -99),
	ENG104A = isnull(ENG104A, -99),
	ENG104B = isnull(ENG104B, -99),
	ENG105 = isnull(ENG105, -99),
	ENG106 = isnull(ENG106, -99),
	ENG107 = isnull(ENG107, -99),
	ENG110 = isnull(ENG110, -99),
	ENG111 = isnull(ENG111, -99),
	ENG170 = isnull(ENG170, -99),
	ENG210 = isnull(ENG210, -99),
	ENG212 = isnull(ENG212, -99),
	ENG301 = isnull(ENG301, -99),
	ENG301B = isnull(ENG301B, -99),
	ENG302 = isnull(ENG302, -99),
	ENG303 = isnull(ENG303, -99),
	ENG303A = isnull(ENG303A, -99),
	ENG303B = isnull(ENG303B, -99),
	ENG306 = isnull(ENG306, -99),
	ENG311 = isnull(ENG311, -99),
	ENG314 = isnull(ENG314, -99),
	ENG317 = isnull(ENG317, -99),
	ENG318 = isnull(ENG318, -99),
	ENG320 = isnull(ENG320, -99),
	ENG323 = isnull(ENG323, -99),
	ENG327 = isnull(ENG327, -99),
	ENG336 = isnull(ENG336, -99),
	ENG85B = isnull(ENG85B, -99),
	ENG95 = isnull(ENG95, -99)
INTO
	MmapCsu.SBEngl
FROM
	MmapCsu.Cohort r
	inner join
	MmapCsu.RetroEnglCourse110510 u
	on r.Derkey1 = u.Derkey1
	cross apply
	dbo.HSEnglishGet(u.Derkey1) h
	outer apply
	dbo.CCEnglishGetFirst(u.Derkey1) c
WHERE
	h.LastYearTermCode < u.year_term_code
	and (
		c.FirstYearTermCode is null
		or
		c.FirstYearTermCode < u.year_term_code
	);