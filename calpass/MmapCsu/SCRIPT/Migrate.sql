DROP TABLE calpass.mmapcsu.engl110617;
DROP TABLE calpass.mmapcsu.engl110574;
DROP TABLE calpass.mmapcsu.engl110510;
DROP TABLE calpass.mmapcsu.engl110608;

GO

SELECT * INTO calpass.mmapcsu.engl110617 FROM [PRO-DAT-SQL-03].calpass.mmapcsu.engl110617;
SELECT * INTO calpass.mmapcsu.engl110574 FROM [PRO-DAT-SQL-03].calpass.mmapcsu.engl110574;
SELECT * INTO calpass.mmapcsu.engl110510 FROM [PRO-DAT-SQL-03].calpass.mmapcsu.engl110510;
SELECT * INTO calpass.mmapcsu.engl110608 FROM [PRO-DAT-SQL-03].calpass.mmapcsu.engl110608;

GO

DROP TABLE calpass.mmapcsu.math110617;
DROP TABLE calpass.mmapcsu.math110574;
DROP TABLE calpass.mmapcsu.math110510;
DROP TABLE calpass.mmapcsu.math110608;

GO

SELECT * INTO calpass.mmapcsu.math110617 FROM [PRO-DAT-SQL-03].calpass.mmapcsu.math110617;
SELECT * INTO calpass.mmapcsu.math110574 FROM [PRO-DAT-SQL-03].calpass.mmapcsu.math110574;
SELECT * INTO calpass.mmapcsu.math110510 FROM [PRO-DAT-SQL-03].calpass.mmapcsu.math110510;
SELECT * INTO calpass.mmapcsu.math110608 FROM [PRO-DAT-SQL-03].calpass.mmapcsu.math110608;