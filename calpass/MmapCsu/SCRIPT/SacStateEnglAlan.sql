USE MmapCsu;

GO

IF (object_id('dbo.SacStateEngl') is not null)
	BEGIN
		DROP TABLE dbo.SacStateEngl;
	END;

GO

SELECT
	derkey1,
	hs_engl09 = isnull(m1.Category,-99),
	hs_engl10 = isnull(m2.Category,-99),
	hs_engl11 = isnull(m3.Category,-99),
	hs_engl12 = isnull(m4.Category,-99),
	hs_langap = isnull(m5.Category,-99),
	hs_litap = isnull(m6.Category,-99),
	hs_rem = isnull(m7.Category,-99),
	hs_esl = isnull(m8.Category,-99),
	hs_expo = isnull(m9.Category,-99),
	cc_engl00 = isnull(m10.Category,-99),
	cc_engl01 = isnull(m11.Category,-99),
	cc_engl02 = isnull(m12.Category,-99),
	cc_engl03 = isnull(m13.Category,-99),
	cc_engl04 = isnull(m14.Category,-99),
	cc_engl05 = isnull(m15.Category,-99),
	engl001 = isnull(engl001,-99),
	engl001a = isnull(engl001a,-99),
	engl002 = isnull(engl002,-99),
	engl005 = isnull(engl005,-99),
	engl010 = isnull(engl010,-99),
	engl010m = isnull(engl010m,-99),
	engl011 = isnull(engl011,-99),
	engl015 = isnull(engl015,-99),
	engl016 = isnull(engl016,-99),
	engl020 = isnull(engl020,-99),
	engl040a = isnull(engl040a,-99),
	engl040b = isnull(engl040b,-99),
	engl050a = isnull(engl050a,-99),
	engl050b = isnull(engl050b,-99),
	engl065 = isnull(engl065,-99),
	engl086 = isnull(engl086,-99),
	engl087 = isnull(engl087,-99),
	engl109e = isnull(engl109e,-99),
	engl109m = isnull(engl109m,-99),
	engl109w = isnull(engl109w,-99),
	engl109x = isnull(engl109x,-99),
	engl110a = isnull(engl110a,-99),
	engl110j = isnull(engl110j,-99),
	engl116a = isnull(engl116a,-99),
	engl116b = isnull(engl116b,-99),
	engl180h = isnull(engl180h,-99),
	engl191a = isnull(engl191a,-99)
INTO
	SacStateEngl
FROM
	(
		SELECT
			a.derkey1,
			ENGL001,
			ENGL001A,
			ENGL002,
			ENGL005,
			ENGL010,
			ENGL010M,
			ENGL011,
			ENGL015,
			ENGL016,
			ENGL020,
			ENGL040A,
			ENGL040B,
			ENGL050A,
			ENGL050B,
			ENGL065,
			ENGL086,
			ENGL087,
			ENGL109E,
			ENGL109M,
			ENGL109W,
			ENGL109X,
			ENGL110A,
			ENGL110J,
			ENGL116A,
			ENGL116B,
			ENGL180H,
			ENGL191A,
			hs_engl09 = 
				case
					when hs_09_course_id = '2130' then hs_09_course_grade
					when hs_10_course_id = '2130' then hs_10_course_grade
					when hs_11_course_id = '2130' then hs_11_course_grade
					when hs_12_course_id = '2130' then hs_12_course_grade
				end,
			hs_engl10 = 
				case
					when hs_09_course_id = '2131' then hs_09_course_grade
					when hs_10_course_id = '2131' then hs_10_course_grade
					when hs_11_course_id = '2131' then hs_11_course_grade
					when hs_12_course_id = '2131' then hs_12_course_grade
				end,
			hs_engl11 = 
				case
					when hs_09_course_id = '2132' then hs_09_course_grade
					when hs_10_course_id = '2132' then hs_10_course_grade
					when hs_11_course_id = '2132' then hs_11_course_grade
					when hs_12_course_id = '2132' then hs_12_course_grade
				end,
			hs_engl12 = 
				case
					when hs_09_course_id = '2133' then hs_09_course_grade
					when hs_10_course_id = '2133' then hs_10_course_grade
					when hs_11_course_id = '2133' then hs_11_course_grade
					when hs_12_course_id = '2133' then hs_12_course_grade
				end,
			hs_LangAP = 
				case
					when hs_09_course_id = '2170' then hs_09_course_grade
					when hs_10_course_id = '2170' then hs_10_course_grade
					when hs_11_course_id = '2170' then hs_11_course_grade
					when hs_12_course_id = '2170' then hs_12_course_grade
				end,
			hs_LitAP = 
				case
					when hs_09_course_id in ('2171','2172') then hs_09_course_grade
					when hs_10_course_id in ('2171','2172') then hs_10_course_grade
					when hs_11_course_id in ('2171','2172') then hs_11_course_grade
					when hs_12_course_id in ('2171','2172') then hs_12_course_grade
				end,
			hs_rem = 
				case
					when hs_09_course_id = '2100' then hs_09_course_grade
					when hs_10_course_id = '2100' then hs_10_course_grade
					when hs_11_course_id = '2100' then hs_11_course_grade
					when hs_12_course_id = '2100' then hs_12_course_grade
				end,
			hs_esl = 
				case
					when hs_09_course_id = '2110' then hs_09_course_grade
					when hs_10_course_id = '2110' then hs_10_course_grade
					when hs_11_course_id = '2110' then hs_11_course_grade
					when hs_12_course_id = '2110' then hs_12_course_grade
				end,
			hs_expo = 
				case
					when hs_09_course_id in ('2118','2114','2113') then hs_09_course_grade
					when hs_10_course_id in ('2118','2114','2113') then hs_10_course_grade
					when hs_11_course_id in ('2118','2114','2113') then hs_11_course_grade
					when hs_12_course_id in ('2118','2114','2113') then hs_12_course_grade
				end,
			cc_engl00 = case when cc_first_course_level_id = 'Y' then cc_first_course_grade_id end,
			cc_engl01 = case when cc_first_course_level_id = 'A' then cc_first_course_grade_id end,
			cc_engl02 = case when cc_first_course_level_id = 'B' then cc_first_course_grade_id end,
			cc_engl03 = case when cc_first_course_level_id = 'C' then cc_first_course_grade_id end,
			cc_engl04 = case when cc_first_course_level_id = 'D' then cc_first_course_grade_id end,
			cc_engl05 = case when cc_first_course_level_id = 'E' then cc_first_course_grade_id end
		FROM
			[pro-dat-sql-03].calpass.mmapcsu.RetroEnglCourse110617 a
			left outer join
			[pro-dat-sql-03].mmap.dbo.organization_engl c
				on c.derkey1 = a.derkey1
				and c.organization_code = '000'
			left outer join
			[pro-dat-sql-03].calpass.comis.Term t
				on t.TermCode = c.cc_first_term_id
				and t.TermCodeFull < 
					case
						when substring(a.year_term_code, 5, 1) = '1' then '7'
						when substring(a.year_term_code, 5, 1) = '2' then '1'
						when substring(a.year_term_code, 5, 1) = '3' then '3'
						when substring(a.year_term_code, 5, 1) = '4' then '5'
					end
	) a
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m1
		on hs_engl09 = m1.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m2
		on hs_engl10 = m2.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m3
		on hs_engl11 = m3.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m4
		on hs_engl12 = m4.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m5
		on hs_LangAP = m5.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m6
		on hs_LitAP = m6.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m7
		on hs_rem = m7.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m8
		on hs_esl = m8.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m9
		on hs_expo = m9.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m10
		on cc_engl00 = m10.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m11
		on cc_engl01 = m11.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m12
		on cc_engl02 = m12.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m13
		on cc_engl03 = m13.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m14
		on cc_engl04 = m14.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m15
		on cc_engl05 = m15.MarkCode