IF (object_id('mmapcsu.SacStateEnglCohort') is not null)
	BEGIN
		DROP TABLE mmapcsu.SacStateEnglCohort;
	END;

GO

SELECT
	u.derkey1,
	CC_ENGL_01 = isnull(case when c.course_college_level = 'A' then m2.Category end, -99),
	CC_ENGL_00 = isnull(case when c.course_college_level = 'Y' then m2.Category end, -99),
	CC_EQUIV_ENGL005 = isnull(case when a.UniversityCourseId = 'ENGL005' then m2.Category end, -99),
	CC_EQUIV_ENGL005M = isnull(case when a.UniversityCourseId = 'ENGL005M' then m2.Category end, -99),
	CC_EQUIV_ENGL020 = isnull(case when a.UniversityCourseId = 'ENGL020' then m2.Category end, -99),
	UN_ENGL001 = isnull(case when u.course_id = 'ENGL001' then m1.Category end, -99),
	UN_ENGL001A = isnull(case when u.course_id = 'ENGL001A' then m1.Category end, -99),
	UN_ENGL002 = isnull(case when u.course_id = 'ENGL002' then m1.Category end, -99),
	UN_ENGL005 = isnull(case when u.course_id = 'ENGL005' then m1.Category end, -99),
	UN_ENGL010 = isnull(case when u.course_id = 'ENGL010' then m1.Category end, -99),
	UN_ENGL010M = isnull(case when u.course_id = 'ENGL010M' then m1.Category end, -99),
	UN_ENGL011 = isnull(case when u.course_id = 'ENGL011' then m1.Category end, -99),
	UN_ENGL015 = isnull(case when u.course_id = 'ENGL015' then m1.Category end, -99),
	UN_ENGL016 = isnull(case when u.course_id = 'ENGL016' then m1.Category end, -99),
	UN_ENGL020 = isnull(case when u.course_id = 'ENGL020' then m1.Category end, -99),
	UN_ENGL040A = isnull(case when u.course_id = 'ENGL040A' then m1.Category end, -99),
	UN_ENGL040B = isnull(case when u.course_id = 'ENGL040B' then m1.Category end, -99),
	UN_ENGL050A = isnull(case when u.course_id = 'ENGL050A' then m1.Category end, -99),
	UN_ENGL050B = isnull(case when u.course_id = 'ENGL050B' then m1.Category end, -99),
	UN_ENGL065 = isnull(case when u.course_id = 'ENGL065' then m1.Category end, -99),
	UN_ENGL086 = isnull(case when u.course_id = 'ENGL086' then m1.Category end, -99),
	UN_ENGL087 = isnull(case when u.course_id = 'ENGL087' then m1.Category end, -99),
	UN_ENGL109E = isnull(case when u.course_id = 'ENGL109E' then m1.Category end, -99),
	UN_ENGL109M = isnull(case when u.course_id = 'ENGL109M' then m1.Category end, -99),
	UN_ENGL109W = isnull(case when u.course_id = 'ENGL109W' then m1.Category end, -99),
	UN_ENGL109X = isnull(case when u.course_id = 'ENGL109X' then m1.Category end, -99),
	UN_ENGL110A = isnull(case when u.course_id = 'ENGL110A' then m1.Category end, -99),
	UN_ENGL110J = isnull(case when u.course_id = 'ENGL110J' then m1.Category end, -99),
	UN_ENGL116A = isnull(case when u.course_id = 'ENGL116A' then m1.Category end, -99),
	UN_ENGL116B = isnull(case when u.course_id = 'ENGL116B' then m1.Category end, -99),
	UN_ENGL180H = isnull(case when u.course_id = 'ENGL180H' then m1.Category end, -99),
	UN_ENGL191A = isnull(case when u.course_id = 'ENGL191A' then m1.Category end, -99),
	hs_ind = e.derkey1
INTO
	calpass.mmapcsu.SacStateEnglCohort
FROM
	mmapcsu.UnivTranscript u
	cross apply
	dbo.f_cc_student_subject_first(u.Derkey1, '150100') c
	inner join
	comis.Term ct
		on ct.TermCode = c.organization_term_code
	inner join
	dbo.UnivTerm ut
		on ut.TermCode = u.year_term_code
	inner join
	calpads.Mark m1
		on m1.MarkCode = u.section_grade
	inner join
	calpads.Mark m2
		on m2.MarkCode = c.section_grade
	left outer join
	dbo.UnivArticulation a
		on a.CollegeCode = c.organization_comis_code
		and a.CollegeCourseId = 
			replace(
			replace(
			replace(
			replace(
			replace(
				c.course_id,
				' ', ''),
				'-', ''),
				'*', ''),
				' ', ''),
				'ISH', '')
	left outer join
	mmap.dbo.organization_engl e
		on e.derkey1 = u.derkey1
WHERE
	u.college_code = '110617'
	and u.SubjectCode = 'ENGL'
	and ct.TermCodeFull < ut.TermCodeFullComis
	and exists (
		SELECT
			1
		FROM
			dbo.UnivStudentProd xfer
		WHERE
			xfer.Derkey1 = u.Derkey1
			and xfer.School = u.college_code
			and xfer.EnrollStatus in ('3', '5')
	)