USE MmapCsu;

GO

IF (object_id('dbo.EastBayMath') is not null)
	BEGIN
		DROP TABLE dbo.EastBayMath;
	END;

GO

SELECT
	derkey1,
	hs_stats_ap = isnull(m1.Category,-99),
	hs_alg1 = isnull(m2.Category,-99),
	hs_alg2 = isnull(m3.Category,-99),
	hs_trig = isnull(m4.Category,-99),
	hs_geom = isnull(m5.Category,-99),
	hs_prealg = isnull(m6.Category,-99),
	hs_precalc = isnull(m7.Category,-99),
	hs_calc = isnull(m8.Category,-99),
	hs_calc_ab = isnull(m9.Category,-99),
	hs_calc_bc = isnull(m10.Category,-99),
	cc_ge_math = isnull(m11.Category,-99),
	cc_statistic = isnull(m12.Category,-99),
	cc_coll_alg = isnull(m13.Category,-99),
	cc_pre_calc = isnull(m14.Category,-99),
	cc_calc_i = isnull(m15.Category,-99),
	cc_calc_ii = isnull(m16.Category,-99),
	cc_bus_math = isnull(m17.Category,-99),
	cc_diff_eq = isnull(m18.Category,-99),
	MATH1110 = isnull(MATH1110,-99),
	MATH1130 = isnull(MATH1130,-99),
	MATH1300 = isnull(MATH1300,-99),
	MATH1304 = isnull(MATH1304,-99),
	MATH1305 = isnull(MATH1305,-99),
	MATH1810 = isnull(MATH1810,-99),
	MATH2011 = isnull(MATH2011,-99),
	MATH2101 = isnull(MATH2101,-99),
	MATH2150 = isnull(MATH2150,-99),
	MATH4012 = isnull(MATH4012,-99),
	MATH800 = isnull(MATH800,-99),
	MATH801 = isnull(MATH801,-99),
	MATH805 = isnull(MATH805,-99),
	MATH806 = isnull(MATH806,-99),
	MATH900 = isnull(MATH900,-99),
	MATH950 = isnull(MATH950,-99),
	STAT1000 = isnull(STAT1000,-99),
	STAT2010 = isnull(STAT2010,-99),
	STAT3010 = isnull(STAT3010,-99),
	STAT3031 = isnull(STAT3031,-99),
	STAT3050 = isnull(STAT3050,-99),
	STAT3401 = isnull(STAT3401,-99),
	STAT6204 = isnull(STAT6204,-99),
	STAT6304 = isnull(STAT6304,-99)
INTO
	EastBayMath
FROM
	(
		SELECT
			a.derkey1,
			hs_stats_ap = 
				case
					when hs_09_course_id = '2483' then hs_09_course_grade
					when hs_10_course_id = '2483' then hs_10_course_grade
					when hs_11_course_id = '2483' then hs_11_course_grade
					when hs_12_course_id = '2483' then hs_12_course_grade
				end,
			hs_alg1 = 
				case
					when hs_09_course_id = '2403' then hs_09_course_grade
					when hs_10_course_id = '2403' then hs_10_course_grade
					when hs_11_course_id = '2403' then hs_11_course_grade
					when hs_12_course_id = '2403' then hs_12_course_grade
				end,
			hs_alg2 = 
				case
					when hs_09_course_id = '2404' then hs_09_course_grade
					when hs_10_course_id = '2404' then hs_10_course_grade
					when hs_11_course_id = '2404' then hs_11_course_grade
					when hs_12_course_id = '2404' then hs_12_course_grade
				end,
			hs_trig = 
				case
					when hs_09_course_id = '2407' then hs_09_course_grade
					when hs_10_course_id = '2407' then hs_10_course_grade
					when hs_11_course_id = '2407' then hs_11_course_grade
					when hs_12_course_id = '2407' then hs_12_course_grade
				end,
			hs_geom = 
				case
					when hs_09_course_id = '2413' then hs_09_course_grade
					when hs_10_course_id = '2413' then hs_10_course_grade
					when hs_11_course_id = '2413' then hs_11_course_grade
					when hs_12_course_id = '2413' then hs_12_course_grade
				end,
			hs_prealg = 
				case
					when hs_09_course_id = '2424' then hs_09_course_grade
					when hs_10_course_id = '2424' then hs_10_course_grade
					when hs_11_course_id = '2424' then hs_11_course_grade
					when hs_12_course_id = '2424' then hs_12_course_grade
				end,
			hs_precalc = 
				case
					when hs_09_course_id = '2414' then hs_09_course_grade
					when hs_10_course_id = '2414' then hs_10_course_grade
					when hs_11_course_id = '2414' then hs_11_course_grade
					when hs_12_course_id = '2414' then hs_12_course_grade
				end,
			hs_calc = 
				case
					when hs_09_course_id = '2415' then hs_09_course_grade
					when hs_10_course_id = '2415' then hs_10_course_grade
					when hs_11_course_id = '2415' then hs_11_course_grade
					when hs_12_course_id = '2415' then hs_12_course_grade
				end,
			hs_calc_ab =
				case
					when hs_09_course_id = '2480' then hs_09_course_grade
					when hs_10_course_id = '2480' then hs_10_course_grade
					when hs_11_course_id = '2480' then hs_11_course_grade
					when hs_12_course_id = '2480' then hs_12_course_grade
				end,
			hs_calc_bc = 
				case
					when hs_09_course_id = '2481' then hs_09_course_grade
					when hs_10_course_id = '2481' then hs_10_course_grade
					when hs_11_course_id = '2481' then hs_11_course_grade
					when hs_12_course_id = '2481' then hs_12_course_grade
				end,
			cc_ge_math = case when cc_ge_math = 1 then cc_first_course_grade_id end,
			cc_statistic = case when cc_statistics = 1 then cc_first_course_grade_id end,
			cc_coll_alg = case when cc_coll_alg = 1 then cc_first_course_grade_id end,
			cc_pre_calc = case when cc_pre_calc = 1 then cc_first_course_grade_id end,
			cc_calc_i = case when cc_calc_i = 1 then cc_first_course_grade_id end,
			cc_calc_ii = case when cc_calc_ii = 1 then cc_first_course_grade_id end,
			cc_bus_math = case when cc_bus_math = 1 then cc_first_course_grade_id end,
			cc_diff_eq = case when cc_diff_eq = 1 then cc_first_course_grade_id end,
			MATH1110,
			MATH1130,
			MATH1300,
			MATH1304,
			MATH1305,
			MATH1810,
			MATH2011,
			MATH2101,
			MATH2150,
			MATH4012,
			MATH800,
			MATH801,
			MATH805,
			MATH806,
			MATH900,
			MATH950,
			STAT1000,
			STAT2010,
			STAT3010,
			STAT3031,
			STAT3050,
			STAT3401,
			STAT6204,
			STAT6304
		FROM
			[pro-dat-sql-03].calpass.mmapcsu.RetroMathCourse110574 a
			left outer join
			[pro-dat-sql-03].mmap.dbo.organization_math c
				on c.derkey1 = a.derkey1
				and c.organization_code = '000'
			left outer join
			[pro-dat-sql-03].calpass.comis.Term t
				on t.TermCode = c.cc_first_term_id
				and t.TermCodeFull < 
					case
						when substring(a.year_term_code, 5, 1) = '1' then '7'
						when substring(a.year_term_code, 5, 1) = '2' then '1'
						when substring(a.year_term_code, 5, 1) = '3' then '3'
						when substring(a.year_term_code, 5, 1) = '4' then '5'
					end
	) a
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m1
		on hs_stats_ap = m1.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m2
		on hs_alg1 = m2.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m3
		on hs_alg2 = m3.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m4
		on hs_trig = m4.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m5
		on hs_geom = m5.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m6
		on hs_prealg = m6.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m7
		on hs_precalc = m7.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m8
		on hs_calc = m8.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m9
		on hs_calc_ab = m9.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m10
		on hs_calc_bc = m10.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m11
		on cc_ge_math = m11.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m12
		on cc_statistic = m12.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m13
		on cc_coll_alg = m13.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m14
		on cc_pre_calc = m14.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m15
		on cc_calc_i = m15.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m16
		on cc_calc_ii = m16.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m17
		on cc_bus_math = m17.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m18
		on cc_diff_eq = m18.MarkCode