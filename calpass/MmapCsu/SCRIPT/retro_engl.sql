USE calpass;

GO

IF (object_id('mmapcsu.chrt') is not null)
	BEGIN
		DROP TABLE mmapcsu.chrt;
	END;

GO

SELECT DISTINCT
	UniversityIpedsCode = s.school,
	s.Derkey1,
	urm_ind = case
			when EthnicityHispanic = 'Y' then 1
			when EthnicityBlack = 'Y' then 1
			when EthnicityNativeAmerican = 'Y' then 1
			when EthnicityPacificIslander = 'Y' then 1
			else 0
		end,
	ethnicity_hispanic = EthnicityHispanic,
	ethnicity_black = EthnicityBlack,
	ethnicity_native = EthnicityNativeAmerican,
	ethnicity_asian  = EthnicityAsian,
	ethnicity_pacific = EthnicityPacificIslander,
	ethnicity_white = EthnicityWhite,
	gender,
	fincancial_aid_code = FinancialAid
INTO
	mmapcsu.chrt
FROM
	dbo.UnivStudentProd s
WHERE
	-- from san bernardino, sacramento, east bay
	s.school in ('110510', '110617', '110574')
	-- first term of enrollment
	and s.YrTerm = (
		SELECT
			min(s1.YrTerm)
		FROM
			dbo.UnivStudentProd s1
		WHERE
			s1.School = s.School
			and s1.StudentId = s.StudentId
	)
	-- must have course enrollment
	and exists (
		SELECT
			1
		FROM
			dbo.UnivCourseProd c
		WHERE
			c.School = s.School
			and c.StudentId = s.StudentId
			and c.YrTerm = s.YrTerm
	)
	-- no duplicate derkeys given studentId
	and not exists (
		SELECT
			0
		FROM
			dbo.UnivStudentProd s2
		WHERE
			s2.Derkey1 = s.Derkey1
		GROUP BY
			s2.Derkey1
		HAVING
			count(distinct s2.StudentId) > 1
	);

-- remove students with enrollments at multiple schools
DELETE
	s
FROM
	mmapcsu.chrt s
WHERE
	exists (
		SELECT
			0
		FROM
			mmapcsu.chrt s1
		WHERE
			s1.Derkey1 = s.Derkey1
		GROUP BY
			s1.Derkey1
		HAVING
			count(distinct s1.UniversityIpedsCode) > 1
	);

TRUNCATE TABLE mmapcsu.hs_subj;

INSERT INTO
	mmapcsu.hs_subj
	(
		Derkey1,
		SubjectCode,
		hs_09_grade_level,
		hs_09_school_code,
		hs_09_year_code,
		hs_09_term_code,
		hs_09_course_id,
		hs_09_course_title,
		hs_09_course_level_code,
		hs_09_course_type_code,
		hs_09_section_grade,
		hs_09_section_grade_points,
		hs_09_section_success_ind,
		hs_09_gpa_cum,
		hs_10_grade_level,
		hs_10_school_code,
		hs_10_year_code,
		hs_10_term_code,
		hs_10_course_id,
		hs_10_course_title,
		hs_10_course_level_code,
		hs_10_course_type_code,
		hs_10_section_grade,
		hs_10_section_grade_points,
		hs_10_section_success_ind,
		hs_10_gpa_cum,
		hs_11_grade_level,
		hs_11_school_code,
		hs_11_year_code,
		hs_11_term_code,
		hs_11_course_id,
		hs_11_course_title,
		hs_11_course_level_code,
		hs_11_course_type_code,
		hs_11_section_grade,
		hs_11_section_grade_points,
		hs_11_section_success_ind,
		hs_11_gpa_cum,
		hs_12_grade_level,
		hs_12_school_code,
		hs_12_year_code,
		hs_12_term_code,
		hs_12_course_id,
		hs_12_course_title,
		hs_12_course_level_code,
		hs_12_course_type_code,
		hs_12_section_grade,
		hs_12_section_grade_points,
		hs_12_section_success_ind,
		hs_12_gpa_cum,
		engl_eap_ind,
		engl_scaled_score,
		math_subject,
		math_eap_ind,
		math_scaled_score,
		esl_ind
	)
SELECT
	c.Derkey1,
	s.subject_code,
	s.hs_09_grade_level,
	s.hs_09_school_code,
	s.hs_09_year_code,
	s.hs_09_term_code,
	s.hs_09_course_id,
	s.hs_09_course_title,
	s.hs_09_course_level_code,
	s.hs_09_course_type_code,
	hs_09_section_grade = s.hs_09_course_grade,
	hs_09_section_grade_points = s.hs_09_course_grade_points,
	hs_09_section_success_ind = s.hs_09_course_success_ind,
	hs_09_gpa_cum = g.hs_09_gpa,
	s.hs_10_grade_level,
	s.hs_10_school_code,
	s.hs_10_year_code,
	s.hs_10_term_code,
	s.hs_10_course_id,
	s.hs_10_course_title,
	s.hs_10_course_level_code,
	s.hs_10_course_type_code,
	hs_10_section_grade = s.hs_10_course_grade,
	hs_10_section_grade_points = s.hs_10_course_grade_points,
	hs_10_section_success_ind = s.hs_10_course_success_ind,
	hs_10_gpa_cum = g.hs_10_gpa,
	s.hs_11_grade_level,
	s.hs_11_school_code,
	s.hs_11_year_code,
	s.hs_11_term_code,
	s.hs_11_course_id,
	s.hs_11_course_title,
	s.hs_11_course_level_code,
	s.hs_11_course_type_code,
	hs_11_section_grade = s.hs_11_course_grade,
	hs_11_section_grade_points = s.hs_11_course_grade_points,
	hs_11_section_success_ind = s.hs_11_course_success_ind,
	hs_11_gpa_cum = g.hs_11_gpa,
	s.hs_12_grade_level,
	s.hs_12_school_code,
	s.hs_12_year_code,
	s.hs_12_term_code,
	s.hs_12_course_id,
	s.hs_12_course_title,
	s.hs_12_course_level_code,
	s.hs_12_course_type_code,
	hs_12_section_grade = s.hs_12_course_grade,
	hs_12_section_grade_points = s.hs_12_course_grade_points,
	hs_12_section_success_ind = s.hs_12_course_success_ind,
	hs_12_gpa_cum = g.hs_12_gpa,
	a.engl_eap_ind,
	a.engl_scaled_score,
	a.math_subject,
	a.math_eap_ind,
	a.math_scaled_score,
	esl_ind = case
			when a.esl_el_ind = 1 or e.gen_esl_any = 1 then 1
			when a.esl_el_ind = 0 then 0
			when e.gen_esl_any = 0 then 0
		end
FROM
	mmapcsu.chrt c
	inner join
	mmap.k12.student_gpa g
		on c.Derkey1 = g.Derkey1
	inner join
	mmap.k12.student_subj s
		on g.Derkey1 = s.Derkey1
	left outer join
	mmap.assess.student_star a
		on a.Derkey1 = g.Derkey1
	left outer join
	mmap.k12.prospective_subj_esl e
		on g.Derkey1 = e.Derkey1;

GO

/*
	UNIVERSITY
*/


TRUNCATE TABLE mmapcsu.UnivTranscript;

GO

INSERT INTO
	mmapcsu.UnivTranscript
	(
		Derkey1,
		SubjectCode,
		college_code,
		course_level_id,
		year_code,
		year_term_code,
		term_ordinal,
		course_id,
		course_title,
		course_units_attempted,
		section_grade,
		section_grade_points,
		section_grade_points_categorical,
		section_success_ind
	)
SELECT DISTINCT
	stu.Derkey1,
	SubjectCode = case
			when crs.CourseAbbr in ('ENG', 'ENGL') then 'ENGL'
			when crs.CourseAbbr in ('MATH', 'MLSK') then 'MATH'
		end,
	college_code = stu.School,
	course_level_id = isnull(rem.LevelBelow, 0),
	year_code = t.AcademicYear,
	year_term_code = stu.YrTerm,
	term_ordinal = t.Ordinal,
	course_id = crs.CourseAbbr + crs.CourseNum,
	course_title = crs.CourseTitle,
	course_units_attempted = crs.Units,
	section_grade = crs.Grade,
	section_grade_points = g.points,
	section_grade_points_categorical = g.Category,
	section_success_ind = g.success_ind
FROM
	mmapcsu.chrt crt
	inner join
	dbo.UnivStudentProd stu
		on crt.UniversityIpedsCode = stu.school
		and crt.Derkey1 = stu.Derkey1
	inner join
	dbo.UnivCourseProd crs
		on stu.School = crs.School
		and stu.YrTerm = crs.YrTerm
		and stu.StudentId = crs.StudentId
	inner join
	dbo.UnivTerm t
		on t.TermCode = stu.YrTerm
	inner join
	comis.Grade g
		on g.GradeCode = crs.Grade
	left outer join
	MmapCsu.UnivRemedial rem
		on rem.CollegeCode = crs.school
		and rem.CourseAbbr = crs.CourseAbbr
		and rem.CourseNum = crs.CourseNum
WHERE
	crs.CourseAbbr in ('ENG', 'ENGL', 'MATH', 'MLSK');

-- remove duplicates within SubjectCode by course level
DELETE
	s1
FROM
	(
		SELECT
			del_ind = row_number() over(
				partition by 
					s.Derkey1,
					s.SubjectCode,
					s.college_code,
					s.course_level_id
				order by
					s.year_code asc,
					s.term_ordinal asc,
					s.course_units_attempted desc,
					s.section_grade_points desc
			)
		FROM
			mmapcsu.UnivTranscript s
	) s1
WHERE
	s1.del_ind > 1;

GO

TRUNCATE TABLE mmapcsu.csu_subj;

GO

-- populate csu subj
INSERT INTO
	mmapcsu.csu_subj
	(
		derkey1,
		SubjectCode,
		csu_00_course_level_id,
		csu_00_year_code,
		csu_00_year_term_code,
		csu_00_term_ordinal,
		csu_00_course_id,
		csu_00_course_title,
		csu_00_course_units_attempted,
		csu_00_section_grade,
		csu_00_section_grade_points,
		csu_00_section_grade_points_categorical,
		csu_00_section_success_ind,
		csu_01_course_level_id,
		csu_01_year_code,
		csu_01_year_term_code,
		csu_01_term_ordinal,
		csu_01_course_id,
		csu_01_course_title,
		csu_01_course_units_attempted,
		csu_01_section_grade,
		csu_01_section_grade_points,
		csu_01_section_grade_points_categorical,
		csu_01_section_success_ind,
		csu_02_course_level_id,
		csu_02_year_code,
		csu_02_year_term_code,
		csu_02_term_ordinal,
		csu_02_course_id,
		csu_02_course_title,
		csu_02_course_units_attempted,
		csu_02_section_grade,
		csu_02_section_grade_points,
		csu_02_section_grade_points_categorical,
		csu_02_section_success_ind,
		csu_03_course_level_id,
		csu_03_year_code,
		csu_03_year_term_code,
		csu_03_term_ordinal,
		csu_03_course_id,
		csu_03_course_title,
		csu_03_course_units_attempted,
		csu_03_section_grade,
		csu_03_section_grade_points,
		csu_03_section_grade_points_categorical,
		csu_03_section_success_ind
	)
SELECT
	Derkey1,
	SubjectCode,
	max(case when course_level_id = 0 then course_level_id end),
	max(case when course_level_id = 0 then year_code end),
	max(case when course_level_id = 0 then year_term_code end),
	max(case when course_level_id = 0 then term_ordinal end),
	max(case when course_level_id = 0 then course_id end),
	max(case when course_level_id = 0 then course_title end),
	max(case when course_level_id = 0 then course_units_attempted end),
	max(case when course_level_id = 0 then section_grade end),
	max(case when course_level_id = 0 then section_grade_points end),
	max(case when course_level_id = 0 then section_grade_points_categorical end),
	max(case when course_level_id = 0 then convert(int, section_success_ind) end),
	max(case when course_level_id = 1 then course_level_id end),
	max(case when course_level_id = 1 then year_code end),
	max(case when course_level_id = 1 then year_term_code end),
	max(case when course_level_id = 1 then term_ordinal end),
	max(case when course_level_id = 1 then course_id end),
	max(case when course_level_id = 1 then course_title end),
	max(case when course_level_id = 1 then course_units_attempted end),
	max(case when course_level_id = 1 then section_grade end),
	max(case when course_level_id = 1 then section_grade_points end),
	max(case when course_level_id = 1 then section_grade_points_categorical end),
	max(case when course_level_id = 1 then convert(int, section_success_ind) end),
	max(case when course_level_id = 2 then course_level_id end),
	max(case when course_level_id = 2 then year_code end),
	max(case when course_level_id = 2 then year_term_code end),
	max(case when course_level_id = 2 then term_ordinal end),
	max(case when course_level_id = 2 then course_id end),
	max(case when course_level_id = 2 then course_title end),
	max(case when course_level_id = 2 then course_units_attempted end),
	max(case when course_level_id = 2 then section_grade end),
	max(case when course_level_id = 2 then section_grade_points end),
	max(case when course_level_id = 2 then section_grade_points_categorical end),
	max(case when course_level_id = 2 then convert(int, section_success_ind) end),
	max(case when course_level_id = 3 then course_level_id end),
	max(case when course_level_id = 3 then year_code end),
	max(case when course_level_id = 3 then year_term_code end),
	max(case when course_level_id = 3 then term_ordinal end),
	max(case when course_level_id = 3 then course_id end),
	max(case when course_level_id = 3 then course_title end),
	max(case when course_level_id = 3 then course_units_attempted end),
	max(case when course_level_id = 3 then section_grade end),
	max(case when course_level_id = 3 then section_grade_points end),
	max(case when course_level_id = 3 then section_grade_points_categorical end),
	max(case when course_level_id = 3 then convert(int, section_success_ind) end)
FROM
	mmapcsu.UnivTranscript
GROUP BY
	Derkey1,
	SubjectCode;

-- remove non-firstime within SubjectCode
DELETE
	s1
FROM
	(
		SELECT
			del_ind = row_number() over(
				partition by 
					s.Derkey1,
					s.SubjectCode,
					s.college_code
				order by
					s.year_code asc,
					s.term_ordinal asc,
					s.course_level_id desc
			)
		FROM
			mmapcsu.UnivTranscript s
	) s1
WHERE
	s1.del_ind > 1;

-- update first attmept within SubjectCode
UPDATE
	t
SET
	csu_first_course_level_id = course_level_id,
	csu_first_year_code = year_code,
	csu_first_year_term_code = year_term_code,
	csu_first_term_ordinal = term_ordinal,
	csu_first_course_id = course_id,
	csu_first_course_title = course_title,
	csu_first_course_units_attempted = course_units_attempted,
	csu_first_section_grade = section_grade,
	csu_first_section_grade_points = section_grade_points,
	csu_first_section_grade_points_categorical = section_grade_points_categorical,
	csu_first_section_success_ind = section_success_ind
FROM
	mmapcsu.csu_subj t
	inner join
	mmapcsu.UnivTranscript s
		on s.derkey1 = t.derkey1
		and s.SubjectCode = t.SubjectCode;

IF (object_id('mmapcsu.retro_engl') is not null)
	BEGIN
		DROP TABLE mmapcsu.retro_engl;
	END;

GO

SELECT
	c.Derkey1,
	UniversityIpedsCode,
	urm_ind,
	ethnicity_hispanic,
	ethnicity_black,
	ethnicity_nativeAmerican,
	ethnicity_asian,
	ethnicity_pacificIslander,
	ethnicity_white,
	gender,
	financial_aid_code,
	hs_09_grade_level,
	hs_09_school_code,
	hs_09_year_code,
	hs_09_term_code,
	hs_09_course_id,
	hs_09_course_title,
	hs_09_course_level_code,
	hs_09_course_type_code,
	hs_09_section_grade,
	hs_09_section_grade_points,
	hs_09_section_success_ind,
	hs_09_gpa_cum,
	hs_10_grade_level,
	hs_10_school_code,
	hs_10_year_code,
	hs_10_term_code,
	hs_10_course_id,
	hs_10_course_title,
	hs_10_course_level_code,
	hs_10_course_type_code,
	hs_10_section_grade,
	hs_10_section_grade_points,
	hs_10_section_success_ind,
	hs_10_gpa_cum,
	hs_11_grade_level,
	hs_11_school_code,
	hs_11_year_code,
	hs_11_term_code,
	hs_11_course_id,
	hs_11_course_title,
	hs_11_course_level_code,
	hs_11_course_type_code,
	hs_11_section_grade,
	hs_11_section_grade_points,
	hs_11_section_success_ind,
	hs_11_gpa_cum,
	hs_12_grade_level,
	hs_12_school_code,
	hs_12_year_code,
	hs_12_term_code,
	hs_12_course_id,
	hs_12_course_title,
	hs_12_course_level_code,
	hs_12_course_type_code,
	hs_12_section_grade,
	hs_12_section_grade_points,
	hs_12_section_success_ind,
	hs_12_gpa_cum,
	engl_eap_ind,
	engl_scaled_score,
	math_subject,
	math_eap_ind,
	math_scaled_score,
	esl_ind,
	expository_up11,
	expository_up11_c,
	expository_up11_cplus,
	expository_up11_bminus,
	expository_up11_b,
	expository_any,
	expository_any_c,
	expository_any_cplus,
	expository_any_bminus,
	expository_any_b,
	remedial_up11,
	remedial_up11_c,
	remedial_up11_cplus,
	remedial_up11_bminus,
	remedial_up11_b,
	remedial_any,
	remedial_any_c,
	remedial_any_cplus,
	remedial_any_bminus,
	remedial_any_b,
	csu_first_course_level_id,
	csu_first_year_code,
	csu_first_year_term_code,
	csu_first_term_ordinal,
	csu_first_course_id,
	csu_first_course_title,
	csu_first_course_units_attempted,
	csu_first_section_grade,
	csu_first_section_grade_points,
	csu_first_section_success_ind,
	csu_00_course_level_id,
	csu_00_year_code,
	csu_00_year_term_code,
	csu_00_term_ordinal,
	csu_00_course_id,
	csu_00_course_title,
	csu_00_course_units_attempted,
	csu_00_section_grade,
	csu_00_section_grade_points,
	csu_00_section_grade_points_categorical,
	csu_00_section_success_ind,
	csu_01_course_level_id,
	csu_01_year_code,
	csu_01_year_term_code,
	csu_01_term_ordinal,
	csu_01_course_id,
	csu_01_course_title,
	csu_01_course_units_attempted,
	csu_01_section_grade,
	csu_01_section_grade_points,
	csu_01_section_grade_points_categorical,
	csu_01_section_success_ind,
	csu_02_course_level_id,
	csu_02_year_code,
	csu_02_year_term_code,
	csu_02_term_ordinal,
	csu_02_course_id,
	csu_02_course_title,
	csu_02_course_units_attempted,
	csu_02_section_grade,
	csu_02_section_grade_points,
	csu_02_section_grade_points_categorical,
	csu_02_section_success_ind,
	csu_03_course_level_id,
	csu_03_year_code,
	csu_03_year_term_code,
	csu_03_term_ordinal,
	csu_03_course_id,
	csu_03_course_title,
	csu_03_course_units_attempted,
	csu_03_section_grade,
	csu_03_section_grade_points,
	csu_03_section_grade_points_categorical,
	csu_03_section_success_ind
INTO
	mmapcsu.retro_engl
FROM
	mmapcsu.chrt c
	inner join
	mmapcsu.hs_subj hs
		on c.Derkey1 = hs.Derkey1
	inner join
	mmapcsu.csu_subj csu
		on hs.Derkey1 = csu.Derkey1
		and hs.SubjectCode = csu.SubjectCode
	left outer join
	mmap.k12.prospective_subj_engl e
		on e.derkey1 = c.derkey1
WHERE
	hs.SubjectCode = 'ENGL';

-- extract
EXECUTE xp_cmdshell
	'bcp "SELECT * FROM calpass.mmapcsu.retro_engl" QUERYOUT "C:\Users\dlamoree\Desktop\retro_engl.txt" -T -c -q';
-- extract
EXECUTE dbo.sp_spss_output
	'calpass',
	'mmapcsu',
	'retro_engl',
	'C:\Users\dlamoree\desktop\', --'
	'1';

GO

-- upload
DECLARE
	@user nvarchar(255) = 'dlamoree',
	@password nvarchar(255) = '',
	@server nvarchar(255) = '174.127.112.130',
	@port nvarchar(255) = '22',
	@ssh_rsa nvarchar(255) = 'f7:0c:30:21:0d:d4:bf:5c:c2:79:f4:86:6f:21:3c:75';

EXECUTE sp_sftp_winscp_upload
	@user = @user,
	@password = @password,
	@server = @server,
	@port = @port,
	@ssh_rsa = @ssh_rsa,
	@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
	@source_file_name = 'retro_engl.txt',
	@target_file_dir = '/home/MmapCsu/';

EXECUTE sp_sftp_winscp_upload
	@user = @user,
	@password = @password,
	@server = @server,
	@port = @port,
	@ssh_rsa = @ssh_rsa,
	@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
	@source_file_name = 'retro_engl_spss_import.sps',
	@target_file_dir = '/home/MmapCsu/';

GO

-- delete
EXECUTE master.dbo.clr_file_delete
	@file_path = 'C:\Users\dlamoree\desktop\retro_engl.txt';

EXECUTE master.dbo.clr_file_delete
	@file_path = 'C:\Users\dlamoree\desktop\retro_engl_spss_import.sps';
	
-- clean up
TRUNCATE TABLE mmapcsu.hs_subj;
TRUNCATE TABLE mmapcsu.csu_subj;
TRUNCATE TABLE mmapcsu.UnivTranscript;