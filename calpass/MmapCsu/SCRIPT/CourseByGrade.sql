SELECT DISTINCT
	course_id
FROM
	(
		SELECT
			course_id,
			section_grade,
			cnt = sum(case when section_grade in ('CR', 'NC', 'P', 'NP') then 1 else 0 end),
			ttl = sum(count(*)) over(partition by course_id)
		FROM
			mmapcsu.UnivTranscript
		WHERE
			college_code in ('110617', '110574')
			and SubjectCode = 'ENGL'
		GROUP BY
			course_id,
			section_grade
	) a
WHERE
	round(convert(decimal(5,2), 100.00 * cnt / ttl), 2) > 50.00
ORDER BY
	1