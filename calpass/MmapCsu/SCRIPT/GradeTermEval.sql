USE calpass;

GO

IF (object_id('tempdb..#GradeTermEval') is not null)
	BEGIN
		DROP TABLE #GradeTermEval;
	END;

GO

CREATE TABLE
	#GradeTermEval
	(
		Ssid varchar(10),
		Semester1 decimal(2,1),
		Semester2 decimal(2,1)
	);

GO

INSERT INTO
	#GradeTermEval
	(
		Ssid,
		Semester1,
		Semester2
	)
SELECT
	s.ssid,
	semester1 = max(case when marking_period_code = 'S1' then g.Points end),
	semester2 = max(case when marking_period_code = 'S2' then g.Points end)
FROM
	calpads.scsc s
	inner join
	calpads.crsc c
		on s.district_code = c.district_code
		and s.school_code = c.school_code
		and s.year_code = c.year_code
		and s.course_id = c.course_id
		and s.section_id = c.section_id
	inner join
	comis.Grade g
		on s.course_grade = g.GradeCode
WHERE
	s.district_code = '3868478'
	and s.school_code not in ('0127530', '3830437')
	and s.year_code = '2015-2016'
	and c.course_code = '2404'
	and s.marking_period_code in ('S1', 'S2')
GROUP BY
	s.ssid;

SELECT
	Semester1,
	Semester2,
	count = count(*)
FROM
	#GradeTermEval
WHERE
	Semester1 is not null
	and Semester2 is not null
GROUP BY
	Semester1,
	Semester2
ORDER BY
	Semester1,
	Semester2;