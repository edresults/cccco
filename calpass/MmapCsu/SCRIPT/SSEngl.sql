USE calpass;

GO

IF (object_id('MmapCsu.SSEngl') is not null)
	BEGIN
		DROP TABLE MmapCsu.SSEngl;
	END;

GO

SELECT
	r.derkey1,
	MajorCode,
	IsUrm,
	IsHispanic,
	IsBlack,
	IsNativeAmerican,
	IsAsian,
	IsPacificIslander,
	IsWhite,
	Gender,
	FinancialAidCode,
	FRSL = null,
	HSSubjLastYearTermCode = h.SubjLastYearTermCode,
	HSLastYearTermCode = h.LastYearTermCode,
	CCFirstYearTermCode = c.FirstYearTermCode,
	CCSubjFirstYearTermCode = c.SubjFirstYearTermCode,
	UNFirstYearTermCode = u.year_term_code,
	RemedialMarkCategory = isnull(h.RemedialMarkCategory, -99),
	English09MarkCategory = isnull(h.English09MarkCategory, -99),
	English10MarkCategory = isnull(h.English10MarkCategory, -99),
	English11MarkCategory = isnull(h.English11MarkCategory, -99),
	English12MarkCategory = isnull(h.English12MarkCategory, -99),
	ExpositoryMarkCategory = isnull(h.ExpositoryMarkCategory, -99),
	LiteratureAPMarkCategory = isnull(h.LiteratureAPMarkCategory, -99),
	EnglishAPMarkCategory = isnull(h.EnglishAPMarkCategory, -99),
	Level0MarkCategory = isnull(h.Level0MarkCategory, -99),
	Level1MarkCategory = isnull(h.Level1MarkCategory, -99),
	Level2MarkCategory = isnull(h.Level2MarkCategory, -99),
	Level3MarkCategory = isnull(h.Level3MarkCategory, -99),
	Level4MarkCategory = isnull(h.Level4MarkCategory, -99),
	Level5MarkCategory = isnull(h.Level5MarkCategory, -99),
	ENGL001 = isnull(u.ENGL001, -99),
	ENGL001A = isnull(u.ENGL001A, -99),
	ENGL002 = isnull(u.ENGL002, -99),
	ENGL005 = isnull(u.ENGL005, -99),
	ENGL010 = isnull(u.ENGL010, -99),
	ENGL010M = isnull(u.ENGL010M, -99),
	ENGL011 = isnull(u.ENGL011, -99),
	ENGL011M = isnull(u.ENGL011M, -99),
	ENGL015 = isnull(u.ENGL015, -99),
	ENGL016 = isnull(u.ENGL016, -99),
	ENGL020 = isnull(u.ENGL020, -99),
	ENGL020M = isnull(u.ENGL020M, -99),
	ENGL040A = isnull(u.ENGL040A, -99),
	ENGL040B = isnull(u.ENGL040B, -99),
	ENGL050A = isnull(u.ENGL050A, -99),
	ENGL060 = isnull(u.ENGL060, -99),
	ENGL065 = isnull(u.ENGL065, -99),
	ENGL086 = isnull(u.ENGL086, -99),
	ENGL087 = isnull(u.ENGL087, -99),
	ENGL109E = isnull(u.ENGL109E, -99),
	ENGL109M = isnull(u.ENGL109M, -99),
	ENGL109W = isnull(u.ENGL109W, -99),
	ENGL109X = isnull(u.ENGL109X, -99),
	ENGL110A = isnull(u.ENGL110A, -99),
	ENGL116A = isnull(u.ENGL116A, -99),
	ENGL116B = isnull(u.ENGL116B, -99),
	ENGL180H = isnull(u.ENGL180H, -99),
	ENGL191A = isnull(u.ENGL191A, -99)
INTO
	MmapCsu.SSEngl
FROM
	MmapCsu.Cohort r
	inner join
	MmapCsu.RetroEnglCourse110617 u
	on r.Derkey1 = u.Derkey1
	cross apply
	dbo.HSEnglishGet(u.Derkey1) h
	outer apply
	dbo.CCEnglishGetFirst(u.Derkey1) c
WHERE
	h.LastYearTermCode < u.year_term_code
	and (
		c.FirstYearTermCode is null
		or
		c.FirstYearTermCode < u.year_term_code
	);