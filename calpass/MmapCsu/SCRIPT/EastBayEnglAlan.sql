USE MmapCsu;

GO

IF (object_id('dbo.EastBayEngl') is not null)
	BEGIN
		DROP TABLE dbo.EastBayEngl;
	END;

GO

SELECT
	derkey1,
	hs_engl09 = isnull(m1.Category,-99),
	hs_engl10 = isnull(m2.Category,-99),
	hs_engl11 = isnull(m3.Category,-99),
	hs_engl12 = isnull(m4.Category,-99),
	hs_langap = isnull(m5.Category,-99),
	hs_litap = isnull(m6.Category,-99),
	hs_rem = isnull(m7.Category,-99),
	hs_esl = isnull(m8.Category,-99),
	hs_expo = isnull(m9.Category,-99),
	cc_engl00 = isnull(m10.Category,-99),
	cc_engl01 = isnull(m11.Category,-99),
	cc_engl02 = isnull(m12.Category,-99),
	cc_engl03 = isnull(m13.Category,-99),
	cc_engl04 = isnull(m14.Category,-99),
	cc_engl05 = isnull(m15.Category,-99),
	ENGL1001 = isnull(ENGL1001,-99),
	ENGL1002 = isnull(ENGL1002,-99),
	ENGL2000 = isnull(ENGL2000,-99),
	ENGL2005 = isnull(ENGL2005,-99),
	ENGL2030 = isnull(ENGL2030,-99),
	ENGL2070 = isnull(ENGL2070,-99),
	ENGL2075 = isnull(ENGL2075,-99),
	ENGL2600 = isnull(ENGL2600,-99),
	ENGL3000 = isnull(ENGL3000,-99),
	ENGL3001 = isnull(ENGL3001,-99),
	ENGL3003 = isnull(ENGL3003,-99),
	ENGL3005 = isnull(ENGL3005,-99),
	ENGL3010 = isnull(ENGL3010,-99),
	ENGL3020 = isnull(ENGL3020,-99),
	ENGL3600 = isnull(ENGL3600,-99),
	ENGL3650 = isnull(ENGL3650,-99),
	ENGL3691 = isnull(ENGL3691,-99),
	ENGL4251 = isnull(ENGL4251,-99),
	ENGL725 = isnull(ENGL725,-99),
	ENGL801 = isnull(ENGL801,-99),
	ENGL802 = isnull(ENGL802,-99),
	ENGL803 = isnull(ENGL803,-99),
	ENGL910 = isnull(ENGL910,-99),
	ENGL930 = isnull(ENGL930,-99)
INTO
	EastBayEngl
FROM
	(
		SELECT
			a.derkey1,
			hs_engl09 = 
				case
					when hs_09_course_id = '2130' then hs_09_course_grade
					when hs_10_course_id = '2130' then hs_10_course_grade
					when hs_11_course_id = '2130' then hs_11_course_grade
					when hs_12_course_id = '2130' then hs_12_course_grade
				end,
			hs_engl10 = 
				case
					when hs_09_course_id = '2131' then hs_09_course_grade
					when hs_10_course_id = '2131' then hs_10_course_grade
					when hs_11_course_id = '2131' then hs_11_course_grade
					when hs_12_course_id = '2131' then hs_12_course_grade
				end,
			hs_engl11 = 
				case
					when hs_09_course_id = '2132' then hs_09_course_grade
					when hs_10_course_id = '2132' then hs_10_course_grade
					when hs_11_course_id = '2132' then hs_11_course_grade
					when hs_12_course_id = '2132' then hs_12_course_grade
				end,
			hs_engl12 = 
				case
					when hs_09_course_id = '2133' then hs_09_course_grade
					when hs_10_course_id = '2133' then hs_10_course_grade
					when hs_11_course_id = '2133' then hs_11_course_grade
					when hs_12_course_id = '2133' then hs_12_course_grade
				end,
			hs_LangAP = 
				case
					when hs_09_course_id = '2170' then hs_09_course_grade
					when hs_10_course_id = '2170' then hs_10_course_grade
					when hs_11_course_id = '2170' then hs_11_course_grade
					when hs_12_course_id = '2170' then hs_12_course_grade
				end,
			hs_LitAP = 
				case
					when hs_09_course_id in ('2171','2172') then hs_09_course_grade
					when hs_10_course_id in ('2171','2172') then hs_10_course_grade
					when hs_11_course_id in ('2171','2172') then hs_11_course_grade
					when hs_12_course_id in ('2171','2172') then hs_12_course_grade
				end,
			hs_rem = 
				case
					when hs_09_course_id = '2100' then hs_09_course_grade
					when hs_10_course_id = '2100' then hs_10_course_grade
					when hs_11_course_id = '2100' then hs_11_course_grade
					when hs_12_course_id = '2100' then hs_12_course_grade
				end,
			hs_esl = 
				case
					when hs_09_course_id = '2110' then hs_09_course_grade
					when hs_10_course_id = '2110' then hs_10_course_grade
					when hs_11_course_id = '2110' then hs_11_course_grade
					when hs_12_course_id = '2110' then hs_12_course_grade
				end,
			hs_expo = 
				case
					when hs_09_course_id in ('2118','2114','2113') then hs_09_course_grade
					when hs_10_course_id in ('2118','2114','2113') then hs_10_course_grade
					when hs_11_course_id in ('2118','2114','2113') then hs_11_course_grade
					when hs_12_course_id in ('2118','2114','2113') then hs_12_course_grade
				end,
			cc_engl00 = case when cc_first_course_level_id = 'Y' then cc_first_course_grade_id end,
			cc_engl01 = case when cc_first_course_level_id = 'A' then cc_first_course_grade_id end,
			cc_engl02 = case when cc_first_course_level_id = 'B' then cc_first_course_grade_id end,
			cc_engl03 = case when cc_first_course_level_id = 'C' then cc_first_course_grade_id end,
			cc_engl04 = case when cc_first_course_level_id = 'D' then cc_first_course_grade_id end,
			cc_engl05 = case when cc_first_course_level_id = 'E' then cc_first_course_grade_id end,
			ENGL1001,
			ENGL1002,
			ENGL2000,
			ENGL2005,
			ENGL2030,
			ENGL2070,
			ENGL2075,
			ENGL2600,
			ENGL3000,
			ENGL3001,
			ENGL3003,
			ENGL3005,
			ENGL3010,
			ENGL3020,
			ENGL3600,
			ENGL3650,
			ENGL3691,
			ENGL4251,
			ENGL725,
			ENGL801,
			ENGL802,
			ENGL803,
			ENGL910,
			ENGL930
		FROM
			[pro-dat-sql-03].calpass.mmapcsu.RetroEnglCourse110574 a
			left outer join
			[pro-dat-sql-03].mmap.dbo.organization_engl c
				on c.derkey1 = a.derkey1
				and c.organization_code = '000'
			left outer join
			[pro-dat-sql-03].calpass.comis.Term t
				on t.TermCode = c.cc_first_term_id
				and t.TermCodeFull < 
					case
						when substring(a.year_term_code, 5, 1) = '1' then '7'
						when substring(a.year_term_code, 5, 1) = '2' then '1'
						when substring(a.year_term_code, 5, 1) = '3' then '3'
						when substring(a.year_term_code, 5, 1) = '4' then '5'
					end
	) a
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m1
		on hs_engl09 = m1.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m2
		on hs_engl10 = m2.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m3
		on hs_engl11 = m3.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m4
		on hs_engl12 = m4.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m5
		on hs_LangAP = m5.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m6
		on hs_LitAP = m6.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m7
		on hs_rem = m7.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m8
		on hs_esl = m8.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m9
		on hs_expo = m9.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m10
		on cc_engl00 = m10.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m11
		on cc_engl01 = m11.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m12
		on cc_engl02 = m12.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m13
		on cc_engl03 = m13.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m14
		on cc_engl04 = m14.MarkCode
	left outer join
	[pro-dat-sql-03].calpass.calpads.Mark m15
		on cc_engl05 = m15.MarkCode