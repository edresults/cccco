IF (object_id('mmapcsu.CsunEngl') is not null)
	BEGIN
		DROP TABLE mmapcsu.CsunEngl;
	END;

GO

CREATE TABLE
	mmapcsu.CsunEngl
	(
		InterSegmentKey                                  binary(64)   not null,
		OverallCumulativeGradePointAverage               decimal(4,3)     null,
		SubjectCumulativeGradePointAverage               decimal(4,3)     null,
		WithoutSubjectOverallCumulativeGradePointAverage decimal(4,3)     null,
		Remedial_1_MarkCategory                          smallint         null,
		Remedial_2_MarkCategory                          smallint         null,
		English09_1_MarkCategory                         smallint         null,
		English09_2_MarkCategory                         smallint         null,
		English10_1_MarkCategory                         smallint         null,
		English10_2_MarkCategory                         smallint         null,
		English11_1_MarkCategory                         smallint         null,
		English11_2_MarkCategory                         smallint         null,
		English12_1_MarkCategory                         smallint         null,
		English12_2_MarkCategory                         smallint         null,
		Expository_1_MarkCategory                        smallint         null,
		Expository_2_MarkCategory                        smallint         null,
		Literature_1_MarkCateogory                       smallint         null,
		Literature_2_MarkCateogory                       smallint         null,
		LanguageAP_1_MarkCateogory                       smallint         null,
		LanguageAP_2_MarkCateogory                       smallint         null,
		LiteratureAP_1_MarkCategory                      smallint         null,
		LiteratureAP_2_MarkCategory                      smallint         null,
		Rhetoric_1_MarkCategory                          smallint         null,
		Rhetoric_2_MarkCategory                          smallint         null
	);

ALTER TABLE
	mmapcsu.CsunEngl
ADD CONSTRAINT
	PK_CsunEngl
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey
	);

GO

INSERT
	mmapcsu.CsunEngl
	(
		InterSegmentKey,
		OverallCumulativeGradePointAverage,
		SubjectCumulativeGradePointAverage,
		WithoutSubjectOverallCumulativeGradePointAverage,
		Remedial_1_MarkCategory,
		Remedial_2_MarkCategory,
		English09_1_MarkCategory,
		English09_2_MarkCategory,
		English10_1_MarkCategory,
		English10_2_MarkCategory,
		English11_1_MarkCategory,
		English11_2_MarkCategory,
		English12_1_MarkCategory,
		English12_2_MarkCategory,
		Expository_1_MarkCategory,
		Expository_2_MarkCategory,
		Literature_1_MarkCateogory,
		Literature_2_MarkCateogory,
		LanguageAP_1_MarkCateogory,
		LanguageAP_2_MarkCateogory,
		LiteratureAP_1_MarkCategory,
		LiteratureAP_2_MarkCategory,
		Rhetoric_1_MarkCategory,
		Rhetoric_2_MarkCategory
	)
SELECT
	b.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage,
	-- high school
	Remedial_1_MarkCategory     = isnull(max(case when rcc.ContentCode = 'Remedial'    and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	Remedial_2_MarkCategory     = isnull(max(case when rcc.ContentCode = 'Remedial'    and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	English09_1_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English09'   and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	English09_2_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English09'   and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	English10_1_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English10'   and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	English10_2_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English10'   and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	English11_1_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English11'   and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	English11_2_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English11'   and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	English12_1_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English12'   and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	English12_2_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English12'   and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	Expository_1_MarkCategory   = isnull(max(case when rcc.ContentCode = 'Composition' and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	Expository_2_MarkCategory   = isnull(max(case when rcc.ContentCode = 'Composition' and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	Literature_1_MarkCateogory  = isnull(max(case when rcc.ContentCode = 'Literature'  and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	Literature_2_MarkCateogory  = isnull(max(case when rcc.ContentCode = 'Literature'  and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	LanguageAP_1_MarkCateogory  = isnull(max(case when rcc.ContentCode = 'EnglishAP'   and rcc.ContentSelector = 2 and rcc.CourseCode = '2170' then rcc.MarkCategory end), -99),
	LanguageAP_2_MarkCateogory  = isnull(max(case when rcc.ContentCode = 'EnglishAP'   and rcc.ContentSelector = 1 and rcc.CourseCode = '2170' then rcc.MarkCategory end), -99),
	LiteratureAP_1_MarkCategory = isnull(max(case when rcc.ContentCode = 'EnglishAP'   and rcc.ContentSelector = 2 and rcc.CourseCode = '2171' then rcc.MarkCategory end), -99),
	LiteratureAP_2_MarkCategory = isnull(max(case when rcc.ContentCode = 'EnglishAP'   and rcc.ContentSelector = 1 and rcc.CourseCode = '2171' then rcc.MarkCategory end), -99),
	Rhetoric_1_MarkCategory     = isnull(max(case when rcc.ContentCode = 'Rhetoric'    and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	Rhetoric_2_MarkCategory     = isnull(max(case when rcc.ContentCode = 'Rhetoric'    and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99)
FROM
	(
		SELECT
			p.InterSegmentKey,
			OverallCumulativeGradePointAverage = max(case when DepartmentCode = 0 then CumulativeGradePointAverage end),
			SubjectCumulativeGradePointAverage = max(case when DepartmentCode = 14 then CumulativeGradePointAverage end),
			WithoutSubjectOverallCumulativeGradePointAverage = 
				convert(
					decimal(9,3),
					isnull(
						case
							when max(case when DepartmentCode = 0 then CumulativeQualityPoints end) - max(case when DepartmentCode = 14 then CumulativeQualityPoints end) <= 0 then 0
							else max(case when DepartmentCode = 0 then CumulativeQualityPoints end) - max(case when DepartmentCode = 14 then CumulativeQualityPoints end)
						end
						/
						nullif(
							case
								when max(case when DepartmentCode = 0 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 14 then CumulativeCreditAttempted end) <= 0 then 0
								else max(case when DepartmentCode = 0 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 14 then CumulativeCreditAttempted end)
							end,
							0
						)
					,0)
				)
		FROM
			MmapCsu.Prospective p
			inner join
			mmap.RetrospectivePerformance rp
				on  rp.InterSegmentKey = p.InterSegmentKey
		WHERE
			rp.DepartmentCode in (0, 14)
			and rp.IsLast = 1
		GROUP BY
			p.InterSegmentKey
	) b
	inner join
	mmap.RetrospectiveCourseContent rcc
		on rcc.InterSegmentKey = b.InterSegmentKey
	inner join
	dbo.Student s
		on s.InterSegmentKey = b.InterSegmentKey
WHERE
	rcc.DepartmentCode = 14
	and rcc.ContentSelector in (1,2)
	and s.IsGrade11SectionInclusive = 1
	and s.IsHighSchoolCollision = 0
GROUP BY
	b.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage;