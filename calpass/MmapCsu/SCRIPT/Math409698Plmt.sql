SELECT
	c.*,
	gpa.OverallCumulativeGradePointAverage,
	gpa.SubjectOverallCumulativeGradePointAverage,
	gpa.WithoutSubjectOverallCumulativeGradePointAverage,
	IsFrsl = null,
	HS12Rank = null,
	HSLastYearTermCode = null,
	HSContentAreaLastYearTermCode = hs.LastYearTermCode,
	HSArithmetic_1 = isnull(HSArithmetic_1, -99),
	HSArithmetic_2 = isnull(HSArithmetic_2, -99),
	HSPreAlgebra_1 = isnull(HSPreAlgebra_1, -99),
	HSPreAlgebra_2 = isnull(HSPreAlgebra_2, -99),
	HSAlgebraI_1 = isnull(HSAlgebraI_1, -99),
	HSAlgebraI_2 = isnull(HSAlgebraI_2, -99),
	HSGeometry_1 = isnull(HSGeometry_1, -99),
	HSGeometry_2 = isnull(HSGeometry_2, -99),
	HSAlgebraII_1 = isnull(HSAlgebraII_1, -99),
	HSAlgebraII_2 = isnull(HSAlgebraII_2, -99),
	HSStatistics_1 = isnull(HSStatistics_1, -99),
	HSStatistics_2 = isnull(HSStatistics_2, -99),
	HSStatisticsAP_1 = isnull(HSStatisticsAP_1, -99),
	HSStatisticsAP_2 = isnull(HSStatisticsAP_2, -99),
	HSTrigonometry_1 = isnull(HSTrigonometry_1, -99),
	HSTrigonometry_2 = isnull(HSTrigonometry_2, -99),
	HSPreCalculus_1 = isnull(HSPreCalculus_1, -99),
	HSPreCalculus_2 = isnull(HSPreCalculus_2, -99),
	HSCalculus_1 = isnull(HSCalculus_1, -99),
	HSCalculus_2 = isnull(HSCalculus_2, -99),
	HSCalculusAB_1 = isnull(HSCalculusAB_1, -99),
	HSCalculusAB_2 = isnull(HSCalculusAB_2, -99),
	HSCalculusBC_1 = isnull(HSCalculusBC_1, -99),
	HSCalculusBC_2 = isnull(HSCalculusBC_2, -99)
INTO
	MmapCsu.Math409698Plmt
FROM
	MmapCsu.MontereyBayPlacement c
	inner join
	(
		SELECT
			Derkey1                     = rcc.InterSegmentKey,
			LastYearTermCode            = max(rcc.YearTermCode),
			HSArithmetic_1              = isnull(max(case when ContentCode = 'Arithmetic'   and ContentSelector = 1 then MarkCategory end), -99),
			HSArithmetic_2              = isnull(max(case when ContentCode = 'Arithmetic'   and ContentSelector = 2 then MarkCategory end), -99),
			HSPreAlgebra_1              = isnull(max(case when ContentCode = 'PreAlgebra'   and ContentSelector = 1 then MarkCategory end), -99),
			HSPreAlgebra_2              = isnull(max(case when ContentCode = 'PreAlgebra'   and ContentSelector = 2 then MarkCategory end), -99),
			HSAlgebraI_1                = isnull(max(case when ContentCode = 'AlgebraI'     and ContentSelector = 1 then MarkCategory end), -99),
			HSAlgebraI_2                = isnull(max(case when ContentCode = 'AlgebraI'     and ContentSelector = 2 then MarkCategory end), -99),
			HSGeometry_1                = isnull(max(case when ContentCode = 'Geometry'     and ContentSelector = 1 then MarkCategory end), -99),
			HSGeometry_2                = isnull(max(case when ContentCode = 'Geometry'     and ContentSelector = 2 then MarkCategory end), -99),
			HSAlgebraII_1               = isnull(max(case when ContentCode = 'AlgebraII'    and ContentSelector = 1 then MarkCategory end), -99),
			HSAlgebraII_2               = isnull(max(case when ContentCode = 'AlgebraII'    and ContentSelector = 2 then MarkCategory end), -99),
			HSStatistics_1              = isnull(max(case when ContentCode = 'Statistics'   and CourseCode in ('2410','2430','2443','2445') and ContentSelector = 1 then MarkCategory end), -99),
			HSStatistics_2              = isnull(max(case when ContentCode = 'Statistics'   and CourseCode in ('2410','2430','2443','2445') and ContentSelector = 2 then MarkCategory end), -99),
			HSStatisticsAP_1            = isnull(max(case when ContentCode = 'Statistics'   and CourseCode = '2483'and ContentSelector = 1 then MarkCategory end), -99),
			HSStatisticsAP_2            = isnull(max(case when ContentCode = 'Statistics'   and CourseCode = '2483'and ContentSelector = 2 then MarkCategory end), -99),
			HSTrigonometry_1            = isnull(max(case when ContentCode = 'Trigonometry' and ContentSelector = 1 then MarkCategory end), -99),
			HSTrigonometry_2            = isnull(max(case when ContentCode = 'Trigonometry' and ContentSelector = 2 then MarkCategory end), -99),
			HSPreCalculus_1             = isnull(max(case when ContentCode = 'PreCalculus'  and ContentSelector = 1 then MarkCategory end), -99),
			HSPreCalculus_2             = isnull(max(case when ContentCode = 'PreCalculus'  and ContentSelector = 2 then MarkCategory end), -99),
			HSCalculus_1                = isnull(max(case when ContentCode = 'Calculus'     and CourseCode in ('2415','2462') and ContentSelector = 1 then MarkCategory end), -99),
			HSCalculus_2                = isnull(max(case when ContentCode = 'Calculus'     and CourseCode in ('2415','2462') and ContentSelector = 2 then MarkCategory end), -99),
			HSCalculusAB_1              = isnull(max(case when ContentCode = 'Calculus'     and CourseCode = '2480' and ContentSelector = 1 then MarkCategory end), -99),
			HSCalculusAB_2              = isnull(max(case when ContentCode = 'Calculus'     and CourseCode = '2480' and ContentSelector = 2 then MarkCategory end), -99),
			HSCalculusBC_1              = isnull(max(case when ContentCode = 'Calculus'     and CourseCode = '2481' and ContentSelector = 1 then MarkCategory end), -99),
			HSCalculusBC_2              = isnull(max(case when ContentCode = 'Calculus'     and CourseCode = '2481' and ContentSelector = 2 then MarkCategory end), -99)
		FROM
			mmap.RetrospectiveCourseContent rcc
		WHERE
			rcc.DepartmentCode = 18
		GROUP BY
			rcc.InterSegmentKey
	) hs
		on hs.Derkey1 = c.InterSegmentKey
	inner join
	(
		SELECT
			Derkey1 = InterSegmentKey,
			OverallCumulativeGradePointAverage = max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeGradePointAverage end),
			SubjectOverallCumulativeGradePointAverage = max(case when DepartmentCode = 18 and IsLast = 1 then CumulativeGradePointAverage end),
			WithoutSubjectOverallCumulativeGradePointAverage = 
				convert(
					decimal(9,3),
					isnull(
						case
							when max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeQualityPoints end) - max(case when DepartmentCode = 18 and IsLast = 1 then CumulativeQualityPoints end) <= 0 then 0
							else max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeQualityPoints end) - max(case when DepartmentCode = 18 and IsLast = 1 then CumulativeQualityPoints end)
						end
						/
						nullif(
							case
								when max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 18 and IsLast = 1 then CumulativeCreditAttempted end) <= 0 then 0
								else max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 18 and IsLast = 1 then CumulativeCreditAttempted end)
							end,
							0
						)
					,0)
				)
		FROM
			mmap.RetrospectivePerformance
		GROUP BY
			InterSegmentKey
	) gpa
		on gpa.Derkey1 = c.InterSegmentKey;