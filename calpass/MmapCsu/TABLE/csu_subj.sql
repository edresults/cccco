USE calpass;

IF (object_id('mmapcsu.csu_subj') is not null)
	BEGIN
		DROP TABLE mmapcsu.csu_subj;
	END;

GO

CREATE TABLE
	mmapcsu.csu_subj
	(
		Derkey1 binary(64) not null,
		SubjectCode char(4) not null,
		csu_first_course_level_id tinyint,
		csu_first_year_code char(9),
		csu_first_year_term_code char(5),
		csu_first_term_ordinal tinyint,
		csu_first_course_id varchar(13),
		csu_first_course_title varchar(40),
		csu_first_course_units_attempted decimal(5,2),
		csu_first_section_grade varchar(3),
		csu_first_section_grade_points decimal(3,2),
		csu_first_section_grade_points_categorical smallint,
		csu_first_section_success_ind bit,
		csu_00_course_level_id tinyint,
		csu_00_year_code char(9),
		csu_00_year_term_code char(5),
		csu_00_term_ordinal tinyint,
		csu_00_course_id varchar(13),
		csu_00_course_title varchar(40),
		csu_00_course_units_attempted decimal(5,2),
		csu_00_section_grade varchar(3),
		csu_00_section_grade_points decimal(3,2),
		csu_00_section_grade_points_categorical smallint,
		csu_00_section_success_ind bit,
		csu_01_course_level_id tinyint,
		csu_01_year_code char(9),
		csu_01_year_term_code char(5),
		csu_01_term_ordinal tinyint,
		csu_01_course_id varchar(13),
		csu_01_course_title varchar(40),
		csu_01_course_units_attempted decimal(5,2),
		csu_01_section_grade varchar(3),
		csu_01_section_grade_points decimal(3,2),
		csu_01_section_grade_points_categorical smallint,
		csu_01_section_success_ind bit,
		csu_02_course_level_id tinyint,
		csu_02_year_code char(9),
		csu_02_year_term_code char(5),
		csu_02_term_ordinal tinyint,
		csu_02_course_id varchar(13),
		csu_02_course_title varchar(40),
		csu_02_course_units_attempted decimal(5,2),
		csu_02_section_grade varchar(3),
		csu_02_section_grade_points decimal(3,2),
		csu_02_section_grade_points_categorical smallint,
		csu_02_section_success_ind bit,
		csu_03_course_level_id tinyint,
		csu_03_year_code char(9),
		csu_03_year_term_code char(5),
		csu_03_term_ordinal tinyint,
		csu_03_course_id varchar(13),
		csu_03_course_title varchar(40),
		csu_03_course_units_attempted decimal(5,2),
		csu_03_section_grade varchar(3),
		csu_03_section_grade_points decimal(3,2),
		csu_03_section_grade_points_categorical smallint,
		csu_03_section_success_ind bit
	);

GO

CREATE CLUSTERED INDEX
	ixc_csu_subj__Derkey1__SubjectCode
ON
	mmapcsu.csu_subj
	(
		Derkey1,
		SubjectCode
	);