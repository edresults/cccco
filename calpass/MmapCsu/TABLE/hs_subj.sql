USE calpass;

IF (object_id('mmapcsu.hs_subj') is not null)
	BEGIN
		DROP TABLE mmapcsu.hs_subj;
	END;

GO

CREATE TABLE
	mmapcsu.hs_subj
	(
		Derkey1 binary(64) not null,
		SubjectCode varchar(4) not null,
		hs_09_grade_level char(2),
		hs_09_school_code char(14),
		hs_09_year_code smallint,
		hs_09_term_code varchar(3),
		hs_09_course_id char(4),
		hs_09_course_title varchar(40),
		hs_09_course_level_code varchar(2),
		hs_09_course_type_code varchar(2),
		hs_09_section_grade varchar(5),
		hs_09_section_grade_points decimal(2,1),
		hs_09_section_success_ind bit,
		hs_09_gpa_cum decimal(3,2),
		hs_10_grade_level char(2),
		hs_10_school_code char(14),
		hs_10_year_code smallint,
		hs_10_term_code varchar(3),
		hs_10_course_id char(4),
		hs_10_course_title varchar(40),
		hs_10_course_level_code varchar(2),
		hs_10_course_type_code varchar(2),
		hs_10_section_grade varchar(5),
		hs_10_section_grade_points decimal(2,1),
		hs_10_section_success_ind bit,
		hs_10_gpa_cum decimal(3,2),
		hs_11_grade_level char(2),
		hs_11_school_code char(14),
		hs_11_year_code smallint,
		hs_11_term_code varchar(3),
		hs_11_course_id char(4),
		hs_11_course_title varchar(40),
		hs_11_course_level_code varchar(2),
		hs_11_course_type_code varchar(2),
		hs_11_section_grade varchar(5),
		hs_11_section_grade_points decimal(2,1),
		hs_11_section_success_ind bit,
		hs_11_gpa_cum decimal(3,2),
		hs_12_grade_level char(2),
		hs_12_school_code char(14),
		hs_12_year_code smallint,
		hs_12_term_code varchar(3),
		hs_12_course_id char(4),
		hs_12_course_title varchar(40),
		hs_12_course_level_code varchar(2),
		hs_12_course_type_code varchar(2),
		hs_12_section_grade varchar(5),
		hs_12_section_grade_points decimal(2,1),
		hs_12_section_success_ind bit,
		hs_12_gpa_cum decimal(3,2),
		engl_eap_ind bit,
		engl_scaled_score int,
		math_subject char(1),
		math_eap_ind bit,
		math_scaled_score int,
		esl_ind bit
	);

ALTER TABLE
	mmapcsu.hs_subj
ADD CONSTRAINT
	pk_hs_subj__Derkey1__SubjectCode
PRIMARY KEY CLUSTERED
	(
		Derkey1,
		SubjectCode
	);