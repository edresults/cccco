USE calpass;

GO

IF (object_id('MmapCsu.UnivCourseEquivalency') is not null)
	BEGIN
		DROP TABLE MmapCsu.UnivCourseEquivalency;
	END;

GO

CREATE TABLE
	MmapCsu.UnivCourseEquivalency
	(
		UniversityCode char(6) not null,
		CourseAbbr varchar(255) not null,
		CourseNum varchar(255) not null,
		CourseAbbrEquivalency varchar(255) not null,
		CourseNumEquivalency varchar(255) not null
	);

GO

ALTER TABLE
	MmapCsu.UnivCourseEquivalency
ADD CONSTRAINT
	pk_UnivCourseEquivalency__UniversityCode__CourseId__CourseNum
PRIMARY KEY CLUSTERED
	(
		UniversityCode,
		CourseAbbr,
		CourseNum
	);

INSERT INTO
	MmapCsu.UnivCourseEquivalency
	(
		UniversityCode,
		CourseAbbr,
		CourseNum,
		CourseAbbrEquivalency,
		CourseNumEquivalency
	)
VALUES
	-- CSU Sacramento State :: English
	('110617','ENGL','015','ENGL','15'),
	('110617','LS','015','ENGL','15'),
	('110617','LS','15','ENGL','15'),
	-- CSU Monterey :: Mathematics
	('409698','STAT','200','STAT','100'),
	('409698','ENGL','98','ENGL','098'),
	('409698','ENGL','99','ENGL','099');