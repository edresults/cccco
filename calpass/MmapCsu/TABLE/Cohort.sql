USE calpass;

IF (object_id('MmapCsu.Cohort') is not null)
	BEGIN
		DROP TABLE MmapCsu.Cohort;
	END;

GO

CREATE TABLE
	MmapCsu.Cohort
	(
		UniversityCode char(6) not null,
		Derkey1 binary(64) not null,
		MajorCode char(6),
		IsUrm bit,
		IsHispanic char(1),
		IsBlack char(1),
		IsNativeAmerican char(1),
		IsAsian char(1),
		IsPacificIslander char(1),
		IsWhite char(1),
		Gender char(1),
		FinancialAidCode char(1),
		FirstYearTermCode char(5)
	);

GO

ALTER TABLE
	MmapCsu.Cohort
ADD CONSTRAINT
	pk_Cohort
PRIMARY KEY CLUSTERED
	(
		UniversityCode,
		Derkey1
	);