USE calpass;

GO

IF (object_id('MmapCsu.Prospective') is not null)
	BEGIN
		DROP TABLE MmapCsu.Prospective;
	END;

GO

CREATE TABLE
	MmapCsu.Prospective
	(
		InterSegmentKey      binary(64)  not null,
		CollegeCode          char(6)         null,
		NameFirst            varchar(30)     null,
		NameLast             varchar(40)     null,
		BirthDate            char(8)         null,
		GenderCode           char(1)         null,
		HighSchool           char(14)        null,
		MajorCode            char(6)         null,
		PlacementLocalEnglId tinyint         null,
		PlacementLocalMathId tinyint         null,
		PlacementModelEnglId tinyint         null,
		PlacementModelMathId tinyint         null
	);

GO

ALTER TABLE
	MmapCsu.Prospective
ADD CONSTRAINT
	PK_Prospective
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey
	);

ALTER TABLE
	MmapCsu.Prospective
ADD CONSTRAINT
	FK_Prospective__PlacementLocalEnglId
FOREIGN KEY
	(
		PlacementLocalEnglId
	)
REFERENCES
	MmapCsu.PlacementCategory
	(
		Id
	);

ALTER TABLE
	MmapCsu.Prospective
ADD CONSTRAINT
	FK_Prospective__PlacementLocalMathId
FOREIGN KEY
	(
		PlacementLocalMathId
	)
REFERENCES
	MmapCsu.PlacementCategory
	(
		Id
	);

ALTER TABLE
	MmapCsu.Prospective
ADD CONSTRAINT
	FK_Prospective__PlacementModelEnglId
FOREIGN KEY
	(
		PlacementModelEnglId
	)
REFERENCES
	MmapCsu.PlacementCategory
	(
		Id
	);

ALTER TABLE
	MmapCsu.Prospective
ADD CONSTRAINT
	FK_Prospective__PlacementModelMathId
FOREIGN KEY
	(
		PlacementModelMathId
	)
REFERENCES
	MmapCsu.PlacementCategory
	(
		Id
	);

GO

CREATE NONCLUSTERED INDEX
	IX_Prospective__CollegeCode
ON
	MmapCsu.Prospective
	(
		CollegeCode
	);
