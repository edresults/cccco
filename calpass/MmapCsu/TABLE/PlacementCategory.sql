USE calpass;

GO

IF (object_id('MmapCsu.PlacementCategory') is not null)
	BEGIN
		DROP TABLE MmapCsu.PlacementCategory;
	END;

GO

CREATE TABLE
	MmapCsu.PlacementCategory
	(
		Id          tinyint      not null identity(1,1),
		Label       varchar(25)  not null,
		Description varchar(255) not null
	);

GO

ALTER TABLE
	MmapCsu.PlacementCategory
ADD CONSTRAINT
	PK_PlacementCategory
PRIMARY KEY CLUSTERED
	(
		Id
	);

GO

INSERT
	MmapCsu.PlacementCategory
	(
		Label,
		Description
	)
VALUES
	('Category I','Has fulfilled the GE Subarea A2 or B4 requirement'),
	('Category II','Placement in a GE Subarea A2 or B4 course'),
	('Category III','Recommend placement in a supported GE Subarea A2 or B4 course'),
	('Category IV','Require placement in a supported GE Subarea A2 or B4 course or the first term of an applicable stretch course');