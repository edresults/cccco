USE calpass;

IF (object_id('MmapCsu.Transcript') is not null)
	BEGIN
		DROP TABLE MmapCsu.Transcript;
	END;

GO

CREATE TABLE
	MmapCsu.Transcript
	(
		UniversityCode char(6) not null,
		Derkey1 binary(64) not null,
		ContentAreaCode char(4) not null,
		YearTermCode char(5) not null,
		CourseRowNumber tinyint not null,
		YearTermRank tinyint not null,
		ContentAreaFirstYearTermCode char(5) not null,
		LevelBelow tinyint not null,
		CourseId varchar(13) not null,
		CourseTitle varchar(40) not null,
		UnitsTry decimal(5,2) not null,
		Mark varchar(5) not null,
		Points decimal(3,2) not null,
		Category smallint not null,
		IsSuccess bit not null
	);

GO

ALTER TABLE
	MmapCsu.Transcript
ADD CONSTRAINT
	pk_Transcript
PRIMARY KEY CLUSTERED
	(
		UniversityCode,
		Derkey1,
		ContentAreaCode,
		YearTermCode,
		CourseRowNumber
	);