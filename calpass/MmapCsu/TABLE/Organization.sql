USE calpass;

GO

IF (object_id('MmapCsu.Organization') is not null)
	BEGIN
		DROP TABLE MmapCsu.Organization;
	END;

GO

CREATE TABLE
	MmapCsu.Organization
	(
		ORG_ID                varchar(255) not null,
		INST_ORIGIN_CODE      varchar(255)     null,
		CDS_CODE_7            varchar(255)     null,
		CDS_CODE_14           varchar(255)     null,
		INST_NAME             varchar(255)     null,
		INST_STREET_ADDRESS   varchar(255)     null,
		INST_CITY             varchar(255)     null,
		INST_ZIP_CODE         varchar(255)     null,
		INST_OBS_CODE         varchar(255)     null,
		EFFECTIVE_DATE        varchar(255)     null,
		OBSOLETE_DATE         varchar(255)     null,
		TABLE_CODE            varchar(255)     null,
		CMS_SCHOOL_TYPE       varchar(255)     null,
		CMS_PROPRIETORSHIP    varchar(255)     null
	);

GO

ALTER TABLE
	MmapCsu.Organization
ADD CONSTRAINT
	PK_Organization
PRIMARY KEY CLUSTERED
	(
		ORG_ID
	);

GO

MERGE
	MmapCsu.Organization t
USING
	(
		SELECT
			ORG_ID,
			INST_ORIGIN_CODE,
			CDS_CODE_7,
			CDS_CODE_14,
			INST_NAME,
			INST_STREET_ADDRESS,
			INST_CITY,
			INST_ZIP_CODE,
			INST_OBS_CODE,
			EFFECTIVE_DATE,
			OBSOLETE_DATE,
			TABLE_CODE,
			CMS_SCHOOL_TYPE,
			CMS_PROPRIETORSHIP
		FROM
			(
				SELECT
					ORG_ID,
					INST_ORIGIN_CODE,
					CDS_CODE_7,
					CDS_CODE_14,
					INST_NAME,
					INST_STREET_ADDRESS,
					INST_CITY,
					INST_ZIP_CODE,
					INST_OBS_CODE,
					EFFECTIVE_DATE,
					OBSOLETE_DATE,
					TABLE_CODE,
					CMS_SCHOOL_TYPE,
					CMS_PROPRIETORSHIP,
					selector = row_number() over(partition by ORG_ID order by convert(date, EFFECTIVE_DATE) desc)
				FROM
					OPENROWSET(
						BULK
						N'C:\Data\CSU\DATA\esapprd-commonc-commoncinstorigin-prd-1.csv',
						FORMATFILE = 'C:\Data\CSU\FORMAT\Organization.fmt',
						FIRSTROW = 2
					) a
				WHERE
					a.ORG_ID is not null
					and a.OBSOLETE_DATE is null
			) b
		WHERE
			selector = 1
	) s
ON
	(
		s.INST_ORIGIN_CODE = t.INST_ORIGIN_CODE
	)
WHEN MATCHED THEN
	UPDATE SET
		t.ORG_ID              = s.ORG_ID,
		t.CDS_CODE_7          = s.CDS_CODE_7,
		t.CDS_CODE_14         = s.CDS_CODE_14,
		t.INST_NAME           = s.INST_NAME,
		t.INST_STREET_ADDRESS = s.INST_STREET_ADDRESS,
		t.INST_CITY           = s.INST_CITY,
		t.INST_ZIP_CODE       = s.INST_ZIP_CODE,
		t.INST_OBS_CODE       = s.INST_OBS_CODE,
		t.EFFECTIVE_DATE      = s.EFFECTIVE_DATE,
		t.OBSOLETE_DATE       = s.OBSOLETE_DATE,
		t.TABLE_CODE          = s.TABLE_CODE,
		t.CMS_SCHOOL_TYPE     = s.CMS_SCHOOL_TYPE,
		t.CMS_PROPRIETORSHIP  = s.CMS_PROPRIETORSHIP
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			ORG_ID,
			INST_ORIGIN_CODE,
			CDS_CODE_7,
			CDS_CODE_14,
			INST_NAME,
			INST_STREET_ADDRESS,
			INST_CITY,
			INST_ZIP_CODE,
			INST_OBS_CODE,
			EFFECTIVE_DATE,
			OBSOLETE_DATE,
			TABLE_CODE,
			CMS_SCHOOL_TYPE,
			CMS_PROPRIETORSHIP
		)
	VALUES
		(
			s.ORG_ID,
			s.INST_ORIGIN_CODE,
			s.CDS_CODE_7,
			s.CDS_CODE_14,
			s.INST_NAME,
			s.INST_STREET_ADDRESS,
			s.INST_CITY,
			s.INST_ZIP_CODE,
			s.INST_OBS_CODE,
			s.EFFECTIVE_DATE,
			s.OBSOLETE_DATE,
			s.TABLE_CODE,
			s.CMS_SCHOOL_TYPE,
			s.CMS_PROPRIETORSHIP
		);