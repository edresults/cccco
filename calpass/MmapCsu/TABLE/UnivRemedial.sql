USE calpass;

GO

IF (object_id('MmapCsu.UnivRemedial') is not null)
	BEGIN
		DROP TABLE MmapCsu.UnivRemedial;
	END;

GO

CREATE TABLE
	MmapCsu.UnivRemedial
	(
		UniversityCode char(6) not null,
		CourseAbbr varchar(255) not null,
		CourseNum varchar(255) not null,
		LevelBelow tinyint not null
	);

GO

ALTER TABLE
	MmapCsu.UnivRemedial
ADD CONSTRAINT
	pk_UnivRemedial__UniversityCode__CourseId__CourseNum
PRIMARY KEY CLUSTERED
	(
		UniversityCode,
		CourseAbbr,
		CourseNum
	);

INSERT INTO
	MmapCsu.UnivRemedial
	(
		UniversityCode,
		CourseAbbr,
		CourseNum,
		LevelBelow
	)
VALUES
	-- CSU San Bernardino :: Mathematics 
	-- ('110510', 'MATH', '100', 0),
	-- ('110510', 'MATH', '115', 0),
	-- ('110510', 'MATH', '120', 0),
	-- ('110510', 'MATH', '192', 0),
	-- ('110510', 'MATH', '211', 0),
	('110510', 'MATH', '90', 1),
	('110510', 'MATH', '80', 2),
	('110510', 'MATH', '75C', 2),
	('110510', 'MATH', '75B', 3),
	('110510', 'MATH', '75A', 4),
	-- CSU San Bernardino :: English 
	-- A: Single-Lingual Students
	-- B: Multi-Lingual Students
	-- ('110510', 'ENG', '106A', 0),
	('110510', 'ENG', '105A', 1),
	-- ('110510', 'ENG', '106B', 0),
	('110510', 'ENG', '105B', 1),
	-- ('110510', 'ENG', '104A', 0),
	('110510', 'ENG', '103A', 1),
	('110510', 'ENG', '102A', 2),
	-- ('110510', 'ENG', '104B', 0),
	('110510', 'ENG', '103B', 1),
	('110510', 'ENG', '102B', 2),
	-- CSU East Bay :: Mathematics
	('110574', 'MATH', '950', 1),
	('110574', 'MATH', '900', 2),
	('110574', 'MATH', '807', 1),
	('110574', 'MATH', '806', 2),
	('110574', 'MATH', '805', 3),
	('110574', 'MATH', '0950', 1),
	('110574', 'MATH', '0900', 2),
	('110574', 'MATH', '0807', 1),
	('110574', 'MATH', '0806', 2),
	('110574', 'MATH', '0805', 3),
	-- CSU East Bay :: English
	('110574', 'ENGL', '910', 1),
	('110574', 'ENGL', '801', 3),
	('110574', 'ENGL', '802', 2),
	('110574', 'ENGL', '803', 1),
	('110574', 'ENGL', '0910', 1),
	('110574', 'ENGL', '0801', 3),
	('110574', 'ENGL', '0802', 2),
	('110574', 'ENGL', '0803', 1),
	-- CSU Sacramento State :: Mathematics
	-- ('110617', 'MLSK', '7A', 3),
	-- ('110617', 'MLSK', '10A', 2),
	-- ('110617', 'MLSK', '10X', 1),
	('110617', 'MATH', 'O09', 1),
	('110617', 'MATH', '09', 1),
	('110617', 'MATH', '9', 1),
	('110617', 'MATH', '011', 1),
	('110617', 'MATH', '11', 1),
	('110617', 'STAT', '50', 0),
	('110617', 'STAT', '050', 0),
	('110617', 'STAT', '1', 0),
	('110617', 'STAT', '01', 0),
	('110617', 'STAT', '001', 0),
	-- ('110617', 'LA', '10I', 1),
	-- CSU Sacramento State :: English
	('110617', 'ENGL', '15', 2),
	('110617', 'ENGL', '015', 2),
	('110617', 'LS', '15', 2),
	('110617', 'LS', '015', 2),
	('110617', 'ENGL', '1', 1),
	('110617', 'ENGL', '01', 1),
	('110617', 'ENGL', '001', 1),
	('110617', 'ENGL', '86', 2),
	('110617', 'ENGL', '086', 2),
	('110617', 'ENGL', '87', 1),
	('110617', 'ENGL', '087', 1),
	('110617', 'ENGL', '10', 2),
	('110617', 'ENGL', '010', 2),
	('110617', 'ENGL', '11', 1),
	('110617', 'ENGL', '011', 1),
	('110617', 'ENGL', '10M', 2),
	('110617', 'ENGL', '010M', 2),
	('110617', 'ENGL', '11M', 1),
	('110617', 'ENGL', '011M', 1),
	-- CSU Northridge :: Mathematics
	('110608', 'MATH', '092', 2),
	('110608', 'MATH', '093', 1),
	-- CSU Northridge :: English
	('110608', 'ENGL', '113A', 1),
	('110608', 'UNIV', '061', 1),
	-- CSU Monterey :: Mathematics
	('409698', 'MATH', '098', 3),
	('409698', 'MATH', '099', 2),
	('409698', 'MATH', '100', 1),
	('409698', 'MATH', '109', 0),
	('409698', 'MATH', '115', 0),
	('409698', 'MATH', '116', 0),
	('409698', 'MATH', '122', 0),
	('409698', 'MATH', '130', 0),
	('409698', 'MATH', '150', 0),
	('409698', 'MATH', '151', 0),
	('409698', 'STAT', '100', 0),
	('409698', 'STAT', '200', 0),
	-- CSU Monterey :: English
	('409698', 'CAD','100A', 0),
	('409698', 'CAD','100B', 0),
	('409698', 'CHHS','111', 0),
	('409698', 'CHHS','125', 0),
	('409698', 'CST','211', 0),
	('409698', 'ENSTU','211', 0),
	('409698', 'HCOM','112', 0),
	('409698', 'HCOM','211S', 0),
	('409698', 'HCOM','211', 0),
	('409698', 'SBS','200', 0),
	('409698', 'VPA','210', 0);