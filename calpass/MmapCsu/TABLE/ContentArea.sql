USE calpass;

GO

IF (object_id('MmapCsu.ContentArea') is not null)
	BEGIN
		DROP TABLE MmapCsu.ContentArea;
	END

GO

CREATE TABLE
	MmapCsu.ContentArea
	(
		ContentAreaCode varchar(4) not null,
		UniversityCode char(6) not null,
		CourseAbbr varchar(5) not null
	);

GO

ALTER TABLE
	MmapCsu.ContentArea
ADD CONSTRAINT
	pk_ContentArea
PRIMARY KEY CLUSTERED
	(
		ContentAreaCode,
		UniversityCode,
		CourseAbbr
	);

GO

INSERT INTO
	MmapCsu.ContentArea
	(
		ContentAreaCode,
		UniversityCode,
		CourseAbbr
	)
VALUES
	('ENGL','110617','ENGL'),
	('ENGL','110617','LS'),
	('MATH','110617','MATH'),
	('MATH','110617','STAT'),
	('ENGL','110574','ENGL'),
	('MATH','110574','MATH'),
	('MATH','110574','STAT'),
	('ENGL','110510','ENG'),
	('MATH','110510','MATH'),
	('MATH','110510','STAT'),
	-- CSU Northridge
	('ENGL','110608','ENGL'),
	('ENGL','110608','UNIV'),
	('MATH','110608','MATH'),
	-- CSU Monterey
	('MATH','409698','STAT'),
	('MATH','409698','MATH'),
	('ENGL','409698','ENGL'),
	('ENGL','409698','CAD'),
	('ENGL','409698','CHHS'),
	('ENGL','409698','CST'),
	('ENGL','409698','ENSTU'),
	('ENGL','409698','HCOM'),
	('ENGL','409698','SBS'),
	('ENGL','409698','VPA');