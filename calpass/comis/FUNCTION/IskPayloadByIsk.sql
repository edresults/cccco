USE calpass;

GO

IF (object_id('comis.IskPayloadByIsk') is not null)
	BEGIN
		DROP FUNCTION comis.IskPayloadByIsk;
	END;

GO

CREATE FUNCTION
	comis.IskPayloadByIsk
	(
		@InterSegmentKey binary(64)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			CollegeCode = id.college_id,
			StudentId = id.student_id,
			Payload = 
				upper(convert(nchar(3), st.name_first)) + 
				upper(convert(nchar(3), st.name_last)) + 
				upper(convert(nchar(1), st.gender)) + 
				convert(nchar(8), convert(char(8), sb.birthdate, 112)),
			PayloadBug = 
				upper(convert(char(5), substring(st.name_first, 1, 1) + '\000')) + 
				upper(convert(char(5), substring(st.name_first, 2, 1) + '\000')) + 
				upper(convert(char(5), substring(st.name_first, 3, 1) + '\000')) + 
				upper(convert(char(5), substring(st.name_last, 1, 1) + '\000')) + 
				upper(convert(char(5), substring(st.name_last, 2, 1) + '\000')) + 
				upper(convert(char(5), substring(st.name_last, 3, 1) + '\000')) + 
				upper(convert(char(5), st.gender + '\000')) + 
				convert(char(5), substring(convert(char(8), sb.birthdate, 112), 1, 1) + '\000') + 
				convert(char(5), substring(convert(char(8), sb.birthdate, 112), 2, 1) + '\000') + 
				convert(char(5), substring(convert(char(8), sb.birthdate, 112), 3, 1) + '\000') + 
				convert(char(5), substring(convert(char(8), sb.birthdate, 112), 4, 1) + '\000') + 
				convert(char(5), substring(convert(char(8), sb.birthdate, 112), 5, 1) + '\000') + 
				convert(char(5), substring(convert(char(8), sb.birthdate, 112), 6, 1) + '\000') + 
				convert(char(5), substring(convert(char(8), sb.birthdate, 112), 7, 1) + '\000') + 
				convert(char(5), substring(convert(char(8), sb.birthdate, 112), 8, 1) + '\000')
		FROM
			comis.studntid id
			inner join
			comis.sbstudnt sb
				on sb.college_id = id.college_id
				and sb.student_id = id.student_id
			inner join
			comis.stterm st
				on st.college_id = id.college_id
				and st.student_id = id.student_id
			inner join
			comis.Term t
				on t.TermCode = st.term_id
		WHERE
			id.InterSegmentKey = @InterSegmentKey
			and t.YearTermCode = (
				SELECT
					min(t1.YearTermCode)
				FROM
					comis.stterm st1
					inner join
					comis.Term t1
						on t1.TermCode = st1.term_id
				WHERE
					st1.college_id = st.college_id
					and st1.student_id = st.student_id
			)
	);