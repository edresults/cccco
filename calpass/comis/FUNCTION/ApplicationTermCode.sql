ALTER FUNCTION
    comis.ApplicationTermCode
    (
        @CollegeCode     char(3),
        @TermDescription varchar(100)
    )
RETURNS
    TABLE
AS

RETURN (
    SELECT
        TermCode = 
            case
                when @TermDescription like '%2027%'         then '27'
                when @TermDescription like '%2026%'         then '26'
                when @TermDescription like '%2025%'         then '25'
                when @TermDescription like '%2024%'         then '24'
                when @TermDescription like '%2023%'         then '23'
                when @TermDescription like '%2022%'         then '22'
                when @TermDescription like '%2021%'         then '21'
                when @TermDescription like '%2020%'         then '20'
                when @TermDescription like '%2019%'         then '19'
                when @TermDescription like '%2018%'         then '18'
                when @TermDescription like '%2017%'         then '17'
                when @TermDescription like '%2016%'         then '16'
                when @TermDescription like '%2015%'         then '15'
                when @TermDescription like '%2014%'         then '14'
                when @TermDescription like '%2013%'         then '13'
                when @TermDescription like '%2012%'         then '12'
                when @TermDescription like '%2011%'         then '11'
            end + 
            convert(
                char(1),
                case
                    when @TermDescription like '%Summer%'       then 5
                    when @TermDescription like '%Fall%'         then 7
                    when @TermDescription like '%Winter%'       then 1
                    when @TermDescription like '%Intersession%' then 1
                    when @TermDescription like '%Spring%'       then 3
                    -- misspellings
                    when @TermDescription like '%Sumner%'       then 5
                    when @TermDescription like '%Sping%'        then 3
                end + 
                case
                    -- quarter colleges
                    when @CollegeCode in ('221', '421', '422', '863') then 1
                    -- semester colleges
                    else 0
                end
            )
);