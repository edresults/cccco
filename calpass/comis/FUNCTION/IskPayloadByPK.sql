USE calpass;

GO

IF (object_id('comis.IskPayloadByPK') is not null)
	BEGIN
		DROP FUNCTION comis.IskPayloadByPK;
	END;

GO

CREATE FUNCTION
	comis.IskPayloadByPK
	(
		@CollegeCode char(3),
		@StudentId char(9)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			StudentId = sb.student_id,
			Payload = 
				upper(convert(nchar(3), st.name_first)) + 
				upper(convert(nchar(3), st.name_last)) + 
				upper(convert(nchar(1), st.gender)) + 
				convert(nchar(8), sb.birthdate, 112),
			PayloadOrigin = 
				upper(convert(nchar(3), st.name_first)) + 
				upper(convert(nchar(3), st.name_last)) + 
				upper(convert(nchar(1), st.gender)) + 
				convert(nchar(8), convert(char(8), sb.birthdate, 112)) + 
				convert(nchar(6), high_school),
			PayloadFull = 
				upper(convert(nvarchar(30), st.name_first)) + 
				upper(convert(nvarchar(40), st.name_last)) + 
				upper(convert(nvarchar(1), st.gender)) + 
				convert(nchar(8), birthdate, 112),
			PayloadFullOrigin = 
				upper(convert(nvarchar(30), st.name_first)) + 
				upper(convert(nvarchar(40), st.name_last)) + 
				upper(convert(nvarchar(1), st.gender)) + 
				convert(nchar(8), birthdate, 112) + 
				convert(nchar(6), high_school)
		FROM
			comis.sbstudnt sb
			inner join
			comis.stterm st
				on st.college_id = sb.college_id
				and st.student_id = sb.student_id
			inner join
			comis.Term t
				on t.TermCode = st.term_id
		WHERE
			sb.college_id = @CollegeCode
			and sb.student_id = @StudentId
			and t.YearTermCode = (
				SELECT
					min(t1.YearTermCode)
				FROM
					comis.stterm st1
					inner join
					comis.Term t1
						on t1.TermCode = st1.term_id
				WHERE
					st1.college_id = st.college_id
					and st1.student_id = st.student_id
			)
	);