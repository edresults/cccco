IF (object_id('Comis.StudentGet') is not null)
	BEGIN
		DROP FUNCTION Comis.StudentGet;
	END;

GO

CREATE FUNCTION
	Comis.StudentGet
	(
		@InterSegmentKey binary(64)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			IsCollision     = case when count(distinct case when id3.student_id_status = 'S' then id3.ssn end) > 1 then 1 else 0 end,
			IsMutation      = case when count(distinct case when id3.student_id_status = 'S' then id3.ssn end) = 1 and count(distinct id3.student_id_status) > 1 then 1 else 0 end,
			IsMultiCollege  = case when count(distinct case when id3.student_id_status = 'S' then id3.ssn end) = 1 and max(case when c1.CollegeCode != c2.CollegeCode then 1 else 0 end) = 1 then 1 else 0 end,
			IsMultiDistrict = case when count(distinct case when id3.student_id_status = 'S' then id3.ssn end) = 1 and max(case when c1.DistrictCode != c2.DistrictCode then 1 else 0 end) = 1 then 1 else 0 end
		FROM
			comis.studntid id1
			inner join
			comis.College c1
				on c1.CollegeCode = id1.college_id
			left outer join
			comis.studntid id2
				on id2.ssn = id1.ssn
				and id2.student_id_status = id1.student_id_status
			inner join
			comis.College c2
				on c2.CollegeCode = id2.college_id
			left outer join
			comis.studntid id3
				on id3.InterSegmentKey = id1.InterSegmentKey
		WHERE
			id1.InterSegmentKey = @InterSegmentKey
	);