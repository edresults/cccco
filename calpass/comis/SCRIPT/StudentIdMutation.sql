DECLARE
	-- variables
	@CollegeCode char(3),
	@CollegeIpedsCode char(6);

BEGIN
	DECLARE
		CollegeCursor
	CURSOR 
		FAST_FORWARD
	FOR 
		SELECT
			CollegeCode,
			IpedsCodeLegacy
		FROM
			comis.College
		ORDER BY
			CollegeCode;

	-- open cursor
	OPEN CollegeCursor;

	-- capture cursor variables
	FETCH NEXT FROM
		CollegeCursor
	INTO
		@CollegeCode,
		@CollegeIpedsCode;

	-- loop over cursor
	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- output college
		RAISERROR(@CollegeCode, 0, 1) WITH NOWAIT;

		SELECT
			id.college_id,
			id.ssn,
			id.student_id_status,
			st.term_id
		FROM
			comis.stterm st
			inner join
			comis.studntid id
				on st.college_id = id.college_id
				and st.student_id = id.student_id
		WHERE
			id.college_id = @CollegeCode
		GROUP BY
			id.college_id,
			id.ssn,
			id.student_id_status,
			st.term_id
		HAVING
			count(distinct st.student_id) > 1;

		-- capture cursor variables
		FETCH NEXT FROM
			CollegeCursor
		INTO
			@CollegeCode,
			@CollegeIpedsCode;
	END;

	CLOSE CollegeCursor;
	-- trash cursor
	DEALLOCATE CollegeCursor;

END;