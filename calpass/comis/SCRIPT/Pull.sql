DECLARE
	@sql nvarchar(max) = N'',
	@term_start char(3) = '055',
	@term_end char(3) = '171';

DECLARE
	@Tab
AS TABLE
	(
		TableName sysname,
		Prepend varchar(4),
		SubmitType varchar(6)
	);

INSERT INTO
	@Tab
	(
		TableName,
		Prepend,
		SubmitType
	)
VALUES
	('cbcrsinv_prod', 'cb_', 'term'),
	('cwappl_prod', 'cw_', 'term'),
	('cwwork_prod', 'cw_', 'term'),
	('sbstudnt_production', 'sc_', 'annual'),
	('sddsps_prod', 'sd_', 'term'),
	('seeops_prod', 'se_', 'term'),
	('sfappl_prod', 'sf_', 'term'),
	('sfawards_prod', 'sfa_', 'term'),
	('sgpops_prod', 'sg_', 'term'),
	('smmatric_prod', 'sm_', 'term'),
	('spawards_prod', 'sp_', 'term'),
	('sssuccess_prod', 'ss_', 'term'),
	('stterm_prod', 'st_', 'term'),
	('studntid_production', '', 'annual'),
	('svvatea_prod', 'sv_', 'term'),
	('sxenrlm_prod', 'sx_', 'term'),
	('xbsecton_prod', 'xb_', 'term'),
	('xeassign_prod', 'xe_', 'term'),
	('xfsesion_prod', 'xf_', 'term');

SELECT
	@sql += 'SELECT * INTO comis.dbo.' + t.TableName + ' FROM [10.10.5.157].ERDB01.dbo.' + 
		t.TableName + ' WHERE ' + t.Prepend + 'college_id in (''345'',''411'',''451'')' + 
		+ case
			when SubmitType = 'term' then	' and ' + t.Prepend + 'term_id >= @term_start and ' +  t.Prepend + 'term_id <= @term_end;'
			else ';'
		end + char(13) + char(10)
FROM
	@Tab t

EXECUTE sp_executesql
	@sql,
	N'@term_start char(3), @term_end char(3)',
	@term_start = @term_start,
	@term_end = @term_end;