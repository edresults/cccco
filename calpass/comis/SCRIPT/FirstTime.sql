-- example student with mutation
-- first year local student id submitted, second year social submitted
-- result is two cccco_assigned_student_id
convert(binary(64), '00e9cfe80ca08397cfa33a0a5c159bfc2d5f773ffd945f7c5b7b118f0e4f9f80a15fdd3525b4fb9b81322ca538469865eadd4b99d81cbcb07922a780fe260d81', 2)
-- example student with subject taken at 2+ colleges
convert(binary(64), '0352c31885949f52b27c80dd500f29ebd5f1d037c54773502d02d9ed43dc0ebc8a0cf9938da9cfadcc29bc49922d7582f7a4c699b31618d1d17f5482c86d1c6b', 2)
-- clearly same student with ssn not updated properly causing 2 CCCCO student_id values
convert(binary(64), '00007ABA7600E9CCB292F9C7152C3115F24B4B8D4F7649E7F91B4986B8B8E442F8B0D3CF58BEB36EC6C53BD2AD079E8BFEDD4497FDF5DC74C028C6C1DECB6A19', 2)