SELECT
	t.AcademicYear,
	f.FirstTermCr,
	Count = count(f.InterSegmentKey)
FROM
	comis.firstterm f
	inner join
	comis.Term t
		on f.FirstTermCr = t.YearTermCode
GROUP BY
	t.AcademicYear,
	f.FirstTermCr;

SELECT
	t.AcademicYear,
	f.FirstTermCrNsa,
	Count = count(f.InterSegmentKey)
FROM
	comis.firstterm f
	inner join
	comis.Term t
		on f.FirstTermCrNsa = t.YearTermCode
GROUP BY
	t.AcademicYear,
	f.FirstTermCrNsa;