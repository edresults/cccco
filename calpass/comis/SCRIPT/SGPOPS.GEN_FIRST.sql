SET NOCOUNT ON;
SET XACT_ABORT ON;

DECLARE
	@Message     nvarchar(2048),
	@True        bit = 1,
	@False       bit = 0,
	@CollegeCode char(3);

BEGIN

	IF (CURSOR_STATUS('local','CursorCollege') >= 0)
		-- open
		BEGIN
			CLOSE CursorCollege;
		END;

	IF (CURSOR_STATUS('local','CursorCollege') = -1)
		-- closed
		BEGIN
			DEALLOCATE CursorCollege;
		END;

	-- declare
	DECLARE
		CursorCollege
	CURSOR
		FAST_FORWARD
		LOCAL
	FOR
		SELECT
			CollegeCode
		FROM
			comis.College
		ORDER BY
			CollegeCode;
	
	-- open
	OPEN CursorCollege;

	-- fetch
	FETCH NEXT FROM
		CursorCollege
	INTO
		@CollegeCode;

	-- loop
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		RAISERROR(@CollegeCode, 0, 1) WITH NOWAIT;

		UPDATE
			t
		SET
			t.[1st_gen] = sg.gen_first
		FROM
			calpass.dbo.CCStudentProd t
			inner join
			calpass.comis.College c
				on c.IpedsCodeLegacy = t.CollegeId
			inner join
			calpass.comis.SGPOPS sg
				on sg.college_id = c.CollegeCode
				and sg.student_id = t.StudentComisId
				and sg.term_id = t.TermId
		WHERE
			c.CollegeCode = @CollegeCode;

		FETCH NEXT FROM
			CursorCollege
		INTO
			@CollegeCode;
	END;

	IF (CURSOR_STATUS('local','CursorCollege') >= 0)
		-- open
		BEGIN
			CLOSE CursorCollege;
		END;

	IF (CURSOR_STATUS('local','CursorCollege') = -1)
		-- closed
		BEGIN
			DEALLOCATE CursorCollege;
		END;

END;