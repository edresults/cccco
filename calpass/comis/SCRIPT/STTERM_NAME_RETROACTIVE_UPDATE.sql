SET NOCOUNT ON;
SET XACT_ABORT ON;

DECLARE
	@Message      nvarchar(2048),
	@True         bit = 1,
	@False        bit = 0,
	@YearTermCode char(5) = '20115',
	@CollegeCode  char(3);

	DECLARE
		CursorCollege
	CURSOR
		FAST_FORWARD
	FOR
		SELECT
			CollegeCode
		FROM
			comis.College
		ORDER BY
			CollegeCode;

BEGIN
	OPEN CursorCollege;
	
	FETCH NEXT FROM
		CursorCollege
	INTO
		@CollegeCode;

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		RAISERROR(@CollegeCode, 0, 1) WITH NOWAIT;
		-- Reset Escrow Values

		UPDATE
			st3
		SET
			st3.name_first = s.name_first,
			st3.name_last = s.name_last
		FROM
			comis.stterm st3
			inner join
			comis.Term t3
				on t3.TermCode = st3.term_id
			inner join
			(
				SELECT
					st.college_id,
					st.student_id,
					st.name_first,
					st.name_last
				FROM
					comis.stterm st
					inner join
					comis.Term t
						on t.TermCode = st.term_id
				WHERE
					st.college_id = @CollegeCode
					and t.YearTermCode = (
						SELECT
							min(t1.YearTermCode)
						FROM
							comis.stterm st1
							inner join
							comis.Term t1
								on t1.TermCode = st1.term_id
						WHERE
							st1.college_id = st.college_id
							and st1.student_id = st.student_id
							and t1.YearTermCode >= @YearTermCode
					)
				) s
					on s.college_id = st3.college_id
					and s.student_id = st3.student_id
			WHERE
				t3.YearTermCode < @YearTermCode;

		FETCH NEXT FROM
			CursorCollege
		INTO
			@CollegeCode;
	END;

	CLOSE CursorCollege;
	DEALLOCATE CursorCollege;

END;