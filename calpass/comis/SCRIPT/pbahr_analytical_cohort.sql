IF (object_id('comis.first') is not null)
	BEGIN
		DROP TABLE comis.first;
	END;

GO

SELECT DISTINCT
	id.InterSegmentKey
INTO
	comis.first
FROM
	comis.studntid id
	inner join
	comis.sxenrlm sx
		on sx.college_id = id.college_id
		and sx.student_id = id.student_id
	inner join
	comis.Grade g
		on g.GradeCode = sx.grade
	inner join
	comis.Term t
		on t.TermCode = sx.term_id
	inner join
	comis.cbcrsinv cb
		on cb.college_id = sx.college_id
		and cb.term_id = sx.term_id
		and cb.course_id = sx.course_id
		and cb.control_number = sx.control_number
	inner join
	comis.Program p
		on p.ProgramCode = cb.top_code
WHERE
	-- Only enrollments greater that one unit
	-- Effectively remove support courses
	sx.units_attempted > 1
	-- T = Transferable; Credit; Degree Applicable
	-- D = Not Transferable; Credit; Degree Applicable
	-- C = Not Transferable; Credit; Not Degree Applicable
	-- S = Not Transferable; Credit; Not Degree Applicable; Basic Skills
	and sx.credit_flag in ('T','D','C','S')
	-- Only enrollments included in the success denominator
	and g.success_denominator_ind = 1
	and (
		(
			-- Taxonomy of Programs code associated with MATH (170100)
			p.SubjectCode = 'MATH'
			and
			(
				-- Y = Not applicable
				-- Any course prior to college level
				cb.prior_to_college != 'Y'
				or
				(
					-- Y = Not applicable
					-- If course is college level, then it must be transferable
					-- used as another condition to rule out support courses
					cb.prior_to_college = 'Y'
					and
					-- A = Transferable to both UC and CSU
					-- B = Transferable to CSU only
					cb.transfer_status in ('A', 'B')
				)
			)
			-- Only identifiable Mathematics courses for which a student enrolled
			and
			(
				cb.title like '%alg%'
				or
				cb.title like '%trig%'
				or
				cb.title like '%arith%'
				or
				cb.title like '%basic%'
				or
				cb.title like '%calc%'
				or
				cb.title like '%diff%'
				or
				cb.title like '%stat%'
				or
				cb.title like '%geo%'
			)
		)
		or
		(
			-- Taxonomy of Programs code associated with ENGL (150100)
			p.SubjectCode = 'ENGL'
			and
			-- No literature classes; this _should_ only apply to college level coursework
			cb.Title not like '%LIT%'
			and
			(
				-- Any course prior to college level
				cb.prior_to_college != 'Y'
				or
				(
					-- Y = Not applicable
					-- If course is college level, then it must be transferable to both UC and CSU
					-- used as another condition to rule out support courses
					cb.prior_to_college = 'Y'
					and
					-- A = Transferable to both UC and CSU
					cb.transfer_status = 'A'
				)
			)
		)
	)
	-- only records that would have been included in MMAP Phase II 
	and t.YearTermCode <= '20154'
	-- students having a K12 student record
	and exists (
			SELECT
				1
			FROM
				dbo.K12StudentProd s
			WHERE
				s.Derkey1 = id.InterSegmentKey
				-- students having a K12 course record
				and exists (
					SELECT
						1
					FROM
						dbo.K12CourseProd c
						inner join
						calpads.Mark m
							on c.Grade = m.MarkCode
					WHERE
						-- students having a Mark used for GPA
						m.IsGpa = 1
						and c.School = s.School
						and c.LocStudentId = s.LocStudentId
						and c.AcYear = s.AcYear
				)
				and s.GradeLevel in ('09', '10', '11', '12')
			HAVING
				-- students having all four years of high school
				count(distinct s.GradeLevel) = 4
	);

CREATE CLUSTERED INDEX
	ic_first__ISK
ON
	comis.first
	(
		InterSegmentKey
	);