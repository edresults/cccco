DECLARE
	@UserId int = 5552,
	@FileFolder varchar(255) = '\\10.11.6.43\c$\Data\CC\COMIS\Processing',
	@OrganizationId int = 175722,
	@BatchId uniqueidentifier = '15C0983D-C5DC-46E8-B15F-38B0AF4AA45F';

INSERT
	dbo.calpassplus_filedrop
	(
		OrganizationId,
		BatchId,
		SubmissionDateTime,
		ProcessedDateTime,
		Status,
		FileLocation,
		OriginalFileName,
		TempFileName,
		ErrorMessage,
		UserId,
		SubmissionFileSource,
		Removed,
		SubmissionFileDetails,
		SubmissionFileDescriptionId
	)
SELECT
	OrganizationId              = @OrganizationId,
	BatchId                     = @BatchId,
	SubmissionDateTime          = getdate(),
	ProcessedDateTime           = null,
	Status                      = 'New',
	FileLocation                = @FileFolder,
	OriginalFileName            = convert(char(36), @BatchId) + '.' + FileName,
	TempFileName                = @FileFolder + char(92) + convert(char(36), @BatchId) + '.' + FileName,
	ErrorMessage                = null,
	UserId                      = @UserId,
	SubmissionFileSource        = 'Manual',
	Removed                     = null,
	SubmissionFileDetails       = null,
	SubmissionFileDescriptionId = SubmissionFileDescriptionId
FROM
	(
		VALUES
		(11,'CBCRSINV', 'CalPASS_CBCRSINV_2018-08-02_0851 .txt'),
		(11,'HF_FIRST', 'CalPASS_HF_FIRST_2018-08-02_0851 .txt'),
		(11,'SBSTUDNT', 'CalPASS_SBSTUDNT_2018-08-02_0851 .txt'),
		(11,'SDDSPS',   'CalPASS_SDDSPS_2018-08-02_0851 .txt'),
		(11,'SEEOPS',   'CalPASS_SEEOPS_2018-08-02_0851 .txt'),
		(11,'SFAWARDS', 'CalPASS_SFAWARDS_2018-08-02_0851 .txt'),
		(11,'SGPOPS',   'CalPASS_SGPOPS_2018-08-02_0851 .txt'),
		(11,'SMMATRIC', 'CalPASS_SMMATRIC_2018-08-02_0851 .txt'),
		(11,'SPAWARDS', 'CalPASS_SPAWARDS_2018-08-02_0851 .txt'),
		(11,'SSSUCCESS','CalPASS_SSSUCCESS_2018-08-02_0851 .txt'),
		(11,'STTERM',   'CalPASS_STTERM_2018-08-02_0851 .txt'),
		(11,'STUDNTID', 'CalPASS_STUDNTID_2018-08-02_0851 .txt'),
		(11,'SVVATEA',  'CalPASS_SVVATEA_2018-08-02_0851 .txt'),
		(11,'SXENRLM',  'CalPASS_SXENRLM_2018-08-02_0851 .txt'),
		(11,'XBSECTON', 'CalPASS_XBSECTON_2018-08-02_0851 .txt'),
		(11,'XFSESION', 'CalPASS_XFSESION_2018-08-02_0851 .txt'),
		(21,'EDDUI',    'EDDUI_NAICS_8_2018.txt')
	) t
	(
		SubmissionFileDescriptionId,
		TableName,
		FileName
	)