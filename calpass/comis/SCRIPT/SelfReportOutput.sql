USE calpass;

GO

IF (object_id('mmap.SelfReportOutput') is not null)
	BEGIN
		DROP TABLE mmap.SelfReportOutput;
	END;

GO

SELECT
	s.Derkey1,
	SelfReportEleventhGrade = sr.completed_eleventh_grade,
	TranscriptGradeLevel = sh.Attribute,
	SelfReportCgpa = sr.grade_point_average,
	TranscriptCgpa = sh.Value,
	SelfReportEnglishName = sre.Description,
	SelfReportEnglishPoints = m1.Points,
	SelfReportMathTakenName = srm1.Description,
	SelfReportMathTakenPoints = m2.Points,
	SelfReportMathPassedName = srm2.Description,
	SelfReportMathPassedPoints = m3.Points,
	TranscriptRemedialMarkPoints = e.RemedialMarkPoints,
	TranscriptEnglish09MarkPoints = e.English09MarkPoints,
	TranscriptEnglish10MarkPoints = e.English10MarkPoints,
	TranscriptEnglish11MarkPoints = e.English11MarkPoints,
	TranscriptEnglish12MarkPoints = e.English12MarkPoints,
	TranscriptExpositoryMarkPoints = e.ExpositoryMarkPoints,
	TranscriptLiteratureAPMarkPoints = e.LiteratureAPMarkPoints,
	TranscriptEnglishAPMarkPoints = e.EnglishAPMarkPoints,
	ArithmeticMarkPoints = m.ArithmeticMarkPoints,
	PreAlgebraMarkPoints = m.PreAlgebraMarkPoints,
	AlgebraIMarkPoints = m.AlgebraIMarkPoints,
	GeometryMarkPoints = m.GeometryMarkPoints,
	AlgebraIIMarkPoints = m.AlgebraIIMarkPoints,
	StatisticsMarkPoints = m.StatisticsMarkPoints,
	TrigonometryMarkPoints = m.TrigonometryMarkPoints,
	PreCalculusMarkPoints = m.PreCalculusMarkPoints,
	CalculusIMarkPoints = m.CalculusIMarkPoints,
	CalculusIIMarkPoints = m.CalculusIIMarkPoints
INTO
	mmap.SelfReportOutput
FROM
	comis.SelfReport sr
	inner join
	dbo.Student s
		on sr.InterSegmentKey = s.InterSegmentKeyBug
	cross apply
	dbo.HSSummaryHashGet(s.Derkey1) sh
	left outer join
	calpads.Mark m1
		on m1.MarkCode = sr.highest_english_grade
	left outer join
	calpads.Mark m2
		on m2.MarkCode = sr.highest_math_taken_grade
	left outer join
	calpads.Mark m3
		on m3.MarkCode = sr.highest_math_passed_grade
	left outer join
	comis.SelfReportEngl sre
		on sre.EnglId = sr.highest_english_course
	left outer join
	comis.SelfReportMath srm1
		on srm1.MathId = sr.highest_math_course_taken
	left outer join
	comis.SelfReportMath srm2
		on srm2.MathId = sr.highest_math_course_passed
	outer apply
	dbo.HSEnglGet(s.Derkey1) e
	outer apply
	dbo.HSMathGet(s.Derkey1) m
WHERE
	sh.Entity = 'Cgpa';

-- extract
EXECUTE xp_cmdshell
	'bcp "SELECT * FROM calpass.mmap.SelfReportOutput" QUERYOUT "C:\Users\dlamoree\Desktop\SelfReportOutput.txt" -T -c -q';
-- extract
EXECUTE dbo.sp_spss_output
	'calpass',
	'mmap',
	'SelfReportOutput',
	'C:\Users\dlamoree\desktop\',  -- '
	'1';

GO

-- upload
DECLARE
	@user nvarchar(255) = 'dlamoree',
	@password nvarchar(255) = '',
	@server nvarchar(255) = '174.127.112.130',
	@port nvarchar(255) = '22',
	@ssh_rsa nvarchar(255) = 'f7:0c:30:21:0d:d4:bf:5c:c2:79:f4:86:6f:21:3c:75';

EXECUTE sp_sftp_winscp_upload
	@user = @user,
	@password = @password,
	@server = @server,
	@port = @port,
	@ssh_rsa = @ssh_rsa,
	@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
	@source_file_name = 'SelfReportOutput.txt',
	@target_file_dir = '/home/steps2014/SelfReport/';

EXECUTE sp_sftp_winscp_upload
	@user = @user,
	@password = @password,
	@server = @server,
	@port = @port,
	@ssh_rsa = @ssh_rsa,
	@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
	@source_file_name = 'SelfReportOutput_spss_import.sps',
	@target_file_dir = '/home/steps2014/SelfReport/';

GO

-- delete
EXECUTE master.dbo.clr_file_delete
	@file_path = 'C:\Users\dlamoree\desktop\SelfReportOutput.txt';

EXECUTE master.dbo.clr_file_delete
	@file_path = 'C:\Users\dlamoree\desktop\SelfReportOutput_spss_import.sps';

GO

-- clean up
DROP TABLE mmap.SelfReportOutput;