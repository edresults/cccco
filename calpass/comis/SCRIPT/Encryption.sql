OPEN SYMMETRIC KEY
	SecPii
DECRYPTION BY CERTIFICATE
	SecPii;

GO

DECLARE
	@college_id char(3),
	@iterator int = 0,
	@ubound int;

DECLARE
	@colleges
AS TABLE
	(
		id int identity(0,1),
		college_id char(3)
	);

INSERT INTO
	@colleges
	(
		college_id
	)
SELECT
	CollegeCode
FROM
	calpass.comis.College
ORDER BY
	CollegeCode;

SELECT
	@ubound = count(*)
FROM
	@colleges;

OPEN SYMMETRIC KEY
	SecPii
DECRYPTION BY CERTIFICATE
	SecPii;

WHILE (@iterator < @ubound)
BEGIN
	SELECT
		@college_id = college_id
	FROM
		@colleges
	WHERE
		id = @iterator;

	RAISERROR(@college_id, 0, 1) WITH NOWAIT;

	UPDATE
		calpass.comis.stterm
	SET
		name_first_enc = EncryptByKey(Key_GUID('SecPii'), name_first),
		name_first = null,
		name_last_enc = EncryptByKey(Key_GUID('SecPii'), name_last),
		name_last = null,
		gender_enc = EncryptByKey(Key_GUID('SecPii'), gender),
		gender = null,
		multi_race_enc = EncryptByKey(Key_GUID('SecPii'), multi_race),
		multi_race = null,
		ipeds_race_enc = EncryptByKey(Key_GUID('SecPii'), ipeds_race),
		ipeds_race = null,
		race_enc = EncryptByKey(Key_GUID('SecPii'), race),
		race = null
	WHERE
		college_id = @college_id
		and (
			name_first is not null
			or name_last is not null
			or gender is not null
			or multi_race is not null
			or ipeds_race is not null
			or race is not null
		);

	UPDATE
		comis.sbstudnt
	SET
		birthdate_enc = EncryptByKey(Key_GUID('SecPii'), birthdate)
	WHERE
		college_id = @college_id
		and birthdate is not null;

	SET @iterator += 1;
END;

CLOSE SYMMETRIC KEY	SecPii;