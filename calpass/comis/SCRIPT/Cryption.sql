DECLARE @CollegeCode char(3);

DECLARE
	College
CURSOR
	local
	forward_only
	fast_forward
FOR
	SELECT
		CollegeCode
	FROM
		comis.College
	ORDER BY
		CollegeCode;

OPEN College;

FETCH NEXT FROM
	College
INTO
	@CollegeCode;

WHILE (@@FETCH_STATUS = 0)
BEGIN

	RAISERROR(@CollegeCode, 0, 1) WITH NOWAIT;

	EXEC comis.Cryption
		@CollegeCode = @Collegecode;

	FETCH NEXT FROM
		College
	INTO
		@CollegeCode;
END;

CLOSE College;
DEALLOCATE College;