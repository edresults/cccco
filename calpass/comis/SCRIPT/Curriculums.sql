SET NOCOUNT ON;

GO

DROP TABLE IF EXISTS comis.Curriculums;

GO

CREATE TABLE
    comis.Curriculums
    (
        CollegeCode    char(3)      not null,
        ControlNumber  char(5)      not null,
        Title          varchar(255) not null,
        TopCode        char(6)      not null,
        ProgramAward   char(1)      not null,
        CreditType     char(1)      not null,
        CDCP           char(1)          null,
        ApprovedDate   date             null,
        ProposalStatus varchar(9)   not null,
        InactiveDate   datetime2        null,
        TotalHours     decimal(7,1) not null,
        TotalUnits     decimal(7,1) not null,
        GainfulEmp     bit          not null,
        AAT_AST        char(1)          null,
        PHCAST         bit              null,
        CONSTRAINT PK_Curriculums PRIMARY KEY CLUSTERED ( CollegeCode, ControlNumber )
    );

GO

IF (object_id('tempdb.dbo.#Curriculums') is not null)
    BEGIN
        DROP TABLE #Curriculums;
    END;

GO

DECLARE
    @DoubleQuote         char(1)      = '"',
    @BlankString         varchar(1)   = '',
    @TitleDefault        varchar(255) = 'No Title',
    @TopCodeDefault      char(6)      = 'XXXXXX',
    @ProgramAwardNil     char(3)      = 'NIL',
    @ProgramAwardDefault char(1)      = 'X',
    @GainfulEmpTrue      char(4)      = 'TRUE',
    @True                bit          = 1;


SELECT
    CollegeCode      = a.CollegeCode,
    ControlNumber    = a.ControlNumber,
    Title            = isnull(replace(title, @DoubleQuote, @BlankString), @TitleDefault),
    TopCode          = isnull(a.TopCode, @TopCodeDefault),
    ProgramAward     = case when upper(ProgramAward) = @ProgramAwardNil then @ProgramAwardDefault when ProgramAward is null then @ProgramAwardDefault else ProgramAward end,
    CreditType       = a.CreditType,
    CDCP             = a.CDCP,
    ApprovedDate     = convert(date, a.ApprovedDate),
    ProposalStatus   = a.ProposalStatus,
    InactiveDate     = convert(datetime2, replace(a.InactiveDate, @DoubleQuote, @BlankString)),
    TotalHours       = convert(decimal(7,1), TotalHours),
    TotalUnits       = convert(decimal(7,1), TotalUnits),
    GainfulEmp       = case when upper(GainfulEmp) = @GainfulEmpTrue then @True else ~@True end,
    AAT_AST          = a.AAT_AST,
    PHCAST           = a.PHCAST
INTO
    #Curriculums
FROM
    OPENROWSET(
        BULK N'\\10.11.6.43\C$\Data\CC\COMIS\Processing\ProgramMisView201221.txt',
        FORMATFILE = N'\\10.11.6.43\C$\Data\CC\COMIS\Formats\Curriculums.fmt',
        FIRSTROW = 2
    ) a

PRINT 'Inserting Raw Data: ' + convert(varchar, @@rowcount);

DELETE
    a
FROM
    #Curriculums a
WHERE
    ControlNumber is null;

PRINT 'Deleting Records With Null ControlNumber: ' + convert(varchar, @@rowcount);

DELETE
    a
FROM
    #Curriculums a
where 
    len(ltrim(rtrim(ControlNumber))) = 0;

PRINT 'Deleting Records With Blank ControlNumber: ' + convert(varchar, @@rowcount);

DELETE
    a
FROM
    #Curriculums a
where 
    len(ltrim(rtrim(TopCode))) = 0;

PRINT 'Deleting Records With Blank TopCode: ' + convert(varchar, @@rowcount);

DELETE
    a
FROM
    #Curriculums a
WHERE
    isnull(a.ApprovedDate, convert(date, getdate())) <> (
        SELECT
            max(isnull(b.ApprovedDate, convert(date, getdate())))
        FROM
            #Curriculums b
        WHERE
            a.ControlNumber = b.ControlNumber
    );

PRINT 'Deleting Records Without Max ApprovedDate: ' + convert(varchar, @@rowcount);

DELETE
    a
FROM
    #Curriculums a
WHERE
    isnull(a.InactiveDate, sysdatetime()) <> (
        SELECT
            max(isnull(b.InactiveDate, sysdatetime()))
        FROM
            #Curriculums b
        WHERE
            a.ControlNumber = b.ControlNumber
    );

PRINT 'Deleting Records Without Max InactiveDate: ' + convert(varchar, @@rowcount);

INSERT INTO
    comis.Curriculums
SELECT
    *
FROM
    #Curriculums;

PRINT 'Inserting Records Into Production Table: ' + convert(varchar, @@rowcount);