WITH
	cte
	(
		FileName,
		FileColumnName,
		FileColumnIndex
	)
AS
	(
		SELECT
			FileName,
			FileColumnName = ltrim(rtrim(substring(col, [Number], charindex('	', col + '	', [Number]) - [Number]))),
			FileColumnIndex = row_number() over(partition by FileName order by (Number)) - 1
		FROM
			(
				SELECT
					Number = ROW_NUMBER() OVER (ORDER BY name)
				FROM
					sys.all_objects
			) x
			cross join
			(
				SELECT
					FileName,
					col = master.dbo.clr_file_line_select(v.FilePath, 1)
				FROM
					(
						VALUES
						-- ('CBCRSINV','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_CBCRSINV_2017-07-12_0734 .txt'),
						-- ('HF_FIRST','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_HF_FIRST_2017-07-12_0734 .txt'),
						-- ('SBSTUDNT','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_SBSTUDNT_2017-07-12_0734 .txt'),
						-- ('SDDSPS','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_SDDSPS_2017-07-12_0734 .txt'),
						-- ('SEEOPS','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_SEEOPS_2017-07-12_0734 .txt'),
						-- ('SFAWARDS','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_SFAWARDS_2017-07-12_0734 .txt'),
						-- ('SGPOPS','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_SGPOPS_2017-07-12_0734 .txt'),
						-- ('SMMATRIC','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_SMMATRIC_2017-07-12_0734 .txt'),
						-- ('SPAWARDS','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_SPAWARDS_2017-07-12_0734 .txt'),
						-- ('SSSUCCESS','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_SSSUCCESS_2017-07-12_0734 .txt'),
						('STTERM','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_STTERM_2017-07-12_0734 .txt')
						-- ('STUDNTID','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_STUDNTID_2017-07-12_0734 .txt'),
						-- ('SVVATEA','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_SVVATEA_2017-07-12_0734 .txt'),
						-- ('SXENRLM','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_SXENRLM_2017-07-12_0734 .txt'),
						-- ('XBSECTON','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_XBSECTON_2017-07-12_0734 .txt'),
						-- ('XFSESION','C:\Data\CC\COMIS\Processing\770FEE03-C40E-4A2F-8916-2956042AA965.CalPASS_XFSESION_2017-07-12_0734 .txt')
					) v
					(
						FileName,
						FilePath
					)
			) y
		WHERE
			Number <= len(col)
			and substring('	' + col, [Number], len('	')) = '	'
	)
SELECT
	t.name,
	y.FileName,
	y.FileColumnName,
	y.FileColumnIndex,
	c.Name,
	d.Name,
	case
		when FileColumnIndex is null then N''
		else '[ColumnMapping(' + convert(varchar, FileColumnIndex) + ',' + convert(varchar, column_id - 1) + ')]' + char(13) + char(10)
	end +
	'[ColumnMetaData("' + c.name + '", SqlDbType.' + 
		replace(
			replace(
				replace(
						upper(substring(d.name, 1, 1)) + substring(d.name, 2, len(d.name)),
						'char',
						'Char'
					),
				'Datetime',
				'DateTime'
			),
			'int',
			'Int'
		) + ', ' + 
		case
			when d.name = 'decimal' then convert(varchar, c.precision) + ',' + convert(varchar, c.scale)
			when d.name like '%int%' then convert(varchar, c.precision)
			when d.name like '%char%' then convert(varchar, c.max_length)
			when d.name like '%date%' then '10'
		end + ')]' + char(13) + char(10) +
	'public ' +
		case
			when d.name = 'decimal' then 'Decimal'
			when d.name = 'int' then 'int'
			when d.name = 'smallint' then 'Int16'
			when d.name = 'bigint' then 'Int64'
			when d.name = 'tinyint' then 'byte'
			when d.name like '%char%' then 'string'
			when d.name like '%date%' then 'DateTime'
			when d.name = 'uniqueidentifier' then 'Guid'
		end + 
		case
			when d.name not like '%char%' and c.is_nullable = 1 then '?'
			else ''
		end + ' ' + c.Name + ' { get; set; }'
FROM
	sys.tables t
	inner join
	sys.columns c
		on t.object_id = c.object_id
	inner join
	sys.types d
		on d.system_type_id = c.system_type_id
	left outer join
	cte y
		on t.Name = y.FileName
		and c.name = y.FileColumnName
WHERE
	exists (
		SELECT
			1
		FROM
			cte b
		WHERE
			b.FileName = t.name
	)
	and c.name != 'IsEscrow'
ORDER BY
	t.name,
	c.column_id;