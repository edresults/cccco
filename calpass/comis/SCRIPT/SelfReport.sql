USE calpass;

GO

MERGE
	comis.SelfReport t
USING
	(
		SELECT
			InterSegmentKey,
			completed_eleventh_grade,
			grade_point_average,
			highest_english_course,
			highest_english_grade,
			highest_math_course_taken,
			highest_math_taken_grade,
			highest_math_course_passed,
			highest_math_passed_grade
		FROM
			(
				SELECT
					InterSegmentKey = convert(binary(64), a.InterSegmentKey, 2),
					completed_eleventh_grade,
					grade_point_average,
					highest_english_course,
					highest_english_grade,
					highest_math_course_taken,
					highest_math_taken_grade,
					highest_math_course_passed,
					highest_math_passed_grade,
					PartitionSelector =
						row_number() over(
							partition by
								InterSegmentKey
							order by
								(select 1) desc
						),
					PartitionCount = 
						count(*) over(
							partition by
								InterSegmentKey
						)
				FROM
					OPENROWSET(
						BULK
						N'C:\Data\CC\CCC_Apply\Processing\MMAP_Request_1-20150630-20170430.csv',
						FORMATFILE = N'C:\Data\CC\CCC_Apply\Formats\SelfReport.fmt'
					) a
			) b
		WHERE
			PartitionSelector = PartitionCount
	) s
ON
	s.InterSegmentKey = t.InterSegmentKey
WHEN MATCHED THEN
	UPDATE SET
		t.completed_eleventh_grade = s.completed_eleventh_grade,
		t.grade_point_average = s.grade_point_average,
		t.highest_english_course = s.highest_english_course,
		t.highest_english_grade = s.highest_english_grade,
		t.highest_math_course_taken = s.highest_math_course_taken,
		t.highest_math_taken_grade = s.highest_math_taken_grade,
		t.highest_math_course_passed = s.highest_math_course_passed,
		t.highest_math_passed_grade = s.highest_math_passed_grade
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			InterSegmentKey,
			completed_eleventh_grade,
			grade_point_average,
			highest_english_course,
			highest_english_grade,
			highest_math_course_taken,
			highest_math_taken_grade,
			highest_math_course_passed,
			highest_math_passed_grade
		)
	VALUES
		(
			s.InterSegmentKey,
			s.completed_eleventh_grade,
			s.grade_point_average,
			s.highest_english_course,
			s.highest_english_grade,
			s.highest_math_course_taken,
			s.highest_math_taken_grade,
			s.highest_math_course_passed,
			s.highest_math_passed_grade
		);