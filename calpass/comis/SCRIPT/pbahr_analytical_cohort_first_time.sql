IF (object_id('comis.firstterm') is not null)
	BEGIN
		DROP TABLE comis.firstterm;
	END;

GO

CREATE TABLE
	comis.firstterm
	(
		InterSegmentKey binary(64),
		FirstTerm char(5),
		FirstTermCr char(5),
		FirstTermCrNsa char(5),
		FirstTermNc char(5),
		FirstTermNcNsa char(5)
		CONSTRAINT
			pk_firstterm
		PRIMARY KEY CLUSTERED
			(
				InterSegmentKey
			)
	);

GO

INSERT
	comis.firstterm
	(
		InterSegmentKey
	)
SELECT
	s.InterSegmentKey
FROM
	comis.pbahr p
	inner join
	dbo.Student s with(index(pk_student))
		on p.Derkey1 = s.Derkey1;

GO

UPDATE
	f
SET
	f.FirstTerm = a.FirstTerm,
	f.FirstTermCr = a.FirstTermCr,
	f.FirstTermCrNsa = a.FirstTermCrNsa,
	f.FirstTermNc = a.FirstTermNc,
	f.FirstTermNcNsa = a.FirstTermNcNsa
FROM
	comis.firstterm f
	inner join
	(
		SELECT
			id.InterSegmentKey,
			FirstTerm = min(t.YearTermCode),
			FirstTermCr = 
				min(
					case
						when cb.credit_status in ('C', 'D') then t.YearTermCode
					end
				),
			FirstTermCrNsa = 
				min(
					case
						when cb.credit_status in ('C', 'D')
							and st.enrollment != 'Y' then t.YearTermCode
					end
				),
			FirstTermNc = 
				min(
					case
						when cb.credit_status = 'N' then t.YearTermCode
					end
				),
			FirstTermNcNsa = 
				min(
					case
						when cb.credit_status = 'N'
							and st.enrollment != 'Y' then t.YearTermCode
					end
				)
		FROM
			comis.firstterm f
			inner join
			comis.studntid id
				on f.InterSegmentKey = id.InterSegmentKey
			inner join
			comis.stterm st
				on st.college_id = id.college_id
				and st.student_id = id.student_id
			inner join
			comis.sxenrlm sx
				on sx.college_id = st.college_id
				and sx.student_id = st.student_id
				and sx.term_id = st.term_id
			inner join
			comis.Grade g
				on g.GradeCode = sx.grade
			inner join
			comis.Term t
				on t.TermCode = st.term_id
			inner join
			comis.cbcrsinv cb
				on cb.college_id = sx.college_id
				and cb.term_id = sx.term_id
				and cb.course_id = sx.course_id
				and cb.control_number = sx.control_number
		WHERE
			t.YearTermCode <= '20154' -- get check
		GROUP BY
			id.InterSegmentKey
	) a
		on a.InterSegmentKey = f.InterSegmentKey;