SELECT
    upper(substr(NameFirst,1,1))::bytea || E'\\000'::bytea ||
    upper(substr(NameFirst,2,1))::bytea || E'\\000'::bytea ||
    upper(substr(NameFirst,3,1))::bytea || E'\\000'::bytea ||
    upper(substr(NameLast,1,1))::bytea || E'\\000'::bytea ||
    upper(substr(NameLast,2,1))::bytea || E'\\000'::bytea ||
    upper(substr(NameLast,3,1))::bytea || E'\\000'::bytea ||
    upper(Gender)::bytea || E'\\000'::bytea ||
    substr(to_char(extract(year from Birthdate), '0000'), 2, 1)::bytea || E'\\000'::bytea ||
    substr(to_char(extract(year from Birthdate), '0000'), 3, 1)::bytea || E'\\000'::bytea ||
    substr(to_char(extract(year from Birthdate), '0000'), 4, 1)::bytea || E'\\000'::bytea ||
    substr(to_char(extract(year from Birthdate), '0000'), 5, 1)::bytea || E'\\000'::bytea ||
    substr(to_char(extract(month from Birthdate), '00'), 2, 1)::bytea || E'\\000'::bytea ||
    substr(to_char(extract(month from Birthdate), '00'), 3, 1)::bytea || E'\\000'::bytea ||
    substr(to_char(extract(day from Birthdate), '00'), 2, 1)::bytea || E'\\000'::bytea ||
    substr(to_char(extract(day from Birthdate), '00'), 3, 1)::bytea || E'\\000'::bytea as ByteArray,
    upper(substr(NameFirst,1,1))::bytea || '\\000'::bytea ||
    upper(substr(NameFirst,2,1))::bytea || '\\000'::bytea ||
    upper(substr(NameFirst,3,1))::bytea || '\\000'::bytea ||
    upper(substr(NameLast,1,1))::bytea || '\\000'::bytea ||
    upper(substr(NameLast,2,1))::bytea || '\\000'::bytea ||
    upper(substr(NameLast,3,1))::bytea || '\\000'::bytea ||
    upper(Gender)::bytea || E'\\000'::bytea ||
    substr(to_char(extract(year from Birthdate), '0000'), 2, 1)::bytea || '\\000'::bytea ||
    substr(to_char(extract(year from Birthdate), '0000'), 3, 1)::bytea || '\\000'::bytea ||
    substr(to_char(extract(year from Birthdate), '0000'), 4, 1)::bytea || '\\000'::bytea ||
    substr(to_char(extract(year from Birthdate), '0000'), 5, 1)::bytea || '\\000'::bytea ||
    substr(to_char(extract(month from Birthdate), '00'), 2, 1)::bytea || '\\000'::bytea ||
    substr(to_char(extract(month from Birthdate), '00'), 3, 1)::bytea || '\\000'::bytea ||
    substr(to_char(extract(day from Birthdate), '00'), 2, 1)::bytea || '\\000'::bytea ||
    substr(to_char(extract(day from Birthdate), '00'), 3, 1)::bytea || '\\000'::bytea as ByteArrayError,
	encode(
		upper(substr(NameFirst,1,1))::bytea || E'\\000'::bytea ||
		upper(substr(NameFirst,2,1))::bytea || E'\\000'::bytea ||
		upper(substr(NameFirst,3,1))::bytea || E'\\000'::bytea ||
		upper(substr(NameLast,1,1))::bytea || E'\\000'::bytea ||
		upper(substr(NameLast,2,1))::bytea || E'\\000'::bytea ||
		upper(substr(NameLast,3,1))::bytea || E'\\000'::bytea ||
		upper(Gender)::bytea || E'\\000'::bytea ||
		substr(to_char(extract(year from Birthdate), '0000'), 2, 1)::bytea || E'\\000'::bytea ||
		substr(to_char(extract(year from Birthdate), '0000'), 3, 1)::bytea || E'\\000'::bytea ||
		substr(to_char(extract(year from Birthdate), '0000'), 4, 1)::bytea || E'\\000'::bytea ||
		substr(to_char(extract(year from Birthdate), '0000'), 5, 1)::bytea || E'\\000'::bytea ||
		substr(to_char(extract(month from Birthdate), '00'), 2, 1)::bytea || E'\\000'::bytea ||
		substr(to_char(extract(month from Birthdate), '00'), 3, 1)::bytea || E'\\000'::bytea ||
		substr(to_char(extract(day from Birthdate), '00'), 2, 1)::bytea || E'\\000'::bytea ||
		substr(to_char(extract(day from Birthdate), '00'), 3, 1)::bytea || E'\\000'::bytea,
		'hex'
	) as Hex,
	encode(
		upper(substr(NameFirst,1,1))::bytea || '\\000'::bytea ||
		upper(substr(NameFirst,2,1))::bytea || '\\000'::bytea ||
		upper(substr(NameFirst,3,1))::bytea || '\\000'::bytea ||
		upper(substr(NameLast,1,1))::bytea || '\\000'::bytea ||
		upper(substr(NameLast,2,1))::bytea || '\\000'::bytea ||
		upper(substr(NameLast,3,1))::bytea || '\\000'::bytea ||
		upper(Gender)::bytea || '\\000'::bytea ||
		substr(to_char(extract(year from Birthdate), '0000'), 2, 1)::bytea || '\\000'::bytea ||
		substr(to_char(extract(year from Birthdate), '0000'), 3, 1)::bytea || '\\000'::bytea ||
		substr(to_char(extract(year from Birthdate), '0000'), 4, 1)::bytea || '\\000'::bytea ||
		substr(to_char(extract(year from Birthdate), '0000'), 5, 1)::bytea || '\\000'::bytea ||
		substr(to_char(extract(month from Birthdate), '00'), 2, 1)::bytea || '\\000'::bytea ||
		substr(to_char(extract(month from Birthdate), '00'), 3, 1)::bytea || '\\000'::bytea ||
		substr(to_char(extract(day from Birthdate), '00'), 2, 1)::bytea || '\\000'::bytea ||
		substr(to_char(extract(day from Birthdate), '00'), 3, 1)::bytea || '\\000'::bytea,
		'hex'
	) as HexError,
	encode(
		digest(
			upper(substr(NameFirst,1,1))::bytea || E'\\000'::bytea ||
			upper(substr(NameFirst,2,1))::bytea || E'\\000'::bytea ||
			upper(substr(NameFirst,3,1))::bytea || E'\\000'::bytea ||
			upper(substr(NameLast,1,1))::bytea || E'\\000'::bytea ||
			upper(substr(NameLast,2,1))::bytea || E'\\000'::bytea ||
			upper(substr(NameLast,3,1))::bytea || E'\\000'::bytea ||
			upper(Gender)::bytea || E'\\000'::bytea ||
			substr(to_char(extract(year from Birthdate), '0000'), 2, 1)::bytea || E'\\000'::bytea ||
			substr(to_char(extract(year from Birthdate), '0000'), 3, 1)::bytea || E'\\000'::bytea ||
			substr(to_char(extract(year from Birthdate), '0000'), 4, 1)::bytea || E'\\000'::bytea ||
			substr(to_char(extract(year from Birthdate), '0000'), 5, 1)::bytea || E'\\000'::bytea ||
			substr(to_char(extract(month from Birthdate), '00'), 2, 1)::bytea || E'\\000'::bytea ||
			substr(to_char(extract(month from Birthdate), '00'), 3, 1)::bytea || E'\\000'::bytea ||
			substr(to_char(extract(day from Birthdate), '00'), 2, 1)::bytea || E'\\000'::bytea ||
			substr(to_char(extract(day from Birthdate), '00'), 3, 1)::bytea || E'\\000'::bytea,
			'sha512'
		),
		'hex'
	) as NCharHash,
	encode(
		digest(
			upper(substr(NameFirst,1,1))::bytea || '\\000'::bytea ||
			upper(substr(NameFirst,2,1))::bytea || '\\000'::bytea ||
			upper(substr(NameFirst,3,1))::bytea || '\\000'::bytea ||
			upper(substr(NameLast,1,1))::bytea || '\\000'::bytea ||
			upper(substr(NameLast,2,1))::bytea || '\\000'::bytea ||
			upper(substr(NameLast,3,1))::bytea || '\\000'::bytea ||
			upper(Gender)::bytea || '\\000'::bytea ||
			substr(to_char(extract(year from Birthdate), '0000'), 2, 1)::bytea || '\\000'::bytea ||
			substr(to_char(extract(year from Birthdate), '0000'), 3, 1)::bytea || '\\000'::bytea ||
			substr(to_char(extract(year from Birthdate), '0000'), 4, 1)::bytea || '\\000'::bytea ||
			substr(to_char(extract(year from Birthdate), '0000'), 5, 1)::bytea || '\\000'::bytea ||
			substr(to_char(extract(month from Birthdate), '00'), 2, 1)::bytea || '\\000'::bytea ||
			substr(to_char(extract(month from Birthdate), '00'), 3, 1)::bytea || '\\000'::bytea ||
			substr(to_char(extract(day from Birthdate), '00'), 2, 1)::bytea || '\\000'::bytea ||
			substr(to_char(extract(day from Birthdate), '00'), 3, 1)::bytea || '\\000'::bytea,
			'sha512'
		),
		'hex'
	) as NCharHashError
FROM
	(
		VALUES
		(
			'Lisa',
			'Guerra',
			'F',
			'1993-11-11'::date
		)
	) t
	(
		NameFirst,
		NameLast,
		Gender,
		Birthdate
	);