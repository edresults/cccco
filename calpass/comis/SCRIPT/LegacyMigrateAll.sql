SET NOCOUNT ON;

DECLARE
	@YearTermCodeStart char(5) = '20165',
	@YearTermCodeEnd char(5) = '20174',
	@IpedsCodeLegacy char(6),
	@CollegeCode char(3);

	DECLARE
		CursorCollege
	CURSOR
		FAST_FORWARD
	FOR
		SELECT
			CollegeCode,
			IpedsCodeLegacy
		FROM
			comis.College
		ORDER BY
			CollegeCode;

	OPEN CursorCollege;
	
	FETCH NEXT FROM
		CursorCollege
	INTO
		@CollegeCode,
		@IpedsCodeLegacy;

OPEN SYMMETRIC KEY
	SecPii
DECRYPTION BY
	CERTIFICATE SecPii;
		
WHILE (@@FETCH_STATUS = 0)
BEGIN
	RAISERROR(@CollegeCode, 0, 1) WITH NOWAIT;
	
		-- CCStudentProd
		INSERT
			calpass.dbo.CCStudentProd
			(
				Derkey1,
				CollegeId,
				TermId,
				StudentId,
				IdStatus,
				CSISNum,
				Birthdate,
				FName,
				LName,
				Gender,
				Race,
				Citizen,
				ZipCode,
				EdStatus,
				HighSchool,
				StudentGoal,
				EnrollStatus,
				UnitsEarnedLoc,
				UnitsEarnedTrn,
				UnitsAttLoc,
				UnitsAttTrn,
				GPointsLoc,
				GPointsTrn,
				AcademicStanding,
				AcademicLevel,
				DSPS,
				EOPS,
				BoggFlag,
				PellFlag,
				FinancialAidFlag,
				Major,
				ccLongTermId,
				military,
				military_dependent,
				foster_care,
				incarcerated,
				mesa,
				puente,
				mchs,
				umoja,
				[1st_gen],
				caa
			)
		SELECT
			id.InterSegmentKey as Derkey1,
			@IpedsCodeLegacy as CollegeId,
			st.term_id as TermId,
			id.ssn as studentId,
			id.student_id_status as IdStatus,
			null as CSISNum,
			-- CONVERT(VARCHAR(8), sb.birthdate, 112) as Birthdate,
			Birthdate = DecryptByKey(sb.birthdate_enc),
			-- CAST(st.name_first AS CHAR(3)) as FName,
			FName = convert(char(3), DecryptByKey(st.name_first_enc)),
			-- CAST(st.name_last AS CHAR(3)) as LName,
			LName = convert(char(3), DecryptByKey(st.name_last_enc)),
			-- st.gender as Gender,
			Gender = convert(char(1), DecryptByKey(st.gender_enc)),
			-- st.ipeds_race as Race,
			Race = convert(char(1), DecryptByKey(st.ipeds_race_enc)),
			st.citizenship as Citizen,
			st.zip as ZipCode,
			st.education as EdStatus,
			st.high_school as HighSchool,
			st.goal as StudentGoal,
			st.enrollment as EnrollStatus,
			st.u_earned_loc as UnitsEarnedLoc,
			st.u_earned_trn as UnitsEarnedTrn,
			st.u_attmpt_loc as UnitsAttLoc,
			st.u_attmpt_trn as UnitsAttTrn,
			st.g_points_loc as GPointsLoc,
			st.g_points_trn as GPointsTrn,
			st.aca_standing as AcademicStanding,
			st.academic_level as AcademicLevel,
			isnull(sd.primary_disability, 'Y') as DSPS,
			isnull(
				case
					when se.eops_care_status = 'N' then 'E'
					when se.eops_care_status = 'C' then 'C'
					when se.eops_care_status = 'P' then 'Y'
				end
			, 'Y') as EOPS,
			isnull(
				(
					SELECT DISTINCT
						'B'
					FROM
						calpass.comis.sfawards sf
					WHERE
						sf.type_id in ('BA','B1','B2','B3','BB','BC','F1','F2','F3','F4','F5')
						and sf.college_id = st.college_id
						and sf.student_id = st.student_id
						and sf.term_recd = st.term_id
				)
			, 'Y') as boggFlag,
			-- cannot use left outer join when there should be a 1:1 match on primary key.
			-- CO does not validate data this: Should not exist: annual term (100) with 105 term.
			isnull(
				(
					SELECT DISTINCT
						'P'
					FROM
						calpass.comis.sfawards sf
					WHERE
						sf.type_id  = 'GP'
						and sf.college_id = st.college_id
						and sf.student_id = st.student_id
						and sf.term_recd = st.term_id
				)
			, 'Y') as pellFlag,
			isnull(
				(
					SELECT
						min(
							case
								when type_id in ('GS','GW','LD','LG','LS','WF') then 'A'
								when type_id in ('GB','GC','GE','GF','LE','WC','WE','WU') then 'B'
								when type_id in ('LH','LP','LR','LL') then 'C'
								when type_id in ('SU','SV','SX') then 'D'
								when type_id in ('GU', 'GV', 'LI', 'LN', 'GG') then 'E'
								else 'Y'
							end
						)
					FROM
						calpass.comis.sfawards sf
					WHERE
						sf.college_id = st.college_id
						and sf.student_id = st.student_id
						and sf.term_recd = st.term_id
					GROUP BY
						sf.college_id,
						sf.student_id,
						sf.term_recd
				)
			, 'Y') as FinancialAidFlag,
			isnull(sm.major, 'XXXXXX') as Major,
			t.YearTermCode as ccLongTermId,
			sg.military,
			sg.military_dependent,
			sg.foster_care,
			sg.incarcerated,
			sg.mesa,
			sg.puente,
			sg.mchs,
			sg.umoja,
			sg.gen_first,
			sg.caa
		FROM
			@Escrow e
			inner join
			comis.stterm st
				on e.CollegeId = st.college_id
				and e.StudentLocalId = st.student_id
				and e.TermId = st.term_id
			inner join
			calpass.comis.Term t
				on t.TermCode = st.term_id
			inner join
			calpass.comis.studntid id
				on st.college_id = id.college_id
				and st.student_id = id.student_id
			inner join
			calpass.comis.sbstudnt sb
				on sb.college_id = st.college_id
				and sb.student_id = st.student_id
			left outer join
			calpass.comis.sddsps sd
				on sd.college_id = st.college_id
				and sd.student_id = st.student_id
				and sd.term_id = st.term_id
			left outer join
			calpass.comis.seeops se
				on st.college_id = se.college_id
				and st.student_id = se.student_id
				and st.term_id = se.term_id
			left outer join
			calpass.comis.smmatric sm
				on sm.college_id = st.college_id
				and sm.student_id = st.student_id
				and sm.term_id = st.term_id
			left outer join
			calpass.comis.sgpops sg
				on sg.college_id = st.college_id
				and sg.student_id = st.student_id
				and sg.term_id = st.term_id
		WHERE
			st.IsEscrow = 1
			and st.college_id = @CollegeCode;
	
		-- CCStudentProd_EXT
		INSERT INTO
			calpass.dbo.CCStudentProd_EXT
			(
				collegeId,
				studentId,
				termId,
				extSB09_ResidenceCode,
				extSB23_ApprenticeshipStatus,
				extSB24_TransferCenterStatus,
				extSB26_JTPAStatus,
				extSB27_CalWorksStatus,
				extSM01_MatricGoals,
				extSM01_MatricGoalsPos1,
				extSM01_MatricGoalsPos2,
				extSM01_MatricGoalsPos3,
				extSM01_MatricGoalsPos4,
				extSM07_MatricOrientationServices,
				extSM08_MatricAssessmentServicesPlacement,
				extSM09_MatricAssessmentServicesOther,
				extSM09_MatricAssessmentServicesOtherPos1,
				extSM09_MatricAssessmentServicesOtherPos2,
				extSM09_MatricAssessmentServicesOtherPos3,
				extSM12_MatricCounAdviseServices,
				extSM13_MatricAcademicFollowupSerives,
				extSVO1_ProgramPlanStatus,
				extSV03_EconomicallyDisadvStatus,
				extSV03_EconomicallyDisadvStatusPos1,
				extSV03_EconomicallyDisadvStatusPos2,
				extSV04_SingleParentStatus,
				extSV05_DisplacedHomemakerStatus,
				extSV06_CoopWorkExperienceEdType,
				extSV08_TechPrepStatus,
				extSV09_MigrantWorkerStatus,
				extSCD1_FirstTermAttended,
				extSCD2_LastTermAttended,
				extSCD4_LEP,
				extSCD5_AcademicallyDisadvantagedStudent,
				extSTD1_AgeAtTerm,
				extSTD2_1stCensusCreditLoad,
				extSTD3_DayEveningClassCode,
				extSTD5_DegreeAppUnitsEarned,
				extSTD6_DayEveningClassCode2,
				extSTD7_HeadCountStatus,
				extSTD8_LocalCumGPA,
				extSTD9_TotalCumGPA,
				extStudentKey,
				extSD01_StudentPrimaryDisability,
				extSD03_StudentSecondaryDisability,
				extSB29_MultiEthnicity,
				extSTD10_IPESEthnicity
			)
		SELECT
			@IpedsCodeLegacy as collegeId,
			id.ssn as studentId,
			st.term_id as termId,
			st.residency as extSB09_ResidenceCode,
			st.apprentice as extSB23_ApprenticeshipStatus,
			st.transfer_ctr as extSB24_TransferCenterStatus,
			st.jtpa_status as extSB26_JTPAStatus,
			st.calworks_status as extSB27_CalWorksStatus,
			sm.goals as extSM01_MatricGoals,
			sm.goals_primary as extSM01_MatricGoalsPos1,
			sm.goals_secondary as extSM01_MatricGoalsPos2,
			sm.goals_tertiary as extSM01_MatricGoalsPos3,
			sm.goals_placeholder as extSM01_MatricGoalsPos4,
			sm.orientation_services as extSM07_MatricOrientationServices,
			sm.assessment_services_place as extSM08_MatricAssessmentServicesPlacement,
			sm.assessment_services_other as extSM09_MatricAssessmentServicesOther,
			sm.assessment_services_other_01 as extSM09_MatricAssessmentServicesOtherPos1,
			sm.assessment_services_other_02 as extSM09_MatricAssessmentServicesOtherPos2,
			sm.assessment_services_other_03 as extSM09_MatricAssessmentServicesOtherPos3,
			sm.advisement_services as extSM12_MatricCounAdviseServices,
			sm.follow_up_services as extSM13_MatricAcademicFollowupSerives,
			sv.voc_pgm_plan as extSVO1_ProgramPlanStatus,
			sv.econ_disadv as extSV03_EconomicallyDisadvStatus,
			sv.econ_disadv_status as extSV03_EconomicallyDisadvStatusPos1,
			sv.econ_disadv_source as extSV03_EconomicallyDisadvStatusPos2,
			sv.single_parent as extSV04_SingleParentStatus,
			sv.displ_homemker as extSV05_DisplacedHomemakerStatus,
			sv.coop_work_exp as extSV06_CoopWorkExperienceEdType,
			sv.tech_prep as extSV08_TechPrepStatus,
			sv.migrant_worker as extSV09_MigrantWorkerStatus,
			sb.term_first as extSCD1_FirstTermAttended,
			sb.term_last as extSCD2_LastTermAttended,
			sb.lep_flag as extSCD4_LEP,
			sb.acad_disad_flag as extSCD5_AcademicallyDisadvantagedStudent,
			st.age_at_term as extSTD1_AgeAtTerm,
			st.census_crload as extSTD2_1stCensusCreditLoad,
			st.day_evening_code as extSTD3_DayEveningClassCode,
			st.deg_appl_units_earned as extSTD5_DegreeAppUnitsEarned,
			st.day_evening_code2 as extSTD6_DayEveningClassCode2,
			st.headcount_status as extSTD7_HeadCountStatus,
			st.gpa_loc as extSTD8_LocalCumGPA,
			st.gpa_tot as extSTD9_TotalCumGPA,
			st.student_id as extStudentKey,
			sd.primary_disability as extSD01_StudentPrimaryDisability,
			sd.secondary_disability as extSD03_StudentSecondaryDisability,
			DecryptByKey(st.multi_race) as extSB29_MultiEthnicity,
			DecryptByKey(st.ipeds_race) as extSTD10_IPESEthnicity
		FROM
			calpass.comis.studntid id
			inner join
			calpass.comis.sbstudnt sb
				on sb.college_id = id.college_id
				and sb.student_id = id.student_id
			inner join
			calpass.comis.stterm st
				on st.college_id = sb.college_id
				and st.student_id = sb.student_id
			left outer join
			calpass.comis.smmatric sm
				on st.college_id = sm.college_id
				and st.student_id = sm.student_id
				and st.term_id = sm.term_id
			left outer join
			calpass.comis.svvatea sv
				on st.college_id = sv.college_id
				and st.student_id = sv.student_id
				and st.term_id = sv.term_id
			left outer join
			calpass.comis.sddsps sd
				on st.college_id = sd.college_id
				and st.student_id = sd.student_id
				and st.term_id = sd.term_id
		WHERE
			st.IsEscrow = 1
			and st.college_id = @CollegeCode;
		
		-- CCCourseProd
		INSERT INTO
			calpass.dbo.CCCourseProd
			(
				CollegeId,
				TermId,
				StudentId,
				CourseId,
				title,
				sectionId,
				UnitsEarned,
				Grade,
				CreditFlag,
				UnitsAttempted,
				TopCode,
				TransferStatus,
				UnitsMax,
				BSStatus,
				SamCode,
				ClassCode,
				CollegeLevel,
				NCRCategory,
				CCLongTermId
			)
		SELECT
			@IpedsCodeLegacy as collegeId,
			sx.term_id as TermId,
			id.ssn as studentId,
			sx.course_id as CourseId,
			cb.title as title,
			sx.section_id as sectionId,
			sx.units as UnitsEarned,
			sx.grade as Grade,
			sx.credit_flag as CreditFlag,
			sx.units_attempted as UnitsAttempted,
			cb.top_code as TopCode,
			cb.transfer_status as TransferStatus,
			cb.units_maximum as UnitsMax,
			cb.basic_skills_status as BSStatus,
			cb.sam_code as SamCode,
			cb.classification_code as ClassCode,
			cb.prior_to_college as CollegeLevel,
			cb.noncredit_category as NCRCategory,
			t.YearTermCode as CCLongTermId
		FROM
			@Escrow
			inner join
			calpass.comis.studntid id
			
			inner join
			calpass.comis.sxenrlm sx
				on sx.college_id = id.college_id
				and sx.student_id = id.student_id
			inner join
			calpass.comis.cbcrsinv cb
				on sx.college_id = cb.college_id
				and sx.term_id = cb.term_id
				and sx.course_id = cb.course_id
				and sx.control_number = cb.control_number
			inner join
			calpass.comis.Term t
				on t.TermCode = st.term_id
		WHERE
			st.college_id = @CollegeCode;
		
		-- CCCourseProd_EXT
		INSERT INTO
			calpass.dbo.CCCourseProd_EXT
			(
				StudentId,
				CollegeId,
				TermId,
				CourseId,
				Title,
				SectionId,
				extSX05_PositiveAttendanceHours,
				extSXD4_TotalHours,
				extXF01_SessionInstuctionMethod1,
				extXF01_SessionInstuctionMethod2,
				extXF01_SessionInstuctionMethod3,
				extXF01_SessionInstuctionMethod4,
				extXBD3_DayEveningClassCode,
				extCB00_ControlNumber,
				-- extCB01_DepartmentNumber,
				extCB04_CreditStatus,
				extCB10_COOPWorkExpEdStatus,
				extCB13_SpecialClassStatus,
				extCB14_CanCode,
				extCB15_CanSeqCode,
				extCB19_CrosswalkCrsDeptName,
				extCB20_CrosswalkCrsNumber,
				extCB21_PriorToCollegeLevel,
				extCB23_FundingAgencyCategory,
				extCB24_ProgramStatus,
				extStudentKey,
				extxb01_accountingmethod
			)
		SELECT
			id.ssn as studentId,
			@IpedsCodeLegacy as CollegeId,
			xf.term_id as TermId,
			xf.course_id as CourseId,
			cb.Title,
			xf.section_id as SectionId,
			sx.attend_hours as extSX05_PositiveAttendanceHours,
			sx.total_hours as extSXD4_TotalHours,
			xf.extXF01_SessionInstuctionMethod1,
			xf.extXF01_SessionInstuctionMethod2,
			xf.extXF01_SessionInstuctionMethod3,
			xf.extXF01_SessionInstuctionMethod4,
			xb.day_evening_code as extXBD3_DayEveningClassCode,
			cb.control_number as extCB00_ControlNumber,
			-- cb. as extCB01_DepartmentNumber,
			cb.credit_status as extCB04_CreditStatus,
			cb.coop_ed_status as extCB10_COOPWorkExpEdStatus,
			cb.special_class_status as extCB13_SpecialClassStatus,
			cb.can_code as extCB14_CanCode,
			cb.can_seq_code as extCB15_CanSeqCode,
			cb.crosswalk_crs_name as extCB19_CrosswalkCrsDeptName,
			cb.crosswalk_crs_number as extCB20_CrosswalkCrsNumber,
			cb.prior_to_college as extCB21_PriorToCollegeLevel,
			cb.funding_category as extCB23_FundingAgencyCategory,
			cb.program_status as extCB24_ProgramStatus,
			sx.student_id as extStudentKey,
			xb.accounting as extxb01_accountingmethod		
		FROM
			(
				SELECT
					xf.college_id,
					xf.term_id,
					xf.course_id,
					xf.section_id,
					max(case when xf.instruction_row = 1 then xf.instruction end) as extXF01_SessionInstuctionMethod1,
					max(case when xf.instruction_row = 2 then xf.instruction end) as extXF01_SessionInstuctionMethod2,
					max(case when xf.instruction_row = 3 then xf.instruction end) as extXF01_SessionInstuctionMethod3,
					max(case when xf.instruction_row = 4 then xf.instruction end) as extXF01_SessionInstuctionMethod4
				FROM
					(
						SELECT
							xf.college_id,
							xf.term_id,
							xf.course_id,
							xf.section_id,
							xf.instruction,
							row_number() OVER(
								PARTITION BY
									xf.college_id,
									xf.term_id,
									xf.course_id,
									xf.section_id
								ORDER BY
									xf.session_id
							) as instruction_row
						FROM
							calpass.comis.xfsesion xf
						WHERE
							xf.college_id = @CollegeCode
							and exists (
								SELECT
									1
								FROM
									calpass.comis.stterm st
								WHERE
									xf.college_id = st.college_id
									and xf.term_id = st.term_id
									and st.IsEscrow = 1
							)
					) xf
				GROUP BY
					xf.college_id,
					xf.term_id,
					xf.course_id,
					xf.section_id
			) xf
			inner join
			calpass.comis.xbsecton xb
				on xf.college_id = xb.college_id
				and xf.term_id = xb.term_id
				and xf.course_id = xb.course_id
				and xf.section_id= xb.section_id
			inner join
			calpass.comis.sxenrlm sx
				on xb.college_id = sx.college_id
				and xb.term_id = sx.term_id
				and xb.course_id = sx.course_id
				and xb.section_id = sx.section_id
			inner join
			calpass.comis.cbcrsinv cb
				on cb.college_id = sx.college_id
				and cb.term_id = sx.term_id
				and cb.course_id = sx.course_id
				and cb.control_number = sx.control_number
			inner join
			calpass.comis.studntid id
				on sx.college_id = id.college_id
				and sx.student_id = id.student_id;
		
		-- CCAwardProd
		INSERT INTO
			calpass.dbo.CCAwardProd
			(
				CollegeId,
				StudentId,
				TermId,
				TopCode,
				Award,
				DateOfAward,
				RecordId,
				ProgramCode
			)
		SELECT
			collegeId = @IpedsCodeLegacy,
			id.ssn as studentId,
			aw.term_id as TermId,
			aw.top_code as TopCode,
			aw.award as Award,
			DateOfAward = convert(char(8), aw.date, 112),
			aw.record_id as RecordId,
			aw.program_code as ProgramCode
		FROM
			calpass.comis.spawards aw
			inner join
			calpass.comis.studntid id
				on aw.college_id = id.college_id
				and aw.student_id = id.student_id
		WHERE
			aw.college_id = @CollegeCode
			and aw.term_id in ('160', '170');
		
		-- CCFinAwardsProd
		INSERT INTO
			calpass.dbo.CCFinAidAwardsProd
			(
				Derkey1,
				School,
				StudentId,
				TermId,
				TermRecd,
				TypeId,
				Amount,
				AcYear
			)
		SELECT
			Derkey1,
			School,
			StudentId,
			TermId,
			TermRecd,
			TypeId,
			Amount,
			AcYear
		FROM
			(
				SELECT
					id.InterSegmentKey as Derkey1,
					id.ssn as StudentId,
					@IpedsCodeLegacy as School,
					sf.term_id as TermId,
					sf.term_recd as TermRecd,
					sf.type_id as TypeId,
					sf.amount as Amount,
					t.AcademicYearAbbr as AcYear,
					row_number() OVER(
						PARTITION BY
							@IpedsCodeLegacy,
							id.ssn,
							sf.term_id,
							sf.term_recd,
							sf.type_id
						ORDER BY
							(SELECT 1)
					) row_selector
				FROM
					calpass.comis.sfawards sf
					inner join
					calpass.comis.stterm st
						on sf.college_id = st.college_id
						and sf.student_id = st.student_id
						and sf.term_recd = st.term_id
					inner join
					calpass.comis.studntid id
						on sf.college_id = id.college_id
						and sf.student_id = id.student_id
					inner join
					calpass.comis.term t
						on t.TermCode = st.term_id
				WHERE
					st.IsEscrow = 1
					and st.college_id = @CollegeCode
			) a
		WHERE
			a.row_selector = 1;
	
	FETCH NEXT FROM
		CursorCollege
	INTO
		@CollegeCode,
		@IpedsCodeLegacy;
END;

CLOSE SYMMETRIC KEY SecPii;

CLOSE CursorCollege;
DEALLOCATE CursorCollege;