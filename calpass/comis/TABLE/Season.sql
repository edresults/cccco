USE calpass;

GO

IF (object_id('comis.Season') is not null)
	BEGIN
		DROP TABLE comis.Season;
	END;

GO

CREATE TABLE
	comis.Season
	(
		SeasonId int identity(1,1),
		Name char(255) not null,
		Rank int not null,
		Ordinal int not null,
		CONSTRAINT
			pk_Season__SeasonId
		PRIMARY KEY CLUSTERED
			(
				SeasonId
			),
		CONSTRAINT
			uq_Season__Name
		UNIQUE
			(
				Name
			)
	);

GO

INSERT INTO
	comis.Season
	(
		Name,
		Rank,
		Ordinal
	)
VALUES
	('Summer',1,1),
	('Fall',2,2),
	('Winter',3,3),
	('Spring',4,4);