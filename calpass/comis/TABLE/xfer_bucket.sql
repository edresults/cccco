USE calpass;

GO

IF (object_id('comis.xfer_bucket') is not null)
    BEGIN
        DROP TABLE comis.xfer_bucket;
    END;

GO

CREATE TABLE
    comis.xfer_bucket
    (
        ssn                binary(64)  not null,
        fice               char(6)     not null,
        date_of_xfer       date        not null,
        source             char(4)     not null,
        tfr_in_school_name varchar(40) not null,
        tfr_in_inst_type   char(1)     not null,
        sector             char(5)     not null,
        state              char(2)     not null,
        segment            varchar(10) not null,
        CONSTRAINT PK_xfer_bucket PRIMARY KEY CLUSTERED ( ssn, tfr_in_inst_type, date_of_xfer, fice )
    );