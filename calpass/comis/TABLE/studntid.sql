USE calpass;

GO

IF (object_id('comis.studntid') is not null)
	BEGIN
		DROP TABLE comis.studntid;
	END;

GO

CREATE TABLE
	comis.studntid
	(
		college_id char(3) not null,
		student_id char(9) not null,
		ssn varchar(10) not null, -- sb00
		student_id_status char(1) not null, -- sb01
		InterSegmentKey binary(64),
		InterSegmentKeyBug binary(64),
		InterSegmentKeyFull binary(32)
	);

GO

ALTER TABLE
	comis.studntid
ADD CONSTRAINT
	PK_studntid__college_id__student_id
PRIMARY KEY CLUSTERED
	(
		college_id,
		student_id
	);

CREATE NONCLUSTERED INDEX
	IX_studntid__ssn__student_id_status
ON
	comis.studntid
	(
		ssn,
		student_id_status
	);

CREATE NONCLUSTERED INDEX
	IX_studntid__InterSegmentKey
ON
	comis.studntid
	(
		InterSegmentKey
	)
INCLUDE
	(
		ssn,
		student_id_status
	);