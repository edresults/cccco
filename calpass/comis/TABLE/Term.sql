USE calpass;

GO

IF (object_id('comis.Term') is not null)
	BEGIN
		DROP TABLE comis.Term;
	END;

GO

CREATE TABLE
	comis.Term
	(
		TermCode             char(3) not null, -- 105
		YearTermCode         char(5), -- 20105
		YearCode             char(3), -- 110
		AcademicYear         char(9), -- 2010-2011
		AcademicYearAbbr     char(4), -- 1011
		AcademicYearTrailing smallint, -- 2010-2011 == 2010
		AcademicYearLeading  smallint, -- 2010-2011 == 2011
		CalendarYear         char(4), -- 105 = 2010; 113 = 2011
		CalendarYearAbbr     char(2), -- 10
		Season               varchar(30), -- Summer
		System               varchar(30), -- Semester
		Name                 varchar(255), -- 2010 Summer Semester
		BeginDate            date,
		EndDate              date,
		Rank                 tinyint, -- TermCode ranked within AcademicYear; ex. summer = 1; fall = 2; winter = 3; spring = 4;
		Ordinal              int,
		NextTermCode         char(3),
		NextYearCode         char(3)
	);

GO

ALTER TABLE
	comis.Term
ADD CONSTRAINT
	PK_Term
PRIMARY KEY CLUSTERED
	(
		TermCode
	);

GO

CREATE NONCLUSTERED INDEX
	ix_Term__AcademicYear
ON
	comis.Term
	(
		AcademicYear
	);

GO

WITH
	cte_data
	(
		millennium,
		century,
		decade,
		year,
		term
	)
AS
	(
		SELECT
			m.val,
			c.val,
			d.val,
			y.val,
			t.val
		FROM
			(VALUES ('1'), ('2')) m (val)
			cross apply
			(VALUES ('0'),('1'),('2'),('3'),('4'),('5'),('6'),('7'),('8'),('9')) c (val)
			cross apply
			(VALUES ('0'),('1'),('2'),('3'),('4'),('5'),('6'),('7'),('8'),('9')) d (val)
			cross apply
			(VALUES ('0'),('1'),('2'),('3'),('4'),('5'),('6'),('7'),('8'),('9')) y (val)
			cross apply
			(VALUES ('0'),('1'),('2'),('3'),('4'),('5'),('6'),('7'),('8'),('9')) t (val)
		WHERE
			m.val + c.val + d.val + y.val + t.val >= '19905'
			and m.val + c.val + d.val + y.val + t.val <= '20509'
	),
	cte_compute
	(
		YearTermCode,
		TermCode,
		CalendarYear,
		CalendarYearAbbr,
		AcademicYearTrailing,
		AcademicYearLeading,
		Season,
		System,
		Rank,
		Ordinal,
		BeginDate,
		EndDate
	)
AS
	(
		SELECT
			YearTermCode = Millennium + Century + Decade + Year + Term,
			TermCode = Decade + Year + Term,
			CalendarYear = Millennium + Century + Decade + Year,
			CalendarYearAbbr = Decade + Year,
			AcademicYearTrailing = 
				case
					when Term in ('0','1','2','3','4') then convert(smallint, Millennium + Century + Decade + Year) - 1
					when Term in ('5','6','7','8','9') then convert(smallint, Millennium + Century + Decade + Year)
				end,
			AcademicYearLeading = 
				case
					when Term in ('0','1','2','3','4') then convert(smallint, Millennium + Century + Decade + Year)
					when Term in ('5','6','7','8','9') then convert(smallint, Millennium + Century + Decade + Year) + 1
				end,
			Season = 
				case
					when Term in ('1','2') then 'Winter'
					when Term in ('3','4') then 'Spring'
					when Term in ('5','6') then 'Summer'
					when Term in ('7','8') then 'Fall'
					when Term = '9' then 'First Fall Census'
					when Term = '0' then 'Annual'
				end,
			System = 
				case
					when Term in ('1','3','5','7') then 'Semester'
					when Term in ('2','4','6','8') then 'Quarter'
					when Term = '9' then null
					when Term = '0' then null
				end,
			Rank = 
				case
					when Term in ('1','2') then 4
					when Term in ('3','4') then 5
					when Term in ('5','6') then 1
					when Term in ('7','8') then 2
					when Term = '9' then 3
					when Term = '0' then 6
				end,
			Ordinal = 
				case
					when Term in ('1','2') then 4
					when Term in ('3','4') then 5
					when Term in ('5','6') then 1
					when Term in ('7','8') then 2
					when term = '9' then 3
					when Term = '0' then 6
				end,
			BeginDate = 
				case Term
					when '0' then convert(date, Millennium + Century + Decade + Year + '0701', 112)
					when '5' then convert(date, Millennium + Century + Decade + Year + '0601', 112)
					when '6' then convert(date, Millennium + Century + Decade + Year + '0601', 112)
					when '7' then convert(date, Millennium + Century + Decade + Year + '0901', 112)
					when '8' then convert(date, Millennium + Century + Decade + Year + '1001', 112)
					when '1' then convert(date, Millennium + Century + Decade + Year + '0101', 112)
					when '2' then convert(date, Millennium + Century + Decade + Year + '0101', 112)
					when '3' then convert(date, Millennium + Century + Decade + Year + '0201', 112)
					when '4' then convert(date, Millennium + Century + Decade + Year + '0401', 112)
				end,
			EndDate = 
				case Term
					when '0' then convert(date, Millennium + Century + Decade + Year + '0630', 112)
					when '5' then convert(date, Millennium + Century + Decade + Year + '0831', 112)
					when '6' then convert(date, Millennium + Century + Decade + Year + '0930', 112)
					when '7' then convert(date, Millennium + Century + Decade + Year + '1231', 112)
					when '8' then convert(date, Millennium + Century + Decade + Year + '1231', 112)
					when '1' then convert(date, Millennium + Century + Decade + Year + '0131', 112)
					when '2' then convert(date, Millennium + Century + Decade + Year + '0131', 112)
					when '3' then convert(date, Millennium + Century + Decade + Year + '0630', 112)
					when '4' then convert(date, Millennium + Century + Decade + Year + '0630', 112)
				end
		FROM
			cte_data
	),
	cte_source
	(
		TermCode,
		YearTermCode,
		YearCode,
		AcademicYear,
		AcademicYearAbbr,
		AcademicYearTrailing,
		AcademicYearLeading,
		CalendarYear,
		CalendarYearAbbr,
		Season,
		System,
		Name,
		BeginDate,
		EndDate,
		Rank,
		Ordinal
	)
AS
	(
		SELECT
			TermCode,
			YearTermCode,
			YearCode = substring(convert(char(4),AcademicYearLeading),3,2) + '0',
			AcademicYear = 
				convert(char(4),AcademicYearTrailing) + 
				'-' + 
				convert(char(4),AcademicYearLeading),
			AcademicYearAbbr = 
				substring(convert(char(4),AcademicYearTrailing),3,2) + 
				substring(convert(char(4),AcademicYearLeading),3,2),
			AcademicYearTrailing,
			AcademicYearLeading,
			CalendarYear,
			CalendarYearAbbr,
			Season,
			System,
			Name = CalendarYear + ' ' + Season + ' ' + System,
			BeginDate,
			EndDate,
			Rank,
			Ordinal
		FROM
			cte_compute
	)
MERGE
	comis.Term t
USING
	cte_source s
ON
	t.TermCode = s.TermCode
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			TermCode,
			YearTermCode,
			YearCode,
			AcademicYear,
			AcademicYearAbbr,
			AcademicYearTrailing,
			AcademicYearLeading,
			CalendarYear,
			CalendarYearAbbr,
			Season,
			System,
			Name,
			BeginDate,
			EndDate,
			Rank,
			Ordinal
		)
	VALUES
		(
			s.TermCode,
			s.YearTermCode,
			s.YearCode,
			s.AcademicYear,
			s.AcademicYearAbbr,
			s.AcademicYearTrailing,
			s.AcademicYearLeading,
			s.CalendarYear,
			s.CalendarYearAbbr,
			s.Season,
			s.System,
			s.Name,
			s.BeginDate,
			s.EndDate,
			s.Rank,
			s.Ordinal
		)
WHEN MATCHED THEN
	UPDATE SET
		t.TermCode = s.TermCode,
		t.YearTermCode = s.YearTermCode,
		t.YearCode = s.YearCode,
		t.AcademicYear = s.AcademicYear,
		t.AcademicYearAbbr = s.AcademicYearAbbr,
		t.AcademicYearTrailing = s.AcademicYearTrailing,
		t.AcademicYearLeading = s.AcademicYearLeading,
		t.CalendarYear = s.CalendarYear,
		t.CalendarYearAbbr = s.CalendarYearAbbr,
		t.Season = s.Season,
		t.System = s.System,
		t.Name = s.Name,
		t.BeginDate = s.BeginDate,
		t.EndDate = s.EndDate,
		t.Rank = s.Rank,
		t.Ordinal = s.Ordinal;

/*

	MODIFICATIONS

*/

ALTER TABLE
	comis.Term
ADD
	TermCodeNext char(3);

ALTER TABLE
	comis.Term
ADD
	YearCodeNext char(3);

ALTER TABLE
	comis.Term
ADD
	YearCodeFull char(5);
GO
UPDATE
	t
SET
	t.TermCodeNext = s.TermCodeNext
FROM
	comis.Term t
	inner join
	(
		SELECT
			d.YearTermCodeCurr,
			TermCodeNext = c.TermCode
		FROM
			comis.Term c
			inner join
			(
				SELECT
					YearTermCodeCurr = a.YearTermCode,
					YearTermCodeNext = min(b.YearTermCode)
				FROM
					comis.Term a
					inner join
					comis.Term b
						on b.YearTermCode > a.YearTermCode
						and b.System = a.System
				GROUP BY
					a.YearTermCode
			) d
				on d.YearTermCodeNext = c.YearTermCode
		UNION
		SELECT
			d.YearTermCodeCurr,
			TermCodeNext = c.TermCode
		FROM
			comis.Term c
			inner join
			(
				SELECT
					YearTermCodeCurr = a.YearTermCode,
					YearTermCodeNext = min(b.YearTermCode)
				FROM
					comis.Term a
					inner join
					comis.Term b
						on b.YearTermCode > a.YearTermCode
						and b.Season = a.Season
				WHERE
					a.Season = 'Annual'
				GROUP BY
					a.YearTermCode
			) d
				on d.YearTermCodeNext = c.YearTermCode
	) s
		on s.YearTermCodeCurr = t.YearTermCode;

UPDATE
	t
SET
	t.YearCodeNext = s.YearCodeNext
FROM
	comis.Term t
	inner join
	(
		SELECT DISTINCT
			d.AcademicYearTrailingCurr,
			YearCodeNext = c.YearCode
		FROM
			comis.Term c
			inner join
			(
				SELECT
					AcademicYearTrailingCurr = a.AcademicYearTrailing,
					AcademicYearTrailingNext = min(b.AcademicYearTrailing)
				FROM
					comis.Term a
					inner join
					comis.Term b
						on b.AcademicYearTrailing > a.AcademicYearTrailing
				GROUP BY
					a.AcademicYearTrailing
			) d
				on d.AcademicYearTrailingNext = c.AcademicYearTrailing
	) s
		on s.AcademicYearTrailingCurr = t.AcademicYearTrailing;

UPDATE
	comis.Term
SET
	YearCodeFull = substring(convert(char(4), AcademicYearLeading), 1, 2) + YearCode;

GO

ALTER TABLE
	comis.Term
ADD
	FiscalYear smallint;

GO

UPDATE
	comis.Term
SET
	FiscalYear = convert(smallint, CalendarYear);

GO

ALTER TABLE
	comis.Term
ADD
	FiscalQuarter tinyint;

GO

UPDATE
	comis.Term
SET
	FiscalQuarter = 
		case
			when right(TermCode, 1) in ('1','2') then 1
			when right(TermCode, 1) in ('3','4') then 2
			when right(TermCode, 1) in ('5','6') then 3
			when right(TermCode, 1) in ('7','8') then 4
		end;

GO

ALTER TABLE
	comis.Term
ADD
	FiscalYearQuarterSequence smallint;

GO

UPDATE
	t
SET
	t.FiscalYearQuarterSequence = s.FiscalYearQuarterSequence
FROM
	comis.Term t
	inner join
	(
		SELECT
			YearTermCode,
			FiscalYearQuarterSequence = dense_rank() over(order by FiscalYear, FiscalQuarter)
		FROM
			comis.Term
		WHERE
			right(TermCode, 1) not in ('0', '9')
	) s
		on t.YearTermCode = s.YearTermCode;

ALTER TABLE
	comis.Term
ADD
	IsPrimary bit null;

GO

UPDATE
	comis.Term
SET
	IsPrimary = case when right(TermCode, 1) in ('3','7','2','4','8') then 1 else 0 end;

GO

ALTER TABLE
	comis.Term
ALTER COLUMN
	IsPrimary bit not null;

GO

ALTER TABLE
	comis.Term
ADD
	NextPrimaryTermCode char(3) null;

GO

UPDATE
	t
SET
	t.NextPrimaryTermCode = t3.TermCode
FROM
	comis.Term t
	inner join
	(
		SELECT
			t1.YearTermCode,
			NextPrimaryYearTermCode = min(t2.YearTermCode)
		FROM
			comis.Term t1
			inner join
			comis.Term t2
				on t1.System = t2.System
				and t1.YearTermCode < t2.YearTermCode
		WHERE
			(
				(
					t1.IsPrimary = 1
					and
					t2.IsPrimary = 1
				)
				or
				(
					t1.IsPrimary = 0
					and
					t2.IsPrimary = 1
				)
			)
		GROUP BY
			t1.YearTermCode
	) s
		on s.YearTermCode = t.YearTermCode
	inner join
	comis.Term t3
		on t3.YearTermCode = s.NextPrimaryYearTermCode;

GO

ALTER TABLE
	comis.Term
ADD
	NextPrimaryYearTermCode char(5) null;

GO

UPDATE
	t
SET
	t.NextPrimaryYearTermCode = s.NextPrimaryYearTermCode
FROM
	comis.Term t
	inner join
	(
		SELECT
			t1.YearTermCode,
			NextPrimaryYearTermCode = min(t2.YearTermCode)
		FROM
			comis.Term t1
			inner join
			comis.Term t2
				on t1.System = t2.System
				and t1.YearTermCode < t2.YearTermCode
		WHERE
			(
				(
					t1.IsPrimary = 1
					and
					t2.IsPrimary = 1
				)
				or
				(
					t1.IsPrimary = 0
					and
					t2.IsPrimary = 1
				)
			)
		GROUP BY
			t1.YearTermCode
	) s
		on s.YearTermCode = t.YearTermCode;

GO

UPDATE
	comis.Term
SET
	BeginDate = 
		case right(TermCode, 1)
			when '0' then convert(date, CalendarYear + '0701', 112)
			when '5' then convert(date, CalendarYear + '0601', 112)
			when '6' then convert(date, CalendarYear + '0601', 112)
			when '7' then convert(date, CalendarYear + '0901', 112)
			when '8' then convert(date, CalendarYear + '1001', 112)
			when '1' then convert(date, CalendarYear + '0101', 112)
			when '2' then convert(date, CalendarYear + '0101', 112)
			when '3' then convert(date, CalendarYear + '0201', 112)
			when '4' then convert(date, CalendarYear + '0401', 112)
		end,
	EndDate = 
		case right(TermCode, 1)
			when '0' then convert(date, CalendarYear + '0630', 112)
			when '5' then convert(date, CalendarYear + '0831', 112)
			when '6' then convert(date, CalendarYear + '0930', 112)
			when '7' then convert(date, CalendarYear + '1231', 112)
			when '8' then convert(date, CalendarYear + '1231', 112)
			when '1' then convert(date, CalendarYear + '0131', 112)
			when '2' then convert(date, CalendarYear + '0131', 112)
			when '3' then convert(date, CalendarYear + '0630', 112)
			when '4' then convert(date, CalendarYear + '0630', 112)
		end