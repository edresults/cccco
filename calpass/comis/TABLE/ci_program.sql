USE calpass;

GO

IF (object_id('comis.ci_program') is not null)
	BEGIN
		DROP TABLE comis.ci_program;
	END;

GO

CREATE TABLE
	comis.ci_program
	(
		CollegeCode char(3),
		ControlNumber char(5),
		Title char(255),
		TopCode char(6),
		ProgramAward char(1),
		CreditType char(1),
		CDCP char(1),
		ApprovedDate datetime,
		ProposalStatus char(1),
		InActiveDate datetime,
		TotalHours int,
		TotalUnits int,
		GainfulEmp bit,
		AAT_AST bit,
		PHCAST bit
		CONSTRAINT
			pk_ci_program__CollegeCode__ControlNumber
		PRIMARY KEY CLUSTERED
			(
				CollegeCode,
				ControlNumber
			)
	);