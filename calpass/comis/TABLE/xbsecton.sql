DROP TABLE IF EXISTS comis.xbsecton;

CREATE TABLE
    comis.xbsecton
    (
        college_id          char(3)      not null,
        term_id             char(3)      not null,
        course_id           varchar(12)  not null,
        section_id          varchar(6)   not null,
        day_evening_code    char(1)          null,
        accounting          char(1)          null,
        control_number      char(12)     not null,
        first_census        datetime         null,
        contract_ed         char(1)          null,
        units_max           decimal(4,2)     null,
        units_min           decimal(4,2)     null,
        vatea_fund_status   char(1)          null,
        dsps_status         char(1)          null,
        work_based          char(1)          null,
        cvu_cvc             char(1)          null,
        wsch                decimal(6,2)     null,
        first_census_count  int              null,
        total_hours         decimal(7,1)     null,
        "1st_census_status" char(1)          null
    );

ALTER TABLE
    comis.xbsecton
ADD CONSTRAINT
    pk_xbsecton
PRIMARY KEY
    (
        college_id,
        term_id,
        course_id,
        control_number,
        section_id
    );

BULK INSERT
    comis.xbsecton
FROM
    N'C:\Data\CC\COMIS\Processing\mods\XBSECTON_2021-01-29_0820 .txt'
WITH
    (
        FIRSTROW = 2,
        FIELDTERMINATOR = ','
    );