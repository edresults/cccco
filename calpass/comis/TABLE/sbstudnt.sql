USE calpass;

GO

IF (object_id('comis.sbstudnt') is not null)
	BEGIN
		DROP TABLE comis.sbstudnt;
	END;

GO

CREATE TABLE
	comis.sbstudnt
	(
		college_id char(3) not null,
		student_id char(9) not null,
		birthdate datetime,
		term_first char(3),
		term_last char(3),
		lep_flag char(1),
		acad_disad_flag char(1),
		dsps_flag char(1),
		migrant_worker char(1),
		CONSTRAINT
			pk_sbstudnt__college_id__student_id
		PRIMARY KEY CLUSTERED
			(
				college_id,
				student_id
			)
	);