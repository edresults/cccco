USE calpass;

GO

IF (object_id('comis.SelfReportEngl') is not null)
	BEGIN
		DROP TABLE comis.SelfReportEngl;
	END;

GO

CREATE TABLE
	comis.SelfReportEngl
	(
		EnglId tinyint not null,
		Description varchar(255) not null
	);

GO

ALTER TABLE
	comis.SelfReportEngl
ADD CONSTRAINT
	pk_SelfReportEngl__EnglId
PRIMARY KEY CLUSTERED
	(
		EnglId
	);

GO

INSERT INTO
	comis.selfReportEngl
	(
		EnglId,
		Description
	)
VALUES
	(0,'None of the Above / Do Not Know'),
	(1,'12th grade Advanced Placement (AP) English Composition or Literature'),
	(2,'12th grade Honors English Composition or Literature'),
	(3,'12th grade English Composition or Literature'),
	(4,'11th grade Advanced Placement (AP) English Composition or Literature'),
	(5,'11th grade Honors English Composition or Literature'),
	(6,'11th grade English Composition or Literature'),
	(7,'10th grade (or lower) English Composition or Literature');