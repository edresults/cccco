USE calpass;

GO

IF (object_id('comis.hf_first') is not null)
	BEGIN
		DROP TABLE comis.hf_first;
	END;

GO

CREATE TABLE
	comis.hf_first
	(
		district_id char(3) not null,
		college_id char(3) not null,
		student_id char(9) not null, -- cccco_assigned_student_id
		student_ssn	varchar(10) not null, -- ssn or college student_id
		id_status char(1) not null,
		ccc_first_term char(3),
		ccc_first_term_value char(5),
		ccc_first_term_loc char(1),
		ccc_first_term_nsa char(3),
		ccc_first_term_nsa_value char(5),
		ccc_first_term_nsa_loc char(1),
		ccc_first_term_cr char(3),
		ccc_first_term_cr_value char(5),
		ccc_first_term_cr_loc char(1),
		ccc_first_term_cr_nsa char(3),
		ccc_first_term_cr_nsa_value char(5),
		ccc_first_term_cr_nsa_loc char(1),
		ccc_first_term_ncr char(3),
		ccc_first_term_ncr_value char(5),
		ccc_first_term_ncr_loc char(1),
		ccc_first_term_ncr_nsa char(3),
		ccc_first_term_ncr_nsa_value char(5),
		ccc_first_term_ncr_nsa_loc char(1),
		first_term char(3),
		first_term_value char(5),
		first_term_loc char(1),
		first_term_nsa char(3),
		first_term_nsa_value char(5),
		first_term_nsa_loc char(1),
		first_term_cr char(3),
		first_term_cr_value char(5),
		first_term_cr_loc char(1),
		first_term_cr_nsa char(3),
		first_term_cr_nsa_value char(5),
		first_term_cr_nsa_loc char(1),
		directed_year char(9),
		prepared_year char(9),
		first_date_4yr char(8),
		first_segment_4yr char(3),
		last_date_4yr char(8),
		last_segment_4yr char(3)
	);

GO

ALTER TABLE
	comis.hf_first
ADD CONSTRAINT
	pk_hf_first__college_id__student_id__id_status
PRIMARY KEY CLUSTERED
	(
		college_id,
		student_id,
		id_status
	);

CREATE NONCLUSTERED INDEX
	ix_hf_first__student_ssn
ON
	comis.hf_first
	(
		student_ssn
	);

-- CREATE INDEX
	-- ix_hf_first__college_id__student_ssn__id_status
-- ON
	-- comis.hf_first
	-- (
		-- college_id,
		-- student_ssn,
		-- id_status
	-- );