DROP TABLE IF EXISTS comis.xfsesion;

CREATE TABLE
    comis.xfsesion
    (
        college_id     char(3)      not null,
        term_id        char(3)      not null,
        course_id      varchar(12)  not null,
        section_id     varchar(6)   not null,
        control_number char(12)     not null,
        session_id     char(2)      not null,
        instruction    char(2)          null,
        date_begin     char(6)          null,
        date_end       char(6)          null,
        days           char(9)          null,
        time_start     char(4)          null,
        time_end       char(4)          null,
        hours          decimal(5,1)     null
    );

ALTER TABLE
    comis.xfsesion
ADD CONSTRAINT
    pk_xfsesion
PRIMARY KEY
    (
        college_id,
        term_id,
        course_id,
        control_number,
        section_id,
        session_id
    );