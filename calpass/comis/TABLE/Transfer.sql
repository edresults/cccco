USE calpass;

GO

IF (object_id('comis.Transfer') is not null)
	BEGIN
		DROP TABLE comis.Transfer;
	END;

GO

CREATE TABLE
	comis.Transfer
	(
		StudentSsn         binary(64)  not null,
		FiceCode           char(6)     not null,
		TransferDate       date        not null,
		DataSource         char(4)     not null,
		InstitutionName    varchar(40) not null,
		InstitutionType    char(1)     not null,
		InstutitionSector  char(3)     not null,
		InstutitionState   char(2)     not null,
		InstutitionSegment varchar(10) not null
	);

GO

ALTER TABLE
	comis.Transfer
DROP CONSTRAINT
	PK_Transfer;

ALTER TABLE
	comis.Transfer
ADD CONSTRAINT
	PK_Transfer
PRIMARY KEY
	(
		StudentSsn,
		InstitutionType,
		TransferDate,
		FiceCode
	);


-- InstitutionSector
-- PUB
-- PRI

-- InstitutionSector
-- CCC
-- CSU
-- ISP
-- OOS
-- UC