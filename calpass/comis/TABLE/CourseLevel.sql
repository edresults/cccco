USE calpass;

IF (object_id('comis.CourseLevel') is not null)
	BEGIN
		DROP TABLE comis.CourseLevel;
	END;

GO

CREATE TABLE
	comis.CourseLevel
	(
		CourseLevelCode char(1) not null,
		Rank tinyint,
		Description varchar(255),
		LevelsBelow tinyint
	);

GO

ALTER TABLE
	comis.CourseLevel
ADD CONSTRAINT
	pk_CourseLevel__CourseLevelCode
PRIMARY KEY
		(
			CourseLevelCode
		);

GO

INSERT INTO
	comis.CourseLevel
	(
		CourseLevelCode,
		Rank,
		Description,
		LevelsBelow
	)
VALUES
	('Y',10,'Not Applicable',0),
	('A',9,'One level below transfer',1),
	('B',8,'Two levels below transfer',2),
	('C',7,'Three levels below transfer',3),
	('D',6,'Four levels below transfer',4),
	('E',5,'Five levels below transfer',5),
	('F',4,'Six levels below transfer',6),
	('G',3,'Seven levels below transfer',7),
	('H',2,'Eight levels below transfer',8);