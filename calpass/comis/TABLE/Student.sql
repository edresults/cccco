IF (object_id('Comis.Student') is not null)
	BEGIN
		DROP TABLE Comis.Student;
	END;

GO

CREATE TABLE
	Comis.Student
	(
		InterSegmentKey   binary(64) not null,
		IsCollision       bit        not null,
		IsMutation        bit        not null,
		IsMultiCollege    bit        not null,
		IsMultiDistrict   bit        not null
	);

GO

ALTER TABLE
	Comis.Student
ADD CONSTRAINT
	PK_Student__InterSegmentKey
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey
	);