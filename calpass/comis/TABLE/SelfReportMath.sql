USE calpass;

GO

IF (object_id('comis.SelfReportMath') is not null)
	BEGIN
		DROP TABLE comis.SelfReportMath;
	END;

GO

CREATE TABLE
	comis.SelfReportMath
	(
		MathId tinyint not null,
		Description varchar(255) not null
	);

GO

ALTER TABLE
	comis.SelfReportMath
ADD CONSTRAINT
	pk_SelfReportMath__MathId
PRIMARY KEY CLUSTERED
	(
		MathId
	);

GO

INSERT INTO
	comis.selfReportMath
	(
		MathId,
		Description
	)
VALUES
	(0,'None of the Above / Do Not Know'),
	(1,'Pre-algebra or lower'),
	(2,'Algebra 1'),
	(3,'Integrated Math 1'),
	(4,'Integrated Math 2'),
	(5,'Geometry'),
	(6,'Algebra 2'),
	(7,'Integrated Math 3'),
	(8,'Statistics'),
	(9,'Integrated Math 4'),
	(10,'Trigonometry'),
	(11,'Pre-calculus'),
	(12,'Calculus or higher');