CREATE TABLE
	comis.Application20181211
	(
		app_id               BIGINT       NOT NULL,
		ccc_id               VARCHAR(8)   NOT NULL,
		status               CHAR(1)          NULL,
		college_id           CHAR(3)          NULL,
		term_id              BIGINT           NULL,
		major_id             BIGINT           NULL,
		intended_major       VARCHAR(30)      NULL,
		edu_goal             CHAR(1)          NULL,
		ack_fin_aid          BIT              NULL,
		fin_aid_ref          BIT              NULL,
		enroll_status        CHAR(1)          NULL,
		hs_comp_date         CHAR(9)          NULL,
		coenroll_confirm     BIT              NULL,
		gender               CHAR(1)          NULL,
		firstname            VARCHAR(50)      NULL,
		lastname             VARCHAR(50)      NULL,
		race_ethnic          CHAR(21)         NULL,
		hispanic             BIT              NULL,
		race_group           VARCHAR(255)     NULL,
		ssn_display          VARCHAR(255)     NULL,
		birthdate            CHAR(9)          NULL,
		military_status      CHAR(1)          NULL,
		ca_foster_youth      BIT              NULL,
		foster_youths        BIT              NULL,
		financial_assistance BIT              NULL,
		tanf_ssi_ga          BIT              NULL,
		calworks             BIT              NULL,
		dsps                 BIT              NULL,
		eops                 BIT              NULL,
		esl                  BIT              NULL,
		college_name         VARCHAR(50)      NULL,
		district_name        VARCHAR(50)      NULL,
		term_code            VARCHAR(15)      NULL,
		term_description     VARCHAR(100)     NULL,
		foster_youth_status  CHAR(1)          NULL,
		foster_youth_mis     BIT              NULL,
		transgender          CHAR(1)          NULL,
		CONSTRAINT PK_Application20181211 PRIMARY KEY CLUSTERED (app_id),
		INDEX IX_Application20181211_ccc_id NONCLUSTERED (ccc_id)
	);

GO

ALTER TABLE
	comis.Application20181211
ADD
	InterSegmentKey BINARY(64) NULL;

GO

UPDATE
	t
SET
	t.InterSegmentKey = i.InterSegmentKey
FROM
	comis.Application20181211 t
	cross apply
	dbo.InterSegmentKey(firstname, lastname, gender, convert(char(8), convert(date, birthdate), 112)) i;
	
GO

CREATE NONCLUSTERED INDEX
	IX_Application20181211_InterSegmentKey
ON
	comis.Application20181211
	(
		InterSegmentKey
	);