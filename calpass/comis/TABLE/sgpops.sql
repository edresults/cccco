USE calpass;

GO

IF (object_id('comis.sgpops') is not null)
	BEGIN
		DROP TABLE comis.sgpops;
	END;

GO

CREATE TABLE
	comis.sgpops
	(
		college_id char(3) not null,
		student_id char(9) not null,
		term_id char(3) not null,
		military char(4),
		military_dependent char(4),
		foster_care char(1),
		incarcerated char(1),
		mesa char(1),
		puente char(1),
		mchs char(1),
		umoja char(1),
		gen_first char(2),
		caa char(1),
		/* extra columns */
		IsFirstGeneration bit,
		IsActiveDuty bit,
		IsVeteran bit,
		IsActiveReserve bit,
		IsNationalGuard bit,
		IsFoster bit,
		CONSTRAINT
			pk_sgpops__college_id__student_id__term_id
		PRIMARY KEY CLUSTERED
			(
				college_id,
				student_id,
				term_id
			)
	);
GO
-- ALTER TABLE comis.sgpops DROP COLUMN IsFirstGeneration;
-- ALTER TABLE comis.sgpops DROP COLUMN IsActiveDuty;
-- ALTER TABLE comis.sgpops DROP COLUMN IsVeteran;
-- ALTER TABLE comis.sgpops DROP COLUMN IsActiveReserve;
-- ALTER TABLE comis.sgpops DROP COLUMN IsNationalGuard;
-- ALTER TABLE comis.sgpops DROP COLUMN IsFoster;
-- GO
-- ALTER TABLE comis.sgpops ADD IsFirstGeneration bit;
-- ALTER TABLE comis.sgpops ADD IsActiveDuty bit;
-- ALTER TABLE comis.sgpops ADD IsVeteran bit;
-- ALTER TABLE comis.sgpops ADD IsActiveReserve bit;
-- ALTER TABLE comis.sgpops ADD IsNationalGuard bit;
-- ALTER TABLE comis.sgpops ADD IsFoster bit;
GO
UPDATE
	comis.sgpops
SET
	-- IsFirstGeneration = case when substring(gen_first, 1, 1) in ('1','2','3') and substring(gen_first, 2, 1) in ('1','2','3') then 1 else 0 end,
	-- IsActiveDuty      = case when substring(military, 1, 1) = '1' then 1 else 0 end,
	-- IsVeteran         = case when substring(military, 2, 1) = '1' then 1 else 0 end,
	-- IsActiveReserve   = case when substring(military, 3, 1) = '1' then 1 else 0 end,
	-- IsNationalGuard   = case when substring(military, 4, 1) = '1' then 1 else 0 end,
	-- IsFoster          = case when foster_care = '1' then 1 else 0 end;