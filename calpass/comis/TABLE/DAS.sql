-- USE calpass;

-- GO

-- IF (object_id('comis.DAS') is not null)
-- 	BEGIN
-- 		DROP TABLE comis.DAS;
-- 	END;

-- GO

-- CREATE TABLE
-- 	comis.DAS
-- 	(
-- 		INDENTIFIER             varchar(10),
-- 		FIRST_NAME              varchar(30),
-- 		LAST_NAME               varchar(40),
-- 		SSN                     binary(64)   not null,
-- 		LMID_REGION             varchar(22),
-- 		GENDER                  varchar(1),
-- 		ETHNICITY               varchar(33),
-- 		VETERAN_FLAG            varchar(1),
-- 		APPRENTICE_TRAINEE_FLAG varchar(1),
-- 		OCCUPATION              varchar(73)  not null,
-- 		DOT_CODE                varchar(255),
-- 		DEGREESCERTIFICATES     varchar(17),
-- 		START_DATE              date         not null,
-- 		CURRENT_STATUS          varchar(12),
-- 		STATUS_DATE             date         not null
-- 	);

-- GO

-- ALTER TABLE
-- 	comis.DAS
-- ADD CONSTRAINT
-- 	PK_DAS
-- PRIMARY KEY CLUSTERED
-- 	(
-- 		SSN,
-- 		OCCUPATION,
-- 		START_DATE,
-- 		STATUS_DATE
-- 	);

-- GO

MERGE
	comis.DAS t
USING
	(
		SELECT
			INDENTIFIER,
			FIRST_NAME,
			LAST_NAME,
			SSN,
			LMID_REGION,
			GENDER,
			ETHNICITY,
			VETERAN_FLAG,
			APPRENTICE_TRAINEE_FLAG,
			OCCUPATION,
			DOT_CODE,
			DEGREESCERTIFICATES,
			START_DATE,
			CURRENT_STATUS,
			STATUS_DATE
		FROM
			(
				SELECT
					INDENTIFIER,
					FIRST_NAME,
					LAST_NAME,
					SSN = HASHBYTES('SHA2_512', SSN),
					LMID_REGION,
					GENDER,
					ETHNICITY,
					VETERAN_FLAG,
					APPRENTICE_TRAINEE_FLAG,
					OCCUPATION,
					DOT_CODE,
					DEGREESCERTIFICATES,
					START_DATE = convert(date, START_DATE),
					CURRENT_STATUS,
					STATUS_DATE = convert(date, STATUS_DATE),
					IsDuplicate = row_number() over(partition by ssn, occupation, start_date, status_date order by (select 1))
				FROM
					OPENROWSET(
						BULK N'\\10.11.6.43\C$\Data\CC\COMIS\Processing\DAS_DATA_2018.txt',
						FORMATFILE = N'\\10.11.6.43\C$\Data\CC\COMIS\Formats\DAS_DATA_PIPE.fmt',
						FIRSTROW = 2
					) i
				WHERE
					SSN is not null
			) a
		WHERE
			IsDuplicate = 1
	) s
ON
	(
		t.SSN = s.SSN
		and t.OCCUPATION = s.OCCUPATION
		and t.START_DATE = s.START_DATE
		and t.STATUS_DATE = s.STATUS_DATE
	)
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			INDENTIFIER,
			FIRST_NAME,
			LAST_NAME,
			SSN,
			LMID_REGION,
			GENDER,
			ETHNICITY,
			VETERAN_FLAG,
			APPRENTICE_TRAINEE_FLAG,
			OCCUPATION,
			DOT_CODE,
			DEGREESCERTIFICATES,
			START_DATE,
			CURRENT_STATUS,
			STATUS_DATE
		)
	VALUES
		(
			s.INDENTIFIER,
			s.FIRST_NAME,
			s.LAST_NAME,
			s.SSN,
			s.LMID_REGION,
			s.GENDER,
			s.ETHNICITY,
			s.VETERAN_FLAG,
			s.APPRENTICE_TRAINEE_FLAG,
			s.OCCUPATION,
			s.DOT_CODE,
			s.DEGREESCERTIFICATES,
			s.START_DATE,
			s.CURRENT_STATUS,
			s.STATUS_DATE
		)
WHEN MATCHED THEN
	UPDATE SET
		t.INDENTIFIER             = s.INDENTIFIER,
		t.FIRST_NAME              = s.FIRST_NAME,
		t.LAST_NAME               = s.LAST_NAME,
		t.LMID_REGION             = s.LMID_REGION,
		t.GENDER                  = s.GENDER,
		t.ETHNICITY               = s.ETHNICITY,
		t.VETERAN_FLAG            = s.VETERAN_FLAG,
		t.APPRENTICE_TRAINEE_FLAG = s.APPRENTICE_TRAINEE_FLAG,
		t.DOT_CODE                = s.DOT_CODE,
		t.DEGREESCERTIFICATES     = s.DEGREESCERTIFICATES,
		t.CURRENT_STATUS          = s.CURRENT_STATUS;