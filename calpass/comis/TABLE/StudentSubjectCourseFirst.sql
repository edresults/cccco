USE calpass;

GO

IF (object_id('comis.StudentSubjectCourseFirst') is not null)
	BEGIN
		DROP TABLE comis.StudentSubjectCourseFirst;
	END;

GO

CREATE TABLE
	comis.StudentSubjectCourseFirst
	(
		InterSegmentKey binary(64) not null,
		SubjectCode varchar(4) not null,
		CollegeCode char(3),
		TermCodeFull char(5),
		CourseId varchar(12),
		CourseTitle varchar(68),
		CourseLevelCode char(1),
		CourseLevelRank tinyint,
		SectionUnitsAttempted decimal(4,2),
		SectionGradeCode varchar(3),
		SectionGradeRank tinyint
	);

GO

ALTER TABLE
	comis.StudentSubjectCourseFirst
ADD CONSTRAINT
	pk_StudentSubjectCourseFirst
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey,
		SubjectCode
	);