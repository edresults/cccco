USE calpass;

GO

IF (object_id('comis.seeops') is not null)
	BEGIN
		DROP TABLE comis.seeops;
	END;

GO

CREATE TABLE
	comis.seeops
	(
		college_id char(3) not null,
		student_id char(9) not null,
		term_id char(3) not null,
		elig_factor char(1),
		term_accept char(3),
		end_term_status char(1),
		eops_units_registered	decimal(4, 2),
		eops_care_status char(1),
		care_term_accept char(3),
		care_marital_status char(1),
		care_number_dependnts char(1),
		care_afdc_duration char(1),
		care_withdraw_status char(1),
		CONSTRAINT
			pk_seeops__college_id__student_id__term_id
		PRIMARY KEY CLUSTERED
			(
				college_id,
				student_id,
				term_id
			)
	);