DROP TABLE IF EXISTS comis.sxenrlm;

CREATE TABLE
    comis.sxenrlm
    (
        college_id      char(3)      not null,
        student_id      char(9)      not null,
        term_id         char(3)      not null,
        course_id       varchar(12)  not null,
        section_id      varchar(6)   not null,
        units_attempted decimal(4,2)     null,
        units           decimal(4,2)     null,
        grade           varchar(3)       null,
        attend_hours    decimal(5,1)     null,
        total_hours     decimal(7,1)     null,
        credit_flag     char(1)          null,
        control_number  char(12)         null,
        apportionment   char(1)          null
    );

ALTER TABLE
    comis.sxenrlm
ADD CONSTRAINT
    pk_sxenrlm
PRIMARY KEY CLUSTERED
    (
        college_id,
        term_id,
        course_id,
        control_number,
        section_id,
        student_id
    );

CREATE INDEX
    ix_sxenrlm_student
ON
    comis.sxenrlm
    (
        college_id,
        student_id,
        term_id
    );

INSERT INTO
    comis.sxenrlm
SELECT
    college_id,
    student_id,
    term_id,
    rtrim(course_id),
    rtrim(section_id),
    units_attempted,
    units,
    rtrim(grade),
    attend_hours,
    total_hours,
    credit_flag,
    control_number,
    apportionment
FROM
    OPENROWSET(
        BULK N'C:\Data\CC\COMIS\Processing\BFBA7A9C-1720-4CE3-93E6-2D3A0DC02AC2.SXENRLM_2021-01-29_0820 .txt',
        FORMATFILE = N'C:\Data\CC\COMIS\Processing\sxenrlm.fmt',
        FIRSTROW = 2,
        MAXERRORS = 100
    ) a;