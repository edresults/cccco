USE calpass;

GO

IF (object_id('comis.svvatea') is not null)
	BEGIN
		DROP TABLE comis.svvatea;
	END;

GO

CREATE TABLE
	comis.svvatea
	(
		college_id char(3) not null,
		student_id char(9) not null,
		term_id char(3) not null,
		voc_pgm_plan char(1),
		econ_disadv char(2),
		single_parent char(1),
		displ_homemker char(1),
		coop_work_exp char(1),
		tech_prep char(1),
		migrant_worker char(1),
		wia_veteran char(1),
		IsDisadvantaged bit,
		CONSTRAINT
			pk_svvatea__college_id__student_id__term_id
		PRIMARY KEY CLUSTERED
			(
				college_id,
				student_id,
				term_id
			)
	);

-- ALTER TABLE comis.svvatea DROP COLUMN econ_disadv_status;
-- ALTER TABLE comis.svvatea DROP COLUMN econ_disadv_source;
-- GO
-- ALTER TABLE comis.svvatea ADD IsDisadvantaged bit;
-- GO
-- UPDATE
-- 	comis.svvatea
-- SET
-- 	IsDisadvantaged = case when substring(econ_disadv, 1, 1) in ('1','2','3','4') then 1 else 0 end;