CREATE TABLE
    comis.Curriculums
    (
        CollegeCode    char(3)      not null,
        ControlNumber  char(5)      not null,
        Title          varchar(255) not null,
        TopCode        char(6)      not null,
        ProgramAward   char(1)      not null,
        CreditType     char(1)      not null,
        CDCP           char(1)          null,
        ApprovedDate   date             null,
        ProposalStatus varchar(9)   not null,
        InactiveDate   datetime2        null,
        TotalHours     decimal(7,1) not null,
        TotalUnits     decimal(7,1) not null,
        GainfulEmp     bit          not null,
        AAT_AST        char(1)          null,
        PHCAST         bit              null,
        CONSTRAINT PK_Curriculums PRIMARY KEY CLUSTERED ( CollegeCode, ControlNumber )
    );