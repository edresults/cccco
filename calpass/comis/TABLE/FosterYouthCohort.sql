CREATE TABLE
    comis.FosterYouthCohort
    (
        OrganizationCode char(6)      not null,
        StudentId        binary(64)   not null,
        TermId           char(3)      not null,
        ProgramName      varchar(255) not null,
        FirstName        varchar(30)  not null,
        LastName         varchar(40)  not null,
        Birthdate        char(8)      not null,
        Gender           char(1)      not null,
        CONSTRAINT PK_FosterYouthCohort PRIMARY KEY CLUSTERED ( OrganizationCode, StudentId, TermId, ProgramName )
    );