USE calpass;

GO

IF (OBJECT_ID('comis.IdStatus') is not null)
	BEGIN
		DROP TABLE comis.IdStatus;
	END;

GO

CREATE TABLE
	comis.IdStatus
	(
		IdStatusCode CHAR(1) not null,
		IdStatusDescription VARCHAR(255) not null,
		CONSTRAINT
			pk_IdStatus__IdStatusCode
		PRIMARY KEY CLUSTERED
			(
				IdStatusCode
			)
	);

GO
	
INSERT INTO
	comis.IdStatus
	(
		IdStatusCode,
		IdStatusDescription
	)
VALUES
	('S','Student’s Social Security Number reported in Student Identifier (SB00).'),
	('C','Unique number assigned locally by the college reported in Student Identifier (SB00).');