DROP TABLE IF EXISTS comis.cbcrsinv;

CREATE TABLE
    comis.cbcrsinv
    (
        college_id               char(3)      not null,
        term_id                  char(3)      not null,
        course_id                varchar(12)  not null,
        control_number           char(12)     not null,
        top_code                 char(6)          null,
        transfer_status          char(1)          null,
        units_maximum            decimal(4,2)     null,
        basic_skills_status      char(1)          null,
        sam_code                 char(1)          null,
        prior_to_college         char(1)          null,
        noncredit_category       char(1)          null,
        title                    varchar(68)      null,
        credit_status            char(1)          null,
        coop_ed_status           char(1)          null,
        classification_code      char(1)          null,
        special_class_status     char(1)          null,
        funding_category         char(1)          null,
        program_status           char(1)          null,
        general_education_status char(1)          null,
        support_course_status    char(1)          null
    );

ALTER TABLE
    comis.cbcrsinv
ADD CONSTRAINT
    pk_cbcrsinv
PRIMARY KEY
    (
        college_id,
        term_id,
        course_id,
        control_number
    );

BULK INSERT
    comis.cbcrsinv
FROM
    N'C:\Data\CC\COMIS\Processing\mods\CBCRSINV_2021-01-29_0820 .txt'
WITH
    (
        FIRSTROW = 2
    );