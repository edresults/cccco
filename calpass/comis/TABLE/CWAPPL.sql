CREATE TABLE
    comis.CWAPPL
    (
        college_id                CHAR(3)  NOT NULL,
        student_id                CHAR(9)  NOT NULL,
        term_id                   CHAR(3)  NOT NULL,
        eligibility               CHAR(1)  NOT NULL,
        case_mgt                  CHAR(1)  NOT NULL,
        counseling                CHAR(1)  NOT NULL,
        referral                  CHAR(1)  NOT NULL,
        other_services            CHAR(5)  NOT NULL,
        cc_on_campus              SMALLINT NOT NULL,
        cc_off_campus             SMALLINT NOT NULL,
        cc_dependents             TINYINT  NOT NULL,
        total_dependents          TINYINT  NOT NULL,
        family_status             CHAR(1)  NOT NULL,
        employment_asst           CHAR(6)  NOT NULL,
        eligibility_time_limit    CHAR(1)      NULL,
        CONSTRAINT PK_CWAPPL PRIMARY KEY CLUSTERED ( college_id, student_id, term_id )
    );