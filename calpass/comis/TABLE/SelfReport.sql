USE calpass;

GO

IF (object_id('comis.SelfReport') is not null)
	BEGIN
		DROP TABLE comis.SelfReport;
	END;

GO

CREATE TABLE
	comis.SelfReport
	(
		InterSegmentKey binary(64) not null,
		completed_eleventh_grade char(1),
		grade_point_average decimal(4,3),
		highest_english_course tinyint,
		highest_english_grade varchar(2),
		highest_math_course_taken tinyint,
		highest_math_taken_grade varchar(2),
		highest_math_course_passed tinyint,
		highest_math_passed_grade varchar(2)
	);

GO

ALTER TABLE
	comis.SelfReport
ADD CONSTRAINT
	pk_SelfReport__InterSegmentKey
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey
	);

ALTER TABLE
	comis.SelfReport
ADD CONSTRAINT
	fk_SelfReport__highest_math_course_taken
FOREIGN KEY
	(
		highest_math_course_taken
	)
REFERENCES
	comis.SelfReportMath
	(
		MathId
	);

ALTER TABLE
	comis.SelfReport
ADD CONSTRAINT
	fk_SelfReport__highest_math_course_passed
FOREIGN KEY
	(
		highest_math_course_passed
	)
REFERENCES
	comis.SelfReportMath
	(
		MathId
	);