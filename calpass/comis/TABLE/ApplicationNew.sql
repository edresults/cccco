USE calpass;

GO

IF (object_id('comis.ApplicationNew') is not null)
	BEGIN
		DROP TABLE comis.ApplicationNew;
	END;

GO

CREATE TABLE
	comis.ApplicationNew
	(
		InterSegmentKey            binary(64) not null, /* binary(64) not null, */
		ccc_id                     nvarchar(255), /* nvarchar(8), */
        firstname                  nvarchar(255),
        lastname                   nvarchar(255),
        gender                     nvarchar(255),
        birthdate                  nvarchar(255),
		college_id                 char(3) not null, /* nchar(3) not null, */
		term_id                    bigint not null, /* bigint not null, */
		major_id                   nvarchar(255), /* bigint, */
		intended_major             nvarchar(255), /* nvarchar(30), */
		edu_goal                   nvarchar(255), /* nchar(1), */
		highest_edu_level          nvarchar(255), /* nchar(5), */
		ack_fin_aid                nvarchar(255), /* nchar(1), */
		fin_aid_ref                nvarchar(255), /* nchar(1), */
		confirmation               nvarchar(30) not null, /* nvarchar(30) not null, */
		sup_page_code              nvarchar(255), /* nvarchar(30), */
		last_page                  nvarchar(255), /* nvarchar(25), */
		enroll_status              nvarchar(255), /* nchar(1), */
		hs_edu_level               nvarchar(255), /* nchar(1), */
		hs_comp_date               nvarchar(255), /* date, */
		higher_edu_level           nvarchar(255), /* nchar(1), */
		higher_comp_date           nvarchar(255), /* date, */
		hs_not_attended            nvarchar(255), /* nchar(1), */
		cahs_graduated             nvarchar(255), /* nchar(1), */
		cahs_3year                 nvarchar(255), /* nchar(1), */
		hs_name                    nvarchar(255), /* nvarchar(30), */
		hs_city                    nvarchar(255), /* nvarchar(20), */
		hs_state                   nvarchar(255), /* nchar(2), */
		hs_country                 nvarchar(255), /* nchar(2), */
		hs_cds                     nvarchar(255), /* nchar(6), */
		hs_ceeb                    nvarchar(255), /* nchar(7), */
		hs_not_listed              nvarchar(255), /* nchar(1), */
		home_schooled              nvarchar(255), /* nchar(1), */
		college_count              nvarchar(255), /* smallint, */
		hs_attendance              nvarchar(255), /* smallint, */
		coenroll_confirm           nvarchar(255), /* nchar(1), */
		dependent_status           nvarchar(255), /* nchar(1), */
		race_ethnic                nvarchar(255), /* nvarchar(255), */
		hispanic                   nvarchar(255), /* nchar(1), */
		race_group                 nvarchar(255), /* nvarchar(255), */
		military_status            nvarchar(255), /* nchar(1), */
		ca_res_2_years             nvarchar(255), /* nchar(1), */
		ca_date_current            nvarchar(255), /* date, */
		ca_not_arrived             nvarchar(255), /* nchar(1), */
		ca_college_employee        nvarchar(255), /* nchar(1), */
		ca_school_employee         nvarchar(255), /* nchar(1), */
		ca_seasonal_ag             nvarchar(255), /* nchar(1), */
		ca_foster_youth            nvarchar(255), /* nchar(1), */
		experience                 nvarchar(255), /* int, */
		recommend                  nvarchar(255), /* int, */
		comments                   nvarchar(255), /* nvarchar(255), */
		comfortable_english        nvarchar(255), /* nchar(1), */
		financial_assistance       nvarchar(255), /* nchar(1), */
		tanf_ssi_ga                nvarchar(255), /* nchar(1), */
		foster_youths              nvarchar(255), /* nchar(1), */
		athletic_intercollegiate   nvarchar(255), /* nchar(1), */
		athletic_intramural        nvarchar(255), /* nchar(1), */
		athletic_not_interested    nvarchar(255), /* nchar(1), */
		academic_counseling        nvarchar(255), /* nchar(1), */
		basic_skills               nvarchar(255), /* nchar(1), */
		calworks                   nvarchar(255), /* nchar(1), */
		career_planning            nvarchar(255), /* nchar(1), */
		child_care                 nvarchar(255), /* nchar(1), */
		counseling_personal        nvarchar(255), /* nchar(1), */
		dsps                       nvarchar(255), /* nchar(1), */
		eops                       nvarchar(255), /* nchar(1), */
		esl                        nvarchar(255), /* nchar(1), */
		health_services            nvarchar(255), /* nchar(1), */
		housing_info               nvarchar(255), /* nchar(1), */
		employment_assistance      nvarchar(255), /* nchar(1), */
		online_classes             nvarchar(255), /* nchar(1), */
		reentry_program            nvarchar(255), /* nchar(1), */
		scholarship_info           nvarchar(255), /* nchar(1), */
		student_government         nvarchar(255), /* nchar(1), */
		testing_assessment         nvarchar(255), /* nchar(1), */
		transfer_info              nvarchar(255), /* nchar(1), */
		tutoring_services          nvarchar(255), /* nchar(1), */
		veterans_services          nvarchar(255), /* nchar(1), */
		col1_ceeb                  nvarchar(255), /* nchar(7), */
		col1_cds                   nvarchar(255), /* nchar(6), */
		col1_not_listed            nvarchar(255), /* nchar(1), */
		col1_name                  nvarchar(255), /* nvarchar(30), */
		col1_city                  nvarchar(255), /* nvarchar(20), */
		col1_state                 nvarchar(255), /* nvarchar(30), */
		col1_country               nvarchar(255), /* nchar(2), */
		col1_start_date            nvarchar(255), /* date, */
		col1_end_date              nvarchar(255), /* date, */
		col1_degree_date           nvarchar(255), /* date, */
		col1_degree_obtained       nvarchar(255), /* nchar(1), */
		col2_ceeb                  nvarchar(255), /* nchar(7), */
		col2_cds                   nvarchar(255), /* nchar(6), */
		col2_not_listed            nvarchar(255), /* nchar(1), */
		col2_name                  nvarchar(255), /* nvarchar(30), */
		col2_city                  nvarchar(255), /* nvarchar(20), */
		col2_state                 nvarchar(255), /* nvarchar(30), */
		col2_country               nvarchar(255), /* nchar(2), */
		col2_start_date            nvarchar(255), /* date, */
		col2_end_date              nvarchar(255), /* date, */
		col2_degree_date           nvarchar(255), /* date, */
		col2_degree_obtained       nvarchar(255), /* nchar(1), */
		col3_ceeb                  nvarchar(255), /* nchar(7), */
		col3_cds                   nvarchar(255), /* nchar(6), */
		col3_not_listed            nvarchar(255), /* nchar(1), */
		col3_name                  nvarchar(255), /* nvarchar(30), */
		col3_city                  nvarchar(255), /* nvarchar(20), */
		col3_state                 nvarchar(255), /* nvarchar(30), */
		col3_country               nvarchar(255), /* nchar(2), */
		col3_start_date            nvarchar(255), /* date, */
		col3_end_date              nvarchar(255), /* date, */
		col3_degree_date           nvarchar(255), /* date, */
		col3_degree_obtained       nvarchar(255), /* nchar(1), */
		col4_ceeb                  nvarchar(255), /* nchar(7), */
		col4_cds                   nvarchar(255), /* nchar(6), */
		col4_not_listed            nvarchar(255), /* nchar(1), */
		col4_name                  nvarchar(255), /* nvarchar(30), */
		col4_city                  nvarchar(255), /* nvarchar(20), */
		col4_state                 nvarchar(255), /* nvarchar(30), */
		col4_country               nvarchar(255), /* nchar(2), */
		col4_start_date            nvarchar(255), /* date, */
		col4_end_date              nvarchar(255), /* date, */
		col4_degree_date           nvarchar(255), /* date, */
		col4_degree_obtained       nvarchar(255), /* nchar(1), */
		college_name               nvarchar(255), /* nvarchar(50), */
		district_name              nvarchar(255), /* nvarchar(50), */
		term_code                  nvarchar(255), /* nvarchar(15), */
		term_description           nvarchar(255), /* nvarchar(100), */
		major_code                 nvarchar(255), /* nvarchar(30), */
		major_description          nvarchar(255), /* nvarchar(100), */
		foster_youth_status        nvarchar(255), /* nchar(1), */
		foster_youth_preference    nvarchar(255), /* nchar(1), */
		foster_youth_mis           nvarchar(255), /* nchar(1), */
		foster_youth_priority      nvarchar(255), /* nchar(1) */
		completed_eleventh_grade   nvarchar(255),
		grade_point_average        nvarchar(255),
		highest_english_course     nvarchar(255),
		highest_english_grade      nvarchar(255),
		highest_math_course_taken  nvarchar(255),
		highest_math_taken_grade   nvarchar(255),
		highest_math_course_passed nvarchar(255),
		highest_math_passed_grade  nvarchar(255),
	);

GO

ALTER TABLE
	comis.ApplicationNew
ADD CONSTRAINT
	pk_ApplicationNew__InterSegmentKey
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey,
		college_id,
		term_id,
		confirmation
	);

CREATE NONCLUSTERED INDEX
	IX_ApplicationNew__college_id
ON
	comis.ApplicationNew
	(
		college_id
	);