CREATE TABLE
    comis.Locale
    (
        Id              tinyint     not null identity,
        Label           varchar(25) not null,
        PivotColumnName sysname     not null,
        CONSTRAINT PK_Locale PRIMARY KEY CLUSTERED ( Id ),
        CONSTRAINT AK_Locale_Label UNIQUE ( Label )
    );

INSERT
    comis.Locale
    (
        Label,
        PivotColumnName
    )
VALUES
    ('All',        'StateId'),
    ('Macroregion','MacroregionId'),
    ('Microregion','MicroregionId'),
    ('District',   'DistrictId'),
    ('College',    'CollegeId');

CREATE TABLE
    comis.States
    (
        Id    tinyint     not null identity(1,1),
        Code  char(2)     not null,
        Label varchar(35) not null,
        CONSTRAINT PK_States PRIMARY KEY CLUSTERED ( Id )
    );

GO

CREATE TABLE
    comis.Macroregions
    (
        Id      tinyint     not null identity(1,1),
        StateId tinyint     not null,
        Label   varchar(35) not null,
        CONSTRAINT PK_Macroregions PRIMARY KEY CLUSTERED ( Id ),
        CONSTRAINT FK_Macroregions_StateId FOREIGN KEY ( StateId ) REFERENCES comis.States ( Id )
            ON DELETE CASCADE
    );

GO

CREATE TABLE
    comis.Microregions
    (
        Id            tinyint     not null identity(1,1),
        MacroregionId tinyint     not null,
        Label         varchar(35) not null,
        CONSTRAINT PK_Microregions PRIMARY KEY CLUSTERED ( Id ),
        CONSTRAINT FK_Microregions_MacroregionId FOREIGN KEY ( MacroregionId ) REFERENCES comis.Macroregions ( Id )
            ON DELETE CASCADE
    );

GO

CREATE TABLE
    comis.Districts
    (
        Id            tinyint     not null identity(1,1),
        MicroregionId tinyint     not null,
        Code          char(3)     not null,
        Label         varchar(35) not null,
        CONSTRAINT PK_Districts PRIMARY KEY CLUSTERED ( Id ),
        CONSTRAINT AK_Districts_Code UNIQUE ( Code ),
        CONSTRAINT FK_Districts_MicroregionId FOREIGN KEY ( MicroregionId ) REFERENCES comis.Microregions ( Id )
            ON DELETE CASCADE
    );

GO

CREATE TABLE
    comis.Counties
    (
        Id         tinyint     not null identity(1, 1),
        StateId    tinyint     not null,
        Code       char(2)     not null,
        LivingWage integer         null,
        Label      varchar(30) not null,
        CONSTRAINT PK_Counties PRIMARY KEY CLUSTERED ( Id ),
        CONSTRAINT AK_Counties_Code UNIQUE ( Code ),
        CONSTRAINT AK_Counties_Label UNIQUE ( Label ),
        CONSTRAINT FK_Counties_StateId FOREIGN KEY ( StateId ) REFERENCES comis.States ( Id )
            ON DELETE CASCADE
    );

GO

CREATE TABLE
    comis.Colleges
    (
        Id         tinyint     not null identity(1,1),
        DistrictId tinyint     not null,
        CountyId   tinyint     not null,
        Code       char(3)     not null,
        IpedsCode  char(6)     not null,
        Label      varchar(35) not null,
        CONSTRAINT PK_College PRIMARY KEY CLUSTERED ( Id ),
        CONSTRAINT AK_College_Code UNIQUE ( Code ),
        CONSTRAINT AK_College_IpedsCode UNIQUE ( IpedsCode ),
        CONSTRAINT FK_College_DistrictId FOREIGN KEY ( DistrictId ) REFERENCES comis.Districts ( Id )
            ON DELETE CASCADE,
        CONSTRAINT FK_College_CountyId FOREIGN KEY ( CountyId ) REFERENCES comis.Counties ( Id )
            ON DELETE NO ACTION
    );

GO

CREATE VIEW
    comis.Regions
WITH
    SCHEMABINDING
AS
    SELECT
        CollegeCode     = c.Code,
        CollegeName     = c.Label,
        CollegeIpeds    = c.IpedsCode,
        DistrictCode    = d.Code,
        DistrictName    = d.Label,
        MicroregionId   = mi.Id,
        MicroregionName = mi.Label,
        MacroregionId   = ma.Id,
        MacroregionName = ma.Label,
        StateId         = s.Id,
        StateName       = s.Label
    FROM
        comis.States s
        inner join
        comis.Macroregions ma
            on ma.StateId = s.Id
        inner join
        comis.Microregions mi
            on mi.MacroregionId = ma.Id
        inner join
        comis.Districts d
            on d.MicroregionId = mi.Id
        inner join
        comis.Colleges c
            on c.DistrictId = d.Id;

GO

CREATE UNIQUE CLUSTERED INDEX
    PK_Regions
ON
    comis.Regions
    (
        CollegeCode
    );

GO

INSERT INTO
    comis.States
    (
        Code,
        Label
    )
VALUES
    ('CA', 'California');
GO

INSERT INTO
    comis.Macroregions
    (
        StateId,
        Label
    )
VALUES
    (1, 'North/Far North'),
    (1, 'Bay Area'),
    (1, 'Central/Mother Lode'),
    (1, 'South Central Coast'),
    (1, 'Los Angeles/Orange County'),
    (1, 'Inland Empire/Desert'),
    (1, 'San Diego/Imperial Counties');

GO

INSERT INTO
    comis.Microregions
    (
        MacroregionId,
        Label
    )
VALUES
    (6,'Inland Empire'),
    (7,'San Diego-Imperial'),
    (5,'Los Angeles'),
    (5,'Orange'),
    (2,'East Bay'),
    (2,'North Bay'),
    (2,'Silicon Valley'),
    (2,'Santa Cruz-Monterey'),
    (2,'Mid-Peninsula'),
    (1,'Greater Sacramento'),
    (1,'Northern Inland'),
    (1,'Northern Coastal'),
    (4,'South Central Coast'),
    (3,'Northern Central Valley-Mother Lode'),
    (3,'Southern Central Valley-Mother Lode');

GO

INSERT INTO
    comis.Districts
    (
        MicroregionId,
        Code,
        Label
    )
VALUES
    (2,  '020', 'Grossmont-Cuyamaca'),
    (2,  '030', 'Imperial'),
    (2,  '050', 'Mira Costa'),
    (2,  '060', 'Palomar'),
    (2,  '070', 'San Diego'),
    (2,  '090', 'Southwestern'),
    (11, '110', 'Butte'),
    (11, '120', 'Feather River'),
    (11, '130', 'Lassen'),
    (12, '140', 'Mendocino-Lake'),
    (12, '160', 'Redwoods'),
    (11, '170', 'Shasta-Tehama-Trinity'),
    (11, '180', 'Siskiyous'),
    (10, '220', 'Lake Tahoe'),
    (10, '230', 'Los Rios'),
    (6,  '240', 'Napa Valley'),
    (6,  '260', 'Sonoma'),
    (10, '270', 'Sierra'),
    (6,  '280', 'Solano'),
    (10, '290', 'Yuba'),
    (5,  '310', 'Contra Costa'),
    (6,  '330', 'Marin'),
    (5,  '340', 'Peralta'),
    (9,  '360', 'San Francisco'),
    (9,  '370', 'San Mateo'),
    (8,  '410', 'Cabrillo'),
    (7,  '420', 'Foothill-Deanza'),
    (5,  '430', 'Ohlone'),
    (7,  '440', 'Gavilan'),
    (8,  '450', 'Hartnell'),
    (8,  '460', 'Monterey'),
    (7,  '470', 'San Jose-Evergreen'),
    (5,  '480', 'Chabot-Las Positas'),
    (7,  '490', 'West Valley-Mission'),
    (15, '520', 'Kern'),
    (14, '530', 'Merced'),
    (14, '550', 'San Joaquin Delta'),
    (15, '560', 'Sequoias'),
    (15, '570', 'State Center'),
    (15, '580', 'West Hills'),
    (14, '590', 'Yosemite'),
    (13, '610', 'Allan Hancock'),
    (13, '620', 'Antelope Valley'),
    (13, '640', 'San Luis Obispo'),
    (13, '650', 'Santa Barbara'),
    (13, '660', 'Santa Clarita'),
    (13, '680', 'Ventura'),
    (15, '690', 'West Kern'),
    (3,  '710', 'Compton'),
    (3,  '720', 'El Camino'),
    (3,  '730', 'Glendale'),
    (3,  '740', 'Los Angeles'),
    (3,  '770', 'Pasadena'),
    (3,  '780', 'Santa Monica'),
    (3,  '810', 'Cerritos'),
    (3,  '820', 'Citrus'),
    (4,  '830', 'Coast'),
    (3,  '840', 'Long Beach'),
    (3,  '850', 'Mt. San Antonio'),
    (4,  '860', 'North Orange'),
    (4,  '870', 'Rancho Santiago'),
    (3,  '880', 'Rio Hondo'),
    (4,  '890', 'South Orange'),
    (1,  '910', 'Barstow'),
    (1,  '920', 'Chaffey'),
    (1,  '930', 'Desert'),
    (1,  '940', 'Mt. San Jacinto'),
    (1,  '950', 'Palo Verde'),
    (1,  '960', 'Riverside'),
    (1,  '970', 'Copper Mountain'),
    (1,  '980', 'San Bernardino'),
    (1,  '990', 'Victor Valley');

GO

INSERT INTO
    comis.Counties
    (
        StateId,
        Code,
        LivingWage,
        Label
    )
VALUES
    (1, '01', 41858, 'Alameda'),
    (1, '02', null,  'Alpine'),
    (1, '03', null,  'Amador'),
    (1, '04', 22753, 'Butte'),
    (1, '05', null,  'Calaveras'),
    (1, '06', null,  'Colusa'),
    (1, '07', 44112, 'Contra Costa'),
    (1, '08', null,  'Del Norte'),
    (1, '09', 25871, 'El Dorado'),
    (1, '10', 21609, 'Fresno'),
    (1, '11', null,  'Glenn'),
    (1, '12', 21651, 'Humboldt'),
    (1, '13', 20469, 'Imperial'),
    (1, '14', null,  'Inyo'),
    (1, '15', 20358, 'Kern'),
    (1, '16', null,  'Kings'),
    (1, '17', null,  'Lake'),
    (1, '18', 21032, 'Lassen'),
    (1, '19', 31767, 'Los Angeles'),
    (1, '20', null,  'Madera'),
    (1, '21', 61897, 'Marin'),
    (1, '22', null,  'Mariposa'),
    (1, '23', 24423, 'Mendocino'),
    (1, '24', 19913, 'Merced'),
    (1, '25', null,  'Modoc'),
    (1, '26', null,  'Mono'),
    (1, '27', 30013, 'Monterey'),
    (1, '28', 31341, 'Napa'),
    (1, '29', null,  'Nevada'),
    (1, '30', 36655, 'Orange'),
    (1, '31', 26638, 'Placer'),
    (1, '32', 22236, 'Plumas'),
    (1, '33', 25271, 'Riverside'),
    (1, '34', 23998, 'Sacramento'),
    (1, '35', 33601, 'San Benito'),
    (1, '36', 24571, 'San Bernardino'),
    (1, '37', 33761, 'San Diego'),
    (1, '38', 55861, 'San Francisco'),
    (1, '39', 21904, 'San Joaquin'),
    (1, '40', 29091, 'San Luis Obispo'),
    (1, '41', 62147, 'San Mateo'),
    (1, '42', 31858, 'Santa Barbara'),
    (1, '43', 46840, 'Santa Clara'),
    (1, '44', 33152, 'Santa Cruz'),
    (1, '45', 21244, 'Shasta'),
    (1, '46', null,  'Sierra'),
    (1, '47', 20087, 'Siskiyou'),
    (1, '48', 28365, 'Solano'),
    (1, '49', 34885, 'Sonoma'),
    (1, '50', 23013, 'Stanislaus'),
    (1, '51', null,  'Sutter'),
    (1, '52', null,  'Tehama'),
    (1, '53', null,  'Trinity'),
    (1, '54', 19845, 'Tulare'),
    (1, '55', null,  'Tuolumne'),
    (1, '56', 32484, 'Ventura'),
    (1, '57', null,  'Yolo'),
    (1, '58', 20452, 'Yuba');

GO

INSERT INTO
    comis.Colleges
    (
        DistrictId,
        CountyId,
        Code,
        IpedsCode,
        Label
    )
VALUES
    (1,  37, '021', '113218', 'Cuyamaca College'),
    (1,  37, '022', '115296', 'Grossmont College'),
    (2,  13, '031', '115861', 'Imperial Valley College'),
    (3,  37, '051', '118912', 'MiraCosta College'),
    (4,  37, '061', '120971', 'Palomar College'),
    (5,  37, '071', '122339', 'San Diego City College'),
    (5,  37, '072', '122375', 'San Diego Mesa College'),
    (5,  37, '073', '122384', 'San Diego Miramar College'),
    (5,  37, '076', '113953', 'San Diego Adult'),
    (6,  37, '091', '123800', 'Southwestern College'),
    (7,  4,  '111', '110246', 'Butte College'),
    (8,  32, '121', '114433', 'Feather River College'),
    (9,  18, '131', '117274', 'Lassen College'),
    (10, 23, '141', '118684', 'Mendocino College'),
    (11, 12, '161', '121707', 'College of the Redwoods'),
    (12, 45, '171', '123299', 'Shasta College'),
    (13, 47, '181', '123484', 'College of the Siskiyous'),
    (14, 9,  '221', '117195', 'Lake Tahoe Community College'),
    (15, 34, '231', '109208', 'American River College'),
    (15, 34, '232', '113096', 'Cosumnes River College'),
    (15, 34, '233', '122180', 'Sacramento City College'),
    (15, 34, '234', '444219', 'Folsom Lake College'),
    (16, 28, '241', '119331', 'Napa Valley College'),
    (17, 49, '261', '123013', 'Santa Rosa Junior College'),
    (18, 31, '271', '123341', 'Sierra College'),
    (19, 48, '281', '123563', 'Solano Community College'),
    (20, 58, '291', '126119', 'Yuba College'),
    (20, 58, '292', '455512', 'Woodland Community College'),
    (21, 7,  '311', '112826', 'Contra Costa College'),
    (21, 7,  '312', '113634', 'Diablo Valley College'),
    (21, 7,  '313', '117894', 'Los Medanos College'),
    (22, 21, '334', '118347', 'College of Marin'),
    (22, 21, '335', '998347', 'Marin Continuing'),
    (23, 1,  '341', '108667', 'College of Alameda'),
    (23, 1,  '343', '117247', 'Laney College'),
    (23, 1,  '344', '118772', 'Merritt College'),
    (23, 1,  '345', '125170', 'Berkeley City College'),
    (24, 38, '361', '112190', 'City College of San Francisco'),
    (24, 38, '363', '992190', 'San Francisco Centers'),
    (25, 41, '371', '111434', 'Cañada College'),
    (25, 41, '372', '122791', 'College of San Mateo'),
    (25, 41, '373', '123509', 'Skyline College'),
    (26, 44, '411', '110334', 'Cabrillo College'),
    (27, 43, '421', '113333', 'De Anza College'),
    (27, 43, '422', '114716', 'Foothill College'),
    (28, 1,  '431', '120290', 'Ohlone College'),
    (29, 35, '441', '114938', 'Gavilan College'),
    (30, 27, '451', '115393', 'Hartnell College'),
    (31, 27, '461', '119067', 'Monterey Peninsula College'),
    (32, 43, '471', '114266', 'Evergreen Valley College'),
    (32, 43, '472', '122746', 'San Jose City College'),
    (33, 1,  '481', '366401', 'Las Positas College'),
    (33, 1,  '482', '111920', 'Chabot College'),
    (34, 43, '492', '118930', 'Mission College'),
    (34, 43, '493', '125499', 'West Valley College'),
    (35, 15, '521', '109819', 'Bakersfield College'),
    (35, 15, '522', '111896', 'Cerro Coso Community College'),
    (35, 15, '523', '121363', 'Porterville College'),
    (36, 24, '531', '118718', 'Merced College'),
    (37, 39, '551', '122658', 'San Joaquin Delta College'),
    (38, 54, '561', '123217', 'College of the Sequoias'),
    (39, 10, '571', '114789', 'Fresno City College'),
    (39, 10, '572', '117052', 'Reedley College'),
    (39, 10, '576', '999576', 'Clovis Community College'),
    (40, 10, '581', '125462', 'West Hills College Coalinga'),
    (40, 10, '582', '448594', 'West Hills College Lemoore'),
    (41, 50, '591', '112561', 'Columbia College'),
    (41, 50, '592', '118976', 'Modesto Junior College'),
    (42, 42, '611', '108807', 'Allan Hancock College'),
    (43, 19, '621', '109350', 'Antelope Valley College'),
    (44, 40, '641', '113193', 'Cuesta College'),
    (45, 42, '651', '122889', 'Santa Barbara City College'),
    (45, 42, '652', '992889', 'Santa Barbara Continuing'),
    (46, 19, '661', '111461', 'College of the Canyons'),
    (47, 56, '681', '119137', 'Moorpark College'),
    (47, 56, '682', '120421', 'Oxnard College'),
    (47, 56, '683', '125028', 'Ventura College'),
    (48, 15, '691', '124113', 'Taft College'),
    (49, 19, '711', '112686', 'Compton College'),
    (50, 19, '721', '113980', 'El Camino College'),
    (51, 19, '731', '115001', 'Glendale Community College'),
    (52, 19, '741', '117788', 'Los Angeles City College'),
    (52, 19, '742', '117690', 'Los Angeles Harbor College'),
    (52, 19, '743', '117867', 'Los Angeles Mission College'),
    (52, 19, '744', '117706', 'Los Angeles Pierce College'),
    (52, 19, '745', '117715', 'Los Angeles Southwest College'),
    (52, 19, '746', '117724', 'Los Angeles Trade-Tech College'),
    (52, 19, '747', '117733', 'Los Angeles Valley College'),
    (52, 19, '748', '113856', 'East Los Angeles College'),
    (52, 19, '749', '125471', 'West Los Angeles College'),
    (52, 19, '74A', '997788', 'Los Angeles ITV'),
    (53, 19, '771', '121044', 'Pasadena City College'),
    (54, 19, '781', '122977', 'Santa Monica College'),
    (55, 19, '811', '111887', 'Cerritos College'),
    (56, 19, '821', '112172', 'Citrus College'),
    (57, 30, '831', '112385', 'Coastline Community College'),
    (57, 30, '832', '115126', 'Golden West College'),
    (57, 30, '833', '120342', 'Orange Coast College'),
    (58, 19, '841', '117645', 'Long Beach City College'),
    (59, 19, '851', '119164', 'Mt. San Antonio College'),
    (60, 30, '861', '113236', 'Cypress College'),
    (60, 30, '862', '114859', 'Fullerton College'),
    (60, 30, '863', '993236', 'North Orange Adult'),
    (61, 30, '871', '121619', 'Santa Ana College'),
    (61, 30, '872', '991619', 'Rancho Santiago CED'),
    (61, 30, '873', '399212', 'Santiago Canyon College'),
    (62, 19, '881', '121886', 'Rio Hondo College'),
    (63, 30, '891', '122205', 'Saddleback College'),
    (63, 30, '892', '116439', 'Irvine Valley College'),
    (64, 36, '911', '109907', 'Barstow College'),
    (65, 36, '921', '111939', 'Chaffey College'),
    (66, 33, '931', '113573', 'College of the Desert'),
    (67, 33, '941', '119216', 'Mt. San Jacinto College'),
    (68, 36, '951', '120953', 'Palo Verde College'),
    (69, 33, '961', '121901', 'Riverside City College'),
    (69, 33, '962', '460394', 'Moreno Valley College'),
    (69, 33, '963', '460464', 'Norco College'),
    (70, 36, '971', '395362', 'Copper Mountain Community College'),
    (71, 36, '981', '113111', 'Crafton Hills College'),
    (71, 36, '982', '123527', 'San Bernardino Valley College'),
    (72, 36, '991', '125091', 'Victor Valley College');

GO