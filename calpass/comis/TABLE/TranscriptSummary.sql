IF (object_id('Comis.TranscriptSummary') is not null)
	BEGIN
		DROP TABLE Comis.TranscriptSummary;
	END;

GO

CREATE TABLE
	Comis.TranscriptSummary
	(
		CollegeCode          char(3)      not null,
		StudentId            char(9)      not null,
		YearTermCode         char(5)      not null,
		SubjectCode          char(4)      not null,
		CourseLevelCode      char(1)      not null,
		CourseId             varchar(12)  not null,
		CourseTitle          varchar(68)  not null,
		COurseUnitsAttempted decimal(4,2) not null,
		MarkCode             varchar(3)   not null,
		MarkPoints           decimal(3,2) not null,
		MarkCategory         smallint     not null,
		MarkIsSuccess        bit          not null,
		FirstSelector        tinyint      not null,
		LevelSelector        tinyint      not null
	);

GO

ALTER TABLE
	Comis.TranscriptSummary
ADD CONSTRAINT
	PK_TranscriptSummary
PRIMARY KEY CLUSTERED
	(
		CollegeCode,
		StudentId,
		SubjectCode,
		YearTermCode,
		FirstSelector
	);