USE calpass;

GO

IF (object_id('comis.District') is not null)
	BEGIN
		DROP TABLE comis.District;
	END;

GO

CREATE TABLE
	comis.District
	(
		DistrictCode char(3),
		IpedsCode char(6),
		LegacyCode int,
		Name varchar(255),
		CONSTRAINT
			pk_District__DistrictCode
		PRIMARY KEY CLUSTERED
			(
				DistrictCode
			),
		CONSTRAINT
			uq_District_Description
		UNIQUE
			(
				Name
			),
		INDEX
			ix_District__IpedsCode
		NONCLUSTERED
			(
				IpedsCode
			)
	);

INSERT INTO
	comis.District
	(
		DistrictCode,
		IpedsCode,
		LegacyCode,
		Name
	)
VALUES
	('610',null,null,'ALLAN HANCOCK CCD'),
	('620',null,null,'ANTELOPE CCD'),
	('910',null,null,'BARSTOW CCD'),
	('110',null,null,'BUTTE CCD'),
	('410',null,null,'CABRILLO CCD'),
	('810',null,null,'CERRITOS CCD'),
	('480',null,170728,'CHABOT-LAS POSITAS CCD'),
	('920',null,null,'CHAFFEY CCD'),
	('820',null,null,'CITRUS CCD'),
	('830','112376',170733,'COAST CCD'),
	('710',null,null,'COMPTON CCD'),
	('310','112817',170746,'CONTRA COSTA CCD'),
	('970',null,null,'COPPER MOUNTAIN'),
	('930',null,null,'DESERT CCD'),
	('720',null,172781,'EL CAMINO CCD'),
	('120',null,null,'FEATHER RIVER CCD'),
	('420','114831',170761,'FOOTHILL CCD'),
	('440',null,null,'GAVILAN CCD'),
	('730',null,null,'GLENDALE CCD'),
	('020','115287',170768,'GROSSMONT CCD'),
	('450',null,null,'HARTNELL CCD'),
	('030',null,null,'IMPERIAL CCD'),
	('520',null,170772,'KERN CCD'),
	('220',null,null,'LAKE TAHOE CCD'),
	('130',null,null,'LASSEN CCD'),
	('840',null,null,'LONG BEACH CCD'),
	('740','117681',170779,'LOS ANGELES CCD'),
	('230','117900',170788,'LOS RIOS CCD'),
	('330',null,null,'MARIN CCD'),
	('140',null,null,'MENDOCINO CCD'),
	('530',null,null,'MERCED CCD'),
	('050',null,null,'MIRA COSTA CCD'),
	('460',null,null,'MONTEREY CCD'),
	('850',null,null,'MT. SAN ANTONIO CCD'),
	('940',null,null,'MT. SAN JACINTO CCD'),
	('240',null,null,'NAPA CCD'),
	('860','120023',170801,'NORTH ORANGE CCD'),
	('430',null,null,'OHLONE CCD'),
	('950',null,null,'PALO VERDE CCD'),
	('060',null,null,'PALOMAR CCD'),
	('770',null,null,'PASADENA CCD'),
	('340','121178',170808,'PERALTA CCD'),
	('870','438665',170811,'RANCHO SANTIAGO CCD'),
	('160',null,null,'REDWOODS CCD'),
	('880',null,null,'RIO HONDO CCD'),
	('960',null,175134,'RIVERSIDE CCD'),
	('980','428426',170817,'SAN BERNARDINO CCD'),
	('070','122320',170821,'SAN DIEGO CCD'),
	('360',null,170824,'SAN FRANCISCO CCD'),
	('550',null,null,'SAN JOAQUIN DELTA CCD'),
	('470','122737',170827,'SAN JOSE CCD'),
	('640',null,null,'SAN LUIS OBISPO CCD'),
	('370','122782',170828,'SAN MATEO CCD'),
	('650',null,null,'SANTA BARBARA CCD'),
	('660',null,null,'SANTA CLARITA CCD'),
	('780',null,null,'STANA MONICA CCD'),
	('560',null,null,'SEQUOIAS CCD'),
	('170',null,null,'SHASTA TEHAMA CCD'),
	('270',null,null,'SIERRA CCD'),
	('180',null,null,'SISKIYOUS CCD'),
	('280','123563',170837,'SOLANO CCD'),
	('260',null,null,'SONOMA CCD'),
	('890','432144',170838,'SOUTH ORANGE COUNTY CCD'),
	('090',null,null,'SOUTHWESTERN CCD'),
	('570','123925',170840,'STATE CENTER CCD'),
	('680','125019',170843,'VENTURA CCD'),
	('990',null,null,'VICTOR VALLEY CCD'),
	('580','448637',172264,'WEST HILLS CCD'),
	('690',null,null,'WEST KERN CCD'),
	('490','125222',170849,'WEST VALLEY CCD'),
	('590','126100',170850,'YOSEMITE CCD'),
	('290','126119',172784,'YUBA CCD');