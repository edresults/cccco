USE calpass;

GO

IF (OBJECT_ID('comis.Gender') is not null)
	BEGIN
		DROP TABLE comis.Gender;
	END;

GO

CREATE TABLE
	comis.Gender
	(
		GenderCode char(1) not null,
		Description varchar(255) not null,
		CONSTRAINT
			pk_Gender__GenderCode
		PRIMARY KEY CLUSTERED
			(
				GenderCode
			)
	);

GO

INSERT INTO
	comis.Gender
	(
		GenderCode,
		Description
	)
VALUES
	('F','Female'),
	('M','Male'),
	('X','Unknown or non-respondent');