USE calpass;

GO

IF (object_id('comis.grade') is not null)
	BEGIN
		DROP TABLE comis.grade;
	END;

GO

CREATE TABLE
	comis.Grade
	(
		GradeCode varchar(5) not null,
		points decimal(3,2) not null,
		Category smallint not null,
		enroll_ind tinyint not null,
		completion_ind tinyint not null,
		completion_denominator_ind tinyint not null,
		success_ind tinyint not null,
		success_denominator_ind tinyint not null,
		gpa_ind tinyint not null,
		rank tinyint not null unique,
		abbreviation varchar(6) not null,
		description varchar(255) not null
	);

GO

ALTER TABLE
	comis.Grade
ADD CONSTRAINT
	pk_Grade__GradeCode
PRIMARY KEY CLUSTERED
	(
		GradeCode
	);

GO

INSERT INTO
	comis.Grade
	(
		GradeCode,
		points,
		Category,
		enroll_ind,
		completion_ind,
		completion_denominator_ind,
		success_ind,
		success_denominator_ind,
		gpa_ind,
		rank,
		abbreviation,
		description
	)
SELECT
	GradeCode,
	points,
	Category,
	enroll_ind,
	completion_ind,
	completion_denominator_ind,
	success_ind,
	success_denominator_ind,
	gpa_ind,
	rank = row_number() over(order by (select 1)),
	abbreviation,
	description
FROM
	(
		VALUES
		('A+',4,4,1,1,1,1,1,1,'A','Highest level of academic achievement where students demonstrate mastery of a subject'),
		('A',4,4,1,1,1,1,1,1,'A','Highest level of academic achievement where students demonstrate mastery of a subject'),
		('A-',3.7,4,1,1,1,1,1,1,'A','Highest level of academic achievement where students demonstrate mastery of a subject'),
		('B+',3.3,3,1,1,1,1,1,1,'B','Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
		('B',3,3,1,1,1,1,1,1,'B','Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
		('B-',2.7,3,1,1,1,1,1,1,'B','Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
		('C+',2.3,2,1,1,1,1,1,1,'C','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
		('C',2,2,1,1,1,1,1,1,'C','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
		('C-',1.7,2,1,1,1,1,1,1,'C','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
		('D+',1.3,1,1,1,1,0,1,1,'D','Fourth highest level of academic achievement where students demonstrate an inadequate understanding of a subject'),
		('D',1,1,1,1,1,0,1,1,'D','Fourth highest level of academic achievement where students demonstrate an inadequate understanding of a subject'),
		('D-',0.7,1,1,1,1,0,1,1,'D','Fourth highest level of academic achievement where students demonstrate an inadequate understanding of a subject'),
		('F+',0,0,1,1,1,0,1,1,'F','Lowest level of academic achievement where students demonstrate a substandard understanding of a subject'),
		('F',0,0,1,1,1,0,1,1,'F','Lowest level of academic achievement where students demonstrate a substandard understanding of a subject'),
		('CR',0,1,1,1,1,1,1,0,'CR','Credit'),
		('P',0,1,1,1,1,1,1,0,'CR','Pass'),
		('NC',0,0,1,1,1,0,1,0,'NC','No Credit'),
		('NP',0,0,1,1,1,0,1,0,'NC','No Pass'),
		('IA+',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IA',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IA-',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IB+',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IB',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IB-',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IC+',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IC',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IC-',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('ID+',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('ID',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('ID-',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IF+',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IF',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('I',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IP',0,-99,1,0,0,0,0,0,'I','Incomplete'),
		('ICR',0,-99,1,1,1,0,1,0,'I','Incomplete Credit'),
		('IPP',0,-99,1,1,1,0,1,0,'I','Incomplete Pass'),
		('INC',0,-99,1,1,1,0,1,0,'I','Incomplete Pass'),
		('INP',0,-99,1,1,1,0,1,0,'I','Incomplete No Pass'),
		('W',0,-98,1,0,1,0,1,0,'W','Withdrawl'),
		('FW',0,-99,1,0,1,0,1,0,'W','Withdrawl without permission and without having achieved a final passing grade'),
		('MW',0,-99,1,0,0,0,0,0,'Other','Military withdraw'),
		('RD',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD0',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD1',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD2',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD3',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD4',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD5',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD6',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD7',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD8',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD9',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('SP',0,-99,0,0,0,0,0,0,'Other','Satisfactory Progress (Non-Credit)'),
		('UG',0,-99,0,0,0,0,0,0,'Other','Ungraded (Non-Credit)'),
		('UD',0,-99,0,0,0,0,0,0,'Other','Ungraded Dependent'),
		('DR',0,-99,0,0,0,0,0,0,'Drop','Dropped section ON OR AFTER first census date and before withdraw period'),
		('XX',0,-99,0,0,0,0,0,0,'Other','None of the above or unknown')
	) t
	(
		GradeCode,
		points,
		Category,
		enroll_ind,
		completion_ind,
		completion_denominator_ind,
		success_ind,
		success_denominator_ind,
		gpa_ind,
		abbreviation,
		description
	)


-- 'EW', 'IX'