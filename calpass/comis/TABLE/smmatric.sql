USE calpass;

GO

IF (object_id('comis.smmatric') is not null)
	BEGIN
		DROP TABLE comis.smmatric;
	END;

GO

CREATE TABLE
	comis.smmatric
	(
		college_id char(3) not null,
		student_id char(9) not null,
		term_id char(3) not null,
		goals char(4),
		goals_primary as cast(case when substring(goals, 1, 1) in (' ', '') then null else substring(goals, 1, 1) end as char(1)) persisted,
		goals_secondary as cast(case when substring(goals, 2, 1) in (' ', '') then null else substring(goals, 2, 1) end as char(1)) persisted,
		goals_tertiary as cast(case when substring(goals, 3, 1) in (' ', '') then null else substring(goals, 3, 1) end as char(1)) persisted,
		goals_placeholder as cast(case when substring(goals, 4, 1) in (' ', '') then null else substring(goals, 4, 1) end as char(1)) persisted,
		major char(6),
		special_needs char(14),
		special_needs_01_financial_aid as cast(case when substring(special_needs, 1, 1) in (' ', '') then null else substring(special_needs, 1, 1) end as char(1)) persisted,
		special_needs_02_child_care as cast(case when substring(special_needs, 2, 1) in (' ', '') then null else substring(special_needs, 2, 1) end as char(1)) persisted,
		special_needs_03_disabled_services as cast(case when substring(special_needs, 3, 1) in (' ', '') then null else substring(special_needs, 3, 1) end as char(1)) persisted,
		special_needs_04_transfer_services as cast(case when substring(special_needs, 4, 1) in (' ', '') then null else substring(special_needs, 4, 1) end as char(1)) persisted,
		special_needs_05_employment_assistance as cast(case when substring(special_needs, 5, 1) in (' ', '') then null else substring(special_needs, 5, 1) end as char(1)) persisted,
		special_needs_06_basic_skills as cast(case when substring(special_needs, 6, 1) in (' ', '') then null else substring(special_needs, 6, 1) end as char(1)) persisted,
		special_needs_07_tutoring as cast(case when substring(special_needs, 7, 1) in (' ', '') then null else substring(special_needs, 7, 1) end as char(1)) persisted,
		special_needs_08_esl as cast(case when substring(special_needs, 8, 1) in (' ', '') then null else substring(special_needs, 8, 1) end as char(1)) persisted,
		special_needs_09_eops as cast(case when substring(special_needs, 9, 1) in (' ', '') then null else substring(special_needs, 9, 1) end as char(1)) persisted,
		special_needs_10_calworks as cast(case when substring(special_needs, 10, 1) in (' ', '') then null else substring(special_needs, 10, 1) end as char(1)) persisted,
		special_needs_11_placeholder as cast(case when substring(special_needs, 11, 1) in (' ', '') then null else substring(special_needs, 11, 1) end as char(1)) persisted,
		special_needs_12_placeholder as cast(case when substring(special_needs, 12, 1) in (' ', '') then null else substring(special_needs, 12, 1) end as char(1)) persisted,
		special_needs_13_placeholder as cast(case when substring(special_needs, 13, 1) in (' ', '') then null else substring(special_needs, 13, 1) end as char(1)) persisted,
		special_needs_14_placeholder as cast(case when substring(special_needs, 14, 1) in (' ', '') then null else substring(special_needs, 14, 1) end as char(1)) persisted,
		orientation char(4),
		orientation_01 as cast(case when substring(orientation, 1, 1) in (' ', '') then null else substring(orientation, 1, 1) end as char(1)) persisted,
		orientation_02 as cast(case when substring(orientation, 2, 1) in (' ', '') then null else substring(orientation, 2, 1) end as char(1)) persisted,
		orientation_03 as cast(case when substring(orientation, 3, 1) in (' ', '') then null else substring(orientation, 3, 1) end as char(1)) persisted,
		orientation_04 as cast(case when substring(orientation, 4, 1) in (' ', '') then null else substring(orientation, 4, 1) end as char(1)) persisted,
		assessment char(4),
		assessment_01 as cast(case when substring(assessment, 1, 1) in (' ', '') then null else substring(assessment, 1, 1) end as char(1)) persisted,
		assessment_02 as cast(case when substring(assessment, 2, 1) in (' ', '') then null else substring(assessment, 2, 1) end as char(1)) persisted,
		assessment_03 as cast(case when substring(assessment, 3, 1) in (' ', '') then null else substring(assessment, 3, 1) end as char(1)) persisted,
		assessment_04 as cast(case when substring(assessment, 4, 1) in (' ', '') then null else substring(assessment, 4, 1) end as char(1)) persisted,
		advisement char(4),
		advisement_01 as cast(case when substring(advisement, 1, 1) in (' ', '') then null else substring(advisement, 1, 1) end as char(1)) persisted,
		advisement_02 as cast(case when substring(advisement, 2, 1) in (' ', '') then null else substring(advisement, 2, 1) end as char(1)) persisted,
		advisement_03 as cast(case when substring(advisement, 3, 1) in (' ', '') then null else substring(advisement, 3, 1) end as char(1)) persisted,
		advisement_04 as cast(case when substring(advisement, 4, 1) in (' ', '') then null else substring(advisement, 4, 1) end as char(1)) persisted,
		orientation_services char(1),
		assessment_services_place char(1),
		assessment_services_other char(3),
		assessment_services_other_01 as cast(case when substring(assessment_services_other, 1, 1) in (' ', '') then null else substring(assessment_services_other, 1, 1) end as char(1)) persisted,
		assessment_services_other_02 as cast(case when substring(assessment_services_other, 2, 1) in (' ', '') then null else substring(assessment_services_other, 2, 1) end as char(1)) persisted,
		assessment_services_other_03 as cast(case when substring(assessment_services_other, 3, 1) in (' ', '') then null else substring(assessment_services_other, 3, 1) end as char(1)) persisted,
		advisement_services char(1),
		follow_up_services char(1),
		CONSTRAINT
			pk_smmatric__college_id__student_id__term_id
		PRIMARY KEY CLUSTERED
			(
				college_id,
				student_id,
				term_id
			),