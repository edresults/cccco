USE calpass;

GO

IF (object_id('comis.sssuccess') is not null)
	BEGIN
		DROP TABLE comis.sssuccess;
	END;

GO

CREATE TABLE
	comis.sssuccess
	(
		college_id char(3) not null,
		student_id char(9) not null,
		term_id char(3) not null,
		goals char(1),
		major varchar(6), -- sp04 or cb03
		orientation_status char(2),
		orientation_status_01 as cast(substring(orientation_status, 1, 1) as char(1)) persisted,
		orientation_status_02 as cast(substring(orientation_status, 2, 1) as char(1)) persisted,
		assessment_status char(2),
		assessment_status_01 as cast(substring(assessment_status, 1, 1) as char(1)) persisted,
		assessment_status_02 as cast(substring(assessment_status, 2, 1) as char(1)) persisted,
		educationplan_status char(2),
		educationplan_status_01 as cast(substring(educationplan_status, 1, 1) as char(1)) persisted,
		educationplan_status_02 as cast(substring(educationplan_status, 2, 1) as char(1)) persisted,
		initial_orientation char(1),
		initial_assessment char(4),
		initial_assessment_01 as cast(substring(initial_assessment, 1, 1) as char(1)) persisted,
		initial_assessment_02 as cast(substring(initial_assessment, 2, 1) as char(1)) persisted,
		initial_assessment_03 as cast(substring(initial_assessment, 3, 1) as char(1)) persisted,
		initial_assessment_04 as cast(substring(initial_assessment, 4, 1) as char(1)) persisted,
		counseling char(1),
		education_plan char(1),
		progress char(1),
		success char(4),
		success_01 as cast(substring(success, 1, 1) as char(1)) persisted,
		success_02 as cast(substring(success, 2, 1) as char(1)) persisted,
		success_03 as cast(substring(success, 3, 1) as char(1)) persisted,
		success_04 as cast(substring(success, 4, 1) as char(1)) persisted,
		CONSTRAINT
			pk_sssuccess__college_id__student_id__term_id
		PRIMARY KEY CLUSTERED
			(
				college_id,
				student_id,
				term_id
			)
	);