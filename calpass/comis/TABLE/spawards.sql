/*
	NOTES:
		(1) Primary Key removed because GI92 (record_id) is not properly implemented from the CCCCO
*/

USE calpass;

GO

IF (object_id('comis.spawards') is not null)
	BEGIN
		DROP TABLE comis.spawards;
	END;

GO

CREATE TABLE
	comis.spawards
	(
		college_id char(3) not null,
		student_id char(9) not null,
		term_id char(3) not null,
		top_code char(6) not null,
		award char(1) not null,
		date datetime not null,
		record_id char(1) not null,
		program_code char(5) not null,
		CONSTRAINT
			pk_spawards__college_id__student_id__term_id__top_code__program_code__award__record_id__date
		PRIMARY KEY CLUSTERED
			(
				college_id,
				student_id,
				term_id,
				top_code,
				program_code,
				award,
				record_id,
				date
			)
	);
