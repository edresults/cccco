DROP TABLE IF EXISTS comis.stterm;

CREATE TABLE
    comis.stterm
    (
        college_id            char(3)      not null,
        student_id            char(9)      not null,
        term_id               char(3)      not null,
        age_at_term           integer          null,
        zip                   char(5)          null,
        residency             char(5)          null,
        education             char(5)          null,
        high_school           char(6)          null,
        enrollment            char(1)          null,
        u_earned_loc          decimal(6,2)     null,
        u_earned_trn          decimal(6,2)     null,
        u_attmpt_loc          decimal(6,2)     null,
        u_attmpt_trn          decimal(6,2)     null,
        g_points_loc          decimal(6,2)     null,
        g_points_trn          decimal(6,2)     null,
        gpa_loc               decimal(3,2)     null,
        gpa_tot               decimal(3,2)     null,
        aca_standing          char(1)          null,
        apprentice            char(1)          null,
        transfer_ctr          char(1)          null,
        gain_status           char(1)          null,
        goal                  char(1)          null,
        name_first            varchar(30)      null,
        name_last             varchar(40)      null,
        gender                char(1)          null,
        citizenship           char(1)          null,
        jtpa_status           char(1)          null,
        "1census_crload"      decimal(4,2)     null,
        day_evening_code      char(1)          null,
        day_evening_code2     char(1)          null,
        academic_level        char(1)          null,
        deg_appl_units_earned decimal(4,2)     null,
        headcount_status      char(1)          null,
        calworks_status       char(1)          null,
        multi_race            char(21)         null,
        ipeds_race            char(1)          null,
        bs_waiver             char(1)          null,
        parent_ed             char(2)          null,
        transgender           char(1)          null,
        sexual_orientation    char(1)          null,
    );

ALTER TABLE
    comis.stterm
ADD CONSTRAINT
    pk_stterm
PRIMARY KEY CLUSTERED
    (
        college_id,
        student_id,
        term_id
    );

INSERT INTO
    comis.stterm
SELECT
    college_id,
    student_id,
    term_id,
    age_at_term,
    zip,
    residency,
    education,
    high_school,
    enrollment,
    u_earned_loc,
    u_earned_trn,
    u_attmpt_loc,
    u_attmpt_trn,
    g_points_loc,
    g_points_trn,
    gpa_loc,
    gpa_tot,
    aca_standing,
    apprentice,
    transfer_ctr,
    gain_status,
    goal,
    rtrim(name_first),
    rtrim(name_last),
    gender,
    citizenship,
    jtpa_status,
    "1census_crload",
    day_evening_code,
    day_evening_code2,
    academic_level,
    deg_appl_units_earned,
    headcount_status,
    calworks_status,
    multi_race,
    ipeds_race,
    bs_waiver,
    parent_ed,
    transgender,
    sexual_orientation
FROM
    OPENROWSET(
        BULK N'C:\Data\CC\COMIS\Processing\BFBA7A9C-1720-4CE3-93E6-2D3A0DC02AC2.STTERM_2021-01-29_0820 .txt',
        FORMATFILE = N'C:\Data\CC\COMIS\Processing\stterm.fmt',
        FIRSTROW = 2,
        MAXERRORS = 100
    ) a;