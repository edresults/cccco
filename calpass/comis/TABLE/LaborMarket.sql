DROP TABLE IF EXISTS comis.LaborMarket;

GO

CREATE TABLE
    comis.LaborMarket
    (
        Code                     char(7)      not null,
        OccupationalTitle        varchar(255) not null,
        Counties                 varchar(255) not null,
        Macroregion              varchar(255) not null,
        Microregion              varchar(255) not null,
        Skill                    varchar(255) not null,
        Base2018                 varchar(255) not null,
        Projection2023           varchar(255) not null,
        Change20182023           varchar(255) not null,
        PctChange20182023        varchar(255) not null,
        AverageAnnualJobOpenings varchar(255) not null,
        Q1MedianHourly2018       varchar(255) not null,
        Q1MedianAnnual2018       varchar(255) not null,
        TypicalEducationLevel    varchar(255) not null,
        WorkExperience           varchar(255) not null,
        OnTheJobTraining         varchar(255) not null
    );

GO

TRUNCATE TABLE comis.LaborMarket;

GO

BULK INSERT
    comis.LaborMarket
FROM
    N'\\10.11.6.43\C$\data\cc\comis\processing\LaborMarket2.txt'
WITH
    (
        FIELDTERMINATOR = '\t',
        ROWTERMINATOR = '\n'
    );