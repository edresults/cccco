USE calpass;

GO

IF (object_id('comis.sfawards') is not null)
	BEGIN
		DROP TABLE comis.sfawards;
	END;

GO

CREATE TABLE
	comis.sfawards
	(
		college_id char(3) not null,
		student_id char(9) not null,
		term_id char(3) not null,
		term_recd char(3) not null,
		type_id char(2) not null,
		amount int,
		IsLaunchBoard bit
	);

ALTER TABLE
	comis.sfawards
ADD CONSTRAINT
	PK_sfawards
PRIMARY KEY CLUSTERED
	(
		college_id,
		student_id,
		term_id,
		term_recd,
		type_id
	);

-- GO
-- ALTER TABLE comis.sfwards ADD IsLaunchBoard bit;
-- GO
-- UPDATE
-- 	comis.sfawards
-- SET
-- 	IsLaunchBoard = case when type_id in ('BA','B1','B2','B3','BB','BC','BD','F1','F2','F3','F4','F5','WF','GB','GC','GP','GS','GW','GG') then 1 else 0 end;