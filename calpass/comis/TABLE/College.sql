USE calpass;

GO

IF (object_id('comis.College') is not null)
	BEGIN
		DROP TABLE comis.College;
	END;

GO

CREATE TABLE
	comis.College
	(
		CollegeCode char(3),
		DistrictCode char(3),
		IpedsCode char(6),
		IpedsCodeLegacy char(6),
		CodeLegacy int,
		Name varchar(255),
		CONSTRAINT
			pk_CollegeCode
		PRIMARY KEY CLUSTERED
			(
				CollegeCode
			),
		CONSTRAINT
			uq_Name
		UNIQUE
			(
				Name
			),
		CONSTRAINT
			fk_College_Inherits_DistrictCode
		FOREIGN KEY
			(
				DistrictCode
			)
		REFERENCES
			comis.District
			(
				DistrictCode
			),
		INDEX
			ix_DistrictCode
		NONCLUSTERED
			(
				DistrictCode
			),
		INDEX
			ix_IpedsCodeLegacy
		NONCLUSTERED
			(
				IpedsCodeLegacy
			)
	);

GO

INSERT INTO
	comis.College
	(
		CollegeCode,
		DistrictCode,
		IpedsCode,
		IpedsCodeLegacy,
		CodeLegacy,
		Name
	)
SELECT
	b.Code,
	b.DistrictCode,
	b.IpedsCode,
	b.IpedsCodeLegacy,
	IpedsCodeLegacy = a.organizationId,
	b.Name
FROM
	(
		VALUES
		('611','610','108807','108807','ALLAN HANCOCK'),
		('576','570','999576','999576','CLOVIS'),
		('621','620','109350','109350','ANTELOPE VALLEY'),
		('911','910','109907','109907','BARSTOW'),
		('111','110','110246','110246','BUTTE'),
		('411','410','110334','110334','CABRILLO'),
		('811','810','111887','111887','CERRITOS'),
		('482','480','111920','111920','CHABOT HAYWARD'),
		('481','480','366401','366401','LAS POSITAS'),
		('921','920','111939','111939','CHAFFEY'),
		('821','820','112172','112172','CITRUS'),
		('831','830','112385','112385','COASTLINE'),
		('832','830','115126','115126','GOLDEN WEST'),
		('833','830','120342','120342','ORANGE COAST'),
		('711','710','112686','112686','COMPTON'),
		('311','310','112826','112826','CONTRA COSTA'),
		('312','310','113634','113634','DIABLO VALLEY'),
		('313','310','117894','117894','LOS MEDANOS'),
		('971','970','395362','395362','COPPER MOUNTAIN'),
		('931','930','113573','113573','DESERT'),
		('721','720','113980','113980','EL CAMINO'),
		('121','120','114433','114433','FEATHER RIVER'),
		('421','420','113333','113333','DE ANZA'),
		('422','420','114716','114716','FOOTHILL'),
		('441','440','114938','114938','GAVILAN'),
		('731','730','115001','115001','GLENDALE'),
		('021','020','113218','113218','CUYAMACA'),
		('022','020','115296','115296','GROSSMONT'),
		('451','450','115393','115393','HARTNELL'),
		('031','030','115861','115861','IMPERIAL'),
		('521','520','109819','109819','BAKERSFIELD'),
		('522','520','111896','111896','CERRO COSO'),
		('523','520','121363','121363','PORTERVILLE'),
		('221','220','117195','117195','LAKE TAHOE'),
		('131','130','117274','117274','LASSEN'),
		('841','840','117645','117645','LONG BEACH'),
		('748','740','113856','113856','EAST LA'),
		('741','740','117788','117788','LA CITY'),
		('742','740','117690','117690','LA HARBOR'),
		('74A','740','997788','997788','LA ITV'),
		('743','740','117867','117867','LA MISSION'),
		('744','740','117706','117706','LA PIERCE'),
		('745','740','117715','117715','LA SWEST'),
		('746','740','117724','117724','LA TRADE'),
		('747','740','117733','117733','LA VALLEY'),
		('749','740','125471','125471','WEST LA'),
		('231','230','109208','109208','AMERICAN RIVER'),
		('232','230','113096','113096','COSUMNES RIVER'),
		('234','230','444219','444219','FOLSOM LAKE'),
		('233','230','122180','122180','SACRAMENTO CITY'),
		('334','330','118347','118347','MARIN'),
		('335','330',null,'998347','MARIN CONTINUING'), -- set to null; Alex & Co. duplicate (118347)
		('141','140','118684','118684','MENDOCINO'),
		('531','530','118718','118718','MERCED'),
		('051','050','118912','118912','MIRA COSTA'),
		('461','460','119067','119067','MONTEREY'),
		('851','850','119164','119164','MT. SAN ANTONIO'),
		('941','940','119216','119216','MT. SAN JACINTO'),
		('241','240','119331','119331','NAPA'),
		('861','860','113236','113236','CYPRESS'),
		('862','860','114859','114859','FULLERTON'),
		('863','860',null,'993236','NORTH ORANGE ADULT'), -- set to null; Alex & Co. incorrect (993236)
		('431','430','120290','120290','OHLONE'),
		('951','950','120953','120953','PALO VERDE'),
		('061','060','120971','120971','PALOMAR'),
		('771','770','121044','121044','PASADENA'),
		('341','340','108667','108667','ALAMEDA'),
		('345','340','125170','125170','BERKELEY CITY'),
		('343','340','117247','117247','LANEY'),
		('344','340','118772','118772','MERRITT'),
		('872','870','991619','991619','RANCHO SANTIAGO CED'),
		('871','870','121619','121619','SANTA ANA'),
		('873','870','399212','399212','SANTIAGO CANYON'),
		('161','160','121707','121707','REDWOODS'),
		('881','880','121886','121886','RIO HONDO'),
		('962','960','460394','460394','MORENO VALLEY'),
		('963','960','460464','460464','NORCO College'),
		('961','960','121901','121901','RIVERSIDE'),
		('981','980','113111','113111','CRAFTON HILLS'),
		('982','980','123527','123527','SAN BERNADINO'),
		('071','070','122339','122339','SAN DIEGO CITY'),
		('078','070',null,null,'SAN DIEGO CONTINUING'),
		('072','070','122375','122375','SAN DIEGO MESA'),
		('073','070','122384','122384','SAN DIEGO MIRAMAR'),
		('076','070','113953','113953','SAN DIEGO ADULT'),
		('361','360','112190','112190','SAN FRANCISCO'),
		('363','360',null,'992190','SAN FRANCISCO CTRS'), -- this cannot be right; Alex & Co. 
		('551','550','122658','122658','SAN JOAQUIN DELTA'),
		('471','470','114266','114266','EVERGREEN VALLEY'),
		('472','470','122746','122746','SAN JOSE CITY'),
		('641','640','113193','113193','CUESTA'),
		('371','370','111434','111434','CANADA'),
		('372','370','122791','122791','SAN MATEO'),
		('373','370','123509','123509','SKYLINE'),
		('651','650','122889','122889','SANTA BARBARA'),
		('652','650',null,'992889','SANTA BARBARA CONT'),
		('661','660','111461','111461','CANYONS'),
		('781','780','122977','122977','SATNA MONICA'),
		('561','560','123217','123217','SEQUOIAS'),
		('171','170','123299','123299','SHASTA'),
		('271','270','123341','123341','SIERRA'),
		('181','180','123484','123484','SISKIYOUS'),
		('281','280','123563','123563','SOLANO'),
		('261','260','123013','123013','SANTA ROSA'),
		('892','890','116439','116439','IRVINE'),
		('891','890','122205','122205','SADDLEBACK'),
		('091','090','123800','123800','SOUTHWESTERN'),
		('571','570','114789','114789','FRESNO CITY'),
		('572','570','117052','117052','REEDLEY College'),
		('681','680','119137','119137','MOORPARK'),
		('682','680','120421','120421','OXNARD'),
		('683','680','125028','125028','VENTURA'),
		('991','990','125091','125091','VICTOR VALLEY'),
		('581','580','125462','125462','WEST HILLS COALINGA'),
		('582','580','448594','448594','WEST HILLS LEMOORE'),
		('691','690','124113','124113','TAFT'),
		('492','490','118930','118930','MISSION'),
		('493','490','125499','125499','WEST VALLEY'),
		('591','590','112561','112561','COLUMBIA'),
		('592','590','118976','118976','MODESTO'),
		('292','290','455512','455512','WOODLAND'),
		('291','290','126119','126119','YUBA')
	) b (Code,DistrictCode,IpedsCode,IpedsCodeLegacy,Name)
	left outer join
	calpass.dbo.organization a
		on a.organizationCode = b.IpedsCodeLegacy;

UPDATE
	c
SET
	c.Label = rtrim(t.Label)
FROM
	comis.College c
	inner join
	(
		VALUES
		('021','020','Cuyamaca                                                                   '),
		('022','020','Grossmont                                                                  '),
		('031','030','Imperial                                                                   '),
		('051','050','MiraCosta                                                                  '),
		('061','060','Palomar                                                                    '),
		('071','070','San Diego City                                                             '),
		('072','070','San Diego Mesa                                                             '),
		('073','070','San Diego Miramar                                                          '),
		('076','070','San Diego Adult                                                            '),
		('091','090','Southwestern                                                               '),
		('111','110','Butte                                                                      '),
		('121','120','Feather River                                                              '),
		('131','130','Lassen                                                                     '),
		('141','140','Mendocino                                                                  '),
		('161','160','Redwoods                                                                   '),
		('171','170','Shasta                                                                     '),
		('181','180','Siskiyous                                                                  '),
		('221','220','Lake Tahoe                                                                 '),
		('231','230','American River                                                             '),
		('232','230','Cosumnes River                                                             '),
		('233','230','Sacramento City                                                            '),
		('234','230','Folsom Lake                                                                '),
		('241','240','Napa                                                                       '),
		('261','260','Santa Rosa                                                                 '),
		('271','270','Sierra                                                                     '),
		('281','280','Solano                                                                     '),
		('291','290','Yuba                                                                       '),
		('292','290','Woodland                                                                   '),
		('311','310','Contra Costa                                                               '),
		('312','310','Diablo Valley                                                              '),
		('313','310','Los Medanos                                                                '),
		('334','330','Marin                                                                      '),
		('335','330','Marin Continuing                                                           '),
		('341','340','Alameda                                                                    '),
		('343','340','Laney                                                                      '),
		('344','340','Merritt                                                                    '),
		('345','340','Berkeley City                                                              '),
		('361','360','San Francisco                                                              '),
		('363','360','San Francisco Ctrs                                                         '),
		('371','370','Canada                                                                     '),
		('372','370','San Mateo                                                                  '),
		('373','370','Skyline                                                                    '),
		('411','410','Cabrillo                                                                   '),
		('421','420','Deanza                                                                     '),
		('422','420','Foothill                                                                   '),
		('431','430','Ohlone                                                                     '),
		('441','440','Gavilan                                                                    '),
		('451','450','Hartnell                                                                   '),
		('461','460','Monterey                                                                   '),
		('471','470','Evergreen Valley                                                           '),
		('472','470','San Jose City                                                              '),
		('481','480','Las Positas                                                                '),
		('482','480','Chabot Hayward                                                             '),
		('492','490','Mission                                                                    '),
		('493','490','West Valley                                                                '),
		('521','520','Bakersfield                                                                '),
		('522','520','Cerro Coso                                                                 '),
		('523','520','Porterville                                                                '),
		('531','530','Merced                                                                     '),
		('551','550','San Joaquin Delta                                                          '),
		('561','560','Sequoias                                                                   '),
		('571','570','Fresno City                                                                '),
		('572','570','Reedley College                                                            '),
		('581','580','West Hills Coalinga                                                        '),
		('582','580','West Hills Lemoore                                                         '),
		('591','590','Columbia                                                                   '),
		('592','590','Modesto                                                                    '),
		('611','610','Allan Hancock                                                              '),
		('621','620','Antelope Valley                                                            '),
		('641','640','Cuesta                                                                     '),
		('651','650','Santa Barbara                                                              '),
		('652','650','Santa Barbara Cont                                                         '),
		('661','660','Canyons                                                                    '),
		('681','680','Moorpark                                                                   '),
		('682','680','Oxnard                                                                     '),
		('683','680','Ventura                                                                    '),
		('691','690','Taft                                                                       '),
		('711','710','Compton                                                                    '),
		('721','720','El Camino                                                                  '),
		('731','730','Glendale                                                                   '),
		('741','740','LA City                                                                    '),
		('742','740','LA Harbor                                                                  '),
		('743','740','LA Mission                                                                 '),
		('744','740','LA Pierce                                                                  '),
		('745','740','LA Swest                                                                   '),
		('746','740','LA Trade                                                                   '),
		('747','740','LA Valley                                                                  '),
		('748','740','East LA                                                                    '),
		('749','740','West LA                                                                    '),
		('74A','740','LA ITV                                                                     '),
		('771','770','Pasadena                                                                   '),
		('781','780','Santa Monica                                                               '),
		('811','810','Cerritos                                                                   '),
		('821','820','Citrus                                                                     '),
		('831','830','Coastline                                                                  '),
		('832','830','Golden West                                                                '),
		('833','830','Orange Coast                                                               '),
		('841','840','Long Beach                                                                 '),
		('851','850','Mt. San Antonio                                                            '),
		('861','860','Cypress                                                                    '),
		('862','860','Fullerton                                                                  '),
		('863','860','North Orange Adult                                                         '),
		('871','870','Santa Ana                                                                  '),
		('872','870','Rancho Santiago CED                                                        '),
		('873','870','Santiago Canyon                                                            '),
		('881','880','Rio Hondo                                                                  '),
		('891','890','Saddleback                                                                 '),
		('892','890','Irvine                                                                     '),
		('911','910','Barstow                                                                    '),
		('921','920','Chaffey                                                                    '),
		('931','930','Desert                                                                     '),
		('941','940','Mt. San Jacinto                                                            '),
		('951','950','Palo Verde                                                                 '),
		('961','960','Riverside                                                                  '),
		('962','960','Moreno Valley                                                              '),
		('963','960','Norco College                                                              '),
		('971','970','Copper Mountain                                                            '),
		('981','980','Crafton Hills                                                              '),
		('982','980','San Bernardino                                                             '),
		('991','990','Victor Valley                                                              ')
	) t (CollegeCode, DistrictCode, Label)
		on t.CollegeCode = c.CollegeCode


INSERT INTO
    dbo.College
    (
        CollegeCode,
        DistrictCode,
        IpedsCode,
        IpedsCodeLegacy,
        CodeLegacy,
        Name
    )
VALUES
    (
        '211',
        '210',
        
    )