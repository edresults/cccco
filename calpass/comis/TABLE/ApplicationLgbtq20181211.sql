CREATE TABLE
	comis.ApplicationLgbtq20181211
	(
		app_id               BIGINT       NOT NULL,
		ccc_id               VARCHAR(8)   NOT NULL,
		college_id           CHAR(3)          NULL,
		gender               CHAR(1)          NULL,
		race_ethnic          CHAR(21)         NULL,
		race_group           VARCHAR(255)     NULL,
		transgender          CHAR(1)          NULL,
		orientation          CHAR(1)          NULL,
		CONSTRAINT PK_ApplicationLgbtq20181211 PRIMARY KEY CLUSTERED (app_id),
		INDEX IX_Application20181211_ccc_id NONCLUSTERED (ccc_id)
	);