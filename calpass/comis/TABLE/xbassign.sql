DROP TABLE comis.xeassign;

CREATE TABLE
    comis.xeassign
    (
        college_id     char(3)      not null,
        term_id        char(3)      not null,
        course_id      varchar(12)  not null,
        section_id     varchar(6)   not null,
        control_number char(12)     not null,
        session_id     char(2)      not null,
        employee_id    char(9)      not null,
        type           char(1)      not null,
        "percent"      smallint     not null,
        fte            decimal(5,2) not null CONSTRAINT df_xeassign_fte DEFAULT ( 0.00 ),
        hourly_rate    decimal(5,2) not null
    );

ALTER TABLE
    comis.xeassign
ADD CONSTRAINT
    pk_xeassign
PRIMARY KEY
    (
        college_id,
        term_id,
        course_id,
        control_number,
        section_id,
        session_id,
        employee_id,
        type,
        fte
    );

BULK INSERT
    comis.xeassign
FROM
    N'C:\Data\CC\COMIS\Processing\mods\XEASSIGN_2021-01-29_0820 .txt'
WITH
    (
        FIRSTROW = 2
    );