USE calpass;

GO

IF (object_id('comis.sddsps') is not null)
	BEGIN
		DROP TABLE comis.sddsps;
	END;

GO

CREATE TABLE
	comis.sddsps
	(
		college_id char(3) not null,
		student_id char(9) not null,
		term_id char(3) not null,
		primary_disability char(1),
		pri_disab_srvc_contacts smallint,
		secondary_disability char(1),
		sec_disab_srvc_contacts smallint,
		dept_rehab char(1),
		CONSTRAINT
			pk_sddsps__college_id__student_id__term_id
		PRIMARY KEY CLUSTERED
			(
				college_id,
				student_id,
				term_id
			)
	);