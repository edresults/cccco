DROP TABLE comis.SAASEMENT;

GO

CREATE TABLE
    comis.SAASEMENT
    (
        COLLEGE_ID                 char(3)    not null,
        STUDENT_ID                 binary(64) not null,
        TERM_ID                    char(3)    not null,
        INSTRUMENT_ID              char(4)    not null,
        ACCOMMODATION              char(4)    not null,
        PURPOSE                    char(2)    not null,
        DATE_ID                    date       not null,
        EDUCATIONAL_FUNCTION_LEVEL char(2)    not null,
        CONSTRAINT PK_SAASESMENT PRIMARY KEY CLUSTERED ( COLLEGE_ID, STUDENT_ID, TERM_ID, INSTRUMENT_ID, PURPOSE, DATE_ID )
    );

-- GO

-- BULK INSERT
--     comis.saasement
-- FROM
--     N'\\10.11.6.43\C$\Data\CC\COMIS\Processing\SAASEMENT200512.txt'
-- WITH
--     (
--         TABLOCK,
--         FIRSTROW = 2,
--         FIELDTERMINATOR = ','
--     );