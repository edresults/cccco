USE calpass;

IF (object_id('comis.sfappl') is not null)
	BEGIN
		DROP TABLE comis.sfappl;
	END;

GO

CREATE TABLE
	comis.sfappl
	(
		college_id char(3),
		student_id char(9),
		term_id char(3),
		status char(1),
		time_period char(5),
		budget_category char(1),
		budget_amount int,
		dependecy_status char(1),
		household_size tinyint,
		family_status char(2),
		income_agi_parent int,
		income_agi_student int,
		untax_inc_parent int,
		untax_inc_student int,
		efc int,
		CONSTRAINT
			pk_sfappl__college_id__student_id__term_id
		PRIMARY KEY CLUSTERED
			(
				college_id,
				student_id,
				term_id
			)
	);