USE calpass;

GO

IF (object_id('comis.LegacyMigrateAnnual') is not null)
    BEGIN
        DROP PROCEDURE comis.LegacyMigrateAnnual
    END;

GO

CREATE PROCEDURE
    comis.LegacyMigrateAnnual
    (
        @CollegeId char(3),
        @YearId char(3)
    )
AS

    SET NOCOUNT ON;
    SET XACT_ABORT ON;

DECLARE
    @True    bit = 1,
    @Target  sysname,
    @Message nvarchar(2048),
    @IpedsId char(6);

BEGIN

    SELECT
        @IpedsId = IpedsCodeLegacy
    FROM
        comis.College
    WHERE
        CollegeCode = @CollegeId;

	SET @Target = 'CCAwardProd';
	-- CCAwardProd
	MERGE
		dbo.CCAwardProd
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				CollegeId      = @IpedsId,
				StudentComisId = id.student_id,
				StudentId      = id.ssn,
				IdStatus       = id.student_id_status,
				TermId         = aw.term_id,
				TopCode        = aw.top_code,
				Award          = aw.award,
				DateOfAward    = convert(char(8), aw.date, 112),
				RecordId       = aw.record_id,
				ProgramCode    = aw.program_code,
				Derkey1        = id.InterSegmentKey
			FROM
				comis.spawards aw
				inner join
				comis.studntid id
					on aw.college_id = id.college_id
					and aw.student_id = id.student_id
			WHERE
				aw.college_id = @CollegeId
				and aw.term_id = @YearId
		) s
	ON
		t.CollegeId = s.CollegeId
		and t.StudentId = s.StudentId
		and t.TermId = s.TermId
		and t.TopCode = s.TopCode
		and t.Award = s.Award
		and t.ProgramCode = s.ProgramCode
		and t.DateOfAward = s.DateOfAward
		and t.RecordId = s.RecordId
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				TopCode,
				Award,
				DateOfAward,
				RecordId,
				ProgramCode,
				Derkey1
			)
		VALUES
			(
				s.CollegeId,
				s.StudentComisId,
				s.StudentId,
				s.IdStatus,
				s.TermId,
				s.TopCode,
				s.Award,
				s.DateOfAward,
				s.RecordId,
				s.ProgramCode,
				s.Derkey1
			)
	WHEN MATCHED THEN
		UPDATE SET
			t.StudentComisId = s.StudentComisId,
			t.IdStatus = s.IdStatus,
			t.Derkey1 = s.Derkey1;

	SET @Message = 'Target:  ' + @Target + char(13) + char(10) + 'Records: ' + convert(nvarchar(2048), @@ROWCOUNT);
	RAISERROR(@Message, 0, 1) WITH NOWAIT;

	SET @Target = 'CCFinAidAwardsProd';
	-- CCFinAwardsProd
	MERGE
		dbo.CCFinAidAwardsProd
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				CollegeId      = @IpedsId,
				StudentComisId = id.student_id,
				StudentId      = id.ssn,
				IdStatus       = id.student_id_status,
				TermId         = sf.term_id,
				TermRecd       = sf.term_recd,
				TypeId         = sf.type_id,
				Amount         = sf.amount,
				Derkey1        = id.InterSegmentKey
			FROM
				comis.sfawards sf
				inner join
				comis.studntid id
					on sf.college_id = id.college_id
					and sf.student_id = id.student_id
			WHERE
				sf.college_id = @CollegeId
				and sf.term_id = @YearId
		) s
	ON
		s.CollegeId = t.CollegeId
		and s.StudentId = t.StudentId
		and s.TermId = t.TermId
		and s.TermRecd = t.TermRecd
		and s.TypeId = t.TypeId
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				TermRecd,
				TypeId,
				Amount,
				Derkey1
			)
		VALUES
			(
				s.CollegeId,
				s.StudentComisId,
				s.StudentId,
				s.IdStatus,
				s.TermId,
				s.TermRecd,
				s.TypeId,
				s.Amount,
				s.Derkey1
			)
	WHEN MATCHED THEN
		UPDATE SET
			t.StudentComisId = s.StudentComisId,
			t.IdStatus = s.IdStatus,
			t.Amount = s.Amount,
			t.Derkey1 = s.Derkey1;

	SET @Message = 'Target:  ' + @Target + char(13) + char(10) + 'Records: ' + convert(nvarchar(2048), @@ROWCOUNT);
	RAISERROR(@Message, 0, 1) WITH NOWAIT;
END;