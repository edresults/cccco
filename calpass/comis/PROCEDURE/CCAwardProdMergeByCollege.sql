USE calpass;

GO

IF (object_id('comis.CCAwardProdMergeByCollege') is not null)
	BEGIN
		DROP PROCEDURE comis.CCAwardProdMergeByCollege;
	END;

GO

CREATE PROCEDURE
	comis.CCAwardProdMergeByCollege
	(
		@CollegeCode char(3)
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@IpedsCode char(6);

BEGIN

	SELECT
		@IpedsCode = IpedsCodeLegacy
	FROM
		comis.College
	WHERE
		CollegeCode = @CollegeCode;

	MERGE
		dbo.CCAwardProd
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				CollegeId      = @IpedsCode,
				StudentComisId = id.student_id,
				StudentId      = id.ssn,
				IdStatus       = id.student_id_status,
				TermId         = aw.term_id,
				TopCode        = aw.top_code,
				Award          = aw.award,
				DateOfAward    = convert(char(8), aw.date, 112),
				RecordId       = aw.record_id,
				ProgramCode    = aw.program_code,
				Derkey1        = id.InterSegmentKey
			FROM
				calpass.comis.spawards aw
				inner join
				calpass.comis.studntid id
					on aw.college_id = id.college_id
					and aw.student_id = id.student_id
			WHERE
				aw.college_id = @CollegeCode
		) s
	ON
		(
			s.CollegeId       = t.CollegeId
			and s.StudentId   = t.StudentId
			and s.TermId      = t.TermId
			and s.TopCode     = t.TopCode
			and s.Award       = t.Award
			and s.ProgramCode = t.ProgramCode
			and s.DateOfAward = t.DateOfAward
			and s.RecordId    = t.RecordId
		)
	WHEN MATCHED THEN
		UPDATE SET
			t.StudentComisId = s.StudentComisId,
			t.IdStatus       = s.IdStatus
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				TopCode,
				Award,
				DateOfAward,
				RecordId,
				ProgramCode,
				Derkey1
			)
		VALUES
			(
				s.CollegeId,
				s.StudentComisId,
				s.StudentId,
				s.IdStatus,
				s.TermId,
				s.TopCode,
				s.Award,
				s.DateOfAward,
				s.RecordId,
				s.ProgramCode,
				s.Derkey1
			);
END;