USE calpass;

GO

IF (object_id('comis.StudentProcess') is not null)
	BEGIN
		DROP PROCEDURE comis.StudentProcess;
	END;

GO

CREATE PROCEDURE
	comis.StudentProcess
	(
		@CollegeCode char(3) = null
	)
AS

SET NOCOUNT ON;

DECLARE
	@Time       datetime,
	@Seconds    int,
	@Severity   tinyint = 0,
	@State      tinyint = 1,
	@Error      varchar(2048),
	@Increment  int = 100000,
	@Iterator   int = 0,
	@Iterations int,
	@Count      int;

	IF (object_id('tempdb..#Student') is not null)
		BEGIN
			DROP TABLE #Student;
		END;

	CREATE TABLE
		#Student
		(
			i               int        not null identity(0,1) primary key,
			InterSegmentKey binary(64) not null
		);

BEGIN

	INSERT
		#Student
		(
			InterSegmentKey
		)
	SELECT DISTINCT
		InterSegmentKey
	FROM
		comis.studntid
	WHERE
		InterSegmentKey is not null
	ORDER BY
		InterSegmentKey;

	SELECT
		@Count = count(*)
	FROM
		#Student;

	SET @Iterations = floor(1.0 * @Count / @Increment);

	WHILE (@Iterator < @Iterations)
	BEGIN

		SET @Error = convert(varchar, @Iterator) + ' of ' + convert(varchar, ceiling(convert(decimal, @count) / @increment));

		RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

		MERGE
			comis.Student
		WITH
			(
				TABLOCKX
			) t
		USING
			(
				SELECT
					InterSegmentKey = id.InterSegmentKey,
					IsCollision     = case when count(distinct case when id.student_id_status = 'S' then id.ssn end) > 1 then 1 else 0 end,
					IsMutation      = case when count(distinct case when id.student_id_status = 'S' then id.ssn end) = 1 and count(distinct id.student_id_status) > 1 then 1 else 0 end,
					IsMultiCollege  = case when count(distinct case when id.student_id_status = 'S' then id.ssn end) = 1 and count(distinct id.college_id) > 1 then 1 else 0 end,
					IsMultiDistrict = case when count(distinct case when id.student_id_status = 'S' then id.ssn end) = 1 and count(distinct c.DistrictCode) > 1 then 1 else 0 end
				FROM
					comis.studntid id
					inner join
					comis.College c
						on c.CollegeCode = id.college_id
				WHERE
					exists (
						SELECT
							1
						FROM
							#Student s
						WHERE
							i >= (@Increment * @Iterator)
							and i < (@Increment * (@Iterator + 1))
							and s.InterSegmentKey = id.InterSegmentKey
					)
				GROUP BY
					id.InterSegmentKey
			) s
		ON
			(
				s.InterSegmentKey = t.InterSegmentKey
			)
		WHEN MATCHED THEN
			UPDATE SET
				t.IsCollision     = s.IsCollision,
				t.IsMutation      = s.IsMutation,
				t.IsMultiCollege  = s.IsMultiCollege,
				t.IsMultiDistrict = s.IsMultiDistrict
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					InterSegmentKey,
					IsCollision,
					IsMutation,
					IsMultiCollege,
					IsMultiDistrict
				)
			VALUES
				(
					s.InterSegmentKey,
					s.IsCollision,
					s.IsMutation,
					s.IsMultiCollege,
					s.IsMultiDistrict
				);

		-- get time elapsed
		SET @Seconds = datediff(second, @Time, getdate());
		-- set msg
		SET @Error = 'ITERATION: ' + 
			convert(nvarchar, @Seconds / 86400) + ':' +
			convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
		-- output to user
		RAISERROR(@Error, 0, 1) WITH NOWAIT;

		SET @Iterator += 1;
	END;
END;