USE calpass;

GO

IF (object_id('comis.p_StudentSubjectCourseFirst_merge') is not null)
	BEGIN
		DROP PROCEDURE comis.p_StudentSubjectCourseFirst_merge;
	END;

GO

CREATE PROCEDURE
	comis.p_StudentSubjectCourseFirst_merge
	(
		@StudentSubjectCourseFirst comis.StudentSubjectCourseFirst READONLY
	)
AS

	SET NOCOUNT ON;

BEGIN
	MERGE
		comis.StudentSubjectCourseFirst t
	USING
		@StudentSubjectCourseFirst s
	ON
		s.InterSegmentKey = t.InterSegmentKey
		and s.SubjectCode = t.SubjectCode
	WHEN MATCHED THEN
		UPDATE SET
			t.CollegeCode = s.CollegeCode,
			t.TermCodeFull = s.TermCodeFull,
			t.CourseId = s.CourseId,
			t.CourseTitle = s.CourseTitle,
			t.CourseLevelCode = s.CourseLevelCode,
			t.CourseLevelRank = s.CourseLevelRank,
			t.SectionUnitsAttempted = s.SectionUnitsAttempted,
			t.SectionGradeCode = s.SectionGradeCode,
			t.SectionGradeRank = s.SectionGradeRank
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
		(
			InterSegmentKey,
			SubjectCode,
			CollegeCode,
			TermCodeFull,
			CourseId,
			CourseTitle,
			CourseLevelCode,
			CourseLevelRank,
			SectionUnitsAttempted,
			SectionGradeCode,
			SectionGradeRank
		)
	VALUES
		(
			s.InterSegmentKey,
			s.SubjectCode,
			s.CollegeCode,
			s.TermCodeFull,
			s.CourseId,
			s.CourseTitle,
			s.CourseLevelCode,
			s.CourseLevelRank,
			s.SectionUnitsAttempted,
			s.SectionGradeCode,
			s.SectionGradeRank
		);
END;