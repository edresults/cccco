USE calpass;

GO

IF (object_id('comis.p_StudentSubjectCourseFirst_select') is not null)
	BEGIN
		DROP PROCEDURE comis.p_StudentSubjectCourseFirst_select;
	END;

GO

CREATE PROCEDURE
	comis.p_StudentSubjectCourseFirst_select
	(
		@Student comis.Student READONLY
	)
AS

	SET NOCOUNT ON;

DECLARE
	@StudentSubjectCourseFirst comis.StudentSubjectCourseFirst;

BEGIN
	-- populate container
	INSERT INTO
		@StudentSubjectCourseFirst
		(
			InterSegmentKey,
			SubjectCode,
			CollegeCode,
			TermCodeFull,
			CourseId,
			CourseTitle,
			CourseLevelCode,
			CourseLevelRank,
			SectionUnitsAttempted,
			SectionGradeCode,
			SectionGradeRank,
			TotalSubjectUnitsAttempted
		)
	SELECT
		InterSegmentKey = s.InterSegmentKey,
		SubjectCode = p.SubjectCode,
		CollegeCode = sx.college_id,
		TermCodeFull = t.YearTermCode,
		CourseId = sx.course_id,
		CourseTitle = cb.title,
		CourseLevelCode = cb.prior_to_college,
		CourseLevelRank = cl.Rank,
		SectionUnitsAttempted = sx.units_attempted,
		SectionGradeCode = sx.grade,
		SectionGradeRank = g.Rank,
		TotalSubjectUnitsAttempted = sum(sx.units_attempted) over(partition by s.InterSegmentKey, sx.College_id, p.SubjectCode)
	FROM
		@Student s
		inner join
		comis.sxenrlm sx
			on s.CollegeCode = sx.college_id
			and s.StudentId = sx.student_id
		inner join
		comis.cbcrsinv cb
			on cb.college_id = sx.college_id
			and cb.term_id = sx.term_id
			and cb.course_id = sx.course_id
			and cb.control_number = sx.control_number
		inner join
		comis.Grade g
			on g.GradeCode = sx.grade
		inner join
		comis.Term t
			on t.TermCode = sx.term_id
		inner join
		comis.CourseLevel cl
			on cl.CourseLevelCode = cb.prior_to_college
		inner join
		comis.Program p
			on p.ProgramCode = cb.top_code
	WHERE
		p.SubjectCode in ('MATH','ENGL','ESL')
		and g.enroll_ind = 1;

	-- Time :: Lowest YearTerm Value
	DELETE
		a
	FROM
		@StudentSubjectCourseFirst a
	WHERE
		exists (
			SELECT
				1
			FROM
				@StudentSubjectCourseFirst a1
			WHERE
				a1.InterSegmentKey = a.InterSegmentKey
				and a1.SubjectCode = a.SubjectCode
			HAVING
				min(a1.TermCodeFull) != a.TermCodeFull
		);

	-- Rigor :: Lowest CB21 Rank
	DELETE
		a
	FROM
		@StudentSubjectCourseFirst a
	WHERE
		exists (
			SELECT
				1
			FROM
				@StudentSubjectCourseFirst a1
			WHERE
				a1.InterSegmentKey = a.InterSegmentKey
				and a1.SubjectCode = a.SubjectCode
				and a1.TermCodeFull = a.TermCodeFull
			HAVING
				min(a1.CourseLevelRank) != a.CourseLevelRank
		);
	
	-- Rigor :: Most Units Attempted
	DELETE
		a
	FROM
		@StudentSubjectCourseFirst a
	WHERE
		exists (
			SELECT
				1
			FROM
				@StudentSubjectCourseFirst a1
			WHERE
				a1.InterSegmentKey = a.InterSegmentKey
				and a1.SubjectCode = a.SubjectCode
				and a1.TermCodeFull = a.TermCodeFull
				and a1.CourseLevelRank = a.CourseLevelRank
			HAVING
				max(a1.SectionUnitsAttempted) != a.SectionUnitsAttempted
		);
	
	-- Rigor :: Highest Grade Rank
	DELETE
		a
	FROM
		@StudentSubjectCourseFirst a
	WHERE
		exists (
			SELECT
				1
			FROM
				@StudentSubjectCourseFirst a1
			WHERE
				a1.InterSegmentKey = a.InterSegmentKey
				and a1.SubjectCode = a.SubjectCode
				and a1.TermCodeFull = a.TermCodeFull
				and a1.CourseLevelRank = a.CourseLevelRank
				and a1.SectionUnitsAttempted = a.SectionUnitsAttempted
			HAVING
				min(a1.SectionGradeRank) != a.SectionGradeRank
		);
	
	-- Intent :: Most Units Within Subject
	DELETE
		a
	FROM
		@StudentSubjectCourseFirst a
	WHERE
		exists (
			SELECT
				1
			FROM
				@StudentSubjectCourseFirst a1
			WHERE
				a1.InterSegmentKey = a.InterSegmentKey
				and a1.SubjectCode = a.SubjectCode
				and a1.TermCodeFull = a.TermCodeFull
				and a1.CourseLevelRank = a.CourseLevelRank
				and a1.SectionUnitsAttempted = a.SectionUnitsAttempted
				and a1.SectionGradeRank = a.SectionGradeRank
			HAVING
				max(a1.TotalSubjectUnitsAttempted) != a.TotalSubjectUnitsAttempted
		);
	
	-- Sequence :: Highest CourseId
	DELETE
		a
	FROM
		@StudentSubjectCourseFirst a
	WHERE
		exists (
			SELECT
				1
			FROM
				@StudentSubjectCourseFirst a1
			WHERE
				a1.InterSegmentKey = a.InterSegmentKey
				and a1.SubjectCode = a.SubjectCode
				and a1.TermCodeFull = a.TermCodeFull
				and a1.CourseLevelRank = a.CourseLevelRank
				and a1.SectionUnitsAttempted = a.SectionUnitsAttempted
				and a1.SectionGradeRank = a.SectionGradeRank
				and a1.TotalSubjectUnitsAttempted = a.TotalSubjectUnitsAttempted
			HAVING
				max(a1.CourseId) != a.CourseId
		);
	
	-- remove any remaining duplicates
	DELETE
		a
	FROM
		@StudentSubjectCourseFirst a
	WHERE
		exists (
			SELECT
				1
			FROM
				@StudentSubjectCourseFirst a1
			WHERE
				a1.InterSegmentKey = a.InterSegmentKey
				and a1.SubjectCode = a.SubjectCode
				and a1.TermCodeFull = a.TermCodeFull
				and a1.CourseLevelRank = a.CourseLevelRank
				and a1.SectionUnitsAttempted = a.SectionUnitsAttempted
				and a1.SectionGradeRank = a.SectionGradeRank
				and a1.TotalSubjectUnitsAttempted = a.TotalSubjectUnitsAttempted
				and a1.CourseId = a.CourseId
			HAVING
				count(a1.SubjectCode) > 1
		);

	SELECT DISTINCT
		InterSegmentKey,
		SubjectCode,
		CollegeCode,
		TermCodeFull,
		CourseId,
		CourseTitle,
		CourseLevelCode,
		CourseLevelRank,
		SectionUnitsAttempted,
		SectionGradeCode,
		SectionGradeRank,
		TotalSubjectUnitsAttempted
	FROM
		@StudentSubjectCourseFirst;
END;