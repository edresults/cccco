USE calpass;

GO

IF (object_id('comis.TranscriptSummaryProcess') is not null)
	BEGIN
		DROP PROCEDURE comis.TranscriptSummaryProcess;
	END;

GO

CREATE PROCEDURE
	comis.TranscriptSummaryProcess
	(
		@CollegeCode char(3) = null
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	SET ANSI_WARNINGS OFF;

DECLARE
	@Message     nvarchar(2048),
	@Time        datetime,
	@Seconds     int,
	@Severity    tinyint = 0,
	@State       tinyint = 1,
	@Error       varchar(2048),
	@Iterator    int = 0,
	@Iterations  int,
	@College     comis.College;

BEGIN

	TRUNCATE TABLE comis.TranscriptSummary;

	INSERT
		@College
		(
			CollegeCode
		)
	SELECT
		CollegeCode
	FROM
		comis.College
	WHERE
		(
			@CollegeCode is null
			or
			CollegeCode = @CollegeCode
		)
	ORDER BY
		CollegeCode;

	SELECT
		@Iterations = count(*)
	FROM
		@College;

	WHILE (@Iterator < @Iterations)
	BEGIN

		SET @Error = convert(varchar, @Iterator + 1) + ' of ' + convert(varchar, @Iterations);

		RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

		SET @Time = getdate();
		SELECT
			@CollegeCode = CollegeCode
		FROM
			@College
		WHERE
			Iterator = @Iterator;

		INSERT INTO
			comis.TranscriptSummary
			(
				CollegeCode,
				StudentId,
				YearTermCode,
				SubjectCode,
				CourseLevelCode,
				CourseId,
				CourseTitle,
				CourseUnitsAttempted,
				MarkCode,
				MarkPoints,
				MarkCategory,
				MarkIsSuccess,
				FirstSelector,
				LevelSelector
			)
		SELECT
			CollegeCode          = sx.college_id,
			StudentId            = sx.student_id,
			YearTermCode         = t.YearTermCode,
			SubjectCode          = p.SubjectCode,
			CourseLevelCode      = cb.prior_to_college,
			CourseId             = sx.course_id,
			CourseTitle          = cb.title,
			CourseUnitsAttempted = sx.units_attempted,
			MarkCode             = sx.grade,
			MarkPoints           = m.points,
			MarkCategory         = m.category,
			MarkIsSuccess        = m.IsSuccess,
			FirstSelector = 
				row_number() OVER(
					PARTITION BY
						sx.college_id,
						sx.student_id,
						p.SubjectCode
					ORDER BY
						t.YearTermCode ASC,
						l.rank ASC,
						sx.units_attempted DESC, --rigor
						m.rank ASC, -- performance
						cb.course_id ASC
				),
			LevelSelector = 
				row_number() OVER(
					PARTITION BY
						sx.college_id,
						sx.student_id,
						p.SubjectCode,
						l.rank
					ORDER BY
						t.YearTermCode ASC,
						sx.units_attempted DESC, --rigor
						m.rank ASC, -- performance
						cb.course_id ASC
				)
		FROM
			comis.sxenrlm sx
			inner join
			comis.cbcrsinv cb
				on cb.college_id = sx.college_id
				and cb.course_id = sx.course_id
				and cb.control_number = sx.control_number
				and cb.term_id = sx.term_id
			inner join
			comis.Mark m
				on m.MarkCode = sx.grade
			inner join
			comis.Term t
				on t.TermCode = sx.term_id
			inner join
			comis.CourseLevel l
				on l.CourseLevelCode = cb.prior_to_college
			inner join
			comis.Program p
				on p.ProgramCode = cb.top_code
		WHERE
			sx.college_id = @CollegeCode
			and sx.units_attempted > 1
			and sx.credit_flag in ('T','D','C','S')
			and p.SubjectCode is not null
			and (
				p.SubjectCode in ('READ', 'ESL')
				or
				(
					cb.top_code = '170100'
					and
					(
						cb.prior_to_college <> 'Y'
						or
						(
							cb.prior_to_college = 'Y'
							and
							cb.transfer_status in ('A', 'B') -- must be A and B; trig courses can be code CSU only
						)
					)
				)
				or
				(
					cb.top_code = '150100'
					and
					(
						cb.Title not like '%LIT%'
						and
						(
							cb.prior_to_college <> 'Y'
							or
							(
								cb.prior_to_college = 'Y'
								and
								cb.transfer_status = 'A'
							)
						)
					)
				)
			);

		-- get time elapsed
		SET @Seconds = datediff(second, @Time, getdate());
		-- set msg
		SET @Error = 'ITERATION: ' + 
			convert(nvarchar, @Seconds / 86400) + ':' +
			convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
		-- output to user
		RAISERROR(@Error, 0, 1) WITH NOWAIT;

		SET @Iterator += 1;
	END;
END;