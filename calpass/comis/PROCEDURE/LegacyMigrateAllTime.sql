USE calpass;

GO

IF (object_id('comis.LegacyMigrateAllTime') is not null)
    BEGIN
        DROP PROCEDURE comis.LegacyMigrateAllTime
    END;

GO

CREATE PROCEDURE
    comis.LegacyMigrateAllTime
    (
        @CollegeId char(3)
    )
AS

    SET NOCOUNT ON;
    SET XACT_ABORT ON;

DECLARE
    @True    bit = 1,
    @Target  sysname,
    @Message nvarchar(2048);

BEGIN

	SET @Target = 'STUDNTID';

	UPDATE
		id
	SET
		InterSegmentKey =
			HASHBYTES(
				'SHA2_512',
				upper(convert(nchar(3), st.name_first)) + 
				upper(convert(nchar(3), st.name_last)) + 
				upper(convert(nchar(1), st.gender)) + 
				convert(nchar(8), sb.birthdate, 112)
			)
	FROM
		comis.studntid id
		inner join
		comis.sbstudnt sb
			on id.college_id = sb.college_id
			and id.student_id = sb.student_id
		inner join
		comis.stterm st
			on id.college_id = st.college_id
			and id.student_id = st.student_id
	WHERE
		id.college_id = @CollegeId
		and st.term_id = (
			SELECT
				min(st1.term_id)
			FROM
				comis.stterm st1
			WHERE
				st1.college_id = st.college_id
				and st1.student_id = st.student_id
		)
		and st.IsEscrow = @True;

	SET @Message = 'Target:  ' + @Target + char(13) + char(10) + '  Records: ' + convert(nvarchar(2048), @@ROWCOUNT);
	RAISERROR(@Message, 0, 1) WITH NOWAIT;
END;