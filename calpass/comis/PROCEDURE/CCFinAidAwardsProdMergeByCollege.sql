USE calpass;

GO

IF (object_id('comis.CCFinAidAwardsProdMergeByCollege') is not null)
	BEGIN
		DROP PROCEDURE comis.CCFinAidAwardsProdMergeByCollege;
	END;

GO

CREATE PROCEDURE
	comis.CCFinAidAwardsProdMergeByCollege
	(
		@CollegeCode char(3)
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@IpedsCode char(6);

BEGIN

	SELECT
		@IpedsCode = IpedsCodeLegacy
	FROM
		comis.College
	WHERE
		CollegeCode = @CollegeCode;

	MERGE
		dbo.CCFinAidAwardsProd
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				CollegeId      = @IpedsCode,
				StudentComisId = id.student_id,
				StudentId      = id.ssn,
				IdStatus       = id.student_id_status,
				TermId         = sf.term_id,
				TermRecd       = sf.term_recd,
				TypeId         = sf.type_id,
				Amount         = sf.amount,
				Derkey1        = id.InterSegmentKey
			FROM
				calpass.comis.sfawards sf
				inner join
				calpass.comis.studntid id
					on sf.college_id = id.college_id
					and sf.student_id = id.student_id
			WHERE
				sf.college_id = @CollegeCode
		) s
	ON
		(
			s.CollegeId     = t.CollegeId
			and s.StudentId = t.StudentId
			and s.TermId    = t.TermId
			and s.TermRecd  = t.TermRecd
			and s.TypeId    = t.TypeId
		)
	WHEN MATCHED THEN
		UPDATE SET
			t.StudentComisId = s.StudentComisId,
			t.IdStatus       = s.IdStatus,
			t.Amount         = s.Amount,
			t.Derkey1        = s.Derkey1
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				TermRecd,
				TypeId,
				Amount,
				Derkey1
			)
		VALUES
			(
				s.CollegeId,
				s.StudentComisId,
				s.StudentId,
				s.IdStatus,
				s.TermId,
				s.TermRecd,
				s.TypeId,
				s.Amount,
				s.Derkey1
			);
END;