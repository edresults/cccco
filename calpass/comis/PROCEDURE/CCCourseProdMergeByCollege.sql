USE calpass;

GO

IF (object_id('comis.CCCourseProdMergeByCollege') is not null)
	BEGIN
		DROP PROCEDURE comis.CCCourseProdMergeByCollege;
	END;

GO

CREATE PROCEDURE
	comis.CCCourseProdMergeByCollege
	(
		@CollegeCode char(3)
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@IpedsCode char(6);

BEGIN

	SELECT
		@IpedsCode = IpedsCodeLegacy
	FROM
		comis.College
	WHERE
		CollegeCode = @CollegeCode;

	MERGE
		dbo.CCCourseProd
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				CollegeId = @IpedsCode,
				StudentComisId = sx.student_id,
				StudentId = id.ssn,
				IdStatus = id.student_id_status,
				TermId = sx.term_id,
				CourseId = sx.course_id,
				SectionId = sx.section_id,
				Title = cb.title,
				UnitsMax = cb.units_maximum,
				UnitsAttempted = sx.units_attempted,
				UnitsEarned = sx.units,
				Grade = sx.grade,
				CreditFlag = sx.credit_flag,
				TopCode = cb.top_code,
				TransferStatus = cb.transfer_status,
				BSStatus = cb.basic_skills_status,
				SamCode = cb.sam_code,
				ClassCode = cb.classification_code,
				CollegeLevel = cb.prior_to_college,
				NCRCategory = cb.noncredit_category,
				CCLongTermId = t.YearTermCode
			FROM
				comis.studntid id
				inner join
				comis.sxenrlm sx
					on id.college_id = sx.college_id
					and id.student_id = sx.student_id
				inner join
				comis.cbcrsinv cb
					on sx.college_id = cb.college_id
					and sx.term_id = cb.term_id
					and sx.course_id = cb.course_id
					and sx.control_number = cb.control_number
				inner join
				comis.Term t
					on t.TermCode = sx.term_id
			WHERE
				sx.college_id = @CollegeCode
		) s
	ON
		(
			s.CollegeId = t.CollegeId
			and s.StudentId = t.StudentId
			and s.TermId = t.TermId
			and s.CourseId = t.CourseId
			and s.SectionId = t.SectionId
		)
	WHEN MATCHED THEN
		UPDATE SET
			t.StudentComisId = s.StudentComisId,
			t.IdStatus       = s.IdStatus,
			t.Title          = s.Title,
			t.UnitsMax       = s.UnitsMax,
			t.UnitsAttempted = s.UnitsAttempted,
			t.UnitsEarned    = s.UnitsEarned,
			t.Grade          = s.Grade,
			t.CreditFlag     = s.CreditFlag,
			t.TopCode        = s.TopCode,
			t.TransferStatus = s.TransferStatus,
			t.BSStatus       = s.BSStatus,
			t.SamCode        = s.SamCode,
			t.ClassCode      = s.ClassCode,
			t.CollegeLevel   = s.CollegeLevel,
			t.NCRCategory    = s.NCRCategory,
			t.CCLongTermId   = s.CCLongTermId
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				CourseId,
				SectionId,
				Title,
				UnitsMax,
				UnitsAttempted,
				UnitsEarned,
				Grade,
				CreditFlag,
				TopCode,
				TransferStatus,
				BSStatus,
				SamCode,
				ClassCode,
				CollegeLevel,
				NCRCategory,
				CCLongTermId
			)
		VALUES
			(
				s.CollegeId,
				s.StudentComisId,
				s.StudentId,
				s.IdStatus,
				s.TermId,
				s.CourseId,
				s.SectionId,
				s.Title,
				s.UnitsMax,
				s.UnitsAttempted,
				s.UnitsEarned,
				s.Grade,
				s.CreditFlag,
				s.TopCode,
				s.TransferStatus,
				s.BSStatus,
				s.SamCode,
				s.ClassCode,
				s.CollegeLevel,
				s.NCRCategory,
				s.CCLongTermId
			);
END;