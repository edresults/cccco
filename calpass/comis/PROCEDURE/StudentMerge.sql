IF (object_id('Comis.StudentMerge') is not null)
	BEGIN
		DROP PROCEDURE Comis.StudentMerge;
	END;

GO

CREATE PROCEDURE
	Comis.StudentMerge
	(
		@Student dbo.Student READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

BEGIN
	MERGE
		Comis.Student t
	USING
		(
			SELECT
				s.InterSegmentKey,
				IsCollision,
				IsMultiCollege,
				IsMultiDistrict,
				IsInterSegment
			FROM
				@Student s
				cross apply
				Comis.StudentGet(s.InterSegmentKey)
		) s
	ON
		s.InterSegmentKey = t.InterSegmentKey
	WHEN MATCHED THEN
		UPDATE SET
			t.IsCollision = s.IsCollision
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				InterSegmentKey,
				IsCollision,
				IsMultiCollege,
				IsMultiDistrict,
				IsInterSegment
			)
		VALUES
			(
				s.InterSegmentKey,
				s.IsCollision,
				s.IsMultiCollege,
				s.IsMultiDistrict,
				s.IsInterSegment
			);
END;