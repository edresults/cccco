USE calpass;

GO

IF (object_id('comis.p_xfer_bucket_process') is not null)
	BEGIN
		DROP PROCEDURE comis.p_xfer_bucket_process;
	END;

GO

CREATE PROCEDURE
	comis.p_xfer_bucket_process
AS

BEGIN
	UPDATE
		xfer_bucket_process
	SET
		ssn = calpass.dbo.Get1289Encryption(ssn, '');
END;