USE calpass;

GO

IF (object_id('comis.p_StudentSubjectCourseFirst_process') is not null)
	BEGIN
		DROP PROCEDURE comis.p_StudentSubjectCourseFirst_process;
	END;

GO

CREATE PROCEDURE
	comis.p_StudentSubjectCourseFirst_process
AS

	SET NOCOUNT ON;

DECLARE
	@i int = 0, -- iterator
	@b int = 100000, -- batch size
	@c int, -- count
	@message nvarchar(2048), -- message
	@Student comis.Student,
	@StudentBatch comis.Student,
	@StudentSubjectCourseFirst comis.StudentSubjectCourseFirst;

BEGIN
	-- collect all students
	INSERT INTO
		@Student
		(
			CollegeCode,
			StudentId,
			InterSegmentKey,
			BatchNumber
		)
	SELECT
		college_id,
		student_id,
		InterSegmentKey,
		BatchNumber = rank() over(order by InterSegmentKey)
	FROM
		comis.studntid
	WHERE
		InterSegmentKey is not null;

	-- set batch upper bound
	SELECT
		@c = max(BatchNumber)
	FROM
		@Student;

	WHILE ((@i * @b) < @c)
	BEGIN

		SET @message = 'Iteration: ' + convert(nvarchar, @i) + ' [' + convert(nvarchar, (@i * @b)) + 
			' to ' + convert(nvarchar, (@i + 1) * @b) + ' of ' + convert(nvarchar, @c) + ']';
		RAISERROR(@message, 0, 1) WITH NOWAIT;
		
		-- clear batch
		DELETE FROM @StudentBatch;

		-- collect batch
		INSERT INTO
			@StudentBatch
			(
				CollegeCode,
				StudentId,
				InterSegmentKey
			)
		SELECT
			CollegeCode,
			StudentId,
			InterSegmentKey
		FROM
			@Student
		WHERE
			BatchNumber >= (@i * @b)
			and BatchNumber < ((@i + 1) * @b);

		-- clear container
		DELETE FROM @StudentSubjectCourseFirst;

		-- populate container
		INSERT INTO
			@StudentSubjectCourseFirst
		EXECUTE comis.p_StudentSubjectCourseFirst_select
			@Student = @StudentBatch;

		-- merge container
		EXECUTE comis.p_StudentSubjectCourseFirst_merge
			@StudentSubjectCourseFirst = @StudentSubjectCourseFirst;

		-- iterate
		SET @i += 1;
	END;
END;