USE calpass;

GO

IF (object_id('comis.InterSegmentKeyEncrypt') is not null)
	BEGIN
		DROP PROCEDURE comis.InterSegmentKeyEncrypt;
	END;

GO

CREATE PROCEDURE
	comis.InterSegmentKeyEncrypt
AS

	SET NOCOUNT ON;


DECLARE
	@Message     nvarchar(2048),
	@Time        datetime,
	@Seconds     int,
	@Severity    tinyint = 0,
	@State       tinyint = 1,
	@RowCount    varchar(2048),
	@Error       varchar(2048),
	@Iterator    int = 0,
	@Iterations  int,
	@CollegeCode char(3),
	@College     comis.College;

BEGIN
	INSERT
		@College
		(
			CollegeCode
		)
	SELECT
		CollegeCode
	FROM
		comis.College
	ORDER BY
		CollegeCode;

	SELECT
		@Iterations = count(*)
	FROM
		@College;

	WHILE (@Iterator < @Iterations)
	BEGIN

		SET @Error = convert(varchar, @Iterator + 1) + ' of ' + convert(varchar, @Iterations);

		RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

		SET @Time = getdate();

		SELECT
			@CollegeCode = CollegeCode
		FROM
			@College
		WHERE
			Iterator = @Iterator;

		-- delete duplicates
		DELETE
			a
		FROM
			comis.studntid a
		WHERE
			a.college_id = @CollegeCode
			and exists (
				SELECT
					1
				FROM
					comis.studntid b
				WHERE
					b.college_id = a.college_id
					and b.ssn = a.ssn
				HAVING
					count(b.student_id) > 1
			);

		-- update
		UPDATE
			id
		SET
			id.InterSegmentKey = 
				HASHBYTES(
					'SHA2_512',
					Payload
				)
			-- id.InterSegmentKeyOrigin = 
			-- 	HASHBYTES(
			-- 		'SHA2_256',
			-- 		PayloadOrigin
			-- 	),
			-- id.InterSegmentKeyFull = 
			-- 	HASHBYTES(
			-- 		'SHA2_256',
			-- 		PayloadFull
			-- 	)
			-- id.InterSegmentKeyFullOrigin = 
			-- 	HASHBYTES(
			-- 		'SHA2_256',
			-- 		PayloadFullOrigin
			-- 	)
		FROM
			comis.studntid id
			cross apply
			comis.IskPayloadByPK(id.college_id, id.student_id)
		WHERE
			id.college_id = @CollegeCode;
		-- capture
		SET @RowCount = convert(varchar(2048), @@ROWCOUNT);
		-- set
		SET @Error = 'Encrypted ' + @RowCount + ' COMIS.STUDNTID records';
		-- output
		RAISERROR(@Error, @Severity, @State) WITH NOWAIT;
		-- get time elapsed
		SET @Seconds = datediff(second, @Time, getdate());
		-- set msg
		SET @Error = 'ITERATION: ' + 
			convert(nvarchar, @Seconds / 86400) + ':' +
			convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
		-- output to user
		RAISERROR(@Error, 0, 1) WITH NOWAIT;

		SET @Iterator += 1;
	END;
END;