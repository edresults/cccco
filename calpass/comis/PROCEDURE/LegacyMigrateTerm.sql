USE calpass;

GO

IF (object_id('comis.LegacyMigrateTerm') is not null)
    BEGIN
        DROP PROCEDURE comis.LegacyMigrateTerm
    END;

GO

CREATE PROCEDURE
    comis.LegacyMigrateTerm
    (
        @CollegeId char(3),
        @TermId char(3)
    )
AS

    SET NOCOUNT ON;
    SET XACT_ABORT ON;

DECLARE
    @True    bit = 1,
    @Target  sysname,
    @Message nvarchar(2048),
    @IpedsId char(6);

BEGIN

    SELECT
        @IpedsId = IpedsCodeLegacy
    FROM
        comis.College
    WHERE
        CollegeCode = @CollegeId;

	SET @Target = 'CCStudentProd';

	MERGE
		dbo.CCStudentProd
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				Derkey1 = id.InterSegmentKey,
				CollegeId = @IpedsId,
				StudentComisId = st.student_id,
				StudentId = id.ssn,
				IdStatus = id.student_id_status,
				TermId = st.term_id,
				CSISNum = null,
				Birthdate = null,
				FName = null,
				LName = null,
				Gender = null,
				Race = null,
				Citizen = st.citizenship,
				ZipCode = st.zip,
				EdStatus = st.education,
				HighSchool = st.high_school,
				StudentGoal = st.goal,
				EnrollStatus = st.enrollment,
				UnitsEarnedLoc = st.u_earned_loc,
				UnitsEarnedTrn = st.u_earned_trn,
				UnitsAttLoc = st.u_attmpt_loc,
				UnitsAttTrn = st.u_attmpt_trn,
				GPointsLoc = st.g_points_loc,
				GPointsTrn = st.g_points_trn,
				AcademicStanding = st.aca_standing,
				AcademicLevel = st.academic_level,
				DSPS = isnull(sd.primary_disability, 'Y'),
				EOPS = 
					isnull(
						case
							when se.eops_care_status = 'N' then 'E'
							when se.eops_care_status = 'C' then 'C'
							when se.eops_care_status = 'P' then 'Y'
						end,
						'Y'
					),
				BoggFlag = 
					isnull(
						(
							SELECT DISTINCT
								'B'
							FROM
								comis.sfawards sf
							WHERE
								sf.type_id in ('BA','B1','B2','B3','BB','BC','F1','F2','F3','F4','F5')
								and sf.college_id = st.college_id
								and sf.student_id = st.student_id
								and sf.term_recd = st.term_id
						),
						'Y'
					),
				-- cannot use left outer join when there should be a 1:1 match on primary key.
				-- CO does not validate data
				-- this event Should not exist
				-- annual term (100) with 105 term.
				PellFlag = 
					isnull(
						(
							SELECT DISTINCT
								'P'
							FROM
								comis.sfawards sf
							WHERE
								sf.type_id  = 'GP'
								and sf.college_id = st.college_id
								and sf.student_id = st.student_id
								and sf.term_recd = st.term_id
						),
						'Y'
					),
				FinancialAidFlag = 
					isnull(
						(
							SELECT
								min(
									case
										when sf.type_id in ('GS','GW','LD','LG','LS','WF') then 'A'
										when sf.type_id in ('GB','GC','GE','GF','LE','WC','WE','WU') then 'B'
										when sf.type_id in ('LH','LP','LR','LL') then 'C'
										when sf.type_id in ('SU','SV','SX') then 'D'
										when sf.type_id in ('GU', 'GV', 'LI', 'LN', 'GG') then 'E'
										else 'Y'
									end
								)
							FROM
								comis.sfawards sf
							WHERE
								sf.college_id = st.college_id
								and sf.student_id = st.student_id
								and sf.term_recd = st.term_id
						),
						'Y'
					),
				Major = isnull(sm.Major, 'XXXXXX'),
				ccLongTermId = t.YearTermCode,
				sg.military,
				sg.military_dependent,
				sg.foster_care,
				sg.incarcerated,
				sg.mesa,
				sg.puente,
				sg.mchs,
				sg.umoja,
				[1st_gen] = sg.gen_first,
				sg.caa
			FROM
				comis.stterm st
				inner join
				comis.sbstudnt sb
					on sb.college_id = st.college_id
					and sb.student_id = st.student_id
				inner join
				comis.studntid id
					on id.college_id = st.college_id
					and id.student_id = st.student_id
				inner join
				comis.Term t
					on t.TermCode = st.term_id
				left outer join
				comis.sddsps sd
					on sd.college_id = st.college_id
					and sd.student_id = st.student_id
					and sd.term_id = st.term_id
				left outer join
				comis.seeops se
					on se.college_id = st.college_id
					and se.student_id = st.student_id
					and se.term_id = st.term_id
				left outer join
				comis.smmatric sm
					on sm.college_id = st.college_id
					and sm.student_id = st.student_id
					and sm.term_id = st.term_id
				left outer join
				comis.sgpops sg
					on sg.college_id = st.college_id
					and sg.student_id = st.student_id
					and sg.term_id = st.term_id
				left outer join
				comis.svvatea sv
					on sv.college_id = st.college_id
					and sv.student_id = st.student_id
					and sv.term_id = st.term_id
			WHERE
				st.college_id = @CollegeId
				and st.term_id = @TermId
		) s
	ON
		t.CollegeId = s.CollegeId
		and t.StudentId = s.StudentId
		and t.TermId = s.TermId
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				Derkey1,
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				CSISNum,
				Birthdate,
				FName,
				LName,
				Gender,
				Race,
				Citizen,
				ZipCode,
				EdStatus,
				HighSchool,
				StudentGoal,
				EnrollStatus,
				UnitsEarnedLoc,
				UnitsEarnedTrn,
				UnitsAttLoc,
				UnitsAttTrn,
				GPointsLoc,
				GPointsTrn,
				AcademicStanding,
				AcademicLevel,
				DSPS,
				EOPS,
				BoggFlag,
				PellFlag,
				FinancialAidFlag,
				Major,
				ccLongTermId,
				military,
				military_dependent,
				foster_care,
				incarcerated,
				mesa,
				puente,
				mchs,
				umoja,
				[1st_gen],
				caa
			)
		VALUES
			(
				s.Derkey1,
				s.CollegeId,
				s.StudentComisId,
				s.StudentId,
				s.IdStatus,
				s.TermId,
				s.CSISNum,
				s.Birthdate,
				s.FName,
				s.LName,
				s.Gender,
				s.Race,
				s.Citizen,
				s.ZipCode,
				s.EdStatus,
				s.HighSchool,
				s.StudentGoal,
				s.EnrollStatus,
				s.UnitsEarnedLoc,
				s.UnitsEarnedTrn,
				s.UnitsAttLoc,
				s.UnitsAttTrn,
				s.GPointsLoc,
				s.GPointsTrn,
				s.AcademicStanding,
				s.AcademicLevel,
				s.DSPS,
				s.EOPS,
				s.BoggFlag,
				s.PellFlag,
				s.FinancialAidFlag,
				s.Major,
				s.ccLongTermId,
				s.military,
				s.military_dependent,
				s.foster_care,
				s.incarcerated,
				s.mesa,
				s.puente,
				s.mchs,
				s.umoja,
				s.[1st_gen],
				s.caa
			)
	WHEN MATCHED THEN
		UPDATE SET
			t.Derkey1            = t.Derkey1,
			t.StudentComisId     = t.StudentComisId,
			t.IdStatus           = t.IdStatus,
			t.CSISNum            = t.CSISNum,
			t.Birthdate          = t.Birthdate,
			t.FName              = t.FName,
			t.LName              = t.LName,
			t.Gender             = t.Gender,
			t.Race               = t.Race,
			t.Citizen            = t.Citizen,
			t.ZipCode            = t.ZipCode,
			t.EdStatus           = t.EdStatus,
			t.HighSchool         = t.HighSchool,
			t.StudentGoal        = t.StudentGoal,
			t.EnrollStatus       = t.EnrollStatus,
			t.UnitsEarnedLoc     = t.UnitsEarnedLoc,
			t.UnitsEarnedTrn     = t.UnitsEarnedTrn,
			t.UnitsAttLoc        = t.UnitsAttLoc,
			t.UnitsAttTrn        = t.UnitsAttTrn,
			t.GPointsLoc         = t.GPointsLoc,
			t.GPointsTrn         = t.GPointsTrn,
			t.AcademicStanding   = t.AcademicStanding,
			t.AcademicLevel      = t.AcademicLevel,
			t.DSPS               = t.DSPS,
			t.EOPS               = t.EOPS,
			t.BoggFlag           = t.BoggFlag,
			t.PellFlag           = t.PellFlag,
			t.FinancialAidFlag   = t.FinancialAidFlag,
			t.Major              = t.Major,
			t.ccLongTermId       = t.ccLongTermId,
			t.military           = t.military,
			t.military_dependent = t.military_dependent,
			t.foster_care        = t.foster_care,
			t.incarcerated       = t.incarcerated,
			t.mesa               = t.mesa,
			t.puente             = t.puente,
			t.mchs               = t.mchs,
			t.umoja              = t.umoja,
			t.[1st_gen]          = t.[1st_gen],
			t.caa                = t.caa;

	SET @Message = 'Target:  ' + @Target + char(13) + char(10) + 'Records: ' + convert(nvarchar(2048), @@ROWCOUNT)
	RAISERROR(@Message, 0, 1) WITH NOWAIT;

	SET @Target = 'CCStudentProd_EXT';

	-- CCStudentProd_EXT
	MERGE
		dbo.CCStudentProd_EXT
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				CollegeId                                 = @IpedsId,
				StudentComisId                            = st.student_id,
				StudentId                                 = id.ssn,
				IdStatus                                  = id.student_id_status,
				TermId                                    = st.term_id,
				extSB09_ResidenceCode                     = st.residency,
				extSB23_ApprenticeshipStatus              = st.apprentice,
				extSB24_TransferCenterStatus              = st.transfer_ctr,
				extSB26_JTPAStatus                        = st.jtpa_status,
				extSB27_CalWorksStatus                    = st.calworks_status,
				extSM01_MatricGoals                       = sm.goals,
				extSM01_MatricGoalsPos1                   = sm.goals_primary,
				extSM01_MatricGoalsPos2                   = sm.goals_secondary,
				extSM01_MatricGoalsPos3                   = sm.goals_tertiary,
				extSM01_MatricGoalsPos4                   = sm.goals_placeholder,
				extSM07_MatricOrientationServices         = sm.orientation_services,
				extSM08_MatricAssessmentServicesPlacement = sm.assessment_services_place,
				extSM09_MatricAssessmentServicesOther     = sm.assessment_services_other,
				extSM09_MatricAssessmentServicesOtherPos1 = sm.assessment_services_other_01,
				extSM09_MatricAssessmentServicesOtherPos2 = sm.assessment_services_other_02,
				extSM09_MatricAssessmentServicesOtherPos3 = sm.assessment_services_other_03,
				extSM12_MatricCounAdviseServices          = sm.advisement_services,
				extSM13_MatricAcademicFollowupSerives     = sm.follow_up_services,
				extSVO1_ProgramPlanStatus                 = sv.voc_pgm_plan,
				extSV03_EconomicallyDisadvStatus          = sv.econ_disadv,
				extSV03_EconomicallyDisadvStatusPos1      = sv.econ_disadv_status,
				extSV03_EconomicallyDisadvStatusPos2      = sv.econ_disadv_source,
				extSV04_SingleParentStatus                = sv.single_parent,
				extSV05_DisplacedHomemakerStatus          = sv.displ_homemker,
				extSV06_CoopWorkExperienceEdType          = sv.coop_work_exp,
				extSV08_TechPrepStatus                    = sv.tech_prep,
				extSV09_MigrantWorkerStatus               = sv.migrant_worker,
				extSCD1_FirstTermAttended                 = sb.term_first,
				extSCD2_LastTermAttended                  = sb.term_last,
				extSCD4_LEP                               = sb.lep_flag,
				extSCD5_AcademicallyDisadvantagedStudent  = sb.acad_disad_flag,
				extSTD1_AgeAtTerm                         = st.age_at_term,
				extSTD2_1stCensusCreditLoad               = st.census_crload,
				extSTD3_DayEveningClassCode               = st.day_evening_code,
				extSTD5_DegreeAppUnitsEarned              = st.deg_appl_units_earned,
				extSTD6_DayEveningClassCode2              = st.day_evening_code2,
				extSTD7_HeadCountStatus                   = st.headcount_status,
				extSTD8_LocalCumGPA                       = st.gpa_loc,
				extSTD9_TotalCumGPA                       = st.gpa_tot,
				extSD01_StudentPrimaryDisability          = sd.primary_disability,
				extSD03_StudentSecondaryDisability        = sd.secondary_disability,
				extSB29_MultiEthnicity                    = null
			FROM
				comis.stterm st
				inner join
				comis.sbstudnt sb
					on st.college_id = sb.college_id
					and st.student_id = sb.student_id
				inner join
				comis.studntid id
					on id.college_id = st.college_id
					and id.student_id = st.student_id
				inner join
				comis.Term t
					on t.TermCode = st.term_id
				left outer join
				comis.sddsps sd
					on sd.college_id = st.college_id
					and sd.student_id = st.student_id
					and sd.term_id = st.term_id
				left outer join
				comis.seeops se
					on se.college_id = st.college_id
					and se.student_id = st.student_id
					and se.term_id = st.term_id
				left outer join
				comis.smmatric sm
					on sm.college_id = st.college_id
					and sm.student_id = st.student_id
					and sm.term_id = st.term_id
				left outer join
				comis.sgpops sg
					on sg.college_id = st.college_id
					and sg.student_id = st.student_id
					and sg.term_id = st.term_id
				left outer join
				comis.svvatea sv
					on sv.college_id = st.college_id
					and sv.student_id = st.student_id
					and sv.term_id = st.term_id
			WHERE
				st.college_id = @CollegeId
				and st.term_id = @TermId
		) s
	ON
		s.CollegeId = t.CollegeId
		and s.StudentId = t.StudentId
		and s.TermId = t.TermId
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				extSB09_ResidenceCode,
				extSB23_ApprenticeshipStatus,
				extSB24_TransferCenterStatus,
				extSB26_JTPAStatus,
				extSB27_CalWorksStatus,
				extSM01_MatricGoals,
				extSM01_MatricGoalsPos1,
				extSM01_MatricGoalsPos2,
				extSM01_MatricGoalsPos3,
				extSM01_MatricGoalsPos4,
				extSM07_MatricOrientationServices,
				extSM08_MatricAssessmentServicesPlacement,
				extSM09_MatricAssessmentServicesOther,
				extSM09_MatricAssessmentServicesOtherPos1,
				extSM09_MatricAssessmentServicesOtherPos2,
				extSM09_MatricAssessmentServicesOtherPos3,
				extSM12_MatricCounAdviseServices,
				extSM13_MatricAcademicFollowupSerives,
				extSVO1_ProgramPlanStatus,
				extSV03_EconomicallyDisadvStatus,
				extSV03_EconomicallyDisadvStatusPos1,
				extSV03_EconomicallyDisadvStatusPos2,
				extSV04_SingleParentStatus,
				extSV05_DisplacedHomemakerStatus,
				extSV06_CoopWorkExperienceEdType,
				extSV08_TechPrepStatus,
				extSV09_MigrantWorkerStatus,
				extSCD1_FirstTermAttended,
				extSCD2_LastTermAttended,
				extSCD4_LEP,
				extSCD5_AcademicallyDisadvantagedStudent,
				extSTD1_AgeAtTerm,
				extSTD2_1stCensusCreditLoad,
				extSTD3_DayEveningClassCode,
				extSTD5_DegreeAppUnitsEarned,
				extSTD6_DayEveningClassCode2,
				extSTD7_HeadCountStatus,
				extSTD8_LocalCumGPA,
				extSTD9_TotalCumGPA,
				extSD01_StudentPrimaryDisability,
				extSD03_StudentSecondaryDisability,
				extSB29_MultiEthnicity
			)
		VALUES
			(
				s.CollegeId,
				s.StudentComisId,
				s.StudentId,
				s.IdStatus,
				s.TermId,
				s.extSB09_ResidenceCode,
				s.extSB23_ApprenticeshipStatus,
				s.extSB24_TransferCenterStatus,
				s.extSB26_JTPAStatus,
				s.extSB27_CalWorksStatus,
				s.extSM01_MatricGoals,
				s.extSM01_MatricGoalsPos1,
				s.extSM01_MatricGoalsPos2,
				s.extSM01_MatricGoalsPos3,
				s.extSM01_MatricGoalsPos4,
				s.extSM07_MatricOrientationServices,
				s.extSM08_MatricAssessmentServicesPlacement,
				s.extSM09_MatricAssessmentServicesOther,
				s.extSM09_MatricAssessmentServicesOtherPos1,
				s.extSM09_MatricAssessmentServicesOtherPos2,
				s.extSM09_MatricAssessmentServicesOtherPos3,
				s.extSM12_MatricCounAdviseServices,
				s.extSM13_MatricAcademicFollowupSerives,
				s.extSVO1_ProgramPlanStatus,
				s.extSV03_EconomicallyDisadvStatus,
				s.extSV03_EconomicallyDisadvStatusPos1,
				s.extSV03_EconomicallyDisadvStatusPos2,
				s.extSV04_SingleParentStatus,
				s.extSV05_DisplacedHomemakerStatus,
				s.extSV06_CoopWorkExperienceEdType,
				s.extSV08_TechPrepStatus,
				s.extSV09_MigrantWorkerStatus,
				s.extSCD1_FirstTermAttended,
				s.extSCD2_LastTermAttended,
				s.extSCD4_LEP,
				s.extSCD5_AcademicallyDisadvantagedStudent,
				s.extSTD1_AgeAtTerm,
				s.extSTD2_1stCensusCreditLoad,
				s.extSTD3_DayEveningClassCode,
				s.extSTD5_DegreeAppUnitsEarned,
				s.extSTD6_DayEveningClassCode2,
				s.extSTD7_HeadCountStatus,
				s.extSTD8_LocalCumGPA,
				s.extSTD9_TotalCumGPA,
				s.extSD01_StudentPrimaryDisability,
				s.extSD03_StudentSecondaryDisability,
				s.extSB29_MultiEthnicity
			)
	WHEN MATCHED THEN
		UPDATE SET
			t.StudentComisId                            = s.StudentComisId,
			t.IdStatus                                  = s.IdStatus,
			t.extSB09_ResidenceCode                     = s.extSB09_ResidenceCode,
			t.extSB23_ApprenticeshipStatus              = s.extSB23_ApprenticeshipStatus,
			t.extSB24_TransferCenterStatus              = s.extSB24_TransferCenterStatus,
			t.extSB26_JTPAStatus                        = s.extSB26_JTPAStatus,
			t.extSB27_CalWorksStatus                    = s.extSB27_CalWorksStatus,
			t.extSM01_MatricGoals                       = s.extSM01_MatricGoals,
			t.extSM01_MatricGoalsPos1                   = s.extSM01_MatricGoalsPos1,
			t.extSM01_MatricGoalsPos2                   = s.extSM01_MatricGoalsPos2,
			t.extSM01_MatricGoalsPos3                   = s.extSM01_MatricGoalsPos3,
			t.extSM01_MatricGoalsPos4                   = s.extSM01_MatricGoalsPos4,
			t.extSM07_MatricOrientationServices         = s.extSM07_MatricOrientationServices,
			t.extSM08_MatricAssessmentServicesPlacement = s.extSM08_MatricAssessmentServicesPlacement,
			t.extSM09_MatricAssessmentServicesOther     = s.extSM09_MatricAssessmentServicesOther,
			t.extSM09_MatricAssessmentServicesOtherPos1 = s.extSM09_MatricAssessmentServicesOtherPos1,
			t.extSM09_MatricAssessmentServicesOtherPos2 = s.extSM09_MatricAssessmentServicesOtherPos2,
			t.extSM09_MatricAssessmentServicesOtherPos3 = s.extSM09_MatricAssessmentServicesOtherPos3,
			t.extSM12_MatricCounAdviseServices          = s.extSM12_MatricCounAdviseServices,
			t.extSM13_MatricAcademicFollowupSerives     = s.extSM13_MatricAcademicFollowupSerives,
			t.extSVO1_ProgramPlanStatus                 = s.extSVO1_ProgramPlanStatus,
			t.extSV03_EconomicallyDisadvStatus          = s.extSV03_EconomicallyDisadvStatus,
			t.extSV03_EconomicallyDisadvStatusPos1      = s.extSV03_EconomicallyDisadvStatusPos1,
			t.extSV03_EconomicallyDisadvStatusPos2      = s.extSV03_EconomicallyDisadvStatusPos2,
			t.extSV04_SingleParentStatus                = s.extSV04_SingleParentStatus,
			t.extSV05_DisplacedHomemakerStatus          = s.extSV05_DisplacedHomemakerStatus,
			t.extSV06_CoopWorkExperienceEdType          = s.extSV06_CoopWorkExperienceEdType,
			t.extSV08_TechPrepStatus                    = s.extSV08_TechPrepStatus,
			t.extSV09_MigrantWorkerStatus               = s.extSV09_MigrantWorkerStatus,
			t.extSCD1_FirstTermAttended                 = s.extSCD1_FirstTermAttended,
			t.extSCD2_LastTermAttended                  = s.extSCD2_LastTermAttended,
			t.extSCD4_LEP                               = s.extSCD4_LEP,
			t.extSCD5_AcademicallyDisadvantagedStudent  = s.extSCD5_AcademicallyDisadvantagedStudent,
			t.extSTD1_AgeAtTerm                         = s.extSTD1_AgeAtTerm,
			t.extSTD2_1stCensusCreditLoad               = s.extSTD2_1stCensusCreditLoad,
			t.extSTD3_DayEveningClassCode               = s.extSTD3_DayEveningClassCode,
			t.extSTD5_DegreeAppUnitsEarned              = s.extSTD5_DegreeAppUnitsEarned,
			t.extSTD6_DayEveningClassCode2              = s.extSTD6_DayEveningClassCode2,
			t.extSTD7_HeadCountStatus                   = s.extSTD7_HeadCountStatus,
			t.extSTD8_LocalCumGPA                       = s.extSTD8_LocalCumGPA,
			t.extSTD9_TotalCumGPA                       = s.extSTD9_TotalCumGPA,
			t.extSD01_StudentPrimaryDisability          = s.extSD01_StudentPrimaryDisability,
			t.extSD03_StudentSecondaryDisability        = s.extSD03_StudentSecondaryDisability,
			t.extSB29_MultiEthnicity                    = s.extSB29_MultiEthnicity;

	SET @Message = 'Target:  ' + @Target + char(13) + char(10) + 'Records: ' + convert(nvarchar(2048), @@ROWCOUNT)
	RAISERROR(@Message, 0, 1) WITH NOWAIT;

	SET @Target = 'CCCourseProd';

	-- CCCourseProd
	MERGE
		dbo.CCCourseProd
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				CollegeId = @IpedsId,
				StudentComisId = sx.student_id,
				StudentId = id.ssn,
				IdStatus = id.student_id_status,
				TermId = sx.term_id,
				CourseId = sx.course_id,
				SectionId = sx.section_id,
				Title = cb.title,
				UnitsMax = cb.units_maximum,
				UnitsAttempted = sx.units_attempted,
				UnitsEarned = sx.units,
				Grade = sx.grade,
				CreditFlag = sx.credit_flag,
				TopCode = cb.top_code,
				TransferStatus = cb.transfer_status,
				BSStatus = cb.basic_skills_status,
				SamCode = cb.sam_code,
				ClassCode = cb.classification_code,
				CollegeLevel = cb.prior_to_college,
				NCRCategory = cb.noncredit_category,
				CCLongTermId = t.YearTermCode
			FROM
				comis.studntid id
				inner join
				comis.sxenrlm sx
					on id.college_id = sx.college_id
					and id.student_id = sx.student_id
				inner join
				comis.cbcrsinv cb
					on sx.college_id = cb.college_id
					and sx.term_id = cb.term_id
					and sx.course_id = cb.course_id
					and sx.control_number = cb.control_number
				inner join
				comis.Term t
					on t.TermCode = sx.term_id
			WHERE
				sx.college_id = @CollegeId
				and sx.term_id = @TermId
		) s
	ON
		t.CollegeId = s.CollegeId
		and t.StudentId = s.StudentId
		and t.TermId = s.TermId
		and t.CourseId = s.CourseId
		and t.SectionId = s.SectionId
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				CourseId,
				SectionId,
				Title,
				UnitsMax,
				UnitsAttempted,
				UnitsEarned,
				Grade,
				CreditFlag,
				TopCode,
				TransferStatus,
				BSStatus,
				SamCode,
				ClassCode,
				CollegeLevel,
				NCRCategory,
				CCLongTermId
			)
		VALUES
			(
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				CourseId,
				SectionId,
				Title,
				UnitsMax,
				UnitsAttempted,
				UnitsEarned,
				Grade,
				CreditFlag,
				TopCode,
				TransferStatus,
				BSStatus,
				SamCode,
				ClassCode,
				CollegeLevel,
				NCRCategory,
				CCLongTermId
			)
	WHEN MATCHED THEN
		UPDATE SET
			t.StudentComisId = s.StudentComisId,
			t.IdStatus       = s.IdStatus,
			t.Title          = s.Title,
			t.UnitsMax       = s.UnitsMax,
			t.UnitsAttempted = s.UnitsAttempted,
			t.UnitsEarned    = s.UnitsEarned,
			t.Grade          = s.Grade,
			t.CreditFlag     = s.CreditFlag,
			t.TopCode        = s.TopCode,
			t.TransferStatus = s.TransferStatus,
			t.BSStatus       = s.BSStatus,
			t.SamCode        = s.SamCode,
			t.ClassCode      = s.ClassCode,
			t.CollegeLevel   = s.CollegeLevel,
			t.NCRCategory    = s.NCRCategory,
			t.CCLongTermId   = s.CCLongTermId;

	SET @Message = 'Target:  ' + @Target + char(13) + char(10) + 'Records: ' + convert(nvarchar(2048), @@ROWCOUNT)
	RAISERROR(@Message, 0, 1) WITH NOWAIT;

	SET @Target = 'CCCourseProd_EXT';

	MERGE
		dbo.CCCourseProd_EXT
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				CollegeId = @IpedsId,
				StudentComisId = id.student_id,
				StudentId = id.ssn,
				IdStatus = id.student_id_status,
				TermId = xf.term_id,
				CourseId = xf.course_id,
				SectionId = xf.section_id,
				extSX05_PositiveAttendanceHours = sx.attend_hours,
				extSXD4_TotalHours = sx.total_hours,
				extXF01_SessionInstuctionMethod1 = xf.SessionInstuctionMethod1,
				extXF01_SessionInstuctionMethod2 = xf.SessionInstuctionMethod2,
				extXF01_SessionInstuctionMethod3 = xf.SessionInstuctionMethod3,
				extXF01_SessionInstuctionMethod4 = xf.SessionInstuctionMethod4,
				extXBD3_DayEveningClassCode = xb.day_evening_code,
				extCB00_ControlNumber = cb.control_number,
				extCB04_CreditStatus = cb.credit_status,
				extCB10_CoopWorkExpEdStatus = cb.coop_ed_status,
				extCB13_SpecialClassStatus = cb.special_class_status,
				extCB14_CanCode = cb.can_code,
				extCB15_CanSeqCode = cb.can_seq_code,
				extCB19_CrosswalkCrsDeptName = cb.crosswalk_crs_name,
				extCB20_CrosswalkCrsNumber = cb.crosswalk_crs_number,
				extCB21_PriorToCollegeLevel = cb.prior_to_college,
				extCB23_FundingAgencyCategory = cb.funding_category,
				extCB24_ProgramStatus = cb.program_status,
				extXB01_AccountingMethod = xb.accounting
			FROM
				(
					SELECT
						xf.college_id,
						xf.term_id,
						xf.course_id,
						xf.section_id,
						SessionInstuctionMethod1 = max(case when xf.InstructionMethodSelector = 1 then xf.instruction end),
						SessionInstuctionMethod2 = max(case when xf.InstructionMethodSelector = 2 then xf.instruction end),
						SessionInstuctionMethod3 = max(case when xf.InstructionMethodSelector = 3 then xf.instruction end),
						SessionInstuctionMethod4 = max(case when xf.InstructionMethodSelector = 4 then xf.instruction end)
					FROM
						(
							SELECT
								xf.college_id,
								xf.term_id,
								xf.course_id,
								xf.section_id,
								xf.instruction,
								InstructionMethodSelector = row_number() OVER(
										PARTITION BY
											xf.college_id,
											xf.term_id,
											xf.course_id,
											xf.section_id
										ORDER BY
											xf.session_id
									)
							FROM
								comis.xfsesion xf
							WHERE
								xf.college_id = @CollegeId
								and xf.term_id = @TermId
						) xf
					GROUP BY
						xf.college_id,
						xf.term_id,
						xf.course_id,
						xf.section_id
				) xf
				inner join
				comis.xbsecton xb
					on xf.college_id = xb.college_id
					and xf.term_id = xb.term_id
					and xf.course_id = xb.course_id
					and xf.section_id= xb.section_id
				inner join
				comis.sxenrlm sx
					on xb.college_id = sx.college_id
					and xb.term_id = sx.term_id
					and xb.course_id = sx.course_id
					and xb.section_id = sx.section_id
				inner join
				comis.cbcrsinv cb
					on cb.college_id = sx.college_id
					and cb.term_id = sx.term_id
					and cb.course_id = sx.course_id
					and cb.control_number = sx.control_number
				inner join
				comis.studntid id
					on sx.college_id = id.college_id
					and sx.student_id = id.student_id
		) s
	ON
		t.CollegeId = s.CollegeId
		and t.StudentId = s.StudentId
		and t.TermId = s.TermId
		and t.CourseId = s.CourseId
		and t.SectionId = s.SectionId
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				CourseId,
				SectionId,
				extSX05_PositiveAttendanceHours,
				extSXD4_TotalHours,
				extXF01_SessionInstuctionMethod1,
				extXF01_SessionInstuctionMethod2,
				extXF01_SessionInstuctionMethod3,
				extXF01_SessionInstuctionMethod4,
				extXBD3_DayEveningClassCode,
				extCB00_ControlNumber,
				extCB04_CreditStatus,
				extCB10_CoopWorkExpEdStatus,
				extCB13_SpecialClassStatus,
				extCB14_CanCode,
				extCB15_CanSeqCode,
				extCB19_CrosswalkCrsDeptName,
				extCB20_CrosswalkCrsNumber,
				extCB21_PriorToCollegeLevel,
				extCB23_FundingAgencyCategory,
				extCB24_ProgramStatus,
				extXB01_AccountingMethod
			)
		VALUES
			(
				s.CollegeId,
				s.StudentComisId,
				s.StudentId,
				s.IdStatus,
				s.TermId,
				s.CourseId,
				s.SectionId,
				s.extSX05_PositiveAttendanceHours,
				s.extSXD4_TotalHours,
				s.extXF01_SessionInstuctionMethod1,
				s.extXF01_SessionInstuctionMethod2,
				s.extXF01_SessionInstuctionMethod3,
				s.extXF01_SessionInstuctionMethod4,
				s.extXBD3_DayEveningClassCode,
				s.extCB00_ControlNumber,
				s.extCB04_CreditStatus,
				s.extCB10_CoopWorkExpEdStatus,
				s.extCB13_SpecialClassStatus,
				s.extCB14_CanCode,
				s.extCB15_CanSeqCode,
				s.extCB19_CrosswalkCrsDeptName,
				s.extCB20_CrosswalkCrsNumber,
				s.extCB21_PriorToCollegeLevel,
				s.extCB23_FundingAgencyCategory,
				s.extCB24_ProgramStatus,
				s.extXB01_AccountingMethod
			)
	WHEN MATCHED THEN
		UPDATE SET
			t.StudentComisId                   = s.StudentComisId,
			t.IdStatus                         = s.IdStatus,
			t.extSX05_PositiveAttendanceHours  = s.extSX05_PositiveAttendanceHours,
			t.extSXD4_TotalHours               = s.extSXD4_TotalHours,
			t.extXF01_SessionInstuctionMethod1 = s.extXF01_SessionInstuctionMethod1,
			t.extXF01_SessionInstuctionMethod2 = s.extXF01_SessionInstuctionMethod2,
			t.extXF01_SessionInstuctionMethod3 = s.extXF01_SessionInstuctionMethod3,
			t.extXF01_SessionInstuctionMethod4 = s.extXF01_SessionInstuctionMethod4,
			t.extXBD3_DayEveningClassCode      = s.extXBD3_DayEveningClassCode,
			t.extCB00_ControlNumber            = s.extCB00_ControlNumber,
			t.extCB04_CreditStatus             = s.extCB04_CreditStatus,
			t.extCB10_CoopWorkExpEdStatus      = s.extCB10_CoopWorkExpEdStatus,
			t.extCB13_SpecialClassStatus       = s.extCB13_SpecialClassStatus,
			t.extCB14_CanCode                  = s.extCB14_CanCode,
			t.extCB15_CanSeqCode               = s.extCB15_CanSeqCode,
			t.extCB19_CrosswalkCrsDeptName     = s.extCB19_CrosswalkCrsDeptName,
			t.extCB20_CrosswalkCrsNumber       = s.extCB20_CrosswalkCrsNumber,
			t.extCB21_PriorToCollegeLevel      = s.extCB21_PriorToCollegeLevel,
			t.extCB23_FundingAgencyCategory    = s.extCB23_FundingAgencyCategory,
			t.extCB24_ProgramStatus            = s.extCB24_ProgramStatus,
			t.extXB01_AccountingMethod         = s.extXB01_AccountingMethod;

	SET @Message = 'Target:  ' + @Target + char(13) + char(10) + 'Records: ' + convert(nvarchar(2048), @@ROWCOUNT)
	RAISERROR(@Message, 0, 1) WITH NOWAIT;
END;