USE calpass;

GO

IF (object_id('comis.CCCourseProd_EXTMergeByCollege') is not null)
	BEGIN
		DROP PROCEDURE comis.CCCourseProd_EXTMergeByCollege;
	END;

GO

CREATE PROCEDURE
	comis.CCCourseProd_EXTMergeByCollege
	(
		@CollegeCode char(3)
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@IpedsCode char(6);

BEGIN

	SELECT
		@IpedsCode = IpedsCodeLegacy
	FROM
		comis.College
	WHERE
		CollegeCode = @CollegeCode;

	MERGE
		dbo.CCCourseProd_EXT
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				CollegeId = @IpedsCode,
				StudentComisId = id.student_id,
				StudentId = id.ssn,
				IdStatus = id.student_id_status,
				TermId = xf.term_id,
				CourseId = xf.course_id,
				SectionId = xf.section_id,
				extSX05_PositiveAttendanceHours = sx.attend_hours,
				extSXD4_TotalHours = sx.total_hours,
				extXF01_SessionInstuctionMethod1 = xf.SessionInstuctionMethod1,
				extXF01_SessionInstuctionMethod2 = xf.SessionInstuctionMethod2,
				extXF01_SessionInstuctionMethod3 = xf.SessionInstuctionMethod3,
				extXF01_SessionInstuctionMethod4 = xf.SessionInstuctionMethod4,
				extXBD3_DayEveningClassCode = xb.day_evening_code,
				extCB00_ControlNumber = cb.control_number,
				extCB04_CreditStatus = cb.credit_status,
				extCB10_CoopWorkExpEdStatus = cb.coop_ed_status,
				extCB13_SpecialClassStatus = cb.special_class_status,
				extCB14_CanCode = cb.can_code,
				extCB15_CanSeqCode = cb.can_seq_code,
				extCB19_CrosswalkCrsDeptName = cb.crosswalk_crs_name,
				extCB20_CrosswalkCrsNumber = cb.crosswalk_crs_number,
				extCB21_PriorToCollegeLevel = cb.prior_to_college,
				extCB23_FundingAgencyCategory = cb.funding_category,
				extCB24_ProgramStatus = cb.program_status,
				extXB01_AccountingMethod = xb.accounting
			FROM
				(
					SELECT
						xf.college_id,
						xf.term_id,
						xf.course_id,
						xf.section_id,
						SessionInstuctionMethod1 = max(case when xf.InstructionMethodSelector = 1 then xf.instruction end),
						SessionInstuctionMethod2 = max(case when xf.InstructionMethodSelector = 2 then xf.instruction end),
						SessionInstuctionMethod3 = max(case when xf.InstructionMethodSelector = 3 then xf.instruction end),
						SessionInstuctionMethod4 = max(case when xf.InstructionMethodSelector = 4 then xf.instruction end)
					FROM
						(
							SELECT
								xf.college_id,
								xf.term_id,
								xf.course_id,
								xf.section_id,
								xf.instruction,
								InstructionMethodSelector = row_number() OVER(
										PARTITION BY
											xf.college_id,
											xf.term_id,
											xf.course_id,
											xf.section_id
										ORDER BY
											xf.session_id
									)
							FROM
								comis.xfsesion xf
							WHERE
								xf.college_id = @CollegeCode
						) xf
					GROUP BY
						xf.college_id,
						xf.term_id,
						xf.course_id,
						xf.section_id
				) xf
				inner join
				calpass.comis.xbsecton xb
					on xf.college_id = xb.college_id
					and xf.term_id = xb.term_id
					and xf.course_id = xb.course_id
					and xf.section_id= xb.section_id
				inner join
				calpass.comis.sxenrlm sx
					on xb.college_id = sx.college_id
					and xb.term_id = sx.term_id
					and xb.course_id = sx.course_id
					and xb.section_id = sx.section_id
				inner join
				calpass.comis.cbcrsinv cb
					on cb.college_id = sx.college_id
					and cb.term_id = sx.term_id
					and cb.course_id = sx.course_id
					and cb.control_number = sx.control_number
				inner join
				calpass.comis.studntid id
					on sx.college_id = id.college_id
					and sx.student_id = id.student_id
		) s
	ON
		(
			s.CollegeId = t.CollegeId
			and s.StudentId = t.StudentId
			and s.TermId = t.TermId
			and s.CourseId = t.CourseId
			and s.SectionId = t.SectionId
		)
	WHEN MATCHED THEN
		UPDATE SET
			t.StudentComisId                   = s.StudentComisId,
			t.IdStatus                         = s.IdStatus,
			t.extSX05_PositiveAttendanceHours  = s.extSX05_PositiveAttendanceHours,
			t.extSXD4_TotalHours               = s.extSXD4_TotalHours,
			t.extXF01_SessionInstuctionMethod1 = s.extXF01_SessionInstuctionMethod1,
			t.extXF01_SessionInstuctionMethod2 = s.extXF01_SessionInstuctionMethod2,
			t.extXF01_SessionInstuctionMethod3 = s.extXF01_SessionInstuctionMethod3,
			t.extXF01_SessionInstuctionMethod4 = s.extXF01_SessionInstuctionMethod4,
			t.extXBD3_DayEveningClassCode      = s.extXBD3_DayEveningClassCode,
			t.extCB00_ControlNumber            = s.extCB00_ControlNumber,
			t.extCB04_CreditStatus             = s.extCB04_CreditStatus,
			t.extCB10_CoopWorkExpEdStatus      = s.extCB10_CoopWorkExpEdStatus,
			t.extCB13_SpecialClassStatus       = s.extCB13_SpecialClassStatus,
			t.extCB14_CanCode                  = s.extCB14_CanCode,
			t.extCB15_CanSeqCode               = s.extCB15_CanSeqCode,
			t.extCB19_CrosswalkCrsDeptName     = s.extCB19_CrosswalkCrsDeptName,
			t.extCB20_CrosswalkCrsNumber       = s.extCB20_CrosswalkCrsNumber,
			t.extCB21_PriorToCollegeLevel      = s.extCB21_PriorToCollegeLevel,
			t.extCB23_FundingAgencyCategory    = s.extCB23_FundingAgencyCategory,
			t.extCB24_ProgramStatus            = s.extCB24_ProgramStatus,
			t.extXB01_AccountingMethod         = s.extXB01_AccountingMethod
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				CourseId,
				SectionId,
				extSX05_PositiveAttendanceHours,
				extSXD4_TotalHours,
				extXF01_SessionInstuctionMethod1,
				extXF01_SessionInstuctionMethod2,
				extXF01_SessionInstuctionMethod3,
				extXF01_SessionInstuctionMethod4,
				extXBD3_DayEveningClassCode,
				extCB00_ControlNumber,
				extCB04_CreditStatus,
				extCB10_CoopWorkExpEdStatus,
				extCB13_SpecialClassStatus,
				extCB14_CanCode,
				extCB15_CanSeqCode,
				extCB19_CrosswalkCrsDeptName,
				extCB20_CrosswalkCrsNumber,
				extCB21_PriorToCollegeLevel,
				extCB23_FundingAgencyCategory,
				extCB24_ProgramStatus,
				extXB01_AccountingMethod
			)
		VALUES
			(
				s.CollegeId,
				s.StudentComisId,
				s.StudentId,
				s.IdStatus,
				s.TermId,
				s.CourseId,
				s.SectionId,
				s.extSX05_PositiveAttendanceHours,
				s.extSXD4_TotalHours,
				s.extXF01_SessionInstuctionMethod1,
				s.extXF01_SessionInstuctionMethod2,
				s.extXF01_SessionInstuctionMethod3,
				s.extXF01_SessionInstuctionMethod4,
				s.extXBD3_DayEveningClassCode,
				s.extCB00_ControlNumber,
				s.extCB04_CreditStatus,
				s.extCB10_CoopWorkExpEdStatus,
				s.extCB13_SpecialClassStatus,
				s.extCB14_CanCode,
				s.extCB15_CanSeqCode,
				s.extCB19_CrosswalkCrsDeptName,
				s.extCB20_CrosswalkCrsNumber,
				s.extCB21_PriorToCollegeLevel,
				s.extCB23_FundingAgencyCategory,
				s.extCB24_ProgramStatus,
				s.extXB01_AccountingMethod
			);
END;