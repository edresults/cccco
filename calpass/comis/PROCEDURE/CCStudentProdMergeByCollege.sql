USE calpass;

GO

IF (object_id('comis.CCStudentProdMergeByCollege') is not null)
	BEGIN
		DROP PROCEDURE comis.CCStudentProdMergeByCollege;
	END;

GO

CREATE PROCEDURE
	comis.CCStudentProdMergeByCollege
	(
		@CollegeCode char(3)
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@IpedsCode char(6);

BEGIN

	SELECT
		@IpedsCode = IpedsCodeLegacy
	FROM
		comis.College
	WHERE
		CollegeCode = @CollegeCode;

	MERGE
		dbo.CCStudentProd
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				Derkey1 = id.InterSegmentKey,
				CollegeId = @IpedsCode,
				StudentComisId = st.student_id,
				StudentId = id.ssn,
				IdStatus = id.student_id_status,
				TermId = st.term_id,
				CSISNum = null,
				Birthdate = null,
				FName = null,
				LName = null,
				Gender = st.gender,
				Race = 
					case
						when st.ipeds_race not in ('H', 'T') and substring(st.multi_race, 13, 1) = 'Y' then 'F'
						else st.ipeds_race
					end,
				RaceIpeds = st.ipeds_race,
				RaceGroup =
					case
						-- hispanic
						when substring(st.multi_race, 1,  1) = 'Y' then 'HX'
						when substring(st.multi_race, 2,  1) = 'Y' then 'HM'
						when substring(st.multi_race, 3,  1) = 'Y' then 'HC'
						when substring(st.multi_race, 4,  1) = 'Y' then 'HS'
						when substring(st.multi_race, 5,  1) = 'Y' then 'HO'
						-- asian
						when substring(st.multi_race, 13, 1) = 'Y' then 'FX'
						when substring(st.multi_race, 6,  1) = 'Y' then 'AI'
						when substring(st.multi_race, 7,  1) = 'Y' then 'AC'
						when substring(st.multi_race, 8,  1) = 'Y' then 'AJ'
						when substring(st.multi_race, 9,  1) = 'Y' then 'AK'
						when substring(st.multi_race, 10, 1) = 'Y' then 'AL'
						when substring(st.multi_race, 11, 1) = 'Y' then 'AC'
						when substring(st.multi_race, 12, 1) = 'Y' then 'AV'
						when substring(st.multi_race, 14, 1) = 'Y' then 'AO'
						-- black
						when substring(st.multi_race, 15, 1) = 'Y' then 'BX'
						-- native american
						when substring(st.multi_race, 16, 1) = 'Y' then 'NX'
						-- pacific islander
						when substring(st.multi_race, 17, 1) = 'Y' then 'PG'
						when substring(st.multi_race, 18, 1) = 'Y' then 'PH'
						when substring(st.multi_race, 19, 1) = 'Y' then 'PS'
						when substring(st.multi_race, 20, 1) = 'Y' then 'PO'
						-- white
						when substring(st.multi_race, 21, 1) = 'Y' then 'W'
					end,
				Citizen = st.citizenship,
				ZipCode = st.zip,
				EdStatus = st.education,
				HighSchool = st.high_school,
				StudentGoal = st.goal,
				EnrollStatus = st.enrollment,
				UnitsEarnedLoc = st.u_earned_loc,
				UnitsEarnedTrn = st.u_earned_trn,
				UnitsAttLoc = st.u_attmpt_loc,
				UnitsAttTrn = st.u_attmpt_trn,
				GPointsLoc = st.g_points_loc,
				GPointsTrn = st.g_points_trn,
				AcademicStanding = st.aca_standing,
				AcademicLevel = st.academic_level,
				DSPS = isnull(sd.primary_disability, 'Y'),
				EOPS = 
					isnull(
						case
							when se.eops_care_status = 'N' then 'E'
							when se.eops_care_status = 'C' then 'C'
							when se.eops_care_status = 'P' then 'Y'
						end,
						'Y'
					),
				BoggFlag = 
					isnull(
						(
							SELECT DISTINCT
								'B'
							FROM
								comis.sfawards sf
							WHERE
								sf.type_id in ('BA','B1','B2','B3','BB','BC','F1','F2','F3','F4','F5')
								and sf.college_id = st.college_id
								and sf.student_id = st.student_id
								and sf.term_recd = st.term_id
						),
						'Y'
					),
				-- cannot use left outer join when there should be a 1:1 match on primary key.
				-- CO does not validate data
				-- this event should not exist
				-- annual term (100) with 105 term.
				PellFlag = 
					isnull(
						(
							SELECT DISTINCT
								'P'
							FROM
								comis.sfawards sf
							WHERE
								sf.type_id  = 'GP'
								and sf.college_id = st.college_id
								and sf.student_id = st.student_id
								and sf.term_recd = st.term_id
						),
						'Y'
					),
				FinancialAidFlag = 
					isnull(
						(
							SELECT
								min(
									case
										when sf.type_id in ('GS','GW','LD','LG','LS','WF') then 'A'
										when sf.type_id in ('GB','GC','GE','GF','LE','WC','WE','WU') then 'B'
										when sf.type_id in ('LH','LP','LR','LL') then 'C'
										when sf.type_id in ('SU','SV','SX') then 'D'
										when sf.type_id in ('GU', 'GV', 'LI', 'LN', 'GG') then 'E'
										else 'Y'
									end
								)
							FROM
								comis.sfawards sf
							WHERE
								sf.college_id = st.college_id
								and sf.student_id = st.student_id
								and sf.term_recd = st.term_id
						),
						'Y'
					),
				Major = isnull(sm.Major, 'XXXXXX'),
				ccLongTermId = t.YearTermCode,
				sg.military,
				sg.military_dependent,
				sg.foster_care,
				sg.incarcerated,
				sg.mesa,
				sg.puente,
				sg.mchs,
				sg.umoja,
				[1st_gen] = sg.gen_first,
				sg.caa
			FROM
				comis.stterm st
				inner join
				comis.sbstudnt sb
					on sb.college_id = st.college_id
					and sb.student_id = st.student_id
				inner join
				comis.studntid id
					on id.college_id = st.college_id
					and id.student_id = st.student_id
				inner join
				comis.Term t
					on t.TermCode = st.term_id
				left outer join
				comis.sddsps sd
					on sd.college_id = st.college_id
					and sd.student_id = st.student_id
					and sd.term_id = st.term_id
				left outer join
				comis.seeops se
					on se.college_id = st.college_id
					and se.student_id = st.student_id
					and se.term_id = st.term_id
				left outer join
				comis.smmatric sm
					on sm.college_id = st.college_id
					and sm.student_id = st.student_id
					and sm.term_id = st.term_id
				left outer join
				comis.sgpops sg
					on sg.college_id = st.college_id
					and sg.student_id = st.student_id
					and sg.term_id = st.term_id
				left outer join
				comis.svvatea sv
					on sv.college_id = st.college_id
					and sv.student_id = st.student_id
					and sv.term_id = st.term_id
			WHERE
				st.college_id = @CollegeCode
		) s
	ON
		(
			s.CollegeId = t.CollegeId
			and s.StudentId = t.StudentId
			and s.TermId = t.TermId
		)
	WHEN MATCHED THEN
		UPDATE SET
			t.Derkey1            = s.Derkey1,
			t.StudentComisId     = s.StudentComisId,
			t.IdStatus           = s.IdStatus,
			t.CSISNum            = s.CSISNum,
			t.Birthdate          = s.Birthdate,
			t.FName              = s.FName,
			t.LName              = s.LName,
			t.Gender             = s.Gender,
			t.Race               = s.Race,
			t.Citizen            = s.Citizen,
			t.ZipCode            = s.ZipCode,
			t.EdStatus           = s.EdStatus,
			t.HighSchool         = s.HighSchool,
			t.StudentGoal        = s.StudentGoal,
			t.EnrollStatus       = s.EnrollStatus,
			t.UnitsEarnedLoc     = s.UnitsEarnedLoc,
			t.UnitsEarnedTrn     = s.UnitsEarnedTrn,
			t.UnitsAttLoc        = s.UnitsAttLoc,
			t.UnitsAttTrn        = s.UnitsAttTrn,
			t.GPointsLoc         = s.GPointsLoc,
			t.GPointsTrn         = s.GPointsTrn,
			t.AcademicStanding   = s.AcademicStanding,
			t.AcademicLevel      = s.AcademicLevel,
			t.DSPS               = s.DSPS,
			t.EOPS               = s.EOPS,
			t.BoggFlag           = s.BoggFlag,
			t.PellFlag           = s.PellFlag,
			t.FinancialAidFlag   = s.FinancialAidFlag,
			t.Major              = s.Major,
			t.ccLongTermId       = s.ccLongTermId,
			t.military           = s.military,
			t.military_dependent = s.military_dependent,
			t.foster_care        = s.foster_care,
			t.incarcerated       = s.incarcerated,
			t.mesa               = s.mesa,
			t.puente             = s.puente,
			t.mchs               = s.mchs,
			t.umoja              = s.umoja,
			t.[1st_gen]          = s.[1st_gen],
			t.caa                = s.caa
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				Derkey1,
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				CSISNum,
				Birthdate,
				FName,
				LName,
				Gender,
				Race,
				Citizen,
				ZipCode,
				EdStatus,
				HighSchool,
				StudentGoal,
				EnrollStatus,
				UnitsEarnedLoc,
				UnitsEarnedTrn,
				UnitsAttLoc,
				UnitsAttTrn,
				GPointsLoc,
				GPointsTrn,
				AcademicStanding,
				AcademicLevel,
				DSPS,
				EOPS,
				BoggFlag,
				PellFlag,
				FinancialAidFlag,
				Major,
				ccLongTermId,
				military,
				military_dependent,
				foster_care,
				incarcerated,
				mesa,
				puente,
				mchs,
				umoja,
				[1st_gen],
				caa
			)
		VALUES
			(
				s.Derkey1,
				s.CollegeId,
				s.StudentComisId,
				s.StudentId,
				s.IdStatus,
				s.TermId,
				s.CSISNum,
				s.Birthdate,
				s.FName,
				s.LName,
				s.Gender,
				s.Race,
				s.Citizen,
				s.ZipCode,
				s.EdStatus,
				s.HighSchool,
				s.StudentGoal,
				s.EnrollStatus,
				s.UnitsEarnedLoc,
				s.UnitsEarnedTrn,
				s.UnitsAttLoc,
				s.UnitsAttTrn,
				s.GPointsLoc,
				s.GPointsTrn,
				s.AcademicStanding,
				s.AcademicLevel,
				s.DSPS,
				s.EOPS,
				s.BoggFlag,
				s.PellFlag,
				s.FinancialAidFlag,
				s.Major,
				s.ccLongTermId,
				s.military,
				s.military_dependent,
				s.foster_care,
				s.incarcerated,
				s.mesa,
				s.puente,
				s.mchs,
				s.umoja,
				s.[1st_gen],
				s.caa
			);

END;