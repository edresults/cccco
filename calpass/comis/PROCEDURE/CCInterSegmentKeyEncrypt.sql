USE calpass;

GO

IF (object_id('comis.CCInterSegmentKeyEncrypt') is not null)
	BEGIN
		DROP PROCEDURE comis.CCInterSegmentKeyEncrypt;
	END;

GO

CREATE PROCEDURE
	comis.CCInterSegmentKeyEncrypt
	(
		@CollegeCode char(3)
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Algorithm char(8) = 'SHA2_512',
	@Severity tinyint = 0,
	@State tinyint = 1,
	@RowCount varchar(2048),
	@Error varchar(2048);

BEGIN

	OPEN SYMMETRIC KEY
		SecPii
	DECRYPTION BY CERTIFICATE
		SecPii;

	BEGIN TRY
		BEGIN TRANSACTION
			-- update ISK
			UPDATE
				id
			SET
				InterSegmentKey =
					HASHBYTES(
						@Algorithm,
						upper(convert(nchar(3), coalesce(st.name_first, DecryptByKey(st.name_first_enc)))) + 
						upper(convert(nchar(3), coalesce(st.name_last, DecryptByKey(st.name_last_enc)))) + 
						upper(convert(nchar(1), coalesce(st.gender, DecryptByKey(st.gender_enc)))) + 
						convert(nchar(8), coalesce(convert(char(8), sb.birthdate, 112), DecryptByKey(sb.birthdate_enc)))
					)
			FROM
				comis.studntid id
				inner join
				comis.sbstudnt sb
					on id.college_id = sb.college_id
					and id.student_id = sb.student_id
				inner join
				comis.stterm st
					on id.college_id = st.college_id
					and id.student_id = st.student_id
			WHERE
				st.college_id = @CollegeCode
				and id.InterSegmentKey is null
				and st.term_id = (
					SELECT
						min(st1.term_id)
					FROM
						comis.stterm st1
					WHERE
						st1.college_id = st.college_id
						and st1.student_id = st.student_id
				);
			-- capture
			SET @RowCount = convert(varchar(2048), @@ROWCOUNT);
			-- set
			SET @Error = 'Encrypted ' + @RowCount + ' COMIS.STUDNTID records';
			-- output
			RAISERROR(@Error, @Severity, @State) WITH NOWAIT;
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
			END;
    THROW;
	END CATCH;

	CLOSE SYMMETRIC KEY SecPii;
END;