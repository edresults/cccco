USE calpass;

GO

IF (object_id('comis.EscrowGet') is not null)
	BEGIN
		DROP PROCEDURE comis.EscrowGet;
	END;

GO

CREATE PROCEDURE
	comis.EscrowGet
	(
		@CollegeCode char(3)
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Message nvarchar(2048),
	@True bit = 1,
	@EscrowCCStudentProd sysname = 'CCStudentProd',
	@EscrowCCCourseProd sysname = 'CCCourseProd',
	@EscrowCCAwardProd sysname = 'CCAwardProd',
	@EscrowCCFinAidAwardsProd sysname = 'CCFinAidAwardsProd',
	@CollegeIpedsCode char(6);

BEGIN
	-- Ipeds Code
	SELECT
		@CollegeIpedsCode = IpedsCodeLegacy
	FROM
		comis.College
	WHERE
		CollegeCode = @CollegeCode;
	-- stterm
	SELECT
		CollegeId = id.college_id,
		CollegeIpedsCode = @CollegeIpedsCode,
		StudentLocalId = id.student_id,
		StudentSocialId = id.ssn,
		IdStatus = id.student_id_status,
		InterSegmentKey = id.InterSegmentKey,
		TermId = st.term_id,
		EscrowName = @EscrowCCStudentProd
	FROM
		comis.stterm st
		inner join
		comis.studntid id with(index(pk_studntid__college_id__student_id))
			on st.college_id = id.college_id
			and st.student_id = id.student_id
	WHERE
		id.college_id = @CollegeCode
		and st.IsEscrow = @True
	UNION
	-- sxenrlm
	SELECT
		CollegeId = id.college_id,
		CollegeIpedsCode = @CollegeIpedsCode,
		StudentLocalId = id.student_id,
		StudentSocialId = id.ssn,
		IdStatus = id.student_id_status,
		InterSegmentKey = id.InterSegmentKey,
		TermId = sx.term_id,
		EscrowName = @EscrowCCCourseProd
	FROM
		comis.sxenrlm sx
		inner join
		comis.studntid id with(index(pk_studntid__college_id__student_id))
			on sx.college_id = id.college_id
			and sx.student_id = id.student_id
	WHERE
		id.college_id = @CollegeCode
		and sx.IsEscrow = @True
	UNION
	-- cbcrsinv
	SELECT
		CollegeId = id.college_id,
		CollegeIpedsCode = @CollegeIpedsCode,
		StudentLocalId = id.student_id,
		StudentSocialId = id.ssn,
		IdStatus = id.student_id_status,
		InterSegmentKey = id.InterSegmentKey,
		TermId = sx.term_id,
		EscrowName = @EscrowCCCourseProd
	FROM
		comis.sxenrlm sx
		inner join
		comis.studntid id with(index(pk_studntid__college_id__student_id))
			on sx.college_id = id.college_id
			and sx.student_id = id.student_id
	WHERE
		id.college_id = @CollegeCode
		and sx.IsEscrow = @True
		and exists (
			SELECT
				1
			FROM
				comis.cbcrsinv cb
			WHERE
				cb.college_id = sx.college_id
				and cb.term_id = sx.term_id
		)
	UNION
	-- spawards
	SELECT
		CollegeId = id.college_id,
		CollegeIpedsCode = @CollegeIpedsCode,
		StudentLocalId = id.student_id,
		StudentSocialId = id.ssn,
		IdStatus = id.student_id_status,
		InterSegmentKey = id.InterSegmentKey,
		TermId = sp.term_id,
		EscrowName = @EscrowCCAwardProd
	FROM
		comis.spawards sp
		inner join
		comis.studntid id with(index(pk_studntid__college_id__student_id))
			on sp.college_id = id.college_id
			and sp.student_id = id.student_id
	WHERE
		id.college_id = @CollegeCode
		and sp.IsEscrow = @True
	UNION
	-- sfawards
	SELECT
		CollegeId = id.college_id,
		CollegeIpedsCode = @CollegeIpedsCode,
		StudentLocalId = id.student_id,
		StudentSocialId = id.ssn,
		IdStatus = id.student_id_status,
		InterSegmentKey = id.InterSegmentKey,
		TermId = sf.term_id,
		EscrowName = @EscrowCCAwardProd
	FROM
		comis.sfawards sf
		inner join
		comis.studntid id with(index(pk_studntid__college_id__student_id))
			on sf.college_id = id.college_id
			and sf.student_id = id.student_id
	WHERE
		id.college_id = @CollegeCode
		and sf.IsEscrow = @True;
END;