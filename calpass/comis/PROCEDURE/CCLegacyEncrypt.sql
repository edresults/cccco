USE calpass;

GO

IF (object_id('comis.CCLegacyEncrypt') is not null)
	BEGIN
		DROP PROCEDURE comis.CCLegacyEncrypt;
	END;

GO

CREATE PROCEDURE
	comis.CCLegacyEncrypt
	(
		@CollegeCode char(3)
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Social char(1) = 'S',
	@Default char(1) = '',
	@Severity tinyint = 0,
	@State tinyint = 1,
	@Error varchar(4000),
	@RowCount varchar(11);

BEGIN

	-- STUDNTID
	BEGIN TRY
		BEGIN TRANSACTION
			-- update
			UPDATE
				comis.studntid
			SET
				ssn = dbo.Get1289Encryption(ssn, @Default)
			WHERE
				college_id = @CollegeCode
				and student_id_status = @Social;
			-- capture
			SET @RowCount = convert(varchar(11), @@ROWCOUNT);
			-- set
			SET @Error = 'Updated ' + @RowCount + ' COMIS.STUDNTID records for College ' + @CollegeCode;
			-- output
			RAISERROR(@Error, @Severity, @State) WITH NOWAIT;
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
			END;
		THROW;
	END CATCH;

	-- HF_FIRST
	BEGIN TRY
		BEGIN TRANSACTION
			-- update
			UPDATE
				comis.hf_first
			SET
				student_ssn = dbo.Get1289Encryption(student_ssn, @Default)
			WHERE
				college_id = @CollegeCode
				and id_status = @Social;
			-- capture
			SET @RowCount = convert(varchar(2048), @@ROWCOUNT);
			-- set
			SET @Error = 'Updated ' + @RowCount + ' COMIS.HF_FIRST records for College ' + @CollegeCode;
			-- output
			RAISERROR(@Error, @Severity, @State) WITH NOWAIT;
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
			END;
    THROW;
	END CATCH;
END;