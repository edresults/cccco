USE calpass;

GO

IF (object_id('comis.LegacyMigrate') is not null)
	BEGIN
		DROP PROCEDURE comis.LegacyMigrate;
	END;

GO

CREATE PROCEDURE
	comis.LegacyMigrate
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Message     nvarchar(2048),
	@True        bit = 1,
	@False       bit = 0,
	@CollegeCode char(3);

	DECLARE
		CursorCollege
	CURSOR
		FAST_FORWARD
	FOR
		SELECT
			CollegeCode
		FROM
			comis.College
		ORDER BY
			CollegeCode;

BEGIN

	CREATE TABLE
		#Escrow
		(
			Dependent   sysname,
			Entity      sysname,
			IsEscrow    bit
		);

	INSERT INTO
		#Escrow
		(
			Dependent,
			Entity,
			IsEscrow
		)
	SELECT
		Dependent,
		Entity,
		IsEscrow
	FROM
		(
			VALUES
			('CCStudentProd',     'comis.stterm',   @True),
			('CCStudentProd',     'comis.sbstudnt', @True),
			('CCStudentProd',     'comis.studntid', @True),
			('CCStudentProd',     'comis.sddsps',   @True),
			('CCStudentProd',     'comis.seeops',   @True),
			('CCStudentProd',     'comis.smmatric', @True),
			('CCStudentProd',     'comis.sgpops',   @True),
			('CCStudentProd',     'comis.svvatea',  @True),
			('CCStudentProd_EXT', 'comis.stterm',   @True),
			('CCStudentProd_EXT', 'comis.sbstudnt', @True),
			('CCStudentProd_EXT', 'comis.studntid', @True),
			('CCStudentProd_EXT', 'comis.sddsps',   @True),
			('CCStudentProd_EXT', 'comis.seeops',   @True),
			('CCStudentProd_EXT', 'comis.smmatric', @True),
			('CCStudentProd_EXT', 'comis.sgpops',   @True),
			('CCStudentProd_EXT', 'comis.svvatea',  @True),
			('CCCourseProd',      'comis.studntid', @True),
			('CCCourseProd',      'comis.sxenrlm',  @True),
			('CCCourseProd',      'comis.cbcrsinv', @True),
			('CCCourseProd_EXT',  'comis.xfsesion', @True),
			('CCCourseProd_EXT',  'comis.xbsecton', @True),
			('CCCourseProd_EXT',  'comis.sxenrlm',  @True),
			('CCCourseProd_EXT',  'comis.cbcrsinv', @True),
			('CCCourseProd_EXT',  'comis.studntid', @True),
			('CCAwardProd',       'comis.spawards', @True),
			('CCAwardProd',       'comis.studntid', @True),
			('CCFinAidAwardsProd','comis.sfawards', @True),
			('CCFinAidAwardsProd','comis.studntid', @True)
		) t
		(
			Dependent,
			Entity,
			IsEscrow
		);

	-- rebuild from scratch
	TRUNCATE TABLE CCStudentProd;
	TRUNCATE TABLE CCStudentProd_EXT;
	TRUNCATE TABLE CCCourseProd;
	TRUNCATE TABLE CCCourseProd_EXT;
	TRUNCATE TABLE CCAwardProd;
	TRUNCATE TABLE CCFinAidAwardsProd;

	OPEN CursorCollege;
	
	FETCH NEXT FROM
		CursorCollege
	INTO
		@CollegeCode;

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		RAISERROR(@CollegeCode, 0, 1) WITH NOWAIT;
		-- Reset Escrow Values
		UPDATE
			#Escrow
		SET
			IsEscrow = @True;

		BEGIN TRY
			-- set proc name
			SET @Message = 'comis.CCStudentProdMergeByCollege';
			-- merge baseline data to production tables
			EXECUTE comis.CCStudentProdMergeByCollege
				@CollegeCode = @CollegeCode;
			-- update escrow results
			UPDATE
				#Escrow
			SET
				IsEscrow = @False
			WHERE
				Dependent = 'CCStudentProd';
		END TRY
		BEGIN CATCH
			-- set error message
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + @Message + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			-- put error message
			RAISERROR(@message, 0, 1) WITH NOWAIT;
		END CATCH;

		BEGIN TRY
			-- set proc name
			SET @Message = 'comis.CCStudentProd_EXTMergeByCollege';
			-- merge baseline data to production tables
			EXECUTE comis.CCStudentProd_EXTMergeByCollege
				@CollegeCode = @CollegeCode;
			-- update escrow results
			UPDATE
				#Escrow
			SET
				IsEscrow = @False
			WHERE
				Dependent = 'CCStudentProd_EXT';
		END TRY
		BEGIN CATCH
			-- set error message
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + @Message + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			-- put error message
			RAISERROR(@message, 0, 1) WITH NOWAIT;
		END CATCH;

		BEGIN TRY
			-- set proc name
			SET @Message = 'comis.CCCourseProdMergeByCollege';
			-- merge baseline data to production tables
			EXECUTE comis.CCCourseProdMergeByCollege
				@CollegeCode = @CollegeCode;
			-- update escrow results
			UPDATE
				#Escrow
			SET
				IsEscrow = @False
			WHERE
				Dependent = 'CCCourseProd';
		END TRY
		BEGIN CATCH
			-- set error message
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + @Message + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			-- put error message
			RAISERROR(@message, 0, 1) WITH NOWAIT;
		END CATCH;

		BEGIN TRY
			-- set proc name
			SET @Message = 'comis.CCCourseProd_EXTMergeByCollege';
			-- merge baseline data to production tables
			EXECUTE comis.CCCourseProd_EXTMergeByCollege
				@CollegeCode = @CollegeCode;
			-- update escrow results
			UPDATE
				#Escrow
			SET
				IsEscrow = @False
			WHERE
				Dependent = 'CCCourseProd_EXT';
		END TRY
		BEGIN CATCH
			-- set error message
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + @Message + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			-- put error message
			RAISERROR(@message, 0, 1) WITH NOWAIT;
		END CATCH;

		BEGIN TRY
			-- set proc name
			SET @Message = 'comis.CCAwardProdMergeByCollege';
			-- merge baseline data to production tables
			EXECUTE comis.CCAwardProdMergeByCollege
				@CollegeCode = @CollegeCode;
			-- update escrow results
			UPDATE
				#Escrow
			SET
				IsEscrow = @False
			WHERE
				Dependent = 'CCAwardProd';
		END TRY
		BEGIN CATCH
			-- set error message
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + @Message + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			-- put error message
			RAISERROR(@message, 0, 1) WITH NOWAIT;
		END CATCH;

		BEGIN TRY
			-- set proc name
			SET @Message = 'comis.CCFinAidAwardsProdMergeByCollege';
			-- merge baseline data to production tables
			EXECUTE comis.CCFinAidAwardsProdMergeByCollege
				@CollegeCode = @CollegeCode;
			-- update escrow results
			UPDATE
				#Escrow
			SET
				IsEscrow = @False
			WHERE
				Dependent = 'CCFinAidAwardsProd';
		END TRY
		BEGIN CATCH
			-- set error message
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + @Message + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			-- put error message
			RAISERROR(@message, 0, 1) WITH NOWAIT;
		END CATCH;

		IF (EXISTS (SELECT Entity FROM #Escrow WHERE Entity = 'comis.cbcrsinv' GROUP BY Entity HAVING max(convert(tinyint, IsEscrow)) = 0))
			BEGIN
				UPDATE comis.cbcrsinv  SET IsEscrow = @False WHERE college_id = @CollegeCode and IsEscrow = @True;
			END;
		IF (EXISTS (SELECT Entity FROM #Escrow WHERE Entity = 'comis.sddsps' GROUP BY Entity HAVING max(convert(tinyint, IsEscrow)) = 0))
			BEGIN
				UPDATE comis.sddsps    SET IsEscrow = @False WHERE college_id = @CollegeCode and IsEscrow = @True;
			END;
		IF (EXISTS (SELECT Entity FROM #Escrow WHERE Entity = 'comis.seeops' GROUP BY Entity HAVING max(convert(tinyint, IsEscrow)) = 0))
			BEGIN
				UPDATE comis.seeops    SET IsEscrow = @False WHERE college_id = @CollegeCode and IsEscrow = @True;
			END;
		IF (EXISTS (SELECT Entity FROM #Escrow WHERE Entity = 'comis.sfawards' GROUP BY Entity HAVING max(convert(tinyint, IsEscrow)) = 0))
			BEGIN
				UPDATE comis.sfawards  SET IsEscrow = @False WHERE college_id = @CollegeCode and IsEscrow = @True;
			END;
		IF (EXISTS (SELECT Entity FROM #Escrow WHERE Entity = 'comis.sgpops' GROUP BY Entity HAVING max(convert(tinyint, IsEscrow)) = 0))
			BEGIN
				UPDATE comis.sgpops    SET IsEscrow = @False WHERE college_id = @CollegeCode and IsEscrow = @True;
			END;
		IF (EXISTS (SELECT Entity FROM #Escrow WHERE Entity = 'comis.smmatric' GROUP BY Entity HAVING max(convert(tinyint, IsEscrow)) = 0))
			BEGIN
				UPDATE comis.smmatric  SET IsEscrow = @False WHERE college_id = @CollegeCode and IsEscrow = @True;
			END;
		IF (EXISTS (SELECT Entity FROM #Escrow WHERE Entity = 'comis.spawards' GROUP BY Entity HAVING max(convert(tinyint, IsEscrow)) = 0))
			BEGIN
				UPDATE comis.spawards  SET IsEscrow = @False WHERE college_id = @CollegeCode and IsEscrow = @True;
			END;
		IF (EXISTS (SELECT Entity FROM #Escrow WHERE Entity = 'comis.stterm' GROUP BY Entity HAVING max(convert(tinyint, IsEscrow)) = 0))
			BEGIN
				UPDATE comis.stterm    SET IsEscrow = @False WHERE college_id = @CollegeCode and IsEscrow = @True;
			END;
		IF (EXISTS (SELECT Entity FROM #Escrow WHERE Entity = 'comis.svvatea' GROUP BY Entity HAVING max(convert(tinyint, IsEscrow)) = 0))
			BEGIN
				UPDATE comis.svvatea   SET IsEscrow = @False WHERE college_id = @CollegeCode and IsEscrow = @True;
			END;
		IF (EXISTS (SELECT Entity FROM #Escrow WHERE Entity = 'comis.sxenrlm' GROUP BY Entity HAVING max(convert(tinyint, IsEscrow)) = 0))
			BEGIN
				UPDATE comis.sxenrlm   SET IsEscrow = @False WHERE college_id = @CollegeCode and IsEscrow = @True;
			END;
		IF (EXISTS (SELECT Entity FROM #Escrow WHERE Entity = 'comis.xbsecton' GROUP BY Entity HAVING max(convert(tinyint, IsEscrow)) = 0))
			BEGIN
				UPDATE comis.xbsecton  SET IsEscrow = @False WHERE college_id = @CollegeCode and IsEscrow = @True;
			END;
		IF (EXISTS (SELECT Entity FROM #Escrow WHERE Entity = 'comis.xfsesion' GROUP BY Entity HAVING max(convert(tinyint, IsEscrow)) = 0))
			BEGIN
				UPDATE comis.xfsesion  SET IsEscrow = @False WHERE college_id = @CollegeCode and IsEscrow = @True;
			END;

		FETCH NEXT FROM
			CursorCollege
		INTO
			@CollegeCode;
	END;

	CLOSE CursorCollege;
	DEALLOCATE CursorCollege;

END;