USE calpass;

GO

IF (object_id('comis.Rebuild') is not null)
	BEGIN
		DROP PROCEDURE comis.Rebuild;
	END;

GO

CREATE PROCEDURE
	comis.Rebuild
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	-- constants
	@True bit = 1,
	@False bit = 0,
	@Severity tinyint = 0,
	@State tinyint = 1,
	-- variables
	@Error nvarchar(2048),
	@CollegeCode char(3),
	@CollegeIpedsCode char(6);

BEGIN
	-- drop DDL objects
	DROP TABLE dbo.CCStudentProd;
	DROP TABLE dbo.CCStudentProd_EXT;
	DROP TABLE dbo.CCCourseProd;
	DROP TABLE dbo.CCCourseProd_EXT;
	DROP TABLE dbo.CCAwardProd;
	DROP TABLE dbo.CCFinAidAwardsProd;
	-- create DDL objects
	CREATE TABLE
		dbo.CCStudentProd
		(
			Derkey1 binary(64),
			CollegeId char(6) not null,
			StudentComisId char(9) not null,
			StudentId binary(64) not null,
			IdStatus char(1) not null,
			TermId char(3) not null,
			CSISNum char(10),
			Birthdate char(8),
			FName char(3),
			LName char(3),
			Gender char(1),
			Race char(1),
			RaceIpeds char(1),
			RaceGroup char(2),
			Citizen char(1),
			ZipCode char(5),
			EdStatus char(5),
			HighSchool char(6),
			StudentGoal char(1),
			EnrollStatus char(1),
			UnitsEarnedLoc decimal(6,2),
			UnitsEarnedTrn decimal(6,2),
			UnitsAttLoc decimal(6,2),
			UnitsAttTrn decimal(6,2),
			GPointsLoc decimal(6,2),
			GPointsTrn decimal(6,2),
			AcademicStanding char(1),
			AcademicLevel char(1),
			DSPS char(1),
			EOPS char(1),
			BoggFlag char(1),
			PellFlag char(1),
			FinancialAidFlag char(1),
			Major char(6),
			ccLongTermId char(5),
			military char(4),
			military_dependent char(4),
			foster_care char(1),
			incarcerated char(1),
			mesa char(1),
			puente char(1),
			mchs char(1),
			umoja char(1),
			[1st_gen] char(2),
			caa char(1)
		);

	CREATE TABLE
		dbo.CCStudentProd_EXT
		(
			CollegeId char(6) not null,
			StudentComisId char(9) not null,
			StudentId binary(64) not null,
			IdStatus char(1) not null,
			TermId char(3) not null,
			extSB09_ResidenceCode varchar(5),
			extSB23_ApprenticeshipStatus char(1),
			extSB24_TransferCenterStatus char(1),
			extSB26_JTPAStatus char(1),
			extSB27_CalWorksStatus char(1),
			extSM01_MatricGoals varchar(4),
			extSM01_MatricGoalsPos1 char(1),
			extSM01_MatricGoalsPos2 char(1),
			extSM01_MatricGoalsPos3 char(1),
			extSM01_MatricGoalsPos4 char(1),
			extSM07_MatricOrientationServices char(1),
			extSM08_MatricAssessmentServicesPlacement char(1),
			extSM09_MatricAssessmentServicesOther varchar(3),
			extSM09_MatricAssessmentServicesOtherPos1 char(1),
			extSM09_MatricAssessmentServicesOtherPos2 char(1),
			extSM09_MatricAssessmentServicesOtherPos3 char(1),
			extSM12_MatricCounAdviseServices char(1),
			extSM13_MatricAcademicFollowupSerives char(1),
			extSVO1_ProgramPlanStatus char(1),
			extSV03_EconomicallyDisadvStatus varchar(2),
			extSV03_EconomicallyDisadvStatusPos1 char(1),
			extSV03_EconomicallyDisadvStatusPos2 char(1),
			extSV04_SingleParentStatus char(1),
			extSV05_DisplacedHomemakerStatus char(1),
			extSV06_CoopWorkExperienceEdType char(1),
			extSV08_TechPrepStatus char(1),
			extSV09_MigrantWorkerStatus char(1),
			extSCD1_FirstTermAttended char(3),
			extSCD2_LastTermAttended char(3),
			extSCD4_LEP char(1),
			extSCD5_AcademicallyDisadvantagedStudent char(1),
			extSTD1_AgeAtTerm int,
			extSTD2_1stCensusCreditLoad decimal(6,2),
			extSTD3_DayEveningClassCode char(1),
			extSTD5_DegreeAppUnitsEarned decimal(6,2),
			extSTD6_DayEveningClassCode2 char(1),
			extSTD7_HeadCountStatus char(1),
			extSTD8_LocalCumGPA decimal(5,2),
			extSTD9_TotalCumGPA decimal(5,2),
			extSD01_StudentPrimaryDisability char(1),
			extSD03_StudentSecondaryDisability char(1),
			extSB29_MultiEthnicity char(21)
		);

	CREATE TABLE
		dbo.CCCourseProd
		(
			CollegeId char(6) not null,
			StudentComisId char(9) not null,
			StudentId binary(64) not null,
			IdStatus char(1) not null,
			TermId char(3) not null,
			CourseId varchar(12) not null,
			SectionId varchar(6) not null,
			Title varchar(68),
			UnitsAttempted decimal(5,2),
			UnitsEarned decimal(5,2),
			Grade varchar(3),
			CreditFlag char(1),
			TopCode char(6),
			TransferStatus char(1),
			UnitsMax decimal(5,2),
			BSStatus char(1),
			SamCode char(1),
			ClassCode char(1),
			CollegeLevel char(1),
			NCRCategory char(1),
			CCLongTermId char(5)
		);

	CREATE TABLE
		dbo.CCCourseProd_EXT
		(
			CollegeId char(6) not null,
			StudentComisId char(9) not null,
			StudentId binary(64) not null,
			IdStatus char(1) not null,
			TermId char(3) not null,
			CourseId varchar(12) not null,
			SectionId varchar(6) not null,
			extSX05_PositiveAttendanceHours decimal(5,1),
			extSXD4_TotalHours decimal(7,1),
			extXF01_SessionInstuctionMethod1 char(2),
			extXF01_SessionInstuctionMethod2 char(2),
			extXF01_SessionInstuctionMethod3 char(2),
			extXF01_SessionInstuctionMethod4 char(2),
			extXBD3_DayEveningClassCode char(1),
			extCB00_ControlNumber varchar(12),
			extCB04_CreditStatus char(1),
			extCB10_CoopWorkExpEdStatus char(1),
			extCB13_SpecialClassStatus char(1),
			extCB14_CanCode varchar(6),
			extCB15_CanSeqCode varchar(8),
			extCB19_CrosswalkCrsDeptName varchar(7),
			extCB20_CrosswalkCrsNumber varchar(9),
			extCB21_PriorToCollegeLevel char(1),
			extCB23_FundingAgencyCategory char(1),
			extCB24_ProgramStatus char(1),
			extXB01_AccountingMethod char(1),
            extCB25_GeneralEducationStatus char(1),
            extCB26_SupportCourseStatus char(1)
		);

	CREATE TABLE
		dbo.CCAwardProd
		(
			CollegeId char(6) not null,
			StudentComisId char(9) not null,
			StudentId binary(64) not null,
			IdStatus char(1) not null,
			TermId char(3) not null,
			TopCode char(6) not null,
			Award char(1) not null,
			DateOfAward char(8) not null,
			RecordId char(1) not null,
			ProgramCode char(5) not null,
			Derkey1 binary(64) null
		);

	CREATE TABLE
		dbo.CCFinAidAwardsProd
		(
			CollegeId char(6) not null,
			StudentComisId char(9) not null,
			StudentId binary(64) not null,
			IdStatus char(1) not null,
			TermId char(3) not null,
			TermRecd char(3) not null,
			TypeId char(2) not null,
			Amount int not null,
			Derkey1 binary(64) null
		);

	BEGIN TRY
		-- define cursor
		DECLARE
			CollegeCursor
		CURSOR 
			FAST_FORWARD
		FOR 
			SELECT
				CollegeCode,
				IpedsCodeLegacy
			FROM
				comis.College
			ORDER BY
				CollegeCode;

		-- open cursor
		OPEN CollegeCursor;

		-- capture cursor variables
		FETCH NEXT FROM
			CollegeCursor
		INTO
			@CollegeCode,
			@CollegeIpedsCode;

		-- loop over cursor
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			-- output college
			RAISERROR(@CollegeCode, 0, 1) WITH NOWAIT;

			-- CCStudentProd
			INSERT
				dbo.CCStudentProd
				(
					Derkey1,
					CollegeId,
					StudentComisId,
					StudentId,
					IdStatus,
					TermId,
					CSISNum,
					Birthdate,
					FName,
					LName,
					Gender,
					Race,
					RaceIpeds,
					RaceGroup,
					Citizen,
					ZipCode,
					EdStatus,
					HighSchool,
					StudentGoal,
					EnrollStatus,
					UnitsEarnedLoc,
					UnitsEarnedTrn,
					UnitsAttLoc,
					UnitsAttTrn,
					GPointsLoc,
					GPointsTrn,
					AcademicStanding,
					AcademicLevel,
					DSPS,
					EOPS,
					BoggFlag,
					PellFlag,
					FinancialAidFlag,
					Major,
					ccLongTermId,
					military,
					military_dependent,
					foster_care,
					incarcerated,
					mesa,
					puente,
					mchs,
					umoja,
					[1st_gen],
					caa
				)
			SELECT
				Derkey1 = id.InterSegmentKey,
				CollegeId = @CollegeIpedsCode,
				StudentComisId = st.student_id,
				StudentId = id.ssn,
				IdStatus = id.student_id_status,
				TermId = st.term_id,
				CSISNum = null,
				BirthDate = convert(char(8), sb.birthdate, 112),
				FName = null,
				LName = null,
				Gender = st.Gender,
				Race = 
					case
						when st.ipeds_race not in ('H', 'T') and substring(st.multi_race, 13, 1) = 'Y' then 'F'
						else st.ipeds_race
					end,
				RaceIpeds = st.ipeds_race,
				RaceGroup =
					case
						-- hispanic
						when substring(st.multi_race, 1,  1) = 'Y' then 'HX'
						when substring(st.multi_race, 2,  1) = 'Y' then 'HM'
						when substring(st.multi_race, 3,  1) = 'Y' then 'HC'
						when substring(st.multi_race, 4,  1) = 'Y' then 'HS'
						when substring(st.multi_race, 5,  1) = 'Y' then 'HO'
						-- asian
						when substring(st.multi_race, 13, 1) = 'Y' then 'FX'
						when substring(st.multi_race, 6,  1) = 'Y' then 'AI'
						when substring(st.multi_race, 7,  1) = 'Y' then 'AC'
						when substring(st.multi_race, 8,  1) = 'Y' then 'AJ'
						when substring(st.multi_race, 9,  1) = 'Y' then 'AK'
						when substring(st.multi_race, 10, 1) = 'Y' then 'AL'
						when substring(st.multi_race, 11, 1) = 'Y' then 'AC'
						when substring(st.multi_race, 12, 1) = 'Y' then 'AV'
						when substring(st.multi_race, 14, 1) = 'Y' then 'AO'
						-- black
						when substring(st.multi_race, 15, 1) = 'Y' then 'BX'
						-- native american
						when substring(st.multi_race, 16, 1) = 'Y' then 'NX'
						-- pacific islander
						when substring(st.multi_race, 17, 1) = 'Y' then 'PG'
						when substring(st.multi_race, 18, 1) = 'Y' then 'PH'
						when substring(st.multi_race, 19, 1) = 'Y' then 'PS'
						when substring(st.multi_race, 20, 1) = 'Y' then 'PO'
						-- white
						when substring(st.multi_race, 21, 1) = 'Y' then 'W'
					end,
				Citizen = st.citizenship,
				ZipCode = st.zip,
				EdStatus = st.education,
				HighSchool = st.high_school,
				StudentGoal = st.goal,
				EnrollStatus = st.enrollment,
				UnitsEarnedLoc = st.u_earned_loc,
				UnitsEarnedTrn = st.u_earned_trn,
				UnitsAttLoc = st.u_attmpt_loc,
				UnitsAttTrn = st.u_attmpt_trn,
				GPointsLoc = st.g_points_loc,
				GPointsTrn = st.g_points_trn,
				AcademicStanding = st.aca_standing,
				AcademicLevel = st.academic_level,
				DSPS = isnull(sd.primary_disability, 'Y'),
				EOPS = 
					isnull(
						case
							when se.eops_care_status = 'N' then 'E'
							when se.eops_care_status = 'C' then 'C'
							when se.eops_care_status = 'P' then 'Y'
						end,
						'Y'
					),
				BoggFlag = 
					isnull(
						(
							SELECT DISTINCT
								'B'
							FROM
								comis.sfawards sf
							WHERE
								sf.type_id in ('BA','B1','B2','B3','BB','BC','F1','F2','F3','F4','F5')
								and sf.college_id = st.college_id
								and sf.student_id = st.student_id
								and sf.term_recd = st.term_id
						),
						'Y'
					),
				-- cannot use left outer join when there should be a 1:1 match on primary key.
				-- CO does not validate data
				-- this event Should not exist
				-- annual term (100) with 105 term.
				PellFlag = 
					isnull(
						(
							SELECT DISTINCT
								'P'
							FROM
								comis.sfawards sf
							WHERE
								sf.type_id  = 'GP'
								and sf.college_id = st.college_id
								and sf.student_id = st.student_id
								and sf.term_recd = st.term_id
						),
						'Y'
					),
				FinancialAidFlag = 
					isnull(
						(
							SELECT
								min(
									case
										when sf.type_id in ('GS','GW','LD','LG','LS','WF') then 'A'
										when sf.type_id in ('GB','GC','GE','GF','LE','WC','WE','WU') then 'B'
										when sf.type_id in ('LH','LP','LR','LL') then 'C'
										when sf.type_id in ('SU','SV','SX') then 'D'
										when sf.type_id in ('GU', 'GV', 'LI', 'LN', 'GG') then 'E'
										else 'Y'
									end
								)
							FROM
								comis.sfawards sf
							WHERE
								sf.college_id = st.college_id
								and sf.student_id = st.student_id
								and sf.term_recd = st.term_id
						),
						'Y'
					),
				Major = coalesce(ss.major, sm.major, 'XXXXXX'),
				ccLongTermId = t.YearTermCode,
				sg.military,
				sg.military_dependent,
				sg.foster_care,
				sg.incarcerated,
				sg.mesa,
				sg.puente,
				sg.mchs,
				sg.umoja,
				[1st_gen] = coalesce(sg.gen_first, st.parent_ed),
				sg.caa
			FROM
				comis.stterm st
				inner join
				comis.sbstudnt sb
					on sb.college_id = st.college_id
					and sb.student_id = st.student_id
				inner join
				comis.studntid id
					on id.college_id = st.college_id
					and id.student_id = st.student_id
				inner join
				comis.Term t
					on t.TermCode = st.term_id
				left outer join
				comis.sddsps sd
					on sd.college_id = st.college_id
					and sd.student_id = st.student_id
					and sd.term_id = st.term_id
				left outer join
				comis.seeops se
					on se.college_id = st.college_id
					and se.student_id = st.student_id
					and se.term_id = st.term_id
				left outer join
				comis.smmatric sm
					on sm.college_id = st.college_id
					and sm.student_id = st.student_id
					and sm.term_id = st.term_id
				left outer join
				comis.sssuccess ss
					on  ss.college_id = st.college_id
					and ss.student_id = st.student_id
					and ss.term_id    = st.term_id
				left outer join
				comis.sgpops sg
					on sg.college_id = st.college_id
					and sg.student_id = st.student_id
					and sg.term_id = st.term_id
				left outer join
				comis.svvatea sv
					on sv.college_id = st.college_id
					and sv.student_id = st.student_id
					and sv.term_id = st.term_id
			WHERE
				st.college_id = @CollegeCode
				and id.student_id_status is not null;

			-- CCStudentProd_EXT
			INSERT
				dbo.CCStudentProd_EXT
				(
					CollegeId,
					StudentComisId,
					StudentId,
					IdStatus,
					TermId,
					extSB09_ResidenceCode,
					extSB23_ApprenticeshipStatus,
					extSB24_TransferCenterStatus,
					extSB26_JTPAStatus,
					extSB27_CalWorksStatus,
					extSM01_MatricGoals,
					extSM01_MatricGoalsPos1,
					extSM01_MatricGoalsPos2,
					extSM01_MatricGoalsPos3,
					extSM01_MatricGoalsPos4,
					extSM07_MatricOrientationServices,
					extSM08_MatricAssessmentServicesPlacement,
					extSM09_MatricAssessmentServicesOther,
					extSM09_MatricAssessmentServicesOtherPos1,
					extSM09_MatricAssessmentServicesOtherPos2,
					extSM09_MatricAssessmentServicesOtherPos3,
					extSM12_MatricCounAdviseServices,
					extSM13_MatricAcademicFollowupSerives,
					extSVO1_ProgramPlanStatus,
					extSV03_EconomicallyDisadvStatus,
					extSV03_EconomicallyDisadvStatusPos1,
					extSV03_EconomicallyDisadvStatusPos2,
					extSV04_SingleParentStatus,
					extSV05_DisplacedHomemakerStatus,
					extSV06_CoopWorkExperienceEdType,
					extSV08_TechPrepStatus,
					extSV09_MigrantWorkerStatus,
					extSCD1_FirstTermAttended,
					extSCD2_LastTermAttended,
					extSCD4_LEP,
					extSCD5_AcademicallyDisadvantagedStudent,
					extSTD1_AgeAtTerm,
					extSTD2_1stCensusCreditLoad,
					extSTD3_DayEveningClassCode,
					extSTD5_DegreeAppUnitsEarned,
					extSTD6_DayEveningClassCode2,
					extSTD7_HeadCountStatus,
					extSTD8_LocalCumGPA,
					extSTD9_TotalCumGPA,
					extSD01_StudentPrimaryDisability,
					extSD03_StudentSecondaryDisability,
					extSB29_MultiEthnicity
				)
			SELECT
				CollegeId = @CollegeIpedsCode,
				StudentComisId = st.student_id,
				StudentId = id.ssn,
				IdStatus = id.student_id_status,
				TermId = st.term_id,
				extSB09_ResidenceCode = st.residency,
				extSB23_ApprenticeshipStatus = st.apprentice,
				extSB24_TransferCenterStatus = st.transfer_ctr,
				extSB26_JTPAStatus = st.jtpa_status,
				extSB27_CalWorksStatus = st.calworks_status,
				extSM01_MatricGoals = sm.goals,
				extSM01_MatricGoalsPos1 = substring(sm.goals, 1, 1),
				extSM01_MatricGoalsPos2 = substring(sm.goals, 2, 1),
				extSM01_MatricGoalsPos3 = substring(sm.goals, 3, 1),
				extSM01_MatricGoalsPos4 = substring(sm.goals, 4, 1),
				extSM07_MatricOrientationServices =sm.orientation_services,
				extSM08_MatricAssessmentServicesPlacement = sm.assessment_services_place,
				extSM09_MatricAssessmentServicesOther = sm.assessment_services_other,
				extSM09_MatricAssessmentServicesOtherPos1 = sm.assessment_services_other_01,
				extSM09_MatricAssessmentServicesOtherPos2 = sm.assessment_services_other_02,
				extSM09_MatricAssessmentServicesOtherPos3 = sm.assessment_services_other_03,
				extSM12_MatricCounAdviseServices = sm.advisement_services,
				extSM13_MatricAcademicFollowupSerives = sm.follow_up_services,
				extSVO1_ProgramPlanStatus = sv.voc_pgm_plan,
				extSV03_EconomicallyDisadvStatus = sv.econ_disadv,
				extSV03_EconomicallyDisadvStatusPos1 = sv.econ_disadv_status,
				extSV03_EconomicallyDisadvStatusPos2 = sv.econ_disadv_source,
				extSV04_SingleParentStatus = sv.single_parent,
				extSV05_DisplacedHomemakerStatus = sv.displ_homemker,
				extSV06_CoopWorkExperienceEdType = sv.coop_work_exp,
				extSV08_TechPrepStatus = sv.tech_prep,
				extSV09_MigrantWorkerStatus = sv.migrant_worker,
				extSCD1_FirstTermAttended = sb.term_first,
				extSCD2_LastTermAttended = sb.term_last,
				extSCD4_LEP = sb.lep_flag,
				extSCD5_AcademicallyDisadvantagedStudent = sb.acad_disad_flag,
				extSTD1_AgeAtTerm = st.age_at_term,
				extSTD2_1stCensusCreditLoad = st."1census_crload",
				extSTD3_DayEveningClassCode = st.day_evening_code,
				extSTD5_DegreeAppUnitsEarned = st.deg_appl_units_earned,
				extSTD6_DayEveningClassCode2 = st.day_evening_code2,
				extSTD7_HeadCountStatus = st.headcount_status,
				extSTD8_LocalCumGPA = st.gpa_loc,
				extSTD9_TotalCumGPA = st.gpa_tot,
				extSD01_StudentPrimaryDisability = sd.primary_disability,
				extSD03_StudentSecondaryDisability = sd.secondary_disability,
				extSB29_MultiEthnicity = null
			FROM
				comis.stterm st
				inner join
				comis.sbstudnt sb
					on st.college_id = sb.college_id
					and st.student_id = sb.student_id
				inner join
				comis.studntid id
					on id.college_id = st.college_id
					and id.student_id = st.student_id
				inner join
				comis.Term t
					on t.TermCode = st.term_id
				left outer join
				comis.sddsps sd
					on sd.college_id = st.college_id
					and sd.student_id = st.student_id
					and sd.term_id = st.term_id
				left outer join
				comis.seeops se
					on se.college_id = st.college_id
					and se.student_id = st.student_id
					and se.term_id = st.term_id
				left outer join
				comis.smmatric sm
					on  sm.college_id = st.college_id
					and sm.student_id = st.student_id
					and sm.term_id = st.term_id
                left outer join
                comis.sssuccess ss
					on  ss.college_id = st.college_id
					and ss.student_id = st.student_id
					and ss.term_id    = st.term_id
				left outer join
				comis.sgpops sg
					on sg.college_id = st.college_id
					and sg.student_id = st.student_id
					and sg.term_id = st.term_id
				left outer join
				comis.svvatea sv
					on sv.college_id = st.college_id
					and sv.student_id = st.student_id
					and sv.term_id = st.term_id
			WHERE
				st.college_id = @CollegeCode
				and id.student_id_status is not null;

			-- CCCourseProd
			INSERT INTO
				dbo.CCCourseProd
				(
					CollegeId,
					StudentComisId,
					StudentId,
					IdStatus,
					TermId,
					CourseId,
					SectionId,
					Title,
					UnitsMax,
					UnitsAttempted,
					UnitsEarned,
					Grade,
					CreditFlag,
					TopCode,
					TransferStatus,
					BSStatus,
					SamCode,
					ClassCode,
					CollegeLevel,
					NCRCategory,
					CCLongTermId
				)
			SELECT
				CollegeId = @CollegeIpedsCode,
				StudentComisId = sx.student_id,
				StudentId = id.ssn,
				IdStatus = id.student_id_status,
				TermId = sx.term_id,
				CourseId = sx.course_id,
				SectionId = sx.section_id,
				Title = cb.title,
				UnitsMax = cb.units_maximum,
				UnitsAttempted = sx.units_attempted,
				UnitsEarned = sx.units,
				Grade = sx.grade,
				CreditFlag = sx.credit_flag,
				TopCode = cb.top_code,
				TransferStatus = cb.transfer_status,
				BSStatus = cb.basic_skills_status,
				SamCode = cb.sam_code,
				ClassCode = cb.classification_code,
				CollegeLevel = cb.prior_to_college,
				NCRCategory = cb.noncredit_category,
				CCLongTermId = t.YearTermCode
			FROM
				comis.studntid id
				inner join
				comis.sxenrlm sx
					on id.college_id = sx.college_id
					and id.student_id = sx.student_id
				inner join
				comis.cbcrsinv cb
					on sx.college_id = cb.college_id
					and sx.term_id = cb.term_id
					and sx.course_id = cb.course_id
					and sx.control_number = cb.control_number
				inner join
				comis.Term t
					on t.TermCode = sx.term_id
			WHERE
				sx.college_id = @CollegeCode
				and id.student_id_status is not null;

			INSERT INTO
				dbo.CCCourseProd_EXT
				(
					CollegeId,
					StudentComisId,
					StudentId,
					IdStatus,
					TermId,
					CourseId,
					SectionId,
					extSX05_PositiveAttendanceHours,
					extSXD4_TotalHours,
					extXF01_SessionInstuctionMethod1,
					extXF01_SessionInstuctionMethod2,
					extXF01_SessionInstuctionMethod3,
					extXF01_SessionInstuctionMethod4,
					extXBD3_DayEveningClassCode,
					extCB00_ControlNumber,
					extCB04_CreditStatus,
					extCB10_CoopWorkExpEdStatus,
					extCB13_SpecialClassStatus,
					extCB14_CanCode,
					extCB15_CanSeqCode,
					extCB19_CrosswalkCrsDeptName,
					extCB20_CrosswalkCrsNumber,
					extCB21_PriorToCollegeLevel,
					extCB23_FundingAgencyCategory,
					extCB24_ProgramStatus,
					extXB01_AccountingMethod,
                    extCB25_GeneralEducationStatus,
                    extCB26_SupportCourseStatus
				)
			SELECT
				@CollegeIpedsCode           as CollegeId,
				id.student_id               as StudentComisId,
				id.ssn                      as StudentId,
				id.student_id_status        as IdStatus,
				xf.term_id                  as TermId,
				xf.course_id                as CourseId,
				xf.section_id               as SectionId,
				sx.attend_hours             as extSX05_PositiveAttendanceHours,
				sx.total_hours              as extSXD4_TotalHours,
				xf.SessionInstuctionMethod1 as extXF01_SessionInstuctionMethod1,
				xf.SessionInstuctionMethod2 as extXF01_SessionInstuctionMethod2,
				xf.SessionInstuctionMethod3 as extXF01_SessionInstuctionMethod3,
				xf.SessionInstuctionMethod4 as extXF01_SessionInstuctionMethod4,
				xb.day_evening_code         as extXBD3_DayEveningClassCode,
				cb.control_number           as extCB00_ControlNumber,
				cb.credit_status            as extCB04_CreditStatus,
				cb.coop_ed_status           as extCB10_CoopWorkExpEdStatus,
				cb.special_class_status     as extCB13_SpecialClassStatus,
				null as extCB14_CanCode, -- cb.can_code                 as extCB14_CanCode,
				null as extCB15_CanSeqCode, -- cb.can_seq_code             as extCB15_CanSeqCode,
				null as extCB19_CrosswalkCrsDeptName, -- cb.crosswalk_crs_name       as extCB19_CrosswalkCrsDeptName,
				null as extCB20_CrosswalkCrsNumber, -- cb.crosswalk_crs_number     as extCB20_CrosswalkCrsNumber,
				cb.prior_to_college         as extCB21_PriorToCollegeLevel,
				cb.funding_category         as extCB23_FundingAgencyCategory,
				cb.program_status           as extCB24_ProgramStatus,
				xb.accounting               as extXB01_AccountingMethod,
                cb.general_education_status as extCB25_GeneralEducationStatus,
                cb.support_course_status    as extCB26_SupportCourseStatus
			FROM
				(
					SELECT
						xf.college_id,
						xf.term_id,
						xf.course_id,
						xf.section_id,
						SessionInstuctionMethod1 = max(case when xf.InstructionMethodSelector = 1 then xf.instruction end),
						SessionInstuctionMethod2 = max(case when xf.InstructionMethodSelector = 2 then xf.instruction end),
						SessionInstuctionMethod3 = max(case when xf.InstructionMethodSelector = 3 then xf.instruction end),
						SessionInstuctionMethod4 = max(case when xf.InstructionMethodSelector = 4 then xf.instruction end)
					FROM
						(
							SELECT
								xf.college_id,
								xf.term_id,
								xf.course_id,
								xf.section_id,
								xf.instruction,
								InstructionMethodSelector = row_number() OVER(
										PARTITION BY
											xf.college_id,
											xf.term_id,
											xf.course_id,
											xf.section_id
										ORDER BY
											xf.session_id
									)
							FROM
								comis.xfsesion xf
							WHERE
								xf.college_id = @CollegeCode
						) xf
					GROUP BY
						xf.college_id,
						xf.term_id,
						xf.course_id,
						xf.section_id
				) xf
				inner join
				calpass.comis.xbsecton xb
					on xf.college_id = xb.college_id
					and xf.term_id = xb.term_id
					and xf.course_id = xb.course_id
					and xf.section_id= xb.section_id
				inner join
				calpass.comis.sxenrlm sx
					on xb.college_id = sx.college_id
					and xb.term_id = sx.term_id
					and xb.course_id = sx.course_id
					and xb.section_id = sx.section_id
				inner join
				calpass.comis.cbcrsinv cb
					on cb.college_id = sx.college_id
					and cb.term_id = sx.term_id
					and cb.course_id = sx.course_id
					and cb.control_number = sx.control_number
				inner join
				calpass.comis.studntid id
					on sx.college_id = id.college_id
					and sx.student_id = id.student_id
            WHERE
				id.student_id_status is not null;

			-- CCAwardProd
			INSERT INTO
				dbo.CCAwardProd
				(
					CollegeId,
					StudentComisId,
					StudentId,
					IdStatus,
					TermId,
					TopCode,
					Award,
					DateOfAward,
					RecordId,
					ProgramCode,
					Derkey1
				)
			SELECT
				CollegeId = @CollegeIpedsCode,
				StudentComisId = id.student_id,
				StudentId = id.ssn,
				IdStatus = id.student_id_status,
				TermId = aw.term_id,
				TopCode = aw.top_code,
				Award = aw.award,
				DateOfAward = convert(char(8), aw.date, 112),
				RecordId = aw.record_id,
				ProgramCode = aw.program_code,
				Derkey1 = id.InterSegmentKey
			FROM
				calpass.comis.spawards aw
				inner join
				calpass.comis.studntid id
					on aw.college_id = id.college_id
					and aw.student_id = id.student_id
			WHERE
				aw.college_id = @CollegeCode
				and id.student_id_status is not null;

			-- CCFinAwardsProd
			INSERT INTO
				calpass.dbo.CCFinAidAwardsProd
				(
					CollegeId,
					StudentComisId,
					StudentId,
					IdStatus,
					TermId,
					TermRecd,
					TypeId,
					Amount,
					Derkey1
				)
			SELECT
				CollegeCode = @CollegeIpedsCode,
				StudentComisId = id.student_id,
				StudentId = id.ssn,
				IdStatus = id.student_id_status,
				TermId = sf.term_id,
				TermRecd = sf.term_recd,
				TypeId = sf.type_id,
				Amount = sf.amount,
				Derkey1 = id.InterSegmentKey
			FROM
				calpass.comis.sfawards sf
				inner join
				calpass.comis.studntid id
					on sf.college_id = id.college_id
					and sf.student_id = id.student_id
			WHERE
				sf.college_id = @CollegeCode
				and id.student_id_status is not null;

			-- capture cursor variables
			FETCH NEXT FROM
				CollegeCursor
			INTO
				@CollegeCode,
				@CollegeIpedsCode;

		END;
		-- close cursor
		CLOSE CollegeCursor;
		-- trash cursor
		DEALLOCATE CollegeCursor;
	END TRY
	BEGIN CATCH
		-- close cursor
		CLOSE CollegeCursor;
		-- trash cursor
		DEALLOCATE CollegeCursor;
		-- throw error
		THROW
	END CATCH;

	CREATE CLUSTERED INDEX
		PK_CCStudentProd
	ON
		dbo.CCStudentProd
		(
			CollegeId,
			StudentId,
			TermId
		);

	CREATE NONCLUSTERED INDEX
		IX_CCStudentProd__Derkey1
	ON
		dbo.CCStudentProd
		(
			Derkey1
		);

	CREATE NONCLUSTERED INDEX
		IX_CCStudentProd__CollegeId__TermId
	ON
		dbo.CCStudentProd
		(
			CollegeId,
			TermId
		);

	CREATE NONCLUSTERED INDEX
		IX_CCStudentProd__CollegeId__StudentComisId__TermId
	ON
		dbo.CCStudentProd
		(
			CollegeId,
			StudentComisId,
			TermId
		);

	CREATE CLUSTERED INDEX
		PK_CCStudentProd_EXT
	ON
		dbo.CCStudentProd_EXT
		(
			CollegeId,
			StudentId,
			TermId
		);

	CREATE NONCLUSTERED INDEX
		IX_CCStudentProd_EXT__CollegeId__TermId
	ON
		dbo.CCStudentProd_EXT
		(
			CollegeId,
			TermId
		);

	CREATE NONCLUSTERED INDEX
		IX_CCStudentProd_EXT__CollegeId__StudentComisId__TermId
	ON
		dbo.CCStudentProd_EXT
		(
			CollegeId,
			StudentComisId,
			TermId
		);

	CREATE CLUSTERED INDEX
		PK_CCCourseProd
	ON
		dbo.CCCourseProd
		(
			CollegeId,
			StudentId,
			TermId,
			CourseId,
			SectionId
		);

	CREATE NONCLUSTERED INDEX
		IX_CCCourseProd__CollegeId__TermId
	ON
		dbo.CCCourseProd
		(
			CollegeId,
			TermId
		);

	CREATE NONCLUSTERED INDEX
		IX_CCCourseProd__CollegeId__StudentComisId__TermId__CourseId__SectionId
	ON
		dbo.CCCourseProd
		(
			CollegeId,
			StudentComisId,
			TermId,
			CourseId,
			SectionId
		);

	CREATE CLUSTERED INDEX
		PK_CCCourseProd_EXT
	ON
		dbo.CCCourseProd_EXT
		(
			CollegeId,
			StudentId,
			TermId,
			CourseId,
			SectionId
		);

	CREATE NONCLUSTERED INDEX
		IX_CCCourseProd_EXT__CollegeId__TermId
	ON
		dbo.CCCourseProd_EXT
		(
			CollegeId,
			TermId
		);

	CREATE NONCLUSTERED INDEX
		IX_CCCourseProd_EXT__CollegeId__StudentComisId__TermId__CourseId__SectionId
	ON
		dbo.CCCourseProd_EXT
		(
			CollegeId,
			StudentComisId,
			TermId,
			CourseId,
			SectionId
		);

	CREATE CLUSTERED INDEX
		PK_CCAwardProd
	ON
		dbo.CCAwardProd
		(
			CollegeId,
			StudentId,
			TermId,
			TopCode,
			Award,
			ProgramCode,
			DateOfAward,
			RecordId
		);

	CREATE NONCLUSTERED INDEX
		IX_CCAwardProd__CollegeId__TermId
	ON
		dbo.CCAwardProd
		(
			CollegeId,
			TermId
		);

	CREATE NONCLUSTERED INDEX
		IX_CCAwardProd__CollegeId_StudentComisId__TermId
	ON
		dbo.CCAwardProd
		(
			CollegeId,
			StudentComisId,
			TermId
		);

	CREATE CLUSTERED INDEX
		PK_CCFinAidAwardsProd
	ON
		dbo.CCFinAidAwardsProd
		(
			CollegeId,
			StudentId,
			TermId,
			TermRecd,
			TypeId
		);

	CREATE NONCLUSTERED INDEX
		IX_CCFinAidAwardsProd__CollegeId__TermId
	ON
		dbo.CCFinAidAwardsProd
		(
			CollegeId,
			TermId
		);

	CREATE NONCLUSTERED INDEX
		IX_CCFinAidAwardsProd__CollegeId__StudentComisId__TermId
	ON
		dbo.CCFinAidAwardsProd
		(
			CollegeId,
			StudentComisId,
			TermId
		);

END;