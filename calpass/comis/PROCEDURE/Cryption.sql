USE calpass;

GO

IF (object_id('comis.Cryption') is not null)
	BEGIN
		DROP PROCEDURE comis.Cryption;
	END;

GO

CREATE PROCEDURE
	comis.Cryption
	(
		@CollegeCode char(3)
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@False bit = 0,
	@True bit = 1,
	@HashAlgorithm sysname = 'SHA2_512',
	@KeyName sysname = 'SecPii';

BEGIN

	OPEN SYMMETRIC KEY
		SecPii
	DECRYPTION BY CERTIFICATE
		SecPii;

	-- InterSegmentKey
	UPDATE
		id
	SET
		id.InterSegmentKey = 
			HASHBYTES(
				@HashAlgorithm,
				Payload
			),
		InterSegmentKeyBug = 
			HASHBYTES(
				@HashAlgorithm,
				PayloadBug
			)
	FROM
		comis.studntid id
		cross apply
		comis.IskPayloadPK(id.college_id)
	WHERE
		id.college_id = @CollegeCode;

	-- encrypt demographic variables
	UPDATE
		comis.stterm
	SET
		name_first_enc = coalesce(name_first_enc, EncryptByKey(Key_GUID(@KeyName), name_first)),
		name_first = coalesce(name_first, convert(varchar(30), DecryptByKey(name_first_enc))),
		name_last_enc = coalesce(name_last_enc, EncryptByKey(Key_GUID(@KeyName), name_last)),
		name_last = coalesce(name_last, convert(varchar(40), DecryptByKey(name_last_enc))),
		gender_enc = coalesce(gender_enc, EncryptByKey(Key_GUID(@KeyName), gender)),
		gender = coalesce(gender, convert(char(1), DecryptByKey(gender_enc))),
		multi_race_enc = coalesce(multi_race_enc, EncryptByKey(Key_GUID(@KeyName), multi_race)),
		multi_race = coalesce(multi_race, convert(char(21), DecryptByKey(multi_race_enc))),
		ipeds_race_enc = coalesce(ipeds_race_enc, EncryptByKey(Key_GUID(@KeyName), ipeds_race)),
		ipeds_race = coalesce(ipeds_race, convert(char(1), DecryptByKey(ipeds_race_enc))),
		race_enc = coalesce(race_enc, EncryptByKey(Key_GUID(@KeyName), race)),
		race = coalesce(race, convert(char(2), DecryptByKey(race_enc))),
		IsEscrow = @False
	WHERE
		college_id = @CollegeCode;

	-- encrypt demographic variables
	UPDATE
		comis.sbstudnt
	SET
		birthdate_enc = coalesce(birthdate_enc, EncryptByKey(Key_GUID(@KeyName), convert(char(8), birthdate, 112))),
		birthdate = coalesce(birthdate, convert(datetime, convert(char(8), DecryptByKey(birthdate_enc))))
	WHERE
		college_id = @CollegeCode;

	CLOSE SYMMETRIC KEY SecPii;
END;