USE calpass;

GO

IF (object_id('comis.CCStudentProd_EXTMergeByCollege') is not null)
	BEGIN
		DROP PROCEDURE comis.CCStudentProd_EXTMergeByCollege;
	END;

GO

CREATE PROCEDURE
	comis.CCStudentProd_EXTMergeByCollege
	(
		@CollegeCode char(3)
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@IpedsCode char(6);

BEGIN

	SELECT
		@IpedsCode = IpedsCodeLegacy
	FROM
		comis.College
	WHERE
		CollegeCode = @CollegeCode;

	MERGE
		dbo.CCStudentProd_EXT
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				CollegeId = @IpedsCode,
				StudentComisId = st.student_id,
				StudentId = id.ssn,
				IdStatus = id.student_id_status,
				TermId = st.term_id,
				extSB09_ResidenceCode = st.residency,
				extSB23_ApprenticeshipStatus = st.apprentice,
				extSB24_TransferCenterStatus = st.transfer_ctr,
				extSB26_JTPAStatus = st.jtpa_status,
				extSB27_CalWorksStatus = st.calworks_status,
				extSM01_MatricGoals = sm.goals,
				extSM01_MatricGoalsPos1 = sm.goals_primary,
				extSM01_MatricGoalsPos2 = sm.goals_secondary,
				extSM01_MatricGoalsPos3 = sm.goals_tertiary,
				extSM01_MatricGoalsPos4 = sm.goals_placeholder,
				extSM07_MatricOrientationServices =sm.orientation_services,
				extSM08_MatricAssessmentServicesPlacement = sm.assessment_services_place,
				extSM09_MatricAssessmentServicesOther = sm.assessment_services_other,
				extSM09_MatricAssessmentServicesOtherPos1 = sm.assessment_services_other_01,
				extSM09_MatricAssessmentServicesOtherPos2 = sm.assessment_services_other_02,
				extSM09_MatricAssessmentServicesOtherPos3 = sm.assessment_services_other_03,
				extSM12_MatricCounAdviseServices = sm.advisement_services,
				extSM13_MatricAcademicFollowupSerives = sm.follow_up_services,
				extSVO1_ProgramPlanStatus = sv.voc_pgm_plan,
				extSV03_EconomicallyDisadvStatus = sv.econ_disadv,
				extSV03_EconomicallyDisadvStatusPos1 = substring(sv.econ_disadv, 1, 1),
				extSV03_EconomicallyDisadvStatusPos2 = substring(sv.econ_disadv, 2, 1),
				extSV04_SingleParentStatus = sv.single_parent,
				extSV05_DisplacedHomemakerStatus = sv.displ_homemker,
				extSV06_CoopWorkExperienceEdType = sv.coop_work_exp,
				extSV08_TechPrepStatus = sv.tech_prep,
				extSV09_MigrantWorkerStatus = sv.migrant_worker,
				extSCD1_FirstTermAttended = sb.term_first,
				extSCD2_LastTermAttended = sb.term_last,
				extSCD4_LEP = sb.lep_flag,
				extSCD5_AcademicallyDisadvantagedStudent = sb.acad_disad_flag,
				extSTD1_AgeAtTerm = st.age_at_term,
				extSTD2_1stCensusCreditLoad = st.census_crload,
				extSTD3_DayEveningClassCode = st.day_evening_code,
				extSTD5_DegreeAppUnitsEarned = st.deg_appl_units_earned,
				extSTD6_DayEveningClassCode2 = st.day_evening_code2,
				extSTD7_HeadCountStatus = st.headcount_status,
				extSTD8_LocalCumGPA = st.gpa_loc,
				extSTD9_TotalCumGPA = st.gpa_tot,
				extSD01_StudentPrimaryDisability = sd.primary_disability,
				extSD03_StudentSecondaryDisability = sd.secondary_disability,
				extSB29_MultiEthnicity = st.multi_race
			FROM
				comis.stterm st
				inner join
				comis.sbstudnt sb
					on st.college_id = sb.college_id
					and st.student_id = sb.student_id
				inner join
				comis.studntid id
					on id.college_id = st.college_id
					and id.student_id = st.student_id
				inner join
				comis.Term t
					on t.TermCode = st.term_id
				left outer join
				comis.sddsps sd
					on sd.college_id = st.college_id
					and sd.student_id = st.student_id
					and sd.term_id = st.term_id
				left outer join
				comis.seeops se
					on se.college_id = st.college_id
					and se.student_id = st.student_id
					and se.term_id = st.term_id
				left outer join
				comis.smmatric sm
					on sm.college_id = st.college_id
					and sm.student_id = st.student_id
					and sm.term_id = st.term_id
				left outer join
				comis.sgpops sg
					on sg.college_id = st.college_id
					and sg.student_id = st.student_id
					and sg.term_id = st.term_id
				left outer join
				comis.svvatea sv
					on sv.college_id = st.college_id
					and sv.student_id = st.student_id
					and sv.term_id = st.term_id
			WHERE
				st.college_id = @CollegeCode
		) s
	ON
		(
			s.CollegeId = t.CollegeId
			and s.StudentId = t.StudentId
			and s.TermId = t.TermId
		)
	WHEN MATCHED THEN
		UPDATE SET
			t.StudentComisId                            = s.StudentComisId,
			t.IdStatus                                  = s.IdStatus,
			t.extSB09_ResidenceCode                     = s.extSB09_ResidenceCode,
			t.extSB23_ApprenticeshipStatus              = s.extSB23_ApprenticeshipStatus,
			t.extSB24_TransferCenterStatus              = s.extSB24_TransferCenterStatus,
			t.extSB26_JTPAStatus                        = s.extSB26_JTPAStatus,
			t.extSB27_CalWorksStatus                    = s.extSB27_CalWorksStatus,
			t.extSM01_MatricGoals                       = s.extSM01_MatricGoals,
			t.extSM01_MatricGoalsPos1                   = s.extSM01_MatricGoalsPos1,
			t.extSM01_MatricGoalsPos2                   = s.extSM01_MatricGoalsPos2,
			t.extSM01_MatricGoalsPos3                   = s.extSM01_MatricGoalsPos3,
			t.extSM01_MatricGoalsPos4                   = s.extSM01_MatricGoalsPos4,
			t.extSM07_MatricOrientationServices         = s.extSM07_MatricOrientationServices,
			t.extSM08_MatricAssessmentServicesPlacement = s.extSM08_MatricAssessmentServicesPlacement,
			t.extSM09_MatricAssessmentServicesOther     = s.extSM09_MatricAssessmentServicesOther,
			t.extSM09_MatricAssessmentServicesOtherPos1 = s.extSM09_MatricAssessmentServicesOtherPos1,
			t.extSM09_MatricAssessmentServicesOtherPos2 = s.extSM09_MatricAssessmentServicesOtherPos2,
			t.extSM09_MatricAssessmentServicesOtherPos3 = s.extSM09_MatricAssessmentServicesOtherPos3,
			t.extSM12_MatricCounAdviseServices          = s.extSM12_MatricCounAdviseServices,
			t.extSM13_MatricAcademicFollowupSerives     = s.extSM13_MatricAcademicFollowupSerives,
			t.extSVO1_ProgramPlanStatus                 = s.extSVO1_ProgramPlanStatus,
			t.extSV03_EconomicallyDisadvStatus          = s.extSV03_EconomicallyDisadvStatus,
			t.extSV03_EconomicallyDisadvStatusPos1      = s.extSV03_EconomicallyDisadvStatusPos1,
			t.extSV03_EconomicallyDisadvStatusPos2      = s.extSV03_EconomicallyDisadvStatusPos2,
			t.extSV04_SingleParentStatus                = s.extSV04_SingleParentStatus,
			t.extSV05_DisplacedHomemakerStatus          = s.extSV05_DisplacedHomemakerStatus,
			t.extSV06_CoopWorkExperienceEdType          = s.extSV06_CoopWorkExperienceEdType,
			t.extSV08_TechPrepStatus                    = s.extSV08_TechPrepStatus,
			t.extSV09_MigrantWorkerStatus               = s.extSV09_MigrantWorkerStatus,
			t.extSCD1_FirstTermAttended                 = s.extSCD1_FirstTermAttended,
			t.extSCD2_LastTermAttended                  = s.extSCD2_LastTermAttended,
			t.extSCD4_LEP                               = s.extSCD4_LEP,
			t.extSCD5_AcademicallyDisadvantagedStudent  = s.extSCD5_AcademicallyDisadvantagedStudent,
			t.extSTD1_AgeAtTerm                         = s.extSTD1_AgeAtTerm,
			t.extSTD2_1stCensusCreditLoad               = s.extSTD2_1stCensusCreditLoad,
			t.extSTD3_DayEveningClassCode               = s.extSTD3_DayEveningClassCode,
			t.extSTD5_DegreeAppUnitsEarned              = s.extSTD5_DegreeAppUnitsEarned,
			t.extSTD6_DayEveningClassCode2              = s.extSTD6_DayEveningClassCode2,
			t.extSTD7_HeadCountStatus                   = s.extSTD7_HeadCountStatus,
			t.extSTD8_LocalCumGPA                       = s.extSTD8_LocalCumGPA,
			t.extSTD9_TotalCumGPA                       = s.extSTD9_TotalCumGPA,
			t.extSD01_StudentPrimaryDisability          = s.extSD01_StudentPrimaryDisability,
			t.extSD03_StudentSecondaryDisability        = s.extSD03_StudentSecondaryDisability,
			t.extSB29_MultiEthnicity                    = s.extSB29_MultiEthnicity
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				extSB09_ResidenceCode,
				extSB23_ApprenticeshipStatus,
				extSB24_TransferCenterStatus,
				extSB26_JTPAStatus,
				extSB27_CalWorksStatus,
				extSM01_MatricGoals,
				extSM01_MatricGoalsPos1,
				extSM01_MatricGoalsPos2,
				extSM01_MatricGoalsPos3,
				extSM01_MatricGoalsPos4,
				extSM07_MatricOrientationServices,
				extSM08_MatricAssessmentServicesPlacement,
				extSM09_MatricAssessmentServicesOther,
				extSM09_MatricAssessmentServicesOtherPos1,
				extSM09_MatricAssessmentServicesOtherPos2,
				extSM09_MatricAssessmentServicesOtherPos3,
				extSM12_MatricCounAdviseServices,
				extSM13_MatricAcademicFollowupSerives,
				extSVO1_ProgramPlanStatus,
				extSV03_EconomicallyDisadvStatus,
				extSV03_EconomicallyDisadvStatusPos1,
				extSV03_EconomicallyDisadvStatusPos2,
				extSV04_SingleParentStatus,
				extSV05_DisplacedHomemakerStatus,
				extSV06_CoopWorkExperienceEdType,
				extSV08_TechPrepStatus,
				extSV09_MigrantWorkerStatus,
				extSCD1_FirstTermAttended,
				extSCD2_LastTermAttended,
				extSCD4_LEP,
				extSCD5_AcademicallyDisadvantagedStudent,
				extSTD1_AgeAtTerm,
				extSTD2_1stCensusCreditLoad,
				extSTD3_DayEveningClassCode,
				extSTD5_DegreeAppUnitsEarned,
				extSTD6_DayEveningClassCode2,
				extSTD7_HeadCountStatus,
				extSTD8_LocalCumGPA,
				extSTD9_TotalCumGPA,
				extSD01_StudentPrimaryDisability,
				extSD03_StudentSecondaryDisability,
				extSB29_MultiEthnicity
			)
		VALUES
			(
				s.CollegeId,
				s.StudentComisId,
				s.StudentId,
				s.IdStatus,
				s.TermId,
				s.extSB09_ResidenceCode,
				s.extSB23_ApprenticeshipStatus,
				s.extSB24_TransferCenterStatus,
				s.extSB26_JTPAStatus,
				s.extSB27_CalWorksStatus,
				s.extSM01_MatricGoals,
				s.extSM01_MatricGoalsPos1,
				s.extSM01_MatricGoalsPos2,
				s.extSM01_MatricGoalsPos3,
				s.extSM01_MatricGoalsPos4,
				s.extSM07_MatricOrientationServices,
				s.extSM08_MatricAssessmentServicesPlacement,
				s.extSM09_MatricAssessmentServicesOther,
				s.extSM09_MatricAssessmentServicesOtherPos1,
				s.extSM09_MatricAssessmentServicesOtherPos2,
				s.extSM09_MatricAssessmentServicesOtherPos3,
				s.extSM12_MatricCounAdviseServices,
				s.extSM13_MatricAcademicFollowupSerives,
				s.extSVO1_ProgramPlanStatus,
				s.extSV03_EconomicallyDisadvStatus,
				s.extSV03_EconomicallyDisadvStatusPos1,
				s.extSV03_EconomicallyDisadvStatusPos2,
				s.extSV04_SingleParentStatus,
				s.extSV05_DisplacedHomemakerStatus,
				s.extSV06_CoopWorkExperienceEdType,
				s.extSV08_TechPrepStatus,
				s.extSV09_MigrantWorkerStatus,
				s.extSCD1_FirstTermAttended,
				s.extSCD2_LastTermAttended,
				s.extSCD4_LEP,
				s.extSCD5_AcademicallyDisadvantagedStudent,
				s.extSTD1_AgeAtTerm,
				s.extSTD2_1stCensusCreditLoad,
				s.extSTD3_DayEveningClassCode,
				s.extSTD5_DegreeAppUnitsEarned,
				s.extSTD6_DayEveningClassCode2,
				s.extSTD7_HeadCountStatus,
				s.extSTD8_LocalCumGPA,
				s.extSTD9_TotalCumGPA,
				s.extSD01_StudentPrimaryDisability,
				s.extSD03_StudentSecondaryDisability,
				s.extSB29_MultiEthnicity
			);
END;