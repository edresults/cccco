USE calpass;

GO

IF (object_id('comis.p_SelfReport_Merge') is not null)
	BEGIN
		DROP PROCEDURE comis.p_SelfReport_Merge;
	END;

GO

CREATE PROCEDURE
	comis.p_SelfReport_Merge
	(
		@StageSchemaTableName nvarchar(517),
		@CountUpdate int out,
		@CountInsert int out
	)
AS

DECLARE
	@Message nvarchar(2048),
	@Validate bit = 0,
	@Sql nvarchar(max),
	@Needle varchar(255) = char(13) + char(10) + char(9) + char(9),
	@Replace varchar(255) = char(13) + char(10),
	@ActionUpdate char(1) = 'U',
	@ActionInsert char(1) = 'I';

BEGIN
	-- Validate inputs
	SELECT
		@Validate = 1
	FROM
		sys.tables
	WHERE
		object_id = object_id(@StageSchemaTableName);
	-- process validation
	IF (@Validate = 0)
		BEGIN
			SET @Message = N'Table not found';
			THROW 70099, @Message, 1;
		END;
	-- set dynamic merge
	SET @Sql = replace(N'
		SELECT
			@CountUpdate = sum(case when action = @ActionUpdate then 1 else 0 end),
			@CountInsert = sum(case when action = @ActionInsert then 1 else 0 end)
		FROM
			(
				MERGE
					comis.SelfReport t
				USING
					' + @StageSchemaTableName + ' s
				ON
					t.InterSegmentKey = s.InterSegmentKey
				WHEN MATCHED THEN
					UPDATE SET
						completed_eleventh_grade = completed_eleventh_grade,
						grade_point_average = grade_point_average,
						highest_english_course = highest_english_course,
						highest_english_grade = highest_english_grade,
						highest_math_course_taken = highest_math_course_taken,
						highest_math_taken_grade = highest_math_taken_grade,
						highest_math_course_passed = highest_math_course_passed,
						highest_math_passed_grade = highest_math_passed_grade
				WHEN NOT MATCHED BY TARGET THEN
					INSERT
						(
							InterSegmentKey,
							completed_eleventh_grade,
							grade_point_average,
							highest_english_course,
							highest_english_grade,
							highest_math_course_taken,
							highest_math_taken_grade,
							highest_math_course_passed,
							highest_math_passed_grade
						)
					VALUES
						(
							s.InterSegmentKey,
							s.completed_eleventh_grade,
							s.grade_point_average,
							s.highest_english_course,
							s.highest_english_grade,
							s.highest_math_course_taken,
							s.highest_math_taken_grade,
							s.highest_math_course_passed,
							s.highest_math_passed_grade
						)
				OUTPUT
					left($action, 1)
			) o (action);', @Needle, @Replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@Sql,
		N'@ActionUpdate char(1),
			@ActionInsert char(1),
			@CountUpdate int out,
			@CountInsert int out',
		@ActionUpdate = @ActionUpdate,
		@ActionInsert = @ActionInsert,
		@CountUpdate = @CountUpdate out,
		@CountInsert = @CountInsert out;
END;