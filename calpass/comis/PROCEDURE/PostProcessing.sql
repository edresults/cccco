USE calpass;

GO

IF (object_id('comis.PostProcessing') is not null)
	BEGIN
		DROP PROCEDURE comis.PostProcessing;
	END;

GO

CREATE PROCEDURE
	comis.PostProcessing
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;
BEGIN
	EXECUTE comis.InterSegmentKeyEncrypt;
	EXECUTE comis.StudentProcess;
	EXECUTE mmap.StudentProcess;
	EXECUTE comis.Rebuild;
	EXECUTE mmap.CollegeCourseContentPopulate;
    EXECUTE mmap.CCTranscriptProcess;
END;

-- DROP PROC comis.InterSegmentKeyEncrypt;
-- DROP PROC comis.StudentProcess;
-- DROP PROC mmap.StudentProcess;
-- DROP PROC comis.Rebuild;
-- DROP PROC mmap.CollegeCourseContentPopulate;
-- DROP PROC mmap.CCTranscriptProcess;
-- DROP PROC mmap.PostProcessing;