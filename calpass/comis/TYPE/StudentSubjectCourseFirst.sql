USE calpass;

GO

IF (type_id('comis.StudentSubjectCourseFirst') is not null)
	BEGIN
		DROP TYPE comis.StudentSubjectCourseFirst;
	END;

GO

CREATE TYPE
	comis.StudentSubjectCourseFirst
AS TABLE
	(
		InterSegmentKey binary(64),
		SubjectCode varchar(4),
		CollegeCode char(3),
		TermCodeFull char(5),
		CourseId char(12),
		CourseTitle varchar(68),
		CourseLevelCode char(1),
		CourseLevelRank tinyint,
		SectionUnitsAttempted decimal(4,2),
		SectionGradeCode varchar(3),
		SectionGradeRank tinyint,
		TotalSubjectUnitsAttempted decimal(5,2),
		INDEX
			ixc_StudentSubjectCourseFirst
		CLUSTERED
			(
				InterSegmentKey,
				SubjectCode,
				CollegeCode,
				TermCodeFull,
				CourseLevelRank,
				SectionUnitsAttempted,
				SectionGradeRank,
				TotalSubjectUnitsAttempted
			)
	);