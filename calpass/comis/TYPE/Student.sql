USE calpass;

GO

IF (type_id('comis.Student') is not null)
	BEGIN
		DROP TYPE comis.Student;
	END;

GO

CREATE TYPE
	comis.Student
AS TABLE
	(
		CollegeCode char(3) not null,
		StudentId char(9) not null,
		InterSegmentKey binary(64) not null,
		BatchNumber int,
		INDEX
			ixc_Student__CollegeCode__StudentId
		CLUSTERED
			(
				CollegeCode,
				StudentId
			)
	);