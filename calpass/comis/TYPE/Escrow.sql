USE calpass;

GO

IF (type_id('comis.Escrow') is not null)
	BEGIN
		DROP TYPE comis.Escrow;
	END;

GO

CREATE TYPE
	comis.Escrow
AS TABLE
	(
		CollegeId char(3),
		CollegeIpedsCode char(6),
		StudentLocalId varchar(10),
		StudentSocialId varchar(10),
		IdStatus char(1),
		InterSegmentKey binary(64),
		TermId char(3),
		EscrowName sysname,
		PRIMARY KEY CLUSTERED
			(
				CollegeId,
				StudentLocalId,
				TermId,
				EscrowName
			),
		INDEX
			ix_Escrow__EscrowName
		NONCLUSTERED
			(
				EscrowName
			)
	);