USE calpass;

GO

IF (type_id('comis.College') is not null)
	BEGIN
		DROP TYPE comis.College;
	END;

GO

CREATE TYPE
	comis.College
AS TABLE
	(
		Iterator    int identity(0,1) not null,
		CollegeCode char(3)           not null,
		PRIMARY KEY CLUSTERED
			(
				Iterator
			),
		UNIQUE
			(
				CollegeCode
			)
	);