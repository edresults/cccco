USE calpass;

IF (object_id('dbo.ccsf_output', 'U') is not null)
	BEGIN
		DROP TABLE dbo.ccsf_output;
	END;

GO

CREATE TABLE
	dbo.ccsf_output
	(
		School char(14),
		LocStudentId varchar(15),
		AcYear char(4),
		GradeLevel char(2),
		gender char(1),
		ethnicity char(3),
		hispanic_ind tinyint,
		ses_ind tinyint,
		grad_ind tinyint,
		ag_ind tinyint,
		de_ce_ind tinyint,
		eap_math_ind tinyint,
		eap_engl_ind tinyint,
		fafsa_ind tinyint,
		matric_ccsf_first_term_id char(3), -- Number of CCSF students in cohort coming from SFUSD matriculating to CCSF within 1 year
		placement_college_math tinyint, -- Number placing into college level English
		placement_college_engl tinyint, -- Number placing into college level Math
		t1_enrolled_math tinyint, -- Number enrolling in Math in first term
		t1_enrolled_engl tinyint, -- Number enrolling in English in first term
		t1_enrolled_esl tinyint, -- Number enrolling in ESL in first term
		t1_units_attempted decimal(7,2), -- Number of units attempted in first term
		t1_units_earned decimal(7,2), -- Number of units earned in first term
		t1_grade_points decimal(7,2), -- Number of grade points in first term
		y1_units_attempted decimal(7,2), -- Number of units attempted within one year from first term
		y1_units_earned decimal(7,2), -- Number of units passed within one year from first term
		y1_grade_points decimal(7,2), -- Number of grade points within one year from first term
		y1_math_units_6plus tinyint, -- Number passing 6+ units in Mathematics within one year from first term
		y1_engl_units_6plus tinyint, -- Number passing 6+ units in English within one year from first term
		t1_persist_1plus tinyint, -- Number persisting from 1st Semester to subsequent Semester
		t1_persist_2plus tinyint, -- Number persisting from 1st Semester for two subsequent Semesters
		y1_passed_math_college_level tinyint, -- Number passing transfer-level Mathematics within one year from first term
		y1_passed_engl_college_level tinyint, -- Number passing transfer-level English within one year from first term
		y1_passed_esl_college_level tinyint, -- Number passing transfer-level ESL within one year from first term
		y2_passed_math_college_level tinyint, -- Number passing transfer-level Mathematics within two years from first term
		y2_passed_engl_college_level tinyint, -- Number passing transfer-level English within two years from first term
		y2_passed_esl_college_level tinyint, -- Number passing transfer-level ESL within two years from first term
		y2_survived_math_college_level tinyint, -- Number beginning below college-level in Mathematics and enroll in college level Mathematics within two years from first term
		y2_survived_engl_college_level tinyint, -- Number beginning below college-level in English and enroll in college level English within two years from first term
		y2_survived_esl_college_level tinyint, -- Number beginning below college-level in ESL and enroll in ESL within two years from first term
		y3_award_code char(1), -- Number earning post-secondary certificate or degree in third year
		y3_award_years tinyint, -- Numer of years taken to earn certificate or degree
		Derkey1 char(15),
		CONSTRAINT
			pk_ccsf_output__School__LocStudentId__AcYear__GradeLevel
		PRIMARY KEY CLUSTERED
			(
				School,
				LocStudentId,
				AcYear,
				GradeLevel
			)
	);

GO

CREATE INDEX
	ix_ccsf_output__Derkey
ON
	dbo.ccsf_output
	(
		Derkey1
	);