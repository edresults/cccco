USE calpass;

GO

IF (object_id('dbo.HSTranscriptSummaryHash') is not null)
	BEGIN
		DROP TABLE dbo.HSTranscriptSummaryHash;
	END;

GO

CREATE TABLE
	dbo.HSTranscriptSummaryHash
	(
		InterSegmentKey binary(64) not null,
		Entity varchar(25) not null,
		Attribute varchar(25) not null,
		Value decimal(4,3) not null
	);

GO

ALTER TABLE
	dbo.HSTranscriptSummaryHash
ADD CONSTRAINT
	pk_HSTranscriptSummaryHash
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey,
		Entity,
		Attribute
	);

GO

CREATE NONCLUSTERED INDEX
	ix_HSTranscriptSummaryHash__Entity
ON
	dbo.HSTranscriptSummaryHash
	(
		Entity,
		Attribute
	)
INCLUDE
	(
		Value
	);