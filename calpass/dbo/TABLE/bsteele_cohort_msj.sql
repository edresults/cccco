USE calpass;

GO

IF (object_id('calpass.dbo.bsteele_cohort_msj') is not null)
	BEGIN
		DROP TABLE dbo.bsteele_cohort_msj;
	END

GO

CREATE TABLE
	dbo.bsteele_cohort_msj
	(
		derkey1 char(15),
		year_code char(4),
		student_id char(32),
		CONSTRAINT
			pk_bsteel_cohort_msj__derkey1__year_code
		PRIMARY KEY CLUSTERED
			(
				derkey1,
				year_code
			)
	);

GO

CREATE INDEX
	ix_bsteele_cohrot_msj__student_id__year_code
ON
	dbo.bsteele_cohort_msj
	(
		student_id,
		year_code
	);

GO

INSERT INTO
	calpass.dbo.bsteele_cohort_msj
	(
		derkey1,
		year_code,
		student_id
	)
SELECT
	derkey1 = calpass.dbo.derkey1(
		a.name_first,
		a.name_last,
		a.gender,
		a.birthdate
	),
	a.year_code,
	a.student_id
FROM
	OPENROWSET(
		BULK N'\\nas-pro-dat-01\webfileuploads\FileDrop\170798\MSJ_DualEnrollment_Students.txt',
		FORMATFILE = N'\\nas-pro-dat-01\webfileuploads\FileDrop\170798\MSJ_DualEnrollment_Students.fmt'
	) a
WHERE
	not exists (
		SELECT
			'N'
		FROM
			OPENROWSET(
				BULK N'\\nas-pro-dat-01\webfileuploads\FileDrop\170798\MSJ_DualEnrollment_Students.txt',
				FORMATFILE = N'\\nas-pro-dat-01\webfileuploads\FileDrop\170798\MSJ_DualEnrollment_Students.fmt'
			) b
		GROUP BY
			calpass.dbo.derkey1(
					b.name_first,
					b.name_last,
					b.gender,
					b.birthdate
				),
			b.year_code
		HAVING
			count(b.student_id) > 1
			and calpass.dbo.derkey1(
					b.name_first,
					b.name_last,
					b.gender,
					b.birthdate
				) = calpass.dbo.derkey1(
					a.name_first,
					a.name_last,
					a.gender,
					a.birthdate
				)
			and a.year_code = b.year_code
	)