use calpass;

IF (object_id('dbo.dodonovan') is not null)
	BEGIN
		DROP TABLE dbo.dodonovan;
	END;

GO

CREATE TABLE
	dbo.dodonovan
	(
		ssid nvarchar(10) not null,
		student_id nvarchar(15),
		name_last nvarchar(50),
		name_first nvarchar(30),
		date_of_birth nchar(8),
		gender nchar(1),
		hs_cds nchar(14) not null,
		student_year int not null,
		activity_id int
	)

GO

-- ALTER TABLE
	-- dbo.dodonovan
-- ADD CONSTRAINT
	-- pk_dodonovan__ssid__hs_cds__student_year
-- PRIMARY KEY CLUSTERED
	-- (
		-- ssid,
		-- hs_cds,
		-- student_year
	-- );

CREATE CLUSTERED INDEX
	ixc_dodonovan__ssid__hs_cds__student_year
ON
	dbo.dodonovan
	(
		ssid,
		hs_cds,
		student_year
	);

GO

CREATE INDEX
	ix_dodonovan__activity_id
ON
	dbo.dodonovan
	(
		activity_id
	);