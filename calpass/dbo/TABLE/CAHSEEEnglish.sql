USE [calpass]
GO

/****** Object:  Table [dbo].[CAHSEEEnglish]    Script Date: 12/3/2015 8:10:54 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CAHSEEEnglish](
	[derkey1] [char](15) NOT NULL,
	[SchoolYear] [varchar](4) NOT NULL,
	[SchoolIdentifier] [varchar](14) NOT NULL,
	[LocalStudentID] [varchar](50) NOT NULL,
	[ELAAccomTransfer] [char](1) NULL,
	[ELAAccomDictatedMC] [char](1) NULL,
	[ELAccomWP] [char](1) NULL,
	[ELAAccomDictatedEssay] [char](1) NULL,
	[ELAAccomAssistive] [char](1) NULL,
	[ELAAccomBraille] [char](1) NULL,
	[ELAAccomLargePrint] [char](1) NULL,
	[ELAAccomExtratime] [char](1) NULL,
	[ELAAccomBreaks] [char](1) NULL,
	[ELAAccomBesttime] [char](1) NULL,
	[ELAAccomHome] [char](1) NULL,
	[ELAModDictionary] [char](2) NULL,
	[ELAModSign] [char](2) NULL,
	[ELAModOralPresentation] [char](1) NULL,
	[ELAModWPwithchecks] [char](1) NULL,
	[ELAModEssayDictated] [char](2) NULL,
	[ELAModAssistivedevice] [char](2) NULL,
	[ELATestForm] [varchar](3) NULL,
	[ELATestDate] [varchar](8) NULL,
	[ELAPassed] [char](1) NULL,
	[ELAScaleScore] [varchar](3) NULL,
	[ELAReadingWANumberCorrect] [varchar](3) NULL,
	[ELAReadingWAPercentCorrect] [varchar](3) NULL,
	[ELAReadingRCNumberCorrect] [varchar](3) NULL,
	[ELAReadingRCPercentCorrect] [varchar](3) NULL,
	[ELAReadingLANumberCorrect] [varchar](3) NULL,
	[ELAReadingLAPercentCorrect] [varchar](3) NULL,
	[ELAWritingWSNumberCorrect] [varchar](3) NULL,
	[ELAWritingWSPercentCorrect] [varchar](3) NULL,
	[ELAWritingWCNumberCorrect] [varchar](3) NULL,
	[ELAWritingWCPercentCorrect] [varchar](3) NULL,
	[ELAWritingApplicEssay1] [varchar](3) NULL,
	[ELASpecVersionAudioCD] [varchar](2) NULL,
	[ELASpecVersionLargePrint] [varchar](2) NULL,
	[ELASpecVersionBraille] [varchar](2) NULL,
	[ELDateEnrolled] [varchar](8) NULL,
	[TotalELAResponses] [varchar](3) NULL,
	[ELADirPrimeLang] [char](1) NULL,
	[ELASupBreaks] [char](1) NULL,
	[ELASepTest] [char](1) NULL,
	[ELAGlossWL] [char](1) NULL,
	[ELAScoreCode] [char](1) NULL,
	[ELAPrfrmncLvl] [varchar](1) NULL,
	[ELANumStuResponses] [varchar](2) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


