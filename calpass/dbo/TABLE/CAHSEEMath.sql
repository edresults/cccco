USE [calpass]
GO

/****** Object:  Table [dbo].[CAHSEEMath]    Script Date: 12/3/2015 8:11:19 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CAHSEEMath](
	[derkey1] [char](15) NOT NULL,
	[SchoolYear] [varchar](4) NOT NULL,
	[SchoolIdentifier] [varchar](14) NOT NULL,
	[LocalStudentID] [varchar](50) NOT NULL,
	[MathAccomTransfer] [char](1) NULL,
	[MathAccomDictatedMC] [char](1) NULL,
	[MathAccomBraille] [char](1) NULL,
	[MathAccomLargePrint] [char](1) NULL,
	[MathAccomExtratime] [char](1) NULL,
	[MathAccomBreaks] [char](1) NULL,
	[MathAccomBesttime] [char](1) NULL,
	[MathAccomHome] [char](1) NULL,
	[MathModDictionary] [char](2) NULL,
	[MathAccomSign] [char](1) NULL,
	[MathAccommOralPresentation] [char](2) NULL,
	[MathModCalculator] [char](2) NULL,
	[MathModTables] [char](2) NULL,
	[MathModManipulatives] [char](1) NULL,
	[MathModAssistivedevice] [char](2) NULL,
	[MathClassGeneral] [varchar](2) NULL,
	[MathClassPreAlgebra] [varchar](2) NULL,
	[MathClassAlgebra1] [varchar](2) NULL,
	[MathClassGeometry] [varchar](2) NULL,
	[MathClassAlgebra2] [varchar](2) NULL,
	[MathClassIntegMath1] [varchar](2) NULL,
	[MathClassIntegMath2] [varchar](2) NULL,
	[MathClassIntegMath3] [varchar](2) NULL,
	[MathClassAdvanced] [varchar](2) NULL,
	[MathTestForm] [varchar](3) NULL,
	[MathTestDate] [varchar](8) NULL,
	[MathPassed] [char](1) NULL,
	[MathScaleScore] [varchar](3) NULL,
	[MathPSNumberCorrect] [varchar](3) NULL,
	[MathPSPercentCorrect] [varchar](3) NULL,
	[MathNSNumberCorrect] [varchar](3) NULL,
	[MathNSPercentCorrect] [varchar](3) NULL,
	[MathAFNumberCorrect] [varchar](3) NULL,
	[MathAFPercentCorrect] [varchar](3) NULL,
	[MathMGNumberCorrect] [varchar](3) NULL,
	[MathMGPercentCorrect] [varchar](3) NULL,
	[MathA1NumberCorrect] [varchar](3) NULL,
	[MathA1PercentCorrect] [varchar](3) NULL,
	[MathSpecVersionAudioCD] [varchar](2) NULL,
	[MathSpecVersionLargePrint] [varchar](2) NULL,
	[MathSpecVersionBraille] [varchar](2) NULL,
	[TotalMathResponses] [varchar](3) NULL,
	[MathDirPrimeLang] [char](1) NULL,
	[MathSupBreaks] [char](1) NULL,
	[MathSepTest] [char](1) NULL,
	[MathGlossWL] [char](1) NULL,
	[MathScoreCode] [char](1) NULL,
	[MathPrfrmncLvl] [varchar](1) NULL,
	[MathNumStuResponses] [varchar](2) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


