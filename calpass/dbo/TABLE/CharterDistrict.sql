USE calpass;

GO

IF (object_id('dbo.CharterDistrict') is not null)
	BEGIN
		DROP TABLE dbo.CharterDistrict;
	END;

GO

CREATE TABLE
	dbo.CharterDistrict
	(
		CharterDistrictId tinyint identity(0,1) not null,
		Label             varchar(255)          not null
	);

GO

ALTER TABLE
	dbo.CharterDistrict
ADD CONSTRAINT
	PK_CharterDistrict
PRIMARY KEY
	(
		CharterDistrictId
	);

ALTER TABLE
	dbo.CharterDistrict
ADD CONSTRAINT
	UQ_CharterDistrict__Label
PRIMARY KEY
	(
		Label
	);

GO

INSERT
	dbo.CharterDistrict
	(
		Label
	)
VALUES
	('Alliance'),
	('Aspire'),
	('Green Dot'),
	('High Tech'),
	('Magnolia'),
	('PUC');