USE [calpass]
GO

/****** Object:  Table [dbo].[K12CTEProd]    Script Date: 12/3/2015 8:12:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[K12CTEProd](
	[derkey1] [char](15) NULL,
	[School] [char](14) NULL,
	[AcYear] [char](4) NULL,
	[LocStudentId] [char](10) NULL,
	[CSISnum] [char](25) NULL,
	[Fname] [char](30) NULL,
	[Lname] [char](40) NULL,
	[Birthdate] [char](8) NULL,
	[Gender] [char](1) NULL,
	[CTEPathwayCode] [char](3) NULL,
	[CTEPathwayCompAcYear] [char](9) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


