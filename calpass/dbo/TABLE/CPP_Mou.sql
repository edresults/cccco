USE calpass;

GO

IF (object_id('dbo.CPP_Mou') is not null)
	BEGIN
		DROP TABLE dbo.CPP_Mou;
	END;

GO

CREATE TABLE
	dbo.CPP_Mou
	(
		mouId int identity(1,1),
		organizationId int not null,
		submittedBy varchar(100) not null,
		submittedDate datetime not null,
		expirationDate datetime not null,
		notes nvarchar(max),
		restricted smallint,
		deleted smallint,
		deletedReason nvarchar(max)
	);

GO

ALTER TABLE
	dbo.CPP_Mou
ADD CONSTRAINT
	pk_CPP_MOU__MouId
PRIMARY KEY CLUSTERED
	(
		mouId
	);

ALTER TABLE
	dbo.CPP_Mou
ADD CONSTRAINT
	fk_CPP_MOU__OrganizationId
FOREIGN KEY
	(
		OrganizationId
	)
REFERENCES
	dbo.Organization
	(
		OrganizationId
	)
ON DELETE CASCADE;