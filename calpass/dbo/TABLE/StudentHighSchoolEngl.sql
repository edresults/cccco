USE calpass;

GO

IF (object_id('dbo.StudentHighSchoolEngl') is not null)
	BEGIN
		DROP TABLE dbo.StudentHighSchoolEngl;
	END;

GO

CREATE TABLE
	dbo.StudentHighSchoolEngl
	(
		InterSegmentKey binary(64) not null,
		English11 decimal(2,1),
		English12 decimal(2,1)
	);

GO

ALTER TABLE
	dbo.StudentHighSchoolEngl
ADD CONSTRAINT
	pk_StudentHighSchoolEngl__InterSegmentKey
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey
	);