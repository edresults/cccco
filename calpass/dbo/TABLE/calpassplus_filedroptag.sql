USE cms_production_calpass;

GO

IF (object_id('dbo.calpassplus_filedroptag') is not null)
	BEGIN
		DROP TABLE dbo.calpassplus_filedroptag;
	END;

GO

CREATE TABLE
	dbo.calpassplus_filedroptag
	(
		SubmissionFileId int not null,
		Attribute nvarchar(255),
		Value sql_variant,
		CreateDateTime datetime,
		ModifyDateTime datetime,
		CONSTRAINT
			pk_calpassplus_filedroptag
		PRIMARY KEY
			(
				SubmissionFileId,
				Attribute
			)
	);