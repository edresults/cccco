USE calpass;

GO

IF (object_id('calpass.dbo.K12CourseProd_AGstatus') is not null)
	BEGIN
		DROP TABLE dbo.K12CourseProd_AGstatus;
	END;

GO

CREATE TABLE
	dbo.K12CourseProd_AGstatus
	(
		code VARCHAR(2),
		description VARCHAR(255) NOT NULL,
		rank TINYINT NOT NULL,
		CONSTRAINT
			pk_K12CourseProd_AGstatus__code
		PRIMARY KEY CLUSTERED
			(
				code
			)
	);

INSERT INTO
	dbo.K12CourseProd_AGstatus
	(
		code,
		description,
		rank
	)
VALUES
	('A','History/Social Science',1),
	('B','English',1),
	('C','Mathematics',1),
	('D','Laboratory Science',1),
	('E','Foreign Language',1),
	('F','Visual and Performing Arts',1),
	('G','College Prep. Elective',3),
	('GA','History/Social Science Elective',2),
	('GB','English Elective',2),
	('GC','Mathematics Elective',2),
	('GD','Laboratory Science Elective',2),
	('GE','Foreign Language Elective',2),
	('GF','Visual and Performing Arts Elective',2),
	('GO','Other Elective',2),
	('XX','Unknown/Not Reported',86);