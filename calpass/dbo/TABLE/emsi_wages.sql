USE calpass;

IF (object_id('calpass.dbo.emsi_wages') is not null)
	BEGIN
		DROP TABLE dbo.emsi_wages;
	END;

GO

CREATE TABLE
	dbo.emsi_wages
	(
		region varchar(50) not null,
		soc char(7) not null,
		year_curr smallint not null,
		year_past smallint not null,
		year_next smallint not null,
		employment_past decimal,
		employment_curr decimal,
		change_numeric_past_to_curr decimal,
		change_percent_past_to_curr decimal,
		change_numeric_curr_to_next decimal,
		change_percent_curr_to_next decimal,
		openings_curr_to_next int,
		earnings_percent_10 decimal,
		earnings_percent_25 decimal,
		earnings_percent_50 decimal,
		earnings_percent_75 decimal
	);

GO

ALTER TABLE
	dbo.emsi_wages
ADD CONSTRAINT
	pk_emsi_wages__region__soc__year_curr
PRIMARY KEY CLUSTERED
	(
		region,
		soc,
		year_curr
	);