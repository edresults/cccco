USE calpass;

GO

IF (object_id('dbo.MemberList') is not null)
	BEGIN
		DROP TABLE dbo.MemberList;
	END;

GO

CREATE TABLE
	dbo.MemberList
	(
		OrganizationId int not null,
		OrganizationTypeId int not null,
		OrganizationName varchar(255) not null,
		AcademicYear char(9) not null,
		CountyId int not null,
		StudentCount int,
		CourseCount int,
		AwardCount int,
		MouStatus varchar(3)
	);

GO

/* constraints */
ALTER TABLE
	dbo.MemberList
ADD CONSTRAINT
	pk_MemberList__OrganizationId__AcademicYear
PRIMARY KEY CLUSTERED
	(
		OrganizationId,
		AcademicYear
	);

GO

/* indexes */
CREATE INDEX
	ix_MemberList__OrganizationTypeId__OrganizationId__AcademicYear
ON
	dbo.MemberList
	(
		OrganizationTypeId,
		OrganizationId,
		AcademicYear
	);

CREATE INDEX
	ix_MemberList__CountyId__OrganizationId__AcademicYear
ON
	dbo.MemberList
	(
		CountyId,
		OrganizationId,
		AcademicYear
	);