CREATE TABLE
    K12Audits
    (
        DistrictCode char(7)      not null,
        DistrictName varchar(90)  not null,
        TableSource  char(1)      not null,
        AcademicYear char(9)      not null,
        Records      integer          null,
        ZScore       decimal(9,7)     null,
        CONSTRAINT PK_K12Audits PRIMARY KEY CLUSTERED (DistrictCode, TableSource, AcademicYear)
    );