USE calpass;

GO

IF (object_id('calpass.dbo.ccstudentprod_termId') is not null)
	BEGIN
		DROP TABLE calpass.dbo.ccstudentprod_termId;
	END;

GO

CREATE TABLE
	calpass.dbo.ccstudentprod_termId
	(
		code CHAR(3) NOT NULL, -- 097
		year_term CHAR(5), -- 20097
		season VARCHAR(30), -- Fall
		description VARCHAR(30), -- Fall Semester
		type varchar(30), -- intercession; primary
		duration varchar(30), -- Quater; Semester
		season_rank TINYINT, -- season ranked within year; ex. annual = 0 summer = 1; fall = 2; winter = 3; spring = 4;
		year_code CHAR(4), -- ex. 0910
		year_trailing SMALLINT, -- ex. 2009-2010 == 2009
		year_leading SMALLINT, -- ex. 2009-2010 == 2010
		year_description CHAR(9), -- ex. 2009-2010
		year_term_01 char(5),
		year_term_02 char(5),
		year_term_03 char(5),
		year_term_04 char(5),
		year_term_05 char(5),
		year_term_06 char(5),
		year_term_next_primary char(5),
		start_date DATE,
		CONSTRAINT
			pk_term__code
		PRIMARY KEY CLUSTERED
			(
				code
			)
	);


CREATE INDEX
	ix_term__year_term
ON
	calpass.dbo.ccstudentprod_termId
	(
		year_term
	);

CREATE INDEX
	ix_term__year_code
ON
	calpass.dbo.ccstudentprod_termId
	(
		year_code
	);

MERGE
	calpass.dbo.ccstudentprod_termId as target
USING
	(
		SELECT
			a.code as code,
			case
				when substring(a.code, 1, 1) = '9' then '19' + a.code
				else '20' + a.code
			end as year_term,
			case
				when substring(a.code, 3, 1) in ('1', '2') then 'Winter'
				when substring(a.code, 3, 1) in ('3', '4') then 'Spring'
				when substring(a.code, 3, 1) in ('5', '6') then 'Summer'
				when substring(a.code, 3, 1) in ('7', '8') then 'Fall'
				when substring(a.code, 3, 1) = '0' then 'Annual'
			end as season,
			case
				when substring(a.code, 3, 1) = '1' then 'Winter Semester'
				when substring(a.code, 3, 1) = '2' then 'Winter Quarter'
				when substring(a.code, 3, 1) = '3' then 'Spring Semester'
				when substring(a.code, 3, 1) = '4' then 'Spring Quarter'
				when substring(a.code, 3, 1) = '5' then 'Summer Semester'
				when substring(a.code, 3, 1) = '6' then 'Summer Quarter'
				when substring(a.code, 3, 1) = '7' then 'Fall Semester'
				when substring(a.code, 3, 1) = '8' then 'Fall Quarter'
				when substring(a.code, 3, 1) = '0' then 'Annual Year'
			end as description,
			case
				when substring(a.code, 3, 1) = '1' then 'Intercession'
				when substring(a.code, 3, 1) = '2' then 'Intercession'
				when substring(a.code, 3, 1) = '3' then 'Primary'
				when substring(a.code, 3, 1) = '4' then 'Primary'
				when substring(a.code, 3, 1) = '5' then 'Intercession'
				when substring(a.code, 3, 1) = '6' then 'Intercession'
				when substring(a.code, 3, 1) = '7' then 'Primary'
				when substring(a.code, 3, 1) = '8' then 'Primary'
				when substring(a.code, 3, 1) = '0' then 'Annual'
			end as type,
			case
				when substring(a.code, 3, 1) = '1' then 'Semester'
				when substring(a.code, 3, 1) = '2' then 'Quarter'
				when substring(a.code, 3, 1) = '3' then 'Semester'
				when substring(a.code, 3, 1) = '4' then 'Quarter'
				when substring(a.code, 3, 1) = '5' then 'Semester'
				when substring(a.code, 3, 1) = '6' then 'Quarter'
				when substring(a.code, 3, 1) = '7' then 'Semester'
				when substring(a.code, 3, 1) = '8' then 'Quarter'
				when substring(a.code, 3, 1) = '0' then 'Annual'
			end as duration,
			case
				when substring(a.code, 3, 1) in ('1', '2') then 3
				when substring(a.code, 3, 1) in ('3', '4') then 4
				when substring(a.code, 3, 1) in ('5', '6') then 1
				when substring(a.code, 3, 1) in ('7', '8') then 2
				else 0
			end as season_rank,
			RIGHT(CAST(
				case
					when substring(a.code, 1, 2) = '00' and substring(a.code, 3, 1) in ('1', '2', '3', '4') then 1999
					else case
						when cast(substring(a.code, 1, 2) as int) >= 93 then cast('19' + substring(a.code, 1, 2) as int)
						else cast('20' + substring(a.code, 1, 2) as int)
					end + case
						when substring(a.code, 3, 1) in ('1', '2') then -1
						when substring(a.code, 3, 1) in ('3', '4') then -1
						when substring(a.code, 3, 1) in ('5', '6') then 0
						when substring(a.code, 3, 1) in ('7', '8') then 0
						when substring(a.code, 3, 1) = '0' then -1
					end
				end
			as VARCHAR), 2) +
			RIGHT(CAST(
				case
					when substring(a.code, 1, 2) = '00' and substring(a.code, 3, 1) in ('1', '2', '3', '4') then 1999
					else case
						when cast(substring(a.code, 1, 2) as int) >= 93 then cast('19' + substring(a.code, 1, 2) as int)
						else cast('20' + substring(a.code, 1, 2) as int)
					end + case
						when substring(a.code, 3, 1) in ('1', '2') then -1
						when substring(a.code, 3, 1) in ('3', '4') then -1
						when substring(a.code, 3, 1) in ('5', '6') then 0
						when substring(a.code, 3, 1) in ('7', '8') then 0
						when substring(a.code, 3, 1) = '0' then -1
					end
				end + 1
			as VARCHAR), 2) as year_code,
			case
				when substring(a.code, 1, 2) = '00' and substring(a.code, 3, 1) in ('1', '2', '3', '4') then 1999
				else case
					when cast(substring(a.code, 1, 2) as int) >= 93 then cast('19' + substring(a.code, 1, 2) as int)
					else cast('20' + substring(a.code, 1, 2) as int)
				end + case
					when substring(a.code, 3, 1) in ('1', '2') then -1
					when substring(a.code, 3, 1) in ('3', '4') then -1
					when substring(a.code, 3, 1) in ('5', '6') then 0
					when substring(a.code, 3, 1) in ('7', '8') then 0
					when substring(a.code, 3, 1) = '0' then -1
				end
			end as year_trailing,
			case
				when substring(a.code, 1, 2) = '00' and substring(a.code, 3, 1) in ('1', '2', '3', '4') then 1999
				else case
					when cast(substring(a.code, 1, 2) as int) >= 93 then cast('19' + substring(a.code, 1, 2) as int)
					else cast('20' + substring(a.code, 1, 2) as int)
				end + case
					when substring(a.code, 3, 1) in ('1', '2') then -1
					when substring(a.code, 3, 1) in ('3', '4') then -1
					when substring(a.code, 3, 1) in ('5', '6') then 0
					when substring(a.code, 3, 1) in ('7', '8') then 0
					when substring(a.code, 3, 1) = '0' then -1
				end
			end + 1 as year_leading,
			CAST(case
				when substring(a.code, 1, 2) = '00' and substring(a.code, 3, 1) in ('1', '2', '3', '4') then 1999
				else case
					when cast(substring(a.code, 1, 2) as int) >= 93 then cast('19' + substring(a.code, 1, 2) as int)
					else cast('20' + substring(a.code, 1, 2) as int)
				end + case
					when substring(a.code, 3, 1) in ('1', '2') then -1
					when substring(a.code, 3, 1) in ('3', '4') then -1
					when substring(a.code, 3, 1) in ('5', '6') then 0
					when substring(a.code, 3, 1) in ('7', '8') then 0
					when substring(a.code, 3, 1) = '0' then -1
				end
			end as VARCHAR) + '-' +
			CAST(case
				when substring(a.code, 1, 2) = '00' and substring(a.code, 3, 1) in ('1', '2', '3', '4') then 1999
				else case
					when cast(substring(a.code, 1, 2) as int) >= 93 then cast('19' + substring(a.code, 1, 2) as int)
					else cast('20' + substring(a.code, 1, 2) as int)
				end + case
					when substring(a.code, 3, 1) in ('1', '2') then -1
					when substring(a.code, 3, 1) in ('3', '4') then -1
					when substring(a.code, 3, 1) in ('5', '6') then 0
					when substring(a.code, 3, 1) in ('7', '8') then 0
					when substring(a.code, 3, 1) = '0' then -1
				end
			end + 1 as VARCHAR) as year_description,
			case substring(a.code, 3, 1)
				when '0' then convert(date, substring(case substring(a.code, 1, 1) when '9' then '19' + a.code else '20' + a.code end, 1, 4) + '0701', 112)
				when '5' then convert(date, substring(case substring(a.code, 1, 1) when '9' then '19' + a.code else '20' + a.code end, 1, 4) + '0601', 112)
				when '6' then convert(date, substring(case substring(a.code, 1, 1) when '9' then '19' + a.code else '20' + a.code end, 1, 4) + '0601', 112)
				when '7' then convert(date, substring(case substring(a.code, 1, 1) when '9' then '19' + a.code else '20' + a.code end, 1, 4) + '0901', 112)
				when '8' then convert(date, substring(case substring(a.code, 1, 1) when '9' then '19' + a.code else '20' + a.code end, 1, 4) + '1001', 112)
				when '1' then convert(date, substring(case substring(a.code, 1, 1) when '9' then '19' + a.code else '20' + a.code end, 1, 4) + '0101', 112)
				when '2' then convert(date, substring(case substring(a.code, 1, 1) when '9' then '19' + a.code else '20' + a.code end, 1, 4) + '0101', 112)
				when '3' then convert(date, substring(case substring(a.code, 1, 1) when '9' then '19' + a.code else '20' + a.code end, 1, 4) + '0201', 112)
				when '4' then convert(date, substring(case substring(a.code, 1, 1) when '9' then '19' + a.code else '20' + a.code end, 1, 4) + '0401', 112)
			end as start_date
		FROM
			(
				VALUES
					('965'),('966'),('967'),('968'),('971'),('972'),('973'),('974'),('970'),
					('975'),('976'),('977'),('978'),('981'),('982'),('983'),('984'),('980'),
					('985'),('986'),('987'),('988'),('991'),('992'),('993'),('994'),('990'),
					('995'),('996'),('997'),('998'),('001'),('002'),('003'),('004'),('000'),
					('005'),('006'),('007'),('008'),('011'),('012'),('013'),('014'),('010'),
					('015'),('016'),('017'),('018'),('021'),('022'),('023'),('024'),('020'),
					('025'),('026'),('027'),('028'),('031'),('032'),('033'),('034'),('030'),
					('035'),('036'),('037'),('038'),('041'),('042'),('043'),('044'),('040'),
					('045'),('046'),('047'),('048'),('051'),('052'),('053'),('054'),('050'),
					('055'),('056'),('057'),('058'),('061'),('062'),('063'),('064'),('060'),
					('065'),('066'),('067'),('068'),('071'),('072'),('073'),('074'),('070'),
					('075'),('076'),('077'),('078'),('081'),('082'),('083'),('084'),('080'),
					('085'),('086'),('087'),('088'),('091'),('092'),('093'),('094'),('090'),
					('095'),('096'),('097'),('098'),('101'),('102'),('103'),('104'),('100'),
					('105'),('106'),('107'),('108'),('111'),('112'),('113'),('114'),('110'),
					('115'),('116'),('117'),('118'),('121'),('122'),('123'),('124'),('120'),
					('125'),('126'),('127'),('128'),('131'),('132'),('133'),('134'),('130'),
					('135'),('136'),('137'),('138'),('141'),('142'),('143'),('144'),('140'),
					('145'),('146'),('147'),('148'),('151'),('152'),('153'),('154'),('150'),
					('155'),('156'),('157'),('158'),('161'),('162'),('163'),('164'),('160'),
					('165'),('166'),('167'),('168'),('171'),('172'),('173'),('174'),('170'),
					('175'),('176'),('177'),('178'),('181'),('182'),('183'),('184'),('180'),
					('185'),('186'),('187'),('188'),('191'),('192'),('193'),('194'),('190'),
					('195'),('196'),('197'),('198'),('201'),('202'),('203'),('204'),('200'),
					('205'),('206'),('207'),('208'),('211'),('212'),('213'),('214'),('210'),
					('215'),('216'),('217'),('218'),('221'),('222'),('223'),('224'),('220'),
					('225'),('226'),('227'),('228'),('231'),('232'),('233'),('234'),('230'),
					('235'),('236'),('237'),('238'),('241'),('242'),('243'),('244'),('240'),
					('245'),('246'),('247'),('248'),('251'),('252'),('253'),('254'),('250'),
					('255'),('256'),('257'),('258'),('261'),('262'),('263'),('264'),('260'),
					('265'),('266'),('267'),('268'),('271'),('272'),('273'),('274'),('270'),
					('275'),('276'),('277'),('278'),('281'),('282'),('283'),('284'),('280'),
					('285'),('286'),('287'),('288'),('291'),('292'),('293'),('294'),('290'),
					('295'),('296'),('297'),('298'),('301'),('302'),('303'),('304'),('300'),
					('305'),('306'),('307'),('308'),('311'),('312'),('313'),('314'),('310'),
					('315'),('316'),('317'),('318'),('321'),('322'),('323'),('324'),('320'),
					('325'),('326'),('327'),('328'),('331'),('332'),('333'),('334'),('330'),
					('335'),('336'),('337'),('338'),('341'),('342'),('343'),('344'),('340'),
					('345'),('346'),('347'),('348'),('351'),('352'),('353'),('354'),('350'),
					('355'),('356'),('357'),('358'),('361'),('362'),('363'),('364'),('360'),
					('365'),('366'),('367'),('368'),('371'),('372'),('373'),('374'),('370'),
					('375'),('376'),('377'),('378'),('381'),('382'),('383'),('384'),('380'),
					('385'),('386'),('387'),('388'),('391'),('392'),('393'),('394'),('390'),
					('395'),('396'),('397'),('398'),('401'),('402'),('403'),('404'),('400'),
					('405'),('406'),('407'),('408'),('411'),('412'),('413'),('414'),('410'),
					('415'),('416'),('417'),('418'),('421'),('422'),('423'),('424'),('420'),
					('425'),('426'),('427'),('428'),('431'),('432'),('433'),('434'),('430'),
					('435'),('436'),('437'),('438'),('441'),('442'),('443'),('444'),('440'),
					('445'),('446'),('447'),('448'),('451'),('452'),('453'),('454'),('450')
			) a (code)
	) as source
ON
	(
		source.code = target.code
	)
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			code,
			year_term,
			season,
			type,
			duration,
			description,
			season_rank,
			year_code,
			year_trailing,
			year_leading,
			year_description,
			start_date
		)
	VALUES
		(
			source.code,
			source.year_term,
			source.season,
			source.type,
			source.duration,
			source.description,
			source.season_rank,
			source.year_code,
			source.year_trailing,
			source.year_leading,
			source.year_description,
			source.start_date
		)
WHEN MATCHED THEN
	UPDATE SET
			target.year_term = source.year_term,
			target.season = source.season,
			target.type = source.type,
			target.duration = source.duration,
			target.description = source.description,
			target.season_rank = source.season_rank,
			target.year_code = source.year_code,
			target.year_trailing = source.year_trailing,
			target.year_leading = source.year_leading,
			target.year_description = source.year_description,
			target.start_date = source.start_date;

UPDATE
	t
SET
	t.year_term_01 = s.year_term_01
FROM
	calpass.dbo.ccstudentprod_termId t
	inner join
	(
		SELECT
			a.code,
			max(b.year_term) as year_term_01
		FROM
			calpass.dbo.ccstudentprod_termId a
			inner join
			calpass.dbo.ccstudentprod_termId b
				on a.year_trailing = b.year_trailing - 1
				and a.duration = b.duration
				and b.season_rank < a.season_rank
		GROUP BY
			a.code
	) s
		on t.code = s.code;

UPDATE
	t
SET
	t.year_term_02 = s.year_term_02
FROM
	calpass.dbo.ccstudentprod_termId t
	inner join
	(
		SELECT
			a.code,
			max(b.year_term) as year_term_02
		FROM
			calpass.dbo.ccstudentprod_termId a
			inner join
			calpass.dbo.ccstudentprod_termId b
				on a.year_trailing = b.year_trailing - 2
				and a.duration = b.duration
				and b.season_rank < a.season_rank
		GROUP BY
			a.code
	) s
		on t.code = s.code;

UPDATE
	t
SET
	t.year_term_03 = s.year_term_03
FROM
	calpass.dbo.ccstudentprod_termId t
	inner join
	(
		SELECT
			a.code,
			max(b.year_term) as year_term_03
		FROM
			calpass.dbo.ccstudentprod_termId a
			inner join
			calpass.dbo.ccstudentprod_termId b
				on a.year_trailing = b.year_trailing - 3
				and a.duration = b.duration
				and b.season_rank < a.season_rank
		GROUP BY
			a.code
	) s
		on t.code = s.code;

UPDATE
	t
SET
	t.year_term_04 = s.year_term_04
FROM
	calpass.dbo.ccstudentprod_termId t
	inner join
	(
		SELECT
			a.code,
			max(b.year_term) as year_term_04
		FROM
			calpass.dbo.ccstudentprod_termId a
			inner join
			calpass.dbo.ccstudentprod_termId b
				on a.year_trailing = b.year_trailing - 4
				and a.duration = b.duration
				and b.season_rank < a.season_rank
		GROUP BY
			a.code
	) s
		on t.code = s.code;

UPDATE
	t
SET
	t.year_term_05 = s.year_term_05
FROM
	calpass.dbo.ccstudentprod_termId t
	inner join
	(
		SELECT
			a.code,
			max(b.year_term) as year_term_05
		FROM
			calpass.dbo.ccstudentprod_termId a
			inner join
			calpass.dbo.ccstudentprod_termId b
				on a.year_trailing = b.year_trailing - 5
				and a.duration = b.duration
				and b.season_rank < a.season_rank
		GROUP BY
			a.code
	) s
		on t.code = s.code;

UPDATE
	t
SET
	t.year_term_06 = s.year_term_06
FROM
	calpass.dbo.ccstudentprod_termId t
	inner join
	(
		SELECT
			a.code,
			max(b.year_term) as year_term_06
		FROM
			calpass.dbo.ccstudentprod_termId a
			inner join
			calpass.dbo.ccstudentprod_termId b
				on a.year_trailing = b.year_trailing - 6
				and a.duration = b.duration
				and b.season_rank < a.season_rank
		GROUP BY
			a.code
	) s
		on t.code = s.code;

UPDATE
	t
SET
	t.year_term_next_primary = s.year_term_next_primary
FROM
	calpass.dbo.ccstudentprod_termId t
	inner join
	(
		SELECT
			a.code,
			year_term_next_primary = min(b.year_term)
		FROM
			calpass.dbo.ccstudentprod_termId a
			inner join
			calpass.dbo.ccstudentprod_termId b
				on a.year_term < b.year_term
				and b.year_trailing <= a.year_trailing + 1
				and a.duration = b.duration
		WHERE
			b.type = 'Primary'
		GROUP BY
			a.code
	) s
		on t.code = s.code;