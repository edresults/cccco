USE calpass;

GO

IF (object_id('dbo.GoldenRecord') is not null)
	BEGIN
		DROP TABLE dbo.GoldenRecord;
	END;

GO

CREATE TABLE
	dbo.GoldenRecord
	(
		InterSegmentKey                             binary(64)  not null,
		FirstCollegeCode                            char(3)         null,
		FirstYearTermCode                           char(5)         null,
		FinalCollegeCode                            char(5)         null,
		FinalYearTermCode                           char(5)         null,
		GenderCode                                  char(1)         null,
		Birthdate                                   char(8)         null,
		EthnicityCode                               char(1)         null,
		IsEnglishLearner                            bit             null,
		GoalCode                                    char(1)         null,
		K12GraduationYear                           char(5)         null,
		EnglishFirstYearTermCode                    char(5)         null,
		EnglishFirstRemedialYearTermCode            char(5)         null,
		EnglishFirstCollegeYearTermCode             char(5)         null,
		EnglishFirstCollegeCompleteYearTermCode     char(5)         null,
		MathematicsFirstYearTermCode                char(5)         null,
		MathematicsFirstRemedialYearTermCode        char(5)         null,
		MathematicsFirstCollegeYearTermCode         char(5)         null,
		MathematicsFirstCollegeCompleteYearTermCode char(5)         null,
		CreditAttemptedTotal                        decimal(6,2)    null,
		CreditEarnedTotal                           decimal(6,2)    null,
		QualityPointsTotal                          decimal(7,2)    null,
		CumulativeGradePointAverage                 decimal(4,3)    null,
		IsBogRecipient                              bit             null,
		CertificateYearTermCode                     char(5)         null,
		DegreeYearTermCode                          char(5)         null,
		FirstAwardSubdiscipline                     char(6)         null,
		TransferYearTermCode                        char(5)         null,
		WageExitPrior2Quarter                       float           null,
		WageExit                                    float           null,
		WageExitAfter2Quarter                       float           null,
		WageExitAfter4Quarter                       float           null,
		WageExitAfter8Quarter                       float           null,
		FirstPersistedYearTermCode                  char(5)         null,
		IsPeristedAny                               bit             null,
		IsApprentice                                bit             null
	);

GO

ALTER TABLE
	dbo.GoldenRecord
ADD CONSTRAINT
	PK_GoldenRecord
PRIMARY KEY
	(
		InterSegmentKey
	);