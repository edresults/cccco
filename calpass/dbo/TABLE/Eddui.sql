USE calpass;

GO

IF (object_id('dbo.Eddui') is not null)
	BEGIN
		DROP TABLE dbo.Eddui;
	END;

GO

CREATE TABLE
	dbo.Eddui
	(
		StudentIdSsnEnc binary(64) not null,
		YearId          smallint not null,
		QtrId           tinyint not null,
		Wages           int,
		Naics           char(6)
	);

GO

CREATE CLUSTERED INDEX
	IC_Eddui
ON
	dbo.Eddui
	(
		StudentIdSsnEnc,
		YearId,
		QtrId,
		Naics
	)

-- ALTER TABLE
-- 	dbo.Eddui
-- ADD CONSTRAINT
-- 	pk_Eddui
-- PRIMARY KEY CLUSTERED
-- 	(
-- 		StudentIdSsnEnc,
-- 		YearId,
-- 		QtrId,
-- 		Naics
-- 	);