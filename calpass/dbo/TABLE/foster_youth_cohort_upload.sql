USE calpass;

IF (object_id('calpass.dbo.foster_youth_cohort_upload') is not null)
	BEGIN
		DROP TABLE calpass.dbo.foster_youth_cohort_upload;
	END;

GO

CREATE TABLE
	calpass.dbo.foster_youth_cohort_upload
	(
		organization_code VARCHAR(50) NOT NULL, -- comis_ipeds ; CALPADS CDS_code
		student_id VARCHAR(9) NOT NULL, -- COMIS SB00; CALPADS CSISNum
		time_code VARCHAR(4) NOT NULL, -- COMIS GI03; CALPADS academic_year (0102)
		program_name NVARCHAR(255) NOT NULL, -- User Defined
		name_first NVARCHAR(255) NOT NULL, -- COMIS SB31; CALPADS FName
		name_last NVARCHAR(255) NOT NULL, -- COMIS SB32; CALPADS LName
		birthdate DATE NOT NULL, -- COMIS SB03; CALPADS birthdate
		gender CHAR(1) NOT NULL, -- COMIS SB04 ; CALPADS gender
		derkey1 AS
			case UPPER(SUBSTRING(CAST(REPLACE(REPLACE(name_first, ' ', ''), CHAR(39), '') + '$$$' AS CHAR(3)), 1, 1))
				when '$' then 'R'
				when 'A' then '3'
				when 'B' then 'S'
				when 'C' then 'I'
				when 'D' then '#'
				when 'E' then '4'
				when 'F' then 'T'
				when 'G' then 'J'
				when 'H' then '_'
				when 'I' then '5'
				when 'J' then 'U'
				when 'K' then 'K'
				when 'L' then 'A'
				when 'M' then '6'
				when 'N' then 'V'
				when 'O' then 'L'
				when 'P' then 'B'
				when 'Q' then '7'
				when 'R' then 'W'
				when 'S' then 'M'
				when 'T' then 'C'
				when 'U' then '8'
				when 'V' then 'X'
				when 'W' then 'N'
				when 'X' then 'D'
				when 'Y' then '9'
				when 'Z' then 'Y'
			end +
			case UPPER(SUBSTRING(CAST(REPLACE(REPLACE(name_first, ' ', ''), CHAR(39), '') + '$$$' AS CHAR(3)), 2, 1))
				when '$' then 'R'
				when 'A' then '3'
				when 'B' then 'S'
				when 'C' then 'I'
				when 'D' then '#'
				when 'E' then '4'
				when 'F' then 'T'
				when 'G' then 'J'
				when 'H' then '_'
				when 'I' then '5'
				when 'J' then 'U'
				when 'K' then 'K'
				when 'L' then 'A'
				when 'M' then '6'
				when 'N' then 'V'
				when 'O' then 'L'
				when 'P' then 'B'
				when 'Q' then '7'
				when 'R' then 'W'
				when 'S' then 'M'
				when 'T' then 'C'
				when 'U' then '8'
				when 'V' then 'X'
				when 'W' then 'N'
				when 'X' then 'D'
				when 'Y' then '9'
				when 'Z' then 'Y'
			end +
			case UPPER(SUBSTRING(CAST(REPLACE(REPLACE(name_first, ' ', ''), CHAR(39), '') + '$$$' AS CHAR(3)), 3, 1))
				when '$' then 'R'
				when 'A' then '3'
				when 'B' then 'S'
				when 'C' then 'I'
				when 'D' then '#'
				when 'E' then '4'
				when 'F' then 'T'
				when 'G' then 'J'
				when 'H' then '_'
				when 'I' then '5'
				when 'J' then 'U'
				when 'K' then 'K'
				when 'L' then 'A'
				when 'M' then '6'
				when 'N' then 'V'
				when 'O' then 'L'
				when 'P' then 'B'
				when 'Q' then '7'
				when 'R' then 'W'
				when 'S' then 'M'
				when 'T' then 'C'
				when 'U' then '8'
				when 'V' then 'X'
				when 'W' then 'N'
				when 'X' then 'D'
				when 'Y' then '9'
				when 'Z' then 'Y'
			end +
			case UPPER(SUBSTRING(CAST(REPLACE(REPLACE(name_last, ' ', ''), CHAR(39), '') + '$$$' AS CHAR(3)), 1, 1))
				when '$' then 'R'
				when 'A' then '3'
				when 'B' then 'S'
				when 'C' then 'I'
				when 'D' then '#'
				when 'E' then '4'
				when 'F' then 'T'
				when 'G' then 'J'
				when 'H' then '_'
				when 'I' then '5'
				when 'J' then 'U'
				when 'K' then 'K'
				when 'L' then 'A'
				when 'M' then '6'
				when 'N' then 'V'
				when 'O' then 'L'
				when 'P' then 'B'
				when 'Q' then '7'
				when 'R' then 'W'
				when 'S' then 'M'
				when 'T' then 'C'
				when 'U' then '8'
				when 'V' then 'X'
				when 'W' then 'N'
				when 'X' then 'D'
				when 'Y' then '9'
				when 'Z' then 'Y'
			end +
			case UPPER(SUBSTRING(CAST(REPLACE(REPLACE(name_last, ' ', ''), CHAR(39), '') + '$$$' AS CHAR(3)), 2, 1))
				when '$' then 'R'
				when 'A' then '3'
				when 'B' then 'S'
				when 'C' then 'I'
				when 'D' then '#'
				when 'E' then '4'
				when 'F' then 'T'
				when 'G' then 'J'
				when 'H' then '_'
				when 'I' then '5'
				when 'J' then 'U'
				when 'K' then 'K'
				when 'L' then 'A'
				when 'M' then '6'
				when 'N' then 'V'
				when 'O' then 'L'
				when 'P' then 'B'
				when 'Q' then '7'
				when 'R' then 'W'
				when 'S' then 'M'
				when 'T' then 'C'
				when 'U' then '8'
				when 'V' then 'X'
				when 'W' then 'N'
				when 'X' then 'D'
				when 'Y' then '9'
				when 'Z' then 'Y'
			end +
			case UPPER(SUBSTRING(CAST(REPLACE(REPLACE(name_last, ' ', ''), CHAR(39), '') + '$$$' AS CHAR(3)), 3, 1))
				when '$' then 'R'
				when 'A' then '3'
				when 'B' then 'S'
				when 'C' then 'I'
				when 'D' then '#'
				when 'E' then '4'
				when 'F' then 'T'
				when 'G' then 'J'
				when 'H' then '_'
				when 'I' then '5'
				when 'J' then 'U'
				when 'K' then 'K'
				when 'L' then 'A'
				when 'M' then '6'
				when 'N' then 'V'
				when 'O' then 'L'
				when 'P' then 'B'
				when 'Q' then '7'
				when 'R' then 'W'
				when 'S' then 'M'
				when 'T' then 'C'
				when 'U' then '8'
				when 'V' then 'X'
				when 'W' then 'N'
				when 'X' then 'D'
				when 'Y' then '9'
				when 'Z' then 'Y'
			end +
			case SUBSTRING(CONVERT(VARCHAR(8), birthdate, 112), 1, 1)
				when '1' then 'E'
				when '2' then '0'
				when '3' then 'Z'
				when '4' then 'P'
				when '5' then 'F'
				when '6' then '@'
				when '7' then '1'
				when '8' then 'Q'
				when '9' then 'G'
				when '0' then 'O'
			end + 
			case SUBSTRING(CONVERT(VARCHAR(8), birthdate, 112), 2, 1)
				when '1' then 'E'
				when '2' then '0'
				when '3' then 'Z'
				when '4' then 'P'
				when '5' then 'F'
				when '6' then '@'
				when '7' then '1'
				when '8' then 'Q'
				when '9' then 'G'
				when '0' then 'O'
			end + 
			case SUBSTRING(CONVERT(VARCHAR(8), birthdate, 112), 3, 1)
				when '1' then 'E'
				when '2' then '0'
				when '3' then 'Z'
				when '4' then 'P'
				when '5' then 'F'
				when '6' then '@'
				when '7' then '1'
				when '8' then 'Q'
				when '9' then 'G'
				when '0' then 'O'
			end + 
			case SUBSTRING(CONVERT(VARCHAR(8), birthdate, 112), 4, 1)
				when '1' then 'E'
				when '2' then '0'
				when '3' then 'Z'
				when '4' then 'P'
				when '5' then 'F'
				when '6' then '@'
				when '7' then '1'
				when '8' then 'Q'
				when '9' then 'G'
				when '0' then 'O'
			end + 
			case SUBSTRING(CONVERT(VARCHAR(8), birthdate, 112), 5, 1)
				when '1' then 'E'
				when '2' then '0'
				when '3' then 'Z'
				when '4' then 'P'
				when '5' then 'F'
				when '6' then '@'
				when '7' then '1'
				when '8' then 'Q'
				when '9' then 'G'
				when '0' then 'O'
			end + 
			case SUBSTRING(CONVERT(VARCHAR(8), birthdate, 112), 6, 1)
				when '1' then 'E'
				when '2' then '0'
				when '3' then 'Z'
				when '4' then 'P'
				when '5' then 'F'
				when '6' then '@'
				when '7' then '1'
				when '8' then 'Q'
				when '9' then 'G'
				when '0' then 'O'
			end + 
			case SUBSTRING(CONVERT(VARCHAR(8), birthdate, 112), 7, 1)
				when '1' then 'E'
				when '2' then '0'
				when '3' then 'Z'
				when '4' then 'P'
				when '5' then 'F'
				when '6' then '@'
				when '7' then '1'
				when '8' then 'Q'
				when '9' then 'G'
				when '0' then 'O'
			end + 
			case SUBSTRING(CONVERT(VARCHAR(8), birthdate, 112), 8, 1)
				when '1' then 'E'
				when '2' then '0'
				when '3' then 'Z'
				when '4' then 'P'
				when '5' then 'F'
				when '6' then '@'
				when '7' then '1'
				when '8' then 'Q'
				when '9' then 'G'
				when '0' then 'O'
			end +
			case gender
				when 'F' then 'T'
				when 'M' then '6'
				when 'X' then 'D'
				when ' ' then '2'
				else '8'
			end PERSISTED
	);

ALTER TABLE
	calpass.dbo.foster_youth_cohort_upload
ADD CONSTRAINT
	pk_foster_youth_cohort_upload__organization_code__student_id__time_code__program_name
PRIMARY KEY CLUSTERED
	(
		organization_code,
		student_id,
		time_code,
		program_name
	);

ALTER TABLE
	calpass.dbo.foster_youth_cohort_upload
ADD CONSTRAINT
	fk_foster_youth_cohort_upload__organization_code
FOREIGN KEY
	(
		organization_code
	)
REFERENCES
	calpass.dbo.organization
	(
		organizationCode
	);

ALTER TABLE
	calpass.dbo.foster_youth_cohort_upload
ADD CONSTRAINT
	fk_foster_youth_cohort_upload__gender
FOREIGN KEY
	(
		gender
	)
REFERENCES
	calpass.dbo.fk_gender
	(
		code
	);

ALTER TABLE
	calpass.dbo.foster_youth_cohort_upload
ADD CONSTRAINT
	fk_foster_youth_cohort_upload__time_code
FOREIGN KEY
	(
		time_code
	)
REFERENCES
	calpass.dbo.fk_time_code
	(
		code
	);