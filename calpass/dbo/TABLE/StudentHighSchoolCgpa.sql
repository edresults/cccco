USE calpass;

GO

IF (object_id('dbo.StudentHighSchoolCgpa') is not null)
	BEGIN
		DROP TABLE dbo.StudentHighSchoolCgpa;
	END;

GO

CREATE TABLE
	dbo.StudentHighSchoolCgpa
	(
		InterSegmentKey binary(64) not null,
		CurrentGradeLevelCode char(2) not null,
		CumulativeGradePointAverage decimal(3,2) not null
	);

GO

ALTER TABLE
	dbo.StudentHighSchoolCgpa
ADD CONSTRAINT
	pk_StudentHighSchoolCpga
PRIMARY KEY
	(
		InterSegmentKey
	);