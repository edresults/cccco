USE calpass;

GO

IF (object_id('calpass.dbo.bsteele_cohort_cst', 'U') is not null)
	BEGIN
		DROP TABLE calpass.dbo.bsteele_cohort_cst;
	END;

GO

CREATE TABLE
	calpass.dbo.bsteele_cohort_cst
	(
		Derkey1 char(15),
		math_eap_ind bit,
		math_subject tinyint,
		math_scaled_score smallint,
		math_cluster_1 smallint,
		math_cluster_2 smallint,
		math_cluster_3 smallint,
		math_cluster_4 smallint,
		math_cluster_5 smallint,
		math_cluster_6 smallint,
		engl_eap_ind bit,
		engl_scaled_score smallint,
		engl_cluster_1 smallint,
		engl_cluster_2 smallint,
		engl_cluster_3 smallint,
		engl_cluster_4 smallint,
		engl_cluster_5 smallint,
		engl_cluster_6 smallint,
		CONSTRAINT
			pk_bsteel_cohort_cst__Derkey1
		PRIMARY KEY CLUSTERED
			(
				Derkey1
			)
	);