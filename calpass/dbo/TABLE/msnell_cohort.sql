USE calpass;

GO

IF (object_id('dbo.msnell_cohort') is not null)
	BEGIN
		DROP TABLE dbo.msnell_cohort;
	END;

GO

CREATE TABLE
	dbo.msnell_cohort
	(
		derkey1 char(15),
		hs_all_grades_ind bit,
		hs_graduation_year_description char(9),
		cc_first_year_description char(9),
		hs_09_math_highest_rank tinyint,
		hs_10_math_highest_rank tinyint,
		hs_11_math_highest_rank tinyint,
		hs_12_math_highest_rank tinyint,
		hs_up11_math_highest_rank tinyint,
		hs_up12_math_highest_rank tinyint
	);