USE calpass;

GO

IF (object_id('calpass.dbo.bsteele_cohort_cc', 'U') is not null)
	BEGIN
		DROP TABLE calpass.dbo.bsteele_cohort_cc;
	END;

GO

CREATE TABLE
	calpass.dbo.bsteele_cohort_cc
	(
		derkey1 char(15),
		/* organization */
		organization_comis_code char(3),
		organization_ipeds_code char(6),
		organization_term_code char(3),
		/* student */
		student_gender char(1),
		student_ethnicity char(1),
		student_citizenship char(1),
		student_zip_code varchar(5),
		student_education_status char(5),
		student_high_school varchar(6),
		student_goal char(1),
		student_enroll_status char(1),
		student_academic_standing char(1),
		student_academic_level char(1),
		student_dsps_code char(1),
		student_eops_code char(1),
		student_bogg_code char(1),
		student_pell_code char(1),
		student_financial_aid_code char(1),
		student_major char(6),
		/* subject entry */
		student_first_math_title varchar(68),
		student_first_math_grade varchar(3),
		student_first_engl_title varchar(68),
		student_first_engl_grade varchar(3),
		/* milestones */
		student_first_year_units_attempted decimal(6,2),
		student_first_year_units_achieved decimal(6,2),
		student_first_year_grade_points decimal(6,2),
		student_first_year_cgpa decimal(4,3),
		/* course */
		course_id varchar(12),
		course_title varchar(68),
		course_credit_status char(1),
		course_top_code char(6),
		course_transfer_status char(1),
		course_units_max decimal(5,2),
		course_bs_status char(1),
		course_sam_code char(1),
		course_classification_code char(1),
		course_college_level char(1),
		/* section */
		section_id varchar(6),
		section_units_attempted decimal(5,2),
		section_units_achieved decimal(5,2),
		section_grade varchar(3),
		section_grade_points decimal(2,1),
		/* Mt. San Jacinto Dual Enrollment Indicator */
		student_msjc_de_ind tinyint,
		/* Awards */
		student_award_code char(1),
		CONSTRAINT
			pk_bsteel_cohort_cc__derkey1__college_id__term_id__course_id__section_id
		PRIMARY KEY CLUSTERED
			(
				derkey1,
				organization_ipeds_code,
				organization_term_code,
				course_id,
				section_id
			)
	);