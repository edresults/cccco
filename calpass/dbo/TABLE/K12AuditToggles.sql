CREATE TABLE
    K12AuditToggles
    (
        YearOverYear    decimal(3,2) not null,
        ZScoreThreshold decimal(3,2) not null,
        CONSTRAINT CK_K12AuditToggles_YearOverYear CHECK ( YearOverYear > 0.00 and YearOverYear <= 0.25 ),
        CONSTRAINT CK_K12AuditToggles_ZScoreThreshold CHECK ( ZScoreThreshold > 0.00 and ZScoreThreshold <= 3.00 )
    );

GO

INSERT INTO
    K12AuditToggles
    (
        YearOverYear,
        ZScoreThreshold
    )
VALUES
    (
        0.15,
        1.50
    );