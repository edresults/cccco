USE [calpass]
GO

/****** Object:  Table [dbo].[K12AwardProd]    Script Date: 12/3/2015 8:11:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[K12AwardProd](
	[derkey1] [char](15) NULL,
	[School] [char](14) NULL,
	[AcYear] [char](4) NULL,
	[LocStudentId] [char](15) NULL,
	[AwardType] [char](3) NOT NULL,
	[AwardDate] [char](8) NOT NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


