USE calpass;

GO

IF (object_id('dbo.StudentHighSchoolMath') is not null)
	BEGIN
		DROP TABLE dbo.StudentHighSchoolMath;
	END;

GO

CREATE TABLE
	dbo.StudentHighSchoolMath
	(
		InterSegmentKey binary(64) not null,
		Arithmetic decimal(2,1),
		PreAlgebra decimal(2,1),
		AlgebraI decimal(2,1),
		Geometry decimal(2,1),
		AlgebraII decimal(2,1),
		Statistics decimal(2,1),
		Trigonometry decimal(2,1),
		PreCalculus decimal(2,1),
		CalculusI decimal(2,1),
		CalculusII decimal(2,1)
	);

GO

ALTER TABLE
	dbo.StudentHighSchoolMath
ADD CONSTRAINT
	pk_StudentHighSchoolMath__InterSegmentKey
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey
	);