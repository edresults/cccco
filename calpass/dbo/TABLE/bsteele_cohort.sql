USE calpass;

IF (object_id('dbo.bsteele_cohort', 'U') IS NOT NULL)
	BEGIN
		DROP TABLE dbo.bsteele_cohort;
	END;

GO

CREATE TABLE
	dbo.bsteele_cohort
	(
		derkey1 CHAR(15),
		grade_level_code CHAR(2),
		year_code CHAR(4),
		CONSTRAINT
			pk_bsteele_cohort__derkey1__grade_level_code__year_code
		PRIMARY KEY CLUSTERED
			(
				derkey1,
				grade_level_code,
				year_code
			)
	);