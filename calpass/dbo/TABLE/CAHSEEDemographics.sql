USE [calpass]
GO

/****** Object:  Table [dbo].[CAHSEEDemographics]    Script Date: 12/3/2015 8:09:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CAHSEEDemographics](
	[derkey1] [char](15) NOT NULL,
	[SchoolYear] [varchar](4) NULL,
	[SchoolIdentifier] [varchar](14) NULL,
	[LocalStudentID] [varchar](50) NULL,
	[StatewideStudentIdentifier] [varchar](50) NULL,
	[Fname] [varchar](3) NULL,
	[Lname] [varchar](3) NULL,
	[Grade] [varchar](2) NULL,
	[CharterNumber] [varchar](4) NULL,
	[BirthDate] [varchar](50) NULL,
	[Gender] [varchar](1) NULL,
	[EthnicityPrimary] [varchar](3) NULL,
	[EthnSecAmerIndian] [varchar](1) NULL,
	[EthnSecFilipino] [varchar](1) NULL,
	[EthnSecHisp] [varchar](1) NULL,
	[EthnSecAfrAmer] [varchar](1) NULL,
	[EthnSecWhite] [varchar](1) NULL,
	[AsianChinese] [varchar](1) NULL,
	[AsianJapanese] [varchar](1) NULL,
	[AsianKorean] [varchar](1) NULL,
	[AsianVietnamese] [varchar](1) NULL,
	[AsianIndian] [varchar](1) NULL,
	[AsianLaotian] [varchar](1) NULL,
	[AsianCambodian] [varchar](1) NULL,
	[AsianOther] [varchar](1) NULL,
	[PacIslanderHawaiian] [varchar](1) NULL,
	[PacIslanderGuamanian] [varchar](1) NULL,
	[PacIslanderSamoan] [varchar](1) NULL,
	[PacIslanderTahitian] [varchar](1) NULL,
	[PacificIslanderOther] [varchar](1) NULL,
	[StudentsPrimaryLanguage] [varchar](2) NULL,
	[ParentEducationLevel] [varchar](2) NULL,
	[StudentsEnglishProficiency] [varchar](4) NULL,
	[ProgPartTitleI] [varchar](1) NULL,
	[ProgPartMigrant] [varchar](1) NULL,
	[ProgPartIndian] [varchar](1) NULL,
	[ProgPartGifted] [varchar](1) NULL,
	[ProgPartELD] [varchar](1) NULL,
	[NSLP] [varchar](1) NULL,
	[SpecialEdDabltyCde] [varchar](3) NULL,
	[SpEDNPS] [varchar](1) NULL,
	[SEC504] [varchar](1) NULL,
	[IEP] [varchar](1) NULL,
	[ParentCityStateZip] [varchar](41) NULL,
	[RFEPDateReclassified] [varchar](8) NULL,
	[RFEPStudent] [varchar](1) NULL,
	[ASAMSchool] [varchar](1) NULL,
	[CAPAAssessed] [varchar](1) NULL,
	[ResidenceCountyClientCode] [varchar](2) NULL,
	[Grade12Plus] [varchar](1) NULL,
	[HispanicLatinoInd] [varchar](1) NULL,
	[EthnHmong] [varchar](1) NULL,
	[ETSStudentNbr] [varchar](15) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


