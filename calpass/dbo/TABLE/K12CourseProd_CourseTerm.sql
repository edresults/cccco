USE calpass;

GO

IF (object_id('dbo.K12CourseProd_CourseTerm') is not null)
	BEGIN
		DROP TABLE dbo.K12CourseProd_CourseTerm;
	END;

GO

CREATE TABLE
	dbo.K12CourseProd_CourseTerm
	(
		code VARCHAR(3),
		type VARCHAR(3) NULL,
		season VARCHAR(6) NOT NULL,
		description VARCHAR(255) NOT NULL,
		rank TINYINT NOT NULL,
		season_rank TINYINT NOT NULL,
		CONSTRAINT
			pk_K12CourseProd_CourseTerm__code
		PRIMARY KEY CLUSTERED
			(
				code
			)
	);

CREATE INDEX
	ix_K12CourseProd_CourseTerm__rank
ON
	dbo.K12CourseProd_CourseTerm
	(
		rank
	);

CREATE INDEX
	ix_K12CourseProd_CourseTerm__season_rank
ON
	dbo.K12CourseProd_CourseTerm
	(
		season_rank
	);

CREATE INDEX
	ix_K12CourseProd_CourseTerm__season
ON
	dbo.K12CourseProd_CourseTerm
	(
		season
	);

GO

INSERT INTO
	dbo.K12CourseProd_CourseTerm
	(
		code,
		type,
		season,
		description,
		rank,
		season_rank
	)
VALUES
	('CQ1','CQ','Summer','College Quarter 1',20,1),
	('CQ2','CQ','Fall','College Quarter 2',40,2),
	('CQ3','CQ','Winter','College Quarter 3',60,3),
	('CQ4','CQ','Spring','College Quarter 4',80,4),
	('CS1','CS','Fall','College Semester 1',45,2),
	('CS2','CS','Spring','College Semester 2',80,4),
	('FLS','FLS','Full','Full Session (Year)',55,2),
	('FY','FY','Full','Full Session (Year)',55,2),
	('FS1','FS','Summer','Full Session Summer 1',93,1),
	('FS2','FS','Summer','Full Session Summer 2',96,1),
	('FS3','FS','Summer','Full Session Summer 3',99,1),
	('HX1','HX','Fall','Hexmester 1',15,2),
	('HX2','HX','Fall','Hexmester 2',35,2),
	('HX3','HX','Winter','Hexmester 3',45,3),
	('HX4','HX','Spring','Hexmester 4',65,3),
	('HX5','HX','Spring','Hexmester 5',75,4),
	('HX6','HX','Summer','Hexmester 6',90,4),
	('H1','HX','Fall','Hexmester 1',15,2),
	('H2','HX','Fall','Hexmester 2',30,2),
	('H3','HX','Winter','Hexmester 3',45,3),
	('H4','HX','Spring','Hexmester 4',60,3),
	('H5','HX','Spring','Hexmester 5',75,4),
	('H6','HX','Summer','Hexmester 6',90,4),
	('IS1','IS','Winter','Intersession 1',53,3),
	('IS2','IS','Winter','Intersession 2',56,3),
	('IS3','IS','Winter','Intersession 3',59,3),
	('QQ1','QQ','Summer','College Summer Quarter 1',94,1),
	('QQ2','QQ','Summer','College Summer Quarter 2',98,1),
	('QS1','QS','Summer','College Summer Session 1',93,1),
	('QS2','QS','Summer','College Summer Session 2',96,1),
	('QS3','QS','Summer','College Summer Session 3',99,1),
	('Q1','Q','Fall','Quarter 1',20,1),
	('Q2','Q','Winter','Quarter 2',40,2),
	('Q3','Q','Spring','Quarter 3',60,3),
	('Q4','Q','Summer','Quarter 4',80,4),
	('QT1','QT','Fall','Quarter 1',20,1),
	('QT2','QT','Winter','Quarter 2',40,2),
	('QT3','QT','Spring','Quarter 3',60,3),
	('QT4','QT','Summer','Quarter 4',80,4),
	('SC1','SC','Summer','College Summer Semester 1',4,1),
	('SC2','SC','Summer','College Summer Semester 2',8,1),
	('S1','S','Fall','Semester 1 (Fall)',35,2),
	('S2','S','Spring','Semester 2 (Spring)',65,4),
	('SM1','SM','Fall','Semester 1 (Fall)',35,2),
	('SM2','SM','Spring','Semester 2 (Spring)',65,4),
	('SPL','SPL','Summer','Supplemental Session',0,1),
	('SS','S*','Summer','Semester Summer Session',90,1),
	('SS1','SS','Summer','Semester Summer Session 1',93,1),
	('SS2','SS','Summer','Semester Summer Session 2',96,1),
	('SS3','SS','Summer','Semester Summer Session 3',99,1),
	('TR1','TR','Fall','Trimester 1',15,2),
	('TR2','TR','Winter','Trimester 2',45,3),
	('TR3','TR','Spring','Trimester 3',75,4),
	('TS1','TS','Fall','Trimester Summer Session 1',3,2),
	('TS2','TS','Winter','Trimester Summer Session 2',6,3),
	('TS3','TS','Spring','Trimester Summer Session 3',9,4),
	('ZO1','ZO','Fall','Other, User Defined 1',10,1),
	('ZO2','ZO','Fall','Other, User Defined 2',20,1),
	('ZO3','ZO','Winter','Other, User Defined 3',30,2),
	('ZO4','ZO','Winter','Other, User Defined 4',40,2),
	('ZO5','ZO','Spring','Other, User Defined 5',50,3),
	('ZO6','ZO','Spring','Other, User Defined 6',60,3),
	('ZO7','ZO','Summer','Other, User Defined 7',70,4),
	('ZO9','ZO','Summer','Other, User Defined 8',80,4),
	('Z1','Z','Fall','Other, User Defined 1',10,1),
	('Z2','Z','Fall','Other, User Defined 2',20,1),
	('Z3','Z','Winter','Other, User Defined 3',30,2),
	('Z4','Z','Winter','Other, User Defined 4',40,2),
	('Z5','Z','Spring','Other, User Defined 5',50,3),
	('Z6','Z','Spring','Other, User Defined 6',60,3),
	('Z7','Z','Summer','Other, User Defined 7',70,4),
	('Z9','Z','Summer','Other, User Defined 8',80,4);