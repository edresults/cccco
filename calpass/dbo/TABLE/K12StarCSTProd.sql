USE [calpass]
GO

/****** Object:  Table [dbo].[K12StarCSTProd]    Script Date: 12/3/2015 8:15:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[K12StarCSTProd](
	[derkey1] [char](15) NULL,
	[School] [char](14) NOT NULL,
	[AcYear] [char](4) NOT NULL,
	[LocStudentId] [char](15) NOT NULL,
	[CSISNum] [char](10) NULL,
	[CSTMathSubj] [char](1) NULL,
	[CSTSciSubj] [char](1) NULL,
	[CSTWriting] [char](2) NULL,
	[CSTEngRaw] [char](2) NULL,
	[CSTMathRaw] [char](2) NULL,
	[CSTSciRaw] [char](2) NULL,
	[CSTHistRaw] [char](2) NULL,
	[CSTEngClstr1] [char](2) NULL,
	[CSTEngClstr2] [char](2) NULL,
	[CSTEngClstr3] [char](2) NULL,
	[CSTEngClstr4] [char](2) NULL,
	[CSTEngClstr5] [char](2) NULL,
	[CSTEngClstr6] [char](2) NULL,
	[CSTMathClstr1] [char](2) NULL,
	[CSTMathClstr2] [char](2) NULL,
	[CSTMathClstr3] [char](2) NULL,
	[CSTMathClstr4] [char](2) NULL,
	[CSTMathClstr5] [char](2) NULL,
	[CSTMathClstr6] [char](2) NULL,
	[CSTSciClstr1] [char](2) NULL,
	[CSTSciClstr2] [char](2) NULL,
	[CSTSciClstr3] [char](2) NULL,
	[CSTSciClstr4] [char](2) NULL,
	[CSTSciClstr5] [char](2) NULL,
	[CSTSciClstr6] [char](2) NULL,
	[CSTHistClstr1] [char](2) NULL,
	[CSTHistClstr2] [char](2) NULL,
	[CSTHistClstr3] [char](2) NULL,
	[CSTHistClstr4] [char](2) NULL,
	[CSTHistClstr5] [char](2) NULL,
	[CSTHistClstr6] [char](2) NULL,
	[CSTEngSS] [char](3) NULL,
	[CSTMathSS] [char](3) NULL,
	[CSTSciSS] [char](3) NULL,
	[CSTHistSS] [char](3) NULL,
	[CSTEngPerf] [char](1) NULL,
	[CSTMathPerf] [char](1) NULL,
	[CSTSciPerf] [char](1) NULL,
	[CSTHistPerf] [char](1) NULL,
	[RecordType] [char](2) NULL,
	[UIN] [char](20) NULL,
	[CSTSTSItemsAttemptELARLA] [char](2) NULL,
	[CSTSTSItemsAttemptMath] [char](2) NULL,
	[CSTSTSItemsAttemptScience] [char](2) NULL,
	[CSTSTSItemsAttemptHistSS] [char](2) NULL,
	[CSTSTSItemsAttemptWrldHist] [char](2) NULL,
	[CSTSTSItemsAttemptEOCScience] [char](2) NULL,
	[CSTWritingPrompt] [char](1) NULL,
	[CSTCAPASTSInclIndELARLA] [char](1) NULL,
	[CSTCAPASTSInclIndMath] [char](1) NULL,
	[CSTCAPASTSInclIndScience] [char](1) NULL,
	[CSTCAPASTSInclIndHistSS] [char](1) NULL,
	[CSTCAPASTSInclIndWrldHist] [char](1) NULL,
	[CSTCAPASTSInclIndEOCScience] [char](1) NULL,
	[CSTCAPASTSRawWrldHist] [char](2) NULL,
	[CSTCAPASTSRawEOCScience] [char](2) NULL,
	[CSTWrldHistRawClst1] [char](2) NULL,
	[CSTWrldHistRawClst2] [char](2) NULL,
	[CSTWrldHistRawClst3] [char](2) NULL,
	[CSTWrldHistRawClst4] [char](2) NULL,
	[CSTWrldHistRawClst5] [char](2) NULL,
	[CSTWrldHistRawClst6] [char](2) NULL,
	[CSTEOCRawClst1] [char](2) NULL,
	[CSTEOCRawClst2] [char](2) NULL,
	[CSTEOCRawClst3] [char](2) NULL,
	[CSTEOCRawClst4] [char](2) NULL,
	[CSTEOCRawClst5] [char](2) NULL,
	[CSTEOCRawClst6] [char](2) NULL,
	[CSTCAPASSWrldHist] [char](3) NULL,
	[CSTCAPASSEOC] [char](3) NULL,
	[CSTCAPSPerfLevelWrldHist] [char](1) NULL,
	[CSTCAPAPerfLevelEOC] [char](1) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


