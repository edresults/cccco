IF (object_id('dbo.StudentStar') is not null)
	BEGIN
		DROP TABLE dbo.StudentStar;
	END;

GO

CREATE TABLE
	dbo.StudentStar
	(
		derkey1 binary(64) NOT NULL,
		engl_eap_ind INT,
		engl_scaled_score INT,
		engl_cluster_1 INT,
		engl_cluster_2 INT,
		engl_cluster_3 INT,
		engl_cluster_4 INT,
		engl_cluster_5 INT,
		engl_cluster_6 INT,
		math_subject VARCHAR(1),
		math_eap_ind INT,
		math_scaled_score INT,
		math_cluster_1 INT,
		math_cluster_2 INT,
		math_cluster_3 INT,
		math_cluster_4 INT,
		math_cluster_5 INT,
		math_cluster_6 INT,
		esl_eo_ind BIT,
		esl_ifep_ind BIT,
		esl_el_ind BIT,
		esl_rfep_ind BIT
	);

ALTER TABLE
	dbo.StudentStar
ADD CONSTRAINT
	PK_StudentStar
PRIMARY KEY
	(
		Derkey1
	);

INSERT
	dbo.StudentStar
	(
		derkey1,
		engl_eap_ind,
		engl_scaled_score,
		engl_cluster_1,
		engl_cluster_2,
		engl_cluster_3,
		engl_cluster_4,
		engl_cluster_5,
		engl_cluster_6,
		math_subject,
		math_eap_ind,
		math_scaled_score,
		math_cluster_1,
		math_cluster_2,
		math_cluster_3,
		math_cluster_4,
		math_cluster_5,
		math_cluster_6,
		esl_eo_ind,
		esl_ifep_ind,
		esl_el_ind,
		esl_rfep_ind
	)
SELECT
	d.derkey1,
	max(d.engl_eap_ind) as engl_eap_ind,
	max(case d.engl_select_ind when 1 then d.engl_scaled_score end) as engl_scaled_score,
	max(case d.engl_select_ind when 1 then d.engl_cluster_1 end) as engl_cluster_1,
	max(case d.engl_select_ind when 1 then d.engl_cluster_2 end) as engl_cluster_2,
	max(case d.engl_select_ind when 1 then d.engl_cluster_3 end) as engl_cluster_3,
	max(case d.engl_select_ind when 1 then d.engl_cluster_4 end) as engl_cluster_4,
	max(case d.engl_select_ind when 1 then d.engl_cluster_5 end) as engl_cluster_5,
	max(case d.engl_select_ind when 1 then d.engl_cluster_6 end) as engl_cluster_6,
	max(case d.math_select_ind when 1 then d.math_subject end) as math_subject,
	max(d.math_eap_ind) as math_eap_ind,
	max(case d.math_select_ind when 1 then d.math_scaled_score end) as math_scaled_score,
	max(case d.math_select_ind when 1 then d.math_cluster_1 end) as math_cluster_1,
	max(case d.math_select_ind when 1 then d.math_cluster_2 end) as math_cluster_2,
	max(case d.math_select_ind when 1 then d.math_cluster_3 end) as math_cluster_3,
	max(case d.math_select_ind when 1 then d.math_cluster_4 end) as math_cluster_4,
	max(case d.math_select_ind when 1 then d.math_cluster_5 end) as math_cluster_5,
	max(case d.math_select_ind when 1 then d.math_cluster_6 end) as math_cluster_6,
	max(d.esl_eo_ind) as esl_eo_ind,
	max(d.esl_ifep_ind) as esl_ifep_ind,
	max(d.esl_el_ind) as esl_el_ind,
	max(d.esl_rfep_ind) as esl_rfep_ind
FROM
	(
		SELECT
			a.derkey1,
			row_number() OVER(
				PARTITION BY
					a.derkey1
				ORDER BY
					case when a.cstengss is null then 0 else 1 end, -- get non-null scores
					c.YearTrailing DESC, -- most recent year that has a non-null score
					CAST(a.cstengss AS INT) DESC -- highest score
			) as engl_select_ind,
			case z.EAPELAStatus when '1' then 1 else 0 end as engl_eap_ind,
			CAST(a.cstengss AS INT) as engl_scaled_score,
			CAST(case when a.CSTEngClstr1 in ('98', '99') or a.CSTEngClstr1 like '%*%' or a.CSTEngClstr1 like '%[a-Z]%' then null else a.CSTEngClstr1 end AS INT) as engl_cluster_1,
			CAST(case when a.CSTEngClstr2 in ('98', '99') or a.CSTEngClstr2 like '%*%' or a.CSTEngClstr2 like '%[a-Z]%' then null else a.CSTEngClstr2 end AS INT) as engl_cluster_2,
			CAST(case when a.CSTEngClstr3 in ('98', '99') or a.CSTEngClstr3 like '%*%' or a.CSTEngClstr3 like '%[a-Z]%' then null else a.CSTEngClstr3 end AS INT) as engl_cluster_3,
			CAST(case when a.CSTEngClstr4 in ('98', '99') or a.CSTEngClstr4 like '%*%' or a.CSTEngClstr4 like '%[a-Z]%' then null else a.CSTEngClstr4 end AS INT) as engl_cluster_4,
			CAST(case when a.CSTEngClstr5 in ('98', '99') or a.CSTEngClstr5 like '%*%' or a.CSTEngClstr5 like '%[a-Z]%' then null else a.CSTEngClstr5 end AS INT) as engl_cluster_5,
			CAST(case when a.CSTEngClstr6 in ('98', '99') or a.CSTEngClstr6 like '%*%' or a.CSTEngClstr6 like '%[a-Z]%' then null else a.CSTEngClstr6 end AS INT) as engl_cluster_6,
			row_number() OVER(
				PARTITION BY
					a.derkey1
				ORDER BY
					-- b.rank DESC, [1]
					case
						when a.cstmathsubj in ('0', '2', '6', '8') then 1
						else 0
					end DESC,
					c.YearTrailing DESC,
					CAST(a.cstmathss AS INT) DESC
			) as math_select_ind,
			CAST(a.cstmathsubj AS INT) as math_subject,
			case z.EAPMathStatus when '1' then 1 else 0 end as math_eap_ind,
			CAST(a.cstmathss AS INT) as math_scaled_score,
			CAST(case when a.CSTMathClstr1 in ('98', '99') or a.CSTMathClstr1 like '%*%' or a.CSTMathClstr1 like '%[a-Z]%' then null else a.CSTMathClstr1 end AS INT) as math_cluster_1,
			CAST(case when a.CSTMathClstr2 in ('98', '99') or a.CSTMathClstr2 like '%*%' or a.CSTMathClstr1 like '%[a-Z]%' then null else a.CSTMathClstr2 end AS INT) as math_cluster_2,
			CAST(case when a.CSTMathClstr3 in ('98', '99') or a.CSTMathClstr3 like '%*%' or a.CSTMathClstr1 like '%[a-Z]%' then null else a.CSTMathClstr3 end AS INT) as math_cluster_3,
			CAST(case when a.CSTMathClstr4 in ('98', '99') or a.CSTMathClstr4 like '%*%' or a.CSTMathClstr1 like '%[a-Z]%' then null else a.CSTMathClstr4 end AS INT) as math_cluster_4,
			CAST(case when a.CSTMathClstr5 in ('98', '99') or a.CSTMathClstr5 like '%*%' or a.CSTMathClstr1 like '%[a-Z]%' then null else a.CSTMathClstr5 end AS INT) as math_cluster_5,
			CAST(case when a.CSTMathClstr6 in ('98', '99') or a.CSTMathClstr6 like '%*%' or a.CSTMathClstr1 like '%[a-Z]%' then null else a.CSTMathClstr6 end AS INT) as math_cluster_6,
			case
				when rtrim(z.englprof) in ('1', 'EO') then 1
				else 0
			end as esl_eo_ind,
			case
				when rtrim(z.englprof) in ('2', 'IFEP') then 1
				else 0
			end as esl_ifep_ind,
			case
				when rtrim(z.englprof) in ('3', 'EL') then 1
				else 0
			end as esl_el_ind,
			case
				when rtrim(z.englprof) in ('4', 'RFEP') then 1
				else 0
			end as esl_rfep_ind
		FROM
			calpass.dbo.K12StarDEMProd z
			inner join
			calpass.dbo.K12StarCSTProd a
				on a.School = z.School
				and a.LocStudentId = z.LocStudentId
				and a.AcYear = z.AcYear
			left outer join
			dbo.StarMath b
				on a.cstmathsubj = b.code
			left outer join
			calpads.Year c
				on z.acYear = c.YearCodeAbbr
		WHERE
			a.derkey1 is not null
			and z.derkey1 is not null
			and a.cstmathsubj in ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
	) d
GROUP BY
	d.derkey1