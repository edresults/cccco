USE [calpass]
GO

/****** Object:  Table [dbo].[K12PRGProd]    Script Date: 12/3/2015 8:12:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[K12PRGProd](
	[derkey1] [char](15) NULL,
	[School] [char](14) NULL,
	[AcYear] [char](4) NULL,
	[LocStudentId] [char](10) NULL,
	[CSISNum] [char](25) NULL,
	[Fname] [char](30) NULL,
	[Lname] [char](40) NULL,
	[BirthDate] [char](8) NULL,
	[Gender] [char](1) NULL,
	[EdProgramCode] [char](3) NULL,
	[EdProgramMembershipCode] [char](1) NULL,
	[EdPMStart] [char](8) NULL,
	[EdPMEnd] [char](8) NULL,
	[EdServiceYear] [char](9) NULL,
	[EdServiceCode] [char](2) NULL,
	[CaPartAcademyID] [char](5) NULL,
	[MigrantStudentID] [char](11) NULL,
	[PrimaryDisabilityCode] [char](3) NULL,
	[SpecEd] [char](7) NULL,
	[HomelessDwelling] [char](3) NULL,
	[UnaccYouth] [char](1) NULL,
	[Runaway] [char](1) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


