use calpass;

GO

IF (object_id('calpass.dbo.k12studentprod_gradeLevel') is not null)
	BEGIN
		DROP TABLE dbo.k12studentprod_gradeLevel;
	END;

GO

CREATE TABLE
	dbo.k12studentprod_gradeLevel
	(
		code char(4),
		name varchar(255),
		description varchar(8000),
		year_code_start char(9),
		year_code_end char(9),
		code_rank tinyint,
		CONSTRAINT
			pk_k12studentprod_gradeLevel__code__year_code_start__year_code_end
		PRIMARY KEY CLUSTERED
			(
				code,
				year_code_start,
				year_code_end
			)
	);

GO

CREATE INDEX
	ix_k12studentprod__code_rank
ON
	dbo.k12studentprod_gradeLevel
	(
		code_rank
	);

GO

INSERT INTO
	dbo.k12studentprod_gradeLevel
	(
		code,
		name,
		description,
		year_code_start,
		year_code_end,
		code_rank
	)
SELECT
	code,
	name,
	description,
	case
		when cast(month(convert(date, date_start, 101)) as tinyint) >= 7 then
			cast(year(convert(date, date_start, 101)) as char(4)) + '-' + 
			cast(year(convert(date, date_start, 101)) + 1 as char(4))
		else cast(year(convert(date, date_start, 101)) - 1 as char(4)) + '-' + 
			cast(year(convert(date, date_start, 101)) as char(4))
	end as year_code_start,
	case
		when cast(month(convert(date, isnull(date_end, '6/30/2099'), 101)) as tinyint) >= 7 then
			cast(year(convert(date, isnull(date_end, '6/30/2099'), 101)) as char(4)) + '-' + 
			cast(year(convert(date, isnull(date_end, '6/30/2099'), 101)) + 1 as char(4))
		else cast(year(convert(date, isnull(date_end, '6/30/2099'), 101)) - 1 as char(4)) + '-' + 
			cast(year(convert(date, isnull(date_end, '6/30/2099'), 101)) as char(4))
	end as year_code_end,
	case code
		when 'TD' then 0
		when 'PS' then 1
		when 'KN' then 2
		when '01' then 3
		when '02' then 4
		when '03' then 5
		when '04' then 6
		when '05' then 7
		when '06' then 8
		when '07' then 9
		when '08' then 10
		when '09' then 11
		when '10' then 12
		when '11' then 13
		when '12' then 14
		when 'AD' then 15
	end as code_rank
FROM
	(
		VALUES
		('01','First Grade','Grade One','7/1/1990',null),
		('02','Second Grade','Grade Two','7/1/1990',null),
		('03','Third Grade','Grade Three','7/1/1990',null),
		('04','Fourth Grade','Grade Four','7/1/1990',null),
		('05','Fifth Grade','Grade Five','7/1/1990',null),
		('06','Sixth Grade','Grade Six','7/1/1990',null),
		('07','Seventh Grade','Grade Seven','7/1/1990',null),
		('08','Eighth Grade','Grade Eight','7/1/1990',null),
		('09','Ninth Grade','Grade Nine','7/1/1990',null),
		('10','Tenth Grade','Grade Ten','7/1/1990',null),
		('11','Eleventh Grade','Grade Eleven','7/1/1990',null),
		('12','Twelfth Grade','Grade Twelve','7/1/1990',null),
		('AD','Adult','A category for students who are primarily enrolled in an adult education center. These students would not have an affiliation with a K-12 institution.','7/1/1990',null),
		('IN','Infant','A grade level for individuals, ages 0 through 18 months.','7/1/1990',null),
		('KN','Kindergarten','Kindergarten','7/1/1990',null),
		('PS','Prekindergarten','A grade level for children formally enrolled in a preschool program in preparation for entrance to kindergarten.','7/1/1990',null),
		('TD','Toddlers','A grade level for individuals, ages 19 months through 35 months, not yet enrolled in a public educational institution or formal preschool program.','7/1/1990',null),
		('UE','Ungraded Elementary','A grade level for students enrolled in elementary classes (kindergarten through eighth grades) that are not assigned to a specific grade level. This grade level is specifically for students in a special education program.','7/1/1990',null),
		('US','Ungraded Secondary','A grade level for students enrolled in secondary classes (ninth through twelfth grades) that are not assigned to a specific grade level. This grade level is specifically for students in a special education program.','7/1/1990',null),
		('UU','Unknown','The grade level cannot be determined. For example, there were multiple responses or no response at all. You would not see this option on a form, but rather this category is used for storage of data anomalies.','7/1/1990', null)
	) t (code, name, description, date_start, date_end)