USE calpass;

GO

IF (object_id('dbo.Student') is not null)
	BEGIN
		DROP TABLE dbo.Student;
	END;

GO

CREATE TABLE
	dbo.Student
	(
		InterSegmentKey binary(64) not null,
		IsHighSchoolStudent bit not null,
		IsHighSchoolSection bit not null,
		IsGrade09Student bit not null,
		IsGrade09Section bit not null,
		IsGrade10Student bit not null,
		IsGrade10Section bit not null,
		IsGrade10StudentInclusive bit not null,
		IsGrade10SectionInclusive bit not null,
		IsGrade11Student bit not null,
		IsGrade11Section bit not null,
		IsGrade11StudentInclusive bit not null,
		IsGrade11SectionInclusive bit not null,
		IsGrade12Student bit not null,
		IsGrade12Section bit not null,
		IsGrade12StudentInclusive bit not null,
		IsGrade12SectionInclusive bit not null,
		IsHighSchoolCollision bit not null
	);

GO

ALTER TABLE
	dbo.Student
ADD CONSTRAINT
	PK_Student
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey
	);