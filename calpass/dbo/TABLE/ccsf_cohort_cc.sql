USE calpass;

IF (object_id('dbo.ccsf_cohort_cc', 'U') is not null)
	BEGIN
		DROP TABLE dbo.ccsf_cohort_cc;
	END;

GO

CREATE TABLE
	dbo.ccsf_cohort_cc
	(
		CollegeId char(6),
		StudentId varchar(10),
		TermId char(3),
		term_id_first char(3),
		Derkey1 char(15),
		CONSTRAINT
			pk_ccsf_cohort_cc__CollegeId__StudentId__TermId__term_id_first
		PRIMARY KEY CLUSTERED
			(
				CollegeId,
				StudentId,
				TermId,
				term_id_first
			)
	);

GO

CREATE INDEX
	ix_ccsf_cohort_cc__Derkey
ON
	dbo.ccsf_cohort_cc
	(
		Derkey1
	);