IF (object_id('dbo.FosterYouthStudent') is not null)
	BEGIN
		DROP TABLE dbo.FosterYouthStudent;
	END;

GO

CREATE TABLE
	dbo.FosterYouthStudent
	(
		SchoolCode       varchar(255),
		SchoolName       varchar(255),
		SSID             varchar(255),
		NameFirst        varchar(255),
		NameLast         varchar(255),
		LocalID          varchar(255),
		Gender           varchar(255),
		GradeLevel       varchar(255),
		RaceEthnicity    varchar(255),
		EnrollmentStatus varchar(255),
		DirectCert       varchar(255),
		Foster           varchar(255),
		Homeless         varchar(255),
		NSLPProgram      varchar(255),
		MigrantEdProgram varchar(255),
		ELASDesignation  varchar(255)
	);

GO

CREATE CLUSTERED INDEX
	IC_FosterYouthStudent
ON
	calpass.dbo.FosterYouthStudent
	(
		SSID
	);

GO

INSERT
	dbo.FosterYouthStudent
	(
		SchoolCode,
		SchoolName,
		SSID,
		NameFirst,
		NameLast,
		LocalID,
		Gender,
		GradeLevel,
		RaceEthnicity,
		EnrollmentStatus,
		DirectCert,
		Foster,
		Homeless,
		NSLPProgram,
		MigrantEdProgram,
		ELASDesignation
	)
SELECT
	SchoolCode,
	SchoolName,
	SSID = calpass.dbo.get9812Encryption(SSID, 'X'),
	NameFirst =
		substring(
			replace(StudentName, '"', ''),
			charindex(',', replace(StudentName, '"', '')) + 2,
			len(replace(StudentName, '"', ''))
		),
	NameLast =
		substring(
			replace(StudentName, '"', ''),
			1,
			charindex(',', replace(StudentName, '"', '')) - 1
		),
	LocalID,
	Gender,
	GradeLevel,
	RaceEthnicity,
	EnrollmentStatus,
	DirectCert,
	Foster,
	Homeless,
	NSLPProgram,
	MigrantEdProgram,
	ELASDesignation
FROM
	OPENROWSET (
		BULK N'\\10.11.5.61\SFTPRoot\lausd\1.18 FPRM-English Learner-Foster Youth - Student List.txt',
		FORMATFILE = '\\10.11.5.61\SFTPRoot\lausd\FosterYouthStudent.fmt',
		FIRSTROW = 2
	) c