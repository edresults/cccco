CREATE TABLE
	dbo.StarMath
	(
		Code varchar(1) not null,
		Description varchar(255) not null,
		Rank tinyint not null
	);

GO

ALTER TABLE
	dbo.StarMath
ADD CONSTRAINT
	pk_StarMath__Code
PRIMARY KEY CLUSTERED
(
	Code
);

GO

INSERT INTO
	dbo.StarMath
	(
		Code,
		Description,
		Rank
	)
VALUES
	('0','Grade-level mathematics (grade 7); default if unknown',1),
	('1','General Mathematics (grades 8 and 9)',2),
	('2','Summative High School Mathematics (grades 9–11)',3),
	('3','Algebra I',4),
	('4','Integrated Mathematics 1',4),
	('5','Geometry',5),
	('6','Integrated Mathematics 2',5),
	('7','Algebra II',6),
	('8','Integrated Mathematics 3',6),
	('9','Unknown (mathematics test in grades 8–11 was taken)',0);