USE [calpass]
GO

/****** Object:  Table [dbo].[K12StarDemProd]    Script Date: 12/3/2015 8:13:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[K12StarDemProd](
	[derkey1] [char](15) NULL,
	[School] [char](14) NOT NULL,
	[AcYear] [char](4) NOT NULL,
	[LocStudentId] [char](15) NOT NULL,
	[CSISNum] [char](10) NULL,
	[GradeLvl] [char](2) NULL,
	[EnglProf] [char](4) NULL,
	[PrimLang] [char](2) NULL,
	[PrimDisability] [char](3) NULL,
	[ParentEdLvl] [char](2) NULL,
	[TestGradeLvl] [char](2) NULL,
	[Lname] [char](11) NULL,
	[Fname] [char](9) NULL,
	[Birthdate] [char](8) NULL,
	[Gender] [char](1) NULL,
	[RecordType] [char](2) NULL,
	[UIN] [char](20) NULL,
	[DocCode] [char](6) NULL,
	[PearsonOrderNum] [char](8) NULL,
	[ELDate] [char](8) NULL,
	[RFEPDate] [char](8) NULL,
	[RFEPProficient] [char](1) NULL,
	[RecvTitleIServices] [char](1) NULL,
	[MigrantEd] [char](1) NULL,
	[IndianEd] [char](1) NULL,
	[GiftedAndTalented] [char](1) NULL,
	[ELProgramType] [char](1) NULL,
	[ELLessThan12Months] [char](1) NULL,
	[EnrollLess90Days] [char](1) NULL,
	[NSLP] [char](1) NULL,
	[AttndNonPubSchlIEP] [char](1) NULL,
	[NPSSchoolCD] [char](7) NULL,
	[STSOriginCountry] [char](2) NULL,
	[SpecialEdExitDate] [char](8) NULL,
	[PrimaryEthnicity] [char](3) NULL,
	[OtherBlackAfricanAm] [char](1) NULL,
	[OtherAmIndianAlaskaNtv] [char](1) NULL,
	[OtherChinese] [char](1) NULL,
	[OtherJapanese] [char](1) NULL,
	[OtherKorean] [char](1) NULL,
	[OtherVietnamese] [char](1) NULL,
	[OtherAsianIndian] [char](1) NULL,
	[OtherLaotian] [char](1) NULL,
	[OtherCambodian] [char](1) NULL,
	[OtherAsian] [char](1) NULL,
	[OtherFIlipino] [char](1) NULL,
	[OtherHispanicLatino] [char](1) NULL,
	[OtherHawaiian] [char](1) NULL,
	[OtherGuamanian] [char](1) NULL,
	[OtherSamoan] [char](1) NULL,
	[OtherTahitian] [char](1) NULL,
	[OtherPacificIslander] [char](1) NULL,
	[OtherWhite] [char](1) NULL,
	[DistrictCntCBEDS] [char](1) NULL,
	[SchoolCntCBEDS] [char](1) NULL,
	[CalReadListNumber] [char](3) NULL,
	[UnmatchedWriting] [char](1) NULL,
	[UnmatchedGrade3] [char](1) NULL,
	[EAPELAStatus] [char](1) NULL,
	[EAPMathStatus] [char](1) NULL,
	[GroupName] [char](20) NULL,
	[GroupCode] [char](10) NULL,
	[LocalUseFromPreID] [char](10) NULL,
	[PreIDIndicator] [char](1) NULL,
	[CAPALevel] [char](1) NULL,
	[CntyDstrctCDIEP] [char](7) NULL,
	[DemographicOnly] [char](1) NULL,
	[AMSection504] [char](1) NULL,
	[AMIEP] [char](1) NULL,
	[HispanicOrLatino] [char](1) NULL,
	[OtherHmong] [char](1) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


