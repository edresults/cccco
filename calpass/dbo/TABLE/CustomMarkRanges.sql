CREATE TABLE
    CustomMarkRanges
    (
        OrganizationCode varchar(14)  not null,
        MarkCode         varchar(3)   not null,
        LowerBound       decimal(4,3) not null,
        UpperBound       decimal(4,3) not null,
        -- PK
        CONSTRAINT CustomMarkRanges_PK PRIMARY KEY CLUSTERED (OrganizationCode, MarkCode)
    );