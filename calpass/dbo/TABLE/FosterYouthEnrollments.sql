CREATE TABLE
    dbo.FosterYouthEnrollments
    (
        Id                        int         NOT NULL IDENTITY,
        SchoolCode                char(7)     NOT NULL,
        SchoolName                varchar(90)     NULL,
        StateId                   char(10)    NOT NULL,
        StudentName               varchar(72)     NULL,
        LocalId                   varchar(15)     NULL,
        BirthDate                 date            NULL,
        Gender                    char(1)         NULL,
        GradeLevel                char(2)         NULL,
        EnrollmentStartDate       date            NULL,
        EnrollmentStatus          varchar(30)     NULL,
        LocalMatch                char(1)         NULL,
        StateMatch                char(1)         NULL,
        CountyOfJurisdiction      varchar(15)     NULL,
        ServiceType               varchar(2)      NULL,
        ClientId                  char(10)    NOT NULL,
        CaseId                    char(19)        NULL,
        CaseStartDate             date            NULL,
        EpisodeStartDate          date            NULL,
        EpisodeEndDate            date            NULL,
        SocialWorkerName          varchar(72)     NULL,
        SocialWorkerPhoneNumber   char(14)        NULL,
        ResponsibleAgency         char(1)         NULL,
        ParentalRightLimited      char(1)         NULL,
        CourtAppointedName        varchar(72)     NULL,
        CourtAppointedPhoneNumber char(14)        NULL,
        UpdateDate                date            NULL,
        CONSTRAINT PK_FosterYouthEnrollments PRIMARY KEY (Id),
        CONSTRAINT AK_FosterYouthEnrollments_StateId_SchoolCode_ClientId UNIQUE (StateId, SchoolCode, ClientId)
    );

GO

CREATE TRIGGER
    TR_FosterYouthEnrollments
ON
    FosterYouthEnrollments
AFTER
    UPDATE, INSERT
AS

DECLARE
    @ModifyDate DATE = CONVERT(DATE, GetDate());

BEGIN
    UPDATE
        t
    SET
        t.ModifyDate = @ModifyDate
    FROM
        FosterYouthEnrollments t
        inner join
        inserted i
            on  i.StateId    = t.StateId
            and i.SchoolCode = t.SchoolCode
            and i.ClientId   = t.ClientId;
END;