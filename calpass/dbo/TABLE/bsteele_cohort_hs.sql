USE calpass;

GO

IF (object_id('calpass.dbo.bsteele_cohort_hs', 'U') is not null)
	BEGIN
		DROP TABLE calpass.dbo.bsteele_cohort_hs;
	END;

GO

CREATE TABLE
	calpass.dbo.bsteele_cohort_hs
	(
		/* pk */
		derkey1 char(15),
		/* student */
		student_gender char(1),
		student_ethnicity char(3),
		student_hispanic_ind tinyint,
		student_ses_ind tinyint,
		student_birthdate char(8),
		student_language char(2),
		/* higher education */
		student_ccc_first_term_value char(5),
		student_ccc_first_term_nsa_value char(5),
		student_ccc_first_term_cr_value char(5),
		student_ccc_first_term_cr_nsa_value char(5),
		student_ccc_first_term_ncr_value char(5),
		student_ccc_first_term_ncr_nsa_value char(5),
		student_univ_first_date char(8),
		student_univ_first_code char(3),
		student_univ_last_date char(8),
		student_univ_last_code char(3),
		/* summary */
		student_cumulative_gpa decimal(4,3),
		student_highest_math_title varchar(255),
		student_highest_math_grade varchar(3),
		student_highest_engl_title varchar(255),
		student_highest_engl_grade varchar(3),
		/* concurrency */
		ccc_concurrent_section_enrollment_count tinyint,
		ccc_concurrent_section_units_attempted decimal(6,2),
		ccc_concurrent_section_units_achieved decimal(6,2),
		ccc_concurrent_section_grade_points decimal(6,2),
		/* organization */
		organization_cds_code char(14),
		organization_grade_level_code char(2),
		organization_year_code char(4),
		organization_term_code char(3),
		/* course */
		course_title varchar(40),
		course_id varchar(10),
		course_code char(4),
		course_subject varchar(255),
		course_ag_code char(2),
		course_ap_ind tinyint,
		course_ib_ind tinyint,
		course_cte_ind tinyint,
		course_exp_ind tinyint,
		course_avid_ind tinyint,
		/* section */
		section_id varchar(15),
		section_credit_attempted decimal(15,5),
		section_credit_achieved decimal(15,5),
		section_grade varchar(3),
		section_grade_points decimal(2,1),
		/* mt san jacinto college dual enrollment indicator */
		student_msjc_de_ind tinyint,
		student_award_code char(3),
		CONSTRAINT
			pk_bsteel_cohort_hs__derkey1__cds_code__year_code__term_code__course_code__course_title__course_id__section_id__section_grade
		PRIMARY KEY CLUSTERED
			(
				derkey1,
				organization_cds_code,
				organization_year_code,
				organization_term_code,
				course_code,
				course_title,
				course_id,
				section_id,
				section_grade,
				section_credit_attempted
			)
	);