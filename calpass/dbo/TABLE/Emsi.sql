USE calpass;

IF (object_id('calpass.dbo.Emsi') is not null)
	BEGIN
		DROP TABLE dbo.Emsi;
	END;

GO

CREATE TABLE
	dbo.Emsi
	(
		RegionCode char(1) not null,
		RegionName varchar(255) not null,
		Soc char(7) not null,
		YearPast char(4) not null,
		YearCurrent char(4) not null,
		YearFuture char(4) not null,
		EmploymentPast decimal(38,19),
		EmploymentCurrent decimal(38,19),
		ChangePastCurrent decimal(38,19),
		ChangePastCurrentPct decimal(38,19),
		ChangeCurrentFuture decimal(38,19),
		ChangeCurrentFuturePct decimal(38,19),
		OpeningsCurrentFuture decimal(38,19),
		EarningsPctl10 decimal(38,19),
		EarningsPctl25 decimal(38,19),
		EarningsPctl50 decimal(38,19),
		EarningsPctl75 decimal(38,19)
	);

GO

ALTER TABLE
	dbo.Emsi
ADD CONSTRAINT
	PK_Emsi
PRIMARY KEY CLUSTERED
	(
		RegionCode,
		RegionName,
		Soc,
		YearCurrent
	);