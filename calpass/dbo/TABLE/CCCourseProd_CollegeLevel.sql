USE calpass;

IF (object_id('calpass.dbo.CCCourseProd_CollegeLevel') is not null)
	BEGIN
		DROP TABLE dbo.CCCourseProd_CollegeLevel;
	END;

GO

CREATE TABLE
	dbo.CCCourseProd_CollegeLevel
	(
		code VARCHAR(2),
		description VARCHAR(255),
		rank TINYINT,
		level_below TINYINT,
		CONSTRAINT
			pk_CCCourseProd_CollegeLevel__code
		PRIMARY KEY
			(
				code
			)
	);

CREATE INDEX
	ix_CCCourseProd_CollegeLevel__rank
ON
	dbo.CCCourseProd_CollegeLevel
	(
		rank
	);

GO

INSERT INTO
	dbo.CCCourseProd_CollegeLevel
	(
		code,
		description,
		rank,
		level_below
	)
VALUES
	('Y','Not Applicable',10,0),
	('A','One level below transfer',9,1),
	('B','Two levels below transfer',8,2),
	('C','Three levels below transfer',7,3),
	('D','Four levels below transfer',6,4),
	('E','Five levels below transfer',5,5),
	('F','Six levels below transfer',4,6),
	('G','Seven levels below transfer',3,7),
	('H','Eight levels below transfer',2,8);