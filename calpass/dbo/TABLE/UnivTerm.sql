USE calpass;

GO

IF (object_id('dbo.UnivTerm') is not null)
	BEGIN
		DROP TABLE dbo.UnivTerm;
	END;

GO

CREATE TABLE
	dbo.UnivTerm
	(
		TermCode char(5), -- 20101
		AcademicYear char(9), -- 2010-2011
		AcademicYearAbbr char(4), -- 1011
		AcademicYearTrailing smallint, -- 2010-2011 == 2010
		AcademicYearStart smallint, -- 2010-2011 == 2010
		AcademicYearLeading smallint, -- 2010-2011 == 2011
		AcademicYearEnd smallint, -- 2010-2011 == 2011
		CalendarYear char(4), -- 20101 = 2010; 20114 = 2011
		CalendarYearAbbr char(2), -- 10
		Season varchar(30), -- Fall
		Rank tinyint, -- TermCode ranked within AcademicYear; ex. summer = 1; fall = 2; winter = 3; spring = 4;
		Ordinal int,
		YearTermCode char(5),
		CONSTRAINT
			pk_UnivTerm
		PRIMARY KEY CLUSTERED
			(
				TermCode
			),
		INDEX
			ix_Term__AcademicYear
		NONCLUSTERED
			(
				AcademicYear
			)
	);

GO

WITH
	cte_data
	(
		millennium,
		century,
		decade,
		year,
		term
	)
AS
	(
		SELECT
			m.val,
			c.val,
			d.val,
			y.val,
			t.val
		FROM
			(VALUES ('1'), ('2')) m (val)
			cross apply
			(VALUES ('0'),('1'),('2'),('3'),('4'),('5'),('6'),('7'),('8'),('9')) c (val)
			cross apply
			(VALUES ('0'),('1'),('2'),('3'),('4'),('5'),('6'),('7'),('8'),('9')) d (val)
			cross apply
			(VALUES ('0'),('1'),('2'),('3'),('4'),('5'),('6'),('7'),('8'),('9')) y (val)
			cross apply
			(VALUES ('1'),('2'),('3'),('4')) t (val)
		WHERE
			m.val + c.val + d.val + y.val + t.val >= '19901'
			and m.val + c.val + d.val + y.val + t.val <= '20505'
	),
	cte_compute
	(
		Term,
		TermCode,
		CalendarYear,
		CalendarYearAbbr,
		Season,
		Rank
	)
AS
	(
		SELECT
			Term,
			TermCode = Millennium + Century + Decade + Year + Term,
			CalendarYear = Millennium + Century + Decade + Year,
			CalendarYearAbbr = Decade + Year,
			Season = 
				case
					when Term = '1' then 'Fall'
					when Term = '2' then 'Winter'
					when Term = '3' then 'Spring'
					when Term = '4' then 'Summer'
				end,
			Rank = 
				convert(
					tinyint,
					case
						when Term = '4' then 1
						when Term = '1' then 2
						when Term = '2' then 3
						when Term = '3' then 4
					end
				)
		FROM
			cte_data
	),
	cte_source
	(
		TermCode,
		AcademicYearTrailing,
		AcademicYearLeading,
		CalendarYear,
		CalendarYearAbbr,
		Season,
		Rank,
		Ordinal,
		YearTermCode
	)
AS
	(
		SELECT
			TermCode,
			AcademicYearTrailing = 
				convert(
					smallint,
					case
						when Term in ('1', '4') then CalendarYear
						when Term in ('2', '3') then convert(char(4), convert(smallint, CalendarYear) - 1)
					end
				),
			AcademicYearLeading = 
				convert(
					smallint,
					case
						when Term in ('1', '4') then CalendarYear
						when Term in ('2', '3') then convert(char(4), convert(smallint, CalendarYear) - 1)
					end + 1
				),
			CalendarYear,
			CalendarYearAbbr = substring(CalendarYear,3,2),
			Season,
			Rank,
			Ordinal = rank() over(order by convert(smallint, CalendarYear), Rank),
			TermCodeFullComis =
				CalendarYear + 
				case
					when Term = '1' then '7'
					when Term = '2' then '1'
					when Term = '3' then '3'
					when Term = '4' then '5'
				end
		FROM
			cte_compute
	)
MERGE
	dbo.UnivTerm t
USING
	cte_source s
ON
	t.TermCode = s.TermCode
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			TermCode,
			AcademicYear,
			AcademicYearAbbr,
			AcademicYearTrailing,
			AcademicYearStart,
			AcademicYearLeading,
			AcademicYearEnd,
			CalendarYear,
			CalendarYearAbbr,
			Season,
			Rank,
			Ordinal,
			YearTermCode
		)
	VALUES
		(
			s.TermCode,
			convert(char(4), s.AcademicYearTrailing) + '-' + convert(char(4), s.AcademicYearLeading),
			substring(convert(char(4), s.AcademicYearTrailing), 3, 2) + substring(convert(char(4), s.AcademicYearLeading), 3, 2),
			s.AcademicYearTrailing,
			s.AcademicYearTrailing,
			s.AcademicYearLeading,
			s.AcademicYearLeading,
			s.CalendarYear,
			s.CalendarYearAbbr,
			s.Season,
			s.Rank,
			s.Ordinal,
			YearTermCode
		)
WHEN MATCHED THEN
	UPDATE SET
		t.TermCode = s.TermCode,
		t.AcademicYear = convert(char(4), s.AcademicYearTrailing) + '-' + convert(char(4), s.AcademicYearLeading),
		t.AcademicYearAbbr = substring(convert(char(4), s.AcademicYearTrailing), 3, 2) + substring(convert(char(4), s.AcademicYearLeading), 3, 2),
		t.AcademicYearTrailing = s.AcademicYearTrailing,
		t.AcademicYearStart = s.AcademicYearTrailing,
		t.AcademicYearLeading = s.AcademicYearLeading,
		t.AcademicYearEnd = s.AcademicYearLeading,
		t.CalendarYear = s.CalendarYear,
		t.CalendarYearAbbr = s.CalendarYearAbbr,
		t.Season = s.Season,
		t.Rank = s.Rank,
		t.Ordinal = s.Ordinal,
		t.YearTermCode = s.YearTermCode;


/*

	MODIFICATIONS

*/

ALTER TABLE
	dbo.UnivTerm
ADD
	YearCode char(3);

ALTER TABLE
	dbo.UnivTerm
ADD
	YearCodePrev char(3);

ALTER TABLE
	dbo.UnivTerm
ADD
	YearCodeNext char(3);

GO

UPDATE
	ut
SET
	ut.YearCode = t.YearCode
FROM
	dbo.UnivTerm ut
	inner join
	comis.Term t
		on t.YearTermCode = ut.YearTermCode;

UPDATE
	t
SET
	t.YearCodePrev = s.YearCodePrev
FROM
	dbo.UnivTerm t
	inner join
	(
		SELECT DISTINCT
			d.AcademicYearLeadingCurr,
			YearCodePrev = c.YearCode
		FROM
			comis.Term c
			inner join
			(
				SELECT
					AcademicYearLeadingCurr = a.AcademicYearLeading,
					AcademicYearLeadingPrev = max(b.AcademicYearLeading)
				FROM
					dbo.UnivTerm a
					inner join
					dbo.UnivTerm b
						on b.AcademicYearLeading < a.AcademicYearLeading
				GROUP BY
					a.AcademicYearLeading
			) d
				on d.AcademicYearLeadingPrev = c.AcademicYearLeading
	) s
		on s.AcademicYearLeadingCurr = t.AcademicYearLeading;

UPDATE
	t
SET
	t.YearCodeNext = s.YearCodeNext
FROM
	dbo.UnivTerm t
	inner join
	(
		SELECT DISTINCT
			d.AcademicYearLeadingCurr,
			YearCodeNext = c.YearCode
		FROM
			comis.Term c
			inner join
			(
				SELECT
					AcademicYearLeadingCurr = a.AcademicYearLeading,
					AcademicYearLeadingNext = min(b.AcademicYearLeading)
				FROM
					dbo.UnivTerm a
					inner join
					dbo.UnivTerm b
						on b.AcademicYearLeading > a.AcademicYearLeading
				GROUP BY
					a.AcademicYearLeading
			) d
				on d.AcademicYearLeadingNext = c.AcademicYearLeading
	) s
		on s.AcademicYearLeadingCurr = t.AcademicYearLeading;