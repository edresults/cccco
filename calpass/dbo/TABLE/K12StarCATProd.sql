USE [calpass]
GO

/****** Object:  Table [dbo].[K12StarCATProd]    Script Date: 12/3/2015 8:13:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[K12StarCATProd](
	[derkey1] [char](15) NULL,
	[School] [char](14) NOT NULL,
	[AcYear] [char](4) NOT NULL,
	[LocStudentId] [char](15) NOT NULL,
	[CSISNum] [char](10) NULL,
	[ItemsAttemptRead] [char](2) NULL,
	[ItemsAttemptLang] [char](2) NULL,
	[ItemsAttemptSpell] [char](2) NULL,
	[ItemsAttemptMath] [char](2) NULL,
	[ItemsAttemptSci] [char](2) NULL,
	[IncludeIndRead] [char](1) NULL,
	[IncludeIndLang] [char](1) NULL,
	[IncludeIndSpell] [char](1) NULL,
	[IncludeIndMath] [char](1) NULL,
	[IncludeIndSci] [char](1) NULL,
	[ReadRaw] [char](2) NULL,
	[LangRaw] [char](2) NULL,
	[SpellRaw] [char](2) NULL,
	[MathRaw] [char](2) NULL,
	[SCiRaw] [char](2) NULL,
	[ReadSS] [char](3) NULL,
	[LangSS] [char](3) NULL,
	[SpellSS] [char](3) NULL,
	[MathSS] [char](3) NULL,
	[SciSS] [char](3) NULL,
	[ReadPR] [char](2) NULL,
	[LangPR] [char](2) NULL,
	[SpellPR] [char](2) NULL,
	[MathPR] [char](2) NULL,
	[SciPR] [char](2) NULL,
	[RecordType] [char](2) NULL,
	[UIN] [char](20) NULL,
	[StanineReading] [char](1) NULL,
	[StanineLanguage] [char](1) NULL,
	[StanineSpelling] [char](1) NULL,
	[StanineMath] [char](1) NULL,
	[NCEReading] [char](2) NULL,
	[NCELanguage] [char](2) NULL,
	[NCESpelling] [char](2) NULL,
	[NCEMath] [char](2) NULL,
	[batch_id] [int] NULL,
	[dateAdded] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


