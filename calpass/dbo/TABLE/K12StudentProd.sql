USE [calpass]
GO

/****** Object:  Table [dbo].[K12StudentProd]    Script Date: 12/3/2015 8:13:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[K12StudentProd](
	[derkey1] [char](15) NOT NULL,
	[School] [char](14) NOT NULL,
	[AcYear] [char](4) NOT NULL,
	[LocStudentId] [char](15) NOT NULL,
	[StudentId] [char](15) NULL,
	[CSISNum] [char](10) NULL,
	[Fname] [char](30) NULL,
	[Lname] [char](40) NULL,
	[Gender] [char](1) NULL,
	[Ethnicity] [char](3) NULL,
	[Birthdate] [char](8) NULL,
	[GradeLevel] [char](2) NULL,
	[HomeLanguage] [char](2) NULL,
	[HispanicEthnicity] [char](1) NULL,
	[EthnicityCode1] [char](3) NULL,
	[EthnicityCode2] [char](3) NULL,
	[EthnicityCode3] [char](3) NULL,
	[EthnicityCode4] [char](3) NULL,
	[EthnicityCode5] [char](3) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


