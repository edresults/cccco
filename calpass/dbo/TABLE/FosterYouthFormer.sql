IF (object_id('dbo.FosterYouthFormer') is not null)
	BEGIN
		DROP TABLE dbo.FosterYouthFormer;
	END;

GO

CREATE TABLE
	dbo.FosterYouthFormer
	(
		SchoolCode                    varchar(255),
		SchoolName                    varchar(255),
		SSID                          varchar(255),
		YearCode                      char(9),
		NameFirst                     varchar(255),
		NameLast                      varchar(255),
		LocalID                       varchar(255),
		DOB                           varchar(255),
		Gender                        varchar(255),
		GrdLvlCode                    varchar(255),
		StartDate                     varchar(255),
		EnrollmentStatusName          varchar(255),
		LocalMatchIndicator           varchar(255),
		StatewideMatchIndicator       varchar(255),
		CountyOfJurisdiction          varchar(255),
		ServiceType                   varchar(255),
		ClientID                      varchar(255),
		CaseID                        varchar(255),
		CaseStartDate                 varchar(255),
		LCFFFosterEndDate             varchar(255),
		EpisodeStartDate              varchar(255),
		EpisodeEndDate                varchar(255),
		SocialWorkerName              varchar(255),
		SocialWorkPhoneNo             varchar(255),
		ResponsibleAgency             varchar(255),
		ParentalRightLimitedIndicator varchar(255),
		CourtAppointedRepresentative  varchar(255),
		RepresentativePhoneNo         varchar(255),
		UpdateDate                    varchar(255),
		InterSegmentKey               binary(64)
	);

GO

CREATE CLUSTERED INDEX
	IC_FosterYouthFormer
ON
	calpass.dbo.FosterYouthFormer
	(
		SSID,
		YearCode,
		GrdLvlCode,
		ClientID
	);

GO

INSERT
	dbo.FosterYouthFormer
	(
		SchoolCode,
		SchoolName,
		SSID,
		YearCode,
		NameFirst,
		NameLast,
		LocalID,
		DOB,
		Gender,
		GrdLvlCode,
		StartDate,
		EnrollmentStatusName,
		LocalMatchIndicator,
		StatewideMatchIndicator,
		CountyOfJurisdiction,
		ServiceType,
		ClientID,
		CaseID,
		CaseStartDate,
		LCFFFosterEndDate,
		EpisodeStartDate,
		EpisodeEndDate,
		SocialWorkerName,
		SocialWorkPhoneNo,
		ResponsibleAgency,
		ParentalRightLimitedIndicator,
		CourtAppointedRepresentative,
		RepresentativePhoneNo,
		UpdateDate,
		InterSegmentKey
	)
SELECT
	SchoolCode,
	SchoolName,
	SSID,
	YearCode,
	NameFirst,
	NameLast,
	LocalID,
	DOB,
	Gender,
	GrdLvlCode,
	StartDate,
	EnrollmentStatusName,
	LocalMatchIndicator,
	StatewideMatchIndicator,
	CountyOfJurisdiction,
	ServiceType,
	ClientID,
	CaseID,
	CaseStartDate,
	LCFFFosterEndDate,
	EpisodeStartDate,
	EpisodeEndDate,
	SocialWorkerName,
	SocialWorkPhoneNo,
	ResponsibleAgency,
	ParentalRightLimitedIndicator,
	CourtAppointedRepresentative,
	RepresentativePhoneNo,
	UpdateDate,
	 InterSegmentKey = 
	 	HASHBYTES(
	 		'SHA2_512',
	 		upper(convert(nchar(3), NameFirst)) +
	 		upper(convert(nchar(3), NameLast)) +
	 		upper(convert(nchar(1), Gender)) +
	 		convert(nchar(8), convert(date, DOB), 112)
	 	)
FROM
	(
		SELECT
			SchoolCode,
			SchoolName,
			SSID = calpass.dbo.get9812Encryption(SSID, 'X'),
			YearCode = '2016-2017',
			NameFirst =
				substring(
					replace(StudentName, '"', ''),
					charindex(',', replace(StudentName, '"', '')) + 2,
					len(replace(StudentName, '"', ''))
				),
			NameLast =
				substring(
					replace(StudentName, '"', ''),
					1,
					charindex(',', replace(StudentName, '"', '')) - 1
				),
			LocalID,
			DOB,
			Gender,
			GrdLvlCode,
			StartDate,
			EnrollmentStatusName,
			LocalMatchIndicator,
			StatewideMatchIndicator,
			CountyOfJurisdiction,
			ServiceType,
			ClientID,
			CaseID,
			CaseStartDate,
			LCFFFosterEndDate,
			EpisodeStartDate,
			EpisodeEndDate,
			SocialWorkerName,
			SocialWorkPhoneNo,
			ResponsibleAgency,
			ParentalRightLimitedIndicator,
			CourtAppointedRepresentative,
			RepresentativePhoneNo,
			UpdateDate
		FROM
			OPENROWSET (
				BULK N'\\10.11.5.61\SFTPRoot\lausd\LAUSD_2017-06-05.CALPADSFormerFoster.txt',
				FORMATFILE = '\\10.11.5.61\SFTPRoot\lausd\FosterYouthFormer.fmt',
				FIRSTROW = 2
			) c
		UNION ALL
		SELECT
			SchoolCode,
			SchoolName,
			SSID = calpass.dbo.get9812Encryption(SSID, 'X'),
			YearCode = '2015-2016',
			NameFirst =
				substring(
					replace(StudentName, '"', ''),
					charindex(',', replace(StudentName, '"', '')) + 2,
					len(replace(StudentName, '"', ''))
				),
			NameLast =
				substring(
					replace(StudentName, '"', ''),
					1,
					charindex(',', replace(StudentName, '"', '')) - 1
				),
			LocalID,
			DOB,
			Gender,
			GrdLvlCode,
			StartDate,
			EnrollmentStatusName,
			LocalMatchIndicator,
			StatewideMatchIndicator,
			CountyOfJurisdiction,
			ServiceType,
			ClientID,
			CaseID,
			CaseStartDate,
			LCFFFosterEndDate = null,
			EpisodeStartDate,
			EpisodeEndDate,
			SocialWorkerName,
			SocialWorkPhoneNo,
			ResponsibleAgency,
			ParentalRightLimitedIndicator,
			CourtAppointedRepresentative,
			RepresentativePhoneNo,
			UpdateDate = null
		FROM
			OPENROWSET (
				BULK N'\\10.11.5.61\SFTPRoot\lausd\2016-06-01.CALPADSFormerFostera.txt',
				FORMATFILE = '\\10.11.5.61\SFTPRoot\lausd\FosterYouthFormera.fmt',
				FIRSTROW = 2
			) c
	) a