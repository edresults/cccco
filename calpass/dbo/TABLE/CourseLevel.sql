IF (object_id('dbo.CourseLevel') is not null)
	BEGIN
		DROP TABLE dbo.CourseLevel;
	END;

GO

CREATE TABLE
	dbo.CourseLevel
	(
		code VARCHAR(2),
		rank TINYINT,
		description VARCHAR(255),
		level_below TINYINT,
		CONSTRAINT
			pk_CourseLevel__code
			PRIMARY KEY
				(
					code
				)
	);

CREATE INDEX
	ix_CourseLevel__rank
ON
	dbo.CourseLevel
	(
		rank
	);

INSERT INTO
	CourseLevel
	(
		code,
		rank,
		description,
		level_below
	)
VALUES
	('Y',10,'Not Applicable',0),
	('A',9,'One level below transfer',1),
	('B',8,'Two levels below transfer',2),
	('C',7,'Three levels below transfer',3),
	('D',6,'Four levels below transfer',4),
	('E',5,'Five levels below transfer',5),
	('F',4,'Six levels below transfer',6),
	('G',3,'Seven levels below transfer',7),
	('H',2,'Eight levels below transfer',8);