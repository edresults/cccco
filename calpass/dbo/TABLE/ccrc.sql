USE calpass;

GO

IF (object_id('dbo.ccrc') is not null)
	BEGIN
		DROP TABLE dbo.ccrc;
	END;

GO

CREATE TABLE
	dbo.ccrc
	(
		student_id                varchar(9),
		organization_id           char(6),
		term_id                   char(8),
		student_uid               varchar(9),
		firstname                 varchar(255),
		lastname                  varchar(255),
		gender                    varchar(255),
		birthdate                 varchar(255),
		csisnumber                varchar(255),
		student_flag              varchar(255),
		assessment_id             varchar(255),
		assessmentcollegeready_id varchar(255),
		assessmentdate            varchar(255),
		assessmentperformance_id  varchar(255),
		disability_id             varchar(255),
		englishproficiency_id     varchar(255),
		rawscore                  varchar(255),
		studentlevel_id           varchar(255),
		teststudentlevel_id       varchar(255),
		scalescore                varchar(255),
		studentassessment_uid     varchar(255),
		percentilerank            varchar(255),
		assessmentpassed_flag     varchar(255),
		nslp_flag                 varchar(255),
		assessment_flag           varchar(255),
		studentaward_uid          varchar(255),
		award_id                  varchar(255),
		awarddate                 varchar(255),
		award_flag                varchar(255),
		LinkedLearning_uid        varchar(255),
		pathway_id                varchar(255),
		daysenrolled              varchar(255),
		dayssuspended             varchar(255),
		math8thgrade              varchar(255),
		eng8thgrade               varchar(255),
		attendance_count          varchar(255),
		freelunch                 varchar(255),
		attendance                varchar(255),
		englishproficient_id      varchar(255),
		Pathway                   varchar(255),
		IndustrySector            varchar(255),
		IndustrySector_category   varchar(255),
		PathwayType               varchar(255),
		DeliveryModel             varchar(255),
		DeliveryModel_category    varchar(255),
		pathwaycode               varchar(255),
		pathway_key               varchar(255),
		pathway_rank              varchar(255),
		linked_learning_flag      varchar(255),
		ethnicity_id              varchar(255),
		language_id               varchar(255),
		organizationstudentcode   varchar(255),
		unitsattempt              varchar(255),
		unitsearn                 varchar(255),
		studentorganization_uid   varchar(255),
		gender_id                 varchar(255),
		student_plan_id           varchar(255),
		student_org_flag          varchar(255),
		studentcourse_uid         varchar(255),
		course_id                 varchar(255),
		section_id                varchar(255),
		grade_id                  varchar(255),
		unitsattempt_courses      varchar(255),
		unitsearn_courses         varchar(255),
		student_course_flag       varchar(255)
	);