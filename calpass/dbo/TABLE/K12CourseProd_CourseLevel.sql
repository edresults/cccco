USE calpass;

GO

IF (object_id('calpass.dbo.K12CourseProd_CourseLevel') is not null)
	BEGIN
		DROP TABLE dbo.K12CourseProd_CourseLevel;
	END;

GO

CREATE TABLE
	dbo.K12CourseProd_CourseLevel
	(
		code VARCHAR(2),
		description VARCHAR(255) NOT NULL,
		rank TINYINT NOT NULL,
		CONSTRAINT
			pk_K12CourseProd_CourseLevel__code
		PRIMARY KEY CLUSTERED
			(
				code
			)
	);

INSERT INTO
	dbo.K12CourseProd_CourseLevel
	(
		code,
		description,
		rank
	)
VALUES
	('XX','Unknown',0),
	('30','Advanced Placement (AP)',6),
	('31','Basic (Pre K - Grade 12)',3),
	('32','General (Pre K - Grade 12)',4),
	('33','Gifted and talented',5),
	('34','Other honors (non-UC certified)',6),
	('35','Remedial (Pre K - Grade 12)',2),
	('36','Special Education',1),
	('37','International Baccalaureate (IB) - standard level',6),
	('38','International Baccalaureate (IB) - higher level',6),
	('39','UC certified honors',6),
	('40','College level',6);