USE calpass;

GO

IF (object_id('dbo.StudentHighSchoolAssessment') is not null)
	BEGIN
		DROP TABLE dbo.StudentHighSchoolAssessment;
	END;

GO

CREATE TABLE
	dbo.StudentHighSchoolAssessment
	(
		InterSegmentKey binary(64) not null,
		EnglScaledScore int,
		MathSubject char(1),
		MathScaledScore int
	);

GO

ALTER TABLE
	dbo.StudentHighSchoolAssessment
ADD CONSTRAINT
	pk_StudentHighSchoolAssessment__InterSegmentKey
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey
	);