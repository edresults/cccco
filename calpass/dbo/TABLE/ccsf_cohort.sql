USE calpass;

IF (object_id('dbo.ccsf_cohort', 'U') is not null)
	BEGIN
		DROP TABLE dbo.ccsf_cohort;
	END;

GO

CREATE TABLE
	dbo.ccsf_cohort
	(
		college_id char(3),
		student_id varchar(9),
		id_status char(1),
		birthdate char(8),
		gender char(1),
		high_school char(6),
		name_first varchar(30),
		name_last varchar(40),
		engp varchar(255),
		mthp varchar(255),
		algp varchar(255),
		clmp varchar(255),
		math_9_2015 varchar(255),
		derkey1 char(15),
		CONSTRAINT
			pk_ccsf_cohort__college_id__student_id__id_status
		PRIMARY KEY CLUSTERED
			(
				college_id,
				student_id,
				id_status
			)
	);

GO

CREATE INDEX
	ix_ccsf_cohort__derkey1
ON
	dbo.ccsf_cohort
	(
		derkey1
	);

GO

CREATE TRIGGER
	tr_ccsf_cohort__after_insert
ON
	dbo.ccsf_cohort
AFTER
	INSERT
AS

	SET NOCOUNT ON;

BEGIN

	UPDATE
		a
	SET
		a.derkey1 = calpass.dbo.derkey1(
			a.name_first,
			a.name_last,
			a.gender,
			a.birthdate
		)
	FROM
		calpass.dbo.ccsf_cohort a
		inner join
		inserted i
			on a.college_id = i.college_id
			and a.student_id = i.student_id
			and a.id_status = i.id_status;

END;