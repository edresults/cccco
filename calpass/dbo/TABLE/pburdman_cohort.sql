USE calpass;

GO

IF (object_id('dbo.pburdman_cohort') is not null)
	BEGIN
		DROP TABLE dbo.pburdman_cohort;
	END;

GO

CREATE TABLE
	dbo.pburdman_cohort
	(
		high_school char(14),
		math_rank_01_ind bit,
		math_rank_02_ind bit,
		math_rank_03_ind bit,
		math_rank_04_ind bit,
		math_rank_05_ind bit,
		math_rank_06_ind bit,
		math_rank_07_ind bit,
		math_rank_08_ind bit
	);