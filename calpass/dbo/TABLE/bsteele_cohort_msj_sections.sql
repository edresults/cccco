USE calpass;

GO

IF (object_id('calpass.dbo.bsteele_cohort_msj_sections') is not null)
	BEGIN
		DROP TABLE dbo.bsteele_cohort_msj_sections;
	END

GO

CREATE TABLE
	dbo.bsteele_cohort_msj_sections
	(
		student_id char(32),
		year_code char(4),
		term_id char(3),
		course_id varchar(12),
		section_id varchar(6),
		grade varchar(3),
		CONSTRAINT
			pk_bsteel_cohort_msj__student_id__year_code__term_id__course_id__section_id
		PRIMARY KEY CLUSTERED
			(
				student_id,
				year_code,
				term_id,
				course_id,
				section_id
			)
	);

GO

INSERT INTO
	calpass.dbo.bsteele_cohort_msj_sections
	(
		student_id,
		year_code,
		term_id,
		course_id,
		section_id,
		grade
	)
SELECT
	student_id,
	year_code,
	term_id,
	course_id,
	section_id,
	grade
FROM
	OPENROWSET(
		BULK N'\\nas-pro-dat-01\webfileuploads\FileDrop\170798\MSJ_DualEnrollment_Sections.txt',
		FORMATFILE = N'\\nas-pro-dat-01\webfileuploads\FileDrop\170798\MSJ_DualEnrollment_Sections.fmt'
	) a