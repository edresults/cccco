CREATE TABLE
    CustomMarks
    (
        OrganizationCode varchar(14) not null,
        SourceCode       varchar(2)  not null,
        TargetCode       varchar(3)  not null,
        -- PK
        CONSTRAINT CustomMarks_PK PRIMARY KEY CLUSTERED (OrganizationCode, SourceCode)
    );