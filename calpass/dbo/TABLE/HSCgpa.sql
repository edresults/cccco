USE calpass;

GO

IF (object_id('dbo.HSCgpa') is not null)
	BEGIN
		DROP TABLE dbo.HSCgpa;
	END;

GO

CREATE TABLE
	dbo.HSCgpa
	(
		InterSegmentKey binary(64) not null,
		LastGradeCode char(2) not null,
		TotalCreditTry decimal(15,2) not null,
		TotalCreditGet decimal(15,2) not null,
		TotalQualityPoints decimal(15,2) not null,
		Cgpa decimal(4,3) not null
	);

GO

ALTER TABLE
	dbo.HSCgpa
ADD CONSTRAINT
	pk_HSCgpa
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey
	);