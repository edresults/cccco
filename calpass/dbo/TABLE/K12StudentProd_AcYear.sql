USE calpass;

GO

IF (object_id('calpass.dbo.K12StudentProd_AcYear') is not null)
	BEGIN
		DROP TABLE dbo.K12StudentProd_AcYear;
	END;

GO

CREATE TABLE
	dbo.K12StudentProd_AcYear
	(
		code char(4),
		code_calpads char(9),
		year_trailing smallint,
		year_leading smallint,
		CONSTRAINT
			pk_K12StudentProd_AcYear__code
		PRIMARY KEY CLUSTERED
			(
				code
			)
	);

GO

CREATE INDEX
	ix_K12StudentProd_AcYear__code_calpads
ON
	dbo.K12StudentProd_AcYear
	(
		code_calpads
	);

GO

WITH
	cte
AS
	(
		SELECT
			year_code = d.decade + y.year
		FROM
			(
				VALUES
				(1990),
				(2000),
				(2010),
				(2020),
				(2030),
				(2040),
				(2050)
			) d (decade)
			cross join
			(
				VALUES
				(0),
				(1),
				(2),
				(3),
				(4),
				(5),
				(6),
				(7),
				(8),
				(9)
			) y (year)
	)
INSERT INTO
	dbo.K12StudentProd_AcYear
	(
		code,
		code_calpads,
		year_trailing,
		year_leading
	)
SELECT
	code = substring(cast(year_code as char(4)), 3, 2) + substring(cast(year_code + 1 as char(4)), 3, 2),
	code_calpads = cast(year_code as char(4)) + '-' + cast(year_code + 1 as char(4)),
	year_trailing = year_code,
	year_leading = year_code + 1
FROM
	cte;