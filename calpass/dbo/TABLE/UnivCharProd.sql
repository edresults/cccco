USE calpass;

GO

IF (object_id('UnivCharProd') is not null)
	BEGIN
		DROP TABLE dbo.UnivCharProd;
	END;

GO

CREATE TABLE
	dbo.UnivCharProd
	(
		School        char(6)     not null,
		YrTerm        char(5)     not null,
		CourseTitle   varchar(40) not null,
		Discipline    char(2)     null,
		Subdiscipline char(1)     null,
		RemedialCode  char(1)     null,
		GatewayCode   char(1)     null,
		CourseLevel   char(1)     null
	);

GO

ALTER TABLE
	dbo.UnivCharProd
ADD CONSTRAINT
	PK_UnivCharProd
PRIMARY KEY CLUSTERED
	(
		School,
		YrTerm,
		CourseTitle
	);