USE calpass;

GO

IF (object_id('dbo.k12courseprod_grade', 'U') is not null)
	BEGIN
		DROP TABLE dbo.k12courseprod_grade;
	END;

GO

CREATE TABLE
	dbo.k12courseprod_grade
	(
		code VARCHAR(5),
		points DECIMAL(2,1) NOT NULL,
		enroll_ind TINYINT NOT NULL,
		completion_ind TINYINT NOT NULL,
		completion_denominator_ind TINYINT NOT NULL,
		success_ind TINYINT NOT NULL,
		success_denominator_ind TINYINT NOT NULL,
		gpa_ind TINYINT NOT NULL,
		rank TINYINT NOT NULL UNIQUE,
		abbreviation VARCHAR(6) NOT NULL,
		description VARCHAR(255) NOT NULL,
		CONSTRAINT
			pk_k12courseprod_grade__code
		PRIMARY KEY CLUSTERED
		(
			code
		)
	);

CREATE INDEX
	ix_k12courseprod_grade__success_ind
ON
	dbo.k12courseprod_grade
	(
		success_ind
	);

CREATE INDEX
	ix_k12courseprod_grade__success_denominator_ind
ON
	dbo.k12courseprod_grade
	(
		success_denominator_ind
	);

CREATE INDEX
	ix_k12courseprod_grade__gpa_ind
ON
	dbo.k12courseprod_grade
	(
		gpa_ind
	);

INSERT INTO
	dbo.k12courseprod_grade
	(
		code,
		points,
		enroll_ind,
		completion_ind,
		completion_denominator_ind,
		success_ind,
		success_denominator_ind,
		gpa_ind,
		rank,
		abbreviation,
		description
	)
VALUES
	('A+',4,1,1,1,1,1,1,10,'A','Highest level of academic achievement where students demonstrate mastery of a subject'),
	('A',4,1,1,1,1,1,1,11,'A','Highest level of academic achievement where students demonstrate mastery of a subject'),
	('A*',4,1,1,1,1,1,1,12,'A','Highest level of academic achievement where students demonstrate mastery of a subject'),
	('A1',4,1,1,1,1,1,1,13,'A','Highest level of academic achievement where students demonstrate mastery of a subject'),
	('A?',4,1,1,1,1,1,1,14,'A','Highest level of academic achievement where students demonstrate mastery of a subject'),
	('A-',3.7,1,1,1,1,1,1,15,'A','Highest level of academic achievement where students demonstrate mastery of a subject'),
	('B+',3.3,1,1,1,1,1,1,20,'B','Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
	('B',3,1,1,1,1,1,1,21,'B','Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
	('B*',3,1,1,1,1,1,1,22,'B','Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
	('B_',3,1,1,1,1,1,1,23,'B','Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
	('B=',3,1,1,1,1,1,1,24,'B','Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
	('B?',3,1,1,1,1,1,1,25,'B','Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
	('B-',2.7,1,1,1,1,1,1,26,'B','Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
	('C+',2.3,1,1,1,1,1,1,30,'C','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
	('C',2,1,1,1,1,1,1,31,'C','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
	('C_',2,1,1,1,1,1,1,32,'C','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
	('C=',2,1,1,1,1,1,1,33,'C','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
	('C?',2,1,1,1,1,1,1,34,'C','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
	('C*',2,1,1,1,1,1,1,35,'CR','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
	('C-',1.7,1,1,1,1,1,1,36,'C','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
	('D+',1.3,1,1,1,0,1,1,40,'D','Fourth highest level of academic achievement where students demonstrate an inadequate understanding of a subject'),
	('D',1,1,1,1,0,1,1,41,'D','Fourth highest level of academic achievement where students demonstrate an inadequate understanding of a subject'),
	('D-',0.7,1,1,1,0,1,1,42,'D','Fourth highest level of academic achievement where students demonstrate an inadequate understanding of a subject'),
	('D*',1,1,1,1,0,1,1,43,'D','Fourth highest level of academic achievement where students demonstrate an inadequate understanding of a subject'),
	('D_',1,1,1,1,0,1,1,44,'D','Fourth highest level of academic achievement where students demonstrate an inadequate understanding of a subject'),
	('F+',0,1,1,1,0,1,1,50,'F','Lowest level of academic achievement where students demonstrate a substandard understanding of a subject'),
	('F-',0,1,1,1,0,1,1,51,'F','Lowest level of academic achievement where students demonstrate a substandard understanding of a subject'),
	('F*',0,1,1,1,0,1,1,52,'F','Lowest level of academic achievement where students demonstrate a substandard understanding of a subject'),
	('F',0,1,1,1,0,1,1,53,'F','Lowest level of academic achievement where students demonstrate a substandard understanding of a subject'),
	('F`',0,1,1,1,0,1,1,54,'F','Lowest level of academic achievement where students demonstrate a substandard understanding of a subject'),
	('CR',0,1,1,1,1,1,0,60,'CR','Credit/Pass'),
	('C R',0,1,1,1,1,1,0,61,'CR','Credit/Pass'),
	('P+',0,1,1,1,1,1,0,62,'CR','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
	('P',0,1,1,1,1,1,0,63,'CR','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
	('P-',0,1,1,1,1,1,0,64,'CR','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
	('NC',0,1,1,1,0,1,0,70,'NC','No credit'),
	('N C',0,1,1,1,0,1,0,71,'NC','No credit'),
	('N+',0,1,1,1,0,1,0,72,'NC','No credit'),
	('N',0,1,1,1,0,1,0,73,'NC','No credit'),
	('N-',0,1,1,1,0,1,0,74,'NC','No credit'),
	('NA',0,1,1,1,0,1,0,75,'NC','No credit'),
	('NP',0,1,1,1,0,1,0,76,'NC','No pass'),
	('NM',0,1,1,1,0,1,0,77,'NC','No credit'),
	('NI',0,1,1,1,0,1,0,78,'NC','No credit'),
	('I+',0,1,1,1,0,1,0,80,'I','Incomplete'),
	('I',0,1,1,1,0,1,0,81,'I','Incomplete'),
	('I-',0,1,1,1,0,1,0,82,'I','Incomplete'),
	('IA',0,1,1,1,1,1,0,83,'I','Incomplete'),
	('BI',0,1,1,1,1,1,0,84,'I','Incomplete'),
	('IB',0,1,1,1,1,1,0,85,'I','Incomplete'),
	('IC',0,1,1,1,1,1,0,86,'I','Incomplete'),
	('CI',0,1,1,1,1,1,0,87,'I','Incomplete'),
	('ICR',0,1,1,1,0,1,0,88,'I','Incomplete'),
	('INC',0,1,1,1,0,1,0,89,'I','Incomplete'),
	('DI',0,1,1,1,0,1,0,90,'I','Incomplete'),
	('IP',0,1,0,0,0,0,0,91,'I','Incomplete'),
	('ID',0,1,1,1,0,1,0,92,'I','Incomplete'),
	('IE',0,1,1,1,0,1,0,93,'I','Incomplete'),
	('IF',0,1,1,1,0,1,0,94,'I','Incomplete'),
	('FI',0,1,1,1,0,1,0,95,'I','Incomplete'),
	('IX',0,1,1,1,0,0,0,96,'I','Incomplete'),
	('I*',0,1,1,1,0,1,0,97,'I','Incomplete'),
	('AI',0,1,1,1,1,1,0,98,'I','Incomplete'),
	('INP',0,1,1,1,0,1,0,99,'I','Incomplete'),
	('IPP',0,1,1,1,0,1,0,100,'I','Incomplete'),
	('W',0,1,0,1,0,1,0,110,'W','Withdrawl'),
	('WA',0,1,0,1,0,1,0,111,'W','Withdrawl'),
	('WB',0,1,0,1,0,1,0,112,'W','Withdrawl'),
	('WC',0,1,0,1,0,1,0,113,'W','Withdrawl'),
	('WP',0,1,0,1,0,1,0,114,'W','Withdrawl'),
	('WD',0,1,0,1,0,1,0,115,'W','Withdrawl'),
	('WE',0,1,0,1,0,1,0,116,'W','Withdrawl'),
	('WF',0,1,0,1,0,1,0,117,'W','Withdrawl'),
	('FW',0,1,0,1,0,1,0,118,'W','Withdrawl'),
	('WU',0,1,0,1,0,1,0,119,'W','Withdrawl'),
	('WV',0,1,0,1,0,1,0,120,'W','Withdrawl'),
	('MW',0,1,0,0,0,0,0,130,'Other','Military withdraw'),
	('DR',0,0,0,0,0,0,0,140,'Drop','Dropped section before census date'),
	('NG',0,0,0,0,0,0,0,150,'Other','Other/Unknown grade'),
	('--',0,0,0,0,0,0,0,151,'Other','Other/Unknown grade'),
	('-',0,0,0,0,0,0,0,152,'Other','Other/Unknown grade'),
	(' -',0,0,0,0,0,0,0,153,'Other','Other/Unknown grade'),
	(' +',0,0,0,0,0,0,0,154,'Other','Other/Unknown grade'),
	('*-',0,0,0,0,0,0,0,155,'Other','Other/Unknown grade'),
	('*',0,0,0,0,0,0,0,156,'Other','Other/Unknown grade'),
	('*+',0,0,0,0,0,0,0,157,'Other','Other/Unknown grade'),
	('+',0,0,0,0,0,0,0,158,'Other','Other/Unknown grade'),
	('AU',0,0,0,0,0,0,0,159,'Other','Other/Unknown grade'),
	('CBE',0,0,0,0,0,0,0,160,'Other','Other/Unknown grade'),
	('CT',0,0,0,0,0,0,0,161,'Other','Other/Unknown grade'),
	('E-',0,0,0,0,0,0,0,162,'Other','Other/Unknown grade'),
	('E',0,0,0,0,0,0,0,163,'Other','Other/Unknown grade'),
	('E+',0,0,0,0,0,0,0,164,'Other','Other/Unknown grade'),
	('G-',0,0,0,0,0,0,0,165,'Other','Other/Unknown grade'),
	('G',0,0,0,0,0,0,0,166,'Other','Other/Unknown grade'),
	('G+',0,0,0,0,0,0,0,167,'Other','Other/Unknown grade'),
	('GD',0,0,0,0,0,0,0,168,'Other','Other/Unknown grade'),
	('M',0,0,0,0,0,0,0,169,'Other','Other/Unknown grade'),
	('NR',0,0,0,0,0,0,0,170,'Other','Other/Unknown grade'),
	('O-',0,0,0,0,0,0,0,171,'Other','Other/Unknown grade'),
	('O',0,0,0,0,0,0,0,172,'Other','Other/Unknown grade'),
	('O+',0,0,0,0,0,0,0,173,'Other','Other/Unknown grade'),
	('Other',0,0,0,0,0,0,0,174,'Other','Other/Unknown grade'),
	('RD',0,1,1,1,0,0,0,175,'Other','Report delayed'),
	('RP',0,0,0,0,0,0,0,176,'Other','Other/Unknown grade'),
	('S-',0,0,0,0,0,0,0,177,'Other','Other/Unknown grade'),
	('S',0,0,0,0,0,0,0,178,'Other','Other/Unknown grade'),
	('S+',0,0,0,0,0,0,0,179,'Other','Other/Unknown grade'),
	('SP',0,0,0,0,0,0,0,180,'Other','Other/Unknown grade'),
	('T',0,0,0,0,0,0,0,181,'Other','Other/Unknown grade'),
	('X',0,0,0,0,0,0,0,182,'Other','Other/Unknown grade'),
	('XX',0,0,0,0,0,0,0,183,'Other','Other/Unknown grade'),
	('Z',0,0,0,0,0,0,0,184,'Other','Other/Unknown grade'),
	('NULL',0,0,0,0,0,0,0,185,'Other','Other/Unknown grade'),
	('U',0,0,0,0,0,0,0,190,'Other','Ungraded class likely a non-credit course'),
	('UD',0,0,0,0,0,0,0,191,'Other','Ungraded class likely a non-credit course'),
	('UG',0,0,0,0,0,0,0,192,'Other','Ungraded class likely a non-credit course'),
	('UR',0,0,0,0,0,0,0,193,'Other','Ungraded class likely a non-credit course');