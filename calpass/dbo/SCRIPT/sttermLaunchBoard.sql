SET NOCOUNT ON;

DECLARE
	@Message     nvarchar(2048),
	@Time        datetime,
	@Seconds     int,
	@Severity    tinyint = 0,
	@State       tinyint = 1,
	@Error       varchar(2048),
	@Iterator    int = 0,
	@Iterations  int,
	@CollegeCode char(3),
	@College     comis.College;

INSERT
	@College
	(
		CollegeCode
	)
SELECT
	CollegeCode
FROM
	comis.College
ORDER BY
	CollegeCode;

SELECT
	@Iterations = count(*)
FROM
	@College;

BEGIN

	WHILE (@Iterator < @Iterations)
	BEGIN

		SET @Error = convert(varchar, @Iterator + 1) + ' of ' + convert(varchar, @Iterations);

		RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

		SET @Time = getdate();

		SELECT
			@CollegeCode = CollegeCode
		FROM
			@College
		WHERE
			Iterator = @Iterator;

		-- attributes for first term within year for student
		UPDATE
			st
		SET
			st.YearAgeCategoryId = s.YearAgeCategoryId
		FROM
			comis.stterm st
			inner join
			comis.Term t
				on t.termCode = st.term_id
			inner join
			(
				SELECT
					CollegeId     = st.college_id,
					StudentId     = st.student_id,
					YearCode      = t.YearCode,
					YearAgeCategoryId = a.AgeId
				FROM
					comis.stterm st
					inner join
					comis.Term t
						on t.termCode = st.term_id
					inner join
					lbswp.Age a
						on a.lowerbound <= st.age_at_term
						and a.upperbound >= st.age_at_term
				WHERE
					st.college_id = @CollegeCode
					and t.YearTermCode = (
						SELECT
							min(t1.YearTermCode)
						FROM
							comis.stterm st1
							inner join
							comis.Term t1
								on t1.termCode = st1.term_id
						WHERE
							st1.college_id = st.college_id
							and st1.student_id = st.student_id
							and t1.YearCode = t.YearCode
					)
			) s
				on st.college_id  = s.CollegeId
				and st.student_id = s.StudentId
				and t.YearCode    = s.YearCode
		WHERE
			st.college_id = @CollegeCode;

		-- attributes for last term within year for student
		UPDATE
			st
		SET
			st.YearGenderId = s.YearGenderId,
			st.YearRaceId   = s.YearRaceId
		FROM
			comis.stterm st
			inner join
			comis.Term t
				on t.termCode = st.term_id
			inner join
			(
				SELECT
					CollegeId = st.college_id,
					StudentId = st.student_id,
					YearCode  = t.YearCode,
					YearGenderId  = g.GenderId,
					YearRaceId    = r.RaceId
				FROM
					comis.stterm st
					inner join
					comis.Term t
						on t.termCode = st.term_id
					inner join
					lbswp.Gender g
						on g.GenderCode = st.gender
					inner join
					lbswp.Race r
						on r.RaceCode = st.ipeds_race
				WHERE
					st.college_id = @CollegeCode
					and t.YearTermCode = (
						SELECT
							max(t1.YearTermCode)
						FROM
							comis.stterm st1
							inner join
							comis.Term t1
								on t1.termCode = st1.term_id
						WHERE
							st1.college_id = st.college_id
							and st1.student_id = st.student_id
							and t1.YearCode = t.YearCode
					)
			) s
				on st.college_id  = s.CollegeId
				and st.student_id = s.StudentId
				and t.YearCode    = s.YearCode
		WHERE
			st.college_id = @CollegeCode;

		-- get time elapsed
		SET @Seconds = datediff(second, @Time, getdate());
		-- set msg
		SET @Error = 'ITERATION: ' + 
			convert(nvarchar, @Seconds / 86400) + ':' +
			convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
		-- output to user
		RAISERROR(@Error, 0, 1) WITH NOWAIT;

		SET @Iterator += 1;

	END;
END;