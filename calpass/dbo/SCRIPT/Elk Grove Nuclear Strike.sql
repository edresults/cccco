-- Legacy K12 Tables
DELETE
FROM
	calpass.dbo.K12StudentProd
WHERE
	left(School, 7) = '3467314';

DELETE
FROM
	calpass.dbo.K12CourseProd
WHERE
	left(School, 7) = '3467314';

DELETE
FROM
	calpass.dbo.K12AwardProd
WHERE
	left(School, 7) = '3467314';

DELETE
FROM
	calpass.dbo.K12CTEProd
WHERE
	left(School, 7) = '3467314';

DELETE
FROM
	calpass.dbo.K12PRGProd
WHERE
	left(School, 7) = '3467314';

-- CAASPP
DELETE
FROM
	calpass.dbo.K12CAASPPDemProd
WHERE
	left(School, 7) = '3467314';

DELETE
FROM
	calpass.dbo.K12CAASPPTestProd
WHERE
	left(School, 7) = '3467314';

-- STAR
DELETE
FROM
	calpass.dbo.K12StarCSTProd
WHERE
	left(School, 7) = '3467314';

DELETE
FROM
	calpass.dbo.K12StarCATProd
WHERE
	left(School, 7) = '3467314';

DELETE
FROM
	calpass.dbo.K12StarDemProd
WHERE
	left(School, 7) = '3467314';

-- CAHSEE
DELETE
FROM
	calpass.dbo.CAHSEEMath
WHERE
	left(SchoolIdentifier, 7) = '3467314';

DELETE
FROM
	calpass.dbo.CAHSEEEnglish
WHERE
	left(SchoolIdentifier, 7) = '3467314';

DELETE
FROM
	calpass.dbo.CAHSEEDemographics
WHERE
	left(SchoolIdentifier, 7) = '3467314';