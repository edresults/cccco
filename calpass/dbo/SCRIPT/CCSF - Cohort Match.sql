USE calpass;

SET ANSI_WARNINGS OFF;

DECLARE
	@Q01 varchar(255) = 'Number of CCSF students in cohort:  ',
	@Q02 varchar(255) = char(9) + 'Number CCSF students enrolled to any high school: ',
	@Q03 varchar(255) = char(9) + 'Number CCSF students enrolled to any San Francisco Unified high school: ',
	@A01 int,
	@A02 int,
	@A03 int;

SELECT
	@A01 = count(distinct c.Derkey1)
FROM
	calpass.dbo.ccsf_cohort c;

SELECT
	@A02 = count(distinct c.Derkey1)
FROM
	calpass.dbo.ccsf_cohort c
WHERE
	exists (
		SELECT
			1
		FROM
			calpass.dbo.K12StudentProd hss
			inner join
			calpass.dbo.K12CourseProd hsc
				on hss.School = hsc.School
				and hss.LocStudentId = hsc.LocStudentId
				and hss.AcYear = hsc.AcYear
		WHERE
			c.derkey1 = hss.derkey1
			and hss.gradeLevel in ('09','10','11','12')
	);

SELECT
	@A03 = count(distinct c.Derkey1)
FROM
	calpass.dbo.ccsf_cohort c
WHERE
	exists (
		SELECT
			1
		FROM
			calpass.dbo.K12StudentProd hss
			inner join
			calpass.dbo.K12CourseProd hsc
				on hss.School = hsc.School
				and hss.LocStudentId = hsc.LocStudentId
				and hss.AcYear = hsc.AcYear
			inner join
			calpass.dbo.organization o
				on o.organizationCode = hss.school
			inner join
			calpass.dbo.organization d
				on d.organizationId = o.ParentId
		WHERE
			c.derkey1 = hss.derkey1
			and d.organizationName = 'San Francisco Unified'
			and hss.gradeLevel in ('09','10','11','12')
	);

PRINT @Q01 + cast(@A01 as varchar(255))
PRINT @Q02 + case
		when @A02 is null then 'Unknown'
		else cast(@A02 as varchar(255)) + ' (' + cast(cast(100.00 * @A02 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;
PRINT @Q03 + case
		when @A03 is null then 'Unknown'
		else cast(@A03 as varchar(255)) + ' (' + cast(cast(100.00 * @A03 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;

SET ANSI_WARNINGS ON;