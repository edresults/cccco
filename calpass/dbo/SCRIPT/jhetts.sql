CREATE TABLE
#StudentRuleset
(
	InterSegmentKey binary(64),
	CourseCode varchar(30),
	PRIMARY KEY
		(
			InterSegmentKey,
			CourseCode
		)
);

CREATE TABLE
#Placement
(
	InterSegmentKey binary(64) primary key,
	engl_cb21 char (1),
	read_cb21 char (1),
	esl_cb21 char (1),
	math_cb21 char (1),
	math_geo char (1),
	math_alg char (1),
	math_ge char (1),
	math_stat char (1),
	math_pre_calc char (1),
	math_trig char (1),
	math_col_alg char (1),
	math_calc_i char (1)
);

CREATE TABLE
#Star
(
	InterSegmentKey binary(64) primary key,
	engl_eap_ind tinyint,
	engl_scaled_score int,
	math_subject tinyint,
	math_eap_ind tinyint,
	math_scaled_score int,
	esl_ind tinyint
);

CREATE TABLE
#StudentInputs
(
	InterSegmentKey binary(64) primary key,
	grade_level char(2),
	gpa_cum decimal(4,3),
	english decimal(3,2),
	english_ap decimal(3,2),
	pre_alg decimal(3,2),
	pre_alg_ap decimal(3,2),
	alg_i decimal(3,2),
	alg_i_ap decimal(3,2),
	alg_ii decimal(3,2),
	alg_ii_ap decimal(3,2),
	geo decimal(3,2),
	geo_ap decimal(3,2),
	trig decimal(3,2),
	trig_ap decimal(3,2),
	pre_calc decimal(3,2),
	pre_calc_ap decimal(3,2),
	calc decimal(3,2),
	calc_ap decimal(3,2),
	stat decimal(3,2),
	stat_ap decimal(3,2),
	esl_ind tinyint
);

CREATE TABLE
#Ruleset
(
	Entity varchar(255) not null,
	Attribute varchar(255) not null,
	Value decimal(6,3) not null,
	BranchId int not null,
	LeafCount int not null,
	CourseCode varchar(255) not null,
	PRIMARY KEY CLUSTERED
		(
			Entity,
			Attribute,
			Value,
			BranchId
		)
);

CREATE TABLE
	#Student
	(
		InterSegmentKey binary(64) primary key
	);

BEGIN
	
	INSERT INTO
		#Student
		(
			InterSegmentKey
		)
	SELECT
		derkey1
	FROM
		dbo.jhetts_cc_engl
	UNION
	SELECT
		derkey1
	FROM
		dbo.jhetts_cc_math
	UNION
	SELECT
		derkey1
	FROM
		dbo.jhetts_csu_engl
	UNION
	SELECT
		derkey1
	FROM
		dbo.jhetts_csu_math;

	-- set ruleset
	INSERT
		#Ruleset
		(
			Entity,
			Attribute,
			Value,
			BranchId,
			CourseCode,
			LeafCount
		)
	EXECUTE mmap.RulesetSelect
		@OrganizationId = null;

	INSERT INTO
		#StudentRuleset
		(
			InterSegmentKey,
			CourseCode
		)
	SELECT DISTINCT
		h.InterSegmentKey,
		CourseCode
	FROM
		#Ruleset r
		inner join
		dbo.HSTranscriptSummaryHash h
			on r.Entity = h.Entity
			and r.Attribute = h.Attribute
	WHERE
		exists (
			SELECT
				1
			FROM
				#Student s
			WHERE
				s.InterSegmentKey = h.InterSegmentKey
		)
	GROUP BY
		h.InterSegmentKey,
		CourseCode,
		r.BranchId,
		r.LeafCount
	HAVING
		sum(case when h.Value >= r.Value then 1 else 0 end) = r.LeafCount;

	INSERT INTO
		#Placement
		(
			InterSegmentKey,
			engl_cb21,
			read_cb21,
			esl_cb21,
			math_cb21,
			math_geo,
			math_alg,
			math_ge,
			math_stat,
			math_pre_calc,
			math_trig,
			math_col_alg,
			math_calc_i
		)
	SELECT
		InterSegmentKey,
		engl_cb21 = 
			coalesce(
				max(case when CourseCode = 'EnglishCollege' then 'Y' end), 
				max(case when CourseCode = 'EnglishLevelOne' then 'A' end),
				max(case when CourseCode = 'EnglishLevelTwo' then 'B' end),
				max(case when CourseCode = 'EnglishLevelThree' then 'C' end)
			),
		read_cb21 = 
			coalesce(
				max(case when CourseCode = 'ReadingTestOut' then 'M' end), 
				max(case when CourseCode = 'ReadingCollege' then 'Y' end),
				max(case when CourseCode = 'ReadingLevelOne' then 'A' end),
				max(case when CourseCode = 'ReadingLevelTwo' then 'B' end),
				max(case when CourseCode = 'ReadingLevelThree' then 'C' end)
			),
		esl_cb21 = 
			coalesce(
				max(case when CourseCode = 'EslCollege' then 'Y' end),
				max(case when CourseCode = 'EslLevelOne' then 'A' end),
				max(case when CourseCode = 'EslLevelTwo' then 'B' end),
				max(case when CourseCode = 'EslLevelThree' then 'C' end)
			),
		math_cb21 = 
			coalesce(
				max(case when CourseCode = 'CalculusI' then 'Y' end),
				max(case when CourseCode = 'PreCalculus' then 'Y' end),
				max(case when CourseCode = 'Trigonometry' then 'Y' end),
				max(case when CourseCode = 'CollegeAlgebra' then 'Y' end),
				max(case when CourseCode = 'Statistics' then 'Y' end),
				max(case when CourseCode = 'MathGE' then 'Y' end),
				max(case when CourseCode = 'AlgebraII' then 'A' end),
				max(case when CourseCode = 'AlgebraI' then 'B' end),
				max(case when CourseCode = 'PreAlgebra' then 'C' end)
			),
		math_geo = null,
		math_alg = max(case when CourseCode = 'AlgebraII' then 'A' end),
		math_ge = max(case when CourseCode = 'MathGE' then 'Y' end),
		math_stat = max(case when CourseCode = 'Statistics' then 'Y' end),
		math_pre_calc = max(case when CourseCode = 'PreCalculus' then 'Y' end),
		math_trig = max(case when CourseCode = 'Trigonometry' then 'Y' end),
		math_col_alg = max(case when CourseCode = 'CollegeAlgebra' then 'Y' end),
		math_calc_i = max(case when CourseCode = 'CalculusI' then 'Y' end)
	FROM
		#StudentRuleset
	GROUP BY
		InterSegmentKey;

	INSERT INTO
		#StudentInputs
		(
			InterSegmentKey,
			grade_level,
			gpa_cum,
			english,
			english_ap,
			pre_alg,
			pre_alg_ap,
			alg_i,
			alg_i_ap,
			alg_ii,
			alg_ii_ap,
			geo,
			geo_ap,
			trig,
			trig_ap,
			pre_calc,
			pre_calc_ap,
			calc,
			calc_ap,
			stat,
			stat_ap,
			esl_ind
		)
	SELECT
		InterSegmentKey,
		grade_level = max(case when Entity = 'Cgpa' then Attribute end),
		gpa_cum = max(case when Entity = 'Cgpa' then Value end),
		english = 
			coalesce(
				max(case when Entity = 'LiteratureAP' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'LanguageAP' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'Expository' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'English12' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'English11' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'English10' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'English09' and Attribute = 'MarkPoints' then Value end)
			),
		english_ap = 
			coalesce(
				max(case when Entity = 'LiteratureAP' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'LanguageAP' and Attribute = 'MarkPoints' then Value end)
			),
		pre_alg = max(case when Entity = 'PreAlgebra' and Attribute = 'MarkPoints' then Value end),
		pre_alg_ap = null,
		alg_i = max(case when Entity = 'AlgebraI' and Attribute = 'MarkPoints' then Value end),
		alg_i_ap = null,
		alg_ii = max(case when Entity = 'AlgebraII' and Attribute = 'MarkPoints' then Value end),
		alg_ii_ap = null,
		geo = max(case when Entity = 'Geonometry' and Attribute = 'MarkPoints' then Value end),
		geo_ap = null,
		trig = max(case when Entity = 'Trigonometry' and Attribute = 'MarkPoints' then Value end),
		trig_ap = null,
		pre_calc = max(case when Entity = 'PreCalculus' and Attribute = 'MarkPoints' then Value end),
		pre_calc_ap = null,
		calc = 
			coalesce(
				max(case when Entity = 'CalculusIIAP' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'CalculusIAP' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'CalculusII' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'CalculusI' and Attribute = 'MarkPoints' then Value end)
			),
		calc_ap = 
			coalesce(
				max(case when Entity = 'CalculusIIAP' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'CalculusIAP' and Attribute = 'MarkPoints' then Value end)
			),
		stat = max(case when Entity = 'Statistics' and Attribute = 'MarkPoints' then Value end),
		stat_ap = max(case when Entity = 'StatisticsAP' and Attribute = 'MarkPoints' then Value end),
		esl_ind = max(case when Entity = 'Remedial' then 1 else 0 end)
	FROM
		dbo.HSTranscriptSummaryHash h
	WHERE
		exists (
			SELECT
				1
			FROM
				#Student s
			WHERE
				s.InterSegmentKey = h.InterSegmentKey
		)
	GROUP BY
		InterSegmentKey;

	SELECT
		si.InterSegmentKey,
		MatchType = 'Match' + 
			case
				when coalesce(
						math_calc_i,
						math_pre_calc,
						math_stat,
						math_col_alg,
						math_ge,
						math_alg,
						math_geo,
						math_cb21
					) is not null then '; Mathematics Placement'
				else '; No Mathematics Placement'
			end + 
			case
				when engl_cb21 is not null then '; English Placement'
				else '; No English Placement'
			end + 
			case
				when convert(tinyint, grade_level) < 11 then '; No 11th or 12th Grade Data'
				else '' + 
					case
						when english is null then '; No 11th or 12th Grade English Data'
						else ''
					end + 
					case
						when (pre_alg is null
							and alg_i is null
							and alg_ii is null
							and geo is null
							and trig is null
							and pre_calc is null
							and calc is null
							and calc_ap is null
							and stat is null
							and stat_ap is null) then '; No Course Specific Mathematics Data'
						else ''
					end + 
					case
						when p.InterSegmentKey is null then '; No CST Data'
						else ''
					end
			end,
		p.engl_cb21,
		p.read_cb21,
		p.esl_cb21,
		p.math_cb21,
		p.math_geo,
		p.math_alg,
		p.math_ge,
		p.math_stat,
		p.math_pre_calc,
		p.math_trig,
		p.math_col_alg,
		p.math_calc_i,
		si.grade_level,
		si.gpa_cum,
		ap_any =
			coalesce(
				si.english_ap,
				si.stat_ap,
				si.calc_ap
			),
		si.english,
		si.english_ap,
		si.pre_alg,
		si.pre_alg_ap,
		si.alg_i,
		si.alg_i_ap,
		si.alg_ii,
		si.alg_ii_ap,
		si.geo,
		si.geo_ap,
		si.trig,
		si.trig_ap,
		si.pre_calc,
		si.pre_calc_ap,
		si.calc,
		si.calc_ap,
		si.stat,
		si.stat_ap,
		s.engl_eap_ind,
		s.engl_scaled_score,
		s.math_subject,
		s.math_eap_ind,
		s.math_scaled_score,
		esl_ind =
			case
				when si.esl_ind = 1 or s.esl_el_ind = 1 then 1
				else 0
			end
	INTO
		#plmt
	FROM
		#Placement p
		inner join
		#StudentInputs si
			on si.InterSegmentKey = p.InterSegmentKey
		left outer join
		mmap.assess.student_star s
			on s.derkey1 = p.InterSegmentKey;

	CREATE CLUSTERED INDEX ixc_plmt ON #plmt (InterSegmentKey);

SELECT * FROM #StudentInputs WHERE InterSegmentKey = 0x0D90BD8A41C4669847512ED6C7D748E5A47F05B5E835B640E404E919C845835FEA1F36BACD5FA513E1F8283E03780D18BE6CCF868D9CC9C5347CF2DF9ECB38DF

	UPDATE
		t
	SET
		t.engl_cb21 = s.engl_cb21,
		t.read_cb21 = s.read_cb21,
		t.esl_cb21 = s.esl_cb21,
		t.math_cb21 = s.math_cb21
	FROM
		calpass.dbo.jhetts_cc_engl t
		inner join
		#plmt s
			on t.Derkey1 = s.InterSegmentKey;

	UPDATE
		t
	SET
		t.engl_cb21 = s.engl_cb21,
		t.read_cb21 = s.read_cb21,
		t.esl_cb21 = s.esl_cb21,
		t.math_cb21 = s.math_cb21
	FROM
		calpass.dbo.jhetts_cc_math t
		inner join
		#plmt s
			on t.Derkey1 = s.InterSegmentKey;

	UPDATE
		t
	SET
		t.engl_cb21 = s.engl_cb21,
		t.read_cb21 = s.read_cb21,
		t.esl_cb21 = s.esl_cb21,
		t.math_cb21 = s.math_cb21
	FROM
		calpass.dbo.jhetts_csu_math t
		inner join
		#plmt s
			on t.Derkey1 = s.InterSegmentKey;

	UPDATE
		t
	SET
		t.engl_cb21 = s.engl_cb21,
		t.read_cb21 = s.read_cb21,
		t.esl_cb21 = s.esl_cb21,
		t.math_cb21 = s.math_cb21
	FROM
		calpass.dbo.jhetts_csu_engl t
		inner join
		#plmt s
			on t.Derkey1 = s.InterSegmentKey;
END;