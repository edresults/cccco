USE WagesCA;

GO

IF (object_id('dbo.EncryptedYearQtrWages') is not null)
	BEGIN
		DROP TABLE dbo.EncryptedYearQtrWages;
	END;

GO

-- create
CREATE TABLE
	dbo.EncryptedYearQtrWages
	(
		EncryptedSSN binary(64) not null,
		YearQtr      char(5)    not null,
		Wages        int        not null
	);

GO

-- constrain
ALTER TABLE
	dbo.EncryptedYearQtrWages
ADD CONSTRAINT
	PK_EncryptedYearQtrWages
PRIMARY KEY CLUSTERED
	(
		EncryptedSSN,
		YearQtr
	);

GO

-- populate
INSERT
	dbo.EncryptedYearQtrWages
	(
		EncryptedSSN,
		YearQtr,
		Wages
	)
SELECT
	EncryptedSSN = StudentIdSsnEnc,
	YearQtr      = convert(char(4), YearId) + convert(char(1), QtrId),
	Wages        = sum(Wages)
FROM
	calpass.dbo.Eddui
GROUP BY
	StudentIdSsnEnc,
	YearId,
	QtrId;