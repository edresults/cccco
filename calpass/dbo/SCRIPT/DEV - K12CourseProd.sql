MERGE
	K12CourseProd t
USING
	(
		SELECT DISTINCT
			Derkey1                      = k.InterSegmentKey,
			School                       = s.DistrictCode + s.SchoolCode,
			AcYear                       = y.YearCodeAbbr,
			LocStudentId                 = calpass.dbo.get1289Encryption(p.StudentLocalId, 'X'),
			CourseId                     = c.CourseCode,
			LocallyAssignedCourseNumber  = c.CourseId,
			CourseSectionClassNumber     = s.SectionId,
			CourseTitle                  = substring(c.CourseName, 1, 40),
			AGstatus                     = s.UniversityCode,
			Grade                        = s.MarkCode,
			CreditEarned                 = isnull(s.CreditGet, 0.00),
			CreditAttempted              = isnull(s.CreditTry, 0.00),
			CourseLevel = 
				case
					when c.SectionLevelCode = '10' then '35'
					when c.SectionLevelCode = '12' then '33'
					when c.SectionLevelCode = '14' then '39'
					when c.SectionLevelCode = '15' then '34'
					when c.SectionLevelCode = '16' then '40'
					when c.CourseCode in ('2160','2161','2173','2174','2175','2260','2261','2262','2263','2264','2265','2281','2282','2283','2360','2361','2362','2460','2461','2462','2463','2464','2465','2466','2467','2468','2469','2473','2479','2484','2485','2560','2660','2661','2662','2664','2665','2666','2667','2668','2760','2761','2762','2763','2764','2765','2766','2767','2768','2769','2779','2780','2860','2861','2960','2961','2962') then '38'
					when c.CourseCode in ('2170','2171','2270','2271','2272','2273','2274','2275','2276','2277','2278','2279','2370','2470','2471','2472','2480','2481','2483','2670','2671','2672','2673','2674','2675','2676','2770','2771','2772','2773','2774','2775','2776','2777','2778','2825','2826','2870','2874','2875','2876') then '30'
					when c.CourseCode = '2402' then '35'
					else 'XX'
				end,
			CourseType = 
				case
					when c.IsCte = 'Y' and c.ProgramFundingSource = '1' then '33'
					when c.IsCte = 'N' and c.ProgramFundingSource = '1' then '30'
					when c.IsCte = 'Y' and c.ProgramFundingSource != '1' then '32'
					else 'XX'
				end,
			CourseTerm                   = coalesce(s.MarkingPeriodCode, s.TermCode),
			DateAdded                    = CURRENT_TIMESTAMP
		FROM
			calpads.scsc s
			inner join
			calpads.Year y
				on y.YearCode = s.YearCode
			inner join
			calpads.crsc c
				on c.SectionId = s.SectionId
				and c.CourseId = s.CourseId
				and c.TermCode = s.TermCode
				and c.YearCode = s.YearCode
				and c.SchoolCode = s.SchoolCode
			inner join
			calpads.sinf p
				on p.StudentStateId = s.StudentStateId
				and p.DistrictCode = s.DistrictCode
			cross apply
			dbo.InterSegmentKey(p.NameFirst, p.NameLast, p.Gender, p.Birthdate) k
		WHERE
			p.EffectiveStartDate = (
				SELECT
					max(p1.EffectiveStartDate)
				FROM
					calpads.sinf p1
				WHERE
					p1.StudentStateId = p.StudentStateId
					and p1.DistrictCode = p.DistrictCode
			)
			and s.IsEscrow = 1
			and coalesce(s.MarkingPeriodCode, s.TermCode) is not null
			and s.MarkCode is not null
			and p.StudentLocalId is not null
	) s
ON
	(
		s.School = t.School
		and s.LocStudentId = t.LocStudentId
		and s.AcYear = t.AcYear
		and s.CourseTerm = t.CourseTerm
		and s.LocallyAssignedCourseNumber = t.LocallyAssignedCourseNumber
		and s.CourseSectionClassNumber = t.CourseSectionClassNumber
		and s.Grade = t.Grade
		and s.CreditAttempted = t.CreditAttempted
	)
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			Derkey1,
			School,
			AcYear,
			LocStudentId,
			CourseId,
			LocallyAssignedCourseNumber,
			CourseSectionClassNumber,
			CourseTitle,
			AGstatus,
			Grade,
			CreditEarned,
			CreditAttempted,
			CourseLevel,
			CourseType,
			CourseTerm,
			DateAdded
		)
	VALUES
		(
			s.Derkey1,
			s.School,
			s.AcYear,
			s.LocStudentId,
			s.CourseId,
			s.LocallyAssignedCourseNumber,
			s.CourseSectionClassNumber,
			s.CourseTitle,
			s.AGstatus,
			s.Grade,
			s.CreditEarned,
			s.CreditAttempted,
			s.CourseLevel,
			s.CourseType,
			s.CourseTerm,
			s.DateAdded
		);