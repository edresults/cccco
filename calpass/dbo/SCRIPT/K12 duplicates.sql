SELECT
	*
FROM
	calpads.scsc s
WHERE
	s.district_code = '1062166'
	and s.year_code = '2014-2015'
	and s.section_id = '9606'
	and exists (
		SELECT
			1
		FROM
			calpads.scsc s1
		WHERE
			s1.district_code = s.district_code
			and s1.year_code = s.year_code
		GROUP BY
			s1.district_code,
			s1.ssid,
			s1.year_code,
			s1.term_code,
			s1.course_id,
			s1.section_id,
			s1.marking_period_code,
			s1.school_code
		HAVING
			count(*) > 1
			and s1.district_code = s.district_code 
			and s1.ssid = s.ssid 
			and s1.year_code = s.year_code 
			and s1.term_code = s.term_code 
			and s1.course_id = s.course_id 
			and s1.section_id = s.section_id 
			and s1.marking_period_code = s.marking_period_code
			and s1.school_code = s.school_code 
	)
ORDER BY
	ssid;

SELECT
	*
FROM
	K12CourseProd s
WHERE
	s.school = '10621661030675'
	and s.AcYear = '1415'
	and s.CourseSectionClassNumber = '9606'
	and exists (
		SELECT
			1
		FROM
			K12CourseProd s1
		WHERE
			s1.school = s1.school
			and s1.AcYear = s.AcYear
		GROUP BY
			s1.School,
			s1.LocStudentId,
			s1.AcYear,
			s1.LocallyAssignedCourseNumber,
			s1.CourseSectionClassNumber,
			s1.CourseTerm
		HAVING
			count(*) > 1
			and s1.School = s.School
			and s1.LocStudentId = s.LocStudentId
			and s1.AcYear = s.AcYear
			and s1.LocallyAssignedCourseNumber = s.LocallyAssignedCourseNumber
			and s1.CourseSectionClassNumber = s.CourseSectionClassNumber
			and s1.CourseTerm = s.CourseTerm
	);