TRUNCATE TABLE dbo.StudentHighSchoolEngl;

GO

INSERT INTO
	dbo.StudentHighSchoolEngl
	(
		InterSegmentKey,
		English11,
		English12
	)
SELECT
	InterSegmentKey,
	English11 = max(case when GradeLevelCode = '11' then SectionGradePoints end),
	English12 = max(case when GradeLevelCode = '12' then SectionGradePoints end)
FROM
	(
		SELECT
			sb.InterSegmentKey,
			GradeLevelCode = s.GradeLevel,
			SectionGradepoints = g.points,
			RowSelector = row_number() over(
					PARTITION by
						sb.InterSegmentKey,
						s.GradeLevel
					ORDER BY
						y.YearTrailing DESC,
						t.PreCollegiateSeasonRank DESC,
						crs.IsAG DESC,
						crs.ContentAreaIntraRank DESC,
						c.CreditAttempted DESC,
						g.rank ASC
				)
		FROM
			calpads.Student sb
				with(index(ix_Student__Derkey1))
			inner join
			dbo.K12StudentProd s
				with(index(ixc_K12StudentProd__School__LocStudentId__AcYear))
				on sb.Derkey1 = s.Derkey1
			inner join
			dbo.K12CourseProd c
				with(index(ixc_K12CourseProd__School__LocStudentId__AcYear))
				on s.School = c.School
				and s.LocStudentId = c.LocStudentId
				and s.AcYear = c.AcYear
			inner join
			comis.Grade g -- no defined grade values for calpads; use COMIS
				with(index(pk_Grade__GradeCode))
				on g.GradeCode = c.Grade
			inner join
			calpads.Term t
				with(index(pk_Term__TermCode))
				on t.TermCode = c.CourseTerm
			inner join
			calpads.Course crs
				with(index(ixc_Course__CourseCode))
				on crs.CourseCode = c.CourseId
			inner join
			calpads.Year y
				with(index(ix_Year__YearCodeAbbr))
				on y.YearCodeAbbr = s.AcYear
		WHERE
			s.GradeLevel in ('11','12')
			and g.gpa_ind = 1
			and crs.ContentAreaCode = '14'
	) a
WHERE
	RowSelector != 1
GROUP BY
	InterSegmentKey;