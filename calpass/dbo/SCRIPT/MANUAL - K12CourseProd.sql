MERGE
	calpass.dbo.K12CourseProd t
USING
	(
		SELECT
			School,
			AcYear,
			LocStudentId = calpass.dbo.Get1289Encryption(LocStudentId, ''),
			CourseId,
			LocallyAssignedCourseNumber,
			CourseSectionClassNumber,
			CourseTitle,
			AGstatus,
			Grade,
			CreditEarned,
			CreditAttempted,
			CourseLevel,
			CourseType,
			CourseTerm,
			DateAdded = CURRENT_TIMESTAMP
		FROM
			OPENROWSET(
				BULK N'\\10.11.4.21\WebFileUploads\RawFiles\New\158388\6609e418-9d55-4634-9330-bb9734bcf84e.CalPass Course Data.txt',
				FIRSTROW = 2,
				FORMATFILE = N'\\10.11.4.21\WebFileUploads\RawFiles\New\158388\K12CourseProd.fmt'
			) a
	) s
ON
	t.School = s.School
	and t.LocStudentId = s.LocStudentId
	and t.AcYear = s.AcYear
	and t.CourseTerm = s.CourseTerm
	and t.LocallyAssignedCourseNumber = s.LocallyAssignedCourseNumber
	and t.CourseSectionClassNumber = s.CourseSectionClassNumber
	and t.Grade = s.Grade
	and t.CreditAttempted = s.CreditAttempted
WHEN MATCHED THEN
	UPDATE SET
		t.CourseId = s.CourseId,
		t.CourseTitle = s.CourseTitle,
		t.AGstatus = s.AGstatus,
		t.CreditEarned = s.CreditEarned,
		t.CourseLevel = s.CourseLevel,
		t.CourseType = s.CourseType,
		t.DateAdded =  s.DateAdded
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			School,
			AcYear,
			LocStudentId,
			CourseId,
			LocallyAssignedCourseNumber,
			CourseSectionClassNumber,
			CourseTitle,
			AGstatus,
			Grade,
			CreditEarned,
			CreditAttempted,
			CourseLevel,
			CourseType,
			CourseTerm,
			DateAdded
		)
	VALUES
		(
			s.School,
			s.AcYear,
			s.LocStudentId,
			s.CourseId,
			s.LocallyAssignedCourseNumber,
			s.CourseSectionClassNumber,
			s.CourseTitle,
			s.AGstatus,
			s.Grade,
			s.CreditEarned,
			s.CreditAttempted,
			s.CourseLevel,
			s.CourseType,
			s.CourseTerm,
			s.DateAdded
		);

UPDATE
	t
SET
	t.Derkey1 = s.Derkey1
FROM
	calpass.dbo.K12CourseProd t
	inner join
	calpass.dbo.K12StudentProd s
		on s.School        = t.School
		and s.LocStudentId = t.LocStudentId
		and s.AcYear       = t.AcYear
WHERE
	t.School = '36741383695012';