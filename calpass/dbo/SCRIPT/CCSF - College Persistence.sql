USE calpass;

SET ANSI_WARNINGS OFF;

DECLARE
	@Q01 varchar(255) = char(9) + 'Total number of units attempted within the first year:  ',
	@Q02 varchar(255) = char(9) + 'Total number of units passed within the first year:  ',
	@Q03 varchar(255) = char(9) + 'Average GPA in first academic year: ',
	@Q04 varchar(255) = char(9) + 'Number passing 6+ units in Mathematics within first year: ',
	@Q05 varchar(255) = char(9) + 'Number passing 6+ units in English within first year: ',
	@Q06 varchar(255) = char(9) + 'Number persisting from 1st Semester to subsequent Semester: ',
	@Q07 varchar(255) = char(9) + 'Number persisting from 1st Semester for two subsequent Semesters: ',
	@Q08 varchar(255) = char(9) + 'Number passing transfer-level Mathematics within first year:  ',
	@Q09 varchar(255) = char(9) + 'Number passing transfer-level English within first year:  ',
	@Q10 varchar(255) = char(9) + 'Number passing transfer-level Mathematics within second year:  ',
	@Q11 varchar(255) = char(9) + 'Number passing transfer-level English within second year:  ',
	@Q12 varchar(255) = char(9) + 'Number beginning below college-level in Mathematics and enroll in Mathematics within second year:  ',
	@Q13 varchar(255) = char(9) + 'Number beginning below college-level in English and enroll in English within second year:  ',
	@Q14 varchar(255) = char(9) + 'Number beginning below college-level in ESL and enroll in ESL within second year:  ',
	@Q15 varchar(255) = char(9) + 'Number earning post-secondary certificate or degree within third year:  ',
	@Q16 varchar(255) = char(9) + 'Average number of Semesters to earn certificate or degree:  ',
	@A01 decimal,
	@A02 decimal,
	@A03 decimal,
	@A04 int,
	@A05 int,
	@A06 int,
	@A07 int,
	@A08 int,
	@A09 int,
	@A10 int,
	@A11 int,
	@A12 int,
	@A13 int,
	@A14 int,
	@A15 int,
	@A16 decimal(5,3);

SELECT
	@A01 = sum(ccc.UnitsAttempted),
	@A02 = sum(ccc.UnitsAttempted)
FROM
	calpass.dbo.ccsf_cohort_cc c
	inner join
	calpass.dbo.CCCourseProd ccc
		on c.CollegeId = ccc.CollegeId
		and c.StudentId = ccc.StudentId
		and c.TermId = ccc.TermId
	inner join
	calpass.dbo.CCCourseProd_Grade cccg
		on cccg.code = ccc.grade
WHERE
	cccg.enroll_ind = 1
	and ccc.creditFlag in ('T', 'D', 'C', 'S');

SELECT
	@A03 = (sum(ccc.UnitsEarned) * sum(cccg.points) / sum(ccc.UnitsEarned))
FROM
	calpass.dbo.ccsf_cohort_cc c
	inner join
	calpass.dbo.CCCourseProd ccc
		on c.CollegeId = ccc.CollegeId
		and c.StudentId = ccc.StudentId
		and c.TermId = ccc.TermId
		and c.term_id_first = ccc.TermId
	inner join
	calpass.dbo.CCCourseProd_Grade cccg
		on cccg.code = ccc.grade
WHERE
	cccg.enroll_ind = 1
	and ccc.creditFlag in ('T', 'D', 'C', 'S');

PRINT @Q01 + cast(@A01 as varchar(255))
PRINT @Q02 + case
		when @A02 is null then 'Unknown'
		else cast(@A02 as varchar(255))
	end;
PRINT @Q03 + case
		when @A03 is null then 'Unknown'
		else cast(@A03 as varchar(255)) + ' (' + cast(cast(100.00 * @A03 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;
PRINT @Q04 + case
		when @A04 is null then 'Unknown'
		else cast(@A04 as varchar(255)) + ' (' + cast(cast(100.00 * @A04 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;
PRINT @Q05 + case
		when @A05 is null then 'Unknown'
		else cast(@A05 as varchar(255)) + ' (' + cast(cast(100.00 * @A05 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;
PRINT @Q06 + case
		when @A06 is null then 'Unknown'
		else cast(@A06 as varchar(255)) + ' (' + cast(cast(100.00 * @A06 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;
PRINT @Q07 + case
		when @A07 is null then 'Unknown'
		else cast(@A07 as varchar(255))
	end;

SET ANSI_WARNINGS ON;