SELECT
	Derkey1           = k.InterSegmentKey,
	School            = e.DistrictCode + e.SchoolCode,
	AcYear            = y.YearCodeAbbr,
	LocStudentId      = calpass.dbo.get1289Encryption(p.StudentLocalId, 'X'),
	StudentId         = null,
	CSISNum           = calpass.dbo.get9812Encryption(e.StudentStateId, 'X'),
	Fname             = p.NameFirst,
	Lname             = p.NameLast,
	Gender            = p.Gender,
	Ethnicity         = null,
	Birthdate         = p.Birthdate,
	GradeLevel        = e.GradeCode,
	HomeLanguage      = isnull(case when l.PrimaryLanguageCode = 'UU' then '99' else l.PrimaryLanguageCode end, '99'),
	HispanicEthnicity = p.IsHispanicEthnicity,
	EthnicityCode1    = p.Race01,
	EthnicityCode2    = p.Race02,
	EthnicityCode3    = p.Race03,
	EthnicityCode4    = p.Race04,
	EthnicityCode5    = p.Race05,
	DateAdded         = current_timestamp,
	DuplicateIterator = row_number() over(partition by e.DistrictCode, e.SchoolCode, p.StudentLocalId, e.YearCode order by e.GradeCode desc)
FROM
	calpads.senr e
	inner join
	calpads.Year y
		on y.YearCode = e.YearCode
	inner join
	calpads.sinf p
		on p.StudentStateId = e.StudentStateId
		and p.DistrictCode = e.DistrictCode
	cross apply
	dbo.InterSegmentKey(p.NameFirst, p.NameLast, p.Gender, p.Birthdate) k
	left outer join
	calpads.sela l
		on l.StudentStateId = e.StudentStateId
		and l.IsEscrow = 1
		and l.ElaStatusStartDate = (
			SELECT
				max(l1.ElaStatusStartDate)
			FROM
				calpads.sela l1
			WHERE
				l1.StudentStateId = l.StudentStateId
		)
WHERE
	e.EnrollStartDate = (
		SELECT
			max(e1.EnrollStartDate)
		FROM
			calpads.Senr e1
		WHERE
			e1.StudentStateId = e.StudentStateId
			and e1.SchoolCode = e.SchoolCode
	)
	and p.EffectiveStartDate = (
		SELECT
			max(p1.EffectiveStartDate)
		FROM
			calpads.sinf p1
		WHERE
			p1.StudentStateId = p.StudentStateId
	)
	and (
		e.IsEscrow = 1
		or
		p.IsEscrow = 1
	)
	and p.StudentLocalId is not null;