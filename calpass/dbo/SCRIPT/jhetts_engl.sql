IF (object_id('dbo.jhetts_cc_engl') is not null)
	BEGIN
		DROP TABLE dbo.jhetts_cc_engl;
	END;

GO

SELECT
	o.*,
	-- prospective file
	p.engl_cb21,
	p.read_cb21,
	p.esl_cb21,
	p.math_cb21,
	-- awards
	CertificateDate = convert(date, null),
	DegreeDate = convert(date, null),
	TransferDate = convert(date, null)
INTO
	calpass.dbo.jhetts_cc_engl
FROM
	mmap.dbo.organization_engl o
	left outer join
	calpass.mmap.ProspectiveCohort p
		on o.derkey1 = p.Derkey1
		and p.SubmissionFileId = (
			SELECT
				max(pc.SubmissionFileId)
			FROM
				calpass.mmap.ProspectiveCohort pc
			WHERE
				pc.Derkey1 = p.Derkey1
		)
WHERE
	o.organization_code = '000';

GO

CREATE INDEX ix_jhetts_cc_engl__Derkey1 ON dbo.jhetts_cc_engl (Derkey1);

GO

UPDATE
	t
SET
	t.CertificateDate = s.CertificateDate,
	t.DegreeDate = s.DegreeDate
FROM
	dbo.jhetts_cc_engl t
	inner join
	(
		SELECT
			id.InterSegmentKey,
			CertificateDate = min(case when a.award in ('E','B','L','T') then a.[date] end),
			DegreeDate = min(case when a.award in ('A', 'S', 'Y', 'Z') then a.[date] end)
		FROM
			calpass.comis.studntid id
			inner join
			calpass.comis.spawards a
				on a.college_id = id.college_id
				and a.student_id = id.student_id
		WHERE
			exists (
				SELECT
					1
				FROM
					calpass.dbo.jhetts_cc_engl e
				WHERE
					e.Derkey1 = id.InterSegmentKey
			)
		GROUP BY
			id.InterSegmentKey
	) s
		on s.InterSegmentKey = t.Derkey1;

UPDATE
	t
SET
	t.TransferDate = s.TransferDate
FROM
	dbo.jhetts_cc_engl t
	inner join
	(
		SELECT
			id.InterSegmentKey,
			TransferDate = min(date_of_xfer)
		FROM
			comis.xfer_bucket x
			inner join
			comis.studntid id
				on id.ssn = x.ssn
		WHERE
			exists (
				SELECT
					1
				FROM
					dbo.jhetts_cc_engl e
				WHERE
					e.Derkey1 = id.InterSegmentKey
			)
			and not exists (
				SELECT
					1
				FROM
					calpass.comis.studntid i
				WHERE
					i.InterSegmentKey = id.InterSegmentKey
				HAVING
					count(distinct case when i.student_id_status = 'S' then ssn end) > 1
			)
			and tfr_in_inst_type = '4'
		GROUP BY
			id.InterSegmentKey
	) s
ON
	s.InterSegmentKey = t.derkey1;

EXECUTE xp_cmdshell 'bcp "SELECT * FROM calpass.dbo.jhetts_cc_engl" QUERYOUT "C:\Users\dlamoree\desktop\jhetts_cc_engl.txt" -T -c -q';

EXECUTE dbo.sp_spss_output
	'calpass',
	'dbo',
	'jhetts_cc_engl',
	'C:\Users\dlamoree\desktop\',  -- '
	'1';