SET NOCOUNT ON;

DECLARE
	@Time datetime,
	@Seconds int,
	@Severity tinyint = 0,
	@State tinyint = 1,
	@Error varchar(2048),
	@Counter int = 0,
	@Increment int = 100000,
	@Cycles int,
	@Student dbo.InterSegment,
	@StudentCohort dbo.InterSegment;

INSERT
	@Student
	(
		InterSegmentKey
	)
SELECT
	InterSegmentKey
FROM
	dbo.Student;

SELECT
	@Cycles = convert(varchar, ceiling(convert(decimal, count(*)) / @Increment))
FROM
	@Student;

BEGIN

	WHILE (@Counter < @Cycles)
	BEGIN

		SET @Error = convert(varchar, @Counter) + ' of ' + convert(varchar, @Cycles - 1);

		RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

		DELETE @StudentCohort;

		INSERT INTO
			@StudentCohort
			(
				InterSegmentKey
			)
		SELECT
			InterSegmentKey
		FROM
			@Student
		WHERE
			Iterator >= @Increment * @Counter
			and Iterator < @Increment * (@Counter + 1);

		EXEC dbo.HSTranscriptSummaryHashMerge
			@InterSegment = @StudentCohort;

		IF (not exists (SELECT 1 FROM @StudentCohort))
			BEGIN
				BREAK;
			END;

		-- get time elapsed
		SET @Seconds = datediff(second, @Time, getdate());
		-- set msg
		SET @Error = 'INSERT: ' + 
			convert(nvarchar, @Seconds / 86400) + ':' +
			convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
		-- output to user
		RAISERROR(@Error, 0, 1) WITH NOWAIT;

		SET @Counter += 1;
	END;
END;