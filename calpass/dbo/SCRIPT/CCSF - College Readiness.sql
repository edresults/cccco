USE calpass;

SET ANSI_WARNINGS OFF;

DECLARE
	@Q01 varchar(255) = 'Number of SFUSD 12th Graders:  ',
	@Q02 varchar(255) = char(9) + 'Number graduating high school: ',
	@Q03 varchar(255) = char(9) + 'Number passing one or more courses from each A-G category: ',
	@Q04 varchar(255) = char(9) + 'Number passing one or more Dual Enrollment or Concurrent Enrollment courses: ',
	@Q05 varchar(255) = char(9) + 'Number scoring "College Ready" in ELA on EAP exam: ',
	@Q06 varchar(255) = char(9) + 'Number scoring "College Ready" in MATH on EAP exam: ',
	@Q07 varchar(255) = char(9) + 'Number submitting FAFSA: ',
	@A01 int,
	@A02 int,
	@A03 int,
	@A04 int,
	@A05 int,
	@A06 int,
	@A07 int;

SELECT
	@A01 = count(distinct Derkey1)
FROM
	calpass.dbo.ccsf_cohort_12;

SELECT
	@A02 = count(distinct Derkey1)
FROM
	calpass.dbo.K12AwardProd hsa
WHERE
	-- <cohort>
	exists (
		SELECT
			1
		FROM
			calpass.dbo.ccsf_cohort_12 c
		WHERE
			c.School = hsa.School
			and c.LocStudentId = hsa.LocStudentId
			and c.AcYear = hsa.AcYear
	)
	-- </cohort>
	-- <graduation codes>
	and hsa.AwardType in ('100','106','108','120','250','320','330','480');
	-- </graduation codes>	

SELECT
	@A03 = count(distinct r.Derkey1)
FROM
	(
		SELECT
			Derkey1
		FROM
			calpass.dbo.K12CourseProd hsc
			inner join
			calpass.dbo.K12CourseProd_Grade hscg
				on hscg.code = hsc.grade
		WHERE
			-- <cohort>
			exists (
				SELECT
					1
				FROM
					calpass.dbo.ccsf_cohort_12 c
				WHERE
					c.School = hsc.School
					and c.LocStudentId = hsc.LocStudentId
					and c.AcYear = hsc.AcYear
			)
			-- </cohort>
		GROUP BY
			Derkey1
		HAVING
			-- <AG requirements>
			max(case when left(AGstatus, 1) = 'A' and hscg.success_ind = 1 then 1 end) = 1
			and max(case when left(AGstatus, 1) = 'B' and hscg.success_ind = 1 then 1 end) = 1
			and max(case when left(AGstatus, 1) = 'C' and hscg.success_ind = 1 then 1 end) = 1
			and max(case when left(AGstatus, 1) = 'D' and hscg.success_ind = 1 then 1 end) = 1
			and max(case when left(AGstatus, 1) = 'E' and hscg.success_ind = 1 then 1 end) = 1
			and max(case when left(AGstatus, 1) = 'F' and hscg.success_ind = 1 then 1 end) = 1
			and max(case when left(AGstatus, 1) = 'G' and hscg.success_ind = 1 then 1 end) = 1
			-- </AG requirements>
	) r

SELECT
	@A04 = count(distinct r.Derkey1)
FROM
	(
		SELECT
			ccs.Derkey1
		FROM
			calpass.dbo.CCStudentProd ccs
			inner join
			calpass.dbo.CCCourseProd ccc
				on ccs.CollegeId = ccc.CollegeId
				and ccs.StudentId = ccc.StudentId
				and ccs.TermId = ccc.TermId
			inner join
			calpass.dbo.CCCourseProd_Grade cccg
				on cccg.code = ccc.grade
			inner join
			calpass.dbo.CCStudentProd_termId t
				on t.code = ccs.termId
		WHERE
			-- <cohort>
			exists (
				SELECT
					1
				FROM
					calpass.dbo.ccsf_cohort_12 c
				WHERE
					c.Derkey1 = ccs.Derkey1
					and c.AcYear = t.year_code
			)
			-- </cohort>
			-- <credit course>
			and ccc.creditFlag in ('T', 'D', 'C', 'S')
			-- </credit course>
		GROUP BY
			ccs.Derkey1
		HAVING
			max(cccg.success_ind) = 1
	) r;

SELECT
	@A05 = count(distinct eap.Derkey1)
FROM
	calpass.dbo.K12StarDemProd eap
WHERE
	-- <cohort>
	exists (
		SELECT
			1
		FROM
			calpass.dbo.ccsf_cohort_12 c
		WHERE
			c.School = eap.School
			and c.LocStudentId = eap.LocStudentId
			and c.AcYear = eap.AcYear
	)
	-- </cohort>
GROUP BY
	eap.Derkey1
HAVING
	-- enngl eap requirement
	max(case eap.EAPELAStatus when '1' then 1 end) = 1;

SELECT
	@A06 = count(distinct eap.Derkey1)
FROM
	calpass.dbo.K12StarDemProd eap
WHERE
	-- <cohort>
	exists (
		SELECT
			1
		FROM
			calpass.dbo.ccsf_cohort_12 c
		WHERE
			c.School = eap.School
			and c.LocStudentId = eap.LocStudentId
			and c.AcYear = eap.AcYear
	)
	-- </cohort>
GROUP BY
	eap.Derkey1
HAVING
	-- math eap requirement
	max(case eap.EAPMathStatus when '1' then 1 end) = 1;

SELECT @A07 = null;

PRINT @Q01 + cast(@A01 as varchar(255))
PRINT @Q02 + case
		when @A02 is null then 'Unknown'
		else cast(@A02 as varchar(255)) + ' (' + cast(cast(100.00 * @A02 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;
PRINT @Q03 + case
		when @A03 is null then 'Unknown'
		else cast(@A03 as varchar(255)) + ' (' + cast(cast(100.00 * @A03 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;
PRINT @Q04 + case
		when @A04 is null then 'Unknown'
		else cast(@A04 as varchar(255)) + ' (' + cast(cast(100.00 * @A04 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;
PRINT @Q05 + case
		when @A05 is null then 'Unknown'
		else cast(@A05 as varchar(255)) + ' (' + cast(cast(100.00 * @A05 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;
PRINT @Q06 + case
		when @A06 is null then 'Unknown'
		else cast(@A06 as varchar(255)) + ' (' + cast(cast(100.00 * @A06 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;
PRINT @Q07 + case
		when @A07 is null then 'Unknown'
		else cast(@A07 as varchar(255)) + ' (' + cast(cast(100.00 * @A07 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;

SET ANSI_WARNINGS ON;