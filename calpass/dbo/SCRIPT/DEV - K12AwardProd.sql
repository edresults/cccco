MERGE
	dbo.K12AwardProd
WITH
	(
		TABLOCKX
	) t
USING
	(
		SELECT DISTINCT
			Derkey1      = k.InterSegmentKey,
			School       = e.DistrictCode + e.SchoolCode,
			LocStudentId = calpass.dbo.Get1289Encryption(p.StudentLocalId, 'X'),
			AcYear       = y.YearCodeAbbr,
			AwardType    = e.CompletionStatusCode,
			AwardDate    = e.EnrollExitDate,
			DateAdded    = CURRENT_TIMESTAMP
		FROM
			calpads.senr e
			inner join
			calpads.Year y
				on y.YearCode = e.YearCode
			inner join
			calpads.sinf p
				on p.StudentStateId = e.StudentStateId
				and p.DistrictCode = e.DistrictCode
			cross apply
			dbo.InterSegmentKey(p.NameFirst, p.NameLast, p.Gender, p.Birthdate) k
		WHERE
			e.EnrollStartDate = (
				SELECT
					max(e1.EnrollStartDate)
				FROM
					calpads.Senr e1
				WHERE
					e1.StudentStateId = e.StudentStateId
					and e1.SchoolCode = e.SchoolCode
			)
			and p.EffectiveStartDate = (
				SELECT
					max(p1.EffectiveStartDate)
				FROM
					calpads.sinf p1
				WHERE
					p1.StudentStateId = p.StudentStateId
					and p1.DistrictCode = p.DistrictCode
			)
			and e.YearCode = '2015-2016'
			and e.DistrictCode = '3768338'
			and e.CompletionStatusCode not in ('480', '360', '   ')
			and e.EnrollExitDate is not null
			and p.StudentLocalId is not null
	) s
ON
	s.School = t.School
	and s.LocStudentId = t.LocStudentId
	and s.AcYear = t.AcYear
	and s.AwardType = t.AwardType
	and s.AwardDate = t.AwardDate
WHEN MATCHED THEN
	UPDATE SET
		t.Derkey1 = s.Derkey1,
		t.DateAdded = s.DateAdded
WHEN NOT MATCHED THEN
	INSERT
		(
			Derkey1,
			School,
			LocStudentId,
			AcYear,
			AwardType,
			AwardDate,
			DateAdded
		)
	VALUES
		(
			s.Derkey1,
			s.School,
			s.LocStudentId,
			s.AcYear,
			s.AwardType,
			s.AwardDate,
			s.DateAdded
		);