drop table #CCSF;

SELECT
	id.student_id,
	COMIS_high_school = st.high_school,
	CALPASS_high_school = convert(char(6), null),
	t.YearTermCode,
	id.InterSegmentKey
INTO
	#CCSF
FROM
	comis.studntid id
	inner join
	comis.sbstudnt sb
		on id.college_id = sb.college_id
		and id.student_id = sb.student_id
	inner join
	comis.Term t
		on t.TermCode = sb.term_first
	inner join
	comis.stterm st
		on st.college_id = id.college_id
		and st.student_id = id.student_id
		and st.term_id = sb.term_first
WHERE
	id.college_id = '361';

UPDATE
	t
SET
	t.CALPASS_high_school = s.CALPASS_high_school
FROM
	#CCSF t
	inner join
	(
		SELECT
			a.InterSegmentKey,
			CALPASS_high_school = substring(a.School, 8, 6)
		FROM
			(
				SELECT
					sF.InterSegmentKey,
					s.School,
					Selector = row_number() over(partition by s.Derkey1 ORDER BY y.YearTrailing desc)
				FROM
					#CCSF sf
					inner join
					K12StudentProd s
						on sf.InterSegmentKey = s.Derkey1
					inner join
					calpads.Year y
						on y.YearCodeAbbr = s.AcYear
				WHERE
					sf.COMIS_high_school in ('XXXXXX','YYYYYY')
					and s.GradeLevel in ('09', '10', '11', '12')
					and len(s.School) = 14
			) a
		WHERE
			a.Selector = 1
	) s
		on t.InterSegmentkey = s.InterSegmentKey;

SELECT
	*
FROM
	#CCSF