SET NOCOUNT ON;

DECLARE
	@Message nvarchar(2048),
	@Time datetime,
	@Seconds int,
	@Severity tinyint = 0,
	@State tinyint = 1,
	@Error varchar(2048),
	@Counter int = 0,
	@Increment int = 100000,
	@Cycles int,
	@InterSegmentAll dbo.InterSegment,
	@InterSegment dbo.InterSegment;

INSERT
	@InterSegmentAll
	(
		InterSegmentKey
	)
SELECT
	InterSegmentKey
FROM
	dbo.Student;

SELECT
	@Cycles = convert(varchar, ceiling(convert(decimal, count(*)) / @Increment))
FROM
	@InterSegmentAll;

BEGIN

	WHILE (@Counter < @Cycles)
	BEGIN

		SET @Error = convert(varchar, @Counter + 1) + ' of ' + convert(varchar, @Cycles);

		RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

		DELETE @InterSegment;

		INSERT INTO
			@InterSegment
			(
				InterSegmentKey
			)
		SELECT
			InterSegmentKey
		FROM
			@InterSegmentAll
		WHERE
			Iterator >= @Increment * @Counter
			and Iterator < @Increment * (@Counter + 1);

		BEGIN TRY
			EXECUTE mmap.RetrospectivePerformanceMerge
				@InterSegment = @InterSegment;
		END TRY
		BEGIN CATCH
			-- set error message
			SET @Message = isnull(ERROR_MESSAGE(), N'');
			-- put error message
			RAISERROR(@Message, 0, 1) WITH NOWAIT;
			-- exit
			RETURN;
		END CATCH;

		BEGIN TRY
			EXECUTE mmap.RetrospectiveCourseContentMerge
				@InterSegment = @InterSegment;
		END TRY
		BEGIN CATCH
			-- set error message
			SET @Message = isnull(ERROR_MESSAGE(), N'');
			-- put error message
			RAISERROR(@Message, 0, 1) WITH NOWAIT;
			-- exit
			RETURN;
		END CATCH;

		BEGIN TRY
			EXECUTE mmap.PlacementMerge
				@InterSegment = @InterSegment;
		END TRY
		BEGIN CATCH
			-- set error message
			SET @Message = isnull(ERROR_MESSAGE(), N'');
			-- put error message
			RAISERROR(@Message, 0, 1) WITH NOWAIT;
			-- exit
			RETURN;
		END CATCH;

		--
		-- DEPRECATED PROCESSES
		--

		-- -- Old MMAP
		-- BEGIN TRY
		-- 	-- Merge RetrospectivePerformance
		-- 	EXECUTE dbo.HSTranscriptSummaryHashMerge
		-- 		@InterSegment = @InterSegment;
		-- END TRY
		-- BEGIN CATCH
		-- 	-- set error message
		-- 	SET @Message = isnull(ERROR_MESSAGE(), N'');
		-- 	-- put error message
		-- 	RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- 	-- exit
		-- 	RETURN;
		-- END CATCH;

		IF (not exists (SELECT 1 FROM @InterSegment))
			BEGIN
				BREAK;
			END;

		-- get time elapsed
		SET @Seconds = datediff(second, @Time, getdate());
		-- set msg
		SET @Error = 'ITERATION: ' + 
			convert(nvarchar, @Seconds / 86400) + ':' +
			convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
		-- output to user
		RAISERROR(@Error, 0, 1) WITH NOWAIT;

		SET @Counter += 1;
	END;
END;