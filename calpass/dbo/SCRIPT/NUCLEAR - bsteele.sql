USE calpass;

GO

DROP PROCEDURE dbo.p_bsteele_wrapper;
DROP PROCEDURE dbo.p_bsteele_cohort_cc_insert;
DROP TABLE dbo.bsteele_cohort_cc;
DROP PROCEDURE dbo.p_bsteele_cohort_cst_insert;
DROP TABLE dbo.bsteele_cohort_cst;
DROP PROCEDURE dbo.p_bsteele_cohort_hs_insert;
DROP TABLE dbo.bsteele_cohort_hs;
DROP PROCEDURE dbo.p_bsteele_cohort_insert;
DROP TABLE dbo.bsteele_cohort;
DROP TABLE dbo.bsteele_cohort_msj_sections;
DROP TABLE dbo.bsteele_cohort_msj;