USE calpass;

SET ANSI_WARNINGS OFF;

DECLARE
	@Q01 varchar(255) = 'Number of CCSF students in cohort coming from SFUSD matriculating to CCSF within 1 year:  ',
	@Q02 varchar(255) = char(9) + 'Number placing into college level English: ',
	@Q03 varchar(255) = char(9) + 'Number placing into college level Math: ',
	@Q04 varchar(255) = char(9) + 'Number enrolling in Math in their 1st Semester: ',
	@Q05 varchar(255) = char(9) + 'Number enrolling in English in their 1st Semester: ',
	@Q06 varchar(255) = char(9) + 'Number enrolling in ESL in their 1st Semester: ',
	@Q07 varchar(255) = char(9) + 'Average units enrolled in 1st Semester: ',
	@A01 int,
	@A02 int,
	@A03 int,
	@A04 int,
	@A05 int,
	@A06 int,
	@A07 decimal(5,3);

SELECT
	@A01 = count(distinct c.StudentId)
FROM
	calpass.dbo.ccsf_cohort_cc c;

SELECT
	@A02 = null;

SELECT
	@A03 = null;

SELECT
	@A04 = count(distinct ccc.StudentId)
FROM
	calpass.dbo.ccsf_cohort_cc c
	inner join
	calpass.dbo.CCCourseProd ccc
		on c.CollegeId = ccc.CollegeId
		and c.StudentId = ccc.StudentId
		and c.TermId = ccc.TermId
		and c.term_id_first = ccc.TermId
	inner join
	calpass.dbo.CCCourseProd_Grade cccg
		on cccg.code = ccc.grade
WHERE
	cccg.enroll_ind = 1
	and ccc.creditFlag in ('T', 'D', 'C', 'S')
	and ccc.TopCode = '170100';

SELECT
	@A05 = count(distinct ccc.StudentId)
FROM
	calpass.dbo.ccsf_cohort_cc c
	inner join
	calpass.dbo.CCCourseProd ccc
		on c.CollegeId = ccc.CollegeId
		and c.StudentId = ccc.StudentId
		and c.TermId = ccc.TermId
		and c.term_id_first = ccc.TermId
	inner join
	calpass.dbo.CCCourseProd_Grade cccg
		on cccg.code = ccc.grade
WHERE
	cccg.enroll_ind = 1
	and ccc.creditFlag in ('T', 'D', 'C', 'S')
	and ccc.TopCode = '150100';

SELECT
	@A06 = count(distinct ccc.StudentId)
FROM
	calpass.dbo.ccsf_cohort_cc c
	inner join
	calpass.dbo.CCCourseProd ccc
		on c.CollegeId = ccc.CollegeId
		and c.StudentId = ccc.StudentId
		and c.TermId = ccc.TermId
		and c.term_id_first = ccc.TermId
	inner join
	calpass.dbo.CCCourseProd_Grade cccg
		on cccg.code = ccc.grade
WHERE
	cccg.enroll_ind = 1
	and ccc.creditFlag in ('T', 'D', 'C', 'S')
	and ccc.TopCode in ('493084','493085','493086','493087');

SELECT
	@A07 = cast(sum(ccc.UnitsAttempted) / count(distinct ccc.StudentId) as decimal(5,3))
FROM
	calpass.dbo.ccsf_cohort_cc c
	inner join
	calpass.dbo.CCCourseProd ccc
		on c.CollegeId = ccc.CollegeId
		and c.StudentId = ccc.StudentId
		and c.TermId = ccc.TermId
		and c.term_id_first = ccc.TermId
	inner join
	calpass.dbo.CCCourseProd_Grade cccg
		on cccg.code = ccc.grade
WHERE
	cccg.enroll_ind = 1
	and ccc.creditFlag in ('T', 'D', 'C', 'S');

PRINT @Q01 + cast(@A01 as varchar(255))
PRINT @Q02 + case
		when @A02 is null then 'Unknown'
		else cast(@A02 as varchar(255)) + ' (' + cast(cast(100.00 * @A02 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;
PRINT @Q03 + case
		when @A03 is null then 'Unknown'
		else cast(@A03 as varchar(255)) + ' (' + cast(cast(100.00 * @A03 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;
PRINT @Q04 + case
		when @A04 is null then 'Unknown'
		else cast(@A04 as varchar(255)) + ' (' + cast(cast(100.00 * @A04 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;
PRINT @Q05 + case
		when @A05 is null then 'Unknown'
		else cast(@A05 as varchar(255)) + ' (' + cast(cast(100.00 * @A05 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;
PRINT @Q06 + case
		when @A06 is null then 'Unknown'
		else cast(@A06 as varchar(255)) + ' (' + cast(cast(100.00 * @A06 / @A01 as decimal(5,3)) as varchar(255)) + '%)'
	end;
PRINT @Q07 + case
		when @A07 is null then 'Unknown'
		else cast(@A07 as varchar(255))
	end;

SET ANSI_WARNINGS ON;