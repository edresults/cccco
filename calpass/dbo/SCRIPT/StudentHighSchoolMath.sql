TRUNCATE TABLE dbo.StudentHighSchoolMath;

GO

INSERT INTO
	dbo.StudentHighSchoolMath
	(
		InterSegmentKey,
		PreAlgebra,
		AlgebraI,
		AlgebraII,
		Geometry,
		Trigonometry,
		PreCalculus,
		Calculus,
		Stats
	)
SELECT
	InterSegmentKey,
	PreAlgebra = max(case when CourseType = 'PreAlgebra' then SectionGradePoints end),
	AlgebraI = max(case when CourseType = 'AlgebraI' then SectionGradePoints end),
	AlgebraII = max(case when CourseType = 'AlgebraII' then SectionGradePoints end),
	Geometry = max(case when CourseType = 'Geometry' then SectionGradePoints end),
	Trigonometry = max(case when CourseType = 'Trigonometry' then SectionGradePoints end),
	PreCalculus = max(case when CourseType = 'PreCalculus' then SectionGradePoints end),
	Calculus = max(case when CourseType = 'Calculus' then SectionGradePoints end),
	Stats = max(case when CourseType = 'Stats' then SectionGradePoints end)
FROM
	(
		SELECT
			sb.InterSegmentKey,
			crs.CourseType,
			SectionGradepoints = g.points,
			RowSelector = row_number() over(
					PARTITION by
						sb.InterSegmentKey,
						crs.CourseType
					ORDER BY
						y.YearTrailing DESC,
						t.PreCollegiateSeasonRank DESC,
						c.CreditAttempted DESC,
						g.rank ASC
				)
		FROM
			calpads.Student sb
				with(index(ix_Student__Derkey1))
			inner join
			dbo.K12StudentProd s
				with(index(ixc_K12StudentProd__School__LocStudentId__AcYear))
				on sb.Derkey1 = s.Derkey1
			inner join
			dbo.K12CourseProd c
				with(index(ixc_K12CourseProd__School__LocStudentId__AcYear))
				on s.School = c.School
				and s.LocStudentId = c.LocStudentId
				and s.AcYear = c.AcYear
			inner join
			comis.Grade g -- no defined grade values for calpads; use COMIS
				with(index(pk_Grade__GradeCode))
				on g.GradeCode = c.Grade
			inner join
			calpads.Term t
				with(index(pk_Term__TermCode))
				on t.TermCode = c.CourseTerm
			inner join
			calpads.Course crs
				with(index(ixc_Course__CourseCode))
				on crs.CourseCode = c.CourseId
			inner join
			calpads.Year y
				with(index(ix_Year__YearCodeAbbr))
				on y.YearCodeAbbr = s.AcYear
		WHERE
			s.GradeLevel in ('09','10','11','12')
			and g.gpa_ind = 1
			and crs.CourseType is not null
	) a
WHERE
	RowSelector != 1
GROUP BY
	InterSegmentKey;