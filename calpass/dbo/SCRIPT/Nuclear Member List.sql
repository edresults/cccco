DECLARE
	@DistrictCode char(7),
	@YearCode char(9),
	@i int = 0,
	@c int;
	
DECLARE
	@t
AS TABLE
	(
		i int identity(0,1),
		DistrictCode char(7),
		YearCode char(9)
	);

INSERT INTO
	@t
	(
		DistrictCode,
		YearCode
	)
SELECT
	DistrictCode = left(School, 7),
	YearCode = case
		when substring(AcYear, 1, 1) = '9' then '19'
		else '20'
	end + 
	substring(AcYear, 1, 2) + 
	'-' +
	case
		when substring(AcYear, 3, 1) = '9' then '19'
		else '20'
	end +
	substring(AcYear, 3, 2)
FROM
	calpass.dbo.K12StudentProd
UNION
SELECT
	DistrictCode = left(School, 7),
	YearCode = case
		when substring(AcYear, 1, 1) = '9' then '19'
		else '20'
	end + 
	substring(AcYear, 1, 2) + 
	'-' +
	case
		when substring(AcYear, 3, 1) = '9' then '19'
		else '20'
	end +
	substring(AcYear, 3, 2)
FROM
	calpass.dbo.K12CourseProd
ORDER BY
	1,2;

SET @c = @@ROWCOUNT;

WHILE (@i < @c)
BEGIN
	SELECT
		@DistrictCode = DistrictCode,
		@YearCode = YearCode
	FROM
		@t
	WHERE
		i = @i;

	EXECUTE dbo.p_K12StudentProd_MemberList
		@DistrictCode = @DistrictCode,
		@YearCode = @YearCode;
	
	EXECUTE dbo.p_K12CourseProd_MemberList
		@DistrictCode = @DistrictCode,
		@YearCode = @YearCode;
	
	EXECUTE dbo.p_K12AwardProd_MemberList
		@DistrictCode = @DistrictCode,
		@YearCode = @YearCode;
	
	SET @i += 1;
END;