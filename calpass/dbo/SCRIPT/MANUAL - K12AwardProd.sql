MERGE
	calpass.dbo.K12AwardProd t
USING
	(
		SELECT
			School,
			AcYear,
			LocStudentId = calpass.dbo.Get1289Encryption(LocStudentId, 'X'),
			AwardType,
			AwardDate,
			DateAdded = CURRENT_TIMESTAMP,
			Derkey1 = null
		FROM
			OPENROWSET(
				BULK N'\\10.11.4.21\WebFileUploads\RawFiles\New\158388\6609e418-9d55-4634-9330-bb9734bcf84e.CalPass Award Data (Class Competencies).txt',
				FIRSTROW = 2,
				FORMATFILE = N'\\10.11.4.21\WebFileUploads\RawFiles\New\158388\K12AwardProd.fmt'
			) a
		UNION
		SELECT
			School,
			AcYear,
			LocStudentId = calpass.dbo.Get1289Encryption(LocStudentId, 'X'),
			AwardType,
			AwardDate,
			DateAdded = CURRENT_TIMESTAMP,
			Derkey1 = null
		FROM
			OPENROWSET(
				BULK N'\\10.11.4.21\WebFileUploads\RawFiles\New\158388\6609e418-9d55-4634-9330-bb9734bcf84e.CalPass Award Data (Special Statuses).txt',
				FIRSTROW = 2,
				FORMATFILE = N'\\10.11.4.21\WebFileUploads\RawFiles\New\158388\K12AwardProd.fmt'
			) a
	) s
ON
	s.School = t.School
	and s.AcYear = t.AcYear
	and s.LocStudentId = t.LocStudentId
	and s.AwardType = t.AwardType
	and s.AwardDate = t.AwardDate
WHEN MATCHED THEN 
	UPDATE SET
		t.DateAdded = s.DateAdded
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			School,
			AcYear,
			LocStudentId,
			AwardType,
			AwardDate,
			DateAdded,
			Derkey1
		)
	VALUES
		(
			s.School,
			s.AcYear,
			s.LocStudentId,
			s.AwardType,
			s.AwardDate,
			s.DateAdded,
			s.Derkey1
		);

UPDATE
	t
SET
	t.Derkey1 = s.Derkey1
FROM
	calpass.dbo.K12AwardProd t
	inner join
	calpass.dbo.K12StudentProd s
		on s.School        = t.School
		and s.LocStudentId = t.LocStudentId
		and s.AcYear       = t.AcYear
WHERE
	t.School = '36741383695012';