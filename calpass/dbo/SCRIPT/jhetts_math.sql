IF (object_id('dbo.jhetts_cc_math') is not null)
	BEGIN
		DROP TABLE dbo.jhetts_cc_math;
	END;

GO

SELECT
	o.*,
	-- prospective file
	p.engl_cb21,
	p.read_cb21,
	p.esl_cb21,
	p.math_cb21,
	-- awards
	CertificateDate = convert(date, null),
	DegreeDate = convert(date, null),
	TransferDate = convert(date, null)
INTO
	calpass.dbo.jhetts_cc_math
FROM
	mmap.dbo.organization_math o
	left outer join
	calpass.mmap.ProspectiveCohort p
		on o.derkey1 = p.Derkey1
		and p.SubmissionFileId = (
			SELECT
				max(pc.SubmissionFileId)
			FROM
				calpass.mmap.ProspectiveCohort pc
			WHERE
				pc.Derkey1 = p.Derkey1
		)
WHERE
	o.organization_code = '000';

CREATE INDEX ix_jhetts_cc_math__Derkey1 ON dbo.jhetts_cc_math (Derkey1);

GO

UPDATE
	t
SET
	t.CertificateDate = s.CertificateDate,
	t.DegreeDate = s.DegreeDate
FROM
	dbo.jhetts_cc_math t
	inner join
	(
		SELECT
			id.InterSegmentKey,
			CertificateDate = min(case when a.award in ('E','B','L','T') then a.[date] end),
			DegreeDate = min(case when a.award in ('A', 'S', 'Y', 'Z') then a.[date] end)
		FROM
			calpass.comis.studntid id
			inner join
			calpass.comis.spawards a
				on a.college_id = id.college_id
				and a.student_id = id.student_id
		WHERE
			exists (
				SELECT
					1
				FROM
					calpass.dbo.jhetts_cc_math e
				WHERE
					e.Derkey1 = id.InterSegmentKey
			)
			and not exists (
				SELECT
					1
				FROM
					calpass.comis.studntid i
				WHERE
					i.InterSegmentKey = id.InterSegmentKey
				HAVING
					count(distinct case when i.student_id_status = 'S' then ssn end) > 1
			)
		GROUP BY
			id.InterSegmentKey
	) s
		on s.InterSegmentKey = t.Derkey1

UPDATE
	t
SET
	t.TransferDate = s.TransferDate
FROM
	dbo.jhetts_cc_math t
	inner join
	(
		SELECT
			id.InterSegmentKey,
			TransferDate = min(date_of_xfer)
		FROM
			comis.xfer_bucket x
			inner join
			comis.studntid id
				on id.ssn = x.ssn
		WHERE
			exists (
				SELECT
					1
				FROM
					dbo.jhetts_cc_math e
				WHERE
					e.Derkey1 = id.InterSegmentKey
			)
			and not exists (
				SELECT
					1
				FROM
					calpass.comis.studntid i
				WHERE
					i.InterSegmentKey = id.InterSegmentKey
				HAVING
					count(distinct case when i.student_id_status = 'S' then ssn end) > 1
			)
			and tfr_in_inst_type = '4'
		GROUP BY
			id.InterSegmentKey
	) s
ON
	s.InterSegmentKey = t.derkey1;

EXECUTE xp_cmdshell 'bcp "SELECT * FROM calpass.dbo.jhetts_cc_math" QUERYOUT "C:\Users\dlamoree\desktop\jhetts_cc_math.txt" -T -c -q';

-- extract
EXECUTE dbo.sp_spss_output
	'calpass',
	'dbo',
	'jhetts_cc_math',
	'C:\Users\dlamoree\desktop\',  -- '
	'1';