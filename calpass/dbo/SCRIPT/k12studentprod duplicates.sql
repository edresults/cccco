DELETE
	d
FROM
	(
		SELECT
			c.derkey1,
			c.School,
			c.AcYear,
			c.LocStudentId,
			c.StudentId,
			c.CSISNum,
			c.Fname,
			c.Lname,
			c.Gender,
			c.Ethnicity,
			c.Birthdate,
			c.GradeLevel,
			c.HomeLanguage,
			c.HispanicEthnicity,
			c.EthnicityCode1,
			c.EthnicityCode2,
			c.EthnicityCode3,
			c.EthnicityCode4,
			c.EthnicityCode5,
			c.DateAdded
		FROM
			(
				SELECT
					b.derkey1,
					b.School,
					b.AcYear,
					b.LocStudentId,
					b.StudentId,
					b.CSISNum,
					b.Fname,
					b.Lname,
					b.Gender,
					b.Ethnicity,
					b.Birthdate,
					b.GradeLevel,
					b.HomeLanguage,
					b.HispanicEthnicity,
					b.EthnicityCode1,
					b.EthnicityCode2,
					b.EthnicityCode3,
					b.EthnicityCode4,
					b.EthnicityCode5,
					b.DateAdded,
					row_number() OVER(
						PARTITION BY
							b.School,
							b.AcYear,
							b.LocStudentId,
							b.GradeLevel
						ORDER BY
							(SELECT 1)
					) as row_selector
				FROM
					calpass.dbo.k12studentprod b
				WHERE
					exists (
						SELECT
							'Y'
						FROM
							calpass.dbo.k12studentprod a
						GROUP BY
							a.school,
							a.locstudentid,
							a.acyear,
							a.gradeLevel
						HAVING
							count(*) > 1
							and a.school = b.school
							and a.locstudentid = b.locstudentid
							and a.acyear = b.acyear
							and a.gradeLevel = b.gradeLevel
					)
			) c
		WHERE
			c.row_selector > 1
	) d