	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Message     nvarchar(2048),
	@True        bit = 1,
	@False       bit = 0,
	@CollegeCode char(3);

	DECLARE
		CursorCollege
	CURSOR
		FAST_FORWARD
	FOR
		SELECT
			CollegeCode
		FROM
			comis.College
		ORDER BY
			CollegeCode;

BEGIN

	OPEN CursorCollege;
	
	FETCH NEXT FROM
		CursorCollege
	INTO
		@CollegeCode;

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		RAISERROR(@CollegeCode, 0, 1) WITH NOWAIT;
		-- Reset Escrow Values

		UPDATE
			t
		SET
			t.Race = 
				case
					when st.ipeds_race not in ('H', 'T') and substring(st.multi_race, 13, 1) = 'Y' then 'F'
					else st.ipeds_race
				end
		FROM
			CCStudentProd t
			inner join
			comis.College c
				on c.IpedsCodeLegacy = t.CollegeId
			inner join
			comis.stterm st
				on st.college_id = c.CollegeCode
				and st.student_id = t.StudentComisId
				and st.term_id = t.TermId
		WHERE
			st.college_id = @CollegeCode
			and st.term_id >= '115'
			and st.term_id <= '185';

		FETCH NEXT FROM
			CursorCollege
		INTO
			@CollegeCode;
	END;

	CLOSE CursorCollege;
	DEALLOCATE CursorCollege;

END;