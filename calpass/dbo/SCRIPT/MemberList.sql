-- school
SELECT
	DistrictCode = left(CdsCode, 7) + '0000000',
	AcYear,
	Sum(Records)
FROM
	(
		SELECT
			CdsCode = school,
			AcYear,
			Records = count(*)
		FROM
			dbo.K12StudentProd
		GROUP BY
			school,
			acyear
	) a
GROUP BY
	left(CdsCode, 7),
	AcYear;

SELECT
	DistrictCode = left(CdsCode, 7) + '0000000',
	AcYear,
	Sum(Records)
FROM
	(
		SELECT
			CdsCode = school,
			AcYear,
			Records = count(*)
		FROM
			dbo.K12CourseProd
		GROUP BY
			school,
			acyear
	) a
GROUP BY
	left(CdsCode, 7),
	AcYear;

SELECT
	DistrictCode = left(CdsCode, 7) + '0000000',
	AcYear,
	Sum(Records)
FROM
	(
		SELECT
			CdsCode = school,
			AcYear,
			Records = count(*)
		FROM
			dbo.K12AwardProd
		GROUP BY
			school,
			acyear
	) a
GROUP BY
	left(CdsCode, 7),
	AcYear;