--USE master;

--CREATE LOGIN
--	DataScientist
--WITH
--	PASSWORD = 't#^bXlnheRKr',
--	DEFAULT_DATABASE = [calpass];

--GRANT
--	IMPERSONATE
--ON
--	LOGIN::[DataScientist]
--TO
--	[ERP\AChan];

--GRANT
--	IMPERSONATE
--ON
--	LOGIN::[DataScientist]
--TO
--	[CALPASS\AChan];

--GRANT
--	IMPERSONATE
--ON
--	LOGIN::[DataScientist]
--TO
--	[ERP\AChan];

--GRANT
--	IMPERSONATE
--ON
--	LOGIN::[DataScientist]
--TO
--	[ERP\HKu];

--GRANT
--	IMPERSONATE
--ON
--	LOGIN::[DataScientist]
--TO
--	[ERP\ERice];

--USE msdb;

--CREATE USER
--	DataScientist
--FOR LOGIN
--	DataScientist;

--ALTER ROLE
--	SQLAgentOperatorRole
--ADD MEMBER
--	DataScientist;

--ALTER ROLE
--	DatabaseMailUserRole
--ADD MEMBER
--	DataScientist;

-- GRANT
-- 	SELECT
-- ON
-- 	dbo.sysjobs
-- TO
-- 	DataScientist;

--USE calpass;

--CREATE USER
--	DataScientist
--FOR LOGIN
--	DataScientist;

--GRANT
--	EXECUTE
--ON
--	CloneActivity
--TO
--	DataScientist;