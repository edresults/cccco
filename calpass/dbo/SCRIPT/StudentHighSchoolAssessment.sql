TRUNCATE TABLE dbo.StudentHighSchoolAssessment;

INSERT INTO
	dbo.StudentHighSchoolAssessment
	(
		InterSegmentKey,
		EnglScaledScore,
		MathSubject,
		MathScaledScore
	)
SELECT
	InterSegmentKey,
	EnglScaledScore = max(case RowSelectorEngl when 1 then EnglScaledScore end),
	MathSubject = max(case RowSelectorMath when 1 then MathSubject end),
	MathScaledScore = max(case RowSelectorMath when 1 then MathScaledScore end)
FROM
	(
		SELECT
			s.InterSegmentKey,
			RowSelectorEngl = row_number() OVER(
					partition by
						s.InterSegmentKey
					order by
						case when t.cstengss is null then 0 else 1 end, -- get non-null scores
						y.YearTrailing desc, -- most recent year that has a non-null score
						cast(t.cstengss as int) desc -- highest score
				),
			EnglScaledScore = cast(t.cstengss as int),
			RowSelectorMath = row_number() OVER(
					partition by
						s.InterSegmentKey
					order by
						case
							when t.cstmathsubj in ('0', '2', '6', '8') then 1
							else 0
						end desc,
						y.YearTrailing desc,
						cast(t.cstmathss as int) desc
				),
			MathSubject = cast(t.cstmathsubj as int),
			MathScaledScore = cast(t.cstmathss as int)
		FROM
			calpads.Student s
				with(index(ix_Student__Derkey1))
			inner join
			dbo.K12StarCstProd t
				with(index(ix_K12StarCstProd__Derkey1))
				on s.Derkey1 = t.Derkey1
			left outer join
			calpads.Year y
				with(index(ix_Year__YearCodeAbbr))
				on t.acYear = y.YearCodeAbbr
		WHERE
			t.cstmathsubj in ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
	) a
GROUP BY
	InterSegmentKey;