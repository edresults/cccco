SET NOCOUNT ON;

TRUNCATE TABLE Mmap.K12Performance;

GO

IF (object_id('tempdb..#K12Student') is not null)
	BEGIN
		DROP TABLE #K12Student;
	END;

CREATE TABLE #K12Student (Iterator int identity(0,1) primary key clustered, StudentStateId char(10), CSISNum char(10));

GO

IF (object_id('tempdb..#K12StudentCohort') is not null)
	BEGIN
		DROP TABLE #K12StudentCohort;
	END;

CREATE TABLE #K12StudentCohort (StudentStateId char(10) primary key clustered, CSISNum char(10));

GO

INSERT
	#K12Student
	(
		StudentStateId,
		CSISNum
	)
SELECT DISTINCT
	StudentStateId = dbo.Get1289Encryption(CSISNum,'X'),
	CSISNum
FROM
	K12StudentProd
WHERE
	CSISNum is not null
	and CSISNum <> ''
	and len(dbo.Get1289Encryption(CSISNum,'X')) <= 10
ORDER BY
	CSISNum;

DECLARE
	@Time      datetime,
	@Seconds   int,
	@Severity  tinyint = 0,
	@State     tinyint = 1,
	@Error     varchar(2048),
	@Counter   int = 0,
	@Increment int = 100000,
	@Cycles    int;

SELECT
	@Cycles = convert(varchar, ceiling(convert(decimal, count(*)) / @Increment))
FROM
	#K12Student;

WHILE (@Counter < @Cycles)
BEGIN

	SET @Error = convert(varchar, @Counter) + ' of ' + convert(varchar, @Cycles - 1);

	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

	-- reset time
	SET @Time = getdate();

	TRUNCATE TABLE #K12StudentCohort;

	INSERT
		#K12StudentCohort
		(
			StudentStateId,
			CSISNum
		)
	SELECT
		StudentStateId,
		CSISNum
	FROM
		#K12Student i
	WHERE
		Iterator >= @Increment * @Counter
		and Iterator < @Increment * (@Counter + 1);

	DELETE
		a
	FROM
		#K12StudentCohort a
	WHERE
		exists (
			SELECT
				1
			FROM
				#K12StudentCohort b
			WHERE
				a.StudentStateId = b.StudentStateId
			HAVING
				count(*) > 1
		);

	MERGE
		mmap.K12Performance t
	USING
		(
			SELECT
				StudentStateId,
				DepartmentCode,
				GradeCode,
				QualityPoints,
				CreditAttempted,
				GradePointAverage,
				GradePointAverageSans,
				CumulativeQualityPoints,
				CumulativeCreditAttempted,
				CumulativeGradePointAverage,
				CumulativeGradePointAverageSans,
				FirstYearTermCode,
				LastYearTermCode,
				IsFirst,
				IsLast
			FROM
				#K12StudentCohort
				cross apply
				Mmap.K12PerformanceGet(CSISNum)
		) s
	ON
		s.StudentStateId = t.StudentStateId
		and s.DepartmentCode = t.DepartmentCode
		and s.GradeCode = t.GradeCode
	WHEN MATCHED THEN
		UPDATE SET
			t.QualityPoints                   = s.QualityPoints,
			t.CreditAttempted                 = s.CreditAttempted,
			t.GradePointAverage               = s.GradePointAverage,
			t.GradePointAverageSans           = s.GradePointAverageSans,
			t.CumulativeQualityPoints         = s.CumulativeQualityPoints,
			t.CumulativeCreditAttempted       = s.CumulativeCreditAttempted,
			t.CumulativeGradePointAverage     = s.CumulativeGradePointAverage,
			t.CumulativeGradePointAverageSans = s.CumulativeGradePointAverageSans,
			t.FirstYearTermCode               = s.FirstYearTermCode,
			t.LastYearTermCode                = s.LastYearTermCode,
			t.IsFirst                         = s.IsFirst,
			t.IsLast                          = s.IsLast
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				StudentStateId,
				DepartmentCode,
				GradeCode,
				QualityPoints,
				CreditAttempted,
				GradePointAverage,
				GradePointAverageSans,
				CumulativeQualityPoints,
				CumulativeCreditAttempted,
				CumulativeGradePointAverage,
				CumulativeGradePointAverageSans,
				FirstYearTermCode,
				LastYearTermCode,
				IsFirst,
				IsLast
			)
		VALUES
			(
				StudentStateId,
				DepartmentCode,
				GradeCode,
				QualityPoints,
				CreditAttempted,
				GradePointAverage,
				GradePointAverageSans,
				CumulativeQualityPoints,
				CumulativeCreditAttempted,
				CumulativeGradePointAverage,
				CumulativeGradePointAverageSans,
				FirstYearTermCode,
				LastYearTermCode,
				IsFirst,
				IsLast
			);

	-- get time elapsed
	SET @Seconds = datediff(second, @Time, getdate());
	-- set msg
	SET @Error = 'INSERT: ' + 
		convert(nvarchar, @Seconds / 86400) + ':' +
		convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
	-- output to user
	RAISERROR(@Error, 0, 1) WITH NOWAIT;

	SET @Counter += 1;
END;