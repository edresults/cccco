SELECT
	*
INTO
	K12CourseProdQuarantine
FROM
	(
		SELECT
			*,
			RecordNumberPk = row_number() over(
				partition by
					School,
					LocStudentId,
					AcYear,
					CourseTerm,
					LocallyAssignedCourseNumber,
					CourseSectionClassNumber,
					Grade
				order by
					(select 1)
			),
			RecordTotalPk = count(*) over(
				partition by
					School,
					LocStudentId,
					AcYear,
					CourseTerm,
					LocallyAssignedCourseNumber,
					CourseSectionClassNumber,
					Grade
			),
			RecordNumberMark = row_number() over(
				partition by
					School,
					LocStudentId,
					AcYear,
					CourseTerm,
					LocallyAssignedCourseNumber,
					CourseSectionClassNumber,
					Grade
				order by
					(select 1)
			),
			RecordTotalMark = count(*) over(
				partition by
					School,
					LocStudentId,
					AcYear,
					CourseTerm,
					LocallyAssignedCourseNumber,
					CourseSectionClassNumber,
					Grade
			)
		FROM
			K12CourseProd
	) a
WHERE
	RecordTotalPk > 1;