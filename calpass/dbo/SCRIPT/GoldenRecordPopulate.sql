SET NOCOUNT ON;

DECLARE
	@Time datetime,
	@Seconds int,
	@Severity tinyint = 0,
	@State tinyint = 1,
	@Error varchar(2048),
	@Counter int = 0,
	@Increment int = 100000,
	@Cycles int;

IF (object_id('tempdb.dbo.#InterSegment') is not null)
	BEGIN
		DROP TABLE #InterSegment;
	END;

CREATE TABLE #InterSegment (Iterator int identity(0,1) primary key clustered, InterSegmentKey binary(64));

IF (object_id('tempdb.dbo.#InterSegmentCohort') is not null)
	BEGIN
		DROP TABLE #InterSegmentCohort;
	END;

CREATE TABLE #InterSegmentCohort (InterSegmentKey binary(64) primary key clustered);

INSERT
	#InterSegment
	(
		InterSegmentKey
	)
SELECT
	InterSegmentKey
FROM
	comis.Student
WHERE
	IsCollision = 0
ORDER BY
	InterSegmentKey;

SELECT
	@Cycles = convert(varchar, ceiling(convert(decimal, count(*)) / @Increment))
FROM
	#InterSegment;

TRUNCATE TABLE dbo.GoldenRecord;

WHILE (@Counter < @Cycles)
BEGIN

	SET @Error = convert(varchar, @Counter) + ' of ' + convert(varchar, @Cycles - 1);

	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

	-- reset time
	SET @Time = getdate();

	TRUNCATE TABLE #InterSegmentCohort;

	INSERT
		#InterSegmentCohort
		(
			InterSegmentKey
		)
	SELECT
		InterSegmentKey
	FROM
		#InterSegment i
	WHERE
		Iterator >= @Increment * @Counter
		and Iterator < @Increment * (@Counter + 1);

	-- processes
	INSERT
		dbo.GoldenRecord
		(
			InterSegmentKey,
			FirstCollegeCode,
			FirstYearTermCode,
			FinalCollegeCode,
			FinalYearTermCode,
			GenderCode,
			Birthdate,
			EthnicityCode,
			GoalCode,
			IsEnglishLearner,
			K12GraduationYear,
			EnglishFirstYearTermCode,
			EnglishFirstRemedialYearTermCode,
			EnglishFirstCollegeYearTermCode,
			EnglishFirstCollegeCompleteYearTermCode,
			MathematicsFirstYearTermCode,
			MathematicsFirstRemedialYearTermCode,
			MathematicsFirstCollegeYearTermCode,
			MathematicsFirstCollegeCompleteYearTermCode,
			CreditAttemptedTotal,
			CreditEarnedTotal,
			QualityPointsTotal,
			CumulativeGradePointAverage
		)
	SELECT
		InterSegmentKey,
		FirstCollegeCode  = max(case when FirstCollegeCodeSelector   = 1 then CollegeCode end),
		FirstYearTermCode,
		FinalCollegeCode  = max(case when FinalCollegeCodeSelector   = 1 then CollegeCode end),
		FinalYearTermCode,
		GenderCode        = max(case when GenderCodeSelector         = 1 then GenderCode end),
		Birthdate         = max(case when BirthdateSelector          = 1 then Birthdate end),
		EthnicityCode     = max(case when EthnicityCodeSelector      = 1 then EthnicityCode end),
		GoalCode          = max(case when GoalCodeSelector           = 1 then GoalCode end),
		IsEnglishLearner  = max(case when IsEnglishLearnerSelector   = 1 then IsEnglishLearner end),
		K12GraduationYear = max(case when K12GraduationYearSelector  = 1 then K12GraduationYear end),
		EnglishFirstYearTermCode,
		EnglishFirstRemedialYearTermCode,
		EnglishFirstCollegeYearTermCode,
		EnglishFirstCollegeCompleteYearTermCode,
		MathematicsFirstYearTermCode,
		MathematicsFirstRemedialYearTermCode,
		MathematicsFirstCollegeYearTermCode,
		MathematicsFirstCollegeCompleteYearTermCode,
		CreditAttemptedTotal,
		CreditEarnedTotal,
		QualityPointsTotal,
		CumulativeGradePointAverage
	FROM
		(
			SELECT
				InterSegmentKey,
				CollegeCode,
				GenderCode,
				Birthdate,
				EthnicityCode,
				GoalCode,
				IsEnglishLearner,
				K12GraduationYear,
				FirstCollegeCodeSelector                = case when YearTermCode = FirstYearTermCode and max(FirstYearTermCodeMultiCollegeRank)           over (partition by InterSegmentKey) = 1 then 1 else null end,
				FinalCollegeCodeSelector                = case when YearTermCode = FirstYearTermCode and max(FinalYearTermCodeMultiCollegeRank)           over (partition by InterSegmentKey) = 1 then 1 else null end,
				GenderCodeSelector                      = case when YearTermCode = FirstYearTermCode and max(FirstYearTermCodeMultiGenderRank)            over (partition by InterSegmentKey) = 1 then 1 else null end,
				BirthdateSelector                       = case when YearTermCode = FirstYearTermCode and max(FirstYearTermCodeMultiBirthdateRank)         over (partition by InterSegmentKey) = 1 then 1 else null end,
				EthnicityCodeSelector                   = case when YearTermCode = FirstYearTermCode and max(FirstYearTermCodeMultiEthnicityRank)         over (partition by InterSegmentKey) = 1 then 1 else null end,
				GoalCodeSelector                        = case when YearTermCode = FirstYearTermCode and max(FirstYearTermCodeMultiGoalRank)              over (partition by InterSegmentKey) = 1 then 1 else null end,
				IsEnglishLearnerSelector                = case when YearTermCode = FirstYearTermCode and max(FirstYearTermCodeMultiLepRank)               over (partition by InterSegmentKey) = 1 then 1 else null end,
				K12GraduationYearSelector               = case when YearTermCode = FirstYearTermCode and max(FirstYearTermCodeMultiK12GraduationYearRank) over (partition by InterSegmentKey) = 1 then 1 else null end,
				FirstYearTermCode,
				FinalYearTermCode,
				EnglishFirstYearTermCode,
				EnglishFirstRemedialYearTermCode,
				EnglishFirstCollegeYearTermCode,
				EnglishFirstCollegeCompleteYearTermCode,
				MathematicsFirstYearTermCode,
				MathematicsFirstRemedialYearTermCode,
				MathematicsFirstCollegeYearTermCode,
				MathematicsFirstCollegeCompleteYearTermCode,
				CreditAttemptedTotal,
				CreditEarnedTotal,
				QualityPointsTotal,
				CumulativeGradePointAverage
			FROM
				(
					SELECT
						InterSegmentKey                             = id.InterSegmentKey,
						CollegeCode                                 = id.college_id,
						YearTermCode                                = t.YearTermCode,
						FirstYearTermCodeMultiCollegeRank           = case when t.YearTermCode = min(YearTermCode) over(partition by id.InterSegmentKey) then rank() over(partition by id.InterSegmentKey, t.YearTermCode order by id.college_id asc) end,
						FinalYearTermCodeMultiCollegeRank           = case when t.YearTermCode = max(YearTermCode) over(partition by id.InterSegmentKey) then rank() over(partition by id.InterSegmentKey, t.YearTermCode order by id.college_id asc) end,
						GenderCode                                  = st.gender,
						FirstYearTermCodeMultiGenderRank            = case when t.YearTermCode = min(YearTermCode) over(partition by id.InterSegmentKey) then rank() over(partition by id.InterSegmentKey, t.YearTermCode order by st.gender     asc) end,
						Birthdate                                   = convert(char(8), sb.birthdate, 112),
						FirstYearTermCodeMultiBirthdateRank         = case when t.YearTermCode = min(YearTermCode) over(partition by id.InterSegmentKey) then rank() over(partition by id.InterSegmentKey, t.YearTermCode order by sb.birthdate  asc) end,
						EthnicityCode                               = st.ipeds_race,
						FirstYearTermCodeMultiEthnicityRank         = case when t.YearTermCode = min(YearTermCode) over(partition by id.InterSegmentKey) then rank() over(partition by id.InterSegmentKey, t.YearTermCode order by st.ipeds_race asc) end,
						GoalCode                                    = st.goal,
						FirstYearTermCodeMultiGoalRank              = case when t.YearTermCode = min(YearTermCode) over(partition by id.InterSegmentKey) then rank() over(partition by id.InterSegmentKey, t.YearTermCode order by st.goal asc) end,
						IsEnglishLearner                            = case when sb.lep_flag in ('E', 'M') then 1 else 0 end,
						FirstYearTermCodeMultiLepRank               = case when t.YearTermCode = min(YearTermCode) over(partition by id.InterSegmentKey) then rank() over(partition by id.InterSegmentKey, t.YearTermCode order by sb.lep_flag   asc) end,
						K12GraduationYear                           = case when left(st.education, 1) = '3' then right(st.education, 4) end,
						FirstYearTermCodeMultiK12GraduationYearRank = case when t.YearTermCode = min(YearTermCode) over(partition by id.InterSegmentKey) then rank() over(partition by id.InterSegmentKey, t.YearTermCode order by st.education  asc) end,
						FirstYearTermCode                           = min(YearTermCode) over(partition by id.InterSegmentKey),
						FinalYearTermCode                           = max(YearTermCode) over(partition by id.InterSegmentKey),
						EnglishFirstYearTermCode                    = min(case when p.SubjectCode = 'ENGL'                                                    then YearTermCode end) over(partition by id.InterSegmentKey),
						EnglishFirstRemedialYearTermCode            = min(case when p.SubjectCode = 'ENGL' and cb.prior_to_college <> 'Y'                     then YearTermCode end) over(partition by id.InterSegmentKey),
						EnglishFirstCollegeYearTermCode             = min(case when p.SubjectCode = 'ENGL' and cb.prior_to_college =  'Y'                     then YearTermCode end) over(partition by id.InterSegmentKey),
						EnglishFirstCollegeCompleteYearTermCode     = min(case when p.SubjectCode = 'ENGL' and cb.prior_to_college =  'Y' and m.IsSuccess = 1 then YearTermCode end) over(partition by id.InterSegmentKey),
						MathematicsFirstYearTermCode                = min(case when p.SubjectCode = 'MATH'                                                    then YearTermCode end) over(partition by id.InterSegmentKey),
						MathematicsFirstRemedialYearTermCode        = min(case when p.SubjectCode = 'MATH' and cb.prior_to_college <> 'Y'                     then YearTermCode end) over(partition by id.InterSegmentKey),
						MathematicsFirstCollegeYearTermCode         = min(case when p.SubjectCode = 'MATH' and cb.prior_to_college =  'Y'                     then YearTermCode end) over(partition by id.InterSegmentKey),
						MathematicsFirstCollegeCompleteYearTermCode = min(case when p.SubjectCode = 'MATH' and cb.prior_to_college =  'Y' and m.IsSuccess = 1 then YearTermCode end) over(partition by id.InterSegmentKey),
						CreditAttemptedTotal                        = convert(decimal(6,2), round(sum(case when m.IsGpa = 1 then sx.units_attempted  end) over(partition by id.InterSegmentKey), 2)),
						CreditEarnedTotal                           = convert(decimal(6,2), round(sum(case when m.IsGpa = 1 then sx.units            end) over(partition by id.InterSegmentKey), 2)),
						QualityPointsTotal                          = convert(decimal(7,2), round(sum(case when m.IsGpa = 1 then sx.units * m.points end) over(partition by id.InterSegmentKey), 2)),
						CumulativeGradePointAverage                 = convert(decimal(4,3), round(sum(case when m.IsGpa = 1 then sx.units * m.points end) over(partition by id.InterSegmentKey) / nullif(sum(case when m.IsGpa = 1 then sx.units_attempted end) over(partition by id.InterSegmentKey), 0), 3))
					FROM
						#InterSegmentCohort isc
						inner join
						comis.studntid id
							on id.InterSegmentKey = isc.InterSegmentKey
						inner join
						comis.sbstudnt sb
							on id.college_id = sb.college_id
							and id.student_id = sb.student_id
						inner join
						comis.stterm st
							on st.college_id = id.college_id
							and st.student_id = id.student_id
						inner join
						comis.sxenrlm sx
							on sx.college_id = st.college_id
							and sx.student_id = st.student_id
							and sx.term_id = st.term_id
						inner join
						comis.cbcrsinv cb
							on cb.college_id = sx.college_id
							and cb.term_id = sx.term_id
							and cb.course_id = sx.course_id
							and cb.control_number = sx.control_number
						inner join
						comis.Term t
							on t.TermCode = st.term_id
						inner join
						comis.Student s
							on s.InterSegmentKey = id.InterSegmentKey
						inner join
						comis.Mark m
							on m.MarkCode = sx.grade
						inner join
						lbswp.Credit c
							on c.CreditCode = cb.credit_status
						inner join
						comis.Program p
							on p.ProgramCode = cb.top_code
						inner join
						comis.CourseLevel cl
							on cl.CourseLevelCode = cb.prior_to_college
					WHERE
						m.IsEnroll = 1
						and c.IsCredit = 1
						-- and id.InterSegmentKey in (
						-- 	-- one first college
						-- 	0x00002F7AB0F99B904E770D92FBAD06FAAD1446BBCB5A5A1105DD88144488B20C66C2DB500E5AF301023C0BD459D9A02C1E4057C9E61C8700F736CE66851B1C41,
						-- 	-- two first colleges; first enrollment in one
						-- 	0x0000023B12744E85D1FAC9DC988A59E198F63841049916790DC8ECC9B52E284839C45CCD3C599DE9F4DA5E871C410B25D4D4F11459E5DCF40F98414A9E3DCF04,
						-- 	-- two first colleges; first enrollment in two
						-- 	0x00005182E40FC6C6F6CD5D73FFCCC967CEFAB1A0042753AB919388A8188E8E65A2B455661D76247F5501FEED9A37015E16DC2A95F3BCA8D2C74EA3EE0707ED33
						-- )
				) a
		) b
	GROUP BY
		InterSegmentKey,
		FirstYearTermCode,
		FinalYearTermCode,
		EnglishFirstYearTermCode,
		EnglishFirstRemedialYearTermCode,
		EnglishFirstCollegeYearTermCode,
		EnglishFirstCollegeCompleteYearTermCode,
		MathematicsFirstYearTermCode,
		MathematicsFirstRemedialYearTermCode,
		MathematicsFirstCollegeYearTermCode,
		MathematicsFirstCollegeCompleteYearTermCode,
		CreditAttemptedTotal,
		CreditEarnedTotal,
		QualityPointsTotal,
		CumulativeGradePointAverage;

	UPDATE
		gr
	SET
		IsBogRecipient = 1
	FROM
		dbo.GoldenRecord gr
		inner join
		comis.Term t
			on t.YearTermCode = gr.FirstYearTermCode
		inner join
		#InterSegmentCohort i
			on i.InterSegmentKey = gr.InterSegmentKey
	WHERE
		exists (
			SELECT
				1
			FROM
				comis.studntid id
				inner join
				comis.sfawards sf
					on sf.college_id = id.college_id
					and sf.student_id = id.student_id
					and sf.term_recd = t.TermCode
			WHERE
				id.InterSegmentKey = i.InterSegmentKey
				and sf.type_id in ('BA','B1','B2','B3','BB','BC','BD','F1','F2','F3','F4','F5')
		);

	UPDATE
		gr
	SET
		gr.DegreeYearTermCode = s.DegreeYearTermCode,
		gr.CertificateYearTermCode = s.CertificateYearTermCode
	FROM
		dbo.GoldenRecord gr
		inner join
		(
			SELECT
				id.InterSegmentKey,
				DegreeYearTermCode = min(case when a.IsDegree = 1 then t.YearTermCode end),
				CertificateYearTermCode = min(case when a.IsCertificate = 1 then t.YearTermCode end)
			FROM
				#InterSegmentCohort i
				inner join
				comis.studntid id
					on id.InterSegmentKey = i.InterSegmentKey
				inner join
				comis.spawards sp
					on  sp.college_id = id.college_id
					and sp.student_id = id.student_id
				inner join
				lbswp.Award a
					on a.AwardCode = sp.Award
				inner join
				comis.Term t
					on t.TermCode = sp.term_id
			WHERE
				id.InterSegmentKey = i.InterSegmentKey
			GROUP BY
				id.InterSegmentKey
		) s
			on s.InterSegmentKey = gr.InterSegmentKey;

	UPDATE
		gr
	SET
		gr.FirstAwardSubdiscipline = s.SubdisciplineCode
	FROM
		dbo.GoldenRecord gr
		inner join
		(
			SELECT
				InterSegmentKey,
				SubdisciplineCode
			FROM
				(
					SELECT
						id.InterSegmentKey,
						sd.SubdisciplineCode,
						IsUnique = case when count(*) over(partition by id.InterSegmentKey) = 1 then 1 else 0 end
					FROM
						#InterSegmentCohort i
						inner join
						comis.studntid id
							on id.InterSegmentKey = i.InterSegmentKey
						inner join
						comis.spawards sp
							on  sp.college_id = id.college_id
							and sp.student_id = id.student_id
						inner join
						comis.Term t
							on t.TermCode = sp.term_id
						inner join
						lbswp.Program p
							on p.ProgramCode = sp.top_code
						inner join
						lbswp.Subdiscipline sd
							on sd.SubdisciplineId = p.SubdisciplineId
					WHERE
						id.InterSegmentKey = i.InterSegmentKey
						and t.YearTermCode = (
							SELECT
								min(t1.YearTermCode)
							FROM
								comis.spawards sp1
								inner join
								comis.Term t1
									on t1.TermCode = sp1.term_id
							WHERE
								sp1.college_id = sp.college_id
								and sp1.student_id = sp.student_id
						)
				) z
			WHERE
				IsUnique = 1
		) s
			on s.InterSegmentKey = gr.InterSegmentKey;

	UPDATE
		gr
	SET
		gr.TransferYearTermCode = s.TransferYearTermCode
	FROM
		dbo.GoldenRecord gr
		inner join
		(
			SELECT
				i.InterSegmentKey,
				TransferYearTermCode = min(t.YearTermCode)
			FROM
				#InterSegmentCohort i
				inner join
				comis.studntid id
					on id.InterSegmentKey = i.InterSegmentKey
				inner join
				comis.Transfer tr
					on id.ssn = tr.StudentSsn
				inner join
				comis.Term t
					on t.BeginDate <= tr.TransferDate
					and t.EndDate >= tr.TransferDate
			WHERE
				t.System = 'Semester'
				and tr.InstitutionType = '4'
			GROUP BY
				i.InterSegmentKey
		) s
			on s.InterSegmentKey = gr.InterSegmentKey;

	UPDATE
		t
	SET
		t.WageExitPrior2Quarter = s.WageExitPrior2Quarter,
		t.WageExit              = s.WageExit,
		t.WageExitAfter2Quarter = s.WageExitAfter2Quarter,
		t.WageExitAfter4Quarter = s.WageExitAfter4Quarter,
		t.WageExitAfter8Quarter = s.WageExitAfter8Quarter
	FROM
		dbo.GoldenRecord t
		inner join
		(
			SELECT
				a.InterSegmentKey,
				WageExitPrior2Quarter = sum(case when (t2.FiscalYearQuarterSequence - t1.FiscalYearQuarterSequence) = -2 then ed.wages end),
				WageExit              = sum(case when (t2.FiscalYearQuarterSequence - t1.FiscalYearQuarterSequence) =  0 then ed.wages end),
				WageExitAfter2Quarter = sum(case when (t2.FiscalYearQuarterSequence - t1.FiscalYearQuarterSequence) =  2 then ed.wages end),
				WageExitAfter4Quarter = sum(case when (t2.FiscalYearQuarterSequence - t1.FiscalYearQuarterSequence) =  4 then ed.wages end),
				WageExitAfter8Quarter = sum(case when (t2.FiscalYearQuarterSequence - t1.FiscalYearQuarterSequence) =  8 then ed.wages end)
			FROM
				(
					SELECT DISTINCT
						gr.InterSegmentKey,
						gr.FinalYearTermCode,
						id.ssn
					FROM
						#InterSegmentCohort i
						inner join
						GoldenRecord gr
							on gr.InterSegmentKey = i.InterSegmentKey
						inner join
						comis.studntid id
							on id.InterSegmentKey = gr.InterSegmentKey
					WHERE
						id.student_id_status = 'S'
				) a
				inner join
				comis.Term t1
					on t1.TermCode = a.FinalYearTermCode
				inner join
				comis.Term t2
					on t1.System = t2.System
				inner join
				dbo.eddui ed
					on ed.StudentIdSsnEnc = a.ssn
					and ed.YearId = t2.FiscalYear
					and ed.QtrId = t2.FiscalQuarter
			WHERE
				t1.System = 'Semester'
				and t2.FiscalYearQuarterSequence >= t1.FiscalYearQuarterSequence - 2
				and t2.FiscalYearQuarterSequence <= t1.FiscalYearQuarterSequence + 8
				and abs(t1.FiscalYearQuarterSequence - t2.FiscalYearQuarterSequence) % 2 = 0
			GROUP BY
				a.InterSegmentKey
		) s
			on s.InterSegmentKey = t.InterSegmentKey;

	UPDATE
		t
	SET
		t.FirstPersistedYearTermCode = s.FirstPersistedYearTermCode,
		t.IsPeristedAny = s.IsPeristedAny
	FROM
		GoldenRecord t
		inner join
		(
			SELECT
				InterSegmentKey,
				FirstPersistedYearTermCode = min(FirstPersistedYearTermCode),
				IsPeristedAny = max(IsPeristedAny)
			FROM
				(
					SELECT DISTINCT
						id.InterSegmentKey,
						FirstPersistedYearTermCode = 
							case
								when ((t1.IsPrimary = 0
									and st.term_id = t1.NextPrimaryTermCode) or t1.IsPrimary = 1)
									and t.FiscalYearQuarterSequence + 2 = lead(t.FiscalYearQuarterSequence, 1, 0) over(partition by id.InterSegmentKey order by t.FiscalYearQuarterSequence) then t.NextPrimaryYearTermCode
							end,
						IsPeristedAny = case when t.FiscalYearQuarterSequence + 2= lead(t.FiscalYearQuarterSequence, 1, 0) over(partition by id.InterSegmentKey order by t.FiscalYearQuarterSequence) then 1 else 0 end
					FROM
						#InterSegmentCohort isc
						inner join
						GoldenRecord gr
							on gr.InterSegmentKey = isc.InterSegmentKey
						inner join
						comis.studntid id
							on id.InterSegmentKey = gr.InterSegmentKey
						inner join
						comis.stterm st
							on st.college_id = id.college_id
							and st.student_id = id.student_id
						inner join
						comis.sxenrlm sx
							on sx.college_id = st.college_id
							and sx.student_id = st.student_id
							and sx.term_id = st.term_id
						inner join
						comis.cbcrsinv cb
							on cb.college_id = sx.college_id
							and cb.term_id = sx.term_id
							and cb.course_id = sx.course_id
							and cb.control_number = sx.control_number
						inner join
						comis.Term t
							on t.TermCode = st.term_id
						inner join
						comis.Term t1
							on t1.YearTermCode = gr.FirstYearTermCode
						inner join
						comis.Mark m
							on m.MarkCode = sx.grade
						inner join
						lbswp.Credit c
							on c.CreditCode = cb.credit_status
					WHERE
						m.IsEnroll = 1
						and c.IsCredit = 1
						and t.IsPrimary = 1
						-- and id.InterSegmentKey in (
						-- 	-- one first college
						-- 	0x00002F7AB0F99B904E770D92FBAD06FAAD1446BBCB5A5A1105DD88144488B20C66C2DB500E5AF301023C0BD459D9A02C1E4057C9E61C8700F736CE66851B1C41,
						-- 	-- two first colleges; first enrollment in one
						-- 	0x0000023B12744E85D1FAC9DC988A59E198F63841049916790DC8ECC9B52E284839C45CCD3C599DE9F4DA5E871C410B25D4D4F11459E5DCF40F98414A9E3DCF04,
						-- 	-- two first colleges; first enrollment in two
						-- 	0x00005182E40FC6C6F6CD5D73FFCCC967CEFAB1A0042753AB919388A8188E8E65A2B455661D76247F5501FEED9A37015E16DC2A95F3BCA8D2C74EA3EE0707ED33
						-- )
				) a
			GROUP BY
				InterSegmentKey
		) s
			on s.InterSegmentKey = t.InterSegmentKey;

	UPDATE
		gr
	SET
		gr.IsApprentice = 1
	FROM
		GoldenRecord gr
	WHERE
		exists (
			SELECT
				1
			FROM
				#InterSegmentCohort isc
			WHERE
				isc.InterSegmentKey = gr.InterSegmentKey
				and exists (
					SELECT
						1
					FROM
						comis.studntid id
						inner join
						comis.DAS a
							on a.ssn = id.ssn
						inner join
						comis.Term t
							on t.BeginDate <= a.start_date
							and t.EndDate >= a.start_date
					WHERE
						id.InterSegmentKey = gr.InterSegmentKey
						and id.student_id_status = 'S'
						and t.System = 'Semester'
						-- and id.ssn = 0x000EED645BD0BDE6368F96CBE7669C90E521A2FE4C99149C4977DBFD335A3B252F11CE37A032B3D45255A89B6425A7DA01277D25582B92BD0DD6B017F0C4F5DA
				)
		);

	-- get time elapsed
	SET @Seconds = datediff(second, @Time, getdate());
	-- set msg
	SET @Error = 'INSERT: ' + 
		convert(nvarchar, @Seconds / 86400) + ':' +
		convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
	-- output to user
	RAISERROR(@Error, 0, 1) WITH NOWAIT;

	SET @Counter += 1;
END;

UPDATE
	t
SET
	IsBogRecipient = 0
FROM
	GoldenRecord t
WHERE
	IsBogRecipient is null;

UPDATE
	t
SET
	IsApprentice = 0
FROM
	GoldenRecord t
WHERE
	IsApprentice is null;