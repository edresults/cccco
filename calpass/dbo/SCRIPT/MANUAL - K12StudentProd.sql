MERGE
	calpass.dbo.K12StudentProd t
USING
	(
		SELECT
			School,
			AcYear,
			LocStudentId = calpass.dbo.Get1289Encryption(LocStudentId, ''),
			StudentId = null,
			CSISNum = calpass.dbo.Get9812Encryption(CSISNum, ''),
			Fname,
			Lname,
			Gender,
			Ethnicity = null,
			Birthdate,
			GradeLevel,
			HomeLanguage,
			HispanicEthnicity,
			EthnicityCode1,
			DateAdded = CURRENT_TIMESTAMP,
			Derkey1 = i.InterSegmentKey
		FROM
			OPENROWSET(
				BULK N'\\10.11.4.21\WebFileUploads\RawFiles\New\158388\6609e418-9d55-4634-9330-bb9734bcf84e.CalPass Student Data.txt',
				FIRSTROW = 2,
				FORMATFILE = N'\\10.11.4.21\WebFileUploads\RawFiles\New\158388\K12StudentProd.fmt'
			) a
			cross apply
			dbo.InterSegmentKey(Fname, Lname, Gender, Birthdate) i
	) s
ON
	s.School = t.School
	and s.LocStudentId = t.LocStudentId
	and s.AcYear = t.AcYear
WHEN MATCHED THEN 
	UPDATE SET
		t.StudentId          = s.StudentId,
		t.CSISNum            = s.CSISNum,
		t.Fname              = s.Fname,
		t.Lname              = s.Lname,
		t.Gender             = s.Gender,
		t.Ethnicity          = s.Ethnicity,
		t.Birthdate          = s.Birthdate,
		t.GradeLevel         = s.GradeLevel,
		t.HomeLanguage       = s.HomeLanguage,
		t.HispanicEthnicity  = s.HispanicEthnicity,
		t.EthnicityCode1     = s.EthnicityCode1,
		t.DateAdded          = s.DateAdded,
		t.Derkey1            = s.Derkey1
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			School,
			AcYear,
			LocStudentId,
			StudentId,
			CSISNum,
			Fname,
			Lname,
			Gender,
			Ethnicity,
			Birthdate,
			GradeLevel,
			HomeLanguage,
			HispanicEthnicity,
			EthnicityCode1,
			DateAdded,
			Derkey1
		)
	VALUES
		(
			s.School,
			s.AcYear,
			s.LocStudentId,
			s.StudentId,
			s.CSISNum,
			s.Fname,
			s.Lname,
			s.Gender,
			s.Ethnicity,
			s.Birthdate,
			s.GradeLevel,
			s.HomeLanguage,
			s.HispanicEthnicity,
			s.EthnicityCode1,
			s.DateAdded,
			s.Derkey1
		);