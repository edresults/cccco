use calpass;

GO

IF (object_id('calpass.dbo.accuplacerhistorical_id','U') is not null)
	BEGIN
		DROP TABLE dbo.accuplacerhistorical_id;
	END;

GO

CREATE TABLE
	calpass.dbo.accuplacerhistorical_id
	(
		derkey1 char(15),
		college_id_ipeds char(6),
		college_id_comis char(3),
		student_key char(9),
		id_status char(1),
		cccco_assigned_student_id char(9),
		CONSTRAINT
			pk_accuplacerhistorical_id__derkey1__college_id_ipeds
		PRIMARY KEY CLUSTERED
			(
				derkey1,
				college_id_ipeds
			)
	);

CREATE INDEX
	ix_accuplacerhistorical__college_id_comis__student_key__id_status
ON
	calpass.dbo.accuplacerhistorical_id
	(
		college_id_comis,
		student_key,
		id_status
	);

INSERT INTO
	calpass.dbo.accuplacerhistorical_id
	(
		derkey1,
		college_id_ipeds,
		college_id_comis
	)
SELECT DISTINCT
	derkey1,
	c.code_ipeds_legacy,
	c.code
FROM
	calpass.dbo.accuplacerhistorical a
	inner join
	comis.dbo.district_college c
		on a.collegeId = c.code_ipeds_legacy;

UPDATE
	t
SET
	t.student_key = s.student_key,
	t.id_status = s.id_status
FROM
	calpass.dbo.accuplacerhistorical_id t
	inner join
	(
		SELECT DISTINCT
			ccs.derkey1,
			ccs.collegeId,
			student_key = ccs.StudentId,
			id_status = ccs.IdStatus
		FROM
			calpass.dbo.ccstudentprod ccs
		WHERE
			exists (
				SELECT
					1
				FROM
					calpass.dbo.accuplacerhistorical_id b
				WHERE
					b.college_id_ipeds = ccs.collegeId
					and b.derkey1 = ccs.derkey1
			)
	) s
		on t.college_id_ipeds = s.collegeId
		and t.derkey1 = s.derkey1;

UPDATE
	t
SET
	t.cccco_assigned_student_id = s.student_id
FROM
	calpass.dbo.accuplacerhistorical_id t
	inner join
	comis.dbo.studntid s
		on t.college_id_comis = s.college_id
		and t.student_key = s.ssn
		and t.id_status = s.student_id_status;

UPDATE
	t
SET
	t.college_id = s.college_id_comis,
	t.cccco_assigned_student_id = s.cccco_assigned_student_id
FROM
	calpass.dbo.accuplacerhistorical t
	inner join
	calpass.dbo.accuplacerhistorical_id s
		on t.collegeId = s.college_id_ipeds
		and t.derkey1 = s.derkey1;

DROP TABLE calpass.dbo.accuplacerhistorical_id;