CREATE PROCEDURE
    K12RemoveAll
    (
        @DistrictCode char(7)
    )
AS

BEGIN
    DELETE s FROM dbo.K12AwardProd s       WHERE s.School like @DistrictCode + '%';
    DELETE s FROM dbo.K12CAASPPDemProd s   WHERE s.School like @DistrictCode + '%';
    DELETE s FROM dbo.K12CAASPPTestProd s  WHERE s.School like @DistrictCode + '%';
    DELETE s FROM dbo.K12CourseProd s      WHERE s.School like @DistrictCode + '%';
    DELETE s FROM dbo.K12CteProd s         WHERE s.School like @DistrictCode + '%';
    DELETE s FROM dbo.K12PrgProd s         WHERE s.School like @DistrictCode + '%';
    DELETE s FROM dbo.K12StarCATProd s     WHERE s.School like @DistrictCode + '%';
    DELETE s FROM dbo.K12StarCSTProd s     WHERE s.School like @DistrictCode + '%';
    DELETE s FROM dbo.K12StarDemProd s     WHERE s.School like @DistrictCode + '%';
    DELETE s FROM dbo.K12StudentProd s     WHERE s.School like @DistrictCode + '%';
    DELETE s FROM dbo.CAHSEEDemographics s WHERE s.SchoolIdentifier like @DistrictCode + '%';
    DELETE s FROM dbo.CAHSEEMath s         WHERE s.SchoolIdentifier like @DistrictCode + '%';
    DELETE s FROM dbo.CAHSEEEnglish s      WHERE s.SchoolIdentifier like @DistrictCode + '%';
    DELETE s FROM calpads.caaspp20142015 s WHERE s.ela_pt_district_code = @DistrictCode;
    DELETE s FROM calpads.caaspp20152016 s WHERE s.school_code like @DistrictCode + '%';
    DELETE s FROM calpads.caaspp20162017 s WHERE s.school_code like @DistrictCode + '%';
    DELETE s FROM calpads.caaspp20172018 s WHERE s.col10 like @DistrictCode + '%';
END;