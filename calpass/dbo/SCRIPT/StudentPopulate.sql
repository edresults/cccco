SET NOCOUNT ON;

DECLARE
	@Time datetime,
	@Seconds int,
	@Severity tinyint = 0,
	@State tinyint = 1,
	@Error varchar(2048),
	@Counter int = 0,
	@Increment int = 100000,
	@Cycles int;

DROP TABLE #InterSegment;

CREATE TABLE #InterSegment (Iterator int identity(0,1) primary key clustered, InterSegmentKey binary(64));

DROP TABLE #InterSegmentCohort;

CREATE TABLE #InterSegmentCohort (InterSegmentKey binary(64) primary key clustered);

INSERT
	#InterSegment
	(
		InterSegmentKey
	)
SELECT DISTINCT
	InterSegmentKey = Derkey1
FROM
	dbo.K12StudentProd
ORDER BY
	Derkey1;

SELECT
	@Cycles = convert(varchar, ceiling(convert(decimal, count(*)) / @Increment))
FROM
	#InterSegment;

WHILE (@Counter < @Cycles)
BEGIN

	SET @Error = convert(varchar, @Counter) + ' of ' + convert(varchar, @Cycles - 1);

	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

	-- reset time
	SET @Time = getdate();

	TRUNCATE TABLE #InterSegmentCohort;

	INSERT
		#InterSegmentCohort
		(
			InterSegmentKey
		)
	SELECT
		InterSegmentKey
	FROM
		#InterSegment i
	WHERE
		Iterator >= @Increment * @Counter
		and Iterator < @Increment * (@Counter + 1);
	
	MERGE
		dbo.Student
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				InterSegmentKey,
				IsHighSchoolStudent,
				IsHighSchoolSection,
				IsGrade09Student,
				IsGrade09Section,
				IsGrade10Student,
				IsGrade10Section,
				IsGrade10StudentInclusive,
				IsGrade10SectionInclusive,
				IsGrade11Student,
				IsGrade11Section,
				IsGrade11StudentInclusive,
				IsGrade11SectionInclusive,
				IsGrade12Student,
				IsGrade12Section,
				IsGrade12StudentInclusive,
				IsGrade12SectionInclusive,
				IsHighSchoolCollision
			FROM
				#InterSegmentCohort
				cross apply
				dbo.StudentGet(InterSegmentKey)
		) s
	ON
		t.InterSegmentKey = s.InterSegmentKey
	WHEN MATCHED THEN
		UPDATE SET
			t.IsHighSchoolStudent = s.IsHighSchoolStudent,
			t.IsHighSchoolSection = s.IsHighSchoolSection,
			t.IsGrade09Student = s.IsGrade09Student,
			t.IsGrade09Section = s.IsGrade09Section,
			t.IsGrade10Student = s.IsGrade10Student,
			t.IsGrade10Section = s.IsGrade10Section,
			t.IsGrade10StudentInclusive = s.IsGrade10StudentInclusive,
			t.IsGrade10SectionInclusive = s.IsGrade10SectionInclusive,
			t.IsGrade11Student = s.IsGrade11Student,
			t.IsGrade11Section = s.IsGrade11Section,
			t.IsGrade11StudentInclusive = s.IsGrade11StudentInclusive,
			t.IsGrade11SectionInclusive = s.IsGrade11SectionInclusive,
			t.IsGrade12Student = s.IsGrade12Student,
			t.IsGrade12Section = s.IsGrade12Section,
			t.IsGrade12StudentInclusive = s.IsGrade12StudentInclusive,
			t.IsGrade12SectionInclusive = s.IsGrade12SectionInclusive,
			t.IsHighSchoolCollision = s.IsHighSchoolCollision
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				InterSegmentKey,
				IsHighSchoolStudent,
				IsHighSchoolSection,
				IsGrade09Student,
				IsGrade09Section,
				IsGrade10Student,
				IsGrade10Section,
				IsGrade10StudentInclusive,
				IsGrade10SectionInclusive,
				IsGrade11Student,
				IsGrade11Section,
				IsGrade11StudentInclusive,
				IsGrade11SectionInclusive,
				IsGrade12Student,
				IsGrade12Section,
				IsGrade12StudentInclusive,
				IsGrade12SectionInclusive,
				IsHighSchoolCollision
			)
		VALUES
			(
				s.InterSegmentKey,
				s.IsHighSchoolStudent,
				s.IsHighSchoolSection,
				s.IsGrade09Student,
				s.IsGrade09Section,
				s.IsGrade10Student,
				s.IsGrade10Section,
				s.IsGrade10StudentInclusive,
				s.IsGrade10SectionInclusive,
				s.IsGrade11Student,
				s.IsGrade11Section,
				s.IsGrade11StudentInclusive,
				s.IsGrade11SectionInclusive,
				s.IsGrade12Student,
				s.IsGrade12Section,
				s.IsGrade12StudentInclusive,
				s.IsGrade12SectionInclusive,
				s.IsHighSchoolCollision
			);

	-- get time elapsed
	SET @Seconds = datediff(second, @Time, getdate());
	-- set msg
	SET @Error = 'INSERT: ' + 
		convert(nvarchar, @Seconds / 86400) + ':' +
		convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
	-- output to user
	RAISERROR(@Error, 0, 1) WITH NOWAIT;

	SET @Counter += 1;
END;