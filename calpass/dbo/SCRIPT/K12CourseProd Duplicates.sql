DELETE
	d
FROM
	(
		SELECT
			c.derkey1,
			c.School,
			c.AcYear,
			c.LocStudentId,
			c.CourseId,
			c.LocallyAssignedCourseNumber,
			c.CourseSectionClassNumber,
			c.CourseTitle,
			c.AGstatus,
			c.Grade,
			c.CreditEarned,
			c.CreditAttempted,
			c.CourseLevel,
			c.CourseType,
			c.CourseTerm,
			c.DateAdded
		FROM
			(
				SELECT
					b.derkey1,
					b.School,
					b.AcYear,
					b.LocStudentId,
					b.CourseId,
					b.LocallyAssignedCourseNumber,
					b.CourseSectionClassNumber,
					b.CourseTitle,
					b.AGstatus,
					b.Grade,
					b.CreditEarned,
					b.CreditAttempted,
					b.CourseLevel,
					b.CourseType,
					b.CourseTerm,
					b.DateAdded,
					row_selector = row_number() OVER(
						PARTITION BY
							b.School,
							b.LocStudentId,
							b.AcYear,
							b.LocallyAssignedCourseNumber,
							b.CourseSectionClassNumber,
							b.CourseTerm
						ORDER BY
							(SELECT 1)
					)
				FROM
					calpass.dbo.k12courseprod b
				WHERE
					b.DateAdded >= '12/01/2016'
					and exists (
						SELECT
							'Y'
						FROM
							calpass.dbo.k12courseprod a
						WHERE
							b.School = a.School
							and b.LocStudentId = a.LocStudentId
							and b.AcYear = a.AcYear
							and b.LocallyAssignedCourseNumber = a.LocallyAssignedCourseNumber
							and b.CourseSectionClassNumber = a.CourseSectionClassNumber
							and b.CourseTerm = a.CourseTerm
						GROUP BY
							a.School,
							a.LocStudentId,
							a.AcYear,
							a.LocallyAssignedCourseNumber,
							a.CourseSectionClassNumber,
							a.CourseTerm
						HAVING
							count(*) > 1
					)
			) c
		WHERE
			c.row_selector > 1
	) d