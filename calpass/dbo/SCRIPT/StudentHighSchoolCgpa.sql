TRUNCATE TABLE dbo.StudentHighSchoolCgpa;

INSERT INTO
	dbo.StudentHighSchoolCgpa
	(
		InterSegmentKey,
		CurrentGradeLevelCode,
		CumulativeGradePointAverage
	)
SELECT
	InterSegmentKey,
	CurrentGradeLevelCode = max(GradeLevel),
	CumulativeGradePointAverage = cast(
			isnull(
				sum(
					case
						when g.completion_ind = 1 then creditAttempted
						else 0
					end * g.points
				) / nullif(sum(creditAttempted), 0)
			, 0)
			as decimal(4,3)
		)
FROM
	dbo.K12StudentProd s
		with(index(ixc_K12StudentProd__School__LocStudentId__AcYear))
	inner join
	dbo.K12CourseProd c
		with(index(ixc_K12CourseProd__School__LocStudentId__AcYear))
		on s.School = c.School
		and s.LocStudentId = c.LocStudentId
		and s.AcYear = c.AcYear
	inner join
	calpads.Student sb
		with(index(ix_Student__Derkey1))
		on sb.Derkey1 = s.Derkey1
	inner join
	comis.Grade g -- no defined grade values for calpads; use COMIS
		with(index(pk_Grade__GradeCode))
		on g.GradeCode = c.Grade
WHERE
	s.GradeLevel in ('09','10','11','12')
	and g.gpa_ind = 1
GROUP BY
	sb.InterSegmentKey;