ALTER TABLE
	CCStudentProd
ADD
	RaceIpeds char(1) null;

ALTER TABLE
	CCSTudentProd
ADD
	RaceGroup char(2) null;

GO

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Message     nvarchar(2048),
	@True        bit = 1,
	@False       bit = 0,
	@CollegeCode char(3);

	DECLARE
		CursorCollege
	CURSOR
		FAST_FORWARD
	FOR
		SELECT
			CollegeCode
		FROM
			comis.College
		ORDER BY
			CollegeCode;

BEGIN

	OPEN CursorCollege;
	
	FETCH NEXT FROM
		CursorCollege
	INTO
		@CollegeCode;

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		RAISERROR(@CollegeCode, 0, 1) WITH NOWAIT;
		-- Reset Escrow Values

		UPDATE
			t
		SET
			t.RaceIpeds = st.ipeds_race,
			t.RaceGroup =
				case
					-- hispanic
					when substring(st.multi_race, 1,  1) = 'Y' then 'HX'
					when substring(st.multi_race, 2,  1) = 'Y' then 'HM'
					when substring(st.multi_race, 3,  1) = 'Y' then 'HC'
					when substring(st.multi_race, 4,  1) = 'Y' then 'HS'
					when substring(st.multi_race, 5,  1) = 'Y' then 'HO'
					-- asian
					when substring(st.multi_race, 13, 1) = 'Y' then 'FX'
					when substring(st.multi_race, 6,  1) = 'Y' then 'AI'
					when substring(st.multi_race, 7,  1) = 'Y' then 'AC'
					when substring(st.multi_race, 8,  1) = 'Y' then 'AJ'
					when substring(st.multi_race, 9,  1) = 'Y' then 'AK'
					when substring(st.multi_race, 10, 1) = 'Y' then 'AL'
					when substring(st.multi_race, 11, 1) = 'Y' then 'AC'
					when substring(st.multi_race, 12, 1) = 'Y' then 'AV'
					when substring(st.multi_race, 14, 1) = 'Y' then 'AO'
					-- black
					when substring(st.multi_race, 15, 1) = 'Y' then 'BX'
					-- native american
					when substring(st.multi_race, 16, 1) = 'Y' then 'NX'
					-- pacific islander
					when substring(st.multi_race, 17, 1) = 'Y' then 'PG'
					when substring(st.multi_race, 18, 1) = 'Y' then 'PH'
					when substring(st.multi_race, 19, 1) = 'Y' then 'PS'
					when substring(st.multi_race, 20, 1) = 'Y' then 'PO'
					-- white
					when substring(st.multi_race, 21, 1) = 'Y' then 'W'
				end
		FROM
			CCStudentProd t
			inner join
			comis.College c
				on c.IpedsCodeLegacy = t.CollegeId
			inner join
			comis.stterm st
				on st.college_id = c.CollegeCode
				and st.student_id = t.StudentComisId
				and st.term_id = t.TermId
		WHERE
			st.college_id = @CollegeCode;

		FETCH NEXT FROM
			CursorCollege
		INTO
			@CollegeCode;
	END;

	CLOSE CursorCollege;
	DEALLOCATE CursorCollege;

END;