USE calpass;

IF (object_id('dbo.p_K12PrgProd_process') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_K12PrgProd_process;
	END;

GO

CREATE PROCEDURE
	dbo.p_K12PrgProd_process
AS

	SET NOCOUNT ON;

DECLARE
	@Message nvarchar(2048),
	@IsEscrow bit = 1,
	@NotEscrow bit = 0;

BEGIN
	-- create table to hold escrow records
	CREATE TABLE
		#Escrow
		(
			StudentStateId char(10),
			SchoolCode char(7),
			YearCode char(9),
			PRIMARY KEY CLUSTERED
				(
					StudentStateId,
					SchoolCode,
					YearCode
				)
		);

	-- table for transformations
	CREATE TABLE
		#Program
		(
			Derkey1 char(15) null,
			Derkey2 char(21) null,
			School char(14) null,
			AcYear char(4) null,
			LocStudentId varchar(15) null,
			ProgramCode char(3) null,
			MembershipCode char(1) null,
			MembershipStartDate char(8) null,
			MembershipEndDate char(8) null,
			ServiceYearCode char(9) null,
			ServiceCode char(2) null,
			AcademyId char(5) null,
			MigrantId char(11) null,
			DisabilityCode char(3) null,
			AccountabilityDistrictCode char(7) null,
			DwellingCode char(3) null,
			IsHomeless char(1) null,
			IsRunaway char(1) null,
			INDEX
				ixc_#Program
			CLUSTERED
				(
					School,
					LocStudentId,
					AcYear,
					ProgramCode,
					MembershipStartDate,
					ServiceYearCode,
					ServiceCode
				)
		);

	-- populate cohort table
	INSERT
		#Escrow
		(
			StudentStateId,
			SchoolCode,
			YearCode
		)
	SELECT
		p.StudentStateId,
		p.SchoolCode,
		p.YearCode
	FROM
		calpads.Sprg p
	WHERE
		p.IsEscrow = @IsEscrow
	UNION
	SELECT
		f.StudentStateId,
		f.SchoolCode,
		f.YearCode
	FROM
		calpads.Sinf f
	WHERE
		f.IsEscrow = @IsEscrow

	-- populate students
	INSERT
		#Program
		(
			Derkey1,
			Derkey2,
			School,
			AcYear,
			LocStudentId,
			ProgramCode,
			MembershipCode,
			MembershipStartDate,
			MembershipEndDate,
			ServiceYearCode,
			ServiceCode,
			AcademyId,
			MigrantId,
			DisabilityCode,
			AccountabilityDistrictCode,
			DwellingCode,
			IsHomeless,
			IsRunaway
		)
	-- populate student table
	SELECT
		o.Derkey1,
		Derkey2 = o.Derkey1 + substring(o.School, 8, 6),
		o.School,
		o.AcYear,
		o.LocStudentId,
		o.ProgramCode,
		o.MembershipCode,
		o.MembershipStartDate,
		o.MembershipEndDate,
		o.ServiceYearCode,
		o.ServiceCode,
		o.AcademyId,
		o.MigrantId,
		o.DisabilityCode,
		o.AccountabilityDistrictCode,
		o.DwellingCode,
		o.IsHomeless,
		o.IsRunaway
	FROM
		#Escrow e
		cross apply
		dbo.K12PrgProdGet(e.studentstateid, e.schoolcode, e.yearcode) o;

	-- delete duplicates; deterministic
	DELETE
		t
	FROM
		#Program t
	WHERE
		exists (
			SELECT
				1
			FROM
				#Program s
			WHERE
				s.School = t.School
				and s.LocStudentId = t.LocStudentId
				and s.AcYear = t.AcYear
				and s.ProgramCode = t.ProgramCode
				and s.MembershipStartDate = t.MembershipStartDate
				and s.ServiceYearCode = t.ServiceYearCode
				and s.ServiceCode = t.ServiceCode
			HAVING
				count(*) > 1
		);

	BEGIN TRY
		-- Merge K12StudentProd records
		MERGE
			dbo.K12PrgProd t
		USING
			#Program s
		ON
			s.School = t.School
			and s.LocStudentId = t.LocStudentId
			and s.AcYear = t.AcYear
			and s.ProgramCode = t.ProgramCode
			and s.MembershipStartDate = t.MembershipStartDate
			and s.ServiceYearCode = t.ServiceYearCode
			and s.ServiceCode = t.ServiceCode
		WHEN MATCHED THEN
			UPDATE SET
				t.Derkey1 = s.Derkey1,
				t.Derkey2 = s.Derkey2,
				t.MembershipCode = s.MembershipCode,
				t.MembershipEndDate = s.MembershipEndDate,
				t.AcademyId = s.AcademyId,
				t.MigrantId = s.MigrantId,
				t.DisabilityCode = s.DisabilityCode,
				t.AccountabilityDistrictCode = s.AccountabilityDistrictCode,
				t.DwellingCode = s.DwellingCode,
				t.IsHomeless = s.IsHomeless,
				t.IsRunaway = s.IsRunaway,
				t.DateAdded = getdate()
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					Derkey1,
					Derkey2,
					School,
					AcYear,
					LocStudentId,
					ProgramCode,
					MembershipCode,
					MembershipStartDate,
					MembershipEndDate,
					ServiceYearCode,
					ServiceCode,
					AcademyId,
					MigrantId,
					DisabilityCode,
					AccountabilityDistrictCode,
					DwellingCode,
					IsHomeless,
					IsRunaway,
					DateAdded
				)
			VALUES
				(
					s.Derkey1,
					s.Derkey2,
					s.School,
					s.AcYear,
					s.LocStudentId,
					s.ProgramCode,
					s.MembershipCode,
					s.MembershipStartDate,
					s.MembershipEndDate,
					s.ServiceYearCode,
					s.ServiceCode,
					s.AcademyId,
					s.MigrantId,
					s.DisabilityCode,
					s.AccountabilityDistrictCode,
					s.DwellingCode,
					s.IsHomeless,
					s.IsRunaway,
					getdate()
				);
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message =
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Procedure: ' + upper(object_name(@@PROCID)) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		-- put error message
		RAISERROR(@message, 0, 1) WITH NOWAIT;
		RETURN;
	END CATCH;
		
	-- update all associated Escrow tables
	-- Sprg
	UPDATE
		calpads.Sprg
	SET
		IsEscrow = @NotEscrow
	WHERE
		IsEscrow = @IsEscrow;
	-- Sinf
	UPDATE
		calpads.Sinf
	SET
		IsEscrow = @NotEscrow
	WHERE
		IsEscrow = @IsEscrow;
	
	-- dispose
	DROP TABLE #Escrow;
	DROP TABLE #Program;
END;