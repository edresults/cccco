USE calpass;

IF (object_id('dbo.p_K12StudentProd_select') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_K12StudentProd_select;
	END;

GO

CREATE PROCEDURE
	dbo.p_K12StudentProd_select
	(
		@StudentEscrow calpads.StudentEscrow READONLY
	)
AS

	SET NOCOUNT ON;

BEGIN
	SELECT DISTINCT
		Derkey1 = calpass.dbo.Derkey1(
			upper(replace(replace(replace(coalesce(f.name_first, r.name_first), char(39), ''), char(46), ''), char(32), '')),
			upper(replace(replace(replace(coalesce(f.name_last, r.name_last), char(39), ''), char(46), ''), char(32), '')),
			coalesce(f.gender, r.gender),
			coalesce(f.birthdate, r.birthdate)
		),
		Derkey2 = calpass.dbo.Derkey1(
			replace(replace(replace(coalesce(f.name_first, r.name_first), char(39), ''), char(46), ''), char(32), ''),
			replace(replace(replace(coalesce(f.name_last, r.name_last), char(39), ''), char(46), ''), char(32), ''),
			coalesce(f.gender, r.gender),
			coalesce(f.birthdate, r.birthdate)
		) + substring(r.school_code, 1, 6),
		School = r.district_code + r.school_code,
		AcYear = substring(r.year_code, 3, 2) + substring(r.year_code, 8, 2),
		LocStudentId = substring(calpass.dbo.get1289Encryption(r.student_id, 'X'), 1, 15),
		StudentId = null,
		CSISNum = calpass.dbo.get9812Encryption(r.ssid, 'X'),
		Fname = coalesce(f.name_first, r.name_first),
		Lname = coalesce(f.name_last, r.name_last),
		Gender = coalesce(f.gender, r.gender),
		Ethnicity = null,
		Birthdate = coalesce(f.birthdate, r.birthdate),
		GradeLevel = coalesce(r.grade_level_code, f.grade_level_code),
		HomeLanguage = case
				when coalesce(l.primary_language_code, f.primary_language_code) = 'UU' then '99'
				else coalesce(l.primary_language_code, f.primary_language_code)
			end,
		HispanicEthnicity = hispanic_ethnicity_indicator,
		EthnicityCode1 = race_01,
		EthnicityCode2 = race_02,
		EthnicityCode3 = race_03,
		EthnicityCode4 = race_04,
		EthnicityCode5 = race_05,
		DateAdded = getdate(),
		batch_id = null
	FROM
		calpads.senr r
			with (index(ixc_senr__district_code__school_code__year_code__ssid__enroll_start_date))
		left outer join
		calpads.sinf f
			with (index(ixc_sinf__district_code__school_code__year_code__ssid__effective_start_date))
			on f.district_code = r.district_code
			and f.school_code = r.school_code
			and f.year_code <= r.year_code
			and f.ssid = r.ssid
			and f.year_code = (
				SELECT
					max(f1.year_code)
				FROM
					calpads.sinf f1
				WHERE
					f1.district_code = r.district_code
					and f1.school_code = r.school_code
					and f1.year_code <= r.year_code
					and f1.ssid = r.ssid
			)
		left outer join
		calpads.sela l
			with (index(ixc_sela__district_code__school_code__year_code__ssid__ela_status_start_date))
			on l.district_code = r.district_code
			and l.school_code = r.school_code
			and l.year_code <= r.year_code
			and l.ssid = r.ssid
			and l.year_code = (
				SELECT
					max(l1.year_code)
				FROM
					calpads.sela l1
				WHERE
					l1.district_code = r.district_code
					and l1.school_code = r.school_code
					and l1.year_code <= r.year_code
					and l1.ssid = r.ssid
			)
	WHERE
		exists (
			SELECT
				1
			FROM
				@StudentEscrow e
			WHERE
				e.DistrictCode = r.district_code
				and e.SchoolCode = r.school_code
				and e.YearCode = r.year_code
				and e.Ssid = r.ssid
		);
END;