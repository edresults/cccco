USE calpass;

IF (object_id('dbo.p_K12CourseProd_delsert') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_K12CourseProd_delsert;
	END;

GO

CREATE PROCEDURE
	dbo.p_K12CourseProd_delsert
	(
		@K12CourseProd dbo.K12CourseProd READONLY
	)
AS

	SET NOCOUNT ON;

BEGIN
	-- delete old records
	DELETE
		t
	FROM
		dbo.K12CourseProd t with (
				tablockx,
				index(ix_K12CourseProd__School__AcYear)
			)
	WHERE
		exists (
			SELECT
				1
			FROM
				@K12CourseProd s
			WHERE
				t.School = s.School
				and t.AcYear = s.AcYear
		);

	-- insert new records
	INSERT INTO
		dbo.K12CourseProd with ( tablockx )
		(
			derkey1,
			derkey2,
			School,
			AcYear,
			LocStudentId,
			CourseId,
			LocallyAssignedCourseNumber,
			CourseSectionClassNumber,
			CourseTitle,
			AGstatus,
			Grade,
			CreditEarned,
			CreditAttempted,
			CourseLevel,
			CourseType,
			CourseTerm,
			DateAdded
		)
	SELECT
		derkey1,
		derkey2,
		School,
		AcYear,
		LocStudentId,
		CourseId,
		LocallyAssignedCourseNumber,
		CourseSectionClassNumber,
		CourseTitle,
		AGstatus,
		Grade,
		CreditEarned,
		CreditAttempted,
		CourseLevel,
		CourseType,
		CourseTerm,
		DateAdded
	FROM
		@K12CourseProd;
END;