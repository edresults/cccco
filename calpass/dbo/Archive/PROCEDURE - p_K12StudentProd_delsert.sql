USE calpass;

IF (object_id('dbo.p_K12StudentProd_delsert') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_K12StudentProd_delsert;
	END;

GO

CREATE PROCEDURE
	dbo.p_K12StudentProd_delsert
	(
		@K12StudentProd dbo.K12StudentProd READONLY
	)
AS

	SET NOCOUNT ON;

BEGIN
	-- delete old records
	DELETE
		t
	FROM
		dbo.K12StudentProd t
			with (
				tablockx,
				index(ix_K12StudentProd__School__AcYear)
			)
	WHERE
		exists (
			SELECT
				1
			FROM
				@K12StudentProd s
			WHERE
				t.School = s.School
				and t.AcYear = s.AcYear
		);

	-- insert new records
	INSERT INTO
		dbo.K12StudentProd
			with (
				tablockx
			)
		(
			derkey1,
			derkey2,
			School,
			AcYear,
			LocStudentId,
			StudentId,
			CSISNum,
			Fname,
			Lname,
			Gender,
			Ethnicity,
			Birthdate,
			GradeLevel,
			HomeLanguage,
			HispanicEthnicity,
			EthnicityCode1,
			EthnicityCode2,
			EthnicityCode3,
			EthnicityCode4,
			EthnicityCode5,
			DateAdded
		)
	SELECT
		derkey1,
		derkey2,
		School,
		AcYear,
		LocStudentId,
		StudentId,
		CSISNum,
		Fname,
		Lname,
		Gender,
		Ethnicity,
		Birthdate,
		GradeLevel,
		HomeLanguage,
		HispanicEthnicity,
		EthnicityCode1,
		EthnicityCode2,
		EthnicityCode3,
		EthnicityCode4,
		EthnicityCode5,
		DateAdded
	FROM
		@K12StudentProd;
END;