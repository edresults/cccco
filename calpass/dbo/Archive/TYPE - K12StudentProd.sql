USE calpass;

GO

IF (type_id('dbo.K12StudentProd') is not null)
	BEGIN
		DROP TYPE dbo.K12StudentProd;
	END;

GO

CREATE TYPE
	dbo.K12StudentProd
AS TABLE
	(
		Derkey1 char(15) not null,
		School char(14) not null,
		AcYear char(4) not null,
		LocStudentId varchar(15) not null,
		StudentId varchar(15),
		CSISNum varchar(10),
		Fname varchar(30),
		Lname varchar(40),
		Gender char(1),
		Ethnicity char(3),
		Birthdate char(8),
		GradeLevel char(2) not null,
		HomeLanguage char(2),
		HispanicEthnicity char(1),
		EthnicityCode1 char(3),
		EthnicityCode2 char(3),
		EthnicityCode3 char(3),
		EthnicityCode4 char(3),
		EthnicityCode5 char(3),
		DateAdded datetime default getdate(),
		batch_id int,
		INDEX
			ixc_K12StudentProd__School__LocStudentId__AcYear
		CLUSTERED
			(
				School,
				LocStudentId,
				AcYear
			)
	);