USE calpass;

GO

IF EXISTS (
	SELECT
		1
	FROM
		sys.schemas s
		inner join
		sys.procedures p
			on s.schema_id = p.schema_id
	WHERE
		s.name = 'dbo'
		and p.name = 'p_k12_stats'
)
	BEGIN
		DROP PROCEDURE dbo.p_k12_stats
	END;

GO

CREATE PROCEDURE
	dbo.p_k12_stats
	(
		@school_code CHAR(14),
		@year_code CHAR(4)
	)
AS

DECLARE
	@academic_year CHAR(9),
	@school_name NVARCHAR(255),
	@message NVARCHAR(4000),
	@exists BIT = 0;

BEGIN

	-- validate school code
	SELECT
		@school_name = organizationName
	FROM
		calpass.dbo.organization
	WHERE
		organizationCode = @school_code;
	
	IF (@school_name is null)
		BEGIN
			-- set progress message
			SET @message = 'School code (' + cast(@school_code as nvarchar(255)) + ') does not resolve';
			-- exe progress message
			RAISERROR(@message, 0, 1) WITH NOWAIT;
			RETURN;
		END;

	-- validate academic year
	SELECT
		@academic_year = code
	FROM
		calpads.dbo.year_code
	WHERE
		code_abbr = @year_code;
	
	IF (@academic_year is null)
		BEGIN
			-- set progress message
			SET @message = 'Year code (' + cast(@year_code as nvarchar(4)) + ') does not resolve';
			-- exe progress message
			RAISERROR(@message, 0, 1) WITH NOWAIT;
			RETURN;
		END;
	
	SET @message = 'Beginning Evaluation of ' + cast(@school_name as nvarchar(255)) + ' for the ' + cast(@academic_year as nvarchar(9)) + ' academic year';
	-- exe progress message
	RAISERROR(@message, 0, 1) WITH NOWAIT;

	--
	SELECT TOP 1
		s.*
	FROM
		k12studentprod s
		inner join
		k12courseprod c
			on c.school = s.school
			and c.locstudentid = s.locstudentid
			and c.acyear = s.acyear
	WHERE
		s.school = @school_code
		and s.acyear = @year_code;

END;