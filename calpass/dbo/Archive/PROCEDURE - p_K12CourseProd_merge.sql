USE calpass;

GO

IF (object_id('dbo.p_K12CourseProd_merge') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_K12CourseProd_merge;
	END;

GO

CREATE PROCEDURE
	dbo.p_K12CourseProd_merge
	(
		@district_code char(7)
	)
AS

	SET NOCOUNT ON;

DECLARE
	@message nvarchar(2048),
	@duplicate_count int = 0,
	@insert_count int = 0,
	@delete_count int = 0,
	@record_count int = 0,
	@replicate_length int,
	@replicate_string char(1) = '-';

BEGIN
	BEGIN TRY
		SET ANSI_WARNINGS OFF;
		-- collect student coursework
		CREATE TABLE
			#k12cp
			(
				district_code char(7) not null,
				school_code char(7) not null,
				year_code char(9) not null,
				sinf_year_code char(9),
				ssid varchar(10) not null,
				student_id varchar(15),
				name_first varchar(30),
				name_last varchar(50),
				gender char(1),
				birthdate char(8),
				course_code char(4),
				course_id varchar(10),
				section_id varchar(15),
				course_name varchar(40),
				university_code varchar(2),
				course_grade varchar(3),
				credit_earned decimal(15,5),
				credit_attempted decimal(15,5),
				section_level_code char(2),
				ib_ind bit,
				ap_ind bit,
				cte_indicator char(1),
				program_funding_source char(3),
				marking_period_code char(2),
				term_code char(2)
			);

		-- collect student coursework
		INSERT INTO
			#k12cp
		SELECT DISTINCT
			s.district_code,
			s.school_code,
			s.year_code,
			sinf_year_code = f.year_code,
			s.ssid,
			student_id = coalesce(s.student_id, f.student_id),
			name_first = coalesce(f.name_first, s.name_first),
			name_last = coalesce(f.name_last, s.name_last),
			gender = coalesce(f.gender, s.gender),
			birthdate = coalesce(f.birthdate, s.birthdate),
			c.course_code,
			c.course_id,
			s.section_id,
			c.course_name,
			s.university_code,
			s.course_grade,
			s.credit_earned,
			s.credit_attempted,
			c.section_level_code,
			cccd.ib_ind,
			cccd.ap_ind,
			c.cte_indicator,
			c.program_funding_source,
			s.marking_period_code,
			s.term_code
		FROM
			calpass.calpads.scsc s
				with(index(ixc_scsc__district_code__school_code__ssid__year_code__term_code__course_id__section_id__marking_period_code))
			left outer join
			calpass.calpads.sinf f
				with(index(ixc_sinf__district_code__school_code__ssid__year_code__effective_start_date))
				on f.district_code = s.district_code
				and f.school_code = s.school_code
				and f.ssid = s.ssid
				-- unduplicate SINF records
				and f.effective_start_date = (
					SELECT
						max(f1.effective_start_date)
					FROM
						calpass.calpads.sinf f1
						with(index(ixc_sinf__district_code__school_code__ssid__year_code__effective_start_date))
					WHERE
						f1.district_code = f.district_code
						and f1.school_code = f.school_code
						and f1.ssid = f.ssid
						and f1.year_code = f.year_code
						and f1.year_code = (
							SELECT
								max(f2.year_code)
							FROM
								calpass.calpads.sinf f2
								with(index(ixc_sinf__district_code__school_code__ssid__year_code__effective_start_date))
							WHERE
								f2.district_code = s.district_code
								and f2.school_code = s.school_code
								and f2.ssid = s.ssid
								and f2.year_code <= s.year_code
						)
				)
			left outer join
			calpass.calpads.crsc c
				with(index(ixc_crsc__district_code__school_code__year_code__term_code__course_id__section_id))
				on s.district_code = c.district_code
				and s.school_code = c.school_code
				and s.year_code = c.year_code
				and s.term_code = c.term_code
				and s.course_id = c.course_id
				and s.section_id = c.section_id
			left outer join
			calpass.calpads.crsc_course_code_detail cccd
				on c.course_code = cccd.code
		WHERE
			s.district_code = @district_code;

		CREATE CLUSTERED INDEX
			ixc_#k12cp__district_code__school_code__ssid__year_code__term_code__course_id__section_id__marking_period_code
		ON
			#k12cp
			(
				district_code,
				school_code,
				ssid,
				year_code,
				term_code,
				course_id,
				section_id,
				marking_period_code
			);

		-- remove students with no CRSE information
		DELETE
		FROM
			#k12cp
		WHERE
			course_code is null;

		-- remove students with 2+ statewide student identifiers
		DELETE
			t
		FROM
			#k12cp t
		WHERE
			exists (
				SELECT
					1
				FROM
					#k12cp s
				GROUP BY
					s.district_code,
					s.school_code,
					s.student_id
				HAVING
					count(distinct s.ssid) > 1
					and s.district_code = t.district_code
					and s.school_code = t.school_code
					and s.student_id = t.student_id
			);

		-- update recordset with sinf records more recent that scsc record
		WITH
			cte_sinf
		AS
			(
				SELECT DISTINCT
					s.district_code,
					s.school_code,
					s.ssid,
					s.year_code,
					sinf_year_code = f.year_code,
					f.name_first,
					f.name_last,
					f.gender,
					f.birthdate
				FROM
					#k12cp s
					inner join
					calpass.calpads.sinf f
						with(index(ixc_sinf__district_code__school_code__ssid__year_code__effective_start_date))
						on f.district_code = s.district_code
						and f.school_code = s.school_code
						and f.ssid = s.ssid
						and f.year_code > s.year_code
				WHERE
					(
						s.sinf_year_code is null
						and
						(
							s.name_last is null
							or s.name_first is null
							or s.gender is null
							or s.birthdate is null
						)
					)
					-- unduplicate SINF records
					and f.effective_start_date = (
						SELECT
							max(f1.effective_start_date)
						FROM
							calpass.calpads.sinf f1
							with(index(ixc_sinf__district_code__school_code__ssid__year_code__effective_start_date))
						WHERE
							f1.district_code = f.district_code
							and f1.school_code = f.school_code
							and f1.ssid = f.ssid
							and f1.year_code = f.year_code
							and f1.year_code = (
								SELECT
									min(f2.year_code)
								FROM
									calpass.calpads.sinf f2
									with(index(ixc_sinf__district_code__school_code__ssid__year_code__effective_start_date))
								WHERE
									f2.district_code = s.district_code
									and f2.school_code = s.school_code
									and f2.ssid = s.ssid
									and f2.year_code > s.year_code
							)
					)
			)
		UPDATE
			t
		SET
			t.sinf_year_code = s.sinf_year_code,
			t.name_first = case
					when t.name_first is not null then t.name_first
					else s.name_first
				end,
			t.name_last = case
					when t.name_last is not null then t.name_last
					else s.name_last
				end,
			t.gender = case
					when t.gender is not null then t.gender
					else s.gender
				end,
			t.birthdate = case
					when t.birthdate is not null then t.birthdate
					else s.birthdate
				end
		FROM
			#k12cp t
			inner join
			cte_sinf s
				on t.district_code = s.district_code
				and t.school_code = s.school_code
				and t.ssid = s.ssid
		WHERE
			not exists (
				SELECT
					0
				FROM
					cte_sinf s1
				GROUP BY
					s1.district_code,
					s1.school_code,
					s1.ssid,
					s1.year_code
				HAVING
					count(*) > 1
					and s1.district_code = s.district_code
					and s1.school_code = s.school_code
					and s1.ssid = s.ssid
					and s1.year_code = s.year_code
			);

		-- remove students with no derkey values or no student_id value
		DELETE
		FROM
			#k12cp
		WHERE
			student_id is null
			or name_first is null
			or name_last is null
			or gender is null
			or birthdate is null;

		-- remove any remaning duplicates
		DELETE
			t
		FROM
			#k12cp t
		WHERE
			exists (
				SELECT
					1
				FROM
					#k12cp s
				GROUP BY
					s.district_code,
					s.school_code,
					s.ssid,
					s.year_code,
					s.term_code,
					s.course_id,
					s.section_id,
					s.marking_period_code
				HAVING
					s.district_code = t.district_code
					and s.school_code = t.school_code
					and s.ssid = t.ssid
					and s.year_code = t.year_code
					and s.term_code = t.term_code
					and s.course_id = t.course_id
					and s.section_id = t.section_id
					and s.marking_period_code = t.marking_period_code
					and count(*) > 1
			);

		SET @duplicate_count = @@rowcount;

		CREATE TABLE
			#K12CourseProd
			(
				Derkey1 char(15) not null,
				Derkey2 char(21) not null,
				School char(14) not null,
				AcYear char(4) not null,
				LocStudentId varchar(15) not null,
				CourseId char(4) not null,
				LocallyAssignedCourseNumber varchar(10) not null,
				CourseSectionClassNumber varchar(15) not null,
				CourseTitle varchar(40) not null,
				AGstatus varchar(2),
				Grade varchar(3),
				CreditEarned decimal(15,5),
				CreditAttempted decimal(15,5),
				CourseLevel char(2),
				CourseType char(2),
				CourseTerm varchar(3),
				DateAdded datetime
			);

		-- transform recordset
		INSERT INTO
			#K12CourseProd
		SELECT
			Derkey1 = calpass.dbo.Derkey1(
				upper(replace(replace(replace(name_first, char(39), ''), char(46), ''), char(32), '')),
				upper(replace(replace(replace(name_last, char(39), ''), char(46), ''), char(32), '')),
				gender,
				birthdate
			),
			Derkey2 = calpass.dbo.Derkey1(
				upper(replace(replace(replace(name_first, char(39), ''), char(46), ''), char(32), '')),
				upper(replace(replace(replace(name_last, char(39), ''), char(46), ''), char(32), '')),
				gender,
				birthdate
			) + substring(school_code, 1, 6),
			School = district_code + school_code,
			AcYear = substring(year_code, 3, 2) + substring(year_code, 8, 2),
			LocStudentId = calpass.dbo.get1289Encryption(student_id, 'X'),
			CourseId = course_code,
			LocallyAssignedCourseNumber = course_id,
			CourseSectionClassNumber = section_id,
			CourseTitle = course_name,
			AGstatus = university_code,
			Grade = course_grade,
			CreditEarned = credit_earned,
			CreditAttempted = credit_attempted,
			CourseLevel = case
					when section_level_code = '10' then '35'
					when section_level_code = '12' then '33'
					when section_level_code = '14' then '39'
					when section_level_code = '15' then '34'
					when section_level_code = '16' then '40'
					when ib_ind = 1 then '38'
					when ap_ind = 1 then '30'
					when course_code = '2402' then '35'
					else 'XX'
				end,
			CourseType = case
					when cte_indicator = 'Y' and program_funding_source = '1' then '33'
					when cte_indicator = 'Y' and program_funding_source != '1' then '32'
					when cte_indicator = 'N' and program_funding_source = '1' then '30'
					else 'XX'
				end,
			CourseTerm = coalesce(marking_period_code, term_code),
			DateAdded = getdate()
		FROM
			#k12cp;

		CREATE CLUSTERED INDEX
			ixc_#K12CourseProd__School__LocStudentId__AcYear__CourseTerm__LocallyAssignedCourseNumber__CourseSectionClassNumber
		ON
			#K12CourseProd
			(
				School,
				LocStudentId,
				AcYear,
				CourseTerm,
				LocallyAssignedCourseNumber,
				CourseSectionClassNumber
			);

		-- Delete matched records from production table
		DELETE
			t
		FROM
			calpass.dbo.K12CourseProd t
			inner join
			#K12CourseProd s
				on s.School = t.School
				and s.LocStudentId = t.LocStudentId
				and s.AcYear = t.AcYear
				and s.CourseTerm = t.CourseTerm
				and s.LocallyAssignedCourseNumber = t.LocallyAssignedCourseNumber
				and s.CourseSectionClassNumber = t.CourseSectionClassNumber;

		SET @delete_count = @@rowcount;

		INSERT INTO
			calpass.dbo.K12CourseProd
			(
				Derkey1,
				Derkey2,
				School,
				AcYear,
				LocStudentId,
				CourseId,
				LocallyAssignedCourseNumber,
				CourseSectionClassNumber,
				CourseTitle,
				AGstatus,
				Grade,
				CreditEarned,
				CreditAttempted,
				CourseLevel,
				CourseType,
				CourseTerm,
				DateAdded
			)
		SELECT
			*
		FROM
			#K12CourseProd;

		SET @insert_count = @@rowcount;

		DROP TABLE #k12cp;
		DROP TABLE #K12CourseProd;
		SET ANSI_WARNINGS ON;

		-- set total records modified
		SET @record_count = @insert_count - @delete_count;
		-- set length to replicate
		SELECT
			@replicate_length = len(max(tbl.col))
		FROM
			(
				VALUES
				(@insert_count),
				(@delete_count)
			) tbl (col);

		-- set progress message
		SET @message = char(9) + N'Data merged successfully to K12CourseProd' + char(13) + char(10) + 
			char(9) + char(9) + 'Dups Ignored: ' + cast(@duplicate_count as nvarchar(255)) + char(13) + char(10) + 
			char(9) + char(9) + 'Delete Count: ' + cast(@delete_count as nvarchar(255)) + char(13) + char(10) + 
			char(9) + char(9) + 'Insert Count: ' + cast(@insert_count as nvarchar(255)) + char(13) + char(10) + 
			char(9) + char(9) + '--------------' + replicate(@replicate_string, @replicate_length) + char(13) + char(10) + 
			char(9) + char(9) + 'Record Count: ' + cast(@record_count as nvarchar(255));
		-- exe progress message
		RAISERROR(@message, 0, 1) WITH NOWAIT;

	END TRY
	BEGIN CATCH
		-- set progress message
		SET @message =
			nchar(9) + N'Error Number:     ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + nchar(13) + nchar(10) +
			nchar(9) + N'Error Severity:   ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + nchar(13) + nchar(10) +
			nchar(9) + N'Error State:      ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + nchar(13) + nchar(10) +
			nchar(9) + N'Error Procedure:  ' + object_name(@@PROCID) + nchar(13) + nchar(10) +
			nchar(9) + N'Error Line:       ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + nchar(13) + nchar(10) +
			nchar(9) + N'Error Message:    ' + isnull(ERROR_MESSAGE(), N'');
		-- exe progress message
		THROW 70099, @message, 1;
	END CATCH;
END;