USE calpass;

GO

IF (object_id('dbo.p_K12CourseProd_MemberList') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_K12CourseProd_MemberList;
	END;

GO

CREATE PROCEDURE
	dbo.p_K12CourseProd_MemberList
	(
		@DistrictCode char(7),
		@YearCode char(9)
	)
AS

	SET NOCOUNT ON;

DECLARE
	@AcYear char(4);

BEGIN
	
	SELECT
		@AcYear = code
	FROM
		dbo.K12StudentProd_AcYear
	WHERE
		code_calpads = @YearCode;

	MERGE
		dbo.MemberList t
	USING
		(
			SELECT
				MemberTypeId = 1,
				DistrictCode = left(c.School, 7),
				AcYear,
				RecordCount = count(*)
			FROM
				dbo.K12CourseProd c
					with(index(ix_K12CourseProd__School__AcYear))
			WHERE
				left(c.School, 7) = @DistrictCode
				and AcYear = @AcYear
			GROUP BY
				left(c.School, 7),
				AcYear
		) s
	ON
		s.MemberTypeId = t.MemberTypeId
		and s.DistrictCode = t.DistrictCode
		and s.AcYear = t.AcYear
	WHEN MATCHED THEN
		UPDATE SET
			t.CourseCount = s.RecordCount
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				MemberTypeId,
				DistrictCode,
				AcYear,
				CourseCount
			)
		VALUES
			(
				s.MemberTypeId,
				s.DistrictCode,
				s.AcYear,
				s.RecordCount
			);

END;