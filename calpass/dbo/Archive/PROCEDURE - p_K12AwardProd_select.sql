USE calpass;

IF (object_id('dbo.p_K12AwardProd_select') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_K12AwardProd_select;
	END;

GO

CREATE PROCEDURE
	dbo.p_K12AwardProd_select
	(
		@StudentEscrow calpads.StudentEscrow READONLY
	)
AS

	SET NOCOUNT ON;

BEGIN
	SELECT DISTINCT
		Derkey1 = calpass.dbo.Derkey1(
			upper(replace(replace(replace(coalesce(f.name_first, r.name_first), char(39), ''), char(46), ''), char(32), '')),
			upper(replace(replace(replace(coalesce(f.name_last, r.name_last), char(39), ''), char(46), ''), char(32), '')),
			coalesce(f.gender, r.gender),
			coalesce(f.birthdate, r.birthdate)
		),
		Derkey2 = calpass.dbo.Derkey1(
			replace(replace(replace(coalesce(f.name_first, r.name_first), char(39), ''), char(46), ''), char(32), ''),
			replace(replace(replace(coalesce(f.name_last, r.name_last), char(39), ''), char(46), ''), char(32), ''),
			coalesce(f.gender, r.gender),
			coalesce(f.birthdate, r.birthdate)
		) + substring(r.school_code, 1, 6),
		School = r.district_code + r.school_code,
		AcYear = substring(r.year_code, 3, 2) + substring(r.year_code, 8, 2),
		LocStudentId = substring(calpass.dbo.get1289Encryption(r.student_id, 'X'), 1, 15),
		AwardType = completion_status_code,
		AwardDate = enroll_exit_date,
		DateAdded = getdate(),
		batch_id = null
	FROM
		calpads.senr r
			with (index(ixc_senr__district_code__school_code__year_code__ssid__enroll_start_date))
		left outer join
		calpads.sinf f
			with (index(ixc_sinf__district_code__school_code__year_code__ssid__effective_start_date))
			on f.district_code = r.district_code
			and f.school_code = r.school_code
			and f.year_code <= r.year_code
			and f.ssid = r.ssid
			and f.year_code = (
				SELECT
					max(f1.year_code)
				FROM
					calpads.sinf f1
				WHERE
					f1.district_code = r.district_code
					and f1.school_code = r.school_code
					and f1.year_code <= r.year_code
					and f1.ssid = r.ssid
			)
	WHERE
		exists (
			SELECT
				1
			FROM
				@StudentEscrow s
			WHERE
				s.DistrictCode = r.district_code
				and s.SchoolCode = r.school_code
				and s.YearCode = r.year_code
				and s.Ssid = r.ssid
		)
		and r.completion_status_code not in ('480', '360');
END;