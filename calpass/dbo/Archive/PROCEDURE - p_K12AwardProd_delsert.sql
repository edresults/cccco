USE calpass;

IF (object_id('dbo.p_K12AwardProd_delsert') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_K12AwardProd_delsert;
	END;

GO

CREATE PROCEDURE
	dbo.p_K12AwardProd_delsert
	(
		@K12AwardProd dbo.K12AwardProd READONLY
	)
AS

	SET NOCOUNT ON;

BEGIN
	-- delete old records
	DELETE
		t
	FROM
		dbo.K12AwardProd t
			with (
				tablockx,
				index(ix_K12AwardProd__School__AcYear)
			)
	WHERE
		exists (
			SELECT
				1
			FROM
				@K12AwardProd s
			WHERE
				t.School = s.School
				and t.AcYear = s.AcYear
		);

	-- insert new records
	INSERT INTO
		dbo.K12AwardProd
		(
			derkey1,
			derkey2,
			School,
			AcYear,
			LocStudentId,
			AwardType,
			AwardDate,
			DateAdded,
			batch_id
		)
	SELECT
		derkey1,
		derkey2,
		School,
		AcYear,
		LocStudentId,
		AwardType,
		AwardDate,
		DateAdded,
		batch_id
	FROM
		@K12AwardProd;
END;