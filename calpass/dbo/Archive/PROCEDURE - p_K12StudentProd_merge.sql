USE calpass;

GO

IF (object_id('dbo.p_K12StudentProd_merge') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_K12StudentProd_merge;
	END;

GO

CREATE PROCEDURE
	dbo.p_K12StudentProd_merge
	(
		@district_code char(7)
	)
AS

	SET NOCOUNT ON;

DECLARE
	@message nvarchar(2048),
	@duplicate_count int = 0,
	@insert_count int = 0,
	@delete_count int = 0,
	@record_count int = 0,
	@replicate_length int,
	@replicate_string char(1) = '-';

BEGIN
	BEGIN TRY
		SET ANSI_WARNINGS OFF;

		CREATE TABLE
			#k12sp
			(
				district_code char(7) not null,
				school_code char(7) not null,
				year_code char(9) not null,
				sinf_year_code char(9),
				sela_year_code char(9),
				ssid varchar(10) not null,
				student_id varchar(15),
				name_first varchar(30),
				name_last varchar(50),
				gender char(1),
				birthdate char(8),
				grade_level_code char(2),
				primary_language_code char(2),
				hispanic_ethnicity_indicator char(1),
				race_01 char(3),
				race_02 char(3),
				race_03 char(3),
				race_04 char(3),
				race_05 char(3),
				completion_status_code char(3),
				enroll_exit_date char(8)
			);

		-- collect students
		INSERT INTO
			#k12sp
		SELECT DISTINCT
			r.district_code,
			r.school_code,
			r.year_code,
			sinf_year_code = f.year_code,
			sela_year_code = e.year_code,
			r.ssid,
			r.student_id,
			name_first = coalesce(f.name_first, r.name_first),
			name_last = coalesce(f.name_last, r.name_last),
			gender = coalesce(f.gender, r.gender),
			birthdate = coalesce(f.birthdate, r.birthdate),
			grade_level_code = coalesce(r.grade_level_code, f.grade_level_code),
			primary_language_code = coalesce(e.primary_language_code, f.primary_language_code),
			f.hispanic_ethnicity_indicator,
			f.race_01,
			f.race_02,
			f.race_03,
			f.race_04,
			f.race_05,
			r.completion_status_code,
			r.enroll_exit_date
		FROM
			calpass.calpads.senr r
				with(index(ixc_senr__district_code__school_code__ssid__year_code__enroll_start_date))
			left outer join
			calpass.calpads.sinf f
				with(index(ixc_sinf__district_code__school_code__ssid__year_code__effective_start_date))
				on f.district_code = r.district_code
				and f.school_code = r.school_code
				and f.ssid = r.ssid
				-- unduplicate SINF records
				and f.effective_start_date = (
					SELECT
						max(f1.effective_start_date)
					FROM
						calpass.calpads.sinf f1
						with(index(ixc_sinf__district_code__school_code__ssid__year_code__effective_start_date))
					WHERE
						f1.district_code = f.district_code
						and f1.school_code = f.school_code
						and f1.ssid = f.ssid
						and f1.year_code = f.year_code
						and f1.year_code = (
							SELECT
								max(f2.year_code)
							FROM
								calpass.calpads.sinf f2
								with(index(ixc_sinf__district_code__school_code__ssid__year_code__effective_start_date))
							WHERE
								f2.district_code = r.district_code
								and f2.school_code = r.school_code
								and f2.ssid = r.ssid
								and f2.year_code <= r.year_code
						)
				)
			left outer join
			calpass.calpads.sela e
				with(index(ixc_sela__district_code__ssid__year_code__ela_status_start_date))
				on e.district_code = r.district_code
				and e.ssid = r.ssid
				and e.year_code = (
					SELECT
						max(e1.year_code)
					FROM
						calpass.calpads.sela e1
					WHERE
						e1.district_code = r.district_code
						and e1.ssid = r.ssid
						and e1.year_code <= r.year_code
				)
		WHERE
			-- unduplicate SENR records
			r.enroll_start_date = (
				SELECT
					max(r1.enroll_start_date)
				FROM
					calpass.calpads.senr r1
					with(index(ixc_senr__district_code__school_code__ssid__year_code__enroll_start_date))
				WHERE
					r1.district_code = r.district_code
					and r1.school_code = r.school_code
					and r1.ssid = r.ssid
					and r1.year_code = r.year_code
			)
			and r.district_code = @district_code;

		CREATE CLUSTERED INDEX
			ixc_#k12sp__district_code__school_code__ssid__year_code__grade_level_code
		ON
			#k12sp
			(
				district_code,
				school_code,
				ssid,
				year_code,
				grade_level_code
			);

		-- remove students with 2+ statewide student identifiers
		DELETE
			t
		FROM
			#k12sp t
		WHERE
			exists (
				SELECT
					1
				FROM
					#k12sp s
				GROUP BY
					s.district_code,
					s.school_code,
					s.student_id
				HAVING
					count(distinct s.ssid) > 1
					and s.district_code = t.district_code
					and s.school_code = t.school_code
					and s.student_id = t.student_id
			);

		-- update sinf records more recent that senr record
		WITH
			cte_sinf
		AS
			(
				SELECT DISTINCT
					r.district_code,
					r.school_code,
					r.ssid,
					r.year_code,
					sinf_year_code = f.year_code,
					f.name_first,
					f.name_last,
					f.gender,
					f.birthdate,
					f.grade_level_code,
					f.primary_language_code,
					f.hispanic_ethnicity_indicator,
					f.race_01,
					f.race_02,
					f.race_03,
					f.race_04,
					f.race_05
				FROM
					#k12sp r
					inner join
					calpass.calpads.sinf f
						with(index(ixc_sinf__district_code__school_code__ssid__year_code__effective_start_date))
						on f.district_code = r.district_code
						and f.school_code = r.school_code
						and f.ssid = r.ssid
						and f.year_code > r.year_code
				WHERE
					(
						r.sinf_year_code is null
						and
						(
							r.name_last is null
							or r.name_first is null
							or r.gender is null
							or r.birthdate is null
							or r.primary_language_code is null
							or r.hispanic_ethnicity_indicator is null
							or r.race_01 is null
						)
					)
					-- unduplicate SINF records
					and f.effective_start_date = (
						SELECT
							max(f1.effective_start_date)
						FROM
							calpass.calpads.sinf f1
							with(index(ixc_sinf__district_code__school_code__ssid__year_code__effective_start_date))
						WHERE
							f1.district_code = f.district_code
							and f1.school_code = f.school_code
							and f1.ssid = f.ssid
							and f1.year_code = f.year_code
							and f1.year_code = (
								SELECT
									min(f2.year_code)
								FROM
									calpass.calpads.sinf f2
									with(index(ixc_sinf__district_code__school_code__ssid__year_code__effective_start_date))
								WHERE
									f2.district_code = r.district_code
									and f2.school_code = r.school_code
									and f2.ssid = r.ssid
									and f2.year_code > r.year_code
							)
					)
			)
		UPDATE
			t
		SET
			t.sinf_year_code = s.sinf_year_code,
			t.name_first = case
					when t.name_first is not null then t.name_first
					else s.name_first
				end,
			t.name_last = case
					when t.name_last is not null then t.name_last
					else s.name_last
				end,
			t.gender = case
					when t.gender is not null then t.gender
					else s.gender
				end,
			t.birthdate = case
					when t.birthdate is not null then t.birthdate
					else s.birthdate
				end,
			t.grade_level_code = case
					when t.grade_level_code is not null then t.grade_level_code
					else s.grade_level_code
				end,
			t.primary_language_code = case
					when t.primary_language_code is not null then t.primary_language_code
					else s.primary_language_code
				end,
			t.hispanic_ethnicity_indicator = s.hispanic_ethnicity_indicator,
			t.race_01 = s.race_01,
			t.race_02 = s.race_02,
			t.race_03 = s.race_03,
			t.race_04 = s.race_04,
			t.race_05 = s.race_05
		FROM
			#k12sp t
			inner join
			cte_sinf s
				on t.district_code = s.district_code
				and t.school_code = s.school_code
				and t.ssid = s.ssid
		WHERE
			not exists (
				SELECT
					0
				FROM
					cte_sinf s1
				GROUP BY
					s1.district_code,
					s1.school_code,
					s1.ssid,
					s1.year_code
				HAVING
					count(*) > 1
					and s1.district_code = s.district_code
					and s1.school_code = s.school_code
					and s1.ssid = s.ssid
					and s1.year_code = s.year_code
			);

		-- update sela records more recent that senr record
		WITH
			cte_sela
		AS
			(
				SELECT DISTINCT
					r.district_code,
					r.school_code,
					r.ssid,
					r.year_code,
					sela_year_code = e.year_code,
					e.primary_language_code
				FROM
					#k12sp r
					inner join
					calpass.calpads.sela e
						with(index(ixc_sela__district_code__ssid__year_code__ela_status_start_date))
						on e.district_code = r.district_code
						and e.ssid = r.ssid
						and e.year_code > r.year_code
				WHERE
					r.sela_year_code is null
					and e.year_code = (
						SELECT
							min(e1.year_code)
						FROM
							calpass.calpads.sela e1
							with(index(ixc_sela__district_code__ssid__year_code__ela_status_start_date))
						WHERE
							e1.district_code = r.district_code
							and e1.ssid = r.ssid
							and e1.year_code > r.year_code
					)
			)
		UPDATE
			t
		SET
			t.sela_year_code = s.sela_year_code,
			t.primary_language_code = s.primary_language_code
		FROM
			#k12sp t
			inner join
			cte_sela s
				on t.district_code = s.district_code
				and t.ssid = s.ssid
		WHERE
			not exists (
				SELECT
					0
				FROM
					cte_sela s1
				GROUP BY
					s1.district_code,
					s1.ssid,
					s1.year_code
				HAVING
					count(*) > 1
					and s1.district_code = s.district_code
					and s1.ssid = s.ssid
					and s1.year_code = s.year_code
			);

		-- Delete lower GradeLevel for students with duplicate grade levels
		DELETE
			t
		FROM
			#k12sp t
		WHERE
			exists (
				SELECT
					1
				FROM
					#k12sp s
				GROUP BY
					s.district_code,
					s.school_code,
					s.ssid,
					s.year_code
				HAVING
					s.district_code = t.district_code
					and s.school_code = t.school_code
					and s.ssid = t.ssid
					and s.year_code = t.year_code
					and count(distinct s.grade_level_code) > 1
					and min(s.grade_level_code) = t.grade_level_code
			);

		-- remove students with no derkey variables
		DELETE
		FROM
			#k12sp
		WHERE
			student_id is null
			or name_first is null
			or name_last is null
			or gender is null
			or birthdate is null;

		-- Delete any remaning duplicates
		DELETE
			t
		FROM
			#k12sp t
		WHERE
			exists (
				SELECT
					1
				FROM
					#k12sp s
				GROUP BY
					s.district_code,
					s.school_code,
					s.ssid,
					s.year_code,
					s.grade_level_code
				HAVING
					s.district_code = t.district_code
					and s.school_code = t.school_code
					and s.ssid = t.ssid
					and s.year_code = t.year_code
					and s.grade_level_code = t.grade_level_code
					and count(*) > 1
			);
			
		SET @duplicate_count = @@rowcount;

		CREATE TABLE
			#K12StudentProd
			(
				Derkey1 char(15) not null,
				Derkey2 char(21) not null,
				School char(14) not null,
				AcYear char(4) not null,
				LocStudentId varchar(15) not null,
				StudentId varchar(15),
				CSISNum varchar(10),
				Fname varchar(30),
				Lname varchar(50),
				Gender char(1),
				Ethnicity char(3),
				Birthdate char(8),
				GradeLevel char(2),
				HomeLanguage char(2),
				HispanicEthnicity char(1),
				EthnicityCode1 char(3),
				EthnicityCode2 char(3),
				EthnicityCode3 char(3),
				EthnicityCode4 char(3),
				EthnicityCode5 char(3),
				AwardType char(3),
				AwardDate char(8),
				DateAdded datetime
			);

		-- Insert records into production table
		INSERT INTO
			#K12StudentProd
			(
				derkey1,
				derkey2,
				School,
				AcYear,
				LocStudentId,
				StudentId,
				CSISNum,
				Fname,
				Lname,
				Gender,
				Ethnicity,
				Birthdate,
				GradeLevel,
				HomeLanguage,
				HispanicEthnicity,
				EthnicityCode1,
				EthnicityCode2,
				EthnicityCode3,
				EthnicityCode4,
				EthnicityCode5,
				AwardType,
				AwardDate,
				DateAdded
			)
		SELECT
			Derkey1 = calpass.dbo.Derkey1(
				upper(replace(replace(replace(name_first, char(39), ''), char(46), ''), char(32), '')),
				upper(replace(replace(replace(name_last, char(39), ''), char(46), ''), char(32), '')),
				gender,
				birthdate
			),
			Derkey2 = calpass.dbo.Derkey1(
				replace(replace(replace(name_first, char(39), ''), char(46), ''), char(32), ''),
				replace(replace(replace(name_last, char(39), ''), char(46), ''), char(32), ''),
				gender,
				birthdate
			) + substring(school_code, 1, 6),
			School = district_code + school_code,
			AcYear = substring(year_code, 3, 2) + substring(year_code, 8, 2),
			LocStudentId = calpass.dbo.get1289Encryption(student_id, 'X'),
			StudentId = null,
			CSISNum = calpass.dbo.get9812Encryption(ssid, 'X'),
			Fname = upper(substring(replace(replace(replace(name_first, char(39), ''), char(46), ''), char(32), ''), 1, 3)),
			Lname = upper(substring(replace(replace(replace(name_last, char(39), ''), char(46), ''), char(32), ''), 1, 3)),
			Gender = gender,
			Ethnicity = null,
			Birthdate = birthdate,
			GradeLevel = grade_level_code,
			HomeLanguage = case
					when primary_language_code = 'UU' then '99'
					else primary_language_code
				end,
			HispanicEthnicity = hispanic_ethnicity_indicator,
			EthnicityCode1 = race_01,
			EthnicityCode2 = race_02,
			EthnicityCode3 = race_03,
			EthnicityCode4 = race_04,
			EthnicityCode5 = race_05,
			AwardType = completion_status_code,
			AwardDate = enroll_exit_date,
			DateAdded = getdate()
		FROM
			#k12sp;

		CREATE CLUSTERED INDEX
			ixc_#K12StudentProd__School__LocStudentId__AcYear
		ON
			#K12StudentProd
			(
				School,
				LocStudentId,
				AcYear
			);

		-- Delete matched records from production table
		DELETE
			t
		FROM
			calpass.dbo.K12StudentProd t
			inner join
			#K12StudentProd s
				on s.school = t.School
				and s.LocStudentId = t.LocStudentId
				and s.AcYear = t.AcYear;
		
		SET @delete_count = @@rowcount;

		INSERT INTO
			calpass.dbo.K12StudentProd
			(
				derkey1,
				derkey2,
				School,
				AcYear,
				LocStudentId,
				StudentId,
				CSISNum,
				Fname,
				Lname,
				Gender,
				Ethnicity,
				Birthdate,
				GradeLevel,
				HomeLanguage,
				HispanicEthnicity,
				EthnicityCode1,
				EthnicityCode2,
				EthnicityCode3,
				EthnicityCode4,
				EthnicityCode5,
				DateAdded
			)
		SELECT
			derkey1,
			derkey2,
			School,
			AcYear,
			LocStudentId,
			StudentId,
			CSISNum,
			Fname,
			Lname,
			Gender,
			Ethnicity,
			Birthdate,
			GradeLevel,
			HomeLanguage,
			HispanicEthnicity,
			EthnicityCode1,
			EthnicityCode2,
			EthnicityCode3,
			EthnicityCode4,
			EthnicityCode5,
			DateAdded
		FROM
			#K12StudentProd;
		
		SET @insert_count = @@rowcount;

		-- set total records modified
		SET @record_count = @insert_count - @delete_count;
		-- set length to replicate
		SELECT
			@replicate_length = len(max(tbl.col))
		FROM
			(
				VALUES
				(@insert_count),
				(@delete_count)
			) tbl (col);

		-- set progress message
		SET @message = char(9) + N'Data merged successfully to K12StudentProd' + char(13) + char(10) + 
			char(9) + char(9) + 'Dups Ignored: ' + cast(@duplicate_count as nvarchar(255)) + char(13) + char(10) + 
			char(9) + char(9) + 'Delete Count: ' + cast(@delete_count as nvarchar(255)) + char(13) + char(10) + 
			char(9) + char(9) + 'Insert Count: ' + cast(@insert_count as nvarchar(255)) + char(13) + char(10) + 
			char(9) + char(9) + '--------------' + replicate(@replicate_string, @replicate_length) + char(13) + char(10) + 
			char(9) + char(9) + 'Record Count: ' + cast(@record_count as nvarchar(255));
		-- exe progress message
		RAISERROR(@message, 0, 1) WITH NOWAIT;

		-- Delete matched records from production table
		DELETE
			t
		FROM
			calpass.dbo.K12AwardProd t
			inner join
			#K12StudentProd s
				on s.school = t.School
				and s.LocStudentId = t.LocStudentId
				and s.AcYear = t.AcYear;
		
		SET @delete_count = @@rowcount;

		INSERT INTO
			calpass.dbo.K12AwardProd
			(
				derkey1,
				derkey2,
				School,
				AcYear,
				LocStudentId,
				AwardType,
				AwardDate,
				DateAdded
			)
		SELECT
			derkey1,
			derkey2,
			School,
			AcYear,
			LocStudentId,
			AwardType,
			AwardDate,
			DateAdded
		FROM
			#K12StudentProd
		WHERE
			AwardType not in ('480', '360')
		
		SET @insert_count = @@rowcount;

		-- set total records modified
		SET @record_count = @insert_count - @delete_count;
		-- set length to replicate
		SELECT
			@replicate_length = len(max(tbl.col))
		FROM
			(
				VALUES
				(@insert_count),
				(@delete_count)
			) tbl (col);

		-- set progress message
		SET @message = char(9) + N'Data merged successfully to K12AwardProd' + char(13) + char(10) + 
			char(9) + char(9) + 'Dups Ignored: ' + cast(@duplicate_count as nvarchar(255)) + char(13) + char(10) + 
			char(9) + char(9) + 'Delete Count: ' + cast(@delete_count as nvarchar(255)) + char(13) + char(10) + 
			char(9) + char(9) + 'Insert Count: ' + cast(@insert_count as nvarchar(255)) + char(13) + char(10) + 
			char(9) + char(9) + '--------------' + replicate(@replicate_string, @replicate_length) + char(13) + char(10) + 
			char(9) + char(9) + 'Record Count: ' + cast(@record_count as nvarchar(255));
		-- exe progress message
		RAISERROR(@message, 0, 1) WITH NOWAIT;

		DROP TABLE #k12sp;
		DROP TABLE #K12StudentProd;

		SET ANSI_WARNINGS ON;

	END TRY
	BEGIN CATCH
		-- set progress message
		SET @message =
			nchar(9) + N'Error Number:     ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + nchar(13) + nchar(10) +
			nchar(9) + N'Error Severity:   ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + nchar(13) + nchar(10) +
			nchar(9) + N'Error State:      ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + nchar(13) + nchar(10) +
			nchar(9) + N'Error Procedure:  ' + object_name(@@PROCID) + nchar(13) + nchar(10) +
			nchar(9) + N'Error Line:       ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + nchar(13) + nchar(10) +
			nchar(9) + N'Error Message:    ' + isnull(ERROR_MESSAGE(), N'');
		-- exe progress message
		THROW 70099, @message, 1;
	END CATCH;
END;