USE calpass;

GO

IF (object_id('dbo.p_K12CourseProd_proliferate') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_K12CourseProd_proliferate;
	END;

GO

CREATE PROCEDURE
	dbo.p_K12CourseProd_proliferate
AS

	SET NOCOUNT ON;

DECLARE
	@message nvarchar(2048),
	@IsEscrow bit = 0,
	@SectionEscrow calpads.SectionEscrow,
	@SectionEscrowBatch calpads.SectionEscrow,
	@DistrictEnumerable calpads.DistrictEnumerable,
	@i int = 0,
	@c int,
	@DistrictCode char(7);

BEGIN
	SET @message = '-=SECTION=-';
	RAISERROR(@message, 0, 1) WITH NOWAIT;

	-- populate @SectionEscrow
	INSERT INTO
		@SectionEscrow
	EXECUTE calpads.p_SectionEscrow_select;

	-- populate @DistrictEnumerable
	INSERT INTO
		@DistrictEnumerable
		(
			DistrictCode
		)
	SELECT DISTINCT
		DistrictCode
	FROM
		@SectionEscrow;

	SET @c = @@ROWCOUNT;

	WHILE (@i < @c)
	BEGIN
		SELECT
			@DistrictCode = DistrictCode
		FROM
			@DistrictEnumerable
		WHERE
			i = @i;

		SET @message = nchar(9) + 'Processing District (' + convert(varchar, @i + 1) + ' of ' + convert(varchar, @c) + '): ' + @DistrictCode;
		RAISERROR(@message, 0, 1) WITH NOWAIT;
		
		-- clear records from @SectionEscrowBatch
		DELETE @SectionEscrowBatch;
		
		-- populate @SectionEscrowBatch
		INSERT INTO
			@SectionEscrowBatch
			(
				DistrictCode,
				SchoolCode,
				YearCode
			)
		SELECT
			DistrictCode,
			SchoolCode,
			YearCode
		FROM
			@SectionEscrow
		WHERE
			DistrictCode = @DistrictCode;
		
		-- COURSE
		BEGIN TRY
			EXECUTE dbo.p_K12CourseProd_process
				@SectionEscrow = @SectionEscrowBatch;
		END TRY
		BEGIN CATCH
			-- set error message
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + upper('dbo.p_K12CourseProd_process') + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			-- put error message
			RAISERROR(@message, 0, 1) WITH NOWAIT;
			-- iterate
			SET @i += 1;
			-- loop
			CONTINUE;
		END CATCH;

		-- ESCROW
		-- update calpads.scsc IsEscrow values
		UPDATE
			s
		SET
			s.IsEscrow = @IsEscrow
		FROM
			calpads.scsc s
				with(index(ixc_scsc__district_code__school_code__year_code__term_code__course_id__section_id__marking_period_code__ssid))
		WHERE
			s.district_code = @DistrictCode;

		-- update calpads.crsc IsEscrow values
		UPDATE
			c
		SET
			c.IsEscrow = @IsEscrow
		FROM
			calpads.crsc c
				with(index(ixc_crsc__district_code__school_code__year_code__term_code__course_id__section_id__seid))
		WHERE
			c.district_code = @DistrictCode;

		-- iterate
		SET @i += 1;

	END;
	
END;