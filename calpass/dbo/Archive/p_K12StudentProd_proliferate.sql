USE calpass;

GO

IF (object_id('dbo.p_K12StudentProd_proliferate') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_K12StudentProd_proliferate;
	END;

GO

CREATE PROCEDURE
	dbo.p_K12StudentProd_proliferate
AS

	SET NOCOUNT ON;

DECLARE
	@message nvarchar(2048),
	@EscrowComplete bit = 0,
	@StudentEscrow calpads.StudentEscrow,
	@StudentEscrowBatch calpads.StudentEscrow,
	@DistrictEnumerable calpads.DistrictEnumerable,
	@i int = 0,
	@c int,
	@DistrictCode char(7);

BEGIN
	SET @message = '-=STUDENT=-';
	RAISERROR(@message, 0, 1) WITH NOWAIT;

	-- populate @StudentEscrow
	INSERT INTO
		@StudentEscrow
	EXECUTE calpads.p_StudentEscrow_select;

	-- populate @DistrictEnumerable
	INSERT INTO
		@DistrictEnumerable
		(
			DistrictCode
		)
	SELECT DISTINCT
		DistrictCode
	FROM
		@StudentEscrow;

	SET @c = @@ROWCOUNT;

	WHILE (@i < @c)
	BEGIN
		SELECT
			@DistrictCode = DistrictCode
		FROM
			@DistrictEnumerable
		WHERE
			i = @i;

		SET @message = nchar(9) + 'Processing District (' + convert(varchar, @i + 1) + ' of ' + convert(varchar, @c) + '): ' + @DistrictCode;
		RAISERROR(@message, 0, 1) WITH NOWAIT;
		
		-- clear records from @StudentEscrowBatch
		DELETE @StudentEscrowBatch;
		
		-- populate @StudentEscrowBatch
		INSERT INTO
			@StudentEscrowBatch
			(
				DistrictCode,
				YearCode
			)
		SELECT
			DistrictCode,
			YearCode
		FROM
			@StudentEscrow
		WHERE
			DistrictCode = @DistrictCode;
		
		-- STUDENT & AWARD
		BEGIN TRY
			EXECUTE dbo.p_K12StudentProd_process
				@StudentEscrow = @StudentEscrowBatch;
		END TRY
		BEGIN CATCH
			-- set error message
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + upper('dbo.p_K12StudentProd_process') + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			-- put error message
			RAISERROR(@message, 0, 1) WITH NOWAIT;
			-- iterate
			SET @i += 1;
			-- loop
			CONTINUE;
		END CATCH;

		-- ESCROW
		-- update calpads.senr IsEscrow values
		UPDATE
			r
		SET
			r.IsEscrow = @EscrowComplete
		FROM
			calpads.Senr r
				with(index(ix_Senr__District))
		WHERE
			r.DistrictCode = @DistrictCode;

		-- update calpads.sinf IsEscrow values
		UPDATE
			f
		SET
			f.IsEscrow = @EscrowComplete
		FROM
			calpads.Sinf f
				with(index(ix_Sinf__District))
		WHERE
			f.DistrictCode = @DistrictCode;

		-- update calpads.sela IsEscrow values
		UPDATE
			e
		SET
			e.IsEscrow = @EscrowComplete
		FROM
			calpads.Sela e
				with(index(ix_Sela__District))
		WHERE
			e.DistrictCode = @DistrictCode;

		-- iterate
		SET @i += 1;

	END;
	
END;