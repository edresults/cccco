USE calpass;

IF (object_id('dbo.p_K12CourseProd_select') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_K12CourseProd_select;
	END;

GO

CREATE PROCEDURE
	dbo.p_K12CourseProd_select
	(
		@SectionEscrow calpads.SectionEscrow READONLY
	)
AS

	SET NOCOUNT ON;

BEGIN
	SELECT DISTINCT
		Derkey1 = calpass.dbo.Derkey1(
			upper(replace(replace(replace(s.name_first, char(39), ''), char(46), ''), char(32), '')),
			upper(replace(replace(replace(s.name_last, char(39), ''), char(46), ''), char(32), '')),
			s.gender,
			s.birthdate
		),
		Derkey2 = calpass.dbo.Derkey1(
			upper(replace(replace(replace(s.name_first, char(39), ''), char(46), ''), char(32), '')),
			upper(replace(replace(replace(s.name_last, char(39), ''), char(46), ''), char(32), '')),
			s.gender,
			s.birthdate
		) + substring(s.school_code, 1, 6),
		School = s.district_code + s.school_code,
		AcYear = substring(s.year_code, 3, 2) + substring(s.year_code, 8, 2),
		LocStudentId = substring(calpass.dbo.get1289Encryption(s.student_id, 'X'), 1, 15),
		CourseId = c.course_code,
		LocallyAssignedCourseNumber = c.course_id,
		CourseSectionClassNumber = s.section_id,
		CourseTitle = substring(c.course_name, 1, 40),
		AGstatus = s.university_code,
		Grade = s.course_grade,
		CreditEarned = s.credit_earned,
		CreditAttempted = s.credit_attempted,
		CourseLevel = case
				when c.section_level_code = '10' then '35'
				when c.section_level_code = '12' then '33'
				when c.section_level_code = '14' then '39'
				when c.section_level_code = '15' then '34'
				when c.section_level_code = '16' then '40'
				when cccd.ib_ind = 1 then '38'
				when cccd.ap_ind = 1 then '30'
				when c.course_code = '2402' then '35'
				else 'XX'
			end,
		CourseType = case
				when c.cte_indicator = 'Y' and c.program_funding_source = '1' then '33'
				when c.cte_indicator = 'Y' and c.program_funding_source != '1' then '32'
				when c.cte_indicator = 'N' and c.program_funding_source = '1' then '30'
				else 'XX'
			end,
		CourseTerm = coalesce(s.marking_period_code, s.term_code),
		DateAdded = getdate()
	FROM
		calpads.scsc s
		inner join
		calpads.crsc c
			on c.district_code = s.district_code
			and c.school_code = s.school_code
			and c.year_code = s.year_code
			and c.term_code = s.term_code
			and c.course_id = s.course_id
			and c.section_id = s.section_id
		left outer join
		calpads.crsc_course_code_detail cccd
			on cccd.code = c.course_code
	WHERE
		exists (
			SELECT
				1
			FROM
				@SectionEscrow e
			WHERE
				e.DistrictCode = s.district_code
				and e.SchoolCode = s.school_code
				and e.YearCode = s.year_code
		);
END;