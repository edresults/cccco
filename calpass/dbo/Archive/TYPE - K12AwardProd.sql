USE calpass;

GO

IF (type_id('dbo.K12AwardProd') is not null)
	BEGIN
		DROP TYPE dbo.K12AwardProd;
	END;

GO

CREATE TYPE
	dbo.K12AwardProd
AS TABLE
	(
		Derkey1 char(15),
		School char(14),
		AcYear char(4),
		LocStudentId varchar(15),
		AwardType char(3),
		AwardDate char(8),
		DateAdded datetime default ( getdate() ),
		batch_id int
	);