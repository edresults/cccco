USE calpass;

GO

IF (object_id('dbo.K12StudentProdGet') is not null)
	BEGIN
		DROP FUNCTION dbo.K12StudentProdGet;
	END;

GO

CREATE FUNCTION
	dbo.K12StudentProdGet
	(
		@StudentStateId char(10),
		@SchoolCode char(7),
		@YearCode char(9)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			Derkey1 = 
				HASHBYTES(
					'SHA2_512',
					upper(convert(nchar(3), f.NameFirst)) + 
					upper(convert(nchar(3), f.NameLast)) + 
					upper(convert(nchar(1), f.Gender)) + 
					convert(nchar(8), f.Birthdate)
				),
			School = r.DistrictCode + r.SchoolCode,
			AcYear = y.YearCodeAbbr,
			LocStudentId = substring(calpass.dbo.get1289Encryption(f.StudentLocalId, 'X'), 1, 15),
			CSISNum = calpass.dbo.get9812Encryption(r.StudentStateId, 'X'),
			Fname = f.NameFirst,
			Lname = f.NameLast,
			Gender = f.Gender,
			Birthdate = f.Birthdate,
			GradeLevel = r.GradeCode,
			HomeLanguage =
				case
					when PrimaryLanguageCode is null then '99'
					when PrimaryLanguageCode = 'UU' then '99'
					else PrimaryLanguageCode
				end,
			HispanicEthnicity = f.IsHispanicEthnicity,
			EthnicityCode1 = f.Race01,
			EthnicityCode2 = f.Race02,
			EthnicityCode3 = f.Race03,
			EthnicityCode4 = f.Race04,
			EthnicityCode5 = f.Race05,
			AwardType = r.CompletionStatusCode,
			AwardDate = r.EnrollExitDate
		FROM
			(
				SELECT
					r.StudentStateId,
					r.SchoolCode,
					r.YearCode,
					r.GradeCode,
					r.DistrictCode,
					r.CompletionStatusCode,
					r.EnrollExitDate
				FROM
					(
						SELECT
							r.StudentStateId,
							r.SchoolCode,
							r.YearCode,
							r.GradeCode,
							r.DistrictCode,
							r.CompletionStatusCode,
							r.EnrollExitDate,
							NonDeterministicSelector = 
								row_number()
								over(
									partition by
										r.StudentStateId,
										r.SchoolCode,
										r.YearCode,
										r.GradeCode
									order by
										r.EnrollExitDate desc,
										(select 1) desc
								)
						FROM
							calpads.Senr r with (index(pk_Senr))
						WHERE
							r.StudentStateId = @StudentStateId
							and (
								@SchoolCode is null
								or
								r.SchoolCode = @SchoolCode
							)
							and (
								@YearCode is null
								or
								r.YearCode = @YearCode
							)
							and r.EnrollStartDate = (
								SELECT
									max(r1.EnrollStartDate)
								FROM
									calpads.Senr r1 with(index(pk_Senr))
								WHERE
									r1.StudentStateId = r.StudentStateId
									and r1.SchoolCode = r.SchoolCode
									and r1.YearCode = r.YearCode
							)
					) r
				WHERE
					r.NonDeterministicSelector = 1
			) r
			-- required sinf record
			inner join
			calpads.Sinf f with (index(pk_Sinf))
				on f.StudentStateId = r.StudentStateId
				and f.SchoolCode = r.SchoolCode
			inner join
			calpads.Year y
				on y.YearCode = r.YearCode
			-- optional sela record
			left outer join
			calpads.Sela l with (index(pk_Sela))
				on l.StudentStateId = r.StudentStateId
				and l.SchoolCode = r.SchoolCode
				and l.ElaStatusStartDate = (
					SELECT
						max(l1.ElaStatusStartDate)
					FROM
						calpads.Sela l1 with(index(pk_Sela))
					WHERE
						l1.StudentStateId = l.StudentStateId
						and l1.SchoolCode = l.SchoolCode
						and l1.YearCode = l.YearCode
						and l1.YearCode = (
							SELECT
								max(l2.YearCode)
							FROM
								calpads.Sela l2 with(index(pk_Sela))
							WHERE
								l2.StudentStateId = l1.StudentStateId
								and l2.SchoolCode = l1.SchoolCode
								and l2.YearCode <= r.YearCode
						)
				)
		WHERE
			f.EffectiveStartDate = (
				SELECT
					max(f1.EffectiveStartDate)
				FROM
					calpads.Sinf f1 with(index(pk_Sinf))
				WHERE
					f1.StudentStateId = f.StudentStateId
					and f1.SchoolCode = f.SchoolCode
					and f1.YearCode = f.YearCode
					and f1.YearCode = (
						SELECT
							max(f2.YearCode)
						FROM
							calpads.Sinf f2 with(index(pk_Sinf))
						WHERE
							f2.StudentStateId = f1.StudentStateId
							and f2.SchoolCode = f1.SchoolCode
							and f2.YearCode <= r.YearCode
					)
			)
	);