use calpass;

GO

IF (object_id('f_hs_student_concurrency', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_hs_student_concurrency;
	END;

GO

CREATE FUNCTION
	f_hs_student_concurrency
	(
		@derkey1 char(15),
		@AcYear char(4)
	)
RETURNS
	TABLE
AS
RETURN (
	SELECT
		ccc_concurrent_section_enrollment_count = count(*),
		ccc_concurrent_section_units_attempted = sum(
				case
					when ccc.UnitsAttempted >= 88.88 then 0.00
					when ccc.UnitsAttempted is null then 0.00
					else ccc.UnitsAttempted
				end
			),
		ccc_concurrent_section_units_achieved = sum(
				case
					when ccc.UnitsEarned >= 88.88 then 0.00
					when ccc.UnitsEarned is null then 0.00
					else ccc.UnitsEarned
				end
			),
		ccc_concurrent_section_grade_points = sum(
				case
					when ccc.UnitsEarned >= 88.88 then 0.00
					when ccc.UnitsEarned is null then 0.00
					else ccc.UnitsEarned
				end * g.points
			)
	FROM
		calpass.dbo.CCStudentProd ccs
			with(index(idx_ccstud_derkey))
		inner join
		calpass.dbo.CCCourseProd ccc
			with(index(idx_CCCourseProd_pk))
			on ccs.collegeId = ccc.collegeId
			and ccs.studentId = ccc.studentId
			and ccs.termId = ccc.termId
		inner join
		calpass.dbo.CCCourseProd_grade g
			on g.code = ccc.grade
		inner join
		calpass.dbo.CCStudentProd_termId t
			on t.code = ccs.termId
	WHERE
		ccs.derkey1 = @derkey1
		and t.year_code = @AcYear
		and g.completion_denominator_ind = 1
		and ccc.creditFlag in ('T', 'D', 'C', 'S')
	GROUP BY
		ccs.derkey1
);