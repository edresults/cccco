USE calpass;

GO

IF (object_id('calpass.dbo.f_hs_hf_first', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_hs_hf_first;
	END;

GO

CREATE FUNCTION
	dbo.f_hs_hf_first
	(
		@derkey1 char(15)
	)
RETURNS
	TABLE
AS
RETURN (
	SELECT
		c.derkey1,
		hf.*
	FROM
		comis.dbo.hf_first hf
		inner join
		calpass.dbo.f_legacy_to_comis_decrypt(
			@derkey1
		) c
			on c.college_id = hf.college_id
			and c.student_id = hf.student_id
);