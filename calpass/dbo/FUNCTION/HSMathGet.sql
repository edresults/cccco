USE calpass;

GO

IF (object_id('dbo.HSMathGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.HSMathGet;
	END;

GO

CREATE FUNCTION
	dbo.HSMathGet
	(
		@Derkey1 binary(64)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			ContentAreaLastYearTermCode = max(ContentAreaLastYearTermCode),
			LastYearTermCode = LastYearTermCode,
			ArithmeticTotalContentPoints = max(case when d.ContentCode = 'Arithmetic' then d.TotalContentPoints end),
			PreAlgebraTotalContentPoints = max(case when d.ContentCode = 'PreAlgebra' then d.TotalContentPoints end),
			AlgebraITotalContentPoints = max(case when d.ContentCode = 'AlgebraI' then d.TotalContentPoints end),
			GeometryTotalContentPoints = max(case when d.ContentCode = 'Geometry' then d.TotalContentPoints end),
			AlgebraIITotalContentPoints = max(case when d.ContentCode = 'AlgebraII' then d.TotalContentPoints end),
			StatisticsTotalContentPoints = max(case when d.ContentCode = 'Statistics' then d.TotalContentPoints end),
			TrigonometryTotalContentPoints = max(case when d.ContentCode = 'Trigonometry' then d.TotalContentPoints end),
			PreCalculusTotalContentPoints = max(case when d.ContentCode = 'PreCalculus' then d.TotalContentPoints end),
			CalculusITotalContentPoints = max(case when d.ContentCode = 'CalculusI' then d.TotalContentPoints end),
			CalculusIITotalContentPoints = max(case when d.ContentCode = 'CalculusII' then d.TotalContentPoints end),
			ArithmeticMarkCategory = max(case when d.ContentCode = 'Arithmetic' then mb.Category end),
			PreAlgebraMarkCategory = max(case when d.ContentCode = 'PreAlgebra' then mb.Category end),
			AlgebraIMarkCategory = max(case when d.ContentCode = 'AlgebraI' then mb.Category end),
			GeometryMarkCategory = max(case when d.ContentCode = 'Geometry' then mb.Category end),
			AlgebraIIMarkCategory = max(case when d.ContentCode = 'AlgebraII' then mb.Category end),
			StatisticsMarkCategory = max(case when d.ContentCode = 'Statistics' then mb.Category end),
			TrigonometryMarkCategory = max(case when d.ContentCode = 'Trigonometry' then mb.Category end),
			PreCalculusMarkCategory = max(case when d.ContentCode = 'PreCalculus' then mb.Category end),
			CalculusIMarkCategory = max(case when d.ContentCode = 'CalculusI' then mb.Category end),
			CalculusIIMarkCategory = max(case when d.ContentCode = 'CalculusII' then mb.Category end),
			ArithmeticTotalCreditTry = max(case when d.ContentCode = 'Arithmetic' then d.TotalCreditTry end),
			PreAlgebraTotalCreditTry = max(case when d.ContentCode = 'PreAlgebra' then d.TotalCreditTry end),
			AlgebraITotalCreditTry = max(case when d.ContentCode = 'AlgebraI' then d.TotalCreditTry end),
			GeometryTotalCreditTry = max(case when d.ContentCode = 'Geometry' then d.TotalCreditTry end),
			AlgebraIITotalCreditTry = max(case when d.ContentCode = 'AlgebraII' then d.TotalCreditTry end),
			StatisticsTotalCreditTry = max(case when d.ContentCode = 'Statistics' then d.TotalCreditTry end),
			TrigonometryTotalCreditTry = max(case when d.ContentCode = 'Trigonometry' then d.TotalCreditTry end),
			PreCalculusTotalCreditTry = max(case when d.ContentCode = 'PreCalculus' then d.TotalCreditTry end),
			CalculusITotalCreditTry = max(case when d.ContentCode = 'CalculusI' then d.TotalCreditTry end),
			CalculusIITotalCreditTry = max(case when d.ContentCode = 'CalculusII' then d.TotalCreditTry end)
		FROM
			(
				SELECT
					t.LastYearTermCode,
					ContentAreaLastYearTermCode = max(t.YearTermCode),
					crct.ContentCode,
					TotalQualityPoints = sum(t.QualityPoints),
					TotalCreditTry = sum(t.CreditTry),
					TotalContentPoints = isnull(convert(decimal(4,3), sum(t.QualityPoints) / nullif(sum(t.CreditTry), 0)), 0)
				FROM
					dbo.HSTranscriptGet(@Derkey1) t
					inner join
					calpads.Course crse
						on crse.Code = t.CourseCode
					inner join
					calpads.CourseContent crct
						on crct.CourseCode = crse.Code
					inner join
					calpads.Content cont
						on cont.Code = crct.ContentCode
				WHERE
					crse.DepartmentCode = 18
				GROUP BY
					t.LastYearTermCode,
					crct.ContentCode
			) d
			inner join
			calpads.MarkBound mb
				on d.TotalContentPoints >= mb.BoundLower
				and d.TotalContentPoints <= mb.BoundUpper
		GROUP BY
			LastYearTermCode
	);