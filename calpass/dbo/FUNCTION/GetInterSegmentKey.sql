USE calpass;

GO

IF (object_id('dbo.GetInterSegmentKey', 'IF') is not null)
    BEGIN
        DROP FUNCTION dbo.GetInterSegmentKey;
    END;

GO

CREATE FUNCTION
    dbo.GetInterSegmentKey
    (
        @SSID char(10)
    )
RETURNS
    TABLE
AS
    RETURN (
        SELECT TOP 1
            i.InterSegmentKey
        FROM
            calpads.sinf f
            cross apply
            dbo.InterSegmentKey(f.NameFirst, f.NameLast, f.Gender, f.Birthdate) i
        WHERE
            isnull(f.EffectiveEndDate, '99999999') = (
                SELECT
                    max(isnull(f1.EffectiveEndDate, '99999999'))
                FROM
                    calpads.sinf f1
                WHERE
                    f1.EffectiveStartDate =  (
                        SELECT
                            max(f2.EffectiveStartDate)
                        FROM
                            calpads.sinf f2
                        WHERE
                            f2.StudentStateId = f1.StudentStateId
                    ) and
                    f1.StudentStateId     = f.StudentStateId and
                    f1.EffectiveStartDate = f.EffectiveStartDate
            ) and
            f.StudentStateId = @SSID
    );