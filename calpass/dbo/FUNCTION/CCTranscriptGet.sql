USE calpass;

GO

IF (object_id('dbo.CCTranscriptGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.CCTranscriptGet;
	END;

GO

CREATE FUNCTION
	dbo.CCTranscriptGet
	(
		@Derkey1 binary(64)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			/* organization */
			s.CollegeId,
			s.StudentId,
			t.TermCode,
			t.YearTermCode,
			/* course */
			CourseTitle = c.Title,
			c.CourseId,
			c.TopCode,
			CourseLevelCode = c.CollegeLevel,
			CourseLevelBelow = l.LevelsBelow,
			/* section */
			m.MarkCode,
			m.IsSuccess,
			MarkCategory = m.Category,
			MarkPoints = m.Points,
			CreditTry = c.UnitsAttempted * IsGpa,
			CreditGet = c.UnitsEarned * IsGpa,
			/* extras */
			CreditPoints = convert(decimal(6,3), c.UnitsEarned * IsGpa * m.Points),
			FirstYearTermCode = min(t.YearTermCode) over(partition by s.Derkey1)
		FROM
			CCStudentProd s
			inner join
			CCCourseProd c
				on s.CollegeId = c.CollegeId
				and s.StudentId = c.StudentId
				and s.TermId = c.TermId
			inner join
			comis.Term t
				on t.TermCode = s.TermId
			inner join
			calpads.Mark m
				on m.MarkCode = c.Grade
			inner join
			comis.CourseLevel l
				on l.CourseLevelCode = c.CollegeLevel
		WHERE
			s.Derkey1 = @Derkey1
	);