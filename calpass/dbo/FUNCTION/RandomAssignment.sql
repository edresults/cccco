USE calpass;

GO

IF (object_id('dbo.RandomAssignment') is not null)
	BEGIN
		DROP FUNCTION dbo.RandomAssignment;
	END;

GO

CREATE FUNCTION
	dbo.RandomAssignment
	(
		@Groups tinyint = 2
	)
RETURNS
	TABLE
AS
	RETURN
	(
		SELECT
			Assignment = ABS(checksum(NewId)) % @Groups
		FROM
			dbo.vNewId
	);