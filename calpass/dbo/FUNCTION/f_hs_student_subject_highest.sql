USE calpass;

GO

IF (object_id('calpass.dbo.f_hs_student_subject_highest', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_hs_student_subject_highest;
	END;

GO

CREATE FUNCTION
	dbo.f_hs_student_subject_highest
	(
		@Derkey1 char(15),
		@Subject char(255),
		@GradeLevel char(2) = null
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			r.course_title,
			r.course_id,
			r.course_code,
			r.course_code_rank,
			r.course_subject,
			r.course_ag_code,
			r.course_ap_ind,
			r.course_ib_ind,
			r.course_cte_ind,
			r.course_exp_ind,
			r.course_avid_ind,
			r.section_id,
			r.section_grade,
			r.section_grade_points,
			r.section_credit_achieved,
			r.section_credit_attempted
		FROM
			(
				SELECT
					t.course_title,
					t.course_id,
					t.course_code,
					t.course_code_rank,
					t.course_subject,
					t.course_ag_code,
					t.course_ap_ind,
					t.course_ib_ind,
					t.course_cte_ind,
					t.course_exp_ind,
					t.course_avid_ind,
					t.section_id,
					t.section_grade,
					t.section_grade_points,
					t.section_credit_achieved,
					t.section_credit_attempted,
					row_selector = row_number() OVER (
							partition by
								t.derkey1,
								case
									when @gradelevel is null then null
									else t.organization_grade_level_code
								end
							order by
								t.year_leading desc, -- year cast as int
								t.season_rank desc, -- school term rank; fall = 1, winter = 2; spring = 3, summer = 4
								t.course_code_rank desc, -- cbeds course rank
								t.course_level_rank desc, -- course level rank
								t.course_ag_code_rank asc, -- ag status rank
								t.section_credit_attempted desc,
								t.course_grade_rank asc, -- grade rank; a = 1, b = 2, ...
								t.course_title desc -- arbitrary; used if all other things are equal and course title is sequenced 100 vs 101, a vs b
						)
				FROM
					calpass.dbo.f_hs_student_transcript(
						@Derkey1,
						@GradeLevel,
						default
					) t
				WHERE
					t.course_subject = @subject
					and (
						@GradeLevel is null
						or
						t.organization_grade_level_code = @GradeLevel
					)
					-- Elk Grove No MOU
					and t.organization_cds_code not in (
						SELECT
							organizationCode
						FROM
							calpass.dbo.organization
						WHERE
							parentId = 159610
					)
			) r
		WHERE
			r.row_selector = 1
	);