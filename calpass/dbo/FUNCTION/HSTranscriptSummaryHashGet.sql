USE calpass;

GO

IF (object_id('dbo.HSTranscriptSummaryHashGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.HSTranscriptSummaryHashGet;
	END;

GO

CREATE FUNCTION
	dbo.HSTranscriptSummaryHashGet
	(
		@Derkey1 binary(64)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			Entity = 
				case
					when grouping(Name) = 1 then 'Cgpa'
					when grouping(ContentCode) = 1 then Name
					when grouping(ContentCode) = 0 and ContentCode is not null then ContentCode
					when ContentCode is null then 'Cgpa'
				end,
			Attribute = 
				case
					when grouping(Name) = 1 then max(GradeCode)
					when grouping(ContentCode) = 1 then 'PromotedRank'
					when grouping(ContentCode) = 0 and ContentCode is not null then 'MarkPoints'
					when ContentCode is null then max(GradeCode)
				end,
			Value = 
				case
					when grouping(Name) = 1 then
						isnull(
							convert(
								decimal(4,3),
								sum(case when IsPrimary = 1 or IsPrimary is null then QualityPoints else 0 end)
								/
								nullif(
									sum(case when IsPrimary = 1 or IsPrimary is null then CreditTry else 0 end),
									0
								)
							),
							0
						)
					when grouping(ContentCode) = 1 then max(case when IsPromoted = 1 then IntraDepartmentRank else 0 end)
					when grouping(ContentCode) = 0 and ContentCode is not null then 
						isnull(
							convert(
								decimal(4,3),
								max(case when ContentSelector = 1 then QualityPoints end)
								/
								nullif(
									max(case when ContentSelector = 1 then CreditTry end),
									0
								)
							),
							0
						)
				end
		FROM
			(
				SELECT
					d.Name,
					ContentCode = 
						case
							when d.name = 'English Language Arts' then 'English' + GradeCode
							when cc.ContentCode in ('CalculusIAP', 'CalculusIIAP', 'CalculusI', 'CalculusII') then 'Calculus'
							else cc.ContentCode
						end,
					t.GradeCode,
					IsPrimary = 1,
					t.QualityPoints,
					t.CreditTry,
					t.IsPromoted,
					IntraDepartmentRank = cc.Rank,
					ContentSelector = row_number() over(
						partition by
							case
								when d.name = 'English Language Arts' then 'English' + GradeCode
								when cc.ContentCode in ('CalculusIAP', 'CalculusIIAP', 'CalculusI', 'CalculusII') then 'Calculus'
								else cc.ContentCode
							end
						order by
							y.YearTrailing desc,
							Term.IntraTermOrdinal desc
						)
				FROM
					dbo.HSTranscriptGet(@Derkey1) t
					inner join
					calpads.Course c
						on c.Code = t.CourseCode
					left outer join
					calpads.CourseContent cc
						on c.Code = cc.CourseCode
					left outer join
					calpads.Content ccc
						on ccc.Code = cc.ContentCode
					left outer join
					calpads.Department d
						on d.Code = ccc.DepartmentCode
					left outer join
					calpads.Term
						on term.TermCode = t.TermCode
					left outer join
					calpads.Year y
						on y.YearCodeAbbr = t.YearCodeAbbr
			) a
		GROUP BY
			rollup(Name,ContentCode)
		HAVING
			(
				grouping(Name) = 1
				or
				Name is not null
			)
	);