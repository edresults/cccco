USE calpass;

GO

IF (object_id('calpass.dbo.f_cc_student_attributes', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_cc_student_attributes;
	END;

GO

CREATE FUNCTION
	dbo.f_cc_student_attributes
	(
		@Derkey1 char(15),
		@TermId char(3) = null
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			r.derkey1,
			/* organization */
			r.organization_comis_code,
			r.organization_ipeds_code,
			r.organization_term_code,
			/* student */
			r.student_gender,
			r.student_ethnicity,
			r.student_citizenship,
			r.student_zip_code,
			r.student_education_status,
			r.student_high_school,
			r.student_goal,
			r.student_enroll_status,
			r.student_academic_standing,
			r.student_academic_level,
			r.student_dsps_code,
			r.student_eops_code,
			r.student_bogg_code,
			r.student_pell_code,
			r.student_financial_aid_code,
			r.student_major
		FROM
			(
				SELECT
					derkey1 = ccs.derkey1,
					/* organization */
					organization_comis_code = ci.code,
					organization_ipeds_code = ccs.CollegeId,
					organization_term_code = ccs.TermId,
					/* student */
					student_gender = rtrim(ccs.Gender),
					student_ethnicity = substring(ccs.Race, 1, 1),
					student_citizenship = ccs.Citizen,
					student_zip_code = ccs.ZipCode,
					student_education_status = ccs.EdStatus,
					student_high_school = ccs.HighSchool,
					student_goal = ccs.StudentGoal,
					student_enroll_status = ccs.EnrollStatus,
					student_academic_standing = ccs.AcademicStanding,
					student_academic_level = ccs.AcademicLevel,
					student_dsps_code = ccs.DSPS,
					student_eops_code = ccs.EOPS,
					student_bogg_code = ccs.BoggFlag,
					student_pell_code = ccs.PellFlag,
					student_financial_aid_code = ccs.FinancialAidFlag,
					student_major = ccs.Major,
					row_selector = row_number() over(
							partition by
								ccs.Derkey1
							order by
								t.year_leading asc,
								t.season_rank asc
						)
				FROM
					calpass.dbo.CCStudentProd ccs
						with(index(idx_ccstud_derkey))
					inner join
					calpass.dbo.CCStudentProd_CollegeId ci
						on ci.code_ipeds_legacy = ccs.collegeId
					inner join
					calpass.dbo.CCStudentProd_TermId t
						on t.code = ccs.termId
				WHERE
					ccs.derkey1 = @derkey1
					and (
						@TermId is null
						or
						ccs.TermId = @TermId
					)
					-- Los Rios MOU
					and ccs.collegeId not in (
						SELECT
							organizationCode
						FROM
							calpass.dbo.organization
						WHERE
							parentId = 170788
					)
			) r
		WHERE
			r.row_selector = 1
	);