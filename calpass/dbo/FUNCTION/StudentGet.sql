IF (object_id('dbo.StudentGet') is not null)
	BEGIN
		DROP FUNCTION dbo.StudentGet;
	END;

GO

CREATE FUNCTION
	dbo.StudentGet
	(
		@InterSegmentKey binary(64)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			IsHighSchoolStudent       = isnull(max(case when g.IsHS = 1 then 1 else 0 end), 0),
			IsHighSchoolSection       = isnull(max(case when g.IsHS = 1 and c.Derkey1 is not null then 1 else 0 end), 0),
			IsGrade09Student          = isnull(max(case when s.GradeLevel = '09' then 1 else 0 end), 0),
			IsGrade09Section          = isnull(max(case when s.GradeLevel = '09' and c.Derkey1 is not null then 1 else 0 end), 0),
			IsGrade10Student          = isnull(max(case when s.GradeLevel = '10' then 1 else 0 end), 0),
			IsGrade10Section          = isnull(max(case when s.GradeLevel = '10' and c.Derkey1 is not null then 1 else 0 end), 0),
			IsGrade10StudentInclusive = 
				case
					when max(case when s.GradeLevel = '09' then 1 else 0 end) + 
						max(case when s.GradeLevel = '10' then 1 else 0 end) = 2 then 1
						else 0
				end,
			IsGrade10SectionInclusive = 
				case
					when max(case when s.GradeLevel = '09' and c.Derkey1 is not null then 1 else 0 end) + 
						max(case when s.GradeLevel = '10' and c.Derkey1 is not null then 1 else 0 end) = 2 then 1
					else 0
				end,
			IsGrade11Student          = isnull(max(case when s.GradeLevel = '11' then 1 else 0 end), 0),
			IsGrade11Section          = isnull(max(case when s.GradeLevel = '11' and c.Derkey1 is not null then 1 else 0 end), 0),
			IsGrade11StudentInclusive = 
				case
					when max(case when s.GradeLevel = '09' then 1 else 0 end) + 
						max(case when s.GradeLevel = '10' then 1 else 0 end) + 
						max(case when s.GradeLevel = '11' then 1 else 0 end) = 3 then 1
					else 0
				end,
			IsGrade11SectionInclusive = 
				case
					when max(case when s.GradeLevel = '09' and c.Derkey1 is not null then 1 else 0 end) + 
						max(case when s.GradeLevel = '10' and c.Derkey1 is not null then 1 else 0 end) + 
						max(case when s.GradeLevel = '11' and c.Derkey1 is not null then 1 else 0 end) = 3 then 1
					else 0
				end,
			IsGrade12Student          = isnull(max(case when s.GradeLevel = '12' then 1 else 0 end), 0),
			IsGrade12Section          = isnull(max(case when s.GradeLevel = '12' and c.Derkey1 is not null then 1 else 0 end), 0),
			IsGrade12StudentInclusive = 
				case
					when max(case when s.GradeLevel = '09' then 1 else 0 end) + 
						max(case when s.GradeLevel = '10' then 1 else 0 end) + 
						max(case when s.GradeLevel = '11' then 1 else 0 end) + 
						max(case when s.GradeLevel = '12' then 1 else 0 end) = 4 then 1
					else 0
				end,
			IsGrade12SectionInclusive = 
				case
					when max(case when s.GradeLevel = '09' and c.Derkey1 is not null then 1 else 0 end) + 
						max(case when s.GradeLevel = '10' and c.Derkey1 is not null then 1 else 0 end) + 
						max(case when s.GradeLevel = '11' and c.Derkey1 is not null then 1 else 0 end) + 
						max(case when s.GradeLevel = '12' and c.Derkey1 is not null then 1 else 0 end) = 4 then 1
					else 0
				end,
			IsHighSchoolCollision =
				case
					when count(distinct case when g.IsHS = 1 and g1.IsHS = 1 then s1.CSISNum end) <= 1 then 0
					when count(distinct case when g.IsHS = 1 and g1.IsHS = 1 then s1.CSISNum end)  > 1 then 1
				end
		FROM
			dbo.K12StudentProd s
			inner join
			calpads.Grade g
				on s.GradeLevel = g.GradeCode
			inner join
			dbo.K12StudentProd s1
				on s1.Derkey1 = s.Derkey1
			inner join
			calpads.Grade g1
				on s1.GradeLevel = g1.GradeCode
			left outer join
			dbo.K12CourseProd c
				on c.School = s.School
				and c.LocStudentId = s.LocStudentId
				and c.AcYear = s.AcYear
		WHERE
			s.Derkey1 = @InterSegmentKey
	);