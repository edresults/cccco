USE calpass;

GO

IF (object_id('calpass.dbo.f_cc_student_milestones', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_cc_student_milestones;
	END;

GO

CREATE FUNCTION
	dbo.f_cc_student_milestones
	(
		@Derkey1 char(15)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			student_first_year_units_attempted = sum(r.section_units_attempted),
			student_first_year_units_achieved = sum(case when r.gpa_ind = 0 then 0 else r.section_units_achieved end),
			student_first_year_grade_points = sum(case when r.gpa_ind = 0 then 0 else r.section_units_achieved end *
				case when r.gpa_ind = 0 then 0 else r.section_units_attempted end),
			student_first_year_cgpa = 1.000 * isnull(sum(case when r.gpa_ind = 0 then 0 else r.section_units_achieved end * 
				r.section_grade_points) / nullif(sum(case when r.gpa_ind = 0 then 0 else r.section_units_attempted end), 0), 0)
		FROM
			(
				SELECT
					t.derkey1,
					/* college */
					t.organization_comis_code,
					t.organization_ipeds_code,
					t.organization_term_code,
					/* course */
					t.course_id,
					t.course_title,
					t.course_credit_status,
					t.course_top_code,
					t.course_transfer_status,
					t.course_units_max,
					t.course_bs_status,
					t.course_sam_code,
					t.course_classification_code,
					t.course_college_level,
					/* section */
					t.section_id,
					t.section_grade,
					t.section_grade_points,
					t.section_units_attempted,
					t.section_units_achieved,
					t.gpa_ind,
					min_year_term_code = min(t.year_term_code) over(partition by derkey1)
				FROM
					calpass.dbo.f_cc_student_transcript(
						@Derkey1,
						default
					) t
			) r
		WHERE
			r.organization_term_code >= substring(r.min_year_term_code, 3, 3)
			and r.organization_term_code <= substring(cast(cast(substring(r.min_year_term_code, 1, 4) as int) + 1 as char(4)) + 
				substring(r.min_year_term_code, 5, 1), 3, 3)
		GROUP BY
			r.derkey1
	);