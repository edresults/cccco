USE calpass;

GO

IF (object_id('dbo.HSEnglGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.HSEnglGet;
	END;

GO

CREATE FUNCTION
	dbo.HSEnglGet
	(
		@Derkey1 binary(64)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			ContentAreaLastYearTermCode = max(ContentAreaLastYearTermCode),
			LastYearTermCode = LastYearTermCode,
			RemedialTotalContentPoints = max(case when d.ContentCode = 'Remedial' then d.TotalContentPoints end),
			English09TotalContentPoints = 
				max(
					case
						when d.ContentCode = 'English09' then d.TotalContentPoints
						when d.ContentCode = 'English' and d.GradeCode = '09' then d.TotalContentPoints
					end
				),
			English10TotalContentPoints = 
				max(
					case
						when d.ContentCode = 'English10' then d.TotalContentPoints
						when d.ContentCode = 'English' and d.GradeCode = '10' then d.TotalContentPoints
					end
				),
			English11TotalContentPoints = 
				max(
					case
						when d.ContentCode = 'English11' then d.TotalContentPoints
						when d.ContentCode = 'English' and d.GradeCode = '11' then d.TotalContentPoints
					end
				),
			English12TotalContentPoints = 
				max(
					case
						when d.ContentCode = 'English12' then d.TotalContentPoints
						when d.ContentCode = 'English' and d.GradeCode = '12' then d.TotalContentPoints
					end
				),
			ExpositoryTotalContentPoints = max(case when d.ContentCode = 'Expository' then d.TotalContentPoints end),
			LanguageAPTotalContentPoints = max(case when d.ContentCode = 'LanguageAP' then d.TotalContentPoints end),
			LiteratureAPTotalContentPoints = max(case when d.ContentCode = 'LiteratureAP' then d.TotalContentPoints end),
			RemedialMarkCategory = max(case when d.ContentCode = 'Remedial' then mb.Category end),
			English09TotalCategory = 
				max(
					case
						when d.ContentCode = 'English09' then mb.Category
						when d.ContentCode = 'English' and d.GradeCode = '09' then mb.Category
					end
				),
			English10TotalCategory = 
				max(
					case
						when d.ContentCode = 'English10' then mb.Category
						when d.ContentCode = 'English' and d.GradeCode = '10' then mb.Category
					end
				),
			English11TotalCategory = 
				max(
					case
						when d.ContentCode = 'English11' then mb.Category
						when d.ContentCode = 'English' and d.GradeCode = '11' then mb.Category
					end
				),
			English12TotalCategory = 
				max(
					case
						when d.ContentCode = 'English12' then mb.Category
						when d.ContentCode = 'English' and d.GradeCode = '12' then mb.Category
					end
				),
			ExpositoryMarkCategory = max(case when d.ContentCode = 'Expository' then mb.Category end),
			LanguageAPMarkCategory = max(case when d.ContentCode = 'LanguageAP' then mb.Category end),
			LiteratureAPMarkCategory = max(case when d.ContentCode = 'LiteratureAP' then mb.Category end),
			RemedialTotalCreditTry = max(case when d.ContentCode = 'Remedial' then d.TotalCreditTry end),
			English09TotalTotalCreditTry = 
				max(
					case
						when d.ContentCode = 'English09' then d.TotalCreditTry
						when d.ContentCode = 'English' and d.GradeCode = '09' then d.TotalCreditTry
					end
				),
			English10TotalTotalCreditTry = 
				max(
					case
						when d.ContentCode = 'English10' then d.TotalCreditTry
						when d.ContentCode = 'English' and d.GradeCode = '10' then d.TotalCreditTry
					end
				),
			English11TotalTotalCreditTry = 
				max(
					case
						when d.ContentCode = 'English11' then d.TotalCreditTry
						when d.ContentCode = 'English' and d.GradeCode = '11' then d.TotalCreditTry
					end
				),
			English12TotalTotalCreditTry = 
				max(
					case
						when d.ContentCode = 'English12' then d.TotalCreditTry
						when d.ContentCode = 'English' and d.GradeCode = '12' then d.TotalCreditTry
					end
				),
			ExpositoryTotalCreditTry = max(case when d.ContentCode = 'Expository' then d.TotalCreditTry end),
			LanguageAPTotalCreditTry = max(case when d.ContentCode = 'LanguageAP' then d.TotalCreditTry end),
			LiteratureAPTotalCreditTry = max(case when d.ContentCode = 'LiteratureAP' then d.TotalCreditTry end)
		FROM
			(
				SELECT
					t.GradeCode,
					t.LastYearTermCode,
					ContentAreaLastYearTermCode = max(t.YearTermCode),
					crct.ContentCode,
					TotalQualityPoints = sum(t.QualityPoints),
					TotalCreditTry = sum(t.CreditTry),
					TotalContentPoints = isnull(convert(decimal(4,3), sum(t.QualityPoints) / nullif(sum(t.CreditTry), 0)), 0)
				FROM
					dbo.HSTranscriptGet(@Derkey1) t
					inner join
					calpads.Course crse
						on crse.Code = t.CourseCode
					inner join
					calpads.CourseContent crct
						on crct.CourseCode = crse.Code
					inner join
					calpads.Content cont
						on cont.Code = crct.ContentCode
				WHERE
					crse.DepartmentCode = 14
				GROUP BY
					t.GradeCode,
					t.LastYearTermCode,
					crct.ContentCode
			) d
			inner join
			calpads.MarkBound mb
				on d.TotalContentPoints >= mb.BoundLower
				and d.TotalContentPoints <= mb.BoundUpper
		GROUP BY
			LastYearTermCode
	);