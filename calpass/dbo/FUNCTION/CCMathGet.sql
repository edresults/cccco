USE calpass;

GO

IF (object_id('dbo.CCMathGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.CCMathGet;
	END;

GO

CREATE FUNCTION
	dbo.CCMathGet
	(
		@Derkey1 char(15)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			t.FirstYearTermCode,
			ContentAreaFirstYearTermCode = t.YearTermCode,
			Arithmetic = convert(decimal(4,3), max(case when CourseLevelCode = 'D' then t.MarkPoints end)),
			PreAlgebra = convert(decimal(4,3), max(case when CourseLevelCode = 'C' then t.MarkPoints end)),
			AlgebraI = convert(decimal(4,3), max(case when CourseLevelCode = 'B' then t.MarkPoints end)),
			Geometry = convert(decimal(4,3), max(case when CourseTitle like '%Geo%' then t.MarkPoints end)),
			AlgebraII = convert(decimal(4,3), max(case when CourseLevelCode = 'A' then t.MarkPoints end)),
			CollegeAlgebra = 
				convert(
					decimal(4,3),
					max(
						case
							when CourseTitle like '%COLL%' then t.MarkPoints
							when CourseTitle like '%ADVANCED ALG%' then t.MarkPoints
							when CourseTitle like '%ALGEBRA FOR CALC%' then t.MarkPoints
						end
					)
				),
			GEMath = 
				convert(
					decimal(4,3),
					max(
						case
							when CourseTitle like '%MATH FOR%' then t.MarkPoints
							when CourseTitle like '%MATH%LIB%' then t.MarkPoints
							when CourseTitle like '%MATH%ELEM%' then t.MarkPoints
							when CourseTitle like '%MATH TOPICS%' then t.MarkPoints
							when CourseTitle like '%MATHEMATICAL%' then t.MarkPoints
							when CourseTitle like '%MATHEMATICS%' then t.MarkPoints
							when CourseTitle like '%THE IDEAS%' then t.MarkPoints
							when CourseTitle like '%LIBERAL%' then t.MarkPoints
							when CourseTitle like '%CONCEPTS%' then t.MarkPoints
							when CourseTitle like '%IDEAS%' then t.MarkPoints
							when CourseTitle like '%TEACH%' then t.MarkPoints
							when CourseTitle like '%LIFE%' then t.MarkPoints
							when CourseTitle like '%BUS%' then t.MarkPoints
							when CourseTitle like '%BRIEF%' then t.MarkPoints
							when CourseTitle like '%SOC%' then t.MarkPoints
							when CourseTitle like '%BIO%' then t.MarkPoints
							when CourseTitle like '%SHORT%' then t.MarkPoints
						end
					)
				),
			[Statistics] = 
				convert(
					decimal(4,3),
					max(
						case
							when CourseTitle like '%STA%' then t.MarkPoints
							when CourseTitle like '%INTRO STA%' then t.MarkPoints
							when CourseTitle like '%ELEM PROB%' then t.MarkPoints
							when CourseTitle like '%ELEM. PROB%' then t.MarkPoints
							when CourseTitle like '%ELEM STA%' then t.MarkPoints
							when CourseTitle like '%ELEMENTARY STA%' then t.MarkPoints
							when CourseTitle like '%ELEMNTRY STA%' then t.MarkPoints
							when CourseTitle like '%INTRO PROB%' then t.MarkPoints
							when CourseTitle like '%INTRO TO PROB%' then t.MarkPoints
							when CourseTitle like '%INTRO APPLIED STATS%' then t.MarkPoints
							when CourseTitle like '%HONORS STA%' then t.MarkPoints
						end
					)
				),
			PreCalculus = 
				convert(
					decimal(4,3),
					max(
						case
							when CourseTitle like '%PRE%CALC%' then t.MarkPoints
						end
					)
				),
			CalculusI = 
				convert(
					decimal(4,3),
					max(
						case
							when CourseTitle like '%CALC%' then
								case
									when CourseTitle not like '%PRE%' then t.MarkPoints
									when CourseTitle not like '%II%' then t.MarkPoints
									when CourseTitle not like '%2%' then t.MarkPoints
									when CourseTitle not like '%3%' then t.MarkPoints
									when CourseTitle not like '%4A%' then t.MarkPoints
									when CourseTitle not like '%DIFF%' then t.MarkPoints
								end
						end
					)
				),
			CalculusII = 
				convert(
					decimal(4,3),
					max(
						case
							when CourseTitle like '%CALC%' and CourseTitle not like '%PRE%' then
								case
									when CourseTitle like '%II%' then t.MarkPoints
									when CourseTitle like '%2%' then t.MarkPoints
									when CourseTitle like '%3%' then t.MarkPoints
								end
						end
					)
				)
		FROM
			dbo.CCTranscriptGet(@Derkey1) t
		WHERE
			t.TopCode = '170100'
			and t.YearTermCode = (
				SELECT
					min(t1.YearTermCode)
				FROM
					dbo.CCTranscriptGet(@Derkey1) t1
				WHERE
					t1.TopCode = t.TopCode
					and t1.CourseLevelCode = t.CourseLevelCode
			)
		GROUP BY
			t.FirstYearTermCode,
			t.YearTermCode
	);