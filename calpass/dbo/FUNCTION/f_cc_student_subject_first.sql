USE calpass;

GO

IF (object_id('calpass.dbo.f_cc_student_subject_first', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_cc_student_subject_first;
	END;

GO

CREATE FUNCTION
	dbo.f_cc_student_subject_first
	(
		@Derkey1 char(15),
		@TopCode char(6)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			r.derkey1,
			/* college */
			r.organization_comis_code,
			r.organization_ipeds_code,
			r.organization_term_code,
			/* course */
			r.course_id,
			r.course_title,
			r.course_credit_status,
			r.course_top_code,
			r.course_transfer_status,
			r.course_units_max,
			r.course_bs_status,
			r.course_sam_code,
			r.course_classification_code,
			r.course_college_level,
			/* section */
			r.section_id,
			r.section_grade,
			r.section_grade_points,
			r.section_units_attempted,
			r.section_units_achieved
		FROM
			(
				SELECT
					t.derkey1,
					/* college */
					t.organization_comis_code,
					t.organization_ipeds_code,
					t.organization_term_code,
					/* course */
					t.course_id,
					t.course_title,
					t.course_credit_status,
					t.course_top_code,
					t.course_transfer_status,
					t.course_units_max,
					t.course_bs_status,
					t.course_sam_code,
					t.course_classification_code,
					t.course_college_level,
					/* section */
					t.section_id,
					t.section_grade,
					t.section_grade_points,
					t.section_units_attempted,
					t.section_units_achieved,
					row_selector = row_number() OVER(
						partition by
							t.derkey1
						order by
							t.year_leading asc,
							t.season_rank asc,
							t.course_college_level_rank asc,
							t.section_units_attempted desc,
							t.section_grade_rank asc,
							t.course_id asc
					)
				FROM
					calpass.dbo.f_cc_student_transcript(
						@Derkey1,
						default
					) t
				WHERE
					t.course_top_code = @TopCode
			) r
		WHERE
			r.row_selector = 1
	);