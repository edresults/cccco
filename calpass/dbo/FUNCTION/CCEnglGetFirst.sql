USE calpass;

GO

IF (object_id('dbo.CCEnglGetFirst', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.CCEnglGetFirst;
	END;

GO

CREATE FUNCTION
	dbo.CCEnglGetFirst
	(
		@Derkey1 binary(64)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			t.FirstYearTermCode,
			ContentAreaFirstYearTermCode = t.YearTermCode,
			Level0MarkPoints = max(case when t.CourseLevelCode = 'Y' then t.MarkPoints end),
			Level1MarkPoints = max(case when t.CourseLevelCode = 'A' then t.MarkPoints end),
			Level2MarkPoints = max(case when t.CourseLevelCode = 'B' then t.MarkPoints end),
			Level3MarkPoints = max(case when t.CourseLevelCode = 'C' then t.MarkPoints end),
			Level4MarkPoints = max(case when t.CourseLevelCode = 'D' then t.MarkPoints end),
			Level5MarkPoints = max(case when t.CourseLevelCode = 'E' then t.MarkPoints end),
			Level0MarkCategory = max(case when t.CourseLevelCode = 'Y' then t.MarkCategory end),
			Level1MarkCategory = max(case when t.CourseLevelCode = 'A' then t.MarkCategory end),
			Level2MarkCategory = max(case when t.CourseLevelCode = 'B' then t.MarkCategory end),
			Level3MarkCategory = max(case when t.CourseLevelCode = 'C' then t.MarkCategory end),
			Level4MarkCategory = max(case when t.CourseLevelCode = 'D' then t.MarkCategory end),
			Level5MarkCategory = max(case when t.CourseLevelCode = 'E' then t.MarkCategory end)
		FROM
			dbo.CCTranscriptGet(@Derkey1) t
		WHERE
			t.TopCode = '150100'
			and t.CourseLevelBelow = (
				SELECT
					min(t1.CourseLevelBelow)
				FROM
					dbo.CCTranscriptGet(@Derkey1) t1
				WHERE
					t1.YearTermCode = (
						SELECT
							min(t2.YearTermCode)
						FROM
							dbo.CCTranscriptGet(@Derkey1) t2
						WHERE
							t2.TopCode = t1.TopCode
					)
					and t1.YearTermCode = t.YearTermCode
					and t1.TopCode = t.TopCode
			)
		GROUP BY
			t.FirstYearTermCode,
			t.YearTermCode
	);