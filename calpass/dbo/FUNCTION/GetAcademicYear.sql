CREATE FUNCTION
    GetAcademicYear
    (
        @Date DATE
    )
RETURNS
    TABLE
AS
    RETURN
    (
        SELECT AcademicYear = 
            CONVERT(
                CHAR(9),
                case
                    when DATEPART(mm, @Date) >= 7 then CONVERT(CHAR(4), DATEPART(yyyy, @Date)) + '-' + CONVERT(CHAR(4), DATEPART(yyyy, @Date) + 1)
                    when DATEPART(mm, @Date) <= 6 then CONVERT(CHAR(4), DATEPART(yyyy, @Date) - 1) + '-' + CONVERT(CHAR(4), DATEPART(yyyy, @Date))
                end
            )
    );