USE calpass;

GO

IF (object_id('dbo.K12PrgProdGet') is not null)
	BEGIN
		DROP FUNCTION dbo.K12PrgProdGet;
	END;

GO

CREATE FUNCTION
	dbo.K12PrgProdGet
	(
		@StudentStateId char(10),
		@SchoolCode char(7),
		@YearCode char(9)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			Derkey1 = calpass.dbo.Derkey1(
				upper(replace(replace(replace(f.NameFirst, char(39), ''), char(46), ''), char(32), '')),
				upper(replace(replace(replace(f.NameLast, char(39), ''), char(46), ''), char(32), '')),
				f.Gender,
				f.Birthdate
			),
			School = p.DistrictCode + p.SchoolCode,
			AcYear = y.YearCodeAbbr,
			LocStudentId = substring(calpass.dbo.get1289Encryption(f.StudentLocalId, 'X'), 1, 15),
			p.ProgramCode,
			p.MembershipCode,
			p.MembershipStartDate,
			p.MembershipEndDate,
			p.ServiceYearCode,
			p.ServiceCode,
			p.AcademyId,
			p.MigrantId,
			p.DisabilityCode,
			p.AccountabilityDistrictCode,
			p.DwellingCode,
			p.IsHomeless,
			p.IsRunaway
		FROM
			calpads.Sprg p with (index(pk_Sprg))
			inner join
			calpads.Sinf f with (index(pk_Sinf))
				on f.StudentStateId = p.StudentStateId
				and f.SchoolCode = p.SchoolCode
			inner join
			calpads.Year y
				on y.YearCode = p.YearCode
		WHERE
			p.StudentStateId = @StudentStateId
			and (
				@SchoolCode is null
				or
				p.SchoolCode = @SchoolCode
			)
			and (
				@YearCode is null
				or
				p.YearCode = @YearCode
			)
			and f.EffectiveStartDate = (
				SELECT
					max(f1.EffectiveStartDate)
				FROM
					calpads.Sinf f1 with(index(pk_Sinf))
				WHERE
					f1.StudentStateId = f.StudentStateId
					and f1.SchoolCode = f.SchoolCode
					and f1.YearCode = f.YearCode
					and f1.YearCode = (
						SELECT
							max(f2.YearCode)
						FROM
							calpads.Sinf f2 with(index(pk_Sinf))
						WHERE
							f2.StudentStateId = f1.StudentStateId
							and f2.SchoolCode = f1.SchoolCode
							and f2.YearCode <= p.YearCode
					)
			)
	);