USE calpass;

GO

IF (object_id('calpass.dbo.f_cc_student_transcript', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_cc_student_transcript;
	END;

GO

CREATE FUNCTION
	dbo.f_cc_student_transcript
	(
		@Derkey1 char(15),
		@TermId char(3) = null
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			derkey1 = ccs.derkey1,
			/* organization */
			organization_comis_code = ci.code,
			organization_ipeds_code = ccs.CollegeId,
			organization_term_code = rtrim(ccs.TermId),
			/* course */
			course_id = rtrim(ccc.CourseId),
			course_title = rtrim(ccc.Title),
			course_credit_status = ccc.CreditFlag,
			course_top_code = ccc.TopCode,
			course_transfer_status = ccc.TransferStatus,
			course_units_max = ccc.UnitsMax,
			course_bs_status = ccc.BSStatus,
			course_sam_code = ccc.SamCode,
			course_classification_code = ccc.ClassCode,
			course_college_level = ccc.CollegeLevel,
			/* section */
			section_id = rtrim(ccc.SectionId),
			section_units_attempted = case
					when g.gpa_ind = 0 then 0.00
					when ccc.UnitsAttempted >= 88.88 then 0.00
					when ccc.UnitsAttempted is null then 0.00
					else ccc.UnitsAttempted
				end,
			section_units_achieved = case
					when g.gpa_ind = 0 then 0.00
					when ccc.UnitsEarned >= 88.88 then 0.00
					when ccc.UnitsEarned is null then 0.00
					else ccc.UnitsEarned
				end,
			section_grade = rtrim(ccc.Grade),
			section_grade_points = g.Points,
			/* extras */
			year_leading = t.year_leading,
			season_rank = t.season_rank,
			course_college_level_rank = cl.rank,
			section_grade_rank = g.rank,
			year_term_code = t.year_term,
			gpa_ind = g.gpa_ind
		FROM
			calpass.dbo.CCStudentProd ccs
				with(index(ix_CCStudentProd__Derkey1))
			inner join
			calpass.dbo.CCCourseProd ccc
				with(index(ixc_CCCourseProd__CollegeId__StudentId__TermId__CourseId__SectionId))
				on ccc.CollegeId = ccs.CollegeId
				and ccc.TermId = ccs.TermId
				and ccc.StudentId = ccs.StudentId
			inner join
			calpass.dbo.CCStudentProd_CollegeId ci
				on ci.code_ipeds_legacy = ccs.collegeId
			inner join
			calpass.dbo.CCStudentProd_TermId t
				on t.code = ccs.termId
			inner join
			calpass.dbo.CCCourseProd_Grade g
				on g.code = ccc.grade
			inner join
			calpass.dbo.CCCourseProd_CollegeLevel cl
				on cl.code = ccc.collegeLevel
		WHERE
			ccs.derkey1 = @derkey1
			and (
				@TermId is null
				or
				ccs.TermId = @TermId
			)
			-- Los Rios MOU
			and ccs.collegeId not in (
				SELECT
					organizationCode
				FROM
					calpass.dbo.organization
				WHERE
					parentId = 170788
			)
			and g.completion_denominator_ind = 1
			and ccc.creditFlag in ('T', 'D', 'C', 'S')
	);