USE calpass;

GO

IF (object_id('calpass.dbo.f_hs_student_attributes', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_hs_student_attributes;
	END;

GO

CREATE FUNCTION
	dbo.f_hs_student_attributes
	(
		@Derkey1 char(15),
		@AcYear char(4) = null
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			derkey1,
			student_gender,
			student_ethnicity,
			student_hispanic_ind,
			student_birthdate,
			student_language,
			student_ses_ind = isnull(s.socioeconomic_ind, 0),
			student_graduation_code = graduation_code,
			student_graduation_year_code = graduation_year_code,
			student_graduation_year_description = graduation_year_description
		FROM
			(
				SELECT DISTINCT
					derkey1 = hss.derkey1,
					student_gender = rtrim(hss.Gender),
					student_ethnicity = coalesce(
						case when hss.Ethnicity = '   ' then null else hss.Ethnicity end,
						case when hss.EthnicityCode1 = '   ' then null else hss.EthnicityCode1 end,
						case when hss.HispanicEthnicity = 'Y' then '500' else null end
					),
					student_hispanic_ind = case
							when hss.HispanicEthnicity = 'Y' then 1
							else 0
						end,
					student_birthdate = rtrim(hss.Birthdate),
					student_language = rtrim(hss.HomeLanguage),
					row_selector = row_number() over(
							partition by
								hss.Derkey1
							order by
								ay.year_leading
						)
				FROM
					calpass.dbo.K12StudentProd hss
						with(index(ix_K12StudentProd__Derkey1__GradeLevel__AcYear))
					inner join
					calpass.dbo.K12StudentProd_AcYear ay
						on ay.code = hss.AcYear
				WHERE
					hss.Derkey1 = @derkey1
					and hss.GradeLevel in ('09', '10', '11', '12')
					and (
						@AcYear is null
						or
						hss.AcYear = @AcYear
					)
					-- Elk Grove No MOU
					and hss.School not in (
						SELECT
							organizationCode
						FROM
							calpass.dbo.organization
						WHERE
							parentId = 159610
					)
			) a
			outer apply
			calpass.dbo.f_hs_socioeconomic_ind(
				a.derkey1
			) s
			outer apply
			calpass.dbo.f_hs_student_graduation(
				a.derkey1
			) g
		WHERE
			a.row_selector = 1
	);