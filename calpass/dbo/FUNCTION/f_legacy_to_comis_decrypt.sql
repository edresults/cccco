USE calpass;

GO

IF (object_id('calpass.dbo.f_legacy_to_comis_decrypt', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_legacy_to_comis_decrypt;
	END;

GO

CREATE FUNCTION
	dbo.f_legacy_to_comis_decrypt
	(
		@derkey1 char(15)
	)
RETURNS
	TABLE
AS
RETURN (
	SELECT
		ld.derkey1,
		id.college_id,
		id.student_id
	FROM
		comis.dbo.studntid id
			with(index(uk_studntid__college_id__ssn__student_id_status))
		inner join
		calpass.dbo.f_legacy_decrypt(
			@derkey1
		) ld
			on ld.college_id = id.college_id
			and ld.student_key = id.ssn
			and ld.id_status = id.student_id_status
);