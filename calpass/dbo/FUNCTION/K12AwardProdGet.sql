USE calpass;

GO

IF (object_id('dbo.K12AwardProdGet') is not null)
	BEGIN
		DROP FUNCTION dbo.K12AwardProdGet;
	END;

GO

CREATE FUNCTION
	dbo.K12AwardProdGet
	(
		@StudentStateId char(10),
		@SchoolCode char(7),
		@YearCode char(9)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			Derkey1 = 
				calpass.dbo.Derkey1(
					upper(replace(replace(replace(f.NameFirst, char(39), ''), char(46), ''), char(32), '')),
					upper(replace(replace(replace(f.NameLast, char(39), ''), char(46), ''), char(32), '')),
					f.Gender,
					f.Birthdate
				),
			School = r.DistrictCode + r.SchoolCode,
			AcYear = y.YearCodeAbbr,
			LocStudentId = substring(calpass.dbo.get1289Encryption(f.StudentLocalId, 'X'), 1, 15),
			AwardType = r.CompletionStatusCode,
			AwardDate = r.EnrollExitDate
		FROM
			(
				SELECT
					r.StudentStateId,
					r.SchoolCode,
					r.YearCode,
					r.GradeCode,
					r.DistrictCode,
					r.CompletionStatusCode,
					r.EnrollExitDate
				FROM
					(
						SELECT
							r.StudentStateId,
							r.SchoolCode,
							r.YearCode,
							r.GradeCode,
							r.DistrictCode,
							r.CompletionStatusCode,
							r.EnrollExitDate,
							NonDeterministicSelector = 
								row_number()
								over(
									partition by
										r.StudentStateId,
										r.SchoolCode,
										r.YearCode,
										r.GradeCode
									order by
										r.EnrollExitDate desc,
										(select 1) desc
								)
						FROM
							calpads.Senr r with (index(pk_Senr))
						WHERE
							r.StudentStateId = @StudentStateId
							and (
								@SchoolCode is null
								or
								r.SchoolCode = @SchoolCode
							)
							and (
								@YearCode is null
								or
								r.YearCode = @YearCode
							)
							and r.EnrollStartDate = (
								SELECT
									max(r1.EnrollStartDate)
								FROM
									calpads.Senr r1 with(index(pk_Senr))
								WHERE
									r1.StudentStateId = r.StudentStateId
									and r1.SchoolCode = r.SchoolCode
									and r1.YearCode = r.YearCode
							)
							and r.CompletionStatusCode not in ('480', '360', '   ')
							and r.EnrollExitDate is not null
					) r
				WHERE
					r.NonDeterministicSelector = 1
			) r
			-- required sinf record
			inner join
			calpads.Sinf f with (index(pk_Sinf))
				on f.StudentStateId = r.StudentStateId
				and f.SchoolCode = r.SchoolCode
			inner join
			calpads.Year y
				on y.YearCode = r.YearCode
		WHERE
			f.EffectiveStartDate = (
				SELECT
					max(f1.EffectiveStartDate)
				FROM
					calpads.Sinf f1 with(index(pk_Sinf))
				WHERE
					f1.StudentStateId = f.StudentStateId
					and f1.SchoolCode = f.SchoolCode
					and f1.YearCode = f.YearCode
					and f1.YearCode = (
						SELECT
							max(f2.YearCode)
						FROM
							calpads.Sinf f2 with(index(pk_Sinf))
						WHERE
							f2.StudentStateId = f1.StudentStateId
							and f2.SchoolCode = f1.SchoolCode
							and f2.YearCode <= r.YearCode
					)
			)
	);