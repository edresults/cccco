USE calpass;

GO

IF (object_id('calpass.dbo.f_hs_student_transcript', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_hs_student_transcript;
	END;

GO

CREATE FUNCTION
	dbo.f_hs_student_transcript
	(
		@Derkey1 char(15),
		@GradeLevel char(2) = null,
		@AcYear char(4) = null
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT DISTINCT
			derkey1 = hss.Derkey1,
			/* school */
			organization_cds_code = hss.School,
			organization_year_code = hss.AcYear,
			organization_term_code = rtrim(hsc.courseTerm),
			organization_grade_level_code = rtrim(hss.GradeLevel),
			/* course */
			course_title = rtrim(hsc.CourseTitle),
			course_id = rtrim(hsc.LocallyAssignedCourseNumber),
			course_code = rtrim(hsc.CourseId),
			course_subject = cbeds.content_area_category_assignment_name,
			course_ag_code = rtrim(
					case
						when hsc.AGstatus = '  ' then null
						else hsc.AGstatus
					end
				),
			course_ap_ind = max(cbeds.ap_ind),
			course_ib_ind = max(cbeds.ib_ind),
			course_cte_ind = max(
					case
						when cbeds.cte_pathway_code <> '000' then 1
						else 0
					end
				),
			course_exp_ind = max(
					case
						when cbeds.code = '2722' then 1
						else 0
					end
				),
			course_avid_ind = max(
					case
						when cbeds.code = '6023' then 1
						else 0
					end
				),
			/* section */
			section_id = rtrim(hsc.CourseSectionClassNumber),
			section_grade = rtrim(hsc.Grade),
			section_grade_points = hscg.Points,
			section_credit_achieved = case
					when hsc.CreditEarned >= 99 then 0.00
					else hsc.CreditEarned
				end,
			section_credit_attempted = case
				when hsc.CreditAttempted >= 99 then 0.00
				else hsc.CreditAttempted
			end,
			/* extras */
			year_leading = ay.year_leading, -- year cast as int
			season_rank = ct.season_rank, -- school term rank; fall = 1, winter = 2; spring = 3, summer = 4
			course_code_rank = cbeds_rank.rank, -- cbeds course rank
			course_level_rank = cl.rank, -- course level rank
			course_ag_code_rank = ag.rank, -- ag status rank
			course_grade_rank = hscg.rank -- grade rank; a = 1, b = 2, ...
		FROM
			calpass.dbo.K12StudentProd hss
				with(index(ix_K12StudentProd__Derkey1__GradeLevel__AcYear))
			inner join
			calpass.dbo.K12CourseProd hsc
				with(index(ixc_K12CourseProd__School__LocStudentId__AcYear))
				on hss.school = hsc.school
				and hss.LocStudentId = hsc.LocStudentId
				and hss.AcYear = hsc.AcYear
			inner join
			calpass.dbo.K12StudentProd_AcYear ay
				on ay.code = hss.AcYear
			inner join
			calpass.dbo.K12CourseProd_grade hscg
				on hscg.code = hsc.grade
			left outer join
			calpass.dbo.K12CourseProd_CourseTerm ct
				on ct.code = hsc.CourseTerm
			left outer join
			calpass.dbo.K12CourseProd_AGstatus cl
				on cl.code = hsc.CourseLevel
			left outer join
			calpass.dbo.K12CourseProd_AGstatus ag
				on ag.code = hsc.AGstatus
			left outer join
			calpass.dbo.K12CourseProd_cbeds cbeds
				on cbeds.code = case
						when hsc.courseid = '6098' then
							case
								when lower(hsc.coursetitle) like '%alg%' then
									case
										when lower(hsc.coursetitle) like '%pre%' then '2424'
										when lower(hsc.coursetitle) like '%ii%' then '2404'
										when lower(hsc.coursetitle) like '%i%' then '2403'
									end
								when lower(hsc.coursetitle) like '%trig%' then '2407'
								when lower(hsc.coursetitle) like '%arith%' then '2400'
								when lower(hsc.coursetitle) like '%calc%' then
									case
										when lower(hsc.coursetitle) like '%pre%' then '2414'
										else '2415'
									end
								when lower(hsc.coursetitle) like '%stat%' then '2445'
								when (
									lower(hsc.coursetitle) like '%geo%'
									and lower(hsc.coursetitle) not like '%geol%'
									and lower(hsc.coursetitle) not like '%astron%'
									and lower(hsc.coursetitle) not like '%hlth%'
									and lower(hsc.coursetitle) not like '%his%'
									and lower(hsc.coursetitle) not like '%hst%'
									and lower(hsc.coursetitle) not like '%geog%'
									and lower(hsc.coursetitle) not like '%world%'
									and lower(hsc.coursetitle) not like '%wld%'
									and lower(hsc.coursetitle) not like '%wrld%'
									and lower(hsc.coursetitle) not like '%lap%'
									and lower(hsc.coursetitle) not like '%clt%'
									and lower(hsc.coursetitle) not like '%cult%'
								) then '2413'
							end
						else hsc.courseid
					end
				and cbeds.year_code_start <= ay.code_calpads
				and cbeds.year_code_end >= ay.code_calpads
			left outer join
			calpass.dbo.K12CourseProd_cbeds_rank cbeds_rank
				on cbeds_rank.code = cbeds.code
		WHERE
			hss.Derkey1 = @derkey1
			and hss.GradeLevel in ('09', '10', '11', '12')
			and (
				@AcYear is null
				or
				hss.AcYear = @AcYear
			)
			and (
				@GradeLevel is null
				or
				hss.GradeLevel = @GradeLevel
			)
			-- Elk Grove No MOU
			and hss.School not in (
				SELECT
					organizationCode
				FROM
					calpass.dbo.organization
				WHERE
					parentId = 159610
			)
		GROUP BY
			hss.Derkey1,
			/* school */
			hss.School,
			hss.AcYear,
			rtrim(hsc.courseTerm),
			rtrim(hss.GradeLevel),
			/* course */
			rtrim(hsc.CourseTitle),
			rtrim(hsc.LocallyAssignedCourseNumber),
			rtrim(hsc.CourseId),
			cbeds.content_area_category_assignment_name,
			rtrim(
				case
					when hsc.AGstatus = '  ' then null
					else hsc.AGstatus
				end
			),
			/* section */
			rtrim(hsc.CourseSectionClassNumber),
			rtrim(hsc.Grade),
			hscg.Points,
			case
				when hsc.CreditEarned >= 99 then 0.00
				else hsc.CreditEarned
			end,
			case
				when hsc.CreditAttempted >= 99 then 0.00
				else hsc.CreditAttempted
			end,
			/* ranks */
			ay.year_leading, -- year cast as int
			ct.season_rank, -- school term rank; fall = 1, winter = 2; spring = 3, summer = 4
			cbeds_rank.rank, -- cbeds course rank
			cl.rank, -- course level rank
			ag.rank, -- ag status rank
			hscg.rank -- grade rank; a = 1, b = 2, ...
	);