use calpass;

GO

IF (object_id('f_hs_student_cgpa', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_hs_student_cgpa;
	END;

GO

CREATE FUNCTION
	f_hs_student_cgpa
	(
		@Derkey1 char(15),
		@AcYear char(4) = null,
		@GradeLevel char(2) = null
	)
RETURNS
	TABLE
AS
RETURN (
	SELECT
		student_units_attempted = sum(
				case
					when section_credit_attempted >= 99 then 0.00
					else section_credit_attempted
				end
			),
		student_units_completed = sum(
				case
					when section_credit_achieved >= 99 then 0.00
					else section_credit_achieved
				end
			),
		student_grade_points_possible = sum(
				case
					when section_credit_achieved >= 99 then 0.00
					else section_credit_achieved
				end * 4.0
			),
		student_grade_points_achieved = sum(
				case
					when section_credit_achieved >= 99 then 0.00
					else section_credit_achieved
				end * section_grade_points
			),
		student_cumulative_gpa = 1.000 * isnull(sum(
				case
					when section_credit_achieved >= 99 then 0.00
					else section_credit_achieved
				end * section_grade_points
			) / nullif(sum(
				case
					when section_credit_attempted >= 99 then 0.00
					else section_credit_attempted
				end
			), 0), 0),
		hs_schools_09 = count(distinct case when organization_grade_level_code = '09' then organization_cds_code end),
		hs_schools_10 = count(distinct case when organization_grade_level_code = '10' then organization_cds_code end),
		hs_schools_11 = count(distinct case when organization_grade_level_code = '11' then organization_cds_code end),
		hs_schools_12 = count(distinct case when organization_grade_level_code = '12' then organization_cds_code end),
		student_years_09 = count(distinct case when organization_grade_level_code = '09' then organization_year_code end),
		student_years_10 = count(distinct case when organization_grade_level_code = '10' then organization_year_code end),
		student_years_11 = count(distinct case when organization_grade_level_code = '11' then organization_year_code end),
		student_years_12 = count(distinct case when organization_grade_level_code = '12' then organization_year_code end)
	FROM
		calpass.dbo.f_hs_student_transcript(
			@Derkey1,
			@GradeLevel,
			@AcYear
		) t
	WHERE
		(
			@GradeLevel is null
			or
			t.organization_grade_level_code in (
				SELECT
					gl1.code
				FROM
					calpass.dbo.K12StudentProd_GradeLevel gl1
					inner join
					calpass.dbo.K12StudentProd_GradeLevel gl2
						on gl1.code_rank <= gl2.code_rank
				WHERE
					gl2.code = @GradeLevel
			)
		)
		and (
			@AcYear is null
			or
			t.organization_year_code in (
				SELECT
					ay1.code
				FROM
					calpass.dbo.K12StudentProd_AcYear ay1
					inner join
					calpass.dbo.K12StudentProd_AcYear ay2
						on ay1.year_leading <= ay2.year_leading
				WHERE
					ay2.code = @AcYear
			)
		)
	GROUP BY
		t.derkey1
	HAVING
		(1.000 * isnull(sum(
			case
				when section_credit_achieved >= 99 then 0.00
				else section_credit_achieved
			end * section_grade_points
		) / nullif(sum(
			case
				when section_credit_attempted >= 99 then 0.00
				else section_credit_attempted
			end
		), 0), 0)) < 10.000
);