use calpass;

GO

IF (object_id('f_hs_socioeconomic_ind', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_hs_socioeconomic_ind;
	END;

GO

CREATE FUNCTION
	f_hs_socioeconomic_ind
	(
		@derkey1 char(15)
	)
RETURNS
	TABLE
AS
RETURN (
	SELECT
		socioeconomic_ind = max(
			case
				when ParentEdLvl = '14' then 1
				when NSLP = 'Y' then 1
				when NSLP <> 'Y' then 0
			end
		)
	FROM
		calpass.dbo.k12stardemprod
	WHERE
		derkey1 = @derkey1
	GROUP BY
		derkey1
);