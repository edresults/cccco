USE calpass;

GO

IF (object_id('dbo.CCMathGetFirst', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.CCMathGetFirst;
	END;

GO

CREATE FUNCTION
	dbo.CCMathGetFirst
	(
		@Derkey1 binary(64)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			t.FirstYearTermCode,
			ContentAreaFirstYearTermCode = t.YearTermCode,
			ArithmeticMarkPoints = max(case when CourseLevelCode = 'D' then t.MarkPoints end),
			PreAlgebraMarkPoints = max(case when CourseLevelCode = 'C' then t.MarkPoints end),
			AlgebraIMarkPoints = max(case when CourseLevelCode = 'B' then t.MarkPoints end),
			GeometryMarkPoints = max(case when CourseTitle like '%Geo%' then t.MarkPoints end),
			AlgebraIIMarkPoints = max(case when CourseLevelCode = 'A' then t.MarkPoints end),
			CollegeAlgebraMarkPoints = 
				max(
					case
						when CourseTitle like '%COLL%' then t.MarkPoints
						when CourseTitle like '%ADVANCED ALG%' then t.MarkPoints
						when CourseTitle like '%ALGEBRA FOR CALC%' then t.MarkPoints
					end
				),
			GEMathMarkPoints = 
				max(
					case
						when CourseTitle like '%MATH FOR%' then t.MarkPoints
						when CourseTitle like '%MATH%LIB%' then t.MarkPoints
						when CourseTitle like '%MATH%ELEM%' then t.MarkPoints
						when CourseTitle like '%MATH TOPICS%' then t.MarkPoints
						when CourseTitle like '%MATHEMATICAL%' then t.MarkPoints
						when CourseTitle like '%MATHEMATICS%' then t.MarkPoints
						when CourseTitle like '%THE IDEAS%' then t.MarkPoints
						when CourseTitle like '%LIBERAL%' then t.MarkPoints
						when CourseTitle like '%CONCEPTS%' then t.MarkPoints
						when CourseTitle like '%IDEAS%' then t.MarkPoints
						when CourseTitle like '%TEACH%' then t.MarkPoints
						when CourseTitle like '%LIFE%' then t.MarkPoints
						when CourseTitle like '%BUS%' then t.MarkPoints
						when CourseTitle like '%BRIEF%' then t.MarkPoints
						when CourseTitle like '%SOC%' then t.MarkPoints
						when CourseTitle like '%BIO%' then t.MarkPoints
						when CourseTitle like '%SHORT%' then t.MarkPoints
					end
				),
			StatisticsMarkPoints = 
				max(
					case
						when CourseTitle like '%STA%' then t.MarkPoints
						when CourseTitle like '%INTRO STA%' then t.MarkPoints
						when CourseTitle like '%ELEM PROB%' then t.MarkPoints
						when CourseTitle like '%ELEM. PROB%' then t.MarkPoints
						when CourseTitle like '%ELEM STA%' then t.MarkPoints
						when CourseTitle like '%ELEMENTARY STA%' then t.MarkPoints
						when CourseTitle like '%ELEMNTRY STA%' then t.MarkPoints
						when CourseTitle like '%INTRO PROB%' then t.MarkPoints
						when CourseTitle like '%INTRO TO PROB%' then t.MarkPoints
						when CourseTitle like '%INTRO APPLIED STATS%' then t.MarkPoints
						when CourseTitle like '%HONORS STA%' then t.MarkPoints
					end
				),
			PreCalculusMarkPoints = 
				max(
					case
						when CourseTitle like '%PRE%CALC%' then t.MarkPoints
					end
				),
			CalculusIMarkPoints = 
				max(
					case
						when CourseTitle like '%CALC%' then
							case
								when CourseTitle not like '%PRE%' then t.MarkPoints
								when CourseTitle not like '%II%' then t.MarkPoints
								when CourseTitle not like '%2%' then t.MarkPoints
								when CourseTitle not like '%3%' then t.MarkPoints
								when CourseTitle not like '%4A%' then t.MarkPoints
								when CourseTitle not like '%DIFF%' then t.MarkPoints
							end
					end
				),
			CalculusIIMarkPoints = 
				max(
					case
						when CourseTitle like '%CALC%' and CourseTitle not like '%PRE%' then
							case
								when CourseTitle like '%II%' then t.MarkPoints
								when CourseTitle like '%2%' then t.MarkPoints
								when CourseTitle like '%3%' then t.MarkPoints
							end
					end
				),
			ArithmeticMarkCategory = max(case when CourseLevelCode = 'D' then t.MarkCategory end),
			PreAlgebraMarkCategory = max(case when CourseLevelCode = 'C' then t.MarkCategory end),
			AlgebraIMarkCategory = max(case when CourseLevelCode = 'B' then t.MarkCategory end),
			GeometryMarkCategory = max(case when CourseTitle like '%Geo%' then t.MarkCategory end),
			AlgebraIIMarkCategory = max(case when CourseLevelCode = 'A' then t.MarkCategory end),
			CollegeAlgebraMarkCategory = 
				max(
					case
						when CourseTitle like '%COLL%' then t.MarkCategory
						when CourseTitle like '%ADVANCED ALG%' then t.MarkCategory
						when CourseTitle like '%ALGEBRA FOR CALC%' then t.MarkCategory
					end
				),
			GEMathMarkCategory = 
				max(
					case
						when CourseTitle like '%MATH FOR%' then t.MarkCategory
						when CourseTitle like '%MATH%LIB%' then t.MarkCategory
						when CourseTitle like '%MATH%ELEM%' then t.MarkCategory
						when CourseTitle like '%MATH TOPICS%' then t.MarkCategory
						when CourseTitle like '%MATHEMATICAL%' then t.MarkCategory
						when CourseTitle like '%MATHEMATICS%' then t.MarkCategory
						when CourseTitle like '%THE IDEAS%' then t.MarkCategory
						when CourseTitle like '%LIBERAL%' then t.MarkCategory
						when CourseTitle like '%CONCEPTS%' then t.MarkCategory
						when CourseTitle like '%IDEAS%' then t.MarkCategory
						when CourseTitle like '%TEACH%' then t.MarkCategory
						when CourseTitle like '%LIFE%' then t.MarkCategory
						when CourseTitle like '%BUS%' then t.MarkCategory
						when CourseTitle like '%BRIEF%' then t.MarkCategory
						when CourseTitle like '%SOC%' then t.MarkCategory
						when CourseTitle like '%BIO%' then t.MarkCategory
						when CourseTitle like '%SHORT%' then t.MarkCategory
					end
				),
			StatisticsMarkCategory = 
				max(
					case
						when CourseTitle like '%STA%' then t.MarkCategory
						when CourseTitle like '%INTRO STA%' then t.MarkCategory
						when CourseTitle like '%ELEM PROB%' then t.MarkCategory
						when CourseTitle like '%ELEM. PROB%' then t.MarkCategory
						when CourseTitle like '%ELEM STA%' then t.MarkCategory
						when CourseTitle like '%ELEMENTARY STA%' then t.MarkCategory
						when CourseTitle like '%ELEMNTRY STA%' then t.MarkCategory
						when CourseTitle like '%INTRO PROB%' then t.MarkCategory
						when CourseTitle like '%INTRO TO PROB%' then t.MarkCategory
						when CourseTitle like '%INTRO APPLIED STATS%' then t.MarkCategory
						when CourseTitle like '%HONORS STA%' then t.MarkCategory
					end
				),
			PreCalculusMarkCategory = 
				max(
					case
						when CourseTitle like '%PRE%CALC%' then t.MarkCategory
					end
				),
			CalculusIMarkCategory = 
				max(
					case
						when CourseTitle like '%CALC%' then
							case
								when CourseTitle not like '%PRE%' then t.MarkCategory
								when CourseTitle not like '%II%' then t.MarkCategory
								when CourseTitle not like '%2%' then t.MarkCategory
								when CourseTitle not like '%3%' then t.MarkCategory
								when CourseTitle not like '%4A%' then t.MarkCategory
								when CourseTitle not like '%DIFF%' then t.MarkCategory
							end
					end
				),
			CalculusIIMarkCategory = 
				max(
					case
						when CourseTitle like '%CALC%' and CourseTitle not like '%PRE%' then
							case
								when CourseTitle like '%II%' then t.MarkCategory
								when CourseTitle like '%2%' then t.MarkCategory
								when CourseTitle like '%3%' then t.MarkCategory
							end
					end
				)
		FROM
			dbo.CCTranscriptGet(@Derkey1) t
		WHERE
			t.TopCode = '170100'
			and t.CourseLevelBelow = (
				SELECT
					min(t1.CourseLevelBelow)
				FROM
					dbo.CCTranscriptGet(@Derkey1) t1
				WHERE
					t1.YearTermCode = (
						SELECT
							min(t2.YearTermCode)
						FROM
							dbo.CCTranscriptGet(@Derkey1) t2
						WHERE
							t2.TopCode = t1.TopCode
					)
					and t1.YearTermCode = t.YearTermCode
					and t1.TopCode = t.TopCode
			)
		GROUP BY
			t.FirstYearTermCode,
			t.YearTermCode
	);