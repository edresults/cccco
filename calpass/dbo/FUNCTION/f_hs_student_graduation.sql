USE calpass;

GO

IF (object_id('dbo.f_hs_student_graduation', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_hs_student_graduation;
	END;

GO

CREATE FUNCTION
	dbo.f_hs_student_graduation
	(
		@Derkey1 char(15)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			graduation_code = a2.AwardType,
			graduation_year_code = a2.AcYear,
			graduation_year_description = y2.code_calpads
		FROM
			calpass.dbo.K12AwardProd a2
			inner join
			calpass.dbo.K12StudentProd_AcYear y2
				on a2.AcYear = y2.code
		WHERE
			a2.Derkey1 = @Derkey1
			and a2.AwardType = (
				SELECT
					min(a1.AwardType)
				FROM
					calpass.dbo.K12AwardProd a1
					inner join
					calpass.dbo.K12StudentProd_AcYear y1
						on a1.AcYear = y1.code
				WHERE
					a2.School = a1.School
					and a2.LocStudentId = a1.LocStudentId
					and a2.AcYear = a1.AcYear
					and y2.year_trailing = y1.year_trailing
					and y1.year_trailing = (
						SELECT
							min(y.year_trailing)
						FROM
							calpass.dbo.K12AwardProd a
							inner join
							calpass.dbo.K12StudentProd_AcYear y
								on a.AcYear = y.code
						WHERE
							a1.School = a.School
							and a1.LocStudentId = a.LocStudentId
							and a1.AcYear = a.AcYear
							and a.AwardType in ('100','106','108','120','250','320','330','480')
							and exists (
								SELECT
									1
								FROM
									calpass.dbo.K12StudentProd s
								WHERE
									s.gradeLevel in ('09', '10', '11', '12')
									and s.School = a.School
									and s.LocStudentId = a.LocStudentId
									and s.AcYear = a.AcYear
							)
					)
			)
	);