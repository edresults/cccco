USE calpass;

GO

IF (object_id('calpass.dbo.f_cc_student_financial_aid', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_cc_student_financial_aid;
	END;

GO

CREATE FUNCTION
	dbo.f_cc_student_financial_aid
	(
		@CollegeId char(6),
		@StudentId varchar(10),
		@AcademicYear char(9)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			ComisCode = ap.college_id,
			StudentCode = ap.student_id,
			ap.term_id,
			ap.status,
			ap.time_period,
			ap.budget_category,
			ap.budget_amount,
			ap.dependecy_status,
			ap.household_size,
			ap.family_status,
			ap.income_agi_parent,
			ap.income_agi_student,
			ap.untax_inc_parent,
			ap.untax_inc_student,
			ap.efc,
			aw.term_recd,
			aw.type_id,
			aw.amount
		FROM
			calpass.comis.studntid id
			inner join
			calpass.comis.sfappl ap
				on ap.college_id = id.college_id
				and ap.student_id = id.student_id
			left outer join
			calpass.comis.sfawards aw
				on aw.college_id = ap.college_id
				and aw.student_id = ap.student_id
				and aw.term_id = ap.term_id
		WHERE
			id.college_id = (
				SELECT
					CollegeCode
				FROM
					calpass.comis.College
				WHERE
					IpedsCode = @CollegeId
			)
			and id.ssn = @StudentId
			and ap.term_id = (
				SELECT DISTINCT
					YearCode
				FROM
					calpass.comis.Term
				WHERE
					AcademicYear = @AcademicYear
			)
	);