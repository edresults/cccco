USE calpass;

GO

IF (object_id('calpass.dbo.f_legacy_decrypt', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_legacy_decrypt;
	END;

GO

CREATE FUNCTION
	dbo.f_legacy_decrypt
	(
		@derkey1 char(15)
	)
RETURNS
	TABLE
AS
RETURN (
	SELECT DISTINCT
		s.derkey1,
		college_id = c.code,
		student_key = s.StudentId,
		id_status = s.IdStatus
	FROM
		calpass.dbo.ccstudentprod s
			with(index(idx_ccstud_derkey))
		inner join
		comis.dbo.district_college c
			on s.collegeId = c.code_ipeds_legacy
	WHERE
		s.derkey1 = @derkey1
);