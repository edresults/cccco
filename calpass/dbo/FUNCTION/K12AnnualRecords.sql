CREATE FUNCTION
    dbo.K12AnnualRecords
    (
        @CdsCode char(14)
    )
RETURNS
    TABLE
AS
    RETURN (
        SELECT
            Origin       = case when School = @CdsCode then 'District' else 'Charter' end,
            SourceType   = 'Student',
            School       = s.School,
            AcademicYear = y.YearCode,
            Records      = count(*)
        FROM
            calpass.dbo.K12StudentProd s
            inner join
            calpass.calpads.Year y
                on y.YearCodeAbbr = s.AcYear
        WHERE
            School in (@CdsCode, replicate(substring(@CdsCode, 8, 7), 2))
        GROUP BY
            s.School,
            y.YearCode
        UNION ALL
        SELECT
            Origin       = case when School = @CdsCode then 'District' else 'Charter' end,
            SourceType   = 'Course',
            School       = c.School,
            AcademicYear = y.YearCode,
            Records      = count(*)
        FROM
            calpass.dbo.K12CourseProd c
            inner join
            calpass.calpads.Year y
                on y.YearCodeAbbr = c.AcYear
        WHERE
            School in (@CdsCode, replicate(substring(@CdsCode, 8, 7), 2))
        GROUP BY
            c.School,
            y.YearCode
        UNION ALL
        SELECT
            Origin       = case when School = @CdsCode then 'District' else 'Charter' end,
            SourceType   = 'Award',
            School       = a.School,
            AcademicYear = y.YearCode,
            Records      = count(*)
        FROM
            calpass.dbo.K12AwardProd a
            inner join
            calpass.calpads.Year y
                on y.YearCodeAbbr = a.AcYear
        WHERE
            School in (@CdsCode, replicate(substring(@CdsCode, 8, 7), 2))
        GROUP BY
            a.School,
            y.YearCode
        ORDER BY
            SourceType   asc,
            School       asc,
            AcademicYear asc
    );