USE calpass;

GO

IF (object_id('dbo.Decipher') is not null)
	BEGIN
		DROP FUNCTION dbo.Decipher;
	END;

GO

CREATE FUNCTION
	dbo.Decipher
	(
		@Payload char(15)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			Payload = 
				convert(
					nchar(15),
					substring(o.payoff, 1, 6) + 
					substring(o.payoff, 15, 1) + 
					substring(o.payoff, 7, 8)
				)
		FROM
			(
				values
				(@Payload)
			) p
			(
				Payload
			)
			cross apply
			(
				SELECT
					case substring(Payload, v.number, 1)
						when '3' then 'A'
						when 'S' then 'B'
						when 'I' then 'C'
						when '#' then 'D'
						when '4' then 'E'
						when 'T' then 'F'
						when 'J' then 'G'
						when '_' then 'H'
						when '5' then 'I'
						when 'U' then 'J'
						when 'K' then 'K'
						when 'A' then 'L'
						when '6' then 'M'
						when 'V' then 'N'
						when 'L' then 'O'
						when 'B' then 'P'
						when '7' then 'Q'
						when 'W' then 'R'
						when 'M' then 'S'
						when 'C' then 'T'
						when '8' then 'U'
						when 'X' then 'V'
						when 'N' then 'W'
						when 'D' then 'X'
						when '9' then 'Y'
						when 'Y' then 'Z'
						when 'O' then '0'
						when 'E' then '1'
						when '0' then '2'
						when 'Z' then '3'
						when 'P' then '4'
						when 'F' then '5'
						when '@' then '6'
						when '1' then '7'
						when 'Q' then '8'
						when 'G' then '9'
						when '2' then ' '
						when 'R' then '$'
						when 'H' then '-'
						when '%' then '.'
						when '&' then ''''
						else ''
					end
				FROM
					master.dbo.spt_values v -- this table will cater to strings up to length 2047
				WHERE
					v.type = 'P'
					and v.number >= 0
					and v.number <= 14
				ORDER BY
					v.number
				FOR
					XML PATH ('')
			) o
			(
				payoff
			)
	);