USE calpass;

GO

IF (object_id('dbo.HSTranscriptGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.HSTranscriptGet;
	END;

GO

CREATE FUNCTION
	dbo.HSTranscriptGet
	(
		@Derkey1 binary(64)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			/* organization */
			CdsCode = s.School,
			GradeCode = s.GradeLevel,
			YearCodeAbbr = s.AcYear,
			TermCode = c.CourseTerm,
			/* course */
			CourseName = c.CourseTitle,
			CourseId = c.LocallyAssignedCourseNumber,
			CourseCode = c.CourseId,
			IsAP = case when c.CourseLevel = '30' then 1 else 0 end,
			/* section */
			m.MarkCode,
			m.IsSuccess,
			m.IsPromoted,
			MarkCategory = m.Category,
			MarkPoints = m.Points,
			CreditTry = c.CreditAttempted * IsGpa,
			CreditGet = c.CreditEarned * IsGpa,
			/* extras */
			QualityPoints = convert(decimal(6,3), m.Points * c.CreditEarned * IsGpa)
		FROM
			dbo.K12StudentProd s
			inner join
			dbo.K12CourseProd c
				on s.School = c.School
				and s.LocStudentId = c.LocStudentId
				and s.AcYear = c.AcYear
			inner join
			calpads.Mark m
				on m.MarkCode = c.Grade
			inner join
			calpads.Grade g
				on g.GradeCode = s.GradeLevel
		WHERE
			s.Derkey1 = @Derkey1
			and g.IsHS = 1
			and c.CreditEarned <= c.CreditAttempted
			and c.CreditAttempted != 99.99
			and c.CreditEarned != 99.99
	);