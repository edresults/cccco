USE calpass;

GO

IF (object_id('dbo.InterSegmentKey', 'IF') is not null)
    BEGIN
        DROP FUNCTION dbo.InterSegmentKey;
    END;

GO

CREATE FUNCTION
    dbo.InterSegmentKey
    (
        @NameFirst varchar(30),
        @NameLast  varchar(40),
        @Gender    char(1),
        @BirthDate varchar(10)
    )
RETURNS
    TABLE
AS
    RETURN (
        SELECT
            InterSegmentKey = 
                HASHBYTES(
                    'SHA2_512',
                    upper(convert(nchar(3), isnull(ltrim(rtrim(@NameFirst)), 'NFN'))) + 
                    upper(convert(nchar(3), isnull(ltrim(rtrim(@NameLast)), 'NLN'))) + 
                    upper(convert(nchar(1), isnull(ltrim(rtrim(@Gender)), 'X'))) + 
                    case when isdate(@Birthdate) = 1 then convert(nchar(8), convert(date, @Birthdate), 112) end
                )
    );