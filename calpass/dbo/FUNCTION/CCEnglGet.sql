USE calpass;

GO

IF (object_id('dbo.CCEnglGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.CCEnglGet;
	END;

GO

CREATE FUNCTION
	dbo.CCEnglGet
	(
		@Derkey1 char(15)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			t.FirstYearTermCode,
			ContentAreaFirstYearTermCode = t.YearTermCode,
			Level0MarkPoints = convert(decimal(4,3), max(case when t.CourseLevelCode = 'Y' then t.MarkPoints end)),
			Level0YearTermCode = max(case when t.CourseLevelCode = 'Y' then t.YearTermCode end),
			Level1MarkPoints = convert(decimal(4,3), max(case when t.CourseLevelCode = 'A' then t.MarkPoints end)),
			Level1YearTermCode = max(case when t.CourseLevelCode = 'A' then t.YearTermCode end),
			Level2MarkPoints = convert(decimal(4,3), max(case when t.CourseLevelCode = 'B' then t.MarkPoints end)),
			Level2YearTermCode = max(case when t.CourseLevelCode = 'B' then t.YearTermCode end),
			Level3MarkPoints = convert(decimal(4,3), max(case when t.CourseLevelCode = 'C' then t.MarkPoints end)),
			Level3YearTermCode = max(case when t.CourseLevelCode = 'C' then t.YearTermCode end),
			Level4MarkPoints = convert(decimal(4,3), max(case when t.CourseLevelCode = 'D' then t.MarkPoints end)),
			Level4YearTermCode = max(case when t.CourseLevelCode = 'D' then t.YearTermCode end),
			Level5MarkPoints = convert(decimal(4,3), max(case when t.CourseLevelCode = 'E' then t.MarkPoints end)),
			Level5YearTermCode = max(case when t.CourseLevelCode = 'E' then t.YearTermCode end)
		FROM
			dbo.CCTranscriptGet(@Derkey1) t
		WHERE
			t.TopCode = '150100'
			and t.YearTermCode = (
				SELECT
					min(t1.YearTermCode)
				FROM
					dbo.CCTranscriptGet(@Derkey1) t1
				WHERE
					t1.TopCode = t.TopCode
					and t1.CourseLevelCode = t.CourseLevelCode
			)
		GROUP BY
			t.FirstYearTermCode,
			t.YearTermCode
	);