USE calpass;

GO

IF (object_id('calpass.dbo.f_hs_student_higher_education', 'IF') is not null)
	BEGIN
		DROP FUNCTION dbo.f_hs_student_higher_education;
	END;

GO

CREATE FUNCTION
	dbo.f_hs_student_higher_education
	(
		@derkey1 char(15)
	)
RETURNS
	TABLE
AS
RETURN (
	SELECT
		cc.derkey1,
		cc.student_ccc_first_term_value,
		cc.student_ccc_first_term_nsa_value,
		cc.student_ccc_first_term_cr_value,
		cc.student_ccc_first_term_cr_nsa_value,
		cc.student_ccc_first_term_ncr_value,
		cc.student_ccc_first_term_ncr_nsa_value,
		univ.student_univ_first_date,
		univ.student_univ_first_code,
		univ.student_univ_last_date,
		univ.student_univ_last_code
	FROM
		(
			SELECT
				c.derkey1,
				-- community college
				student_ccc_first_term_value = min(nullif(nullif(hf.ccc_first_term_value, '     '), '00000')),
				student_ccc_first_term_nsa_value = min(nullif(nullif(hf.ccc_first_term_nsa_value, '     '), '00000')),
				student_ccc_first_term_cr_value = min(nullif(nullif(hf.ccc_first_term_cr_value, '     '), '00000')),
				student_ccc_first_term_cr_nsa_value = min(nullif(nullif(hf.ccc_first_term_cr_nsa_value, '     '), '00000')),
				student_ccc_first_term_ncr_value = min(nullif(nullif(hf.ccc_first_term_ncr_value, '     '), '00000')),
				student_ccc_first_term_ncr_nsa_value = min(nullif(nullif(hf.ccc_first_term_ncr_nsa_value, '     '), '00000'))
			FROM
				(
					SELECT DISTINCT
						derkey1 = s.derkey1,
						college_id = o.code,
						student_key = s.StudentId,
						id_status = s.IdStatus
					FROM
						calpass.dbo.ccstudentprod s
							with(index(idx_ccstud_derkey))
						inner join
						comis.dbo.district_college o
							on s.collegeId = o.code_ipeds_legacy
					WHERE
						s.derkey1 = @derkey1
				) c
				inner join
				comis.dbo.studntid id
					with(index(uk_studntid__college_id__ssn__student_id_status))
					on id.college_id = c.college_id
					and id.ssn = c.student_key
					and id.student_id_status = c.id_status
				inner join
				comis.dbo.hf_first hf
					on hf.college_id = id.college_id
					and hf.student_id = id.student_id
			GROUP BY
				c.derkey1
		) cc
		left outer join
		(
			SELECT
				c.derkey1,
				student_univ_first_date = min(hf.first_date_4yr),
				student_univ_first_code = nullif(hf.first_segment_4yr, '   '),
				student_univ_last_date = max(hf.last_date_4yr),
				student_univ_last_code = nullif(hf.last_segment_4yr, '   ')
			FROM
				(
					SELECT DISTINCT
						derkey1 = s.derkey1,
						college_id = o.code,
						student_key = s.StudentId,
						id_status = s.IdStatus
					FROM
						calpass.dbo.ccstudentprod s
							with(index(idx_ccstud_derkey))
						inner join
						comis.dbo.district_college o
							on s.collegeId = o.code_ipeds_legacy
					WHERE
						s.derkey1 = @derkey1
				) c
				inner join
				comis.dbo.studntid id
					with(index(uk_studntid__college_id__ssn__student_id_status))
					on id.college_id = c.college_id
					and id.ssn = c.student_key
					and id.student_id_status = c.id_status
				inner join
				comis.dbo.hf_first hf
					on hf.college_id = id.college_id
					and hf.student_id = id.student_id
			WHERE
				hf.first_date_4yr is not null
			GROUP BY
				c.derkey1,
				nullif(hf.first_segment_4yr, '   '),
				nullif(hf.last_segment_4yr, '   ')
		) univ
			on cc.derkey1 = univ.derkey1
);