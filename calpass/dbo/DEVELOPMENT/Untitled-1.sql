The InterSegmentKey

Problem:     Matching between educaitonal datasets (e.g., CALPADS, COMIS, CSUOP, UCOP) without unified identifier
ProblemHow:  Fuzzy or Power matching becomes more challenging with only partial first name and partial last name available in COMIS prior to Summer 2011.
ProblemWhy:  Longitudinal analysis from K to 16.

Goal: Generate Unified Unique Cal-PASS Identifier

Identifiers: SSID (CALPADS), SSN (COMIS)

Methods to mitigate. Retroactively update partial first name and partial last name with full first name and full last name of the most primal term for a student starting with Summer 2011 and onward. All students crossing the Summer 2011 threshold will have their partial names updated to full names.

(1) Combination of ISK and SSN?
(1) Update STTERM with full name

Hierarchy of Indetifiers
(1) SSN
(2) SSID
(3) 


A student with a unique SSN may enroll at two or more colleges, and each college report: (1) two or more different first names, or (2) two or more different last names.

Problem with some students adding a middle or last name