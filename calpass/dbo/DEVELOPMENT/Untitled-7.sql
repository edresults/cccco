DECLARE
	@sql        nvarchar(2048),
	@Schema     sysname,
	@Table      sysname,
	@FillFactor char(2) = '90';

	DECLARE
		TableCursor
	CURSOR
		LOCAL
		FAST_FORWARD
	FOR
		SELECT
			table_schema,
			table_name
		FROM
			calpass.information_schema.tables
		WHERE
			table_type = 'BASE TABLE';

BEGIN

	OPEN TableCursor;

	FETCH NEXT FROM
		TableCursor
	INTO
		@Schema,
		@Table;

	WHILE (@@FETCH_STATUS = 0)
	BEGIN

		SET @sql = 'ALTER INDEX ALL ON ' + @Table + ' REBUILD WITH (FILLFACTOR = ' + @FillFactor + ')';
		EXEC (@sql);

		FETCH NEXT FROM
			TableCursor
		INTO
			@Schema,
			@Table
	END

	CLOSE TableCursor
	DEALLOCATE TableCursor;

END;