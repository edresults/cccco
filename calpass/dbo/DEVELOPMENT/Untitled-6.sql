-- IF (object_id('tempdb..#Student') is not null)
-- 	BEGIN
-- 		DROP TABLE #Student;
-- 	END;

-- GO

-- CREATE TABLE
-- 	#Student
-- 	(
-- 		i   int        not null identity(0,1) primary key,
-- 		ssn binary(64) not null
-- 	);

-- SET NOCOUNT ON;

-- INSERT
-- 	#Student
-- 	(
-- 		ssn
-- 	)
-- SELECT
-- 	ssn
-- FROM
-- 	comis.studntid
-- WHERE
-- 	student_id_status = 'S'
-- ORDER BY
-- 	ssn;

DECLARE
	@Severity   tinyint = 0,
	@State      tinyint = 1,
	@Error      varchar(2048),
	@Iterator   int = 0,
	@Increment  int = 10000,
	@Iterations int,
	@Count      int;

SELECT
	@Count = count(*)
FROM
	#Student;

SET @Iterations = ceiling(1.0 * @Count / @Increment);

TRUNCATE TABLE ##test;

WHILE (@Iterator < @Iterations)
BEGIN

	SET @Error = convert(varchar, @Iterator) + ' of ' + convert(varchar, @Iterations - 1);

	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

	INSERT
		##test
	SELECT
		ssn
	FROM
		(
			SELECT
				ssn,
				name_first,
				NameFirstRankMax = max(NameFirstRank) over(partition by ssn),
				name_last,
				NameLastRankMax = max(NameLastRank) over(partition by ssn),
				RecordCount
			FROM
				(
					SELECT
						ssn,
						name_first,
						NameFirstRank = rank() over(partition by ssn order by name_first),
						name_last,
						NameLastRank = rank() over(partition by ssn order by name_last),
						RecordCount = count(*) over(partition by ssn)
					FROM
						(
							SELECT
								id.ssn,
								st.name_first,
								st.name_last,
								IsFirstTerm = rank() over (partition by id.ssn order by t.YearTermCode asc)
							FROM
								comis.studntid id
								inner join
								comis.stterm st
									on st.college_id = id.college_id
									and st.student_id = id.student_id
								inner join
								comis.Term t
									on t.TermCode = st.term_id
							WHERE
								exists (
									SELECT
										1
									FROM
										#Student s
									WHERE
										i >= (@Increment * @Iterator)
										and i < (@Increment * (@Iterator + 1))
										and s.ssn = id.ssn
								)
								and t.YearTermCode >= '20115'
								and substring(st.name_first, 1, 3) not in ('XXX', 'NFN')
								and substring(st.name_last, 1, 3) not in ('XXX', 'NLN')
						) a
					WHERE
						IsFirstTerm = 1
				) b
		) c
	WHERE
		(
			NameFirstRankMax > 1
			or
			NameFirstRankMax > 1
		);

	SET @Iterator += 1;
END;