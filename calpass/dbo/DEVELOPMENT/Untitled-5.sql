SELECT
	a.InterSegmentKey,
	id1.college_id,
	id1.student_id,
	id1.ssn,
	id1.student_id_status,
	t.TermCode,
	Payload = 
		upper(convert(nchar(3), st.name_first)) + 
		upper(convert(nchar(3), st.name_last)) + 
		upper(convert(nchar(1), st.gender)) + 
		convert(nchar(8), sb.birthdate, 112),
	PayloadFull = 
		upper(convert(nvarchar(30), st.name_first)) + 
		upper(convert(nvarchar(40), st.name_last)) + 
		upper(convert(nvarchar(1), st.gender)) + 
		convert(nchar(8), birthdate, 112)
FROM
	(
		SELECT TOP 10
			s.InterSegmentKey
		FROM
			comis.Student s
			inner join
			comis.studntid id
				on id.InterSegmentKey = s.InterSegmentKey
			inner join
			comis.sbstudnt sb
				on sb.college_id = id.college_id
				and sb.student_id = id.student_id
			inner join
			comis.Term t
				on t.TermCode = sb.term_first
		WHERE
			s.IsCollision = 1
			and t.YearTermCode >= '20115'
	) a
	inner join
	comis.studntid id1
		on id1.InterSegmentKey = a.InterSegmentKey
	inner join
	comis.sbstudnt sb
		on sb.college_id = id1.college_id
		and sb.student_id = id1.student_id
	inner join
	comis.stterm st
		on st.college_id = sb.college_id
		and st.student_id = sb.student_id
	inner join
	comis.Term t
		on t.TermCode = st.term_id
WHERE
	t.YearTermCode = (
		SELECT
			min(t1.YearTermCode)
		FROM
			comis.stterm st1
			inner join
			comis.Term t1
				on t1.TermCode = st1.term_id
		WHERE
			st1.college_id = st.college_id
			and st1.student_id = st.student_id
	)

SELECT
	id.*
FROM
	comis.studntid id
	cross apply
	comis.IskPayloadByPK(id.college_id, id.student_id)
WHERE
	id.InterSegmentKey = 0xFFFF6E1A38684E30A5C51AC88D84F8A287F026641A09B6C27A95E338AC0F2EF85F27148EB7220BB755F4F6B2A3BCB976BE27A5DB65762C4E87E223AB0A46988C


SELECT * FROM ccstudentprod where derkey1 = 0xFFFF6E1A38684E30A5C51AC88D84F8A287F026641A09B6C27A95E338AC0F2EF85F27148EB7220BB755F4F6B2A3BCB976BE27A5DB65762C4E87E223AB0A46988C
SELECT
	*
FROM
	ccstudentprod
WHERE
		   (CollegeId = '993236' and StudentComisId = 'C51664643')
		or (CollegeId = '993236' and StudentComisId = 'C13453412')
		or (CollegeId = '114716' and StudentComisId = 'F26320160')
		or (CollegeId = '113218' and StudentComisId = '276851745')
		or (CollegeId = '111939' and StudentComisId = '845861737')
		or (CollegeId = '113111' and StudentComisId = 'A40943943')