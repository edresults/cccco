USE calpass;

GO

IF (type_id('dbo.CCFinAidAwardsProd') is not null)
	BEGIN
		DROP TYPE dbo.CCFinAidAwardsProd;
	END;

GO

CREATE TYPE
	dbo.CCFinAidAwardsProd
AS TABLE
	(
		CollegeId char(6) not null,
		StudentComisId char(9) not null,
		StudentId varchar(10) not null,
		IdStatus char(1) not null,
		TermId char(3) not null,
		TermRecd char(3) not null,
		TypeId char(2) not null,
		Amount int not null,
		PRIMARY KEY CLUSTERED
			(
				CollegeId,
				StudentId,
				IdStatus,
				TermId,
				TermRecd,
				TypeId
			)
	);