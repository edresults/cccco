IF (type_id('dbo.InterSegment') is not null)
	BEGIN
		DROP TYPE dbo.InterSegment;
	END;

GO

CREATE TYPE
	dbo.InterSegment
AS TABLE
	(
		Iterator        int identity(0,1) not null,
		InterSegmentKey binary(64)        not null,
		PRIMARY KEY CLUSTERED
			(
				InterSegmentKey
			),
		INDEX
			IX_InterSegment__Iterator
		NONCLUSTERED
			(
				Iterator
			)
	);