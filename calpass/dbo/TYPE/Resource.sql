USE calpass;

GO

IF (type_id('dbo.Resource') is not null)
	BEGIN
		DROP TYPE dbo.Resource;
	END;

GO

CREATE TYPE
	dbo.Resource
AS TABLE
	(
		Iterator int identity(0,1),
		ResourceName nvarchar(255),
		IsLocked bit default ( 0 ),
		PRIMARY KEY CLUSTERED
			(
				Iterator
			)
	);