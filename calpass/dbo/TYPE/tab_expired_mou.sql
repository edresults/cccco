USE calpass;

GO

IF (
	EXISTS (
		SELECT
			1
		FROM
			calpass.sys.schemas s
			inner join
			calpass.sys.types t
				on s.schema_id = t.schema_id
		WHERE
			s.name = 'dbo'
			and t.name = 'tab_expired_mou'
		)
	)
	BEGIN
		DROP TYPE dbo.tab_expired_mou;
	END;

GO

CREATE TYPE
	dbo.tab_expired_mou
AS TABLE
	(
		organizationCode CHAR(6) NOT NULL PRIMARY KEY CLUSTERED
	);
