USE calpass;

GO

IF (type_id('dbo.K12StudentProd') is not null)
	BEGIN
		DROP TYPE dbo.K12StudentProd;
	END;

GO

CREATE TYPE
	dbo.K12StudentProd
AS TABLE
	(
		Derkey1 binary(64) not null,
		School char(14) not null,
		AcYear char(4) not null,
		LocStudentId char(15) not null,
		StudentId char(15),
		CSISNum char(10),
		Fname char(30),
		Lname char(40),
		Gender char(1),
		Ethnicity char(3),
		Birthdate char(8),
		GradeLevel char(2) not null,
		HomeLanguage char(2),
		HispanicEthnicity char(1),
		EthnicityCode1 char(3),
		EthnicityCode2 char(3),
		EthnicityCode3 char(3),
		EthnicityCode4 char(3),
		EthnicityCode5 char(3),
		DateAdded datetime null default ( getdate() ),
		PRIMARY KEY CLUSTERED
		(
			School,
			LocStudentId,
			AcYear,
			GradeLevel
		)
	);