USE calpass;

GO

IF (type_id('dbo.HSTranscriptSummaryHash') is not null)
	BEGIN
		DROP TYPE dbo.HSTranscriptSummaryHash;
	END;

GO

CREATE TYPE
	dbo.HSTranscriptSummaryHash
AS TABLE
	(
		Entity varchar(255) not null,
		Attribute varchar(255) not null,
		Value decimal(4,3) not null,
		InterSegmentKey binary(64) not null,
		PRIMARY KEY CLUSTERED
			(
				Entity,
				Attribute,
				Value,
				InterSegmentKey
			)
	);