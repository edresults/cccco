USE calpass;

GO

-- DROP PROCEDURE dbo.HSTranscriptSummaryHashMerge;
-- DROP PROCEDURE mmap.StudentTranscriptEnglishHashSelect;
-- DROP PROCEDURE mmap.StudentTranscriptMathematicsHashSelect;
-- DROP PROCEDURE mmap.StudentTranscriptProcess;
-- DROP PROCEDURE dbo.StudentMerge;
-- DROP PROCEDURE comis.StudentMerge;
-- DROP PROCEDURE mmap.StudentTranscriptCgpaHashSelect;
-- DROP PROCEDURE dbo.HSTranscriptSummaryHashSelect;
-- DROP PROCEDURE dbo.HSCgpaMerge;

IF (type_id('dbo.Student') is not null)
	BEGIN
		DROP TYPE dbo.Student;
	END;

GO

CREATE TYPE
	dbo.Student
AS TABLE
	(
		InterSegmentKey binary(64) not null,
		IsHighSchoolStudent bit not null,
		IsHighSchoolSection bit not null,
		IsGrade09Student bit not null,
		IsGrade09Section bit not null,
		IsGrade10Student bit not null,
		IsGrade10Section bit not null,
		IsGrade10StudentInclusive bit not null,
		IsGrade10SectionInclusive bit not null,
		IsGrade11Student bit not null,
		IsGrade11Section bit not null,
		IsGrade11StudentInclusive bit not null,
		IsGrade11SectionInclusive bit not null,
		IsGrade12Student bit not null,
		IsGrade12Section bit not null,
		IsGrade12StudentInclusive bit not null,
		IsGrade12SectionInclusive bit not null,
		IsHighSchoolCollision bit not null,
		PRIMARY KEY CLUSTERED
		(
			InterSegmentKey
		)
	);