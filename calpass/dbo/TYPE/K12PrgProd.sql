USE calpass;

GO

IF (type_id('dbo.K12PrgProd') is not null)
	BEGIN
		DROP TYPE dbo.K12PrgProd;
	END;

GO

CREATE TYPE
	dbo.K12PrgProd
AS TABLE
	(
		Derkey1 binary(64) not null,
		School char(14) not null,
		AcYear char(4) not null,
		LocStudentId varchar(15) not null,
		ProgramCode char(3) not null,
		MembershipCode char(1),
		MembershipStartDate char(8) not null,
		MembershipEndDate char(8),
		ServiceYearCode char(9) not null,
		ServiceCode char(2) not null,
		AcademyId char(5),
		MigrantId char(11),
		DisabilityCode char(3),
		AccountabilityDistrictCode char(7),
		DwellingCode char(3),
		IsHomeless char(1),
		IsRunaway char(1),
		DateAdded datetime default ( getdate() ),
		PRIMARY KEY CLUSTERED 
			(
				School,
				LocStudentId,
				AcYear,
				ProgramCode,
				MembershipStartDate,
				ServiceYearCode,
				ServiceCode
			)
	);