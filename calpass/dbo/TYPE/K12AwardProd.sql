USE calpass;

GO

IF (type_id('dbo.K12AwardProd') is not null)
	BEGIN
		DROP TYPE dbo.K12AwardProd;
	END;

GO

CREATE TYPE
	dbo.K12AwardProd
AS TABLE
	(
		Derkey1 binary(64) not null,
		School char(14) not null,
		AcYear char(4) not null,
		LocStudentId varchar(15) not null,
		AwardType char(3) not null,
		AwardDate char(8) not null,
		DateAdded datetime default ( getdate() ),
		PRIMARY KEY CLUSTERED
		(
			School,
			LocStudentId,
			AcYear,
			AwardType,
			AwardDate
		)
	);