USE calpass;

GO

IF (type_id('dbo.Escrow') is not null)
	BEGIN
		DROP TYPE dbo.Escrow;
	END;

GO

CREATE TYPE
	dbo.Escrow
AS TABLE
	(
		SubmissionFileId int,
		SubmissionFileRecordNumber int,
		IsEscrow bit DEFAULT ( 0 ),
		INDEX
			ixc_Escrow__SubmissionFileId__SubmissionFileRecordNumber
		CLUSTERED
			(
				SubmissionFileId,
				SubmissionFileRecordNumber
			)
	);