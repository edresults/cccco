USE calpass;

GO

IF (type_id('dbo.K12CteProd') is not null)
	BEGIN
		DROP TYPE dbo.K12CteProd;
	END;

GO

CREATE TYPE
	dbo.K12CteProd
AS TABLE
	(
		Derkey1 binary(64) not null,
		School char(14) not null,
		AcYear char(4) not null,
		LocStudentId varchar(15) not null,
		CtePathwayCode char(3) not null,
		CtePathwayCompAcYear char(9),
		DateAdded datetime default ( getdate() ),
		PRIMARY KEY CLUSTERED 
			(
				School,
				LocStudentId,
				AcYear,
				CtePathwayCode
			)
	);