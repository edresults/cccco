USE calpass;

GO

IF (type_id('dbo.CCCourseProdPlus') is not null)
	BEGIN
		DROP TYPE dbo.CCCourseProdPlus;
	END;

GO

CREATE TYPE
	dbo.CCCourseProdPlus
AS TABLE
	(
		CollegeId char(6) not null,
		StudentComisId char(9) not null,
		StudentId varchar(10) not null,
		IdStatus char(1) not null,
		TermId char(3) not null,
		CourseId varchar(12) not null,
		SectionId varchar(6) not null,
		Title varchar(68),
		UnitsMax decimal(5,2),
		UnitsAttempted decimal(5,2),
		UnitsEarned decimal(5,2),
		Grade varchar(3),
		CreditFlag char(1),
		TopCode char(6),
		TransferStatus char(1),
		BSStatus char(1),
		SamCode char(1),
		ClassCode char(1),
		CollegeLevel char(1),
		NCRCategory char(1),
		CCLongTermId char(5),
		extSX05_PositiveAttendanceHours decimal(5,1),
		extSXD4_TotalHours decimal(7,1),
		extXF01_SessionInstuctionMethod1 char(2),
		extXF01_SessionInstuctionMethod2 char(2),
		extXF01_SessionInstuctionMethod3 char(2),
		extXF01_SessionInstuctionMethod4 char(2),
		extXBD3_DayEveningClassCode char(1),
		extCB00_ControlNumber varchar(12),
		extCB04_CreditStatus char(1),
		extCB10_CoopWorkExpEdStatus char(1),
		extCB13_SpecialClassStatus char(1),
		extCB14_CanCode varchar(6),
		extCB15_CanSeqCode varchar(8),
		extCB19_CrosswalkCrsDeptName varchar(7),
		extCB20_CrosswalkCrsNumber varchar(9),
		extCB23_FundingAgencyCategory char(1),
		extCB24_ProgramStatus char(1),
		extXB01_AccountingMethod char(1),
		PRIMARY KEY CLUSTERED
			(
				CollegeId,
				StudentId,
				IdStatus,
				TermId,
				CourseId,
				SectionId
			)
	)