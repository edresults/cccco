USE calpass;

GO

IF (type_id('dbo.CCAwardProd') is not null)
	BEGIN
		DROP TYPE dbo.CCAwardProd;
	END;

GO

CREATE TYPE
	dbo.CCAwardProd
AS TABLE
	(
		CollegeId char(6) not null,
		StudentComisId char(9) not null,
		StudentId varchar(10) not null,
		IdStatus char(1) not null,
		TermId char(3) not null,
		TopCode char(6) not null,
		Award char(1) not null,
		DateOfAward char(8) not null,
		RecordId char(1) not null,
		ProgramCode char(5) not null,
		PRIMARY KEY CLUSTERED
			(
				CollegeId,
				StudentId,
				IdStatus,
				TermId,
				TopCode,
				Award,
				ProgramCode,
				DateOfAward,
				RecordId
			)
	);