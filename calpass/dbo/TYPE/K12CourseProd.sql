USE calpass;

GO

IF (type_id('dbo.K12CourseProd') is not null)
	BEGIN
		DROP TYPE dbo.K12CourseProd;
	END;

GO

CREATE TYPE
	dbo.K12CourseProd
AS TABLE
	(
		Derkey1 binary(64) not null,
		School char(14) not null,
		AcYear char(4) not null,
		LocStudentId varchar(15) not null,
		CourseId char(4),
		LocallyAssignedCourseNumber varchar(10) not null,
		CourseSectionClassNumber varchar(15) not null,
		CourseTitle varchar(40),
		AGstatus char(2),
		Grade varchar(3) not null,
		CreditEarned decimal(4,2),
		CreditAttempted decimal(4,2) not null,
		CourseLevel char(2),
		CourseType char(2),
		CourseTerm varchar(3) not null,
		DateAdded datetime default ( getdate() ),
		PRIMARY KEY CLUSTERED
			(
				School,
				LocStudentId,
				AcYear,
				CourseTerm,
				LocallyAssignedCourseNumber,
				CourseSectionClassNumber,
				Grade,
				CreditAttempted
			)
	)