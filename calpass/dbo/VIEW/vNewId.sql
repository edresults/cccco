USE calpass;

GO

IF (object_id('dbo.vNewId', 'V') is not null)
	BEGIN
		DROP VIEW dbo.vNewId;
	END;

GO

CREATE VIEW
	dbo.vNewId
AS
	SELECT NewId = NEWID();