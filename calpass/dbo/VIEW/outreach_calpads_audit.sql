CREATE VIEW
    dbo.outreach_calpads_audit
WITH
    SCHEMABINDING
AS
    SELECT
        p.county                                          as County,
        p.macro                                           as Macro,
        p.micro                                           as Micro,
        p.org_name                                        as Name,
        p.org_code                                        as Code,
        p.academic_year                                   as Year,
        case p.is_charter when 1 then 'Yes' else 'No' end as Charter,
        case when CRSC is null then 'No' else 'Yes' end   as CRSC,
        case when PSTS is null then 'No' else 'Yes' end   as PSTS,
        case when SCSC is null then 'No' else 'Yes' end   as SCSC,
        case when SCTE is null then 'No' else 'Yes' end   as SCTE,
        case when SDIS is null then 'No' else 'Yes' end   as SDIS,
        case when SELA is null then 'No' else 'Yes' end   as SELA,
        case when SENR is null then 'No' else 'Yes' end   as SENR,
        case when SINC is null then 'No' else 'Yes' end   as SINC,
        case when SINF is null then 'No' else 'Yes' end   as SINF,
        case when SIRS is null then 'No' else 'Yes' end   as SIRS,
        case when SOFF is null then 'No' else 'Yes' end   as SOFF,
        case when SPED is null then 'No' else 'Yes' end   as SPED,
        case when SPRG is null then 'No' else 'Yes' end   as SPRG,
        case when STAS is null then 'No' else 'Yes' end   as STAS
    FROM
        dbo.outreach_daily_output
    PIVOT
        (
            max(records) for file_name in (CRSC,PSTS,SCSC,SCTE,SDIS,SELA,SENR,SINC,SINF,SIRS,SOFF,SPED,SPRG,STAS)
        ) p