ALTER PROCEDURE
    OutreachExpirationAudit
AS

    SET NOCOUNT ON;

DECLARE
    -- Constaints
    @Offset         tinyint        = 10,
    @DateInjection  char(14)       = REPLACE(REPLACE(REPLACE(CONVERT(CHAR(19), GETDATE(), 120), ' ', ''), '-', ''), ':', ''),
    @Now            datetime2      = SYSDATETIME(),
    -- Variables
    @Rank           integer        = 0,
    -- Mail
    @Message        nvarchar(2048) = N'',
    @ProfileName    sysname        = 'Cal-PASS SQL Support Profile',
     @Recipients     varchar(max)   = 'dlamoree@edresults.org;outreach@edresults.org',
    --@Recipients     varchar(max)   = 'dlamoree@edresults.org',
    @Subject        nvarchar(255)  = N'',
    @Query          nvarchar(max)  = N'',
    @QueryHeader    bit            = 0,
    @QueryNoPadding bit            = 1,
    @QueryAttach    bit            = 1,
    @QueryFileName  nvarchar(255)  = N'',
    @QuerySeparator char(1)        = '	',
    @QueryWidth     smallint       = 8000,
    @ExcludeInline  bit            = 1;

BEGIN

    SET @Subject       = N'CPP :: Expiration Status';
    SET @QueryFileName = N'CPP_EXPIRATION_STATUS_' + @DateInjection + '.txt';
    SET @Query         = N'
        SET NOCOUNT ON;

        SELECT
            ''Name''   as "Name",
            ''Id''     as "Id",
            ''Date''   as "Date",
            ''Status'' as "Status"
        UNION ALL
        SELECT
            CONVERT(VARCHAR(255), o.OrganizationName) as "Name",
            CONVERT(VARCHAR(255), o.OrganizationId)   as "Id",
            CONVERT(
                VARCHAR(255),
                CONVERT(
                    DATE,
                    m.expirationDate
                ),
                120
            )   as "Date",
            CONVERT(
                VARCHAR(255),
                case
                    when m.expirationDate <= CURRENT_TIMESTAMP then ''Expired''
                    when m.expirationDate - 30 <= CURRENT_TIMESTAMP then ''Stale''
                end
            ) as "Status"
        FROM
            calpass.dbo.cpp_mou m
            inner join
            calpass.dbo.organization o oN
                o.OrganizationId = m.OrganizationId
        WHERE
            m.expirationDate - 30 <= CURRENT_TIMESTAMP;
    ';

    EXEC msdb.dbo.sp_send_dbmail
        @profile_name                = @ProfileName,
        @recipients                  = @Recipients,
        @subject                     = @Subject,
        @body                        = @Message,
        @query                       = @Query,
        @query_result_no_padding     = @QueryNoPadding,
        @query_result_header         = @QueryHeader,
        @attach_query_result_as_file = @QueryAttach,
        @query_attachment_filename   = @QueryFileName,
        @query_result_separator      = @QuerySeparator,
        @exclude_query_output        = @ExcludeInline;
END;