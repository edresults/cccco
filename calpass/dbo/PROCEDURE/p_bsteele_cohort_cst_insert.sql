USE calpass;

GO

IF (object_id('calpass.dbo.p_bsteele_cohort_cst_insert', 'P') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_bsteele_cohort_cst_insert;
	END;

GO

CREATE PROCEDURE
	dbo.p_bsteele_cohort_cst_insert
AS

	SET NOCOUNT ON;

BEGIN

	TRUNCATE TABLE calpass.dbo.bsteele_cohort_cst;
	
	INSERT INTO
		calpass.dbo.bsteele_cohort_cst
		(
			Derkey1,
			math_eap_ind,
			math_subject,
			math_scaled_score,
			math_cluster_1,
			math_cluster_2,
			math_cluster_3,
			math_cluster_4,
			math_cluster_5,
			math_cluster_6,
			engl_eap_ind,
			engl_scaled_score,
			engl_cluster_1,
			engl_cluster_2,
			engl_cluster_3,
			engl_cluster_4,
			engl_cluster_5,
			engl_cluster_6
		)
	SELECT
		coalesce(m.Derkey1, e.Derkey1),
		math_eap_ind,
		math_subject,
		math_scaled_score,
		math_cluster_1,
		math_cluster_2,
		math_cluster_3,
		math_cluster_4,
		math_cluster_5,
		math_cluster_6,
		engl_eap_ind,
		engl_scaled_score,
		engl_cluster_1,
		engl_cluster_2,
		engl_cluster_3,
		engl_cluster_4,
		engl_cluster_5,
		engl_cluster_6
	FROM
		(
			SELECT
				derkey1,
				math_eap_ind,
				math_subject,
				math_scaled_score,
				math_cluster_1,
				math_cluster_2,
				math_cluster_3,
				math_cluster_4,
				math_cluster_5,
				math_cluster_6
			FROM
				(
					SELECT
						derkey1,
						(
							SELECT
								max(case b.EAPMathStatus when '1' then 1 else 0 end)
							FROM
								calpass.dbo.k12StarDemProd b
							WHERE
								b.derkey1 = a.derkey1
						) as math_eap_ind,
						cast(cstmathsubj as int) as math_subject,
						cast(cstmathss as int) as math_scaled_score,
						cast(case when cstMathClstr1 in ('98', '99') or cstMathClstr1 like '%*%' or cstMathClstr1 like '%[a-Z]%' then null else cstMathClstr1 end as int) as math_cluster_1,
						cast(case when cstMathClstr2 in ('98', '99') or cstMathClstr2 like '%*%' or cstMathClstr1 like '%[a-Z]%' then null else cstMathClstr2 end as int) as math_cluster_2,
						cast(case when cstMathClstr3 in ('98', '99') or cstMathClstr3 like '%*%' or cstMathClstr1 like '%[a-Z]%' then null else cstMathClstr3 end as int) as math_cluster_3,
						cast(case when cstMathClstr4 in ('98', '99') or cstMathClstr4 like '%*%' or cstMathClstr1 like '%[a-Z]%' then null else cstMathClstr4 end as int) as math_cluster_4,
						cast(case when cstMathClstr5 in ('98', '99') or cstMathClstr5 like '%*%' or cstMathClstr1 like '%[a-Z]%' then null else cstMathClstr5 end as int) as math_cluster_5,
						cast(case when cstMathClstr6 in ('98', '99') or cstMathClstr6 like '%*%' or cstMathClstr1 like '%[a-Z]%' then null else cstMathClstr6 end as int) as math_cluster_6,
						row_number() over(
							partition by
								derkey1
							order by
								case
									when substring(a.acyear, 1, 1) = '9' then '19' + substring(a.acyear, 1, 2)
									else '20' + substring(a.acyear, 1, 2)
								end desc,
								CSTMathSS desc,
								CSTMathClstr1 desc,
								CSTMathClstr2 desc,
								CSTMathClstr3 desc,
								CSTMathClstr4 desc,
								CSTMathClstr5 desc,
								CSTMathClstr6 desc
						) as row_selector
					FROM
						calpass.dbo.K12StarcstProd a
					WHERE
						exists (
							SELECT
								'Y'
							FROM
								calpass.dbo.bsteele_cohort bc
							WHERE
								bc.derkey1 = a.derkey1
						)
						and a.CSTEngSS is not null
						and a.CSTEngSS <> '   '
				) m
			WHERE
				m.row_selector = 1
		) m
		full outer join
		(
			SELECT
				derkey1,
				engl_eap_ind,
				engl_scaled_score,
				engl_cluster_1,
				engl_cluster_2,
				engl_cluster_3,
				engl_cluster_4,
				engl_cluster_5,
				engl_cluster_6
			FROM
				(
					SELECT
						derkey1,
						(
							SELECT
								max(case EAPELAStatus when '1' then 1 else 0 end)
							FROM
								calpass.dbo.k12StarDemProd b
							WHERE
								b.derkey1 = a.derkey1
						) as engl_eap_ind,
						cast(cstengss as int) as engl_scaled_score,
						cast(case when cstEngClstr1 in ('98', '99') or cstEngClstr1 like '%*%' or cstEngClstr1 like '%[a-Z]%' then null else cstEngClstr1 end as int) as engl_cluster_1,
						cast(case when cstEngClstr2 in ('98', '99') or cstEngClstr2 like '%*%' or cstEngClstr2 like '%[a-Z]%' then null else cstEngClstr2 end as int) as engl_cluster_2,
						cast(case when cstEngClstr3 in ('98', '99') or cstEngClstr3 like '%*%' or cstEngClstr3 like '%[a-Z]%' then null else cstEngClstr3 end as int) as engl_cluster_3,
						cast(case when cstEngClstr4 in ('98', '99') or cstEngClstr4 like '%*%' or cstEngClstr4 like '%[a-Z]%' then null else cstEngClstr4 end as int) as engl_cluster_4,
						cast(case when cstEngClstr5 in ('98', '99') or cstEngClstr5 like '%*%' or cstEngClstr5 like '%[a-Z]%' then null else cstEngClstr5 end as int) as engl_cluster_5,
						cast(case when cstEngClstr6 in ('98', '99') or cstEngClstr6 like '%*%' or cstEngClstr6 like '%[a-Z]%' then null else cstEngClstr6 end as int) as engl_cluster_6,
						row_number() over(
							partition by
								derkey1
							order by
								case
									when substring(a.acyear, 1, 1) = '9' then '19' + substring(a.acyear, 1, 2)
									else '20' + substring(a.acyear, 1, 2)
								end desc,
								CSTEngSS desc,
								cstEngClstr1 desc,
								cstEngClstr2 desc,
								cstEngClstr3 desc,
								cstEngClstr4 desc,
								cstEngClstr5 desc,
								cstEngClstr6 desc
						) as row_selector
					FROM
						calpass.dbo.K12StarcstProd a
					WHERE
						exists (
							SELECT
								'Y'
							FROM
								calpass.dbo.bsteele_cohort bc
							WHERE
								bc.derkey1 = a.derkey1
						)
						and a.CSTEngSS is not null
						and a.CSTEngSS <> '   '
				) e
			WHERE
				e.row_selector = 1
		) e
			on m.derkey1 = e.derkey1;
END;