USE calpass;

GO

IF (object_id('dbo.p_K12_proliferate') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_K12_proliferate;
	END;

GO

CREATE PROCEDURE
	dbo.p_K12_proliferate
AS
	
	SET NOCOUNT ON;

DECLARE
	@Message nvarchar(2048),
	@Escrow calpads.Escrow;

BEGIN
	-- Collect Students
	BEGIN TRY
		INSERT
			@Escrow
			(
				StudentStateId,
				SchoolCode,
				YearCode,
				EscrowName
			)
		EXECUTE calpads.EscrowGet;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message =
			nchar(9) + N'Error Procedure: ' + upper('calpads.EscrowGet') + char(13) + char(10) +
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		-- put error message
		RAISERROR(@message, 0, 1) WITH NOWAIT;
		RETURN;
	END CATCH;

	-- K12AwardProd
	BEGIN TRY
		-- merge Awards into production
		EXECUTE dbo.K12AwardProdMerge
			@Escrow = @Escrow;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		-- put error message
		RAISERROR(ERROR_MESSAGE(), 0, 1) WITH NOWAIT;
		RETURN;
	END CATCH;

	-- K12StudentProd
	BEGIN TRY
		-- merge students into production
		EXECUTE dbo.K12StudentProdMerge
			@Escrow = @Escrow;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		-- put error message
		RAISERROR(ERROR_MESSAGE(), 0, 1) WITH NOWAIT;
		RETURN;
	END CATCH;
	
/* 		-- COURSE
		EXECUTE dbo.p_K12CourseProd_process;
		-- PROGRAM
		EXECUTE dbo.p_K12PRGProd_process; */
END;