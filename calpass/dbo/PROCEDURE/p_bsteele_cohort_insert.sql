USE calpass;

GO

IF (object_id('calpass.dbo.p_bsteele_cohort_insert', 'P') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_bsteele_cohort_insert;
	END;

GO

CREATE PROCEDURE
	dbo.p_bsteele_cohort_insert
AS

	SET NOCOUNT ON;

BEGIN

	TRUNCATE TABLE calpass.dbo.bsteele_cohort;

	INSERT INTO
		dbo.bsteele_cohort
		(
			derkey1,
			grade_level_code,
			year_code
		)
	SELECT DISTINCT
		calpass.dbo.derkey1(
			s.fname,
			s.lname,
			s.gender,
			s.birthdate
		) as derkey1,
		s.gradeLevel,
		s.AcYear
	FROM
		(
			VALUES
			-- Shasta College
			('171','45701364532750'),
			('171','45701364530044'),
			('171','45701364537304'),
			('171','45701364537304'),
			('171','45698564530028'),
			-- Santa Barbara City College
			('651','42691464230587'),
			('651','42767864231726'),
			('651','42767864235230'),
			('651','42767864235727'),
			-- Mt. San Jacinto College
			('941','33672493337656'),
			('941','33672493331006'),
			('941','33751923330743'),
			('941','33752000118794'),
			('941','33752000102079'),
			('941','33752000100420'),
			('941','33752003330529'),
			('941','33751763332350'),
			('941','33751763330487'),
			('941','33751923330370'),
			('941','33751763330115'),
			('941','33751760107920'),
			('941','33751760116343'),
			('941','33744923390028')
		) t (college_id, cds_code)
		inner join
		comis.dbo.district_college dc
			on dc.code = t.college_id
		cross join
		(
			VALUES
			('0809','2008-2009'),
			('0910','2009-2010'),
			('1011','2010-2011'),
			('1112','2011-2012'),
			('1213','2012-2013'),
			('1314','2013-2014'),
			('1415','2014-2015')
		) y (year_code, year_description)
		cross join
		(
			VALUES
			('12')
		) g (grade_level_code)
		inner join
		calpass.dbo.k12studentprod s
			on s.School = t.cds_code
			and s.AcYear = y.year_code
			and s.gradeLevel = g.grade_level_code
	WHERE
		exists (
			SELECT
				'Y'
			FROM
				calpass.dbo.k12courseprod c
			WHERE
				c.school = s.school
				and c.LocStudentId = s.LocStudentId
				and c.AcYear = s.AcYear
		)
		/* remove collisions: derkey1 values with 2+ corresponding encrypted SSIDs */
		and not exists (
			SELECT
				'N'
			FROM
				calpass.dbo.k12studentprod s1
			GROUP BY
				s1.derkey1
			HAVING
				count(distinct case when s1.csisnum = 'XXXXXXXXXX' then null else s1.csisnum end) > 1
				and s1.derkey1 = s.derkey1
		)
		/* remove collisions: derkey1 values with 2+ corresponding encrypted socials */
		and not exists (
			SELECT
				s2.derkey1
			FROM
				calpass.dbo.ccstudentprod s2
					with(index(idx_ccstud_derkey))
			GROUP BY
				s2.derkey1
			HAVING
				count(distinct case when s2.IdStatus = 'C' then null else s2.studentId end) > 1
				and s2.derkey1 = s.derkey1
		);

	-- remove CALPADS muckups 
	DELETE
		c
	FROM
		bsteele_cohort c
		left outer join
		(
			SELECT
				*
			FROM
				bsteele_cohort a
			WHERE
				a.year_code = (
					SELECT
						min(b.year_code)
					FROM
						bsteele_cohort b
					WHERE
						a.derkey1 = b.derkey1
				)
		) d
			on c.derkey1 = d.derkey1
			and c.year_code = d.year_code
			and c.grade_level_code = d.grade_level_code
	WHERE
		d.derkey1 is null;

END;