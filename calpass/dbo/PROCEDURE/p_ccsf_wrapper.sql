USE calpass;

GO

IF (object_id('calpass.dbo.p_ccsf_wrapper', 'P') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_ccsf_wrapper;
	END;

GO

CREATE PROCEDURE
	dbo.p_ccsf_wrapper
	(
		@user nvarchar(255),
		@password nvarchar(255),
		@server nvarchar(255),
		@ssh_rsa nvarchar(255),
		@port nvarchar(255)
	)
AS

	SET NOCOUNT ON;

BEGIN
	-- populate cohort
	EXECUTE calpass.dbo.p_ccsf_cohort_process;
	-- process cohort
	EXECUTE calpass.dbo.p_ccsf_output_process;

	-- extract
	EXECUTE xp_cmdshell 'bcp "SELECT * FROM calpass.dbo.ccsf_output" QUERYOUT "C:\Users\dlamoree\Desktop\ccsf_output.txt" -T -c -q';

	-- upload
	EXECUTE sp_sftp_winscp_upload
		@user = @user,
		@password = @password,
		@server = @server,
		@port = @port,
		@ssh_rsa = @ssh_rsa,
		@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
		@source_file_name = 'ccsf_output.txt',
		@target_file_dir = '/home/jhetts/'

	-- delete
	EXECUTE master.dbo.clr_file_delete
		@file_path = 'C:\Users\dlamoree\desktop\ccsf_output.txt';

	-- extract
	EXECUTE dbo.sp_spss_output
		'calpass',
		'dbo',
		'ccsf_output',
		'C:\Users\dlamoree\desktop\',  -- '
		'1';

	-- upload
	EXECUTE sp_sftp_winscp_upload
		@user = @user,
		@password = @password,
		@server = @server,
		@port = @port,
		@ssh_rsa = @ssh_rsa,
		@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
		@source_file_name = 'ccsf_output_spss_import.sps',
		@target_file_dir = '/home/jhetts/'

	-- delete
	EXECUTE master.dbo.clr_file_delete
		@file_path = 'C:\Users\dlamoree\desktop\ccsf_output_spss_import.sps';

END;