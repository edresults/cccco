ALTER PROCEDURE
    OutreachDailyAudit
AS

    /*
        author: dlamoree, kdavis
          desc: K12 SWP Grantees list
         notes: Another Day In Paradise
          tags: Cal-PASS Plus, K12, SWP
     changelog:
                1.03 (2020-01-26): dlamoree
                    Removed kdavis code, replaced with County <-> Microregion association
                1.02 (2020-01-25): dlamoree
                    Removed all constraints and validations for PSTS until CALPADS fixes bugs
                1.01 (2020-01-22): kdavis
                    Integrated county and regions to summary report
                1.00 (Lost to time and space): dlamoree
                    Implemented
    */

    SET NOCOUNT ON;

DECLARE
    -- Constaints
    @DateInjection  char(14)       = REPLACE(REPLACE(REPLACE(CONVERT(CHAR(19), GETDATE(), 120), ' ', ''), '-', ''), ':', ''),
    -- Mail
    @Message        nvarchar(2048) = N'',
    @ProfileName    sysname        = 'Cal-PASS SQL Support Profile',
    @Recipients     varchar(max)   = 'dlamoree@edresults.org;kdavis@edresults.org;outreach@edresults.org',
    -- @Recipients     varchar(max)   = 'dlamoree@edresults.org',
    @BCC            varchar(max)   = 'qdorsey@wested.org',
    @Subject        nvarchar(255)  = N'',
    @Query          nvarchar(max)  = N'',
    @QueryHeader    bit            = 0,
    @QueryNoPadding bit            = 1,
    @QueryAttach    bit            = 1,
    @QueryFileName  nvarchar(255)  = N'',
    @QuerySeparator char(1)        = '	',
    @QueryWidth     smallint       = 8000,
    @ExcludeInline  bit            = 1;

BEGIN

    DROP TABLE IF EXISTS #grantee;
    DROP TABLE IF EXISTS #calpads;
    DROP TABLE IF EXISTS #data;
    -- DROP TABLE IF EXISTS outreach_daily_output;

    -- DDL

    CREATE TABLE
        #grantee
        (
            org_code   char(14)       not null,
            org_name   varchar(90)    not null,
            is_charter bit            not null,
            CONSTRAINT pk_#grantee PRIMARY KEY CLUSTERED ( org_code )
        );

    CREATE TABLE
        #calpads
        (
            file_name     char(4) not null,
            academic_year char(9) not null,
            CONSTRAINT pk_#calpads PRIMARY KEY CLUSTERED ( file_name, academic_year )
        );

    CREATE TABLE
        #data
        (
            org_name      varchar(90) not null,
            org_code      char(14)    not null,
            academic_year char(9)     not null,
            file_name     char(4)     not null,
            records       int         not null,
            CONSTRAINT pk_#data PRIMARY KEY CLUSTERED ( org_code, academic_year, file_name )
        );

    -- CREATE TABLE
    --     outreach_daily_output
    --     (
    --         county        varchar(30) not null,
    --         macro         varchar(35) not null,
    --         micro         varchar(35) not null,
    --         org_name      varchar(90) not null,
    --         org_code      char(14)    not null,
    --         academic_year char(9)     not null,
    --         file_name     char(4)     not null,
    --         records       int             null,
    --         is_charter    bit             null,
    --         CONSTRAINT pk_outreach_daily_output PRIMARY KEY CLUSTERED ( org_code, academic_year, file_name )
    --     );

    -- DML

    INSERT INTO
        #calpads
        (
            file_name,
            academic_year
        )
    VALUES
        ('CRSC', '2015-2016'),
        ('CRSC', '2016-2017'),
        ('CRSC', '2017-2018'),
        ('CRSC', '2018-2019'),
        ('CRSC', '2019-2020'),
        ('PSTS', '2018-2019'),
        ('PSTS', '2019-2020'),
        ('SCSC', '2015-2016'),
        ('SCSC', '2016-2017'),
        ('SCSC', '2017-2018'),
        ('SCSC', '2018-2019'),
        ('SCSC', '2019-2020'),
        ('SCTE', '2015-2016'),
        ('SCTE', '2016-2017'),
        ('SCTE', '2017-2018'),
        ('SCTE', '2018-2019'),
        ('SCTE', '2019-2020'),
        ('SDIS', '2015-2016'),
        ('SDIS', '2016-2017'),
        ('SDIS', '2017-2018'),
        ('SDIS', '2018-2019'),
        ('SELA', '2015-2016'),
        ('SELA', '2016-2017'),
        ('SELA', '2017-2018'),
        ('SELA', '2018-2019'),
        ('SELA', '2019-2020'),
        ('SENR', '2015-2016'),
        ('SENR', '2016-2017'),
        ('SENR', '2017-2018'),
        ('SENR', '2018-2019'),
        ('SENR', '2019-2020'),
        ('SINC', '2019-2020'),
        ('SINF', '2015-2016'),
        ('SINF', '2016-2017'),
        ('SINF', '2017-2018'),
        ('SINF', '2018-2019'),
        ('SINF', '2019-2020'),
        ('SIRS', '2019-2020'),
        ('SOFF', '2019-2020'),
        ('SPED', '2018-2019'),
        ('SPED', '2019-2020'),
        ('SPRG', '2015-2016'),
        ('SPRG', '2016-2017'),
        ('SPRG', '2017-2018'),
        ('SPRG', '2018-2019'),
        ('SPRG', '2019-2020'),
        ('STAS', '2015-2016'),
        ('STAS', '2016-2017'),
        ('STAS', '2017-2018'),
        ('STAS', '2018-2019'),
        ('STAS', '2019-2020');

    INSERT INTO
        #grantee
        (
            org_code,
            org_name,
            is_charter
        )
    SELECT DISTINCT
        case charter
            when 'Y' then replicate(right(g.cds_code, 7), 2)
            else g.cds_code
        end,
        coalesce(i.school, i.district),
        case charter
            when 'Y' then 1
            else 0
        end
    FROM
        comis.swp_grantee g
        inner join
        calpads.Institution i on
            i.CdsCode = g.cds_code;

    INSERT INTO
        #data
        (
            org_code,
            org_name,
            academic_year,
            file_name,
            records
        )
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.crsc b on a.org_code = b.DistrictCode + b.SchoolCode         inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 1 and c.file_name = 'CRSC' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.scsc b on a.org_code = b.DistrictCode + b.SchoolCode         inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 1 and c.file_name = 'SCSC' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.scte b on a.org_code = b.DistrictCode + b.SchoolCode         inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 1 and c.file_name = 'SCTE' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.sdis b on a.org_code = b.DistrictCode + b.SchoolCode         inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 1 and c.file_name = 'SDIS' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.sela b on a.org_code = b.DistrictCode + b.SchoolCode         inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 1 and c.file_name = 'SELA' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.senr b on a.org_code = b.DistrictCode + b.SchoolCode         inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 1 and c.file_name = 'SENR' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.sinf b on a.org_code = b.DistrictCode + b.SchoolCode         inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 1 and c.file_name = 'SINF' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.sprg b on a.org_code = b.DistrictCode + b.SchoolCode         inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 1 and c.file_name = 'SPRG' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.stas b on a.org_code = b.DistrictCode + b.SchoolCode         inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 1 and c.file_name = 'STAS' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.AcademicYearID as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.psts b on a.org_code = b.ReportingLEA + b.SchoolofAttendance inner join #calpads c on c.academic_year = b.AcademicYearID WHERE a.is_charter = 1 and c.file_name = 'PSTS' GROUP BY a.is_charter, a.org_code, b.AcademicYearID, a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.AcademicYearID as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.sinc b on a.org_code = b.ReportingLEA + b.SchoolofAttendance inner join #calpads c on c.academic_year = b.AcademicYearID WHERE a.is_charter = 1 and c.file_name = 'SINC' GROUP BY a.is_charter, a.org_code, b.AcademicYearID, a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.AcademicYearID as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.sirs b on a.org_code = b.ReportingLEA + b.SchoolofAttendance inner join #calpads c on c.academic_year = b.AcademicYearID WHERE a.is_charter = 1 and c.file_name = 'SIRS' GROUP BY a.is_charter, a.org_code, b.AcademicYearID, a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.AcademicYearID as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.soff b on a.org_code = b.ReportingLEA + b.SchoolofAttendance inner join #calpads c on c.academic_year = b.AcademicYearID WHERE a.is_charter = 1 and c.file_name = 'SOFF' GROUP BY a.is_charter, a.org_code, b.AcademicYearID, a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.AcademicYearID as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.sped b on a.org_code = b.ReportingLEA + b.SchoolofAttendance inner join #calpads c on c.academic_year = b.AcademicYearID WHERE a.is_charter = 1 and c.file_name = 'SPED' GROUP BY a.is_charter, a.org_code, b.AcademicYearID, a.org_name, c.file_name;

    INSERT INTO
        #data
        (
            org_code,
            org_name,
            academic_year,
            file_name,
            records
        )
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.crsc b on left(a.org_code, 7) = b.DistrictCode inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 0 and c.file_name = 'CRSC' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.scsc b on left(a.org_code, 7) = b.DistrictCode inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 0 and c.file_name = 'SCSC' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.scte b on left(a.org_code, 7) = b.DistrictCode inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 0 and c.file_name = 'SCTE' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.sdis b on left(a.org_code, 7) = b.DistrictCode inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 0 and c.file_name = 'SDIS' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.sela b on left(a.org_code, 7) = b.DistrictCode inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 0 and c.file_name = 'SELA' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.senr b on left(a.org_code, 7) = b.DistrictCode inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 0 and c.file_name = 'SENR' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.sinf b on left(a.org_code, 7) = b.DistrictCode inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 0 and c.file_name = 'SINF' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.sprg b on left(a.org_code, 7) = b.DistrictCode inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 0 and c.file_name = 'SPRG' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.YearCode       as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.stas b on left(a.org_code, 7) = b.DistrictCode inner join #calpads c on c.academic_year = b.YearCode       WHERE a.is_charter = 0 and c.file_name = 'STAS' GROUP BY a.is_charter, a.org_code, b.YearCode,       a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.AcademicYearID as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.psts b on left(a.org_code, 7) = b.ReportingLEA inner join #calpads c on c.academic_year = b.AcademicYearID WHERE a.is_charter = 0 and c.file_name = 'PSTS' GROUP BY a.is_charter, a.org_code, b.AcademicYearID, a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.AcademicYearID as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.sinc b on left(a.org_code, 7) = b.ReportingLEA inner join #calpads c on c.academic_year = b.AcademicYearID WHERE a.is_charter = 0 and c.file_name = 'SINC' GROUP BY a.is_charter, a.org_code, b.AcademicYearID, a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.AcademicYearID as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.sirs b on left(a.org_code, 7) = b.ReportingLEA inner join #calpads c on c.academic_year = b.AcademicYearID WHERE a.is_charter = 0 and c.file_name = 'SIRS' GROUP BY a.is_charter, a.org_code, b.AcademicYearID, a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.AcademicYearID as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.soff b on left(a.org_code, 7) = b.ReportingLEA inner join #calpads c on c.academic_year = b.AcademicYearID WHERE a.is_charter = 0 and c.file_name = 'SOFF' GROUP BY a.is_charter, a.org_code, b.AcademicYearID, a.org_name, c.file_name UNION ALL
    SELECT a.org_code, a.org_name, b.AcademicYearID as year_code, c.file_name, count(*) as Records FROM #grantee a inner join calpads.sped b on left(a.org_code, 7) = b.ReportingLEA inner join #calpads c on c.academic_year = b.AcademicYearID WHERE a.is_charter = 0 and c.file_name = 'SPED' GROUP BY a.is_charter, a.org_code, b.AcademicYearID, a.org_name, c.file_name;

    MERGE
        outreach_daily_output t
    USING
        (
            SELECT
                c.Label as county,
                ma.Label as macro,
                mi.Label as micro,
                g.org_name,
                g.org_code,
                g.is_charter,
                f.academic_year,
                f.file_name,
                d.records
            FROM
                #grantee g
                inner join
                comis.counties c on
                    c.code = left(g.org_code, 2)
                inner join
                comis.Microregions mi on
                    mi.Id = c.MicroregionId
                inner join
                comis.Macroregions ma on
                    ma.Id = mi.MacroregionId
                cross join
                #calpads f
                left outer join
                #data d on
                    d.org_code      = g.org_code and
                    d.academic_year = f.academic_year and
                    d.file_name     = f.file_name
        ) s
    ON
        (
            s.org_code      = t.org_code and
            s.academic_year = t.academic_year and
            s.file_name     = t.file_name
        )
    WHEN NOT MATCHED BY TARGET THEN
        INSERT
            (
                county,
                macro,
                micro,
                org_name,
                org_code,
                is_charter,
                academic_year,
                file_name,
                records
            )
        VALUES
            (
                s.county,
                s.macro,
                s.micro,
                s.org_name,
                s.org_code,
                s.is_charter,
                s.academic_year,
                s.file_name,
                s.records
            )
    WHEN MATCHED THEN
        UPDATE SET
            t.org_name   = s.org_name,
            t.is_charter = s.is_charter,
            t.records    = s.records;

    SET @Subject       = N'CALPADS :: FILES';
    SET @QueryFileName = N'CALPADS_FILES_' + @DateInjection + '.txt';
    SET @Query         = N'
        SET NOCOUNT ON;

        SELECT
            ''County''  as "County",
            ''Macro''   as "Macro",
            ''Micro''   as "Micro",
            ''Name''    as "Name",
            ''Code''    as "Code",
            ''Year''    as "Year",
            ''Charter'' as "Charter",
            ''CRSC''    as "CRSC",
            ''PSTS''    as "PSTS",
            ''SCSC''    as "SCSC",
            ''SCTE''    as "SCTE",
            ''SDIS''    as "SDIS",
            ''SELA''    as "SELA",
            ''SENR''    as "SENR",
            ''SINC''    as "SINC",
            ''SINF''    as "SINF",
            ''SIRS''    as "SIRS",
            ''SOFF''    as "SOFF",
            ''SPED''    as "SPED",
            ''SPRG''    as "SPRG",
            ''STAS''    as "STAS"
        UNION ALL
        SELECT
            County,
            Macro,
            Micro,
            Name,
            Code,
            Year,
            Charter,
            CRSC,
            PSTS,
            SCSC,
            SCTE,
            SDIS,
            SELA,
            SENR,
            SINC,
            SINF,
            SIRS,
            SOFF,
            SPED,
            SPRG,
            STAS
        FROM
            calpass.dbo.outreach_calpads_audit
    ';

    EXEC msdb.dbo.sp_send_dbmail
        @profile_name                = @ProfileName,
        @recipients                  = @Recipients,
        @subject                     = @Subject,
        @body                        = @Message,
        @query                       = @Query,
        @query_result_no_padding     = @QueryNoPadding,
        @query_result_header         = @QueryHeader,
        @attach_query_result_as_file = @QueryAttach,
        @query_attachment_filename   = @QueryFileName,
        @query_result_separator      = @QuerySeparator,
        @exclude_query_output        = @ExcludeInline;
END;