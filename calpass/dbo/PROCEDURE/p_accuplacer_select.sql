USE calpass;

GO

IF (object_id('dbo.p_accuplacer_select') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_accuplacer_select;
	END;

GO

CREATE PROCEDURE
	dbo.p_accuplacer_select
AS
	
BEGIN

	SELECT
		Derkey1 = rtrim(Derkey1),
		TestSessionId = rtrim(TestSessionId),
		StudentId = rtrim(StudentId),
		FName = rtrim(FName),
		LName = rtrim(LName),
		MName = rtrim(MName),
		City = rtrim(City),
		ZipCode = rtrim(ZipCode),
		Gender = rtrim(Gender),
		Ethnicity = rtrim(Ethnicity),
		Birthdate = rtrim(Birthdate),
		InstitutionId = rtrim(InstitutionId),
		BranchingProfileName = rtrim(BranchingProfileName),
		TestSessionDate = rtrim(TestSessionDate),
		CompletionDate = rtrim(CompletionDate),
		FederalFinancialAid = rtrim(FederalFinancialAid),
		FathersEducation = rtrim(FathersEducation),
		YearsEnglish = rtrim(YearsEnglish),
		LastMathCourse = rtrim(LastMathCourse),
		EnglishFirstLanguage = rtrim(EnglishFirstLanguage),
		FirstLanguageSpoken = rtrim(FirstLanguageSpoken),
		StudiedAlgebra = rtrim(StudiedAlgebra),
		MothersEducation = rtrim(MothersEducation),
		HighSchoolGraduate = rtrim(HighSchoolGraduate),
		Major = rtrim(Major),
		YearsMathematics = rtrim(YearsMathematics),
		Elementary_Algebra = rtrim(Elementary_Algebra),
		ESLLanguageUse = rtrim(ESLLanguageUse),
		SentenceSkills = rtrim(SentenceSkills),
		CollegeLevelMath = rtrim(CollegeLevelMath),
		Arithmetic = rtrim(Arithmetic),
		DGNArithmetic = rtrim(DGNArithmetic),
		DGNSentenceSkills = rtrim(DGNSentenceSkills),
		DGNElementaryAlgebra = rtrim(DGNElementaryAlgebra),
		DGNReadingComprehension = rtrim(DGNReadingComprehension),
		ReadingComprehension = rtrim(ReadingComprehension),
		ESLReadingSkills = rtrim(ESLReadingSkills),
		ESLSentence_Meaning = rtrim(ESLSentence_Meaning),
		ESLListening = rtrim(ESLListening),
		CSP_XP_2003 = rtrim(CSP_XP_2003),
		CSP_Vista_2007 = rtrim(CSP_Vista_2007),
		CSP_Basic_Vista_2007 = rtrim(CSP_Basic_Vista_2007),
		CSP_Basic_XP_2003 = rtrim(CSP_Basic_XP_2003),
		CSP_Win7_2010 = rtrim(CSP_Win7_2010),
		CSP_Basic_Win7_2010 = rtrim(CSP_Basic_Win7_2010),
		Absolute_Responsibility = rtrim(Absolute_Responsibility),
		Acquisition_of_Money = rtrim(Acquisition_of_Money),
		Decisions = rtrim(Decisions),
		Differences_Among_People = rtrim(Differences_Among_People),
		DuBois_Work = rtrim(DuBois_Work),
		Friends = rtrim(Friends),
		Happiness_Not_an_Accident = rtrim(Happiness_Not_an_Accident),
		Independent_Ideas = rtrim(Independent_Ideas),
		Is_History_Valuable = rtrim(Is_History_Valuable),
		Mistakes = rtrim(Mistakes),
		Necessary_to_Make_Mistakes = rtrim(Necessary_to_Make_Mistakes),
		Practical_Skills = rtrim(Practical_Skills),
		Results_of_Deception = rtrim(Results_of_Deception),
		Something_Special = rtrim(Something_Special),
		Strong_Influence = rtrim(Strong_Influence),
		Success_Technological_Progress = rtrim(Success_Technological_Progress),
		Unlimited_Change = rtrim(Unlimited_Change),
		CollegeId = rtrim(CollegeId),
		college_id = rtrim(college_id),
		cccco_assigned_student_id = rtrim(cccco_assigned_student_id)
	FROM
		calpass.dbo.AccuplacerHistorical
END;