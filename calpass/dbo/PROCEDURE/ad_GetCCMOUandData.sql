ALTER PROCEDURE
	dbo.ad_GetCCMOUandData
	(
		@District Int
	)
AS

BEGIN
/*
select  m.districtname, m.districtorganizationid, so.academicyear, max(mouexpireddate) as MOUExpire, sum(students) as Students, sum(courses) as courses, sum(awards) as awards   
from ad_memberlist as m  
left outer join [calpass].[dbo].[ad_DistrictMOU] as so on m.districtorganizationid = so.districtorganizationid   
left outer join calpass.dbo.org on org.organizationid = m.districtorganizationid  
left outer join ad_CCStudentCount as STD on org.OrganizationCode = std.collegeid and so.Academicyear = std.academicyear  
left outer join ad_CCCourseCount as COU on org.OrganizationCode = cou.collegeid and so.Academicyear=cou.academicyear  
left outer join ad_CCAwardCount as AWA on org.OrganizationCode = awa.collegeid and so.Academicyear=awa.academicyear  
where m.districtorganizationid = @District and (students is not null or courses is not null or awards is not null) and substring(so.academicyear,1,4) >= 2004  
group by m.districtname, m.districtorganizationid, so.Academicyear  
order by districtname, academicyear  

select  m.districtname, m.districtorganizationid, std.academicyear, max(expirationdate) as MOUExpire, sum(students) as Students, sum(courses) as courses, sum(awards) as awards   
from ad_memberlist as m  
left outer join calpass.dbo.org on org.organizationid = m.districtorganizationid  
left outer join calpass.dbo.CPP_Mou as so on ORG.OrganizationId = so.organizationId  
left outer join ad_CCStudentCount as STD on org.OrganizationCode = std.collegeid   
left outer join ad_CCCourseCount as COU on org.OrganizationCode = cou.collegeid and STD.Academicyear=cou.academicyear  
left outer join ad_CCAwardCount as AWA on org.OrganizationCode = awa.collegeid and STD.Academicyear=awa.academicyear  
where m.districtorganizationid = @District and (students is not null or courses is not null or awards is not null) and substring(STD.academicyear,1,4) >= 2006  
group by m.districtname, m.districtorganizationid, STD.Academicyear  
order by districtname, academicyear
*/

	SELECT
		DistrictName,
		DistrictOrganizationId,
		AcademicYear,
		MOUExpire,
		Students,
		Courses,
		Awards
	FROM
		(
			SELECT TOP 10
				DistrictName           = m.DistrictName,
				DistrictOrganizationId = m.DistrictOrganizationId,
				AcademicYear           = s.AcademicYear,
				MOUExpire              = max(e.ExpirationDate),
				Students               = sum(s.Students),
				Courses                = sum(c.Courses),
				Awards                 = sum(a.Awards)
			FROM
				ad_memberlist m
				inner join
				calpass.dbo.Org o
					on o.OrganizationId = m.DistrictOrganizationId
				left outer join
				calpass.dbo.CPP_Mou e
					on  o.OrganizationId = e.OrganizationId
                    and e.deleted is null
				left outer join
				ad_CCStudentCount s
					on o.OrganizationCode = s.CollegeId
				left outer join
				ad_CCCourseCount c
					on o.OrganizationCode = c.CollegeId
					and s.Academicyear = c.AcademicYear
				left outer join
				ad_CCAwardCount a
					on o.OrganizationCode = a.CollegeId
					and s.Academicyear = a.academicyear
			WHERE
				m.DistrictOrganizationId = @District
				and (
					s.Students is not null
					or
					c.Courses is not null
					or
					a.Awards is not null
				)
			GROUP BY
				m.DistrictName,
				m.DistrictOrganizationId,
				s.Academicyear
			ORDER BY
				m.DistrictName,
				s.AcademicYear DESC
		) a
	ORDER BY
		a.AcademicYear ASC

END;