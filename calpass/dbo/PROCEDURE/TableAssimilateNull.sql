USE calpass;

GO

IF (object_id('dbo.TableAssimilateNull') is not null)
	BEGIN
		DROP PROCEDURE dbo.TableAssimilateNull;
	END;

GO

CREATE PROCEDURE
	dbo.TableAssimilateNull
	(
		@ProductionName nvarchar(517),
		@StageName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Message nvarchar(2048) = N'',
	@NullConstraintWhere nvarchar(max) = N'',
	@RowCount int,
	@Count int,
	@Sql nvarchar(max);

BEGIN

	SELECT
		@Count = count(*)
	FROM
		sys.columns c
		inner join
		sys.columns sc
			on sc.name = c.name
	WHERE
		c.object_id = object_id(@ProductionName)
		and sc.object_id = object_id(@StageName)
		and c.is_nullable = 0
		and c.is_identity = 0;

	IF (@Count = 0)
		BEGIN
			SET @Message = 'Table has no column constraints of not null';
			THROW 70099, @Message, 1;
		END;
		
	SELECT
		@NullConstraintWhere +=
			case
				when c.column_id = min(c.column_id) over() then N''
				else N' or '
			end
			+ c.Name + N' is null'
	FROM
		sys.columns c
		inner join
		sys.columns sc
			on sc.name = c.name
	WHERE
		c.object_id = object_id(@ProductionName)
		and sc.object_id = object_id(@StageName)
		and c.is_nullable = 0
		and c.is_identity = 0
	ORDER BY
		c.column_id;

	SET @Sql = N'DELETE FROM ' + @StageName + N' WHERE ' + @NullConstraintWhere + N'; SET @RowCount = @@ROWCOUNT;';
	-- delete null values
	EXECUTE sp_executesql
		@Sql,
		N'@RowCount int out',
		@RowCount = @RowCount out;
	-- update message
	SET @Message = convert(nvarchar, @RowCount) + N' Stage Records deleted as Null Constraint';
	-- output message
	PRINT @Message;
END;