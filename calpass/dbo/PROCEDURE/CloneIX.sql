USE calpass;

GO

IF (object_id('dbo.CloneIX') is not null)
	BEGIN
		DROP PROCEDURE dbo.CloneIX;
	END;

GO

CREATE PROCEDURE
	dbo.CloneIX
	(
		@QualifiedFullName nvarchar(776),
		@Command nvarchar(max) out
	)
AS

	SET NOCOUNT ON;

DECLARE
	-- constaints
	@False bit = 0,
	@True bit = 1,
	@DatabasePiece int = 3,
	@SchemaPiece int = 2,
	@TablePiece int = 1,
	@ValidationError nvarchar(2048) = ' Not Found',
	-- variables
	@DatabaseName sysname,
	@SchemaName sysname,
	@TableName sysname,
	@Sql nvarchar(max) = N'',
	@Message nvarchar(2048);

BEGIN
	-- set all object names
	SELECT
		@DatabaseName = isnull(parsename(@QualifiedFullName, @DatabasePiece), db_name(db_id())), 
		@SchemaName = isnull(parsename(@QualifiedFullName, @SchemaPiece), schema_name(schema_id())),
		@TableName = parsename(@QualifiedFullName, @TablePiece);

	-- validate database
	IF (@DatabaseName is not null
		and 0 = (SELECT count(*) FROM sys.databases WHERE name = @DatabaseName))
		BEGIN
			SET @Message = @DatabaseName + @ValidationError;
			THROW 70099, @Message, 1;
		END;

	-- validate schema
	IF (@SchemaName is not null
		and 0 = (SELECT count(*) FROM sys.schemas WHERE name = @SchemaName))
		BEGIN
			SET @Message = @SchemaName + @ValidationError;
			THROW 70099, @Message, 1;
		END;

	-- validate table
	IF (0 = (SELECT count(*) FROM sys.tables WHERE name = @TableName))
		BEGIN
			SET @Message = @TableName + @ValidationError;
			THROW 70099, @Message, 1;
		END;

	-- initialize output variable
	SET @Command = N'';

	-- set dynamic sql
	SET @Sql = '
		SELECT
			@Command += 
				case
					when ic.index_column_id = min(ic.index_column_id) over(partition by i.index_id) then
						case
							when i.is_primary_key = @False then 
								N''CREATE '' + 
								convert(nvarchar(max), i.type_desc) collate SQL_Latin1_General_CP1_CI_AS + 
								N'' INDEX '' + quotename(i.name) + N'' ON '' + 
								quotename(@DatabaseName) + nchar(46) + 
								quotename(s.name) + nchar(46) + 
								quotename(t.name) + 
								N'' (''
							when i.is_primary_key = @True then
								N''ALTER TABLE '' +
								quotename(@DatabaseName) + nchar(46) + 
								quotename(s.name) + nchar(46) + 
								quotename(t.name) + 
								N'' ADD CONSTRAINT '' + 
								quotename(i.name) + 
								N'' PRIMARY KEY '' + convert(nvarchar(max), i.type_desc) collate SQL_Latin1_General_CP1_CI_AS +
								N'' (''
							else N''''
						end
					else N''''
				end + 
				case
					when ic.is_included_column = 1 and lag(ic.is_included_column, 1, null) over(partition by ic.index_id order by ic.index_column_id) = 0 then '') INCLUDE (''
					else N''''
				end + 
				c.name + 
				case
					when ic.is_included_column = @False and is_descending_key = @True then N'' desc''
					when ic.is_included_column = @False and is_descending_key = @False then N'' asc''
					else N''''
				end + 
				case
					when ic.is_included_column = 0 and lead(ic.is_included_column, 1, null) over(partition by ic.index_id order by ic.index_column_id) = 1 then N''''
					when ic.index_column_id = max(ic.index_column_id) over(partition by i.index_id) then N'');''
					else N'',''
				end
		FROM
			' + @DatabaseName + N'.sys.schemas s
			inner join
			' + @DatabaseName + N'.sys.tables t
				on s.schema_id = t.schema_id
			inner join
			' + @DatabaseName + N'.sys.columns c
				on t.object_id = c.object_id
			inner join
			' + @DatabaseName + N'.sys.indexes i
				on i.object_id = t.object_id
			inner join
			' + @DatabaseName + N'.sys.index_columns ic
				on i.object_id = ic.object_id
				and i.index_id = ic.index_id
				and ic.column_id = c.column_id
		WHERE
			s.name = @SchemaName
			and t.name = @TableName';

	-- execute dynamic sql
	EXECUTE sp_executesql
		@Sql,
		N'@False bit,
			@True bit,
			@DatabaseName sysname,
			@SchemaName sysname,
			@TableName sysname,
			@Command nvarchar(max) out',
		@False = @False,
		@True = @True,
		@DatabaseName = @DatabaseName,
		@SchemaName = @SchemaName,
		@TableName = @TableName,
		@Command = @Command out;
END;