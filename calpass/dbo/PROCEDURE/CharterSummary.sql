USE calpass;

GO

IF (object_id('dbo.CharterSummary') is not null)
	BEGIN
		DROP PROCEDURE dbo.CharterSummary;
	END;

GO

CREATE PROCEDURE
	dbo.CharterSummary
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

BEGIN
	SELECT
		ObjectName    = 'K12AwardProd',
		CdsCodeStatus = 'Valid',
		DistrictName  = cd.Label,
		AcademicYear  = s.AcYear,
		Records       = count(*)
	FROM
		dbo.K12AwardProd s
		inner join
		dbo.CharterSchool cs
			on cs.CdsCode = s.School
		inner join
		dbo.CharterDistrict cd
			on cd.CharterDistrictId = cs.CharterDistrictId
	GROUP BY
		cd.Label,
		s.AcYear
	UNION ALL
	SELECT
		ObjectName    = 'K12AwardProd',
		CdsCodeStatus = 'Invalid',
		DistrictName  = cd.Label,
		AcademicYear  = s.AcYear,
		Records       = count(*)
	FROM
		dbo.K12AwardProd s
		inner join
		dbo.CharterSchool cs
			on right(cs.CdsCode, 7) + right(cs.CdsCode, 7) = s.School
		inner join
		dbo.CharterDistrict cd
			on cd.CharterDistrictId = cs.CharterDistrictId
	GROUP BY
		cd.Label,
		s.AcYear
	UNION ALL
	SELECT
		ObjectName    = 'K12StudentProd',
		CdsCodeStatus = 'Valid',
		DistrictName  = cd.Label,
		AcademicYear  = s.AcYear,
		Records       = count(*)
	FROM
		dbo.K12StudentProd s
		inner join
		dbo.CharterSchool cs
			on cs.CdsCode = s.School
		inner join
		dbo.CharterDistrict cd
			on cd.CharterDistrictId = cs.CharterDistrictId
	GROUP BY
		cd.Label,
		s.AcYear
	UNION ALL
	SELECT
		ObjectName    = 'K12StudentProd',
		CdsCodeStatus = 'Invalid',
		DistrictName  = cd.Label,
		AcademicYear  = s.AcYear,
		Records       = count(*)
	FROM
		dbo.K12StudentProd s
		inner join
		dbo.CharterSchool cs
			on right(cs.CdsCode, 7) + right(cs.CdsCode, 7) = s.School
		inner join
		dbo.CharterDistrict cd
			on cd.CharterDistrictId = cs.CharterDistrictId
	GROUP BY
		cd.Label,
		s.AcYear
	UNION ALL
	SELECT
		ObjectName    = 'K12CourseProd',
		CdsCodeStatus = 'Valid',
		DistrictName  = cd.Label,
		AcademicYear  = s.AcYear,
		Records       = count(*)
	FROM
		dbo.K12CourseProd s
		inner join
		dbo.CharterSchool cs
			on cs.CdsCode = s.School
		inner join
		dbo.CharterDistrict cd
			on cd.CharterDistrictId = cs.CharterDistrictId
	GROUP BY
		cd.Label,
		s.AcYear
	UNION ALL
	SELECT
		ObjectName    = 'K12CourseProd',
		CdsCodeStatus = 'Invalid',
		DistrictName  = cd.Label,
		AcademicYear  = s.AcYear,
		Records       = count(*)
	FROM
		dbo.K12CourseProd s
		inner join
		dbo.CharterSchool cs
			on right(cs.CdsCode, 7) + right(cs.CdsCode, 7) = s.School
		inner join
		dbo.CharterDistrict cd
			on cd.CharterDistrictId = cs.CharterDistrictId
	GROUP BY
		cd.Label,
		s.AcYear
	ORDER BY
		ObjectName,
		DistrictName,
		AcademicYear;
END;