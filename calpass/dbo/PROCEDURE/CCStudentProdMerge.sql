USE calpass;

GO

IF (object_id('dbo.CCStudentProdMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.CCStudentProdMerge;
	END;

GO

CREATE PROCEDURE
	dbo.CCStudentProdMerge
	(
		@CCStudentProdPlus dbo.CCStudentProdPlus READONLY
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Severity tinyint = 0,
	@State tinyint = 1,
	@RowCount varchar(2048),
	@Error varchar(2048);

BEGIN
	MERGE
		dbo.CCStudentProd t
	USING
		@CCStudentProdPlus s
	ON
		t.CollegeId = s.CollegeId
		and t.StudentId = s.StudentId
		and t.IdStatus = s.IdStatus
		and t.TermId = s.TermId
	WHEN MATCHED THEN
		UPDATE SET
			t.CSISNum = s.CSISNum,
			t.Birthdate = s.Birthdate,
			t.FName = s.FName,
			t.LName = s.LName,
			t.Gender = s.Gender,
			t.Race = s.Race,
			t.Citizen = s.Citizen,
			t.ZipCode = s.ZipCode,
			t.EdStatus = s.EdStatus,
			t.HighSchool = s.HighSchool,
			t.StudentGoal = s.StudentGoal,
			t.EnrollStatus = s.EnrollStatus,
			t.UnitsEarnedLoc = s.UnitsEarnedLoc,
			t.UnitsEarnedTrn = s.UnitsEarnedTrn,
			t.UnitsAttLoc = s.UnitsAttLoc,
			t.UnitsAttTrn = s.UnitsAttTrn,
			t.GPointsLoc = s.GPointsLoc,
			t.GPointsTrn = s.GPointsTrn,
			t.AcademicStanding = s.AcademicStanding,
			t.AcademicLevel = s.AcademicLevel,
			t.DSPS = s.DSPS,
			t.EOPS = s.EOPS,
			t.BoggFlag = s.BoggFlag,
			t.PellFlag = s.PellFlag,
			t.FinancialAidFlag = s.FinancialAidFlag,
			t.Major = s.Major,
			t.ccLongTermId = s.ccLongTermId,
			t.dateAdded = s.dateAdded,
			t.military = s.military,
			t.military_dependent = s.military_dependent,
			t.foster_care = s.foster_care,
			t.incarcerated = s.incarcerated,
			t.mesa = s.mesa,
			t.puente = s.puente,
			t.mchs = s.mchs,
			t.umoja = s.umoja,
			t.[1st_gen] = s.[1st_gen],
			t.caa = s.caa,
			t.Derkey1 = s.Derkey1
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				TermId,
				StudentId,
				IdStatus,
				CSISNum,
				Birthdate,
				FName,
				LName,
				Gender,
				Race,
				Citizen,
				ZipCode,
				EdStatus,
				HighSchool,
				StudentGoal,
				EnrollStatus,
				UnitsEarnedLoc,
				UnitsEarnedTrn,
				UnitsAttLoc,
				UnitsAttTrn,
				GPointsLoc,
				GPointsTrn,
				AcademicStanding,
				AcademicLevel,
				DSPS,
				EOPS,
				BoggFlag,
				PellFlag,
				FinancialAidFlag,
				Major,
				ccLongTermId,
				dateAdded,
				military,
				military_dependent,
				foster_care,
				incarcerated,
				mesa,
				puente,
				mchs,
				umoja,
				[1st_gen],
				caa,
				Derkey1
			)
		VALUES
			(
				s.CollegeId,
				s.TermId,
				s.StudentId,
				s.IdStatus,
				s.CSISNum,
				s.Birthdate,
				s.FName,
				s.LName,
				s.Gender,
				s.Race,
				s.Citizen,
				s.ZipCode,
				s.EdStatus,
				s.HighSchool,
				s.StudentGoal,
				s.EnrollStatus,
				s.UnitsEarnedLoc,
				s.UnitsEarnedTrn,
				s.UnitsAttLoc,
				s.UnitsAttTrn,
				s.GPointsLoc,
				s.GPointsTrn,
				s.AcademicStanding,
				s.AcademicLevel,
				s.DSPS,
				s.EOPS,
				s.BoggFlag,
				s.PellFlag,
				s.FinancialAidFlag,
				s.Major,
				s.ccLongTermId,
				s.dateAdded,
				s.military,
				s.military_dependent,
				s.foster_care,
				s.incarcerated,
				s.mesa,
				s.puente,
				s.mchs,
				s.umoja,
				s.[1st_gen],
				s.caa,
				s.Derkey1
			);
	-- capture
	SET @RowCount = convert(varchar(2048), @@ROWCOUNT);
	-- set
	SET @Error = 'Merged ' + @RowCount + ' dbo.CCStudentProd records';
	-- output
	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;
END;