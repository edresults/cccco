USE calpass;

GO

IF (object_id('dbo.StudentMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.StudentMerge;
	END;

GO

CREATE PROCEDURE
	dbo.StudentMerge
	(
		@InterSegment dbo.InterSegment READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Message nvarchar(2048),
	@ErrorCode int = 70099,
	@ErrorSeverity int = 1;

BEGIN
	BEGIN TRY
		MERGE
			dbo.Student
		WITH
			(
				TABLOCKX
			) t
		USING
			(
				SELECT
					i.InterSegmentKey,
					s.IsHighSchoolStudent,
					s.IsHighSchoolSection,
					s.IsGrade09Student,
					s.IsGrade09Section,
					s.IsGrade10Student,
					s.IsGrade10Section,
					s.IsGrade10StudentInclusive,
					s.IsGrade10SectionInclusive,
					s.IsGrade11Student,
					s.IsGrade11Section,
					s.IsGrade11StudentInclusive,
					s.IsGrade11SectionInclusive,
					s.IsGrade12Student,
					s.IsGrade12Section,
					s.IsGrade12StudentInclusive,
					s.IsGrade12SectionInclusive,
					s.IsHighSchoolCollision
				FROM
					@InterSegment i
					cross apply
					dbo.StudentGet(InterSegmentKey) s
			) s
		ON
			t.InterSegmentKey = s.InterSegmentKey
		WHEN MATCHED THEN
			UPDATE SET
				t.IsHighSchoolStudent = s.IsHighSchoolStudent,
				t.IsHighSchoolSection = s.IsHighSchoolSection,
				t.IsGrade09Student = s.IsGrade09Student,
				t.IsGrade09Section = s.IsGrade09Section,
				t.IsGrade10Student = s.IsGrade10Student,
				t.IsGrade10Section = s.IsGrade10Section,
				t.IsGrade10StudentInclusive = s.IsGrade10StudentInclusive,
				t.IsGrade10SectionInclusive = s.IsGrade10SectionInclusive,
				t.IsGrade11Student = s.IsGrade11Student,
				t.IsGrade11Section = s.IsGrade11Section,
				t.IsGrade11StudentInclusive = s.IsGrade11StudentInclusive,
				t.IsGrade11SectionInclusive = s.IsGrade11SectionInclusive,
				t.IsGrade12Student = s.IsGrade12Student,
				t.IsGrade12Section = s.IsGrade12Section,
				t.IsGrade12StudentInclusive = s.IsGrade12StudentInclusive,
				t.IsGrade12SectionInclusive = s.IsGrade12SectionInclusive,
				t.IsHighSchoolCollision = s.IsHighSchoolCollision
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					InterSegmentKey,
					IsHighSchoolStudent,
					IsHighSchoolSection,
					IsGrade09Student,
					IsGrade09Section,
					IsGrade10Student,
					IsGrade10Section,
					IsGrade10StudentInclusive,
					IsGrade10SectionInclusive,
					IsGrade11Student,
					IsGrade11Section,
					IsGrade11StudentInclusive,
					IsGrade11SectionInclusive,
					IsGrade12Student,
					IsGrade12Section,
					IsGrade12StudentInclusive,
					IsGrade12SectionInclusive,
					IsHighSchoolCollision
				)
			VALUES
				(
					s.InterSegmentKey,
					s.IsHighSchoolStudent,
					s.IsHighSchoolSection,
					s.IsGrade09Student,
					s.IsGrade09Section,
					s.IsGrade10Student,
					s.IsGrade10Section,
					s.IsGrade10StudentInclusive,
					s.IsGrade10SectionInclusive,
					s.IsGrade11Student,
					s.IsGrade11Section,
					s.IsGrade11StudentInclusive,
					s.IsGrade11SectionInclusive,
					s.IsGrade12Student,
					s.IsGrade12Section,
					s.IsGrade12StudentInclusive,
					s.IsGrade12SectionInclusive,
					s.IsHighSchoolCollision
				);
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message =
			nchar(9) + N'Error Procedure: ' + upper(object_name(@@PROCID)) + char(13) + char(10) +
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		-- put error message
		THROW @ErrorCode, @Message, @ErrorSeverity;
	END CATCH;
END;