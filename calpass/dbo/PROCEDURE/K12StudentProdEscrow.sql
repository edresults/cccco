USE calpass;

GO

IF (object_id('dbo.K12StudentProdEscrow') is not null)
	BEGIN
		DROP PROCEDURE dbo.K12StudentProdEscrow;
	END;

GO

CREATE PROCEDURE
	dbo.K12StudentProdEscrow
	(
		@Escrow calpads.Escrow READONLY
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Message nvarchar(2048),
	@ErrorCode int = 70099,
	@ErrorSeverity int = 1,
	@Algorithm char(8) = 'SHA2_512',
	@EscrowName sysname = 'K12StudentProd';

BEGIN
	SELECT
		Derkey1,
		School,
		AcYear,
		LocStudentId,
		StudentId,
		CSISNum,
		Fname,
		Lname,
		Gender,
		Ethnicity,
		Birthdate,
		GradeLevel,
		HomeLanguage,
		HispanicEthnicity,
		EthnicityCode1,
		EthnicityCode2,
		EthnicityCode3,
		EthnicityCode4,
		EthnicityCode5,
		DateAdded
	FROM
		(
			SELECT
				Derkey1 = 
					HASHBYTES(
						@Algorithm,
						upper(convert(nchar(3), f.NameFirst)) + 
						upper(convert(nchar(3), f.NameLast)) + 
						upper(convert(nchar(1), f.Gender)) + 
						convert(nchar(8), f.Birthdate)
					),
				School = r.DistrictCode + r.SchoolCode,
				AcYear = y.YearCodeAbbr,
				LocStudentId = calpass.dbo.get1289Encryption(f.StudentLocalId, 'X'),
				StudentId = null,
				CSISNum = calpass.dbo.get9812Encryption(r.StudentStateId, 'X'),
				Fname = f.NameFirst,
				Lname = f.NameLast,
				Gender = f.Gender,
				Ethnicity = null,
				Birthdate = f.Birthdate,
				GradeLevel = r.GradeCode,
				HomeLanguage =
					case
						when l.PrimaryLanguageCode is null then '99'
						when l.PrimaryLanguageCode = 'UU' then '99'
						else l.PrimaryLanguageCode
					end,
				HispanicEthnicity = f.IsHispanicEthnicity,
				EthnicityCode1 = f.Race01,
				EthnicityCode2 = f.Race02,
				EthnicityCode3 = f.Race03,
				EthnicityCode4 = f.Race04,
				EthnicityCode5 = f.Race05,
				DateAdded = getdate(),
				RecordCount = row_number() over(
						partition by
							r.DistrictCode,
							r.SchoolCode,
							calpass.dbo.get1289Encryption(f.StudentLocalId, 'X'),
							r.YearCode
						order by
							r.GradeCode desc
					)
			FROM
				@Escrow e
				inner join
				calpads.Senr r with(index(pk_Senr))
					on e.StudentStateId = r.StudentStateId
					and e.SchoolCode = r.SchoolCode
					and e.YearCode = r.YearCode
				-- required sinf record
				inner join
				calpads.Sinf f with(index(pk_Sinf))
					on e.StudentStateId = f.StudentStateId
					and e.SchoolCode = f.SchoolCode
					and e.SinfYearCode = f.YearCode
					and e.SinfEffectiveStartDate = f.EffectiveStartDate
				inner join
				calpads.Year y with(index(Pk_Year))
					on y.YearCode = r.YearCode
				-- optional sela record
				left outer join
				calpads.Sela l with(index(pk_Sela))
					on l.StudentStateId = r.StudentStateId
					and l.SchoolCode = r.SchoolCode
					and l.ElaStatusStartDate = (
						SELECT
							max(l1.ElaStatusStartDate)
						FROM
							calpads.Sela l1 with(index(pk_Sela))
						WHERE
							l1.StudentStateId = l.StudentStateId
							and l1.SchoolCode = l.SchoolCode
							and l1.YearCode = l.YearCode
							and l1.YearCode = (
								SELECT
									max(l2.YearCode)
								FROM
									calpads.Sela l2 with(index(pk_Sela))
								WHERE
									l2.StudentStateId = l1.StudentStateId
									and l2.SchoolCode = l1.SchoolCode
									and l2.YearCode <= r.YearCode
							)
					)
			WHERE
				e.EscrowName = @EscrowName
				and r.EnrollStartDate = (
					SELECT
						max(r1.EnrollStartDate)
					FROM
						calpads.Senr r1 with(index(pk_Senr))
					WHERE
						r1.StudentStateId = r.StudentStateId
						and r1.SchoolCode = r.SchoolCode
						and r1.YearCode = r.YearCode
				)
				and f.StudentLocalId is not null
		) a
	WHERE
		RecordCount = 1;
END;