USE calpass;

IF (object_id('dbo.p_ccsf_cohort_cc_merge', 'P') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_ccsf_cohort_cc_merge;
	END;

GO

CREATE PROCEDURE
	dbo.p_ccsf_cohort_cc_merge
AS
	
	SET NOCOUNT ON;

DECLARE
	@college_id char(6) = '112190',
	@year_increment tinyint = 1;

BEGIN

	MERGE
		calpass.dbo.ccsf_cohort_cc t
	USING
		(
			SELECT
				ccs.CollegeId,
				ccs.StudentId,
				term_id_first = min(ccs.TermId),
				ccs.Derkey1
			FROM
				calpass.dbo.CCStudentProd ccs
				inner join
				calpass.dbo.CCCourseProd ccc
					on ccs.CollegeId = ccc.CollegeId
					and ccs.StudentId = ccc.StudentId
					and ccs.TermId = ccc.TermId
				inner join
				calpass.dbo.CCStudentProd_TermId ccst
					on ccst.code = ccs.termId
				inner join
				(
					-- Graduation Year
					SELECT
						c.Derkey1,
						min(hssy.code) as grad_year_code,
						min(hssn.code) as next_year_code
					FROM
						calpass.dbo.ccsf_cohort_12 c
						inner join
						calpass.dbo.K12AwardProd hsa
							on c.School = hsa.School
							and c.LocStudentId = hsa.LocStudentId
							and c.AcYear = hsa.AcYear
						inner join
						calpass.dbo.K12StudentProd_AcYear hssy
							on hssy.code = hsa.AcYear
						inner join
						calpass.dbo.K12StudentProd_AcYear hssn
							on hssy.year_trailing + 1 = hssn.year_trailing
					WHERE
						hsa.AwardType in ('100','106','108','120','250','320','330','480')
					GROUP BY
						c.Derkey1
				) grad
					on grad.Derkey1 = ccs.Derkey1
					and ccst.year_code > grad.grad_year_code
					and ccst.year_code <= grad.next_year_code
			WHERE
				-- Credit
				ccc.creditFlag in ('T', 'D', 'C', 'S')
				-- CCSF
				and ccc.collegeId = @college_id
			GROUP BY
				ccs.CollegeId,
				ccs.StudentId,
				ccs.Derkey1
		) s
	ON
		(
			t.CollegeId = s.CollegeId
			and t.StudentId = s.StudentId
			and t.term_id_first = s.term_id_first
		)
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				StudentId,
				TermId,
				term_id_first,
				Derkey1
			)
		VALUES
			(
				s.CollegeId,
				s.StudentId,
				s.TermId,
				s.term_id_first,
				s.Derkey1
			)
	WHEN NOT MATCHED BY SOURCE THEN
		DELETE
	WHEN MATCHED THEN
		UPDATE SET
			t.term_id_first = s.term_id_first,
			t.Derkey1 = s.Derkey1;

END;