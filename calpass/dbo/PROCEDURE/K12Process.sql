USE calpass;

GO

IF (object_id('dbo.K12Process') is not null)
	BEGIN
		DROP PROCEDURE dbo.K12Process;
	END;

GO

CREATE PROCEDURE
	dbo.K12Process
AS

	SET NOCOUNT ON;

DECLARE
	-- error message
	@Message	   nvarchar(2048),
	@ErrorCode	 int = 70099,
	@ErrorSeverity int = 16,
	-- mail vas
	@ProfileName   sysname = 'Cal-PASS SQL Support Profile',
	@Recipients	varchar(max) = 'dlamoree@edresults.org',
	@Subject	   nvarchar(255) = object_name(@@PROCID),
	-- proc vars
	@True		  bit = 1,
	@False		 bit = 0,
	@Escrow		calpads.Escrow,
	@InterSegment  dbo.InterSegment,
	@Resource	  dbo.Resource,
	@IsSuccess	 bit;

BEGIN
	-- insert all resources for locking
	INSERT
		@Resource
		(
			ResourceName
		)
	VALUES
		('dbo.K12Process'),
		('calpads.Sinf'),
		('calpads.Senr'),
		('calpads.Sela'),
		('calpads.Scsc'),
		('calpads.Crsc'),
		('calpads.Scte'),
		('calpads.Sprg');

	-- lock all resources
	EXECUTE dbo.LockResource
		@Resource = @Resource,
		@IsSuccess = @IsSuccess out;

	-- exit if no cannot lock all resources
	IF (@IsSuccess = 0)
		BEGIN
			SET @Message = 'Could not lock resources';
			THROW @ErrorCode, @Message, @ErrorSeverity;
		END;

	-- Collect New Students Determine Tables to Update
	BEGIN TRY
		INSERT
			@Escrow
			(
				StudentStateId,
				SchoolCode,
				YearCode,
				EscrowName,
				InterSegmentKey,
				SinfYearCode,
				SinfEffectiveStartDate
			)
		EXECUTE calpads.EscrowGet;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'calpads.EscrowGet';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	-- K12AwardProd
	BEGIN TRY
		-- merge awards
		EXECUTE dbo.K12AwardProdMerge
			@Escrow = @Escrow;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'dbo.K12AwardProdMerge';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	-- K12StudentProd
	BEGIN TRY
		EXECUTE dbo.K12StudentProdMerge
			@Escrow = @Escrow;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'dbo.K12StudentProdMerge';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;
		
	-- update escrow of base calpads tables
	UPDATE calpads.Senr SET IsEscrow = @False WHERE IsEscrow = @True;
	UPDATE calpads.Sela SET IsEscrow = @False WHERE IsEscrow = @True;

	-- K12CourseProd
	BEGIN TRY
		-- merge sections
		EXECUTE dbo.K12CourseProdMerge
			@Escrow = @Escrow;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'dbo.K12CourseProdMerge';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	-- update escrow of base calpads tables
	UPDATE calpads.Crsc SET IsEscrow = @False WHERE IsEscrow = @True;
	UPDATE calpads.Scsc SET IsEscrow = @False WHERE IsEscrow = @True;

	-- K12PrgProd
	BEGIN TRY
		EXECUTE dbo.K12PrgProdMerge
			@Escrow = @Escrow;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'dbo.K12PrgProdMerge';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	-- update escrow of base calpads table
	UPDATE calpads.Sprg SET IsEscrow = @False WHERE IsEscrow = @True;
	
	-- K12CteProd
	BEGIN TRY
		-- merge careers
		EXECUTE dbo.K12CteProdMerge
			@Escrow = @Escrow;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'dbo.K12CteProdMerge';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	-- update escrow of base calpads table
	UPDATE calpads.Scte SET IsEscrow = @False WHERE IsEscrow = @True;

	-- update remaining sinf calpads table
	UPDATE calpads.Sinf SET IsEscrow = @False WHERE IsEscrow = @True;

	-- 
	-- BOOTSTRAP
	-- 

	-- InterSegment
	BEGIN TRY
		-- get unique students
		INSERT
			@InterSegment
			(
				InterSegmentKey
			)
		SELECT DISTINCT
			InterSegmentKey
		FROM
			@Escrow
		WHERE
			InterSegmentKey is not null;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	-- Student
	BEGIN TRY
		EXECUTE dbo.StudentMerge
			@InterSegment = @InterSegment;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'dbo.StudentMerge';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	--
	-- Retrospective
	--

	BEGIN TRY
		EXECUTE mmap.RetrospectivePerformanceMerge
			@InterSegment = @InterSegment;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'mmap.RetrospectivePerformanceMerge';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	BEGIN TRY
		EXECUTE mmap.RetrospectiveCourseContentMerge
			@InterSegment = @InterSegment;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'mmap.RetrospectiveCourseContentMerge';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	--
	-- PlacementByISK
	--

	BEGIN TRY
		EXECUTE mmap.PlacementMerge
			@InterSegment = @InterSegment;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'mmap.PlacementMerge';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	BEGIN TRY
		EXECUTE mmap.TranscriptMerge
			@InterSegment = @InterSegment;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'mmap.TranscriptMerge';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	--
	-- PlacementBySsid
	--

	BEGIN TRY
		EXECUTE mmap.K12PerformanceMerge
			@Escrow = @Escrow;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'mmap.K12PerformanceMerge';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	BEGIN TRY
		EXECUTE mmap.K12CourseContentMerge
			@Escrow = @Escrow;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'mmap.K12CourseContentMerge';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	-- BEGIN TRY
	-- 	EXECUTE mmap.K12PlacementMerge
	-- 		@Escrow = @Escrow;
	-- END TRY
	-- BEGIN CATCH
	-- 	-- set error message
	-- 	SET @Message = isnull(ERROR_MESSAGE(), N'');
	-- 	SET @Subject = 'mmap.K12PlacementMerge';
	-- 	-- put error message
	-- 	RAISERROR(@Message, 0, 1) WITH NOWAIT;
	-- 	-- unlock resources
	-- 	EXECUTE dbo.UnlockResource
	-- 		@Resource = @Resource;
	-- 	-- email error
	-- 	EXEC msdb.dbo.sp_send_dbmail
	-- 		@profile_name = @ProfileName,
	-- 		@recipients = @Recipients,
	-- 		@subject = @Subject,
	-- 		@body = @Message;
	-- 	-- exit
	-- 	RETURN;
	-- END CATCH;

	-- BEGIN TRY
	-- 	EXECUTE mmap.K12TranscriptMerge
	-- 		@Escrow = @Escrow;
	-- END TRY
	-- BEGIN CATCH
	-- 	-- set error message
	-- 	SET @Message = isnull(ERROR_MESSAGE(), N'');
	-- 	SET @Subject = 'mmap.K12TranscriptMerge';
	-- 	-- put error message
	-- 	RAISERROR(@Message, 0, 1) WITH NOWAIT;
	-- 	-- unlock resources
	-- 	EXECUTE dbo.UnlockResource
	-- 		@Resource = @Resource;
	-- 	-- email error
	-- 	EXEC msdb.dbo.sp_send_dbmail
	-- 		@profile_name = @ProfileName,
	-- 		@recipients = @Recipients,
	-- 		@subject = @Subject,
	-- 		@body = @Message;
	-- 	-- exit
	-- 	RETURN;
	-- END CATCH;

	-- AB705 API
	BEGIN TRY
		UPDATE
			s
		SET
			s.InterSegmentKey = a.InterSegmentKey,
			s.IsCollision = a.IsCollision,
			s.IsEscrow = a.IsEscrow
		FROM
			calpads.Students s
			inner join
			(
				SELECT
					StudentStateId,
					InterSegmentKey,
					IsCollision,
					IsEscrow,
					cnt = row_number() over(partition by StudentStateId order by InterSegmentKey)
				FROM
					(
						SELECT DISTINCT
							StudentStateId,
							InterSegmentKey,
							IsCollision = 0,
							IsEscrow = 1
						FROM
							@Escrow
					) a
			) b
				on b.StudentStateId = s.StudentStateId
		WHERE
			b.cnt = 1;

		INSERT INTO
			calpads.Students
			(
				StudentStateId,
				InterSegmentKey,
				IsCollision,
				IsEscrow
			)
		SELECT
			StudentStateId,
			InterSegmentKey,
			IsCollision,
			IsEscrow
		FROM
			(
				SELECT
					StudentStateId,
					InterSegmentKey,
					IsCollision,
					IsEscrow,
					cnt = row_number() over(partition by StudentStateId order by InterSegmentKey)
				FROM
					@Escrow b
				WHERE
					not exists (
						SELECT
							null
						FROM
							calpads.Students s1
						WHERE
							s1.InterSegmentKey = b.InterSegmentKey
					)
			) b
		WHERE
			b.cnt = 1;

		UPDATE
			t
		SET
			IsCollision = 1
		FROM
			calpads.Students t
		WHERE
			exists (
				SELECT
					NULL
				FROM
					calpads.Students a
				WHERE
					a.InterSegmentKey = t.InterSegmentKey
				HAVING
					COUNT(DISTINCT a.StudentStateId) > 1
			);
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'calpads.Student';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	--
	-- DEPRECATED PROCESSES
	--

	-- Old MMAP
	BEGIN TRY
		-- Merge HSTranscriptSummaryHashMerge
		EXECUTE dbo.HSTranscriptSummaryHashMerge
			@InterSegment = @InterSegment;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'dbo.HSTranscriptSummaryHashMerge';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- unlock resources
		EXECUTE dbo.UnlockResource
			@Resource = @Resource;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	EXECUTE dbo.UnlockResource
		@Resource = @Resource;
END;