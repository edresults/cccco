USE calpass;

GO

IF (object_id('dbo.K12PrgProdEscrow') is not null)
	BEGIN
		DROP PROCEDURE dbo.K12PrgProdEscrow;
	END;

GO

CREATE PROCEDURE
	dbo.K12PrgProdEscrow
	(
		@Escrow calpads.Escrow READONLY
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Message nvarchar(2048),
	@ErrorCode int = 70099,
	@ErrorSeverity int = 1,
	@Algorithm char(8) = 'SHA2_512',
	@EscrowName sysname = 'K12PrgProd';

BEGIN
	SELECT
		Derkey1,
		School,
		AcYear,
		LocStudentId,
		ProgramCode,
		MembershipCode,
		MembershipStartDate,
		MembershipEndDate,
		ServiceYearCode,
		ServiceCode,
		AcademyId,
		MigrantId,
		DisabilityCode,
		AccountabilityDistrictCode,
		DwellingCode,
		IsHomeless,
		IsRunaway,
		DateAdded
	FROM
		(
			SELECT
				Derkey1 = 
					HASHBYTES(
						@Algorithm,
						upper(convert(nchar(3), f.NameFirst)) + 
						upper(convert(nchar(3), f.NameLast)) + 
						upper(convert(nchar(1), f.Gender)) + 
						convert(nchar(8), f.Birthdate)
					),
				School = p.DistrictCode + p.SchoolCode,
				AcYear = y.YearCodeAbbr,
				LocStudentId = calpass.dbo.get1289Encryption(f.StudentLocalId, 'X'),
				p.ProgramCode,
				p.MembershipCode,
				p.MembershipStartDate,
				p.MembershipEndDate,
				p.ServiceYearCode,
				p.ServiceCode,
				p.AcademyId,
				p.MigrantId,
				p.DisabilityCode,
				p.AccountabilityDistrictCode,
				p.DwellingCode,
				p.IsHomeless,
				p.IsRunaway,
				DateAdded = getdate(),
				RecordCount = row_number() over(
						partition by
							p.DistrictCode,
							p.SchoolCode,
							calpass.dbo.Get1289Encryption(f.StudentLocalId, 'X'),
							p.YearCode,
							p.ProgramCode,
							p.MembershipStartDate,
							p.ServiceYearCode,
							p.ServiceCode
						order by
							(select 1) desc
					)
			FROM
				@Escrow e
				inner join
				calpads.Sprg p with(index(pk_Sprg))
					on p.StudentStateId = e.StudentStateId
					and p.SchoolCode = e.SchoolCode
					and p.YearCode = e.YearCode
				inner join
				calpads.Year y with(index(PK_Year))
					on y.YearCode = p.YearCode
				inner join
				calpads.Sinf f with(index(pk_Sinf))
					on e.StudentStateId = f.StudentStateId
					and e.SchoolCode = f.SchoolCode
					and e.SinfYearCode = f.YearCode
					and e.SinfEffectiveStartDate = f.EffectiveStartDate
			WHERE
				e.EscrowName = @EscrowName
				and f.StudentLocalId is not null
		) a
	WHERE
		a.RecordCount = 1;
END;