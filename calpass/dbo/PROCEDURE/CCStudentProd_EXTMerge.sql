USE calpass;

GO

IF (object_id('dbo.CCStudentProd_EXTMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.CCStudentProd_EXTMerge;
	END;

GO

CREATE PROCEDURE
	dbo.CCStudentProd_EXTMerge
	(
		@CCStudentProdPlus dbo.CCStudentProdPlus READONLY
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Severity tinyint = 0,
	@State tinyint = 1,
	@RowCount varchar(2048),
	@Error varchar(2048);

BEGIN
	-- CCStudentProd
	BEGIN TRY
		BEGIN TRANSACTION
			-- update
			MERGE
				dbo.CCStudentProd_EXT t
			USING
				@CCStudentProdPlus s
			ON
				t.CollegeId = s.CollegeId
				and t.StudentId = s.StudentId
				and t.IdStatus = s.IdStatus
				and t.TermId = s.TermId
			WHEN MATCHED THEN
				UPDATE SET
					t.extSB09_ResidenceCode = s.extSB09_ResidenceCode,
					t.extSB23_ApprenticeshipStatus = s.extSB23_ApprenticeshipStatus,
					t.extSB24_TransferCenterStatus = s.extSB24_TransferCenterStatus,
					t.extSB26_JTPAStatus = s.extSB26_JTPAStatus,
					t.extSB27_CalWorksStatus = s.extSB27_CalWorksStatus,
					t.extSM01_MatricGoals = s.extSM01_MatricGoals,
					t.extSM01_MatricGoalsPos1 = s.extSM01_MatricGoalsPos1,
					t.extSM01_MatricGoalsPos2 = s.extSM01_MatricGoalsPos2,
					t.extSM01_MatricGoalsPos3 = s.extSM01_MatricGoalsPos3,
					t.extSM01_MatricGoalsPos4 = s.extSM01_MatricGoalsPos4,
					t.extSM07_MatricOrientationServices = s.extSM07_MatricOrientationServices,
					t.extSM08_MatricAssessmentServicesPlacement = s.extSM08_MatricAssessmentServicesPlacement,
					t.extSM09_MatricAssessmentServicesOther = s.extSM09_MatricAssessmentServicesOther,
					t.extSM09_MatricAssessmentServicesOtherPos1 = s.extSM09_MatricAssessmentServicesOtherPos1,
					t.extSM09_MatricAssessmentServicesOtherPos2 = s.extSM09_MatricAssessmentServicesOtherPos2,
					t.extSM09_MatricAssessmentServicesOtherPos3 = s.extSM09_MatricAssessmentServicesOtherPos3,
					t.extSM12_MatricCounAdviseServices = s.extSM12_MatricCounAdviseServices,
					t.extSM13_MatricAcademicFollowupSerives = s.extSM13_MatricAcademicFollowupSerives,
					t.extSVO1_ProgramPlanStatus = s.extSVO1_ProgramPlanStatus,
					t.extSV03_EconomicallyDisadvStatus = s.extSV03_EconomicallyDisadvStatus,
					t.extSV03_EconomicallyDisadvStatusPos1 = s.extSV03_EconomicallyDisadvStatusPos1,
					t.extSV03_EconomicallyDisadvStatusPos2 = s.extSV03_EconomicallyDisadvStatusPos2,
					t.extSV04_SingleParentStatus = s.extSV04_SingleParentStatus,
					t.extSV05_DisplacedHomemakerStatus = s.extSV05_DisplacedHomemakerStatus,
					t.extSV06_CoopWorkExperienceEdType = s.extSV06_CoopWorkExperienceEdType,
					t.extSV08_TechPrepStatus = s.extSV08_TechPrepStatus,
					t.extSV09_MigrantWorkerStatus = s.extSV09_MigrantWorkerStatus,
					t.extSCD1_FirstTermAttended = s.extSCD1_FirstTermAttended,
					t.extSCD2_LastTermAttended = s.extSCD2_LastTermAttended,
					t.extSCD4_LEP = s.extSCD4_LEP,
					t.extSCD5_AcademicallyDisadvantagedStudent = s.extSCD5_AcademicallyDisadvantagedStudent,
					t.extSTD1_AgeAtTerm = s.extSTD1_AgeAtTerm,
					t.extSTD2_1stCensusCreditLoad = s.extSTD2_1stCensusCreditLoad,
					t.extSTD3_DayEveningClassCode = s.extSTD3_DayEveningClassCode,
					t.extSTD5_DegreeAppUnitsEarned = s.extSTD5_DegreeAppUnitsEarned,
					t.extSTD6_DayEveningClassCode2 = s.extSTD6_DayEveningClassCode2,
					t.extSTD7_HeadCountStatus = s.extSTD7_HeadCountStatus,
					t.extSTD8_LocalCumGPA = s.extSTD8_LocalCumGPA,
					t.extSTD9_TotalCumGPA = s.extSTD9_TotalCumGPA,
					t.extSD01_StudentPrimaryDisability = s.extSD01_StudentPrimaryDisability,
					t.extSD03_StudentSecondaryDisability = s.extSD03_StudentSecondaryDisability,
					t.extSB29_MultiEthnicity = s.extSB29_MultiEthnicity
			WHEN NOT MATCHED BY TARGET THEN
				INSERT
					(
						CollegeId,
						TermId,
						StudentId,
						extSB09_ResidenceCode,
						extSB23_ApprenticeshipStatus,
						extSB24_TransferCenterStatus,
						extSB26_JTPAStatus,
						extSB27_CalWorksStatus,
						extSM01_MatricGoals,
						extSM01_MatricGoalsPos1,
						extSM01_MatricGoalsPos2,
						extSM01_MatricGoalsPos3,
						extSM01_MatricGoalsPos4,
						extSM07_MatricOrientationServices,
						extSM08_MatricAssessmentServicesPlacement,
						extSM09_MatricAssessmentServicesOther,
						extSM09_MatricAssessmentServicesOtherPos1,
						extSM09_MatricAssessmentServicesOtherPos2,
						extSM09_MatricAssessmentServicesOtherPos3,
						extSM12_MatricCounAdviseServices,
						extSM13_MatricAcademicFollowupSerives,
						extSVO1_ProgramPlanStatus,
						extSV03_EconomicallyDisadvStatus,
						extSV03_EconomicallyDisadvStatusPos1,
						extSV03_EconomicallyDisadvStatusPos2,
						extSV04_SingleParentStatus,
						extSV05_DisplacedHomemakerStatus,
						extSV06_CoopWorkExperienceEdType,
						extSV08_TechPrepStatus,
						extSV09_MigrantWorkerStatus,
						extSCD1_FirstTermAttended,
						extSCD2_LastTermAttended,
						extSCD4_LEP,
						extSCD5_AcademicallyDisadvantagedStudent,
						extSTD1_AgeAtTerm,
						extSTD2_1stCensusCreditLoad,
						extSTD3_DayEveningClassCode,
						extSTD5_DegreeAppUnitsEarned,
						extSTD6_DayEveningClassCode2,
						extSTD7_HeadCountStatus,
						extSTD8_LocalCumGPA,
						extSTD9_TotalCumGPA,
						extSD01_StudentPrimaryDisability,
						extSD03_StudentSecondaryDisability,
						extSB29_MultiEthnicity
					)
				VALUES
					(
						s.CollegeId,
						s.TermId,
						s.StudentId,
						s.extSB09_ResidenceCode,
						s.extSB23_ApprenticeshipStatus,
						s.extSB24_TransferCenterStatus,
						s.extSB26_JTPAStatus,
						s.extSB27_CalWorksStatus,
						s.extSM01_MatricGoals,
						s.extSM01_MatricGoalsPos1,
						s.extSM01_MatricGoalsPos2,
						s.extSM01_MatricGoalsPos3,
						s.extSM01_MatricGoalsPos4,
						s.extSM07_MatricOrientationServices,
						s.extSM08_MatricAssessmentServicesPlacement,
						s.extSM09_MatricAssessmentServicesOther,
						s.extSM09_MatricAssessmentServicesOtherPos1,
						s.extSM09_MatricAssessmentServicesOtherPos2,
						s.extSM09_MatricAssessmentServicesOtherPos3,
						s.extSM12_MatricCounAdviseServices,
						s.extSM13_MatricAcademicFollowupSerives,
						s.extSVO1_ProgramPlanStatus,
						s.extSV03_EconomicallyDisadvStatus,
						s.extSV03_EconomicallyDisadvStatusPos1,
						s.extSV03_EconomicallyDisadvStatusPos2,
						s.extSV04_SingleParentStatus,
						s.extSV05_DisplacedHomemakerStatus,
						s.extSV06_CoopWorkExperienceEdType,
						s.extSV08_TechPrepStatus,
						s.extSV09_MigrantWorkerStatus,
						s.extSCD1_FirstTermAttended,
						s.extSCD2_LastTermAttended,
						s.extSCD4_LEP,
						s.extSCD5_AcademicallyDisadvantagedStudent,
						s.extSTD1_AgeAtTerm,
						s.extSTD2_1stCensusCreditLoad,
						s.extSTD3_DayEveningClassCode,
						s.extSTD5_DegreeAppUnitsEarned,
						s.extSTD6_DayEveningClassCode2,
						s.extSTD7_HeadCountStatus,
						s.extSTD8_LocalCumGPA,
						s.extSTD9_TotalCumGPA,
						s.extSD01_StudentPrimaryDisability,
						s.extSD03_StudentSecondaryDisability,
						s.extSB29_MultiEthnicity
					);
			-- capture
			SET @RowCount = convert(varchar(2048), @@ROWCOUNT);
			-- set
			SET @Error = 'Merged ' + @RowCount + ' dbo.CCStudentProd records';
			-- output
			RAISERROR(@Error, @Severity, @State) WITH NOWAIT;
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
    THROW;
	END CATCH;
END;