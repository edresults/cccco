USE calpass;

GO

IF (object_id('dbo.CCProcess') is not null)
	BEGIN
		DROP PROCEDURE dbo.CCProcess;
	END;

GO

CREATE PROCEDURE
	dbo.CCProcess
AS

	SET NOCOUNT ON;

DECLARE
	-- variables
	@CollegeCode char(3),
	@CollegeIpedsCode char(6),
	@Escrow comis.Escrow,
	@CCStudentProdPlus dbo.CCStudentProdPlus,
	@CCCourseProdPlus dbo.CCCourseProdPlus,
	@CCAwardProd dbo.CCAwardProd,
	@CCFinAidAwardsProd dbo.CCFinAidAwardsProd;

BEGIN
	BEGIN TRY
		-- define cursor
		DECLARE
			CollegeCursor
		CURSOR 
			FAST_FORWARD
		FOR 
			SELECT
				CollegeCode,
				IpedsCodeLegacy
			FROM
				comis.College
			ORDER BY
				CollegeCode;

		-- open cursor
		OPEN CollegeCursor;

		-- capture cursor variables
		FETCH NEXT FROM
			CollegeCursor
		INTO
			@CollegeCode,
			@CollegeIpedsCode;

		-- loop over cursor
		WHILE @@FETCH_STATUS = 0
		BEGIN
			-- output college
			RAISERROR(@CollegeCode, 0, 1) WITH NOWAIT;
			
			-- Legacy Encryption
			EXECUTE comis.CCLegacyEncrypt
				@CollegeCode = @CollegeCode;

			-- InterSegmentKey Encryption
			EXECUTE comis.CCInterSegmentKeyEncrypt
				@CollegeCode = @CollegeCode;

			-- Escrow
			INSERT
				@Escrow
			EXECUTE comis.EscrowGet
				@CollegeCode = @CollegeCode;

			-- Student
			INSERT
				@CCStudentProdPlus
			EXECUTE dbo.CCStudentProdPlusEscrow
				@Escrow = @Escrow;

			-- CCStudentProd
			EXECUTE dbo.CCStudentProdMerge
				@CCStudentProdPlus = @CCStudentProdPlus;

			-- CCStudentProd_EXT
			EXECUTE dbo.CCStudentProd_EXTMerge
				@CCStudentProdPlus = @CCStudentProdPlus;

			-- SecPii Encryption
			EXECUTE comis.CCModernEncryption
				@CollegeCode = @CollegeCode;

			--Course
			INSERT
				@CCCourseProdPlus
			EXECUTE dbo.CCCourseProdPlusEscrow
				@Escrow = @Escrow;

			-- CCCourseProd
			EXECUTE dbo.CCCourseProdMerge
				@CCCourseProdPlus = @CCCourseProdPlus;

			-- CCCourseProd_EXT
			EXECUTE dbo.CCCourseProd_EXTMerge
				@CCCourseProdPlus = @CCCourseProdPlus;

			-- Program
			INSERT
				@CCAwardProd
			EXECUTE dbo.CCAwardProdEscrow
				@Escrow = @Escrow;
			
			-- CCAwardProd
			EXECUTE dbo.CCAwardProdMerge
				@CCAwardProd = @CCAwardProd;

			-- Financial
			INSERT
				@CCFinAidAwardsProd
			EXECUTE dbo.CCFinAidAwardsProdEscrow
				@Escrow = @Escrow;
			
			-- CCFinAidAwardsProd
			EXECUTE dbo.CCFinAidAwardsProdMerge
				@CCFinAidAwardsProd = @CCFinAidAwardsProd;

		END;
		-- close cursor
		CLOSE CollegeCursor;
		-- trash cursor
		DEALLOCATE CollegeCursor;
	END TRY
	BEGIN CATCH
		-- close cursor
		CLOSE CollegeCursor;
		-- trash cursor
		DEALLOCATE CollegeCursor;
		-- throw error
		THROW;
	END CATCH;
END;