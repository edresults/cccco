USE calpass;

GO

IF (object_id('dbo.UnlockResource') is not null)
	BEGIN
		DROP PROCEDURE dbo.UnlockResource;
	END;

GO

CREATE PROCEDURE
	dbo.UnlockResource
	(
		@Resource dbo.Resource READONLY
	)
AS

	SET NOCOUNT ON;

DECLARE
	-- applock
	@LockMode varchar(32) = 'Exclusive',
	@LockOwner varchar(32) = 'Session',
	@LockTimeout int = null,
	@DbPrincipal sysname = N'public',
	@LockCode smallint,
	-- error message
	@Message nvarchar(2048),
	@ErrorCode int = 70099,
	@ErrorSeverity int = 16,
	-- email message
	@ProfileName sysname = 'Cal-PASS SQL Support Profile',
	@Recipients varchar(max) = 'dlamoree@edresults.org',
	@Subject nvarchar(255) = object_name(@@PROCID),
	-- proc vars
	@Locked bit = 1,
	@ResourceName nvarchar(255),
	@i int = 0,
	@c int = 0;

BEGIN
	-- assign upper bound
	SELECT
		@c = count(*)
	FROM
		@Resource;

	WHILE (@i < @c)
	BEGIN
		-- Resource Name
		SELECT
			@ResourceName = ResourceName
		FROM
			@Resource
		WHERE
			Iterator = @i;

		BEGIN TRY
			EXECUTE @LockCode = sys.sp_releaseapplock
				@Resource = @ResourceName,
				@LockOwner = @LockOwner,
				@DbPrincipal = @DbPrincipal;

			SET @Message = 
				case @LockCode
					when 0 then 'Lock was successfully released.'
					when -999 then 'Indicates a parameter validation or other call error.'
				end

		END TRY
		BEGIN CATCH
			-- set error message
			SET @Message =
				nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			-- email error
			EXEC msdb.dbo.sp_send_dbmail
				@profile_name = @ProfileName,
				@recipients = @Recipients,
				@subject = @Subject,
				@body = @Message;
		END CATCH;

		IF (@LockCode not in (0, 1))
			BEGIN
				-- email error
				EXEC msdb.dbo.sp_send_dbmail
					@profile_name = @ProfileName,
					@recipients = @Recipients,
					@subject = @Subject,
					@body = @Message;
			END;
		
		SET @i += 1;
	END;
END;