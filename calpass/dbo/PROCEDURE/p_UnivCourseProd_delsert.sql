USE calpass;

GO

IF (object_id('dbo.p_UnivCourseProd_delsert') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_UnivCourseProd_delsert;
	END;

GO

CREATE PROCEDURE
	dbo.p_UnivCourseProd_delsert
	(
		@StageSchemaTableName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
	@message nvarchar(2048),
	@validate bit = 0,
	@sql nvarchar(max),
	@tab_needle varchar(255) = char(13) + char(10) + char(9) + char(9),
	@tab_replace varchar(255) = char(13) + char(10),
	@Query nvarchar(max),
	@ColumnArray nvarchar(max) = N'';

BEGIN
	-- validate inputs
	SELECT
		@validate = 1
	FROM
		sys.schemas s
		inner join
		sys.tables t
			on s.schema_id = t.schema_id
	WHERE
		s.name = parsename(@StageSchemaTableName, 2)
		and t.name = parsename(@StageSchemaTableName, 1);
	-- process validation
	IF (@validate = 0)
		BEGIN
			SET @message = N'Table not found';
			THROW 70099, @message, 1;
		END;
	-- set query string
	SET @Query = N'SELECT * FROM ' + @StageSchemaTableName + ';';
	-- Encrypt
	SET @sql = replace(N'
		UPDATE
			' + @StageSchemaTableName + '
		SET
			StudentId = calpass.dbo.Get1289Encryption(StudentId, '''');', @tab_needle, @tab_replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@sql;
	-- delete data from production using staging
	SET @sql = replace(N'
		DELETE
			t
		FROM
			dbo.UnivCourseProd t with (tablockx)
			inner join
			' + @StageSchemaTableName + ' s
				on t.School = s.School
				and t.YrTerm = s.YrTerm;', @tab_needle, @tab_replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@sql;
	-- set columns
	SET @sql = replace(N'
		SELECT
			@ColumnArray += name + case when count(*) over() = row_number() over(order by (select 1)) then '''' else '','' end
		FROM
			sys.dm_exec_describe_first_result_set(@query, null, 0);', @tab_needle, @tab_replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@sql,
		N'@query nvarchar(max), @ColumnArray nvarchar(max) out',
		@query = @query,
		@ColumnArray = @ColumnArray out;
	-- set dynamic sql
	SET @sql = replace(N'
		INSERT INTO
			dbo.UnivCourseProd with (tablockx)
			(
				' + @ColumnArray + '
			)
		SELECT
			' + @ColumnArray + '
		FROM
			' + @StageSchemaTableName + ';', @tab_needle, @tab_replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@sql;
	-- update column values
	UPDATE
		dbo.UnivCourseProd
	SET
		IsEscrow = 0
	WHERE
		IsEscrow = 1;
END;