USE calpass;

GO

IF (object_id('dbo.EmsiMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.EmsiMerge
	END;

GO

CREATE PROCEDURE
	dbo.EmsiMerge
	(
		@StageSchemaTableName nvarchar(517),
		@RegionCode char(1),
		@YearCurrent char(4)
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Sql nvarchar(4000) = N'',
	@Message nvarchar(2048) = N'',
	@YearPast char(4) = convert(char(4), convert(smallint, @YearCurrent) - 5),
	@YearFuture char(4) = convert(char(4), convert(smallint, @YearCurrent) + 5);

BEGIN
	-- validate stage schema table name
	IF (object_id(@StageSchemaTableName) is null)
		BEGIN
			SET @Message = N'Stage table not found';
			THROW 70099, @Message, 1;
		END;

	SET @Sql = '
		MERGE
			dbo.Emsi t
		USING
			' + @StageSchemaTableName + ' s
		ON
			t.RegionCode = @RegionCode
			and t.YearCurrent = @YearCurrent
			and t.RegionName = s.RegionName
			and t.Soc = s.Soc
		WHEN MATCHED THEN
			UPDATE SET
				t.YearPast = @YearPast,
				t.YearFuture = @YearFuture,
				t.EmploymentPast = s.EmploymentPast,
				t.EmploymentCurrent = s.EmploymentCurrent,
				t.ChangePastCurrent = s.ChangePastCurrent,
				t.ChangePastCurrentPct = s.ChangePastCurrentPct,
				t.ChangeCurrentFuture = s.ChangeCurrentFuture,
				t.ChangeCurrentFuturePct = s.ChangeCurrentFuturePct,
				t.OpeningsCurrentFuture = s.OpeningsCurrentFuture,
				t.EarningsPctl10 = s.EarningsPctl10,
				t.EarningsPctl25 = s.EarningsPctl25,
				t.EarningsPctl50 = s.EarningsPctl50,
				t.EarningsPctl75 = s.EarningsPctl75
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					RegionCode,
					RegionName,
					Soc,
					YearPast,
					YearCurrent,
					YearFuture,
					EmploymentPast,
					EmploymentCurrent,
					ChangePastCurrent,
					ChangePastCurrentPct,
					ChangeCurrentFuture,
					ChangeCurrentFuturePct,
					OpeningsCurrentFuture,
					EarningsPctl10,
					EarningsPctl25,
					EarningsPctl50,
					EarningsPctl75
				)
			VALUES
				(
					@RegionCode,
					s.RegionName,
					s.Soc,
					@YearPast,
					@YearCurrent,
					@YearFuture,
					s.EmploymentPast,
					s.EmploymentCurrent,
					s.ChangePastCurrent,
					s.ChangePastCurrentPct,
					s.ChangeCurrentFuture,
					s.ChangeCurrentFuturePct,
					s.OpeningsCurrentFuture,
					s.EarningsPctl10,
					s.EarningsPctl25,
					s.EarningsPctl50,
					s.EarningsPctl75
				);';

		SELECT @Sql;
	-- exe dynamic sql
	EXECUTE sp_executesql
		@sql,
		N'@RegionCode char(5),
			@YearCurrent char(4),
			@YearPast char(4),
			@YearFuture char(4)',
		@RegionCode = @RegionCode,
		@YearCurrent = @YearCurrent,
		@YearPast = @YearPast,
		@YearFuture = @YearFuture;
END;