USE calpass;

GO

IF (object_id('dbo.CCFinAidAwardsProdEscrow') is not null)
	BEGIN
		DROP PROCEDURE dbo.CCFinAidAwardsProdEscrow;
	END;

GO

CREATE PROCEDURE
	dbo.CCFinAidAwardsProdEscrow
	(
		@Escrow comis.Escrow READONLY
	)
AS

-- Output rows for dbo.CCFinAidAwardsProd data type

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@EscrowName sysname = 'CCFinAidAwardsProd';

BEGIN
	SELECT
		CollegeId = e.CollegeIpedsCode,
		StudentComisId = e.StudentLocalId,
		StudentId = e.StudentSocialId,
		IdStatus = e.IdStatus,
		TermId = sf.term_id,
		TermRecd = sf.term_recd,
		TypeId = sf.type_id,
		Amount = sf.amount
	FROM
		@Escrow e
		inner join
		comis.sfawards sf
			on sf.college_id = e.CollegeId
			and sf.student_id = e.StudentLocalId
			and sf.term_id = e.TermId
	WHERE
		e.EscrowName = @EscrowName;
END;