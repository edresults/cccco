USE calpass;

GO

IF (object_id('dbo.K12PrgProdMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.K12PrgProdMerge;
	END;

GO

CREATE PROCEDURE
	dbo.K12PrgProdMerge
	(
		@Escrow calpads.Escrow READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Message nvarchar(2048),
	@ErrorCode int = 70099,
	@ErrorSeverity int = 1,
	@EscrowName sysname = 'K12PrgProd';

BEGIN
	BEGIN TRY
		MERGE
			dbo.K12PrgProd
		WITH
			(
				TABLOCKX
			) t
		USING 
			(
				SELECT
					Derkey1,
					School,
					AcYear,
					LocStudentId,
					ProgramCode,
					MembershipCode,
					MembershipStartDate,
					MembershipEndDate,
					ServiceYearCode,
					ServiceCode,
					AcademyId,
					MigrantId,
					DisabilityCode,
					AccountabilityDistrictCode,
					DwellingCode,
					IsHomeless,
					IsRunaway,
					DateAdded
				FROM
					(
						SELECT
							Derkey1 = e.InterSegmentKey,
							School = p.DistrictCode + p.SchoolCode,
							AcYear = y.YearCodeAbbr,
							LocStudentId = calpass.dbo.get1289Encryption(f.StudentLocalId, 'X'),
							p.ProgramCode,
							p.MembershipCode,
							p.MembershipStartDate,
							p.MembershipEndDate,
							p.ServiceYearCode,
							p.ServiceCode,
							p.AcademyId,
							p.MigrantId,
							p.DisabilityCode,
							p.AccountabilityDistrictCode,
							p.DwellingCode,
							p.IsHomeless,
							p.IsRunaway,
							DateAdded = getdate(),
							RecordCount = row_number() over(
									partition by
										p.DistrictCode,
										p.SchoolCode,
										calpass.dbo.Get1289Encryption(f.StudentLocalId, 'X'),
										p.YearCode,
										p.ProgramCode,
										p.MembershipStartDate,
										p.ServiceYearCode,
										p.ServiceCode
									order by
										(select 1) desc
								)
						FROM
							@Escrow e
							inner join
							calpads.Sprg p with(index(pk_Sprg))
								on p.StudentStateId = e.StudentStateId
								and p.SchoolCode = e.SchoolCode
								and p.YearCode = e.YearCode
							inner join
							calpads.Year y with(index(PK_Year))
								on y.YearCode = p.YearCode
							inner join
							calpads.Sinf f with(index(pk_Sinf))
								on e.StudentStateId = f.StudentStateId
								and e.SchoolCode = f.SchoolCode
								and e.SinfYearCode = f.YearCode
								and e.SinfEffectiveStartDate = f.EffectiveStartDate
						WHERE
							e.EscrowName = @EscrowName
							and f.StudentLocalId is not null
					) a
				WHERE
					a.RecordCount = 1
			) s
		ON
			t.School = s.School
			and t.LocStudentId = s.LocStudentId
			and t.AcYear = s.AcYear
			and t.ProgramCode = s.ProgramCode
			and t.MembershipStartDate = s.MembershipStartDate
			and t.ServiceYearCode = s.ServiceYearCode
			and t.ServiceCode = s.ServiceCode
		WHEN MATCHED THEN
			UPDATE SET
				t.Derkey1 = s.Derkey1,
				t.MembershipCode = s.MembershipCode,
				t.MembershipEndDate = s.MembershipEndDate,
				t.AcademyId = s.AcademyId,
				t.MigrantId = s.MigrantId,
				t.DisabilityCode = s.DisabilityCode,
				t.AccountabilityDistrictCode = s.AccountabilityDistrictCode,
				t.DwellingCode = s.DwellingCode,
				t.IsHomelessUnaccompanied = s.IsHomeless,
				t.IsRunaway = s.IsRunaway,
				t.DateAdded = s.DateAdded
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					Derkey1,
					School,
					AcYear,
					LocStudentId,
					ProgramCode,
					MembershipCode,
					MembershipStartDate,
					MembershipEndDate,
					ServiceYearCode,
					ServiceCode,
					AcademyId,
					MigrantId,
					DisabilityCode,
					AccountabilityDistrictCode,
					DwellingCode,
					IsHomelessUnaccompanied,
					IsRunaway,
					DateAdded
				)
			VALUES
				(
					s.Derkey1,
					s.School,
					s.AcYear,
					s.LocStudentId,
					s.ProgramCode,
					s.MembershipCode,
					s.MembershipStartDate,
					s.MembershipEndDate,
					s.ServiceYearCode,
					s.ServiceCode,
					s.AcademyId,
					s.MigrantId,
					s.DisabilityCode,
					s.AccountabilityDistrictCode,
					s.DwellingCode,
					s.IsHomeless,
					s.IsRunaway,
					s.DateAdded
				);
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message =
			nchar(9) + N'Error Procedure: ' + upper(object_name(@@PROCID)) + char(13) + char(10) +
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		-- put error message
		THROW @ErrorCode, @Message, @ErrorSeverity;
	END CATCH;
END;