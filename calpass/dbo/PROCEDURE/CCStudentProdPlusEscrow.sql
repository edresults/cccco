USE calpass;

GO

IF (object_id('dbo.CCStudentProdPlusEscrow') is not null)
	BEGIN
		DROP PROCEDURE dbo.CCStudentProdPlusEscrow;
	END;

GO

CREATE PROCEDURE
	dbo.CCStudentProdPlusEscrow
	(
		@Escrow comis.Escrow READONLY
	)
AS

-- Output rows for dbo.CCStudentProdPlus data type

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@EscrowName sysname = 'CCStudentProd';

BEGIN
	SELECT
		Derkey1 = e.InterSegmentKey,
		CollegeId = e.CollegeIpedsCode,
		StudentId = StudentSocialId,
		IdStatus = e.IdStatus,
		TermId = st.term_id,
		CSISNum = null,
		Birthdate = convert(char(8), coalesce(sb.birthdate, DecryptByKey(sb.birthdate_enc)), 112),
		FName = convert(char(3), coalesce(st.name_first, DecryptByKey(st.name_first_enc))),
		LName = convert(char(3), coalesce(st.name_last, DecryptByKey(st.name_last_enc))),
		Gender = convert(char(1), coalesce(st.gender, DecryptByKey(st.gender_enc))),
		Race = convert(char(1), coalesce(st.ipeds_race, DecryptByKey(st.ipeds_race_enc))),
		Citizen = st.citizenship,
		ZipCode = st.zip,
		EdStatus = st.education,
		HighSchool = st.high_school,
		StudentGoal = st.goal,
		EnrollStatus = st.enrollment,
		UnitsEarnedLoc = st.u_earned_loc,
		UnitsEarnedTrn = st.u_earned_trn,
		UnitsAttLoc = st.u_attmpt_loc,
		UnitsAttTrn = st.u_attmpt_trn,
		GPointsLoc = st.g_points_loc,
		GPointsTrn = st.g_points_trn,
		AcademicStanding = st.aca_standing,
		AcademicLevel = st.academic_level,
		DSPS = isnull(sd.primary_disability, 'Y'),
		EOPS = 
			isnull(
				case
					when se.eops_care_status = 'N' then 'E'
					when se.eops_care_status = 'C' then 'C'
					when se.eops_care_status = 'P' then 'Y'
				end,
				'Y'
			),
		BoggFlag = 
			isnull(
				(
					SELECT DISTINCT
						'B'
					FROM
						comis.sfawards sf
					WHERE
						sf.type_id in ('BA','B1','B2','B3','BB','BC','F1','F2','F3','F4','F5')
						and sf.college_id = st.college_id
						and sf.student_id = st.student_id
						and sf.term_recd = st.term_id
				),
				'Y'
			),
		-- cannot use left outer join when there should be a 1:1 match on primary key.
		-- CO does not validate data
		-- this event Should not exist
		-- annual term (100) with 105 term.
		PellFlag = 
			isnull(
				(
					SELECT DISTINCT
						'P'
					FROM
						comis.sfawards sf
					WHERE
						sf.type_id  = 'GP'
						and sf.college_id = st.college_id
						and sf.student_id = st.student_id
						and sf.term_recd = st.term_id
				),
				'Y'
			),
		FinancialAidFlag = 
			isnull(
				(
					SELECT
						min(
							case
								when sf.type_id in ('GS','GW','LD','LG','LS','WF') then 'A'
								when sf.type_id in ('GB','GC','GE','GF','LE','WC','WE','WU') then 'B'
								when sf.type_id in ('LH','LP','LR','LL') then 'C'
								when sf.type_id in ('SU','SV','SX') then 'D'
								when sf.type_id in ('GU', 'GV', 'LI', 'LN', 'GG') then 'E'
								else 'Y'
							end
						)
					FROM
						comis.sfawards sf
					WHERE
						sf.college_id = st.college_id
						and sf.student_id = st.student_id
						and sf.term_recd = st.term_id
					GROUP BY
						sf.college_id,
						sf.student_id,
						sf.term_recd
				),
				'Y'
			),
		Major = isnull(sm.Major, 'XXXXXX'),
		ccLongTermId = t.YearTermCode,
		sg.military,
		sg.military_dependent,
		sg.foster_care,
		sg.incarcerated,
		sg.mesa,
		sg.puente,
		sg.mchs,
		sg.umoja,
		sg.gen_first,
		sg.caa,
		-- extended version
		extSB09_ResidenceCode = st.residency,
		extSB23_ApprenticeshipStatus = st.apprentice,
		extSB24_TransferCenterStatus = st.transfer_ctr,
		extSB26_JTPAStatus = st.jtpa_status,
		extSB27_CalWorksStatus = st.calworks_status,
		extSM01_MatricGoals = sm.goals,
		extSM01_MatricGoalsPos1 = sm.goals_primary,
		extSM01_MatricGoalsPos2 = sm.goals_secondary,
		extSM01_MatricGoalsPos2 = sm.goals_secondary,
		extSM01_MatricGoalsPos3 = sm.goals_tertiary,
		extSM01_MatricGoalsPos4 = sm.goals_placeholder,
		extSM07_MatricOrientationServices =sm.orientation_services,
		extSM08_MatricAssessmentServicesPlacement = sm.assessment_services_place,
		extSM09_MatricAssessmentServicesOther = sm.assessment_services_other,
		extSM09_MatricAssessmentServicesOtherPos1 = sm.assessment_services_other_01,
		extSM09_MatricAssessmentServicesOtherPos2 = sm.assessment_services_other_02,
		extSM09_MatricAssessmentServicesOtherPos3 = sm.assessment_services_other_03,
		extSM12_MatricCounAdviseServices = sm.advisement_services,
		extSM13_MatricAcademicFollowupSerives = sm.follow_up_services,
		extSVO1_ProgramPlanStatus = sv.voc_pgm_plan,
		extSV03_EconomicallyDisadvStatus = sv.econ_disadv,
		extSV03_EconomicallyDisadvStatusPos1 = sv.econ_disadv_status,
		extSV03_EconomicallyDisadvStatusPos2 = sv.econ_disadv_source,
		extSV04_SingleParentStatus = sv.single_parent,
		extSV05_DisplacedHomemakerStatus = sv.displ_homemker,
		extSV06_CoopWorkExperienceEdType = sv.coop_work_exp,
		extSV08_TechPrepStatus = sv.tech_prep,
		extSV09_MigrantWorkerStatus = sv.migrant_worker,
		extSCD1_FirstTermAttended = sb.term_first,
		extSCD2_LastTermAttended = sb.term_last,
		extSCD4_LEP = sb.lep_flag,
		extSCD5_AcademicallyDisadvantagedStudent = sb.acad_disad_flag,
		extSTD1_AgeAtTerm = st.age_at_term,
		extSTD2_1stCensusCreditLoad = st.census_crload,
		extSTD3_DayEveningClassCode = st.day_evening_code,
		extSTD5_DegreeAppUnitsEarned = st.deg_appl_units_earned,
		extSTD6_DayEveningClassCode2 = st.day_evening_code2,
		extSTD7_HeadCountStatus = st.headcount_status,
		extSTD8_LocalCumGPA = st.gpa_loc,
		extSTD9_TotalCumGPA = st.gpa_tot,
		extSD01_StudentPrimaryDisability = sd.primary_disability,
		extSD03_StudentSecondaryDisability = sd.secondary_disability,
		extSB29_MultiEthnicity = st.multi_race,
		extSTD10_IPESEthnicity = st.ipeds_race
	FROM
		@Escrow e
		inner join
		comis.stterm st
			on e.CollegeId = st.college_id
			and e.StudentLocalId = st.student_id
			and e.TermId = st.term_id
		inner join
		comis.sbstudnt sb
			on st.college_id = sb.college_id
			and st.student_id = sb.student_id
		inner join
		comis.Term t
			on t.TermCode = st.term_id
		left outer join
		comis.sddsps sd
			on sd.college_id = st.college_id
			and sd.student_id = st.student_id
			and sd.term_id = st.term_id
		left outer join
		comis.seeops se
			on se.college_id = st.college_id
			and se.student_id = st.student_id
			and se.term_id = st.term_id
		left outer join
		comis.smmatric sm
			on sm.college_id = st.college_id
			and sm.student_id = st.student_id
			and sm.term_id = st.term_id
		left outer join
		comis.sgpops sg
			on sg.college_id = st.college_id
			and sg.student_id = st.student_id
			and sg.term_id = st.term_id
		left outer join
		comis.svvatea sv
			on sv.college_id = st.college_id
			and sv.student_id = st.student_id
			and sv.term_id = st.term_id
	WHERE
		e.EscrowName = @EscrowName;
END;