USE calpass;

GO

IF (object_id('mmap.K12CourseContentMerge') is not null)
	BEGIN
		DROP PROCEDURE mmap.K12CourseContentMerge;
	END;

GO

CREATE PROCEDURE
	mmap.K12CourseContentMerge
	(
		@Escrow calpads.Escrow READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Message       nvarchar(2048),
	@ErrorCode     int = 70099,
	@ErrorSeverity int = 1;

BEGIN

	SELECT DISTINCT
		StudentStateId,
		CSISNum = dbo.Get9812Encryption(StudentStateId, 'X')
	INTO
		#Escrow
	FROM
		@Escrow;

	DELETE
		a
	FROM
		#Escrow a
	WHERE
		exists (
			SELECT
				1
			FROM
				#Escrow b
			WHERE
				a.StudentStateId = b.StudentStateId
			HAVING
				count(*) > 1
		);

	BEGIN TRY
		MERGE
			mmap.K12CourseContent
		WITH
			(
				TABLOCKX
			) t
		USING
			(
				SELECT
					i.StudentStateId,
					rp.DepartmentCode,
					rp.GradeCode,
					rp.SchoolCode,
					rp.YearTermCode,
					rp.TermCode,
					rp.ContentCode,
					rp.ContentRank,
					rp.CourseCode,
					rp.CourseUnits,
					rp.CourseTitle,
					rp.CourseAGCode,
					rp.CourseLevelCode,
					rp.CourseTypeCode,
					rp.SectionMark,
					rp.MarkPoints,
					rp.MarkCategory,
					rp.IsSuccess,
					rp.IsPromoted,
					rp.ContentSelector,
					rp.GradeSelector,
					rp.RecencySelector
				FROM
					#Escrow i
					cross apply
					Mmap.K12CourseContentGet(i.CSISNum) rp
			) s
		ON
			(
				s.StudentStateId = t.StudentStateId
				and s.DepartmentCode = t.DepartmentCode
				and s.RecencySelector = t.RecencySelector
			)
		WHEN MATCHED THEN
			UPDATE SET
				t.GradeCode       = s.GradeCode,
				t.SchoolCode      = s.SchoolCode,
				t.YearTermCode    = s.YearTermCode,
				t.TermCode        = s.TermCode,
				t.ContentCode     = s.ContentCode,
				t.CourseCode      = s.CourseCode,
				t.ContentRank     = s.ContentRank,
				t.CourseUnits     = s.CourseUnits,
				t.CourseTitle     = s.CourseTitle,
				t.CourseAGCode    = s.CourseAGCode,
				t.CourseLevelCode = s.CourseLevelCode,
				t.CourseTypeCode  = s.CourseTypeCode,
				t.SectionMark     = s.SectionMark,
				t.MarkPoints      = s.MarkPoints,
				t.MarkCategory    = s.MarkCategory,
				t.IsSuccess       = s.IsSuccess,
				t.IsPromoted      = s.IsPromoted,
				t.ContentSelector = s.ContentSelector,
				t.GradeSelector   = s.GradeSelector
		WHEN NOT MATCHED THEN
			INSERT
				(
					StudentStateId,
					DepartmentCode,
					GradeCode,
					SchoolCode,
					YearTermCode,
					TermCode,
					ContentCode,
					ContentRank,
					CourseCode,
					CourseUnits,
					CourseTitle,
					CourseAGCode,
					CourseLevelCode,
					CourseTypeCode,
					SectionMark,
					MarkPoints,
					MarkCategory,
					IsSuccess,
					IsPromoted,
					ContentSelector,
					GradeSelector,
					RecencySelector
				)
			VALUES
				(
					s.StudentStateId,
					s.DepartmentCode,
					s.GradeCode,
					s.SchoolCode,
					s.YearTermCode,
					s.TermCode,
					s.ContentCode,
					s.ContentRank,
					s.CourseCode,
					s.CourseUnits,
					s.CourseTitle,
					s.CourseAGCode,
					s.CourseLevelCode,
					s.CourseTypeCode,
					s.SectionMark,
					s.MarkPoints,
					s.MarkCategory,
					s.IsSuccess,
					s.IsPromoted,
					s.ContentSelector,
					s.GradeSelector,
					s.RecencySelector
				);
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message =
			nchar(9) + N'Error Procedure: ' + upper(object_name(@@PROCID)) + char(13) + char(10) +
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		-- put error message
		THROW @ErrorCode, @Message, @ErrorSeverity;
	END CATCH;
END;