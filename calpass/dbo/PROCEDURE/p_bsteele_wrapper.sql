USE calpass;

GO

IF (object_id('calpass.dbo.p_bsteele_wrapper', 'P') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_bsteele_wrapper;
	END;

GO

CREATE PROCEDURE
	dbo.p_bsteele_wrapper
	(
		@user nvarchar(255),
		@password nvarchar(255),
		@server nvarchar(255),
		@ssh_rsa nvarchar(255),
		@port nvarchar(255),
		@segment_code varchar(3)
	)
AS

	SET NOCOUNT ON;

BEGIN

	IF (upper(@segment_code) = 'ALL')
		BEGIN
			-- get cohorts
			EXECUTE calpass.dbo.p_bsteele_cohort_insert;
			
			-- extract
			EXECUTE xp_cmdshell
				'bcp "SELECT * FROM calpass.dbo.bsteele_cohort_msj" QUERYOUT "C:\Users\dlamoree\Desktop\bsteele_cohort_msj.txt" -T -c -q';
			-- upload
			EXECUTE sp_sftp_winscp_upload
				@user = @user,
				@password = @password,
				@server = @server,
				@port = @port,
				@ssh_rsa = @ssh_rsa,
				@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
				@source_file_name = 'bsteele_cohort_msj.txt',
				@target_file_dir = '/home/bsteele/'
			-- delete
			EXECUTE master.dbo.clr_file_delete
				@file_path = 'C:\Users\dlamoree\desktop\bsteele_cohort_msj.txt';
			-- extract
			EXECUTE dbo.sp_spss_output
				'calpass',
				'dbo',
				'bsteele_cohort_msj',
				'C:\Users\dlamoree\desktop\',  -- '
				'1';
			-- upload
			EXECUTE sp_sftp_winscp_upload
				@user = @user,
				@password = @password,
				@server = @server,
				@port = @port,
				@ssh_rsa = @ssh_rsa,
				@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
				@source_file_name = 'bsteele_cohort_msj_spss_import.sps',
				@target_file_dir = '/home/bsteele/'
			-- delete
			EXECUTE master.dbo.clr_file_delete
				@file_path = 'C:\Users\dlamoree\desktop\bsteele_cohort_msj_spss_import.sps';
		END;

	IF (upper(@segment_code) = 'HS' or upper(@segment_code) = 'ALL')
		BEGIN
			-- get high school transcripts for cohorts
			EXECUTE calpass.dbo.p_bsteele_cohort_hs_insert;
			-- extract
			EXECUTE xp_cmdshell
				'bcp "SELECT * FROM calpass.dbo.bsteele_cohort_hs" QUERYOUT "C:\Users\dlamoree\Desktop\bsteele_cohort_hs.txt" -T -c -q';
			-- upload
			EXECUTE sp_sftp_winscp_upload
				@user = @user,
				@password = @password,
				@server = @server,
				@port = @port,
				@ssh_rsa = @ssh_rsa,
				@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
				@source_file_name = 'bsteele_cohort_hs.txt',
				@target_file_dir = '/home/bsteele/'
			-- delete
			EXECUTE master.dbo.clr_file_delete
				@file_path = 'C:\Users\dlamoree\desktop\bsteele_cohort_hs.txt';
			-- extract
			EXECUTE dbo.sp_spss_output
				'calpass',
				'dbo',
				'bsteele_cohort_hs',
				'C:\Users\dlamoree\Desktop\',  -- '
				'1';
			-- upload
			EXECUTE sp_sftp_winscp_upload
				@user = @user,
				@password = @password,
				@server = @server,
				@port = @port,
				@ssh_rsa = @ssh_rsa,
				@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
				@source_file_name = 'bsteele_cohort_hs_spss_import.sps',
				@target_file_dir = '/home/bsteele/';
			-- delete
			EXECUTE master.dbo.clr_file_delete
				@file_path = 'C:\Users\dlamoree\desktop\bsteele_cohort_hs_spss_import.sps';
		END;

	IF (upper(@segment_code) = 'CST' or upper(@segment_code) = 'ALL')
		BEGIN
			-- get cst transcripts for cohorts
			EXECUTE calpass.dbo.p_bsteele_cohort_cst_insert;

			EXECUTE xp_cmdshell
				'bcp "SELECT * FROM calpass.dbo.bsteele_cohort_cst" QUERYOUT "C:\Users\dlamoree\Desktop\bsteele_cohort_cst.txt" -T -c -q';

			EXECUTE sp_sftp_winscp_upload
				@user = @user,
				@password = @password,
				@server = @server,
				@port = @port,
				@ssh_rsa = @ssh_rsa,
				@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
				@source_file_name = 'bsteele_cohort_cst.txt',
				@target_file_dir = '/home/bsteele/';

			EXECUTE master.dbo.clr_file_delete
				@file_path = 'C:\Users\dlamoree\desktop\bsteele_cohort_cst.txt';

			EXECUTE dbo.sp_spss_output
				'calpass',
				'dbo',
				'bsteele_cohort_cst',
				'C:\Users\dlamoree\desktop\',  -- '
				'1';

			EXECUTE sp_sftp_winscp_upload
				@user = @user,
				@password = @password,
				@server = @server,
				@port = @port,
				@ssh_rsa = @ssh_rsa,
				@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
				@source_file_name = 'bsteele_cohort_cst_spss_import.sps',
				@target_file_dir = '/home/bsteele/'

			EXECUTE master.dbo.clr_file_delete
				@file_path = 'C:\Users\dlamoree\desktop\bsteele_cohort_cst_spss_import.sps';

		END;

	IF (upper(@segment_code) = 'CC' or upper(@segment_code) = 'ALL')
		BEGIN
			-- get cc transcripts for cohorts
			EXECUTE calpass.dbo.p_bsteele_cohort_cc_insert;
			-- extract
			EXECUTE xp_cmdshell
				'bcp "SELECT * FROM calpass.dbo.bsteele_cohort_cc" QUERYOUT "C:\Users\dlamoree\Desktop\bsteele_cohort_cc.txt" -T -c -q';
			-- upload
			EXECUTE sp_sftp_winscp_upload
				@user = @user,
				@password = @password,
				@server = @server,
				@port = @port,
				@ssh_rsa = @ssh_rsa,
				@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
				@source_file_name = 'bsteele_cohort_cc.txt',
				@target_file_dir = '/home/bsteele/'
			-- delete
			EXECUTE master.dbo.clr_file_delete
				@file_path = 'C:\Users\dlamoree\desktop\bsteele_cohort_cc.txt';
			-- extract
			EXECUTE dbo.sp_spss_output
				'calpass',
				'dbo',
				'bsteele_cohort_cc',
				'C:\Users\dlamoree\desktop\',  -- '
				'1';
			-- upload
			EXECUTE sp_sftp_winscp_upload
				@user = @user,
				@password = @password,
				@server = @server,
				@port = @port,
				@ssh_rsa = @ssh_rsa,
				@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
				@source_file_name = 'bsteele_cohort_cc_spss_import.sps',
				@target_file_dir = '/home/bsteele/'
			-- delete
			EXECUTE master.dbo.clr_file_delete
				@file_path = 'C:\Users\dlamoree\desktop\bsteele_cohort_cc_spss_import.sps';
		END;

END;