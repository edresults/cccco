USE calpass;

GO

IF (object_id('dbo.K12AwardProdMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.K12AwardProdMerge;
	END;

GO

CREATE PROCEDURE
	dbo.K12AwardProdMerge
	(
		@Escrow calpads.Escrow READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Message nvarchar(2048),
	@ErrorCode int = 70099,
	@ErrorSeverity int = 1,
	@EscrowName sysname = 'K12AwardProd';

BEGIN
	BEGIN TRY
		MERGE
			dbo.K12AwardProd
		WITH
			(
				TABLOCKX
			) t
		USING
			(
				SELECT
					Derkey1,
					School,
					LocStudentId,
					AcYear,
					AwardType,
					AwardDate,
					DateAdded
				FROM
					(
						SELECT
							Derkey1 = e.InterSegmentKey,
							School = r.DistrictCode + r.SchoolCode,
							LocStudentId = calpass.dbo.Get1289Encryption(f.StudentLocalId, 'X'),
							AcYear = y.YearCodeAbbr,
							AwardType = r.CompletionStatusCode,
							AwardDate = r.EnrollExitDate,
							DateAdded = getdate(),
							RecordCount = row_number() over(
									partition by
										r.DistrictCode,
										r.SchoolCode,
										calpass.dbo.Get1289Encryption(f.StudentLocalId, 'X'),
										r.YearCode,
										r.CompletionStatusCode,
										r.EnrollExitDate
									order by
										(select 1) desc
								)
						FROM
							@Escrow e
							inner join
							calpads.Senr r with(index(pk_Senr))
								on e.StudentStateId = r.StudentStateId
								and e.SchoolCode = r.SchoolCode
								and e.YearCode = r.YearCode
							inner join
							calpads.Sinf f with(index(pk_Sinf))
								on e.StudentStateId = f.StudentStateId
								and e.SchoolCode = f.SchoolCode
								and e.SinfYearCode = f.YearCode
								and e.SinfEffectiveStartDate = f.EffectiveStartDate
							inner join
							calpads.Year y with(index(PK_Year))
								on y.YearCode = r.YearCode
						WHERE
							e.EscrowName = @EscrowName
							and r.EnrollStartDate = (
								SELECT
									max(r1.EnrollStartDate)
								FROM
									calpads.Senr r1 with(index(pk_Senr))
								WHERE
									r1.StudentStateId = r.StudentStateId
									and r1.SchoolCode = r.SchoolCode
									and r1.YearCode = r.YearCode
							)
							and r.CompletionStatusCode not in ('480', '360', '   ')
							and r.EnrollExitDate is not null
							and f.StudentLocalId is not null
					) a
				WHERE
					a.RecordCount = 1
			) s
		ON
			s.School = t.School
			and s.LocStudentId = t.LocStudentId
			and s.AcYear = t.AcYear
			and s.AwardType = t.AwardType
			and s.AwardDate = t.AwardDate
		WHEN MATCHED THEN
			UPDATE SET
				t.Derkey1 = s.Derkey1,
				t.DateAdded = s.DateAdded
		WHEN NOT MATCHED THEN
			INSERT
				(
					Derkey1,
					School,
					LocStudentId,
					AcYear,
					AwardType,
					AwardDate,
					DateAdded
				)
			VALUES
				(
					s.Derkey1,
					s.School,
					s.LocStudentId,
					s.AcYear,
					s.AwardType,
					s.AwardDate,
					s.DateAdded
				);
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message =
			nchar(9) + N'Error Procedure: ' + upper(object_name(@@PROCID)) + char(13) + char(10) +
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		-- put error message
		THROW @ErrorCode, @Message, @ErrorSeverity;
	END CATCH;
END;