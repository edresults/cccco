USE calpass;

GO

IF (object_id('dbo.p_emsi_wages_merge') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_emsi_wages_merge;
	END;

GO

CREATE PROCEDURE
	dbo.p_emsi_wages_merge
	(
		@year_curr smallint,
		@source_file_path nvarchar(255),
		@format_file_path nvarchar(255) = N'C:\data\cc\emsi\formats\emsi_wages.fmt',
		@first_row int = 2
	)
AS

	SET NOCOUNT ON;

DECLARE
	
	@sql nvarchar(max),
	@tab_needle varchar(255) = char(13) + char(10) + char(9) + char(9),
	@tab_replace varchar(255) = char(13) + char(10);

BEGIN

	SET @sql = replace(N'
		MERGE
			dbo.emsi_wages t
		USING
			(
				SELECT
					region,
					soc = replace(soc, char(39), ''''),
					year_curr = @year_curr,
					year_past = @year_curr - 5,
					year_next = @year_curr + 5,
					employment_past,
					employment_curr,
					change_numeric_past_to_curr,
					change_percent_past_to_curr,
					change_numeric_curr_to_next,
					change_percent_curr_to_next,
					openings_curr_to_next,
					earnings_percent_10,
					earnings_percent_25,
					earnings_percent_50,
					earnings_percent_75
				FROM
					openrowset(
						bulk N''' + @source_file_path + ''',
						formatfile = N''' + @format_file_path + ''',
						firstrow = ' + convert(varchar, @first_row) + '
					) a
			) s
		ON
			(
				t.region = s.region
				and t.soc = s.soc
				and t.year_curr = s.year_curr
			)
		WHEN MATCHED THEN
			UPDATE SET
				t.employment_past = s.employment_past,
				t.employment_curr = s.employment_curr,
				t.change_numeric_past_to_curr = s.change_numeric_past_to_curr,
				t.change_percent_past_to_curr = s.change_percent_past_to_curr,
				t.change_numeric_curr_to_next = s.change_numeric_curr_to_next,
				t.change_percent_curr_to_next = s.change_percent_curr_to_next,
				t.openings_curr_to_next = s.openings_curr_to_next,
				t.earnings_percent_10 = s.earnings_percent_10,
				t.earnings_percent_25 = s.earnings_percent_25,
				t.earnings_percent_50 = s.earnings_percent_50,
				t.earnings_percent_75 = s.earnings_percent_75
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					region,
					soc,
					year_curr,
					year_past,
					year_next,
					employment_past,
					employment_curr,
					change_numeric_past_to_curr,
					change_percent_past_to_curr,
					change_numeric_curr_to_next,
					change_percent_curr_to_next,
					openings_curr_to_next,
					earnings_percent_10,
					earnings_percent_25,
					earnings_percent_50,
					earnings_percent_75
				)
			VALUES
				(
					region,
					soc,
					year_curr,
					year_past,
					year_next,
					employment_past,
					employment_curr,
					change_numeric_past_to_curr,
					change_percent_past_to_curr,
					change_numeric_curr_to_next,
					change_percent_curr_to_next,
					openings_curr_to_next,
					earnings_percent_10,
					earnings_percent_25,
					earnings_percent_50,
					earnings_percent_75
				);', @tab_needle, @tab_replace
	);

	EXECUTE sp_executesql
		@sql,
		N'@year_curr smallint',
		@year_curr = @year_curr;

END;