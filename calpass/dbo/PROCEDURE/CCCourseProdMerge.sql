USE calpass;

GO

IF (object_id('dbo.CCCourseProdMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.CCCourseProdMerge;
	END;

GO

CREATE PROCEDURE
	dbo.CCCourseProdMerge
	(
		@CCCourseProdPlus dbo.CCCourseProdPlus READONLY
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Severity tinyint = 0,
	@State tinyint = 1,
	@RowCount varchar(2048),
	@Error varchar(2048);

BEGIN
	MERGE
		dbo.CCCourseProd t
	USING
		@CCCourseProdPlus s
	ON
		t.CollegeId = s.CollegeId
		and t.StudentId = s.StudentId
		and t.IdStatus = s.IdStatus
		and t.TermId = s.TermId
		and t.CourseId = s.CourseId
		and t.SectionId = s.SectionId
	WHEN MATCHED THEN
		UPDATE SET
			t.StudentComisId = s.StudentComisId,
			t.Title = s.Title,
			t.UnitsMax = s.UnitsMax,
			t.UnitsAttempted = s.UnitsAttempted,
			t.UnitsEarned = s.UnitsEarned,
			t.Grade = s.Grade,
			t.CreditFlag = s.CreditFlag,
			t.TopCode = s.TopCode,
			t.TransferStatus = s.TransferStatus,
			t.BSStatus = s.BSStatus,
			t.SamCode = s.SamCode,
			t.ClassCode = s.ClassCode,
			t.CollegeLevel = s.CollegeLevel,
			t.NCRCategory = s.NCRCategory,
			t.CCLongTermId = s.CCLongTermId
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				CourseId,
				SectionId,
				Title,
				UnitsMax,
				UnitsAttempted,
				UnitsEarned,
				Grade,
				CreditFlag,
				TopCode,
				TransferStatus,
				BSStatus,
				SamCode,
				ClassCode,
				CollegeLevel,
				NCRCategory,
				CCLongTermId
			)
		VALUES
			(
				s.CollegeId,
				s.StudentComisId,
				s.StudentId,
				s.IdStatus,
				s.TermId,
				s.CourseId,
				s.SectionId,
				s.Title,
				s.UnitsMax,
				s.UnitsAttempted,
				s.UnitsEarned,
				s.Grade,
				s.CreditFlag,
				s.TopCode,
				s.TransferStatus,
				s.BSStatus,
				s.SamCode,
				s.ClassCode,
				s.CollegeLevel,
				s.NCRCategory,
				s.CCLongTermId
			);
	-- capture
	SET @RowCount = convert(varchar(2048), @@ROWCOUNT);
	-- set
	SET @Error = 'Merged ' + @RowCount + ' dbo.CCCourseProd records';
	-- output
	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;
END;