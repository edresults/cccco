USE calpass;

GO

IF (object_id('dbo.p_OrgId') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_OrgId;
	END;

GO

CREATE PROCEDURE
	dbo.p_OrgId
	(
		@OrganizationName nvarchar(4000)
	)
AS
BEGIN
	SELECT
		*
	FROM
		calpass.dbo.Organization
	WHERE
		OrganizationName like '%' + @OrganizationName + '%';
END;