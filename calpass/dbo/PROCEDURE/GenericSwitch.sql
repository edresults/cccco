USE calpass;

GO

IF (object_id('dbo.GenericSwitch') is not null)
	BEGIN
		DROP PROCEDURE dbo.GenericSwitch;
	END;

GO

CREATE PROCEDURE
	dbo.GenericSwitch
	(
		@SourceSchemaTableName nvarchar(517),
		@StageSchemaTableName nvarchar(517)
	)
AS

DECLARE
	-- variables
	@Message nvarchar(2048),
	@Sql nvarchar(max),
	@Command nvarchar(max);

BEGIN
	-- validate objects are not the same
	IF (@SourceSchemaTableName = @StageSchemaTableName)
		BEGIN
			SET @Message = N'Objects equivalent';
			THROW 70099, @Message, 1;
		END;
	-- validate source table
	IF (object_id(@SourceSchemaTableName) is null)
		BEGIN
			SET @Message = N'Source table not found';
			THROW 70099, @Message, 1;
		END;
	-- validate stage table
	IF (object_id(@StageSchemaTableName) is null)
		BEGIN
			SET @Message = N'Stage table not found';
			THROW 70099, @Message, 1;
		END;

	EXECUTE dbo.CloneIX
		@QualifiedFullName = @SourceSchemaTableName,
		@Command = @Command out;

	-- drop table
	SET	@Sql = 'DROP TABLE ' + @SourceSchemaTableName + ';';
	-- ext sql
	EXECUTE sp_executesql
		@Sql;

	SET @SourceSchemaTableName = parsename(@SourceSchemaTableName, 1);
	
	-- rename stage table to source table
	EXECUTE sp_rename
		@objname = @StageSchemaTableName,
		@newname = @SourceSchemaTableName

	-- rebuild indexes
	EXECUTE sp_executesql
		@Command;

END;