DECLARE
	@UserId                      int              = 5552,
	@FileFolder                  varchar(255)     = '\\10.11.6.43\C$\Data\CC\COMIS\Processing\',
	@OrganizationId              int              = 175722,
	@BatchId                     uniqueidentifier = 'CC49665E-1DCC-409A-A3A7-51A912376197',
	@SubmissionFileDescriptionId int              = 13;

INSERT
	[PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedrop
	(
		OrganizationId,
		BatchId,
		SubmissionDateTime,
		ProcessedDateTime,
		Status,
		FileLocation,
		OriginalFileName,
		TempFileName,
		ErrorMessage,
		UserId,
		SubmissionFileSource,
		Removed,
		SubmissionFileDetails,
		SubmissionFileDescriptionId
	)
SELECT
	OrganizationId              = @OrganizationId,
	BatchId                     = @BatchId,
	SubmissionDateTime          = GETDATE(),
	ProcessedDateTime           = null,
	Status                      = 'TDB',
	FileLocation                = @FileFolder,
	OriginalFileName            = CONVERT(char(36), @BatchId) + '.' + FileName,
	TempFileName                = @FileFolder + CONVERT(char(36), @BatchId) + '.' + FileName,
	ErrorMessage                = null,
	UserId                      = @UserId,
	SubmissionFileSource        = 'Manual',
	Removed                     = null,
	SubmissionFileDetails       = null,
	SubmissionFileDescriptionId = @SubmissionFileDescriptionId
FROM
	(
		VALUES
		('EDDUI_NAICS_2019_1.txt')
	) t
	(
		FileName
	);