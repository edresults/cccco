CREATE PROCEDURE
    swp_remaining
AS

    SET NOCOUNT ON;

DECLARE
    -- Constaints
    @Offset         tinyint        = 10,
    @DateInjection  char(14)       = REPLACE(REPLACE(REPLACE(CONVERT(CHAR(19), GETDATE(), 120), ' ', ''), '-', ''), ':', ''),
    @Now            datetime2      = SYSDATETIME(),
    -- Variables
    @Rank           integer        = 0,
    -- Mail
    @Message        nvarchar(2048) = N'',
    @ProfileName    sysname        = 'Cal-PASS SQL Support Profile',
    @Recipients     varchar(max)   = 'dlamoree@edresults.org;kknox@edresults.org;vmarrero@edresults.org',
    -- @Recipients     varchar(max)   = 'dlamoree@edresults.org',
    @Subject        nvarchar(255)  = N'',
    @Query          nvarchar(max)  = N'',
    @QueryHeader    bit            = 1,
    @QueryNoPadding bit            = 1,
    @QueryAttach    bit            = 1,
    @QueryFileName  nvarchar(255)  = N'',
    @QuerySeparator char(1)        = '	',
    @ExcludeInline  bit            = 1;

BEGIN

    DROP TABLE IF EXISTS #grantee;
    DROP TABLE IF EXISTS #file;
    DROP TABLE IF EXISTS #data;
    DROP TABLE IF EXISTS calpass.dbo.outreach_daily_output;

    CREATE TABLE
        #file
        (
            file_name     char(4) not null,
            academic_year char(9) not null,
            CONSTRAINT pk_#file PRIMARY KEY CLUSTERED ( file_name, academic_year )
        );

    CREATE TABLE
        #grantee
        (
            district_code char(7)      not null,
            district_name varchar(255) not null,
            CONSTRAINT pk_#grantee PRIMARY KEY CLUSTERED ( district_code )
        );

    CREATE TABLE
        #data
        (
            district_name varchar(255) not null,
            district_code char(7)      not null,
            academic_year char(9)      not null,
            file_name     char(4)      not null,
            records       int          not null,
            CONSTRAINT pk_#data PRIMARY KEY CLUSTERED ( district_code, academic_year, file_name )
        );

    CREATE TABLE
        calpass.dbo.outreach_daily_output
        (
            district_name varchar(255) not null,
            district_code char(7)      not null,
            academic_year char(9)      not null,
            file_name     char(4)      not null,
            records       int              null,
            CONSTRAINT pk_outreach_daily_output PRIMARY KEY CLUSTERED ( district_code, academic_year, file_name )
        )

    INSERT INTO
        #file
        (
            file_name,
            academic_year
        )
    VALUES
        ('CRSC', '2015-2016'),
        ('CRSC', '2016-2017'),
        ('CRSC', '2017-2018'),
        ('CRSC', '2018-2019'),
        ('CRSC', '2019-2020'),
        ('PSTS', '2018-2019'),
        ('PSTS', '2019-2020'),
        ('SCSC', '2015-2016'),
        ('SCSC', '2016-2017'),
        ('SCSC', '2017-2018'),
        ('SCSC', '2018-2019'),
        ('SCSC', '2019-2020'),
        ('SCTE', '2015-2016'),
        ('SCTE', '2016-2017'),
        ('SCTE', '2017-2018'),
        ('SCTE', '2018-2019'),
        ('SCTE', '2019-2020'),
        ('SDIS', '2015-2016'),
        ('SDIS', '2016-2017'),
        ('SDIS', '2017-2018'),
        ('SDIS', '2018-2019'),
        ('SELA', '2015-2016'),
        ('SELA', '2016-2017'),
        ('SELA', '2017-2018'),
        ('SELA', '2018-2019'),
        ('SELA', '2019-2020'),
        ('SENR', '2015-2016'),
        ('SENR', '2016-2017'),
        ('SENR', '2017-2018'),
        ('SENR', '2018-2019'),
        ('SENR', '2019-2020'),
        ('SINC', '2019-2020'),
        ('SINF', '2015-2016'),
        ('SINF', '2016-2017'),
        ('SINF', '2017-2018'),
        ('SINF', '2018-2019'),
        ('SINF', '2019-2020'),
        ('SIRS', '2019-2020'),
        ('SOFF', '2019-2020'),
        ('SPED', '2018-2019'),
        ('SPED', '2019-2020'),
        ('SPRG', '2015-2016'),
        ('SPRG', '2016-2017'),
        ('SPRG', '2017-2018'),
        ('SPRG', '2018-2019'),
        ('SPRG', '2019-2020'),
        ('STAS', '2015-2016'),
        ('STAS', '2016-2017'),
        ('STAS', '2017-2018'),
        ('STAS', '2018-2019'),
        ('STAS', '2019-2020');

    INSERT INTO
        #Grantee
        (
            district_code,
            district_name
        )
    SELECT DISTINCT
        district_code,
        District
    FROM
        (
            VALUES
            ('3868478'),('1410140'),('3066621'),('1975291'),('3066548'),('3375192'),('3710371'),('5171464'),
            ('3868478'),('1910199'),('3066647'),('3567538'),('3066555'),('4770466'),('3768411'),('5171464'),
            ('3868478'),('1910199'),('3066670'),('3476505'),('3066563'),('4010405'),('1864139'),('3768338'),
            ('3868478'),('1964634'),('3073635'),('3476505'),('3066589'),('4010405'),('3467330'),('3768338'),
            ('5410546'),('1964246'),('3073643'),('3476505'),('3066597'),('4068700'),('3467330'),('3773528'),
            ('5472256'),('1964667'),('3073650'),('1964725'),('3066613'),('4068726'),('3610363'),('3768338'),
            ('0161127'),('1965102'),('3073924'),('0510058'),('3066621'),('4068759'),('3667686'),('3467439'),
            ('0161275'),('0461424'),('1062257'),('0561564'),('3066647'),('4068809'),('3667710'),('3968577'),
            ('0710074'),('3710371'),('1062265'),('0510058'),('3066670'),('4068825'),('3667843'),('3968585'),
            ('0710074'),('4510454'),('1062364'),('0561564'),('3066696'),('4068825'),('3667959'),('1573908'),
            ('0761630'),('4569856'),('1062414'),('3366993'),('3066746'),('4068833'),('3674138'),('1964352'),
            ('0761648'),('4574583'),('1062430'),('3366993'),('3073635'),('4068841'),('1964519'),('1964733'),
            ('0761697'),('4575267'),('1074153'),('3366993'),('3073643'),('4075457'),('3710371'),('3768452'),
            ('0761721'),('4510454'),('5471860'),('3710371'),('3073650'),('4075465'),('3773791'),('1964295'),
            ('0761739'),('4569856'),('5475531'),('1110116'),('3073924'),('4210421'),('1110116'),('0161150'),
            ('0761754'),('4574583'),('3010306'),('1110116'),('3074104'),('4269120'),('1162661'),('0161192'),
            ('0761788'),('4575267'),('3066431'),('1110116'),('3074112'),('4269146'),('1110116'),('0161291'),
            ('0761796'),('1964345'),('3066449'),('1110116'),('3074120'),('4269203'),('1162661'),('0161309'),
            ('0761804'),('1964667'),('3066464'),('1162554'),('3074252'),('4269229'),('1964576'),('0174013'),
            ('0774344'),('1965136'),('3066522'),('1162596'),('3010306'),('4269260'),('1965094'),('1964840'),
            ('0761721'),('1965136'),('3066548'),('1162638'),('3066431'),('4269260'),('1973460'),('1973437'),
            ('0761788'),('1965136'),('3066597'),('1162646'),('3066449'),('4269310'),('1974195'),('3768106'),
            ('0373981'),('1964519'),('3066621'),('1162653'),('3066464'),('4269328'),('0161200'),('5010504'),
            ('3166944'),('1110116'),('3066647'),('1162661'),('3066522'),('4274567'),('0174005'),('5071043'),
            ('3166944'),('1110116'),('3066670'),('1175481'),('3066548'),('4275010'),('0175093'),('5071175'),
            ('2065243'),('4169062'),('3073635'),('1176562'),('3066555'),('4276786'),('0175101'),('5071217'),
            ('3110314'),('5610561'),('3073643'),('1563818'),('3066597'),('3768304'),('1965094'),('5074609'),
            ('3110314'),('5610561'),('3073650'),('1563818'),('3066621'),('4169062'),('1974195'),('5075739'),
            ('3166894'),('5610561'),('3073924'),('3968650'),('3066647'),('1610165'),('4970953'),('0161259'),
            ('3166928'),('5672447'),('3074104'),('3367090'),('3066670'),('1663891'),('4970953'),('0610066'),
            ('3166951'),('5672454'),('3074112'),('3367090'),('3073635'),('1663982'),('0161143'),('0661598'),
            ('3174732'),('5672462'),('3074120'),('3367090'),('3073643'),('5410546'),('0761796'),('0661606'),
            ('3175085'),('5672470'),('3074252'),('0761754'),('3073650'),('5410546'),('0161143'),('0661614'),
            ('1964568'),('5672504'),('1510157'),('2365540'),('3073924'),('5471860'),('0710074'),('0661622'),
            ('1864139'),('5672512'),('0610066'),('5475523'),('3074104'),('5471993'),('0761721'),('5110512'),
            ('4710470'),('5672520'),('0661614'),('1910199'),('3074112'),('5472249'),('0761788'),('5171373'),
            ('4776455'),('5672520'),('0910090'),('1964451'),('3074120'),('5472256'),('0161143'),('5171381'),
            ('3166944'),('5672538'),('0961853'),('5271506'),('3074252'),('5475325'),('0110017'),('5171399'),
            ('3110314'),('5672546'),('2966357'),('5271506'),('2910298'),('5475531'),('0161119'),('5171449'),
            ('3110314'),('5672546'),('3110314'),('5310538'),('2966340'),('5476794'),('0161192'),('5171464'),
            ('3166894'),('5672553'),('3166894'),('5371662'),('2966357'),('5476836'),('0161259'),('5171464'),
            ('3166928'),('5672561'),('3166928'),('5371670'),('3010306'),('1310132'),('0161291'),('5174633'),
            ('3166951'),('5672603'),('3166951'),('5371696'),('3066431'),('1363073'),('0161309'),('5710579'),
            ('3174732'),('5672611'),('3175085'),('5371738'),('3066449'),('1363099'),('0710074'),('5774625'),
            ('3175085'),('5672652'),('3467314'),('5371746'),('3066464'),('1363107'),('1864139'),('5810587'),
            ('3110314'),('5673759'),('3467330'),('5371761'),('3066522'),('1363123'),('0110017'),('5872728'),
            ('3110314'),('5673874'),('3467348'),('5373833'),('3066548'),('1363131'),('0161119'),('5872736'),
            ('3166894'),('5673940'),('3467439'),('5375028'),('3066597'),('1363149'),('0161143'),('5872769'),
            ('3166928'),('5676828'),('3467447'),('5376513'),('3066621'),('1363164'),('0161168'),('0161119'),
            ('3166951'),('0161200'),('3473973'),('5872751'),('3066647'),('1363172'),('0161259'),('1210124'),
            ('3174732'),('2173361'),('3474500'),('5872769'),('3066670'),('1363180'),('0110017'),('1210124'),
            ('3175085'),('2866290'),('3475283'),('4870573'),('3073635'),('1363198'),('0161192'),('1262687'),
            ('1964733'),('4168890'),('3476505'),('3375200'),('3073643'),('1363206'),('0161259'),('1262810'),
            ('2310231'),('4970607'),('5171373'),('3310330'),('3073650'),('1363214'),('0161291'),('1262901'),
            ('2373866'),('4970862'),('5772686'),('1062414'),('3073924'),('1363222'),('1964733'),('1263032'),
            ('3166944'),('4970953'),('5772694'),('1075127'),('3074104'),('1363230'),('1964733'),('1263040'),
            ('2410249'),('4975390'),('5772710'),('1510157'),('3074112'),('3910397'),('1974435'),('1275515'),
            ('2465698'),('1964212'),('2110215'),('1563859'),('3074120'),('3775416'),('1964733'),('1275515'),
            ('2465722'),('3667611'),('2110215'),('1673932'),('3074252'),('1964733'),('0161192'),('4810488'),
            ('2465730'),('3667934'),('2165417'),('4369641'),('3010306'),('1964733'),('0161259'),('4870540'),
            ('2465755'),('3673957'),('2165466'),('1910199'),('3066431'),('4369369'),('0761796'),('4870565'),
            ('2465821'),('3675044'),('2165482'),('1964733'),('3066449'),('4369377'),('0110017'),('2765987'),
            ('2473619'),('3675051'),('2173361'),('3710371'),('3066464'),('4369427'),('0161259'),('2766068'),
            ('2474476'),('3675077'),('2174658'),('3773551'),('3066522'),('4369435'),('1210124'),('2766092'),
            ('2475317'),('3667611'),('1563677'),('0661614'),('3066548'),('4369450'),('1275515'),('2766159'),
            ('2475366'),('3667934'),('1875036'),('0910090'),('3066555'),('4369625'),('1210124'),('2773825'),
            ('1964485'),('3673890'),('1964907'),('3467314'),('3066597'),('3710371'),('1275515'),('2774054'),
            ('1964527'),('3675044'),('3367116'),('3467330'),('3066621'),('3768296'),('0161291'),('2775440'),
            ('1965128'),('3675077'),('0475507'),('3467447'),('3066647'),('1062166'),('3610363'),('2775473'),
            ('1974328'),('3667934'),('1964444'),('3473973'),('3066670'),('3367033'),('3610363'),('2765987'),
            ('1964774'),('3673957'),('1964303'),('3474500'),('3073635'),('3667876'),('3667637'),('2766068'),
            ('3066522'),('3675044'),('1964774'),('3475283'),('3073643'),('1964311'),('3667777'),('2766092'),
            ('3010306'),('3675077'),('1964873'),('5772686'),('3073650'),('1062117'),('3667850'),('2766159'),
            ('3066431'),('3667611'),('1977016'),('3710371'),('3073924'),('2710272'),('3667868'),('2773825'),
            ('3066449'),('3667934'),('3467314'),('3768031'),('3074104'),('5710579'),('3667876'),('2774054'),
            ('3066464'),('3673890'),('3467439'),('3710371'),('3074112'),('5772678'),('3674518'),('2775440'),
            ('3066514'),('3673957'),('3476505'),('3768346'),('3074120'),('5772686'),('3675044'),('2775473'),
            ('3066522'),('3675044'),('3476505'),('3710371'),('3074252'),('5772694'),('1964881'),('1563859'),
            ('3066548'),('3675051'),('1964303'),('3768106'),('1965029'),('5772702'),('0910090'),('2573585'),
            ('3066597'),('3675077'),('1964774'),('3768296'),('1062257'),('5772710'),('0961853'),('0110017'),
            ('3066621'),('4410447'),('1964873'),('3768338'),('1062265'),('5774625'),('0974377'),('0161259'),
            ('3066647'),('4469799'),('1977016'),('3768411'),('1062364'),('0661606'),('0910090'),('1210124'),
            ('3066670'),('4469807'),('3467314'),('3773551'),('1062414'),('5710579'),('0961853'),('1210124'),
            ('3073635'),('4469823'),('3467439'),('3773569'),('1062430'),('5772678'),('0974377'),('1262687'),
            ('3073643'),('4475432'),('3476505'),('3710371'),('1074153'),('5772686'),('3968593'),('1262810'),
            ('3073650'),('1563529'),('3476505'),('3768031'),('5471860'),('5772694'),('0910090'),('1263032'),
            ('3073924'),('1574807'),('1973452'),('3768106'),('5475531'),('5772702'),('0961853'),('1275515'),
            ('3074104'),('1964394'),('1964337'),('3768122'),('3367215'),('5772710'),('0974377'),('1275515'),
            ('3074112'),('3667652'),('1964337'),('3768130'),('0861820'),('5774625'),('3968593'),('1310132'),
            ('3074120'),('3667678'),('3467314'),('3768130'),('1262687'),('2465698'),('4569856'),('1363081'),
            ('3074252'),('3674211'),('3467314'),('3768130'),('1262810'),('3367124'),('2866241'),('1363099'),
            ('3010306'),('3675069'),('3467314'),('3768213'),('1275374'),('2410249'),('2866266'),('1363107'),
            ('3066423'),('1964527'),('3467314'),('3768296'),('1275515'),('2465730'),('2866290'),('1363115'),
            ('3066431'),('1965037'),('3467314'),('3768304'),('2365540'),('3467447'),('2874484'),('1363149'),
            ('3066449'),('1965128'),('3467314'),('3768338'),('3367173'),('3210322'),('4870532'),('1363164'),
            ('3066456'),('1974328'),('3467314'),('3768346'),('3010306'),('3610363'),('3768130'),('1363214'),
            ('3066464'),('1964527'),('1964980'),('3768411'),('3066431'),('3667652'),('3768452'),('1374401'),
            ('3066472'),('1965128'),('1510157'),('3768452'),('3066449'),('3667710'),('1210124'),('4110413'),
            ('3066480'),('1974328'),('1563412'),('3773551'),('3066464'),('3674211'),('1275515'),('4168924'),
            ('3066498'),('1964733'),('1563529'),('3773569'),('3066522'),('4570136'),('1210124'),('4169047'),
            ('3066506'),('3773569'),('4570136'),('3773791'),('3066548'),('4570136'),('1275515'),('4169062'),
            ('3066522'),('1964436'),('2465771'),('3775416'),('3066555'),('1964287'),('1975309'),('4169070'),
            ('3066530'),('3476505'),('3066670'),('5171464'),('3066597'),('1964469'),('4310439'),('1973445')
        ) t ( district_code )
        inner join
        calpads.Institution i on
            i.CdsCode = t.district_code + '0000000';

    INSERT INTO
        #data
        (
            district_code,
            district_name,
            academic_year,
            file_name,
            records
        )
    SELECT a.district_code, a.district_name, b.YearCode       as year_code, f.file_name, count(*) as Records FROM #grantee a inner join calpads.crsc b on a.district_code = b.DistrictCode inner join #file f on f.academic_year = b.YearCode       WHERE f.file_name = 'CRSC' GROUP BY a.district_code, b.YearCode,       a.district_name, f.file_name UNION ALL
    SELECT a.district_code, a.district_name, b.YearCode       as year_code, f.file_name, count(*) as Records FROM #grantee a inner join calpads.scsc b on a.district_code = b.DistrictCode inner join #file f on f.academic_year = b.YearCode       WHERE f.file_name = 'SCSC' GROUP BY a.district_code, b.YearCode,       a.district_name, f.file_name UNION ALL
    SELECT a.district_code, a.district_name, b.YearCode       as year_code, f.file_name, count(*) as Records FROM #grantee a inner join calpads.scte b on a.district_code = b.DistrictCode inner join #file f on f.academic_year = b.YearCode       WHERE f.file_name = 'SCTE' GROUP BY a.district_code, b.YearCode,       a.district_name, f.file_name UNION ALL
    SELECT a.district_code, a.district_name, b.YearCode       as year_code, f.file_name, count(*) as Records FROM #grantee a inner join calpads.sdis b on a.district_code = b.DistrictCode inner join #file f on f.academic_year = b.YearCode       WHERE f.file_name = 'SDIS' GROUP BY a.district_code, b.YearCode,       a.district_name, f.file_name UNION ALL
    SELECT a.district_code, a.district_name, b.YearCode       as year_code, f.file_name, count(*) as Records FROM #grantee a inner join calpads.sela b on a.district_code = b.DistrictCode inner join #file f on f.academic_year = b.YearCode       WHERE f.file_name = 'SELA' GROUP BY a.district_code, b.YearCode,       a.district_name, f.file_name UNION ALL
    SELECT a.district_code, a.district_name, b.YearCode       as year_code, f.file_name, count(*) as Records FROM #grantee a inner join calpads.senr b on a.district_code = b.DistrictCode inner join #file f on f.academic_year = b.YearCode       WHERE f.file_name = 'SENR' GROUP BY a.district_code, b.YearCode,       a.district_name, f.file_name UNION ALL
    SELECT a.district_code, a.district_name, b.YearCode       as year_code, f.file_name, count(*) as Records FROM #grantee a inner join calpads.sinf b on a.district_code = b.DistrictCode inner join #file f on f.academic_year = b.YearCode       WHERE f.file_name = 'SINF' GROUP BY a.district_code, b.YearCode,       a.district_name, f.file_name UNION ALL
    SELECT a.district_code, a.district_name, b.YearCode       as year_code, f.file_name, count(*) as Records FROM #grantee a inner join calpads.sprg b on a.district_code = b.DistrictCode inner join #file f on f.academic_year = b.YearCode       WHERE f.file_name = 'SPRG' GROUP BY a.district_code, b.YearCode,       a.district_name, f.file_name UNION ALL
    SELECT a.district_code, a.district_name, b.YearCode       as year_code, f.file_name, count(*) as Records FROM #grantee a inner join calpads.stas b on a.district_code = b.DistrictCode inner join #file f on f.academic_year = b.YearCode       WHERE f.file_name = 'STAS' GROUP BY a.district_code, b.YearCode,       a.district_name, f.file_name UNION ALL
    SELECT a.district_code, a.district_name, b.AcademicYearID as year_code, f.file_name, count(*) as Records FROM #grantee a inner join calpads.psts b on a.district_code = b.ReportingLEA inner join #file f on f.academic_year = b.AcademicYearID WHERE f.file_name = 'PSTS' GROUP BY a.district_code, b.AcademicYearID, a.district_name, f.file_name UNION ALL
    SELECT a.district_code, a.district_name, b.AcademicYearID as year_code, f.file_name, count(*) as Records FROM #grantee a inner join calpads.sinc b on a.district_code = b.ReportingLEA inner join #file f on f.academic_year = b.AcademicYearID WHERE f.file_name = 'SINC' GROUP BY a.district_code, b.AcademicYearID, a.district_name, f.file_name UNION ALL
    SELECT a.district_code, a.district_name, b.AcademicYearID as year_code, f.file_name, count(*) as Records FROM #grantee a inner join calpads.sirs b on a.district_code = b.ReportingLEA inner join #file f on f.academic_year = b.AcademicYearID WHERE f.file_name = 'SIRS' GROUP BY a.district_code, b.AcademicYearID, a.district_name, f.file_name UNION ALL
    SELECT a.district_code, a.district_name, b.AcademicYearID as year_code, f.file_name, count(*) as Records FROM #grantee a inner join calpads.soff b on a.district_code = b.ReportingLEA inner join #file f on f.academic_year = b.AcademicYearID WHERE f.file_name = 'SOFF' GROUP BY a.district_code, b.AcademicYearID, a.district_name, f.file_name;

    SELECT
        g.district_name,
        g.district_code,
        f.academic_year,
        f.file_name,
        d.records
    FROM
        #grantee g
        cross join
        #file f
        left outer join
        #data d on
            d.district_code = g.district_code and
            d.academic_year = f.academic_year and
            d.file_name     = f.file_name
    WHERE
        d.records is null
    ORDER BY
        district_name,
        academic_year,
        file_name;
END;