CREATE PROCEDURE
    OutreachDailyFailure
AS

    SET NOCOUNT ON;

DECLARE
    -- Constaints
    @Offset          tinyint        = 10,
    @DateInjection   char(14)       = REPLACE(REPLACE(REPLACE(CONVERT(CHAR(19), GETDATE(), 120), ' ', ''), '-', ''), ':', ''),
    @Now             datetime2      = SYSDATETIME(),
    -- Variables
    @Rank            integer        = 0,
    -- Mail
    @Message         nvarchar(2048) = N'',
    @ProfileName     sysname        = 'Cal-PASS SQL Support Profile',
    @Recipients      varchar(max)   = 'kknox@edresults.org;vmarrero@edresults.org',
    -- @Recipients      varchar(max)   = 'dlamoree@edresults.org',
    @Subject         nvarchar(255)  = N'',
    @Query           nvarchar(max)  = N'',
    @QueryHeader     bit            = 0,
    @QueryNoPadding  bit            = 1,
    @QueryAttach     bit            = 1,
    @QueryFileName   nvarchar(255)  = N'',
    @QuerySeparator  char(1)        = '	',
    @QueryWidth      smallint       = 8000,
    @ExcludeInline   bit            = 1;

BEGIN

    SET @Subject       = N'CALPADS :: FAILURES';
    SET @QueryFileName = N'CALPADS_FAILURES_' + @DateInjection + '.txt';
    SET @Query         = N'
        SET NOCOUNT ON;

        SELECT
            OrganizationName      as "name",
            SubmissionDateTime    as "submit_date",
            ProcessedDateTime     as "process_date",
            Status                as "status",
            TempFileName          as "file_name",
            ErrorMessage          as "error",
            SubmissionFileDetails as "details"
        FROM
            [PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedrop f
            inner join
            [PRO-DAT-SQL-01].calpass.dbo.Organization o on
                o.OrganizationId = f.OrganizationId
        WHERE
            Status = ''Failed'' and
            SubmissionFileDescriptionId = 2 and 
            SubmissionDateTime >= ''2020-07-01''
        ORDER BY
            OrganizationName DESC,
            SubmissionDateTime DESC;';

    EXEC msdb.dbo.sp_send_dbmail
        @profile_name                = @ProfileName,
        @recipients                  = @Recipients,
        @subject                     = @Subject,
        @body                        = @Message,
        @query                       = @Query,
        @query_result_no_padding     = @QueryNoPadding,
        @query_result_header         = @QueryHeader,
        @attach_query_result_as_file = @QueryAttach,
        @query_attachment_filename   = @QueryFileName,
        @query_result_separator      = @QuerySeparator,
        @query_result_width          = @QueryWidth,
        @exclude_query_output        = @ExcludeInline;
END;