USE calpass;

GO

IF (object_id('dbo.CCCourseProdPlusEscrow') is not null)
	BEGIN
		DROP PROCEDURE dbo.CCCourseProdPlusEscrow;
	END;

GO

CREATE PROCEDURE
	dbo.CCCourseProdPlusEscrow
	(
		@Escrow comis.Escrow READONLY
	)
AS

-- Output rows for dbo.CCCourseProdPlus data type

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@EscrowName sysname = 'CCCourseProd';

BEGIN
	SELECT
		CollegeId = e.CollegeIpedsCode,
		StudentComisId = e.StudentLocalId,
		StudentId = e.StudentSocialId,
		IdStatus = e.IdStatus,
		TermId = sx.term_id,
		CourseId = sx.course_id,
		SectionId = sx.section_id,
		Title = cb.title,
		UnitsMax = cb.units_maximum,
		UnitsAttempted = sx.units_attempted,
		UnitsEarned = sx.units,
		Grade = sx.grade,
		CreditFlag = sx.credit_flag,
		TopCode = cb.top_code,
		TransferStatus = cb.transfer_status,
		BSStatus = cb.basic_skills_status,
		SamCode = cb.sam_code,
		ClassCode = cb.classification_code,
		CollegeLevel = cb.prior_to_college,
		NCRCategory = cb.noncredit_category,
		CCLongTermId = t.YearTermCode,
		extSX05_PositiveAttendanceHours = sx.attend_hours,
		extSXD4_TotalHours = sx.total_hours,
		extXF01_SessionInstuctionMethod1 = xf.SessionInstuctionMethod1,
		extXF01_SessionInstuctionMethod2 = xf.SessionInstuctionMethod2,
		extXF01_SessionInstuctionMethod3 = xf.SessionInstuctionMethod3,
		extXF01_SessionInstuctionMethod4 = xf.SessionInstuctionMethod4,
		extXBD3_DayEveningClassCode = xb.day_evening_code,
		extCB00_ControlNumber = cb.control_number,
		extCB04_CreditStatus = cb.credit_status,
		extCB10_CoopWorkExpEdStatus = cb.coop_ed_status,
		extCB13_SpecialClassStatus = cb.special_class_status,
		extCB14_CanCode = cb.can_code,
		extCB15_CanSeqCode = cb.can_seq_code,
		extCB19_CrosswalkCrsDeptName = cb.crosswalk_crs_name,
		extCB20_CrosswalkCrsNumber = cb.crosswalk_crs_number,
		extCB21_PriorToCollegeLevel = cb.prior_to_college,
		extCB23_FundingAgencyCategory = cb.funding_category,
		extCB24_ProgramStatus = cb.program_status,
		extXB01_AccountingMethod = xb.accounting
	FROM
		@Escrow e
		inner join
		comis.sxenrlm sx
			on sx.college_id = e.CollegeId
			and sx.student_id = e.StudentLocalId
			and sx.term_id = e.TermId
		inner join
		comis.xbsecton xb
			on xb.college_id = sx.college_id
			and xb.term_id = sx.term_id
			and xb.course_id = sx.course_id
			and xb.section_id = sx.section_id
		inner join
		comis.cbcrsinv cb
			on cb.college_id = sx.college_id
			and cb.term_id = sx.term_id
			and cb.course_id = sx.course_id
			and cb.control_number = sx.control_number
		inner join
		(
			SELECT
				xf.college_id,
				xf.term_id,
				xf.course_id,
				xf.section_id,
				SessionInstuctionMethod1 = max(case when xf.InstructionMethodSelector = 1 then xf.instruction end),
				SessionInstuctionMethod2 = max(case when xf.InstructionMethodSelector = 2 then xf.instruction end),
				SessionInstuctionMethod3 = max(case when xf.InstructionMethodSelector = 3 then xf.instruction end),
				SessionInstuctionMethod4 = max(case when xf.InstructionMethodSelector = 4 then xf.instruction end)
			FROM
				(
					SELECT
						xf.college_id,
						xf.term_id,
						xf.course_id,
						xf.section_id,
						xf.instruction,
						InstructionMethodSelector = row_number() OVER(
								PARTITION BY
									xf.college_id,
									xf.term_id,
									xf.course_id,
									xf.section_id
								ORDER BY
									xf.session_id
							)
					FROM
						comis.xfsesion xf
					WHERE
						exists (
							SELECT
								1
							FROM
								@Escrow e
							WHERE
								e.CollegeId = xf.college_id
						)
				) xf
			GROUP BY
				xf.college_id,
				xf.term_id,
				xf.course_id,
				xf.section_id
		) xf
			on xf.college_id = xb.college_id
			and xf.term_id = xb.term_id
			and xf.course_id = xb.course_id
			and xf.section_id= xb.section_id
		inner join
		comis.Term t
			on t.TermCode = sx.college_id
	WHERE
		e.EscrowName = @EscrowName;
END;