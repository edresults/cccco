USE calpass;

GO

IF (object_id('dbo.K12CourseProdMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.K12CourseProdMerge;
	END;

GO

CREATE PROCEDURE
	dbo.K12CourseProdMerge
	(
		@Escrow calpads.Escrow READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Message nvarchar(2048),
	@ErrorCode int = 70099,
	@ErrorSeverity int = 1,
	@EscrowName sysname = 'K12CourseProd';

BEGIN
	BEGIN TRY
		MERGE
			dbo.K12CourseProd
		WITH
			(
				TABLOCKX
			) t
		USING 
			(
				SELECT
					Derkey1,
					School,
					AcYear,
					LocStudentId,
					CourseId,
					LocallyAssignedCourseNumber,
					CourseSectionClassNumber,
					CourseTitle,
					AGstatus,
					Grade,
					CreditEarned,
					CreditAttempted,
					CourseLevel,
					CourseType,
					CourseTerm,
					DateAdded
				FROM
					(
						SELECT
							Derkey1 = e.InterSegmentKey,
							School = s.DistrictCode + s.SchoolCode,
							AcYear = y.YearCodeAbbr,
							LocStudentId = calpass.dbo.get1289Encryption(f.StudentLocalId, 'X'),
							CourseId = c.CourseCode,
							LocallyAssignedCourseNumber = c.CourseId,
							CourseSectionClassNumber = s.SectionId,
							CourseTitle = substring(c.CourseName, 1, 40),
							AGstatus = s.UniversityCode,
							Grade = isnull(cm.TargetCode, s.MarkCode),
							CreditEarned = isnull(s.CreditGet, 0.00),
							CreditAttempted = isnull(s.CreditTry, 0.00),
							CourseLevel = 
								case
									when c.SectionLevelCode = '10' then '35'
									when c.SectionLevelCode = '12' then '33'
									when c.SectionLevelCode = '14' then '39'
									when c.SectionLevelCode = '15' then '34'
									when c.SectionLevelCode = '16' then '40'
									when c.CourseCode in ('2160','2161','2173','2174','2175','2260','2261','2262','2263','2264','2265','2281','2282','2283','2360','2361','2362','2460','2461','2462','2463','2464','2465','2466','2467','2468','2469','2473','2479','2484','2485','2560','2660','2661','2662','2664','2665','2666','2667','2668','2760','2761','2762','2763','2764','2765','2766','2767','2768','2769','2779','2780','2860','2861','2960','2961','2962') then '38'
									when c.CourseCode in ('2170','2171','2270','2271','2272','2273','2274','2275','2276','2277','2278','2279','2370','2470','2471','2472','2480','2481','2483','2670','2671','2672','2673','2674','2675','2676','2770','2771','2772','2773','2774','2775','2776','2777','2778','2825','2826','2870','2874','2875','2876') then '30'
									when c.CourseCode = '2402' then '35'
									else 'XX'
								end,
							CourseType = 
								case
									when c.IsCte = 'Y' and c.ProgramFundingSource = '1' then '33'
									when c.IsCte = 'N' and c.ProgramFundingSource = '1' then '30'
									when c.IsCte = 'Y' and c.ProgramFundingSource != '1' then '32'
									else 'XX'
								end,
							CourseTerm = coalesce(s.MarkingPeriodCode, s.TermCode),
							DateAdded = getdate(),
							RecordCount = row_number() over(
									partition by
										s.DistrictCode,
										s.SchoolCode,
										calpass.dbo.Get1289Encryption(f.StudentLocalId, 'X'),
										s.YearCode,
										coalesce(s.MarkingPeriodCode, s.TermCode),
										c.CourseId,
										s.SectionId,
										isnull(cm.TargetCode, s.MarkCode),
										isnull(s.CreditTry, 0.00)
									order by
										(select 1) desc
								)
						FROM
							@Escrow e
							inner join
							calpads.Scsc s with(index(pk_Scsc))
								on s.StudentStateId = e.StudentStateId
								and s.SchoolCode = e.SchoolCode
								and s.YearCode = e.YearCode
							inner join
							calpads.Year y with(index(PK_Year))
								on y.YearCode = s.YearCode
							inner join
							calpads.Crsc c with(index(pk_Crsc))
								on c.SectionId = s.SectionId
								and c.CourseId = s.CourseId
								and c.TermCode = s.TermCode
								and c.YearCode = s.YearCode
								and c.SchoolCode = s.SchoolCode
							inner join
							calpads.Sinf f with(index(pk_Sinf))
								on e.StudentStateId = f.StudentStateId
								and e.SchoolCode = f.SchoolCode
								and e.SinfYearCode = f.YearCode
								and e.SinfEffectiveStartDate = f.EffectiveStartDate
							left outer join
							CustomMarks cm
								on  cm.OrganizationCode = s.DistrictCode + s.SchoolCode
								and cm.SourceCode = s.MarkCode
						WHERE
							e.EscrowName = @EscrowName
							and f.StudentLocalId is not null
							and coalesce(s.MarkingPeriodCode, s.TermCode) is not null
							and s.MarkCode is not null
					) a
				WHERE
					RecordCount = 1
			) s
		ON
			t.School = s.School
			and t.LocStudentId = s.LocStudentId
			and t.AcYear = s.AcYear
			and t.CourseTerm = s.CourseTerm
			and t.LocallyAssignedCourseNumber = s.LocallyAssignedCourseNumber
			and t.CourseSectionClassNumber = s.CourseSectionClassNumber
			and t.Grade = s.Grade
			and t.CreditAttempted = s.CreditAttempted
		WHEN MATCHED THEN
			UPDATE SET
				t.Derkey1 = s.Derkey1,
				t.CourseId = s.CourseId,
				t.CourseTitle = s.CourseTitle,
				t.AGstatus = s.AGstatus,
				t.CreditEarned = s.CreditEarned,
				t.CourseLevel = s.CourseLevel,
				t.CourseType = s.CourseType,
				t.DateAdded =  s.DateAdded
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					Derkey1,
					School,
					AcYear,
					LocStudentId,
					CourseId,
					LocallyAssignedCourseNumber,
					CourseSectionClassNumber,
					CourseTitle,
					AGstatus,
					Grade,
					CreditEarned,
					CreditAttempted,
					CourseLevel,
					CourseType,
					CourseTerm,
					DateAdded
				)
			VALUES
				(
					s.Derkey1,
					s.School,
					s.AcYear,
					s.LocStudentId,
					s.CourseId,
					s.LocallyAssignedCourseNumber,
					s.CourseSectionClassNumber,
					s.CourseTitle,
					s.AGstatus,
					s.Grade,
					s.CreditEarned,
					s.CreditAttempted,
					s.CourseLevel,
					s.CourseType,
					s.CourseTerm,
					s.DateAdded
				);
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message =
			nchar(9) + N'Error Procedure: ' + upper(object_name(@@PROCID)) + char(13) + char(10) +
			nchar(9) + N'Error Number:	' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:	 ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Line:	  ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		-- put error message
		THROW @ErrorCode, @Message, @ErrorSeverity;
	END CATCH;
END;