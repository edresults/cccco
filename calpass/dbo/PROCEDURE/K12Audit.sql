ALTER PROCEDURE
    K12Audit
    (
        @DistrictCode char(7) = null
    )
AS

    SET NOCOUNT ON;

DECLARE
    -- Constaints
    @Offset         tinyint        = 10,
    @DateInjection  char(14)       = REPLACE(REPLACE(REPLACE(CONVERT(CHAR(19), GETDATE(), 120), ' ', ''), '-', ''), ':', ''),
    @Now            datetime2      = SYSDATETIME(),
    -- Variables
    @Rank           integer        = 0,
    -- Mail
    @Message        nvarchar(2048) = N'',
    @ProfileName    sysname        = 'Cal-PASS SQL Support Profile',
    @Recipients     varchar(max)   = 'dlamoree@edresults.org;msamra@edresults.org;agreen@edresults.org;vmarrero@edresults.org',
    @Subject        nvarchar(255)  = N'',
    @Query          nvarchar(max)  = N'',
    @QueryHeader    bit            = 1,
    @QueryAttach    bit            = 1,
    @QueryFileName  nvarchar(255)  = N'',
    @QuerySeparator char(1)        = ' ',
    @ExcludeInline  bit            = 1;

BEGIN

    IF (object_id('tempdb..#Mous') is not null)
        BEGIN
            DROP TABLE #Mous;
        END;

    IF (object_id('tempdb..#Schools') is not null)
        BEGIN
            DROP TABLE #Schools;
        END;

    IF (object_id('tempdb..#Stats') is not null)
        BEGIN
            DROP TABLE #Stats;
        END;

    IF (object_id('tempdb..#Sources') is not null)
        BEGIN
            DROP TABLE #Sources;
        END;

    CREATE TABLE
        #Mous
        (
            DistrictCode char(7)
        );

    CREATE TABLE
        #Sources
        (
            Code char(1)
        );

    CREATE TABLE
        #Schools
        (
            OrganizationCode char(14) primary key,
            DistrictCode     char(7),
            DistrictName     varchar(90)
        );

    CREATE TABLE
        #Stats
        (
            DistrictCode char(7),
            TableSource  char(1),
            Mean         decimal(38,19),
            StdDev       decimal(38,19),
            INDEX IX_#Stats CLUSTERED (DistrictCode, TableSource)
        );

    INSERT INTO
        #Mous
        (
            DistrictCode
        )
    SELECT
        DistrictCode = substring(o.OrganizationCode, 1, 7)
    FROM
        [PRO-DAT-SQL-01].calpass.dbo.Organization o
        inner join
        (
            SELECT
                OrganizationId,
                MouExpireDate = DateMouExpires
            FROM
                [PRO-DAT-SQL-01].calpass.dbo.oms_mou
            UNION ALL
            SELECT
                OrganizationId,
                MouExpireDate = dtMouExpires
            FROM
                [PRO-DAT-SQL-01].calpass.dbo.OrganizationMou
            WHERE
                MouStatus = 5
            UNION ALL
            SELECT
                OrganizationId,
                MouExpireDate = expirationDate
            FROM
                [PRO-DAT-SQL-01].calpass.dbo.cpp_mou
        ) m
            on o.OrganizationId = m.OrganizationId
    WHERE
        o.OrganizationCodeTypeId = 1
    GROUP BY
        substring(o.OrganizationCode, 1, 7)
    HAVING
        max(m.MouExpireDate) > @Now;

    INSERT INTO
        #Sources
        (
            Code
        )
    VALUES
        ('S'),
        ('C'),
        ('A');

    INSERT INTO
        #Schools
        (
            OrganizationCode,
            DistrictCode,
            DistrictName
        )
    SELECT DISTINCT
        OrganizationCode = i.CdsCode,
        DistrictCode     = substring(i.CdsCode, 1, 7),
        DistrictName     = i.District
    FROM
        calpass.calpads.Institution i
    WHERE
        (
            @DistrictCode is null
            or
            substring(CdsCode, 1, 7) = @DistrictCode
        )
        and exists (
            SELECT
                null
            FROM
                #Mous m
            WHERE
                m.DistrictCode = substring(i.CdsCode, 1, 7)
        )
        and exists (
            SELECT
                1
            FROM
                [PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedrop f
                inner join
                [PRO-DAT-SQL-01].calpass.dbo.Organization o
                    on o.OrganizationId = f.OrganizationId
            WHERE
                i.CdsCode like '%' + substring(o.OrganizationCode, 1, 7) + '%'
                and f.SubmissionDateTime > case
                    when DATEPART(weekday, SYSDATETIME()) = 2 then DATEADD(week, -1, SYSDATETIME())
                    else DATEADD(day, -1, SYSDATETIME())
                end
        );

    SELECT TOP 1
        @Rank = Rank
    FROM
        calpass.calpads.Year
    WHERE
        DateEnd < @Now
    ORDER BY
        Rank desc;

    TRUNCATE TABLE K12Audits;

    INSERT INTO
        K12Audits
        (
            DistrictCode,
            DistrictName,
            TableSource,
            AcademicYear
        )
    SELECT DISTINCT
        DistrictCode = s.DistrictCode,
        DistrictName = s.DistrictName,
        TableSource  = t.Code,
        AcademicYear = y.YearCode
    FROM
        #Schools s
        cross join
        calpass.calpads.Year y
        cross join
        #Sources t
    WHERE
        Rank <= @Rank
        and Rank > @Rank - @Offset;

    IF (@@ROWCOUNT = 0)
        BEGIN
            RETURN;
        END;

    -- K12SchoolsProd
    UPDATE
        t
    SET
        t.Records = s.Records
    FROM
        calpass.dbo.K12Audits t
        inner join
        (
            SELECT
                DistrictCode = o.DistrictCode,
                AcademicYear = y.YearCode,
                Records      = count(*)
            FROM
                calpass.dbo.K12StudentProd s
                inner join
                #Schools o
                    on o.OrganizationCode = s.School
                inner join
                calpass.calpads.Year y
                    on y.YearCodeAbbr = s.AcYear
            GROUP BY
                o.DistrictCode,
                y.YearCode
        ) s
            on  t.DistrictCode = s.DistrictCode
            and t.AcademicYear = s.AcademicYear
    WHERE
        t.TableSource = 'S';

    -- K12CourseProd
    UPDATE
        t
    SET
        t.Records = c.Records
    FROM
        calpass.dbo.K12Audits t
        inner join
        (
            SELECT
                DistrictCode = o.DistrictCode,
                AcademicYear = y.YearCode,
                Records      = count(*)
            FROM
                calpass.dbo.K12CourseProd c
                inner join
                #Schools o
                    on o.OrganizationCode = c.School
                inner join
                calpass.calpads.Year y
                    on y.YearCodeAbbr = c.AcYear
            GROUP BY
                o.DistrictCode,
                y.YearCode
        ) c
            on  t.DistrictCode = c.DistrictCode
            and t.AcademicYear = c.AcademicYear
    WHERE
        t.TableSource = 'C';

    -- K12AwardProd
    UPDATE
        t
    SET
        t.Records = a.Records
    FROM
        calpass.dbo.K12Audits t
        inner join
        (
            SELECT
                DistrictCode = o.DistrictCode,
                AcademicYear = y.YearCode,
                Records      = count(*)
            FROM
                calpass.dbo.K12AwardProd a
                inner join
                #Schools o
                    on o.OrganizationCode = a.School
                inner join
                calpass.calpads.Year y
                    on y.YearCodeAbbr = a.AcYear
            GROUP BY
                o.DistrictCode,
                y.YearCode
        ) a
            on  t.DistrictCode = a.DistrictCode
            and t.AcademicYear = a.AcademicYear
    WHERE
        t.TableSource = 'A';

    INSERT INTO
        #Stats
        (
            DistrictCode,
            TableSource,
            Mean
        )
    SELECT
        DistrictCode = DistrictCode,
        TableSource  = TableSource,
        Mean         = AVG(Records)
    FROM
        calpass.dbo.K12Audits
    WHERE
        Records is not null
    GROUP BY
        DistrictCode,
        TableSource;

    UPDATE
        t
    SET
        t.StdDev = s.StdDev
    FROM
        #Stats t
        inner join
        (
            SELECT
                DistrictCode = r.DistrictCode,
                TableSource  = r.TableSource,
                StdDev       = SQRT(SUM(POWER(r.Records - s.Mean, 2)) / count(r.Records))
            FROM
                calpass.dbo.K12Audits r
                inner join
                #Stats s
                    on  r.DistrictCode = s.DistrictCode
                    and r.TableSource  = s.TableSource
            WHERE
                r.Records is not null
            GROUP BY
                r.DistrictCode,
                r.TableSource
        ) s
            on  t.DistrictCode = s.DistrictCode
            and t.TableSource  = s.TableSource;

    UPDATE
        t
    SET
        t.ZScore = ROUND((t.Records - s.Mean) / s.StdDev, 7)
    FROM
        calpass.dbo.K12Audits t
        inner join
        #Stats s
            on  t.DistrictCode = s.DistrictCode
            and t.TableSource  = s.TableSource
    WHERE
        t.Records is not null
        and s.StdDev > 0.0;

    SET @Subject       = N'K12Audit: Missing Gaps';
    SET @QueryFileName = N'K12Audit_MissingGaps_' + @DateInjection + '.txt';
    SET @Query         = N'
        SET NOCOUNT ON;

        SELECT
            a.DistrictCode,
            a.TableSource,
            a.AcademicYear
        FROM
            calpass.dbo.K12Audits a
        WHERE
            a.Records is null
        ORDER BY
            (
                SELECT
                    MAX(a1.Records)
                FROM
                    calpass.dbo.K12Audits a1
                WHERE
                    a1.DistrictCode = a.DistrictCode
            ),
            a.DistrictName,
            a.TableSource,
            a.AcademicYear;';

    EXEC msdb.dbo.sp_send_dbmail
        @profile_name                = @ProfileName,
        @recipients                  = @Recipients,
        @subject                     = @Subject,
        @body                        = @Message,
        @query                       = @Query,
        @query_result_header         = @QueryHeader,
        @attach_query_result_as_file = @QueryAttach,
        @query_attachment_filename   = @QueryFileName,
        @query_result_separator      = @QuerySeparator,
        @exclude_query_output        = @ExcludeInline;

    SET @Subject       = N'K12Audit: Uniformity';
    SET @QueryFileName = N'K12Audit_Uniformity_' + @DateInjection + '.txt';
    SET @Query         = N'
        SET NOCOUNT ON;

        SELECT
            DistrictCode = a.DistrictCode,
            TableSource  = a.TableSource,
            AcademicYear = a.AcademicYear,
            Records      = a.Records,
            ZScore       = ROUND(a.ZScore, 5)
        FROM
            calpass.dbo.K12Audits a
            cross join
            calpass.dbo.K12AuditToggles t
        WHERE
            ABS(a.ZScore) >= t.ZScoreThreshold
        ORDER BY
            (
                SELECT
                    MAX(a1.Records)
                FROM
                    calpass.dbo.K12Audits a1
                WHERE
                    a1.DistrictCode = a.DistrictCode
            ),
            a.DistrictName,
            a.TableSource,
            a.AcademicYear;';

    EXEC msdb.dbo.sp_send_dbmail
        @profile_name                = @ProfileName,
        @recipients                  = @Recipients,
        @subject                     = @Subject,
        @body                        = @Message,
        @query                       = @Query,
        @query_result_header         = @QueryHeader,
        @attach_query_result_as_file = @QueryAttach,
        @query_attachment_filename   = @QueryFileName,
        @query_result_separator      = @QuerySeparator,
        @exclude_query_output        = @ExcludeInline;

    SET @Subject       = N'K12Audit: Disparity';
    SET @QueryFileName = N'K12Audit_Disparity_' + @DateInjection + '.txt';
    SET @Query         = N'
        SET NOCOUNT ON;

        WITH
            base
        AS
            (
                SELECT
                    DistrictCode,
                    TableSource,
                    AcademicYear,
                    Records,
                    SequenceNumber = ROW_NUMBER() OVER(PARTITION BY DistrictCode, TableSource ORDER BY AcademicYear)
                FROM
                    calpass.dbo.K12Audits
                WHERE
                    Records is not null
            )
        SELECT
            b1.DistrictCode,
            b1.TableSource,
            b1.AcademicYear,
            b1.Records
        FROM
            base b1
            inner join
            base b2
                on  b2.DistrictCode   = b1.DistrictCode
                and b2.TableSource    = b1.TableSource
                and b2.SequenceNumber = b1.SequenceNumber - 1
            cross join
            calpass.dbo.K12AuditToggles t
        WHERE
            ABS(b1.Records - b2.Records) > ROUND(b2.Records * t.YearOverYear, 0)
        ORDER BY
            (
                SELECT
                    MAX(b3.Records)
                FROM
                    calpass.dbo.K12Audits b3
                WHERE
                    b3.DistrictCode = b1.DistrictCode
            )';

    EXEC msdb.dbo.sp_send_dbmail
        @profile_name                = @ProfileName,
        @recipients                  = @Recipients,
        @subject                     = @Subject,
        @body                        = @Message,
        @query                       = @Query,
        @query_result_header         = @QueryHeader,
        @attach_query_result_as_file = @QueryAttach,
        @query_attachment_filename   = @QueryFileName,
        @query_result_separator      = @QuerySeparator,
        @exclude_query_output        = @ExcludeInline;
END;