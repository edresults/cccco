USE calpass;

GO

IF (object_id('dbo.p_pburdman_cohort_insert') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_pburdman_cohort_insert;
	END;

GO

CREATE PROCEDURE
	dbo.p_pburdman_cohort_insert
AS
	
BEGIN

	-- start fresh
	TRUNCATE TABLE calpass.dbo.pburdman_cohort;

	INSERT INTO
		calpass.dbo.pburdman_cohort
		(
			high_school,
			math_rank_01_ind,
			math_rank_02_ind,
			math_rank_03_ind,
			math_rank_04_ind,
			math_rank_05_ind,
			math_rank_06_ind,
			math_rank_07_ind,
			math_rank_08_ind
		)
	SELECT
		hsc.school,
		math_rank_01_ind = max(case when cbeds_rank.rank = 1 then 1 else 0 end),
		math_rank_02_ind = max(case when cbeds_rank.rank = 2 then 1 else 0 end),
		math_rank_03_ind = max(case when cbeds_rank.rank = 3 then 1 else 0 end),
		math_rank_04_ind = max(case when cbeds_rank.rank = 4 then 1 else 0 end),
		math_rank_05_ind = max(case when cbeds_rank.rank = 5 then 1 else 0 end),
		math_rank_06_ind = max(case when cbeds_rank.rank = 6 then 1 else 0 end),
		math_rank_07_ind = max(case when cbeds_rank.rank = 7 then 1 else 0 end),
		math_rank_08_ind = max(case when cbeds_rank.rank = 8 then 1 else 0 end)
	FROM
		calpass.dbo.K12StudentProd hss
			with(index(ix_K12StudentProd__Derkey1__GradeLevel__AcYear))
		inner join
		calpass.dbo.K12CourseProd hsc
			with(index(ixc_K12CourseProd__School__LocStudentId__AcYear))
			on hss.school = hsc.school
			and hss.LocStudentId = hsc.LocStudentId
			and hss.AcYear = hsc.AcYear
		inner join
		calpass.dbo.K12StudentProd_AcYear ay
			on ay.code = hsc.AcYear
		inner join
		calpass.dbo.K12CourseProd_cbeds cbeds
			on cbeds.code = case
					when hsc.courseid = '6098' then
						case
							when lower(hsc.coursetitle) like '%alg%' then
								case
									when lower(hsc.coursetitle) like '%pre%' then '2424'
									when lower(hsc.coursetitle) like '%ii%' then '2404'
									when lower(hsc.coursetitle) like '%i%' then '2403'
								end
							when lower(hsc.coursetitle) like '%trig%' then '2407'
							when lower(hsc.coursetitle) like '%arith%' then '2400'
							when lower(hsc.coursetitle) like '%calc%' then
								case
									when lower(hsc.coursetitle) like '%pre%' then '2414'
									else '2415'
								end
							when lower(hsc.coursetitle) like '%stat%' then '2445'
							when (
								lower(hsc.coursetitle) like '%geo%'
								and lower(hsc.coursetitle) not like '%geol%'
								and lower(hsc.coursetitle) not like '%astron%'
								and lower(hsc.coursetitle) not like '%hlth%'
								and lower(hsc.coursetitle) not like '%his%'
								and lower(hsc.coursetitle) not like '%hst%'
								and lower(hsc.coursetitle) not like '%geog%'
								and lower(hsc.coursetitle) not like '%world%'
								and lower(hsc.coursetitle) not like '%wld%'
								and lower(hsc.coursetitle) not like '%wrld%'
								and lower(hsc.coursetitle) not like '%lap%'
								and lower(hsc.coursetitle) not like '%clt%'
								and lower(hsc.coursetitle) not like '%cult%'
							) then '2413'
						end
					else hsc.courseid
				end
			and cbeds.year_code_start <= ay.code_calpads
			and cbeds.year_code_end >= ay.code_calpads
		inner join
		calpass.dbo.K12CourseProd_cbeds_rank cbeds_rank
			on cbeds_rank.code = cbeds.code
	WHERE
		hss.GradeLevel in ('09', '10', '11', '12')
	GROUP BY
		hsc.school;

END;