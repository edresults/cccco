USE calpass;

GO

IF (object_id('dbo.K12StudentProdMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.K12StudentProdMerge;
	END;

GO

CREATE PROCEDURE
	dbo.K12StudentProdMerge
	(
		@Escrow calpads.Escrow READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Message nvarchar(2048),
	@ErrorCode int = 70099,
	@ErrorSeverity int = 1,
	@EscrowName sysname = 'K12StudentProd';

BEGIN
	BEGIN TRY
		MERGE
			dbo.K12StudentProd
		WITH
			(
				TABLOCKX
			) t
		USING
			(
				SELECT
					Derkey1,
					School,
					AcYear,
					LocStudentId,
					StudentId,
					CSISNum,
					Fname,
					Lname,
					Gender,
					Ethnicity,
					Birthdate,
					GradeLevel,
					HomeLanguage,
					HispanicEthnicity,
					EthnicityCode1,
					EthnicityCode2,
					EthnicityCode3,
					EthnicityCode4,
					EthnicityCode5,
					DateAdded
				FROM
					(
						SELECT
							Derkey1 = e.InterSegmentKey,
							School = r.DistrictCode + r.SchoolCode,
							AcYear = y.YearCodeAbbr,
							LocStudentId = convert(varchar(15), calpass.dbo.get1289Encryption(f.StudentLocalId, 'X')),
							StudentId = null,
							CSISNum = calpass.dbo.get9812Encryption(r.StudentStateId, 'X'),
							Fname = f.NameFirst,
							Lname = f.NameLast,
							Gender = f.Gender,
							Ethnicity = null,
							Birthdate = f.Birthdate,
							GradeLevel = r.GradeCode,
							HomeLanguage =
								case
									when l.PrimaryLanguageCode is null then '99'
									when l.PrimaryLanguageCode = 'UU' then '99'
									else l.PrimaryLanguageCode
								end,
							HispanicEthnicity = f.IsHispanicEthnicity,
							EthnicityCode1 = f.Race01,
							EthnicityCode2 = f.Race02,
							EthnicityCode3 = f.Race03,
							EthnicityCode4 = f.Race04,
							EthnicityCode5 = f.Race05,
							DateAdded = getdate(),
							RecordCount = row_number() over(
									partition by
										r.DistrictCode,
										r.SchoolCode,
										calpass.dbo.get1289Encryption(f.StudentLocalId, 'X'),
										r.YearCode
									order by
										r.GradeCode desc
								)
						FROM
							@Escrow e
							inner join
							calpads.Senr r with(index(pk_Senr))
								on e.StudentStateId = r.StudentStateId
								and e.SchoolCode = r.SchoolCode
								and e.YearCode = r.YearCode
							-- required sinf record
							inner join
							calpads.Sinf f with(index(pk_Sinf))
								on e.StudentStateId = f.StudentStateId
								and e.SchoolCode = f.SchoolCode
								and e.SinfYearCode = f.YearCode
								and e.SinfEffectiveStartDate = f.EffectiveStartDate
							inner join
							calpads.Year y with(index(Pk_Year))
								on y.YearCode = r.YearCode
							-- optional sela record
							left outer join
							calpads.Sela l with(index(pk_Sela))
								on l.StudentStateId = r.StudentStateId
								and l.SchoolCode = r.SchoolCode
								and l.ElaStatusStartDate = (
									SELECT
										max(l1.ElaStatusStartDate)
									FROM
										calpads.Sela l1 with(index(pk_Sela))
									WHERE
										l1.StudentStateId = l.StudentStateId
										and l1.SchoolCode = l.SchoolCode
										and l1.YearCode = l.YearCode
										and l1.YearCode = (
											SELECT
												max(l2.YearCode)
											FROM
												calpads.Sela l2 with(index(pk_Sela))
											WHERE
												l2.StudentStateId = l1.StudentStateId
												and l2.SchoolCode = l1.SchoolCode
												and l2.YearCode <= r.YearCode
										)
								)
						WHERE
							e.EscrowName = @EscrowName
							and r.EnrollStartDate = (
								SELECT
									max(r1.EnrollStartDate)
								FROM
									calpads.Senr r1 with(index(pk_Senr))
								WHERE
									r1.StudentStateId = r.StudentStateId
									and r1.SchoolCode = r.SchoolCode
									and r1.YearCode = r.YearCode
							)
							and f.StudentLocalId is not null
					) a
				WHERE
					RecordCount = 1
			) s
		ON
			s.School = t.School
			and s.LocStudentId = t.LocStudentId
			and s.AcYear = t.AcYear
		WHEN MATCHED THEN
			UPDATE SET
				t.Derkey1 = s.Derkey1,
				t.StudentId = s.StudentId,
				t.CSISNum = s.CSISNum,
				t.Fname = s.Fname,
				t.Lname = s.Lname,
				t.Gender = s.Gender,
				t.Ethnicity = s.Ethnicity,
				t.Birthdate = s.Birthdate,
				t.GradeLevel = s.GradeLevel,
				t.HomeLanguage = s.HomeLanguage,
				t.HispanicEthnicity = s.HispanicEthnicity,
				t.EthnicityCode1 = s.EthnicityCode1,
				t.EthnicityCode2 = s.EthnicityCode2,
				t.EthnicityCode3 = s.EthnicityCode3,
				t.EthnicityCode4 = s.EthnicityCode4,
				t.EthnicityCode5 = s.EthnicityCode5,
				t.DateAdded = s.DateAdded
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					Derkey1,
					School,
					AcYear,
					LocStudentId,
					StudentId,
					CSISNum,
					Fname,
					Lname,
					Gender,
					Ethnicity,
					Birthdate,
					GradeLevel,
					HomeLanguage,
					HispanicEthnicity,
					EthnicityCode1,
					EthnicityCode2,
					EthnicityCode3,
					EthnicityCode4,
					EthnicityCode5,
					DateAdded
				)
			VALUES
				(
					s.Derkey1,
					s.School,
					s.AcYear,
					s.LocStudentId,
					s.StudentId,
					s.CSISNum,
					s.Fname,
					s.Lname,
					s.Gender,
					s.Ethnicity,
					s.Birthdate,
					s.GradeLevel,
					s.HomeLanguage,
					s.HispanicEthnicity,
					s.EthnicityCode1,
					s.EthnicityCode2,
					s.EthnicityCode3,
					s.EthnicityCode4,
					s.EthnicityCode5,
					s.DateAdded
				);
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message =
			nchar(9) + N'Error Procedure: ' + upper(object_name(@@PROCID)) + char(13) + char(10) +
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		-- put error message
		THROW @ErrorCode, @Message, @ErrorSeverity;
	END CATCH;
END;