ALTER PROCEDURE
    dbo.SessionStatusMonitor
    (
        @SessionId  integer,
        @Recipients varchar(8000)
    )
AS

    SET NOCOUNT ON;
    SET XACT_ABORT ON;

DECLARE
    -- email
    @ProfileName    sysname        = 'Cal-PASS SQL Support Profile',
    @CopyRecipients varchar(8000)  = 'dlamoree@edresults.org',
    @Subject        nvarchar(255)  = 'Session Complete',
    @Body           nvarchar(max)  = N'',
    @BodyFormat     varchar(20)    = 'HTML',
    @Space          nchar(5)       = N'&nbsp',
    @CRLF           nchar(4)       = N'<br>',
    -- session
    @IsRunning      bit            = 1,
    @HostName       nvarchar(128)  = N'',
    @ProgramName    nvarchar(128)  = N'',
    @LoginName      sysname        = N'',
    @Statement      nvarchar(max)  = N'',
    -- internal
    @ErrorNumber    integer        = 51000,
    @ErrorMessage   nvarchar(4000) = N'',
    @ErrorState     integer        = 1;

BEGIN
    -- validate session active
    IF (NOT EXISTS (SELECT 1 FROM sys.dm_exec_sessions WHERE session_id = @SessionId))
        BEGIN
            SET @ErrorMessage  = N'Session is not active';
            THROW @ErrorNumber, @ErrorMessage, @ErrorState;
        END;

    -- collection info
    SELECT
        @HostName    = s.host_name,
        @ProgramName = s.program_name,
        @LoginName   = s.login_name,
        @Statement   = t.text
    FROM
        sys.dm_exec_sessions s
        left outer join
        sys.dm_exec_requests r
            on s.session_id = r.session_id
        outer apply
        sys.dm_exec_sql_text(r.sql_handle) t
    WHERE
        s.session_id = @SessionId;

    -- loop
     WHILE (@IsRunning = 1)
     BEGIN

         -- wait five seconds
         WAITFOR DELAY '00:00:10'

         -- check if running
         SELECT
             @IsRunning = case when count(*) > 0 then 1 else 0 end
         FROM
             sys.dm_exec_requests
         WHERE
             session_id = @SessionId;
     END;

    -- set email body
    SET @Body = 
        'Session Details' + char(13) + char(10) + char(13) + char(10) + 
        'LoginName:   ' + isnull(@LoginName, '') + char(13) + char(10) + 
        'ProgramName: ' + isnull(@ProgramName, '') + char(13) + char(10) + 
        'HostName:    ' + isnull(@HostName, '') + char(13) + char(10) + 
        char(13) + char(10) + 
        'Statement:   ' + char(13) + char(10) + 
        isnull(@Statement, '');

    SET @Body = '<tt>' + @Body + '</tt>';
    SET @Body = replace(@Body, char(13) + char(10), @CRLF);
    SET @Body = replace(@Body, '  ', @Space + @Space);

    -- exe email proc
    EXEC msdb.dbo.sp_send_dbmail
        @profile_name    = @ProfileName,
        @recipients      = @Recipients,
        @copy_recipients = @CopyRecipients,
        @subject         = @Subject,
        @body            = @Body,
        @body_format     = @BodyFormat;

END;