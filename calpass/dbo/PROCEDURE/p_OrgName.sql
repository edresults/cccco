USE calpass;

GO

IF (object_id('dbo.p_OrgName') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_OrgName;
	END;

GO

CREATE PROCEDURE
	dbo.p_OrgName
	(
		@OrganizationId int
	)
AS
BEGIN
	SELECT
		*
	FROM
		calpass.dbo.Organization
	WHERE
		OrganizationId = @OrganizationId;
END;