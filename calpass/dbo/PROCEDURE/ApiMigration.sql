USE calpass;

GO

IF (object_id('dbo.ApiMigration') is not null)
	BEGIN
		DROP PROCEDURE dbo.ApiMigration;
	END;

GO

CREATE PROCEDURE
	dbo.ApiMigration
AS

	SET NOCOUNT ON;

DECLARE
	-- error message
	@Message       nvarchar(2048),
	@ErrorCode     int = 70099,
	@ErrorSeverity int = 16,
	-- mail vas
	@ProfileName   sysname = 'Cal-PASS SQL Support Profile',
	@Recipients    varchar(max) = 'dlamoree@edresults.org',
	@Subject       nvarchar(255) = object_name(@@PROCID);

BEGIN
	--
	-- CPPDELSQLP01
	--
	BEGIN TRY
		-- Migrate Placement
		EXECUTE mmap.PlacementMigrate;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'mmap.PlacementMigrate';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	BEGIN TRY
		-- Migrate Transcript
		EXECUTE mmap.TranscriptMigrate;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'mmap.TranscriptMigrate';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	BEGIN TRY
		-- Migrate Placement
		EXECUTE mmap.K12PlacementMigrate;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		SET @Subject = 'mmap.K12PlacementMigrate';
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	BEGIN TRY
		-- Migrate Transcript
		EXECUTE mmap.K12TranscriptMigrate;
		SET @Subject = 'mmap.K12TranscriptMigrate';
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message = isnull(ERROR_MESSAGE(), N'');
		-- put error message
		RAISERROR(@Message, 0, 1) WITH NOWAIT;
		-- email error
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = @ProfileName,
			@recipients = @Recipients,
			@subject = @Subject,
			@body = @Message;
		-- exit
		RETURN;
	END CATCH;

	UPDATE
		calpads.Students
	SET
		IsEscrow = 0
	WHERE
		IsEscrow = 1;
END;