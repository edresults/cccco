USE calpass;

GO

IF (object_id('dbo.LockResource') is not null)
	BEGIN
		DROP PROCEDURE dbo.LockResource;
	END;

GO

CREATE PROCEDURE
	dbo.LockResource
	(
		@Resource dbo.Resource READONLY,
		@IsSuccess bit = 0 out
	)
AS

	SET NOCOUNT ON;

DECLARE
	-- applock
	@LockMode varchar(32) = 'Exclusive',
	@LockOwner varchar(32) = 'Session',
	@LockTimeout int = null,
	@DbPrincipal sysname = N'public',
	@LockCode smallint,
	-- error message
	@Message nvarchar(2048),
	@ErrorCode int = 70099,
	@ErrorSeverity int = 16,
	-- proc vars
	@Resources dbo.Resource,
	@Locked bit = 1,
	@ResourceName nvarchar(255),
	@i int = 0,
	@c int = 0;

BEGIN
	-- create local resource table for modification
	INSERT
		@Resources
		(
			ResourceName,
			IsLocked
		)
	SELECT
		ResourceName,
		IsLocked
	FROM
		@Resource;
	-- assign upper bound
	SELECT
		@c = count(*)
	FROM
		@Resources;

	WHILE (@i < @c)
	BEGIN
		-- Resource Name
		SELECT
			@ResourceName = ResourceName
		FROM
			@Resources
		WHERE
			Iterator = @i;

		BEGIN TRY
			EXECUTE @LockCode = sys.sp_getapplock
				@Resource = @ResourceName,
				@LockMode = @LockMode,
				@LockOwner = @LockOwner,
				@LockTimeout = @LockTimeout,
				@DbPrincipal = @DbPrincipal;

			SET @Message = 
				case @LockCode
					when 0 then 'The lock was successfully granted synchronously.'
					when 1 then 'The lock was granted successfully after waiting for other incompatible locks to be released.'
					when -1 then 'The lock request timed out.'
					when -2 then 'The lock request was canceled.'
					when -3 then 'The lock request was chosen as a deadlock victim.'
					when -999 then 'Indicates a parameter validation or other call error.'
				end

			IF (@LockCode in (0, 1))
				BEGIN
					UPDATE
						@Resources
					SET
						IsLocked = @Locked
					WHERE
						Iterator = @i;
				END;
			ELSE
				BEGIN
					THROW @ErrorCode, @Message, @ErrorSeverity;
				END;

		END TRY
		BEGIN CATCH

			SET @Message = 'Could not lock ' + @ResourceName + '; Rolling back all locks and exiting';
			PRINT @Message;
			
			-- delete all unlocked resources
			DELETE
			FROM
				@Resources
			WHERE
				IsLocked != @Locked;

			-- unlock all locked resources
			EXECUTE dbo.UnlockResource
				@Resource = @Resources;

			-- error
			SET @IsSuccess = 0;

			-- exit
			RETURN;

		END CATCH;

		SET @i += 1;
	END;

	SET @IsSuccess = 1;
END;