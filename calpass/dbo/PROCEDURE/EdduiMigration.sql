USE calpass;

GO

DROP TABLE calpass.dbo.Eddui;

GO

SELECT
	*
INTO
	calpass.dbo.Eddui
FROM
	[PRO-DAT-SQL-03].calpass.dbo.Eddui;

GO

CREATE CLUSTERED INDEX
	IC_Eddui
ON
	calpass.dbo.Eddui
	(
		StudentIdSsnEnc,
		YearId,
		QtrId,
		Naics
	);

GO

USE WAGESCA;

GO

DROP TABLE dbo.EncryptedYearQtrWages;

GO

SELECT
	EncryptedSSN = CONVERT(BINARY(64), StudentIdSsnEnc),
	YearQtr      = CONVERT(CHAR(5), convert(char(4), YearId) + convert(char(1), QtrId)),
	Wages        = CONVERT(INTEGER, sum(Wages))
INTO
	dbo.EncryptedYearQtrWages
FROM
	calpass.dbo.Eddui
GROUP BY
	StudentIdSsnEnc,
	YearId,
	QtrId;

GO

ALTER TABLE DBO.EncryptedYearQtrWages ALTER COLUMN EncryptedSSN BINARY(64) NOT NULL;
ALTER TABLE DBO.EncryptedYearQtrWages ALTER COLUMN YearQtr      CHAR(5)    NOT NULL;

GO

ALTER TABLE
	dbo.EncryptedYearQtrWages
ADD CONSTRAINT
	PK_EncryptedYearQtrWages
PRIMARY KEY CLUSTERED
	(
		EncryptedSSN,
		YearQtr
	);