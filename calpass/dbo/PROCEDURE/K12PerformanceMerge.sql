USE calpass;

GO

IF (object_id('mmap.K12PerformanceMerge') is not null)
	BEGIN
		DROP PROCEDURE mmap.K12PerformanceMerge;
	END;

GO

CREATE PROCEDURE
	mmap.K12PerformanceMerge
	(
		@Escrow calpads.Escrow READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Message nvarchar(2048),
	@ErrorCode int = 70099,
	@ErrorSeverity int = 1;

BEGIN

	SELECT DISTINCT
		StudentStateId,
		CSISNum = dbo.Get9812Encryption(StudentStateId, 'X')
	INTO
		#Escrow
	FROM
		@Escrow;

	DELETE
		a
	FROM
		#Escrow a
	WHERE
		exists (
			SELECT
				1
			FROM
				#Escrow b
			WHERE
				a.StudentStateId = b.StudentStateId
			HAVING
				count(*) > 1
		);

	BEGIN TRY
		MERGE
			mmap.K12Performance
		WITH
			(
				TABLOCKX
			) t
		USING
			(
				SELECT
					i.StudentStateId,
					rp.DepartmentCode,
					rp.GradeCode,
					rp.QualityPoints,
					rp.CreditAttempted,
					rp.GradePointAverage,
					rp.GradePointAverageSans,
					rp.CumulativeQualityPoints,
					rp.CumulativeCreditAttempted,
					rp.CumulativeGradePointAverage,
					rp.CumulativeGradePointAverageSans,
					rp.FirstYearTermCode,
					rp.LastYearTermCode,
					rp.IsFirst,
					rp.IsLast
				FROM
					#Escrow i
					cross apply
					Mmap.K12PerformanceGet(i.CSISNum) rp
			) s
		ON
			s.StudentStateId = t.StudentStateId
			and s.DepartmentCode = t.DepartmentCode
			and s.GradeCode = t.GradeCode
		WHEN MATCHED THEN
			UPDATE SET
				t.QualityPoints                   = s.QualityPoints,
				t.CreditAttempted                 = s.CreditAttempted,
				t.GradePointAverage               = s.GradePointAverage,
				t.GradePointAverageSans           = s.GradePointAverageSans,
				t.CumulativeQualityPoints         = s.CumulativeQualityPoints,
				t.CumulativeCreditAttempted       = s.CumulativeCreditAttempted,
				t.CumulativeGradePointAverage     = s.CumulativeGradePointAverage,
				t.CumulativeGradePointAverageSans = s.CumulativeGradePointAverageSans,
				t.FirstYearTermCode               = s.FirstYearTermCode,
				t.LastYearTermCode                = s.LastYearTermCode,
				t.IsFirst                         = s.IsFirst,
				t.IsLast                          = s.IsLast
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					StudentStateId,
					DepartmentCode,
					GradeCode,
					QualityPoints,
					CreditAttempted,
					GradePointAverage,
					GradePointAverageSans,
					CumulativeQualityPoints,
					CumulativeCreditAttempted,
					CumulativeGradePointAverage,
					CumulativeGradePointAverageSans,
					FirstYearTermCode,
					LastYearTermCode,
					IsFirst,
					IsLast
				)
			VALUES
				(
					s.StudentStateId,
					s.DepartmentCode,
					s.GradeCode,
					s.QualityPoints,
					s.CreditAttempted,
					s.GradePointAverage,
					s.GradePointAverageSans,
					s.CumulativeQualityPoints,
					s.CumulativeCreditAttempted,
					s.CumulativeGradePointAverage,
					s.CumulativeGradePointAverageSans,
					s.FirstYearTermCode,
					s.LastYearTermCode,
					s.IsFirst,
					s.IsLast
				);
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message =
			nchar(9) + N'Error Procedure: ' + upper(object_name(@@PROCID)) + char(13) + char(10) +
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		-- put error message
		THROW @ErrorCode, @Message, @ErrorSeverity;
	END CATCH;
END;