USE calpass;

GO

IF (object_id('calpass.dbo.p_bsteele_cohort_hs_insert', 'P') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_bsteele_cohort_hs_insert;
	END;

GO

CREATE PROCEDURE
	dbo.p_bsteele_cohort_hs_insert
AS

	SET NOCOUNT ON;

BEGIN

	TRUNCATE TABLE calpass.dbo.bsteele_cohort_hs;

	-- insert courses
	INSERT INTO
		calpass.dbo.bsteele_cohort_hs
		(
			/* pk */
			derkey1,
			/* school */
			organization_cds_code,
			organization_grade_level_code,
			organization_year_code,
			organization_term_code,
			/* course */
			course_title,
			course_id,
			course_code,
			course_subject,
			course_ag_code,
			course_ap_ind,
			course_ib_ind,
			course_cte_ind,
			course_exp_ind,
			course_avid_ind,
			/* section */
			section_id,
			section_credit_attempted,
			section_credit_achieved,
			section_grade,
			section_grade_points
		)
	SELECT
		/* pk */
		t.derkey1,
		/* school */
		t.organization_cds_code,
		t.organization_grade_level_code,
		t.organization_year_code,
		isnull(t.organization_term_code, ''),
		/* course */
		isnull(t.course_title, ''),
		isnull(t.course_id, ''),
		isnull(t.course_code, ''),
		t.course_subject,
		t.course_ag_code,
		t.course_ap_ind,
		t.course_ib_ind,
		t.course_cte_ind,
		t.course_exp_ind,
		t.course_avid_ind,
		/* section */
		isnull(t.section_id, ''),
		t.section_credit_attempted,
		t.section_credit_achieved,
		isnull(t.section_grade, ''),
		t.section_grade_points
	FROM
		calpass.dbo.bsteele_cohort c
		cross apply
		calpass.dbo.f_hs_student_transcript(
			c.derkey1,
			default,
			default
		) t;

	/* student */
	UPDATE
		t
	SET
		t.student_gender = a.student_gender,
		t.student_ethnicity = a.student_ethnicity,
		t.student_hispanic_ind = a.student_hispanic_ind,
		t.student_ses_ind = a.student_ses_ind,
		t.student_birthdate = a.student_birthdate,
		t.student_language = a.student_language
	FROM
		calpass.dbo.bsteele_cohort_hs t
		inner join
		calpass.dbo.bsteele_cohort c
			on c.derkey1 = t.derkey1
		cross apply
		calpass.dbo.f_hs_student_attributes(
			c.derkey1,
			default
		) a;

	/* summary: cgpa */
	UPDATE
		t
	SET
		t.student_cumulative_gpa = g.student_cumulative_gpa
	FROM
		calpass.dbo.bsteele_cohort_hs t
		inner join
		calpass.dbo.bsteele_cohort c
			on c.derkey1 = t.derkey1
		cross apply
		calpass.dbo.f_hs_student_cgpa(
			c.derkey1,
			default,
			default
		) g;

	/* summary: mathematics */
	UPDATE
		t
	SET
		t.student_highest_math_title = m.course_title,
		t.student_highest_math_grade = m.section_grade
	FROM
		calpass.dbo.bsteele_cohort_hs t
		inner join
		calpass.dbo.bsteele_cohort c
			on c.derkey1 = t.derkey1
		cross apply
		calpass.dbo.f_hs_student_subject_highest(
			c.derkey1,
			'Mathematics',
			default
		) m;

	/* summary: english */
	UPDATE
		t
	SET
		t.student_highest_engl_title = e.course_title,
		t.student_highest_engl_grade = e.section_grade
	FROM
		calpass.dbo.bsteele_cohort_hs t
		inner join
		calpass.dbo.bsteele_cohort c
			on c.derkey1 = t.derkey1
		cross apply
		calpass.dbo.f_hs_student_subject_highest(
			c.derkey1,
			'English Language Arts',
			default
		) e;

	/* concurrency */
	UPDATE
		t
	SET
		t.ccc_concurrent_section_enrollment_count = ccc.ccc_concurrent_section_enrollment_count,
		t.ccc_concurrent_section_units_attempted = ccc.ccc_concurrent_section_units_attempted,
		t.ccc_concurrent_section_units_achieved = ccc.ccc_concurrent_section_units_achieved,
		t.ccc_concurrent_section_grade_points = ccc.ccc_concurrent_section_grade_points
	FROM
		calpass.dbo.bsteele_cohort_hs t
		inner join
		(
			SELECT DISTINCT
				derkey1,
				organization_year_code
			FROM
				calpass.dbo.bsteele_cohort_hs a
		) a
			on a.derkey1 = t.derkey1
			and a.organization_year_code = t.organization_year_code
		cross apply
		calpass.dbo.f_hs_student_concurrency(
			a.derkey1,
			a.organization_year_code
		) ccc;

	/* higher education */
	UPDATE
		t
	SET
		t.student_ccc_first_term_value = she.student_ccc_first_term_value,
		t.student_ccc_first_term_nsa_value = she.student_ccc_first_term_nsa_value,
		t.student_ccc_first_term_cr_value = she.student_ccc_first_term_cr_value,
		t.student_ccc_first_term_cr_nsa_value = she.student_ccc_first_term_cr_nsa_value,
		t.student_ccc_first_term_ncr_value = she.student_ccc_first_term_ncr_value,
		t.student_ccc_first_term_ncr_nsa_value = she.student_ccc_first_term_ncr_nsa_value,
		t.student_univ_first_date = she.student_univ_first_date,
		t.student_univ_first_code = she.student_univ_first_code,
		t.student_univ_last_date = she.student_univ_last_date,
		t.student_univ_last_code = she.student_univ_last_code
	FROM
		calpass.dbo.bsteele_cohort_hs t
		inner join
		(
			SELECT
				c.derkey1,
				student_ccc_first_term_value = min(hf.ccc_first_term_value),
				student_ccc_first_term_nsa_value = min(hf.ccc_first_term_nsa_value),
				student_ccc_first_term_cr_value = min(hf.ccc_first_term_cr_value),
				student_ccc_first_term_cr_nsa_value = min(hf.ccc_first_term_cr_nsa_value),
				student_ccc_first_term_ncr_value = min(hf.ccc_first_term_ncr_value),
				student_ccc_first_term_ncr_nsa_value = min(hf.ccc_first_term_ncr_nsa_value),
				student_univ_first_date = hf.first_date_4yr,
				student_univ_first_code = hf.first_segment_4yr,
				student_univ_last_date = hf.last_date_4yr,
				student_univ_last_code = hf.last_segment_4yr
			FROM
				(
					SELECT DISTINCT
						derkey1 = s.derkey1,
						college_id = o.code,
						student_key = s.StudentId,
						id_status = s.IdStatus
					FROM
						calpass.dbo.bsteele_cohort c
						inner join
						calpass.dbo.ccstudentprod s
							with(index(idx_ccstud_derkey))
							on c.derkey1 = s.derkey1
						inner join
						comis.dbo.district_college o
							on s.collegeId = o.code_ipeds_legacy
				) c
				inner join
				comis.dbo.studntid id
					with(index(uk_studntid__college_id__ssn__student_id_status))
					on id.college_id = c.college_id
					and id.ssn = c.student_key
					and id.student_id_status = c.id_status
				inner join
				comis.dbo.hf_first hf
					on hf.college_id = id.college_id
					and hf.student_id = id.student_id
			GROUP BY
				c.derkey1,
				hf.first_date_4yr,
				hf.first_segment_4yr,
				hf.last_date_4yr,
				hf.last_segment_4yr
		) she
			on t.derkey1 = she.derkey1;

	/* mt san jacinto college dual enrollment indicator */
	UPDATE
		t
	SET
		t.student_msjc_de_ind = case
			when msj.derkey1 is not null then 1
			else 0
		end
	FROM
		calpass.dbo.bsteele_cohort_hs t
		left outer join
		calpass.dbo.bsteele_cohort_msj msj
			on msj.derkey1 = t.derkey1
			and msj.year_code = t.organization_year_code

	/* graduation indicator */
	UPDATE
		t
	SET
		t.student_award_code = a.AwardType
	FROM
		calpass.dbo.bsteele_cohort_hs t
		left outer join
		calpass.dbo.k12awardprod a
			on t.derkey1 = a.derkey1
			and t.organization_year_code = a.Acyear;
END;