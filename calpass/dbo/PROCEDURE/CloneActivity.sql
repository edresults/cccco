USE calpass;

GO

IF (object_id('CloneActivity') is not null)
	BEGIN
		DROP PROCEDURE CloneActivity;
	END;

GO

CREATE PROCEDURE
	dbo.CloneActivity
	(
		@IsActive bit = null
	)
AS

DECLARE
	-- job variables
	@JobId               uniqueidentifier = 'F79277AE-2CBB-432B-85B4-5599DF3D0DF2',
	@IsEnabled           tinyint          = null,
	-- email variables
	@ProfileName         sysname          = 'Cal-PASS SQL Support Profile',
	@BlindCopyRecipients varchar(max)     = 'dlamoree@edresults.org;achan@edresults.org;erice@edresults.org;austinchan@edresults.org;hku@edresults.org;adalton@edresults.org;jbarajas@edresults.org',
	@Subject             varchar(255)     = 'CLONE :: CALPASS :: ',
	@Body                varchar(max)     = 'At the request of ' + ORIGINAL_LOGIN() + ', the cloning of [PRO-DAT-SQL-03].[CALPASS] to [DEV-DAT-SQL-04].[CALPASS] is ',
	-- misc variables
	@Noun                varchar(255)     = '';

BEGIN

	IF (@IsActive is null)
		BEGIN
			SELECT
				@IsEnabled = enabled
			FROM
				[PRO-DAT-SQL-03].msdb.dbo.sysjobs
			WHERE
				job_id = @JobId;

			IF (@IsEnabled = 0)
				BEGIN
					PRINT 'Clone Job Step Status: INACTIVE';
				END;
			ELSE IF (@IsEnabled = 1)
				BEGIN
					PRINT 'Clone Job Step Status: ACTIVE';
				END;

			RETURN;
		END;

	IF (@IsActive = 0)
		BEGIN
			SET @IsEnabled = 0; -- Disable
			SET @Noun      = 'INACTIVE';
		END;
	ELSE IF (@IsActive = 1)
		BEGIN
			SET @IsEnabled = 1; -- Enable
			SET @Noun      = 'ACTIVE';
		END;

	SET @Subject = @Subject + @Noun;
	SET @Body    = @Body + @Noun + '.';

	EXECUTE [PRO-DAT-SQL-03].msdb.dbo.sp_update_job
		@job_id  = @JobId,
		@enabled = @IsEnabled;

	EXECUTE [PRO-DAT-SQL-03].msdb.dbo.sp_send_dbmail
		@profile_name          = @ProfileName,
		@blind_copy_recipients = @BlindCopyRecipients,
		@subject               = @Subject,
		@body                  = @Body;
END;