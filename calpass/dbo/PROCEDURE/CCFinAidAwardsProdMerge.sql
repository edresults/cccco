USE calpass;

GO

IF (object_id('dbo.CCFinAidAwardsProdMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.CCFinAidAwardsProdMerge;
	END;

GO

CREATE PROCEDURE
	dbo.CCFinAidAwardsProdMerge
	(
		@CCFinAidAwardsProd dbo.CCFinAidAwardsProd READONLY
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Severity tinyint = 0,
	@State tinyint = 1,
	@RowCount varchar(2048),
	@Error varchar(2048);

BEGIN
	MERGE
		dbo.CCFinAidAwardsProd t
	USING
		@CCFinAidAwardsProd s
	ON
		t.CollegeId = s.CollegeId
		and t.StudentId = s.StudentId
		and t.IdStatus = s.IdStatus
		and t.TermId = s.TermId
		and t.TermRecd = s.TermRecd
		and t.TypeId = s.TypeId
	WHEN MATCHED THEN
		UPDATE SET
			t.StudentComisId = s.StudentComisId,
			t.Amount = s.Amount
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				TermRecd,
				TypeId,
				Amount
			)
		VALUES
			(
				s.CollegeId,
				s.StudentComisId,
				s.StudentId,
				s.IdStatus,
				s.TermId,
				s.TermRecd,
				s.TypeId,
				s.Amount
			);
	-- capture
	SET @RowCount = convert(varchar(2048), @@ROWCOUNT);
	-- set
	SET @Error = 'Merged ' + @RowCount + ' dbo.CCFinAidAwardsProd records';
	-- output
	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;
END;