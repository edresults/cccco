USE calpass;

GO

IF (object_id('dbo.HSCgpaMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.HSCgpaMerge;
	END;

GO

CREATE PROCEDURE
	dbo.HSCgpaMerge
	(
		@Student dbo.Student READONLY
	)
AS

	SET NOCOUNT ON;

BEGIN
	MERGE
		HSCgpa t
	USING
		(
			SELECT
				s.InterSegmentKey,
				LastGradeCode = max(GradeCode),
				CreditTry = sum(CreditTry),
				CreditGet = sum(CreditGet),
				QualityPoints = sum(CreditPoints),
				Cgpa = 
					isnull(
						convert(
							decimal(4,3),
							sum(CreditPoints) / nullif(sum(CreditTry), 0)
						),
						0
					)
			FROM
				@Student s
				cross apply
				dbo.HSTranscriptGet(s.InterSegmentKey) t
			GROUP BY
				s.InterSegmentKey
		) s
	ON
		t.InterSegmentKey = s.InterSegmentKey
	WHEN MATCHED THEN
		UPDATE SET
			t.InterSegmentKey = s.InterSegmentKey,
			t.LastGradeCode = s.LastGradeCode,
			t.Cgpa = s.Cgpa
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				InterSegmentKey,
				LastGradeCode,
				Cgpa
			)
		VALUES
			(
				s.InterSegmentKey,
				s.LastGradeCode,
				s.Cgpa
			);
END;