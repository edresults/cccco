CREATE PROCEDURE
	dbo.CCCApplyExtract
AS

BEGIN
	SELECT
		app_id,
		ccc_id,
		college_id,
		ssn,
		isk,
		AcademicYear =  case
							when Term in ('Summer', 'Fall')   then convert(char(4), Year) + '-' + convert(char(4), Year + 1)
							when Term in ('Winter', 'Spring') then convert(char(4), Year - 1) + '-' + convert(char(4), Year)
						end
	FROM
		(
			SELECT
				app_id,
				ccc_id,
				college_id,
				ssn = HASHBYTES('SHA2_512', replace(ssn, '-', '')),
				isk = isk.InterSegmentKey,
				Term = coalesce(
						case when term_description like '%Summer%'       then 'Summer' end,
						case when term_description like '%Sumner%'       then 'Summer' end,
						case when term_description like '%Fall%'         then 'Fall'   end,
						case when term_description like '%Winter%'       then 'Winter' end,
						case when term_description like '%Intersession%' then 'Winter' end,
						case when term_description like '%Spring%'       then 'Spring' end,
						case when term_description like '%Sping%'        then 'Spring' end,
						-- user transposed term_code and term_description
						case when term_code        like '%Summer%'       then 'Summer' end,
						case when term_code        like '%Sumner%'       then 'Summer' end,
						case when term_code        like '%Fall%'         then 'Fall'   end,
						case when term_code        like '%Winter%'       then 'Winter' end,
						case when term_code        like '%Intersession%' then 'Winter' end,
						case when term_code        like '%Spring%'       then 'Spring' end,
						case when term_code        like '%Sping%'        then 'Spring' end,
						-- user using non-typical word boundary
						case when term_code        like '%SP%'           then 'Spring' end,
						-- Citrus
						case when college_id = '821' and term_code = '201420' then 'Fall'   end,
						case when college_id = '821' and term_code = '201425' then 'Winter' end,
						case when college_id = '821' and term_code = '201430' then 'Spring' end,
						case when college_id = '821' and term_code = '201440' then 'Summer' end,
						-- Irvine Valley
						case when college_id = '892' and term_code = '20172'  then 'Summer' end
					),
				Year = coalesce(
						case when term_description like '%2027%' then 2027 end,
						case when term_description like '%2026%' then 2026 end,
						case when term_description like '%2025%' then 2025 end,
						case when term_description like '%2024%' then 2024 end,
						case when term_description like '%2023%' then 2023 end,
						case when term_description like '%2022%' then 2022 end,
						case when term_description like '%2021%' then 2021 end,
						case when term_description like '%2020%' then 2020 end,
						case when term_description like '%2019%' then 2019 end,
						case when term_description like '%2018%' then 2018 end,
						case when term_description like '%2017%' then 2017 end,
						case when term_description like '%2016%' then 2016 end,
						case when term_description like '%2015%' then 2015 end,
						case when term_description like '%2014%' then 2014 end,
						case when term_description like '%2013%' then 2013 end,
						case when term_description like '%2012%' then 2012 end,
						case when term_description like '%2011%' then 2011 end,
						-- Siskiyous :: Calendar (decrement if winter or spring)
						case when college_id = '181' and term_code = '202010' then 2020 end,
						-- De Anza :: Academic Year, Ending
						case when college_id = '421' and term_code = '201823' then 2018 end,
						-- Bakersfield :: Calendar Year (decrement if winter or spring)
						case when college_id = '521' and term_code = '201750' then 2018 end,
						-- East LA :: Calendar Year (decrement if winter or spring)
						case when college_id = '748' and term_code = '2152'   then 2015 end,
						case when college_id = '748' and term_code = '2154'   then 2015 end,
						-- West LA :: Calendar Year (decrement if winter or spring)
						case when college_id = '749' and term_code = '2154'   then 2015 end,
						-- Citrus :: Academic Year, Ending
						case when college_id = '821' and term_code = '201420' then 2014 end,
						case when college_id = '821' and term_code = '201425' then 2014 end,
						case when college_id = '821' and term_code = '201430' then 2014 end,
						case when college_id = '821' and term_code = '201440' then 2015 end,
						-- MSAC :: Academic Year, Beginning
						case when college_id = '851' and term_code = '201720' then 2017 end,
						-- Rio Hondo :: Calendar Year (decrement if winter or spring)
						case when college_id = '881' and term_code = '201430' then 2014 end,
						-- Irvine Valley :: Calendar Year (decrement if winter or spring)
						case when college_id = '892' and term_code = '20172'  then 2018 end
					)
			FROM
				OPENQUERY(
					CCCAPPLY,
					'
						SELECT app_id, CAST(ccc_id AS VARCHAR(8)), CAST(college_id AS CHAR(3)), CAST(ssn AS CHAR(11)), CAST(pg_firstname AS VARCHAR(30)), CAST(pg_lastname AS VARCHAR(40)), CAST(gender AS CHAR(1)), birthdate, CAST(edu_goal AS CHAR(1)), CAST(term_code AS VARCHAR(100)), cast(term_description as VARCHAR(100)) FROM [public].[submitted_application];
					'
				)
				cross apply
				dbo.InterSegmentKey(pg_firstname, pg_lastname, case gender when '' then 'X' else gender end, convert(char(8), birthdate, 112)) isk
		) a
END;