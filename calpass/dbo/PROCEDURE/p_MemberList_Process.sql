USE calpass;

GO

IF (object_id('dbo.p_MemberList_Process') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_MemberList_Process;
	END;

GO

CREATE PROCEDURE
	dbo.p_MemberList_Process
AS

DECLARE
	@ErrorCode int = 70099,
	@ErrorSeverity int = 1,
	@Message nvarchar(2048);

BEGIN
	IF (@@ServerName != 'PRO-DAT-SQL-03')
		BEGIN
			SET @Message = 'Procedure must originate from PRO-DAT-SQL-03';
			THROW @ErrorCode, @Message, @ErrorSeverity;
		END;

	ALTER TABLE
		mmap.RemedialUpperBound
	NOCHECK CONSTRAINT
		fk_RemedialUpperBound__Organization;

	ALTER TABLE
		mmap.Model
	NOCHECK CONSTRAINT
		fk_Model__Organization;
		
	-- dbo.Organization
	DELETE dbo.Organization;

	INSERT INTO
		dbo.Organization
		(
			OrganizationId,
			OrganizationTypeId,
			OrganizationName,
			OrganizationCode,
			OrganizationCodeTypeId,
			ParentId,
			CountyId,
			Submit_Files,
			school_cd
		)
	SELECT
		OrganizationId,
		OrganizationTypeId,
		OrganizationName,
		OrganizationCode,
		OrganizationCodeTypeId,
		ParentId,
		CountyId,
		Submit_Files,
		school_cd
	FROM
		[PRO-DAT-SQL-01].calpass.dbo.Organization;

	ALTER TABLE
		mmap.RemedialUpperBound
	WITH CHECK CHECK CONSTRAINT
		fk_RemedialUpperBound__Organization;
		
	ALTER TABLE
		mmap.Model
	WITH CHECK CHECK CONSTRAINT
		fk_Model__Organization;

	-- dbo.CPP_Mou
	SET IDENTITY_INSERT dbo.CPP_Mou ON;
	
	INSERT INTO
		dbo.CPP_Mou
		(
			mouId,
			organizationId,
			submittedBy,
			submittedDate,
			expirationDate,
			notes,
			restricted,
			deleted,
			deletedReason
		)
	SELECT
		mouId,
		organizationId,
		submitedBy,
		submittedDate,
		expirationDate,
		notes,
		restricted,
		deleted,
		deletedReason
	FROM
		[PRO-DAT-SQL-01].calpass.dbo.CPP_Mou;

	SET IDENTITY_INSERT dbo.CPP_Mou OFF;

	-- remove all records; fresh table
	TRUNCATE TABLE dbo.MemberList;

	-- K12 :: Course
	MERGE
		MemberList with (tablockx) target
	USING
		(
			SELECT
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				AcademicYear = y.YearCode,
				CourseCount = sum(m.RecordCount)
			FROM
				(
					SELECT
						School,
						AcYear,
						RecordCount = count(*)
					FROM
						dbo.K12CourseProd
							with(
								tablockx,
								index(ix_K12CourseProd__School__AcYear)
							)
					GROUP BY
						School,
						AcYear
				) m
				inner join
				calpads.Year y
					with(index(ix_Year__YearCodeAbbr))
					on y.YearCodeAbbr = m.AcYear
				inner join
				dbo.Organization o
					with(index(ix_Organization__OrganizationCode))
					on o.OrganizationCode = substring(m.School, 1, 7) + '0000000'
			GROUP BY
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				y.YearCode
		) source
	ON
		target.OrganizationId = source.OrganizationId
		and target.AcademicYear = source.AcademicYear
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				OrganizationId,
				OrganizationTypeId,
				OrganizationName,
				AcademicYear,
				CountyId,
				CourseCount
			)
		VALUES
			(
				source.OrganizationId,
				source.OrganizationTypeId,
				source.OrganizationName,
				source.AcademicYear,
				source.CountyId,
				source.CourseCount
			)
	WHEN MATCHED THEN
		UPDATE SET
			target.CourseCount = source.CourseCount;

	-- K12 :: Student
	MERGE
		MemberList with (tablockx) target
	USING
		(
			SELECT
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				AcademicYear = y.YearCode,
				StudentCount = sum(m.RecordCount)
			FROM
				(
					SELECT
						School,
						AcYear,
						RecordCount = count(*)
					FROM
						dbo.K12StudentProd
							with(
								tablockx,
								index(ix_K12StudentProd__School__AcYear)
							)
					GROUP BY
						School,
						AcYear
				) m
				inner join
				calpads.Year y
					with(index(ix_Year__YearCodeAbbr))
					on y.YearCodeAbbr = m.AcYear
				inner join
				dbo.Organization o
					with(index(ix_Organization__OrganizationCode))
					on o.OrganizationCode = substring(m.School, 1, 7) + '0000000'
			GROUP BY
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				y.YearCode
		) source
	ON
		target.OrganizationId = source.OrganizationId
		and target.AcademicYear = source.AcademicYear
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				OrganizationId,
				OrganizationTypeId,
				OrganizationName,
				AcademicYear,
				CountyId,
				StudentCount
			)
		VALUES
			(
				source.OrganizationId,
				source.OrganizationTypeId,
				source.OrganizationName,
				source.AcademicYear,
				source.CountyId,
				source.StudentCount
			)
	WHEN MATCHED THEN
		UPDATE SET
			target.StudentCount = source.StudentCount;

	-- K12 :: Award
	MERGE
		MemberList with (tablockx) target
	USING
		(
			SELECT
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				AcademicYear = y.YearCode,
				AwardCount = sum(m.RecordCount)
			FROM
				(
					SELECT
						School,
						AcYear,
						RecordCount = count(*)
					FROM
						dbo.K12AwardProd
							with(
								tablockx,
								index(pk_K12AwardProd)
							)
					GROUP BY
						School,
						AcYear
				) m
				inner join
				calpads.Year y
					with(index(ix_Year__YearCodeAbbr))
					on y.YearCodeAbbr = m.AcYear
				inner join
				dbo.Organization o
					with(index(ix_Organization__OrganizationCode))
					on o.OrganizationCode = substring(m.School, 1, 7) + '0000000'
			GROUP BY
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				y.YearCode
		) source
	ON
		target.OrganizationId = source.OrganizationId
		and target.AcademicYear = source.AcademicYear
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				OrganizationId,
				OrganizationTypeId,
				OrganizationName,
				AcademicYear,
				CountyId,
				AwardCount
			)
		VALUES
			(
				source.OrganizationId,
				source.OrganizationTypeId,
				source.OrganizationName,
				source.AcademicYear,
				source.CountyId,
				source.AwardCount
			)
	WHEN MATCHED THEN
		UPDATE SET
			target.AwardCount = source.AwardCount;

	-- CC :: Course
	MERGE
		MemberList with (tablockx) target
	USING
		(
			SELECT
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				t.AcademicYear,
				CourseCount = sum(m.RecordCount)
			FROM
				(
					SELECT
						CollegeId,
						TermId,
						RecordCount = count(*)
					FROM
						dbo.CCCourseProd
							with(
								tablockx,
								index(ix_CCCourseProd__CollegeId__TermId)
							)
					GROUP BY
						CollegeId,
						TermId
				) m
				inner join
				comis.Term t
					with(index(pk_Term))
					on m.TermId = t.TermCode
				inner join
				dbo.Organization o
					with(index(ix_Organization__OrganizationCode))
					on o.OrganizationCode = m.CollegeId
			GROUP BY
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				t.AcademicYear
		) source
	ON
		target.OrganizationId = source.OrganizationId
		and target.AcademicYear = source.AcademicYear
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				OrganizationId,
				OrganizationTypeId,
				OrganizationName,
				AcademicYear,
				CountyId,
				CourseCount
			)
		VALUES
			(
				source.OrganizationId,
				source.OrganizationTypeId,
				source.OrganizationName,
				source.AcademicYear,
				source.CountyId,
				source.CourseCount
			)
	WHEN MATCHED THEN
		UPDATE SET
			target.CourseCount = source.CourseCount;

	-- CC :: Student
	MERGE
		MemberList with (tablockx) target
	USING
		(
			SELECT
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				t.AcademicYear,
				StudentCount = sum(m.RecordCount)
			FROM
				(
					SELECT
						CollegeId,
						TermId,
						RecordCount = count(*)
					FROM
						dbo.CCStudentProd
							with(
								tablockx,
								index(ix_CCStudentProd__CollegeId__TermId)
							)
					GROUP BY
						CollegeId,
						TermId
				) m
				inner join
				comis.Term t
					with(index(pk_Term))
					on m.TermId = t.TermCode
				inner join
				dbo.Organization o
					with(index(ix_Organization__OrganizationCode))
					on o.OrganizationCode = m.CollegeId
			GROUP BY
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				t.AcademicYear
		) source
	ON
		target.OrganizationId = source.OrganizationId
		and target.AcademicYear = source.AcademicYear
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				OrganizationId,
				OrganizationTypeId,
				OrganizationName,
				AcademicYear,
				CountyId,
				StudentCount
			)
		VALUES
			(
				source.OrganizationId,
				source.OrganizationTypeId,
				source.OrganizationName,
				source.AcademicYear,
				source.CountyId,
				source.StudentCount
			)
	WHEN MATCHED THEN
		UPDATE SET
			target.StudentCount = source.StudentCount;

	-- CC :: Award
	MERGE
		MemberList with (tablockx) target
	USING
		(
			SELECT
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				t.AcademicYear,
				AwardCount = sum(m.RecordCount)
			FROM
				(
					SELECT
						CollegeId,
						TermId,
						RecordCount = count(*)
					FROM
						dbo.CCAwardProd
							with(
								tablockx,
								index(ix_CCAwardProd__CollegeId__TermId)
							)
					GROUP BY
						CollegeId,
						TermId
				) m
				inner join
				comis.Term t
					with(index(pk_Term))
					on m.TermId = t.TermCode
				inner join
				dbo.Organization o
					with(index(ix_Organization__OrganizationCode))
					on o.OrganizationCode = m.CollegeId
			GROUP BY
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				t.AcademicYear
		) source
	ON
		target.OrganizationId = source.OrganizationId
		and target.AcademicYear = source.AcademicYear
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				OrganizationId,
				OrganizationTypeId,
				OrganizationName,
				AcademicYear,
				CountyId,
				AwardCount
			)
		VALUES
			(
				source.OrganizationId,
				source.OrganizationTypeId,
				source.OrganizationName,
				source.AcademicYear,
				source.CountyId,
				source.AwardCount
			)
	WHEN MATCHED THEN
		UPDATE SET
			target.AwardCount = source.AwardCount;

	-- Univ :: Course
	MERGE
		MemberList with (tablockx) target
	USING
		(
			SELECT
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				t.AcademicYear,
				CourseCount = sum(RecordCount)
			FROM
				(
					SELECT
						School,
						YrTerm,
						RecordCount = count(*)
					FROM
						dbo.UnivCourseProd
							with(
								tablockx,
								index(IX_UnivCourseProd__School__YrTerm)
							)
					GROUP BY
						School,
						YrTerm
				) m
				inner join
				dbo.UnivTerm t
					with(index(pk_UnivTerm))
					on m.YrTerm = t.TermCode
				inner join
				dbo.Organization o
					with(index(ix_Organization__OrganizationCode))
					on o.OrganizationCode = m.School
			GROUP BY
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				t.AcademicYear
		) source
	ON
		target.OrganizationId = source.OrganizationId
		and target.AcademicYear = source.AcademicYear
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				OrganizationId,
				OrganizationTypeId,
				OrganizationName,
				AcademicYear,
				CountyId,
				CourseCount
			)
		VALUES
			(
				source.OrganizationId,
				source.OrganizationTypeId,
				source.OrganizationName,
				source.AcademicYear,
				source.CountyId,
				source.CourseCount
			)
	WHEN MATCHED THEN
		UPDATE SET
			target.CourseCount = source.CourseCount;

	-- Univ :: Student
	MERGE
		MemberList with (tablockx) target
	USING
		(
			SELECT
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				t.AcademicYear,
				StudentCount = sum(RecordCount)
			FROM
				(
					SELECT
						School,
						YrTerm,
						RecordCount = count(*)
					FROM
						dbo.UnivStudentProd
							with(
								tablockx,
								index(ix_UnivStudentProd__School__YrTerm)
							)
					GROUP BY
						School,
						YrTerm
				) m
				inner join
				dbo.UnivTerm t
					with(index(pk_UnivTerm))
					on m.YrTerm = t.TermCode
				inner join
				dbo.Organization o
					with(index(ix_Organization__OrganizationCode))
					on o.OrganizationCode = m.School
			GROUP BY
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				t.AcademicYear
		) source
	ON
		target.OrganizationId = source.OrganizationId
		and target.AcademicYear = source.AcademicYear
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				OrganizationId,
				OrganizationTypeId,
				OrganizationName,
				AcademicYear,
				CountyId,
				StudentCount
			)
		VALUES
			(
				source.OrganizationId,
				source.OrganizationTypeId,
				source.OrganizationName,
				source.AcademicYear,
				source.CountyId,
				source.StudentCount
			)
	WHEN MATCHED THEN
		UPDATE SET
			target.StudentCount = source.StudentCount;

	-- Univ :: Award
	MERGE
		MemberList with (tablockx) target
	USING
		(
			SELECT
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				t.AcademicYear,
				AwardCount = sum(RecordCount)
			FROM
				(
					SELECT
						School,
						YrTerm,
						RecordCount = count(*)
					FROM
						dbo.UnivAwardProd
							with(
								tablockx,
								index(ix_UnivAwardProd__School__YrTerm)
							)
					GROUP BY
						School,
						YrTerm
				) m
				inner join
				dbo.UnivTerm t
					with(index(pk_UnivTerm))
					on m.YrTerm = t.TermCode
				inner join
				dbo.Organization o
					with(index(ix_Organization__OrganizationCode))
					on o.OrganizationCode = m.School
			GROUP BY
				o.OrganizationId,
				o.OrganizationTypeId,
				o.OrganizationName,
				o.CountyId,
				t.AcademicYear
		) source
	ON
		target.OrganizationId = source.OrganizationId
		and target.AcademicYear = source.AcademicYear
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				OrganizationId,
				OrganizationTypeId,
				OrganizationName,
				AcademicYear,
				CountyId,
				AwardCount
			)
		VALUES
			(
				source.OrganizationId,
				source.OrganizationTypeId,
				source.OrganizationName,
				source.AcademicYear,
				source.CountyId,
				source.AwardCount
			)
	WHEN MATCHED THEN
		UPDATE SET
			target.AwardCount = source.AwardCount;

	UPDATE
		l
	SET
		l.MouStatus = 
			case
				when m1.OrganizationId is not null then 'Yes'
				when m.OrganizationId is not null then 'Yes'
				else 'No'
			end
	FROM
		dbo.MemberList l
			with(index(pk_MemberList__OrganizationId__AcademicYear))
		left outer join
		dbo.CPP_Mou m
			with(index(ix_CPP_Mou__organizationId))
			on l.OrganizationId = m.OrganizationId
			and convert(date, m.expirationDate) >= convert(date, getdate())
		left outer join
		dbo.Organization o
			with(index(pk_Organization__OrganizationId))
			on o.OrganizationId = l.OrganizationId
		left outer join
		dbo.Organization o1
			with(index(pk_Organization__OrganizationId))
			on o.ParentId = o1.OrganizationId
		left outer join
		dbo.CPP_Mou m1
			with(index(ix_CPP_Mou__organizationId))
			on o1.OrganizationId = m1.OrganizationId
			and convert(date, m1.expirationDate) >= convert(date, getdate());
		
END;