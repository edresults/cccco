CREATE PROCEDURE
    dbo.ad_GetK12MOUListDistrictOnly
    (
        @District int
    )
AS

    /*
        author: adalton, dlamoree
        desc: Generate counts for Cal-PASS Plus Member List
        notes: 
        tags: Cal-PASS PLUS, MemberList
    changelog: 
                1.1 (2018-07-19): dlamoree
                Modification to accept @District as ParentId for ChildId Organizations
                1.0 (2015-04-16): adalton
                Implemented
    */

BEGIN
    SELECT
        District               = left(d.OrganizationCode, 7),
        districtorganizationid = d.OrganizationID,
        districtname           = d.OrganizationName,
        MOUExpire              = m.ExpirationDate,
        ACYear                 = max(s.AcYear),
        MostRecent             = max(t.DisplayYear),
        opensharing            = case when m.restricted = 0 then 1 else 0 end
    FROM
        Organization d
        left outer join
        calpass.dbo.CPP_Mou m
            on  m.OrganizationId = d.OrganizationId
            and m.deleted is null
        left outer join
        [calpass].[dbo].[Lookup_External_EconomicRegion2CountyMap] r
            on r.county_id = d.countyid
        left outer join
        Organization o
            on o.ParentId = d.OrganizationId
        left outer join
        ad_StudentCount s
            on s.School = o.OrganizationCode
            and s.Students > 10
        left outer join
        ad_CourseCount c
            on c.School = s.School
            and c.AcYear = s.AcYear
            and c.Courses > 10
        left outer join
        ad_AwardCount a
            on a.School = s.School
            and a.AcYear = s.AcYear
            and a.Awards > 10
        left outer join
        ad_StarCount st
            on st.School = s.School
            and st.AcYear = s.AcYear
        left outer join
        calpass.dbo.ad_term as t
            on t.Term = s.acyear
    WHERE
        d.OrganizationID = @District
        and d.ParentId is null
        and d.OrganizationTypeId = 1
    GROUP BY
        left(d.OrganizationCode, 7),
        d.OrganizationId,
        d.OrganizationName,
        m.ExpirationDate,
        case when m.restricted = 0 then 1 else 0 end
    ORDER BY
        d.OrganizationName;
END;

/*
select   
left(organizationcode,7) as District, org.OrganizationID as districtorganizationid, OrganizationName as districtname, MOU.expirationDate as MOUExpire, max(STD.AcYear) as ACYear, Max(TERM.DisplayYear) as MostRecent,   
case when restricted = 0 then 1 else 0 end as opensharing  
from calpass.dbo.Organization as ORG  
left join ad_StudentCount as STD   
on left(org.organizationcode,7) = left(STD.School,7)  
left join ad_CourseCount as COURSE  
on std.school = course.school  
and STD.AcYear = COURSE.AcYear  
left join ad_AwardCount as AWARD  
on std.school = AWARD.school  
and STD.AcYear = AWARD.AcYear  
left join ad_StarCount as STAR  
on std.school = STAR.school  
and STD.AcYear = STAR.AcYear  
left join calpass.dbo.CPP_Mou as MOU  
on ORG.OrganizationId = MOU.organizationId  
left outer join [calpass].[dbo].[Lookup_External_EconomicRegion2CountyMap] REGION   
on ORG.countyid = REGION.county_id  
left outer join calpass.dbo.ad_term as TERM  
on STD.acyear = TERM.Term   
where parentid is null  and organizationtypeid = 1 and (std.acyear is null or std.acyear between '0405' and '8888')  
and (students is not null or courses is not null or awards is not null or star is not null)  
and org.OrganizationID = @District  
and (isnull(students,0) + isnull(courses,0) + isnull(awards,0)) > 10  

group by left(organizationcode,7), org.OrganizationId, OrganizationName,MOU.expirationDate, restricted  
order by OrganizationName  
*/

/*
select  m.districtname, m.districtorganizationid, max(case when students is not null or courses is not null or awards is not null or star is not null or cahsee is not null then so.academicyear else null end) as MostRecent, 
max(mouexpireddate) as MOUExpire, opensharing
from ad_memberlist as m
inner join calpass.dbo.organization b on m.districtorganizationid = b.parentid
left outer join [calpass].[dbo].[ad_DistrictMOU] as so on m.districtorganizationid = so.districtorganizationid 
left outer join calpass.dbo.year as YR on so.AcademicYear = yr.FullYearWithDash
left outer join ad_StudentCount as STD on b.organizationcode = std.school and yr.shortyear = std.acyear
left outer join ad_CourseCount as COU on b.organizationcode = cou.school and yr.ShortYear=cou.acyear
left outer join ad_AwardCount as AWA on b.organizationcode = awa.school and yr.ShortYear=awa.acyear
left outer join ad_STARCount as STA on b.organizationcode = sta.school and yr.ShortYear=sta.acyear
left outer join ad_CAHSEECount as CAH on b.organizationcode = cah.school and yr.ShortYear=cah.acyear
left outer join ad_SharingSummary as SS on m.districtorganizationid = ss.userorganizationid
where m.districtorganizationid = @District and (students + courses + awards) > 10
group by m.districtname, m.districtorganizationid, opensharing
order by districtname
*/