USE calpass;

GO

IF (object_id('dbo.HSTranscriptSummaryHashSelect') is not null)
	BEGIN
		DROP PROCEDURE dbo.HSTranscriptSummaryHashSelect;
	END;

GO

CREATE PROCEDURE
	dbo.HSTranscriptSummaryHashSelect
	(
		@Student dbo.Student READONLY
	)
AS

	SET NOCOUNT ON;

BEGIN
	SELECT
		h.Entity,
		h.Attribute,
		h.Value,
		h.InterSegmentKey
	FROM
		dbo.HSTranscriptSummaryHash h
	WHERE
		exists (
			SELECT
				1
			FROM
				@Student s
			WHERE
				s.InterSegmentKey = h.InterSegmentKey
		);
END;