USE calpass;

GO

IF (object_id('calpass.dbo.p_bsteele_cohort_cc_insert', 'P') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_bsteele_cohort_cc_insert;
	END;

GO

CREATE PROCEDURE
	dbo.p_bsteele_cohort_cc_insert
AS

	SET NOCOUNT ON;

BEGIN

	TRUNCATE TABLE calpass.dbo.bsteele_cohort_cc;

	INSERT INTO
		calpass.dbo.bsteele_cohort_cc
		(
			derkey1,
			/* organization */
			organization_comis_code,
			organization_ipeds_code,
			organization_term_code,
			/* course */
			course_id,
			course_title,
			course_credit_status,
			course_top_code,
			course_transfer_status,
			course_units_max,
			course_bs_status,
			course_sam_code,
			course_classification_code,
			course_college_level,
			/* section */
			section_id,
			section_units_attempted,
			section_units_achieved,
			section_grade,
			section_grade_points
		)
	SELECT DISTINCT
		c.derkey1,
		/* organization */
		t.organization_comis_code,
		t.organization_ipeds_code,
		t.organization_term_code,
		/* course */
		t.course_id,
		t.course_title,
		t.course_credit_status,
		t.course_top_code,
		t.course_transfer_status,
		t.course_units_max,
		t.course_bs_status,
		t.course_sam_code,
		t.course_classification_code,
		t.course_college_level,
		/* section */
		t.section_id,
		t.section_units_attempted,
		t.section_units_achieved,
		t.section_grade,
		t.section_grade_points
	FROM
		calpass.dbo.bsteele_cohort c
		cross apply
		calpass.dbo.f_cc_student_transcript(
			c.derkey1,
			default
		) t;

	/* student */
	UPDATE
		t
	SET
		t.student_gender = a.student_gender,
		t.student_ethnicity = a.student_ethnicity,
		t.student_citizenship = a.student_citizenship,
		t.student_zip_code = a.student_zip_code,
		t.student_education_status = a.student_education_status,
		t.student_high_school = a.student_high_school,
		t.student_goal = a.student_goal,
		t.student_enroll_status = a.student_enroll_status,
		t.student_academic_standing = a.student_academic_standing,
		t.student_academic_level = a.student_academic_level,
		t.student_dsps_code = a.student_dsps_code,
		t.student_eops_code = a.student_eops_code,
		t.student_bogg_code = a.student_bogg_code,
		t.student_pell_code = a.student_pell_code,
		t.student_financial_aid_code = a.student_financial_aid_code,
		t.student_major = a.student_major
	FROM
		calpass.dbo.bsteele_cohort_cc t
		inner join
		calpass.dbo.bsteele_cohort c
			on c.derkey1 = t.derkey1
		cross apply
		calpass.dbo.f_cc_student_attributes(
			c.derkey1,
			default
		) a;

	/* summary: mathematicss */
	UPDATE
		t
	SET
		t.student_first_math_title = m.course_title,
		t.student_first_math_grade = m.section_grade
	FROM
		calpass.dbo.bsteele_cohort_cc t
		inner join
		calpass.dbo.bsteele_cohort c
			on c.derkey1 = t.derkey1
		cross apply
		calpass.dbo.f_cc_student_subject_first(
			c.derkey1,
			'170100'
		) m;

	/* summary: english */
	UPDATE
		t
	SET
		t.student_first_engl_title = e.course_title,
		t.student_first_engl_grade = e.section_grade
	FROM
		calpass.dbo.bsteele_cohort_cc t
		inner join
		calpass.dbo.bsteele_cohort c
			on c.derkey1 = t.derkey1
		cross apply
		calpass.dbo.f_cc_student_subject_first(
			c.derkey1,
			'150100'
		) e;

	/* milestones */
	UPDATE
		t
	SET
		t.student_first_year_units_attempted = m.student_first_year_units_attempted,
		t.student_first_year_units_achieved = m.student_first_year_units_achieved,
		t.student_first_year_grade_points = m.student_first_year_grade_points,
		t.student_first_year_cgpa = m.student_first_year_cgpa
	FROM
		calpass.dbo.bsteele_cohort_cc t
		inner join
		calpass.dbo.bsteele_cohort c
			on c.derkey1 = t.derkey1
		cross apply
		calpass.dbo.f_cc_student_milestones(
			c.derkey1
		) m;

	/* msjc de ind */
	UPDATE
		t
	SET
		t.student_msjc_de_ind = case
			when c.derkey1 is not null then 1
			else 0
		end
	FROM
		calpass.dbo.bsteele_cohort_cc t
		inner join
		calpass.dbo.ccstudentprod_termId z
			on t.organization_term_code = z.code
		left outer join
		calpass.dbo.bsteele_cohort_msj c
			on t.derkey1 = c.derkey1
			and z.year_code = c.year_code;

	/* student award */
	UPDATE
		t
	SET
		t.student_award_code = a.Award
	FROM
		calpass.dbo.bsteele_cohort_cc t
		inner join
		(
			SELECT
				bc.derkey1,
				award = rtrim(cca.Award),
				row_selector = row_number() over(
					partition by
						bc.derkey1
					order by
						cca.DateOfAward asc,
						case
							when rtrim(cca.Award) in ('A', 'S') then 1
							when rtrim(cca.Award) = 'F' then 2
							when rtrim(cca.Award) = 'T' then 3
							when rtrim(cca.Award) = 'L' then 5
							when rtrim(cca.Award) = 'B' then 6
							when rtrim(cca.Award) = 'E' then 7
							when rtrim(cca.Award) = 'R' then 8
							when rtrim(cca.Award) = 'Q' then 9
							when rtrim(cca.Award) = 'P' then 10
							when rtrim(cca.Award) = 'K' then 11
							when rtrim(cca.Award) = 'J' then 12
							when rtrim(cca.Award) = 'I' then 13
							when rtrim(cca.Award) = 'H' then 14
							when rtrim(cca.Award) = 'G' then 15
							else 99
						end asc,
						cca.RecordId asc
				)
			FROM
				calpass.dbo.bsteele_cohort bc
				inner join
				calpass.dbo.CCStudentProd ccs
					on ccs.derkey1 = bc.derkey1
				inner join
				calpass.dbo.CCStudentProd_TermId t1
					on t1.code = ccs.termId
				inner join
				calpass.dbo.CCAwardProd cca
					on ccs.CollegeId = cca.CollegeId
					and ccs.StudentId = cca.StudentId
				inner join
				calpass.dbo.CCStudentProd_TermId t2
					on t2.code = cca.termId
					and t1.year_code = t2.year_code
		) a
			on t.derkey1 = a.derkey1
	WHERE
		a.row_selector = 1;

END;