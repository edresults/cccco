USE calpass;

GO

IF (object_id('dbo.K12CteProdMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.K12CteProdMerge;
	END;

GO

CREATE PROCEDURE
	dbo.K12CteProdMerge
	(
		@Escrow calpads.Escrow READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Message nvarchar(2048),
	@ErrorCode int = 70099,
	@ErrorSeverity int = 1,
	@EscrowName sysname = 'K12CteProd';

BEGIN
	BEGIN TRY
		MERGE
			dbo.K12CteProd t
		USING 
			(
				SELECT
					Derkey1,
					School,
					AcYear,
					LocStudentId,
					CtePathwayCode,
					CtePathwayCompAcYear,
					DateAdded
				FROM
					(
						SELECT
							Derkey1 = e.InterSegmentKey,
							School = s.DistrictCode + s.SchoolCode,
							AcYear = y.YearCodeAbbr,
							LocStudentId = calpass.dbo.get1289Encryption(f.StudentLocalId, 'X'),
							CtePathwayCode = s.CtePathwayCode,
							CtePathwayCompAcYear = s.CtePathwayCompletionYearCode,
							DateAdded = getdate(),
							RecordCount = row_number() over(
									partition by
										s.DistrictCode,
										s.SchoolCode,
										calpass.dbo.Get1289Encryption(f.StudentLocalId, 'X'),
										s.YearCode,
										s.CtePathwayCode
									order by
										(select 1) desc
								)
						FROM
							@Escrow e
							inner join
							calpads.Scte s with(index(pk_Scte))
								on s.StudentStateId = e.StudentStateId
								and s.SchoolCode = e.SchoolCode
								and s.YearCode = e.YearCode
							inner join
							calpads.Year y with(index(PK_Year))
								on y.YearCode = s.YearCode
							inner join
							calpads.Sinf f with(index(pk_Sinf))
								on e.StudentStateId = f.StudentStateId
								and e.SchoolCode = f.SchoolCode
								and e.SinfYearCode = f.YearCode
								and e.SinfEffectiveStartDate = f.EffectiveStartDate
						WHERE
							e.EscrowName = @EscrowName
							and f.StudentLocalId is not null
					) a
				WHERE
					RecordCount = 1
			) s
		ON
			t.School = s.School
			and t.LocStudentId = s.LocStudentId
			and t.AcYear = s.AcYear
			and t.CtePathwayCode = s.CtePathwayCode
		WHEN MATCHED THEN
			UPDATE SET
				t.Derkey1 = s.Derkey1,
				t.CtePathwayCompAcYear = s.CtePathwayCompAcYear,
				t.DateAdded =  s.DateAdded
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					Derkey1,
					School,
					AcYear,
					LocStudentId,
					CtePathwayCode,
					CtePathwayCompAcYear,
					DateAdded
				)
			VALUES
				(
					s.Derkey1,
					s.School,
					s.AcYear,
					s.LocStudentId,
					s.CtePathwayCode,
					s.CtePathwayCompAcYear,
					s.DateAdded
				);
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message =
			nchar(9) + N'Error Procedure: ' + upper(object_name(@@PROCID)) + char(13) + char(10) +
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		-- put error message
		THROW @ErrorCode, @Message, @ErrorSeverity;
	END CATCH;
END;