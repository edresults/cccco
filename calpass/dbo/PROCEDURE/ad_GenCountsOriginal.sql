alter Procedure [dbo].[ad_GenCounts] as  
/*  
exec ad_GenCounts  
*/  
  
--Student Counts  
drop table ad_StudentCount  
SELECT school, acyear, count(locstudentid) as Students  
into ad_StudentCount  
FROM [calpass].[dbo].[K12StudentProd]  
Group by school, acyear  
  
--Course Count  
drop table ad_CourseCount  
SELECT school, acyear, count(locstudentid) as Courses  
into ad_CourseCount  
FROM [calpass].[dbo].[K12CourseProd]  
Group by school, acyear  
  
--Award Count  
drop table ad_AwardCount  
SELECT school, acyear, count(locstudentid) as Awards  
into ad_AwardCount  
FROM [calpass].[dbo].[K12AwardProd]  
Group by school, acyear  
  
--Star Count  
--drop table ad_StarCount  
SELECT school, acyear, count(locstudentid) as Star  
into #ad_StarCount  
FROM [calpass].[dbo].[K12StarDemProd]  
Group by school, acyear  
  
--CAASPP Count  
SELECT school, acyear, count(locstudentid) as CAASPP  
into #ad_CAASPPCount  
FROM [calpass].[dbo].[K12CAASPPTestProd]  
Group by school, acyear  
  
--Combined STAR/CAASPP  
drop table ad_StarCount  
select *   
into ad_StarCount  
from (  
select * from #ad_starcount  
union all  
select * from #ad_CAASPPCount) f  
order by school, acyear  
  
--CAHSEE Count  
drop table ad_CAHSEECount  
SELECT schoolidentifier as school, schoolyear as acyear, count(localstudentid) as CAHSEE  
into ad_CAHSEECount  
FROM [calpass].[dbo].[CAHSEEDemographics]  
Group by schoolidentifier, schoolyear  
  
  
  
--CC Student Count  
drop table ad_CCStudentCount  
SELECT collegeid, academicyear, count(studentid) as Students  
into ad_CCStudentCount  
FROM [calpass].[dbo].[CCStudentProd] C  
inner join calpass.dbo.term T on c.termid = t.termcode  
Group by collegeid, academicyear  
  
--CC Course Count  
drop table ad_CCCourseCount  
SELECT collegeid, academicyear, count(studentid) as Courses  
into ad_CCCourseCount  
FROM [calpass].[dbo].[CCCourseProd] C  
inner join calpass.dbo.term T on c.termid = t.termcode  
Group by collegeid, academicyear  
  
--CC Award Count  
drop table ad_CCAwardCount  
SELECT collegeid, academicyear, count(studentid) as Awards  
into ad_CCAwardCount  
FROM [calpass].[dbo].[CCAwardProd] C  
inner join calpass.dbo.term T on c.termid = t.termcode  
Group by collegeid, academicyear  
  
--UNIV Student Count  
drop table ad_UNIVStudentCount  
SELECT school as collegeid, academicyear, count(studentid) as Students  
into ad_UNIVStudentCount  
FROM [calpass].[dbo].[UNIVStudentProd] C  
inner join calpass.dbo.term T on c.yrterm = t.termcode  
Group by school, academicyear  
  
--UNIV Course Count  
drop table ad_UNIVCourseCount  
SELECT school as collegeid, academicyear, count(studentid) as Courses  
into ad_UNIVCourseCount  
FROM [calpass].[dbo].[UNIVCourseProd] C  
inner join calpass.dbo.term T on c.yrterm = t.termcode  
Group by school, academicyear  
  
--UNIV Award Count  
drop table ad_UNIVAwardCount  
SELECT school as collegeid, academicyear, count(studentid) as Awards  
into ad_UNIVAwardCount  
FROM [calpass].[dbo].[UNIVAwardProd] C  
inner join calpass.dbo.term T on c.yrterm = t.termcode  
Group by school, academicyear  
  
--Create Higher Ed List Parent vs District  
drop table org  
select *   
into org  
from  
(select  organizationid, organizationtypeid, organizationname, organizationcode, organizationcodetypeid from calpass.dbo.organization  
where organizationcodetypeid > 1 and parentid is null  
union all  
select  parentid as organizationid, organizationtypeid, organizationname, organizationcode, organizationcodetypeid from calpass.dbo.organization  
where organizationcodetypeid > 1 and parentid is not null  
) as HigherEdList  
order by OrganizationName  
  
  
--Create MOU Master  
drop table ad_MOUMaster  
select organizationid, max(datemouexpires) as dtMOUExpires   
Into ad_MOUMaster  
from   
(select organizationid, case when organizationid in ('171235','166702','171235') then '2015-06-30' else DateMouExpires end as DateMouExpires  
from oms_mou  
union all  
select OrganizationId,case when organizationid in ('171235','166702','171235') then '2015-06-30' else dtMouExpires end as dtMouExpires   
from organizationmou where moustatus = 5) as AllMOU  
group by OrganizationId  
order by OrganizationId  
  
  
--MOU Count  
drop table ad_DistrictMOU  
select distinct districtorganizationid, max(case when dtmouexpires is null then mouexpireddate else dtmouexpires end) as MOUExpiredDate, Academicyear   
into ad_DistrictMOU  
from [calpass].[dbo].[SumOrganization] as so  
left outer join [calpass].[dbo].[ad_MOUMaster] as O on so.DistrictOrganizationId=o.OrganizationId  
group by districtorganizationid,Academicyear  
order by DistrictOrganizationId, AcademicYear  
  
--Get Sharing Counts  
drop table ad_SharingSummary  
SELECT userorganizationid, case when count(organizationid) > 370 then 1 else 0 end as OpenSharing  
into ad_SharingSummary  
FROM [calpass].[dbo].[Reporting_ShareListSummary]  
where academicyear = 2015  
group by userorganizationid  
order by userorganizationid  
  
--build AD_memberlist  
drop table ad_memberlist  
select region_id, countyid, organizationname as districtname, organizationid as districtorganizationid, organizationcodetypeid, organizationtypeid   
into ad_memberlist  
from calpass.dbo.organization a  
left outer join [calpass].[dbo].[Lookup_External_EconomicRegion2CountyMap] c on a.countyid = c.county_id  
where a.OrganizationTypeId in (1,2,3) and ParentId is null and substring(organizationname,1,1) <> '"'  
order by a.OrganizationName  
  
--Distinct School List  
Drop Table ad_OrgList  
select *   
into ad_orglist  
from  
(select distinct m.districtname, m.districtorganizationid  
from ad_memberlist as m  
left outer join [calpass].[dbo].[ad_DistrictMOU] as so on m.districtorganizationid = so.districtorganizationid   
left outer join calpass.dbo.org on org.organizationid = m.districtorganizationid  
where m.organizationtypeid=2  
union all  
select distinct m.districtname, m.districtorganizationid  
from ad_memberlist as m  
left outer join [calpass].[dbo].[ad_DistrictMOU] as so on m.districtorganizationid = so.districtorganizationid   
left outer join calpass.dbo.org on org.organizationid = m.districtorganizationid  
where m.organizationtypeid=3 and m.organizationcodetypeid = 2  
union all  
select distinct m.districtname, m.districtorganizationid  
from ad_memberlist as m  
inner join calpass.dbo.organization b on m.districtorganizationid = b.parentid  
left outer join [calpass].[dbo].[ad_DistrictMOU] as so on m.districtorganizationid = so.districtorganizationid   
left outer join calpass.dbo.year as YR on so.AcademicYear = yr.FullYearWithDash  
) as Orgs  
order by districtname  
  
  
insert into ad_orglist   
values('Orcutt Academy Charter','173029')  
  
--Al's Stuff  
-- if object_id('TransitionReporting.dbo.ValidHSDistricts') is not null drop table TransitionReporting.dbo.ValidHSDistricts;  
  
-- select *   
-- into TransitionReporting.dbo.ValidHSDistricts  
-- from [PRO-DAT-SQL-03].TransitionReporting.dbo.ValidHSDistricts  
-- ;  
  
-- create nonclustered index ValidHSDistrictsIndex on  
 -- TransitionReporting.dbo.ValidHSDistricts (DistrictCode, AcYear)  
-- ;  
  
-- if object_id('TransitionReporting.dbo.ValidHSCampus') is not null drop table TransitionReporting.dbo.ValidHSCampus;  
  
-- select *   
-- into TransitionReporting.dbo.ValidHSCampus  
-- from [PRO-DAT-SQL-03].TransitionReporting.dbo.ValidHSCampus  
-- ;  
  
-- create nonclustered index ValidHSCampus on  
 -- TransitionReporting.dbo.ValidHSCampus (hscode, academicyear)  
-- ;  
  
-- if object_id('TransitionReporting.dbo.validunivs') is not null drop table TransitionReporting.dbo.validunivs;  
  
-- select *   
-- into TransitionReporting.dbo.validunivs  
-- from [PRO-DAT-SQL-03].TransitionReporting.dbo.validunivs  
-- ;  
  
-- create nonclustered index validunivs on  
 -- TransitionReporting.dbo.validunivs (collegeid, academicyear)  
-- ;  