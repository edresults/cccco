DECLARE @Error nvarchar(2048);

IF (@@ServerName != 'DEV-DAT-SQL-04')
	BEGIN
		SET @Error = N'Wrong Server';
		THROW 70099, @Error, 1; 
	END;


IF (object_id('calpass.dbo.FosterYouthStudent') is not null)
	BEGIN
		DROP TABLE calpass.dbo.FosterYouthStudent;
	END;

SELECT * INTO calpass.dbo.FosterYouthStudent FROM [pro-dat-sql-03].calpass.dbo.FosterYouthStudent;

CREATE CLUSTERED INDEX IC_FosterYouthStudent ON calpass.dbo.FosterYouthStudent (SSID);


IF (object_id('calpass.dbo.FosterYouthFormer') is not null)
	BEGIN
		DROP TABLE calpass.dbo.FosterYouthFormer;
	END;

SELECT * INTO calpass.dbo.FosterYouthFormer FROM [pro-dat-sql-03].calpass.dbo.FosterYouthFormer;

CREATE CLUSTERED INDEX IC_FosterYouthFormer ON calpass.dbo.FosterYouthFormer (SSID, GrdLvlCode, ClientID);


IF (object_id('calpass.dbo.FosterYouthCurrent') is not null)
	BEGIN
		DROP TABLE calpass.dbo.FosterYouthCurrent;
	END;

SELECT * INTO calpass.dbo.FosterYouthCurrent FROM [pro-dat-sql-03].calpass.dbo.FosterYouthCurrent;

CREATE CLUSTERED INDEX IC_FosterYouthCurrent ON calpass.dbo.FosterYouthCurrent (SSID, GrdLvlCode, ClientID);