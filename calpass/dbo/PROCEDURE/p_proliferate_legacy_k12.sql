USE calpass;

GO

IF (object_id('dbo.p_proliferate_legacy_k12') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_proliferate_legacy_k12;
	END;

GO

CREATE PROCEDURE
	dbo.p_proliferate_legacy_k12
AS

DECLARE
	@i int = 0,
	@u int,
	@OrganizationId int,
	@DistrictCode char(7),
	@FileType char(4);

	DECLARE
		@tab
	AS TABLE
		(
			Iterator int identity(0, 1),
			OrganizationId int,
			DistrictCode char(7),
			FileType char(4)
		);

BEGIN
	-- populate table
	INSERT INTO
		@tab
		(
			OrganizationId,
			DistrictCode,
			FileType
		)
	SELECT DISTINCT
		o.OrganizationId,
		DistrictCode = left(o.OrganizationCode, 7),
		FileType = 
			case
				when t.Value in ('SENR', 'SENR-R') then 'SENR'
				when t.Value in ('SCSC', 'SCSC-R') then 'SCSC'
			end
	FROM
		[PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedrop f
		inner join
		[PRO-DAT-SQL-01].calpass.dbo.organization o
			on o.organizationId = f.organizationId
		inner join
		[PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedroptag t
			on t.SubmissionFileId = f.SubmissionFileId
	WHERE
		f.Status = 'Loaded'
		and t.Attribute = 'FileType'
		and t.Value in ('SENR','SENR-R','SCSC', 'SCSC-R');

	SELECT
		@u = count(*)
	FROM
		@tab;

	WHILE (@i < @u)
	BEGIN
		-- get
		SELECT
			@OrganizationId = OrganizationId,
			@DistrictCode = DistrictCode,
			@FileType = FileType
		FROM
			@tab
		WHERE
			iterator = @i;
		
		IF (@FileType = 'SENR')
			BEGIN
				EXECUTE calpass.dbo.p_K12StudentProd_merge
					@district_code = @DistrictCode;
			END;
		ELSE IF (@FileType = 'SCSC')
			BEGIN
				EXECUTE calpass.dbo.p_K12CourseProd_merge
					@district_code = @DistrictCode;
			END;

		-- update records
		UPDATE
			f
		SET
			Status = 'Proliferated'
		FROM
			[PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedrop f
			inner join
			[PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedroptag t
				on f.SubmissionFileId = t.SubmissionFileId
		WHERE
			f.OrganizationId = @OrganizationId
			and f.Status = 'Loaded'
			and t.Attribute = 'FileType'
			and convert(char(4), t.Value) = @FileType;

		-- iterate
		SET @i += 1;
	END;
END;