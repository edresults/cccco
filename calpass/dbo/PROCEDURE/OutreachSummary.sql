USE calpass;

GO

IF (object_id('dbo.OutreachSummary') is not null)
	BEGIN
		DROP PROCEDURE dbo.OutreachSummary;
	END;

GO

CREATE PROCEDURE
	dbo.OutreachSummary
	(
		@AcademicYear char(9) = null,
		@OnlyLastWeek bit     = 1
	)
AS

-- query variables
DECLARE
	@YearCodeAbbr           char(4),
	@OrganizationCodeTypeId integer      = 1,
	@ColumnSpacer           char(4)      = '   ',
	@DeadlineDate           date         = convert(date, convert(char(4), datepart(year, CURRENT_TIMESTAMP)) + '0801'),
	@Weekday                tinyint      = 5,
	@Nth                    tinyint      = 3;

-- email variables
DECLARE
	@ProfileName            sysname      = 'Cal-PASS SQL Support Profile',
	@BlindCopyRecipients    varchar(max) = 'dlamoree@edresults.org;msamra@edresults.org;agreen@edresults.org;vmarrero@edresults.org',
	@Subject                varchar(255) = 'OUTREACH :: FILE UPLOAD :: K12 :: SUMMARY :: ',
	@Body                   varchar(max) = '',
	@BodyFormat             varchar(20)  = 'HTML',
	@Space                  char(5)      = '&nbsp',
	@CarriageReturnLineFeed char(4)      = '<br>';

BEGIN

	IF (@AcademicYear is null)
		BEGIN
			SELECT
				@AcademicYear = 
					case
						when 
							CURRENT_TIMESTAMP > dateadd(
									day,
									7 * @Nth - 7 * sign(sign(@Nth) + 1) + 
									(@Weekday + 6 - datediff(day, '17530101', dateadd(month, datediff(month, @Nth, @DeadlineDate), '19000101')) % 7) % 7,
									dateadd(month, datediff(month, @Nth, @DeadlineDate), '19000101')
							) then convert(char(4), datepart(year, @DeadlineDate) - 1) + '-' + convert(char(4), datepart(year, @DeadlineDate))
						else
							convert(char(4), datepart(year, @DeadlineDate) - 2) + '-' + convert(char(4), datepart(year, @DeadlineDate) - 1)
					end
			WHERE
				@Weekday >= 1
				and @Weekday <= 7
				and @Nth >= 1
				and @Nth <= 5
		END;

	SELECT
		@YearCodeAbbr = ShortYear
	FROM
		[PRO-DAT-SQL-01].calpass.dbo.Year
	WHERE
		FullYearWithDash = @AcademicYear;

 	IF (@AcademicYear is null)
	 	BEGIN
		 	RETURN;
		END;
 	ELSE IF (@YearCodeAbbr is null)
	 	BEGIN
		 	RETURN;
		END;
	ELSE
		BEGIN
			SET @Subject = @Subject + @AcademicYear;
		end;

	SELECT
		@Body += 
			case
				when New = 1 then District + replicate(' ', max(len(District)) over() - len(District))
				when New = 0 then replicate(' ', max(len(District)) over())
			end + @ColumnSpacer +
			case
				when New = 1 then Students + replicate(' ', max(len(Students)) over() - len(Students))
				when New = 0 then replicate(' ', max(len(Students)) over())
			end + @ColumnSpacer + 
			case
				when New = 1 then Courses + replicate(' ', max(len(Courses)) over() - len(Courses))
				when New = 0 then replicate(' ', max(len(Courses)) over())
			end + @ColumnSpacer + 
			case
				when New = 1 then Awards + replicate(' ', max(len(Awards)) over() - len(Awards))
				when New = 0 then replicate(' ', max(len(Awards)) over())
			end + @ColumnSpacer + 
			Files +
			case
				when count(*) over() = row_number() over(order by (select 1)) then ''
				else char(13) + char(10)
			end
	FROM
		(
			SELECT
				District = convert(varchar(255), 'District'),
				Students = convert(varchar(255), 'Students'),
				Courses  = convert(varchar(255), 'Courses'),
				Awards   = convert(varchar(255), 'Awards'),
				Files    = convert(varchar(255), 'Files'),
				Ordinal  = 0,
				New      = 1
			UNION ALL
			SELECT
				District,
				Students,
				Courses,
				Awards,
				Files = substring(f.OriginalFileName, 38, len(f.OriginalFileName)) + ' (' + convert(char(8), f.SubmissionDateTime, 112) + ')',
				Ordinal,
				New = 
					case
						when lag(District) over(order by District) = District then 0
						else 1
					end
			FROM
				(
					SELECT
						d.OrganizationId,
						District = convert(varchar(255), d.OrganizationName),
						Students = isnull(convert(varchar(255), sum(s.Students)), ''),
						Courses  = isnull(convert(varchar(255), sum(c.Courses)), ''),
						Awards   = isnull(convert(varchar(255), sum(a.Awards)), ''),
						Ordinal  = 1
					FROM
						[PRO-DAT-SQL-01].calpass.dbo.Organization d
						inner join
						[PRO-DAT-SQL-01].calpass.dbo.Organization o
							on o.ParentId = d.OrganizationId
						left outer join
						[PRO-DAT-SQL-01].calpass.dbo.ad_StudentCount s
							on s.School = o.OrganizationCode
							and s.AcYear = @YearCodeAbbr
						left outer join
						[PRO-DAT-SQL-01].calpass.dbo.ad_CourseCount c
							on c.School = o.OrganizationCode
							and c.AcYear = @YearCodeAbbr
						left outer join
						[PRO-DAT-SQL-01].calpass.dbo.ad_AwardCount a
							on a.School = o.OrganizationCode
							and a.AcYear = @YearCodeAbbr
					WHERE
						d.OrganizationCodeTypeId = 1
						and d.OrganizationId in (
							SELECT
								OrganizationId
							FROM
								(
									SELECT
										OrganizationId,
										MouExpireDate = DateMouExpires
									FROM
										[PRO-DAT-SQL-01].calpass.dbo.oms_mou
									UNION ALL
									SELECT
										OrganizationId,
										MouExpireDate = dtMouExpires
									FROM
										[PRO-DAT-SQL-01].calpass.dbo.OrganizationMou
									WHERE
										MouStatus = 5
									UNION ALL
									SELECT
										OrganizationId,
										MouExpireDate = expirationDate
									FROM
										[PRO-DAT-SQL-01].calpass.dbo.cpp_mou
								) AllMou
							GROUP BY
								OrganizationId
							HAVING
								max(MouExpireDate) >= CURRENT_TIMESTAMP
						)
					GROUP BY
						d.OrganizationId,
						d.OrganizationName
				) a
				inner join
				[PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedrop f
					on f.OrganizationId = a.OrganizationId
			WHERE
				(
					@OnlyLastWeek = 0
					or
					(
						@OnlyLastWeek = 1
						and
						f.SubmissionDateTime >= DATEADD(day, -7, CURRENT_TIMESTAMP)
					)
				)
		) a
	ORDER BY
		Ordinal asc,
		District asc;

	-- Formatting
	SET @Body = '<tt>' + @Body + '</tt>';
	SET @Body = replace(@Body, char(13) + char(10), @CarriageReturnLineFeed);
	SET @Body = replace(@Body, '  ', @Space + @Space);

	EXECUTE msdb.dbo.sp_send_dbmail
		@profile_name          = @ProfileName,
		@blind_copy_recipients = @BlindCopyRecipients,
		@subject               = @Subject,
		@body                  = @Body,
		@body_format           = @BodyFormat;
END;