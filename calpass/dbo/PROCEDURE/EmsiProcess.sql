USE calpass;

GO

IF (object_id('dbo.EmsiProcess') is not null)
	BEGIN
		DROP PROCEDURE dbo.EmsiProcess;
	END;

GO

CREATE PROCEDURE
	dbo.EmsiProcess
	(
		@FilePath nvarchar(255)
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@IsRunning bit = 1,
	@JobName sysname = 'EmsiProcess',
	@StepId int = 1,
	@StepName sysname = 'ETL',
	@BinPath nvarchar(4000) = N'"C:\Users\dlamoree\Documents\Visual Studio 2013\Projects\Emsi\Emsi\bin\Release\Emsi.exe"',
	@Command nvarchar(4000) = N'';

BEGIN
	-- set command
	SET @Command = @BinPath + ' "' + @FilePath + '"';

	-- update job step to include file path
	EXECUTE msdb.dbo.sp_update_jobstep
		@job_name = @JobName,
		@step_id = @StepId,
		@command = @Command;

	-- start job
	EXECUTE msdb.dbo.sp_start_job
		@job_name = @JobName,
		@step_name = @StepName;

	WHILE (@IsRunning = 1)
	BEGIN

		-- wait five seconds
		WAITFOR DELAY '00:00:05'

		-- check if running
		SELECT
			@IsRunning = case when count(*) > 0 then 1 else 0 end
		FROM
			msdb.dbo.sysjobs j
			inner join
			msdb.dbo.sysjobactivity a
				on j.job_id = a.job_id
		WHERE
			j.name = @JobName
			and a.start_execution_date is not null
			and a.stop_execution_date is null;
	END;

	-- return to initial state
	EXECUTE msdb.dbo.sp_update_jobstep
		@job_name = @JobName,
		@step_id = @StepId,
		@command = @BinPath;

END;