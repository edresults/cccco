USE calpass;

GO

IF (object_id('dbo.HSTranscriptSummaryHashMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.HSTranscriptSummaryHashMerge;
	END;

GO

CREATE PROCEDURE
	dbo.HSTranscriptSummaryHashMerge
	(
		@InterSegment dbo.InterSegment READONLY
	)
AS

	SET NOCOUNT ON;

BEGIN
	MERGE
		HSTranscriptSummaryHash t
	USING
		(
			SELECT
				InterSegmentKey,
				h.Entity,
				h.Attribute,
				h.Value
			FROM
				@InterSegment
				cross apply
				dbo.HSTranscriptSummaryHashGet(InterSegmentKey) h
		) s
	ON
		s.InterSegmentKey = t.InterSegmentKey
		and s.Entity = t.Entity
		and s.Attribute = t.Attribute
	WHEN MATCHED THEN 
		UPDATE SET
			t.Value = s.Value
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				InterSegmentKey,
				Entity,
				Attribute,
				Value
			)
		VALUES
			(
				s.InterSegmentKey,
				s.Entity,
				s.Attribute,
				s.Value
			);
END;