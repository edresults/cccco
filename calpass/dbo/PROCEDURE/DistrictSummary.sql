USE calpass;

GO

IF (object_id('dbo.DistrictSummary') is not null)
	BEGIN
		DROP PROCEDURE dbo.DistrictSummary;
	END;

GO

CREATE PROCEDURE
	dbo.DistrictSummary
	(
		@AcademicYear char(9) = null,
		@DistrictId   int     = null
	)
AS

-- email variables
DECLARE
	@ProfileName            sysname      = 'Cal-PASS SQL Support Profile',
	@BlindCopyRecipients    varchar(max) = 'dlamoree@edresults.org;dmoynihan@edresults.org;jbarajas@edresults.org',
	@Subject                varchar(255) = 'OUTREACH :: FILE UPLOAD :: K12 :: SUMMARY :: ',
	@Body                   varchar(max) = '',
	@BodyFormat             varchar(20)  = 'HTML',
	@Space                  char(5)      = '&nbsp',
	@CarriageReturnLineFeed char(4)      = '<br>';

BEGIN
	SELECT
		DistrictName = d.OrganizationName,
		AcademicYear = y.AcademicYear,
		Students     = sum(s.students),
		Courses      = sum(c.Courses),
		Awards       = sum(a.Awards),
		Star         = sum(r.Star),
		CAHSEE       = sum(e.CAHSEE)
	FROM
		(
			SELECT
				AcademicYear = FullYearWithDash,
				AcYear       = ShortYear
			FROM
				dbo.Year y1
			WHERE
				(
					FullYearWithDash = @AcademicYear
					or
					(
						@AcademicYear is null
						and
						convert(int, substring(FullYearWithDash, 1, 4)) <= year(CURRENT_TIMESTAMP)
						and
						convert(int, substring(FullYearWithDash, 6, 4)) >= year(dateadd(year, -8, CURRENT_TIMESTAMP))
					)
				)
		) y
		cross join
		(
			SELECT
				OrganizationId
			FROM
				(
					SELECT
						OrganizationId,
						MouExpireDate = DateMouExpires
					FROM
						oms_mou
					UNION ALL
					SELECT
						OrganizationId,
						MouExpireDate = dtMouExpires
					FROM
						OrganizationMou
					WHERE
						MouStatus = 5
					UNION ALL
					SELECT
						OrganizationId,
						MouExpireDate = expirationDate
					FROM
						cpp_mou
				) m
			WHERE
				(
					@DistrictId is null
					or
					m.OrganizationId = @DistrictId
				)
			GROUP BY
				m.OrganizationId
			HAVING
				max(MouExpireDate) > CURRENT_TIMESTAMP
		) m
		inner join
		Organization d
			on d.OrganizationId = m.OrganizationId
		inner join
		Organization o
			on o.ParentId = d.OrganizationId
		left outer join
		ad_StudentCount s
			on s.School = o.OrganizationCode
			and s.AcYear = y.AcYear
		left outer join
		ad_CourseCount c
			on c.School = o.OrganizationCode
			and c.AcYear = y.AcYear
		left outer join
		ad_AwardCount a
			on a.School = o.OrganizationCode
			and a.AcYear = y.AcYear
		left outer join
		ad_StarCount r
			on r.School = o.OrganizationCode
			and r.AcYear = y.AcYear
		left outer join
		ad_CAHSEECount e
			on e.School = o.OrganizationCode
			and e.AcYear = y.AcYear
	GROUP BY
		d.OrganizationName,
		y.AcademicYear
	ORDER BY
		d.OrganizationName,
		y.AcademicYear;
END;