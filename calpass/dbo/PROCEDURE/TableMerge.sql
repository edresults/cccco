CREATE PROCEDURE
    dbo.TableMerge
    (
        @ProductionName sysname,
        @StageName      sysname,
        @Message        nvarchar(2048) = N'' out
    )
AS

    SET NOCOUNT ON;

DECLARE
    @HasEscrow bit = 0,
    @IsEscrow  bit = 1,
    @Join      nvarchar(max) = N'',
    @Update    nvarchar(max) = N'',
    @Insert    nvarchar(max) = N'',
    @Values    nvarchar(max) = N'',
    @RowCount  int,
    @Sql       nvarchar(max);

BEGIN
    -- compare names
    IF (@ProductionName = @StageName)
        BEGIN
            SET @Message = N'Production Table Name and Stage Table Name must not be the same';
            THROW 70099, @Message, 1;
        END;

    -- validate production table
    IF (object_id(@ProductionName) is null)
        BEGIN
            SET @Message = N'Table not found';
            THROW 70099, @Message, 1;
        END;

    -- validate stage table
    IF (object_id(@StageName) is null)
        BEGIN
            SET @Message = N'Table not found';
            THROW 70099, @Message, 1;
        END;

    SELECT
        @HasEscrow = count(*)
    FROM
        sys.columns c
    WHERE
        c.object_id = object_id(@ProductionName) and
        c.name      = 'IsEscrow';

    -- collect merge strings
    SELECT
        @Update +=
            N't.' + tc.Name + ' = s.' + tc.Name + 
            case
                when row_number() over(order by tc.column_id) != count(tc.column_id) over() then N','
                else
                    case
                        when @HasEscrow = 1 then N', t.IsEscrow = @IsEscrow'
                        else N''
                    end
            end
    FROM
        sys.columns tc
        inner join
        sys.columns sc
            on tc.name = sc.name
        left outer join
        sys.indexes i on
            i.object_id      = tc.object_id and
            i.is_primary_key = 1
        left outer join
        sys.index_columns ic on
            ic.object_id = i.object_id and
            ic.index_id  = i.index_id and
            ic.column_id = tc.column_id
    WHERE
        tc.object_id    = object_id(@ProductionName) and
        sc.object_id    = object_id(@StageName) and
        tc.Name        != 'IsEscrow' and
        sc.Name        != 'IsEscrow' and
        tc.is_computed  = 0 and
        sc.is_computed  = 0 and
        ic.column_id   is null
    ORDER BY
        tc.column_id;

    -- collect merge strings
    SELECT
        @Insert +=
            tc.Name + 
            case
                when row_number() over(order by tc.column_id) != count(tc.column_id) over() then N','
                else
                    case
                        when @HasEscrow = 1 then N', IsEscrow'
                        else N''
                    end
            end,
        @Values +=
            's.' + tc.Name + 
            case
                when row_number() over(order by tc.column_id) != count(tc.column_id) over() then N','
                else
                    case
                        when @HasEscrow = 1 then N', @IsEscrow'
                        else N''
                    end
            end
    FROM
        sys.columns tc
        inner join
        sys.columns sc
            on tc.name = sc.name
    WHERE
        tc.object_id = object_id(@ProductionName)
        and sc.object_id = object_id(@StageName)
        and tc.Name != 'IsEscrow'
        and sc.Name != 'IsEscrow'
        and tc.is_computed = 0
        and sc.is_computed = 0
    ORDER BY
        tc.column_id;

    -- collect join string
    SELECT
        @Join +=
            case
                when ic.key_ordinal = min(ic.key_ordinal) over() then N''
                else N' and '
            end
            + N's.' + c.Name + ' = t.' + c.Name
    FROM
        sys.indexes i
        inner join
        sys.index_columns ic
            on i.object_id = ic.object_id
            and i.index_id = ic.index_id
        inner join
        sys.columns c
            on ic.object_id = c.object_id
            and c.column_id = ic.column_id
        inner join
        sys.columns sc
            on sc.name = c.name
    WHERE
        i.object_id = object_id(@ProductionName)
        and i.is_primary_key = 1
        and sc.object_id = object_id(@StageName)
        and c.is_identity = 0
    ORDER BY
        ic.key_ordinal;

    IF (@Join != N'')
        BEGIN
            SET @Join = N'MERGE ' + @ProductionName + N' t USING ' + @StageName + N' s ON ' + @Join;
        END;
        
    IF (@Update != N'')
        BEGIN
            SET @Update = N' WHEN MATCHED THEN UPDATE SET ' + @Update;
        END;
    
    IF (@Insert != N'')
        BEGIN
            SET @Insert = N' WHEN NOT MATCHED BY TARGET THEN INSERT (' + @Insert + N') VALUES (' + @Values + N');';
        END;
        
    SET @Sql = @Join + 
        + @Update + 
        + @Insert + 
        ' SET @RowCount = @@ROWCOUNT;';

    -- merge stage records into production table
    EXECUTE sp_executesql
        @Sql,
        N'@IsEscrow bit, @RowCount int out',
        @IsEscrow = @IsEscrow,
        @RowCount = @RowCount out;
    -- update output message
    SET @Message += convert(nvarchar, @RowCount) + N' Stage Records Merged to Production ' + nchar(13) + nchar(10);
END;