USE calpass;

GO

IF (object_id('dbo.K12CteProdEscrow') is not null)
	BEGIN
		DROP PROCEDURE dbo.K12CteProdEscrow;
	END;

GO

CREATE PROCEDURE
	dbo.K12CteProdEscrow
	(
		@Escrow calpads.Escrow READONLY
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Message nvarchar(2048),
	@ErrorCode int = 70099,
	@ErrorSeverity int = 1,
	@Algorithm char(8) = 'SHA2_512',
	@EscrowName sysname = 'K12CteProd';

BEGIN
	SELECT
		Derkey1,
		School,
		AcYear,
		LocStudentId,
		CtePathwayCode,
		CtePathwayCompAcYear,
		DateAdded
	FROM
		(
			SELECT
				Derkey1 = 
					HASHBYTES(
						@Algorithm,
						upper(convert(nchar(3), f.NameFirst)) + 
						upper(convert(nchar(3), f.NameLast)) + 
						upper(convert(nchar(1), f.Gender)) + 
						convert(nchar(8), f.Birthdate)
					),
				School = s.DistrictCode + s.SchoolCode,
				AcYear = y.YearCodeAbbr,
				LocStudentId = calpass.dbo.get1289Encryption(f.StudentLocalId, 'X'),
				CtePathwayCode = s.CtePathwayCode,
				CtePathwayCompAcYear = s.CtePathwayCompletionYearCode,
				DateAdded = getdate(),
				RecordCount = row_number() over(
						partition by
							s.DistrictCode,
							s.SchoolCode,
							calpass.dbo.Get1289Encryption(f.StudentLocalId, 'X'),
							s.YearCode,
							s.CtePathwayCode
						order by
							(select 1) desc
					)
			FROM
				@Escrow e
				inner join
				calpads.Scte s with(index(pk_Scte))
					on s.StudentStateId = e.StudentStateId
					and s.SchoolCode = e.SchoolCode
					and s.YearCode = e.YearCode
				inner join
				calpads.Year y with(index(PK_Year))
					on y.YearCode = s.YearCode
				inner join
				calpads.Sinf f with(index(pk_Sinf))
					on e.StudentStateId = f.StudentStateId
					and e.SchoolCode = f.SchoolCode
					and e.SinfYearCode = f.YearCode
					and e.SinfEffectiveStartDate = f.EffectiveStartDate
			WHERE
				e.EscrowName = @EscrowName
				and f.StudentLocalId is not null
		) a
	WHERE
		RecordCount = 1;
END;