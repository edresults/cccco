USE calpass;

GO

IF (
	EXISTS (
		SELECT
			1
		FROM
			sys.schemas s
			inner join
			sys.procedures p
				on s.schema_id = p.schema_id
		WHERE
			s.name = 'dbo'
			and p.name = 'p_accuplacer_output'
		)
	)
	BEGIN
		DROP PROCEDURE dbo.p_accuplacer_output;
	END;

GO

CREATE PROCEDURE
	dbo.p_accuplacer_output
	(
		@user nvarchar(255),
		@password nvarchar(255),
		@server nvarchar(255),
		@ssh_rsa nvarchar(255),
		@port nvarchar(255)
	)
AS
	
DECLARE
	-- local variables
	@source_file_dir nvarchar(255) = '\\PRO-DAT-SQL-03\C$\Users\dlamoree\desktop\', --'
	@source_file_name nvarchar(255),
	@source_file_path nvarchar(255),
	@target_file_dir nvarchar(255) = '/home/accuplacer/', --'
	@date_string varchar(14),
	@cmd varchar(1024),
	@cmd_error bit,
	@cmd_lines varchar(max) = '';
	
BEGIN
	-- get time formatted string
	EXEC master.dbo.sp_file_time_format
		@date_output = @date_string OUTPUT;

	-- set file name
	SET @source_file_name = 'accuplacer_' + @date_string + '.txt';

	-- set file path
	SET @source_file_path = @source_file_dir + @source_file_name;

	-- set export command
	SET @cmd = 'bcp "EXEC calpass.dbo.p_accuplacer_select" QUERYOUT "' + @source_file_path + '" -T -c -q';

	-- execute command
	EXECUTE dbo.sp_shell_command_execute
		@cmd = @cmd,
		@cmd_error = @cmd_error OUTPUT,
		@cmd_lines = @cmd_lines OUTPUT;

	-- upload file
	EXECUTE sp_sftp_winscp_upload
		@user = @user,
		@password = @password,
		@server = @server,
		@port = @port,
		@ssh_rsa = @ssh_rsa,
		@source_file_dir = @source_file_dir,
		@source_file_name = @source_file_name,
		@target_file_dir = @target_file_dir;

	-- delete file
	EXECUTE master.dbo.clr_file_delete
		@file_path = @source_file_path;

	-- extract
	EXECUTE architecture.dbo.p_spss_output
		@object_name = 'accuplacerhistorical',
		@file_path = '\\PRO-DAT-SQL-03\C$\Users\dlamoree\desktop\accuplacer_spss_import.sps';

	-- upload
	EXECUTE sp_sftp_winscp_upload
		@user = @user,
		@password = @password,
		@server = @server,
		@port = @port,
		@ssh_rsa = @ssh_rsa,
		@source_file_dir = '\\PRO-DAT-SQL-03\C$\Users\dlamoree\desktop\', --'
		@source_file_name = 'accuplacer_spss_import.sps',
		@target_file_dir = @target_file_dir

	-- delete
	EXECUTE master.dbo.clr_file_delete
		@file_path = '\\PRO-DAT-SQL-03\C$\Users\dlamoree\desktop\accuplacer_spss_import.sps';
END;