CREATE PROCEDURE
    dbo.ad_GenCounts
AS

    /*
        author: adalton, dlamoree
        desc:   Generate counts for Cal-PASS Plus Member List
        notes:  
        tags:   Cal-PASS Plus, MemberList, Aspire, New Designs
    changelog: 
                1.12 (2019-11-27): dlamoree
                     Modification for DDL statements, Primary Keys
                1.11 (2019-09-04): dlamoree
                     Modification remove STAR data from CAASPP data if overlap
                1.10 (2019-07-31): dlamoree
                     Modification MOU tables replaced with singular table (calpass.dbo.cpp_mou)
                1.09 (2019-06-07): dlamoree
                     Modification ad_starcount table to sum CAASPP and STAR counts
                1.08 (2018-12-04): dlamoree
                     Modification for SIATech Charter Schools to be manually dissociated from their geographic Districts
                1.07 (2018-12-04): dlamoree
                     Replaced dependency for Univ data on Term to UnivTerm
                1.06 (2018-09-27): dlamoree
                     Hotfix for CAASPP
                1.05 (2018-09-20): dlamoree
                     Modification for Clayton Valley Charter High to be manually dissociated from Contra Costa County Office of Education
                1.04 (2018-08-09): dlamoree
                     Modification for CORE Butte Charter Schools to be manually dissociated from Butte County Office of Education
                1.03 (2018-07-17): dlamoree
                     Modification for New Designs Charter Schools to be manually dissociated from LA Unified.
                1.02 (2018-02-07): dlamoree
                     Added cpp_mou table to ad_MouMaster table to acccount for new University MOUs (dlamoree)
                1.01 (2018-01-10): dlamoree
                     Modification for some Aspire schools have submitted data with a DistrictCode value equal to the SchoolCode value (dlamoree)
                1.00 (2015-04-16): adalton
                     Implemented
    */

    SET NOCOUNT ON;
    SET XACT_ABORT ON;

BEGIN

    -- Charter Base
    IF (object_id('tempdb.dbo.#Charter') is not null)
        BEGIN
            DROP TABLE #Charter;
        END;

    -- Charter schools
    CREATE TABLE
        #Charter
        (
            CdsCodeFubar char(14) not null,
            CdsCodeRemap char(14) not null
        );

    ALTER TABLE
        #Charter
    ADD CONSTRAINT
        PK_#Charter
    PRIMARY KEY CLUSTERED
        (
            CdsCodeFubar
        );

    INSERT
        #Charter
        (
            CdsCodeFubar,
            CdsCodeRemap
        )
    SELECT DISTINCT
        CdsCodeFubar = right(i.CdsCode, 7) + right(i.CdsCode, 7),
        CdsCodeRemap = i.CdsCode
    FROM
        [PRO-DAT-SQL-03].calpass.calpads.Institution i
    WHERE
        i.Charter    = 'Y' and
        i.StatusType = 'Active';

    --
    -- K12
    --
    DROP TABLE ad_StudentCount;
    CREATE TABLE ad_StudentCount   ( School char(14) not null, AcYear char(4) not null, Students int not null, CONSTRAINT PK_ad_StudentCount PRIMARY KEY CLUSTERED ( School, Acyear ) );
    DROP TABLE ad_CourseCount;
    CREATE TABLE ad_CourseCount    ( School char(14) not null, AcYear char(4) not null, Courses  int not null, CONSTRAINT PK_ad_CourseCount  PRIMARY KEY CLUSTERED ( School, Acyear ) );
    DROP TABLE ad_AwardCount ;
    CREATE TABLE ad_AwardCount     ( School char(14) not null, AcYear char(4) not null, Awards   int not null, CONSTRAINT PK_ad_AwardCount   PRIMARY KEY CLUSTERED ( School, Acyear ) );
    DROP TABLE ad_StarCount  ;
    CREATE TABLE ad_StarCount      ( School char(14) not null, AcYear char(4) not null, Star     int not null, CONSTRAINT PK_ad_StarCount    PRIMARY KEY CLUSTERED ( School, Acyear ) );
    DROP TABLE ad_CAHSEECount;
    CREATE TABLE ad_CAHSEECount    ( School char(14) not null, AcYear char(4) not null, CAHSEE   int not null, CONSTRAINT PK_ad_CAHSEECount  PRIMARY KEY CLUSTERED ( School, Acyear ) );

    -- K12StudentProd Counts
    INSERT INTO
        ad_StudentCount
    SELECT
        School   = coalesce(a.CdsCodeRemap, p.School),
        AcYear   = p.AcYear,
        Students = count(p.LocStudentId)
    FROM
        calpass.dbo.K12StudentProd p
        left outer join
        #Charter a on
            a.CdsCodeFubar = p.School
    GROUP BY
        coalesce(a.CdsCodeRemap, p.School),
        p.AcYear;

    -- K12CourseProd Count
    INSERT INTO
        ad_CourseCount
    SELECT
        School  = coalesce(a.CdsCodeRemap, p.School),
        AcYear  = p.AcYear,
        Courses = count(p.LocStudentId)
    FROM
        calpass.dbo.K12CourseProd p
        left outer join
        #Charter a on
            a.CdsCodeFubar = p.School
    GROUP BY
        coalesce(a.CdsCodeRemap, p.School),
        p.AcYear;

    INSERT INTO
        ad_AwardCount
    SELECT
        School = coalesce(a.CdsCodeRemap, p.School),
        AcYear = p.AcYear,
        Awards = count(p.LocStudentId)
    FROM
        calpass.dbo.K12AwardProd p
        left outer join
        #Charter a on
            a.CdsCodeFubar = p.School
    GROUP BY
        coalesce(a.CdsCodeRemap, p.School),
        p.AcYear;

    IF (object_id('tempdb.dbo.#ad_StarCount') is not null)
        BEGIN
            DROP TABLE #ad_StarCount;
        END;

    --Star Count
    SELECT
        School,
        AcYear,
        Star = count(LocStudentId)
    INTO
        #ad_StarCount
    FROM
        [calpass].[dbo].[K12StarDemProd]
    GROUP BY
        School,
        AcYear;

    IF (object_id('tempdb.dbo.#ad_CAASPPCount') is not null)
        BEGIN
            DROP TABLE #ad_CAASPPCount;
        END;

    --CAASPP
    SELECT
        School,
        AcYear,
        CAASPP = sum(CAASPP)
    INTO
        #ad_CAASPPCount
    FROM
        (
            SELECT
                School = col10 + col16,
                AcYear = '1314',
                CAASPP = count(distinct col4)
            FROM
                [PRO-DAT-SQL-03].calpass.calpads.caaspp20132014Test
            GROUP BY
                col10 + col16
            UNION
            SELECT
                School = coalesce(
                        ela_pt_district_code,
                        ela_npt_district_code,
                        mat_pt_district_code,
                        mat_npt_district_code,
                        sci_district_code,
                        rla_district_code
                    ) + coalesce(
                        ela_pt_school_code,
                        ela_npt_school_code,
                        mat_pt_school_code,
                        mat_npt_school_code,
                        sci_school_code,
                        rla_school_code
                    ),
                AcYear = '1415',
                CAASPP = count(distinct student_state_id)
            FROM
                [PRO-DAT-SQL-03].calpass.calpads.caaspp20142015
            WHERE
                coalesce(
                    ela_pt_district_code,
                    ela_npt_district_code,
                    mat_pt_district_code,
                    mat_npt_district_code,
                    sci_district_code,
                    rla_district_code
                ) is not null
            GROUP BY
                coalesce(
                        ela_pt_district_code,
                        ela_npt_district_code,
                        mat_pt_district_code,
                        mat_npt_district_code,
                        sci_district_code,
                        rla_district_code
                    ) + coalesce(
                        ela_pt_school_code,
                        ela_npt_school_code,
                        mat_pt_school_code,
                        mat_npt_school_code,
                        sci_school_code,
                        rla_school_code
                    )
            UNION
            SELECT
                School = school_code,
                AcYear = '1516',
                CAASPP = count(distinct student_state_id)
            FROM
                [PRO-DAT-SQL-03].calpass.calpads.caaspp20152016
            GROUP BY
                school_code
            UNION
            SELECT
                School = school_code,
                AcYear = '1617',
                CAASPP = count(distinct student_state_id)
            FROM
                [PRO-DAT-SQL-03].calpass.calpads.caaspp20162017
            GROUP BY
                school_code
            UNION
            SELECT
                School = col10,
                AcYear = '1718',
                CAASPP = count(distinct col1)
            FROM
                [PRO-DAT-SQL-03].calpass.calpads.caaspp20172018
            GROUP BY
                col10
        ) u
    GROUP BY
        School,
        AcYear;

    --Combined STAR/CAASPP
    INSERT INTO
        ad_StarCount
    SELECT
        School,
        AcYear,
        Star = sum(Star)
    FROM
        (
            SELECT
                School,
                AcYear,
                Star
            FROM
                #ad_starcount s
            WHERE
                not exists (
                    SELECT
                        1
                    FROM
                        #ad_CAASPPCount c
                    WHERE
                        c.AcYear = '1314'
                        and s.School = c.School
                        and s.AcYear = c.AcYear
                )
            UNION
            SELECT
                School,
                AcYear,
                CAASPP
            FROM
                #ad_CAASPPCount
        ) u
    GROUP BY
        School,
        AcYear
    ORDER BY
        School,
        AcYear;

    --CAHSEE Count
    INSERT INTO
        ad_CAHSEECount
    SELECT
        School = schoolidentifier,
        AcYear = schoolyear,
        CAHSEE = count(localstudentid)
    FROM
        calpass.dbo.CAHSEEDemographics
    GROUP BY
        schoolidentifier,
        schoolyear;

    --
    -- Community College
    --

    DROP TABLE ad_CCStudentCount;
    CREATE TABLE ad_CCStudentCount ( CollegeId char(6) not null, AcademicYear char(9) not null, Students int not null, CONSTRAINT PK_ad_CCStudentCount PRIMARY KEY CLUSTERED ( CollegeId, AcademicYear ) );
    DROP TABLE ad_CCCourseCount;
    CREATE TABLE ad_CCCourseCount  ( CollegeId char(6) not null, AcademicYear char(9) not null, Courses  int not null, CONSTRAINT PK_ad_CCCourseCount  PRIMARY KEY CLUSTERED ( CollegeId, AcademicYear ) );
    DROP TABLE ad_CCAwardCount;
    CREATE TABLE ad_CCAwardCount   ( CollegeId char(6) not null, AcademicYear char(9) not null, Awards   int not null, CONSTRAINT PK_ad_CCAwardCount   PRIMARY KEY CLUSTERED ( CollegeId, AcademicYear ) );

    --CC Student Count
    INSERT INTO
        ad_CCStudentCount
    SELECT
        CollegeId    = collegeid,
        AcademicYear = AcademicYear,
        Students     = count(studentid)
    FROM
        [PRO-DAT-SQL-03].calpass.dbo.CCStudentProd s
        inner join
        calpass.dbo.term t
            on s.termid = t.termcode
    GROUP BY
        CollegeId,
        AcademicYear;

    --CC Course Count
    INSERT INTO
        ad_CCCourseCount
    SELECT
        CollegeId    = collegeid,
        AcademicYear = AcademicYear,
        Courses      = count(studentid)
    FROM
        [PRO-DAT-SQL-03].calpass.dbo.CCCourseProd c
        inner join
        calpass.dbo.term t
            on c.termid = t.termcode
    GROUP BY
        CollegeId,
        AcademicYear;

    --CC Award Count
    INSERT INTO
        ad_CCAwardCount
    SELECT
        CollegeId    = collegeid,
        AcademicYear = AcademicYear,
        Awards       = count(studentid)
    FROM
        [PRO-DAT-SQL-03].calpass.dbo.CCAwardProd a
        inner join
        calpass.dbo.term t
            on a.termid = t.termcode
    GROUP BY
        CollegeId,
        AcademicYear;

    --
    -- University
    --

    DROP TABLE ad_UNIVStudentCount;
    CREATE TABLE ad_UNIVStudentCount ( CollegeId char(6) not null, AcademicYear char(9) not null, Students int not null, CONSTRAINT PK_ad_UNIVStudentCount PRIMARY KEY CLUSTERED ( CollegeId, AcademicYear ) );
    DROP TABLE ad_UNIVCourseCount;
    CREATE TABLE ad_UNIVCourseCount  ( CollegeId char(6) not null, AcademicYear char(9) not null, Courses  int not null, CONSTRAINT PK_ad_UNIVCourseCount  PRIMARY KEY CLUSTERED ( CollegeId, AcademicYear ) );
    DROP TABLE ad_UNIVAwardCount;
    CREATE TABLE ad_UNIVAwardCount   ( CollegeId char(6) not null, AcademicYear char(9) not null, Awards   int not null, CONSTRAINT PK_ad_UNIVAwardCount   PRIMARY KEY CLUSTERED ( CollegeId, AcademicYear ) );

    --UNIV Student Count
    INSERT INTO
        ad_UNIVStudentCount
    SELECT
        CollegeId    = s.School,
        AcademicYear = t.AcademicYear,
        Students     = count(s.StudentId)
    FROM
        UNIVStudentProd s
        inner join
        UnivTerm t
            on s.YrTerm = t.TermCode
    GROUP BY
        s.School,
        t.AcademicYear

    --UNIV Course Count
    INSERT INTO
        ad_UNIVCourseCount
    SELECT
        CollegeId    = c.School,
        AcademicYear = t.AcademicYear,
        Courses      = count(c.StudentId)
    FROM
        UnivCourseProd c
        inner join
        UnivTerm t
            on c.YrTerm = t.TermCode
    GROUP BY
        c.School,
        t.AcademicYear;

    --UNIV Award Count
    INSERT INTO
        ad_UNIVAwardCount
    SELECT
        CollegeId    = a.School,
        AcademicYear = t.AcademicYear,
        Awards       = count(a.StudentId)
    FROM
        UnivAwardProd a
        inner join
        UnivTerm t
            on a.YrTerm = t.TermCode
    GROUP BY
        a.School,
        t.AcademicYear;

    --
    -- Organization
    --

    --Create Higher Ed List Parent vs District
    drop table org
    select * 
    into org
    from
    (select  organizationid, organizationtypeid, organizationname, organizationcode, organizationcodetypeid from calpass.dbo.organization
    where organizationcodetypeid > 1 and parentid is null
    union all
    select  parentid as organizationid, organizationtypeid, organizationname, organizationcode, organizationcodetypeid from calpass.dbo.organization
    where organizationcodetypeid > 1 and parentid is not null
    ) as HigherEdList
    order by OrganizationName


    --Create MOU Master
    --drop table ad_MOUMaster
    --select organizationid, max(datemouexpires) as dtMOUExpires 
    --Into ad_MOUMaster
    --from 
    --(select organizationid, case when organizationid in ('171235','166702','171235') then '2015-06-30' else DateMouExpires end as DateMouExpires
    --from oms_mou
    --union all
    --select OrganizationId,case when organizationid in ('171235','166702','171235') then '2015-06-30' else dtMouExpires end as dtMouExpires 
    --from organizationmou where moustatus = 5) as AllMOU
    --group by OrganizationId
    --order by OrganizationId

    IF (object_id('ad_MouMaster') is not null)
        BEGIN
            DROP TABLE ad_MOUMaster;
        END;

    SELECT
        OrganizationId,
        MouExpireDate = max(ExpirationDate)
    INTO
        ad_MOUMaster
    FROM
        cpp_mou
    WHERE
        deleted is null
    GROUP BY
        OrganizationId
    ORDER BY
        OrganizationId;

    --MOU Count
    drop table ad_DistrictMOU
    select distinct districtorganizationid, max(case when o.MouExpireDate is null then so.MouExpiredDate else o.MouExpireDate end) as MOUExpiredDate, Academicyear 
    into ad_DistrictMOU
    from [calpass].[dbo].[SumOrganization] as so
    left outer join [calpass].[dbo].[ad_MOUMaster] as O on so.DistrictOrganizationId=o.OrganizationId
    group by districtorganizationid,Academicyear
    order by DistrictOrganizationId, AcademicYear

    --Get Sharing Counts
    drop table ad_SharingSummary
    SELECT userorganizationid, case when count(organizationid) > 370 then 1 else 0 end as OpenSharing
    into ad_SharingSummary
    FROM [calpass].[dbo].[Reporting_ShareListSummary]
    where academicyear = 2015
    group by userorganizationid
    order by userorganizationid

    --build AD_memberlist
    DROP TABLE ad_memberlist;

    SELECT
        region_id,
        countyid,
        districtname = organizationname,
        districtorganizationid = organizationid,
        organizationcodetypeid,
        organizationtypeid 
    INTO
        ad_memberlist
    FROM
        calpass.dbo.organization a
        left outer join
        [calpass].[dbo].[Lookup_External_EconomicRegion2CountyMap] c
            on a.countyid = c.county_id
    WHERE
        a.OrganizationTypeId in (1,2,3)
        and a.ParentId is null
        and substring(a.OrganizationName,1,1) <> '"'
    ORDER BY
        a.OrganizationName;

    --Distinct School List
    DROP TABLE ad_OrgList;

    SELECT
        districtname,
        DistrictOrganizationId
    INTO
        ad_OrgList
    FROM
        (
            SELECT DISTINCT
                m.districtname,
                m.DistrictOrganizationId
            FROM
                ad_memberlist m
                -- left outer join
                -- [calpass].[dbo].[ad_DistrictMOU] so
                -- 	on m.DistrictOrganizationId = so.DistrictOrganizationId 
                -- left outer join
                -- calpass.dbo.org
                -- 	on org.organizationid = m.DistrictOrganizationId
            WHERE
                m.organizationtypeid = 2
            UNION ALL
            SELECT DISTINCT
                m.districtname,
                m.DistrictOrganizationId
            FROM
                ad_memberlist m
                -- left outer join
                -- [calpass].[dbo].[ad_DistrictMOU] so
                -- 	on m.DistrictOrganizationId = so.DistrictOrganizationId 
                -- left outer join
                -- calpass.dbo.org
                -- 	on org.organizationid = m.DistrictOrganizationId
            WHERE
                m.organizationtypeid = 3
                and m.organizationcodetypeid = 2
            UNION ALL
            SELECT DISTINCT
                m.districtname,
                m.DistrictOrganizationId
            FROM
                ad_memberlist m
                inner join
                calpass.dbo.organization b
                    on m.DistrictOrganizationId = b.ParentId
                -- left outer join
                -- [calpass].[dbo].[ad_DistrictMOU] so
                -- 	on m.DistrictOrganizationId = so.DistrictOrganizationId 
                -- left outer join
                -- calpass.dbo.year YR
                -- 	on so.AcademicYear = yr.FullYearWithDash
        ) as Orgs
    ORDER BY
        districtname;

    INSERT INTO
        ad_orglist
        (
            districtname,
            districtorganizationid
        )
    VALUES
        (
            'Orcutt Academy Charter',
            '173029'
        );


END;