USE calpass;

GO

IF (object_id('dbo.GenericMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.GenericMerge;
	END;

GO

CREATE PROCEDURE
	dbo.GenericMerge
	(
		@SourceSchemaTableName nvarchar(517),
		@StageSchemaTableName nvarchar(517),
		@IsAssimilate bit = 1
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Message nvarchar(2048);

BEGIN
	-- validate objects are not the same
	IF (@SourceSchemaTableName = @StageSchemaTableName)
		BEGIN
			SET @Message = N'Objects equivalent';
			THROW 70099, @Message, 1;
		END;
	-- validate source table
	IF (object_id(@SourceSchemaTableName) is null)
		BEGIN
			SET @Message = N'Source table not found';
			THROW 70099, @Message, 1;
		END;
	-- validate stage table
	IF (object_id(@StageSchemaTableName) is null)
		BEGIN
			SET @Message = N'Stage table not found';
			THROW 70099, @Message, 1;
		END;

	IF (@IsAssimilate = 1)
		BEGIN
			-- force stage table to adhere to production primary key
			EXECUTE dbo.TableAssimilate
				@ProductionName = @SourceSchemaTableName,
				@StageName = @StageSchemaTableName;
		END;

	-- merge stage table to production table
	EXECUTE dbo.TableMerge
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;
END;