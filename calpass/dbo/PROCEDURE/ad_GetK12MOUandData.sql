CREATE PROCEDURE
    dbo.ad_GetK12MOUandData
    (
        @District int
    )
AS

BEGIN
    SELECT
        District,
        DistrictOrganizationId,
        DistrictName,
        MOUExpire,
        AcademicYear,
        Students,
        Courses,
        Awards,
        Star,
        CAHSEE
    FROM
        (
            SELECT TOP 10
                District               = left(d.OrganizationCode, 7),
                DistrictOrganizationId = d.OrganizationID,
                DistrictName           = d.OrganizationName,
                MOUExpire              = u.expirationDate,
                AcademicYear           = t.DisplayYear,
                Students               = sum(s.students),
                Courses                = sum(c.Courses),
                Awards                 = sum(a.Awards),
                Star                   = sum(r.Star),
                CAHSEE                 = sum(e.CAHSEE)
            FROM
                ad_term t
                inner join
                Organization d
                    on d.OrganizationTypeId = t.OrgType
                inner join
                Organization o
                    on o.ParentId = d.OrganizationId
                left outer join
                CPP_Mou u
                    on  d.OrganizationId = u.organizationId
                    and u.deleted is null
                left outer join
                ad_StudentCount s
                    on s.School = o.OrganizationCode
                    and s.AcYear = t.Term
                left outer join
                ad_CourseCount c
                    on c.School = o.OrganizationCode
                    and c.AcYear = t.Term
                left outer join
                ad_AwardCount a
                    on a.School = o.OrganizationCode
                    and a.AcYear = t.Term
                left outer join
                ad_StarCount r
                    on r.School = o.OrganizationCode
                    and r.AcYear = t.Term
                left outer join
                ad_CAHSEECount e
                    on e.School = o.OrganizationCode
                    and e.AcYear = t.Term
            WHERE
                d.OrganizationId = @District
                and convert(date, substring(t.DisplayYear, 6, 4) + '0601') <= CURRENT_TIMESTAMP
            GROUP BY
                t.DisplayYear,
                left(d.OrganizationCode, 7),
                d.OrganizationID,
                d.OrganizationName,
                u.expirationDate,
                t.Term
            ORDER BY
                t.DisplayYear DESC
        ) a
    ORDER BY
        AcademicYear ASC
END;


/*
exec ad_GetK12MOUandData 156784

select * from calpass.dbo.organization
where organizationname like 'berkeley%'

select * from calpass.dbo.ad_term as TERM
where orgtype = 1 and [order] between 6 and 16

as

select * 
into #ActiveYear from calpass.dbo.ad_term as TERM
where orgtype = 1 and [order] between 8 and 17

select 
left(organizationcode,7) as District, org.OrganizationID as districtorganizationid, OrganizationName as districtname, MOU.expirationDate as MOUExpire, STD.AcYear , TERM.DisplayYear, sum(STD.students) as Students, sum(cast(COURSE.Courses as float)) as cour
ses, sum(cast(AWARD.Awards as float)) as awards, sum(cast(STAR.Star as float)) as star, sum(cast(CAHSEE.CAHSEE as float)) as cahsee
into #ValidData
from calpass.dbo.Organization as ORG
left join ad_StudentCount as STD 
on left(org.organizationcode,7) = left(STD.School,7)
left join ad_CourseCount as COURSE
on std.school = course.school
and STD.AcYear = COURSE.AcYear
left join ad_AwardCount as AWARD
on std.school = AWARD.school
and STD.AcYear = AWARD.AcYear
left join ad_StarCount as STAR
on std.school = STAR.school
and STD.AcYear = STAR.AcYear
left join ad_CAHSEECount as CAHSEE
on std.school = CAHSEE.school
and STD.AcYear = CAHSEE.AcYear
left join calpass.dbo.CPP_Mou as MOU
on ORG.OrganizationId = MOU.organizationId
left outer join calpass.dbo.ad_term as TERM
on STD.acyear = TERM.Term 
where parentid is null  and organizationtypeid = 1 and (std.acyear is null or std.acyear between '0405' and '8888') and org.OrganizationId = @District
group by left(organizationcode,7), org.OrganizationId, OrganizationName,MOU.expirationDate, STD.AcYear, Term.DisplayYear
having isnull(sum(STD.students),0) + isnull(sum(cast(COURSE.Courses as float)),0) + isnull(sum(cast(AWARD.Awards as float)),0) > 10
order by OrganizationName, Term.DisplayYear 

select a.DisplayYear as academicyear, b.* from #ActiveYear A
left join #ValidData B
on a.displayYear = b.displayyear

select a.DisplayYear as academicyear, b.* from #ActiveYear A
left join #ValidData B
on a.displayYear = b.displayyear

exec ad_GetK12MOUandData 156784

select * from calpass.dbo.organization
where organizationname like 'berkeley%'

select * from calpass.dbo.ad_term as TERM
where orgtype = 1 and [order] between 6 and 16

select  m.districtname, m.districtorganizationid, so.academicyear, max(mouexpireddate) as MOUExpire, sum(students) as Students, sum(courses) as courses, sum(awards) as awards, sum(star) as star,
sum(cahsee) as cahsee from ad_memberlist as m
inner join calpass.dbo.organization b on m.districtorganizationid = b.parentid
left outer join [calpass].[dbo].[ad_DistrictMOU] as so on m.districtorganizationid = so.districtorganizationid 
left outer join calpass.dbo.year as YR on so.AcademicYear = yr.FullYearWithDash
left outer join ad_StudentCount as STD on b.organizationcode = s.school and yr.shortyear = s.acyear
left outer join ad_CourseCount as COU on b.organizationcode = cou.school and yr.ShortYear=cou.acyear
left outer join ad_AwardCount as AWA on b.organizationcode = awa.school and yr.ShortYear=awa.acyear
left outer join ad_STARCount as STA on b.organizationcode = sta.school and yr.ShortYear=sta.acyear
left outer join ad_CAHSEECount as CAH on b.organizationcode = cah.school and yr.ShortYear=cah.acyear
where m.districtorganizationid= @District and (students is not null or courses is not null or awards is not null or star is not null or cahsee is not null)
and (students + courses + awards) > 10
and yr.ShortYear between 0405 and 8888  and substring(so.academicyear,1,4) >= 2005
group by m.districtname, m.districtorganizationid, so.Academicyear
order by districtname, academicyear
*/