USE calpass;

GO

IF (object_id('dbo.CCAwardProdEscrow') is not null)
	BEGIN
		DROP PROCEDURE dbo.CCAwardProdEscrow;
	END;

GO

CREATE PROCEDURE
	dbo.CCAwardProdEscrow
	(
		@Escrow comis.Escrow READONLY
	)
AS

-- Output rows for dbo.CCAwardProd data type

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@EscrowName sysname = 'CCAwardProd';

BEGIN
	SELECT
		CollegeId = e.CollegeIpedsCode,
		StudentComisId = e.StudentLocalId,
		StudentId = e.StudentSocialId,
		IdStatus = e.IdStatus,
		TermId = aw.term_id,
		TopCode = aw.top_code,
		Award = aw.award,
		DateOfAward = convert(char(8), aw.date, 112),
		RecordId = aw.record_id,
		ProgramCode = aw.program_code
	FROM
		@Escrow e
		inner join
		comis.spawards aw
			on aw.college_id = e.CollegeId
			and aw.student_id = e.StudentLocalId
			and aw.term_id = e.TermId
	WHERE
		e.EscrowName = @EscrowName;
END;