USE calpass;

GO

IF (object_id('dbo.TableAssimilate') is not null)
	BEGIN
		DROP PROCEDURE dbo.TableAssimilate;
	END;

GO

CREATE PROCEDURE
	dbo.TableAssimilate
	(
		@ProductionName nvarchar(517),
		@StageName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Message nvarchar(2048),
	@NullConstraintWhere nvarchar(4000) = N'',
	@DuplicateConstraintWhere nvarchar(4000) = N'',
	@DuplicateConstraintGroup nvarchar(4000) = N'',
	@RowCount int,
	@Sql nvarchar(max);

BEGIN
	-- compare names
	IF (@ProductionName = @StageName)
		BEGIN
			SET @Message = N'Production Table Name and Stage Table Name must not be the same';
			THROW 70099, @Message, 1;
		END;

	-- validate production table
	IF (object_id(@ProductionName) is null)
		BEGIN
			SET @Message = N'Production table not found';
			THROW 70099, @Message, 1;
		END;

	-- validate stage table
	IF (object_id(@StageName) is null)
		BEGIN
			SET @Message = N'Stage table not found';
			THROW 70099, @Message, 1;
		END;

	-- force stage table to adhere to not null column constraints
	EXECUTE dbo.TableAssimilateNull
		@ProductionName = @ProductionName,
		@StageName = @StageName;
		
	-- force stage table to adhere to production primary key
	EXECUTE dbo.TableAssimilatePk
		@ProductionName = @ProductionName,
		@StageName = @StageName;

END;