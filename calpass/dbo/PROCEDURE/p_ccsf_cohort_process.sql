USE calpass;

IF (object_id('dbo.p_ccsf_cohort_process', 'P') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_ccsf_cohort_process;
	END;

GO

CREATE PROCEDURE
	dbo.p_ccsf_cohort_process
AS
	
	SET NOCOUNT ON;

BEGIN

	TRUNCATE TABLE calpass.dbo.ccsf_cohort;

	BULK INSERT
		calpass.dbo.ccsf_cohort
	FROM
		N'\\nas-pro-dat-01\webfileuploads\FileDrop\170731\CCSF Placement data file.txt'
	WITH
		(
			FORMATFILE = '\\nas-pro-dat-01\webfileuploads\FileDrop\170731\ccsf_cohort.fmt',
			FIRE_TRIGGERS,
			CHECK_CONSTRAINTS,
			KEEPNULLS
		);

	-- delete derkey collisions: 2 records deleted (93V_8REGG0EO0ET)
	DELETE
		a
	FROM
		calpass.dbo.ccsf_cohort a
	WHERE
		exists (
			SELECT
				b.derkey1
			FROM
				calpass.dbo.ccsf_cohort b
			GROUP BY
				b.derkey1
			HAVING
				count(*) > 1
				and a.derkey1 = b.derkey1
		);
END;