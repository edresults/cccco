USE calpass;

IF (object_id('dbo.p_ccsf_output_process', 'P') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_ccsf_output_process;
	END;

GO

CREATE PROCEDURE
	dbo.p_ccsf_output_process
AS
	
	SET NOCOUNT ON;

DECLARE
	@organization_name varchar(255) = 'San Francisco Unified',
	@AcYear char(4) = '1415',
	@CollegeId char(6) = '112190',
	@GradeLevel char(2) = '12';

BEGIN

	TRUNCATE TABLE calpass.dbo.ccsf_output;

	INSERT INTO
		calpass.dbo.ccsf_output
		(
			School,
			LocStudentId,
			AcYear,
			GradeLevel,
			gender,
			ethnicity,
			hispanic_ind,
			ses_ind,
			Derkey1				
		)
	SELECT
		School = rtrim(hss.School),
		LocStudentId = rtrim(hss.LocStudentId),
		AcYear = rtrim(hss.AcYear),
		GradeLevel = rtrim(hss.GradeLevel),
		gender = a.student_gender,
		ethnicity = a.student_ethnicity,
		hispanic_ind = a.student_hispanic_ind,
		ses_ind = a.student_ses_ind,
		Derkey1 = rtrim(hss.Derkey1)
	FROM
		calpass.dbo.K12StudentProd hss
		inner join
		calpass.dbo.organization o
			on o.organizationCode = hss.school
		inner join
		calpass.dbo.organization d
			on d.organizationId = o.ParentId
		cross apply
		calpass.dbo.f_hs_student_attributes(
			hss.derkey1,
			hss.AcYear
		) a
	WHERE
		d.organizationName = @organization_name
		and hss.GradeLevel = @GradeLevel
		and hss.AcYear = @AcYear;

	DELETE
		a
	FROM
		calpass.dbo.ccsf_output a
	WHERE
		exists (
			SELECT
				1
			FROM
				calpass.dbo.ccsf_output b
			GROUP BY
				b.derkey1
			HAVING
				count(*) > 1
				and a.derkey1 = b.derkey1
		);

	UPDATE
		t
	SET
		t.grad_ind = isnull(s.grad_ind, 0)
	FROM
		calpass.dbo.ccsf_output t
		left outer join
		(
			SELECT
				hsa.School,
				hsa.LocStudentId,
				max(case when hsa.AwardType in ('100','106','108','120','250','320','330','480') then 1 else 0 end) as grad_ind
			FROM
				calpass.dbo.K12AwardProd hsa
			WHERE
				exists (
					SELECT
						1
					FROM
						calpass.dbo.ccsf_output a
					WHERE
						hsa.School = a.School
						and hsa.LocStudentId = a.LocStudentId
				)
			GROUP BY
				hsa.School,
				hsa.LocStudentId
		) s
			on t.School = s.School
			and t.LocStudentId = s.LocStudentId;

	UPDATE
		t
	SET
		t.ag_ind = isnull(s.ag_ind, 0)
	FROM
		calpass.dbo.ccsf_output t
		left outer join
		(
			SELECT
				hsc.School,
				hsc.LocStudentId,
				ag_ind = 1
			FROM
				calpass.dbo.ccsf_output hss
				inner join
				calpass.dbo.K12CourseProd hsc
					on hss.School = hsc.School
					and hss.LocStudentId = hsc.LocStudentId
				inner join
				calpass.dbo.K12CourseProd_Grade hscg
					on hscg.code = hsc.grade
			GROUP BY
				hsc.School,
				hsc.LocStudentId
			HAVING
				-- <AG requirements>
				max(case when left(AGstatus, 1) = 'A' and hscg.success_ind = 1 then 1 else 0 end) = 1
				and max(case when left(AGstatus, 1) = 'B' and hscg.success_ind = 1 then 1 else 0 end) = 1
				and max(case when left(AGstatus, 1) = 'C' and hscg.success_ind = 1 then 1 else 0 end) = 1
				and max(case when left(AGstatus, 1) = 'D' and hscg.success_ind = 1 then 1 else 0 end) = 1
				and max(case when left(AGstatus, 1) = 'E' and hscg.success_ind = 1 then 1 else 0 end) = 1
				and max(case when left(AGstatus, 1) = 'F' and hscg.success_ind = 1 then 1 else 0 end) = 1
				and max(case when left(AGstatus, 1) = 'G' and hscg.success_ind = 1 then 1 else 0 end) = 1
				-- </AG requirements>
		) s
			on t.School = s.School
			and t.LocStudentId = s.LocStudentId;

	UPDATE
		t
	SET
		t.de_ce_ind = isnull(s.de_ce_ind, 0)
	FROM
		calpass.dbo.ccsf_output t
		left outer join
		(
			SELECT
				c.derkey1,
				de_ce_ind = max(cccg.success_ind)
			FROM
				calpass.dbo.ccsf_output c
				inner join
				calpass.dbo.K12StudentProd_AcYear hsst
					on c.AcYear = hsst.code
				inner join
				calpass.dbo.CCStudentProd ccs
					on c.Derkey1 = ccs.Derkey1
				inner join
				calpass.dbo.CCCourseProd ccc
					on ccs.CollegeId = ccc.CollegeId
					and ccs.StudentId = ccc.StudentId
					and ccs.TermId = ccc.TermId
				inner join
				calpass.dbo.CCCourseProd_Grade cccg
					on cccg.code = ccc.grade
				inner join
				calpass.dbo.CCStudentProd_termId ccst
					on ccst.code = ccs.termId
					and ccst.year_trailing <= hsst.year_trailing
			WHERE
				ccc.creditFlag in ('T', 'D', 'C', 'S')
			GROUP BY
				c.Derkey1
			HAVING
				max(cccg.success_ind) = 1
		) s
			on s.Derkey1 = t.Derkey1;

	UPDATE
		t
	SET
		t.eap_math_ind = isnull(s.eap_math_ind, 0),
		t.eap_engl_ind = isnull(s.eap_engl_ind, 0)
	FROM
		calpass.dbo.ccsf_output t
		left outer join
		(
			SELECT
				eap.School,
				eap.LocStudentId,
				eap_math_ind = max(case eap.EAPMATHStatus when '1' then 1 else 0 end),
				eap_engl_ind = max(case eap.EAPELAStatus when '1' then 1 else 0 end)
			FROM
				calpass.dbo.K12StarDemProd eap
			WHERE
				-- <cohort>
				exists (
					SELECT
						1
					FROM
						calpass.dbo.ccsf_output c
					WHERE
						c.School = eap.School
						and c.LocStudentId = eap.LocStudentId
				)
				-- </cohort>
			GROUP BY
				eap.School,
				eap.LocStudentId
		) s
			on t.School = s.School
			and t.LocStudentId = s.LocStudentId;

	UPDATE
		t
	SET
		t.matric_ccsf_first_term_id = s.matric_ccsf_first_term_id
	FROM
		calpass.dbo.ccsf_output t
		left outer join
		(
			SELECT
				c.derkey1,
				matric_ccsf_first_term_id = min(t2.code)
			FROM
				calpass.dbo.ccsf_output c
				inner join
				calpass.dbo.K12StudentProd_AcYear t1
					on t1.code = c.AcYear
				inner join
				calpass.dbo.CCStudentProd ccs
					on ccs.derkey1 = c.derkey1
				inner join
				calpass.dbo.CCCourseProd ccc
					on ccc.CollegeId = ccs.CollegeId
					and ccc.StudentId = ccs.StudentId
					and ccc.TermId = ccs.TermId
				inner join
				calpass.dbo.CCCourseProd_Grade cccg
					on cccg.code = ccc.Grade
				inner join
				calpass.dbo.CCStudentProd_TermId t2
					on t2.code = ccs.termId
			WHERE
				ccs.CollegeId = @CollegeId
				and cccg.completion_denominator_ind = 1
				and ccc.creditFlag in ('T', 'D', 'C', 'S')
				and t1.year_trailing < t2.year_trailing
			GROUP BY
				c.derkey1
		) s
			on s.derkey1 = t.derkey1;

	UPDATE
		t
	SET
		t.t1_enrolled_math = isnull(s.t1_enrolled_math, 0),
		t.t1_enrolled_engl = isnull(s.t1_enrolled_engl, 0),
		t.t1_enrolled_esl = isnull(s.t1_enrolled_esl, 0),
		t.t1_units_attempted = isnull(s.t1_units_attempted, 0),
		t.t1_units_earned = isnull(s.t1_units_earned, 0),
		t.t1_grade_points = isnull(s.t1_grade_points, 0)
	FROM
		calpass.dbo.ccsf_output t
		left outer join
		(
			SELECT
				c.derkey1,
				t1_enrolled_math = max(case when ccc.TopCode = '170100' and cccg.enroll_ind = 1 then 1 else 0 end),
				t1_enrolled_engl = max(case when ccc.TopCode = '150100' and cccg.enroll_ind = 1 then 1 else 0 end),
				t1_enrolled_esl = max(case when ccc.TopCode in ('493084','493085','493086','493087') and cccg.enroll_ind = 1 then 1 else 0 end),
				t1_units_attempted = sum(case when cccg.gpa_ind = 1 then ccc.UnitsAttempted else 0 end),
				t1_units_earned = sum(case when cccg.gpa_ind = 1 then ccc.UnitsEarned else 0 end),
				t1_grade_points = sum(case when cccg.gpa_ind = 1 then ccc.UnitsAttempted * cccg.points else 0 end)
			FROM
				calpass.dbo.ccsf_output c
				inner join
				calpass.dbo.CCStudentProd ccs
					on c.derkey1 = ccs.derkey1
					and c.matric_ccsf_first_term_id = ccs.termId
				inner join
				calpass.dbo.CCCourseProd ccc
					on ccs.CollegeId = ccc.CollegeId
					and ccs.StudentId = ccc.StudentId
					and ccs.TermId = ccc.TermId
				inner join
				calpass.dbo.CCCourseProd_Grade cccg
					on cccg.code = ccc.grade
			WHERE
				ccc.creditFlag in ('T', 'D', 'C', 'S')
			GROUP BY
				c.derkey1
		) s
			on t.derkey1 = s.derkey1;

	UPDATE
		t
	SET
		t.y1_units_attempted = isnull(s.y1_units_attempted, 0),
		t.y1_units_earned = isnull(s.y1_units_earned, 0),
		t.y1_grade_points = isnull(s.y1_grade_points, 0),
		t.y1_math_units_6plus = isnull(s.y1_math_units_6plus, 0),
		t.y1_engl_units_6plus = isnull(s.y1_engl_units_6plus, 0),
		t.y1_passed_math_college_level = isnull(s.y1_passed_math_college_level, 0),
		t.y1_passed_engl_college_level = isnull(s.y1_passed_engl_college_level, 0),
		t.y1_passed_esl_college_level = isnull(s.y1_passed_esl_college_level, 0)
	FROM
		calpass.dbo.ccsf_output t
		left outer join
		(
			SELECT
				c.derkey1,
				y1_units_attempted = sum(case when cccg.gpa_ind = 1 then ccc.UnitsAttempted else 0 end),
				y1_units_earned = sum(case when cccg.gpa_ind = 1 then ccc.UnitsEarned else 0 end),
				y1_grade_points = sum(case when cccg.gpa_ind = 1 then ccc.UnitsAttempted * cccg.points else 0 end),
				y1_math_units_6plus = case when sum(case when ccc.TopCode = '170100' and cccg.success_ind = 1 then ccc.UnitsAttempted end) >= 6 then 1 else 0 end,
				y1_engl_units_6plus = case when sum(case when ccc.TopCode = '150100' and cccg.success_ind = 1 then ccc.UnitsAttempted end) >= 6 then 1 else 0 end,
				y1_passed_math_college_level = max(case when ccc.TopCode = '170100' and ccc.CollegeLevel = 'Y' and cccg.success_ind = 1 then 1 else 0 end),
				y1_passed_engl_college_level = max(case when ccc.TopCode = '150100' and ccc.CollegeLevel = 'Y' and cccg.success_ind = 1 then 1 else 0 end),
				y1_passed_esl_college_level = max(case when ccc.TopCode in ('493084','493085','493086','493087') and ccc.CollegeLevel = 'Y' and cccg.success_ind = 1 then 1 else 0 end)
			FROM
				calpass.dbo.ccsf_output c
				inner join
				calpass.dbo.CCStudentProd_TermId t1
					on c.matric_ccsf_first_term_id = t1.code
				inner join
				calpass.dbo.CCStudentProd ccs
					on c.derkey1 = ccs.derkey1
				inner join
				calpass.dbo.CCStudentProd_TermId t2
					on ccs.termId = t2.code
				inner join
				calpass.dbo.CCCourseProd ccc
					on ccs.CollegeId = ccc.CollegeId
					and ccs.StudentId = ccc.StudentId
					and ccs.TermId = ccc.TermId
				inner join
				calpass.dbo.CCCourseProd_Grade cccg
					on cccg.code = ccc.grade
			WHERE
				cccg.enroll_ind = 1
				and ccc.creditFlag in ('T', 'D', 'C', 'S')
				and t2.year_term >= t1.year_term
				and t2.year_term <= t1.year_term_01
			GROUP BY
				c.derkey1
		) s
			on t.derkey1 = s.derkey1;

	UPDATE
		t
	SET
		t.t1_persist_1plus = isnull(s.t1_persist_1plus, 0),
		t.t1_persist_2plus = isnull(s.t1_persist_2plus, 0)
	FROM
		calpass.dbo.ccsf_output t
		left outer join
		(
			SELECT
				c.derkey1,
				t1_persist_1plus = max(case when p1.year_term_next_primary = t2.year_term then 1 else 0 end),
				t1_persist_2plus = max(case when p2.year_term_next_primary = t2.year_term then 1 else 0 end)
			FROM
				calpass.dbo.ccsf_output c
				inner join
				calpass.dbo.CCStudentProd_TermId t1
					on c.matric_ccsf_first_term_id = t1.code
				inner join
				calpass.dbo.CCStudentProd ccs
					on c.derkey1 = ccs.derkey1
				inner join
				calpass.dbo.CCStudentProd_TermId t2
					on ccs.termId = t2.code
				inner join
				calpass.dbo.CCStudentProd_TermId p1
					on p1.year_term = t1.year_term
				left outer join
				calpass.dbo.CCStudentProd_TermId p2
					on p2.year_term = p1.year_term_next_primary
			WHERE
				exists (
					SELECT
						1
					FROM
						calpass.dbo.CCCourseProd ccc
						inner join
						calpass.dbo.CCCourseProd_Grade cccg
							on cccg.code = ccc.grade
					WHERE
						ccc.creditFlag in ('T', 'D', 'C', 'S')
						and cccg.enroll_ind = 1
						and ccs.CollegeId = ccc.CollegeId
						and ccs.StudentId = ccc.StudentId
						and ccs.TermId = ccc.TermId
				)
				and t2.year_term >= t1.year_term
				and t2.year_term <= t1.year_term_02
			GROUP BY
				c.derkey1
		) s
			on t.derkey1 = s.derkey1;

	UPDATE
		t
	SET
		t.y2_passed_math_college_level = isnull(s.y2_passed_math_college_level, 0),
		t.y2_survived_math_college_level = isnull(s.y2_survived_math_college_level, 0),
		t.y2_passed_engl_college_level = isnull(s.y2_passed_engl_college_level, 0),
		t.y2_survived_engl_college_level = isnull(s.y2_survived_engl_college_level, 0),
		t.y2_passed_esl_college_level = isnull(s.y2_passed_esl_college_level, 0),
		t.y2_survived_esl_college_level = isnull(s.y2_survived_esl_college_level, 0)
	FROM
		calpass.dbo.ccsf_output t
		left outer join
		(
			SELECT
				c.derkey1,
				y2_passed_math_college_level = max(case when ccc.TopCode = '170100' and ccc.CollegeLevel = 'Y' and cccg.success_ind = 1 then 1 else 0 end),
				y2_survived_math_college_level = case
						when max(case when ccc.TopCode = '170100' and ccc.CollegeLevel != 'Y' then 1 else 0 end) = 1
							and max(case when ccc.TopCode = '170100' and ccc.CollegeLevel = 'Y' then 1 else 0 end) = 1 then 1
						else 0
					end,
				y2_passed_engl_college_level = max(case when ccc.TopCode = '150100' and ccc.CollegeLevel = 'Y' and cccg.success_ind = 1 then 1 else 0 end),
				y2_survived_engl_college_level = case
						when max(case when ccc.TopCode = '150100' and ccc.CollegeLevel != 'Y' then 1 else 0 end) = 1
							and max(case when ccc.TopCode = '150100' and ccc.CollegeLevel = 'Y' then 1 else 0 end) = 1 then 1
						else 0
					end,
				y2_passed_esl_college_level = max(case when ccc.TopCode in ('493084','493085','493086','493087') and ccc.CollegeLevel = 'Y' and cccg.success_ind = 1 then 1 else 0 end),
				y2_survived_esl_college_level = case
						when max(case when ccc.TopCode in ('493084','493085','493086','493087') and ccc.CollegeLevel != 'Y' then 1 else 0 end) = 1
							and max(case when ccc.TopCode in ('493084','493085','493086','493087') and ccc.CollegeLevel = 'Y' then 1 else 0 end) = 1 then 1
						else 0
					end
			FROM
				calpass.dbo.ccsf_output c
				inner join
				calpass.dbo.CCStudentProd_TermId t1
					on c.matric_ccsf_first_term_id = t1.code
				inner join
				calpass.dbo.CCStudentProd ccs
					on c.derkey1 = ccs.derkey1
				inner join
				calpass.dbo.CCStudentProd_TermId t2
					on ccs.termId = t2.code
				inner join
				calpass.dbo.CCCourseProd ccc
					on ccs.CollegeId = ccc.CollegeId
					and ccs.StudentId = ccc.StudentId
					and ccs.TermId = ccc.TermId
				inner join
				calpass.dbo.CCCourseProd_Grade cccg
					on cccg.code = ccc.grade
			WHERE
				cccg.enroll_ind = 1
				and ccc.creditFlag in ('T', 'D', 'C', 'S')
				and t2.year_term >= t1.year_term
				and t2.year_term <= t1.year_term_02
			GROUP BY
				c.derkey1
		) s
			on t.derkey1 = s.derkey1;

	UPDATE
		t
	SET
		t.y3_award_code = s.y3_award_code,
		t.y3_award_years = s.y3_award_years
	FROM
		calpass.dbo.ccsf_output t
		left outer join
		(
			SELECT
				d.derkey1,
				y3_award_code = d.Award,
				y3_award_years = d.year_award - year_start
			FROM
				(
					SELECT
						c.derkey1,
						cca.Award,
						year_start = t1.year_trailing,
						year_award = t3.year_trailing,
						row_selector = row_number() over(partition by c.derkey1 order by t3.year_trailing asc, cca.RecordId asc)
					FROM
						calpass.dbo.ccsf_output c
						inner join
						calpass.dbo.CCStudentProd_TermId t1
							on c.matric_ccsf_first_term_id = t1.code
						inner join
						calpass.dbo.CCStudentProd ccs
							on c.derkey1 = ccs.derkey1
						inner join
						calpass.dbo.CCStudentProd_TermId t2
							on ccs.termId = t2.code
						inner join
						calpass.dbo.CCAwardProd cca
							on ccs.CollegeId = cca.CollegeId
							and ccs.StudentId = cca.StudentId
						inner join
						calpass.dbo.CCStudentProd_TermId t3
							on t3.code = cca.TermId
							and t3.year_trailing <= t2.year_trailing
							and t3.year_trailing >= t1.year_trailing
					WHERE
						t2.year_term >= t1.year_term
						and t2.year_term <= t1.year_term_03
						and cca.Award in ('A', 'S', 'E', 'B', 'L', 'T', 'F', 'O')
				) d
			WHERE
				row_selector = 1
		) s
			on t.derkey1 = s.derkey1;
END;