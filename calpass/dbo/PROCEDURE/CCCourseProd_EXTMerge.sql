USE calpass;

GO

IF (object_id('dbo.CCCourseProd_EXTMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.CCCourseProd_EXTMerge;
	END;

GO

CREATE PROCEDURE
	dbo.CCCourseProd_EXTMerge
	(
		@CCCourseProdPlus dbo.CCCourseProdPlus READONLY
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Severity tinyint = 0,
	@State tinyint = 1,
	@RowCount varchar(2048),
	@Error varchar(2048);

BEGIN
	MERGE
		dbo.CCCourseProd_EXT t
	USING
		@CCCourseProdPlus s
	ON
		t.CollegeId = s.CollegeId
		and t.StudentId = s.StudentId
		and t.IdStatus = s.IdStatus
		and t.TermId = s.TermId
		and t.CourseId = s.CourseId
		and t.SectionId = s.SectionId
	WHEN MATCHED THEN
		UPDATE SET
			t.StudentComisId = s.StudentComisId,
			t.extSX05_PositiveAttendanceHours = s.extSX05_PositiveAttendanceHours,
			t.extSXD4_TotalHours = s.extSXD4_TotalHours,
			t.extXF01_SessionInstuctionMethod1 = s.extXF01_SessionInstuctionMethod1,
			t.extXF01_SessionInstuctionMethod2 = s.extXF01_SessionInstuctionMethod2,
			t.extXF01_SessionInstuctionMethod3 = s.extXF01_SessionInstuctionMethod3,
			t.extXF01_SessionInstuctionMethod4 = s.extXF01_SessionInstuctionMethod4,
			t.extXBD3_DayEveningClassCode = s.extXBD3_DayEveningClassCode,
			t.extCB00_ControlNumber = s.extCB00_ControlNumber,
			t.extCB04_CreditStatus = s.extCB04_CreditStatus,
			t.extCB10_CoopWorkExpEdStatus = s.extCB10_CoopWorkExpEdStatus,
			t.extCB13_SpecialClassStatus = s.extCB13_SpecialClassStatus,
			t.extCB14_CanCode = s.extCB14_CanCode,
			t.extCB15_CanSeqCode = s.extCB15_CanSeqCode,
			t.extCB19_CrosswalkCrsDeptName = s.extCB19_CrosswalkCrsDeptName,
			t.extCB20_CrosswalkCrsNumber = s.extCB20_CrosswalkCrsNumber,
			t.extCB23_FundingAgencyCategory = s.extCB23_FundingAgencyCategory,
			t.extCB24_ProgramStatus = s.extCB24_ProgramStatus,
			t.extXB01_AccountingMethod = s.extXB01_AccountingMethod
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				CourseId,
				SectionId,
				extSX05_PositiveAttendanceHours,
				extSXD4_TotalHours,
				extXF01_SessionInstuctionMethod1,
				extXF01_SessionInstuctionMethod2,
				extXF01_SessionInstuctionMethod3,
				extXF01_SessionInstuctionMethod4,
				extXBD3_DayEveningClassCode,
				extCB00_ControlNumber,
				extCB04_CreditStatus,
				extCB10_CoopWorkExpEdStatus,
				extCB13_SpecialClassStatus,
				extCB14_CanCode,
				extCB15_CanSeqCode,
				extCB19_CrosswalkCrsDeptName,
				extCB20_CrosswalkCrsNumber,
				extCB23_FundingAgencyCategory,
				extCB24_ProgramStatus,
				extXB01_AccountingMethod
			)
		VALUES
			(
				s.CollegeId,
				s.StudentComisId,
				s.StudentId,
				s.IdStatus,
				s.TermId,
				s.CourseId,
				s.SectionId,
				s.extSX05_PositiveAttendanceHours,
				s.extSXD4_TotalHours,
				s.extXF01_SessionInstuctionMethod1,
				s.extXF01_SessionInstuctionMethod2,
				s.extXF01_SessionInstuctionMethod3,
				s.extXF01_SessionInstuctionMethod4,
				s.extXBD3_DayEveningClassCode,
				s.extCB00_ControlNumber,
				s.extCB04_CreditStatus,
				s.extCB10_CoopWorkExpEdStatus,
				s.extCB13_SpecialClassStatus,
				s.extCB14_CanCode,
				s.extCB15_CanSeqCode,
				s.extCB19_CrosswalkCrsDeptName,
				s.extCB20_CrosswalkCrsNumber,
				s.extCB23_FundingAgencyCategory,
				s.extCB24_ProgramStatus,
				s.extXB01_AccountingMethod
			);
	-- capture
	SET @RowCount = convert(varchar(2048), @@ROWCOUNT);
	-- set
	SET @Error = 'Merged ' + @RowCount + ' dbo.CCCourseProd records';
	-- output
	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;
END;