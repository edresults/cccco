USE calpass;

GO

IF (object_id('dbo.CCAwardProdMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.CCAwardProdMerge;
	END;

GO

CREATE PROCEDURE
	dbo.CCAwardProdMerge
	(
		@CCAwardProd dbo.CCAwardProd READONLY
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Severity tinyint = 0,
	@State tinyint = 1,
	@RowCount varchar(2048),
	@Error varchar(2048);

BEGIN
	MERGE
		dbo.CCAwardProd t
	USING
		@CCAwardProd s
	ON
		t.CollegeId = s.CollegeId
		and t.StudentId = s.StudentId
		and t.IdStatus = s.IdStatus
		and t.TermId = s.TermId
		and t.TopCode = s.TopCode
		and t.Award = s.Award
		and t.ProgramCode = s.ProgramCode
		and t.DateOfAward = s.DateOfAward
		and t.RecordId = s.RecordId
	WHEN MATCHED THEN
		UPDATE SET
			t.StudentComisId = s.StudentComisId
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				CollegeId,
				StudentComisId,
				StudentId,
				IdStatus,
				TermId,
				TopCode,
				Award,
				DateOfAward,
				RecordId,
				ProgramCode
			)
		VALUES
			(
				s.CollegeId,
				s.StudentComisId,
				s.StudentId,
				s.IdStatus,
				s.TermId,
				s.TopCode,
				s.Award,
				s.DateOfAward,
				s.RecordId,
				s.ProgramCode
			);
	-- capture
	SET @RowCount = convert(varchar(2048), @@ROWCOUNT);
	-- set
	SET @Error = 'Merged ' + @RowCount + ' dbo.CCAwardProd records';
	-- output
	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;
END;