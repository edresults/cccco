USE calpass;

GO

IF (object_id('dbo.TableAssimilatePk') is not null)
	BEGIN
		DROP PROCEDURE dbo.TableAssimilatePk;
	END;

GO

CREATE PROCEDURE
	dbo.TableAssimilatePk
	(
		@ProductionName nvarchar(517),
		@StageName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Message nvarchar(2048),
	@DuplicateConstraintWhere nvarchar(max) = N'',
	@DuplicateConstraintGroup nvarchar(max) = N'',
	@RowCount int,
	@Count int,
	@Sql nvarchar(max);

BEGIN
	-- compare names
	IF (@ProductionName = @StageName)
		BEGIN
			SET @Message = N'Production Table Name and Stage Table Name must not be the same';
			THROW 70099, @Message, 1;
		END;

	-- validate production table
	IF (object_id(@ProductionName) is null)
		BEGIN
			SET @Message = N'Production table not found';
			THROW 70099, @Message, 1;
		END;

	-- validate stage table
	IF (object_id(@StageName) is null)
		BEGIN
			SET @Message = N'Stage table not found';
			THROW 70099, @Message, 1;
		END;
		
	SELECT
		@Count = count(*)
	FROM
		sys.indexes i
		inner join
		sys.index_columns ic
			on i.object_id = ic.object_id
			and i.index_id = ic.index_id
		inner join
		sys.columns c
			on ic.object_id = c.object_id
			and c.column_id = ic.column_id
		inner join
		sys.columns sc
			on sc.name = c.name
	WHERE
		i.object_id = object_id(@ProductionName)
		and i.is_primary_key = 1
		and sc.object_id = object_id(@StageName)
		and c.is_identity = 0;

	IF (@Count = 0)
		BEGIN
			SET @Message = 'Production table and Stage table share no columns with an associated Primary Key';
			THROW 70099, @Message, 1;
		END;
		
	-- collect key columns
	SELECT
		@DuplicateConstraintWhere +=
			case
				when ic.key_ordinal = min(ic.key_ordinal) over() then N''
				else N' and '
			end
			+ N's.' + c.Name + ' = t.' + c.Name,
		@DuplicateConstraintGroup +=
			c.Name + 
			case
				when ic.key_ordinal != max(ic.key_ordinal) over() then N','
				else N''
			end
	FROM
		sys.indexes i
		inner join
		sys.index_columns ic
			on i.object_id = ic.object_id
			and i.index_id = ic.index_id
		inner join
		sys.columns c
			on ic.object_id = c.object_id
			and c.column_id = ic.column_id
		inner join
		sys.columns sc
			on sc.name = c.name
	WHERE
		i.object_id = object_id(@ProductionName)
		and i.is_primary_key = 1
		and sc.object_id = object_id(@StageName)
		and c.is_identity = 0
	ORDER BY
		ic.key_ordinal;

	SET @Sql = N'DELETE t FROM ' + @StageName + N' t WHERE exists (SELECT 1 FROM ' + 
		@StageName + N' s WHERE ' + @DuplicateConstraintWhere + N' GROUP BY ' + @DuplicateConstraintGroup + 
		N' HAVING max(s.SubmissionFileRecordNumber) != t.SubmissionFileRecordNumber); SET @RowCount = @@ROWCOUNT;';
	-- delete duplicates
	EXECUTE sp_executesql
		@Sql,
		N'@RowCount int out',
		@RowCount = @RowCount out;
	-- update message
	SET @Message = convert(nvarchar, @RowCount) + N' Stage Records deleted as Primary Key Violation';
	-- output message
	PRINT @Message;
END;