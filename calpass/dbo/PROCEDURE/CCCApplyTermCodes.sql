SELECT
    Records,
    college_id,
    college_name,
    term_code,
    term_description,
    Term,
    AcademicYear =  case
                        when Term in ('Summer', 'Fall')   then convert(char(4), Year) + '-' + convert(char(4), Year + 1)
                        when Term in ('Winter', 'Spring') then convert(char(4), Year - 1) + '-' + convert(char(4), Year)
                    end
FROM
    (
        SELECT
            Records = count(*),
            college_id,
            college_name,
            term_code,
            term_description,
            Term = coalesce(
                    case when term_description like '%Summer%'       then 'Summer' end,
                    case when term_description like '%Sumner%'       then 'Summer' end,
                    case when term_description like '%Fall%'         then 'Fall'   end,
                    case when term_description like '%Winter%'       then 'Winter' end,
                    case when term_description like '%Intersession%' then 'Winter' end,
                    case when term_description like '%Spring%'       then 'Spring' end,
                    case when term_description like '%Sping%'        then 'Spring' end,
                    -- user transposed term_code and term_description
                    case when term_code        like '%Summer%'       then 'Summer' end,
                    case when term_code        like '%Sumner%'       then 'Summer' end,
                    case when term_code        like '%Fall%'         then 'Fall'   end,
                    case when term_code        like '%Winter%'       then 'Winter' end,
                    case when term_code        like '%Intersession%' then 'Winter' end,
                    case when term_code        like '%Spring%'       then 'Spring' end,
                    case when term_code        like '%Sping%'        then 'Spring' end,
                    -- user using non-typical word boundary
                    case when term_code        like '%SP%'           then 'Spring' end,
                    -- Citrus
                    case when college_id = '821' and term_code = '201420' then 'Fall'   end,
                    case when college_id = '821' and term_code = '201425' then 'Winter' end,
                    case when college_id = '821' and term_code = '201430' then 'Spring' end,
                    case when college_id = '821' and term_code = '201440' then 'Summer' end,
                    -- Irvine Valley
                    case when college_id = '892' and term_code = '20172'  then 'Summer' end
                ),
            Year = coalesce(
                    case when term_description like '%2022%' then 2022 end,
                    case when term_description like '%2021%' then 2021 end,
                    case when term_description like '%2020%' then 2020 end,
                    case when term_description like '%2019%' then 2019 end,
                    case when term_description like '%2018%' then 2018 end,
                    case when term_description like '%2017%' then 2017 end,
                    case when term_description like '%2016%' then 2016 end,
                    case when term_description like '%2015%' then 2015 end,
                    case when term_description like '%2014%' then 2014 end,
                    case when term_description like '%2013%' then 2013 end,
                    case when term_description like '%2012%' then 2012 end,
                    case when term_description like '%2011%' then 2011 end,
                    -- Siskiyous :: Calendar (decrement if winter or spring)
                    case when college_id = '181' and term_code = '202010' then 2020 end,
                    -- De Anza :: Academic Year, Ending
                    case when college_id = '421' and term_code = '201823' then 2018 end,
                    -- Bakersfield :: Calendar Year (decrement if winter or spring)
                    case when college_id = '521' and term_code = '201750' then 2018 end,
                    -- East LA :: Calendar Year (decrement if winter or spring)
                    case when college_id = '748' and term_code = '2152'   then 2015 end,
                    case when college_id = '748' and term_code = '2154'   then 2015 end,
                    -- West LA :: Calendar Year (decrement if winter or spring)
                    case when college_id = '749' and term_code = '2154'   then 2015 end,
                    -- Citrus :: Academic Year, Ending
                    case when college_id = '821' and term_code = '201420' then 2014 end,
                    case when college_id = '821' and term_code = '201425' then 2014 end,
                    case when college_id = '821' and term_code = '201430' then 2014 end,
                    case when college_id = '821' and term_code = '201440' then 2015 end,
                    -- MSAC :: Academic Year, Beginning
                    case when college_id = '851' and term_code = '201720' then 2017 end,
                    -- Rio Hondo :: Calendar Year (decrement if winter or spring)
                    case when college_id = '881' and term_code = '201430' then 2014 end,
                    -- Irvine Valley :: Calendar Year (decrement if winter or spring)
                    case when college_id = '892' and term_code = '20172'  then 2018 end
                )
        FROM
            [PRO-DAT-SQL-03].calpass.COMIS.Application20181211
        WHERE
            substring(college_id, 1, 2) != 'ZZ'
            and term_code not like '%test%'
            and college_name is not null
        GROUP BY
            college_id,
            college_name,
            term_code,
            term_description
    ) a
ORDER BY
    college_id,
    term_code;

/*
+---------+------------+--------------------------------------------+---------------+---------------------------------------------------------------------------------------+--------+--------------+
| Records | college_id |                college_name                |   term_code   |                                   term_description                                    |  Term  | AcademicYear |
+---------+------------+--------------------------------------------+---------------+---------------------------------------------------------------------------------------+--------+--------------+
|    3272 | 021        | Cuyamaca College                           | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|      16 | 021        | Cuyamaca College                           | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
|    1051 | 021        | Cuyamaca College                           | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
|    4590 | 021        | Cuyamaca College                           | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    2833 | 021        | Cuyamaca College                           | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|    1522 | 021        | Cuyamaca College                           | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
|    4148 | 021        | Cuyamaca College                           | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    2733 | 021        | Cuyamaca College                           | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|    1705 | 021        | Cuyamaca College                           | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
|    3668 | 021        | Cuyamaca College                           | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    2744 | 021        | Cuyamaca College                           | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|    1671 | 021        | Cuyamaca College                           | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
|      96 | 021        | Cuyamaca College                           | 2019FA        | Fall 2019                                                                             | Fall   | 2019-2020    |
|    1134 | 021        | Cuyamaca College                           | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|      60 | 021        | Cuyamaca College                           | 2019SU        | Summer 2019                                                                           | Summer | 2019-2020    |
|      12 | 021        | Cuyamaca College                           | SP 18         | Spring 2018                                                                           | Spring | 2017-2018    |
|    8978 | 022        | Grossmont College                          | 2015FA        | Fall 2015 - Grossmont College                                                         | Fall   | 2015-2016    |
|     102 | 022        | Grossmont College                          | 2015SP        | Spring 2015 - Grossmont College                                                       | Spring | 2014-2015    |
|    3703 | 022        | Grossmont College                          | 2015SU        | Summer 2015 - Grossmont College                                                       | Summer | 2015-2016    |
|   10728 | 022        | Grossmont College                          | 2016FA        | Fall 2016 -  Grossmont College                                                        | Fall   | 2016-2017    |
|    6941 | 022        | Grossmont College                          | 2016SP        | Spring 2016 - Grossmont College                                                       | Spring | 2015-2016    |
|    4759 | 022        | Grossmont College                          | 2016SU        | Summer 2016 - Grossmont College                                                       | Summer | 2016-2017    |
|   11254 | 022        | Grossmont College                          | 2017FA        | Fall 2017 - Grossmont College                                                         | Fall   | 2017-2018    |
|    6744 | 022        | Grossmont College                          | 2017SP        | Spring 2017-Grossmont College                                                         | Spring | 2016-2017    |
|    4472 | 022        | Grossmont College                          | 2017SU        | Summer 2017 - Grossmont College                                                       | Summer | 2017-2018    |
|   10697 | 022        | Grossmont College                          | 2018FA        | Fall 2018 - Grossmont College                                                         | Fall   | 2018-2019    |
|    6137 | 022        | Grossmont College                          | 2018SP        | Spring 2018-Grossmont College                                                         | Spring | 2017-2018    |
|    4670 | 022        | Grossmont College                          | 2018SU        | Summer 2018 - Grossmont College                                                       | Summer | 2018-2019    |
|     293 | 022        | Grossmont College                          | 2019FA        | Fall 2019 - Grossmont College                                                         | Fall   | 2019-2020    |
|    2878 | 022        | Grossmont College                          | 2019SP        | Spring 2019-Grossmont College                                                         | Spring | 2018-2019    |
|      80 | 022        | Grossmont College                          | 2019SU        | Summer 2019 - Grossmont College                                                       | Summer | 2019-2020    |
|    5121 | 031        | Imperial Valley College                    | 2015SF        | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
|    1408 | 031        | Imperial Valley College                    | 2015WS        | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
|    6770 | 031        | Imperial Valley College                    | 2016SF        | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
|    3197 | 031        | Imperial Valley College                    | 2016WS        | 2016 Winter/Spring                                                                    | Winter | 2015-2016    |
|    8155 | 031        | Imperial Valley College                    | 2017SF        | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
|    3129 | 031        | Imperial Valley College                    | 2017WS        | 2017 Winter/Spring                                                                    | Winter | 2016-2017    |
|    8430 | 031        | Imperial Valley College                    | 2018SF        | 2018 Summer/Fall                                                                      | Summer | 2018-2019    |
|    3404 | 031        | Imperial Valley College                    | 2018WS        | 2018 Winter/Spring                                                                    | Winter | 2017-2018    |
|    2494 | 031        | Imperial Valley College                    | 2019SF        | 2019 Summer/Fall                                                                      | Summer | 2019-2020    |
|    2020 | 031        | Imperial Valley College                    | 2019WS        | 2019 Winter Spring                                                                    | Winter | 2018-2019    |
|    2172 | 051        | MiraCosta College                          | 20140602      | Summer 2014 - MiraCosta College                                                       | Summer | 2014-2015    |
|    8585 | 051        | MiraCosta College                          | 20140818      | Fall 2014 - MiraCosta College                                                         | Fall   | 2014-2015    |
|    8461 | 051        | MiraCosta College                          | 20150120      | Spring 2015 - MiraCosta College                                                       | Spring | 2014-2015    |
|    6737 | 051        | MiraCosta College                          | 20150615      | Summer 2015 - MiraCosta College                                                       | Summer | 2015-2016    |
|   11170 | 051        | MiraCosta College                          | 20150824      | Fall 2015 - MiraCosta College                                                         | Fall   | 2015-2016    |
|    6874 | 051        | MiraCosta College                          | 20160125      | Spring 2016 - MiraCosta College                                                       | Spring | 2015-2016    |
|    5829 | 051        | MiraCosta College                          | 20160613      | Summer 2016 - MiraCosta College                                                       | Summer | 2016-2017    |
|   10129 | 051        | MiraCosta College                          | 20160822      | Fall 2016 - MiraCosta College                                                         | Fall   | 2016-2017    |
|    6971 | 051        | MiraCosta College                          | 20170123      | Spring 2017 - MiraCosta College                                                       | Spring | 2016-2017    |
|    5910 | 051        | MiraCosta College                          | 20170612      | Summer 2017 - MiraCosta College                                                       | Summer | 2017-2018    |
|   10374 | 051        | MiraCosta College                          | 20170821      | Fall 2017 - MiraCosta College                                                         | Fall   | 2017-2018    |
|    6113 | 051        | MiraCosta College                          | 20180122      | Spring 2018 - MiraCosta College                                                       | Spring | 2017-2018    |
|    5509 | 051        | MiraCosta College                          | 20180611      | Summer 2018 - MiraCosta College                                                       | Summer | 2018-2019    |
|    8790 | 051        | MiraCosta College                          | 20180820      | Fall 2018 - MiraCosta College                                                         | Fall   | 2018-2019    |
|    3736 | 051        | MiraCosta College                          | 20190122      | Spring 2019 - MiraCosta College                                                       | Spring | 2018-2019    |
|       1 | 061        | Palomar College                            | 2173          | 2017 Spring                                                                           | Spring | 2016-2017    |
|       5 | 061        | Palomar College                            | 2185          | Summer 2018                                                                           | Summer | 2018-2019    |
|    1824 | 061        | Palomar College                            | 2187          | Fall 2018                                                                             | Fall   | 2018-2019    |
|    6888 | 061        | Palomar College                            | 2193          | Spring 2019                                                                           | Spring | 2018-2019    |
|     204 | 061        | Palomar College                            | 2195          | Summer 2019                                                                           | Summer | 2019-2020    |
|    1693 | 061        | Palomar College                            | 2197          | Fall 2019                                                                             | Fall   | 2019-2020    |
|    5366 | 091        | Southwestern College                       | 14/FA         | Fall 2014                                                                             | Fall   | 2014-2015    |
|    9865 | 091        | Southwestern College                       | 14/SU         | Summer 2014 and Fall 2014                                                             | Summer | 2014-2015    |
|      66 | 091        | Southwestern College                       | 15/FA         | Fall 2015                                                                             | Fall   | 2015-2016    |
|   10047 | 091        | Southwestern College                       | 15/FA         | Fall 2015 Only                                                                        | Fall   | 2015-2016    |
|    8504 | 091        | Southwestern College                       | 15/SP         | Spring 2015                                                                           | Spring | 2014-2015    |
|    8044 | 091        | Southwestern College                       | 15/SU         | Summer 2015 and Fall 2015                                                             | Summer | 2015-2016    |
|      71 | 091        | Southwestern College                       | 15/SU         | Summer 2015 and Fall 2015 (06/08/15-08/06/15)                                         | Summer | 2015-2016    |
|      90 | 091        | Southwestern College                       | 15/SU         | Summer 2015 and Fall 2015 (06/08/15-12/12/15)                                         | Summer | 2015-2016    |
|   12062 | 091        | Southwestern College                       | 16/FA         | Fall 2016                                                                             | Fall   | 2016-2017    |
|    9520 | 091        | Southwestern College                       | 16/SP         | Spring 2016                                                                           | Spring | 2015-2016    |
|    5825 | 091        | Southwestern College                       | 16/SU         | Summer 2016 & Fall 2016                                                               | Summer | 2016-2017    |
|    9688 | 091        | Southwestern College                       | 17/FA         | Fall 2017                                                                             | Fall   | 2017-2018    |
|    8535 | 091        | Southwestern College                       | 17/SP         | Spring 2017                                                                           | Spring | 2016-2017    |
|    7917 | 091        | Southwestern College                       | 17/SU         | Summer 2017 & Fall 2017                                                               | Summer | 2017-2018    |
|    6099 | 091        | Southwestern College                       | 18/FA         | Fall 2018                                                                             | Fall   | 2018-2019    |
|    9074 | 091        | Southwestern College                       | 18/SP         | Spring 2018                                                                           | Spring | 2017-2018    |
|   13466 | 091        | Southwestern College                       | 18/SU         | Summer 2018 & Fall 2018                                                               | Summer | 2018-2019    |
|      28 | 091        | Southwestern College                       | 19/FA         | Fall 2019                                                                             | Fall   | 2019-2020    |
|    4283 | 091        | Southwestern College                       | 19/SP         | Spring 2019                                                                           | Spring | 2018-2019    |
|       5 | 091        | Southwestern College                       | 19/SU         | Summer 2019 & Fall 2019                                                               | Summer | 2019-2020    |
|    3064 | 091        | Southwestern College                       | 19/SU         | Summer 2019 / Fall 2019                                                               | Summer | 2019-2020    |
|       4 | 111        | Butte College                              | 11113         | Summer 2014                                                                           | Summer | 2014-2015    |
|       2 | 111        | Butte College                              | 11114         | Fall 2014                                                                             | Fall   | 2014-2015    |
|    2887 | 111        | Butte College                              | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
|      96 | 111        | Butte College                              | 2014SU        | Summer 2014                                                                           | Summer | 2014-2015    |
|    7328 | 111        | Butte College                              | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|     405 | 111        | Butte College                              | 2015IW        | Winter 2015                                                                           | Winter | 2014-2015    |
|    2247 | 111        | Butte College                              | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
|    1723 | 111        | Butte College                              | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
|    7302 | 111        | Butte College                              | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|     719 | 111        | Butte College                              | 2016IW        | Winter 2016                                                                           | Winter | 2015-2016    |
|    2318 | 111        | Butte College                              | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|    1556 | 111        | Butte College                              | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
|    8273 | 111        | Butte College                              | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|     925 | 111        | Butte College                              | 2017IW        | Winter 2017                                                                           | Winter | 2016-2017    |
|    3775 | 111        | Butte College                              | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|    2317 | 111        | Butte College                              | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
|    8405 | 111        | Butte College                              | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|     914 | 111        | Butte College                              | 2018IW        | Winter 2018                                                                           | Winter | 2017-2018    |
|    3899 | 111        | Butte College                              | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|    2291 | 111        | Butte College                              | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
|     593 | 111        | Butte College                              | 2019FA        | Fall 2019                                                                             | Fall   | 2019-2020    |
|     737 | 111        | Butte College                              | 2019IW        | Winter 2019                                                                           | Winter | 2018-2019    |
|    1798 | 111        | Butte College                              | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|      54 | 111        | Butte College                              | 2019SU        | Summer 2019                                                                           | Summer | 2019-2020    |
|     359 | 131        | Lassen Community College                   | 2013FA        | Fall 2013                                                                             | Fall   | 2013-2014    |
|     924 | 131        | Lassen Community College                   | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
|     802 | 131        | Lassen Community College                   | 2014SP        | Spring 2014                                                                           | Spring | 2013-2014    |
|     399 | 131        | Lassen Community College                   | 2014SU        | Summer 2014                                                                           | Summer | 2014-2015    |
|     779 | 131        | Lassen Community College                   | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|     477 | 131        | Lassen Community College                   | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
|     290 | 131        | Lassen Community College                   | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
|     661 | 131        | Lassen Community College                   | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|     469 | 131        | Lassen Community College                   | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|     345 | 131        | Lassen Community College                   | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
|     879 | 131        | Lassen Community College                   | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|     335 | 131        | Lassen Community College                   | 2017SP        | Sping 2017                                                                            | Spring | 2016-2017    |
|     175 | 131        | Lassen Community College                   | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|       8 | 131        | Lassen Community College                   | 2017SU        | Summer2017                                                                            | Summer | 2017-2018    |
|     384 | 131        | Lassen Community College                   | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
|     944 | 131        | Lassen Community College                   | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|     721 | 131        | Lassen Community College                   | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|     341 | 131        | Lassen Community College                   | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
|     283 | 131        | Lassen Community College                   | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|    1775 | 141        | Mendocino College                          | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|       6 | 141        | Mendocino College                          | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
|     829 | 141        | Mendocino College                          | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
|    1676 | 141        | Mendocino College                          | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    1185 | 141        | Mendocino College                          | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|    1049 | 141        | Mendocino College                          | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
|    1647 | 141        | Mendocino College                          | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    1393 | 141        | Mendocino College                          | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|     978 | 141        | Mendocino College                          | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
|    1681 | 141        | Mendocino College                          | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1337 | 141        | Mendocino College                          | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|     856 | 141        | Mendocino College                          | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
|     449 | 141        | Mendocino College                          | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|    1611 | 161        | College of the Redwoods                    | 2014F         | Fall 2014                                                                             | Fall   | 2014-2015    |
|      36 | 161        | College of the Redwoods                    | 2014X         | Summer 2014                                                                           | Summer | 2014-2015    |
|    3627 | 161        | College of the Redwoods                    | 2015F         | Fall 2015                                                                             | Fall   | 2015-2016    |
|    1529 | 161        | College of the Redwoods                    | 2015S         | Spring 2015                                                                           | Spring | 2014-2015    |
|     980 | 161        | College of the Redwoods                    | 2015X         | Summer 2015                                                                           | Summer | 2015-2016    |
|    4126 | 161        | College of the Redwoods                    | 2016F         | Fall 2016                                                                             | Fall   | 2016-2017    |
|    1822 | 161        | College of the Redwoods                    | 2016S         | Spring 2016                                                                           | Spring | 2015-2016    |
|    1022 | 161        | College of the Redwoods                    | 2016X         | Summer 2016                                                                           | Summer | 2016-2017    |
|    3817 | 161        | College of the Redwoods                    | 2017F         | Fall 2017                                                                             | Fall   | 2017-2018    |
|    2311 | 161        | College of the Redwoods                    | 2017S         | Spring 2017                                                                           | Spring | 2016-2017    |
|    1210 | 161        | College of the Redwoods                    | 2017X         | Summer 2017                                                                           | Summer | 2017-2018    |
|    4123 | 161        | College of the Redwoods                    | 2018F         | Fall 2018                                                                             | Fall   | 2018-2019    |
|    2035 | 161        | College of the Redwoods                    | 2018S         | Spring 2018                                                                           | Spring | 2017-2018    |
|    1203 | 161        | College of the Redwoods                    | 2018X         | Summer 2018                                                                           | Summer | 2018-2019    |
|      44 | 161        | College of the Redwoods                    | 2019F         | Fall 2019                                                                             | Fall   | 2019-2020    |
|    1381 | 161        | College of the Redwoods                    | 2019S         | Spring 2019                                                                           | Spring | 2018-2019    |
|       6 | 161        | College of the Redwoods                    | 2019X         | Summer 2019                                                                           | Summer | 2019-2020    |
|     153 | 171        | Shasta College                             | 2014F         | Fall 2014                                                                             | Fall   | 2014-2015    |
|       2 | 171        | Shasta College                             | 2014U         | Summer 2014                                                                           | Summer | 2014-2015    |
|    4325 | 171        | Shasta College                             | 2015F         | Fall 2015                                                                             | Fall   | 2015-2016    |
|    1581 | 171        | Shasta College                             | 2015S         | Spring 2015                                                                           | Spring | 2014-2015    |
|    1048 | 171        | Shasta College                             | 2015U         | Summer 2015                                                                           | Summer | 2015-2016    |
|    5294 | 171        | Shasta College                             | 2016F         | Fall 2016                                                                             | Fall   | 2016-2017    |
|    1367 | 171        | Shasta College                             | 2016S         | Spring 2016                                                                           | Spring | 2015-2016    |
|    1067 | 171        | Shasta College                             | 2016U         | Summer 2016                                                                           | Summer | 2016-2017    |
|    5292 | 171        | Shasta College                             | 2017F         | Fall 2017                                                                             | Fall   | 2017-2018    |
|    2277 | 171        | Shasta College                             | 2017S         | Spring 2017                                                                           | Spring | 2016-2017    |
|    1330 | 171        | Shasta College                             | 2017U         | Summer 2017                                                                           | Summer | 2017-2018    |
|    5062 | 171        | Shasta College                             | 2018F         | Fall 2018                                                                             | Fall   | 2018-2019    |
|    2578 | 171        | Shasta College                             | 2018S         | Spring 2018                                                                           | Spring | 2017-2018    |
|    1464 | 171        | Shasta College                             | 2018U         | Summer 2018                                                                           | Summer | 2018-2019    |
|     249 | 171        | Shasta College                             | 2019F         | Fall 2019                                                                             | Fall   | 2019-2020    |
|     992 | 171        | Shasta College                             | 2019S         | Spring 2019                                                                           | Spring | 2018-2019    |
|      34 | 171        | Shasta College                             | 2019U         | Summer 2019                                                                           | Summer | 2019-2020    |
|      55 | 181        | College of the Siskiyous                   | 201630        | Spring 2016                                                                           | Spring | 2015-2016    |
|     280 | 181        | College of the Siskiyous                   | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
|    1132 | 181        | College of the Siskiyous                   | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
|     590 | 181        | College of the Siskiyous                   | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
|     333 | 181        | College of the Siskiyous                   | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
|    1425 | 181        | College of the Siskiyous                   | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
|      99 | 181        | College of the Siskiyous                   | 201810        | Winter 2018                                                                           | Winter | 2017-2018    |
|     736 | 181        | College of the Siskiyous                   | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
|     418 | 181        | College of the Siskiyous                   | 201850        | Summer 2018                                                                           | Summer | 2018-2019    |
|    1459 | 181        | College of the Siskiyous                   | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
|     179 | 181        | College of the Siskiyous                   | 201910        | Winter 2019                                                                           | Winter | 2018-2019    |
|     332 | 181        | College of the Siskiyous                   | 201930        | Spring 2019                                                                           | Spring | 2018-2019    |
|      20 | 181        | College of the Siskiyous                   | 201950        | Summer 2019                                                                           | Summer | 2019-2020    |
|     102 | 181        | College of the Siskiyous                   | 201970        | Fall 2019                                                                             | Fall   | 2019-2020    |
|       3 | 181        | College of the Siskiyous                   | 202010        | Winter Intersession                                                                   | Winter | 2019-2020    |
|       6 | 181        | College of the Siskiyous                   | 202030        | Spring 2020                                                                           | Spring | 2019-2020    |
|    1720 | 221        | Lake Tahoe Community College               | 2014FA        | Fall 2014 Quarter                                                                     | Fall   | 2014-2015    |
|     846 | 221        | Lake Tahoe Community College               | 2014SP        | Spring 2014 Quarter                                                                   | Spring | 2013-2014    |
|    1535 | 221        | Lake Tahoe Community College               | 2014SU        | Summer 2014 Session                                                                   | Summer | 2014-2015    |
|      49 | 221        | Lake Tahoe Community College               | 2014WI        | Winter 2014 Quarter                                                                   | Winter | 2013-2014    |
|    1827 | 221        | Lake Tahoe Community College               | 2015FA        | Fall 2015 Quarter                                                                     | Fall   | 2015-2016    |
|    1007 | 221        | Lake Tahoe Community College               | 2015SP        | Spring 2015 Quarter                                                                   | Spring | 2014-2015    |
|    1550 | 221        | Lake Tahoe Community College               | 2015SU        | Summer 2015 Quarter                                                                   | Summer | 2015-2016    |
|    1152 | 221        | Lake Tahoe Community College               | 2015WI        | Winter 2015 Quarter                                                                   | Winter | 2014-2015    |
|    1665 | 221        | Lake Tahoe Community College               | 2016FA        | Fall 2016 Quarter                                                                     | Fall   | 2016-2017    |
|    1012 | 221        | Lake Tahoe Community College               | 2016SP        | Spring 2016 Quarter                                                                   | Spring | 2015-2016    |
|    1345 | 221        | Lake Tahoe Community College               | 2016SU        | Summer 2016 Quarter                                                                   | Summer | 2016-2017    |
|    1280 | 221        | Lake Tahoe Community College               | 2016WI        | Winter 2016 Quarter                                                                   | Winter | 2015-2016    |
|    1805 | 221        | Lake Tahoe Community College               | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    1106 | 221        | Lake Tahoe Community College               | 2017SP        | Spring 2017 Quarter                                                                   | Spring | 2016-2017    |
|    1673 | 221        | Lake Tahoe Community College               | 2017SU        | Summer 2017 Quarter                                                                   | Summer | 2017-2018    |
|    1166 | 221        | Lake Tahoe Community College               | 2017WI        | Winter 2017 Quarter                                                                   | Winter | 2016-2017    |
|    2059 | 221        | Lake Tahoe Community College               | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1214 | 221        | Lake Tahoe Community College               | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|    1730 | 221        | Lake Tahoe Community College               | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
|    1459 | 221        | Lake Tahoe Community College               | 2018WI        | Winter 2018                                                                           | Winter | 2017-2018    |
|      48 | 221        | Lake Tahoe Community College               | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|     886 | 221        | Lake Tahoe Community College               | 2019WI        | Winter 2019                                                                           | Winter | 2018-2019    |
|    1469 | 231        | American River College                     | 1139          | Fall 2013                                                                             | Fall   | 2013-2014    |
|   12170 | 231        | American River College                     | 1143          | Spring 2014                                                                           | Spring | 2013-2014    |
|    6476 | 231        | American River College                     | 1146          | Summer 2014                                                                           | Summer | 2014-2015    |
|   15579 | 231        | American River College                     | 1149          | Fall 2014                                                                             | Fall   | 2014-2015    |
|   10753 | 231        | American River College                     | 1153          | Spring 2015                                                                           | Spring | 2014-2015    |
|    6232 | 231        | American River College                     | 1156          | Summer 2015                                                                           | Summer | 2015-2016    |
|   15928 | 231        | American River College                     | 1159          | Fall 2015                                                                             | Fall   | 2015-2016    |
|   11113 | 231        | American River College                     | 1163          | Spring 2016                                                                           | Spring | 2015-2016    |
|    7208 | 231        | American River College                     | 1166          | Summer 2016                                                                           | Summer | 2016-2017    |
|   15921 | 231        | American River College                     | 1169          | Fall 2016                                                                             | Fall   | 2016-2017    |
|   12163 | 231        | American River College                     | 1173          | Spring 2017                                                                           | Spring | 2016-2017    |
|    8064 | 231        | American River College                     | 1176          | Summer 2017                                                                           | Summer | 2017-2018    |
|   20741 | 231        | American River College                     | 1179          | Fall 2017                                                                             | Fall   | 2017-2018    |
|   17848 | 231        | American River College                     | 1183          | Spring 2018                                                                           | Spring | 2017-2018    |
|   10961 | 231        | American River College                     | 1186          | Summer 2018                                                                           | Summer | 2018-2019    |
|   28083 | 231        | American River College                     | 1189          | Fall 2018                                                                             | Fall   | 2018-2019    |
|    6128 | 231        | American River College                     | 1193          | Spring 2019                                                                           | Spring | 2018-2019    |
|     165 | 231        | American River College                     | 1196          | Summer 2019                                                                           | Summer | 2019-2020    |
|     583 | 231        | American River College                     | 1199          | Fall 2019                                                                             | Fall   | 2019-2020    |
|     555 | 232        | Cosumnes River College                     | 1139          | Fall 2013                                                                             | Fall   | 2013-2014    |
|    4697 | 232        | Cosumnes River College                     | 1143          | Spring 2014                                                                           | Spring | 2013-2014    |
|    3561 | 232        | Cosumnes River College                     | 1146          | Summer 2014                                                                           | Summer | 2014-2015    |
|    7243 | 232        | Cosumnes River College                     | 1149          | Fall 2014                                                                             | Fall   | 2014-2015    |
|    4400 | 232        | Cosumnes River College                     | 1153          | Spring 2015                                                                           | Spring | 2014-2015    |
|    3337 | 232        | Cosumnes River College                     | 1156          | Summer 2015                                                                           | Summer | 2015-2016    |
|    8054 | 232        | Cosumnes River College                     | 1159          | Fall 2015                                                                             | Fall   | 2015-2016    |
|    5006 | 232        | Cosumnes River College                     | 1163          | Spring 2016                                                                           | Spring | 2015-2016    |
|    3799 | 232        | Cosumnes River College                     | 1166          | Summer 2016                                                                           | Summer | 2016-2017    |
|    7981 | 232        | Cosumnes River College                     | 1169          | Fall 2016                                                                             | Fall   | 2016-2017    |
|    5283 | 232        | Cosumnes River College                     | 1173          | Spring 2017                                                                           | Spring | 2016-2017    |
|    3667 | 232        | Cosumnes River College                     | 1176          | Summer 2017                                                                           | Summer | 2017-2018    |
|    8886 | 232        | Cosumnes River College                     | 1179          | Fall 2017                                                                             | Fall   | 2017-2018    |
|    5529 | 232        | Cosumnes River College                     | 1183          | Spring 2018                                                                           | Spring | 2017-2018    |
|    4150 | 232        | Cosumnes River College                     | 1186          | Summer 2018                                                                           | Summer | 2018-2019    |
|    8595 | 232        | Cosumnes River College                     | 1189          | Fall 2018                                                                             | Fall   | 2018-2019    |
|    2344 | 232        | Cosumnes River College                     | 1193          | Spring 2019                                                                           | Spring | 2018-2019    |
|      99 | 232        | Cosumnes River College                     | 1196          | Summer 2019                                                                           | Summer | 2019-2020    |
|     859 | 232        | Cosumnes River College                     | 1199          | Fall 2019                                                                             | Fall   | 2019-2020    |
|       1 | 232        | Cosumnes River College                     | 201660        | Summer 2016                                                                           | Summer | 2016-2017    |
|     965 | 233        | Sacramento City College                    | 1139          | Fall 2013                                                                             | Fall   | 2013-2014    |
|    9007 | 233        | Sacramento City College                    | 1143          | Spring 2014                                                                           | Spring | 2013-2014    |
|    5516 | 233        | Sacramento City College                    | 1146          | Summer 2014                                                                           | Summer | 2014-2015    |
|   12526 | 233        | Sacramento City College                    | 1149          | Fall 2014                                                                             | Fall   | 2014-2015    |
|    7975 | 233        | Sacramento City College                    | 1153          | Spring 2015                                                                           | Spring | 2014-2015    |
|    6058 | 233        | Sacramento City College                    | 1156          | Summer 2015                                                                           | Summer | 2015-2016    |
|   12926 | 233        | Sacramento City College                    | 1159          | Fall 2015                                                                             | Fall   | 2015-2016    |
|    7727 | 233        | Sacramento City College                    | 1163          | Spring 2016                                                                           | Spring | 2015-2016    |
|    5817 | 233        | Sacramento City College                    | 1166          | Summer 2016                                                                           | Summer | 2016-2017    |
|   12223 | 233        | Sacramento City College                    | 1169          | Fall 2016                                                                             | Fall   | 2016-2017    |
|   10019 | 233        | Sacramento City College                    | 1173          | Spring 2017                                                                           | Spring | 2016-2017    |
|    6462 | 233        | Sacramento City College                    | 1176          | Summer 2017                                                                           | Summer | 2017-2018    |
|   17507 | 233        | Sacramento City College                    | 1179          | Fall 2017                                                                             | Fall   | 2017-2018    |
|   12534 | 233        | Sacramento City College                    | 1183          | Spring 2018                                                                           | Spring | 2017-2018    |
|    6343 | 233        | Sacramento City College                    | 1186          | Summer 2018                                                                           | Summer | 2018-2019    |
|   14266 | 233        | Sacramento City College                    | 1189          | Fall 2018                                                                             | Fall   | 2018-2019    |
|    3908 | 233        | Sacramento City College                    | 1193          | Spring 2019                                                                           | Spring | 2018-2019    |
|     124 | 233        | Sacramento City College                    | 1196          | Summer 2019                                                                           | Summer | 2019-2020    |
|     947 | 233        | Sacramento City College                    | 1199          | Fall 2019                                                                             | Fall   | 2019-2020    |
|     290 | 234        | Folsom Lake College                        | 1139          | Fall 2013                                                                             | Fall   | 2013-2014    |
|    2963 | 234        | Folsom Lake College                        | 1143          | Spring 2014                                                                           | Spring | 2013-2014    |
|    1640 | 234        | Folsom Lake College                        | 1146          | Summer 2014                                                                           | Summer | 2014-2015    |
|    4844 | 234        | Folsom Lake College                        | 1149          | Fall 2014                                                                             | Fall   | 2014-2015    |
|    2733 | 234        | Folsom Lake College                        | 1153          | Spring 2015                                                                           | Spring | 2014-2015    |
|    1848 | 234        | Folsom Lake College                        | 1156          | Summer 2015                                                                           | Summer | 2015-2016    |
|    5154 | 234        | Folsom Lake College                        | 1159          | Fall 2015                                                                             | Fall   | 2015-2016    |
|    3174 | 234        | Folsom Lake College                        | 1163          | Spring 2016                                                                           | Spring | 2015-2016    |
|    2070 | 234        | Folsom Lake College                        | 1166          | Summer 2016                                                                           | Summer | 2016-2017    |
|    5067 | 234        | Folsom Lake College                        | 1169          | Fall 2016                                                                             | Fall   | 2016-2017    |
|    3630 | 234        | Folsom Lake College                        | 1173          | Spring 2017                                                                           | Spring | 2016-2017    |
|    2487 | 234        | Folsom Lake College                        | 1176          | Summer 2017                                                                           | Summer | 2017-2018    |
|    5635 | 234        | Folsom Lake College                        | 1179          | Fall 2017                                                                             | Fall   | 2017-2018    |
|    5953 | 234        | Folsom Lake College                        | 1183          | Spring 2018                                                                           | Spring | 2017-2018    |
|    2386 | 234        | Folsom Lake College                        | 1186          | Summer 2018                                                                           | Summer | 2018-2019    |
|    5828 | 234        | Folsom Lake College                        | 1189          | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1582 | 234        | Folsom Lake College                        | 1193          | Spring 2019                                                                           | Spring | 2018-2019    |
|      56 | 234        | Folsom Lake College                        | 1196          | Summer 2019                                                                           | Summer | 2019-2020    |
|     272 | 234        | Folsom Lake College                        | 1199          | Fall 2019                                                                             | Fall   | 2019-2020    |
|    3941 | 241        | Napa Valley College                        | 15/FA         | Fall 2015                                                                             | Fall   | 2015-2016    |
|    1616 | 241        | Napa Valley College                        | 15/SP         | Spring 2015                                                                           | Spring | 2014-2015    |
|    2212 | 241        | Napa Valley College                        | 15/SU         | Summer 2015                                                                           | Summer | 2015-2016    |
|    3973 | 241        | Napa Valley College                        | 16/FA         | Fall 2016                                                                             | Fall   | 2016-2017    |
|    2919 | 241        | Napa Valley College                        | 16/SP         | Spring 2016                                                                           | Spring | 2015-2016    |
|    2006 | 241        | Napa Valley College                        | 16/SU         | Summer 2016                                                                           | Summer | 2016-2017    |
|    3774 | 241        | Napa Valley College                        | 17/FA         | Fall 2017                                                                             | Fall   | 2017-2018    |
|    3079 | 241        | Napa Valley College                        | 17/SP         | Spring 2017                                                                           | Spring | 2016-2017    |
|    2107 | 241        | Napa Valley College                        | 17/SU         | Summer 2017                                                                           | Summer | 2017-2018    |
|    3847 | 241        | Napa Valley College                        | 18/FA         | Fall 2018                                                                             | Fall   | 2018-2019    |
|    2709 | 241        | Napa Valley College                        | 18/SP         | Spring 2018                                                                           | Spring | 2017-2018    |
|    2044 | 241        | Napa Valley College                        | 18/SU         | Summer 2018                                                                           | Summer | 2018-2019    |
|    1321 | 241        | Napa Valley College                        | 19/SP         | Spring 2019                                                                           | Spring | 2018-2019    |
|       1 | 261        | Santa Rosa Junior College                  | 20133         | Sping 2013                                                                            | Spring | 2012-2013    |
|     732 | 261        | Santa Rosa Junior College                  | 20133         | Spring 2013                                                                           | Spring | 2012-2013    |
|    7035 | 261        | Santa Rosa Junior College                  | 20135         | Summer 2013                                                                           | Summer | 2013-2014    |
|   13665 | 261        | Santa Rosa Junior College                  | 20137         | Fall 2013                                                                             | Fall   | 2013-2014    |
|   12459 | 261        | Santa Rosa Junior College                  | 20143         | Spring 2014                                                                           | Spring | 2013-2014    |
|    8477 | 261        | Santa Rosa Junior College                  | 20145         | Summer 2014                                                                           | Summer | 2014-2015    |
|   13982 | 261        | Santa Rosa Junior College                  | 20147         | Fall 2014                                                                             | Fall   | 2014-2015    |
|   11404 | 261        | Santa Rosa Junior College                  | 20153         | Spring 2015                                                                           | Spring | 2014-2015    |
|    9015 | 261        | Santa Rosa Junior College                  | 20155         | Summer 2015                                                                           | Summer | 2015-2016    |
|   13987 | 261        | Santa Rosa Junior College                  | 20157         | Fall 2015                                                                             | Fall   | 2015-2016    |
|   12301 | 261        | Santa Rosa Junior College                  | 20163         | Spring 2016                                                                           | Spring | 2015-2016    |
|    9307 | 261        | Santa Rosa Junior College                  | 20165         | Summer 2016                                                                           | Summer | 2016-2017    |
|   13571 | 261        | Santa Rosa Junior College                  | 20167         | Fall 2016                                                                             | Fall   | 2016-2017    |
|   11171 | 261        | Santa Rosa Junior College                  | 20173         | Spring 2017                                                                           | Spring | 2016-2017    |
|    8927 | 261        | Santa Rosa Junior College                  | 20175         | Summer 2017                                                                           | Summer | 2017-2018    |
|   14364 | 261        | Santa Rosa Junior College                  | 20177         | Fall 2017                                                                             | Fall   | 2017-2018    |
|   15864 | 261        | Santa Rosa Junior College                  | 20183         | Spring 2018                                                                           | Spring | 2017-2018    |
|    2270 | 261        | Santa Rosa Junior College                  | 20187         | Fall 2018                                                                             | Fall   | 2018-2019    |
|   19106 | 261        | Santa Rosa Junior College                  | 2018XF        | Summer 2018 and Fall 2018                                                             | Summer | 2018-2019    |
|    5847 | 261        | Santa Rosa Junior College                  | 20193         | Spring 2019                                                                           | Spring | 2018-2019    |
|    1490 | 261        | Santa Rosa Junior College                  | 2019XF        | Summer 2019 and Fall 2019                                                             | Summer | 2019-2020    |
|       3 | 261        | Santa Rosa Junior College                  | 26112         | Spring 2012 - Santa Rosa Junior College                                               | Spring | 2011-2012    |
|      10 | 271        | Sierra College                             | 201540        | Spring 2015                                                                           | Spring | 2014-2015    |
|    1178 | 271        | Sierra College                             | 201560        | Summer 2015                                                                           | Summer | 2015-2016    |
|    7173 | 271        | Sierra College                             | 201580        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    9515 | 271        | Sierra College                             | 201640        | Spring 2016                                                                           | Spring | 2015-2016    |
|    6515 | 271        | Sierra College                             | 201660        | Summer 2016                                                                           | Summer | 2016-2017    |
|   14542 | 271        | Sierra College                             | 201680        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    9934 | 271        | Sierra College                             | 201740        | Spring 2017                                                                           | Spring | 2016-2017    |
|    7084 | 271        | Sierra College                             | 201760        | Summer 2017                                                                           | Summer | 2017-2018    |
|   15474 | 271        | Sierra College                             | 201780        | Fall 2017                                                                             | Fall   | 2017-2018    |
|   12037 | 271        | Sierra College                             | 201840        | Spring 2018                                                                           | Spring | 2017-2018    |
|    7729 | 271        | Sierra College                             | 201860        | Summer 2018                                                                           | Summer | 2018-2019    |
|   16794 | 271        | Sierra College                             | 201880        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    8520 | 271        | Sierra College                             | 201940        | Spring 2019                                                                           | Spring | 2018-2019    |
|     356 | 271        | Sierra College                             | 201960        | Summer 2019                                                                           | Summer | 2019-2020    |
|    1191 | 271        | Sierra College                             | 201980        | Fall 2019                                                                             | Fall   | 2019-2020    |
|     313 | 281        | Solano Community College                   | 201410        | Spring 2014                                                                           | Spring | 2013-2014    |
|    4596 | 281        | Solano Community College                   | 201460        | Summer 2014                                                                           | Summer | 2014-2015    |
|    7545 | 281        | Solano Community College                   | 201480        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    6503 | 281        | Solano Community College                   | 201510        | Spring 2015                                                                           | Spring | 2014-2015    |
|    4581 | 281        | Solano Community College                   | 201560        | Summer 2015                                                                           | Summer | 2015-2016    |
|    8554 | 281        | Solano Community College                   | 201580        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    5405 | 281        | Solano Community College                   | 201610        | Spring 2016                                                                           | Spring | 2015-2016    |
|    5308 | 281        | Solano Community College                   | 201660        | Summer 2016                                                                           | Summer | 2016-2017    |
|    8856 | 281        | Solano Community College                   | 201680        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    6123 | 281        | Solano Community College                   | 201710        | Spring 2017                                                                           | Spring | 2016-2017    |
|    4805 | 281        | Solano Community College                   | 201760        | Summer 2017                                                                           | Summer | 2017-2018    |
|    9111 | 281        | Solano Community College                   | 201780        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    7490 | 281        | Solano Community College                   | 201810        | Spring 2018                                                                           | Spring | 2017-2018    |
|   10113 | 281        | Solano Community College                   | 201860        | Summer 2018                                                                           | Summer | 2018-2019    |
|   13913 | 281        | Solano Community College                   | 201880        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    5245 | 281        | Solano Community College                   | 201910        | Spring 2019                                                                           | Spring | 2018-2019    |
|      44 | 281        | Solano Community College                   | Spring 2016   | 201610                                                                                | Spring | 2015-2016    |
|       3 | 291        | Yuba College                               | 2013FA        | Fall 2013                                                                             | Fall   | 2013-2014    |
|    4426 | 291        | Yuba College                               | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    3552 | 291        | Yuba College                               | 2014SP        | Spring 2014                                                                           | Spring | 2013-2014    |
|    2196 | 291        | Yuba College                               | 2014SU        | Summer 2014                                                                           | Summer | 2014-2015    |
|    4628 | 291        | Yuba College                               | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    3221 | 291        | Yuba College                               | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
|    2562 | 291        | Yuba College                               | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
|    4112 | 291        | Yuba College                               | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    3337 | 291        | Yuba College                               | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|    2595 | 291        | Yuba College                               | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
|    4090 | 291        | Yuba College                               | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    3094 | 291        | Yuba College                               | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|    2579 | 291        | Yuba College                               | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
|    4726 | 291        | Yuba College                               | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    3116 | 291        | Yuba College                               | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|    2982 | 291        | Yuba College                               | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
|    1231 | 291        | Yuba College                               | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|       2 | 291        | Yuba College                               | 29111         | Fall 2011 - Yuba College                                                              | Fall   | 2011-2012    |
|       3 | 291        | Yuba College                               | 29112         | Spring 2012 - Yuba College                                                            | Spring | 2011-2012    |
|    1870 | 292        | Woodland Community College                 | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    1620 | 292        | Woodland Community College                 | 2014SP        | Spring 2014                                                                           | Spring | 2013-2014    |
|    1111 | 292        | Woodland Community College                 | 2014SU        | Summer 2014                                                                           | Summer | 2014-2015    |
|    2039 | 292        | Woodland Community College                 | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    1425 | 292        | Woodland Community College                 | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
|    1341 | 292        | Woodland Community College                 | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
|    2915 | 292        | Woodland Community College                 | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    1629 | 292        | Woodland Community College                 | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|    1762 | 292        | Woodland Community College                 | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
|    3163 | 292        | Woodland Community College                 | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    2176 | 292        | Woodland Community College                 | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|    1575 | 292        | Woodland Community College                 | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
|    3108 | 292        | Woodland Community College                 | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    2058 | 292        | Woodland Community College                 | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|    1712 | 292        | Woodland Community College                 | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
|     725 | 292        | Woodland Community College                 | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|      28 | 311        | Contra Costa College                       | 2012FA        | Fall 2012                                                                             | Fall   | 2012-2013    |
|    3680 | 311        | Contra Costa College                       | 2013FA        | Fall 2013                                                                             | Fall   | 2013-2014    |
|    2273 | 311        | Contra Costa College                       | 2013SP        | Spring 2013                                                                           | Spring | 2012-2013    |
|    1643 | 311        | Contra Costa College                       | 2013SU        | Summer 2013                                                                           | Summer | 2013-2014    |
|    4267 | 311        | Contra Costa College                       | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    3590 | 311        | Contra Costa College                       | 2014SP        | Spring 2014                                                                           | Spring | 2013-2014    |
|    2331 | 311        | Contra Costa College                       | 2014SU        | Summer 2014                                                                           | Summer | 2014-2015    |
|    3903 | 311        | Contra Costa College                       | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    3328 | 311        | Contra Costa College                       | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
|    2514 | 311        | Contra Costa College                       | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
|    4228 | 311        | Contra Costa College                       | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    3536 | 311        | Contra Costa College                       | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|    2545 | 311        | Contra Costa College                       | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
|    4217 | 311        | Contra Costa College                       | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    3886 | 311        | Contra Costa College                       | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|    2548 | 311        | Contra Costa College                       | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
|    5220 | 311        | Contra Costa College                       | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    3245 | 311        | Contra Costa College                       | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|    2832 | 311        | Contra Costa College                       | 2018su        | Summer 2018                                                                           | Summer | 2018-2019    |
|     159 | 311        | Contra Costa College                       | 2019FA        | Fall 2019                                                                             | Fall   | 2019-2020    |
|    1834 | 311        | Contra Costa College                       | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|      44 | 311        | Contra Costa College                       | 2019SU        | Sumner 2019                                                                           | Summer | 2019-2020    |
|     109 | 312        | Diablo Valley College                      | 2012FA        | Fall 2012                                                                             | Fall   | 2012-2013    |
|    9987 | 312        | Diablo Valley College                      | 2013FA        | Fall 2013                                                                             | Fall   | 2013-2014    |
|    7554 | 312        | Diablo Valley College                      | 2013SP        | Spring 2013                                                                           | Spring | 2012-2013    |
|    8116 | 312        | Diablo Valley College                      | 2013SU        | Summer 2013                                                                           | Summer | 2013-2014    |
|       1 | 312        | Diablo Valley College                      | 201405        | Summer/Fall 2014                                                                      | Summer | 2014-2015    |
|       4 | 312        | Diablo Valley College                      | 2014FA        | Fall 2013                                                                             | Fall   | 2013-2014    |
|   10150 | 312        | Diablo Valley College                      | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    9563 | 312        | Diablo Valley College                      | 2014SP        | Spring 2014                                                                           | Spring | 2013-2014    |
|    9184 | 312        | Diablo Valley College                      | 2014SU        | Summer 2014                                                                           | Summer | 2014-2015    |
|   10058 | 312        | Diablo Valley College                      | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    8975 | 312        | Diablo Valley College                      | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
|    8376 | 312        | Diablo Valley College                      | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
|    9305 | 312        | Diablo Valley College                      | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    8950 | 312        | Diablo Valley College                      | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|    8245 | 312        | Diablo Valley College                      | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
|   14642 | 312        | Diablo Valley College                      | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|   12587 | 312        | Diablo Valley College                      | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|    8906 | 312        | Diablo Valley College                      | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
|   11622 | 312        | Diablo Valley College                      | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    9304 | 312        | Diablo Valley College                      | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|   10010 | 312        | Diablo Valley College                      | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
|     321 | 312        | Diablo Valley College                      | 2019FA        | Fall 2019                                                                             | Fall   | 2019-2020    |
|    3137 | 312        | Diablo Valley College                      | 2019sp        | Spring 2019                                                                           | Spring | 2018-2019    |
|      80 | 312        | Diablo Valley College                      | 2019SU        | Summer 2019                                                                           | Summer | 2019-2020    |
|      63 | 313        | Los Medanos College                        | 2012FA        | Fall 2012                                                                             | Fall   | 2012-2013    |
|    5974 | 313        | Los Medanos College                        | 2013FA        | Fall 2013                                                                             | Fall   | 2013-2014    |
|    3897 | 313        | Los Medanos College                        | 2013SP        | Spring 2013                                                                           | Spring | 2012-2013    |
|    2896 | 313        | Los Medanos College                        | 2013SU        | Summer 2013                                                                           | Summer | 2013-2014    |
|    5774 | 313        | Los Medanos College                        | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    4415 | 313        | Los Medanos College                        | 2014SP        | Spring 2014                                                                           | Spring | 2013-2014    |
|    2824 | 313        | Los Medanos College                        | 2014SU        | Summer 2014                                                                           | Summer | 2014-2015    |
|    6080 | 313        | Los Medanos College                        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    4096 | 313        | Los Medanos College                        | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
|    2838 | 313        | Los Medanos College                        | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
|    5571 | 313        | Los Medanos College                        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    4242 | 313        | Los Medanos College                        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|    2952 | 313        | Los Medanos College                        | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
|    6705 | 313        | Los Medanos College                        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    4203 | 313        | Los Medanos College                        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|    3315 | 313        | Los Medanos College                        | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
|    7560 | 313        | Los Medanos College                        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    4645 | 313        | Los Medanos College                        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|    3354 | 313        | Los Medanos College                        | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
|     114 | 313        | Los Medanos College                        | 2019FA        | Fall 2019                                                                             | Fall   | 2019-2020    |
|    1837 | 313        | Los Medanos College                        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|      84 | 313        | Los Medanos College                        | 2019SU        | Summer 2019                                                                           | Summer | 2019-2020    |
|      14 | 334        | College of Marin                           | 201460        | Summer 2014 Credit/Noncredit                                                          | Summer | 2014-2015    |
|    2157 | 334        | College of Marin                           | 201480        | Fall 2014 Credit/Noncredit                                                            | Fall   | 2014-2015    |
|    2453 | 334        | College of Marin                           | 201510        | Spring 2015 Credit/Noncredit                                                          | Spring | 2014-2015    |
|    1449 | 334        | College of Marin                           | 201560        | Summer 2015 Credit/Noncredit                                                          | Summer | 2015-2016    |
|    3341 | 334        | College of Marin                           | 201580        | Fall 2015 Credit/Noncredit                                                            | Fall   | 2015-2016    |
|    2894 | 334        | College of Marin                           | 201610        | Spring 2016 Credit/NonCredit                                                          | Spring | 2015-2016    |
|    1524 | 334        | College of Marin                           | 201660        | Summer 2016 Credit/Noncredit                                                          | Summer | 2016-2017    |
|    3679 | 334        | College of Marin                           | 201680        | Fall 2016 Credit/Noncredit                                                            | Fall   | 2016-2017    |
|    2670 | 334        | College of Marin                           | 201710        | Spring 2017 Credit/Noncredit                                                          | Spring | 2016-2017    |
|      65 | 334        | College of Marin                           | 201760        | Summer 2017 Credit/Noncredit                                                          | Summer | 2017-2018    |
|    3185 | 334        | College of Marin                           | 201760        | Summer/Fall 2017 Credit/Noncredit                                                     | Summer | 2017-2018    |
|    1950 | 334        | College of Marin                           | 201780        | Fall 2017 Credit/Non Credit                                                           | Fall   | 2017-2018    |
|    3700 | 334        | College of Marin                           | 201810        | Spring 2018 Credit/Noncredit                                                          | Spring | 2017-2018    |
|    3399 | 334        | College of Marin                           | 201860        | Summer/Fall 2018 Credit/Noncredit                                                     | Summer | 2018-2019    |
|    1685 | 334        | College of Marin                           | 201880        | Fall 2018 Credit/Non Credit                                                           | Fall   | 2018-2019    |
|    1225 | 334        | College of Marin                           | 201910        | Spring 2019 Credit/Noncredit                                                          | Spring | 2018-2019    |
|     172 | 334        | College of Marin                           | 201960        | Summer/Fall 2019 Credit/Non-Credit                                                    | Summer | 2019-2020    |
|     230 | 341        | College of Alameda                         | 1152          | Spring 2015                                                                           | Spring | 2014-2015    |
|     218 | 341        | College of Alameda                         | 1152          | Spring 2015/Spring Intersession 2015                                                  | Winter | 2014-2015    |
|    1664 | 341        | College of Alameda                         | 1153          | Summer 2015                                                                           | Summer | 2015-2016    |
|    2843 | 341        | College of Alameda                         | 1154          | Fall 2015/Winter Intersession 2015                                                    | Fall   | 2015-2016    |
|     163 | 341        | College of Alameda                         | 1154          | Fall 2015                                                                             | Fall   | 2015-2016    |
|    2146 | 341        | College of Alameda                         | 1162          | Spring 2016/Spring Intersession 2016                                                  | Winter | 2015-2016    |
|    1490 | 341        | College of Alameda                         | 1163          | Summer 2016                                                                           | Summer | 2016-2017    |
|    3238 | 341        | College of Alameda                         | 1164          | Fall 2016/Winter Intersession 2016                                                    | Fall   | 2016-2017    |
|    1999 | 341        | College of Alameda                         | 1172          | Spring 2017/Spring Intersession 2017                                                  | Winter | 2016-2017    |
|    1508 | 341        | College of Alameda                         | 1173          | Summer 2017                                                                           | Summer | 2017-2018    |
|    3010 | 341        | College of Alameda                         | 1174          | Fall 2017/Winter Intersession 2017                                                    | Fall   | 2017-2018    |
|    1898 | 341        | College of Alameda                         | 1182          | Spring 2018/Spring Intersession 2018                                                  | Winter | 2017-2018    |
|    1313 | 341        | College of Alameda                         | 1183          | Summer 2018                                                                           | Summer | 2018-2019    |
|    2532 | 341        | College of Alameda                         | 1184          | Fall 2018/Winter Intersession 2018                                                    | Fall   | 2018-2019    |
|     635 | 341        | College of Alameda                         | 1192          | Spring 2019/Spring Intersession 2019                                                  | Winter | 2018-2019    |
|     514 | 343        | Laney College                              | 1152          | Spring 2015                                                                           | Spring | 2014-2015    |
|    3737 | 343        | Laney College                              | 1153          | Summer 2015                                                                           | Summer | 2015-2016    |
|    1824 | 343        | Laney College                              | 1154          | Fall 2015                                                                             | Fall   | 2015-2016    |
|    4360 | 343        | Laney College                              | 1154          | Fall 2015/Winter Intersession 2015                                                    | Fall   | 2015-2016    |
|    5039 | 343        | Laney College                              | 1162          | Spring 2016/Spring Intersession 2016                                                  | Winter | 2015-2016    |
|    3417 | 343        | Laney College                              | 1163          | Summer 2016                                                                           | Summer | 2016-2017    |
|    5622 | 343        | Laney College                              | 1164          | Fall 2016/Winter Intersession 2016                                                    | Fall   | 2016-2017    |
|    5387 | 343        | Laney College                              | 1172          | Spring 2017/Spring Intersession 2017                                                  | Winter | 2016-2017    |
|    3730 | 343        | Laney College                              | 1173          | Summer 2017                                                                           | Summer | 2017-2018    |
|    5676 | 343        | Laney College                              | 1174          | Fall 2017/Winter Intersession 2017                                                    | Fall   | 2017-2018    |
|    4882 | 343        | Laney College                              | 1182          | Spring 2018/Spring Intersession 2018                                                  | Winter | 2017-2018    |
|    3130 | 343        | Laney College                              | 1183          | Summer 2018                                                                           | Summer | 2018-2019    |
|    5482 | 343        | Laney College                              | 1184          | Fall 2018/Winter Intersession 2018                                                    | Fall   | 2018-2019    |
|    1531 | 343        | Laney College                              | 1192          | Spring 2019/Spring Intersession 2019                                                  | Winter | 2018-2019    |
|       1 | 343        | Laney College                              | 34312         | Spring 2012 - Laney College                                                           | Spring | 2011-2012    |
|     433 | 344        | Merritt College                            | 1152          | Spring 2015                                                                           | Spring | 2014-2015    |
|    1820 | 344        | Merritt College                            | 1153          | Summer 2015                                                                           | Summer | 2015-2016    |
|    1283 | 344        | Merritt College                            | 1154          | Fall 2015                                                                             | Fall   | 2015-2016    |
|    2750 | 344        | Merritt College                            | 1154          | Fall 2015/Winter Intersession 2015                                                    | Fall   | 2015-2016    |
|    2852 | 344        | Merritt College                            | 1162          | Spring 2016/Spring Intersession 2016                                                  | Winter | 2015-2016    |
|    1704 | 344        | Merritt College                            | 1163          | Summer 2016                                                                           | Summer | 2016-2017    |
|    3969 | 344        | Merritt College                            | 1164          | Fall 2016/Winter Intersession 2016                                                    | Fall   | 2016-2017    |
|    3247 | 344        | Merritt College                            | 1172          | Spring 2017/Spring Intersession 2017                                                  | Winter | 2016-2017    |
|    2121 | 344        | Merritt College                            | 1173          | Summer 2017                                                                           | Summer | 2017-2018    |
|    4232 | 344        | Merritt College                            | 1174          | Fall 2017/Winter Intersession 2017                                                    | Fall   | 2017-2018    |
|    3314 | 344        | Merritt College                            | 1182          | Spring 2018/Spring Intersession 2018                                                  | Winter | 2017-2018    |
|    1574 | 344        | Merritt College                            | 1183          | Summer 2018                                                                           | Summer | 2018-2019    |
|    3915 | 344        | Merritt College                            | 1184          | Fall 2018/Winter Intersession 2018                                                    | Fall   | 2018-2019    |
|    1025 | 344        | Merritt College                            | 1192          | Spring 2019/Spring Intersession 2019                                                  | Winter | 2018-2019    |
|     321 | 345        | Berkeley City College                      | 1152          | Spring 2015                                                                           | Spring | 2014-2015    |
|    3035 | 345        | Berkeley City College                      | 1153          | Summer 2015                                                                           | Summer | 2015-2016    |
|    1747 | 345        | Berkeley City College                      | 1154          | Fall 2015                                                                             | Fall   | 2015-2016    |
|    3193 | 345        | Berkeley City College                      | 1154          | Fall 2015/Winter Intersession 2015                                                    | Fall   | 2015-2016    |
|    3661 | 345        | Berkeley City College                      | 1162          | Spring 2016/Spring Intersession 2016                                                  | Winter | 2015-2016    |
|    2833 | 345        | Berkeley City College                      | 1163          | Summer 2016                                                                           | Summer | 2016-2017    |
|    4628 | 345        | Berkeley City College                      | 1164          | Fall 2016/Winter Intersession 2016                                                    | Fall   | 2016-2017    |
|    3663 | 345        | Berkeley City College                      | 1172          | Spring 2017/Spring Intersession 2017                                                  | Winter | 2016-2017    |
|    2837 | 345        | Berkeley City College                      | 1173          | Summer 2017                                                                           | Summer | 2017-2018    |
|    4571 | 345        | Berkeley City College                      | 1174          | Fall 2017/Winter Intersession 2017                                                    | Fall   | 2017-2018    |
|    3671 | 345        | Berkeley City College                      | 1182          | Spring 2018/Spring Intersession 2018                                                  | Winter | 2017-2018    |
|    2548 | 345        | Berkeley City College                      | 1183          | Summer 2018                                                                           | Summer | 2018-2019    |
|    7770 | 345        | Berkeley City College                      | 1184          | Fall 2018/Winter Intersession 2018                                                    | Fall   | 2018-2019    |
|    1603 | 345        | Berkeley City College                      | 1192          | Spring 2019/Spring Intersession 2019                                                  | Winter | 2018-2019    |
|       1 | 345        | Berkeley City College                      | 34511         | Fall 2011 - Berkeley City College                                                     | Fall   | 2011-2012    |
|     401 | 361        | City College of San Francisco              | 201570        | Fall 2015                                                                             | Fall   | 2015-2016    |
|   12306 | 361        | City College of San Francisco              | 201630        | Spring 2016                                                                           | Spring | 2015-2016    |
|    8171 | 361        | City College of San Francisco              | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
|   17332 | 361        | City College of San Francisco              | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
|   14255 | 361        | City College of San Francisco              | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
|   17387 | 361        | City College of San Francisco              | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
|   71454 | 361        | City College of San Francisco              | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
|   22595 | 361        | City College of San Francisco              | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
|   15070 | 361        | City College of San Francisco              | 201850        | Summer 2018                                                                           | Summer | 2018-2019    |
|   26275 | 361        | City College of San Francisco              | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    8911 | 361        | City College of San Francisco              | 201930        | Spring 2019                                                                           | Spring | 2018-2019    |
|    1252 | 361        | City College of San Francisco              | 201950        | Summer 2019                                                                           | Summer | 2019-2020    |
|    1053 | 361        | City College of San Francisco              | 201970        | Fall 2019                                                                             | Fall   | 2019-2020    |
|      41 | 371        | Canada College                             | 201308        | Fall 2013                                                                             | Fall   | 2013-2014    |
|    1648 | 371        | Canada College                             | 201403        | Spring 2014                                                                           | Spring | 2013-2014    |
|    3446 | 371        | Canada College                             | 201405        | Summer/Fall 2014                                                                      | Summer | 2014-2015    |
|     508 | 371        | Canada College                             | 201408        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    1956 | 371        | Canada College                             | 201503        | Spring 2015                                                                           | Spring | 2014-2015    |
|    2710 | 371        | Canada College                             | 201505        | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
|    2094 | 371        | Canada College                             | 201508        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    2279 | 371        | Canada College                             | 201603        | Spring 2016                                                                           | Spring | 2015-2016    |
|    9572 | 371        | Canada College                             | 201605        | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
|    4195 | 371        | Canada College                             | 201608        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    3512 | 371        | Canada College                             | 201703        | Spring 2017                                                                           | Spring | 2016-2017    |
|   24897 | 371        | Canada College                             | 201705        | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
|   17521 | 371        | Canada College                             | 201708        | Fall 2017                                                                             | Fall   | 2017-2018    |
|   28441 | 371        | Canada College                             | 201803        | Spring 2018                                                                           | Spring | 2017-2018    |
|   40826 | 371        | Canada College                             | 201805        | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
|   10356 | 371        | Canada College                             | 201808        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    3932 | 371        | Canada College                             | 201903        | Spring 2019                                                                           | Spring | 2018-2019    |
|    2517 | 371        | Canada College                             | 201905        | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
|      21 | 372        | College of San Mateo                       | 201308        | Fall 2013                                                                             | Fall   | 2013-2014    |
|    2852 | 372        | College of San Mateo                       | 201403        | Spring 2014                                                                           | Spring | 2013-2014    |
|    8745 | 372        | College of San Mateo                       | 201405        | Summer/Fall 2014                                                                      | Summer | 2014-2015    |
|     968 | 372        | College of San Mateo                       | 201408        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    3136 | 372        | College of San Mateo                       | 201503        | Spring 2015                                                                           | Spring | 2014-2015    |
|    5769 | 372        | College of San Mateo                       | 201505        | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
|    2953 | 372        | College of San Mateo                       | 201508        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    3097 | 372        | College of San Mateo                       | 201603        | Spring 2016                                                                           | Spring | 2015-2016    |
|    4682 | 372        | College of San Mateo                       | 201605        | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
|    6384 | 372        | College of San Mateo                       | 201608        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    4325 | 372        | College of San Mateo                       | 201703        | Spring 2017                                                                           | Spring | 2016-2017    |
|    7449 | 372        | College of San Mateo                       | 201705        | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
|    2120 | 372        | College of San Mateo                       | 201708        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    3470 | 372        | College of San Mateo                       | 201803        | Spring 2018                                                                           | Spring | 2017-2018    |
|    7800 | 372        | College of San Mateo                       | 201805        | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
|    1222 | 372        | College of San Mateo                       | 201808        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1522 | 372        | College of San Mateo                       | 201903        | Spring 2019                                                                           | Spring | 2018-2019    |
|     213 | 372        | College of San Mateo                       | 201905        | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
|      53 | 373        | Skyline College                            | 201308        | Fall 2013                                                                             | Fall   | 2013-2014    |
|    2972 | 373        | Skyline College                            | 201403        | Spring 2014                                                                           | Spring | 2013-2014    |
|    8620 | 373        | Skyline College                            | 201405        | Summer/Fall 2014                                                                      | Summer | 2014-2015    |
|    1250 | 373        | Skyline College                            | 201408        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    3504 | 373        | Skyline College                            | 201503        | Spring 2015                                                                           | Spring | 2014-2015    |
|     482 | 373        | Skyline College                            | 201505        | Summer 2015                                                                           | Summer | 2015-2016    |
|    5466 | 373        | Skyline College                            | 201505        | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
|    2775 | 373        | Skyline College                            | 201508        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    3135 | 373        | Skyline College                            | 201603        | Spring 2016                                                                           | Spring | 2015-2016    |
|    2106 | 373        | Skyline College                            | 201605        | Summer 2016                                                                           | Summer | 2016-2017    |
|    3527 | 373        | Skyline College                            | 201605        | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
|    2549 | 373        | Skyline College                            | 201608        | Fall 2016                                                                             | Fall   | 2016-2017    |
|     279 | 373        | Skyline College                            | 201608        | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
|    3446 | 373        | Skyline College                            | 201703        | Spring 2017                                                                           | Spring | 2016-2017    |
|    6844 | 373        | Skyline College                            | 201705        | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
|    2160 | 373        | Skyline College                            | 201708        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    4332 | 373        | Skyline College                            | 201803        | Spring 2018                                                                           | Spring | 2017-2018    |
|    6735 | 373        | Skyline College                            | 201805        | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
|    1711 | 373        | Skyline College                            | 201808        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1588 | 373        | Skyline College                            | 201903        | Spring 2019                                                                           | Spring | 2018-2019    |
|     261 | 373        | Skyline College                            | 201905        | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
|       9 | 373        | Skyline College                            | Fall 2017     | Fall 2017                                                                             | Fall   | 2017-2018    |
|     764 | 411        | Cabrillo College                           | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    8245 | 411        | Cabrillo College                           | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    4235 | 411        | Cabrillo College                           | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
|    2761 | 411        | Cabrillo College                           | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
|    8050 | 411        | Cabrillo College                           | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    4440 | 411        | Cabrillo College                           | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|    2758 | 411        | Cabrillo College                           | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
|    8467 | 411        | Cabrillo College                           | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    4611 | 411        | Cabrillo College                           | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|    3220 | 411        | Cabrillo College                           | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
|    8222 | 411        | Cabrillo College                           | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    5060 | 411        | Cabrillo College                           | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|    2974 | 411        | Cabrillo College                           | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
|     764 | 411        | Cabrillo College                           | 2019FA        | Fall 2019                                                                             | Fall   | 2019-2020    |
|    1994 | 411        | Cabrillo College                           | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|      63 | 411        | Cabrillo College                           | 2019SU        | Summer 2019                                                                           | Summer | 2019-2020    |
|      37 | 411        | Cabrillo College                           | 2020SP        | Spring 2020                                                                           | Spring | 2019-2020    |
|       9 | 421        | De Anza College                            | 201512        | Summer 2014                                                                           | Summer | 2014-2015    |
|    2750 | 421        | De Anza College                            | 201522        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    8771 | 421        | De Anza College                            | 201532        | Winter 2015                                                                           | Winter | 2014-2015    |
|    8387 | 421        | De Anza College                            | 201542        | Spring 2015                                                                           | Spring | 2014-2015    |
|   12782 | 421        | De Anza College                            | 201612        | Summer 2015                                                                           | Summer | 2015-2016    |
|   14962 | 421        | De Anza College                            | 201622        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    8543 | 421        | De Anza College                            | 201632        | Winter 2016                                                                           | Winter | 2015-2016    |
|    7534 | 421        | De Anza College                            | 201642        | Spring 2016                                                                           | Spring | 2015-2016    |
|   12517 | 421        | De Anza College                            | 201712        | Summer 2016                                                                           | Summer | 2016-2017    |
|   14904 | 421        | De Anza College                            | 201722        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    8606 | 421        | De Anza College                            | 201732        | Winter 2017                                                                           | Winter | 2016-2017    |
|    7418 | 421        | De Anza College                            | 201742        | Spring 2017                                                                           | Spring | 2016-2017    |
|   10953 | 421        | De Anza College                            | 201812        | Summer 2017                                                                           | Summer | 2017-2018    |
|   13766 | 421        | De Anza College                            | 201822        | Fall 2017                                                                             | Fall   | 2017-2018    |
|      16 | 421        | De Anza College                            | 201823        | Winter 2120                                                                           | Winter | 2017-2018    |
|    8030 | 421        | De Anza College                            | 201832        | Winter 2018                                                                           | Winter | 2017-2018    |
|    6967 | 421        | De Anza College                            | 201842        | Spring 2018                                                                           | Spring | 2017-2018    |
|    9913 | 421        | De Anza College                            | 201912        | Summer 2018                                                                           | Summer | 2018-2019    |
|   14439 | 421        | De Anza College                            | 201922        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    5003 | 421        | De Anza College                            | 201932        | Winter 2019                                                                           | Winter | 2018-2019    |
|      21 | 422        | Foothill College                           | 201511        | 2014 Summer -  Foothill College                                                       | Summer | 2014-2015    |
|    2843 | 422        | Foothill College                           | 201521        | 2014 Fall- Foothill College                                                           | Fall   | 2014-2015    |
|      39 | 422        | Foothill College                           | 201521        | 2014 Fall Apprenticeship Program                                                      | Fall   | 2014-2015    |
|    7654 | 422        | Foothill College                           | 201531        | 2015 Winter - Foothill College                                                        | Winter | 2014-2015    |
|    7926 | 422        | Foothill College                           | 201541        | 2015 Spring - Foothill College                                                        | Spring | 2014-2015    |
|       1 | 422        | Foothill College                           | 201542        | Spring 2015                                                                           | Spring | 2014-2015    |
|   12761 | 422        | Foothill College                           | 201611        | 2015 Summer - Foothill College                                                        | Summer | 2015-2016    |
|   11649 | 422        | Foothill College                           | 201621        | 2015 Fall - Foothill College                                                          | Fall   | 2015-2016    |
|    8929 | 422        | Foothill College                           | 201631        | 2016 Winter - Foothill College                                                        | Winter | 2015-2016    |
|    9038 | 422        | Foothill College                           | 201641        | 2016 Spring - Foothill College                                                        | Spring | 2015-2016    |
|   13834 | 422        | Foothill College                           | 201711        | 2016 Summer - Foothill College                                                        | Summer | 2016-2017    |
|   11963 | 422        | Foothill College                           | 201721        | Fall 2016 - Foothill College                                                          | Fall   | 2016-2017    |
|      14 | 422        | Foothill College                           | 201721        | Fall 2016 Apprenticeship Program - Foothill College                                   | Fall   | 2016-2017    |
|    8527 | 422        | Foothill College                           | 201731        | 2017 Winter - Foothill College                                                        | Winter | 2016-2017    |
|    1383 | 422        | Foothill College                           | 201731        | Winter 2017 - Foothill College                                                        | Winter | 2016-2017    |
|   10829 | 422        | Foothill College                           | 201741        | Spring 2017 - Foothill College                                                        | Spring | 2016-2017    |
|   13841 | 422        | Foothill College                           | 201811        | Summer 2017 - Foothill College                                                        | Summer | 2017-2018    |
|      25 | 422        | Foothill College                           | 201821        | Fall 2017 - SOUTH BAY                                                                 | Fall   | 2017-2018    |
|   12738 | 422        | Foothill College                           | 201821        | Fall 2017 - Foothill College                                                          | Fall   | 2017-2018    |
|   10807 | 422        | Foothill College                           | 201831        | Winter 2018 - Foothill College                                                        | Winter | 2017-2018    |
|      87 | 422        | Foothill College                           | 201831        | Winter 2018 South Bay - Foothill College                                              | Winter | 2017-2018    |
|   10515 | 422        | Foothill College                           | 201841        | Spring 2018 - Foothill College                                                        | Spring | 2017-2018    |
|   14097 | 422        | Foothill College                           | 201911        | Summer 2018 - Foothill College                                                        | Summer | 2018-2019    |
|   12380 | 422        | Foothill College                           | 201921        | Fall 2018 - Foothill College                                                          | Fall   | 2018-2019    |
|    5645 | 422        | Foothill College                           | 201931        | Winter 2019 - Foothill College                                                        | Winter | 2018-2019    |
|    1395 | 431        | Ohlone College                             | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    7861 | 431        | Ohlone College                             | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    4597 | 431        | Ohlone College                             | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
|    3038 | 431        | Ohlone College                             | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
|    8206 | 431        | Ohlone College                             | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    5519 | 431        | Ohlone College                             | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|    3834 | 431        | Ohlone College                             | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
|    8859 | 431        | Ohlone College                             | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    5141 | 431        | Ohlone College                             | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|    4257 | 431        | Ohlone College                             | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
|    9445 | 431        | Ohlone College                             | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    6029 | 431        | Ohlone College                             | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|    4258 | 431        | Ohlone College                             | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
|    2087 | 431        | Ohlone College                             | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|     142 | 441        | Gavilan College                            | 201550        | Summer 2015                                                                           | Summer | 2015-2016    |
|    1436 | 441        | Gavilan College                            | 201570        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    2967 | 441        | Gavilan College                            | 201600        | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
|    1534 | 441        | Gavilan College                            | 201630        | Spring 2016                                                                           | Spring | 2015-2016    |
|     566 | 441        | Gavilan College                            | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
|     844 | 441        | Gavilan College                            | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    5412 | 441        | Gavilan College                            | 201700        | Summer and Fall 2017                                                                  | Summer | 2017-2018    |
|    2344 | 441        | Gavilan College                            | 201730        | Winter Intersession and Spring 2017                                                   | Winter | 2016-2017    |
|       2 | 441        | Gavilan College                            | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
|    1693 | 441        | Gavilan College                            | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    7265 | 441        | Gavilan College                            | 201800        | Summer and Fall 2018                                                                  | Summer | 2018-2019    |
|    2868 | 441        | Gavilan College                            | 201830        | Winter and Spring 2018                                                                | Winter | 2017-2018    |
|    3073 | 441        | Gavilan College                            | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
|     721 | 441        | Gavilan College                            | 201900        | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
|    1403 | 441        | Gavilan College                            | 201930        | Winter Intersession/Spring Semester 2019                                              | Winter | 2018-2019    |
|      39 | 441        | Gavilan College                            | 201950/201970 | Summer and Fall 2019                                                                  | Summer | 2019-2020    |
|      98 | 451        | Hartnell College                           | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    5619 | 451        | Hartnell College                           | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    2065 | 451        | Hartnell College                           | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
|    1873 | 451        | Hartnell College                           | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
|    6191 | 451        | Hartnell College                           | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    3193 | 451        | Hartnell College                           | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|    2246 | 451        | Hartnell College                           | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
|    6395 | 451        | Hartnell College                           | 2017FA        | FAll 2017                                                                             | Fall   | 2017-2018    |
|    3334 | 451        | Hartnell College                           | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|    2232 | 451        | Hartnell College                           | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
|   10151 | 451        | Hartnell College                           | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    3339 | 451        | Hartnell College                           | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|    2365 | 451        | Hartnell College                           | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
|    2188 | 451        | Hartnell College                           | 2019FA        | Fall 2019                                                                             | Fall   | 2019-2020    |
|    1734 | 451        | Hartnell College                           | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|      82 | 451        | Hartnell College                           | 2019SU        | Summer 2019                                                                           | Summer | 2019-2020    |
|     134 | 461        | Monterey Peninsula College                 | 20153         | Spring 2015                                                                           | Spring | 2014-2015    |
|    2345 | 461        | Monterey Peninsula College                 | 20155         | Summer 2015                                                                           | Summer | 2015-2016    |
|    5310 | 461        | Monterey Peninsula College                 | 20157         | Fall 2015                                                                             | Fall   | 2015-2016    |
|    4840 | 461        | Monterey Peninsula College                 | 20163         | Spring 2016                                                                           | Spring | 2015-2016    |
|    2681 | 461        | Monterey Peninsula College                 | 20165         | Summer 2016                                                                           | Summer | 2016-2017    |
|    5612 | 461        | Monterey Peninsula College                 | 20167         | Fall 2016                                                                             | Fall   | 2016-2017    |
|    4387 | 461        | Monterey Peninsula College                 | 20173         | Spring 2017                                                                           | Spring | 2016-2017    |
|    2379 | 461        | Monterey Peninsula College                 | 20175         | Summer 2017                                                                           | Summer | 2017-2018    |
|    6196 | 461        | Monterey Peninsula College                 | 20177         | Fall 2017                                                                             | Fall   | 2017-2018    |
|    5558 | 461        | Monterey Peninsula College                 | 20183         | Spring 2018                                                                           | Spring | 2017-2018    |
|    2827 | 461        | Monterey Peninsula College                 | 20185         | Summer 2018                                                                           | Summer | 2018-2019    |
|    6677 | 461        | Monterey Peninsula College                 | 20187         | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1905 | 461        | Monterey Peninsula College                 | 20193         | Spring 2019                                                                           | Spring | 2018-2019    |
|    3985 | 471        | Evergreen Valley College                   | 2016FAR       | Fall 2016                                                                             | Fall   | 2016-2017    |
|    2712 | 471        | Evergreen Valley College                   | 2016SPR       | Spring 2016                                                                           | Spring | 2015-2016    |
|    3931 | 471        | Evergreen Valley College                   | 2016SUR       | Summer 2016                                                                           | Summer | 2016-2017    |
|    4001 | 471        | Evergreen Valley College                   | 2017FAR       | Fall 2017                                                                             | Fall   | 2017-2018    |
|    2892 | 471        | Evergreen Valley College                   | 2017SPR       | Spring 2017                                                                           | Spring | 2016-2017    |
|    3719 | 471        | Evergreen Valley College                   | 2017SUR       | Summer 2017                                                                           | Summer | 2017-2018    |
|    4300 | 471        | Evergreen Valley College                   | 2018FAR       | Fall 2018                                                                             | Fall   | 2018-2019    |
|     539 | 471        | Evergreen Valley College                   | 2018SPR       | Spring/Intersession 2018                                                              | Winter | 2017-2018    |
|    2443 | 471        | Evergreen Valley College                   | 2018SPR       | Spring 2018                                                                           | Spring | 2017-2018    |
|    3621 | 471        | Evergreen Valley College                   | 2018SUR       | Summer 2018                                                                           | Summer | 2018-2019    |
|    1371 | 471        | Evergreen Valley College                   | 2019SPR       | Intersession & Spring 2019                                                            | Winter | 2018-2019    |
|    5254 | 472        | San Jose City College                      | 2016FAR       | Fall 2016                                                                             | Fall   | 2016-2017    |
|    4240 | 472        | San Jose City College                      | 2016SPR       | Spring 2016                                                                           | Spring | 2015-2016    |
|    3592 | 472        | San Jose City College                      | 2016SUR       | Summer 2016                                                                           | Summer | 2016-2017    |
|    6808 | 472        | San Jose City College                      | 2017FAR       | Fall 2017                                                                             | Fall   | 2017-2018    |
|    4194 | 472        | San Jose City College                      | 2017SPR       | Spring 2017                                                                           | Spring | 2016-2017    |
|    4312 | 472        | San Jose City College                      | 2017SUR       | Summer 2017                                                                           | Summer | 2017-2018    |
|    6606 | 472        | San Jose City College                      | 2018FAR       | Fall 2018                                                                             | Fall   | 2018-2019    |
|    4198 | 472        | San Jose City College                      | 2018SPR       | Spring 2018                                                                           | Spring | 2017-2018    |
|    4575 | 472        | San Jose City College                      | 2018SUR       | Summer 2018                                                                           | Summer | 2018-2019    |
|    1642 | 472        | San Jose City College                      | 2019SPR       | Spring 2019                                                                           | Spring | 2018-2019    |
|     101 | 472        | San Jose City College                      | 2019SUR       | Summer 2019                                                                           | Summer | 2019-2020    |
|       5 | 481        | Las Positas College                        | 201303        | Spring 2014 (ROP Only)                                                                | Spring | 2013-2014    |
|    1456 | 481        | Las Positas College                        | 201402        | Fall 2014                                                                             | Fall   | 2014-2015    |
|      50 | 481        | Las Positas College                        | 201403        | Spring 2015 (ROP Only)                                                                | Spring | 2014-2015    |
|    2644 | 481        | Las Positas College                        | 201403        | Spring 2015                                                                           | Spring | 2014-2015    |
|    2585 | 481        | Las Positas College                        | 201501        | Summer 2015                                                                           | Summer | 2015-2016    |
|    4761 | 481        | Las Positas College                        | 201502        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    2823 | 481        | Las Positas College                        | 201503        | Spring 2016                                                                           | Spring | 2015-2016    |
|    2598 | 481        | Las Positas College                        | 201601        | Summer 2016                                                                           | Summer | 2016-2017    |
|    5138 | 481        | Las Positas College                        | 201602        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    3631 | 481        | Las Positas College                        | 201603        | Spring 2017                                                                           | Spring | 2016-2017    |
|    2539 | 481        | Las Positas College                        | 201701        | Summer 2017                                                                           | Summer | 2017-2018    |
|    5242 | 481        | Las Positas College                        | 201702        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    3898 | 481        | Las Positas College                        | 201703        | Spring 2018                                                                           | Spring | 2017-2018    |
|   11818 | 481        | Las Positas College                        | 201801        | Summer 2018                                                                           | Summer | 2018-2019    |
|    5830 | 481        | Las Positas College                        | 201802        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    2198 | 481        | Las Positas College                        | 201803        | Spring 2019                                                                           | Spring | 2018-2019    |
|     109 | 481        | Las Positas College                        | 201901        | Summer 2019                                                                           | Summer | 2019-2020    |
|     262 | 481        | Las Positas College                        | 201902        | Fall 2019                                                                             | Fall   | 2019-2020    |
|       1 | 481        | Las Positas College                        | 48112         | Spring 2012 - Las Positas College                                                     | Spring | 2011-2012    |
|       7 | 482        | Chabot College                             | 201401        | Summer 2014                                                                           | Summer | 2014-2015    |
|    3651 | 482        | Chabot College                             | 201402        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    6018 | 482        | Chabot College                             | 201403        | Spring 2015                                                                           | Spring | 2014-2015    |
|    5753 | 482        | Chabot College                             | 201501        | Summer 2015                                                                           | Summer | 2015-2016    |
|    9531 | 482        | Chabot College                             | 201502        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    6095 | 482        | Chabot College                             | 201503        | Spring 2016                                                                           | Spring | 2015-2016    |
|    6052 | 482        | Chabot College                             | 201601        | Summer 2016                                                                           | Summer | 2016-2017    |
|    9738 | 482        | Chabot College                             | 201602        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    8350 | 482        | Chabot College                             | 201603        | Spring 2017                                                                           | Spring | 2016-2017    |
|    6482 | 482        | Chabot College                             | 201701        | Summer 2017                                                                           | Summer | 2017-2018    |
|   12846 | 482        | Chabot College                             | 201702        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    8448 | 482        | Chabot College                             | 201703        | Spring 2018                                                                           | Spring | 2017-2018    |
|    8770 | 482        | Chabot College                             | 201801        | Summer 2018                                                                           | Summer | 2018-2019    |
|   11394 | 482        | Chabot College                             | 201802        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    4009 | 482        | Chabot College                             | 201803        | Spring 2019                                                                           | Spring | 2018-2019    |
|     362 | 482        | Chabot College                             | 201901        | Summer 2019                                                                           | Summer | 2019-2020    |
|     913 | 482        | Chabot College                             | 201902        | Fall 2019                                                                             | Fall   | 2019-2020    |
|    5887 | 492        | Mission College                            | 2015FA        | 2015 Fall                                                                             | Fall   | 2015-2016    |
|     768 | 492        | Mission College                            | 2015SP        | 2015 Spring                                                                           | Spring | 2014-2015    |
|    4205 | 492        | Mission College                            | 2015SU        | 2015 Summer                                                                           | Summer | 2015-2016    |
|    5678 | 492        | Mission College                            | 2016FA        | 2016 Fall                                                                             | Fall   | 2016-2017    |
|    4055 | 492        | Mission College                            | 2016SP        | 2016 Spring                                                                           | Spring | 2015-2016    |
|    4453 | 492        | Mission College                            | 2016SU        | 2016 Summer                                                                           | Summer | 2016-2017    |
|    1356 | 492        | Mission College                            | 2016WI        | 2016 Winter                                                                           | Winter | 2015-2016    |
|   10568 | 492        | Mission College                            | 201799        | Summer 2017 - Fall 2017                                                               | Summer | 2017-2018    |
|      12 | 492        | Mission College                            | 2017FA        | 2017 Fall                                                                             | Fall   | 2017-2018    |
|    4030 | 492        | Mission College                            | 2017SP        | 2017 Spring                                                                           | Spring | 2016-2017    |
|      19 | 492        | Mission College                            | 2017SU        | 2017 Summer                                                                           | Summer | 2017-2018    |
|    1162 | 492        | Mission College                            | 2017WI        | 2017 Winter                                                                           | Winter | 2016-2017    |
|    4798 | 492        | Mission College                            | 201800        | Winter 2018 - Spring 2018                                                             | Winter | 2017-2018    |
|    9615 | 492        | Mission College                            | 201899        | Summer 2018 - Fall 2018                                                               | Summer | 2018-2019    |
|    1621 | 492        | Mission College                            | 201900        | Winter 2019 - Spring 2019                                                             | Winter | 2018-2019    |
|    4520 | 493        | West Valley College                        | 2015FA        | 2015 Fall                                                                             | Fall   | 2015-2016    |
|     420 | 493        | West Valley College                        | 2015SP        | 2015 Spring                                                                           | Spring | 2014-2015    |
|    2916 | 493        | West Valley College                        | 2015SU        | 2015 Summer                                                                           | Summer | 2015-2016    |
|      46 | 493        | West Valley College                        | 2015SU/FA     | 2015 Summer & Fall                                                                    | Summer | 2015-2016    |
|    4718 | 493        | West Valley College                        | 2016FA        | 2016 Fall                                                                             | Fall   | 2016-2017    |
|    2603 | 493        | West Valley College                        | 2016SP        | 2016 Spring                                                                           | Spring | 2015-2016    |
|    3092 | 493        | West Valley College                        | 2016SU        | 2016 Summer                                                                           | Summer | 2016-2017    |
|    1171 | 493        | West Valley College                        | 2016WI        | 2016 Winter                                                                           | Winter | 2015-2016    |
|    7765 | 493        | West Valley College                        | 201799        | Summer 2017 - Fall 2017                                                               | Summer | 2017-2018    |
|    2897 | 493        | West Valley College                        | 2017SP        | 2017 Spring                                                                           | Spring | 2016-2017    |
|    1196 | 493        | West Valley College                        | 2017WI        | 2017 Winter                                                                           | Winter | 2016-2017    |
|    3534 | 493        | West Valley College                        | 201800        | Winter 2018 - Spring 2018                                                             | Winter | 2017-2018    |
|    7981 | 493        | West Valley College                        | 201899        | Summer 2018 - Fall 2018                                                               | Summer | 2018-2019    |
|    1630 | 493        | West Valley College                        | 201900        | Winter 2019 / Spring 2019                                                             | Winter | 2018-2019    |
|       1 | 521        | Bakersfield College                        | 201550        | Summer 2015                                                                           | Summer | 2015-2016    |
|    4032 | 521        | Bakersfield College                        | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
|   15848 | 521        | Bakersfield College                        | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    6371 | 521        | Bakersfield College                        | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
|    3922 | 521        | Bakersfield College                        | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
|       8 | 521        | Bakersfield College                        | 201750        | Summer 2107                                                                           | Summer | 2018-2019    |
|   14342 | 521        | Bakersfield College                        | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    6154 | 521        | Bakersfield College                        | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
|    4434 | 521        | Bakersfield College                        | 201850        | Summer 2018                                                                           | Summer | 2018-2019    |
|   14391 | 521        | Bakersfield College                        | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    2957 | 521        | Bakersfield College                        | 201930        | Spring 2019                                                                           | Spring | 2018-2019    |
|     285 | 521        | Bakersfield College                        | 201950        | Summer 2019                                                                           | Summer | 2019-2020    |
|    2324 | 521        | Bakersfield College                        | 201970        | Fall 2019                                                                             | Fall   | 2019-2020    |
|    1190 | 522        | Cerro Coso Community College               | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
|    2570 | 522        | Cerro Coso Community College               | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    1691 | 522        | Cerro Coso Community College               | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
|    1215 | 522        | Cerro Coso Community College               | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
|    2345 | 522        | Cerro Coso Community College               | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    1503 | 522        | Cerro Coso Community College               | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
|    1262 | 522        | Cerro Coso Community College               | 201850        | Summer 2018                                                                           | Summer | 2018-2019    |
|    2363 | 522        | Cerro Coso Community College               | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
|     589 | 522        | Cerro Coso Community College               | 201930        | Spring 2019                                                                           | Spring | 2018-2019    |
|      14 | 522        | Cerro Coso Community College               | 201950        | Summer 2019                                                                           | Summer | 2019-2020    |
|      32 | 522        | Cerro Coso Community College               | 201970        | Fall 2019                                                                             | Fall   | 2019-2020    |
|     655 | 523        | Porterville College                        | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
|    3113 | 523        | Porterville College                        | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    1116 | 523        | Porterville College                        | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
|     695 | 523        | Porterville College                        | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
|    2941 | 523        | Porterville College                        | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    1229 | 523        | Porterville College                        | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
|     801 | 523        | Porterville College                        | 201850        | Summer 2018                                                                           | Summer | 2018-2019    |
|    3471 | 523        | Porterville College                        | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
|     463 | 523        | Porterville College                        | 201930        | Spring 2019                                                                           | Spring | 2018-2019    |
|      37 | 523        | Porterville College                        | 201950        | Summer 2019                                                                           | Summer | 2019-2020    |
|      86 | 523        | Porterville College                        | 201970        | Fall 2019                                                                             | Fall   | 2019-2020    |
|    2159 | 531        | Merced College                             | 2014F         | Fall 2014                                                                             | Fall   | 2014-2015    |
|      38 | 531        | Merced College                             | 2014U         | Summer 2014                                                                           | Summer | 2014-2015    |
|    6258 | 531        | Merced College                             | 2015F         | Fall 2015                                                                             | Fall   | 2015-2016    |
|    3905 | 531        | Merced College                             | 2015S         | Spring 2015                                                                           | Spring | 2014-2015    |
|    2302 | 531        | Merced College                             | 2015U         | Summer 2015                                                                           | Summer | 2015-2016    |
|    6760 | 531        | Merced College                             | 2016F         | Fall 2016                                                                             | Fall   | 2016-2017    |
|    4129 | 531        | Merced College                             | 2016S         | Spring 2016                                                                           | Spring | 2015-2016    |
|    2464 | 531        | Merced College                             | 2016U         | Summer 2016                                                                           | Summer | 2016-2017    |
|     124 | 531        | Merced College                             | 2017F         | Fall 2017                                                                             | Fall   | 2017-2018    |
|    5189 | 531        | Merced College                             | 2017S         | Spring 2017                                                                           | Spring | 2016-2017    |
|   10709 | 531        | Merced College                             | 2017U         | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
|    6428 | 531        | Merced College                             | 2018S         | Spring 2018                                                                           | Spring | 2017-2018    |
|   12499 | 531        | Merced College                             | 2018U         | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
|    4199 | 531        | Merced College                             | 2019S         | Spring 2019                                                                           | Spring | 2018-2019    |
|    1558 | 531        | Merced College                             | 2019U         | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
|    9191 | 551        | San Joaquin Delta College                  | 011617        | Summer 2016                                                                           | Summer | 2016-2017    |
|    9125 | 551        | San Joaquin Delta College                  | 011718        | Summer 2017                                                                           | Summer | 2017-2018    |
|    9074 | 551        | San Joaquin Delta College                  | 011819        | Summer 2018                                                                           | Summer | 2018-2019    |
|    4119 | 551        | San Joaquin Delta College                  | 031819        | Spring 2019                                                                           | Spring | 2018-2019    |
|      71 | 551        | San Joaquin Delta College                  | 201516        | Fall 2015                                                                             | Fall   | 2015-2016    |
|   10461 | 551        | San Joaquin Delta College                  | 201617        | FALL 2016                                                                             | Fall   | 2016-2017    |
|   10740 | 551        | San Joaquin Delta College                  | 201718        | Fall 2017                                                                             | Fall   | 2017-2018    |
|   10239 | 551        | San Joaquin Delta College                  | 201819        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    8407 | 551        | San Joaquin Delta College                  | 301516        | Spring 2016                                                                           | Spring | 2015-2016    |
|    8684 | 551        | San Joaquin Delta College                  | 301617        | Spring 2017                                                                           | Spring | 2016-2017    |
|    8542 | 551        | San Joaquin Delta College                  | 301718        | Spring 2018                                                                           | Spring | 2017-2018    |
|     239 | 561        | College of the Sequoias                    | 201410        | Fall 2013                                                                             | Fall   | 2013-2014    |
|    4857 | 561        | College of the Sequoias                    | 201420        | Spring 2014                                                                           | Spring | 2013-2014    |
|       1 | 561        | College of the Sequoias                    | 201430        | Summer/Fall 2014                                                                      | Summer | 2014-2015    |
|   10634 | 561        | College of the Sequoias                    | 201500        | Summer/Fall 2014                                                                      | Summer | 2014-2015    |
|    4567 | 561        | College of the Sequoias                    | 201520        | Spring 2015                                                                           | Spring | 2014-2015    |
|   12210 | 561        | College of the Sequoias                    | 201600        | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
|    4529 | 561        | College of the Sequoias                    | 201620        | Spring 2016                                                                           | Spring | 2015-2016    |
|   13212 | 561        | College of the Sequoias                    | 201700        | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
|    4828 | 561        | College of the Sequoias                    | 201720        | Spring 2017                                                                           | Spring | 2016-2017    |
|   13559 | 561        | College of the Sequoias                    | 201800        | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
|    4960 | 561        | College of the Sequoias                    | 201820        | Spring 2018                                                                           | Spring | 2017-2018    |
|   14752 | 561        | College of the Sequoias                    | 201900        | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
|    4749 | 561        | College of the Sequoias                    | 201920        | Spring 2019                                                                           | Spring | 2018-2019    |
|    4283 | 561        | College of the Sequoias                    | 202000        | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
|    4845 | 571        | Fresno City College                        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|     119 | 571        | Fresno City College                        | 2015FAENRICH  | Fall 2015 HS Enrichment                                                               | Fall   | 2015-2016    |
|    2528 | 571        | Fresno City College                        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|     668 | 571        | Fresno City College                        | 2016FAENRICH  | Fall 2016 Enrichment/Dual Enrollment                                                  | Fall   | 2016-2017    |
|    7649 | 571        | Fresno City College                        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|     497 | 571        | Fresno City College                        | 2016SPENRICH  | Spring 2016 HS Enrichment                                                             | Spring | 2015-2016    |
|   19978 | 571        | Fresno City College                        | 2016SU/FA     | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
|     214 | 571        | Fresno City College                        | 2016SUENRICH  | Summer/Fall 2016 Enrichment/Dual Enrollment                                           | Summer | 2016-2017    |
|      62 | 571        | Fresno City College                        | 2016SUENRICH  | Summer 2016 Enrichment/Dual Enrollment                                                | Summer | 2016-2017    |
|    2940 | 571        | Fresno City College                        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|     710 | 571        | Fresno City College                        | 2017FAENRICH  | Fall 2017 HS Enrichment/Dual Enrollment                                               | Fall   | 2017-2018    |
|    7794 | 571        | Fresno City College                        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|    1264 | 571        | Fresno City College                        | 2017SPENRICH  | Spring 2017 HS Enrichment/Dual Enrollment                                             | Spring | 2016-2017    |
|   21419 | 571        | Fresno City College                        | 2017SU/FA     | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
|     311 | 571        | Fresno City College                        | 2017SUENRICH  | Summer/Fall 2017 Enrichment/Dual Enrollment                                           | Summer | 2017-2018    |
|    2679 | 571        | Fresno City College                        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1019 | 571        | Fresno City College                        | 2018FAENRICH  | Fall 2018 HS Enrichment/Dual Enrollment                                               | Fall   | 2018-2019    |
|    7262 | 571        | Fresno City College                        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|    2430 | 571        | Fresno City College                        | 2018SPENRICH  | Spring 2018 HS Enrichment/Dual Enrollment                                             | Spring | 2017-2018    |
|   20316 | 571        | Fresno City College                        | 2018SU/FA     | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
|     556 | 571        | Fresno City College                        | 2018SUENRICH  | Summer/Fall 2018 HS Enrichment/Dual Enrollment                                        | Summer | 2018-2019    |
|    3787 | 571        | Fresno City College                        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|    2614 | 571        | Fresno City College                        | 2019SPENRICH  | Spring 2019 HS Enrichment/Dual Enrollment                                             | Spring | 2018-2019    |
|    7007 | 571        | Fresno City College                        | 2019SU/FA     | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
|    1485 | 572        | Reedley College                            | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|      44 | 572        | Reedley College                            | 2015FAENRICH  | 2015 Fall HS Enrichment                                                               | Fall   | 2015-2016    |
|     812 | 572        | Reedley College                            | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|     560 | 572        | Reedley College                            | 2016FAENRICH  | Fall 2016 Enrichment/Dual Enrollment                                                  | Fall   | 2016-2017    |
|      12 | 572        | Reedley College                            | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|    2049 | 572        | Reedley College                            | 2016SP        | 2016 Spring                                                                           | Spring | 2015-2016    |
|     183 | 572        | Reedley College                            | 2016SPENRICH  | Spring 2016 HS Enrichment                                                             | Spring | 2015-2016    |
|    8113 | 572        | Reedley College                            | 2016SU/FA     | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
|     652 | 572        | Reedley College                            | 2016SUENRICH  | Summer/Fall 2016 Enrichment/Dual Enrollment                                           | Summer | 2016-2017    |
|      49 | 572        | Reedley College                            | 2016SUENRICH  | Summer 2016 Enrichment/Dual Enrollment                                                | Summer | 2016-2017    |
|     900 | 572        | Reedley College                            | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|     362 | 572        | Reedley College                            | 2017FAENRICH  | Fall 2017 HS Enrichment/Dual Enrollment                                               | Fall   | 2017-2018    |
|    2350 | 572        | Reedley College                            | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|    1568 | 572        | Reedley College                            | 2017SPENRICH  | Spring 2017 HS Enrichment/Dual Enrollment                                             | Spring | 2016-2017    |
|    7291 | 572        | Reedley College                            | 2017SU/FA     | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
|     521 | 572        | Reedley College                            | 2017SUENRICH  | Summer/Fall 2017 Enrichment/Dual Enrollment                                           | Summer | 2017-2018    |
|     797 | 572        | Reedley College                            | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|     391 | 572        | Reedley College                            | 2018FAENRICH  | Fall  2018 HS Enrichment/Dual Enrollment                                              | Fall   | 2018-2019    |
|    2345 | 572        | Reedley College                            | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|    1924 | 572        | Reedley College                            | 2018SPENRICH  | Spring 2018 HS Enrichment/Dual Enrollment                                             | Spring | 2017-2018    |
|    8044 | 572        | Reedley College                            | 2018SU/FA     | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
|     754 | 572        | Reedley College                            | 2018SUENRICH  | Summer/Fall 2018 HS Enrichment/Dual Enrollment                                        | Summer | 2018-2019    |
|    1182 | 572        | Reedley College                            | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|    2118 | 572        | Reedley College                            | 2019SPENRICH  | Spring 2019 HS Enrichment/Dual Enrollment                                             | Spring | 2018-2019    |
|    3482 | 572        | Reedley College                            | 2019SU/FA     | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
|     738 | 576        | Clovis Community College                   | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|      57 | 576        | Clovis Community College                   | 2015FAENRICH  | Fall 2015 HS Enrichment                                                               | Fall   | 2015-2016    |
|     468 | 576        | Clovis Community College                   | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|     100 | 576        | Clovis Community College                   | 2016FAENRICH  | Fall 2016 Enrichment/Dual Enrollment                                                  | Fall   | 2016-2017    |
|    1325 | 576        | Clovis Community College                   | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|     239 | 576        | Clovis Community College                   | 2016SPENRICH  | Spring 2016 HS Enrichment                                                             | Spring | 2015-2016    |
|    4292 | 576        | Clovis Community College                   | 2016SU/FA     | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
|     508 | 576        | Clovis Community College                   | 2016SUENRICH  | Summer/Fall 2016 Enrichment/Dual Enrollment                                           | Summer | 2016-2017    |
|     146 | 576        | Clovis Community College                   | 2016SUENRICH  | Summer 2016 Enrichment/Dual Enrollment                                                | Summer | 2016-2017    |
|     632 | 576        | Clovis Community College                   | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|     161 | 576        | Clovis Community College                   | 2017FAENRICH  | Fall 2017 HS Enrichment/Dual Enrollment                                               | Fall   | 2017-2018    |
|    1541 | 576        | Clovis Community College                   | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|      11 | 576        | Clovis Community College                   | 2017SPENRICH  | 2017 HS Enrichment/Dual Enrollment                                                    | Spring | 2016-2017    |
|     467 | 576        | Clovis Community College                   | 2017SPENRICH  | Spring 2017 HS Enrichment/Dual Enrollment                                             | Spring | 2016-2017    |
|    4647 | 576        | Clovis Community College                   | 2017SU/FA     | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
|     600 | 576        | Clovis Community College                   | 2017SUENRICH  | Summer/Fall 2017 Enrichment/Dual Enrollment                                           | Summer | 2017-2018    |
|     525 | 576        | Clovis Community College                   | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|     161 | 576        | Clovis Community College                   | 2018FAENRICH  | Fall 2018 HS Enrichment/Dual Enrollment                                               | Fall   | 2018-2019    |
|    1456 | 576        | Clovis Community College                   | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|     618 | 576        | Clovis Community College                   | 2018SPENRICH  | Spring 2018 HS Enrichment/Dual Enrollment                                             | Spring | 2017-2018    |
|    5122 | 576        | Clovis Community College                   | 2018SU/FA     | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
|     672 | 576        | Clovis Community College                   | 2018SUENRICH  | Summer/Fall 2018 HS Enrichment/Dual Enrollment                                        | Summer | 2018-2019    |
|     859 | 576        | Clovis Community College                   | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|     460 | 576        | Clovis Community College                   | 2019SPENRICH  | Spring 2019 HS Enrich/Dual Enrollment                                                 | Spring | 2018-2019    |
|    2334 | 576        | Clovis Community College                   | 2019SU/FA     | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
|     758 | 581        | West Hills College-Coalinga                | 2014FA        | 2014 Fall                                                                             | Fall   | 2014-2015    |
|      11 | 581        | West Hills College-Coalinga                | 2014SU        | 2014 Summer                                                                           | Summer | 2014-2015    |
|    1439 | 581        | West Hills College-Coalinga                | 2015FA        | 2015 Fall                                                                             | Fall   | 2015-2016    |
|     497 | 581        | West Hills College-Coalinga                | 2015SP        | 2015 Spring                                                                           | Spring | 2014-2015    |
|     406 | 581        | West Hills College-Coalinga                | 2015SU        | 2015 Summer                                                                           | Summer | 2015-2016    |
|    1973 | 581        | West Hills College-Coalinga                | 2016FA        | 2016 Fall                                                                             | Fall   | 2016-2017    |
|     523 | 581        | West Hills College-Coalinga                | 2016SP        | 2016 Spring                                                                           | Spring | 2015-2016    |
|     606 | 581        | West Hills College-Coalinga                | 2016SU        | 2016 Summer                                                                           | Summer | 2016-2017    |
|    1800 | 581        | West Hills College-Coalinga                | 2017FA        | 2017 Fall                                                                             | Fall   | 2017-2018    |
|     636 | 581        | West Hills College-Coalinga                | 2017SP        | 2017 Spring                                                                           | Spring | 2016-2017    |
|     600 | 581        | West Hills College-Coalinga                | 2017SU        | 2017 Summer                                                                           | Summer | 2017-2018    |
|    2938 | 581        | West Hills College-Coalinga                | 2018FA        | 2018 Fall                                                                             | Fall   | 2018-2019    |
|    1062 | 581        | West Hills College-Coalinga                | 2018SP        | 2018 Spring                                                                           | Spring | 2017-2018    |
|     673 | 581        | West Hills College-Coalinga                | 2018SU        | 2018 Summer                                                                           | Summer | 2018-2019    |
|     299 | 581        | West Hills College-Coalinga                | 2019FA        | 2019 Fall Term                                                                        | Fall   | 2019-2020    |
|     511 | 581        | West Hills College-Coalinga                | 2019SP        | 2019 Spring                                                                           | Spring | 2018-2019    |
|      29 | 581        | West Hills College-Coalinga                | 2019SU        | 2019 Summer Term                                                                      | Summer | 2019-2020    |
|     772 | 582        | West Hills College-Lemoore                 | 2014FA        | 2014 Fall                                                                             | Fall   | 2014-2015    |
|      34 | 582        | West Hills College-Lemoore                 | 2014SU        | 2014 Summer                                                                           | Summer | 2014-2015    |
|    2526 | 582        | West Hills College-Lemoore                 | 2015FA        | 2015 Fall                                                                             | Fall   | 2015-2016    |
|    1067 | 582        | West Hills College-Lemoore                 | 2015SP        | 2015 Spring                                                                           | Spring | 2014-2015    |
|    1173 | 582        | West Hills College-Lemoore                 | 2015SU        | 2015 Summer                                                                           | Summer | 2015-2016    |
|    2358 | 582        | West Hills College-Lemoore                 | 2016FA        | 2016 Fall                                                                             | Fall   | 2016-2017    |
|    2177 | 582        | West Hills College-Lemoore                 | 2016SP        | 2016 Spring                                                                           | Spring | 2015-2016    |
|    1082 | 582        | West Hills College-Lemoore                 | 2016SU        | 2016 Summer                                                                           | Summer | 2016-2017    |
|    2018 | 582        | West Hills College-Lemoore                 | 2017FA        | 2017 Fall                                                                             | Fall   | 2017-2018    |
|    1856 | 582        | West Hills College-Lemoore                 | 2017SP        | 2017 Spring                                                                           | Spring | 2016-2017    |
|    1232 | 582        | West Hills College-Lemoore                 | 2017SU        | 2017 Summer                                                                           | Summer | 2017-2018    |
|    2767 | 582        | West Hills College-Lemoore                 | 2018FA        | 2018 Fall                                                                             | Fall   | 2018-2019    |
|    1443 | 582        | West Hills College-Lemoore                 | 2018SP        | 2018 Spring                                                                           | Spring | 2017-2018    |
|    1308 | 582        | West Hills College-Lemoore                 | 2018SU        | 2018 Summer                                                                           | Summer | 2018-2019    |
|     533 | 582        | West Hills College-Lemoore                 | 2019FA        | 2019 Fall Term                                                                        | Fall   | 2019-2020    |
|     955 | 582        | West Hills College-Lemoore                 | 2019SP        | 2019 Spring                                                                           | Spring | 2018-2019    |
|     101 | 582        | West Hills College-Lemoore                 | 2019SU        | 2019 Summer Term                                                                      | Summer | 2019-2020    |
|    1926 | 591        | Columbia College                           | 2014CFA       | Columbia College Fall 2014 Application                                                | Fall   | 2014-2015    |
|     213 | 591        | Columbia College                           | 2014CSP       | Columbia College Spring 2014 Application                                              | Spring | 2013-2014    |
|     635 | 591        | Columbia College                           | 2014CSU       | Columbia College Summer 2014 Application                                              | Summer | 2014-2015    |
|    1899 | 591        | Columbia College                           | 2015CFA       | Columbia College Fall 2015 Application                                                | Fall   | 2015-2016    |
|    1232 | 591        | Columbia College                           | 2015CSP       | Columbia College Spring 2015 Application                                              | Spring | 2014-2015    |
|     627 | 591        | Columbia College                           | 2015CSU       | Columbia College Summer 2015 Application                                              | Summer | 2015-2016    |
|    1913 | 591        | Columbia College                           | 2016CFA       | Columbia College Fall 2016 Application                                                | Fall   | 2016-2017    |
|    1361 | 591        | Columbia College                           | 2016CSP       | Columbia College Spring 2016 Application                                              | Spring | 2015-2016    |
|     605 | 591        | Columbia College                           | 2016CSU       | Columbia College Summer 2016 Application                                              | Summer | 2016-2017    |
|    2159 | 591        | Columbia College                           | 2017CFA       | Columbia College Fall 2017 Application                                                | Fall   | 2017-2018    |
|    1413 | 591        | Columbia College                           | 2017CSP       | Columbia College Spring 2017 Application                                              | Spring | 2016-2017    |
|     584 | 591        | Columbia College                           | 2017CSU       | Columbia College Summer 2017 Application                                              | Summer | 2017-2018    |
|    2377 | 591        | Columbia College                           | 2018CFA       | Columbia College Fall 2018 Application                                                | Fall   | 2018-2019    |
|    1223 | 591        | Columbia College                           | 2018CSP       | Columbia College Spring 2018 Application                                              | Spring | 2017-2018    |
|     883 | 591        | Columbia College                           | 2018CSU       | Columbia College Summer 2018 Application                                              | Summer | 2018-2019    |
|     202 | 591        | Columbia College                           | 2019CFA       | Columbia College Fall 2019 Application                                                | Fall   | 2019-2020    |
|     717 | 591        | Columbia College                           | 2019CSP       | Columbia College Spring 2019 Application                                              | Spring | 2018-2019    |
|      14 | 591        | Columbia College                           | 2019CSU       | Columbia College Summer 2019 Application                                              | Summer | 2019-2020    |
|   11022 | 592        | Modesto Junior College                     | 2014MFA       | MJC Fall 2014 Term                                                                    | Fall   | 2014-2015    |
|     991 | 592        | Modesto Junior College                     | 2014MSP       | MJC Spring 2014 Term                                                                  | Spring | 2013-2014    |
|    6173 | 592        | Modesto Junior College                     | 2014MSU       | MJC Summer 2014 Term                                                                  | Summer | 2014-2015    |
|   11549 | 592        | Modesto Junior College                     | 2015MFA       | MJC Fall 2015 Term                                                                    | Fall   | 2015-2016    |
|    7926 | 592        | Modesto Junior College                     | 2015MSP       | MJC Spring 2015 Term                                                                  | Spring | 2014-2015    |
|    6814 | 592        | Modesto Junior College                     | 2015MSU       | MJC Summer 2015 Term                                                                  | Summer | 2015-2016    |
|   12195 | 592        | Modesto Junior College                     | 2016MFA       | MJC Fall 2016 Term                                                                    | Fall   | 2016-2017    |
|    7286 | 592        | Modesto Junior College                     | 2016MSP       | MJC Spring 2016 Term                                                                  | Spring | 2015-2016    |
|    6924 | 592        | Modesto Junior College                     | 2016MSU       | MJC Summer 2016 Term                                                                  | Summer | 2016-2017    |
|   11758 | 592        | Modesto Junior College                     | 2017MFA       | MJC Fall 2017 Term                                                                    | Fall   | 2017-2018    |
|    7183 | 592        | Modesto Junior College                     | 2017MSP       | MJC Spring 2017 Term                                                                  | Spring | 2016-2017    |
|    7299 | 592        | Modesto Junior College                     | 2017MSU       | MJC Summer 2017 Term                                                                  | Summer | 2017-2018    |
|   12189 | 592        | Modesto Junior College                     | 2018MFA       | MJC Fall 2018 Term                                                                    | Fall   | 2018-2019    |
|    6944 | 592        | Modesto Junior College                     | 2018MSP       | MJC Spring 2018 Term                                                                  | Spring | 2017-2018    |
|    7336 | 592        | Modesto Junior College                     | 2018MSU       | MJC Summer 2018 Term                                                                  | Summer | 2018-2019    |
|     805 | 592        | Modesto Junior College                     | 2019MFA       | MJC Fall 2019 Term                                                                    | Fall   | 2019-2020    |
|    4578 | 592        | Modesto Junior College                     | 2019MSP       | MJC Spring 2019 Term                                                                  | Spring | 2018-2019    |
|    2141 | 592        | Modesto Junior College                     | 2019MSU       | MJC Summer 2019 Term                                                                  | Summer | 2019-2020    |
|     773 | 611        | Allan Hancock College                      | 201710        | Summer 2016                                                                           | Summer | 2016-2017    |
|    5324 | 611        | Allan Hancock College                      | 201720        | Fall 2016                                                                             | Fall   | 2016-2017    |
|     655 | 611        | Allan Hancock College                      | 201730        | Winter 2017                                                                           | Winter | 2016-2017    |
|    7752 | 611        | Allan Hancock College                      | 201740        | Spring 2017                                                                           | Spring | 2016-2017    |
|    4993 | 611        | Allan Hancock College                      | 201810        | Summer 2017                                                                           | Summer | 2017-2018    |
|       1 | 611        | Allan Hancock College                      | 201820        | Fall 2018                                                                             | Fall   | 2018-2019    |
|   12650 | 611        | Allan Hancock College                      | 201820        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    1511 | 611        | Allan Hancock College                      | 201830        | Winter 2018                                                                           | Winter | 2017-2018    |
|    6329 | 611        | Allan Hancock College                      | 201840        | Spring 2018                                                                           | Spring | 2017-2018    |
|    4882 | 611        | Allan Hancock College                      | 201910        | Summer 2018                                                                           | Summer | 2018-2019    |
|    9311 | 611        | Allan Hancock College                      | 201920        | Fall 2018                                                                             | Fall   | 2018-2019    |
|     947 | 611        | Allan Hancock College                      | 201930        | Winter 2019                                                                           | Winter | 2018-2019    |
|    2134 | 611        | Allan Hancock College                      | 201940        | Spring 2019                                                                           | Spring | 2018-2019    |
|     182 | 611        | Allan Hancock College                      | 202010        | Summer 2019                                                                           | Summer | 2019-2020    |
|    1046 | 611        | Allan Hancock College                      | 202020        | Fall 2019                                                                             | Fall   | 2019-2020    |
|     363 | 621        | Antelope Valley College                    | 201430        | Spring 2014                                                                           | Spring | 2013-2014    |
|    3077 | 621        | Antelope Valley College                    | 201450        | Summer 2014                                                                           | Summer | 2014-2015    |
|    7549 | 621        | Antelope Valley College                    | 201470        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    1413 | 621        | Antelope Valley College                    | 201510        | Intersession 2015                                                                     | Winter | 2014-2015    |
|    4854 | 621        | Antelope Valley College                    | 201530        | Spring 2015                                                                           | Spring | 2014-2015    |
|    3498 | 621        | Antelope Valley College                    | 201550        | Summer 2015                                                                           | Summer | 2015-2016    |
|    9080 | 621        | Antelope Valley College                    | 201570        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    1348 | 621        | Antelope Valley College                    | 201610        | Intersession 2016                                                                     | Winter | 2015-2016    |
|    4687 | 621        | Antelope Valley College                    | 201630        | Spring 2016                                                                           | Spring | 2015-2016    |
|    3782 | 621        | Antelope Valley College                    | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
|   10124 | 621        | Antelope Valley College                    | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    1261 | 621        | Antelope Valley College                    | 201710        | Intersession 2017                                                                     | Winter | 2016-2017    |
|    4820 | 621        | Antelope Valley College                    | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
|    4804 | 621        | Antelope Valley College                    | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
|   12250 | 621        | Antelope Valley College                    | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    1641 | 621        | Antelope Valley College                    | 201810        | Intersession 2018                                                                     | Winter | 2017-2018    |
|    6856 | 621        | Antelope Valley College                    | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
|    5842 | 621        | Antelope Valley College                    | 201850        | Summer 2018                                                                           | Summer | 2018-2019    |
|   12683 | 621        | Antelope Valley College                    | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1520 | 621        | Antelope Valley College                    | 201910        | Intersession 2019                                                                     | Winter | 2018-2019    |
|    2633 | 621        | Antelope Valley College                    | 201930        | Spring 2019                                                                           | Spring | 2018-2019    |
|     188 | 621        | Antelope Valley College                    | 201950        | Summer 2019                                                                           | Summer | 2019-2020    |
|     930 | 621        | Antelope Valley College                    | 201970        | Fall 2019                                                                             | Fall   | 2019-2020    |
|    4896 | 641        | Cuesta College                             | 201403        | Spring 2014 - Cuesta College                                                          | Spring | 2013-2014    |
|    2927 | 641        | Cuesta College                             | 201405        | Summer 2014 - Cuesta College                                                          | Summer | 2014-2015    |
|    8198 | 641        | Cuesta College                             | 201407        | Fall 2014 - Cuesta College                                                            | Fall   | 2014-2015    |
|    4931 | 641        | Cuesta College                             | 201503        | Spring 2015 - Cuesta College                                                          | Spring | 2014-2015    |
|    2799 | 641        | Cuesta College                             | 201505        | Summer Session 2015 - Cuesta College                                                  | Summer | 2015-2016    |
|    9248 | 641        | Cuesta College                             | 201507        | Fall 2015 - Cuesta College                                                            | Fall   | 2015-2016    |
|    5635 | 641        | Cuesta College                             | 201603        | Spring 2016 - Cuesta College                                                          | Spring | 2015-2016    |
|    2763 | 641        | Cuesta College                             | 201605        | Summer Session 2016 - Cuesta College                                                  | Summer | 2016-2017    |
|    9844 | 641        | Cuesta College                             | 201607        | Fall 2016 - Cuesta College                                                            | Fall   | 2016-2017    |
|     739 | 641        | Cuesta College                             | 201703        | Spring 2017                                                                           | Spring | 2016-2017    |
|    4806 | 641        | Cuesta College                             | 201703        | Spring 2017 - Cuesta College                                                          | Spring | 2016-2017    |
|    2546 | 641        | Cuesta College                             | 201705        | Summer Session 2017 - Cuesta College                                                  | Summer | 2017-2018    |
|    8531 | 641        | Cuesta College                             | 201707        | Fall 2017 - Cuesta College                                                            | Fall   | 2017-2018    |
|    2075 | 641        | Cuesta College                             | 201707        | Fall 2017: August 21 - December 22                                                    | Fall   | 2017-2018    |
|    6582 | 641        | Cuesta College                             | 201803        | Spring 2018: January 16 - May 18                                                      | Spring | 2017-2018    |
|    3948 | 641        | Cuesta College                             | 201805        | Summer 2018: June 18 - July 27                                                        | Summer | 2018-2019    |
|   20556 | 641        | Cuesta College                             | 201807        | Fall 2018: August 13 - December 14                                                    | Fall   | 2018-2019    |
|    3882 | 641        | Cuesta College                             | 201903        | Spring 2019                                                                           | Spring | 2018-2019    |
|    2852 | 641        | Cuesta College                             | 201907        | Fall 2019                                                                             | Fall   | 2019-2020    |
|     248 | 651        | Santa Barbara City College                 | 201430        | Fall 2013                                                                             | Fall   | 2013-2014    |
|    8536 | 651        | Santa Barbara City College                 | 201450        | Spring 2014                                                                           | Spring | 2013-2014    |
|    5740 | 651        | Santa Barbara City College                 | 201510        | Summer 2014                                                                           | Summer | 2014-2015    |
|   15242 | 651        | Santa Barbara City College                 | 201530        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    8898 | 651        | Santa Barbara City College                 | 201550        | Spring 2015                                                                           | Spring | 2014-2015    |
|   20697 | 651        | Santa Barbara City College                 | 201630        | Summer I (May 18, 2015), Summer II (June 29, 2015), Fall (August 24, 2015)            | Summer | 2015-2016    |
|    8728 | 651        | Santa Barbara City College                 | 201650        | Spring 2016                                                                           | Spring | 2015-2016    |
|   19664 | 651        | Santa Barbara City College                 | 201730        | Summer I (May 16, 2016), Summer II (June 27, 2016), Fall (August 22, 2016)            | Summer | 2016-2017    |
|    8863 | 651        | Santa Barbara City College                 | 201750        | Spring 2017                                                                           | Spring | 2016-2017    |
|   20412 | 651        | Santa Barbara City College                 | 201830        | Summer I (May 15, 2017), Summer II (June 26, 2017), Fall (August 21, 2017)            | Summer | 2017-2018    |
|   10977 | 651        | Santa Barbara City College                 | 201850        | Spring 2018                                                                           | Spring | 2017-2018    |
|   18459 | 651        | Santa Barbara City College                 | 201930        | Summer I (May 21, 2018), Summer II (June 30, 2018), Fall (August 27, 2018)            | Summer | 2018-2019    |
|    3357 | 651        | Santa Barbara City College                 | 201950        | Spring 2019                                                                           | Spring | 2018-2019    |
|     543 | 661        | College of the Canyons                     | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
|   10860 | 661        | College of the Canyons                     | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    6203 | 661        | College of the Canyons                     | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
|    6985 | 661        | College of the Canyons                     | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
|    2430 | 661        | College of the Canyons                     | 2015WI        | Winter 2015                                                                           | Winter | 2014-2015    |
|   10636 | 661        | College of the Canyons                     | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    6421 | 661        | College of the Canyons                     | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|    7336 | 661        | College of the Canyons                     | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
|    2689 | 661        | College of the Canyons                     | 2016WI        | Winter 2016                                                                           | Winter | 2015-2016    |
|   10157 | 661        | College of the Canyons                     | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    6118 | 661        | College of the Canyons                     | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|    7516 | 661        | College of the Canyons                     | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
|    3139 | 661        | College of the Canyons                     | 2017WI        | Winter 2017                                                                           | Winter | 2016-2017    |
|   11625 | 661        | College of the Canyons                     | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    6130 | 661        | College of the Canyons                     | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|    7386 | 661        | College of the Canyons                     | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
|    3835 | 661        | College of the Canyons                     | 2018WI        | Winter 2018                                                                           | Winter | 2017-2018    |
|    2805 | 661        | College of the Canyons                     | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|    3097 | 661        | College of the Canyons                     | 2019WI        | Winter 2019                                                                           | Winter | 2018-2019    |
|     303 | 681        | Moorpark College                           | 201403        | Spring 2014 - 201403                                                                  | Spring | 2013-2014    |
|    4527 | 681        | Moorpark College                           | 201405        | Summer 2014 - 201405                                                                  | Summer | 2014-2015    |
|    7486 | 681        | Moorpark College                           | 201407        | Fall 2014 - 201407                                                                    | Fall   | 2014-2015    |
|    3893 | 681        | Moorpark College                           | 201503        | Spring 2015 - 201503                                                                  | Spring | 2014-2015    |
|    4951 | 681        | Moorpark College                           | 201505        | Summer 2015 - 201505                                                                  | Summer | 2015-2016    |
|    8410 | 681        | Moorpark College                           | 201507        | Fall 2015 - 201507                                                                    | Fall   | 2015-2016    |
|    4281 | 681        | Moorpark College                           | 201603        | Spring 2016 - 201603                                                                  | Spring | 2015-2016    |
|    5147 | 681        | Moorpark College                           | 201605        | Summer 2016 - 201605                                                                  | Summer | 2016-2017    |
|    8670 | 681        | Moorpark College                           | 201607        | Fall 2016 - 201607                                                                    | Fall   | 2016-2017    |
|    4932 | 681        | Moorpark College                           | 201703        | Spring 2017                                                                           | Spring | 2016-2017    |
|    5297 | 681        | Moorpark College                           | 201705        | Summer 2017 - 201705                                                                  | Summer | 2017-2018    |
|    8355 | 681        | Moorpark College                           | 201707        | Fall 2017 - 201707                                                                    | Fall   | 2017-2018    |
|    4344 | 681        | Moorpark College                           | 201803        | Spring 2018 - 201803                                                                  | Spring | 2017-2018    |
|    5718 | 681        | Moorpark College                           | 201805        | Summer 2018 - 201805                                                                  | Summer | 2018-2019    |
|    8949 | 681        | Moorpark College                           | 201807        | Fall 2018 - 201807                                                                    | Fall   | 2018-2019    |
|    2612 | 681        | Moorpark College                           | 201903        | Spring 2019 -201903                                                                   | Spring | 2018-2019    |
|      91 | 681        | Moorpark College                           | 201905        | Summer 2019 -201905                                                                   | Summer | 2019-2020    |
|     322 | 681        | Moorpark College                           | 201907        | Fall 2019 - 201907                                                                    | Fall   | 2019-2020    |
|     111 | 682        | Oxnard College                             | 201403        | Spring 2014 - 201403                                                                  | Spring | 2013-2014    |
|    1610 | 682        | Oxnard College                             | 201405        | Summer 2014 - 201405                                                                  | Summer | 2014-2015    |
|    3042 | 682        | Oxnard College                             | 201407        | Fall 2014 - 201407                                                                    | Fall   | 2014-2015    |
|    1903 | 682        | Oxnard College                             | 201503        | Spring 2015 - 201503                                                                  | Spring | 2014-2015    |
|    1689 | 682        | Oxnard College                             | 201505        | Summer 2015 - 201505                                                                  | Summer | 2015-2016    |
|    3559 | 682        | Oxnard College                             | 201507        | Fall 2015 - 201507                                                                    | Fall   | 2015-2016    |
|    2148 | 682        | Oxnard College                             | 201603        | Spring 2016 - 201603                                                                  | Spring | 2015-2016    |
|    1609 | 682        | Oxnard College                             | 201605        | Summer 2016 - 201605                                                                  | Summer | 2016-2017    |
|    3609 | 682        | Oxnard College                             | 201607        | Fall 2016 - 201607                                                                    | Fall   | 2016-2017    |
|    2850 | 682        | Oxnard College                             | 201703        | Spring 2017 - 201703                                                                  | Spring | 2016-2017    |
|    1837 | 682        | Oxnard College                             | 201705        | Summer 2017 - 201705                                                                  | Summer | 2017-2018    |
|    3718 | 682        | Oxnard College                             | 201707        | Fall 2017 - 201707                                                                    | Fall   | 2017-2018    |
|    2910 | 682        | Oxnard College                             | 201803        | Spring 2018 - 201803                                                                  | Spring | 2017-2018    |
|    2101 | 682        | Oxnard College                             | 201805        | Summer 2018 - 201805                                                                  | Summer | 2018-2019    |
|    4518 | 682        | Oxnard College                             | 201807        | Fall 2018 - 201807                                                                    | Fall   | 2018-2019    |
|    1426 | 682        | Oxnard College                             | 201903        | Spring 2019 -201903                                                                   | Spring | 2018-2019    |
|      51 | 682        | Oxnard College                             | 201905        | Summer 2019 - 201905                                                                  | Summer | 2019-2020    |
|     997 | 682        | Oxnard College                             | 201907        | Fall 2019 - 201907                                                                    | Fall   | 2019-2020    |
|     223 | 683        | Ventura College                            | 201403        | Spring 2014 - 201403                                                                  | Spring | 2013-2014    |
|    3266 | 683        | Ventura College                            | 201405        | Summer 2014 - 201405                                                                  | Summer | 2014-2015    |
|    6457 | 683        | Ventura College                            | 201407        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    3658 | 683        | Ventura College                            | 201503        | Spring 2015                                                                           | Spring | 2014-2015    |
|    3101 | 683        | Ventura College                            | 201505        | Summer 2015                                                                           | Summer | 2015-2016    |
|    6663 | 683        | Ventura College                            | 201507        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    3752 | 683        | Ventura College                            | 201603        | Spring 2016                                                                           | Spring | 2015-2016    |
|    3258 | 683        | Ventura College                            | 201605        | Summer 2016                                                                           | Summer | 2016-2017    |
|    6961 | 683        | Ventura College                            | 201607        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    4687 | 683        | Ventura College                            | 201703        | Spring 2017                                                                           | Spring | 2016-2017    |
|    3344 | 683        | Ventura College                            | 201705        | Summer 2017                                                                           | Summer | 2017-2018    |
|    7844 | 683        | Ventura College                            | 201707        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    4353 | 683        | Ventura College                            | 201803        | Spring 2018                                                                           | Spring | 2017-2018    |
|    3621 | 683        | Ventura College                            | 201805        | Summer 2018                                                                           | Summer | 2018-2019    |
|    7804 | 683        | Ventura College                            | 201807        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    2102 | 683        | Ventura College                            | 201903        | Spring 2019                                                                           | Spring | 2018-2019    |
|      81 | 683        | Ventura College                            | 201905        | Summer 2019                                                                           | Summer | 2019-2020    |
|    1331 | 683        | Ventura College                            | 201907        | Fall 2019                                                                             | Fall   | 2019-2020    |
|      49 | 691        | Taft College                               | 201530        | Summer 2015                                                                           | Summer | 2015-2016    |
|    1372 | 691        | Taft College                               | 201550        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    1751 | 691        | Taft College                               | 201620        | Spring 2016                                                                           | Spring | 2015-2016    |
|    1416 | 691        | Taft College                               | 201630        | Summer 2016                                                                           | Summer | 2016-2017    |
|    2744 | 691        | Taft College                               | 201650        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    1648 | 691        | Taft College                               | 201720        | Spring 2017                                                                           | Spring | 2016-2017    |
|    1373 | 691        | Taft College                               | 201730        | Summer 2017                                                                           | Summer | 2017-2018    |
|    3016 | 691        | Taft College                               | 201750        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    2099 | 691        | Taft College                               | 201820        | Spring 2018                                                                           | Spring | 2017-2018    |
|    1955 | 691        | Taft College                               | 201830        | Summer 2018                                                                           | Summer | 2018-2019    |
|      36 | 691        | Taft College                               | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
|    3443 | 691        | Taft College                               | 201850        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1260 | 691        | Taft College                               | 201920        | Spring 2019                                                                           | Spring | 2018-2019    |
|     122 | 691        | Taft College                               | 201930        | Summer 2019                                                                           | Summer | 2019-2020    |
|     378 | 691        | Taft College                               | 201950        | Fall 2019                                                                             | Fall   | 2019-2020    |
|    3351 | 711        | Compton CCD                                | 2014/FA       | Fall Semester 2014                                                                    | Fall   | 2014-2015    |
|     578 | 711        | Compton CCD                                | 2014/SU       | Summer Session 2014                                                                   | Summer | 2014-2015    |
|    4575 | 711        | Compton CCD                                | 2015/FA       | Fall Semester 2015                                                                    | Fall   | 2015-2016    |
|    3606 | 711        | Compton CCD                                | 2015/SP       | Spring Semester 2015                                                                  | Spring | 2014-2015    |
|    2900 | 711        | Compton CCD                                | 2015/SU       | Summer Session 2015                                                                   | Summer | 2015-2016    |
|    5033 | 711        | Compton CCD                                | 2016/FA       | Fall Semester 2016                                                                    | Fall   | 2016-2017    |
|    3488 | 711        | Compton CCD                                | 2016/SP       | Spring Semester 2016                                                                  | Spring | 2015-2016    |
|    2889 | 711        | Compton CCD                                | 2016/SU       | Summer Session 2016                                                                   | Summer | 2016-2017    |
|    4692 | 711        | Compton CCD                                | 2017/FA       | Fall Semester 2017                                                                    | Fall   | 2017-2018    |
|     297 | 711        | Compton CCD                                | 2017/SP       | Spring Session 2017                                                                   | Spring | 2016-2017    |
|    2827 | 711        | Compton CCD                                | 2017/SP       | Spring Semester 2017                                                                  | Spring | 2016-2017    |
|    2457 | 711        | Compton CCD                                | 2017/SU       | Summer Session 2017                                                                   | Summer | 2017-2018    |
|    1234 | 711        | Compton CCD                                | 2017/WI       | Winter Session 2017                                                                   | Winter | 2016-2017    |
|    4530 | 711        | Compton CCD                                | 2018/FA       | Fall Semester 2018                                                                    | Fall   | 2018-2019    |
|    2766 | 711        | Compton CCD                                | 2018/SP       | Spring Semester 2018                                                                  | Spring | 2017-2018    |
|    2458 | 711        | Compton CCD                                | 2018/SU       | Summer Session 2018                                                                   | Summer | 2018-2019    |
|    1164 | 711        | Compton CCD                                | 2018/WI       | Winter Session 2018                                                                   | Winter | 2017-2018    |
|     504 | 711        | Compton CCD                                | 2019/SP       | Spring Semester 2019                                                                  | Spring | 2018-2019    |
|     821 | 711        | Compton CCD                                | 2019/WI       | Winter Session 2019                                                                   | Winter | 2018-2019    |
|       2 | 711        | Compton CCD                                | 71111         | Fall 2011 - Compton CCD                                                               | Fall   | 2011-2012    |
|       5 | 711        | Compton CCD                                | 71112         | Spring 2012 - Compton CCD                                                             | Spring | 2011-2012    |
|   10740 | 721        | El Camino College                          | 2014/FA       | Fall Semester 2014                                                                    | Fall   | 2014-2015    |
|      76 | 721        | El Camino College                          | 2014/FA       | Special Programs Fall 2014                                                            | Fall   | 2014-2015    |
|       1 | 721        | El Camino College                          | 2014/Fall     | Special Programs Fall 2014                                                            | Fall   | 2014-2015    |
|    1404 | 721        | El Camino College                          | 2014/SU       | Summer Session 2014                                                                   | Summer | 2014-2015    |
|      22 | 721        | El Camino College                          | 2015/FA       | Special Programs Fall 2015                                                            | Fall   | 2015-2016    |
|   21221 | 721        | El Camino College                          | 2015/FA       | Fall Semester 2015                                                                    | Fall   | 2015-2016    |
|       1 | 721        | El Camino College                          | 2015/Fall     | Special Programs Fall 2015                                                            | Fall   | 2015-2016    |
|   13229 | 721        | El Camino College                          | 2015/SP       | Spring Semester 2015                                                                  | Spring | 2014-2015    |
|   11417 | 721        | El Camino College                          | 2015/SU       | Summer Session 2015                                                                   | Summer | 2015-2016    |
|   21563 | 721        | El Camino College                          | 2016/FA       | Fall Semester 2016                                                                    | Fall   | 2016-2017    |
|   12664 | 721        | El Camino College                          | 2016/SP       | Spring Semester 2016                                                                  | Spring | 2015-2016    |
|   11618 | 721        | El Camino College                          | 2016/SU       | Summer Session 2016                                                                   | Summer | 2016-2017    |
|   20313 | 721        | El Camino College                          | 2017/FA       | Fall Semester 2017                                                                    | Fall   | 2017-2018    |
|   12196 | 721        | El Camino College                          | 2017/SP       | Spring Semester 2017                                                                  | Spring | 2016-2017    |
|   11852 | 721        | El Camino College                          | 2017/SU       | Summer Session 2017                                                                   | Summer | 2017-2018    |
|    4981 | 721        | El Camino College                          | 2017/WI       | Winter Session 2017                                                                   | Winter | 2016-2017    |
|   19841 | 721        | El Camino College                          | 2018/FA       | Fall Semester 2018                                                                    | Fall   | 2018-2019    |
|   10919 | 721        | El Camino College                          | 2018/SP       | Spring Semester 2018                                                                  | Spring | 2017-2018    |
|   11349 | 721        | El Camino College                          | 2018/SU       | Summer Session 2018                                                                   | Summer | 2018-2019    |
|    4653 | 721        | El Camino College                          | 2018/WI       | Winter Session 2018                                                                   | Winter | 2017-2018    |
|    1318 | 721        | El Camino College                          | 2019/FA       | Fall Semester 2019                                                                    | Fall   | 2019-2020    |
|    4386 | 721        | El Camino College                          | 2019/SP       | Spring Semester 2019                                                                  | Spring | 2018-2019    |
|     141 | 721        | El Camino College                          | 2019/SU       | Summer Session 2019                                                                   | Summer | 2019-2020    |
|    3798 | 721        | El Camino College                          | 2019/WI       | Winter Session 2019                                                                   | Winter | 2018-2019    |
|       1 | 721        | El Camino College                          | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
|    1122 | 731        | Glendale Community College                 | 2157          | Fall 2015/ Winter and Spring 2016                                                     | Fall   | 2016-2017    |
|    2720 | 731        | Glendale Community College                 | 2161          | Winter and Spring 2016                                                                | Winter | 2015-2016    |
|    2522 | 731        | Glendale Community College                 | 2163          | Spring 2016                                                                           | Spring | 2015-2016    |
|   12479 | 731        | Glendale Community College                 | 2165          | Summer and Fall 2016                                                                  | Summer | 2016-2017    |
|    2883 | 731        | Glendale Community College                 | 2167          | Fall 2016                                                                             | Fall   | 2016-2017    |
|    5700 | 731        | Glendale Community College                 | 2171          | Winter and Spring 2017                                                                | Winter | 2016-2017    |
|    2147 | 731        | Glendale Community College                 | 2173          | Spring 2017                                                                           | Spring | 2016-2017    |
|   13685 | 731        | Glendale Community College                 | 2175          | Summer and Fall 2017                                                                  | Summer | 2017-2018    |
|    1368 | 731        | Glendale Community College                 | 2177          | Fall 2017                                                                             | Fall   | 2017-2018    |
|    7211 | 731        | Glendale Community College                 | 2181          | Winter and Spring 2018                                                                | Winter | 2017-2018    |
|    1127 | 731        | Glendale Community College                 | 2183          | Spring 2018                                                                           | Spring | 2017-2018    |
|   14723 | 731        | Glendale Community College                 | 2185          | Summer and Fall 2018                                                                  | Summer | 2018-2019    |
|    3905 | 731        | Glendale Community College                 | 2191          | Winter and Spring 2019                                                                | Winter | 2018-2019    |
|     197 | 731        | Glendale Community College                 | 2195          | Summer and Fall 2019                                                                  | Summer | 2019-2020    |
|     192 | 741        | Los Angeles City College                   | 2148          | LATE-START CLASSES Fall 2014 Application                                              | Fall   | 2014-2015    |
|    4410 | 741        | Los Angeles City College                   | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
|     398 | 741        | Los Angeles City College                   | 2148          | LATE Fall 2014 Application                                                            | Fall   | 2014-2015    |
|    4463 | 741        | Los Angeles City College                   | 2152          | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
|      22 | 741        | Los Angeles City College                   | 2152          | Winter 2015 - Late Application, semester began January 5th                            | Winter | 2014-2015    |
|    3971 | 741        | Los Angeles City College                   | 2154          | Spring 2015 - semester begins February 9th                                            | Spring | 2014-2015    |
|     188 | 741        | Los Angeles City College                   | 2154          | Spring 2015                                                                           | Spring | 2014-2015    |
|       6 | 741        | Los Angeles City College                   | 2156          | Summer 2015                                                                           | Summer | 2015-2016    |
|   13125 | 741        | Los Angeles City College                   | 2156          | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
|    4377 | 741        | Los Angeles City College                   | 2158          | Fall 2015 (Begins August 31, 2015)                                                    | Fall   | 2015-2016    |
|     411 | 741        | Los Angeles City College                   | 2158          | Fall 2015 Late-Start Class Application                                                | Fall   | 2015-2016    |
|    4583 | 741        | Los Angeles City College                   | 2162          | Winter and/or Spring 2016 - This application is valid for Winter and Spring semesters | Winter | 2015-2016    |
|    3505 | 741        | Los Angeles City College                   | 2164          | Spring 2016 - begins February 8th                                                     | Spring | 2015-2016    |
|   12683 | 741        | Los Angeles City College                   | 2166          | Summer and/or Fall 2016 - This application is valid for Summer and Fall semesters     | Summer | 2016-2017    |
|    4249 | 741        | Los Angeles City College                   | 2168          | Fall 2016 - begins August 29th                                                        | Fall   | 2016-2017    |
|       3 | 741        | Los Angeles City College                   | 2168          | Summer and/or Fall 2016 - This application is valid for Summer and Fall semesters     | Summer | 2016-2017    |
|     483 | 741        | Los Angeles City College                   | 2168          | Fall 2016 - open for Late-Start Classes that begin October 24                         | Fall   | 2016-2017    |
|    6783 | 741        | Los Angeles City College                   | 2172          | Winter and/or Spring 2017 - valid for both semesters                                  | Winter | 2016-2017    |
|     611 | 741        | Los Angeles City College                   | 2174          | Spring 2017 Late Start Classes Begin April 10                                         | Spring | 2016-2017    |
|    1053 | 741        | Los Angeles City College                   | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
|    6068 | 741        | Los Angeles City College                   | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
|   15284 | 741        | Los Angeles City College                   | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
|    3559 | 741        | Los Angeles City College                   | 2182          | Winter 2018 applications                                                              | Winter | 2017-2018    |
|    8135 | 741        | Los Angeles City College                   | 2184          | Spring 2018 applications                                                              | Spring | 2017-2018    |
|    7447 | 741        | Los Angeles City College                   | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
|   12717 | 741        | Los Angeles City College                   | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
|    2293 | 741        | Los Angeles City College                   | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
|    1841 | 741        | Los Angeles City College                   | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
|     146 | 741        | Los Angeles City College                   | 2196          | Summer 2019                                                                           | Summer | 2019-2020    |
|     499 | 741        | Los Angeles City College                   | 2198          | Fall 2019                                                                             | Fall   | 2019-2020    |
|    1162 | 742        | Los Angeles Harbor College                 | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
|      74 | 742        | Los Angeles Harbor College                 | 2152          | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
|    2873 | 742        | Los Angeles Harbor College                 | 2154          | Spring 2015                                                                           | Spring | 2014-2015    |
|    2649 | 742        | Los Angeles Harbor College                 | 2156          | Summer 2015                                                                           | Summer | 2015-2016    |
|    4599 | 742        | Los Angeles Harbor College                 | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
|    1706 | 742        | Los Angeles Harbor College                 | 2162          | Winter/Spring 2016                                                                    | Winter | 2015-2016    |
|    1510 | 742        | Los Angeles Harbor College                 | 2164          | Spring 2016                                                                           | Spring | 2015-2016    |
|    2348 | 742        | Los Angeles Harbor College                 | 2166          | Summer 2016                                                                           | Summer | 2016-2017    |
|    3943 | 742        | Los Angeles Harbor College                 | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
|    1110 | 742        | Los Angeles Harbor College                 | 2172          | Winter 2017                                                                           | Winter | 2016-2017    |
|    2338 | 742        | Los Angeles Harbor College                 | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
|       2 | 742        | Los Angeles Harbor College                 | 2176          | Summer-Fall 2017                                                                      | Summer | 2017-2018    |
|    1727 | 742        | Los Angeles Harbor College                 | 2176          | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
|    1356 | 742        | Los Angeles Harbor College                 | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
|    4813 | 742        | Los Angeles Harbor College                 | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
|     999 | 742        | Los Angeles Harbor College                 | 2182          | Winter 2018                                                                           | Winter | 2017-2018    |
|    3505 | 742        | Los Angeles Harbor College                 | 2184          | Spring 2018                                                                           | Spring | 2017-2018    |
|    2160 | 742        | Los Angeles Harbor College                 | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
|    4816 | 742        | Los Angeles Harbor College                 | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
|     680 | 742        | Los Angeles Harbor College                 | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
|     988 | 742        | Los Angeles Harbor College                 | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
|      23 | 742        | Los Angeles Harbor College                 | 2196          | Summer 2019                                                                           | Summer | 2019-2020    |
|      90 | 742        | Los Angeles Harbor College                 | 2198          | Fall 2019                                                                             | Fall   | 2019-2020    |
|    1316 | 743        | Los Angeles Mission College                | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
|    1654 | 743        | Los Angeles Mission College                | 2152          | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
|    1489 | 743        | Los Angeles Mission College                | 2154          | Spring 2015                                                                           | Spring | 2014-2015    |
|    2485 | 743        | Los Angeles Mission College                | 2156          | Summer 2015                                                                           | Summer | 2015-2016    |
|    5370 | 743        | Los Angeles Mission College                | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
|     792 | 743        | Los Angeles Mission College                | 2162          | Winter 2016                                                                           | Winter | 2015-2016    |
|       1 | 743        | Los Angeles Mission College                | 2162          | Winter/Spring 2016                                                                    | Winter | 2015-2016    |
|    2306 | 743        | Los Angeles Mission College                | 2164          | Spring 2016                                                                           | Spring | 2015-2016    |
|    2230 | 743        | Los Angeles Mission College                | 2166          | Summer 2016                                                                           | Summer | 2016-2017    |
|    4970 | 743        | Los Angeles Mission College                | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
|    1001 | 743        | Los Angeles Mission College                | 2172          | Winter 2017                                                                           | Winter | 2016-2017    |
|    3696 | 743        | Los Angeles Mission College                | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
|    3017 | 743        | Los Angeles Mission College                | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
|    7181 | 743        | Los Angeles Mission College                | 2178          | FALL 2017                                                                             | Fall   | 2017-2018    |
|    1218 | 743        | Los Angeles Mission College                | 2182          | Winter 2018                                                                           | Winter | 2017-2018    |
|    4156 | 743        | Los Angeles Mission College                | 2184          | Spring 2018                                                                           | Spring | 2017-2018    |
|    3068 | 743        | Los Angeles Mission College                | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
|    5473 | 743        | Los Angeles Mission College                | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1006 | 743        | Los Angeles Mission College                | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
|     997 | 743        | Los Angeles Mission College                | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
|     446 | 743        | Los Angeles Mission College                | 2196          | Summer 2019                                                                           | Summer | 2019-2020    |
|     256 | 743        | Los Angeles Mission College                | 2198          | Fall 2019                                                                             | Fall   | 2019-2020    |
|       8 | 743        | Los Angeles Mission College                | 22018         | Winter 2018                                                                           | Winter | 2017-2018    |
|       3 | 743        | Los Angeles Mission College                | 42018         | Spring 2018                                                                           | Spring | 2017-2018    |
|    1233 | 744        | Los Angeles Pierce College                 | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
|    3667 | 744        | Los Angeles Pierce College                 | 2152          | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
|    1985 | 744        | Los Angeles Pierce College                 | 2154          | Spring 2015                                                                           | Spring | 2014-2015    |
|       1 | 744        | Los Angeles Pierce College                 | 2156          | Summer 2015                                                                           | Summer | 2015-2016    |
|   10183 | 744        | Los Angeles Pierce College                 | 2156          | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
|    6360 | 744        | Los Angeles Pierce College                 | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
|    2624 | 744        | Los Angeles Pierce College                 | 2162          | Winter 2016                                                                           | Winter | 2015-2016    |
|    6284 | 744        | Los Angeles Pierce College                 | 2164          | Spring 2016                                                                           | Spring | 2015-2016    |
|    7440 | 744        | Los Angeles Pierce College                 | 2166          | Summer 2016                                                                           | Summer | 2016-2017    |
|   13292 | 744        | Los Angeles Pierce College                 | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
|    3314 | 744        | Los Angeles Pierce College                 | 2172          | Winter 2017                                                                           | Winter | 2016-2017    |
|    7367 | 744        | Los Angeles Pierce College                 | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
|      45 | 744        | Los Angeles Pierce College                 | 2176          | ENCORE students ONLY - Summer 2017                                                    | Summer | 2017-2018    |
|    8189 | 744        | Los Angeles Pierce College                 | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
|   12416 | 744        | Los Angeles Pierce College                 | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
|    3259 | 744        | Los Angeles Pierce College                 | 2182          | Winter 2018                                                                           | Winter | 2017-2018    |
|    6956 | 744        | Los Angeles Pierce College                 | 2184          | Spring 2018                                                                           | Spring | 2017-2018    |
|     206 | 744        | Los Angeles Pierce College                 | 2186          | Summer 2017                                                                           | Summer | 2017-2018    |
|    7127 | 744        | Los Angeles Pierce College                 | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
|   10452 | 744        | Los Angeles Pierce College                 | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1968 | 744        | Los Angeles Pierce College                 | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
|    2102 | 744        | Los Angeles Pierce College                 | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
|     602 | 744        | Los Angeles Pierce College                 | 2196          | Summer 2019                                                                           | Summer | 2019-2020    |
|     672 | 744        | Los Angeles Pierce College                 | 2198          | Fall 2019                                                                             | Fall   | 2019-2020    |
|    1603 | 745        | Los Angeles Southwest College              | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
|    1247 | 745        | Los Angeles Southwest College              | 2152          | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
|     871 | 745        | Los Angeles Southwest College              | 2154          | Spring 2015                                                                           | Spring | 2014-2015    |
|    2718 | 745        | Los Angeles Southwest College              | 2156          | Summer 2015                                                                           | Summer | 2015-2016    |
|    2417 | 745        | Los Angeles Southwest College              | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
|    3061 | 745        | Los Angeles Southwest College              | 2162          | Winter/Spring 2016                                                                    | Winter | 2015-2016    |
|    1586 | 745        | Los Angeles Southwest College              | 2166          | Summer 2016                                                                           | Summer | 2016-2017    |
|    3607 | 745        | Los Angeles Southwest College              | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
|     787 | 745        | Los Angeles Southwest College              | 2172          | Winter 2017                                                                           | Winter | 2016-2017    |
|    1569 | 745        | Los Angeles Southwest College              | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
|    2241 | 745        | Los Angeles Southwest College              | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
|    5160 | 745        | Los Angeles Southwest College              | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
|    1466 | 745        | Los Angeles Southwest College              | 2182          | Winter 2018                                                                           | Winter | 2017-2018    |
|    3377 | 745        | Los Angeles Southwest College              | 2184          | Spring 2018                                                                           | Spring | 2017-2018    |
|    2796 | 745        | Los Angeles Southwest College              | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
|    4607 | 745        | Los Angeles Southwest College              | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
|     863 | 745        | Los Angeles Southwest College              | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
|     592 | 745        | Los Angeles Southwest College              | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
|      81 | 745        | Los Angeles Southwest College              | 2196          | Summer 2019                                                                           | Summer | 2019-2020    |
|      99 | 745        | Los Angeles Southwest College              | 2198          | Fall 2019                                                                             | Fall   | 2019-2020    |
|    3677 | 746        | Los Angeles Trade Technical College        | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
|    1882 | 746        | Los Angeles Trade Technical College        | 2152          | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
|    5861 | 746        | Los Angeles Trade Technical College        | 2154          | Spring 2015                                                                           | Spring | 2014-2015    |
|    4226 | 746        | Los Angeles Trade Technical College        | 2156          | Summer 2015                                                                           | Summer | 2015-2016    |
|     271 | 746        | Los Angeles Trade Technical College        | 2157          | FALL 2015                                                                             | Fall   | 2015-2016    |
|    8239 | 746        | Los Angeles Trade Technical College        | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
|       1 | 746        | Los Angeles Trade Technical College        | 2162          | Winter/Spring 2016                                                                    | Winter | 2015-2016    |
|    1694 | 746        | Los Angeles Trade Technical College        | 2162          | Winter 2016                                                                           | Winter | 2015-2016    |
|    4165 | 746        | Los Angeles Trade Technical College        | 2164          | Spring 2016                                                                           | Spring | 2015-2016    |
|    3895 | 746        | Los Angeles Trade Technical College        | 2166          | SUMMER 2016                                                                           | Summer | 2016-2017    |
|    8089 | 746        | Los Angeles Trade Technical College        | 2168          | FALL 2016                                                                             | Fall   | 2016-2017    |
|    1709 | 746        | Los Angeles Trade Technical College        | 2172          | WINTER 2017                                                                           | Winter | 2016-2017    |
|    4319 | 746        | Los Angeles Trade Technical College        | 2174          | SPRING 2017                                                                           | Spring | 2016-2017    |
|    4743 | 746        | Los Angeles Trade Technical College        | 2176          | SUMMER2017                                                                            | Summer | 2017-2018    |
|   11377 | 746        | Los Angeles Trade Technical College        | 2178          | FALL 2017                                                                             | Fall   | 2017-2018    |
|    2964 | 746        | Los Angeles Trade Technical College        | 2182          | WINTER 2018                                                                           | Winter | 2017-2018    |
|    6831 | 746        | Los Angeles Trade Technical College        | 2184          | SPRING 2018                                                                           | Spring | 2017-2018    |
|    4919 | 746        | Los Angeles Trade Technical College        | 2186          | SUMMER 2018                                                                           | Summer | 2018-2019    |
|   10205 | 746        | Los Angeles Trade Technical College        | 2188          | FALL 2018                                                                             | Fall   | 2018-2019    |
|    2007 | 746        | Los Angeles Trade Technical College        | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
|    1294 | 746        | Los Angeles Trade Technical College        | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
|      13 | 746        | Los Angeles Trade Technical College        | 2196          | Summer 2019                                                                           | Summer | 2019-2020    |
|     295 | 746        | Los Angeles Trade Technical College        | 2198          | Fall 2019                                                                             | Fall   | 2019-2020    |
|    1448 | 747        | Los Angeles Valley College                 | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
|    2583 | 747        | Los Angeles Valley College                 | 2152          | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
|    2854 | 747        | Los Angeles Valley College                 | 2154          | Spring 2015                                                                           | Spring | 2014-2015    |
|    6513 | 747        | Los Angeles Valley College                 | 2156          | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
|    5496 | 747        | Los Angeles Valley College                 | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
|      64 | 747        | Los Angeles Valley College                 | 2162          | winter 2016                                                                           | Winter | 2015-2016    |
|    2622 | 747        | Los Angeles Valley College                 | 2162          | winter/spring 2016                                                                    | Winter | 2015-2016    |
|    2144 | 747        | Los Angeles Valley College                 | 2164          | Spring 2016                                                                           | Spring | 2015-2016    |
|    7266 | 747        | Los Angeles Valley College                 | 2166          | summer/fall 2016                                                                      | Summer | 2016-2017    |
|    4108 | 747        | Los Angeles Valley College                 | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
|    1406 | 747        | Los Angeles Valley College                 | 2172          | Winter 2017                                                                           | Winter | 2016-2017    |
|     170 | 747        | Los Angeles Valley College                 | 2174          | Spring 2017 Late Start Classes                                                        | Spring | 2016-2017    |
|    3204 | 747        | Los Angeles Valley College                 | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
|    4139 | 747        | Los Angeles Valley College                 | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
|    8932 | 747        | Los Angeles Valley College                 | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
|    1543 | 747        | Los Angeles Valley College                 | 2182          | Winter 2018                                                                           | Winter | 2017-2018    |
|    5954 | 747        | Los Angeles Valley College                 | 2184          | Spring 2018                                                                           | Spring | 2017-2018    |
|    5187 | 747        | Los Angeles Valley College                 | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
|    8452 | 747        | Los Angeles Valley College                 | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1676 | 747        | Los Angeles Valley College                 | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
|    1717 | 747        | Los Angeles Valley College                 | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
|     331 | 747        | Los Angeles Valley College                 | 2194          | Spring 2018                                                                           | Spring | 2017-2018    |
|     336 | 747        | Los Angeles Valley College                 | 2196          | Summer 2019                                                                           | Summer | 2019-2020    |
|     347 | 747        | Los Angeles Valley College                 | 2198          | Fall 2019                                                                             | Fall   | 2019-2020    |
|       2 | 747        | Los Angeles Valley College                 | 2198          | Fall 2019 Applications                                                                | Fall   | 2019-2020    |
|      57 | 747        | Los Angeles Valley College                 | 22016         | winter/spring 2016                                                                    | Winter | 2015-2016    |
|      45 | 747        | Los Angeles Valley College                 | 62016         | summer/fall 2016                                                                      | Summer | 2016-2017    |
|    3159 | 748        | East Los Angeles College                   | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
|    7087 | 748        | East Los Angeles College                   | 2152          | Winter/Spring                                                                         | Winter | 2014-2015    |
|     118 | 748        | East Los Angeles College                   | 2152          | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
|     937 | 748        | East Los Angeles College                   | 2154          | Spring                                                                                | Spring | 2014-2015    |
|       7 | 748        | East Los Angeles College                   | 2156          | Summer 2015                                                                           | Summer | 2015-2016    |
|   16519 | 748        | East Los Angeles College                   | 2156          | Summer 2015/Fall 2015                                                                 | Summer | 2015-2016    |
|     714 | 748        | East Los Angeles College                   | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
|    2563 | 748        | East Los Angeles College                   | 2162          | Winter 2016                                                                           | Winter | 2015-2016    |
|    4509 | 748        | East Los Angeles College                   | 2164          | Spring 2016                                                                           | Spring | 2015-2016    |
|    5788 | 748        | East Los Angeles College                   | 2166          | Summer 2016                                                                           | Summer | 2016-2017    |
|   10560 | 748        | East Los Angeles College                   | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
|    1838 | 748        | East Los Angeles College                   | 2172          | Winter 2017                                                                           | Winter | 2016-2017    |
|    3414 | 748        | East Los Angeles College                   | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
|    9751 | 748        | East Los Angeles College                   | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
|   14520 | 748        | East Los Angeles College                   | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
|    2304 | 748        | East Los Angeles College                   | 2182          | Winter 2018                                                                           | Winter | 2017-2018    |
|   11357 | 748        | East Los Angeles College                   | 2184          | Spring 2018                                                                           | Spring | 2017-2018    |
|   13609 | 748        | East Los Angeles College                   | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
|   14466 | 748        | East Los Angeles College                   | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1802 | 748        | East Los Angeles College                   | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
|    1365 | 748        | East Los Angeles College                   | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
|     118 | 748        | East Los Angeles College                   | 2196          | Summer 2019                                                                           | Summer | 2019-2020    |
|     246 | 748        | East Los Angeles College                   | 2198          | Fall 2019                                                                             | Fall   | 2019-2020    |
|    1433 | 749        | West Los Angeles College                   | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
|     210 | 749        | West Los Angeles College                   | 2152          | Spring 2015                                                                           | Spring | 2014-2015    |
|    1555 | 749        | West Los Angeles College                   | 2152          | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
|       4 | 749        | West Los Angeles College                   | 2154          | Spring                                                                                | Spring | 2014-2015    |
|    1125 | 749        | West Los Angeles College                   | 2154          | Spring 2015                                                                           | Spring | 2014-2015    |
|     122 | 749        | West Los Angeles College                   | 2154          | Spring/Summer/Fall 2015                                                               | Summer | 2015-2016    |
|    3653 | 749        | West Los Angeles College                   | 2156          | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
|    4154 | 749        | West Los Angeles College                   | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
|    2668 | 749        | West Los Angeles College                   | 2162          | Winter/Spring 2016                                                                    | Winter | 2015-2016    |
|    3048 | 749        | West Los Angeles College                   | 2164          | Spring 2016                                                                           | Spring | 2015-2016    |
|    3321 | 749        | West Los Angeles College                   | 2166          | Summer 2016                                                                           | Summer | 2016-2017    |
|    7563 | 749        | West Los Angeles College                   | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
|       1 | 749        | West Los Angeles College                   | 2172          | Winter/Spring 2017                                                                    | Winter | 2016-2017    |
|    1593 | 749        | West Los Angeles College                   | 2172          | Winter 2017                                                                           | Winter | 2016-2017    |
|    4961 | 749        | West Los Angeles College                   | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
|    4242 | 749        | West Los Angeles College                   | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
|    7682 | 749        | West Los Angeles College                   | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
|    2155 | 749        | West Los Angeles College                   | 2182          | Winter 2018                                                                           | Winter | 2017-2018    |
|    4831 | 749        | West Los Angeles College                   | 2184          | Spring 2018                                                                           | Spring | 2017-2018    |
|    4207 | 749        | West Los Angeles College                   | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
|    6999 | 749        | West Los Angeles College                   | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1913 | 749        | West Los Angeles College                   | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
|     780 | 749        | West Los Angeles College                   | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
|     432 | 749        | West Los Angeles College                   | 2196          | Summer 2019                                                                           | Summer | 2019-2020    |
|     170 | 749        | West Los Angeles College                   | 2198          | Fall 2019                                                                             | Fall   | 2019-2020    |
|      64 | 74A        | LA I.T.V.                                  | 2148          | Fall 2014 - Session B                                                                 | Fall   | 2014-2015    |
|       1 | 74A        | LA I.T.V.                                  | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
|      99 | 74A        | LA I.T.V.                                  | 2154          | Spring 2015 - Session A and B                                                         | Spring | 2014-2015    |
|       6 | 74A        | LA I.T.V.                                  | 2156          | Summer 2015                                                                           | Summer | 2015-2016    |
|      40 | 74A        | LA I.T.V.                                  | 2158          | Fall 2015 - Session A                                                                 | Fall   | 2015-2016    |
|       9 | 74A        | LA I.T.V.                                  | 2158          | Fall 2015 - Session B                                                                 | Fall   | 2015-2016    |
|      25 | 74A        | LA I.T.V.                                  | 2164          | Spring 2016 - Session A                                                               | Spring | 2015-2016    |
|      77 | 74A        | LA I.T.V.                                  | 2166          | SUMMER 2016                                                                           | Summer | 2016-2017    |
|      97 | 74A        | LA I.T.V.                                  | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
|     120 | 74A        | LA I.T.V.                                  | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
|      38 | 74A        | LA I.T.V.                                  | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
|      82 | 74A        | LA I.T.V.                                  | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
|       5 | 74A        | ITV/The Weekend College                    | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
|       8 | 74A        | LA I.T.V.                                  | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
|      10 | 74A        | LA I.T.V.                                  | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
|       4 | 74A        | ITV/The Weekend College                    | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
|       2 | 74A        | LA I.T.V.                                  | 22015         | SUMMER 2015                                                                           | Summer | 2015-2016    |
|       2 | 74A        | LA I.T.V.                                  | Fall 2015     | Fall 2015 - Session A                                                                 | Fall   | 2015-2016    |
|      13 | 74A        | LA I.T.V.                                  | Summer 2015   | Summer 2015                                                                           | Summer | 2015-2016    |
|       4 | 74A        | LA I.T.V.                                  | update        | Spring 2015 - Session B                                                               | Spring | 2014-2015    |
|      47 | 771        | Pasadena City College                      | 201530        | Spring 2015                                                                           | Spring | 2014-2015    |
|    7080 | 771        | Pasadena City College                      | 201550        | Summer 2015                                                                           | Summer | 2015-2016    |
|   17985 | 771        | Pasadena City College                      | 201570        | Fall 2015                                                                             | Fall   | 2015-2016    |
|   11034 | 771        | Pasadena City College                      | 201630        | Spring 2016                                                                           | Spring | 2015-2016    |
|   11992 | 771        | Pasadena City College                      | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
|   25131 | 771        | Pasadena City College                      | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    2455 | 771        | Pasadena City College                      | 201710        | Winter 2017 Intersession                                                              | Winter | 2016-2017    |
|    9858 | 771        | Pasadena City College                      | 201720        | Winter / Spring 2017                                                                  | Winter | 2016-2017    |
|    3219 | 771        | Pasadena City College                      | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
|   30069 | 771        | Pasadena City College                      | 201760        | Summer / Fall 2017                                                                    | Summer | 2017-2018    |
|    2902 | 771        | Pasadena City College                      | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    9860 | 771        | Pasadena City College                      | 201820        | Winter/Spring 2018                                                                    | Winter | 2017-2018    |
|    3709 | 771        | Pasadena City College                      | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
|   31321 | 771        | Pasadena City College                      | 201860        | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
|   16033 | 771        | Pasadena City College                      | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    6339 | 771        | Pasadena City College                      | 201920        | Winter/Spring 2019                                                                    | Winter | 2018-2019    |
|    1031 | 771        | Pasadena City College                      | 201960        | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
|    2187 | 781        | Santa Monica College                       | 20172         | Summer 2017                                                                           | Summer | 2017-2018    |
|   11056 | 781        | Santa Monica College                       | 20173         | Fall 2017                                                                             | Fall   | 2017-2018    |
|    5974 | 781        | Santa Monica College                       | 20180         | Winter 2018                                                                           | Winter | 2017-2018    |
|   11746 | 781        | Santa Monica College                       | 20181         | Spring 2018                                                                           | Spring | 2017-2018    |
|   21825 | 781        | Santa Monica College                       | 20182         | Summer 2018                                                                           | Summer | 2018-2019    |
|   32307 | 781        | Santa Monica College                       | 20183         | Fall 2018                                                                             | Fall   | 2018-2019    |
|    9004 | 781        | Santa Monica College                       | 20190         | Winter 2019                                                                           | Winter | 2018-2019    |
|   15151 | 781        | Santa Monica College                       | 20191         | Spring 2019                                                                           | Spring | 2018-2019    |
|    1566 | 781        | Santa Monica College                       | 20192         | Summer 2019                                                                           | Summer | 2019-2020    |
|    2936 | 781        | Santa Monica College                       | 20193         | Fall 2019                                                                             | Fall   | 2019-2020    |
|       1 | 781        | Santa Monica College                       | 78112         | Spring 2012 - Santa Monica College                                                    | Spring | 2011-2012    |
|    2060 | 811        | Cerritos College                           | 1146          | 2014 Summer                                                                           | Summer | 2014-2015    |
|   12289 | 811        | Cerritos College                           | 1149          | 2014 Fall                                                                             | Fall   | 2014-2015    |
|   11712 | 811        | Cerritos College                           | 1153          | 2015 Spring                                                                           | Spring | 2014-2015    |
|    9078 | 811        | Cerritos College                           | 1156          | 2015 Summer                                                                           | Summer | 2015-2016    |
|   18514 | 811        | Cerritos College                           | 1159          | 2015 Fall                                                                             | Fall   | 2015-2016    |
|   11590 | 811        | Cerritos College                           | 1163          | Spring 2016                                                                           | Spring | 2015-2016    |
|    9403 | 811        | Cerritos College                           | 1166          | Summer 2016                                                                           | Summer | 2016-2017    |
|   18305 | 811        | Cerritos College                           | 1169          | Fall 2016                                                                             | Fall   | 2016-2017    |
|   11051 | 811        | Cerritos College                           | 1173          | Spring 2017                                                                           | Spring | 2016-2017    |
|    9572 | 811        | Cerritos College                           | 1176          | Summer 2017                                                                           | Summer | 2017-2018    |
|   17238 | 811        | Cerritos College                           | 1179          | Fall 2017                                                                             | Fall   | 2017-2018    |
|   10791 | 811        | Cerritos College                           | 1183          | Spring 2018                                                                           | Spring | 2017-2018    |
|   10532 | 811        | Cerritos College                           | 1186          | Summer 2018                                                                           | Summer | 2018-2019    |
|   15484 | 811        | Cerritos College                           | 1189          | Fall 2018                                                                             | Fall   | 2018-2019    |
|    6267 | 811        | Cerritos College                           | 1193          | Spring 2019                                                                           | Spring | 2018-2019    |
|     251 | 811        | Cerritos College                           | 1196          | Summer 2019                                                                           | Summer | 2019-2020    |
|     597 | 811        | Cerritos College                           | 1199          | Fall 2019                                                                             | Fall   | 2019-2020    |
|      37 | 821        | Citrus College                             | 201340        | Summer 2013                                                                           | Summer | 2013-2014    |
|    5702 | 821        | Citrus College                             | 201420        | Fall 2013                                                                             | Fall   | 2013-2014    |
|    1014 | 821        | Citrus College                             | 201420        | Application to Citrus College                                                         | Fall   | 2014-2015    |
|    4917 | 821        | Citrus College                             | 201425        | Application to Citrus College                                                         | Winter | 2013-2014    |
|    7647 | 821        | Citrus College                             | 201430        | Application to Citrus College                                                         | Spring | 2013-2014    |
|    2392 | 821        | Citrus College                             | 201440        | Summer 2014 Application                                                               | Summer | 2014-2015    |
|     218 | 821        | Citrus College                             | 201440        | Application to Citrus College                                                         | Summer | 2015-2016    |
|    6794 | 821        | Citrus College                             | 201520        | Fall 2014 Application                                                                 | Fall   | 2014-2015    |
|    2380 | 821        | Citrus College                             | 201525        | Winter 2015 Application                                                               | Winter | 2014-2015    |
|    4179 | 821        | Citrus College                             | 201530        | Spring 2015 Application                                                               | Spring | 2014-2015    |
|    5753 | 821        | Citrus College                             | 201540        | Summer 2015 Application                                                               | Summer | 2015-2016    |
|    8407 | 821        | Citrus College                             | 201620        | Fall 2015 Application                                                                 | Fall   | 2015-2016    |
|    2650 | 821        | Citrus College                             | 201625        | Winter 2016 Application                                                               | Winter | 2015-2016    |
|    4220 | 821        | Citrus College                             | 201630        | Spring 2016 Application                                                               | Spring | 2015-2016    |
|    6157 | 821        | Citrus College                             | 201640        | Summer 2016 Application                                                               | Summer | 2016-2017    |
|    8129 | 821        | Citrus College                             | 201720        | Fall 2016 Application                                                                 | Fall   | 2016-2017    |
|    2799 | 821        | Citrus College                             | 201725        | Winter 2017 Application                                                               | Winter | 2016-2017    |
|    5057 | 821        | Citrus College                             | 201730        | Spring 2017 Application                                                               | Spring | 2016-2017    |
|    6465 | 821        | Citrus College                             | 201740        | Summer 2017 Application                                                               | Summer | 2017-2018    |
|    8261 | 821        | Citrus College                             | 201820        | Fall 2017 Application                                                                 | Fall   | 2017-2018    |
|    2832 | 821        | Citrus College                             | 201825        | Winter 2018                                                                           | Winter | 2017-2018    |
|    4644 | 821        | Citrus College                             | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
|    6794 | 821        | Citrus College                             | 201840        | Summer 2018                                                                           | Summer | 2018-2019    |
|    8864 | 821        | Citrus College                             | 201920        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1988 | 821        | Citrus College                             | 201925        | Winter 2019                                                                           | Winter | 2018-2019    |
|    1628 | 821        | Citrus College                             | 201930        | Spring 2019                                                                           | Spring | 2018-2019    |
|     778 | 831        | Coastline Community College                | 201423        | CCC Fall 2014                                                                         | Fall   | 2014-2015    |
|    7684 | 831        | Coastline Community College                | 201433        | CCC Spring 2015                                                                       | Spring | 2014-2015    |
|    9524 | 831        | Coastline Community College                | 201513        | CCC Summer/Fall 2015                                                                  | Summer | 2015-2016    |
|    5931 | 831        | Coastline Community College                | 201523        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    8721 | 831        | Coastline Community College                | 201533        | CCC Winter Intersession/Spring 2016                                                   | Winter | 2015-2016    |
|    9431 | 831        | Coastline Community College                | 201613        | CCC Summer/Fall 2016                                                                  | Summer | 2016-2017    |
|    6678 | 831        | Coastline Community College                | 201623        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    8380 | 831        | Coastline Community College                | 201633        | CCC Winter Intersession/Spring 2017                                                   | Winter | 2016-2017    |
|    9703 | 831        | Coastline Community College                | 201713        | CCC Summer / Fall 2017                                                                | Summer | 2017-2018    |
|    5600 | 831        | Coastline Community College                | 201723        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    8233 | 831        | Coastline Community College                | 201733        | CCC Winter Intersession/Spring 2018                                                   | Winter | 2017-2018    |
|   10885 | 831        | Coastline Community College                | 201813        | CCC Summer/Fall 2018                                                                  | Summer | 2018-2019    |
|    5462 | 831        | Coastline Community College                | 201823        | Fall Term 2018                                                                        | Fall   | 2018-2019    |
|    3231 | 831        | Coastline Community College                | 201833        | CCC Intersession/Spring 2019                                                          | Winter | 2018-2019    |
|       9 | 831        | Coastline Community College                | 201911        | CCC Summer/Fall 2019                                                                  | Summer | 2019-2020    |
|     133 | 831        | Coastline Community College                | 201913        | Intersession/Spring 2019                                                              | Winter | 2018-2019    |
|     289 | 831        | Coastline Community College                | 201913        | CCC Summer/Fall 2019                                                                  | Summer | 2019-2020    |
|     269 | 832        | Golden West College                        | 201422        | GWC Fall 2014                                                                         | Fall   | 2014-2015    |
|     222 | 832        | Golden West College                        | 201432        | GWC Spring 2015                                                                       | Spring | 2014-2015    |
|    5964 | 832        | Golden West College                        | 201432        | GWC Winter Intersession / Spring 2015                                                 | Winter | 2014-2015    |
|   11063 | 832        | Golden West College                        | 201512        | GWC Summer / Fall 2015                                                                | Summer | 2015-2016    |
|    3265 | 832        | Golden West College                        | 201522        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    6994 | 832        | Golden West College                        | 201532        | GWC Winter Intersession / Spring 2016                                                 | Winter | 2015-2016    |
|   11425 | 832        | Golden West College                        | 201612        | GWC Summer / Fall 2016                                                                | Summer | 2016-2017    |
|    3687 | 832        | Golden West College                        | 201622        | Fall 2016 Semester                                                                    | Fall   | 2016-2017    |
|    6793 | 832        | Golden West College                        | 201632        | GWC Winter Intersession / Spring 2017                                                 | Winter | 2016-2017    |
|   11580 | 832        | Golden West College                        | 201712        | GWC Summer / Fall 2017                                                                | Summer | 2017-2018    |
|    4789 | 832        | Golden West College                        | 201722        | GWC Fall 2017                                                                         | Fall   | 2017-2018    |
|    7144 | 832        | Golden West College                        | 201732        | GWC Winter Intersession / Spring 2018                                                 | Winter | 2017-2018    |
|   12943 | 832        | Golden West College                        | 201812        | GWC Summer / Fall 2018                                                                | Summer | 2018-2019    |
|    5233 | 832        | Golden West College                        | 201822        | GWC Fall 2018                                                                         | Fall   | 2018-2019    |
|    3801 | 832        | Golden West College                        | 201832        | GWC Winter Intersession / Spring 2019                                                 | Winter | 2018-2019    |
|    1546 | 832        | Golden West College                        | 201912        | GWC Summer / Fall 2019                                                                | Summer | 2019-2020    |
|     546 | 833        | Orange Coast College                       | 201421        | OCC Fall 2014                                                                         | Fall   | 2014-2015    |
|    3819 | 833        | Orange Coast College                       | 201431        | OCC Spring 2015                                                                       | Spring | 2014-2015    |
|    5715 | 833        | Orange Coast College                       | 201431        | OCC Intersession/Spring 2015                                                          | Winter | 2014-2015    |
|   18560 | 833        | Orange Coast College                       | 201511        | OCC Summer/Fall 2015                                                                  | Summer | 2015-2016    |
|    5935 | 833        | Orange Coast College                       | 201521        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    9603 | 833        | Orange Coast College                       | 201531        | Intersession/Spring 2016                                                              | Winter | 2015-2016    |
|   17304 | 833        | Orange Coast College                       | 201611        | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
|    6362 | 833        | Orange Coast College                       | 201621        | Fall 2016                                                                             | Fall   | 2016-2017    |
|   10167 | 833        | Orange Coast College                       | 201631        | Intersession/Spring 2017                                                              | Winter | 2016-2017    |
|   19010 | 833        | Orange Coast College                       | 201711        | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
|    5747 | 833        | Orange Coast College                       | 201721        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    9480 | 833        | Orange Coast College                       | 201731        | Intersession/Spring 2018                                                              | Winter | 2017-2018    |
|   17507 | 833        | Orange Coast College                       | 201811        | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
|    5148 | 833        | Orange Coast College                       | 201821        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    4703 | 833        | Orange Coast College                       | 201831        | OCC Intersession/Spring 2019                                                          | Winter | 2018-2019    |
|    2079 | 833        | Orange Coast College                       | 201911        | OCC Summer/Fall 2019                                                                  | Summer | 2019-2020    |
|    5184 | 841        | Long Beach City College                    | 1565          | 2016 Fall                                                                             | Fall   | 2016-2017    |
|    4125 | 841        | Long Beach City College                    | 1570          | 2017 Winter                                                                           | Winter | 2016-2017    |
|    9740 | 841        | Long Beach City College                    | 1575          | 2017 Spring                                                                           | Spring | 2016-2017    |
|    8027 | 841        | Long Beach City College                    | 1580          | 2017 Summer                                                                           | Summer | 2017-2018    |
|   18698 | 841        | Long Beach City College                    | 1585          | 2017 Fall                                                                             | Fall   | 2017-2018    |
|    3667 | 841        | Long Beach City College                    | 1590          | 2018 Winter                                                                           | Winter | 2017-2018    |
|    8784 | 841        | Long Beach City College                    | 1595          | 2018 Spring                                                                           | Spring | 2017-2018    |
|    7270 | 841        | Long Beach City College                    | 1600          | 2018 Summer                                                                           | Summer | 2018-2019    |
|   16927 | 841        | Long Beach City College                    | 1605          | 2018 Fall                                                                             | Fall   | 2018-2019    |
|    3260 | 841        | Long Beach City College                    | 1610          | 2019 Winter                                                                           | Winter | 2018-2019    |
|    2897 | 841        | Long Beach City College                    | 1615          | 2019 Spring                                                                           | Spring | 2018-2019    |
|    1871 | 841        | Long Beach City College                    | 1620          | 2019 Summer/Fall                                                                      | Summer | 2019-2020    |
|      21 | 851        | Mt San Antonio College                     | 201340        | Spring 2014                                                                           | Spring | 2013-2014    |
|    1658 | 851        | Mt San Antonio College                     | 201410        | Summer 2014                                                                           | Summer | 2014-2015    |
|   13466 | 851        | Mt San Antonio College                     | 201420        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    5943 | 851        | Mt San Antonio College                     | 201430        | Winter 2015                                                                           | Winter | 2014-2015    |
|   11549 | 851        | Mt San Antonio College                     | 201440        | Spring 2015                                                                           | Spring | 2014-2015    |
|   12499 | 851        | Mt San Antonio College                     | 201510        | Summer 2015                                                                           | Summer | 2015-2016    |
|   24415 | 851        | Mt San Antonio College                     | 201520        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    5703 | 851        | Mt San Antonio College                     | 201530        | Winter 2016                                                                           | Winter | 2015-2016    |
|   11102 | 851        | Mt San Antonio College                     | 201540        | Spring 2016                                                                           | Spring | 2015-2016    |
|   12153 | 851        | Mt San Antonio College                     | 201610        | Summer 2016                                                                           | Summer | 2016-2017    |
|   24439 | 851        | Mt San Antonio College                     | 201620        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    6061 | 851        | Mt San Antonio College                     | 201630        | Winter 2017                                                                           | Winter | 2016-2017    |
|   11995 | 851        | Mt San Antonio College                     | 201640        | Spring 2017                                                                           | Spring | 2016-2017    |
|   13171 | 851        | Mt San Antonio College                     | 201710        | Summer 2017                                                                           | Summer | 2017-2018    |
|     338 | 851        | Mt San Antonio College                     | 201720        | Fall 1017                                                                             | Fall   | 2017-2018    |
|   24902 | 851        | Mt San Antonio College                     | 201720        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    5685 | 851        | Mt San Antonio College                     | 201730        | Winter 2018                                                                           | Winter | 2017-2018    |
|   12313 | 851        | Mt San Antonio College                     | 201740        | Spring 2018                                                                           | Spring | 2017-2018    |
|   11815 | 851        | Mt San Antonio College                     | 201810        | Summer 2018                                                                           | Summer | 2018-2019    |
|   24604 | 851        | Mt San Antonio College                     | 201820        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    5394 | 851        | Mt San Antonio College                     | 201830        | Winter 2019                                                                           | Winter | 2018-2019    |
|    2328 | 851        | Mt San Antonio College                     | 201840        | Spring 2019                                                                           | Spring | 2018-2019    |
|     469 | 851        | Mt San Antonio College                     | 201910        | Summer 2019                                                                           | Summer | 2019-2020    |
|    2194 | 851        | Mt San Antonio College                     | 201920        | Fall 2019                                                                             | Fall   | 2019-2020    |
|      50 | 861        | Cypress College                            | 201320        | Spring 2014                                                                           | Spring | 2013-2014    |
|    4096 | 861        | Cypress College                            | 201330        | Summer 2014                                                                           | Summer | 2014-2015    |
|   10670 | 861        | Cypress College                            | 201410        | Fall 2014                                                                             | Fall   | 2014-2015    |
|    8045 | 861        | Cypress College                            | 201420        | Spring 2015                                                                           | Spring | 2014-2015    |
|    5047 | 861        | Cypress College                            | 201430        | Summer 2015                                                                           | Summer | 2015-2016    |
|   11889 | 861        | Cypress College                            | 201510        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    8278 | 861        | Cypress College                            | 201520        | Spring 2016                                                                           | Spring | 2015-2016    |
|    5025 | 861        | Cypress College                            | 201530        | summer 2016                                                                           | Summer | 2016-2017    |
|   11052 | 861        | Cypress College                            | 201610        | fall 2016                                                                             | Fall   | 2016-2017    |
|    7409 | 861        | Cypress College                            | 201620        | spring 2017                                                                           | Spring | 2016-2017    |
|    5351 | 861        | Cypress College                            | 201630        | Summer 2017                                                                           | Summer | 2017-2018    |
|   11680 | 861        | Cypress College                            | 201710        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    7388 | 861        | Cypress College                            | 201720        | Spring 2018                                                                           | Spring | 2017-2018    |
|    5710 | 861        | Cypress College                            | 201730        | Summer 2018                                                                           | Summer | 2018-2019    |
|   11994 | 861        | Cypress College                            | 201810        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    4306 | 861        | Cypress College                            | 201820        | Spring 2019                                                                           | Spring | 2018-2019    |
|     102 | 861        | Cypress College                            | 201830        | Summer 2019                                                                           | Summer | 2019-2020    |
|     721 | 861        | Cypress College                            | 201910        | Fall 2019                                                                             | Fall   | 2019-2020    |
|     244 | 862        | Fullerton College                          | 201320        | Spring 2014                                                                           | Spring | 2013-2014    |
|    8261 | 862        | Fullerton College                          | 201330        | Summer 2014                                                                           | Summer | 2014-2015    |
|   14587 | 862        | Fullerton College                          | 201410        | Fall 2014                                                                             | Fall   | 2014-2015    |
|   13242 | 862        | Fullerton College                          | 201420        | Spring 2015                                                                           | Spring | 2014-2015    |
|    9355 | 862        | Fullerton College                          | 201430        | Summer 2015                                                                           | Summer | 2015-2016    |
|   15971 | 862        | Fullerton College                          | 201510        | Fall 2015                                                                             | Fall   | 2015-2016    |
|   13146 | 862        | Fullerton College                          | 201520        | Spring 2016                                                                           | Spring | 2015-2016    |
|    9058 | 862        | Fullerton College                          | 201530        | Summer 2016                                                                           | Summer | 2016-2017    |
|   14911 | 862        | Fullerton College                          | 201610        | Fall 2016                                                                             | Fall   | 2016-2017    |
|   12524 | 862        | Fullerton College                          | 201620        | Spring 2017                                                                           | Spring | 2016-2017    |
|    9188 | 862        | Fullerton College                          | 201630        | Summer 2017                                                                           | Summer | 2017-2018    |
|   18734 | 862        | Fullerton College                          | 201710        | Fall 2017                                                                             | Fall   | 2017-2018    |
|   10643 | 862        | Fullerton College                          | 201720        | Spring 2018                                                                           | Spring | 2017-2018    |
|    7836 | 862        | Fullerton College                          | 201730        | Summer 2018                                                                           | Summer | 2018-2019    |
|   16595 | 862        | Fullerton College                          | 201810        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    5610 | 862        | Fullerton College                          | 201820        | Spring 2019                                                                           | Spring | 2018-2019    |
|     189 | 862        | Fullerton College                          | 201830        | Summer 2019                                                                           | Summer | 2019-2020    |
|    1769 | 862        | Fullerton College                          | 201910        | Fall 2019                                                                             | Fall   | 2019-2020    |
|    7622 | 871        | Santa Ana College                          | 2013FA        | Fall 2013 - Santa Ana College                                                         | Fall   | 2013-2014    |
|     186 | 871        | Santa Ana College                          | 2013SU        | Summer 2013 - Santa Ana College                                                       | Summer | 2013-2014    |
|   15205 | 871        | Santa Ana College                          | 2014FA        | Fall 2014 - Santa Ana College                                                         | Fall   | 2014-2015    |
|    1234 | 871        | Santa Ana College                          | 2014SI        | Spring Intersession 2014 - Santa Ana College                                          | Winter | 2013-2014    |
|   10243 | 871        | Santa Ana College                          | 2014SP        | Spring 2014 - Santa Ana college                                                       | Spring | 2013-2014    |
|    4867 | 871        | Santa Ana College                          | 2014SU        | Summer 2014 - Santa Ana College                                                       | Summer | 2014-2015    |
|   15097 | 871        | Santa Ana College                          | 2015FA        | Fall 2015 - Santa Ana College                                                         | Fall   | 2015-2016    |
|    1481 | 871        | Santa Ana College                          | 2015SI        | Spring Intersession 2015 - Santa Ana College                                          | Winter | 2014-2015    |
|    9618 | 871        | Santa Ana College                          | 2015SP        | Spring 2015 - Santa Ana College                                                       | Spring | 2014-2015    |
|    5061 | 871        | Santa Ana College                          | 2015SU        | Summer 2015 - Santa Ana College                                                       | Summer | 2015-2016    |
|   14792 | 871        | Santa Ana College                          | 2016FA        | Fall 2016 - Santa Ana College                                                         | Fall   | 2016-2017    |
|    1504 | 871        | Santa Ana College                          | 2016SI        | Spring Intersession 2016 - Santa Ana College                                          | Winter | 2015-2016    |
|    8737 | 871        | Santa Ana College                          | 2016SP        | Spring 2016 - Santa Ana College                                                       | Spring | 2015-2016    |
|    4617 | 871        | Santa Ana College                          | 2016SU        | Summer 2016 - Santa Ana College                                                       | Summer | 2016-2017    |
|   14769 | 871        | Santa Ana College                          | 2017FA        | Fall 2017 - Santa Ana College                                                         | Fall   | 2017-2018    |
|    1470 | 871        | Santa Ana College                          | 2017SI        | Spring Intersession 2017 - Santa Ana College                                          | Winter | 2016-2017    |
|    6436 | 871        | Santa Ana College                          | 2017SP        | Spring 2017 - Santa Ana College                                                       | Spring | 2016-2017    |
|    4218 | 871        | Santa Ana College                          | 2017SU        | Summer 2017 - Santa Ana College                                                       | Summer | 2017-2018    |
|   15681 | 871        | Santa Ana College                          | 2018FA        | Fall 2018 - Santa Ana College                                                         | Fall   | 2018-2019    |
|    1340 | 871        | Santa Ana College                          | 2018SI        | Spring Intersession 2018 - Santa Ana College                                          | Winter | 2017-2018    |
|    6010 | 871        | Santa Ana College                          | 2018SP        | Spring 2018 - Santa Ana College                                                       | Spring | 2017-2018    |
|    4700 | 871        | Santa Ana College                          | 2018SU        | Summer 2018 - Santa Ana College                                                       | Summer | 2018-2019    |
|    3488 | 871        | Santa Ana College                          | 2019FA        | Fall 2019 - Santa Ana College                                                         | Fall   | 2019-2020    |
|     817 | 871        | Santa Ana College                          | 2019SI        | Spring Intersession 2019 - Santa Ana College                                          | Winter | 2018-2019    |
|    2018 | 871        | Santa Ana College                          | 2019SP        | Spring 2019 - Santa Ana College                                                       | Spring | 2018-2019    |
|    4509 | 873        | Santiago Canyon College                    | 2013FA        | Fall 2013 - Santiago Canyon College                                                   | Fall   | 2013-2014    |
|     103 | 873        | Santiago Canyon College                    | 2013FA        | Fall 2013 - 8/26/13 - 12/15/13                                                        | Fall   | 2013-2014    |
|     174 | 873        | Santiago Canyon College                    | 2013SU        | Summer 2013 - Santiago Canyon College                                                 | Summer | 2013-2014    |
|    6721 | 873        | Santiago Canyon College                    | 2014FA        | Fall 2014 - 8/25/214 - 12/14/2014                                                     | Fall   | 2014-2015    |
|      84 | 873        | Santiago Canyon College                    | 2014FA        | Fall 2014 - 8/25/2014 - 12/14/2014                                                    | Fall   | 2014-2015    |
|    1429 | 873        | Santiago Canyon College                    | 2014FA        | Fall 2014: (Currently Open for High School Seniors Only)                              | Fall   | 2014-2015    |
|     600 | 873        | Santiago Canyon College                    | 2014SI        | Intersession 2014 - 01/06/14 - 02/02/14                                               | Winter | 2013-2014    |
|     169 | 873        | Santiago Canyon College                    | 2014SI        | Intersession 2014 - Santiago Canyon College                                           | Winter | 2013-2014    |
|     442 | 873        | Santiago Canyon College                    | 2014SP        | Spring 2014 - Santiago Canyon College                                                 | Spring | 2013-2014    |
|    5331 | 873        | Santiago Canyon College                    | 2014SP        | Spring 2014 - 02/10/14 - 06/08/14                                                     | Spring | 2013-2014    |
|    3583 | 873        | Santiago Canyon College                    | 2014SU        | Summer 2014 - 6/16/14 - 8/10/14                                                       | Summer | 2014-2015    |
|    6552 | 873        | Santiago Canyon College                    | 2015FA        | Fall 2015 - 8/24/2015 to 12/13/2015                                                   | Fall   | 2015-2016    |
|    1545 | 873        | Santiago Canyon College                    | 2015FA        | Fall 2015 - 8/24/2015 - 12/13/2015                                                    | Fall   | 2015-2016    |
|     783 | 873        | Santiago Canyon College                    | 2015SI        | Intersession 2015 - 01/05/15  - 02/01/15                                              | Winter | 2014-2015    |
|    5159 | 873        | Santiago Canyon College                    | 2015SP        | Spring 2015 - 02/09/15 - 06/07/15                                                     | Spring | 2014-2015    |
|    3675 | 873        | Santiago Canyon College                    | 2015SU        | Summer 2015 - 06/15/15 to 08/16/15                                                    | Summer | 2015-2016    |
|    6596 | 873        | Santiago Canyon College                    | 2016FA        | Fall 2016 - 8/22/16 - 12/11/16                                                        | Fall   | 2016-2017    |
|    1614 | 873        | Santiago Canyon College                    | 2016FA        | Fall 2016 - 8/22/16 - 12/11/16 (High School Seniors Only)                             | Fall   | 2016-2017    |
|     908 | 873        | Santiago Canyon College                    | 2016SI        | Intersession 2016 - 01/04/16 - 01/31/16                                               | Winter | 2015-2016    |
|    5886 | 873        | Santiago Canyon College                    | 2016SP        | Spring 2016 - 02/08/16 to 06/05/16                                                    | Spring | 2015-2016    |
|    3559 | 873        | Santiago Canyon College                    | 2016SU        | Summer 2016 - 06/13/16 to 08/14/16                                                    | Summer | 2016-2017    |
|    1859 | 873        | Santiago Canyon College                    | 2017FA        | Fall 2017 (Application for High School Seniors Only)                                  | Fall   | 2017-2018    |
|    6561 | 873        | Santiago Canyon College                    | 2017FA        | Fall 2017:  8/28/2017 - 12/17/2017                                                    | Fall   | 2017-2018    |
|    1031 | 873        | Santiago Canyon College                    | 2017SI        | Intersession 2017:  01/09/17 - 02/05/17                                               | Winter | 2016-2017    |
|    5487 | 873        | Santiago Canyon College                    | 2017SP        | Spring 2017:  02/13/17 - 06/11/17                                                     | Spring | 2016-2017    |
|    3859 | 873        | Santiago Canyon College                    | 2017SU        | Summer 2017:  06/19/17 - 08/13/17                                                     | Summer | 2017-2018    |
|    6723 | 873        | Santiago Canyon College                    | 2018FA        | Fall 2018:  8/20/18 - 12/9/18                                                         | Fall   | 2018-2019    |
|    1785 | 873        | Santiago Canyon College                    | 2018FA        | Fall 2018 (Currently for High School Seniors Only)                                    | Fall   | 2018-2019    |
|     664 | 873        | Santiago Canyon College                    | 2018SI        | Intersession 2018 - 01/02/2018 - 01/28/2018                                           | Winter | 2017-2018    |
|    5012 | 873        | Santiago Canyon College                    | 2018SP        | Spring 2018 - 02/05/2018 - 06/03/2018                                                 | Spring | 2017-2018    |
|    3526 | 873        | Santiago Canyon College                    | 2018SU        | Summer 2018: 06/18/18 - 08/12/18                                                      | Summer | 2018-2019    |
|     725 | 873        | Santiago Canyon College                    | 2019FA        | Fall 2019 Early Welcome (Graduating High School Seniors Only)                         | Fall   | 2019-2020    |
|     519 | 873        | Santiago Canyon College                    | 2019SI        | Intersession 2019: 01/07/19 - 02/03/19                                                | Winter | 2018-2019    |
|     855 | 873        | Santiago Canyon College                    | 2019SP        | Spring 2019: 02/11/19 - 06/08/19                                                      | Spring | 2018-2019    |
|     171 | 881        | Rio Hondo College                          | 201350        | Summer 2013                                                                           | Summer | 2013-2014    |
|    9034 | 881        | Rio Hondo College                          | 201370        | Fall 2013                                                                             | Fall   | 2013-2014    |
|   11205 | 881        | Rio Hondo College                          | 201430        | Spring 2014                                                                           | Spring | 2013-2014    |
|       1 | 881        | Rio Hondo College                          | 201430        | Spring 20014                                                                          | Spring | 2013-2014    |
|    9026 | 881        | Rio Hondo College                          | 201450        | Summer 2014                                                                           | Summer | 2014-2015    |
|   12845 | 881        | Rio Hondo College                          | 201470        | Fall 2014                                                                             | Fall   | 2014-2015    |
|   10761 | 881        | Rio Hondo College                          | 201530        | Spring 2015                                                                           | Spring | 2014-2015    |
|    8310 | 881        | Rio Hondo College                          | 201550        | Summer 2015                                                                           | Summer | 2015-2016    |
|   14671 | 881        | Rio Hondo College                          | 201570        | Fall 2015                                                                             | Fall   | 2015-2016    |
|   11837 | 881        | Rio Hondo College                          | 201630        | Spring 2016                                                                           | Spring | 2015-2016    |
|    8713 | 881        | Rio Hondo College                          | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
|   14954 | 881        | Rio Hondo College                          | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
|   12974 | 881        | Rio Hondo College                          | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
|    9202 | 881        | Rio Hondo College                          | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
|   24810 | 881        | Rio Hondo College                          | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
|   14174 | 881        | Rio Hondo College                          | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
|   24172 | 881        | Rio Hondo College                          | 201850        | Summer 2018                                                                           | Summer | 2018-2019    |
|   17817 | 881        | Rio Hondo College                          | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
|    6706 | 881        | Rio Hondo College                          | 201930        | Spring 2019                                                                           | Spring | 2018-2019    |
|     209 | 881        | Rio Hondo College                          | 201950        | Summer 2019                                                                           | Summer | 2019-2020    |
|    1119 | 881        | Rio Hondo College                          | 201970        | Fall 2019                                                                             | Fall   | 2019-2020    |
|     737 | 891        | Saddleback College                         | 20142         | Summer 2014                                                                           | Summer | 2014-2015    |
|    8872 | 891        | Saddleback College                         | 20143         | Fall 2014                                                                             | Fall   | 2014-2015    |
|   11627 | 891        | Saddleback College                         | 20151         | Spring 2015                                                                           | Spring | 2014-2015    |
|      15 | 891        | Saddleback College                         | 20151         | Spring 2015 International Students                                                    | Spring | 2014-2015    |
|       7 | 891        | Saddleback College                         | 20152         | Summer 2015 (International Students)                                                  | Summer | 2015-2016    |
|    9283 | 891        | Saddleback College                         | 20152         | Summer 2015                                                                           | Summer | 2015-2016    |
|   12302 | 891        | Saddleback College                         | 20153         | Fall 2015                                                                             | Fall   | 2015-2016    |
|     540 | 891        | Saddleback College                         | 20153         | Fall 2015 (High School Freshman Advantage and International Students)                 | Fall   | 2015-2016    |
|     767 | 891        | Saddleback College                         | 20153         | Fall 2015 (High School Freshman Advantage)                                            | Fall   | 2015-2016    |
|       5 | 891        | Saddleback College                         | 20153         | Fall 2015 (First Time Freshmen)                                                       | Fall   | 2015-2016    |
|   12387 | 891        | Saddleback College                         | 20161         | Spring 2016                                                                           | Spring | 2015-2016    |
|      15 | 891        | Saddleback College                         | 20161         | Spring 2016 - International Students                                                  | Spring | 2015-2016    |
|   10570 | 891        | Saddleback College                         | 20162         | Summer 2016                                                                           | Summer | 2016-2017    |
|   13808 | 891        | Saddleback College                         | 20163         | Fall 2016                                                                             | Fall   | 2016-2017    |
|     965 | 891        | Saddleback College                         | 20163         | Fall 2016 - First Time Freshman                                                       | Fall   | 2016-2017    |
|   13552 | 891        | Saddleback College                         | 20171         | Spring 2017                                                                           | Spring | 2016-2017    |
|   10724 | 891        | Saddleback College                         | 20172         | Summer 2017                                                                           | Summer | 2017-2018    |
|   13980 | 891        | Saddleback College                         | 20173         | Fall 2017                                                                             | Fall   | 2017-2018    |
|     831 | 891        | Saddleback College                         | 20173         | Fall 2017(First Time Freshmen only)                                                   | Fall   | 2017-2018    |
|   13490 | 891        | Saddleback College                         | 20181         | Spring 2018                                                                           | Spring | 2017-2018    |
|   10766 | 891        | Saddleback College                         | 20182         | Summer 2018                                                                           | Summer | 2018-2019    |
|   14279 | 891        | Saddleback College                         | 20183         | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1281 | 891        | Saddleback College                         | 20183         | Fall 2018 - Freshman Advantage                                                        | Fall   | 2018-2019    |
|    7297 | 891        | Saddleback College                         | 20191         | Spring 2019                                                                           | Spring | 2018-2019    |
|     742 | 891        | Saddleback College                         | 20193         | Fall 2019 Freshman Advantage                                                          | Fall   | 2019-2020    |
|       1 | 891        | Saddleback College                         | 89112         | Spring 2012 - Saddleback College                                                      | Spring | 2011-2012    |
|      18 | 891        | Saddleback College                         | Summer 2018   | Summer 2018 Incorrect - closed                                                        | Summer | 2018-2019    |
|      94 | 891        | Saddleback College                         | Summer 2018   | Summer 2018                                                                           | Summer | 2018-2019    |
|     476 | 892        | Irvine Valley College                      | 20142         | Summer 2014                                                                           | Summer | 2014-2015    |
|    6203 | 892        | Irvine Valley College                      | 20143         | Fall 2014                                                                             | Fall   | 2014-2015    |
|    7267 | 892        | Irvine Valley College                      | 20151         | Spring 2015                                                                           | Spring | 2014-2015    |
|      82 | 892        | Irvine Valley College                      | 20151         | Spring 2015 (Int'l Students)                                                          | Spring | 2014-2015    |
|      54 | 892        | Irvine Valley College                      | 20152         | Summer 2015 (Int'l Students Only)                                                     | Summer | 2015-2016    |
|    6147 | 892        | Irvine Valley College                      | 20152         | Summer 2015                                                                           | Summer | 2015-2016    |
|    1102 | 892        | Irvine Valley College                      | 20153         | Fall 2015 (Freshman Advantage Only)                                                   | Fall   | 2015-2016    |
|    9202 | 892        | Irvine Valley College                      | 20153         | Fall 2015                                                                             | Fall   | 2015-2016    |
|     511 | 892        | Irvine Valley College                      | 20153         | Fall 2015 (Freshman Advantage and F-1 Visa Students Only)                             | Fall   | 2015-2016    |
|    6895 | 892        | Irvine Valley College                      | 20161         | Spring 2016                                                                           | Spring | 2015-2016    |
|      62 | 892        | Irvine Valley College                      | 20161         | Spring 2016 (F-1 Visa Students Only)                                                  | Spring | 2015-2016    |
|    7104 | 892        | Irvine Valley College                      | 20162         | Summer 2016                                                                           | Summer | 2016-2017    |
|   10926 | 892        | Irvine Valley College                      | 20163         | Fall 2016                                                                             | Fall   | 2016-2017    |
|     583 | 892        | Irvine Valley College                      | 20163         | Fall 2016 (Freshman Advantage Only)                                                   | Fall   | 2016-2017    |
|    7561 | 892        | Irvine Valley College                      | 20171         | Spring 2017                                                                           | Spring | 2016-2017    |
|       7 | 892        | Irvine Valley College                      | 20172         | 20172                                                                                 | Summer | 2017-2018    |
|    6779 | 892        | Irvine Valley College                      | 20172         | Summer 2017                                                                           | Summer | 2017-2018    |
|   11520 | 892        | Irvine Valley College                      | 20173         | Fall 2017                                                                             | Fall   | 2017-2018    |
|   10006 | 892        | Irvine Valley College                      | 20181         | Spring 2018                                                                           | Spring | 2017-2018    |
|    6518 | 892        | Irvine Valley College                      | 20182         | Summer 2018                                                                           | Summer | 2018-2019    |
|    9883 | 892        | Irvine Valley College                      | 20183         | Fall 2018                                                                             | Fall   | 2018-2019    |
|    1514 | 892        | Irvine Valley College                      | 20183         | Fall 2018 (Freshman Advantage only)                                                   | Fall   | 2018-2019    |
|    4280 | 892        | Irvine Valley College                      | 20191         | Spring 2019                                                                           | Spring | 2018-2019    |
|     778 | 892        | Irvine Valley College                      | 20193         | Fall 2019                                                                             | Fall   | 2019-2020    |
|      15 | 892        | Irvine Valley College                      | Fall 2017     | Fall 2017                                                                             | Fall   | 2017-2018    |
|    2859 | 911        | Barstow Community College                  | 201507        | Fall 2015                                                                             | Fall   | 2015-2016    |
|    3065 | 911        | Barstow Community College                  | 201603        | Spring 2016                                                                           | Spring | 2015-2016    |
|    1474 | 911        | Barstow Community College                  | 201605        | Summer 2016                                                                           | Summer | 2016-2017    |
|    3406 | 911        | Barstow Community College                  | 201607        | Fall 2016                                                                             | Fall   | 2016-2017    |
|    3028 | 911        | Barstow Community College                  | 201700        | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
|    2978 | 911        | Barstow Community College                  | 201703        | Spring 2017                                                                           | Spring | 2016-2017    |
|    1809 | 911        | Barstow Community College                  | 201707        | Fall 2017                                                                             | Fall   | 2017-2018    |
|    3537 | 911        | Barstow Community College                  | 201800        | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
|    2960 | 911        | Barstow Community College                  | 201803        | Spring 2018                                                                           | Spring | 2017-2018    |
|    1779 | 911        | Barstow Community College                  | 201807        | Fall 2018                                                                             | Fall   | 2018-2019    |
|      53 | 911        | Barstow Community College                  | 201900        | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
|    1281 | 911        | Barstow Community College                  | 201903        | Spring 2019                                                                           | Spring | 2018-2019    |
|    6153 | 921        | Chaffey College                            | 2014/FA       | Fall 2014                                                                             | Fall   | 2014-2015    |
|   13027 | 921        | Chaffey College                            | 2014/SP       | Spring 2014                                                                           | Spring | 2013-2014    |
|   21425 | 921        | Chaffey College                            | 2014/SU       | Summer/Fall 2014                                                                      | Summer | 2014-2015    |
|    6164 | 921        | Chaffey College                            | 2015/FA       | Fall 2015                                                                             | Fall   | 2015-2016    |
|   12639 | 921        | Chaffey College                            | 2015/SP       | Spring 2015                                                                           | Spring | 2014-2015    |
|   19974 | 921        | Chaffey College                            | 2015/SU       | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
|       1 | 921        | Chaffey College                            | 201530        | Winter 2016                                                                           | Winter | 2015-2016    |
|    8665 | 921        | Chaffey College                            | 2016/FA       | Fall 2016                                                                             | Fall   | 2016-2017    |
|   13118 | 921        | Chaffey College                            | 2016/SP       | Spring 2016                                                                           | Spring | 2015-2016    |
|   16988 | 921        | Chaffey College                            | 2016/SU       | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
|   14345 | 921        | Chaffey College                            | 2017/FA       | Fall 2017                                                                             | Fall   | 2017-2018    |
|   11768 | 921        | Chaffey College                            | 2017/SP       | Spring 2017                                                                           | Spring | 2016-2017    |
|    6914 | 921        | Chaffey College                            | 2017/SU       | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
|    5659 | 921        | Chaffey College                            | 2017/SU       | Summer 2017                                                                           | Summer | 2017-2018    |
|   17833 | 921        | Chaffey College                            | 2018/FA       | Fall 2018                                                                             | Fall   | 2018-2019    |
|   10061 | 921        | Chaffey College                            | 2018/SP       | Spring 2018                                                                           | Spring | 2017-2018    |
|    8154 | 921        | Chaffey College                            | 2018/SU       | Summer 2018                                                                           | Summer | 2018-2019    |
|    1974 | 921        | Chaffey College                            | 2019/FA       | Fall 2019                                                                             | Fall   | 2019-2020    |
|    5768 | 921        | Chaffey College                            | 2019/SP       | Spring 2019                                                                           | Spring | 2018-2019    |
|     397 | 921        | Chaffey College                            | 2019/SU       | Summer 2019                                                                           | Summer | 2019-2020    |
|    7218 | 931        | College of the Desert                      | 14/FA         | Fall 2014                                                                             | Fall   | 2014-2015    |
|     140 | 931        | College of the Desert                      | 14/SP         | Spring 2014                                                                           | Spring | 2013-2014    |
|    2084 | 931        | College of the Desert                      | 14/SU         | Summer 2014                                                                           | Summer | 2014-2015    |
|    7306 | 931        | College of the Desert                      | 15/FA         | Fall 2015                                                                             | Fall   | 2015-2016    |
|    2734 | 931        | College of the Desert                      | 15/SP         | Spring 2015                                                                           | Spring | 2014-2015    |
|    2197 | 931        | College of the Desert                      | 15/SU         | Summer 2015                                                                           | Summer | 2015-2016    |
|     534 | 931        | College of the Desert                      | 15/WI         | Winter 2015                                                                           | Winter | 2014-2015    |
|    7116 | 931        | College of the Desert                      | 16/FA         | Fall 2016                                                                             | Fall   | 2016-2017    |
|    3425 | 931        | College of the Desert                      | 16/SP         | Spring 2016                                                                           | Spring | 2015-2016    |
|    2321 | 931        | College of the Desert                      | 16/SU         | Summer 2016                                                                           | Summer | 2016-2017    |
|     640 | 931        | College of the Desert                      | 16/WI         | Winter 2016                                                                           | Winter | 2015-2016    |
|    8320 | 931        | College of the Desert                      | 17/FA         | Fall 2017                                                                             | Fall   | 2017-2018    |
|    3108 | 931        | College of the Desert                      | 17/SP         | Spring 2017                                                                           | Spring | 2016-2017    |
|    2257 | 931        | College of the Desert                      | 17/SU         | Summer 2017                                                                           | Summer | 2017-2018    |
|    1135 | 931        | College of the Desert                      | 17/WI         | WInter 2017                                                                           | Winter | 2016-2017    |
|    8333 | 931        | College of the Desert                      | 18/FA         | Fall 2018                                                                             | Fall   | 2018-2019    |
|    3415 | 931        | College of the Desert                      | 18/SP         | Spring 2018                                                                           | Spring | 2017-2018    |
|    2655 | 931        | College of the Desert                      | 18/SU         | Summer 2018                                                                           | Summer | 2018-2019    |
|    1242 | 931        | College of the Desert                      | 18/WI         | Winter 2018                                                                           | Winter | 2017-2018    |
|     610 | 931        | College of the Desert                      | 19/FA         | Fall 2019                                                                             | Fall   | 2019-2020    |
|    1225 | 931        | College of the Desert                      | 19/SP         | Spring 2019                                                                           | Spring | 2018-2019    |
|      44 | 931        | College of the Desert                      | 19/SU         | Summer 2019                                                                           | Summer | 2019-2020    |
|    1001 | 931        | College of the Desert                      | 19/WI         | Winter 2019                                                                           | Winter | 2018-2019    |
|    9231 | 941        | Mt. San Jacinto Community College District | FA14          | Fall 2014                                                                             | Fall   | 2014-2015    |
|   12859 | 941        | Mt. San Jacinto Community College District | FA15          | Fall 2015                                                                             | Fall   | 2015-2016    |
|   12568 | 941        | Mt. San Jacinto Community College District | FA16          | Fall 2016                                                                             | Fall   | 2016-2017    |
|   11455 | 941        | Mt. San Jacinto Community College District | FA17          | Fall 2017                                                                             | Fall   | 2017-2018    |
|   11807 | 941        | Mt. San Jacinto Community College District | FA18          | Fall 2018                                                                             | Fall   | 2018-2019    |
|      28 | 941        | Mt. San Jacinto Community College District | SP14          | Spring 2014                                                                           | Spring | 2013-2014    |
|    9537 | 941        | Mt. San Jacinto Community College District | SP15          | Spring 2015                                                                           | Spring | 2014-2015    |
|   11101 | 941        | Mt. San Jacinto Community College District | SP16          | Spring 2016                                                                           | Spring | 2015-2016    |
|   11895 | 941        | Mt. San Jacinto Community College District | SP17          | Spring 2017                                                                           | Spring | 2016-2017    |
|   10145 | 941        | Mt. San Jacinto Community College District | SP18          | Spring 2018                                                                           | Spring | 2017-2018    |
|    5324 | 941        | Mt. San Jacinto Community College District | SP19          | Spring 2019                                                                           | Spring | 2018-2019    |
|     530 | 941        | Mt. San Jacinto Community College District | SU14          | Summer 2014                                                                           | Summer | 2014-2015    |
|    6412 | 941        | Mt. San Jacinto Community College District | SU15          | Summer 2015                                                                           | Summer | 2015-2016    |
|    5922 | 941        | Mt. San Jacinto Community College District | SU16          | Summer 2016                                                                           | Summer | 2016-2017    |
|    6832 | 941        | Mt. San Jacinto Community College District | SU17          | Summer 2017                                                                           | Summer | 2017-2018    |
|    8751 | 941        | Mt. San Jacinto Community College District | SU18          | Summer 2018                                                                           | Summer | 2018-2019    |
|      43 | 951        | Palo Verde College                         | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
|     740 | 951        | Palo Verde College                         | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
|     470 | 951        | Palo Verde College                         | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
|     163 | 951        | Palo Verde College                         | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
|     640 | 951        | Palo Verde College                         | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
|     488 | 951        | Palo Verde College                         | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
|     206 | 951        | Palo Verde College                         | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
|     656 | 951        | Palo Verde College                         | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
|     435 | 951        | Palo Verde College                         | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
|     192 | 951        | Palo Verde College                         | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
|     121 | 951        | Palo Verde College                         | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
|    7810 | 961        | Riverside Community College                | 14FAL         | Fall 2014                                                                             | Fall   | 2014-2015    |
|     345 | 961        | Riverside Community College                | 14SUM         | Summer 2014                                                                           | Summer | 2014-2015    |
|   16084 | 961        | Riverside Community College                | 15FAL         | Fall 2015                                                                             | Fall   | 2015-2016    |
|    7492 | 961        | Riverside Community College                | 15SPR         | Spring 2015                                                                           | Spring | 2014-2015    |
|    9451 | 961        | Riverside Community College                | 15SUM         | Summer 2015                                                                           | Summer | 2015-2016    |
|    5368 | 961        | Riverside Community College                | 15WIN         | Winter 2015                                                                           | Winter | 2014-2015    |
|   17457 | 961        | Riverside Community College                | 16FAL         | Fall 2016                                                                             | Fall   | 2016-2017    |
|    8217 | 961        | Riverside Community College                | 16SPR         | Spring 2016                                                                           | Spring | 2015-2016    |
|   10454 | 961        | Riverside Community College                | 16SUM         | Summer 2016                                                                           | Summer | 2016-2017    |
|    5135 | 961        | Riverside Community College                | 16WIN         | Winter 2016                                                                           | Winter | 2015-2016    |
|   20939 | 961        | Riverside Community College                | 17FAL         | Fall 2017                                                                             | Fall   | 2017-2018    |
|   10785 | 961        | Riverside Community College                | 17SPR         | Spring 2017                                                                           | Spring | 2016-2017    |
|   10010 | 961        | Riverside Community College                | 17SUM         | Summer 2017                                                                           | Summer | 2017-2018    |
|    6270 | 961        | Riverside Community College                | 17WIN         | 2017 Winter                                                                           | Winter | 2016-2017    |
|      26 | 961        | Riverside Community College                | 17WIN         | Winter 2017                                                                           | Winter | 2016-2017    |
|   20500 | 961        | Riverside Community College                | 18FAL         | Fall 2018                                                                             | Fall   | 2018-2019    |
|   10408 | 961        | Riverside Community College                | 18SPR         | Spring 2018                                                                           | Spring | 2017-2018    |
|   10772 | 961        | Riverside Community College                | 18SUM         | Summer 2018                                                                           | Summer | 2018-2019    |
|   12881 | 961        | Riverside Community College                | 18WIN         | Winter 2018                                                                           | Winter | 2017-2018    |
|    2002 | 961        | Riverside Community College                | 19FAL         | Fall 2019                                                                             | Fall   | 2019-2020    |
|    4135 | 961        | Riverside Community College                | 19SPR         | Spring 2019                                                                           | Spring | 2018-2019    |
|     915 | 961        | Riverside Community College                | 19SUM         | Summer 2019                                                                           | Summer | 2019-2020    |
|    5591 | 961        | Riverside Community College                | 19WIN         | Winter 2019                                                                           | Winter | 2018-2019    |
|       1 | 961        | Riverside Community College                | 2016SP        | Spring 2016 - Victor Valley College                                                   | Spring | 2015-2016    |
|    3250 | 962        | Moreno Valley Community College            | 14FAL         | Fall 2014                                                                             | Fall   | 2014-2015    |
|     240 | 962        | Moreno Valley Community College            | 14SUM         | Summer 2014                                                                           | Summer | 2014-2015    |
|    5251 | 962        | Moreno Valley Community College            | 15FAL         | Fall 2015                                                                             | Fall   | 2015-2016    |
|    3043 | 962        | Moreno Valley Community College            | 15SPR         | Spring 2015                                                                           | Spring | 2014-2015    |
|    2725 | 962        | Moreno Valley Community College            | 15SUM         | Summer 2015                                                                           | Summer | 2015-2016    |
|    2015 | 962        | Moreno Valley Community College            | 15WIN         | Winter 2015                                                                           | Winter | 2014-2015    |
|    5217 | 962        | Moreno Valley Community College            | 16FAL         | Fall 2016                                                                             | Fall   | 2016-2017    |
|    3794 | 962        | Moreno Valley Community College            | 16SPR         | Spring 2016                                                                           | Spring | 2015-2016    |
|    3180 | 962        | Moreno Valley Community College            | 16SUM         | Summer 2016                                                                           | Summer | 2016-2017    |
|    1833 | 962        | Moreno Valley Community College            | 16WIN         | Winter 2016                                                                           | Winter | 2015-2016    |
|    6495 | 962        | Moreno Valley Community College            | 17FAL         | Fall 2017                                                                             | Fall   | 2017-2018    |
|    3470 | 962        | Moreno Valley Community College            | 17SPR         | Spring 2017                                                                           | Spring | 2016-2017    |
|    3560 | 962        | Moreno Valley Community College            | 17SUM         | Summer 2017                                                                           | Summer | 2017-2018    |
|    1730 | 962        | Moreno Valley Community College            | 17WIN         | Winter 2017                                                                           | Winter | 2016-2017    |
|    6332 | 962        | Moreno Valley Community College            | 18FAL         | Fall 2018                                                                             | Fall   | 2018-2019    |
|    3407 | 962        | Moreno Valley Community College            | 18SPR         | Spring 2018                                                                           | Spring | 2017-2018    |
|    4073 | 962        | Moreno Valley Community College            | 18SUM         | Summer 2018                                                                           | Summer | 2018-2019    |
|    1611 | 962        | Moreno Valley Community College            | 18WIN         | Winter 2018                                                                           | Winter | 2017-2018    |
|     399 | 962        | Moreno Valley Community College            | 19FAL         | Fall 2019                                                                             | Fall   | 2019-2020    |
|     888 | 962        | Moreno Valley Community College            | 19SPR         | Spring 2019                                                                           | Spring | 2018-2019    |
|    2084 | 962        | Moreno Valley Community College            | 19SUM         | Summer 2019                                                                           | Summer | 2019-2020    |
|    1264 | 962        | Moreno Valley Community College            | 19WIN         | Winter 2019                                                                           | Winter | 2018-2019    |
|    2379 | 963        | Norco Community College                    | 14FAL         | Fall 2014                                                                             | Fall   | 2014-2015    |
|      66 | 963        | Norco Community College                    | 14SUM         | Summer 2014                                                                           | Summer | 2014-2015    |
|    5749 | 963        | Norco Community College                    | 15FAL         | Fall 2015                                                                             | Fall   | 2015-2016    |
|    2442 | 963        | Norco Community College                    | 15SPR         | Spring 2015                                                                           | Spring | 2014-2015    |
|    2846 | 963        | Norco Community College                    | 15SUM         | Summer 2015                                                                           | Summer | 2015-2016    |
|    1704 | 963        | Norco Community College                    | 15WIN         | Winter 2015                                                                           | Winter | 2014-2015    |
|    5565 | 963        | Norco Community College                    | 16FAL         | Fall 2016                                                                             | Fall   | 2016-2017    |
|    2370 | 963        | Norco Community College                    | 16SPR         | Spring 2016                                                                           | Spring | 2015-2016    |
|    2988 | 963        | Norco Community College                    | 16SUM         | Summer 2016                                                                           | Summer | 2016-2017    |
|    1620 | 963        | Norco Community College                    | 16WIN         | Winter 2016                                                                           | Winter | 2015-2016    |
|    5153 | 963        | Norco Community College                    | 17FAL         | Fall 2017                                                                             | Fall   | 2017-2018    |
|    2436 | 963        | Norco Community College                    | 17SPR         | Spring 2017                                                                           | Spring | 2016-2017    |
|    3235 | 963        | Norco Community College                    | 17SUM         | Summer 2017                                                                           | Summer | 2017-2018    |
|    1734 | 963        | Norco Community College                    | 17WIN         | Winter 2017                                                                           | Winter | 2016-2017    |
|    5030 | 963        | Norco College                              | 18FAL         | Fall 2018                                                                             | Fall   | 2018-2019    |
|     780 | 963        | Norco Community College                    | 18FAL         | Fall 2018                                                                             | Fall   | 2018-2019    |
|    2885 | 963        | Norco Community College                    | 18SPR         | Spring 2018                                                                           | Spring | 2017-2018    |
|     115 | 963        | Norco College                              | 18SPR         | Spring 2018                                                                           | Spring | 2017-2018    |
|     738 | 963        | Norco Community College                    | 18SUM         | Summer 2018                                                                           | Summer | 2018-2019    |
|    2443 | 963        | Norco College                              | 18SUM         | Summer 2018                                                                           | Summer | 2018-2019    |
|    1682 | 963        | Norco Community College                    | 18WIN         | Winter 2018                                                                           | Winter | 2017-2018    |
|     588 | 963        | Norco Community College                    | 19FAL         | Fall 2019                                                                             | Fall   | 2019-2020    |
|    1529 | 963        | Norco Community College                    | 19SPR         | Spring 2019                                                                           | Spring | 2018-2019    |
|     155 | 963        | Norco College                              | 19SPR         | Spring 2019                                                                           | Spring | 2018-2019    |
|       1 | 963        | Norco Community College                    | 19SUM         | Summer 2018                                                                           | Summer | 2018-2019    |
|     130 | 963        | Norco Community College                    | 19SUM         | Summer 2019                                                                           | Summer | 2019-2020    |
|     451 | 963        | Norco College                              | 19WIN         | Winter 2019                                                                           | Winter | 2018-2019    |
|    1133 | 963        | Norco Community College                    | 19WIN         | Winter 2019                                                                           | Winter | 2018-2019    |
|     770 | 971        | Copper Mountain Community College          | 2013FA        | Fall 2013-Copper Mountain College                                                     | Fall   | 2013-2014    |
|       1 | 971        | Copper Mountain Community College          | 2013SU        | Summer 2013-Copper Mountain Community College                                         | Summer | 2013-2014    |
|    1341 | 971        | Copper Mountain Community College          | 2014FA        | Fall 2014-Copper Mountain College                                                     | Fall   | 2014-2015    |
|    1044 | 971        | Copper Mountain Community College          | 2014SP        | Spring 2014-Copper Mountain College                                                   | Spring | 2013-2014    |
|     500 | 971        | Copper Mountain Community College          | 2014SU        | Summer 2014-Copper Mountain College                                                   | Summer | 2014-2015    |
|    1550 | 971        | Copper Mountain Community College          | 2015FA        | Fall 2015-Copper Mountain College                                                     | Fall   | 2015-2016    |
|     851 | 971        | Copper Mountain Community College          | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
|     191 | 971        | Copper Mountain Community College          | 2015SP        | Spring 2015-Copper Mountain College                                                   | Spring | 2014-2015    |
|     568 | 971        | Copper Mountain Community College          | 2015SU        | Summer 2015-Copper Mountain College                                                   | Summer | 2015-2016    |
|    1678 | 971        | Copper Mountain Community College          | 2016FA        | Fall 2016-Copper Mountain College                                                     | Fall   | 2016-2017    |
|    1217 | 971        | Copper Mountain Community College          | 2016SP        | Spring 2016-Copper Mountain College                                                   | Spring | 2015-2016    |
|     590 | 971        | Copper Mountain Community College          | 2016SU        | Summer 2016-Copper Mountain College                                                   | Summer | 2016-2017    |
|    1919 | 971        | Copper Mountain Community College          | 2017FA        | Fall 2017 - Copper Mountain College                                                   | Fall   | 2017-2018    |
|    1373 | 971        | Copper Mountain Community College          | 2017SP        | Spring 2017-Copper Mountain College                                                   | Spring | 2016-2017    |
|     651 | 971        | Copper Mountain Community College          | 2017SU        | Summer 2017 - Copper Mountain College                                                 | Summer | 2017-2018    |
|    1527 | 971        | Copper Mountain Community College          | 2018FA        | Fall 2018 - Copper Mountain College                                                   | Fall   | 2018-2019    |
|    1406 | 971        | Copper Mountain Community College          | 2018SP        | Spring 2018 - Copper Mountain College                                                 | Spring | 2017-2018    |
|     668 | 971        | Copper Mountain Community College          | 2018SU        | Summer 2018 - Copper Mountain College                                                 | Summer | 2018-2019    |
|      35 | 971        | Copper Mountain Community College          | 2019FA        | Fall 2019 - Copper Mountain College                                                   | Fall   | 2019-2020    |
|     503 | 971        | Copper Mountain Community College          | 2019SP        | Spring 2019 - Copper Mountain College                                                 | Spring | 2018-2019    |
|      18 | 971        | Copper Mountain Community College          | 2019SU        | Summer 2019 - Copper Mountain College                                                 | Summer | 2019-2020    |
|    2801 | 981        | Crafton Hills College                      | 2014FA|C1415  | 2014 Fall (Aug - Dec)                                                                 | Fall   | 2014-2015    |
|     362 | 981        | Crafton Hills College                      | 2014SM|C1314  | 2014 Summer (Jun - Aug)                                                               | Summer | 2014-2015    |
|    5089 | 981        | Crafton Hills College                      | 2015FA|C1516  | 2015 Fall (Aug - Dec)                                                                 | Fall   | 2015-2016    |
|    2487 | 981        | Crafton Hills College                      | 2015SM|C1415  | 2015 Summer (Jun - Aug)                                                               | Summer | 2015-2016    |
|    3104 | 981        | Crafton Hills College                      | 2015SP|C1415  | 2015 Spring (Jan - May)                                                               | Spring | 2014-2015    |
|    4875 | 981        | Crafton Hills College                      | 2016FA|C1617  | 2016 Fall (Aug - Dec)                                                                 | Fall   | 2016-2017    |
|    2319 | 981        | Crafton Hills College                      | 2016SM|C1516  | 2016 Summer (Jun - Aug)                                                               | Summer | 2016-2017    |
|    3868 | 981        | Crafton Hills College                      | 2016SP|C1516  | 2016 Spring (Jan - May)                                                               | Spring | 2015-2016    |
|    4906 | 981        | Crafton Hills College                      | 2017FA|C1718  | 2017 Fall (Aug - Dec)                                                                 | Fall   | 2017-2018    |
|    2576 | 981        | Crafton Hills College                      | 2017SM|C1617  | 2017 Summer (Jun - Aug)                                                               | Summer | 2017-2018    |
|    3643 | 981        | Crafton Hills College                      | 2017SP|C1617  | 2017 Spring (Jan - May)                                                               | Spring | 2016-2017    |
|   11066 | 981        | Crafton Hills College                      | 2018FA|C1819  | 2018 Fall (Aug - Dec)                                                                 | Fall   | 2018-2019    |
|    9082 | 981        | Crafton Hills College                      | 2018SM|C1718  | 2018 Summer (Jun - Aug)                                                               | Summer | 2018-2019    |
|    3765 | 981        | Crafton Hills College                      | 2018SP|C1718  | 2018 Spring (Jan - May)                                                               | Spring | 2017-2018    |
|    5028 | 981        | Crafton Hills College                      | 2019SP|C1819  | 2019 Spring (Jan - May)                                                               | Spring | 2018-2019    |
|    7823 | 982        | San Bernardino Valley College              | 2014FA|V1415  | Fall 2014 (September-December)                                                        | Fall   | 2014-2015    |
|     334 | 982        | San Bernardino Valley College              | 2014SM|V1314  | Summer 2014 (June-August)                                                             | Summer | 2014-2015    |
|   11783 | 982        | San Bernardino Valley College              | 2015FA|V1516  | Fall 2015 (August - December)                                                         | Fall   | 2015-2016    |
|    7680 | 982        | San Bernardino Valley College              | 2015SM|V1415  | Summer 2015 (June - August)                                                           | Summer | 2015-2016    |
|    8987 | 982        | San Bernardino Valley College              | 2015SP|V1415  | Spring 2015 (January-May)                                                             | Spring | 2014-2015    |
|   11602 | 982        | San Bernardino Valley College              | 2016FA|V1617  | Fall 2016 (August - December)                                                         | Fall   | 2016-2017    |
|    8027 | 982        | San Bernardino Valley College              | 2016SM|V1516  | Summer 2016 (June - August)                                                           | Summer | 2016-2017    |
|    9629 | 982        | San Bernardino Valley College              | 2016SP|V1516  | Spring 2016 (January - May)                                                           | Spring | 2015-2016    |
|   13626 | 982        | San Bernardino Valley College              | 2017FA|V1718  | Fall 2017 (August - December)                                                         | Fall   | 2017-2018    |
|    8149 | 982        | San Bernardino Valley College              | 2017SM|V1617  | Summer 2017 (June - August)                                                           | Summer | 2017-2018    |
|    9851 | 982        | San Bernardino Valley College              | 2017SP|V1617  | Spring 2017 (January - May)                                                           | Spring | 2016-2017    |
|   19349 | 982        | San Bernardino Valley College              | 2018FA|V1819  | Fall 2018 (August - December)                                                         | Fall   | 2018-2019    |
|    9009 | 982        | San Bernardino Valley College              | 2018SM|V1718  | Summer 2018 (June - August)                                                           | Summer | 2018-2019    |
|   11682 | 982        | San Bernardino Valley College              | 2018SP|V1718  | Spring 2018 (January - May)                                                           | Spring | 2017-2018    |
|    1229 | 982        | San Bernardino Valley College              | 2019FA|V1920  | Fall 2019 (August - December)                                                         | Fall   | 2019-2020    |
|     239 | 982        | San Bernardino Valley College              | 2019SM|V1819  | Summer 2019 (May - August)                                                            | Summer | 2019-2020    |
|    8378 | 982        | San Bernardino Valley College              | 2019SP|V1819  | Spring 2019 (January - May)                                                           | Spring | 2018-2019    |
|      11 | 982        | San Bernardino Valley College              | 2019SU|V1819  | Summer 2019 (May - August)                                                            | Summer | 2019-2020    |
|     302 | 991        | Victor Valley College                      | 2013FA        | Fall 2013  - Victor Valley College                                                    | Fall   | 2013-2014    |
|   10955 | 991        | Victor Valley College                      | 2014FA        | Fall 2014 -Victor Valley College                                                      | Fall   | 2014-2015    |
|    7535 | 991        | Victor Valley College                      | 2014SP        | Spring 2014  - Victor Valley College                                                  | Spring | 2013-2014    |
|    4643 | 991        | Victor Valley College                      | 2014SU        | Summer 2014  - Victor Valley College                                                  | Summer | 2014-2015    |
|       1 | 991        | Victor Valley College                      | 2014WI        | Winter 2014  - Victor Valley College                                                  | Winter | 2013-2014    |
|   10428 | 991        | Victor Valley College                      | 2015FA        | Fall 2015 - Victor Valley College                                                     | Fall   | 2015-2016    |
|    6726 | 991        | Victor Valley College                      | 2015SP        | Spring 2015 -  Victor Valley College                                                  | Spring | 2014-2015    |
|    6033 | 991        | Victor Valley College                      | 2015SU        | Summer 2015 - Victor Valley College                                                   | Summer | 2015-2016    |
|    1694 | 991        | Victor Valley College                      | 2015WI        | Winter 2015 - Victor Valley College                                                   | Winter | 2014-2015    |
|    9955 | 991        | Victor Valley College                      | 2016FA        | Fall 2016 -  Victor Valley College                                                    | Fall   | 2016-2017    |
|    5872 | 991        | Victor Valley College                      | 2016SP        | Spring 2016 - Victor Valley College                                                   | Spring | 2015-2016    |
|    5011 | 991        | Victor Valley College                      | 2016SU        | Summer 2016 -  Victor Valley College                                                  | Summer | 2016-2017    |
|    2157 | 991        | Victor Valley College                      | 2016WI        | Winter 2016 - Victor Valley College                                                   | Winter | 2015-2016    |
|    8384 | 991        | Victor Valley College                      | 2017FA        | Fall 2017 - Victor Valley College                                                     | Fall   | 2017-2018    |
|    5415 | 991        | Victor Valley College                      | 2017SP        | Spring 2017 - Victor Valley College                                                   | Spring | 2016-2017    |
|    7412 | 991        | Victor Valley College                      | 2017SU        | Summer 2017 - Victor Valley College                                                   | Summer | 2017-2018    |
|    1788 | 991        | Victor Valley College                      | 2017WI        | Winter 2017 - Victor Valley College                                                   | Winter | 2016-2017    |
|   10813 | 991        | Victor Valley College                      | 2018FA        | Fall 2018 - Victor Valley College                                                     | Fall   | 2018-2019    |
|    6257 | 991        | Victor Valley College                      | 2018SP        | Spring 2018 - Victor Valley College                                                   | Spring | 2017-2018    |
|    5885 | 991        | Victor Valley College                      | 2018SU        | Summer 2018 - Victor Valley College                                                   | Summer | 2018-2019    |
|    2168 | 991        | Victor Valley College                      | 2018WI        | Winter 2018 - Victor Valley College                                                   | Winter | 2017-2018    |
|    1800 | 991        | Victor Valley College                      | 2019FA        | Fall 2019 - Victor Valley College                                                     | Fall   | 2019-2020    |
|    2311 | 991        | Victor Valley College                      | 2019SP        | Spring 2019 - Victor Valley College                                                   | Spring | 2018-2019    |
|    1859 | 991        | Victor Valley College                      | 2019SU        | Summer 2019 - Victor Valley College                                                   | Summer | 2019-2020    |
|    1565 | 991        | Victor Valley College                      | 2019WI        | Winter 2019 - Victor Valley College                                                   | Winter | 2018-2019    |
|       1 | 999        | Sacramento City College                    | 1143          | Spring 2014                                                                           | Spring | 2013-2014    |
|       1 | 999        | Los Medanos College                        | 2014SP        | Spring 2014                                                                           | Spring | 2013-2014    |
+---------+------------+--------------------------------------------+---------------+---------------------------------------------------------------------------------------+--------+--------------+
*/

/* FROM SOURCE

SELECT
    college_id,
    term_code,
    term_description,
    Term,
    AcademicYear =  case
                        when Term in ('Summer', 'Fall')   then convert(char(4), Year) + '-' + convert(char(4), Year + 1)
                        when Term in ('Winter', 'Spring') then convert(char(4), Year - 1) + '-' + convert(char(4), Year)
                    end
FROM
    (
        SELECT
            college_id,
            term_code,
            term_description,
            Term = coalesce(
                    case when term_description like '%Summer%'       then 'Summer' end,
                    case when term_description like '%Sumner%'       then 'Summer' end,
                    case when term_description like '%Fall%'         then 'Fall'   end,
                    case when term_description like '%Winter%'       then 'Winter' end,
                    case when term_description like '%Intersession%' then 'Winter' end,
                    case when term_description like '%Spring%'       then 'Spring' end,
                    case when term_description like '%Sping%'        then 'Spring' end,
                    -- user transposed term_code and term_description
                    case when term_code        like '%Summer%'       then 'Summer' end,
                    case when term_code        like '%Sumner%'       then 'Summer' end,
                    case when term_code        like '%Fall%'         then 'Fall'   end,
                    case when term_code        like '%Winter%'       then 'Winter' end,
                    case when term_code        like '%Intersession%' then 'Winter' end,
                    case when term_code        like '%Spring%'       then 'Spring' end,
                    case when term_code        like '%Sping%'        then 'Spring' end,
                    -- user using non-typical word boundary
                    case when term_code        like '%SP%'           then 'Spring' end,
                    -- Citrus
                    case when college_id = '821' and term_code = '201420' then 'Fall'   end,
                    case when college_id = '821' and term_code = '201425' then 'Winter' end,
                    case when college_id = '821' and term_code = '201430' then 'Spring' end,
                    case when college_id = '821' and term_code = '201440' then 'Summer' end,
                    -- Irvine Valley
                    case when college_id = '892' and term_code = '20172'  then 'Summer' end
                ),
            Year = coalesce(
                    case when term_description like '%2027%' then 2027 end,
                    case when term_description like '%2026%' then 2026 end,
                    case when term_description like '%2025%' then 2025 end,
                    case when term_description like '%2024%' then 2024 end,
                    case when term_description like '%2023%' then 2023 end,
                    case when term_description like '%2022%' then 2022 end,
                    case when term_description like '%2021%' then 2021 end,
                    case when term_description like '%2020%' then 2020 end,
                    case when term_description like '%2019%' then 2019 end,
                    case when term_description like '%2018%' then 2018 end,
                    case when term_description like '%2017%' then 2017 end,
                    case when term_description like '%2016%' then 2016 end,
                    case when term_description like '%2015%' then 2015 end,
                    case when term_description like '%2014%' then 2014 end,
                    case when term_description like '%2013%' then 2013 end,
                    case when term_description like '%2012%' then 2012 end,
                    case when term_description like '%2011%' then 2011 end,
                    -- Siskiyous :: Calendar (decrement if winter or spring)
                    case when college_id = '181' and term_code = '202010' then 2020 end,
                    -- De Anza :: Academic Year, Ending
                    case when college_id = '421' and term_code = '201823' then 2018 end,
                    -- Bakersfield :: Calendar Year (decrement if winter or spring)
                    case when college_id = '521' and term_code = '201750' then 2018 end,
                    -- East LA :: Calendar Year (decrement if winter or spring)
                    case when college_id = '748' and term_code = '2152'   then 2015 end,
                    case when college_id = '748' and term_code = '2154'   then 2015 end,
                    -- West LA :: Calendar Year (decrement if winter or spring)
                    case when college_id = '749' and term_code = '2154'   then 2015 end,
                    -- Citrus :: Academic Year, Ending
                    case when college_id = '821' and term_code = '201420' then 2014 end,
                    case when college_id = '821' and term_code = '201425' then 2014 end,
                    case when college_id = '821' and term_code = '201430' then 2014 end,
                    case when college_id = '821' and term_code = '201440' then 2015 end,
                    -- MSAC :: Academic Year, Beginning
                    case when college_id = '851' and term_code = '201720' then 2017 end,
                    -- Rio Hondo :: Calendar Year (decrement if winter or spring)
                    case when college_id = '881' and term_code = '201430' then 2014 end,
                    -- Irvine Valley :: Calendar Year (decrement if winter or spring)
                    case when college_id = '892' and term_code = '20172'  then 2018 end
                )
        FROM
            OPENQUERY(CCCApply, 'SELECT DISTINCT CAST(college_id AS CHAR(3)), CAST(term_code AS VARCHAR(100)), cast(description as VARCHAR(100)) as term_description from Term WHERE lower(term_code) not like ''%test%'' and substring(college_id, 1, 2) <> ''ZZ'';')
    ) a
ORDER BY
    college_id,
    term_code;

+------------+---------------+---------------------------------------------------------------------------------------+--------+--------------+
| college_id |   term_code   |                                   term_description                                    |  Term  | AcademicYear |
+------------+---------------+---------------------------------------------------------------------------------------+--------+--------------+
| 021        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 021        | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
| 021        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 021        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 021        | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
| 021        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 021        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 021        | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
| 021        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 021        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 021        | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
| 021        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 021        | SP 18         | Spring 2018                                                                           | Spring | 2017-2018    |
| 022        | 02211         | Fall 2011 - Grossmont College                                                         | Fall   | 2011-2012    |
| 022        | 02212         | Spring 2012 - Grossmont College                                                       | Spring | 2011-2012    |
| 022        | 2015FA        | Fall 2015 - Grossmont College                                                         | Fall   | 2015-2016    |
| 022        | 2015SP        | Spring 2015 - Grossmont College                                                       | Spring | 2014-2015    |
| 022        | 2015SU        | Summer 2015 - Grossmont College                                                       | Summer | 2015-2016    |
| 022        | 2016FA        | Fall 2016 -  Grossmont College                                                        | Fall   | 2016-2017    |
| 022        | 2016SP        | Spring 2016 - Grossmont College                                                       | Spring | 2015-2016    |
| 022        | 2016SU        | Summer 2016 - Grossmont College                                                       | Summer | 2016-2017    |
| 022        | 2017FA        | Fall 2017 - Grossmont College                                                         | Fall   | 2017-2018    |
| 022        | 2017SP        | Spring 2017-Grossmont College                                                         | Spring | 2016-2017    |
| 022        | 2017SU        | Summer 2017 - Grossmont College                                                       | Summer | 2017-2018    |
| 022        | 2018FA        | Fall 2018 - Grossmont College                                                         | Fall   | 2018-2019    |
| 022        | 2018SP        | Spring 2018-Grossmont College                                                         | Spring | 2017-2018    |
| 022        | 2018SU        | Summer 2018 - Grossmont College                                                       | Summer | 2018-2019    |
| 022        | 2019SP        | Spring 2019-Grossmont College                                                         | Spring | 2018-2019    |
| 031        | 2014SF        | Summer/Fall 2014                                                                      | Summer | 2014-2015    |
| 031        | 2015SF        | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
| 031        | 2015WS        | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
| 031        | 2016SF        | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
| 031        | 2016WS        | 2016 Winter/Spring                                                                    | Winter | 2015-2016    |
| 031        | 2017SF        | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
| 031        | 2017WS        | 2017 Winter/Spring                                                                    | Winter | 2016-2017    |
| 031        | 2018SF        | 2018 Summer/Fall                                                                      | Summer | 2018-2019    |
| 031        | 2018WS        | 2018 Winter/Spring                                                                    | Winter | 2017-2018    |
| 031        | 2019SF        | 2019 Summer/Fall                                                                      | Summer | 2019-2020    |
| 031        | 2019WS        | 2019 Winter Spring                                                                    | Winter | 2018-2019    |
| 051        | 20140602      | Summer 2014 - MiraCosta College                                                       | Summer | 2014-2015    |
| 051        | 20140818      | Fall 2014 - MiraCosta College                                                         | Fall   | 2014-2015    |
| 051        | 20150120      | Spring 2015 - MiraCosta College                                                       | Spring | 2014-2015    |
| 051        | 20150615      | Summer 2015 - MiraCosta College                                                       | Summer | 2015-2016    |
| 051        | 20150824      | Fall 2015 - MiraCosta College                                                         | Fall   | 2015-2016    |
| 051        | 20160125      | Spring 2016 - MiraCosta College                                                       | Spring | 2015-2016    |
| 051        | 20160613      | Summer 2016 - MiraCosta College                                                       | Summer | 2016-2017    |
| 051        | 20160822      | Fall 2016 - MiraCosta College                                                         | Fall   | 2016-2017    |
| 051        | 20170123      | Spring 2017 - MiraCosta College                                                       | Spring | 2016-2017    |
| 051        | 20170612      | Summer 2017 - MiraCosta College                                                       | Summer | 2017-2018    |
| 051        | 20170821      | Fall 2017 - MiraCosta College                                                         | Fall   | 2017-2018    |
| 051        | 20180122      | Spring 2018 - MiraCosta College                                                       | Spring | 2017-2018    |
| 051        | 20180611      | Summer 2018 - MiraCosta College                                                       | Summer | 2018-2019    |
| 051        | 20180820      | Fall 2018 - MiraCosta College                                                         | Fall   | 2018-2019    |
| 051        | 20190122      | Spring 2019 - MiraCosta College                                                       | Spring | 2018-2019    |
| 061        | 2165          | 2016 Summer                                                                           | Summer | 2016-2017    |
| 061        | 2167          | 2016 Fall                                                                             | Fall   | 2016-2017    |
| 061        | 2173          | 2017 Spring                                                                           | Spring | 2016-2017    |
| 061        | 2185          | Summer 2018                                                                           | Summer | 2018-2019    |
| 061        | 2187          | Fall 2018                                                                             | Fall   | 2018-2019    |
| 061        | 2193          | Spring 2019                                                                           | Spring | 2018-2019    |
| 071        | 07111         | Fall 2011 - San Diego City College                                                    | Fall   | 2011-2012    |
| 071        | 07112         | Spring 2012 - San Diego City College                                                  | Spring | 2011-2012    |
| 072        | 07211         | Fall 2011 - San Diego Mesa College                                                    | Fall   | 2011-2012    |
| 072        | 07212         | Spring 2012 - San Diego Mesa College                                                  | Spring | 2011-2012    |
| 073        | 07311         | Fall 2011 - San Diego Miramar College                                                 | Fall   | 2011-2012    |
| 073        | 07312         | Spring 2012 - San Diego Miramar College                                               | Spring | 2011-2012    |
| 091        | 14/FA         | Fall 2014                                                                             | Fall   | 2014-2015    |
| 091        | 14/SU         | Summer 2014 and Fall 2014                                                             | Summer | 2014-2015    |
| 091        | 15/FA         | Fall 2015                                                                             | Fall   | 2015-2016    |
| 091        | 15/SP         | Spring 2015                                                                           | Spring | 2014-2015    |
| 091        | 15/SU         | Summer 2015 and Fall 2015                                                             | Summer | 2015-2016    |
| 091        | 16/FA         | Fall 2016                                                                             | Fall   | 2016-2017    |
| 091        | 16/SP         | Spring 2016                                                                           | Spring | 2015-2016    |
| 091        | 16/SU         | Summer 2016 & Fall 2016                                                               | Summer | 2016-2017    |
| 091        | 17/FA         | Fall 2017                                                                             | Fall   | 2017-2018    |
| 091        | 17/SP         | Spring 2017                                                                           | Spring | 2016-2017    |
| 091        | 17/SU         | Summer 2017 & Fall 2017                                                               | Summer | 2017-2018    |
| 091        | 18/FA         | Fall 2018                                                                             | Fall   | 2018-2019    |
| 091        | 18/SP         | Spring 2018                                                                           | Spring | 2017-2018    |
| 091        | 18/SU         | Summer 2018 & Fall 2018                                                               | Summer | 2018-2019    |
| 091        | 19/SP         | Spring 2019                                                                           | Spring | 2018-2019    |
| 111        | 11111         | Fall 2011 - Butte College                                                             | Fall   | 2011-2012    |
| 111        | 11112         | Spring 2012 - Butte College                                                           | Spring | 2011-2012    |
| 111        | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 111        | 2014SU        | Summer 2014                                                                           | Summer | 2014-2015    |
| 111        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 111        | 2015IW        | Winter 2015                                                                           | Winter | 2014-2015    |
| 111        | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
| 111        | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
| 111        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 111        | 2016IW        | Winter 2016                                                                           | Winter | 2015-2016    |
| 111        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 111        | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
| 111        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 111        | 2017IW        | Winter 2017                                                                           | Winter | 2016-2017    |
| 111        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 111        | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
| 111        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 111        | 2018IW        | Winter 2018                                                                           | Winter | 2017-2018    |
| 111        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 111        | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
| 111        | 2019FA        | Fall 2019                                                                             | Fall   | 2019-2020    |
| 111        | 2019IW        | Winter 2019                                                                           | Winter | 2018-2019    |
| 111        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 111        | 2019SU        | Summer 2019                                                                           | Summer | 2019-2020    |
| 121        | 12111         | Fall 2011 - Feather River Community College                                           | Fall   | 2011-2012    |
| 121        | 12112         | Spring 2012 - Feather River Community College                                         | Spring | 2011-2012    |
| 131        | 2013FA        | Fall 2013                                                                             | Fall   | 2013-2014    |
| 131        | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 131        | 2014SP        | Spring 2014                                                                           | Spring | 2013-2014    |
| 131        | 2014SU        | Summer 2014                                                                           | Summer | 2014-2015    |
| 131        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 131        | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
| 131        | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
| 131        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 131        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 131        | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
| 131        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 131        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 131        | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
| 131        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 131        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 131        | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
| 131        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 141        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 141        | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
| 141        | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
| 141        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 141        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 141        | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
| 141        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 141        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 141        | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
| 141        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 141        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 141        | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
| 141        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 141        | 2019SU        | Summer 2019                                                                           | Summer | 2019-2020    |
| 161        | 2014F         | Fall 2014                                                                             | Fall   | 2014-2015    |
| 161        | 2014S         | Spring 2014                                                                           | Spring | 2013-2014    |
| 161        | 2014X         | Summer 2014                                                                           | Summer | 2014-2015    |
| 161        | 2015F         | Fall 2015                                                                             | Fall   | 2015-2016    |
| 161        | 2015S         | Spring 2015                                                                           | Spring | 2014-2015    |
| 161        | 2015X         | Summer 2015                                                                           | Summer | 2015-2016    |
| 161        | 2016F         | Fall 2016                                                                             | Fall   | 2016-2017    |
| 161        | 2016S         | Spring 2016                                                                           | Spring | 2015-2016    |
| 161        | 2016X         | Summer 2016                                                                           | Summer | 2016-2017    |
| 161        | 2017F         | Fall 2017                                                                             | Fall   | 2017-2018    |
| 161        | 2017S         | Spring 2017                                                                           | Spring | 2016-2017    |
| 161        | 2017X         | Summer 2017                                                                           | Summer | 2017-2018    |
| 161        | 2018F         | Fall 2018                                                                             | Fall   | 2018-2019    |
| 161        | 2018S         | Spring 2018                                                                           | Spring | 2017-2018    |
| 161        | 2018X         | Summer 2018                                                                           | Summer | 2018-2019    |
| 161        | 2019S         | Spring 2019                                                                           | Spring | 2018-2019    |
| 171        | 2014F         | Fall 2014                                                                             | Fall   | 2014-2015    |
| 171        | 2014U         | Summer 2014                                                                           | Summer | 2014-2015    |
| 171        | 2015F         | Fall 2015                                                                             | Fall   | 2015-2016    |
| 171        | 2015S         | Spring 2015                                                                           | Spring | 2014-2015    |
| 171        | 2015U         | Summer 2015                                                                           | Summer | 2015-2016    |
| 171        | 2016F         | Fall 2016                                                                             | Fall   | 2016-2017    |
| 171        | 2016S         | Spring 2016                                                                           | Spring | 2015-2016    |
| 171        | 2016U         | Summer 2016                                                                           | Summer | 2016-2017    |
| 171        | 2017F         | Fall 2017                                                                             | Fall   | 2017-2018    |
| 171        | 2017S         | Spring 2017                                                                           | Spring | 2016-2017    |
| 171        | 2017U         | Summer 2017                                                                           | Summer | 2017-2018    |
| 171        | 2018F         | Fall 2018                                                                             | Fall   | 2018-2019    |
| 171        | 2018S         | Spring 2018                                                                           | Spring | 2017-2018    |
| 171        | 2018U         | Summer 2018                                                                           | Summer | 2018-2019    |
| 171        | 2019F         | Fall 2019                                                                             | Fall   | 2019-2020    |
| 171        | 2019S         | Spring 2019                                                                           | Spring | 2018-2019    |
| 171        | 2019U         | Summer 2019                                                                           | Summer | 2019-2020    |
| 171        | 2020F         | Fall 2020                                                                             | Fall   | 2020-2021    |
| 171        | 2020S         | Spring 2020                                                                           | Spring | 2019-2020    |
| 171        | 2020U         | Summer 2020                                                                           | Summer | 2020-2021    |
| 181        | 18111         | Fall 2011 - College of the Siskiyous                                                  | Fall   | 2011-2012    |
| 181        | 18112         | Spring 2012 - College of the Siskiyous                                                | Spring | 2011-2012    |
| 181        | 201630        | Spring 2016                                                                           | Spring | 2015-2016    |
| 181        | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
| 181        | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 181        | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
| 181        | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
| 181        | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 181        | 201810        | Winter 2018                                                                           | Winter | 2017-2018    |
| 181        | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
| 181        | 201850        | Summer 2018                                                                           | Summer | 2018-2019    |
| 181        | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 181        | 201910        | Winter 2019                                                                           | Winter | 2018-2019    |
| 181        | 201930        | Spring 2019                                                                           | Spring | 2018-2019    |
| 181        | 201950        | Summer 2019                                                                           | Summer | 2019-2020    |
| 181        | 201970        | Fall 2019                                                                             | Fall   | 2019-2020    |
| 181        | 202010        | Winter Intersession                                                                   | Winter | 2019-2020    |
| 181        | 202030        | Spring 2020                                                                           | Spring | 2019-2020    |
| 221        | 2014FA        | Fall 2014 Quarter                                                                     | Fall   | 2014-2015    |
| 221        | 2014SP        | Spring 2014 Quarter                                                                   | Spring | 2013-2014    |
| 221        | 2014SU        | Summer 2014 Session                                                                   | Summer | 2014-2015    |
| 221        | 2014WI        | Winter 2014 Quarter                                                                   | Winter | 2013-2014    |
| 221        | 2015FA        | Fall 2015 Quarter                                                                     | Fall   | 2015-2016    |
| 221        | 2015SP        | Spring 2015 Quarter                                                                   | Spring | 2014-2015    |
| 221        | 2015SU        | Summer 2015 Quarter                                                                   | Summer | 2015-2016    |
| 221        | 2015WI        | Winter 2015 Quarter                                                                   | Winter | 2014-2015    |
| 221        | 2016FA        | Fall 2016 Quarter                                                                     | Fall   | 2016-2017    |
| 221        | 2016SP        | Spring 2016 Quarter                                                                   | Spring | 2015-2016    |
| 221        | 2016SU        | Summer 2016 Quarter                                                                   | Summer | 2016-2017    |
| 221        | 2016WI        | Winter 2016 Quarter                                                                   | Winter | 2015-2016    |
| 221        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 221        | 2017SP        | Spring 2017 Quarter                                                                   | Spring | 2016-2017    |
| 221        | 2017SU        | Summer 2017 Quarter                                                                   | Summer | 2017-2018    |
| 221        | 2017WI        | Winter 2017 Quarter                                                                   | Winter | 2016-2017    |
| 221        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 221        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 221        | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
| 221        | 2018WI        | Winter 2018                                                                           | Winter | 2017-2018    |
| 221        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 221        | 2019WI        | Winter 2019                                                                           | Winter | 2018-2019    |
| 231        | 1139          | Fall 2013                                                                             | Fall   | 2013-2014    |
| 231        | 1143          | Spring 2014                                                                           | Spring | 2013-2014    |
| 231        | 1146          | Summer 2014                                                                           | Summer | 2014-2015    |
| 231        | 1149          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 231        | 1153          | Spring 2015                                                                           | Spring | 2014-2015    |
| 231        | 1156          | Summer 2015                                                                           | Summer | 2015-2016    |
| 231        | 1159          | Fall 2015                                                                             | Fall   | 2015-2016    |
| 231        | 1163          | Spring 2016                                                                           | Spring | 2015-2016    |
| 231        | 1166          | Summer 2016                                                                           | Summer | 2016-2017    |
| 231        | 1169          | Fall 2016                                                                             | Fall   | 2016-2017    |
| 231        | 1173          | Spring 2017                                                                           | Spring | 2016-2017    |
| 231        | 1176          | Summer 2017                                                                           | Summer | 2017-2018    |
| 231        | 1179          | Fall 2017                                                                             | Fall   | 2017-2018    |
| 231        | 1183          | Spring 2018                                                                           | Spring | 2017-2018    |
| 231        | 1186          | Summer 2018                                                                           | Summer | 2018-2019    |
| 231        | 1189          | Fall 2018                                                                             | Fall   | 2018-2019    |
| 231        | 1193          | Spring 2019                                                                           | Spring | 2018-2019    |
| 231        | 1196          | Summer 2019                                                                           | Summer | 2019-2020    |
| 231        | 1199          | Fall 2019                                                                             | Fall   | 2019-2020    |
| 232        | 1139          | Fall 2013                                                                             | Fall   | 2013-2014    |
| 232        | 1143          | Spring 2014                                                                           | Spring | 2013-2014    |
| 232        | 1146          | Summer 2014                                                                           | Summer | 2014-2015    |
| 232        | 1149          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 232        | 1153          | Spring 2015                                                                           | Spring | 2014-2015    |
| 232        | 1156          | Summer 2015                                                                           | Summer | 2015-2016    |
| 232        | 1159          | Fall 2015                                                                             | Fall   | 2015-2016    |
| 232        | 1163          | Spring 2016                                                                           | Spring | 2015-2016    |
| 232        | 1166          | Summer 2016                                                                           | Summer | 2016-2017    |
| 232        | 1169          | Fall 2016                                                                             | Fall   | 2016-2017    |
| 232        | 1173          | Spring 2017                                                                           | Spring | 2016-2017    |
| 232        | 1176          | Summer 2017                                                                           | Summer | 2017-2018    |
| 232        | 1179          | Fall 2017                                                                             | Fall   | 2017-2018    |
| 232        | 1183          | Spring 2018                                                                           | Spring | 2017-2018    |
| 232        | 1186          | Summer 2018                                                                           | Summer | 2018-2019    |
| 232        | 1189          | Fall 2018                                                                             | Fall   | 2018-2019    |
| 232        | 1193          | Spring 2019                                                                           | Spring | 2018-2019    |
| 232        | 1196          | Summer 2019                                                                           | Summer | 2019-2020    |
| 232        | 1199          | Fall 2019                                                                             | Fall   | 2019-2020    |
| 233        | 1139          | Fall 2013                                                                             | Fall   | 2013-2014    |
| 233        | 1143          | Spring 2014                                                                           | Spring | 2013-2014    |
| 233        | 1146          | Summer 2014                                                                           | Summer | 2014-2015    |
| 233        | 1149          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 233        | 1153          | Spring 2015                                                                           | Spring | 2014-2015    |
| 233        | 1156          | Summer 2015                                                                           | Summer | 2015-2016    |
| 233        | 1159          | Fall 2015                                                                             | Fall   | 2015-2016    |
| 233        | 1163          | Spring 2016                                                                           | Spring | 2015-2016    |
| 233        | 1166          | Summer 2016                                                                           | Summer | 2016-2017    |
| 233        | 1169          | Fall 2016                                                                             | Fall   | 2016-2017    |
| 233        | 1173          | Spring 2017                                                                           | Spring | 2016-2017    |
| 233        | 1176          | Summer 2017                                                                           | Summer | 2017-2018    |
| 233        | 1179          | Fall 2017                                                                             | Fall   | 2017-2018    |
| 233        | 1183          | Spring 2018                                                                           | Spring | 2017-2018    |
| 233        | 1186          | Summer 2018                                                                           | Summer | 2018-2019    |
| 233        | 1189          | Fall 2018                                                                             | Fall   | 2018-2019    |
| 233        | 1193          | Spring 2019                                                                           | Spring | 2018-2019    |
| 233        | 1196          | Summer 2019                                                                           | Summer | 2019-2020    |
| 233        | 1199          | Fall 2019                                                                             | Fall   | 2019-2020    |
| 234        | 1139          | Fall 2013                                                                             | Fall   | 2013-2014    |
| 234        | 1143          | Spring 2014                                                                           | Spring | 2013-2014    |
| 234        | 1146          | Summer 2014                                                                           | Summer | 2014-2015    |
| 234        | 1149          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 234        | 1153          | Spring 2015                                                                           | Spring | 2014-2015    |
| 234        | 1156          | Summer 2015                                                                           | Summer | 2015-2016    |
| 234        | 1159          | Fall 2015                                                                             | Fall   | 2015-2016    |
| 234        | 1163          | Spring 2016                                                                           | Spring | 2015-2016    |
| 234        | 1166          | Summer 2016                                                                           | Summer | 2016-2017    |
| 234        | 1169          | Fall 2016                                                                             | Fall   | 2016-2017    |
| 234        | 1173          | Spring 2017                                                                           | Spring | 2016-2017    |
| 234        | 1176          | Summer 2017                                                                           | Summer | 2017-2018    |
| 234        | 1179          | Fall 2017                                                                             | Fall   | 2017-2018    |
| 234        | 1183          | Spring 2018                                                                           | Spring | 2017-2018    |
| 234        | 1186          | Summer 2018                                                                           | Summer | 2018-2019    |
| 234        | 1189          | Fall 2018                                                                             | Fall   | 2018-2019    |
| 234        | 1193          | Spring 2019                                                                           | Spring | 2018-2019    |
| 234        | 1196          | Summer 2019                                                                           | Summer | 2019-2020    |
| 234        | 1199          | Fall 2019                                                                             | Fall   | 2019-2020    |
| 241        | 16/FA         | Fall 2016                                                                             | Fall   | 2016-2017    |
| 241        | 16/SP         | Spring 2016                                                                           | Spring | 2015-2016    |
| 241        | 16/SU         | Summer 2016                                                                           | Summer | 2016-2017    |
| 241        | 17/FA         | Fall 2017                                                                             | Fall   | 2017-2018    |
| 241        | 17/SP         | Spring 2017                                                                           | Spring | 2016-2017    |
| 241        | 17/SU         | Summer 2017                                                                           | Summer | 2017-2018    |
| 241        | 18/FA         | Fall 2018                                                                             | Fall   | 2018-2019    |
| 241        | 18/SP         | Spring 2018                                                                           | Spring | 2017-2018    |
| 241        | 18/SU         | Summer 2018                                                                           | Summer | 2018-2019    |
| 241        | 19/SP         | Spring 2019                                                                           | Spring | 2018-2019    |
| 261        | 20133         | Spring 2013                                                                           | Spring | 2012-2013    |
| 261        | 20135         | Summer 2013                                                                           | Summer | 2013-2014    |
| 261        | 20137         | Fall 2013                                                                             | Fall   | 2013-2014    |
| 261        | 20143         | Spring 2014                                                                           | Spring | 2013-2014    |
| 261        | 20145         | Summer 2014                                                                           | Summer | 2014-2015    |
| 261        | 20147         | Fall 2014                                                                             | Fall   | 2014-2015    |
| 261        | 20153         | Spring 2015                                                                           | Spring | 2014-2015    |
| 261        | 20155         | Summer 2015                                                                           | Summer | 2015-2016    |
| 261        | 20157         | Fall 2015                                                                             | Fall   | 2015-2016    |
| 261        | 20163         | Spring 2016                                                                           | Spring | 2015-2016    |
| 261        | 20165         | Summer 2016                                                                           | Summer | 2016-2017    |
| 261        | 20167         | Fall 2016                                                                             | Fall   | 2016-2017    |
| 261        | 20173         | Spring 2017                                                                           | Spring | 2016-2017    |
| 261        | 20175         | Summer 2017                                                                           | Summer | 2017-2018    |
| 261        | 20177         | Fall 2017                                                                             | Fall   | 2017-2018    |
| 261        | 20183         | Spring 2018                                                                           | Spring | 2017-2018    |
| 261        | 20185         | Summer 2018                                                                           | Summer | 2018-2019    |
| 261        | 20187         | Fall 2018                                                                             | Fall   | 2018-2019    |
| 261        | 2018XF        | Summer 2018 and Fall 2018                                                             | Summer | 2018-2019    |
| 261        | 20193         | Spring 2019                                                                           | Spring | 2018-2019    |
| 261        | 20197         | Fall 2019                                                                             | Fall   | 2019-2020    |
| 261        | 2019XF        | Summer 2019 and Fall 2019                                                             | Summer | 2019-2020    |
| 271        | 201480        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 271        | 201540        | Spring 2015                                                                           | Spring | 2014-2015    |
| 271        | 201560        | Summer 2015                                                                           | Summer | 2015-2016    |
| 271        | 201580        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 271        | 201640        | Spring 2016                                                                           | Spring | 2015-2016    |
| 271        | 201660        | Summer 2016                                                                           | Summer | 2016-2017    |
| 271        | 201680        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 271        | 201740        | Spring 2017                                                                           | Spring | 2016-2017    |
| 271        | 201760        | Summer 2017                                                                           | Summer | 2017-2018    |
| 271        | 201780        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 271        | 201840        | Spring 2018                                                                           | Spring | 2017-2018    |
| 271        | 201860        | Summer 2018                                                                           | Summer | 2018-2019    |
| 271        | 201880        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 271        | 201940        | Spring 2019                                                                           | Spring | 2018-2019    |
| 271        | 201960        | Summer 2019                                                                           | Summer | 2019-2020    |
| 281        | 201410        | Spring 2014                                                                           | Spring | 2013-2014    |
| 281        | 201460        | Summer 2014                                                                           | Summer | 2014-2015    |
| 281        | 201480        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 281        | 201510        | Spring 2015                                                                           | Spring | 2014-2015    |
| 281        | 201560        | Summer 2015                                                                           | Summer | 2015-2016    |
| 281        | 201580        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 281        | 201610        | Spring 2016                                                                           | Spring | 2015-2016    |
| 281        | 201660        | Summer 2016                                                                           | Summer | 2016-2017    |
| 281        | 201680        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 281        | 201710        | Spring 2017                                                                           | Spring | 2016-2017    |
| 281        | 201760        | Summer 2017                                                                           | Summer | 2017-2018    |
| 281        | 201780        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 281        | 201806        | Summer Deleted 2018                                                                   | Summer | 2018-2019    |
| 281        | 201810        | Spring 2018                                                                           | Spring | 2017-2018    |
| 281        | 201860        | Summer 2018                                                                           | Summer | 2018-2019    |
| 281        | 201880        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 291        | 2013FA        | Fall 2013                                                                             | Fall   | 2013-2014    |
| 291        | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 291        | 2014SP        | Spring 2014                                                                           | Spring | 2013-2014    |
| 291        | 2014SU        | Summer 2014                                                                           | Summer | 2014-2015    |
| 291        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 291        | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
| 291        | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
| 291        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 291        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 291        | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
| 291        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 291        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 291        | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
| 291        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 291        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 291        | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
| 291        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 292        | 2013FA        | Fall 2013                                                                             | Fall   | 2013-2014    |
| 292        | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 292        | 2014SP        | Spring 2014                                                                           | Spring | 2013-2014    |
| 292        | 2014SU        | Summer 2014                                                                           | Summer | 2014-2015    |
| 292        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 292        | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
| 292        | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
| 292        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 292        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 292        | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
| 292        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 292        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 292        | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
| 292        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 292        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 292        | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
| 292        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 311        | 2012FA        | Fall 2012                                                                             | Fall   | 2012-2013    |
| 311        | 2013FA        | Fall 2013                                                                             | Fall   | 2013-2014    |
| 311        | 2013SP        | Spring 2013                                                                           | Spring | 2012-2013    |
| 311        | 2013SU        | Summer 2013                                                                           | Summer | 2013-2014    |
| 311        | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 311        | 2014SP        | Spring 2014                                                                           | Spring | 2013-2014    |
| 311        | 2014SU        | Summer 2014                                                                           | Summer | 2014-2015    |
| 311        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 311        | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
| 311        | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
| 311        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 311        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 311        | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
| 311        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 311        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 311        | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
| 311        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 311        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 311        | 2018su        | Summer 2018                                                                           | Summer | 2018-2019    |
| 311        | 2019FA        | Fall 2019                                                                             | Fall   | 2019-2020    |
| 311        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 311        | 2019SU        | Sumner 2019                                                                           | Summer | 2019-2020    |
| 312        | 2012FA        | Fall 2012                                                                             | Fall   | 2012-2013    |
| 312        | 2013FA        | Fall 2013                                                                             | Fall   | 2013-2014    |
| 312        | 2013SP        | Spring 2013                                                                           | Spring | 2012-2013    |
| 312        | 2013SU        | Summer 2013                                                                           | Summer | 2013-2014    |
| 312        | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 312        | 2014SP        | Spring 2014                                                                           | Spring | 2013-2014    |
| 312        | 2014SU        | Summer 2014                                                                           | Summer | 2014-2015    |
| 312        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 312        | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
| 312        | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
| 312        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 312        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 312        | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
| 312        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 312        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 312        | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
| 312        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 312        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 312        | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
| 312        | 2019FA        | Fall 2019                                                                             | Fall   | 2019-2020    |
| 312        | 2019sp        | Spring 2019                                                                           | Spring | 2018-2019    |
| 312        | 2019SU        | Summer 2019                                                                           | Summer | 2019-2020    |
| 313        | 2012FA        | Fall 2012                                                                             | Fall   | 2012-2013    |
| 313        | 2013FA        | Fall 2013                                                                             | Fall   | 2013-2014    |
| 313        | 2013SP        | Spring 2013                                                                           | Spring | 2012-2013    |
| 313        | 2013SU        | Summer 2013                                                                           | Summer | 2013-2014    |
| 313        | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 313        | 2014SP        | Spring 2014                                                                           | Spring | 2013-2014    |
| 313        | 2014SU        | Summer 2014                                                                           | Summer | 2014-2015    |
| 313        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 313        | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
| 313        | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
| 313        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 313        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 313        | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
| 313        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 313        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 313        | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
| 313        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 313        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 313        | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
| 313        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 334        | 201410        | Spring 2014 Credit/Noncredit                                                          | Spring | 2013-2014    |
| 334        | 201460        | Summer 2014 Credit/Noncredit                                                          | Summer | 2014-2015    |
| 334        | 201480        | Fall 2014 Credit/Noncredit                                                            | Fall   | 2014-2015    |
| 334        | 201510        | Spring 2015 Credit/Noncredit                                                          | Spring | 2014-2015    |
| 334        | 201560        | Summer 2015 Credit/Noncredit                                                          | Summer | 2015-2016    |
| 334        | 201580        | Fall 2015 Credit/Noncredit                                                            | Fall   | 2015-2016    |
| 334        | 201610        | Spring 2016 Credit/NonCredit                                                          | Spring | 2015-2016    |
| 334        | 201660        | Summer 2016 Credit/Noncredit                                                          | Summer | 2016-2017    |
| 334        | 201680        | Fall 2016 Credit/Noncredit                                                            | Fall   | 2016-2017    |
| 334        | 201710        | Spring 2017 Credit/Noncredit                                                          | Spring | 2016-2017    |
| 334        | 201760        | Summer/Fall 2017 Credit/Noncredit                                                     | Summer | 2017-2018    |
| 334        | 201780        | Fall 2017 Credit/Non Credit                                                           | Fall   | 2017-2018    |
| 334        | 201810        | Spring 2018 Credit/Noncredit                                                          | Spring | 2017-2018    |
| 334        | 201860        | Summer/Fall 2018 Credit/Noncredit                                                     | Summer | 2018-2019    |
| 334        | 201880        | Fall 2018 Credit/Non Credit                                                           | Fall   | 2018-2019    |
| 334        | 201910        | Spring 2019 Credit/Noncredit                                                          | Spring | 2018-2019    |
| 341        | 1144          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 341        | 1152          | Spring 2015/Spring Intersession 2015                                                  | Winter | 2014-2015    |
| 341        | 1153          | Summer 2015                                                                           | Summer | 2015-2016    |
| 341        | 1154          | Fall 2015/Winter Intersession 2015                                                    | Fall   | 2015-2016    |
| 341        | 1162          | Spring 2016/Spring Intersession 2016                                                  | Winter | 2015-2016    |
| 341        | 1163          | Summer 2016                                                                           | Summer | 2016-2017    |
| 341        | 1164          | Fall 2016/Winter Intersession 2016                                                    | Fall   | 2016-2017    |
| 341        | 1172          | Spring 2017/Spring Intersession 2017                                                  | Winter | 2016-2017    |
| 341        | 1173          | Summer 2017                                                                           | Summer | 2017-2018    |
| 341        | 1174          | Fall 2017/Winter Intersession 2017                                                    | Fall   | 2017-2018    |
| 341        | 1182          | Spring 2018/Spring Intersession 2018                                                  | Winter | 2017-2018    |
| 341        | 1183          | Summer 2018                                                                           | Summer | 2018-2019    |
| 341        | 1184          | Fall 2018/Winter Intersession 2018                                                    | Fall   | 2018-2019    |
| 341        | 1192          | Spring 2019/Spring Intersession 2019                                                  | Winter | 2018-2019    |
| 343        | 1144          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 343        | 1152          | Spring 2015                                                                           | Spring | 2014-2015    |
| 343        | 1153          | Summer 2015                                                                           | Summer | 2015-2016    |
| 343        | 1154          | Fall 2015/Winter Intersession 2015                                                    | Fall   | 2015-2016    |
| 343        | 1162          | Spring 2016/Spring Intersession 2016                                                  | Winter | 2015-2016    |
| 343        | 1163          | Summer 2016                                                                           | Summer | 2016-2017    |
| 343        | 1164          | Fall 2016/Winter Intersession 2016                                                    | Fall   | 2016-2017    |
| 343        | 1172          | Spring 2017/Spring Intersession 2017                                                  | Winter | 2016-2017    |
| 343        | 1173          | Summer 2017                                                                           | Summer | 2017-2018    |
| 343        | 1174          | Fall 2017/Winter Intersession 2017                                                    | Fall   | 2017-2018    |
| 343        | 1182          | Spring 2018/Spring Intersession 2018                                                  | Winter | 2017-2018    |
| 343        | 1183          | Summer 2018                                                                           | Summer | 2018-2019    |
| 343        | 1184          | Fall 2018/Winter Intersession 2018                                                    | Fall   | 2018-2019    |
| 343        | 1192          | Spring 2019/Spring Intersession 2019                                                  | Winter | 2018-2019    |
| 344        | 1144          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 344        | 1152          | Spring 2015                                                                           | Spring | 2014-2015    |
| 344        | 1153          | Summer 2015                                                                           | Summer | 2015-2016    |
| 344        | 1154          | Fall 2015/Winter Intersession 2015                                                    | Fall   | 2015-2016    |
| 344        | 1162          | Spring 2016/Spring Intersession 2016                                                  | Winter | 2015-2016    |
| 344        | 1163          | Summer 2016                                                                           | Summer | 2016-2017    |
| 344        | 1164          | Fall 2016/Winter Intersession 2016                                                    | Fall   | 2016-2017    |
| 344        | 1172          | Spring 2017/Spring Intersession 2017                                                  | Winter | 2016-2017    |
| 344        | 1173          | Summer 2017                                                                           | Summer | 2017-2018    |
| 344        | 1174          | Fall 2017/Winter Intersession 2017                                                    | Fall   | 2017-2018    |
| 344        | 1182          | Spring 2018/Spring Intersession 2018                                                  | Winter | 2017-2018    |
| 344        | 1183          | Summer 2018                                                                           | Summer | 2018-2019    |
| 344        | 1184          | Fall 2018/Winter Intersession 2018                                                    | Fall   | 2018-2019    |
| 344        | 1192          | Spring 2019/Spring Intersession 2019                                                  | Winter | 2018-2019    |
| 345        | 1144          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 345        | 1152          | Spring 2015                                                                           | Spring | 2014-2015    |
| 345        | 1153          | Summer 2015                                                                           | Summer | 2015-2016    |
| 345        | 1154          | Fall 2015/Winter Intersession 2015                                                    | Fall   | 2015-2016    |
| 345        | 1162          | Spring 2016/Spring Intersession 2016                                                  | Winter | 2015-2016    |
| 345        | 1163          | Summer 2016                                                                           | Summer | 2016-2017    |
| 345        | 1164          | Fall 2016/Winter Intersession 2016                                                    | Fall   | 2016-2017    |
| 345        | 1172          | Spring 2017/Spring Intersession 2017                                                  | Winter | 2016-2017    |
| 345        | 1173          | Summer 2017                                                                           | Summer | 2017-2018    |
| 345        | 1174          | Fall 2017/Winter Intersession 2017                                                    | Fall   | 2017-2018    |
| 345        | 1182          | Spring 2018/Spring Intersession 2018                                                  | Winter | 2017-2018    |
| 345        | 1183          | Summer 2018                                                                           | Summer | 2018-2019    |
| 345        | 1184          | Fall 2018/Winter Intersession 2018                                                    | Fall   | 2018-2019    |
| 345        | 1192          | Spring 2019/Spring Intersession 2019                                                  | Winter | 2018-2019    |
| 361        | 201530        | Spring 2015                                                                           | Spring | 2014-2015    |
| 361        | 201570        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 361        | 201630        | Spring 2016                                                                           | Spring | 2015-2016    |
| 361        | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
| 361        | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 361        | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
| 361        | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
| 361        | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 361        | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
| 361        | 201850        | Summer 2018                                                                           | Summer | 2018-2019    |
| 361        | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 361        | 201930        | Spring 2019                                                                           | Spring | 2018-2019    |
| 371        | 201508        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 371        | 201603        | Spring 2016                                                                           | Spring | 2015-2016    |
| 371        | 201605        | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
| 371        | 201608        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 371        | 201703        | Spring 2017                                                                           | Spring | 2016-2017    |
| 371        | 201705        | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
| 371        | 201708        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 371        | 201803        | Spring 2018                                                                           | Spring | 2017-2018    |
| 371        | 201805        | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
| 371        | 201808        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 371        | 201903        | Spring 2019                                                                           | Spring | 2018-2019    |
| 372        | 201308        | Fall 2013                                                                             | Fall   | 2013-2014    |
| 372        | 201403        | Spring 2014                                                                           | Spring | 2013-2014    |
| 372        | 201405        | Summer/Fall 2014                                                                      | Summer | 2014-2015    |
| 372        | 201408        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 372        | 201503        | Spring 2015                                                                           | Spring | 2014-2015    |
| 372        | 201505        | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
| 372        | 201508        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 372        | 201603        | Spring 2016                                                                           | Spring | 2015-2016    |
| 372        | 201605        | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
| 372        | 201608        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 372        | 201703        | Spring 2017                                                                           | Spring | 2016-2017    |
| 372        | 201705        | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
| 372        | 201708        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 372        | 201803        | Spring 2018                                                                           | Spring | 2017-2018    |
| 372        | 201805        | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
| 372        | 201808        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 372        | 201903        | Spring 2019                                                                           | Spring | 2018-2019    |
| 372        | 201905        | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
| 372        | 37211         | Fall 2011 - College of San Mateo                                                      | Fall   | 2011-2012    |
| 372        | 37212         | Spring 2012 - College of San Mateo                                                    | Spring | 2011-2012    |
| 373        | 201308        | Fall 2013                                                                             | Fall   | 2013-2014    |
| 373        | 201403        | Spring 2014                                                                           | Spring | 2013-2014    |
| 373        | 201405        | Summer/Fall 2014                                                                      | Summer | 2014-2015    |
| 373        | 201408        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 373        | 201503        | Spring 2015                                                                           | Spring | 2014-2015    |
| 373        | 201505        | Summer 2015                                                                           | Summer | 2015-2016    |
| 373        | 201508        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 373        | 201603        | Spring 2016                                                                           | Spring | 2015-2016    |
| 373        | 201605        | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
| 373        | 201608        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 373        | 201703        | Spring 2017                                                                           | Spring | 2016-2017    |
| 373        | 201705        | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
| 373        | 201708        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 373        | 201803        | Spring 2018                                                                           | Spring | 2017-2018    |
| 373        | 201805        | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
| 373        | 201808        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 373        | 201903        | Spring 2019                                                                           | Spring | 2018-2019    |
| 373        | 201905        | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
| 373        | 37311         | Fall 2011 - Skyline College                                                           | Fall   | 2011-2012    |
| 373        | 37312         | Spring 2012 - Skyline College                                                         | Spring | 2011-2012    |
| 373        | Fall 2017     | Fall 2017                                                                             | Fall   | 2017-2018    |
| 411        | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 411        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 411        | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
| 411        | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
| 411        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 411        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 411        | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
| 411        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 411        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 411        | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
| 411        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 411        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 411        | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
| 411        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 421        | 201512        | Summer 2014                                                                           | Summer | 2014-2015    |
| 421        | 201522        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 421        | 201532        | Winter 2015                                                                           | Winter | 2014-2015    |
| 421        | 201542        | Spring 2015                                                                           | Spring | 2014-2015    |
| 421        | 201612        | Summer 2015                                                                           | Summer | 2015-2016    |
| 421        | 201622        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 421        | 201632        | Winter 2016                                                                           | Winter | 2015-2016    |
| 421        | 201642        | Spring 2016                                                                           | Spring | 2015-2016    |
| 421        | 201712        | Summer 2016                                                                           | Summer | 2016-2017    |
| 421        | 201722        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 421        | 201732        | Winter 2017                                                                           | Winter | 2016-2017    |
| 421        | 201742        | Spring 2017                                                                           | Spring | 2016-2017    |
| 421        | 201812        | Summer 2017                                                                           | Summer | 2017-2018    |
| 421        | 201822        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 421        | 201823        | Winter 2120                                                                           | Winter | 2017-2018    |
| 421        | 201832        | Winter 2018                                                                           | Winter | 2017-2018    |
| 421        | 201842        | Spring 2018                                                                           | Spring | 2017-2018    |
| 421        | 201912        | Summer 2018                                                                           | Summer | 2018-2019    |
| 421        | 201922        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 421        | 201932        | Winter 2019                                                                           | Winter | 2018-2019    |
| 421        | 201942        | Spring 2019                                                                           | Spring | 2018-2019    |
| 422        | 201121        | Fall 2011 - Foothill College                                                          | Fall   | 2011-2012    |
| 422        | 201241        | Spring 2012 - Foothill College                                                        | Spring | 2011-2012    |
| 422        | 201511        | Summer 2014 -  Foothill College                                                       | Summer | 2014-2015    |
| 422        | 201521        | Fall 2014 Apprenticeship Program                                                      | Fall   | 2014-2015    |
| 422        | 201531        | Winter 2015 - Foothill College                                                        | Winter | 2014-2015    |
| 422        | 201541        | Spring 2015 - Foothill College                                                        | Spring | 2014-2015    |
| 422        | 201611        | Summer 2015 - Foothill College                                                        | Summer | 2015-2016    |
| 422        | 201621        | Fall 2015 - Foothill College                                                          | Fall   | 2015-2016    |
| 422        | 201631        | Winter 2016 - Foothill College                                                        | Winter | 2015-2016    |
| 422        | 201641        | Spring 2016 - Foothill College                                                        | Spring | 2015-2016    |
| 422        | 201711        | Summer 2016 - Foothill College                                                        | Summer | 2016-2017    |
| 422        | 201721        | Fall 2016 Apprenticeship Program - Foothill College                                   | Fall   | 2016-2017    |
| 422        | 201731        | Winter 2017 - Foothill College                                                        | Winter | 2016-2017    |
| 422        | 201741        | Spring 2017 - Foothill College                                                        | Spring | 2016-2017    |
| 422        | 201811        | Summer 2017 - Foothill College                                                        | Summer | 2017-2018    |
| 422        | 201821        | Fall 2017 - SOUTH BAY                                                                 | Fall   | 2017-2018    |
| 422        | 201831        | Winter 2018 South Bay - Foothill College                                              | Winter | 2017-2018    |
| 422        | 201841        | Spring 2018 - Foothill College                                                        | Spring | 2017-2018    |
| 422        | 201911        | Summer 2018 - Foothill College                                                        | Summer | 2018-2019    |
| 422        | 201921        | Fall 2018 - Foothill College                                                          | Fall   | 2018-2019    |
| 422        | 201931        | Winter 2019 - Foothill College                                                        | Winter | 2018-2019    |
| 422        | 201941        | Spring 2019 - Foothill College                                                        | Spring | 2018-2019    |
| 431        | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 431        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 431        | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
| 431        | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
| 431        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 431        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 431        | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
| 431        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 431        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 431        | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
| 431        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 431        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 431        | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
| 431        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 431        | 2019SU        | Summer 2019                                                                           | Summer | 2019-2020    |
| 441        | 201470        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 441        | 201530        | Spring 2015                                                                           | Spring | 2014-2015    |
| 441        | 201550        | Summer 2015                                                                           | Summer | 2015-2016    |
| 441        | 201570        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 441        | 201600        | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
| 441        | 201630        | Spring 2016                                                                           | Spring | 2015-2016    |
| 441        | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 441        | 201700        | Summer and Fall 2017                                                                  | Summer | 2017-2018    |
| 441        | 201730        | Winter Intersession and Spring 2017                                                   | Winter | 2016-2017    |
| 441        | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
| 441        | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 441        | 201800        | Summer and Fall 2018                                                                  | Summer | 2018-2019    |
| 441        | 201830        | Winter and Spring 2018                                                                | Winter | 2017-2018    |
| 441        | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 441        | 201930        | Winter Intersession/Spring Semester 2019                                              | Winter | 2018-2019    |
| 441        | 201950/201970 | Summer and Fall 2019                                                                  | Summer | 2019-2020    |
| 451        | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 451        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 451        | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
| 451        | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
| 451        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 451        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 451        | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
| 451        | 2017FA        | FAll 2017                                                                             | Fall   | 2017-2018    |
| 451        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 451        | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
| 451        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 451        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 451        | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
| 451        | 2019FA        | Fall 2019                                                                             | Fall   | 2019-2020    |
| 451        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 451        | 2019SU        | Summer 2019                                                                           | Summer | 2019-2020    |
| 461        | 20153         | Spring 2015                                                                           | Spring | 2014-2015    |
| 461        | 20155         | Summer 2015                                                                           | Summer | 2015-2016    |
| 461        | 20157         | Fall 2015                                                                             | Fall   | 2015-2016    |
| 461        | 20163         | Spring 2016                                                                           | Spring | 2015-2016    |
| 461        | 20165         | Summer 2016                                                                           | Summer | 2016-2017    |
| 461        | 20167         | Fall 2016                                                                             | Fall   | 2016-2017    |
| 461        | 20173         | Spring 2017                                                                           | Spring | 2016-2017    |
| 461        | 20175         | Summer 2017                                                                           | Summer | 2017-2018    |
| 461        | 20177         | Fall 2017                                                                             | Fall   | 2017-2018    |
| 461        | 20183         | Spring 2018                                                                           | Spring | 2017-2018    |
| 461        | 20185         | Summer 2018                                                                           | Summer | 2018-2019    |
| 461        | 20187         | Fall 2018                                                                             | Fall   | 2018-2019    |
| 461        | 20193         | Spring 2019                                                                           | Spring | 2018-2019    |
| 471        | 2016FAR       | Fall 2016                                                                             | Fall   | 2016-2017    |
| 471        | 2016SPR       | Spring 2016                                                                           | Spring | 2015-2016    |
| 471        | 2016SUR       | Summer 2016                                                                           | Summer | 2016-2017    |
| 471        | 2017FAR       | Fall 2017                                                                             | Fall   | 2017-2018    |
| 471        | 2017SPR       | Spring 2017                                                                           | Spring | 2016-2017    |
| 471        | 2017SUR       | Summer 2017                                                                           | Summer | 2017-2018    |
| 471        | 2018FAR       | Fall 2018                                                                             | Fall   | 2018-2019    |
| 471        | 2018SPR       | Spring 2018                                                                           | Spring | 2017-2018    |
| 471        | 2018SUR       | Summer 2018                                                                           | Summer | 2018-2019    |
| 471        | 2019SPR       | Intersession & Spring 2019                                                            | Winter | 2018-2019    |
| 472        | 2016FAR       | Fall 2016                                                                             | Fall   | 2016-2017    |
| 472        | 2016SPR       | Spring 2016                                                                           | Spring | 2015-2016    |
| 472        | 2016SUR       | Summer 2016                                                                           | Summer | 2016-2017    |
| 472        | 2017FAR       | Fall 2017                                                                             | Fall   | 2017-2018    |
| 472        | 2017SPR       | Spring 2017                                                                           | Spring | 2016-2017    |
| 472        | 2017SUR       | Summer 2017                                                                           | Summer | 2017-2018    |
| 472        | 2018FAR       | Fall 2018                                                                             | Fall   | 2018-2019    |
| 472        | 2018SPR       | Spring 2018                                                                           | Spring | 2017-2018    |
| 472        | 2018SUR       | Summer 2018                                                                           | Summer | 2018-2019    |
| 472        | 2019SPR       | Spring 2019                                                                           | Spring | 2018-2019    |
| 472        | 2019SUR       | Summer 2019                                                                           | Summer | 2019-2020    |
| 481        | 201303        | Spring 2014 (ROP Only)                                                                | Spring | 2013-2014    |
| 481        | 201401        | Summer 2014                                                                           | Summer | 2014-2015    |
| 481        | 201402        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 481        | 201403        | Spring 2015 (ROP Only)                                                                | Spring | 2014-2015    |
| 481        | 201501        | Summer 2015                                                                           | Summer | 2015-2016    |
| 481        | 201502        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 481        | 201503        | Spring 2016                                                                           | Spring | 2015-2016    |
| 481        | 201601        | Summer 2016                                                                           | Summer | 2016-2017    |
| 481        | 201602        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 481        | 201603        | Spring 2017                                                                           | Spring | 2016-2017    |
| 481        | 201701        | Summer 2017                                                                           | Summer | 2017-2018    |
| 481        | 201702        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 481        | 201703        | Spring 2018                                                                           | Spring | 2017-2018    |
| 481        | 201801        | Summer 2018                                                                           | Summer | 2018-2019    |
| 481        | 201802        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 481        | 201803        | Spring 2019                                                                           | Spring | 2018-2019    |
| 481        | 201901        | Summer 2019                                                                           | Summer | 2019-2020    |
| 481        | 201902        | Fall 2019                                                                             | Fall   | 2019-2020    |
| 482        | 201401        | Summer 2014                                                                           | Summer | 2014-2015    |
| 482        | 201402        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 482        | 201403        | Spring 2015                                                                           | Spring | 2014-2015    |
| 482        | 201501        | Summer 2015                                                                           | Summer | 2015-2016    |
| 482        | 201502        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 482        | 201503        | Spring 2016                                                                           | Spring | 2015-2016    |
| 482        | 201601        | Summer 2016                                                                           | Summer | 2016-2017    |
| 482        | 201602        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 482        | 201603        | Spring 2017                                                                           | Spring | 2016-2017    |
| 482        | 201701        | Summer 2017                                                                           | Summer | 2017-2018    |
| 482        | 201702        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 482        | 201703        | Spring 2018                                                                           | Spring | 2017-2018    |
| 482        | 201801        | Summer 2018                                                                           | Summer | 2018-2019    |
| 482        | 201802        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 482        | 201803        | Spring 2019                                                                           | Spring | 2018-2019    |
| 482        | 201901        | Summer 2019                                                                           | Summer | 2019-2020    |
| 482        | 201902        | Fall 2019                                                                             | Fall   | 2019-2020    |
| 492        | 2015FA        | 2015 Fall                                                                             | Fall   | 2015-2016    |
| 492        | 2015SP        | 2015 Spring                                                                           | Spring | 2014-2015    |
| 492        | 2015SU        | 2015 Summer                                                                           | Summer | 2015-2016    |
| 492        | 2016FA        | 2016 Fall                                                                             | Fall   | 2016-2017    |
| 492        | 2016SP        | 2016 Spring                                                                           | Spring | 2015-2016    |
| 492        | 2016SU        | 2016 Summer                                                                           | Summer | 2016-2017    |
| 492        | 2016WI        | 2016 Winter                                                                           | Winter | 2015-2016    |
| 492        | 201799        | Summer 2017 - Fall 2017                                                               | Summer | 2017-2018    |
| 492        | 2017FA        | 2017 Fall                                                                             | Fall   | 2017-2018    |
| 492        | 2017SP        | 2017 Spring                                                                           | Spring | 2016-2017    |
| 492        | 2017SU        | 2017 Summer                                                                           | Summer | 2017-2018    |
| 492        | 2017WI        | 2017 Winter                                                                           | Winter | 2016-2017    |
| 492        | 201800        | Winter 2018 - Spring 2018                                                             | Winter | 2017-2018    |
| 492        | 201899        | Summer 2018 - Fall 2018                                                               | Summer | 2018-2019    |
| 492        | 201900        | Winter 2019 - Spring 2019                                                             | Winter | 2018-2019    |
| 493        | 2015FA        | 2015 Fall                                                                             | Fall   | 2015-2016    |
| 493        | 2015SP        | 2015 Spring                                                                           | Spring | 2014-2015    |
| 493        | 2015SU        | 2015 Summer                                                                           | Summer | 2015-2016    |
| 493        | 2016FA        | 2016 Fall                                                                             | Fall   | 2016-2017    |
| 493        | 2016SP        | 2016 Spring                                                                           | Spring | 2015-2016    |
| 493        | 2016SU        | 2016 Summer                                                                           | Summer | 2016-2017    |
| 493        | 2016WI        | 2016 Winter                                                                           | Winter | 2015-2016    |
| 493        | 201750        | 2017 Summer                                                                           | Summer | 2017-2018    |
| 493        | 201770        | 2017 Fall                                                                             | Fall   | 2017-2018    |
| 493        | 201799        | Summer 2017 - Fall 2017                                                               | Summer | 2017-2018    |
| 493        | 2017SP        | 2017 Spring                                                                           | Spring | 2016-2017    |
| 493        | 2017WI        | 2017 Winter                                                                           | Winter | 2016-2017    |
| 493        | 201800        | Winter 2018 - Spring 2018                                                             | Winter | 2017-2018    |
| 493        | 201899        | Summer 2018 - Fall 2018                                                               | Summer | 2018-2019    |
| 521        | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
| 521        | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 521        | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
| 521        | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
| 521        | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 521        | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
| 521        | 201850        | Summer 2018                                                                           | Summer | 2018-2019    |
| 521        | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 521        | 201930        | Spring 2019                                                                           | Spring | 2018-2019    |
| 522        | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
| 522        | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 522        | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
| 522        | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
| 522        | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 522        | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
| 522        | 201850        | Summer 2018                                                                           | Summer | 2018-2019    |
| 522        | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 523        | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
| 523        | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 523        | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
| 523        | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
| 523        | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 523        | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
| 523        | 201850        | Summer 2018                                                                           | Summer | 2018-2019    |
| 523        | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 531        | 2013F         | Fall 2013                                                                             | Fall   | 2013-2014    |
| 531        | 2013S         | Spring 2013                                                                           | Spring | 2012-2013    |
| 531        | 2013U         | Summer 2013                                                                           | Summer | 2013-2014    |
| 531        | 2014F         | Fall 2014                                                                             | Fall   | 2014-2015    |
| 531        | 2014S         | Spring 2014                                                                           | Spring | 2013-2014    |
| 531        | 2014U         | Summer 2014                                                                           | Summer | 2014-2015    |
| 531        | 2015F         | Fall 2015                                                                             | Fall   | 2015-2016    |
| 531        | 2015S         | Spring 2015                                                                           | Spring | 2014-2015    |
| 531        | 2015U         | Summer 2015                                                                           | Summer | 2015-2016    |
| 531        | 2016F         | Fall 2016                                                                             | Fall   | 2016-2017    |
| 531        | 2016S         | Spring 2016                                                                           | Spring | 2015-2016    |
| 531        | 2016U         | Summer 2016                                                                           | Summer | 2016-2017    |
| 531        | 2017F         | Fall 2017 - wrong                                                                     | Fall   | 2017-2018    |
| 531        | 2017S         | Spring 2017                                                                           | Spring | 2016-2017    |
| 531        | 2017U         | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
| 531        | 2017U/F       | 2017 Summer/Fall -wrong                                                               | Summer | 2017-2018    |
| 531        | 2018F         | Fall 2018 - wrong                                                                     | Fall   | 2018-2019    |
| 531        | 2018S         | Spring 2018                                                                           | Spring | 2017-2018    |
| 531        | 2018U         | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
| 531        | 2019S         | Spring 2019                                                                           | Spring | 2018-2019    |
| 531        | 2019U         | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
| 531        | 53111         | Fall 2011 - Merced College                                                            | Fall   | 2011-2012    |
| 531        | 53112         | Spring 2012 - Merced College                                                          | Spring | 2011-2012    |
| 551        | 011617        | Summer 2016                                                                           | Summer | 2016-2017    |
| 551        | 011718        | Summer 2017                                                                           | Summer | 2017-2018    |
| 551        | 011819        | Summer 2018                                                                           | Summer | 2018-2019    |
| 551        | 031819        | Spring 2019                                                                           | Spring | 2018-2019    |
| 551        | 201516        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 551        | 201617        | FALL 2016                                                                             | Fall   | 2016-2017    |
| 551        | 201718        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 551        | 201819        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 551        | 301516        | Spring 2016                                                                           | Spring | 2015-2016    |
| 551        | 301617        | Spring 2017                                                                           | Spring | 2016-2017    |
| 551        | 301718        | Spring 2018                                                                           | Spring | 2017-2018    |
| 561        | 201410        | Fall 2013                                                                             | Fall   | 2013-2014    |
| 561        | 201420        | Spring 2014                                                                           | Spring | 2013-2014    |
| 561        | 201500        | Summer/Fall 2014                                                                      | Summer | 2014-2015    |
| 561        | 201520        | Spring 2015                                                                           | Spring | 2014-2015    |
| 561        | 201600        | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
| 561        | 201620        | Spring 2016                                                                           | Spring | 2015-2016    |
| 561        | 201700        | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
| 561        | 201720        | Spring 2017                                                                           | Spring | 2016-2017    |
| 561        | 201800        | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
| 561        | 201820        | Spring 2018                                                                           | Spring | 2017-2018    |
| 561        | 201900        | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
| 561        | 201920        | Spring 2019                                                                           | Spring | 2018-2019    |
| 561        | 202000        | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
| 571        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 571        | 2015FAENRICH  | Fall 2015 HS Enrichment                                                               | Fall   | 2015-2016    |
| 571        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 571        | 2016FAENRICH  | Fall 2016 Enrichment/Dual Enrollment                                                  | Fall   | 2016-2017    |
| 571        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 571        | 2016SPENRICH  | Spring 2016 HS Enrichment                                                             | Spring | 2015-2016    |
| 571        | 2016SU/FA     | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
| 571        | 2016SUENRICH  | Summer/Fall 2016 Enrichment/Dual Enrollment                                           | Summer | 2016-2017    |
| 571        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 571        | 2017FAENRICH  | Fall 2017 HS Enrichment/Dual Enrollment                                               | Fall   | 2017-2018    |
| 571        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 571        | 2017SPENRICH  | Spring 2017 HS Enrichment/Dual Enrollment                                             | Spring | 2016-2017    |
| 571        | 2017SU/FA     | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
| 571        | 2017SUENRICH  | Summer/Fall 2017 Enrichment/Dual Enrollment                                           | Summer | 2017-2018    |
| 571        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 571        | 2018FAENRICH  | Fall 2018 HS Enrichment/Dual Enrollment                                               | Fall   | 2018-2019    |
| 571        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 571        | 2018SPENRICH  | Spring 2018 HS Enrichment/Dual Enrollment                                             | Spring | 2017-2018    |
| 571        | 2018SU/FA     | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
| 571        | 2018SUENRICH  | Summer/Fall 2018 HS Enrichment/Dual Enrollment                                        | Summer | 2018-2019    |
| 571        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 571        | 2019SPENRICH  | Spring 2019 HS Enrichment/Dual Enrollment                                             | Spring | 2018-2019    |
| 571        | 2019SU/FA     | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
| 571        | 2019SUENRICH  | Summer/Fall 2019 HS Enrichment/Dual Enrollment                                        | Summer | 2019-2020    |
| 572        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 572        | 2015FAENRICH  | Fall 2015 HS Enrichment                                                               | Fall   | 2015-2016    |
| 572        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 572        | 2016FAENRICH  | Fall 2016 Enrichment/Dual Enrollment                                                  | Fall   | 2016-2017    |
| 572        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 572        | 2016SPENRICH  | Spring 2016 HS Enrichment                                                             | Spring | 2015-2016    |
| 572        | 2016SU/FA     | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
| 572        | 2016SUENRICH  | Summer/Fall 2016 Enrichment/Dual Enrollment                                           | Summer | 2016-2017    |
| 572        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 572        | 2017FAENRICH  | Fall 2017 HS Enrichment/Dual Enrollment                                               | Fall   | 2017-2018    |
| 572        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 572        | 2017SPENRICH  | Spring 2017 HS Enrichment/Dual Enrollment                                             | Spring | 2016-2017    |
| 572        | 2017SU/FA     | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
| 572        | 2017SUENRICH  | Summer/Fall 2017 Enrichment/Dual Enrollment                                           | Summer | 2017-2018    |
| 572        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 572        | 2018FAENRICH  | Fall  2018 HS Enrichment/Dual Enrollment                                              | Fall   | 2018-2019    |
| 572        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 572        | 2018SPENRICH  | Spring 2018 HS Enrichment/Dual Enrollment                                             | Spring | 2017-2018    |
| 572        | 2018SU/FA     | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
| 572        | 2018SUENRICH  | Summer/Fall 2018 HS Enrichment/Dual Enrollment                                        | Summer | 2018-2019    |
| 572        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 572        | 2019SPENRICH  | Spring 2019 HS Enrichment/Dual Enrollment                                             | Spring | 2018-2019    |
| 572        | 2019SU/FA     | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
| 572        | 2019SUENRICH  | Summer/Fall 2019 HS Enrichment/Dual Enrollment                                        | Summer | 2019-2020    |
| 576        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 576        | 2015FAENRICH  | Fall 2015 HS Enrichment                                                               | Fall   | 2015-2016    |
| 576        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 576        | 2016FAENRICH  | Fall 2016 Enrichment/Dual Enrollment                                                  | Fall   | 2016-2017    |
| 576        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 576        | 2016SPENRICH  | Spring 2016 HS Enrichment                                                             | Spring | 2015-2016    |
| 576        | 2016SU/FA     | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
| 576        | 2016SUENRICH  | Summer/Fall 2016 Enrichment/Dual Enrollment                                           | Summer | 2016-2017    |
| 576        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 576        | 2017FAENRICH  | Fall 2017 HS Enrichment/Dual Enrollment                                               | Fall   | 2017-2018    |
| 576        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 576        | 2017SPENRICH  | Spring 2017 HS Enrichment/Dual Enrollment                                             | Spring | 2016-2017    |
| 576        | 2017SU/FA     | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
| 576        | 2017SUENRICH  | Summer/Fall 2017 Enrichment/Dual Enrollment                                           | Summer | 2017-2018    |
| 576        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 576        | 2018FAENRICH  | Fall 2018 HS Enrichment/Dual Enrollment                                               | Fall   | 2018-2019    |
| 576        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 576        | 2018SPENRICH  | Spring 2018 HS Enrichment/Dual Enrollment                                             | Spring | 2017-2018    |
| 576        | 2018SU/FA     | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
| 576        | 2018SUENRICH  | Summer/Fall 2018 HS Enrichment/Dual Enrollment                                        | Summer | 2018-2019    |
| 576        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 576        | 2019SPENRICH  | Spring 2019 HS Enrich/Dual Enrollment                                                 | Spring | 2018-2019    |
| 576        | 2019SU/FA     | Summer/Fall 2019                                                                      | Summer | 2019-2020    |
| 576        | 2019SUENRICH  | Summer/Fall 2019 HS Enrichment/Dual Enrollment                                        | Summer | 2019-2020    |
| 581        | 2014FA        | 2014 Fall                                                                             | Fall   | 2014-2015    |
| 581        | 2014SU        | 2014 Summer                                                                           | Summer | 2014-2015    |
| 581        | 2015FA        | 2015 Fall                                                                             | Fall   | 2015-2016    |
| 581        | 2015SP        | 2015 Spring                                                                           | Spring | 2014-2015    |
| 581        | 2015SU        | 2015 Summer                                                                           | Summer | 2015-2016    |
| 581        | 2016FA        | 2016 Fall                                                                             | Fall   | 2016-2017    |
| 581        | 2016SP        | 2016 Spring                                                                           | Spring | 2015-2016    |
| 581        | 2016SU        | 2016 Summer                                                                           | Summer | 2016-2017    |
| 581        | 2017FA        | 2017 Fall                                                                             | Fall   | 2017-2018    |
| 581        | 2017SP        | 2017 Spring                                                                           | Spring | 2016-2017    |
| 581        | 2017SU        | 2017 Summer                                                                           | Summer | 2017-2018    |
| 581        | 2018FA        | 2018 Fall                                                                             | Fall   | 2018-2019    |
| 581        | 2018SP        | 2018 Spring                                                                           | Spring | 2017-2018    |
| 581        | 2018SU        | 2018 Summer                                                                           | Summer | 2018-2019    |
| 581        | 2019FA        | 2019 Fall Term                                                                        | Fall   | 2019-2020    |
| 581        | 2019SP        | 2019 Spring                                                                           | Spring | 2018-2019    |
| 581        | 2019SU        | 2019 Summer Term                                                                      | Summer | 2019-2020    |
| 582        | 2014FA        | 2014 Fall                                                                             | Fall   | 2014-2015    |
| 582        | 2014SU        | 2014 Summer                                                                           | Summer | 2014-2015    |
| 582        | 2015FA        | 2015 Fall                                                                             | Fall   | 2015-2016    |
| 582        | 2015SP        | 2015 Spring                                                                           | Spring | 2014-2015    |
| 582        | 2015SU        | 2015 Summer                                                                           | Summer | 2015-2016    |
| 582        | 2016FA        | 2016 Fall                                                                             | Fall   | 2016-2017    |
| 582        | 2016SP        | 2016 Spring                                                                           | Spring | 2015-2016    |
| 582        | 2016SU        | 2016 Summer                                                                           | Summer | 2016-2017    |
| 582        | 2017FA        | 2017 Fall                                                                             | Fall   | 2017-2018    |
| 582        | 2017SP        | 2017 Spring                                                                           | Spring | 2016-2017    |
| 582        | 2017SU        | 2017 Summer                                                                           | Summer | 2017-2018    |
| 582        | 2018FA        | 2018 Fall                                                                             | Fall   | 2018-2019    |
| 582        | 2018SP        | 2018 Spring                                                                           | Spring | 2017-2018    |
| 582        | 2018SU        | 2018 Summer                                                                           | Summer | 2018-2019    |
| 582        | 2019FA        | 2019 Fall Term                                                                        | Fall   | 2019-2020    |
| 582        | 2019SP        | 2019 Spring                                                                           | Spring | 2018-2019    |
| 582        | 2019SU        | 2019 Summer Term                                                                      | Summer | 2019-2020    |
| 591        | 2014CFA       | Columbia College Fall 2014 Application                                                | Fall   | 2014-2015    |
| 591        | 2014CSU       | Columbia College Summer 2014 Application                                              | Summer | 2014-2015    |
| 591        | 2015CFA       | Columbia College Fall 2015 Application                                                | Fall   | 2015-2016    |
| 591        | 2015CSP       | Columbia College Spring 2015 Application                                              | Spring | 2014-2015    |
| 591        | 2015CSU       | Columbia College Summer 2015 Application                                              | Summer | 2015-2016    |
| 591        | 2016CFA       | Columbia College Fall 2016 Application                                                | Fall   | 2016-2017    |
| 591        | 2016CSP       | Columbia College Spring 2016 Application                                              | Spring | 2015-2016    |
| 591        | 2016CSU       | Columbia College Summer 2016 Application                                              | Summer | 2016-2017    |
| 591        | 2017CFA       | Columbia College Fall 2017 Application                                                | Fall   | 2017-2018    |
| 591        | 2017CSP       | Columbia College Spring 2017 Application                                              | Spring | 2016-2017    |
| 591        | 2017CSU       | Columbia College Summer 2017 Application                                              | Summer | 2017-2018    |
| 591        | 2018CFA       | Columbia College Fall 2018 Application                                                | Fall   | 2018-2019    |
| 591        | 2018CSP       | Columbia College Spring 2018 Application                                              | Spring | 2017-2018    |
| 591        | 2018CSU       | Columbia College Summer 2018 Application                                              | Summer | 2018-2019    |
| 591        | 2019CSP       | Columbia College Spring 2019 Application                                              | Spring | 2018-2019    |
| 592        | 2014MFA       | MJC Fall 2014 Term                                                                    | Fall   | 2014-2015    |
| 592        | 2014MSP       | MJC Spring 2014 Term                                                                  | Spring | 2013-2014    |
| 592        | 2014MSU       | MJC Summer 2014 Term                                                                  | Summer | 2014-2015    |
| 592        | 2015MFA       | MJC Fall 2015 Term                                                                    | Fall   | 2015-2016    |
| 592        | 2015MSP       | MJC Spring 2015 Term                                                                  | Spring | 2014-2015    |
| 592        | 2015MSU       | MJC Summer 2015 Term                                                                  | Summer | 2015-2016    |
| 592        | 2016MFA       | MJC Fall 2016 Term                                                                    | Fall   | 2016-2017    |
| 592        | 2016MSP       | MJC Spring 2016 Term                                                                  | Spring | 2015-2016    |
| 592        | 2016MSU       | MJC Summer 2016 Term                                                                  | Summer | 2016-2017    |
| 592        | 2017MFA       | MJC Fall 2017 Term                                                                    | Fall   | 2017-2018    |
| 592        | 2017MSP       | MJC Spring 2017 Term                                                                  | Spring | 2016-2017    |
| 592        | 2017MSU       | MJC Summer 2017 Term                                                                  | Summer | 2017-2018    |
| 592        | 2018MFA       | MJC Fall 2018 Term                                                                    | Fall   | 2018-2019    |
| 592        | 2018MSP       | MJC Spring 2018 Term                                                                  | Spring | 2017-2018    |
| 592        | 2018MSU       | MJC Summer 2018 Term                                                                  | Summer | 2018-2019    |
| 592        | 2019MSP       | MJC Spring 2019 Term                                                                  | Spring | 2018-2019    |
| 611        | 201710        | Summer 2016                                                                           | Summer | 2016-2017    |
| 611        | 201720        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 611        | 201730        | Winter 2017                                                                           | Winter | 2016-2017    |
| 611        | 201740        | Spring 2017                                                                           | Spring | 2016-2017    |
| 611        | 201810        | Summer 2017                                                                           | Summer | 2017-2018    |
| 611        | 201820        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 611        | 201830        | Winter 2018                                                                           | Winter | 2017-2018    |
| 611        | 201840        | Spring 2018                                                                           | Spring | 2017-2018    |
| 611        | 201910        | Summer 2018                                                                           | Summer | 2018-2019    |
| 611        | 201920        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 611        | 201930        | Winter 2019                                                                           | Winter | 2018-2019    |
| 611        | 201940        | Spring 2019                                                                           | Spring | 2018-2019    |
| 611        | 202010        | Summer 2019                                                                           | Summer | 2019-2020    |
| 611        | 202020        | Fall 2019                                                                             | Fall   | 2019-2020    |
| 621        | 201430        | Spring 2014                                                                           | Spring | 2013-2014    |
| 621        | 201450        | Summer 2014                                                                           | Summer | 2014-2015    |
| 621        | 201470        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 621        | 201510        | Intersession 2015                                                                     | Winter | 2014-2015    |
| 621        | 201530        | Spring 2015                                                                           | Spring | 2014-2015    |
| 621        | 201550        | Summer 2015                                                                           | Summer | 2015-2016    |
| 621        | 201570        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 621        | 201610        | Intersession 2016                                                                     | Winter | 2015-2016    |
| 621        | 201630        | Spring 2016                                                                           | Spring | 2015-2016    |
| 621        | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
| 621        | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 621        | 201710        | Intersession 2017                                                                     | Winter | 2016-2017    |
| 621        | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
| 621        | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
| 621        | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 621        | 201810        | Intersession 2018                                                                     | Winter | 2017-2018    |
| 621        | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
| 621        | 201850        | Summer 2018                                                                           | Summer | 2018-2019    |
| 621        | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 621        | 201910        | Intersession 2019                                                                     | Winter | 2018-2019    |
| 621        | 201930        | Spring 2019                                                                           | Spring | 2018-2019    |
| 641        | 201403        | Spring 2014 - Cuesta College                                                          | Spring | 2013-2014    |
| 641        | 201405        | Summer 2014 - Cuesta College                                                          | Summer | 2014-2015    |
| 641        | 201407        | Fall 2014 - Cuesta College                                                            | Fall   | 2014-2015    |
| 641        | 201503        | Spring 2015 - Cuesta College                                                          | Spring | 2014-2015    |
| 641        | 201505        | Summer Session 2015 - Cuesta College                                                  | Summer | 2015-2016    |
| 641        | 201507        | Fall 2015 - Cuesta College                                                            | Fall   | 2015-2016    |
| 641        | 201603        | Spring 2016 - Cuesta College                                                          | Spring | 2015-2016    |
| 641        | 201605        | Summer Session 2016 - Cuesta College                                                  | Summer | 2016-2017    |
| 641        | 201607        | Fall 2016 - Cuesta College                                                            | Fall   | 2016-2017    |
| 641        | 201703        | Spring 2017 - Cuesta College                                                          | Spring | 2016-2017    |
| 641        | 201705        | Summer Session 2017 - Cuesta College                                                  | Summer | 2017-2018    |
| 641        | 201707        | Fall 2017: August 21 - December 22                                                    | Fall   | 2017-2018    |
| 641        | 201803        | Spring 2018: January 16 - May 18                                                      | Spring | 2017-2018    |
| 641        | 201805        | Summer 2018: June 18 - July 27                                                        | Summer | 2018-2019    |
| 641        | 201807        | Fall 2018: August 13 - December 14                                                    | Fall   | 2018-2019    |
| 641        | 201903        | Spring 2019                                                                           | Spring | 2018-2019    |
| 641        | 201907        | Fall 2019                                                                             | Fall   | 2019-2020    |
| 651        | 201410        | Summer 2013                                                                           | Summer | 2013-2014    |
| 651        | 201430        | Fall 2013                                                                             | Fall   | 2013-2014    |
| 651        | 201450        | Spring 2014                                                                           | Spring | 2013-2014    |
| 651        | 201510        | Summer 2014                                                                           | Summer | 2014-2015    |
| 651        | 201530        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 651        | 201550        | Spring 2015                                                                           | Spring | 2014-2015    |
| 651        | 201630        | Summer I (May 18, 2015), Summer II (June 29, 2015), Fall (August 24, 2015)            | Summer | 2015-2016    |
| 651        | 201650        | Spring 2016                                                                           | Spring | 2015-2016    |
| 651        | 201730        | Summer I (May 16, 2016), Summer II (June 27, 2016), Fall (August 22, 2016)            | Summer | 2016-2017    |
| 651        | 201750        | Spring 2017                                                                           | Spring | 2016-2017    |
| 651        | 201830        | Summer I (May 15, 2017), Summer II (June 26, 2017), Fall (August 21, 2017)            | Summer | 2017-2018    |
| 651        | 201850        | Spring 2018                                                                           | Spring | 2017-2018    |
| 651        | 201930        | Summer I (May 21, 2018), Summer II (June 30, 2018), Fall (August 27, 2018)            | Summer | 2018-2019    |
| 661        | 2014FA        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 661        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 661        | 2015SP        | Spring 2015                                                                           | Spring | 2014-2015    |
| 661        | 2015SU        | Summer 2015                                                                           | Summer | 2015-2016    |
| 661        | 2015WI        | Winter 2015                                                                           | Winter | 2014-2015    |
| 661        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 661        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 661        | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
| 661        | 2016WI        | Winter 2016                                                                           | Winter | 2015-2016    |
| 661        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 661        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 661        | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
| 661        | 2017WI        | Winter 2017                                                                           | Winter | 2016-2017    |
| 661        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 661        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 661        | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
| 661        | 2018WI        | Winter 2018                                                                           | Winter | 2017-2018    |
| 661        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 661        | 2019WI        | Winter 2019                                                                           | Winter | 2018-2019    |
| 681        | 201307        | Fall 2013 - 201307                                                                    | Fall   | 2013-2014    |
| 681        | 201403        | Spring 2014 - 201403                                                                  | Spring | 2013-2014    |
| 681        | 201405        | Summer 2014 - 201405                                                                  | Summer | 2014-2015    |
| 681        | 201407        | Fall 2014 - 201407                                                                    | Fall   | 2014-2015    |
| 681        | 201503        | Spring 2015 - 201503                                                                  | Spring | 2014-2015    |
| 681        | 201505        | Summer 2015 - 201505                                                                  | Summer | 2015-2016    |
| 681        | 201507        | Fall 2015 - 201507                                                                    | Fall   | 2015-2016    |
| 681        | 201603        | Spring 2016 - 201603                                                                  | Spring | 2015-2016    |
| 681        | 201605        | Summer 2016 - 201605                                                                  | Summer | 2016-2017    |
| 681        | 201607        | Fall 2016 - 201607                                                                    | Fall   | 2016-2017    |
| 681        | 201703        | Spring 2017                                                                           | Spring | 2016-2017    |
| 681        | 201705        | Summer 2017 - 201705                                                                  | Summer | 2017-2018    |
| 681        | 201707        | Fall 2017 - 201707                                                                    | Fall   | 2017-2018    |
| 681        | 201803        | Spring 2018 - 201803                                                                  | Spring | 2017-2018    |
| 681        | 201805        | Summer 2018 - 201805                                                                  | Summer | 2018-2019    |
| 681        | 201807        | Fall 2018 - 201807                                                                    | Fall   | 2018-2019    |
| 681        | 201903        | Spring 2019 -201903                                                                   | Spring | 2018-2019    |
| 682        | 201307        | Fall 2013 - 201307                                                                    | Fall   | 2013-2014    |
| 682        | 201403        | Spring 2014 - 201403                                                                  | Spring | 2013-2014    |
| 682        | 201405        | Summer 2014 - 201405                                                                  | Summer | 2014-2015    |
| 682        | 201407        | Fall 2014 - 201407                                                                    | Fall   | 2014-2015    |
| 682        | 201503        | Spring 2015 - 201503                                                                  | Spring | 2014-2015    |
| 682        | 201505        | Summer 2015 - 201505                                                                  | Summer | 2015-2016    |
| 682        | 201507        | Fall 2015 - 201507                                                                    | Fall   | 2015-2016    |
| 682        | 201603        | Spring 2016 - 201603                                                                  | Spring | 2015-2016    |
| 682        | 201605        | Summer 2016 - 201605                                                                  | Summer | 2016-2017    |
| 682        | 201607        | Fall 2016 - 201607                                                                    | Fall   | 2016-2017    |
| 682        | 201703        | Spring 2017 - 201703                                                                  | Spring | 2016-2017    |
| 682        | 201705        | Summer 2017 - 201705                                                                  | Summer | 2017-2018    |
| 682        | 201707        | Fall 2017 - 201707                                                                    | Fall   | 2017-2018    |
| 682        | 201803        | Spring 2018 - 201803                                                                  | Spring | 2017-2018    |
| 682        | 201805        | Summer 2018 - 201805                                                                  | Summer | 2018-2019    |
| 682        | 201807        | Fall 2018 - 201807                                                                    | Fall   | 2018-2019    |
| 682        | 201903        | Spring 2019 -201903                                                                   | Spring | 2018-2019    |
| 682        | 201905        | Summer 2019 - 201905                                                                  | Summer | 2019-2020    |
| 682        | 201907        | Fall 2019 - 201907                                                                    | Fall   | 2019-2020    |
| 683        | 201307        | Fall 2013 - 201307                                                                    | Fall   | 2013-2014    |
| 683        | 201403        | Spring 2014 - 201403                                                                  | Spring | 2013-2014    |
| 683        | 201405        | Summer 2014 - 201405                                                                  | Summer | 2014-2015    |
| 683        | 201407        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 683        | 201503        | Spring 2015                                                                           | Spring | 2014-2015    |
| 683        | 201505        | Summer 2015                                                                           | Summer | 2015-2016    |
| 683        | 201507        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 683        | 201603        | Spring 2016                                                                           | Spring | 2015-2016    |
| 683        | 201605        | Summer 2016                                                                           | Summer | 2016-2017    |
| 683        | 201607        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 683        | 201703        | Spring 2017                                                                           | Spring | 2016-2017    |
| 683        | 201705        | Summer 2017                                                                           | Summer | 2017-2018    |
| 683        | 201707        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 683        | 201803        | Spring 2018                                                                           | Spring | 2017-2018    |
| 683        | 201805        | Summer 2018                                                                           | Summer | 2018-2019    |
| 683        | 201807        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 683        | 201903        | Spring 2019                                                                           | Spring | 2018-2019    |
| 683        | 201905        | Summer 2019                                                                           | Summer | 2019-2020    |
| 683        | 201907        | Fall 2019                                                                             | Fall   | 2019-2020    |
| 691        | 201530        | Summer 2015                                                                           | Summer | 2015-2016    |
| 691        | 201550        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 691        | 201620        | Spring 2016                                                                           | Spring | 2015-2016    |
| 691        | 201630        | Summer 2016                                                                           | Summer | 2016-2017    |
| 691        | 201650        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 691        | 201720        | Spring 2017                                                                           | Spring | 2016-2017    |
| 691        | 201730        | Summer 2017                                                                           | Summer | 2017-2018    |
| 691        | 201750        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 691        | 201820        | Spring 2018                                                                           | Spring | 2017-2018    |
| 691        | 201830        | Summer 2018                                                                           | Summer | 2018-2019    |
| 691        | 201850        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 691        | 201920        | Spring 2019                                                                           | Spring | 2018-2019    |
| 691        | 201930        | Summer 2019                                                                           | Summer | 2019-2020    |
| 691        | 201950        | Fall 2019                                                                             | Fall   | 2019-2020    |
| 691        | 69111         | Fall 2011 - Taft College                                                              | Fall   | 2011-2012    |
| 691        | 69112         | Spring 2012 - Taft College                                                            | Spring | 2011-2012    |
| 711        | 2013/WI       | Winter Session 2013                                                                   | Winter | 2012-2013    |
| 711        | 2014/FA       | Fall Semester 2014                                                                    | Fall   | 2014-2015    |
| 711        | 2015/FA       | Fall Semester 2015                                                                    | Fall   | 2015-2016    |
| 711        | 2015/SP       | Spring Semester 2015                                                                  | Spring | 2014-2015    |
| 711        | 2015/SU       | Summer Session 2015                                                                   | Summer | 2015-2016    |
| 711        | 2016/FA       | Fall Semester 2016                                                                    | Fall   | 2016-2017    |
| 711        | 2016/SP       | Spring Semester 2016                                                                  | Spring | 2015-2016    |
| 711        | 2016/SU       | Summer Session 2016                                                                   | Summer | 2016-2017    |
| 711        | 2017/FA       | Fall Semester 2017                                                                    | Fall   | 2017-2018    |
| 711        | 2017/SP       | Spring Semester 2017                                                                  | Spring | 2016-2017    |
| 711        | 2017/SU       | Summer Session 2017                                                                   | Summer | 2017-2018    |
| 711        | 2017/WI       | Winter Session 2017                                                                   | Winter | 2016-2017    |
| 711        | 2018/FA       | Fall Semester 2018                                                                    | Fall   | 2018-2019    |
| 711        | 2018/SP       | Spring Semester 2018                                                                  | Spring | 2017-2018    |
| 711        | 2018/SU       | Summer Session 2018                                                                   | Summer | 2018-2019    |
| 711        | 2018/WI       | Winter Session 2018                                                                   | Winter | 2017-2018    |
| 711        | 2019/SP       | Spring Semester 2019                                                                  | Spring | 2018-2019    |
| 711        | 2019/WI       | Winter Session 2019                                                                   | Winter | 2018-2019    |
| 721        | 2012/FA       | Fall Semester 2015                                                                    | Fall   | 2015-2016    |
| 721        | 2013/FA       | Special Programs Fall 2014                                                            | Fall   | 2014-2015    |
| 721        | 2013/WI       | Winter Session 2014                                                                   | Winter | 2013-2014    |
| 721        | 2014/FA       | Fall Semester 2014                                                                    | Fall   | 2014-2015    |
| 721        | 2015/FA       | Special Programs Fall 2015                                                            | Fall   | 2015-2016    |
| 721        | 2015/SP       | Spring Semester 2015                                                                  | Spring | 2014-2015    |
| 721        | 2015/SU       | Summer Session 2015                                                                   | Summer | 2015-2016    |
| 721        | 2016/FA       | Fall Semester 2016                                                                    | Fall   | 2016-2017    |
| 721        | 2016/SP       | Spring Semester 2016                                                                  | Spring | 2015-2016    |
| 721        | 2016/SU       | Summer Session 2016                                                                   | Summer | 2016-2017    |
| 721        | 2017/FA       | Fall Semester 2017                                                                    | Fall   | 2017-2018    |
| 721        | 2017/SP       | Spring Semester 2017                                                                  | Spring | 2016-2017    |
| 721        | 2017/SU       | Summer Session 2017                                                                   | Summer | 2017-2018    |
| 721        | 2017/WI       | Winter Session 2017                                                                   | Winter | 2016-2017    |
| 721        | 2018/FA       | Fall Semester 2018                                                                    | Fall   | 2018-2019    |
| 721        | 2018/SP       | Spring Semester 2018                                                                  | Spring | 2017-2018    |
| 721        | 2018/SU       | Summer Session 2018                                                                   | Summer | 2018-2019    |
| 721        | 2018/WI       | Winter Session 2018                                                                   | Winter | 2017-2018    |
| 721        | 2019/SP       | Spring Semester 2019                                                                  | Spring | 2018-2019    |
| 721        | 2019/WI       | Winter Session 2019                                                                   | Winter | 2018-2019    |
| 731        | 2157          | Fall 2015/ Winter and Spring 2016                                                     | Fall   | 2016-2017    |
| 731        | 2161          | Winter and Spring 2016                                                                | Winter | 2015-2016    |
| 731        | 2163          | Spring 2016                                                                           | Spring | 2015-2016    |
| 731        | 2165          | Summer and Fall 2016                                                                  | Summer | 2016-2017    |
| 731        | 2167          | Fall 2016                                                                             | Fall   | 2016-2017    |
| 731        | 2171          | Winter and Spring 2017                                                                | Winter | 2016-2017    |
| 731        | 2173          | Spring 2017                                                                           | Spring | 2016-2017    |
| 731        | 2175          | Summer and Fall 2017                                                                  | Summer | 2017-2018    |
| 731        | 2177          | Fall 2017                                                                             | Fall   | 2017-2018    |
| 731        | 2181          | Winter and Spring 2018                                                                | Winter | 2017-2018    |
| 731        | 2183          | Spring 2018                                                                           | Spring | 2017-2018    |
| 731        | 2185          | Summer and Fall 2018                                                                  | Summer | 2018-2019    |
| 731        | 2191          | Winter and Spring 2019                                                                | Winter | 2018-2019    |
| 731        | 73111         | Fall 2011 - Glendale Community College                                                | Fall   | 2011-2012    |
| 731        | 73112         | Spring 2012 - Glendale Community College                                              | Spring | 2011-2012    |
| 741        | 2148          | LATE-START CLASSES Fall 2014 Application                                              | Fall   | 2014-2015    |
| 741        | 2152          | Winter 2015 - Late Application, semester began January 5th                            | Winter | 2014-2015    |
| 741        | 2154          | Spring 2015 - semester begins February 9th                                            | Spring | 2014-2015    |
| 741        | 2156          | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
| 741        | 2158          | Fall 2015 Late-Start Class Application                                                | Fall   | 2015-2016    |
| 741        | 2162          | Winter and/or Spring 2016 - This application is valid for Winter and Spring semesters | Winter | 2015-2016    |
| 741        | 2164          | Spring 2016 - begins February 8th                                                     | Spring | 2015-2016    |
| 741        | 2166          | Summer and/or Fall 2016 - This application is valid for Summer and Fall semesters     | Summer | 2016-2017    |
| 741        | 2168          | Fall 2016 - open for Late-Start Classes that begin October 24                         | Fall   | 2016-2017    |
| 741        | 2172          | Winter and/or Spring 2017 - valid for both semesters                                  | Winter | 2016-2017    |
| 741        | 2174          | Spring 2017 Late Start Classes Begin April 10                                         | Spring | 2016-2017    |
| 741        | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
| 741        | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
| 741        | 2182          | Winter 2018 applications                                                              | Winter | 2017-2018    |
| 741        | 2184          | Spring 2018 applications                                                              | Spring | 2017-2018    |
| 741        | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
| 741        | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
| 741        | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
| 741        | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
| 742        | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 742        | 2152          | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
| 742        | 2154          | Spring 2015                                                                           | Spring | 2014-2015    |
| 742        | 2156          | Summer 2015                                                                           | Summer | 2015-2016    |
| 742        | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
| 742        | 2162          | Winter/Spring 2016                                                                    | Winter | 2015-2016    |
| 742        | 2164          | Spring 2016                                                                           | Spring | 2015-2016    |
| 742        | 2166          | Summer 2016                                                                           | Summer | 2016-2017    |
| 742        | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
| 742        | 2172          | Winter 2017                                                                           | Winter | 2016-2017    |
| 742        | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
| 742        | 2176          | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
| 742        | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
| 742        | 2182          | Winter 2018                                                                           | Winter | 2017-2018    |
| 742        | 2184          | Spring 2018                                                                           | Spring | 2017-2018    |
| 742        | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
| 742        | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
| 742        | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
| 742        | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
| 743        | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 743        | 2152          | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
| 743        | 2154          | Spring 2015                                                                           | Spring | 2014-2015    |
| 743        | 2156          | Summer 2015                                                                           | Summer | 2015-2016    |
| 743        | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
| 743        | 2162          | Winter 2016                                                                           | Winter | 2015-2016    |
| 743        | 2164          | Spring 2016                                                                           | Spring | 2015-2016    |
| 743        | 2166          | Summer 2016                                                                           | Summer | 2016-2017    |
| 743        | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
| 743        | 2172          | Winter 2017                                                                           | Winter | 2016-2017    |
| 743        | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
| 743        | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
| 743        | 2178          | FALL 2017                                                                             | Fall   | 2017-2018    |
| 743        | 2182          | Winter 2018                                                                           | Winter | 2017-2018    |
| 743        | 2184          | Spring 2018                                                                           | Spring | 2017-2018    |
| 743        | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
| 743        | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
| 743        | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
| 743        | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
| 743        | 22018         | Winter 2018                                                                           | Winter | 2017-2018    |
| 743        | 42018         | Spring 2018                                                                           | Spring | 2017-2018    |
| 744        | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 744        | 2152          | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
| 744        | 2154          | Spring 2015                                                                           | Spring | 2014-2015    |
| 744        | 2156          | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
| 744        | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
| 744        | 2162          | Winter 2016                                                                           | Winter | 2015-2016    |
| 744        | 2164          | Spring 2016                                                                           | Spring | 2015-2016    |
| 744        | 2166          | Summer 2016                                                                           | Summer | 2016-2017    |
| 744        | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
| 744        | 2172          | Winter 2017                                                                           | Winter | 2016-2017    |
| 744        | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
| 744        | 2176          | ENCORE students ONLY - Summer 2017                                                    | Summer | 2017-2018    |
| 744        | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
| 744        | 2182          | Winter 2018                                                                           | Winter | 2017-2018    |
| 744        | 2184          | Spring 2018                                                                           | Spring | 2017-2018    |
| 744        | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
| 744        | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
| 744        | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
| 744        | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
| 745        | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 745        | 2152          | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
| 745        | 2154          | Spring 2015                                                                           | Spring | 2014-2015    |
| 745        | 2156          | Summer 2015                                                                           | Summer | 2015-2016    |
| 745        | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
| 745        | 2162          | Winter/Spring 2016                                                                    | Winter | 2015-2016    |
| 745        | 2166          | Summer 2016                                                                           | Summer | 2016-2017    |
| 745        | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
| 745        | 2172          | Winter 2017                                                                           | Winter | 2016-2017    |
| 745        | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
| 745        | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
| 745        | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
| 745        | 2182          | Winter 2018                                                                           | Winter | 2017-2018    |
| 745        | 2184          | Spring 2018                                                                           | Spring | 2017-2018    |
| 745        | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
| 745        | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
| 745        | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
| 745        | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
| 746        | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 746        | 2152          | Winter/Spring 2015                                                                    | Winter | 2014-2015    |
| 746        | 2154          | Spring 2015                                                                           | Spring | 2014-2015    |
| 746        | 2156          | Summer 2015                                                                           | Summer | 2015-2016    |
| 746        | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
| 746        | 2162          | Winter 2016                                                                           | Winter | 2015-2016    |
| 746        | 2164          | Spring 2016                                                                           | Spring | 2015-2016    |
| 746        | 2166          | SUMMER 2016                                                                           | Summer | 2016-2017    |
| 746        | 2168          | FALL 2016                                                                             | Fall   | 2016-2017    |
| 746        | 2172          | WINTER 2017                                                                           | Winter | 2016-2017    |
| 746        | 2174          | SPRING 2017                                                                           | Spring | 2016-2017    |
| 746        | 2176          | SUMMER2017                                                                            | Summer | 2017-2018    |
| 746        | 2178          | FALL 2017                                                                             | Fall   | 2017-2018    |
| 746        | 2182          | WINTER 2018                                                                           | Winter | 2017-2018    |
| 746        | 2184          | SPRING 2018                                                                           | Spring | 2017-2018    |
| 746        | 2186          | SUMMER 2018                                                                           | Summer | 2018-2019    |
| 746        | 2188          | FALL 2018                                                                             | Fall   | 2018-2019    |
| 746        | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
| 746        | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
| 747        | 2154          | Spring 2015                                                                           | Spring | 2014-2015    |
| 747        | 2156          | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
| 747        | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
| 747        | 2162          | winter/spring 2016                                                                    | Winter | 2015-2016    |
| 747        | 2164          | Spring 2016                                                                           | Spring | 2015-2016    |
| 747        | 2166          | summer/fall 2016                                                                      | Summer | 2016-2017    |
| 747        | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
| 747        | 2172          | Winter 2017                                                                           | Winter | 2016-2017    |
| 747        | 2174          | Spring 2017 Late Start Classes                                                        | Spring | 2016-2017    |
| 747        | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
| 747        | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
| 747        | 2182          | Winter 2018                                                                           | Winter | 2017-2018    |
| 747        | 2184          | Spring 2018                                                                           | Spring | 2017-2018    |
| 747        | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
| 747        | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
| 747        | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
| 747        | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
| 748        | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 748        | 2154          | Spring                                                                                | Spring | 2014-2015    |
| 748        | 2156          | Summer 2015/Fall 2015                                                                 | Summer | 2015-2016    |
| 748        | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
| 748        | 2162          | Winter 2016                                                                           | Winter | 2015-2016    |
| 748        | 2164          | Spring 2016                                                                           | Spring | 2015-2016    |
| 748        | 2166          | Summer 2016                                                                           | Summer | 2016-2017    |
| 748        | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
| 748        | 2172          | Winter 2017                                                                           | Winter | 2016-2017    |
| 748        | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
| 748        | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
| 748        | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
| 748        | 2182          | Winter 2018                                                                           | Winter | 2017-2018    |
| 748        | 2184          | Spring 2018                                                                           | Spring | 2017-2018    |
| 748        | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
| 748        | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
| 749        | 2018          | Winter 2018 Do Not Use                                                                | Winter | 2017-2018    |
| 749        | 2148          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 749        | 2154          | Spring 2015                                                                           | Spring | 2014-2015    |
| 749        | 2156          | Summer/Fall 2015                                                                      | Summer | 2015-2016    |
| 749        | 2158          | Fall 2015                                                                             | Fall   | 2015-2016    |
| 749        | 2162          | Winter/Spring 2016                                                                    | Winter | 2015-2016    |
| 749        | 2164          | Spring 2016                                                                           | Spring | 2015-2016    |
| 749        | 2166          | Summer 2016                                                                           | Summer | 2016-2017    |
| 749        | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
| 749        | 2172          | Winter 2017                                                                           | Winter | 2016-2017    |
| 749        | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
| 749        | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
| 749        | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
| 749        | 2182          | Winter 2018                                                                           | Winter | 2017-2018    |
| 749        | 2184          | Spring 2018                                                                           | Spring | 2017-2018    |
| 749        | 2186          | Summer 2018                                                                           | Summer | 2018-2019    |
| 749        | 2188          | Fall 2018                                                                             | Fall   | 2018-2019    |
| 749        | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
| 749        | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
| 74A        | 2148          | Fall 2014 - Session B                                                                 | Fall   | 2014-2015    |
| 74A        | 2154          | Spring 2015 - Session A and B                                                         | Spring | 2014-2015    |
| 74A        | 2156          | Summer 2015                                                                           | Summer | 2015-2016    |
| 74A        | 2158          | Fall 2015 - Session B                                                                 | Fall   | 2015-2016    |
| 74A        | 2164          | Spring 2016 - Session A                                                               | Spring | 2015-2016    |
| 74A        | 2166          | SUMMER 2016                                                                           | Summer | 2016-2017    |
| 74A        | 2168          | Fall 2016                                                                             | Fall   | 2016-2017    |
| 74A        | 2174          | Spring 2017                                                                           | Spring | 2016-2017    |
| 74A        | 2176          | Summer 2017                                                                           | Summer | 2017-2018    |
| 74A        | 2178          | Fall 2017                                                                             | Fall   | 2017-2018    |
| 74A        | 2192          | Winter 2019                                                                           | Winter | 2018-2019    |
| 74A        | 2194          | Spring 2019                                                                           | Spring | 2018-2019    |
| 771        | 201530        | Spring 2015                                                                           | Spring | 2014-2015    |
| 771        | 201550        | Summer 2015                                                                           | Summer | 2015-2016    |
| 771        | 201570        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 771        | 201630        | Spring 2016                                                                           | Spring | 2015-2016    |
| 771        | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
| 771        | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 771        | 201710        | Winter 2017 Intersession                                                              | Winter | 2016-2017    |
| 771        | 201720        | Winter / Spring 2017                                                                  | Winter | 2016-2017    |
| 771        | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
| 771        | 201740        | Summer / Fall 2017                                                                    | Summer | 2017-2018    |
| 771        | 201760        | Summer / Fall 2017                                                                    | Summer | 2017-2018    |
| 771        | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 771        | 201820        | Winter/Spring 2018                                                                    | Winter | 2017-2018    |
| 771        | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
| 771        | 201860        | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
| 771        | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 771        | 201920        | Winter/Spring 2019                                                                    | Winter | 2018-2019    |
| 771        | 201930        | Spring 2019                                                                           | Spring | 2018-2019    |
| 781        | 20171         | Spring 2017                                                                           | Spring | 2016-2017    |
| 781        | 20172         | Summer 2017                                                                           | Summer | 2017-2018    |
| 781        | 20173         | Fall 2017                                                                             | Fall   | 2017-2018    |
| 781        | 20180         | Winter 2018                                                                           | Winter | 2017-2018    |
| 781        | 20181         | Spring 2018                                                                           | Spring | 2017-2018    |
| 781        | 20182         | Summer 2018                                                                           | Summer | 2018-2019    |
| 781        | 20183         | Fall 2018                                                                             | Fall   | 2018-2019    |
| 781        | 20190         | Winter 2019                                                                           | Winter | 2018-2019    |
| 781        | 20191         | Spring 2019                                                                           | Spring | 2018-2019    |
| 781        | 20192         | Summer 2019                                                                           | Summer | 2019-2020    |
| 781        | 20193         | Fall 2019                                                                             | Fall   | 2019-2020    |
| 781        | 20200         | Winter 2020                                                                           | Winter | 2019-2020    |
| 781        | 20201         | Spring 2020                                                                           | Spring | 2019-2020    |
| 781        | 78111         | Fall 2011 - Santa Monica College                                                      | Fall   | 2011-2012    |
| 781        | 78112         | Spring 2012 - Santa Monica College                                                    | Spring | 2011-2012    |
| 811        | 1143          | 2014 Spring                                                                           | Spring | 2013-2014    |
| 811        | 1146          | 2014 Summer                                                                           | Summer | 2014-2015    |
| 811        | 1149          | 2014 Fall                                                                             | Fall   | 2014-2015    |
| 811        | 1153          | 2015 Spring                                                                           | Spring | 2014-2015    |
| 811        | 1156          | 2015 Summer                                                                           | Summer | 2015-2016    |
| 811        | 1159          | 2015 Fall                                                                             | Fall   | 2015-2016    |
| 811        | 1163          | Spring 2016                                                                           | Spring | 2015-2016    |
| 811        | 1166          | Summer 2016                                                                           | Summer | 2016-2017    |
| 811        | 1169          | Fall 2016                                                                             | Fall   | 2016-2017    |
| 811        | 1173          | Spring 2017                                                                           | Spring | 2016-2017    |
| 811        | 1176          | Summer 2017                                                                           | Summer | 2017-2018    |
| 811        | 1179          | Fall 2017                                                                             | Fall   | 2017-2018    |
| 811        | 1183          | Spring 2018                                                                           | Spring | 2017-2018    |
| 811        | 1186          | Summer 2018                                                                           | Summer | 2018-2019    |
| 811        | 1189          | Fall 2018                                                                             | Fall   | 2018-2019    |
| 811        | 1193          | Spring 2019                                                                           | Spring | 2018-2019    |
| 811        | 1196          | Summer 2019                                                                           | Summer | 2019-2020    |
| 811        | 1199          | Fall 2019                                                                             | Fall   | 2019-2020    |
| 821        | 201620        | Fall 2015 Application                                                                 | Fall   | 2015-2016    |
| 821        | 201625        | Winter 2016 Application                                                               | Winter | 2015-2016    |
| 821        | 201630        | Spring 2016 Application                                                               | Spring | 2015-2016    |
| 821        | 201640        | Summer 2016 Application                                                               | Summer | 2016-2017    |
| 821        | 201720        | Fall 2016 Application                                                                 | Fall   | 2016-2017    |
| 821        | 201725        | Winter 2017 Application                                                               | Winter | 2016-2017    |
| 821        | 201730        | Spring 2017 Application                                                               | Spring | 2016-2017    |
| 821        | 201740        | Summer 2017 Application                                                               | Summer | 2017-2018    |
| 821        | 201820        | Fall 2017 Application                                                                 | Fall   | 2017-2018    |
| 821        | 201825        | Winter 2018                                                                           | Winter | 2017-2018    |
| 821        | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
| 821        | 201840        | Summer 2018                                                                           | Summer | 2018-2019    |
| 821        | 201920        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 821        | 201925        | Winter 2019                                                                           | Winter | 2018-2019    |
| 821        | 201930        | Spring 2019                                                                           | Spring | 2018-2019    |
| 831        | 201413        | CCC Summer 2014                                                                       | Summer | 2014-2015    |
| 831        | 201418        | Military/Contract EdCCC Su'14                                                         | NULL   | NULL         |
| 831        | 201423        | CCC Fall 2014                                                                         | Fall   | 2014-2015    |
| 831        | 201433        | CCC Spring 2015                                                                       | Spring | 2014-2015    |
| 831        | 201513        | CCC Summer/Fall 2015                                                                  | Summer | 2015-2016    |
| 831        | 201523        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 831        | 201533        | CCC Winter Intersession/Spring 2016                                                   | Winter | 2015-2016    |
| 831        | 201613        | CCC Summer/Fall 2016                                                                  | Summer | 2016-2017    |
| 831        | 201623        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 831        | 201633        | CCC Winter Intersession/Spring 2017                                                   | Winter | 2016-2017    |
| 831        | 201713        | CCC Summer / Fall 2017                                                                | Summer | 2017-2018    |
| 831        | 201723        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 831        | 201733        | CCC Winter Intersession/Spring 2018                                                   | Winter | 2017-2018    |
| 831        | 201813        | CCC Summer/Fall 2018                                                                  | Summer | 2018-2019    |
| 831        | 201823        | Fall Term 2018                                                                        | Fall   | 2018-2019    |
| 831        | 201833        | CCC Intersession/Spring 2019                                                          | Winter | 2018-2019    |
| 831        | 201911        | Incorrect OCC Term                                                                    | NULL   | NULL         |
| 831        | 201913        | CCC Summer/Fall 2019                                                                  | Summer | 2019-2020    |
| 832        | 201412        | GWC Summer 2014                                                                       | Summer | 2014-2015    |
| 832        | 201422        | GWC Fall 2014                                                                         | Fall   | 2014-2015    |
| 832        | 201432        | GWC Winter Intersession / Spring 2015                                                 | Winter | 2014-2015    |
| 832        | 201512        | GWC Summer / Fall 2015                                                                | Summer | 2015-2016    |
| 832        | 201522        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 832        | 201532        | GWC Winter Intersession / Spring 2016                                                 | Winter | 2015-2016    |
| 832        | 201612        | GWC Summer / Fall 2016                                                                | Summer | 2016-2017    |
| 832        | 201622        | Fall 2016 Semester                                                                    | Fall   | 2016-2017    |
| 832        | 201632        | GWC Winter Intersession / Spring 2017                                                 | Winter | 2016-2017    |
| 832        | 201712        | GWC Summer / Fall 2017                                                                | Summer | 2017-2018    |
| 832        | 201722        | GWC Fall 2017                                                                         | Fall   | 2017-2018    |
| 832        | 201732        | GWC Winter Intersession / Spring 2018                                                 | Winter | 2017-2018    |
| 832        | 201812        | GWC Summer / Fall 2018                                                                | Summer | 2018-2019    |
| 832        | 201822        | GWC Fall 2018                                                                         | Fall   | 2018-2019    |
| 832        | 201832        | GWC Winter Intersession / Spring 2019                                                 | Winter | 2018-2019    |
| 832        | 201912        | GWC Summer / Fall 2019                                                                | Summer | 2019-2020    |
| 833        | 201411        | OCC Summer 2014                                                                       | Summer | 2014-2015    |
| 833        | 201421        | OCC Fall 2014                                                                         | Fall   | 2014-2015    |
| 833        | 201431        | OCC Intersession/Spring 2015                                                          | Winter | 2014-2015    |
| 833        | 201511        | OCC Summer/Fall 2015                                                                  | Summer | 2015-2016    |
| 833        | 201521        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 833        | 201531        | Intersession/Spring 2016                                                              | Winter | 2015-2016    |
| 833        | 201611        | Summer/Fall 2016                                                                      | Summer | 2016-2017    |
| 833        | 201621        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 833        | 201631        | Intersession/Spring 2017                                                              | Winter | 2016-2017    |
| 833        | 201711        | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
| 833        | 201721        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 833        | 201731        | Intersession/Spring 2018                                                              | Winter | 2017-2018    |
| 833        | 201811        | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
| 833        | 201821        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 833        | 201831        | OCC Intersession/Spring 2019                                                          | Winter | 2018-2019    |
| 833        | 201911        | OCC Summer/Fall 2019                                                                  | Summer | 2019-2020    |
| 841        | 1565          | 2016 Fall                                                                             | Fall   | 2016-2017    |
| 841        | 1570          | 2017 Winter                                                                           | Winter | 2016-2017    |
| 841        | 1575          | 2017 Spring                                                                           | Spring | 2016-2017    |
| 841        | 1580          | 2017 Summer                                                                           | Summer | 2017-2018    |
| 841        | 1585          | 2017 Fall                                                                             | Fall   | 2017-2018    |
| 841        | 1590          | 2018 Winter                                                                           | Winter | 2017-2018    |
| 841        | 1595          | 2018 Spring                                                                           | Spring | 2017-2018    |
| 841        | 1600          | 2018 Summer                                                                           | Summer | 2018-2019    |
| 841        | 1605          | 2018 Fall                                                                             | Fall   | 2018-2019    |
| 841        | 1610          | 2019 Winter                                                                           | Winter | 2018-2019    |
| 841        | 1615          | 2019 Spring                                                                           | Spring | 2018-2019    |
| 841        | 1620          | 2019 Summer/Fall                                                                      | Summer | 2019-2020    |
| 841        | 1625          | 2019 Fall                                                                             | Fall   | 2019-2020    |
| 841        | 1630          | 2020 Winter                                                                           | Winter | 2019-2020    |
| 841        | 1635          | 2020 Spring                                                                           | Spring | 2019-2020    |
| 841        | 1640          | 2020 Summer                                                                           | Summer | 2020-2021    |
| 841        | 1645          | 2020 Fall                                                                             | Fall   | 2020-2021    |
| 841        | 1650          | 2021 Winter                                                                           | Winter | 2020-2021    |
| 841        | 1655          | 2021 Spring                                                                           | Spring | 2020-2021    |
| 841        | 1660          | 2021 Summer                                                                           | Summer | 2021-2022    |
| 841        | 1665          | 2021 Fall                                                                             | Fall   | 2021-2022    |
| 841        | 1670          | 2022 Winter                                                                           | Winter | 2021-2022    |
| 841        | 1675          | 2022 Spring                                                                           | Spring | 2021-2022    |
| 841        | 1680          | 2022 Summer                                                                           | Summer | 2022-2023    |
| 841        | 1685          | 2022 Fall                                                                             | Fall   | 2022-2023    |
| 841        | 1690          | 2023 Winter                                                                           | Winter | 2022-2023    |
| 841        | 1695          | 2023 Spring                                                                           | Spring | 2022-2023    |
| 841        | 1700          | 2023 Summer                                                                           | Summer | 2023-2024    |
| 841        | 1705          | 2023 Fall                                                                             | Fall   | 2023-2024    |
| 841        | 1710          | 2024 Winter                                                                           | Winter | 2023-2024    |
| 841        | 1715          | 2024 Spring                                                                           | Spring | 2023-2024    |
| 841        | 1720          | 2024 Summer                                                                           | Summer | 2024-2025    |
| 841        | 1725          | 2024 Fall                                                                             | Fall   | 2024-2025    |
| 841        | 1730          | 2025 Winter                                                                           | Winter | 2024-2025    |
| 841        | 1735          | 2025 Spring                                                                           | Spring | 2024-2025    |
| 841        | 1740          | 2025 Summer                                                                           | Summer | 2025-2026    |
| 841        | 1745          | 2025 Fall                                                                             | Fall   | 2025-2026    |
| 841        | 1750          | 2026 Winter                                                                           | Winter | 2025-2026    |
| 841        | 1755          | 2026 Spring                                                                           | Spring | 2025-2026    |
| 841        | 1760          | 2026 Summer                                                                           | Summer | 2026-2027    |
| 841        | 1765          | 2026 Fall                                                                             | Fall   | 2026-2027    |
| 841        | 1770          | 2027 Winter                                                                           | Winter | 2026-2027    |
| 841        | 1775          | 2027 Spring                                                                           | Spring | 2026-2027    |
| 841        | 1780          | 2027 Summer                                                                           | Summer | 2027-2028    |
| 851        | 201340        | Spring 2014                                                                           | Spring | 2013-2014    |
| 851        | 201410        | Summer 2014                                                                           | Summer | 2014-2015    |
| 851        | 201420        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 851        | 201430        | Winter 2015                                                                           | Winter | 2014-2015    |
| 851        | 201440        | Spring 2015                                                                           | Spring | 2014-2015    |
| 851        | 201510        | Summer 2015                                                                           | Summer | 2015-2016    |
| 851        | 201520        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 851        | 201530        | Winter 2016                                                                           | Winter | 2015-2016    |
| 851        | 201540        | Spring 2016                                                                           | Spring | 2015-2016    |
| 851        | 201610        | Summer 2016                                                                           | Summer | 2016-2017    |
| 851        | 201620        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 851        | 201630        | Winter 2017                                                                           | Winter | 2016-2017    |
| 851        | 201640        | Spring 2017                                                                           | Spring | 2016-2017    |
| 851        | 201710        | Summer 2017                                                                           | Summer | 2017-2018    |
| 851        | 201720        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 851        | 201730        | Winter 2018                                                                           | Winter | 2017-2018    |
| 851        | 201740        | Spring 2018                                                                           | Spring | 2017-2018    |
| 851        | 201810        | Summer 2018                                                                           | Summer | 2018-2019    |
| 851        | 201820        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 851        | 201830        | Winter 2019                                                                           | Winter | 2018-2019    |
| 851        | 201840        | Spring 2019                                                                           | Spring | 2018-2019    |
| 851        | 201910        | Summer 2019                                                                           | Summer | 2019-2020    |
| 851        | 201920        | Fall 2019                                                                             | Fall   | 2019-2020    |
| 861        | 201520        | Spring 2016                                                                           | Spring | 2015-2016    |
| 861        | 201530        | summer 2016                                                                           | Summer | 2016-2017    |
| 861        | 201610        | fall 2016                                                                             | Fall   | 2016-2017    |
| 861        | 201620        | spring 2017                                                                           | Spring | 2016-2017    |
| 861        | 201630        | Summer 2017                                                                           | Summer | 2017-2018    |
| 861        | 201710        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 861        | 201720        | Spring 2018                                                                           | Spring | 2017-2018    |
| 861        | 201730        | Summer 2018                                                                           | Summer | 2018-2019    |
| 861        | 201810        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 861        | 201820        | Spring 2019                                                                           | Spring | 2018-2019    |
| 862        | 201320        | Spring 2014                                                                           | Spring | 2013-2014    |
| 862        | 201330        | Summer 2014                                                                           | Summer | 2014-2015    |
| 862        | 201410        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 862        | 201420        | Spring 2015                                                                           | Spring | 2014-2015    |
| 862        | 201430        | Summer 2015                                                                           | Summer | 2015-2016    |
| 862        | 201510        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 862        | 201520        | Spring 2016                                                                           | Spring | 2015-2016    |
| 862        | 201530        | Summer 2016                                                                           | Summer | 2016-2017    |
| 862        | 201610        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 862        | 201620        | Spring 2017                                                                           | Spring | 2016-2017    |
| 862        | 201630        | Summer 2017                                                                           | Summer | 2017-2018    |
| 862        | 201710        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 862        | 201720        | Spring 2018                                                                           | Spring | 2017-2018    |
| 862        | 201730        | Summer 2018                                                                           | Summer | 2018-2019    |
| 862        | 201810        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 862        | 201820        | Spring 2019                                                                           | Spring | 2018-2019    |
| 862        | 201830        | Summer 2019                                                                           | Summer | 2019-2020    |
| 862        | 201910        | Fall 2019                                                                             | Fall   | 2019-2020    |
| 871        | 2015FA        | Fall 2015 - Santa Ana College                                                         | Fall   | 2015-2016    |
| 871        | 2016FA        | Fall 2016 - Santa Ana College                                                         | Fall   | 2016-2017    |
| 871        | 2016SI        | Spring Intersession 2016 - Santa Ana College                                          | Winter | 2015-2016    |
| 871        | 2016SP        | Spring 2016 - Santa Ana College                                                       | Spring | 2015-2016    |
| 871        | 2016SU        | Summer 2016 - Santa Ana College                                                       | Summer | 2016-2017    |
| 871        | 2017FA        | Fall 2017 - Santa Ana College                                                         | Fall   | 2017-2018    |
| 871        | 2017SI        | Spring Intersession 2017 - Santa Ana College                                          | Winter | 2016-2017    |
| 871        | 2017SP        | Spring 2017 - Santa Ana College                                                       | Spring | 2016-2017    |
| 871        | 2017SU        | Summer 2017 - Santa Ana College                                                       | Summer | 2017-2018    |
| 871        | 2018FA        | Fall 2018 - Santa Ana College                                                         | Fall   | 2018-2019    |
| 871        | 2018SI        | Spring Intersession 2018 - Santa Ana College                                          | Winter | 2017-2018    |
| 871        | 2018SP        | Spring 2018 - Santa Ana College                                                       | Spring | 2017-2018    |
| 871        | 2018SU        | Summer 2018 - Santa Ana College                                                       | Summer | 2018-2019    |
| 871        | 2019FA        | Fall 2019 - Santa Ana College                                                         | Fall   | 2019-2020    |
| 873        | 2014FA        | Fall 2014 - 8/25/2014 - 12/14/2014                                                    | Fall   | 2014-2015    |
| 873        | 2015FA        | Fall 2015 - 8/24/2015 to 12/13/2015                                                   | Fall   | 2015-2016    |
| 873        | 2015SI        | Intersession 2015 - 01/05/15  - 02/01/15                                              | Winter | 2014-2015    |
| 873        | 2015SP        | Spring 2015 - 02/09/15 - 06/07/15                                                     | Spring | 2014-2015    |
| 873        | 2015SU        | Summer 2015 - 06/15/15 to 08/16/15                                                    | Summer | 2015-2016    |
| 873        | 2016FA        | Fall 2016 - 8/22/16 - 12/11/16                                                        | Fall   | 2016-2017    |
| 873        | 2016SI        | Intersession 2016 - 01/04/16 - 01/31/16                                               | Winter | 2015-2016    |
| 873        | 2016SP        | Spring 2016 - 02/08/16 to 06/05/16                                                    | Spring | 2015-2016    |
| 873        | 2016SU        | Summer 2016 - 06/13/16 to 08/14/16                                                    | Summer | 2016-2017    |
| 873        | 2017FA        | Fall 2017:  8/28/2017 - 12/17/2017                                                    | Fall   | 2017-2018    |
| 873        | 2017SI        | Intersession 2017:  01/09/17 - 02/05/17                                               | Winter | 2016-2017    |
| 873        | 2017SP        | Spring 2017:  02/13/17 - 06/11/17                                                     | Spring | 2016-2017    |
| 873        | 2017SU        | Summer 2017:  06/19/17 - 08/13/17                                                     | Summer | 2017-2018    |
| 873        | 2018FA        | Fall 2018:  8/20/18 - 12/9/18                                                         | Fall   | 2018-2019    |
| 873        | 2018SI        | Intersession 2018 - 01/02/2018 - 01/28/2018                                           | Winter | 2017-2018    |
| 873        | 2018SP        | Spring 2018 - 02/05/2018 - 06/03/2018                                                 | Spring | 2017-2018    |
| 873        | 2018SU        | Summer 2018: 06/18/18 - 08/12/18                                                      | Summer | 2018-2019    |
| 873        | 2019FA        | Fall 2019 Early Welcome (Graduating High School Seniors Only)                         | Fall   | 2019-2020    |
| 881        | 201350        | Summer 2013                                                                           | Summer | 2013-2014    |
| 881        | 201370        | Fall 2013                                                                             | Fall   | 2013-2014    |
| 881        | 201430        | Spring 2014                                                                           | Spring | 2013-2014    |
| 881        | 201450        | Summer 2014                                                                           | Summer | 2014-2015    |
| 881        | 201470        | Fall 2014                                                                             | Fall   | 2014-2015    |
| 881        | 201530        | Spring 2015                                                                           | Spring | 2014-2015    |
| 881        | 201550        | Summer 2015                                                                           | Summer | 2015-2016    |
| 881        | 201570        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 881        | 201630        | Spring 2016                                                                           | Spring | 2015-2016    |
| 881        | 201650        | Summer 2016                                                                           | Summer | 2016-2017    |
| 881        | 201670        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 881        | 201730        | Spring 2017                                                                           | Spring | 2016-2017    |
| 881        | 201750        | Summer 2017                                                                           | Summer | 2017-2018    |
| 881        | 201770        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 881        | 201830        | Spring 2018                                                                           | Spring | 2017-2018    |
| 881        | 201850        | Summer 2018                                                                           | Summer | 2018-2019    |
| 881        | 201870        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 881        | 201930        | Spring 2019                                                                           | Spring | 2018-2019    |
| 881        | 201950        | Summer 2019                                                                           | Summer | 2019-2020    |
| 881        | 201970        | Fall 2019                                                                             | Fall   | 2019-2020    |
| 891        | 20141         | Spring 2014                                                                           | Spring | 2013-2014    |
| 891        | 20142         | Summer 2014                                                                           | Summer | 2014-2015    |
| 891        | 20143         | Fall 2014                                                                             | Fall   | 2014-2015    |
| 891        | 20151         | Spring 2015                                                                           | Spring | 2014-2015    |
| 891        | 20152         | Summer 2015                                                                           | Summer | 2015-2016    |
| 891        | 20153         | Fall 2015                                                                             | Fall   | 2015-2016    |
| 891        | 20161         | Spring 2016                                                                           | Spring | 2015-2016    |
| 891        | 20162         | Summer 2016                                                                           | Summer | 2016-2017    |
| 891        | 20163         | Fall 2016                                                                             | Fall   | 2016-2017    |
| 891        | 20171         | Spring 2017                                                                           | Spring | 2016-2017    |
| 891        | 20172         | Summer 2017                                                                           | Summer | 2017-2018    |
| 891        | 20173         | Fall 2017                                                                             | Fall   | 2017-2018    |
| 891        | 20181         | Spring 2018                                                                           | Spring | 2017-2018    |
| 891        | 20182         | Summer 2018                                                                           | Summer | 2018-2019    |
| 891        | 20183         | Fall 2018                                                                             | Fall   | 2018-2019    |
| 891        | 20191         | Spring 2019                                                                           | Spring | 2018-2019    |
| 891        | 20192         | Summer 2019                                                                           | Summer | 2019-2020    |
| 891        | 20193         | Fall 2019 Freshman Advantage                                                          | Fall   | 2019-2020    |
| 891        | Summer 2018   | Summer 2018 Incorrect - closed                                                        | Summer | 2018-2019    |
| 892        | 20141         | Spring 2014                                                                           | Spring | 2013-2014    |
| 892        | 20142         | Summer 2014                                                                           | Summer | 2014-2015    |
| 892        | 20143         | Fall 2014                                                                             | Fall   | 2014-2015    |
| 892        | 20151         | Spring 2015                                                                           | Spring | 2014-2015    |
| 892        | 20152         | Summer 2015                                                                           | Summer | 2015-2016    |
| 892        | 20153         | Fall 2015                                                                             | Fall   | 2015-2016    |
| 892        | 20161         | Spring 2016                                                                           | Spring | 2015-2016    |
| 892        | 20162         | Summer 2016                                                                           | Summer | 2016-2017    |
| 892        | 20163         | Fall 2016                                                                             | Fall   | 2016-2017    |
| 892        | 20171         | Spring 2017                                                                           | Spring | 2016-2017    |
| 892        | 20172         | Summer 2017                                                                           | Summer | 2017-2018    |
| 892        | 20173         | Fall 2017                                                                             | Fall   | 2017-2018    |
| 892        | 20181         | Spring 2018                                                                           | Spring | 2017-2018    |
| 892        | 20182         | Summer 2018                                                                           | Summer | 2018-2019    |
| 892        | 20183         | Fall 2018                                                                             | Fall   | 2018-2019    |
| 892        | 20191         | Spring 2019                                                                           | Spring | 2018-2019    |
| 892        | 20192         | Summer 2019                                                                           | Summer | 2019-2020    |
| 892        | 20193         | Fall 2019                                                                             | Fall   | 2019-2020    |
| 892        | Fall 2017     | Fall 2017                                                                             | Fall   | 2017-2018    |
| 911        | 201507        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 911        | 201603        | Spring 2016                                                                           | Spring | 2015-2016    |
| 911        | 201605        | Summer 2016                                                                           | Summer | 2016-2017    |
| 911        | 201607        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 911        | 201700        | Summer/Fall 2017                                                                      | Summer | 2017-2018    |
| 911        | 201703        | Spring 2017                                                                           | Spring | 2016-2017    |
| 911        | 201705        | Summer 2017 / Fall 2017                                                               | Summer | 2017-2018    |
| 911        | 201707        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 911        | 201800        | Summer/Fall 2018                                                                      | Summer | 2018-2019    |
| 911        | 201803        | Spring 2018                                                                           | Spring | 2017-2018    |
| 911        | 201807        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 911        | 201903        | Spring 2019                                                                           | Spring | 2018-2019    |
| 921        | 2016/FA       | Fall 2016                                                                             | Fall   | 2016-2017    |
| 921        | 2017/FA       | Fall 2017                                                                             | Fall   | 2017-2018    |
| 921        | 2017/SP       | Spring 2017                                                                           | Spring | 2016-2017    |
| 921        | 2017/SU       | Summer 2017                                                                           | Summer | 2017-2018    |
| 921        | 2018/FA       | Fall 2018                                                                             | Fall   | 2018-2019    |
| 921        | 2018/SP       | Spring 2018                                                                           | Spring | 2017-2018    |
| 921        | 2018/SU       | Summer 2018                                                                           | Summer | 2018-2019    |
| 921        | 2019/SP       | Spring 2019                                                                           | Spring | 2018-2019    |
| 931        | 14/FA         | Fall 2014                                                                             | Fall   | 2014-2015    |
| 931        | 14/SP         | Spring 2014                                                                           | Spring | 2013-2014    |
| 931        | 14/SU         | Summer 2014                                                                           | Summer | 2014-2015    |
| 931        | 15/FA         | Fall 2015                                                                             | Fall   | 2015-2016    |
| 931        | 15/SP         | Spring 2015                                                                           | Spring | 2014-2015    |
| 931        | 15/WI         | Winter 2015                                                                           | Winter | 2014-2015    |
| 931        | 16/FA         | Fall 2016                                                                             | Fall   | 2016-2017    |
| 931        | 16/SP         | Spring 2016                                                                           | Spring | 2015-2016    |
| 931        | 16/SU         | Summer 2016                                                                           | Summer | 2016-2017    |
| 931        | 16/WI         | Winter 2016                                                                           | Winter | 2015-2016    |
| 931        | 17/FA         | Fall 2017                                                                             | Fall   | 2017-2018    |
| 931        | 17/SP         | Spring 2017                                                                           | Spring | 2016-2017    |
| 931        | 17/SU         | Summer 2017                                                                           | Summer | 2017-2018    |
| 931        | 17/WI         | WInter 2017                                                                           | Winter | 2016-2017    |
| 931        | 18/FA         | Fall 2018                                                                             | Fall   | 2018-2019    |
| 931        | 18/SP         | Spring 2018                                                                           | Spring | 2017-2018    |
| 931        | 18/SU         | Summer 2018                                                                           | Summer | 2018-2019    |
| 931        | 18/WI         | Winter 2018                                                                           | Winter | 2017-2018    |
| 931        | 19/SP         | Spring 2019                                                                           | Spring | 2018-2019    |
| 931        | 19/WI         | Winter 2019                                                                           | Winter | 2018-2019    |
| 941        | FA14          | Fall 2014                                                                             | Fall   | 2014-2015    |
| 941        | FA15          | Fall 2015                                                                             | Fall   | 2015-2016    |
| 941        | FA16          | Fall 2016                                                                             | Fall   | 2016-2017    |
| 941        | FA17          | Fall 2017                                                                             | Fall   | 2017-2018    |
| 941        | FA18          | Fall 2018                                                                             | Fall   | 2018-2019    |
| 941        | SP14          | Spring 2014                                                                           | Spring | 2013-2014    |
| 941        | SP15          | Spring 2015                                                                           | Spring | 2014-2015    |
| 941        | SP16          | Spring 2016                                                                           | Spring | 2015-2016    |
| 941        | SP17          | Spring 2017                                                                           | Spring | 2016-2017    |
| 941        | SP18          | Spring 2018                                                                           | Spring | 2017-2018    |
| 941        | SP19          | Spring 2019                                                                           | Spring | 2018-2019    |
| 941        | SU14          | Summer 2014                                                                           | Summer | 2014-2015    |
| 941        | SU15          | Summer 2015                                                                           | Summer | 2015-2016    |
| 941        | SU16          | Summer 2016                                                                           | Summer | 2016-2017    |
| 941        | SU17          | Summer 2017                                                                           | Summer | 2017-2018    |
| 941        | SU18          | Summer 2018                                                                           | Summer | 2018-2019    |
| 951        | 2015FA        | Fall 2015                                                                             | Fall   | 2015-2016    |
| 951        | 2016FA        | Fall 2016                                                                             | Fall   | 2016-2017    |
| 951        | 2016SP        | Spring 2016                                                                           | Spring | 2015-2016    |
| 951        | 2016SU        | Summer 2016                                                                           | Summer | 2016-2017    |
| 951        | 2017FA        | Fall 2017                                                                             | Fall   | 2017-2018    |
| 951        | 2017SP        | Spring 2017                                                                           | Spring | 2016-2017    |
| 951        | 2017SU        | Summer 2017                                                                           | Summer | 2017-2018    |
| 951        | 2018FA        | Fall 2018                                                                             | Fall   | 2018-2019    |
| 951        | 2018SP        | Spring 2018                                                                           | Spring | 2017-2018    |
| 951        | 2018SU        | Summer 2018                                                                           | Summer | 2018-2019    |
| 951        | 2019SP        | Spring 2019                                                                           | Spring | 2018-2019    |
| 951        | 2019SU        | Summer 2019                                                                           | Summer | 2019-2020    |
| 961        | 14FAL         | Fall 2014                                                                             | Fall   | 2014-2015    |
| 961        | 14SUM         | Summer 2014                                                                           | Summer | 2014-2015    |
| 961        | 15FAL         | Fall 2015                                                                             | Fall   | 2015-2016    |
| 961        | 15SPR         | Spring 2015                                                                           | Spring | 2014-2015    |
| 961        | 15SUM         | Summer 2015                                                                           | Summer | 2015-2016    |
| 961        | 15WIN         | Winter 2015                                                                           | Winter | 2014-2015    |
| 961        | 16FAL         | Fall 2016                                                                             | Fall   | 2016-2017    |
| 961        | 16SPR         | Spring 2016                                                                           | Spring | 2015-2016    |
| 961        | 16SUM         | Summer 2016                                                                           | Summer | 2016-2017    |
| 961        | 16WIN         | Winter 2016                                                                           | Winter | 2015-2016    |
| 961        | 17FAL         | Fall 2017                                                                             | Fall   | 2017-2018    |
| 961        | 17SPR         | Spring 2017                                                                           | Spring | 2016-2017    |
| 961        | 17SUM         | Summer 2017                                                                           | Summer | 2017-2018    |
| 961        | 17WIN         | Winter 2017                                                                           | Winter | 2016-2017    |
| 961        | 18FAL         | Fall 2018                                                                             | Fall   | 2018-2019    |
| 961        | 18SPR         | Spring 2018                                                                           | Spring | 2017-2018    |
| 961        | 18SUM         | Summer 2018                                                                           | Summer | 2018-2019    |
| 961        | 18WIN         | Winter 2018                                                                           | Winter | 2017-2018    |
| 961        | 19FAL         | Fall 2019                                                                             | Fall   | 2019-2020    |
| 961        | 19SPR         | Spring 2019                                                                           | Spring | 2018-2019    |
| 961        | 19SUM         | Summer 2019                                                                           | Summer | 2019-2020    |
| 961        | 19WIN         | Winter 2019                                                                           | Winter | 2018-2019    |
| 962        | 14FAL         | Fall 2014                                                                             | Fall   | 2014-2015    |
| 962        | 14SUM         | Summer 2014                                                                           | Summer | 2014-2015    |
| 962        | 15FAL         | Fall 2015                                                                             | Fall   | 2015-2016    |
| 962        | 15SPR         | Spring 2015                                                                           | Spring | 2014-2015    |
| 962        | 15SUM         | Summer 2015                                                                           | Summer | 2015-2016    |
| 962        | 15WIN         | Winter 2015                                                                           | Winter | 2014-2015    |
| 962        | 16FAL         | Fall 2016                                                                             | Fall   | 2016-2017    |
| 962        | 16SPR         | Spring 2016                                                                           | Spring | 2015-2016    |
| 962        | 16SUM         | Summer 2016                                                                           | Summer | 2016-2017    |
| 962        | 16WIN         | Winter 2016                                                                           | Winter | 2015-2016    |
| 962        | 17FAL         | Fall 2017                                                                             | Fall   | 2017-2018    |
| 962        | 17SPR         | Spring 2017                                                                           | Spring | 2016-2017    |
| 962        | 17SUM         | Summer 2017                                                                           | Summer | 2017-2018    |
| 962        | 17WIN         | Winter 2017                                                                           | Winter | 2016-2017    |
| 962        | 18FAL         | Fall 2018                                                                             | Fall   | 2018-2019    |
| 962        | 18SPR         | Spring 2018                                                                           | Spring | 2017-2018    |
| 962        | 18SUM         | Summer 2018                                                                           | Summer | 2018-2019    |
| 962        | 18WIN         | Winter 2018                                                                           | Winter | 2017-2018    |
| 962        | 19FAL         | Fall 2019                                                                             | Fall   | 2019-2020    |
| 962        | 19SPR         | Spring 2019                                                                           | Spring | 2018-2019    |
| 962        | 19SUM         | Summer 2019                                                                           | Summer | 2019-2020    |
| 962        | 19WIN         | Winter 2019                                                                           | Winter | 2018-2019    |
| 963        | 14FAL         | Fall 2014                                                                             | Fall   | 2014-2015    |
| 963        | 14SUM         | Summer 2014                                                                           | Summer | 2014-2015    |
| 963        | 15FAL         | Fall 2015                                                                             | Fall   | 2015-2016    |
| 963        | 15SPR         | Spring 2015                                                                           | Spring | 2014-2015    |
| 963        | 15SUM         | Summer 2015                                                                           | Summer | 2015-2016    |
| 963        | 15WIN         | Winter 2015                                                                           | Winter | 2014-2015    |
| 963        | 16FAL         | Fall 2016                                                                             | Fall   | 2016-2017    |
| 963        | 16SPR         | Spring 2016                                                                           | Spring | 2015-2016    |
| 963        | 16SUM         | Summer 2016                                                                           | Summer | 2016-2017    |
| 963        | 16WIN         | Winter 2016                                                                           | Winter | 2015-2016    |
| 963        | 17FAL         | Fall 2017                                                                             | Fall   | 2017-2018    |
| 963        | 17SPR         | Spring 2017                                                                           | Spring | 2016-2017    |
| 963        | 17SUM         | Summer 2017                                                                           | Summer | 2017-2018    |
| 963        | 17WIN         | Winter 2017                                                                           | Winter | 2016-2017    |
| 963        | 18FAL         | Fall 2018                                                                             | Fall   | 2018-2019    |
| 963        | 18SPR         | Spring 2018                                                                           | Spring | 2017-2018    |
| 963        | 18SUM         | Summer 2018                                                                           | Summer | 2018-2019    |
| 963        | 18WIN         | Winter 2018                                                                           | Winter | 2017-2018    |
| 963        | 19SPR         | Spring 2019                                                                           | Spring | 2018-2019    |
| 963        | 19WIN         | Winter 2019                                                                           | Winter | 2018-2019    |
| 971        | 2013FA        | Fall 2013-Copper Mountain College                                                     | Fall   | 2013-2014    |
| 971        | 2013SP        | Spring 2013-Copper Mountain College                                                   | Spring | 2012-2013    |
| 971        | 2013SU        | Summer 2013-Copper Mountain Community College                                         | Summer | 2013-2014    |
| 971        | 2014FA        | Fall 2014-Copper Mountain College                                                     | Fall   | 2014-2015    |
| 971        | 2014SP        | Spring 2014-Copper Mountain College                                                   | Spring | 2013-2014    |
| 971        | 2014SU        | Summer 2014-Copper Mountain College                                                   | Summer | 2014-2015    |
| 971        | 2015FA        | Fall 2015-Copper Mountain College                                                     | Fall   | 2015-2016    |
| 971        | 2015SP        | Spring 2015-Copper Mountain College                                                   | Spring | 2014-2015    |
| 971        | 2015SU        | Summer 2015-Copper Mountain College                                                   | Summer | 2015-2016    |
| 971        | 2016FA        | Fall 2016-Copper Mountain College                                                     | Fall   | 2016-2017    |
| 971        | 2016SP        | Spring 2016-Copper Mountain College                                                   | Spring | 2015-2016    |
| 971        | 2016SU        | Summer 2016-Copper Mountain College                                                   | Summer | 2016-2017    |
| 971        | 2017FA        | Fall 2017 - Copper Mountain College                                                   | Fall   | 2017-2018    |
| 971        | 2017SP        | Spring 2017-Copper Mountain College                                                   | Spring | 2016-2017    |
| 971        | 2017SU        | Summer 2017 - Copper Mountain College                                                 | Summer | 2017-2018    |
| 971        | 2018FA        | Fall 2018 - Copper Mountain College                                                   | Fall   | 2018-2019    |
| 971        | 2018SP        | Spring 2018 - Copper Mountain College                                                 | Spring | 2017-2018    |
| 971        | 2018SU        | Summer 2018 - Copper Mountain College                                                 | Summer | 2018-2019    |
| 971        | 2019FA        | Fall 2019 - Copper Mountain College                                                   | Fall   | 2019-2020    |
| 971        | 2019SP        | Spring 2019 - Copper Mountain College                                                 | Spring | 2018-2019    |
| 971        | 2019SU        | Summer 2019 - Copper Mountain College                                                 | Summer | 2019-2020    |
| 981        | 2014FA|C1415  | 2014 Fall (Aug - Dec)                                                                 | Fall   | 2014-2015    |
| 981        | 2014SM|C1314  | 2014 Summer (Jun - Aug)                                                               | Summer | 2014-2015    |
| 981        | 2015FA|C1516  | 2015 Fall (Aug - Dec)                                                                 | Fall   | 2015-2016    |
| 981        | 2015SM|C1415  | 2015 Summer (Jun - Aug)                                                               | Summer | 2015-2016    |
| 981        | 2015SP|C1415  | 2015 Spring (Jan - May)                                                               | Spring | 2014-2015    |
| 981        | 2016FA|C1617  | 2016 Fall (Aug - Dec)                                                                 | Fall   | 2016-2017    |
| 981        | 2016SM|C1516  | 2016 Summer (Jun - Aug)                                                               | Summer | 2016-2017    |
| 981        | 2016SP|C1516  | 2016 Spring (Jan - May)                                                               | Spring | 2015-2016    |
| 981        | 2017FA|C1718  | 2017 Fall (Aug - Dec)                                                                 | Fall   | 2017-2018    |
| 981        | 2017SM|C1617  | 2017 Summer (Jun - Aug)                                                               | Summer | 2017-2018    |
| 981        | 2017SP|C1617  | 2017 Spring (Jan - May)                                                               | Spring | 2016-2017    |
| 981        | 2018FA|C1819  | 2018 Fall (Aug - Dec)                                                                 | Fall   | 2018-2019    |
| 981        | 2018SM|C1718  | 2018 Summer (Jun - Aug)                                                               | Summer | 2018-2019    |
| 981        | 2018SP|C1718  | 2018 Spring (Jan - May)                                                               | Spring | 2017-2018    |
| 981        | 2019SP|C1819  | 2019 Spring (Jan - May)                                                               | Spring | 2018-2019    |
| 982        | 2015FA|V1516  | Fall 2015 (August - December)                                                         | Fall   | 2015-2016    |
| 982        | 2016FA|V1617  | Fall 2016 (August - December)                                                         | Fall   | 2016-2017    |
| 982        | 2016SM|V1516  | Summer 2016 (June - August)                                                           | Summer | 2016-2017    |
| 982        | 2016SP|V1516  | Spring 2016 (January - May)                                                           | Spring | 2015-2016    |
| 982        | 2017FA|V1718  | Fall 2017 (August - December)                                                         | Fall   | 2017-2018    |
| 982        | 2017SM|V1617  | Summer 2017 (June - August)                                                           | Summer | 2017-2018    |
| 982        | 2017SP|V1617  | Spring 2017 (January - May)                                                           | Spring | 2016-2017    |
| 982        | 2018FA|V1819  | Fall 2018 (August - December)                                                         | Fall   | 2018-2019    |
| 982        | 2018SM|V1718  | Summer 2018 (June - August)                                                           | Summer | 2018-2019    |
| 982        | 2018SM|v1718  | donotuse                                                                              | NULL   | NULL         |
| 982        | 2018SP|V1718  | Spring 2018 (January - May)                                                           | Spring | 2017-2018    |
| 982        | 2019SP|V1819  | Spring 2019 (January - May)                                                           | Spring | 2018-2019    |
| 991        | 2013FA        | Fall 2013  - Victor Valley College                                                    | Fall   | 2013-2014    |
| 991        | 2014FA        | Fall 2014 -Victor Valley College                                                      | Fall   | 2014-2015    |
| 991        | 2014SP        | Spring 2014  - Victor Valley College                                                  | Spring | 2013-2014    |
| 991        | 2014SU        | Summer 2014  - Victor Valley College                                                  | Summer | 2014-2015    |
| 991        | 2014WI        | Winter 2014  - Victor Valley College                                                  | Winter | 2013-2014    |
| 991        | 2015FA        | Fall 2015 - Victor Valley College                                                     | Fall   | 2015-2016    |
| 991        | 2015SP        | Spring 2015 -  Victor Valley College                                                  | Spring | 2014-2015    |
| 991        | 2015SU        | Summer 2015 - Victor Valley College                                                   | Summer | 2015-2016    |
| 991        | 2015WI        | Winter 2015 - Victor Valley College                                                   | Winter | 2014-2015    |
| 991        | 2016FA        | Fall 2016 -  Victor Valley College                                                    | Fall   | 2016-2017    |
| 991        | 2016SP        | Spring 2016 - Victor Valley College                                                   | Spring | 2015-2016    |
| 991        | 2016SU        | Summer 2016 -  Victor Valley College                                                  | Summer | 2016-2017    |
| 991        | 2016WI        | Winter 2016 - Victor Valley College                                                   | Winter | 2015-2016    |
| 991        | 2017FA        | Fall 2017 - Victor Valley College                                                     | Fall   | 2017-2018    |
| 991        | 2017SP        | Spring 2017 - Victor Valley College                                                   | Spring | 2016-2017    |
| 991        | 2017SU        | Summer 2017 - Victor Valley College                                                   | Summer | 2017-2018    |
| 991        | 2017WI        | Winter 2017 - Victor Valley College                                                   | Winter | 2016-2017    |
| 991        | 2018FA        | Fall 2018 - Victor Valley College                                                     | Fall   | 2018-2019    |
| 991        | 2018SP        | Spring 2018 - Victor Valley College                                                   | Spring | 2017-2018    |
| 991        | 2018SU        | Summer 2018 - Victor Valley College                                                   | Summer | 2018-2019    |
| 991        | 2018WI        | Winter 2018 - Victor Valley College                                                   | Winter | 2017-2018    |
| 991        | 2019FA        | Fall 2019 - Victor Valley College                                                     | Fall   | 2019-2020    |
| 991        | 2019SP        | Spring 2019 - Victor Valley College                                                   | Spring | 2018-2019    |
| 991        | 2019SU        | Summer 2019 - Victor Valley College                                                   | Summer | 2019-2020    |
| 991        | 2019WI        | Winter 2019 - Victor Valley College                                                   | Winter | 2018-2019    |
+------------+---------------+---------------------------------------------------------------------------------------+--------+--------------+
*/