USE calpass;

GO

IF (object_id('dbo.p_msnell_cohort_insert') is not null)
	BEGIN
		DROP PROCEDURE dbo.p_msnell_cohort_insert;
	END;

GO

CREATE PROCEDURE
	dbo.p_msnell_cohort_insert
AS
	
BEGIN

	-- start fresh
	TRUNCATE TABLE calpass.dbo.msnell_cohort;
	
	-- populate table
	INSERT INTO
		calpass.dbo.msnell_cohort
		(
			derkey1,
			hs_all_grades_ind
		)
	SELECT
		derkey1,
		hs_grade_all_ind = case
			when max(case when s.gradelevel = '09' then 1 else 0 end) + 
				max(case when s.gradelevel = '10' then 1 else 0 end) + 
				max(case when s.gradelevel = '11' then 1 else 0 end) + 
				max(case when s.gradelevel = '12' then 1 else 0 end) = 4 then 1
			else 0
		end
	FROM
		calpass.dbo.k12studentprod s
	WHERE
		s.gradeLevel in ('09', '10', '11', '12')
		and exists (
			SELECT
				1
			FROM
				calpass.dbo.k12courseprod c
			WHERE
				s.school = c.school
				and s.locstudentid = c.locstudentid
				and s.acyear = c.acyear
		)
	GROUP BY
		derkey1;

	-- get graduation year
	UPDATE
		t
	SET
		t.hs_graduation_year_description = g.graduation_year_description
	FROM
		calpass.dbo.msnell_cohort t
		outer apply
		calpass.dbo.f_hs_student_graduation(
			t.derkey1
		) g;

	-- get first CC year
	UPDATE
		t
	SET
		t.cc_first_year_description = d.year_description
	FROM
		calpass.dbo.msnell_cohort t
		inner join
		(
			SELECT
				s.derkey1,
				year_description = min(t1.year_description)
			FROM
				calpass.dbo.ccstudentprod s
				inner join
				calpass.dbo.msnell_cohort a
					on a.derkey1 = s.derkey1
				inner join
				calpass.dbo.ccstudentprod_termId t1
					on t1.code = s.termId
				inner join
				calpass.dbo.ccstudentprod_termId t2
					on t2.year_description = a.hs_graduation_year_description
			WHERE
				t1.year_trailing > t2.year_trailing
				and exists (
					SELECT
						1
					FROM
						calpass.dbo.cccourseprod c
					WHERE
						c.collegeId = s.collegeId
						and c.studentId = s.studentId
						and c.termId = s.termId
						and c.creditFlag in ('T', 'D', 'C', 'S')
						and c.grade not in ('DR', 'UD', 'XX') -- RP Group Operational Definition of Course Enrollment
				)
			GROUP BY
				s.derkey1
		) d
			on t.derkey1 = d.derkey1;

	/* summary: mathematics */
	UPDATE
		t
	SET
		t.hs_09_math_highest_rank = s.hs_09_math_highest_rank,
		t.hs_10_math_highest_rank = s.hs_10_math_highest_rank,
		t.hs_11_math_highest_rank = s.hs_11_math_highest_rank,
		t.hs_12_math_highest_rank = s.hs_12_math_highest_rank,
		t.hs_up11_math_highest_rank = s.hs_up11_math_highest_rank,
		t.hs_up12_math_highest_rank = s.hs_up12_math_highest_rank
	FROM
		calpass.dbo.msnell_cohort t
		inner join
		(
			SELECT
				derkey1 = hss.Derkey1,
				hs_09_math_highest_rank = max(
					case
						when cbeds.content_area_category_assignment_name = 'Mathematics'
							and hss.GradeLevel = '09'
							and hscg.points >= 1.7 then cbeds_rank.rank
					end
				),
				hs_10_math_highest_rank = max(
					case
						when cbeds.content_area_category_assignment_name = 'Mathematics'
							and hss.GradeLevel = '10'
							and hscg.points >= 1.7 then cbeds_rank.rank
					end
				),
				hs_11_math_highest_rank = max(
					case
						when cbeds.content_area_category_assignment_name = 'Mathematics'
							and hss.GradeLevel = '11'
							and hscg.points >= 1.7 then cbeds_rank.rank
					end
				),
				hs_12_math_highest_rank = max(
					case
						when cbeds.content_area_category_assignment_name = 'Mathematics'
							and hss.GradeLevel = '12'
							and hscg.points >= 1.7 then cbeds_rank.rank
					end
				),
				hs_up11_math_highest_rank = max(
					case
						when cbeds.content_area_category_assignment_name = 'Mathematics'
							and hss.GradeLevel <= '11'
							and hscg.points >= 1.7 then cbeds_rank.rank
					end
				),
				hs_up12_math_highest_rank = max(
					case
						when cbeds.content_area_category_assignment_name = 'Mathematics'
							and hss.GradeLevel <= '12'
							and hscg.points >= 1.7 then cbeds_rank.rank
					end
				)
			FROM
				calpass.dbo.msnell_cohort c
				inner join
				calpass.dbo.K12StudentProd hss
					with(index(ix_K12StudentProd__Derkey1__GradeLevel__AcYear))
					on c.derkey1 = hss.derkey1
				inner join
				calpass.dbo.K12CourseProd hsc
					with(index(ixc_K12CourseProd__School__LocStudentId__AcYear))
					on hss.school = hsc.school
					and hss.LocStudentId = hsc.LocStudentId
					and hss.AcYear = hsc.AcYear
				inner join
				calpass.dbo.K12StudentProd_AcYear ay
					on ay.code = hss.AcYear
				inner join
				calpass.dbo.K12CourseProd_grade hscg
					on hscg.code = hsc.grade
				left outer join
				calpass.dbo.K12CourseProd_cbeds cbeds
					on cbeds.code = case
							when hsc.courseid = '6098' then
								case
									when lower(hsc.coursetitle) like '%alg%' then
										case
											when lower(hsc.coursetitle) like '%pre%' then '2424'
											when lower(hsc.coursetitle) like '%ii%' then '2404'
											when lower(hsc.coursetitle) like '%i%' then '2403'
										end
									when lower(hsc.coursetitle) like '%trig%' then '2407'
									when lower(hsc.coursetitle) like '%arith%' then '2400'
									when lower(hsc.coursetitle) like '%calc%' then
										case
											when lower(hsc.coursetitle) like '%pre%' then '2414'
											else '2415'
										end
									when lower(hsc.coursetitle) like '%stat%' then '2445'
									when (
										lower(hsc.coursetitle) like '%geo%'
										and lower(hsc.coursetitle) not like '%geol%'
										and lower(hsc.coursetitle) not like '%astron%'
										and lower(hsc.coursetitle) not like '%hlth%'
										and lower(hsc.coursetitle) not like '%his%'
										and lower(hsc.coursetitle) not like '%hst%'
										and lower(hsc.coursetitle) not like '%geog%'
										and lower(hsc.coursetitle) not like '%world%'
										and lower(hsc.coursetitle) not like '%wld%'
										and lower(hsc.coursetitle) not like '%wrld%'
										and lower(hsc.coursetitle) not like '%lap%'
										and lower(hsc.coursetitle) not like '%clt%'
										and lower(hsc.coursetitle) not like '%cult%'
									) then '2413'
								end
							else hsc.courseid
						end
					and cbeds.year_code_start <= ay.code_calpads
					and cbeds.year_code_end >= ay.code_calpads
				left outer join
				calpass.dbo.K12CourseProd_cbeds_rank cbeds_rank
					on cbeds_rank.code = cbeds.code
			WHERE
				hss.GradeLevel in ('09', '10', '11', '12')
				-- Elk Grove No MOU
				and hss.School not in (
					SELECT
						organizationCode
					FROM
						calpass.dbo.organization
					WHERE
						parentId = 159610
				)
			GROUP BY
				hss.Derkey1
		) s
			on s.derkey1 = t.derkey1;

END;