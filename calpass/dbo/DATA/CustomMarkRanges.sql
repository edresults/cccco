-- Lindsay Unified
INSERT INTO
    CustomMarkRanges
    (
        OrganizationCode,
        MarkCode,
        LowerBound,
        UpperBound
    )
SELECT
    OrganizationCode,
    MarkCode,
    LowerBound,
    UpperBound
FROM
    (
        VALUES
        ('54722560109751'),
        ('54722560133819'),
        ('54722565430012'),
        ('54722565430269'),
        ('54722566054555'),
        ('54722566054605'),
        ('54722566092373'),
        ('54722566112049')
    ) o (OrganizationCode)
    cross join
    (
        VALUES
        ('A', 3.834,4.000),
        ('A-',3.500,3.833),
        ('B+',3.167,3.499),
        ('B', 2.834,3.166),
        ('B-',2.500,2.833),
        ('C+',2.167,2.499),
        ('C', 1.834,2.166),
        ('C-',1.500,1.833),
        ('D+',1.167,1.499),
        ('D', 0.834,1.166),
        ('D-',0.500,0.833),
        ('F', 0.000,0.499)
    ) v (MarkCode, LowerBound, UpperBound);