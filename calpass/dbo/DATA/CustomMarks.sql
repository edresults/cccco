-- Lindsay Unified
INSERT INTO
    CustomMarks
    (
        OrganizationCode,
        SourceCode,
        TargetCode
    )
SELECT
    OrganizationCode,
    SourceCode,
    TargetCode
FROM
    (
        VALUES
        ('54719930123646'),
        ('54719930123653'),
        ('54719930123661'),
        ('54719930124776'),
        ('54719935430194'),
        ('54719935430236'),
        ('54719935430293'),
        ('54719935432398'),
        ('54719935432414'),
        ('54719936054142'),
        ('54719936054159'),
        ('54719936060313'),
        ('54719936107262')
    ) o (OrganizationCode)
    cross join
    (
        VALUES
        ('0' , 'F' ),
        ('1-', 'D-'),
        ('1' , 'D' ),
        ('1+', 'D+'),
        ('2-', 'C-'),
        ('2' , 'C' ),
        ('2+', 'C+'),
        ('3-', 'B-'),
        ('3' , 'B' ),
        ('3+', 'B+'),
        ('4-', 'A-'),
        ('4' , 'A' )
    ) v (SourceCode, TargetCode);