USE calpass;

GO

IF (object_id('mmap.TranscriptGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION mmap.TranscriptGet;
	END;

GO

CREATE FUNCTION
	mmap.TranscriptGet
	(
		@InterSegmentKey binary(64)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			GradeLevel                  = max(case when Entity = 'Cgpa' then Attribute end),
			CumulativeGradePointAverage = max(case when Entity = 'Cgpa' then Value end),
			English = 
				coalesce(
					max(case when Entity = 'EnglishAP' and Attribute = 'MarkPoints' then Value end),
					max(case when Entity = 'DualEnrollment' and Attribute = 'MarkPoints' then Value end),
					max(case when Entity = 'Composition' and Attribute = 'MarkPoints' then Value end),
					max(case when Entity = 'Literature' and Attribute = 'MarkPoints' then Value end),
					max(case when Entity = 'English12' and Attribute = 'MarkPoints' then Value end),
					max(case when Entity = 'English11' and Attribute = 'MarkPoints' then Value end),
					max(case when Entity = 'English10' and Attribute = 'MarkPoints' then Value end),
					max(case when Entity = 'English09' and Attribute = 'MarkPoints' then Value end),
					max(case when Entity = 'Comprehensive' and Attribute = 'MarkPoints' then Value end)
				),
			EnglishAP                   = max(case when Entity = 'EnglishAP' and Attribute = 'MarkPoints' then Value end),
			PreAlgebra                  = max(case when Entity = 'PreAlgebra' and Attribute = 'MarkPoints' then Value end),
			PreAlgebraAP                = null,
			AlgebraI                    = max(case when Entity = 'AlgebraI' and Attribute = 'MarkPoints' then Value end),
			AlgebraIAP                  = null,
			AlgebraII                   = max(case when Entity = 'AlgebraII' and Attribute = 'MarkPoints' then Value end),
			AlgebraIIAP                 = null,
			Geometry                    = max(case when Entity = 'Geonometry' and Attribute = 'MarkPoints' then Value end),
			GeometryAP                  = null,
			Trigonometry                = max(case when Entity = 'Trigonometry' and Attribute = 'MarkPoints' then Value end),
			TrigonometryAP              = null,
			PreCalculus                 = max(case when Entity = 'PreCalculus' and Attribute = 'MarkPoints' then Value end),
			PreCalculusAP               = null,
			Calculus                    = max(case when Entity = 'Calculus' and Attribute = 'MarkPoints' then Value end),
			CalculusAP                  = null,
			[Statistics]                = max(case when Entity = 'Statistics' and Attribute = 'MarkPoints' then Value end),
			StatisticsAP                = null,
			EslInd                      = max(case when Entity = 'Remedial' then 1 else 0 end)
		FROM
			mmap.TranscriptHashGet(@InterSegmentKey)
	);