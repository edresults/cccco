USE calpass;

GO

IF (object_id('mmap.HSSummaryHashGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION mmap.HSSummaryHashGet;
	END;

GO

CREATE FUNCTION
	mmap.HSSummaryHashGet
	(
		@OrganizationId int,
		@StudentStateId char(10)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			Attribute = 
				case
					when grouping(c.ContentArea) = 1 then 'Cgpa' + max(t.GradeCode)
					when grouping(c.ContentCode) = 1 then c.ContentArea + 'HighestRank'
					when grouping(c.ContentCode) = 0 and c.ContentCode is not null  then c.ContentCode + 'MarkPoints'
					when c.ContentCode is null then 'Cgpa' + max(t.GradeCode)
				end,
			Value = 
				case
					when grouping(c.ContentArea) = 1 then
						convert(
							decimal(4,3),
							sum(CreditPoints)
							/
							nullif(sum(CreditTry), 0)
						)
					when grouping(c.ContentCode) = 1 then max(c.IntraAreaRank)
					when grouping(c.ContentCode) = 0 and c.ContentCode is not null then convert(decimal(4,3), avg(MarkPoints))
				end
		FROM
			calpads.HSTranscriptGet(@StudentStateId) t
			left outer join
			calpads.CurriculumContent cc
				on cc.CurriculumCode = t.CurriculumCode
			left outer join
			calpads.Content c
				on c.ContentCode = cc.ContentCode
		GROUP BY
			ROLLUP(c.ContentArea, c.ContentCode)
		HAVING
			(
				grouping(c.ContentArea) = 1
				or
				c.ContentArea is not null
			)
		UNION
		SELECT
			Attribute,
			Value
		FROM
			mmap.RemedialUpperBound
		WHERE
			OrganizationId = @OrganizationId
	);