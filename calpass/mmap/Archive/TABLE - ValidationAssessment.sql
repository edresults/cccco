USE calpass;

GO

IF (object_id('mmap.ValidationAssessment', 'U') is not null)
	BEGIN
		DROP TABLE mmap.ValidationAssessment;
	END;

GO

CREATE TABLE
	mmap.ValidationAssessment
	(
		college_id char(3),
		student_id varchar(10),
		id_status char(1),
		InterSegmentKey binary(64),
		birthdate char(8),
		gender char(1),
		name_first varchar(30),
		name_last varchar(40),
		term_id char(3),
		placement_method tinyint,
		multiple_measures tinyint,
		test_subject varchar(4),
		test_description varchar(255),
		test_date char(8),
		test_raw_score decimal(6,2),
		mm_score decimal(6,2),
		test_perecent_correct decimal(5,2),
		test_scaled_score decimal(6,2),
		outcome_description varchar(8000),
		mm_outcome_description varchar(8000),
		unmodified_placement_level char(1),
		mm_placement_level char(1),
		unmodified_math_transfer_level char(1),
		mm_math_transfer_level char(1),
		SubmissionFileId int not null,
		SubmissionFileRecordNumber int not null,
		IsEscrow bit not null
	);

GO

CREATE CLUSTERED INDEX
	ixc_ValidationAssessment__college_id__student_id__id_status__test_subject__test_date__test_description
ON
	mmap.ValidationAssessment
	(
		college_id,
		student_id,
		id_status,
		test_subject,
		test_date,
		test_description
	);

CREATE NONCLUSTERED INDEX
	ix_ValidationAssessment__SubmissionFileId__SubmissionFileRecordNumber
ON
	mmap.ValidationAssessment
	(
		SubmissionFileId,
		SubmissionFileRecordNumber
	)
INCLUDE
	(
		IsEscrow
	);

CREATE NONCLUSTERED INDEX
	ix_ValidationAssessment__IsEscrow
ON
	mmap.ValidationAssessment
	(
		IsEscrow
	);

CREATE NONCLUSTERED INDEX
	ix_ValidationAssessment__InterSegmentKey
ON
	mmap.ValidationAssessment
	(
		InterSegmentKey
	);