USE calpass;

GO

IF (type_id('mmap.Ruleset') is not null)
	BEGIN
		DROP TYPE mmap.Ruleset;
	END;

GO

CREATE TYPE
	mmap.Ruleset
AS TABLE
	(
		Entity varchar(255) not null,
		Attribute varchar(255) not null,
		Value decimal(6,3) not null,
		BranchId int not null,
		LeafCount int not null,
		CourseCode varchar(255) not null,
		PRIMARY KEY CLUSTERED
			(
				Entity,
				Attribute,
				Value,
				BranchId
			)
	);