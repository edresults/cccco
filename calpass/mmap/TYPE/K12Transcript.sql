USE calpass;

GO

IF (type_id('mmap.K12Transcript') is not null)
	BEGIN
		DROP TYPE mmap.K12Transcript;
	END;

GO

CREATE TYPE
	mmap.K12Transcript
AS TABLE
	(
		StudentStateId              char(10)     not null,
		IsHighSchoolGrade09         bit          not null,
		IsHighSchoolGrade10         bit          not null,
		IsHighSchoolGrade11         bit          not null,
		IsHighSchoolGrade12         bit          not null,
		CumulativeGradePointAverage decimal(4,3) not null,
		English                     decimal(2,1)     null,
		PreAlgebra                  decimal(2,1)     null,
		AlgebraI                    decimal(2,1)     null,
		Geometry                    decimal(2,1)     null,
		AlgebraII                   decimal(2,1)     null,
		Trigonometry                decimal(2,1)     null,
		PreCalculus                 decimal(2,1)     null,
		[Statistics]                decimal(2,1)     null,
		Calculus                    decimal(2,1)     null,
		PRIMARY KEY CLUSTERED
			(
				StudentStateId
			)
	);