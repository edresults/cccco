IF (type_id('Mmap.RetrospectiveCourseContent') is not null)
	BEGIN
		DROP TYPE Mmap.RetrospectiveCourseContent;
	END;

GO

CREATE TYPE
	mmap.RetrospectiveCourseContent
AS TABLE
	(
		InterSegmentKey binary(64) not null,
		DepartmentCode tinyint not null,
		-- organization
		GradeCode char(2) not null,
		SchoolCode char(14) not null,
		-- time
		YearTermCode char(5) not null,
		TermCode char(2) not null,
		-- course
		ContentCode varchar(25) not null,
		ContentRank tinyint not null,
		CourseCode char(4) not null,
		CourseUnits decimal(4,2),
		CourseTitle varchar(40),
		CourseAGCode char(2),
		CourseLevelCode char(2),
		CourseTypeCode char(2),
		-- Section
		SectionMark varchar(3) not null,
		MarkPoints decimal(2,1) not null,
		MarkCategory smallint not null,
		IsSuccess bit not null,
		-- selectors
		ContentSelector tinyint not null,
		GradeSelector tinyint not null,
		RecencySelector tinyint not null,
		PRIMARY KEY CLUSTERED
			(
				InterSegmentKey,
				DepartmentCode,
				RecencySelector
			)
	);