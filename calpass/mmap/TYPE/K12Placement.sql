USE calpass;

GO

IF (type_id('mmap.K12Placement') is not null)
	BEGIN
		DROP TYPE mmap.K12Placement;
	END;

GO

CREATE TYPE
	mmap.K12Placement
AS TABLE
	(
		StudentStateId   char(10) not null,
		EnglishY         bit,
		EnglishA         bit,
		EnglishB         bit,
		EnglishC         bit,
		PreAlgebra       bit,
		AlgebraI         bit,
		AlgebraII        bit,
		MathGE           bit,
		[Statistics]     bit,
		CollegeAlgebra   bit,
		Trigonometry     bit,
		PreCalculus      bit,
		CalculusI        bit,
		ReadingM_UboundY bit,
		ReadingY_UboundY bit,
		ReadingA_UboundY bit,
		ReadingB_UboundY bit,
		ReadingC_UboundY bit,
		ReadingM_UboundA bit,
		ReadingA_UboundA bit,
		ReadingB_UboundA bit,
		ReadingC_UboundA bit,
		ReadingM_UboundB bit,
		ReadingB_UboundB bit,
		ReadingC_UboundB bit,
		EslY_UboundY     bit,
		EslA_UboundY     bit,
		EslB_UboundY     bit,
		EslA_UboundA     bit,
		EslB_UboundA     bit,
		EslC_UboundA     bit,
		EslB_UboundB     bit,
		EslC_UboundB     bit,
		EslD_UboundB     bit,
		PRIMARY KEY CLUSTERED
			(
				StudentStateId
			)
	);