USE calpass;

GO

IF (type_id('mmap.Placement') is not null)
	BEGIN
		DROP TYPE mmap.Placement;
	END;

GO

CREATE TYPE
	mmap.Placement
AS TABLE
	(
		InterSegmentKey  binary(64) not null,
		EnglishY         bit            null,
		EnglishA         bit            null,
		EnglishB         bit            null,
		EnglishC         bit            null,
		PreAlgebra       bit            null,
		AlgebraI         bit            null,
		AlgebraII        bit            null,
		MathGE           bit            null,
		[Statistics]     bit            null,
		CollegeAlgebra   bit            null,
		Trigonometry     bit            null,
		PreCalculus      bit            null,
		CalculusI        bit            null,
		ReadingM_UboundY bit            null,
		ReadingY_UboundY bit            null,
		ReadingA_UboundY bit            null,
		ReadingB_UboundY bit            null,
		ReadingC_UboundY bit            null,
		ReadingM_UboundA bit            null,
		ReadingA_UboundA bit            null,
		ReadingB_UboundA bit            null,
		ReadingC_UboundA bit            null,
		ReadingM_UboundB bit            null,
		ReadingB_UboundB bit            null,
		ReadingC_UboundB bit            null,
		EslY_UboundY     bit            null,
		EslA_UboundY     bit            null,
		EslB_UboundY     bit            null,
		EslA_UboundA     bit            null,
		EslB_UboundA     bit            null,
		EslC_UboundA     bit            null,
		EslB_UboundB     bit            null,
		EslC_UboundB     bit            null,
		EslD_UboundB     bit            null,
		IsTreatment      bit            null,
		PRIMARY KEY CLUSTERED
			(
				InterSegmentKey
			)
	);