USE calpass;

GO

IF (type_id('mmap.StudentTranscriptSummaryHash') is not null)
	BEGIN
		DROP TYPE mmap.StudentTranscriptSummaryHash;
	END;

GO

CREATE TYPE
	mmap.StudentTranscriptSummaryHash
AS TABLE
	(
		InterSegmentKey binary(64) not null,
		Entity varchar(25),
		Attribute varchar(25),
		Value decimal(4,3),
		PRIMARY KEY CLUSTERED
			(
				InterSegmentKey,
				Entity,
				Attribute
			),
		INDEX
			IX_StudentTranscriptSummaryHash
		NONCLUSTERED
			(
				Entity,
				Attribute,
				Value
			)
	);