IF (type_id('Mmap.RetrospectivePerformance') is not null)
	BEGIN
		DROP TYPE Mmap.RetrospectivePerformance;
	END;

GO

CREATE TYPE
	Mmap.RetrospectivePerformance
AS TABLE
	(
		InterSegmentKey                 binary(64) not null,
		DepartmentCode                  char(2) not null,
		GradeCode                       char(2) not null,
		QualityPoints                   decimal(9,3) null,
		CreditAttempted                 decimal(9,3) null,
		GradePointAverage               decimal(9,3) null,
		GradePointAverageSans           decimal(9,3) null,
		CumulativeQualityPoints         decimal(9,3) null,
		CumulativeCreditAttempted       decimal(9,3) null,
		CumulativeGradePointAverage     decimal(9,3) null,
		CumulativeGradePointAverageSans decimal(9,3) null,
		FirstYearTermCode               char(5) null,
		LastYearTermCode                char(5) null,
		IsFirst                         bit null,
		IsLast                          bit null,
		PRIMARY KEY CLUSTERED
			(
				InterSegmentKey,
				DepartmentCode,
				GradeCode
			)
	);