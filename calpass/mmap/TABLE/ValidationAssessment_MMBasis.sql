USE calpass;

GO

IF (object_id('mmap.ValidationAssessment_MMBasis') is not null)
	BEGIN
		DROP TABLE mmap.ValidationAssessment_MMBasis;
	END;

GO

CREATE TABLE
	mmap.ValidationAssessment_MMBasis
	(
		MMBasisId tinyint identity(1,1),
		Description varchar(255)
	);

ALTER TABLE
	mmap.ValidationAssessment_MMBasis
ADD CONSTRAINT
	pk_ValidationAssessment_MMBasis
PRIMARY KEY CLUSTERED
	(
		MMBasisId
	);

GO

SET IDENTITY_INSERT mmap.ValidationAssessment_MMBasis ON;

INSERT INTO
	mmap.ValidationAssessment_MMBasis
	(
		MMBasisId,
		Description
	)
VALUES
	(1,'Multiple Measures placement based on self-reported data'),
	(2,'Multiple Measures placement based on CalPass Plus data'),
	(3,'Multiple Measures placement based on other data source'),
	(4,'Multiple possible sources of data used but source info not maintained in local data'),
	(5,'Other'),
	(6,'Unknown'),
	(7,'Not Applicable');

SET IDENTITY_INSERT mmap.ValidationAssessment_MMBasis OFF;