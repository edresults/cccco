USE calpass;

GO

IF (object_id('mmap.ValidationAssessment_PlacementLevel') is not null)
	BEGIN
		DROP TABLE mmap.ValidationAssessment_PlacementLevel;
	END;

GO

CREATE TABLE
	mmap.ValidationAssessment_PlacementLevel
	(
		PlacementLevelCode char(1) not null,
		Description varchar(255)
	);

ALTER TABLE
	mmap.ValidationAssessment_PlacementLevel
ADD CONSTRAINT
	pk_ValidationAssessment_PlacementLevel
PRIMARY KEY CLUSTERED
	(
		PlacementLevelCode
	);

GO

INSERT INTO
	mmap.ValidationAssessment_PlacementLevel
	(
		PlacementLevelCode,
		Description
	)
VALUES
	('Y','Transfer level'),
	('A','One level below'),
	('B','Two levels below'),
	('C','Three levels below'),
	('D','Four levels below'),
	('E','Five levels below'),
	('F','Six levels below'),
	('G','Seven levels below'),
	('H','Eight levels below'),
	('M','Meet or exceeds local reading competency/no Reading course required (e.g., students that test or place out) - For TestSubjectCode = READ only'),
	('Z','Not placed in this ELA discipline (as identified by TestSubjectCode) via this method of placement. Different than NULL.');