DROP TABLE mmap.CapMmapEnglishGrade;

GO

SELECT
	CohortYear,
	b.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage,
	-- high school: 09
	hs_09_grade_level                = max(case when rcc.GradeCode = '09' then rcc.GradeCode end),
	hs_09_school_code                = max(case when rcc.GradeCode = '09' then rcc.SchoolCode end),
	hs_09_year_term_code             = max(case when rcc.GradeCode = '09' then rcc.YearTermCode end),
	hs_09_course_id                  = max(case when rcc.GradeCode = '09' then rcc.CourseCode end),
	hs_09_course_title               = max(case when rcc.GradeCode = '09' then rcc.CourseTitle end),
	hs_09_course_grade               = max(case when rcc.GradeCode = '09' then rcc.SectionMark end),
	hs_09_course_grade_points        = max(case when rcc.GradeCode = '09' then rcc.MarkPoints end),
	hs_09_course_grade_category      = max(case when rcc.GradeCode = '09' then rcc.MarkCategory end),
	hs_09_course_success_ind         = max(case when rcc.GradeCode = '09' then convert(tinyint, rcc.IsSuccess) end),
	hs_09_course_ag_code             = max(case when rcc.GradeCode = '09' then rcc.CourseAGCode end),
	hs_09_course_level_code          = max(case when rcc.GradeCode = '09' then rcc.CourseLevelCode end),
	hs_09_course_type_code           = max(case when rcc.GradeCode = '09' then rcc.CourseTypeCode end),
	-- high school: 10
	hs_10_grade_level                = max(case when rcc.GradeCode = '10' then rcc.GradeCode end),
	hs_10_school_code                = max(case when rcc.GradeCode = '10' then rcc.SchoolCode end),
	hs_10_year_term_code             = max(case when rcc.GradeCode = '10' then rcc.YearTermCode end),
	hs_10_course_id                  = max(case when rcc.GradeCode = '10' then rcc.CourseCode end),
	hs_10_course_title               = max(case when rcc.GradeCode = '10' then rcc.CourseTitle end),
	hs_10_course_grade               = max(case when rcc.GradeCode = '10' then rcc.SectionMark end),
	hs_10_course_grade_points        = max(case when rcc.GradeCode = '10' then rcc.MarkPoints end),
	hs_10_course_grade_category      = max(case when rcc.GradeCode = '10' then rcc.MarkCategory end),
	hs_10_course_success_ind         = max(case when rcc.GradeCode = '10' then convert(tinyint, rcc.IsSuccess) end),
	hs_10_course_ag_code             = max(case when rcc.GradeCode = '10' then rcc.CourseAGCode end),
	hs_10_course_level_code          = max(case when rcc.GradeCode = '10' then rcc.CourseLevelCode end),
	hs_10_course_type_code           = max(case when rcc.GradeCode = '10' then rcc.CourseTypeCode end),
	-- high school: 11
	hs_11_grade_level                = max(case when rcc.GradeCode = '11' then rcc.GradeCode end),
	hs_11_school_code                = max(case when rcc.GradeCode = '11' then rcc.SchoolCode end),
	hs_11_year_term_code             = max(case when rcc.GradeCode = '11' then rcc.YearTermCode end),
	hs_11_course_id                  = max(case when rcc.GradeCode = '11' then rcc.CourseCode end),
	hs_11_course_title               = max(case when rcc.GradeCode = '11' then rcc.CourseTitle end),
	hs_11_course_grade               = max(case when rcc.GradeCode = '11' then rcc.SectionMark end),
	hs_11_course_grade_points        = max(case when rcc.GradeCode = '11' then rcc.MarkPoints end),
	hs_11_course_grade_category      = max(case when rcc.GradeCode = '11' then rcc.MarkCategory end),
	hs_11_course_success_ind         = max(case when rcc.GradeCode = '11' then convert(tinyint, rcc.IsSuccess) end),
	hs_11_course_ag_code             = max(case when rcc.GradeCode = '11' then rcc.CourseAGCode end),
	hs_11_course_level_code          = max(case when rcc.GradeCode = '11' then rcc.CourseLevelCode end),
	hs_11_course_type_code           = max(case when rcc.GradeCode = '11' then rcc.CourseTypeCode end),
	-- high school: 12
	hs_12_grade_level                = max(case when rcc.GradeCode = '12' then rcc.GradeCode end),
	hs_12_school_code                = max(case when rcc.GradeCode = '12' then rcc.SchoolCode end),
	hs_12_year_term_code             = max(case when rcc.GradeCode = '12' then rcc.YearTermCode end),
	hs_12_course_id                  = max(case when rcc.GradeCode = '12' then rcc.CourseCode end),
	hs_12_course_title               = max(case when rcc.GradeCode = '12' then rcc.CourseTitle end),
	hs_12_course_grade               = max(case when rcc.GradeCode = '12' then rcc.SectionMark end),
	hs_12_course_grade_points        = max(case when rcc.GradeCode = '12' then rcc.MarkPoints end),
	hs_12_course_grade_category      = max(case when rcc.GradeCode = '12' then rcc.MarkCategory end),
	hs_12_course_success_ind         = max(case when rcc.GradeCode = '12' then convert(tinyint, rcc.IsSuccess) end),
	hs_12_course_ag_code             = max(case when rcc.GradeCode = '12' then rcc.CourseAGCode end),
	hs_12_course_level_code          = max(case when rcc.GradeCode = '12' then rcc.CourseLevelCode end),
	hs_12_course_type_code           = max(case when rcc.GradeCode = '12' then rcc.CourseTypeCode end),
	-- high school: 12
	hs_recency_grade_level           = max(case when rcc.RecencySelector = 1 then rcc.GradeCode end),
	hs_recency_school_code           = max(case when rcc.RecencySelector = 1 then rcc.SchoolCode end),
	hs_recency_year_term_code        = max(case when rcc.RecencySelector = 1 then rcc.YearTermCode end),
	hs_recency_course_id             = max(case when rcc.RecencySelector = 1 then rcc.CourseCode end),
	hs_recency_course_title          = max(case when rcc.RecencySelector = 1 then rcc.CourseTitle end),
	hs_recency_course_grade          = max(case when rcc.RecencySelector = 1 then rcc.SectionMark end),
	hs_recency_course_grade_points   = max(case when rcc.RecencySelector = 1 then rcc.MarkPoints end),
	hs_recency_course_grade_category = max(case when rcc.RecencySelector = 1 then rcc.MarkCategory end),
	hs_recency_course_success_ind    = max(case when rcc.RecencySelector = 1 then convert(tinyint, rcc.IsSuccess) end),
	hs_recency_course_ag_code        = max(case when rcc.RecencySelector = 1 then rcc.CourseAGCode end),
	hs_recency_course_level_code     = max(case when rcc.RecencySelector = 1 then rcc.CourseLevelCode end),
	hs_recency_course_type_code      = max(case when rcc.RecencySelector = 1 then rcc.CourseTypeCode end),
	-- binary variables
	EXPOSITORY_UP11                  = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_UP11_C                = max(case when rcc.MarkPoints >= 2 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_UP11_Cplus            = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_UP11_Bminus           = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_UP11_B                = max(case when rcc.MarkPoints >= 3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_ANY                   = max(case when rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_ANY_C                 = max(case when rcc.MarkPoints >= 2 and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_ANY_Cplus             = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_ANY_Bminus            = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_ANY_B                 = max(case when rcc.MarkPoints >= 3 and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	REMEDIAL_UP11                    = max(case when rcc.GradeCode <= '11' and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_UP11_C                  = max(case when rcc.MarkPoints >= 2 and rcc.GradeCode <= '11' and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_UP11_Cplus              = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_UP11_Bminus             = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_UP11_B                  = max(case when rcc.MarkPoints >= 3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_ANY                     = max(case when rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_ANY_C                   = max(case when rcc.MarkPoints >= 2 and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_ANY_Cplus               = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_ANY_Bminus              = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_ANY_B                   = max(case when rcc.MarkPoints >= 3 and rcc.CourseCode = '2100' then 1 else 0 end),
	-- college: First
	cc_primacy_college_id            = max(case when ccc.FirstSelector = 1 then ccc.CollegeId end),
	cc_primacy_course_level          = max(case when ccc.FirstSelector = 1 then ccc.CourseLevel end),
	cc_primacy_year_term_code        = max(case when ccc.FirstSelector = 1 then ccc.YearTermCode end),
	cc_primacy_course_id             = max(case when ccc.FirstSelector = 1 then ccc.CourseId end),
	cc_primacy_course_title          = max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end),
	cc_primacy_course_grade          = max(case when ccc.FirstSelector = 1 then ccc.SectionMark end),
	cc_primacy_course_grade_points   = max(case when ccc.FirstSelector = 1 then ccc.MarkPoints end),
	cc_primacy_course_grade_category = max(case when ccc.FirstSelector = 1 then ccc.MarkCategory end),
	cc_primacy_course_success_ind    = max(case when ccc.FirstSelector = 1 then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: Y
	cc_00_college_id                 = max(case when ccc.CourseLevel = 'Y' then ccc.CollegeId end),
	cc_00_course_level               = max(case when ccc.CourseLevel = 'Y' then ccc.CourseLevel end),
	cc_00_year_term_code             = max(case when ccc.CourseLevel = 'Y' then ccc.YearTermCode end),
	cc_00_course_id                  = max(case when ccc.CourseLevel = 'Y' then ccc.CourseId end),
	cc_00_course_title               = max(case when ccc.CourseLevel = 'Y' then ccc.CourseTitle end),
	cc_00_course_grade               = max(case when ccc.CourseLevel = 'Y' then ccc.SectionMark end),
	cc_00_course_grade_points        = max(case when ccc.CourseLevel = 'Y' then ccc.MarkPoints end),
	cc_00_course_grade_category      = max(case when ccc.CourseLevel = 'Y' then ccc.MarkCategory end),
	cc_00_course_success_ind         = max(case when ccc.CourseLevel = 'Y' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: A
	cc_01_college_id                 = max(case when ccc.CourseLevel = 'A' then ccc.CollegeId end),
	cc_01_course_level               = max(case when ccc.CourseLevel = 'A' then ccc.CourseLevel end),
	cc_01_year_term_code             = max(case when ccc.CourseLevel = 'A' then ccc.YearTermCode end),
	cc_01_course_id                  = max(case when ccc.CourseLevel = 'A' then ccc.CourseId end),
	cc_01_course_title               = max(case when ccc.CourseLevel = 'A' then ccc.CourseTitle end),
	cc_01_course_grade               = max(case when ccc.CourseLevel = 'A' then ccc.SectionMark end),
	cc_01_course_grade_points        = max(case when ccc.CourseLevel = 'A' then ccc.MarkPoints end),
	cc_01_course_grade_category      = max(case when ccc.CourseLevel = 'A' then ccc.MarkCategory end),
	cc_01_course_success_ind         = max(case when ccc.CourseLevel = 'A' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: B
	cc_02_college_id                 = max(case when ccc.CourseLevel = 'B' then ccc.CollegeId end),
	cc_02_course_level               = max(case when ccc.CourseLevel = 'B' then ccc.CourseLevel end),
	cc_02_year_term_code             = max(case when ccc.CourseLevel = 'B' then ccc.YearTermCode end),
	cc_02_course_id                  = max(case when ccc.CourseLevel = 'B' then ccc.CourseId end),
	cc_02_course_title               = max(case when ccc.CourseLevel = 'B' then ccc.CourseTitle end),
	cc_02_course_grade               = max(case when ccc.CourseLevel = 'B' then ccc.SectionMark end),
	cc_02_course_grade_points        = max(case when ccc.CourseLevel = 'B' then ccc.MarkPoints end),
	cc_02_course_grade_category      = max(case when ccc.CourseLevel = 'B' then ccc.MarkCategory end),
	cc_02_course_success_ind         = max(case when ccc.CourseLevel = 'B' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: C
	cc_03_college_id                 = max(case when ccc.CourseLevel = 'C' then ccc.CollegeId end),
	cc_03_course_level               = max(case when ccc.CourseLevel = 'C' then ccc.CourseLevel end),
	cc_03_year_term_code             = max(case when ccc.CourseLevel = 'C' then ccc.YearTermCode end),
	cc_03_course_id                  = max(case when ccc.CourseLevel = 'C' then ccc.CourseId end),
	cc_03_course_title               = max(case when ccc.CourseLevel = 'C' then ccc.CourseTitle end),
	cc_03_course_grade               = max(case when ccc.CourseLevel = 'C' then ccc.SectionMark end),
	cc_03_course_grade_points        = max(case when ccc.CourseLevel = 'C' then ccc.MarkPoints end),
	cc_03_course_grade_category      = max(case when ccc.CourseLevel = 'C' then ccc.MarkCategory end),
	cc_03_course_success_ind         = max(case when ccc.CourseLevel = 'C' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: D
	cc_04_college_id                 = max(case when ccc.CourseLevel = 'D' then ccc.CollegeId end),
	cc_04_course_level               = max(case when ccc.CourseLevel = 'D' then ccc.CourseLevel end),
	cc_04_year_term_code             = max(case when ccc.CourseLevel = 'D' then ccc.YearTermCode end),
	cc_04_course_id                  = max(case when ccc.CourseLevel = 'D' then ccc.CourseId end),
	cc_04_course_title               = max(case when ccc.CourseLevel = 'D' then ccc.CourseTitle end),
	cc_04_course_grade               = max(case when ccc.CourseLevel = 'D' then ccc.SectionMark end),
	cc_04_course_grade_points        = max(case when ccc.CourseLevel = 'D' then ccc.MarkPoints end),
	cc_04_course_grade_category      = max(case when ccc.CourseLevel = 'D' then ccc.MarkCategory end),
	cc_04_course_success_ind         = max(case when ccc.CourseLevel = 'D' then convert(tinyint, ccc.MarkIsSuccess) end)
INTO
	mmap.CapMmapEnglishGrade
FROM
	(
		SELECT
			cc.CohortYear,
			cc.TopCode,
			cc.InterSegmentKey,
			OverallCumulativeGradePointAverage = max(case when DepartmentCode = '00' then CumulativeGradePointAverage end),
			SubjectCumulativeGradePointAverage = max(case when DepartmentCode = '14' then CumulativeGradePointAverage end),
			WithoutSubjectOverallCumulativeGradePointAverage = 
				convert(
					decimal(9,3),
					isnull(
						case
							when max(case when DepartmentCode = '00' then CumulativeQualityPoints end) - max(case when DepartmentCode = '14' then CumulativeQualityPoints end) <= 0 then 0
							else max(case when DepartmentCode = '00' then CumulativeQualityPoints end) - max(case when DepartmentCode = '14' then CumulativeQualityPoints end)
						end
						/
						nullif(
							case
								when max(case when DepartmentCode = '00' then CumulativeCreditAttempted end) - max(case when DepartmentCode = '14' then CumulativeCreditAttempted end) <= 0 then 0
								else max(case when DepartmentCode = '00' then CumulativeCreditAttempted end) - max(case when DepartmentCode = '14' then CumulativeCreditAttempted end)
							end,
							0
						)
					,0)
				)
		FROM
			mmap.CapCohort cc
			inner join
			mmap.RetrospectivePerformance rp
				on  cc.InterSegmentKey = rp.InterSegmentKey
		WHERE
			cc.TopCode = '150100'
			and rp.DepartmentCode in ('00', '14')
			and rp.IsLast = 1
		GROUP BY
			cc.CohortYear,
			cc.TopCode,
			cc.InterSegmentKey
	) b
	inner join
	mmap.RetrospectiveCourseContent rcc
		on rcc.InterSegmentKey = b.InterSegmentKey
	inner join
	mmap.CollegeCourseContent ccc
		on ccc.InterSegmentKey = b.InterSegmentKey
		and ccc.ToPCode = b.TopCode
	inner join
	dbo.Student s
		on s.InterSegmentKey = b.InterSegmentKey
WHERE
	rcc.DepartmentCode = '14'
	and rcc.GradeSelector = 1
	and ccc.LevelSelector = 1
	and ccc.CollegeCounter = 1
	and s.IsGrade11SectionInclusive = 1
	and s.IsHighSchoolCollision = 0
GROUP BY
	CohortYear,
	b.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage;

DELETE FROM mmap.CapMmapEnglishGrade WHERE OverallCumulativeGradePointAverage > 4;
DELETE FROM mmap.CapMmapEnglishGrade WHERE SubjectCumulativeGradePointAverage > 4;
DELETE FROM mmap.CapMmapEnglishGrade WHERE WithoutSubjectOverallCumulativeGradePointAverage > 4;

ALTER TABLE
	mmap.CapMmapEnglishGrade
ADD CONSTRAINT
	PK_CapMmapEnglishGrade
PRIMARY KEY CLUSTERED
	(
			CohortYear,
			InterSegmentKey
	);

ALTER TABLE
	mmap.CapMmapEnglishGrade
ADD CONSTRAINT
	UQ_CapMmapEnglishGrade__InterSegmentKey
UNIQUE
	(
		InterSegmentKey
	);

GO

ALTER TABLE
	mmap.CapMmapEnglishGrade
ADD
	IpedsRace char(1);

ALTER TABLE
	mmap.CapMmapEnglishGrade
ADD
	Gender char(1);

ALTER TABLE
	mmap.CapMmapEnglishGrade
ADD
	IsDsps char(1);

ALTER TABLE
	mmap.CapMmapEnglishGrade
ADD
	IsEops char(1);

GO

OPEN SYMMETRIC KEY
  SecPii
DECRYPTION BY CERTIFICATE
  SecPii;

UPDATE
	t
SET
	t.IpedsRace = d.IpedsRace,
	t.Gender    = d.Gender
FROM
	mmap.CapMmapEnglishGrade t
	inner join
	(
		SELECT
			id.InterSegmentKey,
			IpedsRace = convert(char(1), DecryptByKey(st.ipeds_race_enc)),
			Gender    = convert(char(1), DecryptByKey(st.gender_enc))
		FROM
			comis.stterm st
			inner join
			comis.studntid id
				on id.college_id = st.college_id
				and id.student_id = st.student_id
			inner join
			mmap.CapMmapEnglishGrade a
				on a.InterSegmentKey = id.InterSegmentKey
		WHERE
			st.term_id = (
				SELECT
					min(st1.term_id)
				FROM
					comis.stterm st1
				WHERE
					st1.college_id = st.college_id
					and st1.student_id = st.student_id
			)
	) d
		on d.InterSegmentKey = t.InterSegmentKey;

CLOSE SYMMETRIC KEY SecPii;

UPDATE
	t
SET
	t.IsDsps = isnull(d.IsDsps, 0),
	t.IsEops = isnull(e.IsEops, 0)
FROM
	mmap.CapMmapEnglishGrade t
	left outer join
	(
		SELECT DISTINCT
			id.InterSegmentKey,
			IsDsps = 1
		FROM
			comis.sddsps sd
			inner join
			comis.studntid id
				on id.college_id = sd.college_id
				and id.student_id = sd.student_id
			inner join
			mmap.CapMmapEnglishGrade a
				on a.InterSegmentKey = id.InterSegmentKey
	) d
		on d.InterSegmentKey = t.InterSegmentKey
	left outer join
	(
		SELECT DISTINCT
			id.InterSegmentKey,
			IsEops = 1
		FROM
			comis.seeops se
			inner join
			comis.studntid id
				on id.college_id = se.college_id
				and id.student_id = se.student_id
			inner join
			mmap.CapMmapEnglishGrade a
				on a.InterSegmentKey = id.InterSegmentKey
	) e
		on e.InterSegmentKey = t.InterSegmentKey;