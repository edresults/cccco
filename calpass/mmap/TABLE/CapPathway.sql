USE calpass;

GO

IF (object_id('mmap.CapPathway') is not null)
	BEGIN
		DROP TABLE mmap.CapPathway;
	END;

GO

CREATE TABLE
	mmap.CapPathway
	(
		CollegeId char(3) not null,
		DepartmentName varchar(12) not null,
		CourseNumber varchar(12)not null,
		CourseId varchar(12) not null,
		PathwayName varchar(25) not null
	);

GO

ALTER TABLE
	mmap.CapPathway
ADD CONSTRAINT
	PK_CapPathway
PRIMARY KEY
	(
		CollegeId,
		CourseId
	);

GO

INSERT
	mmap.CapPathway
	(
		CollegeId,
		DepartmentName,
		CourseNumber,
		CourseId,
		PathwayName
	)
SELECT DISTINCT
	t.CollegeId,
	t.DepartmentName,
	t.CourseNumber,
	CourseId = cb.course_id,
	PathwayName = 'BusinessCalculus'
FROM
	(
		VALUES
		('021','MATH','178'),
		('022','MATH','178'),
		('031','MATH','170'),
		('051','MATH','115'),
		('061','MATH','130'),
		('071','MATH','121'),
		('072','MATH','121'),
		('073','MATH','121'),
		('091','MATH','120'),
		('111','MATH','13'),
		('121',null,null),
		('131',null,null),
		('141','MTH','230'),
		('161',null,null),
		('171','MATH','9'),
		('181',null,null),
		('221','MAT','118'),
		('231','MATH','3404073'),
		('232','MATH','3414074'),
		('233','MATH','3404075'),
		('234','MAT','34122712'),
		('241','MATH','115'),
		('261','MATH','16'),
		('271','MATH','042'),
		('281',null,null),
		('291','MATH','9'),
		('292','MATH','9'),
		('311','MATH','180'),
		('312','MATH','182'),
		('313','MATH','037'),
		('334','MATH','121'),
		('341','MATH','016A'),
		('343','MATH','016A'),
		('344','MATH','016A'),
		('345','MATH','016A'),
		('361','MATH','75'),
		('361','MATH','100A'),
		('371','MATH','241'),
		('372','MATH','241'),
		('373','MATH','241'),
		('373','BUS','120'),
		('411','MATH','18'),
		('421','MATH','012'),
		('422','MATH','012'),
		('431','MATH','167'),
		('441','MATH','6'),
		('451','MAT','2'),
		('461','MATH','18'),
		('471','MATH','062'),
		('472','MATH','062'),
		('481','MATH','34'),
		('481','MATH','34'),
		('482','MATH','32'),
		('482','MATH','34'),
		('482','MTH','32'),
		('492','MATH','012'),
		('493','MATH','012'),
		('521','MATH','B2'),
		('522','MATH','C131'),
		('523','BSAD','P150'),
		('531',null,null),
		('551','MATH','013'),
		('561',null,null),
		('571',null,null),
		('572','BA','39'),
		('576',null,null),
		('581',null,null),
		('582',null,null),
		('591',null,null),
		('592','MATH','138'),
		('611','MATH','135'),
		('621','MATH','148'),
		('641','MATH','255'),
		('651','MATH','130'),
		('661','MATH','240'),
		('681','MATH','16A'),
		('682','MATH','R106'),
		('683','MATH','V46'),
		('691',null,null),
		('711',null,null),
		('721','MATH','165'),
		('731','MATH','112'),
		('741','MATH','236'),
		('742','MATH','236'),
		('743','MATH','238'),
		('744','MATH','238'),
		('745','MATH','236'),
		('746','MATH','236'),
		('747','MATH','238'),
		('748','MATH','236'),
		('749','MATH','236'),
		('771','BUS','14B'),
		('781','MATH','28'),
		('811','MATH','116'),
		('821','MATH','162'),
		('831','MATH','C140'),
		('832','MATH','G140'),
		('833','MATH','A140'),
		('841','MATH','47'),
		('851','MATH','140'),
		('861','MATH','130C'),
		('862','MATH','130F'),
		('871','MATH','150'),
		('873','MATH','150'),
		('881','MATH','170'),
		('891','MATH','11'),
		('892','MATH','11'),
		('911',null,null),
		('921','MATH','60'),
		('931','MATH','009'),
		('941','MATH','135'),
		('951',null,null),
		('961','MAT','5'),
		('962',null,null),
		('963','MAT','5'),
		('971',null,null),
		('981','MATH','141'),
		('982',null,null),
		('991',null,null)
	) t
	(
		CollegeId,
		DepartmentName,
		CourseNumber
	)
	inner join
	comis.cbcrsinv cb
		on cb.college_id = t.CollegeId
		and t.DepartmentName +  t.CourseNumber = replace(replace(replace(cb.course_id, ' ', ''), '-', ''), '.', '')
ORDER BY
	CollegeId asc;


INSERT
	mmap.CapPathway
	(
		CollegeId,
		DepartmentName,
		CourseNumber,
		CourseId,
		PathwayName
	)
SELECT DISTINCT
	t.CollegeId,
	t.DepartmentName,
	t.CourseNumber,
	CourseId = cb.course_id,
	PathwayName = 'Teaching'
FROM
	(
		VALUES
		('021','MATH','125'),
		('022','MATH','125'),
		('031','MATH','110'),
		('051','MATH','105'),
		('061','MATH','105'),
		('071','MATH','210A'),
		('072','MATH','215'),
		('091','MATH','110'),
		('111','MATH','4'),
		('121','MATH','150'),
		('131','MATH','11A'),
		('141','MTH','120'),
		('171','MATH','41A'),
		('221','MAT','109'),
		('231','MAT','31127569'),
		('241','MATH','130'), -- could not find in comis.cbcrsinv
		('271','MATH','0019'),
		('291','MATH','15'),
		('312','MATH','125'),
		('313','MATH','120'),
		('345','MATH','018'),
		('371','MATH','150'),
		('373','MATH','150'),
		('411','MATH','15'),
		('421','MATH','46'), -- could not find in comis.cbcrsinv
		('422','MATH','42'), -- could not find in comis.cbcrsinv
		('441','MATH','12'),
		('451','MAT','12'),
		('461','MATH','12'),
		('471','MATH','052'),
		('472','MATH','052'),
		('482','MTH','41'),
		('493','MATH','014'),
		('521','MATH','B4A'),
		('523','MATH','P115'), -- could not find in comis.cbcrsinv
		('531','MATH','20A'),
		('551','MATH','017A'),
		('561','MATH','010'),
		('571','MATH','10A'),
		('572','MATH','10A'),
		('576','MATH','10A'),
		('581','MATH','10A'),
		('582','MATH','10A'),
		('591','MATH','4A'),
		('591','MATH','4B'),
		('591','CMATH','4A'),
		('591','CMATH','4B'),
		('592','MATH','105'),
		('611','MATH','105'),
		('621','MATH','120'),
		('651','MATH','108'),
		('661','MATH','130'),
		('681','MATH','M10'),
		('682','MATH','R102'),
		('683','MATH','V38'),
		('721','MATH','110'),
		('731','MATH','138'),
		('741','MATH','215'),
		('742','MATH','215'),
		('743','MATH','215'),
		('744','MATH','215'),
		('745','MATH','215'),
		('747','MATH','215'),
		('748','MATH','215'),
		('749','MATH','215'),
		('771','MATH','038'),
		('781','MATH','41'),
		('811','MATH','110A'),
		('821','MATH','168'),
		('831','MATH','C104'),
		('832','MATH','G104'),
		('833','MATH','A104'),
		('841','MATH','28'),
		('861','MATH','110C'),
		('862','MATH','203F'),
		('871','MATH','204'),
		('873','MATH','203'),
		('881','MATH','140'),
		('891','MATH','112'),
		('892','MATH','20'),
		('921','MATH','4'),
		('931','MATH','011'),
		('961','Mat','26'), -- could not find in comis.cbcrsinv
		('962','MAT','26'), -- could not find in comis.cbcrsinv
		('971','MATH','016'),
		('981','MATH','106')
	) t
	(
		CollegeId,
		DepartmentName,
		CourseNumber
	)
	inner join
	comis.cbcrsinv cb
		on cb.college_id = t.CollegeId
		and t.DepartmentName +  t.CourseNumber = replace(replace(replace(cb.course_id, ' ', ''), '-', ''), '.', '')
ORDER BY
	CollegeId asc;