CREATE TABLE
	Mmap.Levels
	(
		Id      TINYINT     IDENTITY,
		Code    CHAR(1)     NOT NULL,
		Ordinal CHAR(2)     NOT NULL,
		Label   VARCHAR(30) NOT NULL,
		-- PK
		CONSTRAINT PK_Levels PRIMARY KEY CLUSTERED ( Id ),
		-- AK
		CONSTRAINT AK_Levels_Code  UNIQUE ( Code ),
		CONSTRAINT AK_Levels_Rank  UNIQUE ( Ordinal ),
		CONSTRAINT AK_Levels_Label UNIQUE ( Label )
	);

GO

INSERT INTO
	Mmap.Levels
	(
		Code,
		Ordinal,
		Label
	)
VALUES
	('Y',0,'Not Applicable'),
	('A',1,'One level below transfer'),
	('B',2,'Two levels below transfer'),
	('C',3,'Three levels below transfer'),
	('D',4,'Four levels below transfer'),
	('E',5,'Five levels below transfer'),
	('F',6,'Six levels below transfer'),
	('G',7,'Seven levels below transfer'),
	('H',8,'Eight levels below transfer');