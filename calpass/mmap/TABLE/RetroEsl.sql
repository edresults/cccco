DROP TABLE IF EXISTS mmap.RetroEsl;

GO

CREATE TABLE
    Mmap.RetroEsl
    (
        InterSegmentKey                              BINARY(64)   NOT NULL PRIMARY KEY,
        HS09GradeCode                                CHAR(2)          NULL,
        HS09OverallGradePointAverage                 DECIMAL(3,2)     NULL,
        HS09OverallCumulativeGradePointAverage       DECIMAL(3,2)     NULL,
        HS09EnglishGradePointAverage                 DECIMAL(3,2)     NULL,
        HS09EnglishCumulativeGradePointAverage       DECIMAL(3,2)     NULL,
        HS09SchoolCode                               CHAR(14)         NULL,
        HS09YearTermCode                             INTEGER          NULL,
        HS09CourseCode                               CHAR(4)          NULL,
        HS09CourseContentRank                        TINYINT          NULL,
        HS09CourseTitle                              VARCHAR(40)      NULL,
        HS09CourseMarkLetter                         VARCHAR(3)       NULL,
        HS09CourseMarkPoints                         DECIMAL(2,1)     NULL,
        HS09CourseUniversityAdmissionRequirementCode VARCHAR(2)       NULL,
        HS09CourseLevelCode                          CHAR(2)          NULL,
        HS09CourseTypeCode                           CHAR(2)          NULL,
        HS10GradeCode                                CHAR(2)          NULL,
        HS10OverallGradePointAverage                 DECIMAL(3,2)     NULL,
        HS10OverallCumulativeGradePointAverage       DECIMAL(3,2)     NULL,
        HS10EnglishGradePointAverage                 DECIMAL(3,2)     NULL,
        HS10EnglishCumulativeGradePointAverage       DECIMAL(3,2)     NULL,
        HS10SchoolCode                               CHAR(14)         NULL,
        HS10YearTermCode                             INTEGER          NULL,
        HS10CourseCode                               CHAR(4)          NULL,
        HS10CourseContentRank                        TINYINT          NULL,
        HS10CourseTitle                              VARCHAR(40)      NULL,
        HS10CourseMarkLetter                         VARCHAR(3)       NULL,
        HS10CourseMarkPoints                         DECIMAL(2,1)     NULL,
        HS10CourseUniversityAdmissionRequirementCode VARCHAR(2)       NULL,
        HS10CourseLevelCode                          CHAR(2)          NULL,
        HS10CourseTypeCode                           CHAR(2)          NULL,
        HS11GradeCode                                CHAR(2)          NULL,
        HS11OverallGradePointAverage                 DECIMAL(3,2)     NULL,
        HS11OverallCumulativeGradePointAverage       DECIMAL(3,2)     NULL,
        HS11EnglishGradePointAverage                 DECIMAL(3,2)     NULL,
        HS11EnglishCumulativeGradePointAverage       DECIMAL(3,2)     NULL,
        HS11SchoolCode                               CHAR(14)         NULL,
        HS11YearTermCode                             INTEGER          NULL,
        HS11CourseCode                               CHAR(4)          NULL,
        HS11CourseContentRank                        TINYINT          NULL,
        HS11CourseTitle                              VARCHAR(40)      NULL,
        HS11CourseMarkLetter                         VARCHAR(3)       NULL,
        HS11CourseMarkPoints                         DECIMAL(2,1)     NULL,
        HS11CourseUniversityAdmissionRequirementCode VARCHAR(2)       NULL,
        HS11CourseLevelCode                          CHAR(2)          NULL,
        HS11CourseTypeCode                           CHAR(2)          NULL,
        HS12GradeCode                                CHAR(2)          NULL,
        HS12OverallGradePointAverage                 DECIMAL(3,2)     NULL,
        HS12OverallCumulativeGradePointAverage       DECIMAL(3,2)     NULL,
        HS12EnglishGradePointAverage                 DECIMAL(3,2)     NULL,
        HS12EnglishCumulativeGradePointAverage       DECIMAL(3,2)     NULL,
        HS12SchoolCode                               CHAR(14)         NULL,
        HS12YearTermCode                             INTEGER          NULL,
        HS12CourseCode                               CHAR(4)          NULL,
        HS12CourseContentRank                        TINYINT          NULL,
        HS12CourseTitle                              VARCHAR(40)      NULL,
        HS12CourseMarkLetter                         VARCHAR(3)       NULL,
        HS12CourseMarkPoints                         DECIMAL(2,1)     NULL,
        HS12CourseUniversityAdmissionRequirementCode VARCHAR(2)       NULL,
        HS12CourseLevelCode                          CHAR(2)          NULL,
        HS12CourseTypeCode                           CHAR(2)          NULL,
        HSLGGradeCode                                CHAR(2)          NULL,
        HSLGOverallGradePointAverage                 DECIMAL(3,2)     NULL,
        HSLGOverallCumulativeGradePointAverage       DECIMAL(3,2)     NULL,
        HSLGEnglishGradePointAverage                 DECIMAL(3,2)     NULL,
        HSLGEnglishCumulativeGradePointAverage       DECIMAL(3,2)     NULL,
        HSLGSchoolCode                               CHAR(14)         NULL,
        HSLGYearTermCode                             INTEGER          NULL,
        HSLGCourseCode                               CHAR(4)          NULL,
        HSLGCourseContentRank                        TINYINT          NULL,
        HSLGCourseTitle                              VARCHAR(40)      NULL,
        HSLGCourseMarkLetter                         VARCHAR(3)       NULL,
        HSLGCourseMarkPoints                         DECIMAL(2,1)     NULL,
        HSLGCourseUniversityAdmissionRequirementCode VARCHAR(2)       NULL,
        HSLGCourseLevelCode                          CHAR(2)          NULL,
        HSLGCourseTypeCode                           CHAR(2)          NULL,
        CCEACNFLCollegeCode                          CHAR(3)          NULL,
        CCEACNFLYearTermCode                         INTEGER          NULL,
        CCEACNFLCourseId                             VARCHAR(12)      NULL,
        CCEACNFLSectionId                            VARCHAR(6)       NULL,
        CCEACNFLCourseControlNumber                  CHAR(12)         NULL,
        CCEACNFLCourseTopCode                        CHAR(6)          NULL,
        CCEACNFLCourseTitle                          VARCHAR(68)      NULL,
        CCEACNFLCourseMarkLetter                     VARCHAR(3)       NULL,
        CCEACNFLCourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCEACNFLCourseLevelCode                      CHAR(1)          NULL,
        CCEACNFLCourseCreditCode                     CHAR(2)          NULL,
        CCEACNFLSummary                              CHAR(6)          NULL,
        CCEACNFLEslUnits                             SMALLINT         NULL,
        CCEACNFLEnglUnits                            SMALLINT         NULL,
        CCEACN00CollegeCode                          CHAR(3)          NULL,
        CCEACN00YearTermCode                         INTEGER          NULL,
        CCEACN00CourseId                             VARCHAR(12)      NULL,
        CCEACN00SectionId                            VARCHAR(6)       NULL,
        CCEACN00CourseControlNumber                  CHAR(12)         NULL,
        CCEACN00CourseTopCode                        CHAR(6)          NULL,
        CCEACN00CourseTitle                          VARCHAR(68)      NULL,
        CCEACN00CourseMarkLetter                     VARCHAR(3)       NULL,
        CCEACN00CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCEACN00CourseLevelCode                      CHAR(1)          NULL,
        CCEACN00CourseCreditCode                     CHAR(2)          NULL,
        CCEACN00Summary                              CHAR(6)          NULL,
        CCEACN00EslUnits                             SMALLINT         NULL,
        CCEACN00EnglUnits                            SMALLINT         NULL,
        CCEACN01CollegeCode                          CHAR(3)          NULL,
        CCEACN01YearTermCode                         INTEGER          NULL,
        CCEACN01CourseId                             VARCHAR(12)      NULL,
        CCEACN01SectionId                            VARCHAR(6)       NULL,
        CCEACN01CourseControlNumber                  CHAR(12)         NULL,
        CCEACN01CourseTopCode                        CHAR(6)          NULL,
        CCEACN01CourseTitle                          VARCHAR(68)      NULL,
        CCEACN01CourseMarkLetter                     VARCHAR(3)       NULL,
        CCEACN01CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCEACN01CourseLevelCode                      CHAR(1)          NULL,
        CCEACN01CourseCreditCode                     CHAR(2)          NULL,
        CCEACN01Summary                              CHAR(6)          NULL,
        CCEACN01EslUnits                             SMALLINT         NULL,
        CCEACN01EnglUnits                            SMALLINT         NULL,
        CCEACN02CollegeCode                          CHAR(3)          NULL,
        CCEACN02YearTermCode                         INTEGER          NULL,
        CCEACN02CourseId                             VARCHAR(12)      NULL,
        CCEACN02SectionId                            VARCHAR(6)       NULL,
        CCEACN02CourseControlNumber                  CHAR(12)         NULL,
        CCEACN02CourseTopCode                        CHAR(6)          NULL,
        CCEACN02CourseTitle                          VARCHAR(68)      NULL,
        CCEACN02CourseMarkLetter                     VARCHAR(3)       NULL,
        CCEACN02CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCEACN02CourseLevelCode                      CHAR(1)          NULL,
        CCEACN02CourseCreditCode                     CHAR(2)          NULL,
        CCEACN02Summary                              CHAR(6)          NULL,
        CCEACN02EslUnits                             SMALLINT         NULL,
        CCEACN02EnglUnits                            SMALLINT         NULL,
        CCEACN03CollegeCode                          CHAR(3)          NULL,
        CCEACN03YearTermCode                         INTEGER          NULL,
        CCEACN03CourseId                             VARCHAR(12)      NULL,
        CCEACN03SectionId                            VARCHAR(6)       NULL,
        CCEACN03CourseControlNumber                  CHAR(12)         NULL,
        CCEACN03CourseTopCode                        CHAR(6)          NULL,
        CCEACN03CourseTitle                          VARCHAR(68)      NULL,
        CCEACN03CourseMarkLetter                     VARCHAR(3)       NULL,
        CCEACN03CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCEACN03CourseLevelCode                      CHAR(1)          NULL,
        CCEACN03CourseCreditCode                     CHAR(2)          NULL,
        CCEACN03Summary                              CHAR(6)          NULL,
        CCEACN03EslUnits                             SMALLINT         NULL,
        CCEACN03EnglUnits                            SMALLINT         NULL,
        CCEACN04CollegeCode                          CHAR(3)          NULL,
        CCEACN04YearTermCode                         INTEGER          NULL,
        CCEACN04CourseId                             VARCHAR(12)      NULL,
        CCEACN04SectionId                            VARCHAR(6)       NULL,
        CCEACN04CourseControlNumber                  CHAR(12)         NULL,
        CCEACN04CourseTopCode                        CHAR(6)          NULL,
        CCEACN04CourseTitle                          VARCHAR(68)      NULL,
        CCEACN04CourseMarkLetter                     VARCHAR(3)       NULL,
        CCEACN04CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCEACN04CourseLevelCode                      CHAR(1)          NULL,
        CCEACN04CourseCreditCode                     CHAR(2)          NULL,
        CCEACN04Summary                              CHAR(6)          NULL,
        CCEACN04EslUnits                             SMALLINT         NULL,
        CCEACN04EnglUnits                            SMALLINT         NULL,
        CCEACN05CollegeCode                          CHAR(3)          NULL,
        CCEACN05YearTermCode                         INTEGER          NULL,
        CCEACN05CourseId                             VARCHAR(12)      NULL,
        CCEACN05SectionId                            VARCHAR(6)       NULL,
        CCEACN05CourseControlNumber                  CHAR(12)         NULL,
        CCEACN05CourseTopCode                        CHAR(6)          NULL,
        CCEACN05CourseTitle                          VARCHAR(68)      NULL,
        CCEACN05CourseMarkLetter                     VARCHAR(3)       NULL,
        CCEACN05CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCEACN05CourseLevelCode                      CHAR(1)          NULL,
        CCEACN05CourseCreditCode                     CHAR(2)          NULL,
        CCEACN05Summary                              CHAR(6)          NULL,
        CCEACN05EslUnits                             SMALLINT         NULL,
        CCEACN05EnglUnits                            SMALLINT         NULL,
        CCEACN06CollegeCode                          CHAR(3)          NULL,
        CCEACN06YearTermCode                         INTEGER          NULL,
        CCEACN06CourseId                             VARCHAR(12)      NULL,
        CCEACN06SectionId                            VARCHAR(6)       NULL,
        CCEACN06CourseControlNumber                  CHAR(12)         NULL,
        CCEACN06CourseTopCode                        CHAR(6)          NULL,
        CCEACN06CourseTitle                          VARCHAR(68)      NULL,
        CCEACN06CourseMarkLetter                     VARCHAR(3)       NULL,
        CCEACN06CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCEACN06CourseLevelCode                      CHAR(1)          NULL,
        CCEACN06CourseCreditCode                     CHAR(2)          NULL,
        CCEACN06Summary                              CHAR(6)          NULL,
        CCEACN06EslUnits                             SMALLINT         NULL,
        CCEACN06EnglUnits                            SMALLINT         NULL,
        CCEACN07CollegeCode                          CHAR(3)          NULL,
        CCEACN07YearTermCode                         INTEGER          NULL,
        CCEACN07CourseId                             VARCHAR(12)      NULL,
        CCEACN07SectionId                            VARCHAR(6)       NULL,
        CCEACN07CourseControlNumber                  CHAR(12)         NULL,
        CCEACN07CourseTopCode                        CHAR(6)          NULL,
        CCEACN07CourseTitle                          VARCHAR(68)      NULL,
        CCEACN07CourseMarkLetter                     VARCHAR(3)       NULL,
        CCEACN07CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCEACN07CourseLevelCode                      CHAR(1)          NULL,
        CCEACN07CourseCreditCode                     CHAR(2)          NULL,
        CCEACN07Summary                              CHAR(6)          NULL,
        CCEACN07EslUnits                             SMALLINT         NULL,
        CCEACN07EnglUnits                            SMALLINT         NULL,
        CCEACN08CollegeCode                          CHAR(3)          NULL,
        CCEACN08YearTermCode                         INTEGER          NULL,
        CCEACN08CourseId                             VARCHAR(12)      NULL,
        CCEACN08SectionId                            VARCHAR(6)       NULL,
        CCEACN08CourseControlNumber                  CHAR(12)         NULL,
        CCEACN08CourseTopCode                        CHAR(6)          NULL,
        CCEACN08CourseTitle                          VARCHAR(68)      NULL,
        CCEACN08CourseMarkLetter                     VARCHAR(3)       NULL,
        CCEACN08CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCEACN08CourseLevelCode                      CHAR(1)          NULL,
        CCEACN08CourseCreditCode                     CHAR(2)          NULL,
        CCEACN08Summary                              CHAR(6)          NULL,
        CCEACN08EslUnits                             SMALLINT         NULL,
        CCEACN08EnglUnits                            SMALLINT         NULL,
        CCENCRFLCollegeCode                          CHAR(3)          NULL,
        CCENCRFLYearTermCode                         INTEGER          NULL,
        CCENCRFLCourseId                             VARCHAR(12)      NULL,
        CCENCNFLSectionId                            VARCHAR(6)       NULL,
        CCENCRFLCourseControlNumber                  CHAR(12)         NULL,
        CCENCRFLCourseTopCode                        CHAR(6)          NULL,
        CCENCRFLCourseTitle                          VARCHAR(68)      NULL,
        CCENCRFLCourseMarkLetter                     VARCHAR(3)       NULL,
        CCENCRFLCourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCENCRFLCourseLevelCode                      CHAR(1)          NULL,
        CCENCRFLUnitsAttempt                         DECIMAL(4,2)     NULL,
        CCENCRFLUnitsSuccess                         DECIMAL(4,2)     NULL,
        CCENCR00CollegeCode                          CHAR(3)          NULL,
        CCENCR00YearTermCode                         INTEGER          NULL,
        CCENCR00CourseId                             VARCHAR(12)      NULL,
        CCENCR00SectionId                            VARCHAR(6)       NULL,
        CCENCR00CourseControlNumber                  CHAR(12)         NULL,
        CCENCR00CourseTopCode                        CHAR(6)          NULL,
        CCENCR00CourseTitle                          VARCHAR(68)      NULL,
        CCENCR00CourseMarkLetter                     VARCHAR(3)       NULL,
        CCENCR00CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCENCR00CourseLevelCode                      CHAR(1)          NULL,
        CCENCR00CB25                                 char(1)          NULL,
        CCENCR00CB26                                 char(1)          NULL,
        CCENCR00TleComposition                       tinyint          NULL,
        CCENCR00IsEngl                               BIT              NULL,
        CCENCR002NCollegeCode                        CHAR(3)          NULL,
        CCENCR002NYearTermCode                       INTEGER          NULL,
        CCENCR002NCourseId                           VARCHAR(12)      NULL,
        CCENCR002NSectionId                          VARCHAR(6)       NULL,
        CCENCR002NCourseControlNumber                CHAR(12)         NULL,
        CCENCR002NCourseTopCode                      CHAR(6)          NULL,
        CCENCR002NCourseTitle                        VARCHAR(68)      NULL,
        CCENCR002NCourseMarkLetter                   VARCHAR(3)       NULL,
        CCENCR002NCourseMarkPoints                   DECIMAL(4,2)     NULL,
        CCENCR002NCourseLevelCode                    CHAR(1)          NULL,
        CCENCR002NCB25                               char(1)          NULL,
        CCENCR002NCB26                               char(1)          NULL,
        CCENCR002NTleComposition                     tinyint          NULL,
        CCENCR002NIsEngl                             BIT              NULL,
        CCENCR00RQCollegeCode                        CHAR(3)          NULL,
        CCENCR00RQYearTermCode                       INTEGER          NULL,
        CCENCR00RQCourseId                           VARCHAR(12)      NULL,
        CCENCR00RQSectionId                          VARCHAR(6)       NULL,
        CCENCR00RQCourseControlNumber                CHAR(12)         NULL,
        CCENCR00RQCourseTopCode                      CHAR(6)          NULL,
        CCENCR00RQCourseTitle                        VARCHAR(68)      NULL,
        CCENCR00RQCourseMarkLetter                   VARCHAR(3)       NULL,
        CCENCR00RQCourseMarkPoints                   DECIMAL(4,2)     NULL,
        CCENCR00RQCourseLevelCode                    CHAR(1)          NULL,
        CCENCR00RQCB25                               CHAR(1)          NULL,
        CCENCR00RQCB26                               CHAR(1)          NULL,
        CCENCR00RQCreditType                         CHAR(1)          NULL,
        CCENCR00RQAttendHours                        DECIMAL(5,1)     NULL,
        CCENCR002NRQCollegeCode                      CHAR(3)          NULL,
        CCENCR002NRQYearTermCode                     INTEGER          NULL,
        CCENCR002NRQCourseId                         VARCHAR(12)      NULL,
        CCENCR002NRQSectionId                        VARCHAR(6)       NULL,
        CCENCR002NRQCourseControlNumber              CHAR(12)         NULL,
        CCENCR002NRQCourseTopCode                    CHAR(6)          NULL,
        CCENCR002NRQCourseTitle                      VARCHAR(68)      NULL,
        CCENCR002NRQCourseMarkLetter                 VARCHAR(3)       NULL,
        CCENCR002NRQCourseMarkPoints                 DECIMAL(4,2)     NULL,
        CCENCR002NRQCourseLevelCode                  CHAR(1)          NULL,
        CCENCR002NRQCB25                             CHAR(1)          NULL,
        CCENCR002NRQCB26                             CHAR(1)          NULL,
        CCENCR002NRQCreditType                       CHAR(1)          NULL,
        CCENCR002NRQAttendHours                      DECIMAL(5,1)     NULL,
        CCENCR01CollegeCode                          CHAR(3)          NULL,
        CCENCR01YearTermCode                         INTEGER          NULL,
        CCENCR01CourseId                             VARCHAR(12)      NULL,
        CCENCR01SectionId                            VARCHAR(6)       NULL,
        CCENCR01CourseControlNumber                  CHAR(12)         NULL,
        CCENCR01CourseTopCode                        CHAR(6)          NULL,
        CCENCR01CourseTitle                          VARCHAR(68)      NULL,
        CCENCR01CourseMarkLetter                     VARCHAR(3)       NULL,
        CCENCR01CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCENCR01CourseLevelCode                      CHAR(1)          NULL,
        CCENCR01CB25                                 char(1)          NULL,
        CCENCR01CB26                                 char(1)          NULL,
        CCENCR02CollegeCode                          CHAR(3)          NULL,
        CCENCR02YearTermCode                         INTEGER          NULL,
        CCENCR02CourseId                             VARCHAR(12)      NULL,
        CCENCR02SectionId                            VARCHAR(6)       NULL,
        CCENCR02CourseControlNumber                  CHAR(12)         NULL,
        CCENCR02CourseTopCode                        CHAR(6)          NULL,
        CCENCR02CourseTitle                          VARCHAR(68)      NULL,
        CCENCR02CourseMarkLetter                     VARCHAR(3)       NULL,
        CCENCR02CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCENCR02CourseLevelCode                      CHAR(1)          NULL,
        CCENCR02CB25                                 char(1)          NULL,
        CCENCR02CB26                                 char(1)          NULL,
        CCENCR03CollegeCode                          CHAR(3)          NULL,
        CCENCR03YearTermCode                         INTEGER          NULL,
        CCENCR03CourseId                             VARCHAR(12)      NULL,
        CCENCR03SectionId                            VARCHAR(6)       NULL,
        CCENCR03CourseControlNumber                  CHAR(12)         NULL,
        CCENCR03CourseTopCode                        CHAR(6)          NULL,
        CCENCR03CourseTitle                          VARCHAR(68)      NULL,
        CCENCR03CourseMarkLetter                     VARCHAR(3)       NULL,
        CCENCR03CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCENCR03CourseLevelCode                      CHAR(1)          NULL,
        CCENCR03CB25                                 char(1)          NULL,
        CCENCR03CB26                                 char(1)          NULL,
        CCENCR04CollegeCode                          CHAR(3)          NULL,
        CCENCR04YearTermCode                         INTEGER          NULL,
        CCENCR04CourseId                             VARCHAR(12)      NULL,
        CCENCR04SectionId                            VARCHAR(6)       NULL,
        CCENCR04CourseControlNumber                  CHAR(12)         NULL,
        CCENCR04CourseTopCode                        CHAR(6)          NULL,
        CCENCR04CourseTitle                          VARCHAR(68)      NULL,
        CCENCR04CourseMarkLetter                     VARCHAR(3)       NULL,
        CCENCR04CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCENCR04CourseLevelCode                      CHAR(1)          NULL,
        CCENCR04CB25                                 char(1)          NULL,
        CCENCR04CB26                                 char(1)          NULL,
        CCRDCRFLCollegeCode                          CHAR(3)          NULL,
        CCRDCRFLYearTermCode                         INTEGER          NULL,
        CCRDCRFLCourseId                             VARCHAR(12)      NULL,
        CCRDCRFLSectionId                            VARCHAR(6)       NULL,
        CCRDCRFLCourseControlNumber                  CHAR(12)         NULL,
        CCRDCRFLCourseTopCode                        CHAR(6)          NULL,
        CCRDCRFLCourseTitle                          VARCHAR(68)      NULL,
        CCRDCRFLCourseMarkLetter                     VARCHAR(3)       NULL,
        CCRDCRFLCourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCRDCRFLCourseLevelCode                      CHAR(1)          NULL,
        CCRDCR00CollegeCode                          CHAR(3)          NULL,
        CCRDCR00YearTermCode                         INTEGER          NULL,
        CCRDCR00CourseId                             VARCHAR(12)      NULL,
        CCRDCR00SectionId                            VARCHAR(6)       NULL,
        CCRDCR00CourseControlNumber                  CHAR(12)         NULL,
        CCRDCR00CourseTopCode                        CHAR(6)          NULL,
        CCRDCR00CourseTitle                          VARCHAR(68)      NULL,
        CCRDCR00CourseMarkLetter                     VARCHAR(3)       NULL,
        CCRDCR00CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCRDCR00CourseLevelCode                      CHAR(1)          NULL,
        CCRDCR01CollegeCode                          CHAR(3)          NULL,
        CCRDCR01YearTermCode                         INTEGER          NULL,
        CCRDCR01CourseId                             VARCHAR(12)      NULL,
        CCRDCR01SectionId                            VARCHAR(6)       NULL,
        CCRDCR01CourseControlNumber                  CHAR(12)         NULL,
        CCRDCR01CourseTopCode                        CHAR(6)          NULL,
        CCRDCR01CourseTitle                          VARCHAR(68)      NULL,
        CCRDCR01CourseMarkLetter                     VARCHAR(3)       NULL,
        CCRDCR01CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCRDCR01CourseLevelCode                      CHAR(1)          NULL,
        CCRDCR02CollegeCode                          CHAR(3)          NULL,
        CCRDCR02YearTermCode                         INTEGER          NULL,
        CCRDCR02CourseId                             VARCHAR(12)      NULL,
        CCRDCR02SectionId                            VARCHAR(6)       NULL,
        CCRDCR02CourseControlNumber                  CHAR(12)         NULL,
        CCRDCR02CourseTopCode                        CHAR(6)          NULL,
        CCRDCR02CourseTitle                          VARCHAR(68)      NULL,
        CCRDCR02CourseMarkLetter                     VARCHAR(3)       NULL,
        CCRDCR02CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCRDCR02CourseLevelCode                      CHAR(1)          NULL,
        CCRDCR03CollegeCode                          CHAR(3)          NULL,
        CCRDCR03YearTermCode                         INTEGER          NULL,
        CCRDCR03CourseId                             VARCHAR(12)      NULL,
        CCRDCR03SectionId                            VARCHAR(6)       NULL,
        CCRDCR03CourseControlNumber                  CHAR(12)         NULL,
        CCRDCR03CourseTopCode                        CHAR(6)          NULL,
        CCRDCR03CourseTitle                          VARCHAR(68)      NULL,
        CCRDCR03CourseMarkLetter                     VARCHAR(3)       NULL,
        CCRDCR03CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCRDCR03CourseLevelCode                      CHAR(1)          NULL,
        CCRDCR04CollegeCode                          CHAR(3)          NULL,
        CCRDCR04YearTermCode                         INTEGER          NULL,
        CCRDCR04CourseId                             VARCHAR(12)      NULL,
        CCRDCR04SectionId                            VARCHAR(6)       NULL,
        CCRDCR04CourseControlNumber                  CHAR(12)         NULL,
        CCRDCR04CourseTopCode                        CHAR(6)          NULL,
        CCRDCR04CourseTitle                          VARCHAR(68)      NULL,
        CCRDCR04CourseMarkLetter                     VARCHAR(3)       NULL,
        CCRDCR04CourseMarkPoints                     DECIMAL(4,2)     NULL,
        CCRDCR04CourseLevelCode                      CHAR(1)          NULL,
        STIsEnglishEap                               BIT              NULL,
        STEnglishScaledScore                         SMALLINT         NULL,
        STEnglishCluster1                            SMALLINT         NULL,
        STEnglishCluster2                            SMALLINT         NULL,
        STEnglishCluster3                            SMALLINT         NULL,
        STEnglishCluster4                            SMALLINT         NULL,
        STEnglishCluster5                            SMALLINT         NULL,
        STEnglishCluster6                            SMALLINT         NULL,
        STMathematicsSubject                         TINYINT          NULL,
        STIsMathematicsEap                           BIT              NULL,
        STMathematicsScaledScore                     SMALLINT         NULL,
        STMathematicsCluster1                        SMALLINT         NULL,
        STMathematicsCluster2                        SMALLINT         NULL,
        STMathematicsCluster3                        SMALLINT         NULL,
        STMathematicsCluster4                        SMALLINT         NULL,
        STMathematicsCluster5                        SMALLINT         NULL,
        STMathematicsCluster6                        SMALLINT         NULL,
        STIsEnglishOnly                              BIT              NULL,
        STIsEnglishLearner                           BIT              NULL,
        STIsFluentInitially                          BIT              NULL,
        STIsFluentReclassified                       BIT              NULL,
        HSIsRemedial                                 BIT              NULL,
        HSGraduationDate                             CHAR(8)          NULL,
        CCSubjAge                                    SMALLINT         NULL,
        CCGender                                     CHAR(1)          NULL,
        CCCitzenship                                 CHAR(1)          NULL,
        CCResidency                                  CHAR(5)          NULL,
        CCEducationStatus                            CHAR(5)          NULL,
        CCEnrollmentStatus                           CHAR(1)          NULL,
        CCEthnicities                                CHAR(21)         NULL,
        CCParentEducationLevel                       CHAR(21)         NULL,
        CCEthnicity                                  CHAR(1)          NULL,
        MilitaryStatus                               BIT              NULL,
        MilitaryDependentStatus                      BIT              NULL,
        FosterYouthStatus                            BIT              NULL,
        IncarceratedStatus                           BIT              NULL,
        MESAASEMStatus                               BIT              NULL,
        PuenteStatus                                 BIT              NULL,
        MCHSECHSStatus                               BIT              NULL,
        UMOJAStatus                                  BIT              NULL,
        CAAStatus                                    BIT              NULL,
        CAFYESStatus                                 BIT              NULL,
        BaccalaureateProgram                         CHAR(5)          NULL,
        CCAPStatus                                   BIT              NULL,
        EconomicallyDisadvStatus                     BIT              NULL,
        ExOffenderStatus                             BIT              NULL,
        HomelessStatus                               BIT              NULL,
        LongtermUnemployStatus                       BIT              NULL,
        CulturalBarrierStatus                        BIT              NULL,
        SeasonalFarmWorkStatus                       BIT              NULL,
        LiteracyStatus                               BIT              NULL,
        WorkBasedLearningStatus                      BIT              NULL,
        CCIsMultiCollege                             BIT              NULL,
        CCPrimaryDisability                          BIT              NULL,
        CCEducationalGoal                            CHAR(1)          NULL,
        CCInitialGoal                                CHAR(1)          NULL,
        CCMajor                                      CHAR(6)          NULL,
        CCAssessmentStatus                           CHAR(2)          NULL,
        CCAdvisementStatus                           CHAR(2)          NULL,
        CCEnrollmentLastYearTermCode                 CHAR(5)          NULL,
        CCTransferableUnitsAttempted                 DECIMAL(6,2)     NULL,
        CCTransferableUnitsEarned                    DECIMAL(6,2)     NULL,
        CCEopsCareStatus                             BIT              NULL,
        CCEopsEligibilityFactor                      CHAR(1)          NULL,
        CCEopsTermOfAcceptance                       CHAR(3)          NULL,
        APHSCountry                                  CHAR(2)          NULL,
        APUN01Country                                CHAR(2)          NULL,
        APUN01Award                                  CHAR(1)          NULL,
        APUN02Country                                CHAR(2)          NULL,
        APUN02Award                                  CHAR(1)          NULL,
        APUN03Country                                CHAR(2)          NULL,
        APUN03Award                                  CHAR(1)          NULL,
        APUN04Country                                CHAR(2)          NULL,
        APUN04Award                                  CHAR(1)          NULL,
        APVisaType                                   CHAR(2)          NULL,
        APAB540Waiver                                BIT              NULL,
        APComfortableEnglish                         BIT              NULL,
        APApplicationLanguage                        CHAR(2)          NULL,
        APEsl                                        BIT              NULL,
        APBasicSkills                                BIT              NULL,
        APEmploymentAssistance                       BIT              NULL,
        APSeasonalAG                                 BIT              NULL,
        APCountry                                    CHAR(2)          NULL,
        APPermanentCountry                           CHAR(2)          NULL,
        APPermanentCountryInternational              CHAR(2)          NULL,
        APCompletedEleventhGrade                     BIT              NULL,
        APCumulativeGradePointAverage                DECIMAL(3,2)     NULL,
        APEnglishCompletedCourseId                   TINYINT          NULL,
        APEnglishCompletedCourseGrade                VARCHAR(3)       NULL,
        APMathematicsCompletedCourseId               TINYINT          NULL,
        APMathematicsCompletedCourseGrade            VARCHAR(3)       NULL,
        APMathematicsPassedCourseId                  TINYINT          NULL,
        APMathematicsPassedCourseGrade               VARCHAR(3)       NULL,
        XferFice                                     CHAR(6)          NULL,
        XferDate                                     DATE             NULL,
        XfersSource                                  CHAR(4)          NULL,
        XferSchoolName                               VARCHAR(40)      NULL,
        XferSchoolType                               CHAR(1)          NULL,
        XferSector                                   CHAR(5)          NULL,
        XferState                                    CHAR(2)          NULL,
        XferSegment                                  VARCHAR(10)      NULL,
        ElaStatusRecent                              TINYINT          NULL,
        CCSP0101                                     CHAR(6)          NULL,
        CCSP0102                                     CHAR(1)          NULL,
        CCSP0103                                     CHAR(8)          NULL,
        CCSP0104                                     CHAR(5)          NULL,
        CCSP0201                                     CHAR(6)          NULL,
        CCSP0202                                     CHAR(1)          NULL,
        CCSP0203                                     CHAR(8)          NULL,
        CCSP0204                                     CHAR(5)          NULL,
        CCSP0301                                     CHAR(6)          NULL,
        CCSP0302                                     CHAR(1)          NULL,
        CCSP0303                                     CHAR(8)          NULL,
        CCSP0304                                     CHAR(5)          NULL
    );

-- CREATE TABLE
--     Mmap.RetroEsl
--     (
--         -- Record                                       XML COLUMN_SET FOR ALL_SPARSE_COLUMNS,
--         InterSegmentKey                              BINARY(64)   NOT NULL PRIMARY KEY,
--         HS09GradeCode                                CHAR(2)          NULL,      /* SPARSE, */
--         HS09OverallGradePointAverage                 DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS09OverallCumulativeGradePointAverage       DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS09EnglishGradePointAverage                 DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS09EnglishCumulativeGradePointAverage       DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS09SchoolCode                               CHAR(14)         NULL,      /* SPARSE, */
--         HS09YearTermCode                             INTEGER          NULL,      /* SPARSE, */
--         HS09CourseCode                               CHAR(4)          NULL,      /* SPARSE, */
--         HS09CourseContentRank                        TINYINT          NULL,      /* SPARSE, */
--         HS09CourseTitle                              VARCHAR(40)      NULL,      /* SPARSE, */
--         HS09CourseMarkLetter                         VARCHAR(3)       NULL,      /* SPARSE, */
--         HS09CourseMarkPoints                         DECIMAL(2,1)     NULL,      /* SPARSE, */
--         HS09CourseUniversityAdmissionRequirementCode VARCHAR(2)       NULL,      /* SPARSE, */
--         HS09CourseLevelCode                          CHAR(2)          NULL,      /* SPARSE, */
--         HS09CourseTypeCode                           CHAR(2)          NULL,      /* SPARSE, */
--         HS10GradeCode                                CHAR(2)          NULL,      /* SPARSE, */
--         HS10OverallGradePointAverage                 DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS10OverallCumulativeGradePointAverage       DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS10EnglishGradePointAverage                 DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS10EnglishCumulativeGradePointAverage       DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS10SchoolCode                               CHAR(14)         NULL,      /* SPARSE, */
--         HS10YearTermCode                             INTEGER          NULL,      /* SPARSE, */
--         HS10CourseCode                               CHAR(4)          NULL,      /* SPARSE, */
--         HS10CourseContentRank                        TINYINT          NULL,      /* SPARSE, */
--         HS10CourseTitle                              VARCHAR(40)      NULL,      /* SPARSE, */
--         HS10CourseMarkLetter                         VARCHAR(3)       NULL,      /* SPARSE, */
--         HS10CourseMarkPoints                         DECIMAL(2,1)     NULL,      /* SPARSE, */
--         HS10CourseUniversityAdmissionRequirementCode VARCHAR(2)       NULL,      /* SPARSE, */
--         HS10CourseLevelCode                          CHAR(2)          NULL,      /* SPARSE, */
--         HS10CourseTypeCode                           CHAR(2)          NULL,      /* SPARSE, */
--         HS11GradeCode                                CHAR(2)          NULL,      /* SPARSE, */
--         HS11OverallGradePointAverage                 DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS11OverallCumulativeGradePointAverage       DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS11EnglishGradePointAverage                 DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS11EnglishCumulativeGradePointAverage       DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS11SchoolCode                               CHAR(14)         NULL,      /* SPARSE, */
--         HS11YearTermCode                             INTEGER          NULL,      /* SPARSE, */
--         HS11CourseCode                               CHAR(4)          NULL,      /* SPARSE, */
--         HS11CourseContentRank                        TINYINT          NULL,      /* SPARSE, */
--         HS11CourseTitle                              VARCHAR(40)      NULL,      /* SPARSE, */
--         HS11CourseMarkLetter                         VARCHAR(3)       NULL,      /* SPARSE, */
--         HS11CourseMarkPoints                         DECIMAL(2,1)     NULL,      /* SPARSE, */
--         HS11CourseUniversityAdmissionRequirementCode VARCHAR(2)       NULL,      /* SPARSE, */
--         HS11CourseLevelCode                          CHAR(2)          NULL,      /* SPARSE, */
--         HS11CourseTypeCode                           CHAR(2)          NULL,      /* SPARSE, */
--         HS12GradeCode                                CHAR(2)          NULL,      /* SPARSE, */
--         HS12OverallGradePointAverage                 DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS12OverallCumulativeGradePointAverage       DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS12EnglishGradePointAverage                 DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS12EnglishCumulativeGradePointAverage       DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HS12SchoolCode                               CHAR(14)         NULL,      /* SPARSE, */
--         HS12YearTermCode                             INTEGER          NULL,      /* SPARSE, */
--         HS12CourseCode                               CHAR(4)          NULL,      /* SPARSE, */
--         HS12CourseContentRank                        TINYINT          NULL,      /* SPARSE, */
--         HS12CourseTitle                              VARCHAR(40)      NULL,      /* SPARSE, */
--         HS12CourseMarkLetter                         VARCHAR(3)       NULL,      /* SPARSE, */
--         HS12CourseMarkPoints                         DECIMAL(2,1)     NULL,      /* SPARSE, */
--         HS12CourseUniversityAdmissionRequirementCode VARCHAR(2)       NULL,      /* SPARSE, */
--         HS12CourseLevelCode                          CHAR(2)          NULL,      /* SPARSE, */
--         HS12CourseTypeCode                           CHAR(2)          NULL,      /* SPARSE, */
--         HSLGGradeCode                                CHAR(2)          NULL,      /* SPARSE, */
--         HSLGOverallGradePointAverage                 DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HSLGOverallCumulativeGradePointAverage       DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HSLGEnglishGradePointAverage                 DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HSLGEnglishCumulativeGradePointAverage       DECIMAL(3,2)     NULL,      /* SPARSE, */
--         HSLGSchoolCode                               CHAR(14)         NULL,      /* SPARSE, */
--         HSLGYearTermCode                             INTEGER          NULL,      /* SPARSE, */
--         HSLGCourseCode                               CHAR(4)          NULL,      /* SPARSE, */
--         HSLGCourseContentRank                        TINYINT          NULL,      /* SPARSE, */
--         HSLGCourseTitle                              VARCHAR(40)      NULL,      /* SPARSE, */
--         HSLGCourseMarkLetter                         VARCHAR(3)       NULL,      /* SPARSE, */
--         HSLGCourseMarkPoints                         DECIMAL(2,1)     NULL,      /* SPARSE, */
--         HSLGCourseUniversityAdmissionRequirementCode VARCHAR(2)       NULL,      /* SPARSE, */
--         HSLGCourseLevelCode                          CHAR(2)          NULL,      /* SPARSE, */
--         HSLGCourseTypeCode                           CHAR(2)          NULL,      /* SPARSE, */
--         CCEACNFLCollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCEACNFLYearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCEACNFLCourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCEACNFLCourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCEACNFLCourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCEACNFLCourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCEACNFLCourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCEACNFLCourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCEACNFLCourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCEACNFLCourseCreditCode                     CHAR(2)          NULL,      /* SPARSE, */
--         CCEACNFLSummary                              CHAR(6)          NULL,      /* SPARSE, */
--         CCEACNFLEslUnits                             SMALLINT         NULL,      /* SPARSE, */
--         CCEACNFLEnglUnits                            SMALLINT         NULL,      /* SPARSE, */
--         CCEACN00CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCEACN00YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCEACN00CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCEACN00CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCEACN00CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN00CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCEACN00CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCEACN00CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCEACN00CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCEACN00CourseCreditCode                     CHAR(2)          NULL,      /* SPARSE, */
--         CCEACN00Summary                              CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN00EslUnits                             SMALLINT         NULL,      /* SPARSE, */
--         CCEACN00EnglUnits                            SMALLINT         NULL,      /* SPARSE, */
--         CCEACN01CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCEACN01YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCEACN01CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCEACN01CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCEACN01CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN01CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCEACN01CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCEACN01CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCEACN01CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCEACN01CourseCreditCode                     CHAR(2)          NULL,      /* SPARSE, */
--         CCEACN01Summary                              CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN01EslUnits                             SMALLINT         NULL,      /* SPARSE, */
--         CCEACN01EnglUnits                            SMALLINT         NULL,      /* SPARSE, */
--         CCEACN02CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCEACN02YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCEACN02CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCEACN02CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCEACN02CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN02CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCEACN02CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCEACN02CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCEACN02CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCEACN02CourseCreditCode                     CHAR(2)          NULL,      /* SPARSE, */
--         CCEACN02Summary                              CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN02EslUnits                             SMALLINT         NULL,      /* SPARSE, */
--         CCEACN02EnglUnits                            SMALLINT         NULL,      /* SPARSE, */
--         CCEACN03CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCEACN03YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCEACN03CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCEACN03CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCEACN03CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN03CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCEACN03CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCEACN03CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCEACN03CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCEACN03CourseCreditCode                     CHAR(2)          NULL,      /* SPARSE, */
--         CCEACN03Summary                              CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN03EslUnits                             SMALLINT         NULL,      /* SPARSE, */
--         CCEACN03EnglUnits                            SMALLINT         NULL,      /* SPARSE, */
--         CCEACN04CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCEACN04YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCEACN04CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCEACN04CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCEACN04CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN04CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCEACN04CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCEACN04CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCEACN04CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCEACN04CourseCreditCode                     CHAR(2)          NULL,      /* SPARSE, */
--         CCEACN04Summary                              CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN04EslUnits                             SMALLINT         NULL,      /* SPARSE, */
--         CCEACN04EnglUnits                            SMALLINT         NULL,      /* SPARSE, */
--         CCEACN05CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCEACN05YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCEACN05CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCEACN05CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCEACN05CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN05CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCEACN05CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCEACN05CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCEACN05CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCEACN05CourseCreditCode                     CHAR(2)          NULL,      /* SPARSE, */
--         CCEACN05Summary                              CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN05EslUnits                             SMALLINT         NULL,      /* SPARSE, */
--         CCEACN05EnglUnits                            SMALLINT         NULL,      /* SPARSE, */
--         CCEACN06CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCEACN06YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCEACN06CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCEACN06CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCEACN06CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN06CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCEACN06CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCEACN06CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCEACN06CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCEACN06CourseCreditCode                     CHAR(2)          NULL,      /* SPARSE, */
--         CCEACN06Summary                              CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN06EslUnits                             SMALLINT         NULL,      /* SPARSE, */
--         CCEACN06EnglUnits                            SMALLINT         NULL,      /* SPARSE, */
--         CCEACN07CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCEACN07YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCEACN07CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCEACN07CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCEACN07CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN07CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCEACN07CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCEACN07CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCEACN07CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCEACN07CourseCreditCode                     CHAR(2)          NULL,      /* SPARSE, */
--         CCEACN07Summary                              CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN07EslUnits                             SMALLINT         NULL,      /* SPARSE, */
--         CCEACN07EnglUnits                            SMALLINT         NULL,      /* SPARSE, */
--         CCEACN08CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCEACN08YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCEACN08CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCEACN08CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCEACN08CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN08CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCEACN08CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCEACN08CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCEACN08CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCEACN08CourseCreditCode                     CHAR(2)          NULL,      /* SPARSE, */
--         CCEACN08Summary                              CHAR(6)          NULL,      /* SPARSE, */
--         CCEACN08EslUnits                             SMALLINT         NULL,      /* SPARSE, */
--         CCEACN08EnglUnits                            SMALLINT         NULL,      /* SPARSE, */
--         -- CCEWCRFLCollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEWCRFLYearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEWCRFLCourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEWCRFLCourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEWCRFLCourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEWCRFLCourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEWCRFLCourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEWCRFLCourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEWCRFLCourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEWCR00CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEWCR00YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEWCR00CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEWCR00CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEWCR00CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEWCR00CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEWCR00CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEWCR00CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEWCR00CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEWCR01CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEWCR01YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEWCR01CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEWCR01CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEWCR01CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEWCR01CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEWCR01CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEWCR01CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEWCR01CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEWCR02CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEWCR02YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEWCR02CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEWCR02CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEWCR02CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEWCR02CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEWCR02CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEWCR02CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEWCR02CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEWCR03CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEWCR03YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEWCR03CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEWCR03CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEWCR03CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEWCR03CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEWCR03CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEWCR03CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEWCR03CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEWCR04CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEWCR04YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEWCR04CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEWCR04CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEWCR04CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEWCR04CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEWCR04CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEWCR04CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEWCR04CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEWCR05CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEWCR05YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEWCR05CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEWCR05CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEWCR05CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEWCR05CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEWCR05CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEWCR05CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEWCR05CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEWCR06CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEWCR06YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEWCR06CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEWCR06CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEWCR06CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEWCR06CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEWCR06CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEWCR06CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEWCR06CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCERCRFLCollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCERCRFLYearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCERCRFLCourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCERCRFLCourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCERCRFLCourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCERCRFLCourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCERCRFLCourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCERCRFLCourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCERCRFLCourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCERCR00CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCERCR00YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCERCR00CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCERCR00CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCERCR00CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCERCR00CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCERCR00CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCERCR00CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCERCR00CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCERCR01CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCERCR01YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCERCR01CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCERCR01CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCERCR01CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCERCR01CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCERCR01CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCERCR01CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCERCR01CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCERCR02CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCERCR02YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCERCR02CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCERCR02CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCERCR02CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCERCR02CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCERCR02CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCERCR02CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCERCR02CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCERCR03CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCERCR03YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCERCR03CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCERCR03CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCERCR03CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCERCR03CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCERCR03CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCERCR03CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCERCR03CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCERCR04CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCERCR04YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCERCR04CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCERCR04CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCERCR04CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCERCR04CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCERCR04CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCERCR04CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCERCR04CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCERCR05CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCERCR05YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCERCR05CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCERCR05CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCERCR05CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCERCR05CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCERCR05CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCERCR05CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCERCR05CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCERCR06CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCERCR06YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCERCR06CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCERCR06CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCERCR06CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCERCR06CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCERCR06CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCERCR06CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCERCR06CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCESCRFLCollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCESCRFLYearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCESCRFLCourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCESCRFLCourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCESCRFLCourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCESCRFLCourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCESCRFLCourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCESCRFLCourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCESCRFLCourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCESCR00CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCESCR00YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCESCR00CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCESCR00CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCESCR00CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCESCR00CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCESCR00CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCESCR00CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCESCR00CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCESCR01CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCESCR01YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCESCR01CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCESCR01CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCESCR01CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCESCR01CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCESCR01CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCESCR01CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCESCR01CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCESCR02CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCESCR02YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCESCR02CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCESCR02CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCESCR02CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCESCR02CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCESCR02CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCESCR02CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCESCR02CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCESCR03CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCESCR03YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCESCR03CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCESCR03CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCESCR03CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCESCR03CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCESCR03CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCESCR03CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCESCR03CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCESCR04CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCESCR04YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCESCR04CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCESCR04CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCESCR04CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCESCR04CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCESCR04CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCESCR04CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCESCR04CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCESCR05CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCESCR05YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCESCR05CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCESCR05CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCESCR05CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCESCR05CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCESCR05CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCESCR05CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCESCR05CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCESCR06CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCESCR06YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCESCR06CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCESCR06CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCESCR06CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCESCR06CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCESCR06CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCESCR06CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCESCR06CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEICRFLCollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEICRFLYearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEICRFLCourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEICRFLCourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEICRFLCourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEICRFLCourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEICRFLCourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEICRFLCourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEICRFLCourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEICR00CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEICR00YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEICR00CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEICR00CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEICR00CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEICR00CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEICR00CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEICR00CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEICR00CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEICR01CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEICR01YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEICR01CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEICR01CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEICR01CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEICR01CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEICR01CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEICR01CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEICR01CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEICR02CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEICR02YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEICR02CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEICR02CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEICR02CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEICR02CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEICR02CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEICR02CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEICR02CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEICR03CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEICR03YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEICR03CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEICR03CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEICR03CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEICR03CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEICR03CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEICR03CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEICR03CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEICR04CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEICR04YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEICR04CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEICR04CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEICR04CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEICR04CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEICR04CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEICR04CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEICR04CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEICR05CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEICR05YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEICR05CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEICR05CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEICR05CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEICR05CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEICR05CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEICR05CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEICR05CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEICR06CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEICR06YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEICR06CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEICR06CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEICR06CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEICR06CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEICR06CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEICR06CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEICR06CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEWNCFLCollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEWNCFLYearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEWNCFLCourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEWNCFLCourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEWNCFLCourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEWNCFLCourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEWNCFLCourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEWNCFLCourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEWNCFLCourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEWNC01CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEWNC01YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEWNC01CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEWNC01CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEWNC01CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEWNC01CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEWNC01CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEWNC01CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEWNC01CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEWNC02CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEWNC02YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEWNC02CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEWNC02CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEWNC02CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEWNC02CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEWNC02CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEWNC02CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEWNC02CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEWNC03CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEWNC03YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEWNC03CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEWNC03CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEWNC03CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEWNC03CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEWNC03CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEWNC03CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEWNC03CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEWNC04CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEWNC04YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEWNC04CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEWNC04CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEWNC04CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEWNC04CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEWNC04CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEWNC04CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEWNC04CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEWNC05CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEWNC05YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEWNC05CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEWNC05CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEWNC05CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEWNC05CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEWNC05CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEWNC05CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEWNC05CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEWNC06CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEWNC06YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEWNC06CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEWNC06CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEWNC06CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEWNC06CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEWNC06CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEWNC06CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEWNC06CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCERNCFLCollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCERNCFLYearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCERNCFLCourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCERNCFLCourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCERNCFLCourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCERNCFLCourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCERNCFLCourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCERNCFLCourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCERNCFLCourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCERNC01CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCERNC01YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCERNC01CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCERNC01CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCERNC01CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCERNC01CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCERNC01CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCERNC01CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCERNC01CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCERNC02CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCERNC02YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCERNC02CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCERNC02CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCERNC02CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCERNC02CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCERNC02CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCERNC02CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCERNC02CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCERNC03CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCERNC03YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCERNC03CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCERNC03CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCERNC03CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCERNC03CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCERNC03CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCERNC03CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCERNC03CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCERNC04CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCERNC04YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCERNC04CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCERNC04CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCERNC04CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCERNC04CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCERNC04CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCERNC04CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCERNC04CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCERNC05CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCERNC05YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCERNC05CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCERNC05CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCERNC05CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCERNC05CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCERNC05CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCERNC05CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCERNC05CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCERNC06CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCERNC06YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCERNC06CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCERNC06CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCERNC06CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCERNC06CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCERNC06CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCERNC06CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCERNC06CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCESNCFLCollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCESNCFLYearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCESNCFLCourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCESNCFLCourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCESNCFLCourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCESNCFLCourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCESNCFLCourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCESNCFLCourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCESNCFLCourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCESNC01CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCESNC01YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCESNC01CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCESNC01CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCESNC01CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCESNC01CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCESNC01CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCESNC01CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCESNC01CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCESNC02CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCESNC02YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCESNC02CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCESNC02CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCESNC02CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCESNC02CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCESNC02CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCESNC02CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCESNC02CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCESNC03CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCESNC03YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCESNC03CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCESNC03CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCESNC03CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCESNC03CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCESNC03CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCESNC03CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCESNC03CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCESNC04CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCESNC04YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCESNC04CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCESNC04CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCESNC04CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCESNC04CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCESNC04CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCESNC04CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCESNC04CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCESNC05CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCESNC05YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCESNC05CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCESNC05CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCESNC05CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCESNC05CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCESNC05CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCESNC05CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCESNC05CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCESNC06CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCESNC06YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCESNC06CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCESNC06CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCESNC06CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCESNC06CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCESNC06CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCESNC06CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCESNC06CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEINCFLCollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEINCFLYearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEINCFLCourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEINCFLCourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEINCFLCourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEINCFLCourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEINCFLCourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEINCFLCourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEINCFLCourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEINC01CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEINC01YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEINC01CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEINC01CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEINC01CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEINC01CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEINC01CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEINC01CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEINC01CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEINC02CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEINC02YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEINC02CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEINC02CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEINC02CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEINC02CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEINC02CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEINC02CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEINC02CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEINC03CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEINC03YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEINC03CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEINC03CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEINC03CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEINC03CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEINC03CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEINC03CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEINC03CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEINC04CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEINC04YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEINC04CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEINC04CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEINC04CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEINC04CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEINC04CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEINC04CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEINC04CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEINC05CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEINC05YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEINC05CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEINC05CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEINC05CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEINC05CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEINC05CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEINC05CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEINC05CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEINC06CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEINC06YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEINC06CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEINC06CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEINC06CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEINC06CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEINC06CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEINC06CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEINC06CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEINC07CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEINC07YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEINC07CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEINC07CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEINC07CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEINC07CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEINC07CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEINC07CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEINC07CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEINC08CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEINC08YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEINC08CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEINC08CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEINC08CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEINC08CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEINC08CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEINC08CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEINC08CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCECNCFLCollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCECNCFLYearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCECNCFLCourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCECNCFLCourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCECNCFLCourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCECNCFLCourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCECNCFLCourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCECNCFLCourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCECNCFLCourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCECNC00CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCECNC00YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCECNC00CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCECNC00CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCECNC00CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCECNC00CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCECNC00CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCECNC00CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCECNC00CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCECNC01CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCECNC01YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCECNC01CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCECNC01CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCECNC01CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCECNC01CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCECNC01CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCECNC01CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCECNC01CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCECNC02CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCECNC02YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCECNC02CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCECNC02CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCECNC02CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCECNC02CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCECNC02CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCECNC02CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCECNC02CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCECNC03CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCECNC03YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCECNC03CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCECNC03CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCECNC03CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCECNC03CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCECNC03CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCECNC03CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCECNC03CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCECNC04CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCECNC04YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCECNC04CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCECNC04CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCECNC04CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCECNC04CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCECNC04CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCECNC04CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCECNC04CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCECNC05CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCECNC05YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCECNC05CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCECNC05CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCECNC05CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCECNC05CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCECNC05CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCECNC05CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCECNC05CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCECNC06CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCECNC06YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCECNC06CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCECNC06CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCECNC06CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCECNC06CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCECNC06CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCECNC06CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCECNC06CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCECNC07CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCECNC07YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCECNC07CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCECNC07CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCECNC07CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCECNC07CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCECNC07CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCECNC07CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCECNC07CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCECNC08CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCECNC08YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCECNC08CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCECNC08CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCECNC08CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCECNC08CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCECNC08CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCECNC08CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCECNC08CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEVNCFLCollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEVNCFLYearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEVNCFLCourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEVNCFLCourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEVNCFLCourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEVNCFLCourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEVNCFLCourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEVNCFLCourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEVNCFLCourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEVNC00CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEVNC00YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEVNC00CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEVNC00CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEVNC00CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEVNC00CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEVNC00CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEVNC00CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEVNC00CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEVNC01CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEVNC01YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEVNC01CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEVNC01CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEVNC01CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEVNC01CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEVNC01CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEVNC01CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEVNC01CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEVNC02CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEVNC02YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEVNC02CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEVNC02CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEVNC02CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEVNC02CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEVNC02CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEVNC02CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEVNC02CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEVNC03CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEVNC03YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEVNC03CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEVNC03CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEVNC03CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEVNC03CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEVNC03CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEVNC03CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEVNC03CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEVNC04CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEVNC04YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEVNC04CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEVNC04CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEVNC04CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEVNC04CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEVNC04CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEVNC04CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEVNC04CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEVNC05CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEVNC05YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEVNC05CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEVNC05CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEVNC05CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEVNC05CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEVNC05CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEVNC05CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEVNC05CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         -- CCEVNC06CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         -- CCEVNC06YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         -- CCEVNC06CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         -- CCEVNC06CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         -- CCEVNC06CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         -- CCEVNC06CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         -- CCEVNC06CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         -- CCEVNC06CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         -- CCEVNC06CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCENCRFLCollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCENCRFLYearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCENCRFLCourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCENCRFLCourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCENCRFLCourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCENCRFLCourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCENCRFLCourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCENCRFLCourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCENCRFLCourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCENCR00CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCENCR00YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCENCR00CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCENCR00CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCENCR00CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCENCR00CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCENCR00CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCENCR00CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCENCR00CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCENCR00CB25                                 char(1)          NULL,      /* SPARSE, */
--         CCENCR00CB26                                 char(1)          NULL,      /* SPARSE, */
--         CCENCR00TleComposition                       tinyint          NULL,      /* SPARSE, */
--         CCENCR00IsEngl                               BIT              NULL,      /* SPARSE, */
--         CCENCR00CoreqType                            TINYINT          NULL,      /* SPARSE, */
--         CCENCR002NCollegeCode                        CHAR(3)          NULL,      /* SPARSE, */
--         CCENCR002NYearTermCode                       INTEGER          NULL,      /* SPARSE, */
--         CCENCR002NCourseId                           VARCHAR(12)      NULL,      /* SPARSE, */
--         CCENCR002NCourseControlNumber                CHAR(12)         NULL,      /* SPARSE, */
--         CCENCR002NCourseTopCode                      CHAR(6)          NULL,      /* SPARSE, */
--         CCENCR002NCourseTitle                        VARCHAR(68)      NULL,      /* SPARSE, */
--         CCENCR002NCourseMarkLetter                   VARCHAR(3)       NULL,      /* SPARSE, */
--         CCENCR002NCourseMarkPoints                   DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCENCR002NCourseLevelCode                    CHAR(1)          NULL,      /* SPARSE, */
--         CCENCR002NCB25                               char(1)          NULL,      /* SPARSE, */
--         CCENCR002NCB26                               char(1)          NULL,      /* SPARSE, */
--         CCENCR002NTleComposition                     tinyint          NULL,      /* SPARSE, */
--         CCENCR002NIsEngl                             BIT              NULL,      /* SPARSE, */
--         CCENCR002NCoreqType                          TINYINT          NULL,      /* SPARSE, */
--         CCENCR01CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCENCR01YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCENCR01CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCENCR01CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCENCR01CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCENCR01CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCENCR01CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCENCR01CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCENCR01CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCENCR01CB25                                 char(1)          NULL,      /* SPARSE, */
--         CCENCR01CB26                                 char(1)          NULL,      /* SPARSE, */
--         CCENCR02CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCENCR02YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCENCR02CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCENCR02CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCENCR02CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCENCR02CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCENCR02CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCENCR02CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCENCR02CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCENCR02CB25                                 char(1)          NULL,      /* SPARSE, */
--         CCENCR02CB26                                 char(1)          NULL,      /* SPARSE, */
--         CCENCR03CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCENCR03YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCENCR03CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCENCR03CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCENCR03CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCENCR03CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCENCR03CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCENCR03CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCENCR03CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCENCR03CB25                                 char(1)          NULL,      /* SPARSE, */
--         CCENCR03CB26                                 char(1)          NULL,      /* SPARSE, */
--         CCENCR04CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCENCR04YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCENCR04CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCENCR04CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCENCR04CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCENCR04CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCENCR04CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCENCR04CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCENCR04CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCENCR04CB25                                 char(1)          NULL,      /* SPARSE, */
--         CCENCR04CB26                                 char(1)          NULL,      /* SPARSE */
--         CCRDCRFLCollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCRDCRFLYearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCRDCRFLCourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCRDCRFLCourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCRDCRFLCourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCRDCRFLCourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCRDCRFLCourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCRDCRFLCourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCRDCRFLCourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCRDCR00CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCRDCR00YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCRDCR00CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCRDCR00CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCRDCR00CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCRDCR00CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCRDCR00CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCRDCR00CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCRDCR00CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCRDCR01CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCRDCR01YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCRDCR01CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCRDCR01CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCRDCR01CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCRDCR01CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCRDCR01CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCRDCR01CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCRDCR01CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCRDCR02CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCRDCR02YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCRDCR02CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCRDCR02CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCRDCR02CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCRDCR02CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCRDCR02CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCRDCR02CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCRDCR02CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCRDCR03CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCRDCR03YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCRDCR03CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCRDCR03CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCRDCR03CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCRDCR03CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCRDCR03CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCRDCR03CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCRDCR03CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         CCRDCR04CollegeCode                          CHAR(3)          NULL,      /* SPARSE, */
--         CCRDCR04YearTermCode                         INTEGER          NULL,      /* SPARSE, */
--         CCRDCR04CourseId                             VARCHAR(12)      NULL,      /* SPARSE, */
--         CCRDCR04CourseControlNumber                  CHAR(12)         NULL,      /* SPARSE, */
--         CCRDCR04CourseTopCode                        CHAR(6)          NULL,      /* SPARSE, */
--         CCRDCR04CourseTitle                          VARCHAR(68)      NULL,      /* SPARSE, */
--         CCRDCR04CourseMarkLetter                     VARCHAR(3)       NULL,      /* SPARSE, */
--         CCRDCR04CourseMarkPoints                     DECIMAL(4,2)     NULL,      /* SPARSE, */
--         CCRDCR04CourseLevelCode                      CHAR(1)          NULL,      /* SPARSE, */
--         STIsEnglishEap                               BIT              NULL,      /* SPARSE, */
--         STEnglishScaledScore                         SMALLINT         NULL,      /* SPARSE, */
--         STEnglishCluster1                            SMALLINT         NULL,      /* SPARSE, */
--         STEnglishCluster2                            SMALLINT         NULL,      /* SPARSE, */
--         STEnglishCluster3                            SMALLINT         NULL,      /* SPARSE, */
--         STEnglishCluster4                            SMALLINT         NULL,      /* SPARSE, */
--         STEnglishCluster5                            SMALLINT         NULL,      /* SPARSE, */
--         STEnglishCluster6                            SMALLINT         NULL,      /* SPARSE, */
--         STMathematicsSubject                         TINYINT          NULL,      /* SPARSE, */
--         STIsMathematicsEap                           BIT              NULL,      /* SPARSE, */
--         STMathematicsScaledScore                     SMALLINT         NULL,      /* SPARSE, */
--         STMathematicsCluster1                        SMALLINT         NULL,      /* SPARSE, */
--         STMathematicsCluster2                        SMALLINT         NULL,      /* SPARSE, */
--         STMathematicsCluster3                        SMALLINT         NULL,      /* SPARSE, */
--         STMathematicsCluster4                        SMALLINT         NULL,      /* SPARSE, */
--         STMathematicsCluster5                        SMALLINT         NULL,      /* SPARSE, */
--         STMathematicsCluster6                        SMALLINT         NULL,      /* SPARSE, */
--         STIsEnglishOnly                              BIT              NULL,      /* SPARSE, */
--         STIsEnglishLearner                           BIT              NULL,      /* SPARSE, */
--         STIsFluentInitially                          BIT              NULL,      /* SPARSE, */
--         STIsFluentReclassified                       BIT              NULL,      /* SPARSE, */
--         HSIsRemedial                                 BIT              NULL,      /* SPARSE, */
--         HSGraduationDate                             CHAR(8)          NULL,      /* SPARSE, */
--         CCAge                                        SMALLINT         NULL,      /* SPARSE, */
--         CCEslAge                                     SMALLINT         NULL,      /* SPARSE, */
--         CCEnglAge                                    SMALLINT         NULL,      /* SPARSE, */
--         CCGender                                     CHAR(1)          NULL,      /* SPARSE, */
--         CCCitzenship                                 CHAR(1)          NULL,      /* SPARSE, */
--         CCResidency                                  CHAR(5)          NULL,      /* SPARSE, */
--         CCEducationStatus                            CHAR(5)          NULL,      /* SPARSE, */
--         CCEnrollmentStatus                           CHAR(1)          NULL,      /* SPARSE, */
--         CCEthnicities                                CHAR(21)         NULL,      /* SPARSE, */
--         CCParentEducationLevel                       CHAR(21)         NULL,      /* SPARSE, */
--         CCEthnicity                                  CHAR(1)          NULL,      /* SPARSE, */
--         MilitaryStatus                               CHAR(4)          NULL,      /* SPARSE, */
--         MilitaryDependentStatus                      CHAR(4)          NULL,      /* SPARSE, */
--         FosterYouthStatus                            CHAR(1)          NULL,      /* SPARSE, */
--         IncarceratedStatus                           CHAR(1)          NULL,      /* SPARSE, */
--         MESAASEMStatus                               CHAR(1)          NULL,      /* SPARSE, */
--         PuenteStatus                                 CHAR(1)          NULL,      /* SPARSE, */
--         MCHSECHSStatus                               CHAR(1)          NULL,      /* SPARSE, */
--         UMOJAStatus                                  CHAR(1)          NULL,      /* SPARSE, */
--         CAAStatus                                    CHAR(1)          NULL,      /* SPARSE, */
--         CAFYESStatus                                 CHAR(1)          NULL,      /* SPARSE, */
--         BaccalaureateProgram                         CHAR(5)          NULL,      /* SPARSE, */
--         CCAPStatus                                   CHAR(1)          NULL,      /* SPARSE, */
--         EconomicallyDisadvStatus                     CHAR(2)          NULL,      /* SPARSE, */
--         ExOffenderStatus                             CHAR(1)          NULL,      /* SPARSE, */
--         HomelessStatus                               CHAR(1)          NULL,      /* SPARSE, */
--         LongtermUnemployStatus                       CHAR(1)          NULL,      /* SPARSE, */
--         CulturalBarrierStatus                        CHAR(1)          NULL,      /* SPARSE, */
--         SeasonalFarmWorkStatus                       CHAR(1)          NULL,      /* SPARSE, */
--         LiteracyStatus                               CHAR(1)          NULL,      /* SPARSE, */
--         WorkBasedLearningStatus                      CHAR(1)          NULL,      /* SPARSE, */
--         CCIsMultiCollege                             BIT              NULL,      /* SPARSE, */
--         CCPrimaryDisability                          CHAR(1)          NULL,      /* SPARSE, */
--         CCEducationalGoal                            CHAR(1)          NULL,      /* SPARSE, */
--         CCInitialGoal                                CHAR(1)          NULL,      /* SPARSE, */
--         CCMajor                                      CHAR(6)          NULL,      /* SPARSE, */
--         CCAssessmentStatus                           CHAR(2)          NULL,      /* SPARSE, */
--         CCAdvisementStatus                           CHAR(2)          NULL,      /* SPARSE, */
--         CCEnrollmentLastTerm                         CHAR(3)          NULL,      /* SPARSE, */
--         CCTransferableUnitsAttempted                 DECIMAL(6,2)     NULL,      /* SPARSE, */
--         CCTransferableUnitsEarned                    DECIMAL(6,2)     NULL,      /* SPARSE, */
--         CCEopsCareStatus                             CHAR(1)          NULL,      /* SPARSE, */
--         APHSCountry                                  CHAR(2)          NULL,      /* SPARSE, */
--         APUN01Country                                CHAR(2)          NULL,      /* SPARSE, */
--         APUN01Award                                  CHAR(1)          NULL,      /* SPARSE, */
--         APUN02Country                                CHAR(2)          NULL,      /* SPARSE, */
--         APUN02Award                                  CHAR(1)          NULL,      /* SPARSE, */
--         APUN03Country                                CHAR(2)          NULL,      /* SPARSE, */
--         APUN03Award                                  CHAR(1)          NULL,      /* SPARSE, */
--         APUN04Country                                CHAR(2)          NULL,      /* SPARSE, */
--         APUN04Award                                  CHAR(1)          NULL,      /* SPARSE, */
--         APVisaType                                   CHAR(2)          NULL,      /* SPARSE, */
--         APAB540Waiver                                BIT              NULL,      /* SPARSE, */
--         APComfortableEnglish                         BIT              NULL,      /* SPARSE, */
--         APApplicationLanguage                        CHAR(2)          NULL,      /* SPARSE, */
--         APEsl                                        BIT              NULL,      /* SPARSE, */
--         APBasicSkills                                BIT              NULL,      /* SPARSE, */
--         APEmploymentAssistance                       BIT              NULL,      /* SPARSE, */
--         APSeasonalAG                                 BIT              NULL,      /* SPARSE, */
--         APCountry                                    CHAR(2)          NULL,      /* SPARSE, */
--         APPermanentCountry                           CHAR(2)          NULL,      /* SPARSE, */
--         APPermanentCountryInternational              CHAR(2)          NULL,      /* SPARSE, */
--         APCompletedEleventhGrade                     BIT              NULL,      /* SPARSE, */
--         APCumulativeGradePointAverage                DECIMAL(3,2)     NULL,      /* SPARSE, */
--         APEnglishCompletedCourseId                   TINYINT          NULL,      /* SPARSE, */
--         APEnglishCompletedCourseGrade                VARCHAR(3)       NULL,      /* SPARSE, */
--         APMathematicsCompletedCourseId               TINYINT          NULL,      /* SPARSE, */
--         APMathematicsCompletedCourseGrade            VARCHAR(3)       NULL,      /* SPARSE, */
--         APMathematicsPassedCourseId                  TINYINT          NULL,      /* SPARSE, */
--         APMathematicsPassedCourseGrade               VARCHAR(3)       NULL,      /* SPARSE, */
--         XferFice                                     CHAR(6)          NULL,      /* SPARSE, */
--         XferDate                                     DATE             NULL,      /* SPARSE, */
--         XfersSource                                  CHAR(4)          NULL,      /* SPARSE, */
--         XferSchoolName                               VARCHAR(40)      NULL,      /* SPARSE, */
--         XferSchoolType                               CHAR(1)          NULL,      /* SPARSE, */
--         XferSector                                   CHAR(5)          NULL,      /* SPARSE, */
--         XferState                                    CHAR(2)          NULL,      /* SPARSE, */
--         XferSegment                                  VARCHAR(10)      NULL,      /* SPARSE, */
--         ElaStatusRecent                              tinyint          NULL,      /* SPARSE, */
--     );

