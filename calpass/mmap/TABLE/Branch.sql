USE calpass;

GO

IF (object_id('mmap.Branch') is not null)
	BEGIN
		DROP TABLE mmap.Branch;
	END;

GO

CREATE TABLE
	mmap.Branch
	(
		BranchId int identity(0,1),
		TreeId int not null,
		Description varchar(255) not null
	);

GO

ALTER TABLE
	mmap.Branch
ADD CONSTRAINT
	pk_Branch
PRIMARY KEY CLUSTERED
	(
		BranchId
	);

ALTER TABLE
	mmap.Branch
ADD CONSTRAINT
	uk_Branch__TreeId
UNIQUE
	(
		TreeId,
		Description
	);

ALTER TABLE
	mmap.Branch
ADD CONSTRAINT
	fk_Branch__TreeId
FOREIGN KEY
	(
		TreeId
	)
REFERENCES
	mmap.Tree
	(
		TreeId
	)
ON UPDATE CASCADE
ON DELETE CASCADE;

GO

INSERT INTO
	mmap.Branch
	(
		TreeId,
		Description
	)
VALUES
	(0,'EnglishCollege; Non-Direct Matriculant'),
	(1,'EnglishCollege; Direct Matriculant'),
	(2,'EnglishLevelOne; Non-Direct Matriculant'),
	(3,'EnglishLevelOne; Direct Matriculant'),
	(4,'EnglishLevelTwo; Non-Direct Matriculant; 1'),
	(4,'EnglishLevelTwo; Non-Direct Matriculant; 2'),
	(5,'EnglishLevelTwo; Direct Matriculant'),
	(6,'EnglishLevelThree; Non-Direct Matriculant; 1'),
	(6,'EnglishLevelThree; Non-Direct Matriculant; 2'),
	(7,'EnglishLevelThree; Direct Matriculant'),
	(8,'ReadingTestOut; Non-Direct Matriculant; IsReadingCollege'),
	(8,'ReadingTestOut; Non-Direct Matriculant; IsReadingLevelOne'),
	(8,'ReadingTestOut; Non-Direct Matriculant; IsReadingLevelTwo; 1'),
	(8,'ReadingTestOut; Non-Direct Matriculant; IsReadingLevelTwo; 2'),
	(9,'ReadingTestOut; Direct Matriculant; IsReadingCollege'),
	(9,'ReadingTestOut; Direct Matriculant; IsReadingLevelOne'),
	(9,'ReadingTestOut; Direct Matriculant; IsReadingLevelTwo'),
	(10,'ReadingCollege; Non-Direct Matriculant; IsReadingCollege'),
	(11,'ReadingCollege; Direct Matriculant; IsReadingCollege'),
	(12,'ReadingLevelOne; Non-Direct Matriculant; IsReadingCollege'),
	(12,'ReadingLevelOne; Non-Direct Matriculant; IsReadingLevelOne; 1'),
	(12,'ReadingLevelOne; Non-Direct Matriculant; IsReadingLevelOne; 2'),
	(13,'ReadingLevelOne; Direct Matriculant; IsReadingCollege'),
	(13,'ReadingLevelOne; Direct Matriculant; IsReadingLevelOne'),
	(14,'ReadingLevelTwo; Non-Direct Matriculant; IsReadingCollege'),
	(14,'ReadingLevelTwo; Non-Direct Matriculant; IsReadingLevelOne'),
	(14,'ReadingLevelTwo; Non-Direct Matriculant; IsReadingLevelTwo'),
	(15,'ReadingLevelTwo; Direct Matriculant; IsReadingCollege'),
	(15,'ReadingLevelTwo; Direct Matriculant; IsReadingLevelOne'),
	(15,'ReadingLevelTwo; Direct Matriculant; IsReadingLevelTwo'),
	(16,'ReadingLevelThree; Non-Direct Matriculant; IsReadingCollege; 1'),
	(16,'ReadingLevelThree; Non-Direct Matriculant; IsReadingCollege; 2'),
	(16,'ReadingLevelThree; Non-Direct Matriculant; IsReadingLevelOne'),
	(16,'ReadingLevelThree; Non-Direct Matriculant; IsReadingLevelTwo'),
	(17,'ReadingLevelThree; Direct Matriculant; IsReadingCollege'),
	(17,'ReadingLevelThree; Direct Matriculant; IsReadingLevelTwo'),
	(18,'EslCollege; Non-Direct Matriculant; IsEslCollege'),
	(19,'EslCollege; Direct Matriculant; IsEslCollege'),
	(20,'EslLevelOne; Non-Direct Matriculant; IsEslLevelOne; 1'),
	(20,'EslLevelOne; Non-Direct Matriculant; IsEslLevelOnel; 2'),
	(21,'EslLevelOne; Direct Matriculant; IsEslCollege'),
	(21,'EslLevelOne; Direct Matriculant; IsEslLevelOne'),
	(22,'EslLevelTwo; Non-Direct Matriculant; IsEslCollege'),
	(22,'EslLevelTwo; Non-Direct Matriculant; IsEslLevelOne'),
	(22,'EslLevelTwo; Non-Direct Matriculant; IsEslLevelTwo; 1'),
	(22,'EslLevelTwo; Non-Direct Matriculant; IsEslLevelTwo; 2'),
	(23,'EslLevelTwo; Direct Matriculant; IsEslCollege'),
	(23,'EslLevelTwo; Direct Matriculant; IsEslLevelOne'),
	(23,'EslLevelTwo; Direct Matriculant; IsEslLevelTwo'),
	(24,'EslLevelThree; Non-Direct Matriculant; IsEslLevelOne'),
	(24,'EslLevelThree; Non-Direct Matriculant; IsEslLevelTwo; 1'),
	(24,'EslLevelThree; Non-Direct Matriculant; IsEslLevelTwo; 2'),
	(25,'EslLevelThree; Direct Matriculant; IsEslLevelOne; 1'),
	(25,'EslLevelThree; Direct Matriculant; IsEslLevelOne; 2'),
	(25,'EslLevelThree; Direct Matriculant; IsEslLevelOne; 3'),
	(25,'EslLevelThree; Direct Matriculant; IsEslLevelTwo'),
	(26,'PreAlgebra; Non-Direct Matriculant'),
	(27,'PreAlgebra; Direct Matriculant'),
	(28,'AlgebraI; Non-Direct Matriculant'),
	(29,'AlgebraI; Direct Matriculant'),
	(30,'AlgebraII; Non-Direct Matriculant; 1'),
	(30,'AlgebraII; Non-Direct Matriculant; 2'),
	(31,'AlgebraII; Direct Matriculant'),
	(32,'MathGE; Non-Direct Matriculant; 1'),
	(32,'MathGE; Non-Direct Matriculant; 2'),
	(33,'MathGE; Direct Matriculant'),
	(34,'Statistics; Non-Direct Matriculant; 1'),
	(34,'Statistics; Non-Direct Matriculant; 2'),
	(35,'Statistics; Direct Matriculant; 1'),
	(35,'Statistics; Direct Matriculant; 2'),
	(36,'CollegeAlgebra; Non-Direct Matriculant; 1'),
	(36,'CollegeAlgebra; Non-Direct Matriculant; 2'),
	(36,'CollegeAlgebra; Non-Direct Matriculant; 3'),
	(37,'CollegeAlgebra; Direct Matriculant; 1'),
	(37,'CollegeAlgebra; Direct Matriculant; 2'),
	(38,'Trigonometry; Non-Direct Matriculant; 1'),
	(38,'Trigonometry; Non-Direct Matriculant; 2'),
	(39,'Trigonometry; Direct Matriculant; 1'),
	(39,'Trigonometry; Direct Matriculant; 2'),
	(39,'Trigonometry; Direct Matriculant; 3'),
	(40,'PreCalculus; Non-Direct Matriculant; 1'),
	(40,'PreCalculus; Non-Direct Matriculant; 2'),
	(41,'PreCalculus; Direct Matriculant; 1'),
	(41,'PreCalculus; Direct Matriculant; 2'),
	(42,'CalculusI; Non-Direct Matriculant; 1'),
	(42,'CalculusI; Non-Direct Matriculant; 2'),
	(43,'CalculusI; Direct Matriculant; 1'),
	(43,'CalculusI; Direct Matriculant; 2');