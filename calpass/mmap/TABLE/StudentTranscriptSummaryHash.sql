USE calpass;

GO

IF (object_id('mmap.StudentTranscriptSummaryHash') is not null)
	BEGIN
		DROP TABLE mmap.StudentTranscriptSummaryHash;
	END;

GO

CREATE TABLE
	mmap.StudentTranscriptSummaryHash
	(
		InterSegmentKey binary(64) not null,
		Entity varchar(25) not null,
		Attribute varchar(25) not null,
		Value decimal(4,3) not null
	);

GO

ALTER TABLE
	mmap.StudentTranscriptSummaryHash
ADD CONSTRAINT
	PK_StudentTranscriptSummaryHash
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey,
		Entity,
		Attribute
	);

GO

CREATE NONCLUSTERED INDEX
	IX_StudentTranscriptSummaryHash__Entity
ON
	mmap.StudentTranscriptSummaryHash
	(
		Entity,
		Attribute
	)
INCLUDE
	(
		Value
	);