USE calpass;

GO

IF (object_id('mmap.Transcript', 'U') is not null)
	BEGIN
		DROP TABLE mmap.Transcript;
	END;

GO

CREATE TABLE
	mmap.Transcript
	(
		InterSegmentKey             binary(64)   not null,
		IsHighSchoolGrade09         bit          not null,
		IsHighSchoolGrade10         bit          not null,
		IsHighSchoolGrade11         bit          not null,
		IsHighSchoolGrade12         bit          not null,
		CumulativeGradePointAverage decimal(4,3) not null,
		English                     decimal(2,1)     null,
		PreAlgebra                  decimal(2,1)     null,
		AlgebraI                    decimal(2,1)     null,
		Geometry                    decimal(2,1)     null,
		AlgebraII                   decimal(2,1)     null,
		Trigonometry                decimal(2,1)     null,
		PreCalculus                 decimal(2,1)     null,
		[Statistics]                decimal(2,1)     null,
		Calculus                    decimal(2,1)     null,
		IsEscrow                    bit          not null,
	);

GO

ALTER TABLE
	mmap.Transcript
ADD CONSTRAINT
	PK_Transcript
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey
	);