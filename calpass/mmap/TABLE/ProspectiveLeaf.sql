USE calpass;

GO

IF (object_id('mmap.ProspectiveLeaf') is not null)
	BEGIN
		DROP TABLE mmap.ProspectiveLeaf;
	END;

GO

CREATE TABLE
	mmap.ProspectiveLeaf
	(
		Id int identity(0,1),
		ProspectiveBranchId int not null,
		Entity varchar(25) not null,
		Attribute varchar(25) not null,
		Value decimal(6,3) not null
	);

GO

ALTER TABLE
	mmap.ProspectiveLeaf
ADD CONSTRAINT
	PK_ProspectiveLeaf
PRIMARY KEY CLUSTERED
	(
		Id
	);

ALTER TABLE
	mmap.ProspectiveLeaf
ADD CONSTRAINT
	UK_ProspectiveLeaf__ProspectiveBranchId
UNIQUE
	(
		ProspectiveBranchId,
		Entity,
		Attribute
	);

ALTER TABLE
	mmap.ProspectiveLeaf
ADD CONSTRAINT
	FK_ProspectiveLeaf__ProspectiveBranchId
FOREIGN KEY
	(
		ProspectiveBranchId
	)
REFERENCES
	mmap.ProspectiveBranch
	(
		Id
	)
ON DELETE CASCADE
ON UPDATE CASCADE;

GO

CREATE TRIGGER
	ProspectiveLeaf_After
ON
	mmap.Prospectiveleaf
AFTER
	INSERT, UPDATE, DELETE
AS
BEGIN
	UPDATE
		t
	SET
		t.LeafCount = s.LeafCount
	FROM
		mmap.ProspectiveBranch t
		inner join
		(
			SELECT
				ProspectiveBranchId,
				LeafCount = count(*)
			FROM
				inserted i
			GROUP BY
				ProspectiveBranchId
		) s
			on t.Id = s.ProspectiveBranchId;
END;


GO

CREATE NONCLUSTERED INDEX
	ix_ProspectiveLeaf__Entity
ON
	mmap.ProspectiveLeaf
	(
		Entity,
		Attribute
	)
INCLUDE
	(
		ProspectiveBranchId,
		Value
	);

GO

INSERT
	mmap.ProspectiveLeaf
	(
		ProspectiveBranchId,
		Entity,
		Attribute,
		Value
	)
VALUES
	-- English
	(0,'Cgpa','11',2.6), -- Direct Matriculant; EnglishLevel0
	(1,'Cgpa','12',2.6), -- Non-Direct Matriculant; EnglishLevel0
	(2,'Cgpa','11',2.3), -- Direct Matriculant; EnglishLevel1
	(3,'Cgpa','12',2.2), -- Non-Direct Matriculant; EnglishLevel1
	(3,'English12','MarkPoints',2.0), -- Non-Direct Matriculant; EnglishLevel1
	(4,'Cgpa','11',2.0), -- Direct Matriculant; EnglishLevel2
	(5,'Cgpa','12',1.8), -- Non-Direct Matriculant; EnglishLevel2; 1
	(5,'English12','MarkPoints',1.0), -- Non-Direct Matriculant; EnglishLevel2; 1
	(6,'Cgpa','12',1.8), -- Non-Direct Matriculant; EnglishLevel2; 2
	(6,'English','ScaledScore',268), -- Non-Direct Matriculant; EnglishLevel2; 2
	(7,'Cgpa','11',1.4), -- Direct Matriculant; EnglishLevel3
	(8,'Cgpa','12',1.7), -- Non-Direct Matriculant; EnglishLevel3; 1
	(9,'Cgpa','12',1.5), -- Non-Direct Matriculant; EnglishLevel3; 2
	(9,'English','ScaledScore',268), -- Non-Direct Matriculant; EnglishLevel3; 2
	-- Mathematics :: Non-STEM
	(10,'Cgpa','11',2.0), -- Direct Matriculant; PreAlgebra
	(11,'Cgpa','12',2.1), -- Non-Direct Matriculant; PreAlgebra
	(12,'Cgpa','11',2.4), -- Direct Matriculant; AlgebraI
	(13,'Cgpa','11',2.5), -- Non-Direct Matriculant; AlgebraI; 1
	(14,'Cgpa','11',2.0), -- Non-Direct Matriculant; AlgebraI; 2
	(14,'Mathematics','AlgebraIScaledScore',302), -- Non-Direct Matriculant; AlgebraI; 2
	(15,'Cgpa','11',2.8), -- Direct Matriculant; AlgebraII
	(15,'Mathematics','PromotedRank',3), -- Direct Matriculant; AlgebraII
	(16,'Cgpa','12',2.9), -- Non-Direct Matriculant; AlgebraII; 1
	(16,'Mathematics','PromotedRank',3), -- Non-Direct Matriculant; AlgebraII; 1
	(17,'Cgpa','12',2.5), -- Non-Direct Matriculant; AlgebraII; 2
	(17,'Mathematics','AlgebraIIScaledScore',302), -- Non-Direct Matriculant; AlgebraII; 2
	(17,'Mathematics','PromotedRank',3), -- Non-Direct Matriculant; AlgebraII; 2
	(18,'Cgpa','12',2.5), -- Non-Direct Matriculant; AlgebraII; 3
	(18,'PreCalculus','MarkPoints',2.0), -- Non-Direct Matriculant; AlgebraII; 3
	(18,'Mathematics','PromotedRank',3), -- Non-Direct Matriculant; AlgebraII; 3
	(19,'Cgpa','11',3.3), -- Direct Matriculant; MathGE
	(19,'Mathematics','PromotedRank',3), -- Direct Matriculant; MathGE
	(20,'Cgpa','12',3.2), -- Non-Direct Matriculant; MathGE; 1
	(20,'Mathematics','PromotedRank',3), -- Non-Direct Matriculant; MathGE; 1
	(21,'Cgpa','12',2.9), -- Non-Direct Matriculant; MathGE; 2
	(21,'Statistics','MarkPoints',2.0), -- Non-Direct Matriculant; MathGE; 2
	(21,'Mathematics','PromotedRank',3), -- Non-Direct Matriculant; MathGE; 2
	(22,'Cgpa','11',3.0), -- Direct Matriculant; Statistics; 1
	(22,'Mathematics','PromotedRank',3), -- Direct Matriculant; Statistics; 1
	(23,'Cgpa','11',2.3), -- Direct Matriculant; Statistics; 2
	(23,'PreCalculus','MarkPoints',2.3), -- Direct Matriculant; Statistics; 2
	(23,'Mathematics','PromotedRank',3), -- Direct Matriculant; Statistics; 2
	(24,'Cgpa','12',3.0), -- Non-Direct Matriculant; Statistics; 1
	(24,'Mathematics','PromotedRank',3), -- Non-Direct Matriculant; Statistics; 1
	(25,'Cgpa','12',2.6), -- Non-Direct Matriculant; Statistics; 2
	(25,'PreCalculus','MarkPoints',2.0), -- Non-Direct Matriculant; Statistics; 2
	(25,'Mathematics','PromotedRank',3), -- Non-Direct Matriculant; Statistics; 2
	-- Mathematics :: STEM
	(26,'Cgpa','11',3.2), -- Direct Matriculant; CollegeAlgebra; 1
	(26,'Mathematics','PromotedRank',5), -- Direct Matriculant; CollegeAlgebra; 1
	(27,'Cgpa','11',2.9), -- Direct Matriculant; CollegeAlgebra; 2
	(27,'PreCalculus','MarkPoints',2.9), -- Direct Matriculant; CollegeAlgebra; 2
	(27,'Mathematics','PromotedRank',5), -- Direct Matriculant; CollegeAlgebra; 2
	(28,'Cgpa','12',3.2), -- Non-Direct Matriculant; CollegeAlgebra; 1
	(28,'Mathematics','PromotedRank',5), -- Non-Direct Matriculant; CollegeAlgebra; 1
	(29,'Cgpa','12',3.0), -- Non-Direct Matriculant; CollegeAlgebra; 2
	(29,'PreCalculus','MarkPoints',3.0), -- Non-Direct Matriculant; CollegeAlgebra; 2
	(29,'Mathematics','PromotedRank',5), -- Non-Direct Matriculant; CollegeAlgebra; 2
	(30,'Cgpa','12',3.0), -- Non-Direct Matriculant; CollegeAlgebra; 3
	(30,'Statistics','MarkPoints',3.0), -- Non-Direct Matriculant; CollegeAlgebra; 3
	(30,'Mathematics','PromotedRank',5), -- Non-Direct Matriculant; CollegeAlgebra; 3
	(31,'Cgpa','11',3.4), -- Direct Matriculant; Trigonometry; 1
	(31,'Mathematics','PromotedRank',5), -- Direct Matriculant; Trigonometry; 1
	(32,'Cgpa','11',3.0), -- Direct Matriculant; Trigonometry; 2
	(32,'PreCalculus','MarkPoints',2.3), -- Direct Matriculant; Trigonometry; 2
	(32,'Mathematics','PromotedRank',5), -- Direct Matriculant; Trigonometry; 2
	(33,'Cgpa','11',3.0), -- Direct Matriculant; Trigonometry; 3
	(33,'AlgebraII','MarkPoints',3.0), -- Direct Matriculant; Trigonometry; 3
	(33,'Mathematics','PromotedRank',5), -- Direct Matriculant; Trigonometry; 3
	(34,'Cgpa','12',3.3), -- Non-Direct Matriculant; Trigonometry; 1
	(34,'Mathematics','PromotedRank',5), -- Non-Direct Matriculant; Trigonometry; 1
	(35,'Cgpa','12',2.8), -- Non-Direct Matriculant; Trigonometry; 2
	(35,'PreCalculus','MarkPoints',2.0), -- Non-Direct Matriculant; Trigonometry; 2
	(35,'Mathematics','PromotedRank',5), -- Non-Direct Matriculant; Trigonometry; 2
	(36,'Cgpa','11',3.4), -- Direct Matriculant; PreCalculus; 1
	(36,'Mathematics','PromotedRank',5), -- Direct Matriculant; PreCalculus; 1
	(37,'Cgpa','11',2.6), -- Direct Matriculant; PreCalculus; 2
	(37,'Mathematics','PromotedRank',5), -- Direct Matriculant; PreCalculus; 2
	(37,'Calculus','MarkPoints',0.0),
	(38,'Cgpa','12',3.3), -- Non-Direct Matriculant; PreCalculus; 1
	(38,'Mathematics','PromotedRank',5), -- Non-Direct Matriculant; PreCalculus; 1
	(39,'Cgpa','12',3.0), -- Non-Direct Matriculant; PreCalculus; 2
	(39,'Mathematics','PromotedRank',5), -- Non-Direct Matriculant; PreCalculus; 2
	(39,'Mathematics','AlgebraIIScaledScore',340), -- Non-Direct Matriculant; PreCalculus; 2
	(40,'Cgpa','12',3.0), -- Non-Direct Matriculant; PreCalculus; 3
	(40,'Mathematics','PromotedRank',5), -- Non-Direct Matriculant; PreCalculus; 3
	(40,'Calculus','MarkPoints',2.0), -- Non-Direct Matriculant; PreCalculus; 3
	(41,'Cgpa','11',3.6), -- Direct Matriculant; CalculusI; 1
	(41,'Mathematics','PromotedRank',5), -- Direct Matriculant; CalculusI; 1
	(42,'Cgpa','11',3.2), -- Direct Matriculant; CalculusI; 2
	(42,'Mathematics','PromotedRank',5), -- Direct Matriculant; CalculusI; 2
	(42,'PreCalculus','MarkPoints',2.0), -- Direct Matriculant; CalculusI; 2
	(43,'Cgpa','12',3.1), -- Non-Direct Matriculant; CalculusI; 1
	(43,'Mathematics','PromotedRank',7), -- Non-Direct Matriculant; CalculusI; 1
	(43,'Calculus','MarkPoints',0.0), -- Non-Direct Matriculant; CalculusI; 1
	(44,'Cgpa','12',3.5), -- Non-Direct Matriculant; CalculusI; 2
	(44,'Mathematics','PromotedRank',7), -- Non-Direct Matriculant; CalculusI; 2
	-- Reading
	(45,'Cgpa','11',2.7), -- Direct Matriculant; Ubound0ReadingLevelT
	(45,'English11','MarkPoints',3.0), -- Direct Matriculant; Ubound0ReadingLevelT
	(46,'Cgpa','12',3.1), -- Non-Direct Matriculant; Ubound0ReadingLevelT
	(47,'Cgpa','11',2.7), -- Direct Matriculant; Ubound0ReadingLevel0
	(48,'Cgpa','12',2.8), -- Non-Direct Matriculant; Ubound0ReadingLevel0
	(49,'Cgpa','11',2.0), -- Direct Matriculant; Ubound0ReadingLevel1
	(50,'Cgpa','12',2.1), -- Non-Direct Matriculant; Ubound0ReadingLevel1
	(50,'English12','MarkPoints',1.7), -- Non-Direct Matriculant; Ubound0ReadingLevel1
	(51,'Cgpa','11',1.6), -- Direct Matriculant; Ubound0ReadingLevel2
	(52,'Cgpa','12',2.3), -- Non-Direct Matriculant; Ubound0ReadingLevel2
	(53,'Cgpa','11',1.3), -- Direct Matriculant; Ubound0ReadingLevel3
	(54,'Cgpa','12',1.1), -- Non-Direct Matriculant; Ubound0ReadingLevel3; 1
	(55,'English','ScaledScore',286), -- Non-Direct Matriculant; Ubound0ReadingLevel3; 2
	(56,'Cgpa','11',2.8), -- Direct Matriculant; Ubound1ReadingLevelT
	(57,'Cgpa','12',2.5), -- Non-Direct Matriculant; Ubound1ReadingLevelT
	(58,'Cgpa','11',2.3), -- Direct Matriculant; Ubound1ReadingLevel1
	(59,'Cgpa','12',2.0), -- Non-Direct Matriculant; Ubound1ReadingLevel1; 1
	(59,'English12','MarkPoints',2.3), -- Non-Direct Matriculant; Ubound1ReadingLevel1; 1
	(60,'Cgpa','12',2.0), -- Non-Direct Matriculant; Ubound1ReadingLevel1; 2
	(60,'English','ScaledScore',274), -- Non-Direct Matriculant; Ubound1ReadingLevel1; 2
	(61,'Cgpa','11',1.9), -- Direct Matriculant; Ubound1ReadingLevel2
	(62,'English','ScaledScore',310), -- Non-Direct Matriculant; Ubound1ReadingLevel2
	(63,'English','ScaledScore',234), -- Non-Direct Matriculant; Ubound1ReadingLevel3
	(64,'Cgpa','11',2.7), -- Direct Matriculant; Ubound2ReadingLevelT
	(65,'Cgpa','12',2.7), -- Non-Direct Matriculant; Ubound2ReadingLevelT; 1
	(66,'Cgpa','12',2.2), -- Non-Direct Matriculant; Ubound2ReadingLevelT; 2
	(66,'English','ScaledScore',278), -- Non-Direct Matriculant; Ubound2ReadingLevelT; 2
	(67,'Cgpa','11',2.3), -- Direct Matriculant; Ubound2ReadingLevel2
	(68,'Cgpa','12',2.2), -- Non-Direct Matriculant; Ubound2ReadingLevel2
	(69,'Cgpa','11',1.9), -- Direct Matriculant; Ubound2ReadingLevel3
	(70,'English','ScaledScore',234), -- Non-Direct Matriculant; Ubound2ReadingLevel3
	-- Esl
	(71,'Cgpa','11',2.5), -- Direct Matriculant; Ubound0EslLevel0
	(72,'Cgpa','12',2.4), -- Non-Direct Matriculant; Ubound0EslLevel0
	(73,'Cgpa','11',1.5), -- Direct Matriculant; Ubound0EslLevel1
	(74,'Cgpa','12',2.0), -- Non-Direct Matriculant; Ubound0EslLevel1
	(75,'Cgpa','11',1.3), -- Direct Matriculant; Ubound0EslLevel2
	(76,'Cgpa','12',1.8), -- Non-Direct Matriculant; Ubound0EslLevel2
	(77,'Cgpa','11',2.7), -- Direct Matriculant; Ubound1EslLevel1
	(78,'Cgpa','12',2.6), -- Non-Direct Matriculant; Ubound1EslLevel1
	(79,'Cgpa','11',2.2), -- Direct Matriculant; Ubound1EslLevel2
	(80,'Cgpa','12',2.4), -- Non-Direct Matriculant; Ubound1EslLevel2
	(81,'English00','MarkPoints',2.3), -- Direct Matriculant; Ubound1EslLevel3; 1
	(82,'English09','MarkPoints',2.3), -- Direct Matriculant; Ubound1EslLevel3; 2
	(83,'English10','MarkPoints',2.3), -- Direct Matriculant; Ubound1EslLevel3; 3
	(84,'English11','MarkPoints',2.3), -- Direct Matriculant; Ubound1EslLevel3; 3
	(85,'Cgpa','12',1.5), -- Non-Direct Matriculant; Ubound1EslLevel3
	(86,'Cgpa','11',2.8), -- Direct Matriculant; Ubound2EslLevel2
	(87,'Cgpa','12',2.9), -- Non-Direct Matriculant; Ubound2EslLevel2; 1
	(88,'English','ScaledScore',292), -- Non-Direct Matriculant; Ubound2EslLevel2; 2
	(89,'Cgpa','11',2.3), -- Direct Matriculant; Ubound2EslLevel3
	(90,'Cgpa','11',1.8), -- Direct Matriculant; Ubound2EslLevel4
	(91,'Cgpa','12',0.0); -- Non-Direct Matriculant; Ubound2EslLevel4