USE calpass;

GO

IF (object_id('mmap.Course') is not null)
	BEGIN
		DROP TABLE mmap.Course;
	END;

GO

CREATE TABLE
	mmap.Course
	(
		Id int identity(0,1),
		Name varchar(25) not null
	);

GO

ALTER TABLE
	mmap.Course
ADD CONSTRAINT
	PK_Course
PRIMARY KEY CLUSTERED
	(
		Id
	);

ALTER TABLE
	mmap.Course
ADD CONSTRAINT
	UK_Course__Name
UNIQUE
	(
		Name
	);

GO

INSERT
	mmap.Course
	(
		Name
	)
VALUES
	-- English
	('EnglishY'),
	('EnglishA'),
	('EnglishB'),
	('EnglishC'),
	-- Mathematics
	('PreAlgebra'),
	('AlgebraI'),
	('AlgebraII'),
	('MathGE'),
	('Statistics'),
	('CollegeAlgebra'),
	('Trigonometry'),
	('PreCalculus'),
	('CalculusI'),
	-- Reading
	('ReadingM_UboundY'),
	('ReadingY_UboundY'),
	('ReadingA_UboundY'),
	('ReadingB_UboundY'),
	('ReadingC_UboundY'),
	('ReadingM_UboundA'),
	('ReadingA_UboundA'),
	('ReadingB_UboundA'),
	('ReadingC_UboundA'),
	('ReadingM_UboundB'),
	('ReadingB_UboundB'),
	('ReadingC_UboundB'),
	-- Esl
	('EslY_UboundY'),
	('EslA_UboundY'),
	('EslB_UboundY'),
	('EslA_UboundA'),
	('EslB_UboundA'),
	('EslC_UboundA'),
	('EslB_UboundB'),
	('EslC_UboundB'),
	('EslD_UboundB');