USE calpass;

GO

IF (object_id('mmap.Leaf') is not null)
	BEGIN
		DROP TABLE mmap.Leaf;
	END;

GO

CREATE TABLE
	mmap.Leaf
	(
		LeafId int identity(0,1),
		BranchId int not null,
		Entity varchar(25) not null,
		Attribute varchar(25) not null,
		Value decimal(6,3) not null
	);

GO

ALTER TABLE
	mmap.Leaf
ADD CONSTRAINT
	pk_Leaf
PRIMARY KEY CLUSTERED
	(
		LeafId
	);

ALTER TABLE
	mmap.Leaf
ADD CONSTRAINT
	uk_Leaf__BranchId
UNIQUE
	(
		BranchId,
		Entity,
		Attribute
	);

ALTER TABLE
	mmap.Leaf
ADD CONSTRAINT
	fk_Leaf__BranchId
FOREIGN KEY
	(
		BranchId
	)
REFERENCES
	mmap.Branch
	(
		BranchId
	)
ON DELETE CASCADE
ON UPDATE CASCADE;

GO

CREATE NONCLUSTERED INDEX
	ix_Leaf__BranchId
ON
	mmap.Leaf
	(
		BranchId
	)
INCLUDE
	(
		Entity,
		Attribute,
		Value
	);

GO

CREATE NONCLUSTERED INDEX
	ix_Leaf__Entity
ON
	mmap.Leaf
	(
		Entity,
		Attribute
	)
INCLUDE
	(
		BranchId,
		Value
	);

GO

INSERT
	mmap.Leaf
	(
		BranchId,
		Entity,
		Attribute,
		Value
	)
VALUES
	-- 0: EnglishCollege; Non-Direct Matriculant
	(0,'Cgpa','12',2.6),
	-- 1: EnglishCollege; Direct Matriculant
	(1,'Cgpa','11',2.6),
	-- 2: EnglishLevelOne; Non-Direct Matriculant
	(2,'Cgpa','12',2.2),
	(2,'English12','MarkPoints',2.0),
	-- 3: EnglishLevelOne; Direct Matriculant
	(3,'Cgpa','11',2.3),
	-- 4: EnglishLevelTwo; Non-Direct Matriculant; 1
	(4,'Cgpa','12',1.8),
	(4,'English12','MarkPoints',1.0),
	-- 4: EnglishLevelTwo; Non-Direct Matriculant; 2
	(5,'Cgpa','12',1.8),
	(5,'English','ScaledScore',288),
	-- 5: EnglishLevelTwo; Direct Matriculant
	(6,'Cgpa','11',2.0),
	-- 6: EnglishLevelThree; Non-Direct Matriculant; 1
	(7,'Cgpa','12',1.7),
	-- 6: EnglishLevelThree; Non-Direct Matriculant; 2
	(8,'Cgpa','12',1.5),
	(8,'English','ScaledScore',268),
	-- 7: EnglishLevelThree; Direct Matriculant
	(9,'Cgpa','11',1.4),


	-- 8: ReadingTestOut; Non-Direct Matriculant; IsReadingCollege
	(10,'Reading','College',1),
	(10,'Cgpa','12',3.1),
	-- 8: ReadingTestOut; Non-Direct Matriculant; IsReadingLevelOne
	(11,'Reading','LevelOne',1),
	(11,'Cgpa','12',2.5),
	-- 8: ReadingTestOut; Non-Direct Matriculant; IsReadingLevelTwo; 1
	(12,'Reading','LevelTwo',1),
	(12,'Cgpa','12',2.7),
	-- 8: ReadingTestOut; Non-Direct Matriculant; IsReadingLevelTwo; 2
	(13,'Reading','LevelTwo',1),
	(13,'Cgpa','12',2.2),
	(13,'English','ScaledScore',278),
	-- 9: ReadingTestOut; Direct Matriculant; IsReadingCollege
	(14,'Reading','College',1),
	(14,'Cgpa','11',2.7),
	(14,'English11','MarkPoints',3.0),
	-- 9: ReadingTestOut; Direct Matriculant; IsReadingLevelOne
	(15,'Reading','LevelOne',1),
	(15,'Cgpa','11',2.8),
	-- 9: ReadingTestOut; Direct Matriculant; IsReadingLevelTwo
	(16,'Reading','LevelTwo',1),
	(16,'Cgpa','11',2.7),
	-- 10: ReadingCollege; Non-Direct Matriculant; IsReadingCollege
	(17,'Reading','College', 1),
	(17,'Cgpa','12',2.8),
	-- 11: ReadingCollege; Direct Matriculant; IsReadingCollege
	(18,'Reading','College', 1),
	(18,'Cgpa','11',2.7),
	-- 12: ReadingLevelOne; Non-Direct Matriculant; IsReadingCollege
	(19,'Reading','College', 1),
	(19,'Cgpa','12',2.1),
	(19,'English12','MarkPoints',1.7),
	-- 12: ReadingLevelOne; Non-Direct Matriculant; IsReadingLevelOne; 1
	(20,'Reading','LevelOne', 1),
	(20,'Cgpa','12',2.0),
	(20,'English12','MarkPoints',2.3),
	-- 12: ReadingLevelOne; Non-Direct Matriculant; IsReadingLevelOne; 2
	(21,'Reading','LevelOne', 1),
	(21,'Cgpa','12',2.0),
	(21,'English','ScaledScore',274),
	-- 13: ReadingLevelOne; Direct Matriculant; IsReadingCollege
	(22,'Reading','College', 1),
	(22,'Cgpa','11',2.0),
	-- 13: ReadingLevelOne; Direct Matriculant; IsReadingLevelOne
	(23,'Reading','LevelOne', 1),
	(23,'Cgpa','11',2.3),
	-- 14: ReadingLevelTwo; Non-Direct Matriculant; IsReadingCollege
	(24,'Reading','College',1),
	(24,'Cgpa','12',2.3),
	-- 14: ReadingLevelTwo; Non-Direct Matriculant; IsReadingLevelOne
	(25,'Reading','LevelOne', 1),
	(25,'English','ScaledScore',310),
	-- 14: ReadingLevelTwo; Non-Direct Matriculant; IsReadingLevelTwo
	(26,'Reading','LevelTwo', 1),
	(26,'Cgpa','12',2.2),
	-- 15: ReadingLevelTwo; Direct Matriculant; IsReadingCollege
	(27,'Reading','College',1),
	(27,'Cgpa','11',1.6),
	-- 15: ReadingLevelTwo; Direct Matriculant; IsReadingLevelOne
	(28,'Reading','LevelOne', 1),
	(28,'Cgpa','11',1.9),
	-- 15: ReadingLevelTwo; Direct Matriculant; IsReadingLevelTwo
	(29,'Reading','LevelTwo', 1),
	(29,'Cgpa','11',2.3),
	-- 16: ReadingLevelThree; Non-Direct Matriculant; IsReadingCollege; 1
	(30,'Reading','College', 1),
	(30,'Cgpa','12',1.1),
	-- 16: ReadingLevelThree; Non-Direct Matriculant; IsReadingCollege; 2
	(31,'Reading','College', 1),
	(31,'English','ScaledScore',286),
	-- 16: ReadingLevelThree; Non-Direct Matriculant; IsReadingLevelOne
	(32,'Reading','LevelOne', 1),
	(32,'English','ScaledScore',234),
	-- 16: ReadingLevelThree; Non-Direct Matriculant; IsReadingLevelTwo
	(33,'Reading','LevelTwo', 1),
	(33,'English','ScaledScore',236),
	-- 17: ReadingLevelThree; Direct Matriculant; IsReadingCollege
	(34,'Reading','College', 1),
	(34,'Cgpa','11',1.3),
	-- 17: ReadingLevelThree; Direct Matriculant; IsReadingLevelTwo
	(35,'Reading','LevelTwo', 1),
	(35,'Cgpa','11',1.9),


	-- 18: EslCollege; Non-Direct Matriculant; IsEslCollege
	(36,'Esl','College', 1),
	(36,'Cgpa','12',2.4),
	-- 19: EslCollege; Direct Matriculant; IsEslCollege
	(37,'Esl','College', 1),
	(37,'Cgpa','11',2.5),
	-- 20: EslLevelOne; Non-Direct Matriculant; IsEslLevelOne
	(38,'Esl','College', 1),
	(38,'Cgpa','12',2.0),
	-- 20: EslLevelOne; Non-Direct Matriculant; IsEslLevelOne
	(39,'Esl','LevelOne', 1),
	(39,'Cgpa','12',2.6),
	-- 21: EslLevelOne; Direct Matriculant; IsEslCollege
	(40,'Esl','College', 1),
	(40,'Cgpa','11',1.5),
	-- 21: EslLevelOne; Direct Matriculant; IsEslLevelOne
	(41,'Esl','LevelOne', 1),
	(41,'Cgpa','11',2.7),
	-- 22: EslLevelTwo; Non-Direct Matriculant; IsEslCollege
	(42,'Esl','College',1),
	(42,'Cgpa','12',1.8),
	-- 22: EslLevelTwo; Non-Direct Matriculant; IsEslLevelOne
	(43,'Esl','LevelOne',1),
	(43,'Cgpa','12',2.4),
	-- 22: EslLevelTwo; Non-Direct Matriculant; IsEslLevelTwo; 1
	(44,'Esl','LevelTwo',1),
	(44,'Cgpa','12',2.9),
	-- 22: EslLevelTwo; Non-Direct Matriculant; IsEslLevelTwo; 2
	(45,'Esl','LevelTwo',1),
	(45,'English','ScaledScore',292),
	-- 23: EslLevelTwo; Direct Matriculant; IsEslCollege
	(46,'Esl','College',1),
	(46,'Cgpa','11',1.3),
	-- 23: EslLevelTwo; Direct Matriculant; IsEslLevelOne
	(47,'Esl','LevelOne',1),
	(47,'Cgpa','11',2.2),
	-- 23: EslLevelTwo; Direct Matriculant; IsEslLevelTwo
	(48,'Esl','LevelTwo',1),
	(48,'Cgpa','11',2.8),
	-- 24: EslLevelThree; Non-Direct Matriculant; IsEslLevelOne
	(49,'Esl','LevelOne',1),
	(49,'Cgpa','12',1.5),
	-- 24: EslLevelThree; Non-Direct Matriculant; IsEslLevelTwo; 1
	(50,'Esl','LevelTwo',1),
	(50,'Cgpa','12',2.5),
	-- 24: EslLevelThree; Non-Direct Matriculant; IsEslLevelTwo; 2
	(51,'Esl','LevelTwo',1),
	(51,'Cgpa','12',1.9),
	(51,'English12','MarkPoints',2.0),
	-- 25: EslLevelThree; Direct Matriculant; IsEslLevelOne; 1
	(52,'Esl','LevelOne',1),
	(52,'English11','MarkPoints',2.7),
	-- 25: EslLevelThree; Direct Matriculant; IsEslLevelOne; 2
	(53,'Esl','LevelOne',1),
	(53,'English11','MarkPoints',2.3),
	-- 25: EslLevelThree; Direct Matriculant; IsEslLevelOne; 3
	(54,'Esl','LevelOne',1),
	(54,'English00','MarkPoints',2.3),
	-- 25: EslLevelThree; Direct Matriculant; IsEslLevelTwo
	(55,'Esl','LevelTwo',1),
	(55,'Cgpa','11',2.3),


	-- 26: PreAlgebra; Non-Direct Matriculant
	(56,'Cgpa','12',2.1),
	-- 27: PreAlgebra; Direct Matriculant
	(57,'Cgpa','11',2.0),
	-- 28: AlgebraI; Non-Direct Matriculant
	(58,'Cgpa','12',2.5),
	-- 29: AlgebraI; Direct Matriculant
	(59,'Cgpa','11',2.4),
	-- 30: AlgebraII; Non-Direct Matriculant; 1
	(60,'Mathematics','PromotedRank',3),
	(60,'Cgpa','12',2.9),
	-- 30: AlgebraII; Non-Direct Matriculant; 2
	(61,'Mathematics','PromotedRank',3),
	(61,'Cgpa','12',2.5),
	(61,'PreCalculus','MarkPoints',2.0),
	-- 31: AlgebraII; Direct Matriculant
	(62,'Mathematics','PromotedRank',3),
	(62,'Cgpa','11',2.8),
	-- 32: MathGE; Non-Direct Matriculant; 1
	(63,'Mathematics','PromotedRank',3),
	(63,'Cgpa','12',3.2),
	-- 32: MathGE; Non-Direct Matriculant; 2
	(64,'Mathematics','PromotedRank',3),
	(64,'Cgpa','12',2.9),
	(64,'Statistics','MarkPoints',2.0),
	-- 33: MathGE; Direct Matriculant
	(65,'Mathematics','PromotedRank',3),
	(65,'Cgpa','11',3.3),
	-- 34: Statistics; Non-Direct Matriculant; 1
	(66,'Mathematics','PromotedRank',3),
	(66,'Cgpa','12',3.0),
	-- 34: Statistics; Non-Direct Matriculant; 2
	(67,'Mathematics','PromotedRank',3),
	(67,'Cgpa','12',2.6),
	(67,'PreCalculus','MarkPoints',2.0),
	-- 35: Statistics; Direct Matriculant; 1
	(68,'Mathematics','PromotedRank',3),
	(68,'Cgpa','11',3.0),
	-- 35: Statistics; Direct Matriculant; 1
	(69,'Mathematics','PromotedRank',3),
	(69,'Cgpa','11',2.3),
	(69,'PreCalculus','MarkPoints',2.0),
	-- 36: CollegeAlgebra; Non-Direct Matriculant; 1
	(70,'Mathematics','PromotedRank',5),
	(70,'Cgpa','12',3.2),
	-- 36: CollegeAlgebra; Non-Direct Matriculant; 2
	(71,'Mathematics','PromotedRank',5),
	(71,'Cgpa','12',3.0),
	(71,'PreCalculus','MarkPoints',2.0),
	-- 36: CollegeAlgebra; Non-Direct Matriculant; 3
	(72,'Mathematics','PromotedRank',5),
	(72,'Cgpa','12',3.0),
	(72,'Statistics','MarkPoints',2.0),
	-- 37: CollegeAlgebra; Direct Matriculant; 1
	(73,'Mathematics','PromotedRank',5),
	(73,'Cgpa','11',3.2),
	-- 37: CollegeAlgebra; Direct Matriculant; 2
	(74,'Mathematics','PromotedRank',5),
	(74,'Cgpa','11',2.9),
	(74,'PreCalculus','MarkPoints',2.0),
	-- 38: Trigonometry; Non-Direct Matriculant; 1
	(75,'Mathematics','PromotedRank',5),
	(75,'Cgpa','12',3.3),
	-- 38: Trigonometry; Non-Direct Matriculant; 2
	(76,'Mathematics','PromotedRank',5),
	(76,'Cgpa','12',2.8),
	(76,'PreCalculus','MarkPoints',2.0),
	-- 39: Trigonometry; Direct Matriculant; 1
	(77,'Mathematics','PromotedRank',5),
	(77,'Cgpa','11',3.4),
	-- 39: Trigonometry; Direct Matriculant; 2
	(78,'Mathematics','PromotedRank',5),
	(78,'Cgpa','11',3.0),
	(78,'PreCalculus','MarkPoints',2.3),
	-- 39: Trigonometry; Direct Matriculant; 3
	(79,'Mathematics','PromotedRank',5),
	(79,'Cgpa','11',3.0),
	(79,'AlgebraII','MarkPoints',3.0),
	-- 40: PreCalculus; Non-Direct Matriculant; 1
	(80,'Mathematics','PromotedRank',5),
	(80,'Cgpa','12',3.3),
	-- 40: PreCalculus; Non-Direct Matriculant; 2
	(81,'Mathematics','PromotedRank',5),
	(81,'Cgpa','12',3.0),
	(81,'Calculus','MarkPoints',2.0),
	-- 41: PreCalculus; Direct Matriculant; 1
	(82,'Mathematics','PromotedRank',5),
	(82,'Cgpa','11',3.4),
	-- 41: PreCalculus; Direct Matriculant; 2
	(83,'Mathematics','PromotedRank',5),
	(83,'Cgpa','11',2.6),
	(83,'Calculus','MarkPoints',0.0),
	-- 42: CalculusI; Non-Direct Matriculant; 1
	(84,'Mathematics','PromotedRank',7),
	(84,'Cgpa','12',3.5),
	-- 42: CalculusI; Non-Direct Matriculant; 2
	(85,'Mathematics','PromotedRank',7),
	(85,'Cgpa','12',3.1),
	(85,'Calculus','MarkPoints',0.0),
	-- 43: CalculusI; Direct Matriculant; 1
	(86,'Mathematics','PromotedRank',7),
	(86,'Cgpa','11',3.6),
	-- 43: CalculusI; Direct Matriculant; 2
	(87,'Mathematics','PromotedRank',7),
	(87,'Cgpa','11',3.2),
	(87,'PreCalculus','MarkPoints',2.0);