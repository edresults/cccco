USE calpass;

GO

IF (object_id('mmap.ProspectiveCourse') is not null)
	BEGIN
		DROP TABLE mmap.ProspectiveCourse;
	END;

GO

CREATE TABLE
	mmap.ProspectiveCourse
	(
		Id int identity(0,1),
		ProspectiveModelId int not null,
		CourseId int not null
	);

GO

ALTER TABLE
	mmap.ProspectiveCourse
ADD CONSTRAINT
	PK_ProspectiveCourse
PRIMARY KEY CLUSTERED
	(
		Id
	);

ALTER TABLE
	mmap.ProspectiveCourse
ADD CONSTRAINT
	UK_ProspectiveCourse__ProspectiveModelId
UNIQUE
	(
		ProspectiveModelId,
		CourseId
	);

ALTER TABLE
	mmap.ProspectiveCourse
ADD CONSTRAINT
	FK_ProspectiveCourse__ProspectiveModelId
FOREIGN KEY
	(
		ProspectiveModelId
	)
REFERENCES
	mmap.ProspectiveModel
	(
		Id
	)
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE
	mmap.ProspectiveCourse
ADD CONSTRAINT
	FK_ProspectiveCourse__CourseId
FOREIGN KEY
	(
		CourseId
	)
REFERENCES
	mmap.Course
	(
		Id
	)
ON UPDATE CASCADE
ON DELETE CASCADE;

GO

INSERT
	mmap.ProspectiveCourse
	(
		ProspectiveModelId,
		CourseId
	)
VALUES
	-- English
	(0,0), -- EnglishLevel0
	(1,0), -- EnglishLevel0
	(0,1), -- EnglishLevel1
	(1,1), -- EnglishLevel1
	(0,2), -- EnglishLevel2
	(1,2), -- EnglishLevel2
	(0,3), -- EnglishLevel3
	(1,3), -- EnglishLevel3
	-- Mathematics
	(0,4), -- PreAlgebra
	(1,4), -- PreAlgebra
	(0,5), -- AlgebraI
	(1,5), -- AlgebraI
	(0,6), -- AlgebraII
	(1,6), -- AlgebraII
	(0,7), -- MathGE
	(1,7), -- MathGE
	(0,8), -- Statistics
	(1,8), -- Statistics
	(0,9), -- CollegeAlgebra
	(1,9), -- CollegeAlgebra
	(0,10), -- Trigonometry
	(1,10), -- Trigonometry
	(0,11), -- PreCalculus
	(1,11), -- PreCalculus
	(0,12), -- CalculusI
	(1,12), -- CalculusI
	-- Reading
	(0,13), -- ReadingT_UboundY
	(1,13), -- ReadingT_UboundY
	(0,14), -- ReadingY_UboundY
	(1,14), -- ReadingY_UboundY
	(0,15), -- ReadingA_UboundY
	(1,15), -- ReadingA_UboundY
	(0,16), -- ReadingB_UboundY
	(1,16), -- ReadingB_UboundY
	(0,17), -- ReadingC_UboundY
	(1,17), -- ReadingC_UboundY
	(0,18), -- ReadingT_UboundA
	(1,18), -- ReadingT_UboundA
	(0,19), -- ReadingA_UboundA
	(1,19), -- ReadingA_UboundA
	(0,20), -- ReadingB_UboundA
	(1,20), -- ReadingB_UboundA
	(1,21), -- ReadingC_UboundA
	(0,22), -- ReadingT_UboundB
	(1,22), -- ReadingT_UboundB
	(0,23), -- ReadingB_UboundB
	(1,23), -- ReadingB_UboundB
	(0,24), -- ReadingC_UboundB
	(1,24), -- ReadingC_UboundB
	-- Esl
	(0,25), -- EslY_UboundY
	(1,25), -- EslY_UboundY
	(0,26), -- EslA_UboundY
	(1,26), -- EslA_UboundY
	(0,27), -- EslB_UboundY
	(1,27), -- EslB_UboundY
	(0,28), -- EslA_UboundA
	(1,28), -- EslA_UboundA
	(0,29), -- EslB_UboundA
	(1,29), -- EslB_UboundA
	(0,30), -- EslC_UboundA
	(1,30), -- EslC_UboundA
	(0,31), -- EslB_UboundB
	(1,31), -- EslB_UboundB
	(0,32), -- EslC_UboundB
	(0,33); -- EslD_UboundB