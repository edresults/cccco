USE calpass;

GO

IF (object_id('mmap.ProspectivePlacement') is not null)
	BEGIN
		DROP TABLE mmap.ProspectivePlacement;
	END;

GO

CREATE TABLE
	mmap.ProspectivePlacement
	(
		InterSegmentKey binary(64) not null,
		EnglishY bit,
		EnglishA bit,
		EnglishB bit,
		EnglishC bit,
		PreAlgebra bit,
		AlgebraI bit,
		AlgebraII bit,
		MathGE bit,
		[Statistics] bit,
		CollegeAlgebra bit,
		Trigonometry bit,
		PreCalculus bit,
		CalculusI bit,
		ReadingT_UboundY bit,
		ReadingY_UboundY bit,
		ReadingA_UboundY bit,
		ReadingB_UboundY bit,
		ReadingC_UboundY bit,
		ReadingT_UboundA bit,
		ReadingA_UboundA bit,
		ReadingB_UboundA bit,
		ReadingC_UboundA bit,
		ReadingT_UboundB bit,
		ReadingB_UboundB bit,
		ReadingC_UboundB bit,
		EslY_UboundY bit,
		EslA_UboundY bit,
		EslB_UboundY bit,
		EslA_UboundA bit,
		EslB_UboundA bit,
		EslC_UboundA bit,
		EslB_UboundB bit,
		EslC_UboundB bit,
		EslD_UboundB bit
	);

GO

ALTER TABLE
	mmap.ProspectivePlacement
ADD CONSTRAINT
	PK_ProspectivePlacement
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey
	);