USE calpass;

GO

IF (object_id('mmap.ValidationSelfReport') is not null)
	BEGIN
		DROP TABLE mmap.ValidationSelfReport;
	END;

GO

CREATE TABLE
	mmap.ValidationSelfReport
	(
		InterSegmentKey binary(64) not null,
		CumulativeGpaTranscript decimal(4,3),
		CumulativeGpaSelfReport decimal(4,3)
	);

GO

ALTER TABLE
	mmap.ValidationSelfReport
ADD CONSTRAINT
	pk_ValidateSelfReport
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey
	);