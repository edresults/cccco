DROP TABLE mmap.CotcIrtMathematics;

GO

SELECT
	ccc.YearTermCode,
	ccc.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage,
	-- high school
	AlgebraI_1_MarkCategory     = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'AlgebraI' then rcc.MarkCategory end), -99),
	AlgebraI_2_MarkCategory     = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'AlgebraI' then rcc.MarkCategory end), -99),
	AlgebraII_1_MarkCategory    = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'AlgebraII' then rcc.MarkCategory end), -99),
	AlgebraII_2_MarkCategory    = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'AlgebraII' then rcc.MarkCategory end), -99),
	Arithmetic_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Arithmetic' then rcc.MarkCategory end), -99),
	Arithmetic_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Arithmetic' then rcc.MarkCategory end), -99),
	Calculus_1_MarkCategory     = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Calculus' and rcc.CourseCode not in ('2480', '2481') then rcc.MarkCategory end), -99),
	Calculus_2_MarkCategory     = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Calculus' and rcc.CourseCode not in ('2480', '2481') then rcc.MarkCategory end), -99),
	CalculusAB_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2480' then rcc.MarkCategory end), -99),
	CalculusAB_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2480' then rcc.MarkCategory end), -99),
	CalculusBC_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2481' then rcc.MarkCategory end), -99),
	CalculusBC_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2481' then rcc.MarkCategory end), -99),
	Geometry_1_MarkCategory     = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Geometry' then rcc.MarkCategory end), -99),
	Geometry_2_MarkCategory     = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Geometry' then rcc.MarkCategory end), -99),
	PreAlgebra_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'PreAlgebra' then rcc.MarkCategory end), -99),
	PreAlgebra_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'PreAlgebra' then rcc.MarkCategory end), -99),
	PreCalculus_1_MarkCategory  = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'PreCalculus' then rcc.MarkCategory end), -99),
	PreCalculus_2_MarkCategory  = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'PreCalculus' then rcc.MarkCategory end), -99),
	Statistics_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Statistics' and rcc.CourseCode != '2483' then rcc.MarkCategory end), -99),
	Statistics_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Statistics' and rcc.CourseCode != '2483' then rcc.MarkCategory end), -99),
	StatisticsAP_1_MarkCategory = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Statistics' and rcc.CourseCode  = '2483' then rcc.MarkCategory end), -99),
	StatisticsAP_2_MarkCategory = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Statistics' and rcc.CourseCode  = '2483' then rcc.MarkCategory end), -99),
	Trigonometry_1_MarkCategory = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Trigonometry' then rcc.MarkCategory end), -99),
	Trigonometry_2_MarkCategory = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Trigonometry' then rcc.MarkCategory end), -99),
	-- college
	cc_00_course_grade_category = isnull(max(case when ccc.CourseLevel = 'Y' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end end), -99),
	cc_01_course_grade_category = isnull(max(case when ccc.CourseLevel = 'A' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end end), -99),
	cc_02_course_grade_category = isnull(max(case when ccc.CourseLevel = 'B' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end end), -99),
	cc_03_course_grade_category = isnull(max(case when ccc.CourseLevel = 'C' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end end), -99),
	cc_04_course_grade_category = isnull(max(case when ccc.CourseLevel = 'D' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end end), -99),
	-- College Mathematics Content
	CC_GE_MATH = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH FOR%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH%LIB%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH%ELEM%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH TOPICS%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATHEMATICAL%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATHEMATICS%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%THE IDEAS%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%LIBERAL%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SURVEY%' and 
				case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%CALC%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%CONCEPTS%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%IDEAS%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%TEACH%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%LIFE%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BUS%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BRIEF%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SOC%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BIO%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SHORT%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			else -99
		end), -99),
	CC_STATISTICS = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseId    end like '%STA%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%STA%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO STA%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM PROB%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM. PROB%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM STA%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEMENTARY STA%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEMNTRY STA%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO PROB%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO TO PROB%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO APPLIED STATS%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%HONORS STA%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			else -99
		end), -99),
	CC_COLL_ALG =
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%COLL%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ADVANCED ALG%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ALGEBRA FOR CALC%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%PRECALC%' then -99
			else -99
		end), -99),
	CC_PRE_CALC = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%PRE%CALC%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			else -99
		end), -99),
	CC_CALC_I = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%CALC%' then
				case
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%PRE%' then -99
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%II%' then -99
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%2%' then -99
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%3%' then -99
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%4A%' then -99
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%DIFF%' then -99
					else case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
				end
			else -99
		end), -99),
	CC_CALC_II = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%CALC%'
				and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%PRE%' then
				case
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%II%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%2%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%3%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
					else -99
				end
			else -99
		end), -99),
	CC_BUS_MATH = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BUS%'
				and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%FINITE%'
				and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%DISCRETE%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			else -99
		end), -99),
	CC_DIFF_EQ = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%DIFF%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			else -99
		end), -99),
	CC_TRIG = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%Trig%' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end
			else -99
		end), -99),
	CC_TRANSFER_MATH_TYPE = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end = 'Y' then
				case
					-- CC_GE_MATH
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH FOR%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH/LIB%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH/ELEM%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH:ELEM%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH TOPICS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATHEMATICAL%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATHEMATICS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%THE IDEAS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%LIBERAL%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SURVEY%'
						and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%CALC%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%CONCEPTS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%IDEAS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%TEACH%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%LIFE%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BUS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BRIEF%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SOC%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BIO%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SHORT%' then 0
					-- CC_STATISTICS
					when case when ccc.FirstSelector = 1 then ccc.CourseId    end like '%STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM PROB%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM. PROB%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEMENTARY STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEMNTRY STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO PROB%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO TO PROB%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO APPLIED STATS%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%HONORS STA%' then 1
					-- CC_COLL_ALG
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%COLL%' then 2
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ADVANCED ALG%' then 2
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ALGEBRA FOR CALC%' then 2
					-- CC_PRE_CALC & CC_CALC_I & CC_CALC_II
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%CALC%' then
						case
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%PRE%' then 3
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%II%' then 5
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%2%' then 5
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%3%' then 5
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%4A%' then 5
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%DIFF%' then 7
							else 4
						end
					-- CC_BUS_MATH
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BUS%'
						and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%FINITE%'
						and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%DISCRETE%' then 1
					-- CC_DIFF_EQ
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%DIFF%' then 7
					-- CC_LINEAR_ALGEBRA not in variables
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%LINEAR%' then 8
					else -99
				end
			else -99
		end), -99)
INTO
	mmap.CotcIrtMathematics
FROM
	mmap.CollegeCourseContent ccc
	left outer join
	(
		SELECT
			rp.InterSegmentKey,
			OverallCumulativeGradePointAverage = max(case when DepartmentCode = 0 then CumulativeGradePointAverage end),
			SubjectCumulativeGradePointAverage = max(case when DepartmentCode = 18 then CumulativeGradePointAverage end),
			WithoutSubjectOverallCumulativeGradePointAverage = 
				convert(
					decimal(9,3),
					isnull(
						case
							when max(case when DepartmentCode = 0 then CumulativeQualityPoints end) - max(case when DepartmentCode = 18 then CumulativeQualityPoints end) <= 0 then 0
							else max(case when DepartmentCode = 0 then CumulativeQualityPoints end) - max(case when DepartmentCode = 18 then CumulativeQualityPoints end)
						end
						/
						nullif(
							case
								when max(case when DepartmentCode = 0 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 18 then CumulativeCreditAttempted end) <= 0 then 0
								else max(case when DepartmentCode = 0 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 18 then CumulativeCreditAttempted end)
							end,
							0
						)
					,0)
				)
		FROM
			mmap.RetrospectivePerformance rp
		WHERE
			rp.DepartmentCode in (0, 18)
			and rp.IsLast = 1
		GROUP BY
			rp.InterSegmentKey
	) rp
		on rp.InterSegmentkey = ccc.InterSegmentkey
	left outer join
	mmap.RetrospectiveCourseContent rcc
		on rcc.InterSegmentKey = ccc.InterSegmentKey
		and rcc.DepartmentCode = 18
		and rcc.ContentSelector in (1,2)
WHERE
	ccc.TopCode = '170100'
	and ccc.FirstSelector = 1
	and ccc.CourseLevel = 'Y'
	and ccc.CollegeId = '661'
	and ccc.CollegeCounter = 1
GROUP BY
	ccc.YearTermCode,
	ccc.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage;

DELETE FROM mmap.CotcIrtMathematics WHERE OverallCumulativeGradePointAverage > 4;
DELETE FROM mmap.CotcIrtMathematics WHERE SubjectCumulativeGradePointAverage > 4;
DELETE FROM mmap.CotcIrtMathematics WHERE WithoutSubjectOverallCumulativeGradePointAverage > 4;

ALTER TABLE
	mmap.CotcIrtMathematics
ADD CONSTRAINT
	PK_CotcIrtMathematics
PRIMARY KEY CLUSTERED
	(
		InterSegmentkey
	);

ALTER TABLE
	mmap.CotcIrtMathematics
ADD
	IsTransferStem smallint;

ALTER TABLE
	mmap.CotcIrtMathematics
ADD
	IsTransferNonStem smallint;

UPDATE
	mmap.CotcIrtMathematics
SET
	IsTransferStem = 
		case
			when CC_COLL_ALG != -99 then CC_COLL_ALG
			when CC_PRE_CALC != -99 then CC_PRE_CALC
			when CC_CALC_I != -99 then CC_CALC_I
			when CC_CALC_II != -99 then CC_CALC_II
			when CC_DIFF_EQ != -99 then CC_DIFF_EQ
			when CC_BUS_MATH != -99 then CC_BUS_MATH
			else -99
		end,
	IsTransferNonStem = 
		case
			when CC_GE_MATH != -99 then CC_GE_MATH
			when CC_STATISTICS != -99 then CC_STATISTICS
			else -99
		end;