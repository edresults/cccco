USE calpass;

GO

IF (object_id('mmap.ProspectiveEsl') is not null)
	BEGIN
		DROP TABLE mmap.ProspectiveEsl;
	END;

GO

CREATE TABLE
	mmap.ProspectiveEsl
	(
		CollegeCode char(3) not null,
		CourseLevelMax char(1) not null
	);

GO

ALTER TABLE
	mmap.ProspectiveEsl
ADD CONSTRAINT
	pk_ProspectiveEsl__CollegeCode
PRIMARY KEY CLUSTERED
	(
		CollegeCode
	);

GO

INSERT INTO
	mmap.ProspectiveEsl
	(
		CollegeCode,
		CourseLevelMax
	)
VALUES
	('892','A');