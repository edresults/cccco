USE calpass;

GO

IF (object_id('mmap.K12Placement') is not null)
	BEGIN
		DROP TABLE mmap.K12Placement;
	END;

GO

CREATE TABLE
	mmap.K12Placement
	(
		StudentStateId   char(10) not null,
		EnglishY         bit      not null,
		EnglishA         bit      not null,
		EnglishB         bit      not null,
		EnglishC         bit      not null,
		PreAlgebra       bit      not null,
		AlgebraI         bit      not null,
		AlgebraII        bit      not null,
		MathGE           bit      not null,
		[Statistics]     bit      not null,
		CollegeAlgebra   bit      not null,
		Trigonometry     bit      not null,
		PreCalculus      bit      not null,
		CalculusI        bit      not null,
		ReadingM_UboundY bit      not null,
		ReadingY_UboundY bit      not null,
		ReadingA_UboundY bit      not null,
		ReadingB_UboundY bit      not null,
		ReadingC_UboundY bit      not null,
		ReadingM_UboundA bit      not null,
		ReadingA_UboundA bit      not null,
		ReadingB_UboundA bit      not null,
		ReadingC_UboundA bit      not null,
		ReadingM_UboundB bit      not null,
		ReadingB_UboundB bit      not null,
		ReadingC_UboundB bit      not null,
		EslY_UboundY     bit      not null,
		EslA_UboundY     bit      not null,
		EslB_UboundY     bit      not null,
		EslA_UboundA     bit      not null,
		EslB_UboundA     bit      not null,
		EslC_UboundA     bit      not null,
		EslB_UboundB     bit      not null,
		EslC_UboundB     bit      not null,
		EslD_UboundB     bit      not null,
		IsEscrow         bit      not null
	);

GO

ALTER TABLE
	mmap.K12Placement
ADD CONSTRAINT
	pk_K12Placement__StudentStateId
PRIMARY KEY CLUSTERED
	(
		StudentStateId
	);

CREATE NONCLUSTERED INDEX
	IX_K12Placement__IsEscrow
ON
	mmap.K12Placement
	(
		IsEscrow
	);