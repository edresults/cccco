USE calpass;

GO

IF (object_id('mmap.ProspectiveRead') is not null)
	BEGIN
		DROP TABLE mmap.ProspectiveRead;
	END;

GO

CREATE TABLE
	mmap.ProspectiveRead
	(
		CollegeCode char(3) not null,
		CourseLevelMax char(1) not null
	);

GO

ALTER TABLE
	mmap.ProspectiveRead
ADD CONSTRAINT
	pk_ProspectiveRead__CollegeCode
PRIMARY KEY CLUSTERED
	(
		CollegeCode
	);;