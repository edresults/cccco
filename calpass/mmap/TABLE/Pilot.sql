USE calpass;

GO

IF (object_id('mmap.Pilot') is not null)
	BEGIN
		DROP TABLE mmap.Pilot;
	END;

GO


CREATE TABLE
	mmap.Pilot
	(
		PilotCode int not null,
		LocalFolder nvarchar(255) not null,
		SftpFolder nvarchar(255) not null
	);

GO

ALTER TABLE
	mmap.Pilot
ADD CONSTRAINT
	pk_Pilot__PilotCode
PRIMARY KEY CLUSTERED
	(
		PilotCode
	);

GO

INSERT INTO
	mmap.Pilot
	(
		PilotCode,
		LocalFolder,
		SftpFolder
	)
VALUES
	(170797,'\\10.11.5.61\SFTPRoot\msac\','\\10.11.5.61\SFTPRoot\msac\'),
	(170746,'\\10.11.5.61\SFTPRoot\contracosta\','\\10.11.5.61\SFTPRoot\contracosta\'),
	(170828,'\\10.11.5.61\SFTPRoot\sanmateo\','\\10.11.5.61\SFTPRoot\sanmateo\'),
	(170761,'\\10.11.5.61\SFTPRoot\foothill\','\\10.11.5.61\SFTPRoot\foothill\'),
	(170728,'\\10.11.5.61\SFTPRoot\clpccd\','\\10.11.5.61\SFTPRoot\clpccd\'),
	(170772,'\\10.11.5.61\SFTPRoot\kernccd\','\\10.11.5.61\SFTPRoot\kernccd\'),
	(170840,'\\10.11.5.61\SFTPRoot\statecenter\','\\10.11.5.61\SFTPRoot\statecenter\'),
	(170779,'\\10.11.5.61\SFTPRoot\laccd\','\\10.11.5.61\SFTPRoot\laccd\'),
	(170801,'\\10.11.5.61\SFTPRoot\northorange\','\\10.11.5.61\SFTPRoot\northorange\'),
	(170838,'\\10.11.5.61\SFTPRoot\southorange\','\\10.11.5.61\SFTPRoot\southorange\'),
	(170770,'\\10.11.5.61\SFTPRoot\imperialvalley\','\\10.11.5.61\SFTPRoot\imperialvalley\'),
	(170792,'\\10.11.5.61\SFTPRoot\MiraCosta\','\\10.11.5.61\SFTPRoot\MiraCosta\'),
	(170806,'\\10.11.5.61\SFTPRoot\palomar\','\\10.11.5.61\SFTPRoot\palomar\'),
	(170819,'\\10.11.5.61\SFTPRoot\sdccd\','\\10.11.5.61\SFTPRoot\sdccd\'),
	(170820,'\\10.11.5.61\SFTPRoot\sdccd\','\\10.11.5.61\SFTPRoot\sdccd\'),
	(170821,'\\10.11.5.61\SFTPRoot\sdccd\','\\10.11.5.61\SFTPRoot\sdccd\'),
	(170822,'\\10.11.5.61\SFTPRoot\sdccd\','\\10.11.5.61\SFTPRoot\sdccd\'),
	(170823,'\\10.11.5.61\SFTPRoot\sdccd\','\\10.11.5.61\SFTPRoot\sdccd\'),
	(175567,'\\10.11.5.61\SFTPRoot\sdccd\','\\10.11.5.61\SFTPRoot\sdccd\'),
	(170722,'\\10.11.5.61\SFTPRoot\buttecoll\','\\10.11.5.61\SFTPRoot\buttecoll\'),
	(170740,'\\10.11.5.61\SFTPRoot\redwoods\','\\10.11.5.61\SFTPRoot\redwoods\'),
	(170718,'\\10.11.5.61\SFTPRoot\losrios\','\\10.11.5.61\SFTPRoot\losrios\'),
	(170748,'\\10.11.5.61\SFTPRoot\cosumnesriver\','\\10.11.5.61\SFTPRoot\cosumnesriver\'),
	(170815,'\\10.11.5.61\SFTPRoot\losrios\','\\10.11.5.61\SFTPRoot\losrios\'),
	(170759,'\\10.11.5.61\SFTPRoot\losrios\','\\10.11.5.61\SFTPRoot\losrios\'),
	(170835,'\\10.11.5.61\SFTPRoot\sierra\','\\10.11.5.61\SFTPRoot\sierra\'),
	(170745,'\\10.11.5.61\SFTPRoot\contracosta\','\\10.11.5.61\SFTPRoot\contracosta\'),
	(170754,'\\10.11.5.61\SFTPRoot\contracosta\','\\10.11.5.61\SFTPRoot\contracosta\'),
	(170787,'\\10.11.5.61\SFTPRoot\contracosta\','\\10.11.5.61\SFTPRoot\contracosta\'),
	(170731,'\\10.11.5.61\SFTPRoot\ccsf\','\\10.11.5.61\SFTPRoot\ccsf\'),
	(170724,'\\10.11.5.61\SFTPRoot\sanmateo\','\\10.11.5.61\SFTPRoot\sanmateo\'),
	(170737,'\\10.11.5.61\SFTPRoot\sanmateo\','\\10.11.5.61\SFTPRoot\sanmateo\'),
	(170836,'\\10.11.5.61\SFTPRoot\sanmateo\','\\10.11.5.61\SFTPRoot\sanmateo\'),
	(170723,'\\10.11.5.61\SFTPRoot\cabrillo\','\\10.11.5.61\SFTPRoot\cabrillo\'),
	(170753,'\\10.11.5.61\SFTPRoot\foothill\','\\10.11.5.61\SFTPRoot\foothill\'),
	(170760,'\\10.11.5.61\SFTPRoot\foothill\','\\10.11.5.61\SFTPRoot\foothill\'),
	(170802,'\\10.11.5.61\SFTPRoot\Ohlone\','\\10.11.5.61\SFTPRoot\Ohlone\'),
	(170769,'\\10.11.5.61\SFTPRoot\hartnell\','\\10.11.5.61\SFTPRoot\hartnell\'),
	(170757,'\\10.11.5.61\SFTPRoot\sjccd\','\\10.11.5.61\SFTPRoot\sjccd\'),
	(170826,'\\10.11.5.61\SFTPRoot\sjccd\','\\10.11.5.61\SFTPRoot\sjccd\'),
	(170775,'\\10.11.5.61\SFTPRoot\clpccd\','\\10.11.5.61\SFTPRoot\clpccd\'),
	(170727,'\\10.11.5.61\SFTPRoot\clpccd\','\\10.11.5.61\SFTPRoot\clpccd\'),
	(170720,'\\10.11.5.61\SFTPRoot\kernccd\','\\10.11.5.61\SFTPRoot\kernccd\'),
	(170809,'\\10.11.5.61\SFTPRoot\kernccd\','\\10.11.5.61\SFTPRoot\kernccd\'),
	(170825,'\\10.11.5.61\SFTPRoot\sjdc\','\\10.11.5.61\SFTPRoot\sjdc\'),
	(170741,'\\10.11.5.61\SFTPRoot\Sequoias\','\\10.11.5.61\SFTPRoot\Sequoias\'),
	(170762,'\\10.11.5.61\SFTPRoot\statecenter\','\\10.11.5.61\SFTPRoot\statecenter\'),
	(170846,'\\10.11.5.61\SFTPRoot\whccd\','\\10.11.5.61\SFTPRoot\whccd\'),
	(172262,'\\10.11.5.61\SFTPRoot\whccd\','\\10.11.5.61\SFTPRoot\whccd\'),
	(172264,'\\10.11.5.61\SFTPRoot\whccd\','\\10.11.5.61\SFTPRoot\whccd\'),
	(170717,'\\10.11.5.61\SFTPRoot\allanhancock\','\\10.11.5.61\SFTPRoot\allanhancock\'),
	(170830,'\\10.11.5.61\SFTPRoot\sbcc\','\\10.11.5.61\SFTPRoot\sbcc\'),
	(170778,'\\10.11.5.61\SFTPRoot\laccd\','\\10.11.5.61\SFTPRoot\laccd\'),
	(170780,'\\10.11.5.61\SFTPRoot\laccd\','\\10.11.5.61\SFTPRoot\laccd\'),
	(170782,'\\10.11.5.61\SFTPRoot\laccd\','\\10.11.5.61\SFTPRoot\laccd\'),
	(170783,'\\10.11.5.61\SFTPRoot\laccd\','\\10.11.5.61\SFTPRoot\laccd\'),
	(170784,'\\10.11.5.61\SFTPRoot\laccd\','\\10.11.5.61\SFTPRoot\laccd\'),
	(170785,'\\10.11.5.61\SFTPRoot\laccd\','\\10.11.5.61\SFTPRoot\laccd\'),
	(170786,'\\10.11.5.61\SFTPRoot\laccd\','\\10.11.5.61\SFTPRoot\laccd\'),
	(170755,'\\10.11.5.61\SFTPRoot\laccd\','\\10.11.5.61\SFTPRoot\laccd\'),
	(170847,'\\10.11.5.61\SFTPRoot\laccd\','\\10.11.5.61\SFTPRoot\laccd\'),
	(170781,'\\10.11.5.61\SFTPRoot\laccd\','\\10.11.5.61\SFTPRoot\laccd\'),
	(170831,'\\10.11.5.61\SFTPRoot\santamonica\','\\10.11.5.61\SFTPRoot\santamonica\'),
	(170725,'\\10.11.5.61\SFTPRoot\cerritos\','\\10.11.5.61\SFTPRoot\cerritos\'),
	(170733,'\\10.11.5.61\SFTPRoot\coastccd\','\\10.11.5.61\SFTPRoot\coastccd\'),
	(170734,'\\10.11.5.61\SFTPRoot\coastccd\','\\10.11.5.61\SFTPRoot\coastccd\'),
	(170766,'\\10.11.5.61\SFTPRoot\coastccd\','\\10.11.5.61\SFTPRoot\coastccd\'),
	(170803,'\\10.11.5.61\SFTPRoot\coastccd\','\\10.11.5.61\SFTPRoot\coastccd\'),
	(170777,'\\10.11.5.61\SFTPRoot\lbcc\','\\10.11.5.61\SFTPRoot\lbcc\'),
	(170752,'\\10.11.5.61\SFTPRoot\northorange\','\\10.11.5.61\SFTPRoot\northorange\'),
	(170763,'\\10.11.5.61\SFTPRoot\northorange\','\\10.11.5.61\SFTPRoot\northorange\'),
	(170829,'\\10.11.5.61\SFTPRoot\rsccd\','\\10.11.5.61\SFTPRoot\rsccd\'),
	(170833,'\\10.11.5.61\SFTPRoot\rsccd\','\\10.11.5.61\SFTPRoot\rsccd\'),
	(170813,'\\10.11.5.61\SFTPRoot\riohondo\','\\10.11.5.61\SFTPRoot\riohondo\'),
	(170816,'\\10.11.5.61\SFTPRoot\southorange\','\\10.11.5.61\SFTPRoot\southorange\'),
	(170771,'\\10.11.5.61\SFTPRoot\southorange\','\\10.11.5.61\SFTPRoot\southorange\'),
	(170739,'\\10.11.5.61\SFTPRoot\desert\','\\10.11.5.61\SFTPRoot\desert\'),
	(175135,'\\10.11.5.61\SFTPRoot\RiversideCCD\','\\10.11.5.61\SFTPRoot\RiversideCCD\'),
	(175136,'\\10.11.5.61\SFTPRoot\RiversideCCD\','\\10.11.5.61\SFTPRoot\RiversideCCD\'),
	(170730,'\\10.11.5.61\SFTPRoot\Citrus\','\\10.11.5.61\SFTPRoot\citrus\'),
	(170795,'\\10.11.5.61\SFTPRoot\monterey\','\\10.11.5.61\SFTPRoot\monterey\'),
	(170793,'\\10.11.5.61\SFTPRoot\WestValleyCCD\','\\10.11.5.61\SFTPRoot\WestValleyCCD\'),
	(170849,'\\10.11.5.61\SFTPRoot\WestValleyCCD\','\\10.11.5.61\SFTPRoot\WestValleyCCD\'),
	(170812,'\\10.11.5.61\SFTPRoot\Reedley\','\\10.11.5.61\SFTPRoot\Reedley\'),
	(170832,'\\10.11.5.61\SFTPRoot\santarosa\','\\10.11.5.61\SFTPRoot\santarosa\');

INSERT
	mmap.Pilot
	(
		PilotCode,
		LocalFolder,
		SftpFolder
	)
VALUES
	(
		170848,
		'\\10.11.5.61\SFTPRoot\WestValleyCCD\',
		'\\10.11.5.61\SFTPRoot\WestValleyCCD\'
	);

INSERT
	mmap.Pilot
	(
		PilotCode,
		LocalFolder,
		SftpFolder
	)
VALUES
	(
		170756,
		'\\10.11.5.61\SFTPRoot\elcamino\',
		'\\10.11.5.61\SFTPRoot\elcamino\'
	);

INSERT
	mmap.Pilot
	(
		PilotCode,
		LocalFolder,
		SftpFolder
	)
VALUES
	(
		170808,
		'\\10.11.5.61\SFTPRoot\peraltaccd\',
		'\\10.11.5.61\SFTPRoot\peraltaccd\'
	);


INSERT
	mmap.Pilot
	(
		PilotCode,
		LocalFolder,
		SftpFolder
	)
VALUES
	(
		176005,
		'\\10.11.5.61\SFTPRoot\statecenter\',
		'\\10.11.5.61\SFTPRoot\statecenter\'
	);


INSERT
	mmap.Pilot
	(
		PilotCode,
		LocalFolder,
		SftpFolder
	)
VALUES
	(
		170765,
		'\\10.11.5.61\SFTPRoot\glendaleccd\',
		'\\10.11.5.61\SFTPRoot\glendaleccd\'
	);


INSERT
	mmap.Pilot
	(
		PilotCode,
		LocalFolder,
		SftpFolder
	)
VALUES
	(
		170765,
		'\\10.11.5.61\SFTPRoot\glendaleccd\',
		'\\10.11.5.61\SFTPRoot\glendaleccd\'
	);


INSERT
	mmap.Pilot
	(
		PilotCode,
		LocalFolder,
		SftpFolder
	)
VALUES
	(
		170841,
		'\\10.11.5.61\SFTPRoot\taft\',
		'\\10.11.5.61\SFTPRoot\taft\'
	);