USE calpass;

GO

IF (object_id('mmap.ValidationAssessment_TestSubject') is not null)
	BEGIN
		DROP TABLE mmap.ValidationAssessment_TestSubject;
	END;

GO

CREATE TABLE
	mmap.ValidationAssessment_TestSubject
	(
		TestSubjectCode varchar(4) not null,
		Description varchar(255)
	);

ALTER TABLE
	mmap.ValidationAssessment_TestSubject
ADD CONSTRAINT
	pk_ValidationAssessment_TestSubject
PRIMARY KEY CLUSTERED
	(
		TestSubjectCode
	);

GO

INSERT INTO
	mmap.ValidationAssessment_TestSubject
	(
		TestSubjectCode,
		Description
	)
VALUES
	('ENGL','Assessment for placing student in an English sequence'),
	('MATH','Assessment for placing student in a math sequence'),
	('READ','Assessment for placing student into a reading sequence'),
	('ESL','Assessment for placing student into an ESL sequence');