USE calpass;

GO

IF (object_id('mmap.Tree') is not null)
	BEGIN
		DROP TABLE mmap.Tree;
	END;

GO

CREATE TABLE
	mmap.Tree
	(
		TreeId int identity(0,1),
		ModelId int not null,
		CourseCode varchar(25) not null,
		Description varchar(255) not null
	);

GO

ALTER TABLE
	mmap.Tree
ADD CONSTRAINT
	pk_Tree
PRIMARY KEY CLUSTERED
	(
		TreeId
	);

ALTER TABLE
	mmap.Tree
ADD CONSTRAINT
	uk_Tree__ModelId
UNIQUE
	(
		ModelId,
		CourseCode
	);

ALTER TABLE
	mmap.Tree
ADD CONSTRAINT
	fk_Tree__ModelId
FOREIGN KEY
	(
		ModelId
	)
REFERENCES
	mmap.Model
	(
		ModelId
	)
ON UPDATE CASCADE
ON DELETE CASCADE;

GO

INSERT
	mmap.Tree
	(
		ModelId,
		CourseCode,
		Description
	)
VALUES
	(0,'EnglishCollege','College English'),
	(1,'EnglishCollege','College English'),
	(0,'EnglishLevelOne','One Level Below College English'),
	(1,'EnglishLevelOne','One Level Below College English'),
	(0,'EnglishLevelTwo','Two Levels Below College English'),
	(1,'EnglishLevelTwo','Two Levels Below College English'),
	(0,'EnglishLevelThree','Three Levels Below College English'),
	(1,'EnglishLevelThree','Three Levels Below College English'),
	(0,'ReadingTestOut','Test Out Of Reading'),
	(1,'ReadingTestOut','Test Out Of Reading'),
	(0,'ReadingCollege','College Reading'),
	(1,'ReadingCollege','College Reading'),
	(0,'ReadingLevelOne','One Level Below College Reading'),
	(1,'ReadingLevelOne','One Level Below College Reading'),
	(0,'ReadingLevelTwo','Two Levels Below College Reading'),
	(1,'ReadingLevelTwo','Two Levels Below College Reading'),
	(0,'ReadingLevelThree','Three Levels Below College Reading'),
	(1,'ReadingLevelThree','Three Levels Below College Reading'),
	(0,'EslCollege','College ESL'),
	(1,'EslCollege','College ESL'),
	(0,'EslLevelOne','One Level Below College ESL'),
	(1,'EslLevelOne','One Level Below College ESL'),
	(0,'EslLevelTwo','Two Levels Below College ESL'),
	(1,'EslLevelTwo','Two Levels Below College ESL'),
	(0,'EslLevelThree','Three Levels Below College ESL'),
	(1,'EslLevelThree','Three Levels Below College ESL'),
	(0,'PreAlgebra','PreAlgebra'),
	(1,'PreAlgebra','PreAlgebra'),
	(0,'AlgebraI','AlgebraI'),
	(1,'AlgebraI','AlgebraI'),
	(0,'AlgebraII','AlgebraII'),
	(1,'AlgebraII','AlgebraII'),
	(0,'MathGE','General Education Math'),
	(1,'MathGE','General Education Math'),
	(0,'Statistics','Statistics'),
	(1,'Statistics','Statistics'),
	(0,'CollegeAlgebra','College Algebra'),
	(1,'CollegeAlgebra','College Algebra'),
	(0,'Trigonometry','Trigonometry'),
	(1,'Trigonometry','Trigonometry'),
	(0,'PreCalculus','PreCalculus'),
	(1,'PreCalculus','PreCalculus'),
	(0,'CalculusI','CalculusI'),
	(1,'CalculusI','CalculusI'),
	(0,'CalculusII','CalculusII'),
	(1,'CalculusII','CalculusII');