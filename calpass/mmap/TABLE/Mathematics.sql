USE calpass;

GO

IF (object_id('mmap.Mathematics') is not null)
	BEGIN
		DROP TABLE mmap.Mathematics;
	END;

GO

CREATE TABLE
	mmap.Mathematics
	(
		CourseCode char(4) not null,
		ContentCode varchar(25) not null,
		Rank tinyint not null,
		IsPrimary bit not null
	);

GO

ALTER TABLE
	mmap.Mathematics
ADD CONSTRAINT
	PK_Mathematics
PRIMARY KEY CLUSTERED
	(
		CourseCode,
		ContentCode
	);

GO

INSERT INTO
	mmap.Mathematics
	(
		CourseCode,
		ContentCode,
		Rank,
		IsPrimary
	)
VALUES
	('2400','Arithmetic',1,1),
	('2401','Arithmetic',1,1),
	('2402','Arithmetic',1,1),
	('2403','AlgebraI',3,1),
	('2404','AlgebraII',5,1),
	('2405','Geometry',4,1),
	('2406','Geometry',4,1),
	('2407','Trigonometry',7,1),
	('2408','AlgebraII',5,1),
	('2409','Geometry',4,1),
	('2410','Statistics',6,1),
	('2411','AlgebraII',5,1),
	('2412','AlgebraII',5,1),
	('2413','Geometry',4,1),
	('2414','PreCalculus',7,1),
	('2415','Calculus',8,1),
	('2417','AlgebraII',7,1),
	('2420','PreAlgebra',2,1),
	('2421','PreAlgebra',2,1),
	('2422','PreCalculus',7,1),
	('2424','PreAlgebra',2,1),
	('2425','AlgebraI',3,1),
	('2426','Geometry',4,1),
	('2427','AlgebraII',5,1),
	('2428','AlgebraI',3,1),
	('2429','AlgebraI',3,1),
	('2430','AlgebraII',6,1),
	('2430','Statistics',6,0),
	('2433','PreAlgebra',2,1),
	('2437','AlgebraI',3,1),
	('2438','AlgebraII',6,1),
	('2439','Geometry',4,1),
	('2440','AlgebraI',3,1),
	('2441','Geometry',4,1),
	('2442','AlgebraII',5,1),
	('2443','AlgebraII',6,1),
	('2443','Statistics',6,0),
	('2444','PreCalculus',7,1),
	('2445','Statistics',6,1),
	('2446','AlgebraI',3,1),
	('2447','AlgebraI',3,1),
	('2460','PreCalculus',7,1),
	('2461','PreCalculus',7,1),
	('2462','Calculus',8,1),
	('2463','PreCalculus',7,1),
	('2464','Geometry',4,1),
	('2467','AlgebraI',3,1),
	('2468','Geometry',4,1),
	('2469','Geometry',4,1),
	('2473','AlgebraII',5,1),
	('2480','Calculus',8,1),
	('2481','Calculus',8,1),
	('2483','Statistics',6,1),
	('2485','AlgebraI',3,1);