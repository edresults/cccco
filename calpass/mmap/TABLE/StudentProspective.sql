USE calpass;

GO

IF (object_id('mmap.StudentProspective') is not null)
	BEGIN
		DROP TABLE mmap.StudentProspective;
	END;

GO

CREATE TABLE
	mmap.StudentProspective
	(
		InterSegmentKey binary(64) not null,
		English char(1),
		Reading char(1),
		Esl char(1),
		Algebra bit,
		CollegeAlgebra bit,
		Stats bit,
		MathGE bit,
		Trigonometry bit,
		PreCalculus bit,
		Calculus bit
	);

GO

ALTER TABLE
	mmap.StudentProspective
ADD CONSTRAINT
	pk_StudentProspective__InterSegmentKey
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey
	);