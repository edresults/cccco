IF (object_id('Mmap.K12Performance') is not null)
	BEGIN
		DROP TABLE Mmap.K12Performance;
	END;

GO

CREATE TABLE
	Mmap.K12Performance
	(
		StudentStateId                   char(10)     not null,
		DepartmentCode                   tinyint      not null,
		GradeCode                        char(2)      not null,
		QualityPoints                    decimal(9,3)     null,
		CreditAttempted                  decimal(9,3)     null,
		GradePointAverage                decimal(9,3)     null,
		GradePointAverageSans            decimal(9,3)     null,
		CumulativeQualityPoints          decimal(9,3)     null,
		CumulativeCreditAttempted        decimal(9,3)     null,
		CumulativeGradePointAverage      decimal(9,3)     null,
		CumulativeGradePointAverageSans  decimal(9,3)     null,
		FirstYearTermCode                char(5)          null,
		LastYearTermCode                 char(5)          null,
		IsFirst                          bit              null,
		IsLast                           bit              null
	);

GO

ALTER TABLE
	Mmap.K12Performance
ADD CONSTRAINT
	PK_K12Performance
PRIMARY KEY CLUSTERED
	(
		StudentStateId,
		DepartmentCode,
		GradeCode
	);

ALTER TABLE
	mmap.K12Performance
ADD CONSTRAINT
	FK_K12Performance__DepartmentCode
FOREIGN KEY
	(
		DepartmentCode
	)
REFERENCES
	calpads.Department
	(
		Code
	);