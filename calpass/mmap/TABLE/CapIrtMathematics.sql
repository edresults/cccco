-- DROP TABLE mmap.CapMmapMathematics;

-- GO

-- SELECT
-- 	CohortYear,
-- 	b.InterSegmentKey,
-- 	OverallCumulativeGradePointAverage,
-- 	SubjectCumulativeGradePointAverage,
-- 	WithoutSubjectOverallCumulativeGradePointAverage,
-- 	-- high school
-- 	AlgebraI_MarkCategory = isnull(AlgebraI_MarkCategory, -99),
-- 	AlgebraI_IsA,
-- 	AlgebraI_IsAminus,
-- 	AlgebraI_IsBplus,
-- 	AlgebraI_IsB,
-- 	AlgebraI_IsBminus,
-- 	AlgebraI_IsCplus,
-- 	AlgebraI_IsC,
-- 	AlgebraI_IsCminus,
-- 	AlgebraI_IsDplus,
-- 	AlgebraI_IsD,
-- 	AlgebraI_IsDminus,
-- 	AlgebraI_IsF,
-- 	AlgebraII_MarkCategory = isnull(AlgebraII_MarkCategory, -99),
-- 	AlgebraII_IsA,
-- 	AlgebraII_IsAminus,
-- 	AlgebraII_IsBplus,
-- 	AlgebraII_IsB,
-- 	AlgebraII_IsBminus,
-- 	AlgebraII_IsCplus,
-- 	AlgebraII_IsC,
-- 	AlgebraII_IsCminus,
-- 	AlgebraII_IsDplus,
-- 	AlgebraII_IsD,
-- 	AlgebraII_IsDminus,
-- 	AlgebraII_IsF,
-- 	Arithmetic_MarkCategory = isnull(Arithmetic_MarkCategory, -99),
-- 	Arithmetic_IsA,
-- 	Arithmetic_IsAminus,
-- 	Arithmetic_IsBplus,
-- 	Arithmetic_IsB,
-- 	Arithmetic_IsBminus,
-- 	Arithmetic_IsCplus,
-- 	Arithmetic_IsC,
-- 	Arithmetic_IsCminus,
-- 	Arithmetic_IsDplus,
-- 	Arithmetic_IsD,
-- 	Arithmetic_IsDminus,
-- 	Arithmetic_IsF,
-- 	Calculus_MarkCategory = isnull(case when Calculus_CourseCode not in ('2480', '2481') then Calculus_MarkCategory end, -99),
-- 	Calculus_IsA = case when Calculus_CourseCode not in ('2480', '2481') then Calculus_IsA end,
-- 	Calculus_IsAminus = case when Calculus_CourseCode not in ('2480', '2481') then Calculus_IsAminus end,
-- 	Calculus_IsBplus = case when Calculus_CourseCode not in ('2480', '2481') then Calculus_IsBplus end,
-- 	Calculus_IsB = case when Calculus_CourseCode not in ('2480', '2481') then Calculus_IsB end,
-- 	Calculus_IsBminus = case when Calculus_CourseCode not in ('2480', '2481') then Calculus_IsBminus end,
-- 	Calculus_IsCplus = case when Calculus_CourseCode not in ('2480', '2481') then Calculus_IsCplus end,
-- 	Calculus_IsC = case when Calculus_CourseCode not in ('2480', '2481') then Calculus_IsC end,
-- 	Calculus_IsCminus = case when Calculus_CourseCode not in ('2480', '2481') then Calculus_IsCminus end,
-- 	Calculus_IsDplus = case when Calculus_CourseCode not in ('2480', '2481') then Calculus_IsDplus end,
-- 	Calculus_IsD = case when Calculus_CourseCode not in ('2480', '2481') then Calculus_IsD end,
-- 	Calculus_IsDminus = case when Calculus_CourseCode not in ('2480', '2481') then Calculus_IsDminus end,
-- 	Calculus_IsF = case when Calculus_CourseCode not in ('2480', '2481') then Calculus_IsF end,
-- 	CalculusAB_MarkCategory = isnull(case when Calculus_CourseCode = '2480' then Calculus_MarkCategory end, -99),
-- 	CalculusAB_IsA = case when Calculus_CourseCode = '2480' then Calculus_IsA end,
-- 	CalculusAB_IsAminus = case when Calculus_CourseCode = '2480' then Calculus_IsAminus end,
-- 	CalculusAB_IsBplus = case when Calculus_CourseCode = '2480' then Calculus_IsBplus end,
-- 	CalculusAB_IsB = case when Calculus_CourseCode = '2480' then Calculus_IsB end,
-- 	CalculusAB_IsBminus = case when Calculus_CourseCode = '2480' then Calculus_IsBminus end,
-- 	CalculusAB_IsCplus = case when Calculus_CourseCode = '2480' then Calculus_IsCplus end,
-- 	CalculusAB_IsC = case when Calculus_CourseCode = '2480' then Calculus_IsC end,
-- 	CalculusAB_IsCminus = case when Calculus_CourseCode = '2480' then Calculus_IsCminus end,
-- 	CalculusAB_IsDplus = case when Calculus_CourseCode = '2480' then Calculus_IsDplus end,
-- 	CalculusAB_IsD = case when Calculus_CourseCode = '2480' then Calculus_IsD end,
-- 	CalculusAB_IsDminus = case when Calculus_CourseCode = '2480' then Calculus_IsDminus end,
-- 	CalculusAB_IsF = case when Calculus_CourseCode = '2480' then Calculus_IsF end,
-- 	CalculusBC_MarkCategory = isnull(case when Calculus_CourseCode = '2481' then Calculus_MarkCategory end, -99),
-- 	CalculusBC_IsA = case when Calculus_CourseCode = '2481' then Calculus_IsA end,
-- 	CalculusBC_IsAminus = case when Calculus_CourseCode = '2481' then Calculus_IsAminus end,
-- 	CalculusBC_IsBplus = case when Calculus_CourseCode = '2481' then Calculus_IsBplus end,
-- 	CalculusBC_IsB = case when Calculus_CourseCode = '2481' then Calculus_IsB end,
-- 	CalculusBC_IsBminus = case when Calculus_CourseCode = '2481' then Calculus_IsBminus end,
-- 	CalculusBC_IsCplus = case when Calculus_CourseCode = '2481' then Calculus_IsCplus end,
-- 	CalculusBC_IsC = case when Calculus_CourseCode = '2481' then Calculus_IsC end,
-- 	CalculusBC_IsCminus = case when Calculus_CourseCode = '2481' then Calculus_IsCminus end,
-- 	CalculusBC_IsDplus = case when Calculus_CourseCode = '2481' then Calculus_IsDplus end,
-- 	CalculusBC_IsD = case when Calculus_CourseCode = '2481' then Calculus_IsD end,
-- 	CalculusBC_IsDminus = case when Calculus_CourseCode = '2481' then Calculus_IsDminus end,
-- 	CalculusBC_IsF = case when Calculus_CourseCode = '2481' then Calculus_IsF end,
-- 	Geometry_MarkCategory = isnull(Geometry_MarkCategory, -99),
-- 	Geometry_IsA,
-- 	Geometry_IsAminus,
-- 	Geometry_IsBplus,
-- 	Geometry_IsB,
-- 	Geometry_IsBminus,
-- 	Geometry_IsCplus,
-- 	Geometry_IsC,
-- 	Geometry_IsCminus,
-- 	Geometry_IsDplus,
-- 	Geometry_IsD,
-- 	Geometry_IsDminus,
-- 	Geometry_IsF,
-- 	PreAlgebra_MarkCategory = isnull(PreAlgebra_MarkCategory, -99),
-- 	PreAlgebra_IsA,
-- 	PreAlgebra_IsAminus,
-- 	PreAlgebra_IsBplus,
-- 	PreAlgebra_IsB,
-- 	PreAlgebra_IsBminus,
-- 	PreAlgebra_IsCplus,
-- 	PreAlgebra_IsC,
-- 	PreAlgebra_IsCminus,
-- 	PreAlgebra_IsDplus,
-- 	PreAlgebra_IsD,
-- 	PreAlgebra_IsDminus,
-- 	PreAlgebra_IsF,
-- 	PreCalculus_MarkCategory = isnull(PreCalculus_MarkCategory, -99),
-- 	PreCalculus_IsA,
-- 	PreCalculus_IsAminus,
-- 	PreCalculus_IsBplus,
-- 	PreCalculus_IsB,
-- 	PreCalculus_IsBminus,
-- 	PreCalculus_IsCplus,
-- 	PreCalculus_IsC,
-- 	PreCalculus_IsCminus,
-- 	PreCalculus_IsDplus,
-- 	PreCalculus_IsD,
-- 	PreCalculus_IsDminus,
-- 	PreCalculus_IsF,
-- 	Statistics_MarkCategory = isnull(case when Statistics_CourseCode != '2483' then Statistics_MarkCategory end, -99),
-- 	Statistics_IsA = case when Statistics_CourseCode != '2483' then Statistics_IsA end,
-- 	Statistics_IsAminus = case when Statistics_CourseCode != '2483' then Statistics_IsAminus end,
-- 	Statistics_IsBplus = case when Statistics_CourseCode != '2483' then Statistics_IsBplus end,
-- 	Statistics_IsB = case when Statistics_CourseCode != '2483' then Statistics_IsB end,
-- 	Statistics_IsBminus = case when Statistics_CourseCode != '2483' then Statistics_IsBminus end,
-- 	Statistics_IsCplus = case when Statistics_CourseCode != '2483' then Statistics_IsCplus end,
-- 	Statistics_IsC = case when Statistics_CourseCode != '2483' then Statistics_IsC end,
-- 	Statistics_IsCminus = case when Statistics_CourseCode != '2483' then Statistics_IsCminus end,
-- 	Statistics_IsDplus = case when Statistics_CourseCode != '2483' then Statistics_IsDplus end,
-- 	Statistics_IsD = case when Statistics_CourseCode != '2483' then Statistics_IsD end,
-- 	Statistics_IsDminus = case when Statistics_CourseCode != '2483' then Statistics_IsDminus end,
-- 	Statistics_IsF = case when Statistics_CourseCode != '2483' then Statistics_IsF end,
-- 	StatisticsAP_MarkCategory = isnull(case when Statistics_CourseCode = '2483' then Statistics_MarkCategory end, -99),
-- 	StatisticsAP_IsA = case when Statistics_CourseCode = '2483' then Statistics_IsA end,
-- 	StatisticsAP_IsAminus = case when Statistics_CourseCode = '2483' then Statistics_IsAminus end,
-- 	StatisticsAP_IsBplus = case when Statistics_CourseCode = '2483' then Statistics_IsBplus end,
-- 	StatisticsAP_IsB = case when Statistics_CourseCode = '2483' then Statistics_IsB end,
-- 	StatisticsAP_IsBminus = case when Statistics_CourseCode = '2483' then Statistics_IsBminus end,
-- 	StatisticsAP_IsCplus = case when Statistics_CourseCode = '2483' then Statistics_IsCplus end,
-- 	StatisticsAP_IsC = case when Statistics_CourseCode = '2483' then Statistics_IsC end,
-- 	StatisticsAP_IsCminus = case when Statistics_CourseCode = '2483' then Statistics_IsCminus end,
-- 	StatisticsAP_IsDplus = case when Statistics_CourseCode = '2483' then Statistics_IsDplus end,
-- 	StatisticsAP_IsD = case when Statistics_CourseCode = '2483' then Statistics_IsD end,
-- 	StatisticsAP_IsDminus = case when Statistics_CourseCode = '2483' then Statistics_IsDminus end,
-- 	StatisticsAP_IsF = case when Statistics_CourseCode = '2483' then Statistics_IsF end,
-- 	Trigonometry_MarkCategory = isnull(Trigonometry_MarkCategory, -99),
-- 	Trigonometry_IsA,
-- 	Trigonometry_IsAminus,
-- 	Trigonometry_IsBplus,
-- 	Trigonometry_IsB,
-- 	Trigonometry_IsBminus,
-- 	Trigonometry_IsCplus,
-- 	Trigonometry_IsC,
-- 	Trigonometry_IsCminus,
-- 	Trigonometry_IsDplus,
-- 	Trigonometry_IsD,
-- 	Trigonometry_IsDminus,
-- 	Trigonometry_IsF,
-- 	-- college
-- 	cc_00_course_grade_category = isnull(case when CourseLevel = 'Y' then ccc.MarkCategory end, -99),
-- 	cc_01_course_grade_category = isnull(case when CourseLevel = 'A' then ccc.MarkCategory end, -99),
-- 	cc_02_course_grade_category = isnull(case when CourseLevel = 'B' then ccc.MarkCategory end, -99),
-- 	cc_03_course_grade_category = isnull(case when CourseLevel = 'C' then ccc.MarkCategory end, -99),
-- 	cc_04_course_grade_category = isnull(case when CourseLevel = 'D' then ccc.MarkCategory end, -99)
-- INTO
-- 	mmap.CapMmapMathematics
-- FROM
-- 	(
-- 		SELECT
-- 			cc.CohortYear,
-- 			cc.TopCode,
-- 			cc.InterSegmentKey,
-- 			OverallCumulativeGradePointAverage = max(case when DepartmentCode = '00' then CumulativeGradePointAverage end),
-- 			SubjectCumulativeGradePointAverage = max(case when DepartmentCode = '18' then CumulativeGradePointAverage end),
-- 			WithoutSubjectOverallCumulativeGradePointAverage = 
-- 				convert(
-- 					decimal(9,3),
-- 					isnull(
-- 						case
-- 							when max(case when DepartmentCode = '00' then CumulativeQualityPoints end) - max(case when DepartmentCode = '18' then CumulativeQualityPoints end) <= 0 then 0
-- 							else max(case when DepartmentCode = '00' then CumulativeQualityPoints end) - max(case when DepartmentCode = '18' then CumulativeQualityPoints end)
-- 						end
-- 						/
-- 						nullif(
-- 							case
-- 								when max(case when DepartmentCode = '00' then CumulativeCreditAttempted end) - max(case when DepartmentCode = '18' then CumulativeCreditAttempted end) <= 0 then 0
-- 								else max(case when DepartmentCode = '00' then CumulativeCreditAttempted end) - max(case when DepartmentCode = '18' then CumulativeCreditAttempted end)
-- 							end,
-- 							0
-- 						)
-- 					,0)
-- 				)
-- 		FROM
-- 			mmap.CapCohort cc
-- 			inner join
-- 			mmap.RetrospectivePerformance rp
-- 				on  cc.InterSegmentKey = rp.InterSegmentKey
-- 		WHERE
-- 			cc.TopCode = '170100'
-- 			and rp.DepartmentCode in ('00', '18')
-- 			and rp.GradeCode = '12'
-- 			and rp.IsOverall = 1
-- 		GROUP BY
-- 			cc.CohortYear,
-- 			cc.TopCode,
-- 			cc.InterSegmentKey
-- 	) b
-- 	inner join
-- 	mmap.RetrospectiveMathematics rm
-- 		on rm.InterSegmentKey = b.InterSegmentKey
-- 	inner join
-- 	mmap.CollegeCourseContent ccc
-- 		on ccc.InterSegmentKey = b.InterSegmentKey
-- 		and ccc.ToPCode = b.TopCode
-- WHERE
-- 	ccc.FirstSelector = 1;

-- DELETE FROM mmap.CapMmapMathematics WHERE OverallCumulativeGradePointAverage > 4;
-- DELETE FROM mmap.CapMmapMathematics WHERE SubjectCumulativeGradePointAverage > 4;
-- DELETE FROM mmap.CapMmapMathematics WHERE WithoutSubjectOverallCumulativeGradePointAverage > 4;

-- ALTER TABLE
-- 	mmap.CapMmapMathematics
-- ADD CONSTRAINT
-- 	PK_CapMmapMathematics
-- PRIMARY KEY CLUSTERED
-- 	(
-- 		InterSegmentkey
-- 	);

-- GO

TRUNCATE TABLE mmap.CapIrtMathematics;

GO

INSERT
	mmap.CapIrtMathematics
	(
		CohortYear,
		InterSegmentKey,
		OverallCumulativeGradePointAverage,
		SubjectCumulativeGradePointAverage,
		WithoutSubjectOverallCumulativeGradePointAverage,
		AlgebraI_1_MarkCategory,
		AlgebraI_2_MarkCategory,
		AlgebraII_1_MarkCategory,
		AlgebraII_2_MarkCategory,
		Arithmetic_1_MarkCategory,
		Arithmetic_2_MarkCategory,
		Calculus_1_MarkCategory,
		Calculus_2_MarkCategory,
		CalculusAB_1_MarkCategory,
		CalculusAB_2_MarkCategory,
		CalculusBC_1_MarkCategory,
		CalculusBC_2_MarkCategory,
		Geometry_1_MarkCategory,
		Geometry_2_MarkCategory,
		PreAlgebra_1_MarkCategory,
		PreAlgebra_2_MarkCategory,
		PreCalculus_1_MarkCategory,
		PreCalculus_2_MarkCategory,
		Statistics_1_MarkCategory,
		Statistics_2_MarkCategory,
		StatisticsAP_1_MarkCategory,
		StatisticsAP_2_MarkCategory,
		Trigonometry_1_MarkCategory,
		Trigonometry_2_MarkCategory,
		cc_00_course_grade_category,
		cc_01_course_grade_category,
		cc_02_course_grade_category,
		cc_03_course_grade_category,
		cc_04_course_grade_category,
		CC_GE_MATH,
		CC_STATISTICS,
		CC_COLL_ALG,
		CC_PRE_CALC,
		CC_CALC_I,
		CC_CALC_II,
		CC_BUS_MATH,
		CC_DIFF_EQ,
		CC_TRIG,
		CC_TRANSFER_MATH_TYPE
	)
SELECT
	CohortYear,
	b.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage,
	-- high school
	AlgebraI_1_MarkCategory     = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'AlgebraI' then rcc.MarkCategory end), -99),
	AlgebraI_2_MarkCategory     = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'AlgebraI' then rcc.MarkCategory end), -99),
	AlgebraII_1_MarkCategory    = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'AlgebraII' then rcc.MarkCategory end), -99),
	AlgebraII_2_MarkCategory    = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'AlgebraII' then rcc.MarkCategory end), -99),
	Arithmetic_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Arithmetic' then rcc.MarkCategory end), -99),
	Arithmetic_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Arithmetic' then rcc.MarkCategory end), -99),
	Calculus_1_MarkCategory     = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Calculus' and rcc.CourseCode not in ('2480', '2481') then rcc.MarkCategory end), -99),
	Calculus_2_MarkCategory     = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Calculus' and rcc.CourseCode not in ('2480', '2481') then rcc.MarkCategory end), -99),
	CalculusAB_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2480' then rcc.MarkCategory end), -99),
	CalculusAB_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2480' then rcc.MarkCategory end), -99),
	CalculusBC_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2481' then rcc.MarkCategory end), -99),
	CalculusBC_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2481' then rcc.MarkCategory end), -99),
	Geometry_1_MarkCategory     = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Geometry' then rcc.MarkCategory end), -99),
	Geometry_2_MarkCategory     = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Geometry' then rcc.MarkCategory end), -99),
	PreAlgebra_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'PreAlgebra' then rcc.MarkCategory end), -99),
	PreAlgebra_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'PreAlgebra' then rcc.MarkCategory end), -99),
	PreCalculus_1_MarkCategory  = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'PreCalculus' then rcc.MarkCategory end), -99),
	PreCalculus_2_MarkCategory  = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'PreCalculus' then rcc.MarkCategory end), -99),
	Statistics_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Statistics' and rcc.CourseCode != '2483' then rcc.MarkCategory end), -99),
	Statistics_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Statistics' and rcc.CourseCode != '2483' then rcc.MarkCategory end), -99),
	StatisticsAP_1_MarkCategory = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Statistics' and rcc.CourseCode  = '2483' then rcc.MarkCategory end), -99),
	StatisticsAP_2_MarkCategory = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Statistics' and rcc.CourseCode  = '2483' then rcc.MarkCategory end), -99),
	Trigonometry_1_MarkCategory = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Trigonometry' then rcc.MarkCategory end), -99),
	Trigonometry_2_MarkCategory = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Trigonometry' then rcc.MarkCategory end), -99),
	-- college
	cc_00_course_grade_category = isnull(max(case when ccc.FirstSelector = 1 and ccc.CourseLevel = 'Y' then ccc.MarkCategory end), -99),
	cc_01_course_grade_category = isnull(max(case when ccc.FirstSelector = 1 and ccc.CourseLevel = 'A' then ccc.MarkCategory end), -99),
	cc_02_course_grade_category = isnull(max(case when ccc.FirstSelector = 1 and ccc.CourseLevel = 'B' then ccc.MarkCategory end), -99),
	cc_03_course_grade_category = isnull(max(case when ccc.FirstSelector = 1 and ccc.CourseLevel = 'C' then ccc.MarkCategory end), -99),
	cc_04_course_grade_category = isnull(max(case when ccc.FirstSelector = 1 and ccc.CourseLevel = 'D' then ccc.MarkCategory end), -99),
	-- College Mathematics Content
	CC_GE_MATH = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH FOR%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH%LIB%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH%ELEM%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH TOPICS%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATHEMATICAL%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATHEMATICS%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%THE IDEAS%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%LIBERAL%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SURVEY%' and 
				case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%CALC%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%CONCEPTS%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%IDEAS%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%TEACH%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%LIFE%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BUS%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BRIEF%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SOC%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BIO%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SHORT%' then ccc.MarkCategory
			else -99
		end), -99),
	CC_STATISTICS = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseId    end like '%STA%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%STA%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO STA%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM PROB%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM. PROB%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM STA%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEMENTARY STA%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEMNTRY STA%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO PROB%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO TO PROB%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO APPLIED STATS%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%HONORS STA%' then ccc.MarkCategory
			else -99
		end), -99),
	CC_COLL_ALG =
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%COLL%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ADVANCED ALG%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ALGEBRA FOR CALC%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%PRECALC%' then -99
			else -99
		end), -99),
	CC_PRE_CALC = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%PRE%CALC%' then ccc.MarkCategory
			else -99
		end), -99),
	CC_CALC_I = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%CALC%' then
				case
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%PRE%' then -99
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%II%' then -99
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%2%' then -99
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%3%' then -99
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%4A%' then -99
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%DIFF%' then -99
					else ccc.MarkCategory
				end
			else -99
		end), -99),
	CC_CALC_II = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%CALC%'
				and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%PRE%' then
				case
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%II%' then ccc.MarkCategory
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%2%' then ccc.MarkCategory
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%3%' then ccc.MarkCategory
					else -99
				end
			else -99
		end), -99),
	CC_BUS_MATH = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BUS%'
				and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%FINITE%'
				and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%DISCRETE%' then ccc.MarkCategory
			else -99
		end), -99),
	CC_DIFF_EQ = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%DIFF%' then ccc.MarkCategory
			else -99
		end), -99),
	CC_TRIG = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%Trig%' then ccc.MarkCategory
			else -99
		end), -99),
	CC_TRANSFER_MATH_TYPE = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end = 'Y' then
				case
					-- CC_GE_MATH
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH FOR%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH/LIB%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH/ELEM%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH:ELEM%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH TOPICS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATHEMATICAL%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATHEMATICS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%THE IDEAS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%LIBERAL%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SURVEY%'
						and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%CALC%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%CONCEPTS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%IDEAS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%TEACH%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%LIFE%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BUS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BRIEF%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SOC%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BIO%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SHORT%' then 0
					-- CC_STATISTICS
					when case when ccc.FirstSelector = 1 then ccc.CourseId    end like '%STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM PROB%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM. PROB%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEMENTARY STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEMNTRY STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO PROB%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO TO PROB%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO APPLIED STATS%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%HONORS STA%' then 1
					-- CC_COLL_ALG
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%COLL%' then 2
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ADVANCED ALG%' then 2
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ALGEBRA FOR CALC%' then 2
					-- CC_PRE_CALC & CC_CALC_I & CC_CALC_II
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%CALC%' then
						case
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%PRE%' then 3
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%II%' then 5
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%2%' then 5
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%3%' then 5
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%4A%' then 5
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%DIFF%' then 7
							else 4
						end
					-- CC_BUS_MATH
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BUS%'
						and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%FINITE%'
						and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%DISCRETE%' then 1
					-- CC_DIFF_EQ
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%DIFF%' then 7
					-- CC_LINEAR_ALGEBRA not in variables
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%LINEAR%' then 8
					else -99
				end
			else -99
		end), -99)
FROM
	(
		SELECT
			cc.CohortYear,
			cc.TopCode,
			cc.InterSegmentKey,
			OverallCumulativeGradePointAverage = max(case when DepartmentCode = '00' then CumulativeGradePointAverage end),
			SubjectCumulativeGradePointAverage = max(case when DepartmentCode = '18' then CumulativeGradePointAverage end),
			WithoutSubjectOverallCumulativeGradePointAverage = 
				convert(
					decimal(9,3),
					isnull(
						case
							when max(case when DepartmentCode = '00' then CumulativeQualityPoints end) - max(case when DepartmentCode = '18' then CumulativeQualityPoints end) <= 0 then 0
							else max(case when DepartmentCode = '00' then CumulativeQualityPoints end) - max(case when DepartmentCode = '18' then CumulativeQualityPoints end)
						end
						/
						nullif(
							case
								when max(case when DepartmentCode = '00' then CumulativeCreditAttempted end) - max(case when DepartmentCode = '18' then CumulativeCreditAttempted end) <= 0 then 0
								else max(case when DepartmentCode = '00' then CumulativeCreditAttempted end) - max(case when DepartmentCode = '18' then CumulativeCreditAttempted end)
							end,
							0
						)
					,0)
				)
		FROM
			mmap.CapCohort cc
			inner join
			mmap.RetrospectivePerformance rp
				on  cc.InterSegmentKey = rp.InterSegmentKey
		WHERE
			cc.TopCode = '170100'
			and rp.DepartmentCode in ('00', '18')
			and rp.IsLast = 1
		GROUP BY
			cc.CohortYear,
			cc.TopCode,
			cc.InterSegmentKey
	) b
	inner join
	mmap.RetrospectiveCourseContent rcc
		on rcc.InterSegmentKey = b.InterSegmentKey
	inner join
	(
		SELECT
			ccc.*
		FROM
			mmap.CapCohort cc
			inner join
			mmap.CollegeCourseContent ccc
				on  cc.InterSegmentKey = ccc.InterSegmentKey
				and cc.TopCode = ccc.TopCode
		WHERE
			cc.TopCode = '170100'
	) ccc
		on ccc.InterSegmentKey = b.InterSegmentKey
		and ccc.ToPCode = b.TopCode
	inner join
	dbo.Student s
		on s.InterSegmentKey = b.InterSegmentKey
WHERE
	rcc.DepartmentCode = '18'
	and rcc.ContentSelector in (1,2)
	and ccc.LevelSelector = 1
	and ccc.CollegeCounter = 1
	and s.IsGrade11SectionInclusive = 1
	and s.IsHighSchoolCollision = 0
GROUP BY
	CohortYear,
	b.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage;

DELETE FROM mmap.CapIrtMathematics WHERE OverallCumulativeGradePointAverage > 4;
DELETE FROM mmap.CapIrtMathematics WHERE SubjectCumulativeGradePointAverage > 4;
DELETE FROM mmap.CapIrtMathematics WHERE WithoutSubjectOverallCumulativeGradePointAverage > 4;

-- ALTER TABLE
-- 	mmap.CapIrtMathematics
-- ADD CONSTRAINT
-- 	PK_CapIrtMathematics
-- PRIMARY KEY CLUSTERED
-- 	(
-- 		InterSegmentkey
-- 	);

ALTER TABLE
	mmap.CapIrtMathematics
ADD
	IsTransferStem smallint;

ALTER TABLE
	mmap.CapIrtMathematics
ADD
	IsTransferNonStem smallint;

UPDATE
	mmap.CapIrtMathematics
SET
	IsTransferStem = 
		case
			when CC_COLL_ALG != -99 then CC_COLL_ALG
			when CC_PRE_CALC != -99 then CC_PRE_CALC
			when CC_CALC_I != -99 then CC_CALC_I
			when CC_CALC_II != -99 then CC_CALC_II
			when CC_DIFF_EQ != -99 then CC_DIFF_EQ
			when CC_BUS_MATH != -99 then CC_BUS_MATH
			else -99
		end,
	IsTransferNonStem = 
		case
			when CC_GE_MATH != -99 then CC_GE_MATH
			when CC_STATISTICS != -99 then CC_STATISTICS
			else -99
		end;

GO

TRUNCATE TABLE mmap.CapIrtMathematicsMod;

GO

INSERT
	mmap.CapIrtMathematicsMod
	(
		CohortYear,
		InterSegmentKey,
		OverallCumulativeGradePointAverage,
		SubjectCumulativeGradePointAverage,
		WithoutSubjectOverallCumulativeGradePointAverage,
		AlgebraI_1_MarkCategory,
		AlgebraI_2_MarkCategory,
		AlgebraII_1_MarkCategory,
		AlgebraII_2_MarkCategory,
		Arithmetic_1_MarkCategory,
		Arithmetic_2_MarkCategory,
		Calculus_1_MarkCategory,
		Calculus_2_MarkCategory,
		CalculusAB_1_MarkCategory,
		CalculusAB_2_MarkCategory,
		CalculusBC_1_MarkCategory,
		CalculusBC_2_MarkCategory,
		Geometry_1_MarkCategory,
		Geometry_2_MarkCategory,
		PreAlgebra_1_MarkCategory,
		PreAlgebra_2_MarkCategory,
		PreCalculus_1_MarkCategory,
		PreCalculus_2_MarkCategory,
		Statistics_1_MarkCategory,
		Statistics_2_MarkCategory,
		StatisticsAP_1_MarkCategory,
		StatisticsAP_2_MarkCategory,
		Trigonometry_1_MarkCategory,
		Trigonometry_2_MarkCategory,
		cc_00_course_grade_category,
		cc_01_course_grade_category,
		cc_02_course_grade_category,
		cc_03_course_grade_category,
		cc_04_course_grade_category,
		CC_GE_MATH,
		CC_STATISTICS,
		CC_COLL_ALG,
		CC_PRE_CALC,
		CC_CALC_I,
		CC_CALC_II,
		CC_BUS_MATH,
		CC_DIFF_EQ,
		CC_TRIG,
		CC_TRANSFER_MATH_TYPE
	)
SELECT
	CohortYear,
	b.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage,
	-- high school
	AlgebraI_1_MarkCategory     = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'AlgebraI' then rcc.MarkCategory end), -99),
	AlgebraI_2_MarkCategory     = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'AlgebraI' then rcc.MarkCategory end), -99),
	AlgebraII_1_MarkCategory    = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'AlgebraII' then rcc.MarkCategory end), -99),
	AlgebraII_2_MarkCategory    = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'AlgebraII' then rcc.MarkCategory end), -99),
	Arithmetic_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Arithmetic' then rcc.MarkCategory end), -99),
	Arithmetic_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Arithmetic' then rcc.MarkCategory end), -99),
	Calculus_1_MarkCategory     = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Calculus' and rcc.CourseCode not in ('2480', '2481') then rcc.MarkCategory end), -99),
	Calculus_2_MarkCategory     = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Calculus' and rcc.CourseCode not in ('2480', '2481') then rcc.MarkCategory end), -99),
	CalculusAB_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2480' then rcc.MarkCategory end), -99),
	CalculusAB_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2480' then rcc.MarkCategory end), -99),
	CalculusBC_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2481' then rcc.MarkCategory end), -99),
	CalculusBC_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Calculus' and rcc.CourseCode = '2481' then rcc.MarkCategory end), -99),
	Geometry_1_MarkCategory     = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Geometry' then rcc.MarkCategory end), -99),
	Geometry_2_MarkCategory     = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Geometry' then rcc.MarkCategory end), -99),
	PreAlgebra_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'PreAlgebra' then rcc.MarkCategory end), -99),
	PreAlgebra_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'PreAlgebra' then rcc.MarkCategory end), -99),
	PreCalculus_1_MarkCategory  = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'PreCalculus' then rcc.MarkCategory end), -99),
	PreCalculus_2_MarkCategory  = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'PreCalculus' then rcc.MarkCategory end), -99),
	Statistics_1_MarkCategory   = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Statistics' and rcc.CourseCode != '2483' then rcc.MarkCategory end), -99),
	Statistics_2_MarkCategory   = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Statistics' and rcc.CourseCode != '2483' then rcc.MarkCategory end), -99),
	StatisticsAP_1_MarkCategory = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Statistics' and rcc.CourseCode  = '2483' then rcc.MarkCategory end), -99),
	StatisticsAP_2_MarkCategory = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Statistics' and rcc.CourseCode  = '2483' then rcc.MarkCategory end), -99),
	Trigonometry_1_MarkCategory = isnull(max(case when rcc.ContentSelector = 2 and rcc.ContentCode = 'Trigonometry' then rcc.MarkCategory end), -99),
	Trigonometry_2_MarkCategory = isnull(max(case when rcc.ContentSelector = 1 and rcc.ContentCode = 'Trigonometry' then rcc.MarkCategory end), -99),
	-- college
	cc_00_course_grade_category = isnull(max(case when ccc.FirstSelector = 1 and ccc.CourseLevel = 'Y' then ccc.MarkCategory end), -99),
	cc_01_course_grade_category = isnull(max(case when ccc.FirstSelector = 1 and ccc.CourseLevel = 'A' then ccc.MarkCategory end), -99),
	cc_02_course_grade_category = isnull(max(case when ccc.FirstSelector = 1 and ccc.CourseLevel = 'B' then ccc.MarkCategory end), -99),
	cc_03_course_grade_category = isnull(max(case when ccc.FirstSelector = 1 and ccc.CourseLevel = 'C' then ccc.MarkCategory end), -99),
	cc_04_course_grade_category = isnull(max(case when ccc.FirstSelector = 1 and ccc.CourseLevel = 'D' then ccc.MarkCategory end), -99),
	-- College Mathematics Content
	CC_GE_MATH = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH FOR%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH%LIB%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH%ELEM%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH TOPICS%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATHEMATICAL%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATHEMATICS%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%THE IDEAS%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%LIBERAL%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SURVEY%' and 
				case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%CALC%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%CONCEPTS%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%IDEAS%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%TEACH%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%LIFE%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BUS%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BRIEF%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SOC%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BIO%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SHORT%' then ccc.MarkCategory
			else -99
		end), -99),
	CC_STATISTICS = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseId    end like '%STA%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%STA%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO STA%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM PROB%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM. PROB%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM STA%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEMENTARY STA%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEMNTRY STA%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO PROB%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO TO PROB%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO APPLIED STATS%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%HONORS STA%' then ccc.MarkCategory
			else -99
		end), -99),
	CC_COLL_ALG =
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%COLL%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ADVANCED ALG%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ALGEBRA FOR CALC%' then ccc.MarkCategory
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%PRECALC%' then -99
			else -99
		end), -99),
	CC_PRE_CALC = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%PRE%CALC%' then ccc.MarkCategory
			else -99
		end), -99),
	CC_CALC_I = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%CALC%' then
				case
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%PRE%' then -99
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%II%' then -99
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%2%' then -99
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%3%' then -99
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%4A%' then -99
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%DIFF%' then -99
					else ccc.MarkCategory
				end
			else -99
		end), -99),
	CC_CALC_II = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%CALC%'
				and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%PRE%' then
				case
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%II%' then ccc.MarkCategory
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%2%' then ccc.MarkCategory
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%3%' then ccc.MarkCategory
					else -99
				end
			else -99
		end), -99),
	CC_BUS_MATH = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BUS%'
				and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%FINITE%'
				and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%DISCRETE%' then ccc.MarkCategory
			else -99
		end), -99),
	CC_DIFF_EQ = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%DIFF%' then ccc.MarkCategory
			else -99
		end), -99),
	CC_TRIG = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%Trig%' then ccc.MarkCategory
			else -99
		end), -99),
	CC_TRANSFER_MATH_TYPE = 
		isnull(max(case
			when case when ccc.FirstSelector = 1 then ccc.CourseTitle end = 'Y' then
				case
					-- CC_GE_MATH
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH FOR%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH/LIB%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH/ELEM%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH:ELEM%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATH TOPICS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATHEMATICAL%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%MATHEMATICS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%THE IDEAS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%LIBERAL%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SURVEY%'
						and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%CALC%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%CONCEPTS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%IDEAS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%TEACH%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%LIFE%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BUS%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BRIEF%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SOC%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BIO%' then 0
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%SHORT%' then 0
					-- CC_STATISTICS
					when case when ccc.FirstSelector = 1 then ccc.CourseId    end like '%STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM PROB%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM. PROB%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEM STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEMENTARY STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ELEMNTRY STA%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO PROB%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO TO PROB%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%INTRO APPLIED STATS%' then 1
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%HONORS STA%' then 1
					-- CC_COLL_ALG
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%COLL%' then 2
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ADVANCED ALG%' then 2
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%ALGEBRA FOR CALC%' then 2
					-- CC_PRE_CALC & CC_CALC_I & CC_CALC_II
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%CALC%' then
						case
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%PRE%' then 3
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%II%' then 5
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%2%' then 5
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%3%' then 5
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%4A%' then 5
							when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%DIFF%' then 7
							else 4
						end
					-- CC_BUS_MATH
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%BUS%'
						and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%FINITE%'
						and case when ccc.FirstSelector = 1 then ccc.CourseTitle end not like '%DISCRETE%' then 1
					-- CC_DIFF_EQ
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%DIFF%' then 7
					-- CC_LINEAR_ALGEBRA not in variables
					when case when ccc.FirstSelector = 1 then ccc.CourseTitle end like '%LINEAR%' then 8
					else -99
				end
			else -99
		end), -99)
FROM
	(
		SELECT
			cc.CohortYear,
			cc.TopCode,
			cc.InterSegmentKey,
			OverallCumulativeGradePointAverage = max(case when DepartmentCode = '00' then CumulativeGradePointAverage end),
			SubjectCumulativeGradePointAverage = max(case when DepartmentCode = '18' then CumulativeGradePointAverage end),
			WithoutSubjectOverallCumulativeGradePointAverage = 
				convert(
					decimal(9,3),
					isnull(
						case
							when max(case when DepartmentCode = '00' then CumulativeQualityPoints end) - max(case when DepartmentCode = '18' then CumulativeQualityPoints end) <= 0 then 0
							else max(case when DepartmentCode = '00' then CumulativeQualityPoints end) - max(case when DepartmentCode = '18' then CumulativeQualityPoints end)
						end
						/
						nullif(
							case
								when max(case when DepartmentCode = '00' then CumulativeCreditAttempted end) - max(case when DepartmentCode = '18' then CumulativeCreditAttempted end) <= 0 then 0
								else max(case when DepartmentCode = '00' then CumulativeCreditAttempted end) - max(case when DepartmentCode = '18' then CumulativeCreditAttempted end)
							end,
							0
						)
					,0)
				)
		FROM
			mmap.CapCohort cc
			inner join
			mmap.RetrospectivePerformance rp
				on  cc.InterSegmentKey = rp.InterSegmentKey
		WHERE
			cc.TopCode = '170100'
			and rp.DepartmentCode in ('00', '18')
			and rp.IsLast = 1
		GROUP BY
			cc.CohortYear,
			cc.TopCode,
			cc.InterSegmentKey
	) b
	inner join
	mmap.RetrospectiveCourseContent rcc
		on rcc.InterSegmentKey = b.InterSegmentKey
	inner join
	(
		SELECT
			ccc.InterSegmentKey,
			ccc.TopCode,
			ccc.CourseLevel,
			ccc.CollegeId,
			ccc.YearTermCode,
			ccc.CourseId,
			ccc.CourseTitle,
			ccc.CourseUnitsAttempted,
			ccc.SectionMark,
			ccc.MarkPoints,
			ccc.MarkCategory,
			ccc.MarkIsSuccess,
			FirstSelector = 1,
			ccc.LevelSelector,
			ccc.CollegeCounter
		FROM
			mmap.CapCohort cc
			inner join
			mmap.CollegeCourseContent ccc
				on  cc.InterSegmentKey = ccc.InterSegmentKey
				and cc.TopCode = ccc.TopCode
		WHERE
			cc.TopCode = '170100'
			and exists (
				SELECT
					1
				FROM
					mmap.CollegeCourseContent ccc1
				WHERE
					ccc1.InterSegmentKey = ccc.InterSegmentKey
					and ccc1.TopCode = ccc.TopCode
					and ccc1.FirstSelector = 1
					and ccc1.SectionMark = 'W'
			)
			and ccc.FirstSelector = 2
			and ccc.SectionMark != 'W'
		UNION
		SELECT
			ccc.InterSegmentKey,
			ccc.TopCode,
			ccc.CourseLevel,
			ccc.CollegeId,
			ccc.YearTermCode,
			ccc.CourseId,
			ccc.CourseTitle,
			ccc.CourseUnitsAttempted,
			ccc.SectionMark,
			ccc.MarkPoints,
			ccc.MarkCategory,
			ccc.MarkIsSuccess,
			ccc.FirstSelector,
			ccc.LevelSelector,
			ccc.CollegeCounter
		FROM
			mmap.CapCohort cc
			inner join
			mmap.CollegeCourseContent ccc
				on  cc.InterSegmentKey = ccc.InterSegmentKey
				and cc.TopCode = ccc.TopCode
		WHERE
			cc.TopCode = '170100'
			and not exists (
				SELECT
					1
				FROM
					mmap.CollegeCourseContent ccc1
				WHERE
					ccc1.InterSegmentKey = ccc.InterSegmentKey
					and ccc1.TopCode = ccc.TopCode
					and ccc1.FirstSelector = 1
					and ccc1.SectionMark = 'W'
			)
	) ccc
		on ccc.InterSegmentKey = b.InterSegmentKey
		and ccc.ToPCode = b.TopCode
	inner join
	dbo.Student s
		on s.InterSegmentKey = b.InterSegmentKey
WHERE
	rcc.DepartmentCode = '18'
	and rcc.ContentSelector in (1,2)
	and ccc.LevelSelector = 1
	and ccc.CollegeCounter = 1
	and s.IsGrade11SectionInclusive = 1
	and s.IsHighSchoolCollision = 0
GROUP BY
	CohortYear,
	b.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage;

DELETE FROM mmap.CapIrtMathematicsMod WHERE OverallCumulativeGradePointAverage > 4;
DELETE FROM mmap.CapIrtMathematicsMod WHERE SubjectCumulativeGradePointAverage > 4;
DELETE FROM mmap.CapIrtMathematicsMod WHERE WithoutSubjectOverallCumulativeGradePointAverage > 4;

-- ALTER TABLE
-- 	mmap.CapIrtMathematicsMod
-- ADD CONSTRAINT
-- 	PK_CapIrtMathematicsMod
-- PRIMARY KEY CLUSTERED
-- 	(
-- 		InterSegmentkey
-- 	);

ALTER TABLE
	mmap.CapIrtMathematicsMod
ADD
	IsTransferStem smallint;

ALTER TABLE
	mmap.CapIrtMathematicsMod
ADD
	IsTransferNonStem smallint;

UPDATE
	mmap.CapIrtMathematicsMod
SET
	IsTransferStem = 
		case
			when CC_COLL_ALG != -99 then CC_COLL_ALG
			when CC_PRE_CALC != -99 then CC_PRE_CALC
			when CC_CALC_I != -99 then CC_CALC_I
			when CC_CALC_II != -99 then CC_CALC_II
			when CC_DIFF_EQ != -99 then CC_DIFF_EQ
			when CC_BUS_MATH != -99 then CC_BUS_MATH
			else -99
		end,
	IsTransferNonStem = 
		case
			when CC_GE_MATH != -99 then CC_GE_MATH
			when CC_STATISTICS != -99 then CC_STATISTICS
			else -99
		end;