USE calpass;

GO

IF (object_id('mmap.RemedialUpperBound') is not null)
	BEGIN
		DROP TABLE mmap.RemedialUpperBound;
	END;

GO

CREATE TABLE
	mmap.RemedialUpperBound
	(
		RemedialUpperBoundId int identity(0,1),
		OrganizationId int not null,
		Entity varchar(30) not null,
		Attribute varchar(30) not null,
		Value sql_variant not null
	);

GO

ALTER TABLE
	mmap.RemedialUpperBound
ADD CONSTRAINT
	pk_RemedialUpperBound
PRIMARY KEY CLUSTERED
	(
		RemedialUpperBoundId
	);

ALTER TABLE
	mmap.RemedialUpperBound
ADD CONSTRAINT
	uk_RemedialUpperBound__Entry
UNIQUE
	(
		OrganizationId,
		Entity,
		Attribute
	);

ALTER TABLE
	mmap.RemedialUpperBound
ADD CONSTRAINT
	fk_RemedialUpperBound__Organization
FOREIGN KEY
	(
		OrganizationId
	)
REFERENCES
	dbo.Organization
	(
		OrganizationId
	);

GO

INSERT INTO
	mmap.RemedialUpperBound
	(
		OrganizationId,
		Entity,
		Attribute,
		Value
	)
VALUES
	(170802,'Reading','LevelOne',1),
	(170802,'Esl','LevelTwo',1),
	(170771,'Esl','LevelOne',1),
	(170752,'Esl','LevelOne',1),
	(170849,'Reading','College',1),
	(170830,'Reading','LevelOne',1),
	(170830,'Esl','LevelOne',1),
	(170739,'Reading','LevelOne',1);