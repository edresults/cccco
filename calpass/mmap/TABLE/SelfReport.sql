USE calpass;

GO

IF (object_id('mmap.SelfReport') is not null)
	BEGIN
		DROP TABLE mmap.SelfReport;
	END;

GO

CREATE TABLE
	mmap.SelfReport
	(
		CollegeCode char(3) not null,
		StudentId varchar(10) not null,
		IdStatus char(1) not null,
		Birthdate char(8),
		Gender char(1),
		NameFirst varchar(30),
		NameLast varchar(40),
		EnglCourseId varchar(12),
		EnglGradeCode varchar(3),
		MathCourseId varchar(12),
		MathGradeCode varchar(3),
		MathCourseIdCgteq varchar(12),
		CumulativeGpa decimal(3,2)
	);

GO

ALTER TABLE
	mmap.SelfReport
ADD CONSTRAINT
	pk_SelfReport
PRIMARY KEY CLUSTERED
	(
		CollegeCode,
		StudentId,
		IdStatus
	);