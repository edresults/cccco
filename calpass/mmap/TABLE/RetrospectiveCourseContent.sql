USE calpass;

GO

IF (object_id('mmap.RetrospectiveCourseContent') is not null)
	BEGIN
		DROP TABLE mmap.RetrospectiveCourseContent;
	END;

GO

CREATE TABLE
	mmap.RetrospectiveCourseContent
	(
		InterSegmentKey  binary(64)   not null,
		DepartmentCode   tinyint      not null,
		GradeCode        char(2)      not null,
		SchoolCode       char(14)     not null,
		YearTermCode     char(5)      not null,
		TermCode         char(2)      not null,
		ContentCode      varchar(25)  not null,
		ContentRank      tinyint      not null,
		CourseCode       char(4)      not null,
		CourseUnits      decimal(4,2)     null,
		CourseTitle      varchar(40)  not null,
		CourseAGCode     char(2)          null,
		CourseLevelCode  char(2)          null,
		CourseTypeCode   char(2)          null,
		SectionMark      varchar(3)   not null,
		MarkPoints       decimal(2,1) not null,
		MarkCategory     smallint     not null,
		IsSuccess        bit          not null,
		IsPromoted       bit          not null,
		ContentSelector  tinyint      not null,
		GradeSelector    tinyint      not null,
		RecencySelector  tinyint      not null
	);

GO

ALTER TABLE
	mmap.RetrospectiveCourseContent
ADD CONSTRAINT
	PK_RetrospectiveCourseContent
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey,
		DepartmentCode,
		RecencySelector
	);

ALTER TABLE
	mmap.RetrospectiveCourseContent
ADD CONSTRAINT
	FK_RetrospectiveCourseContent__InterSegmentKey
FOREIGN KEY
	(
		InterSegmentKey
	)
REFERENCES
	dbo.Student
	(
		InterSegmentKey
	);

ALTER TABLE
	mmap.RetrospectiveCourseContent
ADD CONSTRAINT
	FK_RetrospectiveCourseContent__DepartmentCode
FOREIGN KEY
	(
		DepartmentCode
	)
REFERENCES
	calpads.Department
	(
		Code
	);