CREATE TABLE
	Mmap.Credits
	(
		Id    TINYINT     IDENTITY,
		Code  CHAR(1)     NOT NULL,
		Type  CHAR(2)     NOT NULL,
		Label VARCHAR(30) NOT NULL,
		-- PK
		CONSTRAINT PK_Credits PRIMARY KEY CLUSTERED ( Id ),
		-- AK
		CONSTRAINT AK_Credits_Code  UNIQUE ( Code ),
		CONSTRAINT AK_Credits_Label UNIQUE ( Label )
	);

GO

INSERT INTO
	Mmap.Credits
	(
		Code,
		Type,
		Label
	)
VALUES
	('D','CR','Credit: Degree Applicable'),
	('C','CR','Credit: Not Degree Applicable'),
	('N','NC','Non-Credit');