IF (object_id('Mmap.RetrospectivePerformance') is not null)
	BEGIN
		DROP TABLE Mmap.RetrospectivePerformance;
	END;

-- SYSTEM
-- SEGMENT
-- MODULE
-- N
-- A
-- M
-- E

GO

CREATE TABLE
	Mmap.RetrospectivePerformance
	(
		InterSegmentKey binary(64) not null,
		DepartmentCode tinyint not null,
		GradeCode char(2) not null,
		QualityPoints decimal(9,3) null,
		CreditAttempted decimal(9,3) null,
		GradePointAverage decimal(9,3) null,
		GradePointAverageSans decimal(9,3) null,
		CumulativeQualityPoints decimal(9,3) null,
		CumulativeCreditAttempted decimal(9,3) null,
		CumulativeGradePointAverage decimal(9,3) null,
		CumulativeGradePointAverageSans decimal(9,3) null,
		FirstYearTermCode char(5) null,
		LastYearTermCode char(5) null,
		IsFirst bit null,
		IsLast bit null
	);

GO

ALTER TABLE
	Mmap.RetrospectivePerformance
ADD CONSTRAINT
	PK_RetrospectivePerformance
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey,
		DepartmentCode,
		GradeCode
	);

ALTER TABLE
	mmap.RetrospectivePerformance
ADD CONSTRAINT
	FK_RetrospectivePerformance__InterSegmentKey
FOREIGN KEY
	(
		InterSegmentKey
	)
REFERENCES
	dbo.Student
	(
		InterSegmentKey
	);

ALTER TABLE
	mmap.RetrospectivePerformance
ADD CONSTRAINT
	FK_RetrospectivePerformance__DepartmentCode
FOREIGN KEY
	(
		DepartmentCode
	)
REFERENCES
	calpads.Department
	(
		Code
	);