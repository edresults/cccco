USE calpass;

GO

IF (object_id('mmap.StudentTranscriptSummary') is not null)
	BEGIN
		DROP TABLE mmap.StudentTranscriptSummary;
	END;

GO

CREATE TABLE
	mmap.StudentTranscriptSummary
	(
		InterSegmentKey binary(64) not null,
		Cgpa decimal(4,3),
		English00 decimal(4,3),
		English09 decimal(4,3),
		English10 decimal(4,3),
		English11 decimal(4,3),
		English12 decimal(4,3),
		EnglishAP decimal(4,3),
		Arithmetic decimal(4,3),
		PreAlgebra decimal(4,3),
		AlgebraI decimal(4,3),
		Geometry decimal(4,3),
		AlgebraII decimal(4,3),
		[Statistics] decimal(4,3),
		PreCalculus decimal(4,3),
		Trigonometry decimal(4,3),
		Calculus decimal(4,3)
	);

GO

ALTER TABLE
	mmap.StudentTranscriptSummary
ADD CONSTRAINT
	PK_StudentTranscriptSummary
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey
	);