USE calpass;

GO

IF (object_id('mmap.ProspectiveModel') is not null)
	BEGIN
		DROP TABLE mmap.ProspectiveModel;
	END;

GO

CREATE TABLE
	mmap.ProspectiveModel
	(
		Id int identity(0,1),
		Name varchar(255) not null
	);

GO

ALTER TABLE
	mmap.ProspectiveModel
ADD CONSTRAINT
	PK_ProspectiveModel
PRIMARY KEY CLUSTERED
	(
		Id
	);

ALTER TABLE
	mmap.ProspectiveModel
ADD CONSTRAINT
	UK_ProspectiveModel__Name
UNIQUE
	(
		Name
	);

GO

INSERT INTO
	mmap.ProspectiveModel
	(
		Name
	)
VALUES
	('Direct Matriculant'),
	('Non-Direct Matriculant');