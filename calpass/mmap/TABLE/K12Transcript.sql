USE calpass;

GO

IF (object_id('mmap.K12Transcript', 'U') is not null)
	BEGIN
		DROP TABLE mmap.K12Transcript;
	END;

GO

CREATE TABLE
	mmap.K12Transcript
	(
		StudentStateId              char(10)     not null,
		IsHighSchoolGrade09         bit          not null,
		IsHighSchoolGrade10         bit          not null,
		IsHighSchoolGrade11         bit          not null,
		IsHighSchoolGrade12         bit          not null,
		CumulativeGradePointAverage decimal(4,3) not null,
		English                     decimal(2,1)     null,
		PreAlgebra                  decimal(2,1)     null,
		AlgebraI                    decimal(2,1)     null,
		Geometry                    decimal(2,1)     null,
		AlgebraII                   decimal(2,1)     null,
		Trigonometry                decimal(2,1)     null,
		PreCalculus                 decimal(2,1)     null,
		[Statistics]                decimal(2,1)     null,
		Calculus                    decimal(2,1)     null,
		IsEscrow                    bit          not null
	);

GO

ALTER TABLE
	mmap.K12Transcript
ADD CONSTRAINT
	PK_K12Transcript
PRIMARY KEY CLUSTERED
	(
		StudentStateId
	);