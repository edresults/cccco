USE calpass;

GO

IF (object_id('mmap.ProspectiveBranch') is not null)
	BEGIN
		DROP TABLE mmap.ProspectiveBranch;
	END;

GO

CREATE TABLE
	mmap.ProspectiveBranch
	(
		Id int identity(0,1),
		ProspectiveCourseId int not null,
		LeafCount tinyint not null
	);

GO

ALTER TABLE
	mmap.ProspectiveBranch
ADD CONSTRAINT
	PK_ProspectiveBranch
PRIMARY KEY CLUSTERED
	(
		Id
	);

ALTER TABLE
	mmap.ProspectiveBranch
ADD CONSTRAINT
	FK_ProspectiveBranch__ProspectiveCourseId
FOREIGN KEY
	(
		ProspectiveCourseId
	)
REFERENCES
	mmap.ProspectiveCourse
	(
		Id
	)
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE
	mmap.ProspectiveBranch
ADD CONSTRAINT
	DF_ProspectiveBranch__LeafCount
DEFAULT
	(
		0
	)
FOR
	LeafCount;

GO

INSERT INTO
	mmap.ProspectiveBranch
	(
		ProspectiveCourseId
	)
VALUES
	-- English
	(0), -- Direct Matriculant; EnglishLevel0
	(1), -- Non-Direct Matriculant; EnglishLevel0
	(2), -- Direct Matriculant; EnglishLevel1
	(3), -- Non-Direct Matriculant; EnglishLevel1
	(4), -- Direct Matriculant; EnglishLevel2
	(5), -- Non-Direct Matriculant; EnglishLevel2; 1
	(5), -- Non-Direct Matriculant; EnglishLevel2; 2
	(6), -- Direct Matriculant; EnglishLevel3
	(7), -- Non-Direct Matriculant; EnglishLevel3; 1
	(7), -- Non-Direct Matriculant; EnglishLevel3; 2
	-- Mathematics
	(8), -- Direct Matriculant; PreAlgebra
	(9), -- Non-Direct Matriculant; PreAlgebra
	(10), -- Direct Matriculant; AlgebraI
	(11), -- Non-Direct Matriculant; AlgebraI; 1
	(11), -- Non-Direct Matriculant; AlgebraI; 2
	(12), -- Direct Matriculant; AlgebraII
	(13), -- Non-Direct Matriculant; AlgebraII; 1
	(13), -- Non-Direct Matriculant; AlgebraII; 2
	(13), -- Non-Direct Matriculant; AlgebraII; 3
	(14), -- Direct Matriculant; MathGE
	(15), -- Non-Direct Matriculant; MathGE; 1
	(15), -- Non-Direct Matriculant; MathGE; 2
	(16), -- Direct Matriculant; Statistics; 1
	(16), -- Direct Matriculant; Statistics; 2
	(17), -- Non-Direct Matriculant; Statistics; 1
	(17), -- Non-Direct Matriculant; Statistics; 2
	(18), -- Direct Matriculant; CollegeAlgebra; 1
	(18), -- Direct Matriculant; CollegeAlgebra; 2
	(19), -- Non-Direct Matriculant; CollegeAlgebra; 1
	(19), -- Non-Direct Matriculant; CollegeAlgebra; 2
	(19), -- Non-Direct Matriculant; CollegeAlgebra; 3
	(20), -- Direct Matriculant; Trigonometry; 1
	(20), -- Direct Matriculant; Trigonometry; 2
	(20), -- Direct Matriculant; Trigonometry; 3
	(21), -- Non-Direct Matriculant; Trigonometry; 1
	(21), -- Non-Direct Matriculant; Trigonometry; 2
	(22), -- Direct Matriculant; PreCalculus; 1
	(22), -- Direct Matriculant; PreCalculus; 2
	(23), -- Non-Direct Matriculant; PreCalculus; 1
	(23), -- Non-Direct Matriculant; PreCalculus; 2
	(23), -- Non-Direct Matriculant; PreCalculus; 3
	(24), -- Direct Matriculant; CalculusI; 1
	(24), -- Direct Matriculant; CalculusI; 2
	(25), -- Non-Direct Matriculant; CalculusI; 1
	(25), -- Non-Direct Matriculant; CalculusI; 2
	-- Reading
	(26), -- Direct Matriculant; Ubound0ReadingLevelT
	(27), -- Non-Direct Matriculant; Ubound0ReadingLevelT
	(28), -- Direct Matriculant; Ubound0ReadingLevel0
	(29), -- Non-Direct Matriculant; Ubound0ReadingLevel0
	(30), -- Direct Matriculant; Ubound0ReadingLevel1
	(31), -- Non-Direct Matriculant; Ubound0ReadingLevel1
	(32), -- Direct Matriculant; Ubound0ReadingLevel2
	(33), -- Non-Direct Matriculant; Ubound0ReadingLevel2
	(34), -- Direct Matriculant; Ubound0ReadingLevel3
	(35), -- Non-Direct Matriculant; Ubound0ReadingLevel3; 1
	(35), -- Non-Direct Matriculant; Ubound0ReadingLevel3; 2
	(36), -- Direct Matriculant; Ubound1ReadingLevelT
	(37), -- Non-Direct Matriculant; Ubound1ReadingLevelT
	(38), -- Direct Matriculant; Ubound1ReadingLevel1
	(39), -- Non-Direct Matriculant; Ubound1ReadingLevel1; 1
	(39), -- Non-Direct Matriculant; Ubound1ReadingLevel1; 2
	(40), -- Direct Matriculant; Ubound1ReadingLevel2
	(41), -- Non-Direct Matriculant; Ubound1ReadingLevel2
	(42), -- Non-Direct Matriculant; Ubound1ReadingLevel3
	(43), -- Direct Matriculant; Ubound2ReadingLevelT
	(44), -- Non-Direct Matriculant; Ubound2ReadingLevelT; 1
	(44), -- Non-Direct Matriculant; Ubound2ReadingLevelT; 2
	(45), -- Direct Matriculant; Ubound2ReadingLevel2
	(46), -- Non-Direct Matriculant; Ubound2ReadingLevel2
	(47), -- Direct Matriculant; Ubound2ReadingLevel3
	(48), -- Non-Direct Matriculant; Ubound2ReadingLevel3
	-- Esl
	(49), -- Direct Matriculant; Ubound0EslLevel0
	(50), -- Non-Direct Matriculant; Ubound0EslLevel0
	(51), -- Direct Matriculant; Ubound0EslLevel1
	(52), -- Non-Direct Matriculant; Ubound0EslLevel1
	(53), -- Direct Matriculant; Ubound0EslLevel2
	(54), -- Non-Direct Matriculant; Ubound0EslLevel2
	(55), -- Direct Matriculant; Ubound1EslLevel1
	(56), -- Non-Direct Matriculant; Ubound1EslLevel1
	(57), -- Direct Matriculant; Ubound1EslLevel2
	(58), -- Non-Direct Matriculant; Ubound1EslLevel2
	(59), -- Direct Matriculant; Ubound1EslLevel3; 1
	(59), -- Direct Matriculant; Ubound1EslLevel3; 2
	(59), -- Direct Matriculant; Ubound1EslLevel3; 3
	(59), -- Direct Matriculant; Ubound1EslLevel3; 4
	(60), -- Non-Direct Matriculant; Ubound1EslLevel3
	(61), -- Direct Matriculant; Ubound2EslLevel2
	(62), -- Non-Direct Matriculant; Ubound2EslLevel2; 1
	(62), -- Non-Direct Matriculant; Ubound2EslLevel2; 2
	(63), -- Direct Matriculant; Ubound2EslLevel3
	(64), -- Direct Matriculant; Ubound2EslLevel4
	(64); -- Non-Direct Matriculant; Ubound2EslLevel4