USE calpass;

GO

IF (object_id('mmap.English') is not null)
	BEGIN
		DROP TABLE mmap.English;
	END;

GO

CREATE TABLE
	mmap.English
	(
		CourseCode char(4) not null,
		ContentCode varchar(25) not null,
		Rank tinyint not null,
		IsPrimary bit not null
	);

GO

ALTER TABLE
	mmap.English
ADD CONSTRAINT
	PK_English
PRIMARY KEY CLUSTERED
	(
		CourseCode,
		ContentCode
	);

GO

INSERT INTO
	mmap.English
	(
		CourseCode,
		ContentCode,
		Rank,
		IsPrimary
	)
VALUES
	('2100','Remedial',1,1),
	('2101','Comprehensive',5,1),
	('2102','Remedial',1,1),
	('2105','Literature',6,1),
	('2106','Literature',6,1),
	('2107','Literature',6,1),
	('2108','Literature',6,1),
	('2109','Literature',6,1),
	('2110','Remedial',1,1),
	('2111','Composition',7,1),
	('2112','Rhetoric',5,1),
	('2113','Composition',7,1),
	('2114','Composition',8,1),
	('2115','Rhetoric',5,1),
	('2116','Remedial',1,1),
	('2117','Literature',6,1),
	('2118','Composition',7,1),
	('2120','Remedial',1,1),
	('2130','English09',2,1),
	('2131','English10',3,1),
	('2132','English11',4,1),
	('2133','English12',5,1),
	('2160','Literature',6,1),
	('2161','Remedial',1,1),
	('2170','EnglishAP',9,1),
	('2171','EnglishAP',9,1),
	('2173','Literature',6,1),
	('2174','Literature',6,1),
	('2175','Literature',6,1),
	('2190','DualEnrollment',9,1);