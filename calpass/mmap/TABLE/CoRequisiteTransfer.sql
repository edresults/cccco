CREATE TABLE
	calpass.mmap.CoRequisiteTransfer
	(
		college_id         char(3)     not null,
		control_number     varchar(12) not null,
		course_id          varchar(12) not null,
		CONSTRAINT
			PK_CoRequisiteTransfer
		PRIMARY KEY
			(
				college_id,
				control_number
			)
	);

GO

INSERT INTO
	calpass.mmap.CoRequisiteTransfer
	(
		college_id,
		control_number,
		course_id
	)
VALUES
	('021','CCC000124615','MATH175'),
	('021','CCC000198733','MATH176'),
	('021','CCC000310098','MATH178'),
	('021','CCC000547216','MATH160'),
	('021','CCC000335336','ENGL120'),
	('021','CCC000578010','ENGL120'),
	('051','CCC000335943','ENGL100'),
	('051','CCC000550339','ENGL100'),
	('072','CCC000023704','ENGL101'),
	('073','CCC000372083','ENGL101'),
	('171','CCC000354042','ENGL-1A'),
	('233','CCC000309428','ENGW300-2406'),
	('281','CCC000435163','ENGL001'),
	('281','CCC000567531','ENGL001'),
	('281','CCC000590670','ENGL001'),
	('313','CCC000241172','MATH034'),
	('341','CCC000376233','MATH013'),
	('373','CCC000375204','ENGL100'),
	('523','CCC000340135','ENGLP101A'),
	('523','CCC000582809','ENGLP101A'),
	('551','CCC000366355','ENG001A'),
	('581','CCC000283340','ENG-001A'),
	('592','CCC000066812','ENGL101'),
	('731','CCC000157746','MATH136'),
	('771','CCC000391838','ENGL001A'),
	('811','CCC000388774','ENGL100'),
	('832','CCC000125303','ENGLG100'),
	('892','CCC000419132','WR1'),
	('941','CCC000289406','ENGL-101');