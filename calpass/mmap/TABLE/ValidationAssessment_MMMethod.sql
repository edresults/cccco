USE calpass;

GO

IF (object_id('mmap.ValidationAssessment_MMMethod') is not null)
	BEGIN
		DROP TABLE mmap.ValidationAssessment_MMMethod;
	END;

GO

CREATE TABLE
	mmap.ValidationAssessment_MMMethod
	(
		MMMethodId tinyint identity(1,1),
		Description varchar(255)
	);

ALTER TABLE
	mmap.ValidationAssessment_MMMethod
ADD CONSTRAINT
	pk_ValidationAssessment_MMMethod
PRIMARY KEY CLUSTERED
	(
		MMMethodId
	);

GO

SET IDENTITY_INSERT mmap.ValidationAssessment_MMMethod ON;

INSERT INTO
	mmap.ValidationAssessment_MMMethod
	(
		MMMethodId,
		Description
	)
VALUES
	(1,'Multiple Measures (Local Rulesets OLD)'),
	(2,'Multiple Measures (Locally developed empirically derived Ruleset NEW)'),
	(3,'Multiple Measures (MMAP State Ruleset with modifications)'),
	(4,'Multiple Measures (MMAP State Ruleset no modifications)'),
	(5,'No Multiple Measures (just test scores)'),
	(6,'Other'),
	(7,'Unknown');

SET IDENTITY_INSERT mmap.ValidationAssessment_MMMethod OFF;