CREATE TABLE
    Mmap.Files
    (
        Id       TINYINT     IDENTITY,
        Label    VARCHAR(25) NOT NULL,
        CONSTRAINT PK_Files PRIMARY KEY CLUSTERED ( Id ),
        CONSTRAINT AK_Files_Label UNIQUE ( Label )
    );

GO

INSERT INTO
    Mmap.Files
    (
        Label
    )
VALUES
    ('Retrospective ESL'),
    ('Retrospective English'),
    ('Retrospective Mathematics');

GO

INSERT INTO
    mmap.files
    (
        Label
    )
VALUES
    ('Student Centered Funding Formula');

GO

CREATE TABLE
    mmap.Exports
    (
        Id          tinyint      not null identity,
        FileId      tinyint      not null,
        Description varchar(255) not null,
        FileDate    Datetime2    not null
            CONSTRAINT DF_Exports_FileDate DEFAULT CURRENT_TIMESTAMP,
        CONSTRAINT PK_Exports PRIMARY KEY CLUSTERED ( Id ),
        CONSTRAINT FK_Exports_FileId FOREIGN KEY ( FileId ) REFERENCES mmap.Files ( Id )
    );

CREATE TABLE
    mmap.Identifiers
    (
        ExportId        tinyint    not null,
        InterSegmentKey binary(64) not null,
        StudentId       integer    not null,
        CONSTRAINT PK_Identifiers PRIMARY KEY CLUSTERED ( ExportId, InterSegmentKey ),
        CONSTRAINT FK_Identifiers FOREIGN KEY ( ExportId ) REFERENCES mmap.Exports ( Id )
    );