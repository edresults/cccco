USE calpass;

GO

IF (object_id('mmap.NonCognitiveGrit') is not null)
	BEGIN
		DROP TABLE mmap.NonCognitiveGrit;
	END;

GO

CREATE TABLE
	mmap.NonCognitiveGrit
	(
		CollegeCode char(3) not null,
		StudentId varchar(10) not null,
		IdStatus char(1) not null,
		Birthdate char(8),
		Gender char(1),
		NameFirst varchar(30),
		NameLast varchar(40),
		Discourage varchar(18),
		Distract varchar(18),
		Diligent varchar(18),
		Change varchar(18),
		Worker varchar(18),
		Maintain varchar(18),
		Pursue varchar(18),
		Finish varchar(18)
	);

GO

ALTER TABLE
	mmap.NonCognitiveGrit
ADD CONSTRAINT
	pk_NonCognitiveGrit
PRIMARY KEY CLUSTERED
	(
		CollegeCode,
		StudentId,
		IdStatus
	);