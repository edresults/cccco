IF (object_id('mmap.CapCohort') is not null)
	BEGIN
		DROP TABLE mmap.CapCohort;
	END;

GO

CREATE TABLE
	mmap.CapCohort
	(
		InterSegmentKey binary(64) not null,
		TopCode char(6) not null,
		CohortYear char(9) not null
	);

ALTER TABLE
	mmap.CapCohort
ADD CONSTRAINT
	PK_CapCohort
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey,
		TopCode
	);

GO

DECLARE
	@AcademicYear
AS TABLE
	(
		Iterator tinyint identity(0, 1),
		YearCode char(9)
	);

INSERT
	@AcademicYear
	(
		YearCode
	)
VALUES
	('2012-2013'),
	('2013-2014'),
	('2014-2015'),
	('2015-2016');

DECLARE
	@YearCode char(9),
	@Iterator tinyint = 0,
	@Count tinyint = 0;

SELECT
	@Count = count(*)
FROM
	@AcademicYear;

WHILE (@Iterator < @Count)
BEGIN

	SELECT
		@YearCode = YearCode
	FROM
		@AcademicYear
	WHERE
		Iterator = @Iterator;

	INSERT
		mmap.CapCohort
		(
			InterSegmentKey,
			TopCode,
			CohortYear
		)
	SELECT
		id.InterSegmentKey,
		cb.top_code,
		@YearCode
	FROM
		comis.studntid id
		inner join
		comis.stterm st
			on st.college_id = id.college_id
			and st.student_id = id.student_id
		inner join
		comis.sxenrlm sx
			on sx.college_id = st.college_id
			and sx.student_id = st.student_id
			and sx.term_id = st.term_id
		inner join
		comis.cbcrsinv cb
			on cb.college_id = sx.college_id
			and cb.course_id = sx.course_id
			and cb.control_number = sx.control_number
			and cb.term_id = sx.term_id
		inner join
		comis.term t
			on st.term_id = t.TermCode
	WHERE
		sx.units_attempted > 1
		and sx.credit_flag in ('T','D','C','S')
		and id.college_id != '841'
		and (
			(
				cb.top_code = '170100'
				and
				(
					cb.prior_to_college <> 'Y'
					or
					(
						cb.prior_to_college = 'Y'
						and
						cb.transfer_status in ('A', 'B') -- must be A and B; trig courses can be code CSU only
					)
				)
			)
			or
			(
				cb.top_code = '150100'
				and
				(
					cb.Title not like '%LIT%'
					and
					(
						(
							case
								when cb.college_id = '651' and cb.course_id = 'ENG100' then 'A' -- SBCC MOD
								else cb.prior_to_college
							end <> 'Y'
						)
						or
						(
							case
								when cb.college_id = '651' and cb.course_id = 'ENG100' then 'A' -- SBCC MOD
								else cb.prior_to_college
							end = 'Y'
							and
							cb.transfer_status = 'A'
						)
					)
				)
			)
		)
	GROUP BY
		id.InterSegmentKey,
		cb.top_code
	HAVING
		min(t.AcademicYear) = @YearCode;

	-- Iterate
	SET @Iterator += 1;
END;