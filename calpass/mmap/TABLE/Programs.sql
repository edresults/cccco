CREATE TABLE
	Mmap.Programs
	(
		Id      TINYINT     IDENTITY,
		Rank    TINYINT     NOT NULL,
		Code    CHAR(6)     NOT NULL,
		Abbr    CHAR(2)     NOT NULL,
		Label   VARCHAR(25) NOT NULL,
		Content VARCHAR(4)  NOT NULL,
		-- PK
		CONSTRAINT PK_Programs PRIMARY KEY ( Id ),
		-- AK
		CONSTRAINT AK_rograms UNIQUE CLUSTERED ( Code )
	);

GO

INSERT INTO
	Mmap.Programs
	(
		Code,
		Rank,
		Content,
		Abbr,
		Label
	)
VALUES
	('170100',0,'MATH','MA','Mathematics'),
	('150100',0,'ENGL','EN','English'),
	('152000',0,'READ','RD','Reading'),
	('493084',2,'ESL','EW','ESL Writing'),
	('493085',3,'ESL','ER','ESL Reading'),
	('493086',4,'ESL','ES','ESL Speaking'),
	('493087',1,'ESL','EI','ESL Integrated'),
	('493090',6,'ESL','EC','ESL Citizenship'),
	('493100',5,'ESL','EV','ESL Vocational');