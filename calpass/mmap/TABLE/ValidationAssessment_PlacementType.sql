USE calpass;

GO

IF (object_id('mmap.ValidationAssessment_PlacementType') is not null)
	BEGIN
		DROP TABLE mmap.ValidationAssessment_PlacementType;
	END;

GO

CREATE TABLE
	mmap.ValidationAssessment_PlacementType
	(
		PlacementTypeId tinyint identity(1,1),
		Description varchar(255)
	);

ALTER TABLE
	mmap.ValidationAssessment_PlacementType
ADD CONSTRAINT
	pk_ValidationAssessment_PlacementType
PRIMARY KEY CLUSTERED
	(
		PlacementTypeId
	);

GO

SET IDENTITY_INSERT mmap.ValidationAssessment_PlacementType ON;

INSERT INTO
	mmap.ValidationAssessment_PlacementType
	(
		PlacementTypeId,
		Description
	)
VALUES
	(1,'Disjunctive placement (Highest placement across MM and Test)'),
	(2,'Compensatory placement (MM information are combined with Test information. E.g., add or subtract points on test score or weighting each element)'),
	(3,'Conjunctive placement'),
	(4,'Student is not required to take test if MM is available'),
	(5,'Other'),
	(6,'Unknown');

SET IDENTITY_INSERT mmap.ValidationAssessment_PlacementType OFF;