DROP TABLE mmap.CotcIrtEnglish;

GO

SELECT
	ccc.YearTermCode,
	ccc.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage,
	-- high school
	Remedial_1_MarkCategory     = isnull(max(case when rcc.ContentCode = 'Remedial'    and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	Remedial_2_MarkCategory     = isnull(max(case when rcc.ContentCode = 'Remedial'    and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	English09_1_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English09'   and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	English09_2_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English09'   and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	English10_1_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English10'   and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	English10_2_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English10'   and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	English11_1_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English11'   and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	English11_2_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English11'   and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	English12_1_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English12'   and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	English12_2_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English12'   and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	Expository_1_MarkCategory   = isnull(max(case when rcc.ContentCode = 'Composition' and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	Expository_2_MarkCategory   = isnull(max(case when rcc.ContentCode = 'Composition' and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	Literature_1_MarkCateogory  = isnull(max(case when rcc.ContentCode = 'Literature'  and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	Literature_2_MarkCateogory  = isnull(max(case when rcc.ContentCode = 'Literature'  and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	LanguageAP_1_MarkCateogory  = isnull(max(case when rcc.ContentCode = 'EnglishAP'   and rcc.ContentSelector = 1 and rcc.CourseCode = '2170' then rcc.MarkCategory end), -99),
	LanguageAP_2_MarkCateogory  = isnull(max(case when rcc.ContentCode = 'EnglishAP'   and rcc.ContentSelector = 2 and rcc.CourseCode = '2170' then rcc.MarkCategory end), -99),
	LiteratureAP_1_MarkCategory = isnull(max(case when rcc.ContentCode = 'EnglishAP'   and rcc.ContentSelector = 1 and rcc.CourseCode = '2171' then rcc.MarkCategory end), -99),
	LiteratureAP_2_MarkCategory = isnull(max(case when rcc.ContentCode = 'EnglishAP'   and rcc.ContentSelector = 2 and rcc.CourseCode = '2171' then rcc.MarkCategory end), -99),
	Rhetoric_1_MarkCategory     = isnull(max(case when rcc.ContentCode = 'Rhetoric'    and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	Rhetoric_2_MarkCategory     = isnull(max(case when rcc.ContentCode = 'Rhetoric'    and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	-- college
	cc_00_course_grade_category = isnull(max(case when CourseLevel = 'Y' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end end), -99),
	cc_01_course_grade_category = isnull(max(case when CourseLevel = 'A' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end end), -99),
	cc_02_course_grade_category = isnull(max(case when CourseLevel = 'B' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end end), -99),
	cc_03_course_grade_category = isnull(max(case when CourseLevel = 'C' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end end), -99),
	cc_04_course_grade_category = isnull(max(case when CourseLevel = 'D' then case when ccc.SectionMark = 'W' then 0 else ccc.MarkCategory end end), -99)
INTO
	mmap.CotcIrtEnglish
FROM
	mmap.CollegeCourseContent ccc
	left outer join
	(
		SELECT
			rp.InterSegmentKey,
			OverallCumulativeGradePointAverage = max(case when DepartmentCode = 0 then CumulativeGradePointAverage end),
			SubjectCumulativeGradePointAverage = max(case when DepartmentCode = 14 then CumulativeGradePointAverage end),
			WithoutSubjectOverallCumulativeGradePointAverage = 
				convert(
					decimal(9,3),
					isnull(
						case
							when max(case when DepartmentCode = 0 then CumulativeQualityPoints end) - max(case when DepartmentCode = 14 then CumulativeQualityPoints end) <= 0 then 0
							else max(case when DepartmentCode = 0 then CumulativeQualityPoints end) - max(case when DepartmentCode = 14 then CumulativeQualityPoints end)
						end
						/
						nullif(
							case
								when max(case when DepartmentCode = 0 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 14 then CumulativeCreditAttempted end) <= 0 then 0
								else max(case when DepartmentCode = 0 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 14 then CumulativeCreditAttempted end)
							end,
							0
						)
					,0)
				)
		FROM
			mmap.RetrospectivePerformance rp
		WHERE
			rp.DepartmentCode in (0, 14)
			and rp.IsLast = 1
		GROUP BY
			rp.InterSegmentKey
	) rp
		on rp.InterSegmentkey = ccc.InterSegmentkey
	left outer join
	mmap.RetrospectiveCourseContent rcc
		on rcc.InterSegmentKey = ccc.InterSegmentKey
		and rcc.DepartmentCode = 14
		and rcc.ContentSelector in (1,2)
WHERE
	ccc.TopCode = '150100'
	and ccc.FirstSelector = 1
	and ccc.CourseLevel = 'Y'
	and ccc.CollegeId = '661'
	and ccc.CollegeCounter = 1
GROUP BY
	ccc.YearTermCode,
	ccc.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage;

DELETE FROM mmap.CotcIrtEnglish WHERE OverallCumulativeGradePointAverage > 4;
DELETE FROM mmap.CotcIrtEnglish WHERE SubjectCumulativeGradePointAverage > 4;
DELETE FROM mmap.CotcIrtEnglish WHERE WithoutSubjectOverallCumulativeGradePointAverage > 4;

ALTER TABLE
	mmap.CotcIrtEnglish
ADD CONSTRAINT
	PK_CotcIrtEnglish
PRIMARY KEY CLUSTERED
	(
		InterSegmentkey
	);