USE calpass;

GO

IF (object_id('mmap.NonCognitiveIvc') is not null)
	BEGIN
		DROP TABLE mmap.NonCognitiveIvc;
	END;

GO

CREATE TABLE
	mmap.NonCognitiveIvc
	(
		college_id char(3) not null,
		student_id char(9) not null,
		id_status char(1) not null,
		birthdate char(8),
		gender char(1),
		name_first varchar(30),
		name_last varchar(40),
		DateofSurvey datetime,
		q0004 int,
		q0005 int,
		q0006 int,
		q0007 int,
		q0008 int,
		q0009 int,
		q0010 int,
		q0011 int,
		q0012 int,
		q0013 int,
		q0014 int,
		q0015 int,
		q0016 int,
		q0017 int,
		q0018 int,
		q0019 int,
		q0020 int,
		q0021 int,
		q0022 int,
		q0023 int,
		q0024 int,
		q0025 int,
		q0026 int,
		q0027 int,
		q0028 int,
		q0029 int,
		q0030 int,
		q0031 int,
		q0032 int,
		q0033 int,
		q0034 int,
		q0035 int,
		q0036 int,
		q0037 int,
		q0038 int,
		q0039 int,
		q0040 int,
		q0041 int,
		q0042 int,
		q0043 int,
		q0044 int,
		q0045 int,
		q0046 int,
		q0047 int,
		q0048 int,
		q0049 int,
		q0050 int,
		q0051 int,
		q0052 int,
		q0053 int,
		q0054 int,
		q0055 int,
		q0056 int,
		q0057 int,
		q0058 varchar(255),
		q0059 int,
		q0059_other varchar(255),
		q0060 int,
		q0061 int,
		q0061_other varchar(255),
		q0062 int,
		q0063 int,
		q0063_other varchar(255)
	);

GO

ALTER TABLE
	mmap.NonCognitiveIvc
ADD CONSTRAINT
	pk_NonCognitiveIvc
PRIMARY KEY CLUSTERED
	(
		college_id,
		student_id,
		id_status
	);