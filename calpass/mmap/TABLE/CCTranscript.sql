CREATE TABLE
	Mmap.CCTranscript
	(
		CollegeCode         CHAR(3)      NOT NULL,
		StudentId           CHAR(9)      NOT NULL,
		YearTermCode        INTEGER      NOT NULL,
		CourseId            VARCHAR(12)  NOT NULL,
		CourseControlNumber CHAR(12)     NOT NULL,
		CourseSectionId     VARCHAR(6)   NOT NULL,
		CourseTopCode       CHAR(6)      NOT NULL,
		CourseTitle         VARCHAR(68)  NOT NULL,
		CourseMarkLetter    VARCHAR(3)   NOT NULL,
		CourseMarkPoints    DECIMAL(4,2) NOT NULL,
		CourseLevelCode     CHAR(1)      NOT NULL,
		CourseCreditCode    CHAR(1)      NOT NULL,
		CourseUnitsTry      DECIMAL(4,2) NOT NULL,
		CourseUnitsGet      DECIMAL(4,2) NOT NULL,
		InterSegmentKey     BINARY(64)   NOT NULL,
		Selector            SMALLINT         NULL,
		SelectorFL          SMALLINT         NULL,
		SelectorCN          SMALLINT         NULL,
		SelectorCNFL        SMALLINT         NULL,
		CONSTRAINT
			PK_CCTranscript
		PRIMARY KEY
			(
				CollegeCode,
				StudentId,
				YearTermCode,
				CourseId,
				CourseControlNumber,
				CourseSectionId
			)
	);
	