USE calpass;

GO

IF (object_id('mmap.Model') is not null)
	BEGIN
		DROP TABLE mmap.Model;
	END;

GO

CREATE TABLE
	mmap.Model
	(
		ModelId int identity(0,1),
		OrganizationId int not null,
		Description varchar(255) not null
	);

GO

ALTER TABLE
	mmap.Model
ADD CONSTRAINT
	pk_Model
PRIMARY KEY CLUSTERED
	(
		ModelId
	);

ALTER TABLE
	mmap.Model
ADD CONSTRAINT
	fk_Model__Organization
FOREIGN KEY
	(
		OrganizationId
	)
REFERENCES
	dbo.Organization
	(
		OrganizationId
	);

GO

INSERT INTO
	mmap.Model
	(
		OrganizationId,
		Description
	)
VALUES
	(175721,'Non-Direct Matriculant'),
	(175721,'Direct Matriculant');