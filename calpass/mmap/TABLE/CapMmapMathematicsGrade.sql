DROP TABLE mmap.CapMmapMathematicsGrade;

GO

SELECT
	CohortYear,
	b.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage,
	-- high school: 09
	hs_09_grade_level                = max(case when rcc.GradeCode = '09' then rcc.GradeCode end),
	hs_09_school_code                = max(case when rcc.GradeCode = '09' then rcc.SchoolCode end),
	hs_09_year_term_code             = max(case when rcc.GradeCode = '09' then rcc.YearTermCode end),
	hs_09_course_id                  = max(case when rcc.GradeCode = '09' then rcc.CourseCode end),
	hs_09_course_title               = max(case when rcc.GradeCode = '09' then rcc.CourseTitle end),
	hs_09_course_grade               = max(case when rcc.GradeCode = '09' then rcc.SectionMark end),
	hs_09_course_grade_points        = max(case when rcc.GradeCode = '09' then rcc.MarkPoints end),
	hs_09_course_grade_category      = max(case when rcc.GradeCode = '09' then rcc.MarkCategory end),
	hs_09_course_success_ind         = max(case when rcc.GradeCode = '09' then convert(tinyint, rcc.IsSuccess) end),
	hs_09_course_ag_code             = max(case when rcc.GradeCode = '09' then rcc.CourseAGCode end),
	hs_09_course_level_code          = max(case when rcc.GradeCode = '09' then rcc.CourseLevelCode end),
	hs_09_course_type_code           = max(case when rcc.GradeCode = '09' then rcc.CourseTypeCode end),
	-- high school: 10
	hs_10_grade_level                = max(case when rcc.GradeCode = '10' then rcc.GradeCode end),
	hs_10_school_code                = max(case when rcc.GradeCode = '10' then rcc.SchoolCode end),
	hs_10_year_term_code             = max(case when rcc.GradeCode = '10' then rcc.YearTermCode end),
	hs_10_course_id                  = max(case when rcc.GradeCode = '10' then rcc.CourseCode end),
	hs_10_course_title               = max(case when rcc.GradeCode = '10' then rcc.CourseTitle end),
	hs_10_course_grade               = max(case when rcc.GradeCode = '10' then rcc.SectionMark end),
	hs_10_course_grade_points        = max(case when rcc.GradeCode = '10' then rcc.MarkPoints end),
	hs_10_course_grade_category      = max(case when rcc.GradeCode = '10' then rcc.MarkCategory end),
	hs_10_course_success_ind         = max(case when rcc.GradeCode = '10' then convert(tinyint, rcc.IsSuccess) end),
	hs_10_course_ag_code             = max(case when rcc.GradeCode = '10' then rcc.CourseAGCode end),
	hs_10_course_level_code          = max(case when rcc.GradeCode = '10' then rcc.CourseLevelCode end),
	hs_10_course_type_code           = max(case when rcc.GradeCode = '10' then rcc.CourseTypeCode end),
	-- high school: 11
	hs_11_grade_level                = max(case when rcc.GradeCode = '11' then rcc.GradeCode end),
	hs_11_school_code                = max(case when rcc.GradeCode = '11' then rcc.SchoolCode end),
	hs_11_year_term_code             = max(case when rcc.GradeCode = '11' then rcc.YearTermCode end),
	hs_11_course_id                  = max(case when rcc.GradeCode = '11' then rcc.CourseCode end),
	hs_11_course_title               = max(case when rcc.GradeCode = '11' then rcc.CourseTitle end),
	hs_11_course_grade               = max(case when rcc.GradeCode = '11' then rcc.SectionMark end),
	hs_11_course_grade_points        = max(case when rcc.GradeCode = '11' then rcc.MarkPoints end),
	hs_11_course_grade_category      = max(case when rcc.GradeCode = '11' then rcc.MarkCategory end),
	hs_11_course_success_ind         = max(case when rcc.GradeCode = '11' then convert(tinyint, rcc.IsSuccess) end),
	hs_11_course_ag_code             = max(case when rcc.GradeCode = '11' then rcc.CourseAGCode end),
	hs_11_course_level_code          = max(case when rcc.GradeCode = '11' then rcc.CourseLevelCode end),
	hs_11_course_type_code           = max(case when rcc.GradeCode = '11' then rcc.CourseTypeCode end),
	-- high school: 12
	hs_12_grade_level                = max(case when rcc.GradeCode = '12' then rcc.GradeCode end),
	hs_12_school_code                = max(case when rcc.GradeCode = '12' then rcc.SchoolCode end),
	hs_12_year_term_code             = max(case when rcc.GradeCode = '12' then rcc.YearTermCode end),
	hs_12_course_id                  = max(case when rcc.GradeCode = '12' then rcc.CourseCode end),
	hs_12_course_title               = max(case when rcc.GradeCode = '12' then rcc.CourseTitle end),
	hs_12_course_grade               = max(case when rcc.GradeCode = '12' then rcc.SectionMark end),
	hs_12_course_grade_points        = max(case when rcc.GradeCode = '12' then rcc.MarkPoints end),
	hs_12_course_grade_category      = max(case when rcc.GradeCode = '12' then rcc.MarkCategory end),
	hs_12_course_success_ind         = max(case when rcc.GradeCode = '12' then convert(tinyint, rcc.IsSuccess) end),
	hs_12_course_ag_code             = max(case when rcc.GradeCode = '12' then rcc.CourseAGCode end),
	hs_12_course_level_code          = max(case when rcc.GradeCode = '12' then rcc.CourseLevelCode end),
	hs_12_course_type_code           = max(case when rcc.GradeCode = '12' then rcc.CourseTypeCode end),
	-- high school: 12
	hs_recency_grade_level           = max(case when rcc.RecencySelector = 1 then rcc.GradeCode end),
	hs_recency_school_code           = max(case when rcc.RecencySelector = 1 then rcc.SchoolCode end),
	hs_recency_year_term_code        = max(case when rcc.RecencySelector = 1 then rcc.YearTermCode end),
	hs_recency_course_id             = max(case when rcc.RecencySelector = 1 then rcc.CourseCode end),
	hs_recency_course_title          = max(case when rcc.RecencySelector = 1 then rcc.CourseTitle end),
	hs_recency_course_grade          = max(case when rcc.RecencySelector = 1 then rcc.SectionMark end),
	hs_recency_course_grade_points   = max(case when rcc.RecencySelector = 1 then rcc.MarkPoints end),
	hs_recency_course_grade_category = max(case when rcc.RecencySelector = 1 then rcc.MarkCategory end),
	hs_recency_course_success_ind    = max(case when rcc.RecencySelector = 1 then convert(tinyint, rcc.IsSuccess) end),
	hs_recency_course_ag_code        = max(case when rcc.RecencySelector = 1 then rcc.CourseAGCode end),
	hs_recency_course_level_code     = max(case when rcc.RecencySelector = 1 then rcc.CourseLevelCode end),
	hs_recency_course_type_code      = max(case when rcc.RecencySelector = 1 then rcc.CourseTypeCode end),
	-- binary variables
	/* Pre Algebra: 09, 10, 11 */
	hs_pre_alg_UP11                  = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_F                = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_Dminus           = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_D                = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_Dplus            = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_Cminus           = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_C                = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_Cplus            = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_Bminus           = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_B                = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_Bplus            = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_Aminus           = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_A                = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	/* Pre Algebra: 09, 10, 11, 12 */
	hs_pre_alg_ANY                   = max(case when rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_F                 = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_Dminus            = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_D                 = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_Dplus             = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_Cminus            = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_C                 = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_Cplus             = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_Bminus            = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_B                 = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_Bplus             = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_Aminus            = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_A                 = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	/* Algebra I: 09, 10, 11 */
	hs_alg_i_UP11                    = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_F                  = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_Dminus             = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_D                  = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_Dplus              = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_Cminus             = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_C                  = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_Cplus              = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_Bminus             = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_B                  = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_Bplus              = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_Aminus             = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_A                  = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	/* Algebra I: 09, 10, 11, 12 */
	hs_alg_i_ANY                     = max(case when rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_F                   = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_Dminus              = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_D                   = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_Dplus               = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_Cminus              = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_C                   = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_Cplus               = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_Bminus              = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_B                   = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_Bplus               = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_Aminus              = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_A                   = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	/* Algebra II: 09, 10, 11 */
	hs_alg_ii_UP11                   = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_F                 = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_Dminus            = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_D                 = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_Dplus             = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_Cminus            = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_C                 = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_Cplus             = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_Bminus            = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_B                 = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_Bplus             = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_Aminus            = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_A                 = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	/* Algebra II: 09, 10, 11, 12 */
	hs_alg_ii_ANY                    = max(case when rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_F                  = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_Dminus             = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_D                  = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_Dplus              = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_Cminus             = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_C                  = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_Cplus              = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_Bminus             = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_B                  = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_Bplus              = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_Aminus             = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_A                  = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	/* Geometry: 09, 10, 11 */
	hs_geo_UP11                      = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_F                    = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_Dminus               = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_D                    = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_Dplus                = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_Cminus               = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_C                    = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_Cplus                = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_Bminus               = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_B                    = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_Bplus                = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_Aminus               = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_A                    = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	/* Geometry: 09, 10, 11, 12 */
	hs_geo_ANY                       = max(case when rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_F                     = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_Dminus                = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_D                     = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_Dplus                 = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_Cminus                = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_C                     = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_Cplus                 = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_Bminus                = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_B                     = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_Bplus                 = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_Aminus                = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_A                     = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	/* Trigonometry: 09, 10, 11 */
	hs_trig_UP11                     = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_F                   = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_Dminus              = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_D                   = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_Dplus               = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_Cminus              = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_C                   = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_Cplus               = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_Bminus              = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_B                   = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_Bplus               = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_Aminus              = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_A                   = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	/* Trigonometry: 09, 10, 11, 12 */
	hs_trig_ANY                      = max(case when rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_F                    = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_Dminus               = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_D                    = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_Dplus                = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_Cminus               = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_C                    = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_Cplus                = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_Bminus               = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_B                    = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_Bplus                = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_Aminus               = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_A                    = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	/* PreCalculus: 09, 10, 11 */
	hs_pre_calc_UP11                 = max(case when rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_F               = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_Dminus          = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_D               = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_Dplus           = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_Cminus          = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_C               = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_Cplus           = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_Bminus          = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_B               = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_Bplus           = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_Aminus          = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_A               = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	/* PreCalculus: 09, 10, 11, 12 */
	hs_pre_calc_ANY                  = max(case when rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_F                = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_Dminus           = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_D                = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_Dplus            = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_Cminus           = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_C                = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_Cplus            = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_Bminus           = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_B                = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_Bplus            = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_Aminus           = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_A                = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode = '2414' then 1 else 0 end),
	/* Calculus: 09, 10, 11 */
	hs_calc_UP11                     = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_F                   = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_Dminus              = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_D                   = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_Dplus               = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_Cminus              = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_C                   = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_Cplus               = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_Bminus              = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_B                   = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_Bplus               = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_Aminus              = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_A                   = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	/* Calculus: 09, 10, 11, 12 */
	hs_calc_ANY                      = max(case when rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_F                    = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_Dminus               = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_D                    = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_Dplus                = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_Cminus               = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_C                    = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_Cplus                = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_Bminus               = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_B                    = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_Bplus                = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_Aminus               = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_A                    = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	/* Calculus AB: 09, 10, 11 */
	hs_calc_ab_UP11                  = max(case when rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_F                = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_Dminus           = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_D                = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_Dplus            = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_Cminus           = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_C                = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_Cplus            = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_Bminus           = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_B                = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_Bplus            = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_Aminus           = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_A                = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	/* Calculus AB: 09, 10, 11, 12 */
	hs_calc_ab_ANY                   = max(case when rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_F                 = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_Dminus            = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_D                 = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_Dplus             = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_Cminus            = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_C                 = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_Cplus             = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_Bminus            = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_B                 = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_Bplus             = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_Aminus            = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_A                 = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode = '2480' then 1 else 0 end),
	/* Calculus BC: 09, 10, 11 */
	hs_calc_bc_UP11                  = max(case when rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_F                = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_Dminus           = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_D                = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_Dplus            = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_Cminus           = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_C                = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_Cplus            = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_Bminus           = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_B                = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_Bplus            = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_Aminus           = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_A                = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	/* Calculus BC: 09, 10, 11, 12 */
	hs_calc_bc_ANY                   = max(case when rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_F                 = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_Dminus            = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_D                 = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_Dplus             = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_Cminus            = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_C                 = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_Cplus             = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_Bminus            = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_B                 = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_Bplus             = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_Aminus            = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_A                 = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode = '2481' then 1 else 0 end),
	/* Statistics: 09, 10, 11 */
	hs_stat_UP11                     = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_F                   = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_Dminus              = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_D                   = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_Dplus               = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_Cminus              = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_C                   = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_Cplus               = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_Bminus              = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_B                   = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_Bplus               = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_Aminus              = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_A                   = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	/* Statistics: 09, 10, 11, 12 */
	hs_stat_ANY                      = max(case when rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_F                    = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_Dminus               = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_D                    = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_Dplus                = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_Cminus               = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_C                    = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_Cplus                = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_Bminus               = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_B                    = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_Bplus                = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_Aminus               = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_A                    = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	/* Statistics AP: 09, 10, 11 */
	hs_stat_ap_UP11                  = max(case when rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_F                = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_Dminus           = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_D                = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_Dplus            = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_Cminus           = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_C                = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_Cplus            = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_Bminus           = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_B                = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_Bplus            = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_Aminus           = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_A                = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	/* Statistics AP: 09, 10, 11, 12 */
	hs_stat_ap_ANY                   = max(case when rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_F                 = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_Dminus            = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_D                 = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_Dplus             = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_Cminus            = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_C                 = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_Cplus             = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_Bminus            = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_B                 = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_Bplus             = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_Aminus            = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_A                 = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode = '2483' then 1 else 0 end),
	-- college: First
	cc_primacy_college_id            = max(case when ccc.FirstSelector = 1 then ccc.CollegeId end),
	cc_primacy_course_level          = max(case when ccc.FirstSelector = 1 then ccc.CourseLevel end),
	cc_primacy_year_term_code        = max(case when ccc.FirstSelector = 1 then ccc.YearTermCode end),
	cc_primacy_course_id             = max(case when ccc.FirstSelector = 1 then ccc.CourseId end),
	cc_primacy_course_title          = max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end),
	cc_primacy_course_grade          = max(case when ccc.FirstSelector = 1 then ccc.SectionMark end),
	cc_primacy_course_grade_points   = max(case when ccc.FirstSelector = 1 then ccc.MarkPoints end),
	cc_primacy_course_grade_category = max(case when ccc.FirstSelector = 1 then ccc.MarkCategory end),
	cc_primacy_course_success_ind    = max(case when ccc.FirstSelector = 1 then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: Y
	cc_00_college_id                 = max(case when ccc.CourseLevel = 'Y' then ccc.CollegeId end),
	cc_00_course_level               = max(case when ccc.CourseLevel = 'Y' then ccc.CourseLevel end),
	cc_00_year_term_code             = max(case when ccc.CourseLevel = 'Y' then ccc.YearTermCode end),
	cc_00_course_id                  = max(case when ccc.CourseLevel = 'Y' then ccc.CourseId end),
	cc_00_course_title               = max(case when ccc.CourseLevel = 'Y' then ccc.CourseTitle end),
	cc_00_course_grade               = max(case when ccc.CourseLevel = 'Y' then ccc.SectionMark end),
	cc_00_course_grade_points        = max(case when ccc.CourseLevel = 'Y' then ccc.MarkPoints end),
	cc_00_course_grade_category      = max(case when ccc.CourseLevel = 'Y' then ccc.MarkCategory end),
	cc_00_course_success_ind         = max(case when ccc.CourseLevel = 'Y' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: A
	cc_01_college_id                 = max(case when ccc.CourseLevel = 'A' then ccc.CollegeId end),
	cc_01_course_level               = max(case when ccc.CourseLevel = 'A' then ccc.CourseLevel end),
	cc_01_year_term_code             = max(case when ccc.CourseLevel = 'A' then ccc.YearTermCode end),
	cc_01_course_id                  = max(case when ccc.CourseLevel = 'A' then ccc.CourseId end),
	cc_01_course_title               = max(case when ccc.CourseLevel = 'A' then ccc.CourseTitle end),
	cc_01_course_grade               = max(case when ccc.CourseLevel = 'A' then ccc.SectionMark end),
	cc_01_course_grade_points        = max(case when ccc.CourseLevel = 'A' then ccc.MarkPoints end),
	cc_01_course_grade_category      = max(case when ccc.CourseLevel = 'A' then ccc.MarkCategory end),
	cc_01_course_success_ind         = max(case when ccc.CourseLevel = 'A' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: B
	cc_02_college_id                 = max(case when ccc.CourseLevel = 'B' then ccc.CollegeId end),
	cc_02_course_level               = max(case when ccc.CourseLevel = 'B' then ccc.CourseLevel end),
	cc_02_year_term_code             = max(case when ccc.CourseLevel = 'B' then ccc.YearTermCode end),
	cc_02_course_id                  = max(case when ccc.CourseLevel = 'B' then ccc.CourseId end),
	cc_02_course_title               = max(case when ccc.CourseLevel = 'B' then ccc.CourseTitle end),
	cc_02_course_grade               = max(case when ccc.CourseLevel = 'B' then ccc.SectionMark end),
	cc_02_course_grade_points        = max(case when ccc.CourseLevel = 'B' then ccc.MarkPoints end),
	cc_02_course_grade_category      = max(case when ccc.CourseLevel = 'B' then ccc.MarkCategory end),
	cc_02_course_success_ind         = max(case when ccc.CourseLevel = 'B' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: C
	cc_03_college_id                 = max(case when ccc.CourseLevel = 'C' then ccc.CollegeId end),
	cc_03_course_level               = max(case when ccc.CourseLevel = 'C' then ccc.CourseLevel end),
	cc_03_year_term_code             = max(case when ccc.CourseLevel = 'C' then ccc.YearTermCode end),
	cc_03_course_id                  = max(case when ccc.CourseLevel = 'C' then ccc.CourseId end),
	cc_03_course_title               = max(case when ccc.CourseLevel = 'C' then ccc.CourseTitle end),
	cc_03_course_grade               = max(case when ccc.CourseLevel = 'C' then ccc.SectionMark end),
	cc_03_course_grade_points        = max(case when ccc.CourseLevel = 'C' then ccc.MarkPoints end),
	cc_03_course_grade_category      = max(case when ccc.CourseLevel = 'C' then ccc.MarkCategory end),
	cc_03_course_success_ind         = max(case when ccc.CourseLevel = 'C' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: D
	cc_04_college_id                 = max(case when ccc.CourseLevel = 'D' then ccc.CollegeId end),
	cc_04_course_level               = max(case when ccc.CourseLevel = 'D' then ccc.CourseLevel end),
	cc_04_year_term_code             = max(case when ccc.CourseLevel = 'D' then ccc.YearTermCode end),
	cc_04_course_id                  = max(case when ccc.CourseLevel = 'D' then ccc.CourseId end),
	cc_04_course_title               = max(case when ccc.CourseLevel = 'D' then ccc.CourseTitle end),
	cc_04_course_grade               = max(case when ccc.CourseLevel = 'D' then ccc.SectionMark end),
	cc_04_course_grade_points        = max(case when ccc.CourseLevel = 'D' then ccc.MarkPoints end),
	cc_04_course_grade_category      = max(case when ccc.CourseLevel = 'D' then ccc.MarkCategory end),
	cc_04_course_success_ind         = max(case when ccc.CourseLevel = 'D' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- College Mathematics Content
	CC_GE_MATH = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH FOR%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH%LIB%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH%ELEM%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH TOPICS%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATHEMATICAL%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATHEMATICS%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%THE IDEAS%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%LIBERAL%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%SURVEY%' and max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) not like '%CALC%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%CONCEPTS%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%IDEAS%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%TEACH%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%LIFE%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BUS%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BRIEF%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%SOC%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BIO%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%SHORT%' then 1
			else 0
		end,
	CC_STATISTICS = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseId    end) like '%STA%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%STA%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO STA%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEM PROB%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEM. PROB%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEM STA%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEMENTARY STA%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEMNTRY STA%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO PROB%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO TO PROB%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO APPLIED STATS%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%HONORS STA%' then 1
			else 0
		end,
	CC_COLL_ALG =
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%COLL%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ADVANCED ALG%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ALGEBRA FOR CALC%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%PRECALC%' then 0
			else 0
		end,
	CC_PRE_CALC = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%PRE%CALC%' then 1
			else 0
		end,
	CC_CALC_I = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%CALC%' then
				case
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%PRE%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%II%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%2%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%3%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%4A%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%DIFF%' then 0
					else 1
				end
			else 0
		end,
	CC_CALC_II = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%CALC%'
				and max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) not like '%PRE%' then
				case
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%II%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%2%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%3%' then 1
					else 0
				end
			else 0
		end,
	CC_BUS_MATH = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BUS%'
				and max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) not like '%FINITE%'
				and max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) not like '%DISCRETE%' then 1
			else 0
		end,
	CC_DIFF_EQ = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%DIFF%' then 1
			else 0
		end,
	CC_TRIG = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%Trig%' then 1
			else 0
		end,
	CC_BUSINESS_CALC = max(case when ccc.FirstSelector = 1 and cp.PathwayName = 'BusinessCalculus' then 1 else 0 end),
	CC_ELEMENTARY    = max(case when ccc.FirstSelector = 1 and cp.PathwayName = 'Teaching'         then 1 else 0 end),
	CC_TRANSFER_MATH_TYPE = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) = 'Y' then
				case
					-- CC_GE_MATH
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH FOR%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH/LIB%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH/ELEM%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH:ELEM%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH TOPICS%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATHEMATICAL%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATHEMATICS%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%THE IDEAS%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%LIBERAL%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%SURVEY%'
						and max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) not like '%CALC%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%CONCEPTS%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%IDEAS%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%TEACH%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%LIFE%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BUS%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BRIEF%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%SOC%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BIO%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%SHORT%' then 0
					-- CC_STATISTICS
					when max(case when ccc.FirstSelector = 1 then ccc.CourseId    end) like '%STA%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%STA%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO STA%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEM PROB%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEM. PROB%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEM STA%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEMENTARY STA%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEMNTRY STA%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO PROB%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO TO PROB%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO APPLIED STATS%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%HONORS STA%' then 1
					-- CC_COLL_ALG
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%COLL%' then 2
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ADVANCED ALG%' then 2
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ALGEBRA FOR CALC%' then 2
					-- CC_PRE_CALC & CC_CALC_I & CC_CALC_II
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%CALC%' then
						case
							when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%PRE%' then 3
							when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%II%' then 5
							when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%2%' then 5
							when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%3%' then 5
							when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%4A%' then 5
							when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%DIFF%' then 7
							else 4
						end
					-- CC_BUS_MATH
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BUS%'
						and max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) not like '%FINITE%'
						and max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) not like '%DISCRETE%' then 1
					-- CC_DIFF_EQ
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%DIFF%' then 7
					-- CC_LINEAR_ALGEBRA not in variables
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%LINEAR%' then 8
					else -9
				end
			else -9
		end,
	IsMmapStats      = null,
	IsCapStats       = null,
	IsMmapGeMath     = null,
	IsCapGeMath      = null,
	IsMmapCalc       = null,
	IsCapCalc        = null,
	IsMmapPreCalc    = null,
	IsCapPreCalc     = null,
	IsMmapTrig       = null,
	IsCapTrig        = null,
	IsMmapColAlg     = null,
	IsCapColAlg      = null,
	IsMmapIntAlg     = null,
	IsCapIntAlg      = null,
	IsMmapElmAlg     = null,
	IsCapElmAlg      = null
INTO
	mmap.CapMmapMathematicsGrade
FROM
	(
		SELECT
			cc.CohortYear,
			cc.TopCode,
			cc.InterSegmentKey,
			OverallCumulativeGradePointAverage = max(case when DepartmentCode = 0 then CumulativeGradePointAverage end),
			SubjectCumulativeGradePointAverage = max(case when DepartmentCode = 18 then CumulativeGradePointAverage end),
			WithoutSubjectOverallCumulativeGradePointAverage = 
				convert(
					decimal(9,3),
					isnull(
						case
							when max(case when DepartmentCode = 0 then CumulativeQualityPoints end) - max(case when DepartmentCode = 18 then CumulativeQualityPoints end) <= 0 then 0
							else max(case when DepartmentCode = 0 then CumulativeQualityPoints end) - max(case when DepartmentCode = 18 then CumulativeQualityPoints end)
						end
						/
						nullif(
							case
								when max(case when DepartmentCode = 0 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 18 then CumulativeCreditAttempted end) <= 0 then 0
								else max(case when DepartmentCode = 0 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 18 then CumulativeCreditAttempted end)
							end,
							0
						)
					,0)
				)
		FROM
			mmap.CapCohort cc
			inner join
			mmap.RetrospectivePerformance rp
				on  cc.InterSegmentKey = rp.InterSegmentKey
		WHERE
			cc.TopCode = '170100'
			and rp.DepartmentCode in (0, 18)
			and IsLast = 1
		GROUP BY
			cc.CohortYear,
			cc.TopCode,
			cc.InterSegmentKey
	) b
	inner join
	mmap.RetrospectiveCourseContent rcc
		on rcc.InterSegmentKey = b.InterSegmentKey
	inner join
	mmap.CollegeCourseContent ccc
		on ccc.InterSegmentKey = b.InterSegmentKey
		and ccc.ToPCode = b.TopCode
	inner join
	dbo.Student s
		on s.InterSegmentKey = b.InterSegmentKey
	left outer join
	mmap.CapPathway cp
		on cp.CollegeId = ccc.CollegeId
		and cp.CourseId = ccc.CourseId
WHERE
	rcc.DepartmentCode = 18
	and rcc.GradeSelector = 1
	and ccc.LevelSelector = 1
	and ccc.CollegeCounter = 1
	and s.IsGrade11SectionInclusive = 1
	and s.IsHighSchoolCollision = 0
GROUP BY
	CohortYear,
	b.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage;

DELETE FROM mmap.CapMmapMathematicsGrade WHERE OverallCumulativeGradePointAverage > 4;
DELETE FROM mmap.CapMmapMathematicsGrade WHERE SubjectCumulativeGradePointAverage > 4;
DELETE FROM mmap.CapMmapMathematicsGrade WHERE WithoutSubjectOverallCumulativeGradePointAverage > 4;

ALTER TABLE
	mmap.CapMmapMathematicsGrade
ADD CONSTRAINT
	PK_CapMmapMathematicsGrade
PRIMARY KEY CLUSTERED
	(
		CohortYear,
		InterSegmentKey
	);

ALTER TABLE
	mmap.CapMmapMathematicsGrade
ADD CONSTRAINT
	UQ_CapMmapMathematicsGrade__InterSegmentKey
UNIQUE
	(
		InterSegmentKey
	);

ALTER TABLE
	mmap.CapMmapMathematicsGrade
ADD
	IsTransferStem bit null;

ALTER TABLE
	mmap.CapMmapMathematicsGrade
ADD
	IsTransferNonStem bit null;

UPDATE
	mmap.CapMmapMathematicsGrade
SET
	IsTransferStem = 
		case
			when CC_COLL_ALG = 1 then 1
			when CC_PRE_CALC = 1 then 1
			when CC_CALC_I = 1 then 1
			when CC_CALC_II = 1 then 1
			when CC_DIFF_EQ = 1 then 1
			when CC_BUS_MATH = 1 then 1
			else 0
		end,
	IsTransferNonStem = 
		case
			when CC_GE_MATH = 1 then 1
			when CC_STATISTICS = 1 then 1
			else 0
		end;

UPDATE
	mmap.CapMmapMathematicsGrade
SET
	/* Calculus */
	IsMmapCalc = 
		case
			when OverallCumulativeGradePointAverage >= 3.6 then 1
			when OverallCumulativeGradePointAverage >= 3.2 and hs_pre_calc_ANY_C = 1 then 1
			else 0
		end,
	IsCapCalc =
		case
			when OverallCumulativeGradePointAverage >= 2.8 then 1
			when OverallCumulativeGradePointAverage >= 2.7 and hs_trig_ANY_C = 1 then 1
			else 0
		end,
	/* College Algebra */
	IsMmapColAlg = 
		case
			when OverallCumulativeGradePointAverage >= 3.2 then 1
			when OverallCumulativeGradePointAverage >= 2.9 and hs_pre_calc_any_c = 1 then 1
			else 0
		end,
	IsCapColAlg =
		case
			when OverallCumulativeGradePointAverage >= 2.9 then 1
			when OverallCumulativeGradePointAverage >= 2.5 and hs_alg_i_ANY_C = 1 then 1
			when OverallCumulativeGradePointAverage >= 2.5 and
				(
					hs_calc_bc_ANY_Cplus     = 1
					or hs_calc_ab_ANY_Cplus  = 1
					or hs_stat_ap_ANY_Cplus  = 1
					or hs_stat_ANY_Cplus     = 1
					or hs_trig_ANY_Cplus     = 1
					or hs_pre_calc_ANY_Cplus = 1
					or hs_alg_ii_ANY_Cplus   = 1
					or hs_geo_ANY_Cplus      = 1
					or hs_alg_i_ANY_Cplus    = 1
					or hs_pre_alg_ANY_Cplus  = 1
				) then 1
			else 0
		end,
	/* General Ed */
	IsMmapGeMath = 
		case
			when OverallCumulativeGradePointAverage >= 3.3 then 1
			else 0
		end,
	IsCapGeMath =
		case
			when OverallCumulativeGradePointAverage >= 3.0 then 1
			when OverallCumulativeGradePointAverage >= 2.4 and hs_alg_i_ANY_C = 1 then 1
			when OverallCumulativeGradePointAverage >= 2.4 and
				(
					hs_calc_bc_ANY_Dminus     = 1
					or hs_calc_ab_ANY_Dminus  = 1
					or hs_stat_ap_ANY_Dminus  = 1
					or hs_stat_ANY_Dminus     = 1
					or hs_trig_ANY_Dminus     = 1
					or hs_pre_calc_ANY_Dminus = 1
					or hs_alg_ii_ANY_Dminus   = 1
					or hs_geo_ANY_Dminus      = 1
					or hs_alg_i_ANY_Dminus    = 1
					or hs_pre_alg_ANY_Dminus  = 1
				) then 1
			else 0
		end,
	/* PreCalculus */
	IsMmapPreCalc = 
		case
			when OverallCumulativeGradePointAverage >= 3.4 then 1
			when OverallCumulativeGradePointAverage >= 2.6 and hs_calc_ANY = 1 then 1
			else 0
		end,
	IsCapPreCalc =
		case
			when OverallCumulativeGradePointAverage >= 2.6 then 1
			when OverallCumulativeGradePointAverage >= 2.4 and hs_calc_ANY = 1 then 1
			else 0
		end,
	/* Statistics */
	IsMmapStats = 
		case
			when OverallCumulativeGradePointAverage >= 3.0 then 1
			when OverallCumulativeGradePointAverage >= 2.3 and hs_pre_alg_ANY_C = 1 then 1
			else 0
		end,
	IsCapStats =
		case
			when OverallCumulativeGradePointAverage >= 2.3 then 1
			else 0
		end,
	/* Trigonometry */
	IsMmapTrig = 
		case
			when OverallCumulativeGradePointAverage >= 3.4 then 1
			when OverallCumulativeGradePointAverage >= 3.0 and hs_pre_calc_ANY_Cplus = 1 then 1
			when OverallCumulativeGradePointAverage >= 3.0 and hs_alg_ii_ANY_B = 1 then 1
			else 0
		end,
	IsCapTrig =
		case
			when OverallCumulativeGradePointAverage >= 3.0 then 1
			when OverallCumulativeGradePointAverage >= 2.6 and (hs_pre_calc_any_c = 1 or hs_alg_ii_ANY_Bminus = 1) then 1
			else 0
		end,
	/* Intermediate Algebra */
	IsMmapIntAlg = 
		case
			when OverallCumulativeGradePointAverage >= 2.8 then 1
			else 0
		end,
	IsCapIntAlg = 
		case
			when OverallCumulativeGradePointAverage >= 2.35 then 1
			else 0
		end,
	/* Elementary Algebra */
	IsMmapElmAlg = 
		case
			when OverallCumulativeGradePointAverage >= 2.4 then 1
			else 0
		end,
	IsCapElmAlg =
		case
			when OverallCumulativeGradePointAverage >= 2.25 then 1
			else 0
		end;

GO

ALTER TABLE
	mmap.CapMmapMathematicsGrade
ADD
	IpedsRace char(1);

ALTER TABLE
	mmap.CapMmapMathematicsGrade
ADD
	Gender char(1);

ALTER TABLE
	mmap.CapMmapMathematicsGrade
ADD
	IsDsps char(1);

ALTER TABLE
	mmap.CapMmapMathematicsGrade
ADD
	IsEops char(1);

GO

OPEN SYMMETRIC KEY
  SecPii
DECRYPTION BY CERTIFICATE
  SecPii;

UPDATE
	t
SET
	t.IpedsRace = d.IpedsRace,
	t.Gender    = d.Gender
FROM
	mmap.CapMmapMathematicsGrade t
	inner join
	(
		SELECT
			id.InterSegmentKey,
			IpedsRace = convert(char(1), DecryptByKey(st.ipeds_race_enc)),
			Gender    = convert(char(1), DecryptByKey(st.gender_enc))
		FROM
			comis.stterm st
			inner join
			comis.studntid id
				on id.college_id = st.college_id
				and id.student_id = st.student_id
			inner join
			mmap.CapMmapMathematicsGrade a
				on a.InterSegmentKey = id.InterSegmentKey
		WHERE
			st.term_id = (
				SELECT
					min(st1.term_id)
				FROM
					comis.stterm st1
				WHERE
					st1.college_id = st.college_id
					and st1.student_id = st.student_id
			)
	) d
		on d.InterSegmentKey = t.InterSegmentKey;

CLOSE SYMMETRIC KEY SecPii;

UPDATE
	t
SET
	t.IsDsps = isnull(d.IsDsps, 0),
	t.IsEops = isnull(e.IsEops, 0)
FROM
	mmap.CapMmapMathematicsGrade t
	left outer join
	(
		SELECT DISTINCT
			id.InterSegmentKey,
			IsDsps = 1
		FROM
			comis.sddsps sd
			inner join
			comis.studntid id
				on id.college_id = sd.college_id
				and id.student_id = sd.student_id
			inner join
			mmap.CapMmapMathematicsGrade a
				on a.InterSegmentKey = id.InterSegmentKey
	) d
		on d.InterSegmentKey = t.InterSegmentKey
	left outer join
	(
		SELECT DISTINCT
			id.InterSegmentKey,
			IsEops = 1
		FROM
			comis.seeops se
			inner join
			comis.studntid id
				on id.college_id = se.college_id
				and id.student_id = se.student_id
			inner join
			mmap.CapMmapMathematicsGrade a
				on a.InterSegmentKey = id.InterSegmentKey
	) e
		on e.InterSegmentKey = t.InterSegmentKey;