IF (object_id('Mmap.CollegeCourseContent') is not null)
	BEGIN
		DROP TABLE Mmap.CollegeCourseContent;
	END;

GO

CREATE TABLE
	Mmap.CollegeCourseContent
	(
		InterSegmentKey             binary(64)   not null,
		TopCode                     char(6)      not null,
		CourseLevel                 char(1)      not null,
		CollegeId                   char(3)      not null,
		YearTermCode                char(5)      not null,
		CourseId                    varchar(12)  not null,
		ControlNumber               varchar(12)  not null,
		CourseTitle                 varchar(68)  not null,
		CourseUnitsAttempted        decimal(4,2) not null,
		SectionMark                 varchar(3)   not null,
		MarkPoints                  decimal(3,2) not null,
		MarkCategory                smallint     not null,
		MarkIsSuccess               bit          not null,
		FirstSelector               smallint     not null,
		LevelSelector               smallint     not null,
		CoRequisiteSupportSelector  smallint         null,
		CoRequisiteTransferSelector smallint         null,
		CollegeCounter              smallint         null,
		DistrictCounter             smallint         null
	);

GO

ALTER TABLE
	mmap.CollegeCourseContent
ADD CONSTRAINT
	PK_CollegeCourseContent
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey,
		TopCode,
		FirstSelector
	);