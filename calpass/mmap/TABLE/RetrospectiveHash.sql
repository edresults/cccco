USE calpass;

GO

IF (object_id('mmap.RetrospectiveHash') is not null)
	BEGIN
		DROP TABLE mmap.RetrospectiveHash;
	END;

GO

CREATE TABLE
	mmap.RetrospectiveHash
	(
		InterSegmentKey binary(64) not null,
		Entity varchar(25) not null,
		Attribute varchar(25) not null,
		Value decimal(4,3) not null
	);

GO

ALTER TABLE
	mmap.RetrospectiveHash
ADD CONSTRAINT
	PK_RetrospectiveHash
PRIMARY KEY CLUSTERED
	(
		InterSegmentKey,
		Entity,
		Attribute
	);

GO

CREATE NONCLUSTERED INDEX
	IX_RetrospectiveHash__Entity
ON
	mmap.RetrospectiveHash
	(
		Entity,
		Attribute
	)
INCLUDE
	(
		Value
	);