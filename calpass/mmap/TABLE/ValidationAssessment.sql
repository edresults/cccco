USE calpass;

GO

IF (object_id('mmap.ValidationAssessment') is not null)
	BEGIN
		DROP TABLE mmap.ValidationAssessment;
	END;

GO

CREATE TABLE
	mmap.ValidationAssessment
	(
		CollegeId char(3) not null,
		StudentId varchar(10) not null,
		IdStatusCode char(1) not null,
		InterSegmentKey binary(64) not null,
		Birthdate char(8) not null,
		Gender char(1) not null,
		NameFirst varchar(30) not null,
		NameLast varchar(40) not null,
		TermCode char(3) not null,
		PlacementTypeId tinyint not null,
		MMMethodId tinyint not null,
		MMEligibilityId tinyint not null,
		MMBasisId tinyint not null,
		TestSubjectCode varchar(4) not null,
		TestDescription varchar(255) not null,
		TestDate char(8) not null,
		TestScoreDescriptionId tinyint not null,
		TestScore decimal(6,2),
		TestScoreCompensatory decimal(6,2),
		TestPlacementLevelDescription varchar(8000),
		TestPlacementLevelCode char(1),
		TestPlacementMathCourseTransferArray char(9),
		MMPlacementLevelDescription varchar(8000),
		MMPlacementLevelCode char(1),
		MMPlacementMathCourseTransferArray char(9),
		FinalPlacementLevelDescription varchar(8000),
		FinalPlacementLevelCode char(1),
		FinalPlacementMathCourseTransferArray char(9),
		SubmissionFileId int,
		SubmissionFileRecordNumber int
	);

GO

/* primary key */

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	pk_ValidationAssessment
PRIMARY KEY CLUSTERED
	(
		CollegeId,
		StudentId,
		IdStatusCode,
		TestSubjectCode,
		TestDescription,
		TestDate
	);

/* foreign keys */
ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	fk_ValidationAssessment__IdStatusCode
FOREIGN KEY
	(
		IdStatusCode
	)
REFERENCES
	comis.IdStatus
	(
		IdStatusCode
	);

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	fk_ValidationAssessment__Gender
FOREIGN KEY
	(
		Gender
	)
REFERENCES
	comis.Gender
	(
		GenderCode
	);

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	fk_ValidationAssessment__TermCode
FOREIGN KEY
	(
		TermCode
	)
REFERENCES
	comis.Term
	(
		TermCode
	);

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	fk_ValidationAssessment__PlacementTypeId
FOREIGN KEY
	(
		PlacementTypeId
	)
REFERENCES
	mmap.ValidationAssessment_PlacementType
	(
		PlacementTypeId
	);

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	fk_ValidationAssessment__MMMethodId
FOREIGN KEY
	(
		MMMethodId
	)
REFERENCES
	mmap.ValidationAssessment_MMMethod
	(
		MMMethodId
	);

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	fk_ValidationAssessment__MMEligibilityId
FOREIGN KEY
	(
		MMEligibilityId
	)
REFERENCES
	mmap.ValidationAssessment_MMEligibility
	(
		MMEligibilityId
	);

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	fk_ValidationAssessment__MMBasisId
FOREIGN KEY
	(
		MMBasisId
	)
REFERENCES
	mmap.ValidationAssessment_MMBasis
	(
		MMBasisId
	);

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	fk_ValidationAssessment__TestSubjectCode
FOREIGN KEY
	(
		TestSubjectCode
	)
REFERENCES
	mmap.ValidationAssessment_TestSubject
	(
		TestSubjectCode
	);

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	fk_ValidationAssessment__TestScoreDescriptionId
FOREIGN KEY
	(
		TestScoreDescriptionId
	)
REFERENCES
	mmap.ValidationAssessment_TestScoreDescription
	(
		TestScoreDescriptionId
	);

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	fk_ValidationAssessment__TestPlacementLevelCode
FOREIGN KEY
	(
		TestPlacementLevelCode
	)
REFERENCES
	mmap.ValidationAssessment_PlacementLevel
	(
		PlacementLevelCode
	);

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	fk_ValidationAssessment__MMPlacementLevelCode
FOREIGN KEY
	(
		MMPlacementLevelCode
	)
REFERENCES
	mmap.ValidationAssessment_PlacementLevel
	(
		PlacementLevelCode
	);

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	fk_ValidationAssessment__FinalPlacementLevelCode
FOREIGN KEY
	(
		FinalPlacementLevelCode
	)
REFERENCES
	mmap.ValidationAssessment_PlacementLevel
	(
		PlacementLevelCode
	);

/* check constraints */
ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	ck_ValidationAssessment__Birthdate
CHECK
	(
		case
			when isdate(Birthdate) = 0 then 0
			when Birthdate not like replicate('[0-9]', 8) then 0
			when convert(date, Birthdate) not between dateadd(year, -120, convert(date, getdate())) and convert(date, getdate()) then 0
			else 1
		end = 1
	);

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	ck_ValidationAssessment__TestDate
CHECK
	(
		case
			when isdate(TestDate) = 0 then 0
			when TestDate not like replicate('[0-9]', 8) then 0
			when convert(date, TestDate) not between dateadd(year, -120, convert(date, getdate())) and convert(date, getdate()) then 0
			else 1
		end = 1
	);

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	ck_ValidationAssessment__TestPlacementMathCourseTransferArray
CHECK
	(
		TestPlacementMathCourseTransferArray = null
		or
		TestPlacementMathCourseTransferArray like replicate('[NY]', 9)
	);

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	ck_ValidationAssessment__MMPlacementMathCourseTransferArray
CHECK
	(
		MMPlacementMathCourseTransferArray = null
		or
		MMPlacementMathCourseTransferArray like replicate('[NY]', 9)
	);

ALTER TABLE
	mmap.ValidationAssessment
ADD CONSTRAINT
	ck_ValidationAssessment__FinalPlacementMathCourseTransferArray
CHECK
	(
		FinalPlacementMathCourseTransferArray = null
		or
		FinalPlacementMathCourseTransferArray like replicate('[NY]', 9)
	);

/* indexes */
CREATE INDEX
	ix_ValidationAssessment__SubmissionFileId
ON
	mmap.ValidationAssessment
	(
		SubmissionFileId
	);