USE calpass;

GO

IF (object_id('mmap.ValidationAssessment_TestScoreDescription') is not null)
	BEGIN
		DROP TABLE mmap.ValidationAssessment_TestScoreDescription;
	END;

GO

CREATE TABLE
	mmap.ValidationAssessment_TestScoreDescription
	(
		TestScoreDescriptionId tinyint identity(1,1),
		Description varchar(255)
	);

ALTER TABLE
	mmap.ValidationAssessment_TestScoreDescription
ADD CONSTRAINT
	pk_ValidationAssessment_TestScoreDescription
PRIMARY KEY CLUSTERED
	(
		TestScoreDescriptionId
	);

GO

SET IDENTITY_INSERT mmap.ValidationAssessment_TestScoreDescription ON;

INSERT INTO
	mmap.ValidationAssessment_TestScoreDescription
	(
		TestScoreDescriptionId,
		Description
	)
VALUES
	(1,'Scaled Score'),
	(2,'Percent correct'),
	(3,'Number correct'),
	(4,'Other');

SET IDENTITY_INSERT mmap.ValidationAssessment_TestScoreDescription OFF;