USE calpass;

GO

IF (object_id('mmap.ValidationAssessment_MMEligibility') is not null)
	BEGIN
		DROP TABLE mmap.ValidationAssessment_MMEligibility;
	END;

GO

CREATE TABLE
	mmap.ValidationAssessment_MMEligibility
	(
		MMEligibilityId tinyint identity(1,1),
		Description varchar(255)
	);

ALTER TABLE
	mmap.ValidationAssessment_MMEligibility
ADD CONSTRAINT
	pk_ValidationAssessment_MMEligibility
PRIMARY KEY CLUSTERED
	(
		MMEligibilityId
	);

GO

SET IDENTITY_INSERT mmap.ValidationAssessment_MMEligibility ON;

INSERT INTO
	mmap.ValidationAssessment_MMEligibility
	(
		MMEligibilityId,
		Description
	)
VALUES
	(1,'Not eligible (e.g. not part of sample, exempt, etc.)'),
	(2,'Eligible, multiple measurees not applied (no or insufficent data available, other reasons)'),
	(3,'Eligible, multiple measures applied'),
	(4,'Other'),
	(5,'Unknown'),
	(6,'Eligible after previous course-taking in discipline, multiple measures applied');

SET IDENTITY_INSERT mmap.ValidationAssessment_MMEligibility OFF;