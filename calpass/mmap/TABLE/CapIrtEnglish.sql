-- DROP TABLE mmap.CapMmapEnglish;

-- GO

-- SELECT
-- 	CohortYear,
-- 	b.InterSegmentKey,
-- 	OverallCumulativeGradePointAverage,
-- 	SubjectCumulativeGradePointAverage,
-- 	WithoutSubjectOverallCumulativeGradePointAverage,
-- 	-- high school
-- 	Composition_MarkCategory = isnull(Composition_MarkCategory, -99),
-- 	Composition_IsA,
-- 	Composition_IsAminus,
-- 	Composition_IsBplus,
-- 	Composition_IsB,
-- 	Composition_IsBminus,
-- 	Composition_IsCplus,
-- 	Composition_IsC,
-- 	Composition_IsCminus,
-- 	Composition_IsDplus,
-- 	Composition_IsD,
-- 	Composition_IsDminus,
-- 	Composition_IsF,
-- 	DualEnrollment_MarkCategory = isnull(DualEnrollment_MarkCategory, -99),
-- 	DualEnrollment_IsA,
-- 	DualEnrollment_IsAminus,
-- 	DualEnrollment_IsBplus,
-- 	DualEnrollment_IsB,
-- 	DualEnrollment_IsBminus,
-- 	DualEnrollment_IsCplus,
-- 	DualEnrollment_IsC,
-- 	DualEnrollment_IsCminus,
-- 	DualEnrollment_IsDplus,
-- 	DualEnrollment_IsD,
-- 	DualEnrollment_IsDminus,
-- 	DualEnrollment_IsF,
-- 	English09_MarkCategory = isnull(English09_MarkCategory, -99),
-- 	English09_IsA,
-- 	English09_IsAminus,
-- 	English09_IsBplus,
-- 	English09_IsB,
-- 	English09_IsBminus,
-- 	English09_IsCplus,
-- 	English09_IsC,
-- 	English09_IsCminus,
-- 	English09_IsDplus,
-- 	English09_IsD,
-- 	English09_IsDminus,
-- 	English09_IsF,
-- 	English10_MarkCategory = isnull(English10_MarkCategory, -99),
-- 	English10_IsA,
-- 	English10_IsAminus,
-- 	English10_IsBplus,
-- 	English10_IsB,
-- 	English10_IsBminus,
-- 	English10_IsCplus,
-- 	English10_IsC,
-- 	English10_IsCminus,
-- 	English10_IsDplus,
-- 	English10_IsD,
-- 	English10_IsDminus,
-- 	English10_IsF,
-- 	English11_MarkCategory = isnull(English11_MarkCategory, -99),
-- 	English11_IsA,
-- 	English11_IsAminus,
-- 	English11_IsBplus,
-- 	English11_IsB,
-- 	English11_IsBminus,
-- 	English11_IsCplus,
-- 	English11_IsC,
-- 	English11_IsCminus,
-- 	English11_IsDplus,
-- 	English11_IsD,
-- 	English11_IsDminus,
-- 	English11_IsF,
-- 	English12_MarkCategory = isnull(English12_MarkCategory, -99),
-- 	English12_IsA,
-- 	English12_IsAminus,
-- 	English12_IsBplus,
-- 	English12_IsB,
-- 	English12_IsBminus,
-- 	English12_IsCplus,
-- 	English12_IsC,
-- 	English12_IsCminus,
-- 	English12_IsDplus,
-- 	English12_IsD,
-- 	English12_IsDminus,
-- 	English12_IsF,
-- 	EnglishAP_MarkCategory = isnull(EnglishAP_MarkCategory, -99),
-- 	EnglishAP_IsA,
-- 	EnglishAP_IsAminus,
-- 	EnglishAP_IsBplus,
-- 	EnglishAP_IsB,
-- 	EnglishAP_IsBminus,
-- 	EnglishAP_IsCplus,
-- 	EnglishAP_IsC,
-- 	EnglishAP_IsCminus,
-- 	EnglishAP_IsDplus,
-- 	EnglishAP_IsD,
-- 	EnglishAP_IsDminus,
-- 	EnglishAP_IsF,
-- 	Literature_MarkCategory = isnull(Literature_MarkCategory, -99),
-- 	Literature_IsA,
-- 	Literature_IsAminus,
-- 	Literature_IsBplus,
-- 	Literature_IsB,
-- 	Literature_IsBminus,
-- 	Literature_IsCplus,
-- 	Literature_IsC,
-- 	Literature_IsCminus,
-- 	Literature_IsDplus,
-- 	Literature_IsD,
-- 	Literature_IsDminus,
-- 	Literature_IsF,
-- 	Remedial_MarkCategory = isnull(Remedial_MarkCategory, -99),
-- 	Remedial_IsA,
-- 	Remedial_IsAminus,
-- 	Remedial_IsBplus,
-- 	Remedial_IsB,
-- 	Remedial_IsBminus,
-- 	Remedial_IsCplus,
-- 	Remedial_IsC,
-- 	Remedial_IsCminus,
-- 	Remedial_IsDplus,
-- 	Remedial_IsD,
-- 	Remedial_IsDminus,
-- 	Remedial_IsF,
-- 	Rhetoric_MarkCategory = isnull(Rhetoric_MarkCategory, -99),
-- 	Rhetoric_IsA,
-- 	Rhetoric_IsAminus,
-- 	Rhetoric_IsBplus,
-- 	Rhetoric_IsB,
-- 	Rhetoric_IsBminus,
-- 	Rhetoric_IsCplus,
-- 	Rhetoric_IsC,
-- 	Rhetoric_IsCminus,
-- 	Rhetoric_IsDplus,
-- 	Rhetoric_IsD,
-- 	Rhetoric_IsDminus,
-- 	Rhetoric_IsF,
-- 	-- college
-- 	cc_00_course_grade_category = isnull(case when CourseLevel = 'Y' then ccc.MarkCategory end, -99),
-- 	cc_01_course_grade_category = isnull(case when CourseLevel = 'A' then ccc.MarkCategory end, -99),
-- 	cc_02_course_grade_category = isnull(case when CourseLevel = 'B' then ccc.MarkCategory end, -99),
-- 	cc_03_course_grade_category = isnull(case when CourseLevel = 'C' then ccc.MarkCategory end, -99),
-- 	cc_04_course_grade_category = isnull(case when CourseLevel = 'D' then ccc.MarkCategory end, -99)
-- INTO
-- 	mmap.CapMmapEnglish
-- FROM
-- 	(
-- 		SELECT
-- 			cc.CohortYear,
-- 			cc.TopCode,
-- 			cc.InterSegmentKey,
-- 			OverallCumulativeGradePointAverage = max(case when DepartmentCode = 0 then CumulativeGradePointAverage end),
-- 			SubjectCumulativeGradePointAverage = max(case when DepartmentCode = 14 then CumulativeGradePointAverage end),
-- 			WithoutSubjectOverallCumulativeGradePointAverage = 
-- 				convert(
-- 					decimal(9,3),
-- 					isnull(
-- 						case
-- 							when max(case when DepartmentCode = 0 then CumulativeQualityPoints end) - max(case when DepartmentCode = 14 then CumulativeQualityPoints end) <= 0 then 0
-- 							else max(case when DepartmentCode = 0 then CumulativeQualityPoints end) - max(case when DepartmentCode = 14 then CumulativeQualityPoints end)
-- 						end
-- 						/
-- 						nullif(
-- 							case
-- 								when max(case when DepartmentCode = 0 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 14 then CumulativeCreditAttempted end) <= 0 then 0
-- 								else max(case when DepartmentCode = 0 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 14 then CumulativeCreditAttempted end)
-- 							end,
-- 							0
-- 						)
-- 					,0)
-- 				)
-- 		FROM
-- 			mmap.CapCohort cc
-- 			inner join
-- 			mmap.RetrospectivePerformance rp
-- 				on  cc.InterSegmentKey = rp.InterSegmentKey
-- 		WHERE
-- 			cc.TopCode = '150100'
-- 			and rp.DepartmentCode in (0, 14)
-- 			and rp.GradeCode = '12'
-- 			and rp.IsOverall = 1
-- 		GROUP BY
-- 			cc.CohortYear,
-- 			cc.TopCode,
-- 			cc.InterSegmentKey
-- 	) b
-- 	inner join
-- 	mmap.RetrospectiveEnglish re
-- 		on re.InterSegmentKey = b.InterSegmentKey
-- 	inner join
-- 	mmap.CollegeCourseContent ccc
-- 		on ccc.InterSegmentKey = b.InterSegmentKey
-- 		and ccc.ToPCode = b.TopCode
-- WHERE
-- 	ccc.FirstSelector = 1;

-- DELETE FROM mmap.CapMmapEnglish WHERE OverallCumulativeGradePointAverage > 4;
-- DELETE FROM mmap.CapMmapEnglish WHERE SubjectCumulativeGradePointAverage > 4;
-- DELETE FROM mmap.CapMmapEnglish WHERE WithoutSubjectOverallCumulativeGradePointAverage > 4;

-- ALTER TABLE
-- 	mmap.CapMmapEnglish
-- ADD CONSTRAINT
-- 	PK_CapMmapEnglish
-- PRIMARY KEY CLUSTERED
-- 	(
-- 		InterSegmentkey
-- 	);

-- GO

TRUNCATE TABLE mmap.CapIrtEnglish;

GO

INSERT
	mmap.CapIrtEnglish
SELECT
	CohortYear,
	b.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage,
	-- high school
	Remedial_1_MarkCategory     = isnull(max(case when rcc.ContentCode = 'Remedial'    and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	Remedial_2_MarkCategory     = isnull(max(case when rcc.ContentCode = 'Remedial'    and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	English09_1_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English09'   and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	English09_2_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English09'   and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	English10_1_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English10'   and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	English10_2_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English10'   and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	English11_1_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English11'   and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	English11_2_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English11'   and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	English12_1_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English12'   and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	English12_2_MarkCategory    = isnull(max(case when rcc.ContentCode = 'English12'   and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	Expository_1_MarkCategory   = isnull(max(case when rcc.ContentCode = 'Composition' and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	Expository_2_MarkCategory   = isnull(max(case when rcc.ContentCode = 'Composition' and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	Literature_1_MarkCateogory  = isnull(max(case when rcc.ContentCode = 'Literature'  and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	Literature_2_MarkCateogory  = isnull(max(case when rcc.ContentCode = 'Literature'  and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	LanguageAP_1_MarkCateogory  = isnull(max(case when rcc.ContentCode = 'EnglishAP'   and rcc.ContentSelector = 2 and rcc.CourseCode = '2170' then rcc.MarkCategory end), -99),
	LanguageAP_2_MarkCateogory  = isnull(max(case when rcc.ContentCode = 'EnglishAP'   and rcc.ContentSelector = 1 and rcc.CourseCode = '2170' then rcc.MarkCategory end), -99),
	LiteratureAP_1_MarkCategory = isnull(max(case when rcc.ContentCode = 'EnglishAP'   and rcc.ContentSelector = 2 and rcc.CourseCode = '2171' then rcc.MarkCategory end), -99),
	LiteratureAP_2_MarkCategory = isnull(max(case when rcc.ContentCode = 'EnglishAP'   and rcc.ContentSelector = 1 and rcc.CourseCode = '2171' then rcc.MarkCategory end), -99),
	Rhetoric_1_MarkCategory     = isnull(max(case when rcc.ContentCode = 'Rhetoric'    and rcc.ContentSelector = 2 then rcc.MarkCategory end), -99),
	Rhetoric_2_MarkCategory     = isnull(max(case when rcc.ContentCode = 'Rhetoric'    and rcc.ContentSelector = 1 then rcc.MarkCategory end), -99),
	-- college
	cc_00_course_grade_category = isnull(max(case when CourseLevel = 'Y' then ccc.MarkCategory end), -99),
	cc_01_course_grade_category = isnull(max(case when CourseLevel = 'A' then ccc.MarkCategory end), -99),
	cc_02_course_grade_category = isnull(max(case when CourseLevel = 'B' then ccc.MarkCategory end), -99),
	cc_03_course_grade_category = isnull(max(case when CourseLevel = 'C' then ccc.MarkCategory end), -99),
	cc_04_course_grade_category = isnull(max(case when CourseLevel = 'D' then ccc.MarkCategory end), -99)
FROM
	(
		SELECT
			cc.CohortYear,
			cc.TopCode,
			cc.InterSegmentKey,
			OverallCumulativeGradePointAverage = max(case when DepartmentCode = 0 then CumulativeGradePointAverage end),
			SubjectCumulativeGradePointAverage = max(case when DepartmentCode = 14 then CumulativeGradePointAverage end),
			WithoutSubjectOverallCumulativeGradePointAverage = 
				convert(
					decimal(9,3),
					isnull(
						case
							when max(case when DepartmentCode = 0 then CumulativeQualityPoints end) - max(case when DepartmentCode = 14 then CumulativeQualityPoints end) <= 0 then 0
							else max(case when DepartmentCode = 0 then CumulativeQualityPoints end) - max(case when DepartmentCode = 14 then CumulativeQualityPoints end)
						end
						/
						nullif(
							case
								when max(case when DepartmentCode = 0 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 14 then CumulativeCreditAttempted end) <= 0 then 0
								else max(case when DepartmentCode = 0 then CumulativeCreditAttempted end) - max(case when DepartmentCode = 14 then CumulativeCreditAttempted end)
							end,
							0
						)
					,0)
				)
		FROM
			mmap.CapCohort cc
			inner join
			mmap.RetrospectivePerformance rp
				on  cc.InterSegmentKey = rp.InterSegmentKey
		WHERE
			cc.TopCode = '150100'
			and rp.DepartmentCode in (0, 14)
			and rp.IsLast = 1
		GROUP BY
			cc.CohortYear,
			cc.TopCode,
			cc.InterSegmentKey
	) b
	inner join
	mmap.RetrospectiveCourseContent rcc
		on rcc.InterSegmentKey = b.InterSegmentKey
	inner join
	mmap.CollegeCourseContent ccc
		on ccc.InterSegmentKey = b.InterSegmentKey
		and ccc.ToPCode = b.TopCode
	inner join
	dbo.Student s
		on s.InterSegmentKey = b.InterSegmentKey
WHERE
	rcc.DepartmentCode = 14
	and rcc.ContentSelector in (1,2)
	and ccc.LevelSelector = 1
	and ccc.CollegeCounter = 1
	and s.IsGrade11SectionInclusive = 1
	and s.IsHighSchoolCollision = 0
GROUP BY
	CohortYear,
	b.InterSegmentKey,
	OverallCumulativeGradePointAverage,
	SubjectCumulativeGradePointAverage,
	WithoutSubjectOverallCumulativeGradePointAverage;

DELETE FROM mmap.CapIrtEnglish WHERE OverallCumulativeGradePointAverage > 4;
DELETE FROM mmap.CapIrtEnglish WHERE SubjectCumulativeGradePointAverage > 4;
DELETE FROM mmap.CapIrtEnglish WHERE WithoutSubjectOverallCumulativeGradePointAverage > 4;

ALTER TABLE
	mmap.CapIrtEnglish
ADD CONSTRAINT
	PK_CapIrtEnglish
PRIMARY KEY CLUSTERED
	(
		InterSegmentkey
	);