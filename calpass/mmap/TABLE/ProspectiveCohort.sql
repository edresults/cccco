USE calpass;

GO

IF (object_id('mmap.ProspectiveCohort') is not null)
	BEGIN
		DROP TABLE mmap.ProspectiveCohort;
	END;

GO

CREATE TABLE
	mmap.ProspectiveCohort
	(
		college_id char(3) not null,
		student_id varchar(10) not null,
		id_status char(1) not null,
		birthdate char(8) not null,
		gender char(1) not null,
		high_school char(6) not null,
		name_first varchar(30) not null,
		name_last varchar(40) not null,
		derkey1 char(15),
		match_type varchar(255),
		engl_cb21 char(1),
		read_cb21 char(1),
		esl_cb21 char(1),
		math_cb21 char(1),
		math_geo char(1),
		math_alg char(1),
		math_ge char(1),
		math_stat char(1),
		math_pre_calc char(1),
		math_trig char(1),
		math_col_alg char(1),
		math_calc_i char(1),
		grade_level char(2),
		gpa_cum decimal(4,3),
		ap_any bit,
		english decimal(2,1),
		english_ap decimal(2,1),
		pre_alg decimal(2,1),
		pre_alg_ap decimal(2,1),
		alg_i decimal(2,1),
		alg_i_ap decimal(2,1),
		alg_ii decimal(2,1),
		alg_ii_ap decimal(2,1),
		geo decimal(2,1),
		geo_ap decimal(2,1),
		trig decimal(2,1),
		trig_ap decimal(2,1),
		pre_calc decimal(2,1),
		pre_calc_ap decimal(2,1),
		calc decimal(2,1),
		calc_ap decimal(2,1),
		stat decimal(2,1),
		stat_ap decimal(2,1),
		engl_eap_ind int,
		engl_scaled_score int,
		math_subject char(1),
		math_eap_ind int,
		math_scaled_score int,
		esl_ind bit,
		SubmissionFileId int not null,
		SubmissionFileRecordNumber int not null,
		IsEscrow bit not null,
		IsTreatmentRoadmmap bit
	);

ALTER TABLE
	mmap.ProspectiveCohort
ADD CONSTRAINT
	pk_ProspectiveCohort__college_id__student_id__id_status__SubmissionFileId
PRIMARY KEY CLUSTERED
	(
		college_id,
		student_id,
		id_status,
		SubmissionFileId
	);

ALTER TABLE
	mmap.ProspectiveCohort
ADD CONSTRAINT
	fk_ProspectiveCohort__college_id
FOREIGN KEY
	(
		college_id
	)
REFERENCES
	comis.College
	(
		CollegeCode
	);

ALTER TABLE
	mmap.ProspectiveCohort
ADD CONSTRAINT
	fk_ProspectiveCohort__gender
FOREIGN KEY
	(
		gender
	)
REFERENCES
	comis.Gender
	(
		GenderCode
	);

ALTER TABLE
	mmap.ProspectiveCohort
ADD CONSTRAINT
	fk_ProspectiveCohort__id_status
FOREIGN KEY
	(
		id_status
	)
REFERENCES
	comis.IdStatus
	(
		IdStatusCode
	);

CREATE INDEX
	ix_ProspectiveCohort__derkey1__college_id
ON
	mmap.ProspectiveCohort
	(
		derkey1,
		college_id
	);

CREATE INDEX
	ix_ProspectiveCohort__SubmissionFileId__SubmissionFileRecordNumber
ON
	mmap.ProspectiveCohort
	(
		SubmissionFileId,
		SubmissionFileRecordNumber
	);

CREATE INDEX
	ix_ProspectiveCohort__IsEscrow
ON
	mmap.ProspectiveCohort
	(
		IsEscrow
	);

ALTER TABLE
	mmap.ProspectiveCohort
ADD CONSTRAINT
	DF_ProspectiveCohort__IsTreatmentRoadmmap
DEFAULT
	(
		1
	)
FOR
	IsTreatmentRoadmmap;