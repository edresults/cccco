CREATE TABLE
    mmap.Links
    (
        Id  integer    not null identity,
        ISK binary(64) not null,
        CONSTRAINT PK_Links PRIMARY KEY CLUSTERED ( Id ),
        CONSTRAINT PK_Links_ISK UNIQUE ( ISK )
    );

GO

INSERT INTO
    mmap.Links
    (
        ISK
    )
SELECT
    InterSegmentKey
FROM
    mmap.RetroEngl
UNION
SELECT
    InterSegmentKey
FROM
    mmap.RetroMath
UNION
SELECT
    InterSegmentKey
FROM
    mmap.RetroEsl;