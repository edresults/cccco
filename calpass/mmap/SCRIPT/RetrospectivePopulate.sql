SET NOCOUNT ON;

DECLARE
    -- time
    @Time      datetime      = 0,
    @Seconds   integer       = 0,
    -- error
    @Severity  tinyint       = 0,
    @State     tinyint       = 1,
    @Error     varchar(2048) = '',
    -- loop
    @Counter   integer       = 0,
    @Increment integer       = 100000,
    @Cycles    integer       = 0;

TRUNCATE TABLE mmap.RetrospectivePerformance;
TRUNCATE TABLE mmap.RetrospectiveCourseContent;

DROP TABLE IF EXISTS #InterSegment;

CREATE TABLE #InterSegment (Iterator int identity(0,1) primary key clustered, InterSegmentKey binary(64));

DROP TABLE IF EXISTS #InterSegmentCohort;

CREATE TABLE #InterSegmentCohort (InterSegmentKey binary(64) primary key clustered);

INSERT
    #InterSegment
    (
        InterSegmentKey
    )
SELECT
    InterSegmentKey
FROM
    dbo.Student
ORDER BY
    InterSegmentKey;

SELECT
    @Cycles = convert(varchar, ceiling(convert(decimal, count(*)) / @Increment))
FROM
    #InterSegment;

WHILE (@Counter < @Cycles)
BEGIN

    SET @Error = convert(varchar, @Counter) + ' of ' + convert(varchar, @Cycles - 1);

    RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

    -- reset time
    SET @Time = getdate();

    TRUNCATE TABLE #InterSegmentCohort;

    INSERT
        #InterSegmentCohort
        (
            InterSegmentKey
        )
    SELECT
        InterSegmentKey
    FROM
        #InterSegment i
    WHERE
        Iterator >= @Increment * @Counter
        and Iterator < @Increment * (@Counter + 1);

    INSERT INTO
        mmap.RetrospectiveCourseContent
        (
            InterSegmentKey,
            DepartmentCode,
            GradeCode,
            SchoolCode,
            YearTermCode,
            TermCode,
            ContentCode,
            ContentRank,
            CourseCode,
            CourseUnits,
            CourseTitle,
            CourseAGCode,
            CourseLevelCode,
            CourseTypeCode,
            SectionMark,
            MarkPoints,
            MarkCategory,
            IsSuccess,
            IsPromoted,
            ContentSelector,
            GradeSelector,
            RecencySelector
        )
    SELECT
        InterSegmentKey,
        DepartmentCode,
        GradeCode,
        SchoolCode,
        YearTermCode,
        TermCode,
        ContentCode,
        ContentRank,
        CourseCode,
        CourseUnits,
        CourseTitle,
        CourseAGCode,
        CourseLevelCode,
        CourseTypeCode,
        SectionMark,
        MarkPoints,
        MarkCategory,
        IsSuccess,
        IsPromoted,
        ContentSelector,
        GradeSelector,
        RecencySelector
    FROM
        #InterSegmentCohort
        cross apply
        mmap.RetrospectiveCourseContentGet(InterSegmentKey);

    INSERT INTO
        mmap.RetrospectivePerformance
        (
            InterSegmentKey,
            DepartmentCode,
            GradeCode,
            QualityPoints,
            CreditAttempted,
            GradePointAverage,
            GradePointAverageSans,
            CumulativeQualityPoints,
            CumulativeCreditAttempted,
            CumulativeGradePointAverage,
            CumulativeGradePointAverageSans,
            FirstYearTermCode,
            LastYearTermCode,
            IsFirst,
            IsLast
        )
    SELECT
        InterSegmentKey,
        DepartmentCode,
        GradeCode,
        QualityPoints,
        CreditAttempted,
        GradePointAverage,
        GradePointAverageSans,
        CumulativeQualityPoints,
        CumulativeCreditAttempted,
        CumulativeGradePointAverage,
        CumulativeGradePointAverageSans,
        FirstYearTermCode,
        LastYearTermCode,
        IsFirst,
        IsLast
    FROM
        #InterSegmentCohort
        cross apply
        Mmap.RetrospectivePerformanceGet(InterSegmentKey);

    -- get time elapsed
    SET @Seconds = datediff(second, @Time, getdate());
    -- set msg
    SET @Error = 'INSERT: ' + 
        convert(nvarchar, @Seconds / 86400) + ':' +
        convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
    -- output to user
    RAISERROR(@Error, 0, 1) WITH NOWAIT;

    SET @Counter += 1;
END;