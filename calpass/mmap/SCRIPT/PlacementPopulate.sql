IF (object_id('tempdb..#Student') is not null)
	BEGIN
		DROP TABLE #Student;
	END;

GO

CREATE TABLE
	#Student
	(
		Iterator        int identity(0,1) primary key,
		InterSegmentKey binary(64) not null
	);

SET NOCOUNT ON;
SET XACT_ABORT ON;

INSERT
	#Student
	(
		InterSegmentKey
	)
SELECT
	InterSegmentKey
FROM
	dbo.Student
WHERE
	IsHighSchoolCollision = 0
	and IsHighSchoolSection = 1;

DECLARE
	@Severity tinyint = 0,
	@State tinyint = 1,
	@Error varchar(2048),
	@Counter int = 0,
	@Increment int = 100000,
	@Count int,
	@StudentCohort dbo.InterSegment;

SELECT
	@Count = count(*)
FROM
	#Student;

WHILE (1 = 1)
BEGIN

	SET @Error = convert(varchar, @Counter + 1) + ' of ' + convert(varchar, ceiling(convert(decimal, @count) / @increment));

	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

	DELETE @StudentCohort;

	INSERT INTO
		@StudentCohort
		(
			InterSegmentKey
		)
	SELECT
		InterSegmentKey
	FROM
		#Student
	WHERE
		Iterator >= @Increment * @Counter
		and Iterator < @Increment * (@Counter + 1);

	INSERT
		mmap.Placement
		(
			InterSegmentKey,
			EnglishY,
			EnglishA,
			EnglishB,
			EnglishC,
			PreAlgebra,
			AlgebraI,
			AlgebraII,
			MathGE,
			[Statistics],
			CollegeAlgebra,
			Trigonometry,
			PreCalculus,
			CalculusI,
			ReadingM_UboundY,
			ReadingY_UboundY,
			ReadingA_UboundY,
			ReadingB_UboundY,
			ReadingC_UboundY,
			ReadingM_UboundA,
			ReadingA_UboundA,
			ReadingB_UboundA,
			ReadingC_UboundA,
			ReadingM_UboundB,
			ReadingB_UboundB,
			ReadingC_UboundB,
			EslY_UboundY,
			EslA_UboundY,
			EslB_UboundY,
			EslA_UboundA,
			EslB_UboundA,
			EslC_UboundA,
			EslB_UboundB,
			EslC_UboundB,
			EslD_UboundB,
			IsTreatment
		)
	SELECT
		InterSegmentKey,
		EnglishY,
		EnglishA,
		EnglishB,
		EnglishC,
		PreAlgebra,
		AlgebraI,
		AlgebraII,
		MathGE,
		[Statistics],
		CollegeAlgebra,
		Trigonometry,
		PreCalculus,
		CalculusI,
		ReadingM_UboundY,
		ReadingY_UboundY,
		ReadingA_UboundY,
		ReadingB_UboundY,
		ReadingC_UboundY,
		ReadingM_UboundA,
		ReadingA_UboundA,
		ReadingB_UboundA,
		ReadingC_UboundA,
		ReadingM_UboundB,
		ReadingB_UboundB,
		ReadingC_UboundB,
		EslY_UboundY,
		EslA_UboundY,
		EslB_UboundY,
		EslA_UboundA,
		EslB_UboundA,
		EslC_UboundA,
		EslB_UboundB,
		EslC_UboundB,
		EslD_UboundB,
		IsTreatment
	FROM
		@StudentCohort s
		cross apply
		MMAP.PlacementGet(s.InterSegmentKey) pg

	IF (not exists (SELECT 1 FROM @StudentCohort))
		BEGIN
			BREAK;
		END;

	SET @Counter += 1;
END;