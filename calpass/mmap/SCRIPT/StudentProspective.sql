TRUNCATE TABLE mmap.StudentProspective;

GO

INSERT INTO
	mmap.StudentProspective
	(
		InterSegmentKey,
		English,
/*
		Reading,
		Esl,
*/
		Algebra,
		CollegeAlgebra,
		Stats,
		MathGE,
		Trigonometry,
		PreCalculus,
		Calculus
	)
SELECT
	gpa.InterSegmentKey,
	English = case
			-- 12th grade
			when gpa.CurrentGradeLevelCode = '12' then
				case
					when gpa.CumulativeGradePointAverage >= 2.6 then 'Y'
					when gpa.CumulativeGradePointAverage >= 2.2 and engl.English12 >= 2.0 then 'A'
					when gpa.CumulativeGradePointAverage >= 1.8 and engl.English12 >= 1.0 then 'B'
					when gpa.CumulativeGradePointAverage >= 1.8 and ass.EnglScaledScore >= 288 then 'B'
					when gpa.CumulativeGradePointAverage >= 1.7 then 'C'
					when gpa.CumulativeGradePointAverage >= 1.5 and ass.EnglScaledScore >= 268 then 'C'
				end
			-- 11th grade
			when gpa.CurrentGradeLevelCode = '11' then
				case
					when gpa.CumulativeGradePointAverage >= 2.6 then 'Y'
					when gpa.CumulativeGradePointAverage >= 2.3 then 'A'
					when gpa.CumulativeGradePointAverage >= 2.0 then 'B'
					when gpa.CumulativeGradePointAverage >= 1.4 then 'C'
				end
		end,
/* 	Reading = case
			when pr.CourseLevelMax = 'Y' then
				case
					-- non-direct matriculant
					when gpa.CurrentGradeLevelCode = '12' then
						case
							when gpa.CumulativeGradePointAverage >= 3.1 then 'X' -- test out
							when gpa.CumulativeGradePointAverage >= 2.8 then 'Y'
							when gpa.CumulativeGradePointAverage >= 2.1 and engl.English12 >= 1.7 then 'A'
							when gpa.CumulativeGradePointAverage >= 2.3 then 'B'
							when gpa.CumulativeGradePointAverage >= 1.1 then 'C'
							when ass.EnglScaledScore >= 286 then 'C'
						end								
					-- direct matriculant
					when gpa.CurrentGradeLevelCode = '11' then
						case
							when gpa.CumulativeGradePointAverage >= 2.7 and engl.English11 >= 3.0 then 'X' -- test out
							when gpa.CumulativeGradePointAverage >= 2.7 then 'Y'
							when gpa.CumulativeGradePointAverage >= 2.0 then 'A'
							when gpa.CumulativeGradePointAverage >= 1.6 then 'B'
							when gpa.CumulativeGradePointAverage >= 1.3 then 'C'
						end
				end
			when pr.CourseLevelMax = 'A' then
				case
					-- non-direct matriculant
					when gpa.CurrentGradeLevelCode = '12' then
						case
							when gpa.CumulativeGradePointAverage >= 2.5 then 'X' -- test out
							when gpa.CumulativeGradePointAverage >= 2.0 and engl.English12 >= 2.3 then 'A'
							when gpa.CumulativeGradePointAverage >= 2.0 and ass.EnglScaledScore >= 274 then 'A'
							when gpa.CumulativeGradePointAverage >= 1.7 and engl.English12 >= 1.3 then 'B'
							when ass.EnglScaledScore >= 310 then 'B'
							when gpa.CumulativeGradePointAverage >= 2.1 then 'C'
							when ass.EnglScaledScore >= 234 then 'C'
						end
					-- direct matriculant
					when gpa.CurrentGradeLevelCode = '11' then
						case
							when gpa.CumulativeGradePointAverage >= 2.8 then 'X' -- test out
							when gpa.CumulativeGradePointAverage >= 2.3 then 'A'
							when gpa.CumulativeGradePointAverage >= 1.9 then 'B'
						end
				end
			when pr.CourseLevelMax = 'B' then
				case
					-- non-direct matriculant
					when gpa.CurrentGradeLevelCode = '12' then
						case
							when gpa.CumulativeGradePointAverage >= 2.7 then 'X' -- test out
							when gpa.CumulativeGradePointAverage >= 2.2 and ass.EnglScaledScore >= 278 then 'X'
							when gpa.CumulativeGradePointAverage >= 2.2 then 'B'
							when gpa.CumulativeGradePointAverage >= 1.9 then 'C'
							when ass.EnglScaledScore >= 236 then 'C'
						end								
					-- direct matriculant
					when gpa.CurrentGradeLevelCode = '11' then
						case
							when gpa.CumulativeGradePointAverage >= 2.7 then 'X' -- test out
							when gpa.CumulativeGradePointAverage >= 2.3 then 'B'
							when gpa.CumulativeGradePointAverage >= 1.9 then 'C'
						end
				end
		end,
	ESL = case
			when ecb21.CourseLevelMax = 'Y' then
				case
					-- non-direct matriculant
					when gpa.CurrentGradeLevelCode = '12' then
						case
							when gpa.CumulativeGradePointAverage >= 2.4 then 'Y'
							when gpa.CumulativeGradePointAverage >= 2.0 then 'A'
							when gpa.CumulativeGradePointAverage >= 1.8 then 'B'
						end								
					-- direct matriculant
					when gpa.CurrentGradeLevelCode = '11' then
						case
							when gpa.CumulativeGradePointAverage >= 2.5 then 'Y'
							when gpa.CumulativeGradePointAverage >= 1.5 then 'A'
							when gpa.CumulativeGradePointAverage >= 1.3 then 'B'
						end
				end
			when ecb21.CourseLevelMax = 'A' then
				case
					-- non-direct matriculant
					when gpa.CurrentGradeLevelCode = '12' then
						case
							when gpa.CumulativeGradePointAverage >= 2.6 then 'A'
							when gpa.CumulativeGradePointAverage >= 2.4 then 'B'
							when gpa.CumulativeGradePointAverage >= 1.5 then 'C'
						end
					-- direct matriculant
					when gpa.CurrentGradeLevelCode = '11' then
						case
							when gpa.CumulativeGradePointAverage >= 2.7 then 'A'
							when gpa.CumulativeGradePointAverage >= 2.2 then 'B'
							when engl.English11 >= 2.7 then 'C'
							-- when engl.expository_up11_Cplus = 1 then 'C'
							-- when engl.remedial_up11_Cplus = 1 then 'C'
						end
				end
			when ecb21.CourseLevelMax = 'B' then
				case
					-- non-direct matriculant
					when gpa.CurrentGradeLevelCode = '12' then
						case
							when gpa.CumulativeGradePointAverage >= 2.9 then 'B'
							when ass.EnglScaledScore >= 292 then 'B'
							when gpa.CumulativeGradePointAverage >= 2.5 then 'C'
							when gpa.CumulativeGradePointAverage >= 1.9 and engl.English12 >= 2.0 then 'C'
							when gpa.CumulativeGradePointAverage >= 1.8 then 'D'
						end								
					-- direct matriculant
					when gpa.CurrentGradeLevelCode = '11' then
						case
							when gpa.CumulativeGradePointAverage >= 2.8 then 'B'
							when gpa.CumulativeGradePointAverage >= 2.3 then 'C'
							when gpa.CumulativeGradePointAverage >= 1.8 then 'D'
						end
				end
		end, */
	Algebra = case
			when (
				math.AlgebraI >= 1.7
				or math.Geometry >= 1.7
				or math.AlgebraII >= 1.7
				or math.Stats >= 1.7
				or math.Trigonometry >= 1.7
				or math.PreCalculus >= 1.7
				or math.Calculus >= 1.7
			) then
				case
					when gpa.CurrentGradeLevelCode = '12' then
						case
							when gpa.CumulativeGradePointAverage >= 2.9 then 1
							when gpa.CumulativeGradePointAverage >= 2.5 and ass.MathSubject = 7 and ass.MathScaledScore >= 302 then 1
							when gpa.CumulativeGradePointAverage >= 2.5 and math.PreCalculus >= 2.0 then 1
						end
					when gpa.CurrentGradeLevelCode = '11' then
						case
							when gpa.CumulativeGradePointAverage >= 2.8 then 1
						end
				end
		end,
	CollegeAlgebra = case
			when (
				math.AlgebraII >= 1.7
				or math.Stats >= 1.7
				or math.Trigonometry >= 1.7
				or math.PreCalculus >= 1.7
				or math.Calculus >= 1.7
			) then
				case
					-- 12th grade
					when gpa.CurrentGradeLevelCode = '12' then
						case
							when gpa.CumulativeGradePointAverage >= 3.2 then 1
							when gpa.CumulativeGradePointAverage >= 3.0 and math.PreCalculus >= 2.0 then 1
							when gpa.CumulativeGradePointAverage >= 3.0 and math.Stats >= 2.0 then 1
						end
					-- 11th grade
					when gpa.CurrentGradeLevelCode = '11' then
						case
							when gpa.CumulativeGradePointAverage >= 3.2 then 1
							when gpa.CumulativeGradePointAverage >= 2.9 and math.PreCalculus >= 2.0 then 1
							when gpa.CumulativeGradePointAverage >= 2.9 and math.Stats >= 2.0 then 1
						end
				end
		end,
	Stats = case
			when (
				math.AlgebraI >= 1.7
				or math.Geometry >= 1.7
				or math.AlgebraII >= 1.7
				or math.Stats >= 1.7
				or math.Trigonometry >= 1.7
				or math.PreCalculus >= 1.7
				or math.Calculus >= 1.7
			) then
				case
					when gpa.CurrentGradeLevelCode = '12' then
						case
							when gpa.CumulativeGradePointAverage >= 3.0 then 1
							when gpa.CumulativeGradePointAverage >= 2.6 and math.PreCalculus >= 2.0 then 1
						end
					when gpa.CurrentGradeLevelCode = '11' then
						case
							when gpa.CumulativeGradePointAverage >= 3.0 then 1
							when gpa.CumulativeGradePointAverage >= 2.3 and math.PreCalculus >= 2.0 then 1
						end
				end
		end,
	MathGE = case
			when (
				math.AlgebraI >= 1.7
				or math.Geometry >= 1.7
				or math.AlgebraII >= 1.7
				or math.Stats >= 1.7
				or math.Trigonometry >= 1.7
				or math.PreCalculus >= 1.7
				or math.Calculus >= 1.7
			) then
				case
					when gpa.CurrentGradeLevelCode = '12' then
						case
							when gpa.CumulativeGradePointAverage >= 3.2 then 1
							when gpa.CumulativeGradePointAverage >= 2.9 and Stats >= 2.0 then 1
						end
					when gpa.CurrentGradeLevelCode = '11' then
						case
							when gpa.CumulativeGradePointAverage >= 3.3 then 1
						end
				end
		end,
	Trigonometry = case
			when (
				math.AlgebraII >= 1.7
				or math.Stats >= 1.7
				or math.Trigonometry >= 1.7
				or math.PreCalculus >= 1.7
				or math.Calculus >= 1.7
			) then
				case
					-- 12th grade
					when gpa.CurrentGradeLevelCode = '12' then
						case
							when gpa.CumulativeGradePointAverage >= 3.3 then 1
							when gpa.CumulativeGradePointAverage >= 2.8 and math.PreCalculus >= 2.0 then 1
						end
					-- 11th grade
					when gpa.CurrentGradeLevelCode = '11' then
						case
							when gpa.CumulativeGradePointAverage >= 3.4 then 1
							when gpa.CumulativeGradePointAverage >= 3.0 and math.PreCalculus >= 2.3 then 1
							when gpa.CumulativeGradePointAverage >= 3.0 and math.AlgebraII >= 3.0 then 1
						end
				end
		end,
	PreCalculus = case
			when (
				math.AlgebraII >= 1.7
				or math.Stats >= 1.7
				or math.Trigonometry >= 1.7
				or math.PreCalculus >= 1.7
				or math.Calculus >= 1.7
			) then
				case
					-- 12th grade
					when gpa.CurrentGradeLevelCode = '12' then
						case
							when gpa.CumulativeGradePointAverage >= 3.3 then 1
							when gpa.CumulativeGradePointAverage >= 3.0 and ass.MathScaledScore >= 340 then 1
							when gpa.CumulativeGradePointAverage >= 3.0 and math.Calculus >= 2.0 then 1
						end
					-- 11th grade
					when gpa.CurrentGradeLevelCode = '11' then
						case
							when gpa.CumulativeGradePointAverage >= 3.4 then 1
							when gpa.CumulativeGradePointAverage >= 2.6 and math.Calculus is not null then 1
						end
				end
		end,
	Calculus = case
			when (
				math.Trigonometry >= 1.7
				or math.PreCalculus >= 1.7
				or math.Calculus >= 1.7
			) then
				case
					-- 12th grade
					when gpa.CurrentGradeLevelCode = '12' then
						case
							when gpa.CumulativeGradePointAverage >= 3.5 then 1
							when gpa.CumulativeGradePointAverage >= 3.1 and math.Calculus is not null then 1
						end
					-- 11th grade
					when gpa.CurrentGradeLevelCode = '11' then
						case
							when gpa.CumulativeGradePointAverage >= 3.6 then 1
							when gpa.CumulativeGradePointAverage >= 3.2 and math.PreCalculus >= 2.0 then 1
						end
				end
		end
FROM
	dbo.StudentHighSchoolCgpa gpa
	left outer join
	dbo.StudentHighSchoolMath math
		on math.InterSegmentKey = gpa.InterSegmentKey
	left outer join
	dbo.StudentHighSchoolEngl engl
		on engl.InterSegmentKey = gpa.InterSegmentKey
	left outer join
	dbo.StudentHighSchoolAssessment ass
		on ass.InterSegmentKey = gpa.InterSegmentKey