TRUNCATE TABLE mmap.ValidationSelfReport;

WITH
	sr
	(
		InterSegmentKey,
		CumulativeGpa
	)
AS
	(
		SELECT
			InterSegmentKey = HASHBYTES('SHA2_512',
					upper(convert(char(3), NameFirst)) + 
					upper(convert(char(3), NameLast)) + 
					upper(Gender) + 
					Birthdate
				),
			CumulativeGpa
		FROM
			mmap.SelfReport
		WHERE
			CumulativeGpa is not null
	)
INSERT INTO
	mmap.ValidationSelfReport
	(
		InterSegmentKey,
		CumulativeGpaSelfReport,
		CumulativeGpaTranscript
	)
SELECT
	sr.InterSegmentKey,
	CgpaTranscript = sr.CumulativeGpa,
	CgpaSelfReport = h.CumulativeGradePointAverage
FROM
	sr
	inner join
	dbo.StudentHighSchoolCgpa h
		on h.InterSegmentKey = sr.InterSegmentKey;

SELECT
	Frq = count(*),
	Pct = convert(decimal(5,2), 100.00 * count(*) / Total),
	CumulativeGpaTranscriptCategorical
FROM
	(
		SELECT
			Total = count(*) over(),
			CumulativeGpaTranscriptCategorical = case
					when CumulativeGpaTranscript > 4.0 then '>4.0'
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaTranscript < 1.0 then '<1.0'
				end,
			CumulativeGpaSelfReportCategorical = case
					when CumulativeGpaSelfReport > 4.0 then '>4.0'
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaSelfReport < 1.0 then '<1.0'
				end,
			Within = case when abs(CumulativeGpaTranscript - CumulativeGpaSelfReport) <= 0.3 then 1 else 0 end,
			OrderByTranscript = case
					when CumulativeGpaTranscript > 4.0 then 1
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then 2
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then 3
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then 4
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then 5
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then 6
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then 7
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then 8
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then 9
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then 10
					when CumulativeGpaTranscript < 1.0 then 11
				end,
			OrderBySelfReport = case
					when CumulativeGpaSelfReport > 4.0 then 1
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then 2
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then 3
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then 4
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then 5
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then 6
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then 7
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then 8
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then 9
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then 10
					when CumulativeGpaSelfReport < 1.0 then 11
				end
		FROM
			mmap.ValidationSelfReport
	) a
GROUP BY
	CumulativeGpaTranscriptCategorical,
	Total,
	OrderByTranscript
ORDER BY
	OrderByTranscript;

SELECT
	Frq = count(*),
	Pct = convert(decimal(5,2), 100.00 * count(*) / Total),
	Within,
	CumulativeGpaTranscriptCategorical
FROM
	(
		SELECT
			Total = count(*) over(),
			CumulativeGpaTranscriptCategorical = case
					when CumulativeGpaTranscript > 4.0 then '>4.0'
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaTranscript < 1.0 then '<1.0'
				end,
			CumulativeGpaSelfReportCategorical = case
					when CumulativeGpaSelfReport > 4.0 then '>4.0'
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaSelfReport < 1.0 then '<1.0'
				end,
			Within = case when abs(CumulativeGpaTranscript - CumulativeGpaSelfReport) <= 0.3 then 1 else 0 end,
			OrderByTranscript = case
					when CumulativeGpaTranscript > 4.0 then 1
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then 2
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then 3
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then 4
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then 5
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then 6
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then 7
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then 8
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then 9
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then 10
					when CumulativeGpaTranscript < 1.0 then 11
				end,
			OrderBySelfReport = case
					when CumulativeGpaSelfReport > 4.0 then 1
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then 2
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then 3
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then 4
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then 5
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then 6
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then 7
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then 8
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then 9
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then 10
					when CumulativeGpaSelfReport < 1.0 then 11
				end
		FROM
			mmap.ValidationSelfReport
	) a
GROUP BY
	CumulativeGpaTranscriptCategorical,
	Total,
	OrderByTranscript,
	Within
ORDER BY
	OrderByTranscript;

SELECT
	Frq = count(*),
	Pct = convert(decimal(5,2), 100.00 * count(*) / Total),
	CumulativeGpaSelfReportCategorical
FROM
	(
		SELECT
			Total = count(*) over(),
			CumulativeGpaTranscriptCategorical = case
					when CumulativeGpaTranscript > 4.0 then '>4.0'
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaTranscript < 1.0 then '<1.0'
				end,
			CumulativeGpaSelfReportCategorical = case
					when CumulativeGpaSelfReport > 4.0 then '>4.0'
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaSelfReport < 1.0 then '<1.0'
				end,
			Within = case when abs(CumulativeGpaTranscript - CumulativeGpaSelfReport) <= 0.3 then 1 else 0 end,
			OrderByTranscript = case
					when CumulativeGpaTranscript > 4.0 then 1
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then 2
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then 3
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then 4
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then 5
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then 6
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then 7
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then 8
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then 9
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then 10
					when CumulativeGpaTranscript < 1.0 then 11
				end,
			OrderBySelfReport = case
					when CumulativeGpaSelfReport > 4.0 then 1
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then 2
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then 3
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then 4
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then 5
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then 6
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then 7
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then 8
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then 9
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then 10
					when CumulativeGpaSelfReport < 1.0 then 11
				end
		FROM
			mmap.ValidationSelfReport
	) a
GROUP BY
	CumulativeGpaSelfReportCategorical,
	Total,
	OrderBySelfReport
ORDER BY
	OrderBySelfReport;

SELECT
	Frq = count(*),
	Pct = convert(decimal(5,2), 100.00 * count(*) / Total),
	Within,
	CumulativeGpaSelfReportCategorical
FROM
	(
		SELECT
			Total = count(*) over(),
			CumulativeGpaTranscriptCategorical = case
					when CumulativeGpaTranscript > 4.0 then '>4.0'
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaTranscript < 1.0 then '<1.0'
				end,
			CumulativeGpaSelfReportCategorical = case
					when CumulativeGpaSelfReport > 4.0 then '>4.0'
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaSelfReport < 1.0 then '<1.0'
				end,
			Within = case when abs(CumulativeGpaTranscript - CumulativeGpaSelfReport) <= 0.3 then 1 else 0 end,
			OrderByTranscript = case
					when CumulativeGpaTranscript > 4.0 then 1
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then 2
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then 3
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then 4
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then 5
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then 6
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then 7
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then 8
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then 9
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then 10
					when CumulativeGpaTranscript < 1.0 then 11
				end,
			OrderBySelfReport = case
					when CumulativeGpaSelfReport > 4.0 then 1
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then 2
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then 3
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then 4
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then 5
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then 6
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then 7
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then 8
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then 9
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then 10
					when CumulativeGpaSelfReport < 1.0 then 11
				end
		FROM
			mmap.ValidationSelfReport
	) a
GROUP BY
	CumulativeGpaSelfReportCategorical,
	Total,
	OrderBySelfReport,
	Within
ORDER BY
	OrderBySelfReport;

SELECT
	Frq = count(*),
	AvgCgpaCalc = avg(CumulativeGpaTranscript),
	AvgCgpaSelf = avg(CumulativeGpaSelfReport),
	CumulativeGpaTranscriptCategorical = case
		when CumulativeGpaTranscript > 4.0 then '>4.0'
		when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then '>=3.7 & <= 4.0'
		when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then '>=3.3 & < 3.7'
		when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then '>=3.0 & < 3.3'
		when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then '>=2.7 & < 3.0'
		when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then '>=2.3 & < 2.7'
		when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then '>=2.0 & < 2.3'
		when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then '>=1.7 & < 2.0'
		when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then '>=1.3 & < 1.7'
		when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then '>=1.0 & < 1.3'
		when CumulativeGpaTranscript < 1.0 then '<1.0'
	end
FROM
	mmap.ValidationSelfReport
GROUP BY
	case
		when CumulativeGpaTranscript > 4.0 then '>4.0'
		when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then '>=3.7 & <= 4.0'
		when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then '>=3.3 & < 3.7'
		when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then '>=3.0 & < 3.3'
		when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then '>=2.7 & < 3.0'
		when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then '>=2.3 & < 2.7'
		when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then '>=2.0 & < 2.3'
		when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then '>=1.7 & < 2.0'
		when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then '>=1.3 & < 1.7'
		when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then '>=1.0 & < 1.3'
		when CumulativeGpaTranscript < 1.0 then '<1.0'
	end,
	case
		when CumulativeGpaTranscript > 4.0 then 1
		when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then 2
		when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then 3
		when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then 4
		when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then 5
		when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then 6
		when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then 7
		when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then 8
		when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then 9
		when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then 10
		when CumulativeGpaTranscript < 1.0 then 11
	end
ORDER BY
	case
		when CumulativeGpaTranscript > 4.0 then 1
		when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then 2
		when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then 3
		when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then 4
		when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then 5
		when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then 6
		when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then 7
		when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then 8
		when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then 9
		when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then 10
		when CumulativeGpaTranscript < 1.0 then 11
	end;

SELECT
	frq = count(*),
	pct = convert(decimal(5,2), 100.00 * count(*) / total),
	years = y09 + y10 + y11 + y12
FROM
	(
		SELECT
			s.InterSegmentKey,
			Total = count(*) over(),
			y09 = max(case when k.gradelevel = '09' then 1 else 0 end),
			y10 = max(case when k.gradelevel = '10' then 1 else 0 end),
			y11 = max(case when k.gradelevel = '11' then 1 else 0 end),
			y12 = max(case when k.gradelevel = '12' then 1 else 0 end)
		FROM
			mmap.ValidationSelfReport r
			inner join
			calpads.Student s
				on s.InterSegmentKey = r.InterSegmentKey
			inner join
			k12studentprod k
				on k.derkey1 = s.derkey1
		GROUP BY
			s.InterSegmentKey
	) a
GROUP BY
	total,
	y09 + y10 + y11 + y12
ORDER BY
	3 desc;

DELETE
	r
FROM
	mmap.ValidationSelfReport r
WHERE
	not exists (
		SELECT
			0
		FROM
			calpads.Student s
			inner join
			k12studentprod k
				on k.derkey1 = s.derkey1
		WHERE
			s.InterSegmentKey = r.InterSegmentKey
		GROUP BY
			s.InterSegmentKey
		HAVING
			max(case when k.gradelevel = '09' then 1 else 0 end) + 
			max(case when k.gradelevel = '10' then 1 else 0 end) + 
			max(case when k.gradelevel = '11' then 1 else 0 end) + 
			max(case when k.gradelevel = '12' then 1 else 0 end) = 4
	);

SELECT
	Frq = count(*),
	Pct = convert(decimal(5,2), 100.00 * count(*) / Total),
	CumulativeGpaTranscriptCategorical
FROM
	(
		SELECT
			Total = count(*) over(),
			CumulativeGpaTranscriptCategorical = case
					when CumulativeGpaTranscript > 4.0 then '>4.0'
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaTranscript < 1.0 then '<1.0'
				end,
			CumulativeGpaSelfReportCategorical = case
					when CumulativeGpaSelfReport > 4.0 then '>4.0'
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaSelfReport < 1.0 then '<1.0'
				end,
			Within = case when abs(CumulativeGpaTranscript - CumulativeGpaSelfReport) <= 0.3 then 1 else 0 end,
			OrderByTranscript = case
					when CumulativeGpaTranscript > 4.0 then 1
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then 2
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then 3
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then 4
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then 5
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then 6
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then 7
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then 8
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then 9
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then 10
					when CumulativeGpaTranscript < 1.0 then 11
				end,
			OrderBySelfReport = case
					when CumulativeGpaSelfReport > 4.0 then 1
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then 2
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then 3
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then 4
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then 5
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then 6
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then 7
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then 8
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then 9
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then 10
					when CumulativeGpaSelfReport < 1.0 then 11
				end
		FROM
			mmap.ValidationSelfReport
	) a
GROUP BY
	CumulativeGpaTranscriptCategorical,
	Total,
	OrderByTranscript
ORDER BY
	OrderByTranscript;

SELECT
	Frq = count(*),
	Pct = convert(decimal(5,2), 100.00 * count(*) / Total),
	Within,
	CumulativeGpaTranscriptCategorical
FROM
	(
		SELECT
			Total = count(*) over(),
			CumulativeGpaTranscriptCategorical = case
					when CumulativeGpaTranscript > 4.0 then '>4.0'
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaTranscript < 1.0 then '<1.0'
				end,
			CumulativeGpaSelfReportCategorical = case
					when CumulativeGpaSelfReport > 4.0 then '>4.0'
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaSelfReport < 1.0 then '<1.0'
				end,
			Within = case when abs(CumulativeGpaTranscript - CumulativeGpaSelfReport) <= 0.3 then 1 else 0 end,
			OrderByTranscript = case
					when CumulativeGpaTranscript > 4.0 then 1
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then 2
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then 3
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then 4
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then 5
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then 6
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then 7
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then 8
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then 9
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then 10
					when CumulativeGpaTranscript < 1.0 then 11
				end,
			OrderBySelfReport = case
					when CumulativeGpaSelfReport > 4.0 then 1
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then 2
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then 3
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then 4
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then 5
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then 6
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then 7
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then 8
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then 9
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then 10
					when CumulativeGpaSelfReport < 1.0 then 11
				end
		FROM
			mmap.ValidationSelfReport
	) a
GROUP BY
	CumulativeGpaTranscriptCategorical,
	Total,
	OrderByTranscript,
	Within
ORDER BY
	OrderByTranscript;

SELECT
	Frq = count(*),
	Pct = convert(decimal(5,2), 100.00 * count(*) / Total),
	CumulativeGpaSelfReportCategorical
FROM
	(
		SELECT
			Total = count(*) over(),
			CumulativeGpaTranscriptCategorical = case
					when CumulativeGpaTranscript > 4.0 then '>4.0'
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaTranscript < 1.0 then '<1.0'
				end,
			CumulativeGpaSelfReportCategorical = case
					when CumulativeGpaSelfReport > 4.0 then '>4.0'
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaSelfReport < 1.0 then '<1.0'
				end,
			Within = case when abs(CumulativeGpaTranscript - CumulativeGpaSelfReport) <= 0.3 then 1 else 0 end,
			OrderByTranscript = case
					when CumulativeGpaTranscript > 4.0 then 1
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then 2
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then 3
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then 4
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then 5
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then 6
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then 7
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then 8
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then 9
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then 10
					when CumulativeGpaTranscript < 1.0 then 11
				end,
			OrderBySelfReport = case
					when CumulativeGpaSelfReport > 4.0 then 1
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then 2
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then 3
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then 4
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then 5
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then 6
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then 7
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then 8
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then 9
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then 10
					when CumulativeGpaSelfReport < 1.0 then 11
				end
		FROM
			mmap.ValidationSelfReport
	) a
GROUP BY
	CumulativeGpaSelfReportCategorical,
	Total,
	OrderBySelfReport
ORDER BY
	OrderBySelfReport;

SELECT
	Frq = count(*),
	Pct = convert(decimal(5,2), 100.00 * count(*) / Total),
	Within,
	CumulativeGpaSelfReportCategorical
FROM
	(
		SELECT
			Total = count(*) over(),
			CumulativeGpaTranscriptCategorical = case
					when CumulativeGpaTranscript > 4.0 then '>4.0'
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaTranscript < 1.0 then '<1.0'
				end,
			CumulativeGpaSelfReportCategorical = case
					when CumulativeGpaSelfReport > 4.0 then '>4.0'
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then '>=3.7 & <= 4.0'
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then '>=3.3 & < 3.7'
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then '>=3.0 & < 3.3'
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then '>=2.7 & < 3.0'
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then '>=2.3 & < 2.7'
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then '>=2.0 & < 2.3'
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then '>=1.7 & < 2.0'
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then '>=1.3 & < 1.7'
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then '>=1.0 & < 1.3'
					when CumulativeGpaSelfReport < 1.0 then '<1.0'
				end,
			Within = case when abs(CumulativeGpaTranscript - CumulativeGpaSelfReport) <= 0.3 then 1 else 0 end,
			OrderByTranscript = case
					when CumulativeGpaTranscript > 4.0 then 1
					when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then 2
					when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then 3
					when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then 4
					when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then 5
					when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then 6
					when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then 7
					when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then 8
					when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then 9
					when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then 10
					when CumulativeGpaTranscript < 1.0 then 11
				end,
			OrderBySelfReport = case
					when CumulativeGpaSelfReport > 4.0 then 1
					when CumulativeGpaSelfReport >= 3.7 and CumulativeGpaSelfReport <= 4.0 then 2
					when CumulativeGpaSelfReport >= 3.3 and CumulativeGpaSelfReport < 3.7 then 3
					when CumulativeGpaSelfReport >= 3.0 and CumulativeGpaSelfReport < 3.3 then 4
					when CumulativeGpaSelfReport >= 2.7 and CumulativeGpaSelfReport < 3.0 then 5
					when CumulativeGpaSelfReport >= 2.3 and CumulativeGpaSelfReport < 2.7 then 6
					when CumulativeGpaSelfReport >= 2.0 and CumulativeGpaSelfReport < 2.3 then 7
					when CumulativeGpaSelfReport >= 1.7 and CumulativeGpaSelfReport < 2.0 then 8
					when CumulativeGpaSelfReport >= 1.3 and CumulativeGpaSelfReport < 1.7 then 9
					when CumulativeGpaSelfReport >= 1.0 and CumulativeGpaSelfReport < 1.3 then 10
					when CumulativeGpaSelfReport < 1.0 then 11
				end
		FROM
			mmap.ValidationSelfReport
	) a
GROUP BY
	CumulativeGpaSelfReportCategorical,
	Total,
	OrderBySelfReport,
	Within
ORDER BY
	OrderBySelfReport;

SELECT
	Frq = count(*),
	AvgCgpaCalc = avg(CumulativeGpaTranscript),
	AvgCgpaSelf = avg(CumulativeGpaSelfReport),
	CumulativeGpaTranscriptCategorical = case
		when CumulativeGpaTranscript > 4.0 then '>4.0'
		when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then '>=3.7 & <= 4.0'
		when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then '>=3.3 & < 3.7'
		when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then '>=3.0 & < 3.3'
		when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then '>=2.7 & < 3.0'
		when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then '>=2.3 & < 2.7'
		when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then '>=2.0 & < 2.3'
		when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then '>=1.7 & < 2.0'
		when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then '>=1.3 & < 1.7'
		when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then '>=1.0 & < 1.3'
		when CumulativeGpaTranscript < 1.0 then '<1.0'
	end
FROM
	mmap.ValidationSelfReport
GROUP BY
	case
		when CumulativeGpaTranscript > 4.0 then '>4.0'
		when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then '>=3.7 & <= 4.0'
		when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then '>=3.3 & < 3.7'
		when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then '>=3.0 & < 3.3'
		when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then '>=2.7 & < 3.0'
		when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then '>=2.3 & < 2.7'
		when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then '>=2.0 & < 2.3'
		when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then '>=1.7 & < 2.0'
		when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then '>=1.3 & < 1.7'
		when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then '>=1.0 & < 1.3'
		when CumulativeGpaTranscript < 1.0 then '<1.0'
	end,
	case
		when CumulativeGpaTranscript > 4.0 then 1
		when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then 2
		when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then 3
		when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then 4
		when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then 5
		when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then 6
		when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then 7
		when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then 8
		when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then 9
		when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then 10
		when CumulativeGpaTranscript < 1.0 then 11
	end
ORDER BY
	case
		when CumulativeGpaTranscript > 4.0 then 1
		when CumulativeGpaTranscript >= 3.7 and CumulativeGpaTranscript <= 4.0 then 2
		when CumulativeGpaTranscript >= 3.3 and CumulativeGpaTranscript < 3.7 then 3
		when CumulativeGpaTranscript >= 3.0 and CumulativeGpaTranscript < 3.3 then 4
		when CumulativeGpaTranscript >= 2.7 and CumulativeGpaTranscript < 3.0 then 5
		when CumulativeGpaTranscript >= 2.3 and CumulativeGpaTranscript < 2.7 then 6
		when CumulativeGpaTranscript >= 2.0 and CumulativeGpaTranscript < 2.3 then 7
		when CumulativeGpaTranscript >= 1.7 and CumulativeGpaTranscript < 2.0 then 8
		when CumulativeGpaTranscript >= 1.3 and CumulativeGpaTranscript < 1.7 then 9
		when CumulativeGpaTranscript >= 1.0 and CumulativeGpaTranscript < 1.3 then 10
		when CumulativeGpaTranscript < 1.0 then 11
	end;



-- > t = read.table("C:/users/dlamoree/desktop/DATA - GPA All High School Grades.txt", sep="\t", col.names=c("id","tr","sr"))
-- > shapiro.test(t$tr)

        -- Shapiro-Wilk normality test

-- data:  t$tr
-- W = 0.99096, p-value = 0.00169

-- > shapiro.test(t$sr)

        -- Shapiro-Wilk normality test

-- data:  t$sr
-- W = 0.98474, p-value = 1.361e-05

-- > cor(t,y=NULL)
             -- id          tr          sr
-- id  1.000000000 -0.04901712 0.008658506
-- tr -0.049017116  1.00000000 0.685415748
-- sr  0.008658506  0.68541575 1.000000000