SET NOCOUNT ON;

TRUNCATE TABLE Mmap.K12Performance;

GO

IF (object_id('tempdb..#K12Student') is not null)
	BEGIN
		DROP TABLE #K12Student;
	END;

CREATE TABLE #K12Student (Iterator int identity(0,1) primary key clustered, StudentStateId char(10), CSISNum char(10));

GO

IF (object_id('tempdb..#K12StudentCohort') is not null)
	BEGIN
		DROP TABLE #K12StudentCohort;
	END;

CREATE TABLE #K12StudentCohort (StudentStateId char(10) primary key clustered, CSISNum char(10));

GO

INSERT
	#K12Student
	(
		StudentStateId,
		CSISNum
	)
SELECT DISTINCT
	StudentStateId = dbo.Get1289Encryption(CSISNum,'X'),
	CSISNum
FROM
	K12StudentProd
WHERE
	CSISNum is not null
	and CSISNum <> ''
	and len(dbo.Get1289Encryption(CSISNum,'X')) <= 10
ORDER BY
	CSISNum;

DECLARE
	@Time      datetime,
	@Seconds   int,
	@Severity  tinyint = 0,
	@State     tinyint = 1,
	@Error     varchar(2048),
	@Counter   int = 0,
	@Increment int = 100000,
	@Cycles    int;

SELECT
	@Cycles = convert(varchar, ceiling(convert(decimal, count(*)) / @Increment))
FROM
	#K12Student;

WHILE (@Counter < @Cycles)
BEGIN

	SET @Error = convert(varchar, @Counter) + ' of ' + convert(varchar, @Cycles - 1);

	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

	-- reset time
	SET @Time = getdate();

	TRUNCATE TABLE #K12StudentCohort;

	INSERT
		#K12StudentCohort
		(
			StudentStateId,
			CSISNum
		)
	SELECT
		StudentStateId,
		CSISNum
	FROM
		#K12Student i
	WHERE
		Iterator >= @Increment * @Counter
		and Iterator < @Increment * (@Counter + 1);

	DELETE
		a
	FROM
		#K12StudentCohort a
	WHERE
		exists (
			SELECT
				1
			FROM
				#K12StudentCohort b
			WHERE
				a.StudentStateId = b.StudentStateId
			HAVING
				count(*) > 1
		);

	MERGE
		mmap.K12CourseContent t
	USING
		(
			SELECT
				StudentStateId,
				DepartmentCode,
				GradeCode,
				SchoolCode,
				YearTermCode,
				TermCode,
				ContentCode,
				ContentRank,
				CourseCode,
				CourseUnits,
				CourseTitle,
				CourseAGCode,
				CourseLevelCode,
				CourseTypeCode,
				SectionMark,
				MarkPoints,
				MarkCategory,
				IsSuccess,
				IsPromoted,
				ContentSelector,
				GradeSelector,
				RecencySelector
			FROM
				#K12StudentCohort
				cross apply
				mmap.K12CourseContentGet(StudentStateId)
		) s
	ON
		s.StudentStateId = t.StudentStateId
		and s.DepartmentCode = t.DepartmentCode
		and s.RecencySelector = t.RecencySelector
	WHEN MATCHED THEN
		UPDATE SET
			t.StudentStateId  = s.StudentStateId,
			t.DepartmentCode  = s.DepartmentCode,
			t.GradeCode       = s.GradeCode,
			t.SchoolCode      = s.SchoolCode,
			t.YearTermCode    = s.YearTermCode,
			t.TermCode        = s.TermCode,
			t.ContentCode     = s.ContentCode,
			t.ContentRank     = s.ContentRank,
			t.CourseCode      = s.CourseCode,
			t.CourseUnits     = s.CourseUnits,
			t.CourseTitle     = s.CourseTitle,
			t.CourseAGCode    = s.CourseAGCode,
			t.CourseLevelCode = s.CourseLevelCode,
			t.CourseTypeCode  = s.CourseTypeCode,
			t.SectionMark     = s.SectionMark,
			t.MarkPoints      = s.MarkPoints,
			t.MarkCategory    = s.MarkCategory,
			t.IsPromoted      = s.IsPromoted,
			t.IsSuccess       = s.IsSuccess,
			t.ContentSelector = s.ContentSelector,
			t.GradeSelector   = s.GradeSelector,
			t.RecencySelector = s.RecencySelector
	WHEN NOT MATCHED THEN
		INSERT
			(
				StudentStateId,
				DepartmentCode,
				GradeCode,
				SchoolCode,
				YearTermCode,
				TermCode,
				ContentCode,
				ContentRank,
				CourseCode,
				CourseUnits,
				CourseTitle,
				CourseAGCode,
				CourseLevelCode,
				CourseTypeCode,
				SectionMark,
				MarkPoints,
				MarkCategory,
				IsSuccess,
				IsPromoted,
				ContentSelector,
				GradeSelector,
				RecencySelector
			)
		VALUES
			(
				s.StudentStateId,
				s.DepartmentCode,
				s.GradeCode,
				s.SchoolCode,
				s.YearTermCode,
				s.TermCode,
				s.ContentCode,
				s.ContentRank,
				s.CourseCode,
				s.CourseUnits,
				s.CourseTitle,
				s.CourseAGCode,
				s.CourseLevelCode,
				s.CourseTypeCode,
				s.SectionMark,
				s.MarkPoints,
				s.MarkCategory,
				s.IsSuccess,
				s.IsPromoted,
				s.ContentSelector,
				s.GradeSelector,
				s.RecencySelector
			);

	-- get time elapsed
	SET @Seconds = datediff(second, @Time, getdate());
	-- set msg
	SET @Error = 'INSERT: ' + 
		convert(nvarchar, @Seconds / 86400) + ':' +
		convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
	-- output to user
	RAISERROR(@Error, 0, 1) WITH NOWAIT;

	SET @Counter += 1;
END;