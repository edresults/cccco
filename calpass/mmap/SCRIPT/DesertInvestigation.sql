WITH
	cte
	(
		Derkey1,
		engl_cb21,
		math_cb21,
		gpa_cum,
		match_type,
		SubmissionFileId,
		IsTreatment,
		IsBulk,
		IsFirst,
		IsMultiCondition
	)
AS
	(
		SELECT
			Derkey1,
			engl_cb21,
			math_cb21,
			gpa_cum,
			match_type,
			SubmissionFileId,
			IsTreatmentRoadmmap,
			IsBulk  = max(case when SubmissionFileId in (22457, 22621) then 1 else 0 end) over(partition by Derkey1),
			IsFirst = row_number() over(partition by Derkey1 order by submissionfileid asc),
			IsMultiCondition = case when (SELECT count(distinct IsTreatmentRoadmmap) FROM mmap.ProspectiveCohort p1 WHERE p1.CollegeId = p.CollegeId and p1.Derkey1 = p.Derkey1) > 1 then 1 else 0 end
		FROM
			mmap.ProspectiveCohort p
		WHERE
			p.CollegeId = '931'
	)
SELECT
	Treatment = count(distinct case when IsTreatment = 1 then Derkey1 end),
	Control   = count(distinct case when IsTreatment = 0 then Derkey1 end),
	Detail    = 'Randomized; Not Bulk'
FROM
	cte
WHERE
	IsBulk = 0

UNION ALL

SELECT
	Treatment = count(distinct case when IsTreatment = 1 then Derkey1 end),
	Control   = count(distinct case when IsTreatment = 0 then Derkey1 end),
	Detail    = 'Randomized; Not Bulk; Single Assignment'
FROM
	cte
WHERE
	IsBulk = 0
	and IsMultiCondition = 0

UNION ALL

SELECT
	Treatment = count(distinct case when IsTreatment = 1 then Derkey1 end),
	Control   = count(distinct case when IsTreatment = 0 then Derkey1 end),
	Detail    = 'Randomized; Not Bulk; Single Assignment; Matched'
FROM
	cte
WHERE
	IsBulk = 0
	and IsMultiCondition = 0
	and (
		match_type <> 'No Match'
		or
		(
			match_type is null
			and
			exists (
				SELECT
					1
				FROM
					mmap.Transcript t
				WHERE
					t.InterSegmentKey = Derkey1
			)
		)
	)

UNION ALL

SELECT
	Treatment = count(distinct case when IsTreatment = 1 then Derkey1 end),
	Control   = count(distinct case when IsTreatment = 0 then Derkey1 end),
	Detail    = 'Randomized; Not Bulk; Single Assignment; Matched; GPA >= 2.6;'
FROM
	cte
WHERE
	IsBulk = 0
	and IsMultiCondition = 0
	and (
		gpa_cum >= 2.6
		or
		(
			match_type is null
			and
			exists (
				SELECT
					1
				FROM
					mmap.Transcript t
				WHERE
					t.InterSegmentKey = Derkey1
					and t.CumulativeGradePointAverage > 2.6
			)
		)
	)
	and (
		match_type <> 'No Match'
		or
		(
			match_type is null
			and
			exists (
				SELECT
					1
				FROM
					mmap.Transcript t
				WHERE
					t.InterSegmentKey = Derkey1
			)
		)
	)