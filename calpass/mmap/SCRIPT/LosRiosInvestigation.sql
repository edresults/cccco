DECLARE
	@OrganizationCode char(3) = '271',
	@DateUpperBound char(8) = '20180202';


	IF (object_id('tempdb..#College') is not null)
		BEGIN
			DROP TABLE #College;
		END;

	IF (object_id('tempdb..#ApplicationEvaluation') is not null)
		BEGIN
			DROP TABLE #ApplicationEvaluation;
		END;

	IF (object_id('tempdb..#ApplicationFeeders') is not null)
		BEGIN
			DROP TABLE #ApplicationFeeders;
		END;

	CREATE TABLE
		#College
		(
			college_id char(3) not null,
			PRIMARY KEY
				(
					college_id
				)
		);

	CREATE TABLE
		#ApplicationEvaluation
		(
			ccc_id             char(7)    not null,
			college_id         char(3)    not null,
			term_id            int        not null,
			InterSegmentKey    binary(64)     null,
			hs_cds             char(6)        null,
			Birthdate          datetime       null,
			IsLTEQ18           bit            null,
			IsApply            bit            null,
			IsApplyCollision   bit            null,
			IsCC               bit            null,
			IsCCCollision      bit            null,
			IsCCMutation       bit            null,
			IsHS               bit            null,
			IsHSCollision      bit            null,
			IsSubmitted        bit            null,
			IsEngl             bit            null,
			IsMath             bit            null,
			PRIMARY KEY
				(
					ccc_id,
					college_id,
					term_id
				),
			INDEX
				IX_LosRios__InterSegmentKey
			NONCLUSTERED
				(
					InterSegmentKey,
					college_id
				)
		);

	CREATE TABLE
		#ApplicationFeeders
		(
			CdsCode             char(14)     not null,
			DistrictName        varchar(255) not null,
			Applicants          int              null,
			Recent              int              null,
			Collisionless       int              null,
			Matched             int              null,
			MatchedRate         decimal(5,2)     null,
			Submitted           int              null,
			Placed              int              null,
			PlacedRate          decimal(5,2)     null,
			Student1617         int              null,
			Course1617          int              null,
			Student1516         int              null,
			Course1516          int              null,
			Student1415         int              null,
			Course1415          int              null,
			Student1314         int              null,
			Course1314          int              null,
			PRIMARY KEY
				(
					CdsCode
				)
		);

	INSERT
		#College
		(
			college_id
		)
	SELECT DISTINCT
		CollegeCode
	FROM
		comis.College
	WHERE
		CollegeCode = @OrganizationCode
		or DistrictCode = @OrganizationCode;

	-- base
	INSERT
		#ApplicationEvaluation
		(
			ccc_id,
			college_id,
			term_id,
			InterSegmentKey,
			hs_cds,
			IsApply,
			IsLTEQ18,
			IsApplyCollision,
			IsCC,
			IsCCCollision,
			IsCCMutation,
			IsHS,
			IsHSCollision,
			IsEngl,
			IsMath,
			IsSubmitted
		)
	SELECT
		a.ccc_id,
		a.college_id,
		a.term_id,
		a.InterSegmentKey,
		a.hs_cds,
		IsApply = 1,
		IsLTEQ18 = 0,
		IsApplyCollision = 0,
		IsCC = 0,
		IsCCCollision = 0,
		IsCCMutation = 0,
		IsHS = 0,
		IsHSCollision = 0,
		IsEngl = 0,
		IsMath = 0,
		IsSubmitted = 0
	FROM
		comis.ApplicationNew a
		inner join
		#College c
			on c.college_id = a.college_id
	WHERE
		hs_cds <> '000000';

	-- Recent Application Term
	DELETE
		t
	FROM
		#ApplicationEvaluation t
	WHERE
		t.term_id <> (
			SELECT
				max(t1.term_id)
			FROM
				#ApplicationEvaluation t1
			WHERE
				t1.ccc_id = t.ccc_id
				and t1.college_id = t.college_id
		);

	-- Apply Collision
	UPDATE
		t
	SET
		t.IsApplyCollision = 1
	FROM
		#ApplicationEvaluation t
		inner join
		(
			SELECT
				ae.InterSegmentKey
			FROM
				#ApplicationEvaluation ae
			GROUP BY
				ae.InterSegmentKey
			HAVING
				count(ae.ccc_id) > 1
		) s
			on s.InterSegmentKey= t.InterSegmentKey;

	-- InterSegmentKey
	UPDATE
		t
	SET
		t.IsCC = 1,
		t.BirthDate = convert(char(8), sb.birthdate, 112),
		t.IsLTEQ18 = case when (convert(int, convert(char(8), @DateUpperBound, 112)) - convert(char(8), sb.Birthdate, 112)) / 10000 <= 18 then 1 else 0 end
	FROM
		#ApplicationEvaluation t
		inner join
		comis.studntid s with(index=ix_studntid__InterSegmentKey)
			on s.InterSegmentKey = t.InterSegmentKey
			and s.college_id = t.college_id
		inner join
		comis.sbstudnt sb
			on sb.college_id = s.college_id
			and sb.student_id = s.student_id;

	UPDATE
		t
	SET
		t.IsCCCollision = s.IsCollision,
		t.IsCCMutation = s.IsMutation
	FROM
		#ApplicationEvaluation t
		inner join
		comis.Student s
			on s.InterSegmentKey = t.InterSegmentKey;

	-- InterSegmentKey
	UPDATE
		t
	SET
		t.IsHS = IsHighSchoolSection,
		t.IsHSCollision = s.IsHighSchoolCollision
	FROM
		#ApplicationEvaluation t
		inner join
		dbo.Student s
			on s.InterSegmentKey = t.InterSegmentKey;

	-- Placement
	UPDATE
		t
	SET
		t.IsSubmitted = 1,
		t.IsEngl = case when engl_cb21 is not null then 1 else 0 end,
		t.IsMath = case when math_cb21 is not null then 1 else 0 end
	FROM
		#ApplicationEvaluation t
		inner join
		mmap.ProspectiveCohort pc
			on pc.CollegeId = t.college_id
			and pc.Derkey1 = t.InterSegmentKey
	WHERE
		pc.SubmissionFileId = (
			SELECT
				max(SubmissionFileId)
			FROM
				mmap.ProspectiveCohort pc1
			WHERE
				pc1.CollegeId = t.college_id
				and pc1.Derkey1 = pc.Derkey1
				and pc1.SubmissionFileId <= pc.SubmissionFileId
		);

	-- feeders
	INSERT
		#ApplicationFeeders
		(
			CdsCode,
			DistrictName,
			Applicants,
			Recent,
			Collisionless,
			Matched,
			MatchedRate,
			Submitted,
			Placed,
			PlacedRate,
			Student1617,
			Course1617,
			Student1516,
			Course1516,
			Student1415,
			Course1415,
			Student1314,
			Course1314
		)
	SELECT
		OrganizationCode,
		District,
		Applicants,
		Recent,
		Collisionless,
		Matched,
		MatchedRate,
		Submitted,
		Placed,
		PlacedRate,
		Student1617  = max(case when ml.AcademicYear = '2016-2017' then ml.StudentCount end),
		Course1617   = max(case when ml.AcademicYear = '2016-2017' then ml.CourseCount  end),
		Student1516  = max(case when ml.AcademicYear = '2015-2016' then ml.StudentCount end),
		Course1516   = max(case when ml.AcademicYear = '2015-2016' then ml.CourseCount  end),
		Student1415  = max(case when ml.AcademicYear = '2014-2015' then ml.StudentCount end),
		Course1415   = max(case when ml.AcademicYear = '2014-2015' then ml.CourseCount  end),
		Student1314  = max(case when ml.AcademicYear = '2013-2014' then ml.StudentCount end),
		Course1314   = max(case when ml.AcademicYear = '2013-2014' then ml.CourseCount  end)
	FROM
		(
			SELECT TOP 10
				o.OrganizationId,
				o.OrganizationCode,
				District = i.District,
				Applicants = count(IsApply),
				Recent = sum(case when IsLTEQ18 = 1 then 1 else 0 end),
				Collisionless = sum(case when IsLTEQ18 = 1 and ae.IsHSCollision = 0 then 1 else 0 end),
				Matched = sum(case when IsLTEQ18 = 1 and ae.IsHSCollision = 0 and ae.IsHS = 1 then 1 else 0 end),
				MatchedRate = round(100.00 * sum(case when IsLTEQ18 = 1 and ae.IsHSCollision = 0 and ae.IsHS = 1 then 1 else 0 end) / nullif(sum(case when IsLTEQ18 = 1 and ae.IsHSCollision = 0 then 1 else 0 end), 0), 2),
				Submitted = sum(case when IsLTEQ18 = 1 and ae.IsHSCollision = 0 and ae.IsHS = 1 and IsSubmitted = 1 then 1 else 0 end),
				Placed = sum(case when IsLTEQ18 = 1 and ae.IsHSCollision = 0 and ae.IsHS = 1 and IsSubmitted = 1 and (IsEngl = 1 or IsMath = 1) then 1 else 0 end),
				PlacedRate = round(100.00 * sum(case when IsLTEQ18 = 1 and ae.IsHSCollision = 0 and ae.IsHS = 1 and IsSubmitted = 1  and (IsEngl = 1 or IsMath = 1) then 1 else 0 end) / nullif(sum(case when IsLTEQ18 = 1 and ae.IsHSCollision = 0 and ae.IsHS = 1 and IsSubmitted = 1 then 1 else 0 end), 0), 2)
			FROM
				#ApplicationEvaluation ae
				inner join
				calpads.Institution i
					on substring(i.CdsCode, 8, 6) = ae.hs_cds
				inner join
				dbo.Organization o
					on o.OrganizationCode = substring(i.CdsCode, 1, 7) + '0000000'
			WHERE
				ae.IsCC = 1
				and ae.IsApplyCollision = 0
				and ae.IsCCCollision = 0
				and ae.IsCCMutation = 0
			GROUP BY
				i.District,
				o.OrganizationId,
				o.OrganizationCode
			ORDER BY
				count(IsApply) DESC
		) d
		left outer join
		dbo.MemberList ml
			on ml.OrganizationId = d.OrganizationId
	GROUP BY
		OrganizationCode,
		District,
		Applicants,
		Recent,
		Collisionless,
		Matched,
		MatchedRate,
		Submitted,
		Placed,
		PlacedRate
	ORDER BY
		Applicants DESC;

	SELECT
		DistrictName,
		Applicants,
		Recent,
		Collisionless,
		Matched,
		MatchedRate,
		Submitted,
		Placed,
		PlacedRate,
		Student1617,
		Course1617,
		Student1516,
		Course1516,
		Student1415,
		Course1415,
		Student1314,
		Course1314
	FROM
		(
			SELECT
				DistrictName,
				Applicants,
				Recent,
				Collisionless,
				Matched,
				MatchedRate,
				Submitted,
				Placed,
				PlacedRate,
				Student1617,
				Course1617,
				Student1516,
				Course1516,
				Student1415,
				Course1415,
				Student1314,
				Course1314,
				OrderBy = 0
			FROM
				#ApplicationFeeders
			UNION ALL
			SELECT
				DistrictName  = 'Total: Top 10 Feeders',
				Applicants    = sum(isnull(Applicants, 0)),
				Recent        = sum(isnull(Recent, 0)),
				Collisionless = sum(isnull(Collisionless, 0)),
				Matched       = sum(isnull(Matched, 0)),
				MatchedRate   = convert(decimal(5,2), round((100.00 * sum(Matched)) / sum(Collisionless), 2)),
				Submitted     = sum(isnull(Submitted, 0)),
				Placed        = sum(isnull(Placed, 0)),
				PlacedRate    = convert(decimal(5,2), round((100.00 * sum(Placed)) / sum(Submitted), 2)),
				Student1617   = sum(isnull(Student1617, 0)),
				Course1617    = sum(isnull(Course1617, 0)),
				Student1516   = sum(isnull(Student1516, 0)),
				Course1516    = sum(isnull(Course1516, 0)),
				Student1415   = sum(isnull(Student1415, 0)),
				Course1415    = sum(isnull(Course1415, 0)),
				Student1314   = sum(isnull(Student1314, 0)),
				Course1314    = sum(isnull(Course1314, 0)),
				OrderBy = 1
			FROM
				#ApplicationFeeders
		) a
	ORDER BY
		OrderBy,
		Applicants DESC;


	-- SELECT
	-- 	ae.InterSegmentKey,
	-- 	f.SubmissionDateTime,
	-- 	engl_cb21,
	-- 	math_cb21,
	-- 	match_type
	-- FROM
	-- 	#ApplicationEvaluation ae
	-- 	inner join
	-- 	calpads.Institution i
	-- 		on substring(i.CdsCode, 8, 6) = ae.hs_cds
	-- 	inner join
	-- 	dbo.Organization o
	-- 		on o.OrganizationCode = substring(i.CdsCode, 1, 7) + '0000000'
	-- 	left outer join
	-- 	mmap.ProspectiveCohort pc
	-- 		on pc.CollegeId = ae.college_id
	-- 		and pc.Derkey1 = ae.InterSegmentKey
	-- 	left outer join
	-- 	[PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedrop f
	-- 		on f.SubmissionFileId = pc.SubmissionFileId
	-- WHERE
	-- 	ae.IsCC = 1
	-- 	and ae.IsApplyCollision = 0
	-- 	and ae.IsCCCollision = 0
	-- 	and ae.IsCCMutation = 0
	-- 	and o.OrganizationCode = '31669280000000'
	-- 	and IsLTEQ18 = 1
	-- 	and ae.IsHSCollision = 0
	-- 	and ae.IsHS = 1
	-- 	and IsSubmitted = 1
	-- 	and pc.SubmissionFileId = (
	-- 		SELECT
	-- 			max(SubmissionFileId)
	-- 		FROM
	-- 			mmap.ProspectiveCohort pc1
	-- 		WHERE
	-- 			pc1.CollegeId = pc.CollegeId
	-- 			and pc1.Derkey1 = pc.Derkey1
	-- 			and pc1.SubmissionFileId <= pc.SubmissionFileId
	-- 	);