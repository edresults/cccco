SET NOCOUNT ON;

-- GO

-- IF (object_id('tempdb..#K12Student') is not null)
-- 	BEGIN
-- 		DROP TABLE #K12Student;
-- 	END;

-- CREATE TABLE #K12Student (Iterator int identity(0,1) primary key clustered, StudentStateId char(10), CSISNum char(10));

-- GO

-- IF (object_id('tempdb..#K12StudentCohort') is not null)
-- 	BEGIN
-- 		DROP TABLE #K12StudentCohort;
-- 	END;

-- CREATE TABLE #K12StudentCohort (StudentStateId char(10) primary key clustered, CSISNum char(10));

-- GO

-- INSERT
-- 	#K12Student
-- 	(
-- 		StudentStateId,
-- 		CSISNum
-- 	)
-- SELECT DISTINCT
-- 	StudentStateId = dbo.Get1289Encryption(CSISNum,'X'),
-- 	CSISNum
-- FROM
-- 	K12StudentProd s
-- WHERE
-- 	CSISNum is not null
-- 	and CSISNum <> ''
-- 	and len(dbo.Get1289Encryption(CSISNum,'X')) <= 10
-- 	and exists (
-- 		SELECT
-- 			1
-- 		FROM
-- 			K12CourseProd c
-- 		WHERE
-- 			s.School           = c.School
-- 			and s.LocStudentId = c.LocStudentId
-- 			and s.AcYear       = c.AcYear
-- 			and s.GradeLevel in ('09', '10', '11', '12')
-- 	)
-- ORDER BY
-- 	CSISNum;

DECLARE
	@Message       nvarchar(2048),
	@ErrorCode     int = 70099,
	@ErrorSeverity int = 1,
	@True          bit = 1,
	@Time          datetime,
	@Seconds       int,
	@Severity      tinyint = 0,
	@State         tinyint = 1,
	@Error         varchar(2048),
	@Counter       int = 0,
	@Increment     int = 100000,
	@Cycles        int;

SELECT
	@Cycles = convert(varchar, ceiling(convert(decimal, count(*)) / @Increment))
FROM
	#K12Student;

WHILE (@Counter < @Cycles)
BEGIN

	SET @Error = convert(varchar, @Counter) + ' of ' + convert(varchar, @Cycles - 1);

	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

	-- reset time
	SET @Time = getdate();

	TRUNCATE TABLE #K12StudentCohort;

	INSERT
		#K12StudentCohort
		(
			StudentStateId,
			CSISNum
		)
	SELECT
		StudentStateId,
		CSISNum
	FROM
		#K12Student i
	WHERE
		Iterator >= @Increment * @Counter
		and Iterator < @Increment * (@Counter + 1);

	DELETE
		a
	FROM
		#K12StudentCohort a
	WHERE
		exists (
			SELECT
				1
			FROM
				#K12StudentCohort b
			WHERE
				a.StudentStateId = b.StudentStateId
			HAVING
				count(*) > 1
		);

	INSERT
		mmap.K12Performance
		(
			StudentStateId,
			DepartmentCode,
			GradeCode,
			QualityPoints,
			CreditAttempted,
			GradePointAverage,
			GradePointAverageSans,
			CumulativeQualityPoints,
			CumulativeCreditAttempted,
			CumulativeGradePointAverage,
			CumulativeGradePointAverageSans,
			FirstYearTermCode,
			LastYearTermCode,
			IsFirst,
			IsLast
		)
	SELECT
		StudentStateId,
		DepartmentCode,
		GradeCode,
		QualityPoints,
		CreditAttempted,
		GradePointAverage,
		GradePointAverageSans,
		CumulativeQualityPoints,
		CumulativeCreditAttempted,
		CumulativeGradePointAverage,
		CumulativeGradePointAverageSans,
		FirstYearTermCode,
		LastYearTermCode,
		IsFirst,
		IsLast
	FROM
		#K12StudentCohort
		cross apply
		Mmap.K12PerformanceGet(CSISNum);

	INSERT
		mmap.K12CourseContent
		(
			StudentStateId,
			DepartmentCode,
			GradeCode,
			SchoolCode,
			YearTermCode,
			TermCode,
			ContentCode,
			ContentRank,
			CourseCode,
			CourseUnits,
			CourseTitle,
			CourseAGCode,
			CourseLevelCode,
			CourseTypeCode,
			SectionMark,
			MarkPoints,
			MarkCategory,
			IsSuccess,
			IsPromoted,
			ContentSelector,
			GradeSelector,
			RecencySelector
		)
	SELECT
		StudentStateId,
		DepartmentCode,
		GradeCode,
		SchoolCode,
		YearTermCode,
		TermCode,
		ContentCode,
		ContentRank,
		CourseCode,
		CourseUnits,
		CourseTitle,
		CourseAGCode,
		CourseLevelCode,
		CourseTypeCode,
		SectionMark,
		MarkPoints,
		MarkCategory,
		IsSuccess,
		IsPromoted,
		ContentSelector,
		GradeSelector,
		RecencySelector
	FROM
		#K12StudentCohort
		cross apply
		mmap.K12CourseContentGet(CSISNum);

	INSERT
		mmap.K12Placement
		(
			StudentStateId,
			EnglishY,
			EnglishA,
			EnglishB,
			EnglishC,
			PreAlgebra,
			AlgebraI,
			AlgebraII,
			MathGE,
			[Statistics],
			CollegeAlgebra,
			Trigonometry,
			PreCalculus,
			CalculusI,
			ReadingM_UboundY,
			ReadingY_UboundY,
			ReadingA_UboundY,
			ReadingB_UboundY,
			ReadingC_UboundY,
			ReadingM_UboundA,
			ReadingA_UboundA,
			ReadingB_UboundA,
			ReadingC_UboundA,
			ReadingM_UboundB,
			ReadingB_UboundB,
			ReadingC_UboundB,
			EslY_UboundY,
			EslA_UboundY,
			EslB_UboundY,
			EslA_UboundA,
			EslB_UboundA,
			EslC_UboundA,
			EslB_UboundB,
			EslC_UboundB,
			EslD_UboundB,
			IsEscrow
		)
	SELECT
		i.StudentStateId,
		p.EnglishY,
		p.EnglishA,
		p.EnglishB,
		p.EnglishC,
		p.PreAlgebra,
		p.AlgebraI,
		p.AlgebraII,
		p.MathGE,
		p.[Statistics],
		p.CollegeAlgebra,
		p.Trigonometry,
		p.PreCalculus,
		p.CalculusI,
		p.ReadingM_UboundY,
		p.ReadingY_UboundY,
		p.ReadingA_UboundY,
		p.ReadingB_UboundY,
		p.ReadingC_UboundY,
		p.ReadingM_UboundA,
		p.ReadingA_UboundA,
		p.ReadingB_UboundA,
		p.ReadingC_UboundA,
		p.ReadingM_UboundB,
		p.ReadingB_UboundB,
		p.ReadingC_UboundB,
		p.EslY_UboundY,
		p.EslA_UboundY,
		p.EslB_UboundY,
		p.EslA_UboundA,
		p.EslB_UboundA,
		p.EslC_UboundA,
		p.EslB_UboundB,
		p.EslC_UboundB,
		p.EslD_UboundB,
		IsEscrow = @True
	FROM
		#K12StudentCohort i
		cross apply
		Mmap.K12PlacementGet(i.StudentStateId) p;

	INSERT
		mmap.K12Transcript
		(
			StudentStateId,
			IsHighSchoolGrade09,
			IsHighSchoolGrade10,
			IsHighSchoolGrade11,
			IsHighSchoolGrade12,
			CumulativeGradePointAverage,
			English,
			PreAlgebra,
			AlgebraI,
			Geometry,
			AlgebraII,
			Trigonometry,
			PreCalculus,
			[Statistics],
			Calculus,
			IsEscrow
		)
	SELECT
		StudentStateId,
		IsHighSchoolGrade09,
		IsHighSchoolGrade10,
		IsHighSchoolGrade11,
		IsHighSchoolGrade12,
		CumulativeGradePointAverage,
		English,
		PreAlgebra,
		AlgebraI,
		Geometry,
		AlgebraII,
		Trigonometry,
		PreCalculus,
		[Statistics],
		Calculus,
		IsEscrow = @True
	FROM
		#K12StudentCohort i
		cross apply
		mmap.K12TranscriptGet(i.StudentStateId) t;

	-- get time elapsed
	SET @Seconds = datediff(second, @Time, getdate());
	-- set msg
	SET @Error = 'INSERT: ' + 
		convert(nvarchar, @Seconds / 86400) + ':' +
		convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
	-- output to user
	RAISERROR(@Error, 0, 1) WITH NOWAIT;

	SET @Counter += 1;
END;


EXECUTE mmap.K12PlacementMigrate;
EXECUTE mmap.K12TranscriptMigrate;