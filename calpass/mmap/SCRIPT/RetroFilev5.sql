DECLARE
	@ExeStr varchar(2048) = '',
	@CollegeId char(3) = '472';

SET @ExeStr = 'bcp "EXEC calpass.mmap.RetroFileMathReturn ''' + @CollegeId +'''" QUERYOUT "C:\Users\dlamoree.ERP\desktop\' + @CollegeId + '_Retrospective_Mathematics.txt" -T -c -q';
print @exestr;
-- extract
EXECUTE xp_cmdshell
	@ExeStr;

-- extract
EXECUTE sp_spss_output
	'calpass',
	'mmap',
	'RetroMath',
	'C:\Users\dlamoree.ERP\desktop\',
	'1';

SET @ExeStr = 'bcp "calpass.mmap.RetroFileEnglReturn ''' + @CollegeId +'''" QUERYOUT "C:\Users\dlamoree.ERP\desktop\' + @CollegeId + '_Retrospective_English.txt" -T -c -q';

-- extract
EXECUTE xp_cmdshell
	@ExeStr;

-- extract
EXECUTE sp_spss_output
	'calpass',
	'mmap',
	'RetroEngl',
	'C:\Users\dlamoree.ERP\desktop\',
	'1';