SELECT
	f.*
FROM
	cms_production_calpass.dbo.calpassplus_filedrop f
	inner join
	cms_production_calpass.dbo.organization o
		on o.OrganizationId = f.OrganizationId
	inner join
	[PRO-DAT-SQL-03].calpass.comis.college c
		on c.ipedscodelegacy = o.organizationcode
WHERE
	f.submissionfiledescriptionid = 8
	and c.CollegeCode = '871'
	and Status != 'Failed'
	and not exists (
		SELECT
			1
		FROM
			[PRO-DAT-SQL-03].calpass.mmap.ValidationAssessment va
		WHERE
			va.CollegeId = c.CollegeCode
	)
	and not exists (
		SELECT
			1
		FROM
			[PRO-DAT-SQL-03].calpass.mmap.ValidationAssessmentArchive va
		WHERE
			va.college_id = c.CollegeCode
	);

SELECT
	a.Type,
	c.CollegeCode,
	c.Label
FROM
	(
		SELECT
			CollegeId,
			Type = 'New'
		FROM
			[PRO-DAT-SQL-03].calpass.mmap.ValidationAssessment
		UNION
		SELECT
			college_id,
			Type = 'Old'
		FROM
			[PRO-DAT-SQL-03].calpass.mmap.ValidationAssessmentArchive
	) a
	inner join
	comis.College c
		on c.CollegeCode = a.CollegeId
ORDER BY
	a.Type,
	c.CollegeCode