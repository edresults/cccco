USE calpass;

GO

IF (object_id('mmap.ValidationAssessmentOutput') is not null)
	BEGIN
		DROP TABLE mmap.ValidationAssessmentOutput;
	END;

GO

CREATE TABLE
	mmap.ValidationAssessmentOutput
	(
		-- <ixc>
		CollegeId char(3),
		InterSegmentKey binary(64),
		-- </ixc>
		-- <ValidationAssessment>
		TermCode char(3) not null,
		PlacementTypeId tinyint not null,
		MMMethodId tinyint not null,
		MMEligibilityId tinyint not null,
		MMBasisId tinyint not null,
		TestSubjectCode varchar(4) not null,
		TestDescription varchar(255) not null,
		TestDate char(8) not null,
		TestScoreDescriptionId tinyint not null,
		TestScore decimal(6,2),
		TestScoreCompensatory decimal(6,2),
		TestPlacementLevelDescription varchar(8000),
		TestPlacementLevelCode char(1),
		TestPlacementMathCourseTransferArray char(9),
		MMPlacementLevelDescription varchar(8000),
		MMPlacementLevelCode char(1),
		MMPlacementMathCourseTransferArray char(9),
		FinalPlacementLevelDescription varchar(8000),
		FinalPlacementLevelCode char(1),
		FinalPlacementMathCourseTransferArray char(9),
		-- </ValidationAssessment>
		-- <transcript>
		TermCodeFull char(5),
		CourseId varchar(12),
		CourseTitle varchar(68),
		CourseLevelCode char(1),
		CourseLevelRank tinyint,
		SectionId varchar(6),
		SectionUnitsAttempted decimal(4,2),
		SectionGradeCode varchar(3),
		SectionGradeRank tinyint,
		-- </transcript>
		-- <demographic>
		gender char(1),
		ipeds_race char(1),
		goal char(1),
		eops char(1),
		dsps char(1),
		-- </demographic>
		-- <additional>
		MultiCourseEnrollmentInd bit
		-- </additional>
	);

GO

CREATE CLUSTERED INDEX
	ixc_ValidationAssessmentOutput
ON
	mmap.ValidationAssessmentOutput
	(
		CollegeId,
		InterSegmentKey,
		TestSubjectCode,
		TermCodeFull,
		CourseLevelRank,
		SectionUnitsAttempted,
		SectionGradeRank
	);

GO

OPEN SYMMETRIC KEY
	SecPii
DECRYPTION BY CERTIFICATE
	SecPii;

INSERT INTO
	mmap.ValidationAssessmentOutput
	(
		CollegeId,
		InterSegmentKey,
		TermCode,
		PlacementTypeId,
		MMMethodId,
		MMEligibilityId,
		MMBasisId,
		TestSubjectCode,
		TestDescription,
		TestDate,
		TestScoreDescriptionId,
		TestScore,
		TestScoreCompensatory,
		TestPlacementLevelDescription,
		TestPlacementLevelCode,
		TestPlacementMathCourseTransferArray,
		MMPlacementLevelDescription,
		MMPlacementLevelCode,
		MMPlacementMathCourseTransferArray,
		FinalPlacementLevelDescription,
		FinalPlacementLevelCode,
		FinalPlacementMathCourseTransferArray,
		TermCodeFull,
		CourseId,
		CourseTitle,
		CourseLevelCode,
		CourseLevelRank,
		SectionId,
		SectionUnitsAttempted,
		SectionGradeCode,
		SectionGradeRank,
		gender,
		ipeds_race,
		goal,
		eops,
		dsps
	)
SELECT
	va.CollegeId,
	va.InterSegmentKey,
	va.TermCode,
	va.PlacementTypeId,
	va.MMMethodId,
	va.MMEligibilityId,
	va.MMBasisId,
	va.TestSubjectCode,
	va.TestDescription,
	va.TestDate,
	va.TestScoreDescriptionId,
	va.TestScore,
	va.TestScoreCompensatory,
	va.TestPlacementLevelDescription,
	va.TestPlacementLevelCode,
	va.TestPlacementMathCourseTransferArray,
	va.MMPlacementLevelDescription,
	va.MMPlacementLevelCode,
	va.MMPlacementMathCourseTransferArray,
	va.FinalPlacementLevelDescription,
	va.FinalPlacementLevelCode,
	va.FinalPlacementMathCourseTransferArray,
	TermCodeFull = t.YearTermCode,
	CourseId = sx.course_id,
	CourseTitle = cb.title,
	CourseLevelCode = cb.prior_to_college,
	CourseLevelRank = cl.Rank,
	SectionId = sx.section_id,
	SectionUnitsAttempted = sx.units_attempted,
	SectionGradeCode = sx.grade,
	SectionGradeRank = g.Rank,
	gender = convert(char(1), DecryptByKey(st.gender_enc)),
	ipeds_race = convert(char(1), DecryptByKey(st.ipeds_race_enc)),
	st.goal,
	se.eops_care_status,
	sd.primary_disability
FROM
	mmap.ValidationAssessment va
	left outer join
	comis.studntid id
		on id.college_id = va.CollegeId
		and id.ssn = va.StudentId
		and id.student_id_status = va.IdStatusCode
	left outer join
	comis.sxenrlm sx
		on sx.college_id = id.college_id
		and sx.student_id = id.student_id
	left outer join
	comis.cbcrsinv cb
		on sx.college_id = cb.college_id
		and sx.term_id = cb.term_id
		and sx.course_id = cb.course_id
		and sx.control_number = cb.control_number
	left outer join
	comis.stterm st
		on st.college_id = sx.college_id
		and st.student_id = sx.student_id
		and st.term_id = sx.term_id
	left outer join
	comis.Program p
		on p.ProgramCode = cb.top_code
		and p.SubjectCode = va.TestSubjectCode
	left outer join
	comis.Term t
		on t.TermCode = sx.term_id
	left outer join
	comis.CourseLevel cl
		on cl.CourseLevelCode = cb.prior_to_college
	left outer join
	comis.Grade g
		on g.GradeCode = sx.grade
	left outer join
	comis.sddsps sd
		on sd.college_id = st.college_id
		and sd.student_id = st.student_id
		and sd.term_id = st.term_id
	left outer join
	comis.seeops se
		on se.college_id = st.college_id
		and se.student_id = st.student_id
		and se.term_id = st.term_id
WHERE
	va.InterSegmentKey is not null;

CLOSE SYMMETRIC KEY	SecPii;

-- Time :: Lowest YearTerm Value
DELETE
	a
FROM
	mmap.ValidationAssessmentOutput a
WHERE
	exists (
		SELECT
			1
		FROM
			mmap.ValidationAssessmentOutput a1
		WHERE
			a1.CollegeId = a.CollegeId
			and a1.InterSegmentKey = a.InterSegmentKey
			and a1.TestSubjectCode = a.TestSubjectCode
		HAVING
			min(a1.TermCodeFull) != a.TermCodeFull
	);

UPDATE
	a
SET
	a.MultiCourseEnrollmentInd = 1
FROM
	mmap.ValidationAssessmentOutput a
WHERE
	exists (
		SELECT
			1
		FROM
			mmap.ValidationAssessmentOutput a1
		WHERE
			a1.CollegeId = a.CollegeId
			and a1.InterSegmentKey = a.InterSegmentKey
			and a1.TestSubjectCode = a.TestSubjectCode
			and a1.TermCodeFull = a.TermCodeFull
		HAVING
			count(*) > 1
	);

GO

IF (object_id('mmap.p_ValidationAssessmentOutput') is not null)
	BEGIN
		DROP PROCEDURE mmap.p_ValidationAssessmentOutput;
	END;

GO

CREATE PROCEDURE
	mmap.p_ValidationAssessmentOutput
AS
	
BEGIN
	SELECT
		CollegeId,
		InterSegmentKey,
		TermCode,
		PlacementTypeId,
		MMMethodId,
		MMEligibilityId,
		MMBasisId,
		TestSubjectCode,
		TestDescription,
		TestDate,
		TestScoreDescriptionId,
		TestScore,
		TestScoreCompensatory,
		TestPlacementLevelDescription,
		TestPlacementLevelCode,
		TestPlacementMathCourseTransferArray,
		MMPlacementLevelDescription,
		MMPlacementLevelCode,
		MMPlacementMathCourseTransferArray,
		FinalPlacementLevelDescription,
		FinalPlacementLevelCode,
		FinalPlacementMathCourseTransferArray,
		TermCodeFull,
		CourseId,
		CourseTitle,
		CourseLevelCode,
		CourseLevelRank,
		SectionId,
		SectionUnitsAttempted,
		SectionGradeCode,
		SectionGradeRank,
		gender,
		ipeds_race,
		goal,
		eops,
		dsps,
		MultiCourseEnrollmentInd
	FROM
		mmap.ValidationAssessmentOutput;
END;

GO

-- extract
EXECUTE xp_cmdshell
	'bcp "EXECUTE calpass.mmap.p_ValidationAssessmentOutput" QUERYOUT "\\10.11.5.61\SFTPRoot\steps2014\Assessment\ValidationAssessmentOutput.txt" -T -c -q';
-- extract
EXECUTE dbo.sp_spss_output
	'calpass',
	'mmap',
	'ValidationAssessmentOutput',
	'\\10.11.5.61\SFTPRoot\steps2014\Assessment\',  -- '
	'1';

GO

-- delete
EXECUTE master.dbo.clr_file_delete
	@file_path = 'C:\Users\dlamoree\desktop\ValidationAssessmentOutput.txt';

EXECUTE master.dbo.clr_file_delete
	@file_path = 'C:\Users\dlamoree\desktop\ValidationAssessmentOutput_spss_import.sps';

GO

-- clean up
DROP PROCEDURE mmap.p_ValidationAssessmentOutput;
DROP TABLE mmap.ValidationAssessmentOutput;