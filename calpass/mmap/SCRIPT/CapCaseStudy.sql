/* EXAMPLE STUDENT WITH 2101 ENGLISH */

SELECT
	*
FROM
	Mmap.CapMmapEnglishGrade
WHERE
	InterSegmentKey = 0x002FC08451EE468FF31CE6EAFB6BD5CCE444A3A3BE6EDA2C61F50369B9F1C4740744A4EB23EF9D1741FE6145A65CCA523F69315E16AF60A5877835CB57866A9B;

SELECT
	*
FROM
	dbo.Student
WHERE
	InterSegmentKey = 0x002FC08451EE468FF31CE6EAFB6BD5CCE444A3A3BE6EDA2C61F50369B9F1C4740744A4EB23EF9D1741FE6145A65CCA523F69315E16AF60A5877835CB57866A9B
	and IsGrade11SectionInclusive = 1
	and IsHighSchoolCollision = 0;

SELECT
	*
FROM
	Mmap.RetrospectiveCourseContent
WHERE
	InterSegmentKey = 0x002FC08451EE468FF31CE6EAFB6BD5CCE444A3A3BE6EDA2C61F50369B9F1C4740744A4EB23EF9D1741FE6145A65CCA523F69315E16AF60A5877835CB57866A9B
	and DepartmentCode = '14';

SELECT
	c.*
FROM
	K12CourseProd c
	inner join
	K12StudentProd s
		on c.School = s.School
		and c.LocStudentId = s.LocStudentId
		and c.AcYear = s.AcYear
	inner join
	dbo.Student ss
		on ss.InterSegmentKey = s.Derkey1
	left outer join
	Mmap.English e
		on e.CourseCode = c.CourseId
WHERE
	s.Derkey1 = 0x002FC08451EE468FF31CE6EAFB6BD5CCE444A3A3BE6EDA2C61F50369B9F1C4740744A4EB23EF9D1741FE6145A65CCA523F69315E16AF60A5877835CB57866A9B
	and s.GradeLevel in ('09','10','11','12');

SELECT
	*
FROM
	dbo.Student
WHERE
	InterSegmentKey = 0x002FC08451EE468FF31CE6EAFB6BD5CCE444A3A3BE6EDA2C61F50369B9F1C4740744A4EB23EF9D1741FE6145A65CCA523F69315E16AF60A5877835CB57866A9B
	and IsGrade11SectionInclusive = 1
	and IsHighSchoolCollision = 0;

SELECT
	*
FROM
	Mmap.CollegeCourseContent
WHERE
	InterSegmentKey = 0x002FC08451EE468FF31CE6EAFB6BD5CCE444A3A3BE6EDA2C61F50369B9F1C4740744A4EB23EF9D1741FE6145A65CCA523F69315E16AF60A5877835CB57866A9B
	and TopCode = '150100';

SELECT
	sx.*,
	cb.*
FROM
	comis.studntid id
	inner join
	comis.sxenrlm sx
		on sx.college_id = id.college_id
		and sx.student_id = id.student_id
	inner join
	comis.cbcrsinv cb
		on cb.college_id = sx.college_id
		and cb.course_id = sx.course_id
		and cb.control_number = sx.control_number
		and cb.term_id = sx.term_id
WHERE
	id.InterSegmentKey = 0x002FC08451EE468FF31CE6EAFB6BD5CCE444A3A3BE6EDA2C61F50369B9F1C4740744A4EB23EF9D1741FE6145A65CCA523F69315E16AF60A5877835CB57866A9B
	and sx.credit_flag in ('T','D','C','S')
	and sx.units_attempted > 1
	and cb.top_code in ('150100')

SELECT
	*
FROM
	Mmap.CapCohort
WHERE
	InterSegmentKey = 0x002FC08451EE468FF31CE6EAFB6BD5CCE444A3A3BE6EDA2C61F50369B9F1C4740744A4EB23EF9D1741FE6145A65CCA523F69315E16AF60A5877835CB57866A9B;

-- SELECT
-- 	CourseId,
-- 	Records = count(*)
-- FROM
-- 	K12CourseProd
-- 	inner join
-- 	Mmap.English
-- 		on CourseCode = CourseId
-- GROUP BY
-- 	CourseId
-- UNION
-- SELECT
-- 	'2101',
-- 	Records = count(*)
-- FROM
-- 	K12CourseProd
-- WHERE
-- 	CourseId = '2101';