TRUNCATE TABLE Mmap.RetroEsl;

INSERT INTO
	Mmap.RetroEsl
	(
		InterSegmentKey,
		CCEACNFLCollegeCode,
		CCEACNFLYearTermCode,
		CCEACNFLCourseId,
		CCEACNFLCourseControlNumber,
		CCEACNFLCourseTopCode,
		CCEACNFLCourseTitle,
		CCEACNFLCourseMarkLetter,
		CCEACNFLCourseMarkPoints,
		CCEACNFLCourseLevelCode,
		CCEACNFLCourseCreditCode,
		CCEACN00CollegeCode,
		CCEACN00YearTermCode,
		CCEACN00CourseId,
		CCEACN00CourseControlNumber,
		CCEACN00CourseTopCode,
		CCEACN00CourseTitle,
		CCEACN00CourseMarkLetter,
		CCEACN00CourseMarkPoints,
		CCEACN00CourseLevelCode,
		CCEACN00CourseCreditCode,
		CCEACN01CollegeCode,
		CCEACN01YearTermCode,
		CCEACN01CourseId,
		CCEACN01CourseControlNumber,
		CCEACN01CourseTopCode,
		CCEACN01CourseTitle,
		CCEACN01CourseMarkLetter,
		CCEACN01CourseMarkPoints,
		CCEACN01CourseLevelCode,
		CCEACN01CourseCreditCode,
		CCEACN02CollegeCode,
		CCEACN02YearTermCode,
		CCEACN02CourseId,
		CCEACN02CourseControlNumber,
		CCEACN02CourseTopCode,
		CCEACN02CourseTitle,
		CCEACN02CourseMarkLetter,
		CCEACN02CourseMarkPoints,
		CCEACN02CourseLevelCode,
		CCEACN02CourseCreditCode,
		CCEACN03CollegeCode,
		CCEACN03YearTermCode,
		CCEACN03CourseId,
		CCEACN03CourseControlNumber,
		CCEACN03CourseTopCode,
		CCEACN03CourseTitle,
		CCEACN03CourseMarkLetter,
		CCEACN03CourseMarkPoints,
		CCEACN03CourseLevelCode,
		CCEACN03CourseCreditCode,
		CCEACN04CollegeCode,
		CCEACN04YearTermCode,
		CCEACN04CourseId,
		CCEACN04CourseControlNumber,
		CCEACN04CourseTopCode,
		CCEACN04CourseTitle,
		CCEACN04CourseMarkLetter,
		CCEACN04CourseMarkPoints,
		CCEACN04CourseLevelCode,
		CCEACN04CourseCreditCode,
		CCEACN05CollegeCode,
		CCEACN05YearTermCode,
		CCEACN05CourseId,
		CCEACN05CourseControlNumber,
		CCEACN05CourseTopCode,
		CCEACN05CourseTitle,
		CCEACN05CourseMarkLetter,
		CCEACN05CourseMarkPoints,
		CCEACN05CourseLevelCode,
		CCEACN05CourseCreditCode,
		CCEACN06CollegeCode,
		CCEACN06YearTermCode,
		CCEACN06CourseId,
		CCEACN06CourseControlNumber,
		CCEACN06CourseTopCode,
		CCEACN06CourseTitle,
		CCEACN06CourseMarkLetter,
		CCEACN06CourseMarkPoints,
		CCEACN06CourseLevelCode,
		CCEACN06CourseCreditCode,
		CCEACN07CollegeCode,
		CCEACN07YearTermCode,
		CCEACN07CourseId,
		CCEACN07CourseControlNumber,
		CCEACN07CourseTopCode,
		CCEACN07CourseTitle,
		CCEACN07CourseMarkLetter,
		CCEACN07CourseMarkPoints,
		CCEACN07CourseLevelCode,
		CCEACN07CourseCreditCode,
		CCEACN08CollegeCode,
		CCEACN08YearTermCode,
		CCEACN08CourseId,
		CCEACN08CourseControlNumber,
		CCEACN08CourseTopCode,
		CCEACN08CourseTitle,
		CCEACN08CourseMarkLetter,
		CCEACN08CourseMarkPoints,
		CCEACN08CourseLevelCode,
		CCEACN08CourseCreditCode,
		-- CCEWCRFLCollegeCode,
		-- CCEWCRFLYearTermCode,
		-- CCEWCRFLCourseId,
		-- CCEWCRFLCourseControlNumber,
		-- CCEWCRFLCourseTopCode,
		-- CCEWCRFLCourseTitle,
		-- CCEWCRFLCourseMarkLetter,
		-- CCEWCRFLCourseMarkPoints,
		-- CCEWCRFLCourseLevelCode,
		-- CCEWCR00CollegeCode,
		-- CCEWCR00YearTermCode,
		-- CCEWCR00CourseId,
		-- CCEWCR00CourseControlNumber,
		-- CCEWCR00CourseTopCode,
		-- CCEWCR00CourseTitle,
		-- CCEWCR00CourseMarkLetter,
		-- CCEWCR00CourseMarkPoints,
		-- CCEWCR00CourseLevelCode,
		-- CCEWCR01CollegeCode,
		-- CCEWCR01YearTermCode,
		-- CCEWCR01CourseId,
		-- CCEWCR01CourseControlNumber,
		-- CCEWCR01CourseTopCode,
		-- CCEWCR01CourseTitle,
		-- CCEWCR01CourseMarkLetter,
		-- CCEWCR01CourseMarkPoints,
		-- CCEWCR01CourseLevelCode,
		-- CCEWCR02CollegeCode,
		-- CCEWCR02YearTermCode,
		-- CCEWCR02CourseId,
		-- CCEWCR02CourseControlNumber,
		-- CCEWCR02CourseTopCode,
		-- CCEWCR02CourseTitle,
		-- CCEWCR02CourseMarkLetter,
		-- CCEWCR02CourseMarkPoints,
		-- CCEWCR02CourseLevelCode,
		-- CCEWCR03CollegeCode,
		-- CCEWCR03YearTermCode,
		-- CCEWCR03CourseId,
		-- CCEWCR03CourseControlNumber,
		-- CCEWCR03CourseTopCode,
		-- CCEWCR03CourseTitle,
		-- CCEWCR03CourseMarkLetter,
		-- CCEWCR03CourseMarkPoints,
		-- CCEWCR03CourseLevelCode,
		-- CCEWCR04CollegeCode,
		-- CCEWCR04YearTermCode,
		-- CCEWCR04CourseId,
		-- CCEWCR04CourseControlNumber,
		-- CCEWCR04CourseTopCode,
		-- CCEWCR04CourseTitle,
		-- CCEWCR04CourseMarkLetter,
		-- CCEWCR04CourseMarkPoints,
		-- CCEWCR04CourseLevelCode,
		-- CCEWCR05CollegeCode,
		-- CCEWCR05YearTermCode,
		-- CCEWCR05CourseId,
		-- CCEWCR05CourseControlNumber,
		-- CCEWCR05CourseTopCode,
		-- CCEWCR05CourseTitle,
		-- CCEWCR05CourseMarkLetter,
		-- CCEWCR05CourseMarkPoints,
		-- CCEWCR05CourseLevelCode,
		-- CCEWCR06CollegeCode,
		-- CCEWCR06YearTermCode,
		-- CCEWCR06CourseId,
		-- CCEWCR06CourseControlNumber,
		-- CCEWCR06CourseTopCode,
		-- CCEWCR06CourseTitle,
		-- CCEWCR06CourseMarkLetter,
		-- CCEWCR06CourseMarkPoints,
		-- CCEWCR06CourseLevelCode,
		-- CCERCRFLCollegeCode,
		-- CCERCRFLYearTermCode,
		-- CCERCRFLCourseId,
		-- CCERCRFLCourseControlNumber,
		-- CCERCRFLCourseTopCode,
		-- CCERCRFLCourseTitle,
		-- CCERCRFLCourseMarkLetter,
		-- CCERCRFLCourseMarkPoints,
		-- CCERCRFLCourseLevelCode,
		-- CCERCR00CollegeCode,
		-- CCERCR00YearTermCode,
		-- CCERCR00CourseId,
		-- CCERCR00CourseControlNumber,
		-- CCERCR00CourseTopCode,
		-- CCERCR00CourseTitle,
		-- CCERCR00CourseMarkLetter,
		-- CCERCR00CourseMarkPoints,
		-- CCERCR00CourseLevelCode,
		-- CCERCR01CollegeCode,
		-- CCERCR01YearTermCode,
		-- CCERCR01CourseId,
		-- CCERCR01CourseControlNumber,
		-- CCERCR01CourseTopCode,
		-- CCERCR01CourseTitle,
		-- CCERCR01CourseMarkLetter,
		-- CCERCR01CourseMarkPoints,
		-- CCERCR01CourseLevelCode,
		-- CCERCR02CollegeCode,
		-- CCERCR02YearTermCode,
		-- CCERCR02CourseId,
		-- CCERCR02CourseControlNumber,
		-- CCERCR02CourseTopCode,
		-- CCERCR02CourseTitle,
		-- CCERCR02CourseMarkLetter,
		-- CCERCR02CourseMarkPoints,
		-- CCERCR02CourseLevelCode,
		-- CCERCR03CollegeCode,
		-- CCERCR03YearTermCode,
		-- CCERCR03CourseId,
		-- CCERCR03CourseControlNumber,
		-- CCERCR03CourseTopCode,
		-- CCERCR03CourseTitle,
		-- CCERCR03CourseMarkLetter,
		-- CCERCR03CourseMarkPoints,
		-- CCERCR03CourseLevelCode,
		-- CCERCR04CollegeCode,
		-- CCERCR04YearTermCode,
		-- CCERCR04CourseId,
		-- CCERCR04CourseControlNumber,
		-- CCERCR04CourseTopCode,
		-- CCERCR04CourseTitle,
		-- CCERCR04CourseMarkLetter,
		-- CCERCR04CourseMarkPoints,
		-- CCERCR04CourseLevelCode,
		-- CCERCR05CollegeCode,
		-- CCERCR05YearTermCode,
		-- CCERCR05CourseId,
		-- CCERCR05CourseControlNumber,
		-- CCERCR05CourseTopCode,
		-- CCERCR05CourseTitle,
		-- CCERCR05CourseMarkLetter,
		-- CCERCR05CourseMarkPoints,
		-- CCERCR05CourseLevelCode,
		-- CCERCR06CollegeCode,
		-- CCERCR06YearTermCode,
		-- CCERCR06CourseId,
		-- CCERCR06CourseControlNumber,
		-- CCERCR06CourseTopCode,
		-- CCERCR06CourseTitle,
		-- CCERCR06CourseMarkLetter,
		-- CCERCR06CourseMarkPoints,
		-- CCERCR06CourseLevelCode,
		-- CCESCRFLCollegeCode,
		-- CCESCRFLYearTermCode,
		-- CCESCRFLCourseId,
		-- CCESCRFLCourseControlNumber,
		-- CCESCRFLCourseTopCode,
		-- CCESCRFLCourseTitle,
		-- CCESCRFLCourseMarkLetter,
		-- CCESCRFLCourseMarkPoints,
		-- CCESCRFLCourseLevelCode,
		-- CCESCR00CollegeCode,
		-- CCESCR00YearTermCode,
		-- CCESCR00CourseId,
		-- CCESCR00CourseControlNumber,
		-- CCESCR00CourseTopCode,
		-- CCESCR00CourseTitle,
		-- CCESCR00CourseMarkLetter,
		-- CCESCR00CourseMarkPoints,
		-- CCESCR00CourseLevelCode,
		-- CCESCR01CollegeCode,
		-- CCESCR01YearTermCode,
		-- CCESCR01CourseId,
		-- CCESCR01CourseControlNumber,
		-- CCESCR01CourseTopCode,
		-- CCESCR01CourseTitle,
		-- CCESCR01CourseMarkLetter,
		-- CCESCR01CourseMarkPoints,
		-- CCESCR01CourseLevelCode,
		-- CCESCR02CollegeCode,
		-- CCESCR02YearTermCode,
		-- CCESCR02CourseId,
		-- CCESCR02CourseControlNumber,
		-- CCESCR02CourseTopCode,
		-- CCESCR02CourseTitle,
		-- CCESCR02CourseMarkLetter,
		-- CCESCR02CourseMarkPoints,
		-- CCESCR02CourseLevelCode,
		-- CCESCR03CollegeCode,
		-- CCESCR03YearTermCode,
		-- CCESCR03CourseId,
		-- CCESCR03CourseControlNumber,
		-- CCESCR03CourseTopCode,
		-- CCESCR03CourseTitle,
		-- CCESCR03CourseMarkLetter,
		-- CCESCR03CourseMarkPoints,
		-- CCESCR03CourseLevelCode,
		-- CCESCR04CollegeCode,
		-- CCESCR04YearTermCode,
		-- CCESCR04CourseId,
		-- CCESCR04CourseControlNumber,
		-- CCESCR04CourseTopCode,
		-- CCESCR04CourseTitle,
		-- CCESCR04CourseMarkLetter,
		-- CCESCR04CourseMarkPoints,
		-- CCESCR04CourseLevelCode,
		-- CCESCR05CollegeCode,
		-- CCESCR05YearTermCode,
		-- CCESCR05CourseId,
		-- CCESCR05CourseControlNumber,
		-- CCESCR05CourseTopCode,
		-- CCESCR05CourseTitle,
		-- CCESCR05CourseMarkLetter,
		-- CCESCR05CourseMarkPoints,
		-- CCESCR05CourseLevelCode,
		-- CCESCR06CollegeCode,
		-- CCESCR06YearTermCode,
		-- CCESCR06CourseId,
		-- CCESCR06CourseControlNumber,
		-- CCESCR06CourseTopCode,
		-- CCESCR06CourseTitle,
		-- CCESCR06CourseMarkLetter,
		-- CCESCR06CourseMarkPoints,
		-- CCESCR06CourseLevelCode,
		-- CCEICRFLCollegeCode,
		-- CCEICRFLYearTermCode,
		-- CCEICRFLCourseId,
		-- CCEICRFLCourseControlNumber,
		-- CCEICRFLCourseTopCode,
		-- CCEICRFLCourseTitle,
		-- CCEICRFLCourseMarkLetter,
		-- CCEICRFLCourseMarkPoints,
		-- CCEICRFLCourseLevelCode,
		-- CCEICR00CollegeCode,
		-- CCEICR00YearTermCode,
		-- CCEICR00CourseId,
		-- CCEICR00CourseControlNumber,
		-- CCEICR00CourseTopCode,
		-- CCEICR00CourseTitle,
		-- CCEICR00CourseMarkLetter,
		-- CCEICR00CourseMarkPoints,
		-- CCEICR00CourseLevelCode,
		-- CCEICR01CollegeCode,
		-- CCEICR01YearTermCode,
		-- CCEICR01CourseId,
		-- CCEICR01CourseControlNumber,
		-- CCEICR01CourseTopCode,
		-- CCEICR01CourseTitle,
		-- CCEICR01CourseMarkLetter,
		-- CCEICR01CourseMarkPoints,
		-- CCEICR01CourseLevelCode,
		-- CCEICR02CollegeCode,
		-- CCEICR02YearTermCode,
		-- CCEICR02CourseId,
		-- CCEICR02CourseControlNumber,
		-- CCEICR02CourseTopCode,
		-- CCEICR02CourseTitle,
		-- CCEICR02CourseMarkLetter,
		-- CCEICR02CourseMarkPoints,
		-- CCEICR02CourseLevelCode,
		-- CCEICR03CollegeCode,
		-- CCEICR03YearTermCode,
		-- CCEICR03CourseId,
		-- CCEICR03CourseControlNumber,
		-- CCEICR03CourseTopCode,
		-- CCEICR03CourseTitle,
		-- CCEICR03CourseMarkLetter,
		-- CCEICR03CourseMarkPoints,
		-- CCEICR03CourseLevelCode,
		-- CCEICR04CollegeCode,
		-- CCEICR04YearTermCode,
		-- CCEICR04CourseId,
		-- CCEICR04CourseControlNumber,
		-- CCEICR04CourseTopCode,
		-- CCEICR04CourseTitle,
		-- CCEICR04CourseMarkLetter,
		-- CCEICR04CourseMarkPoints,
		-- CCEICR04CourseLevelCode,
		-- CCEICR05CollegeCode,
		-- CCEICR05YearTermCode,
		-- CCEICR05CourseId,
		-- CCEICR05CourseControlNumber,
		-- CCEICR05CourseTopCode,
		-- CCEICR05CourseTitle,
		-- CCEICR05CourseMarkLetter,
		-- CCEICR05CourseMarkPoints,
		-- CCEICR05CourseLevelCode,
		-- CCEICR06CollegeCode,
		-- CCEICR06YearTermCode,
		-- CCEICR06CourseId,
		-- CCEICR06CourseControlNumber,
		-- CCEICR06CourseTopCode,
		-- CCEICR06CourseTitle,
		-- CCEICR06CourseMarkLetter,
		-- CCEICR06CourseMarkPoints,
		-- CCEICR06CourseLevelCode,
		-- CCEWNCFLCollegeCode,
		-- CCEWNCFLYearTermCode,
		-- CCEWNCFLCourseId,
		-- CCEWNCFLCourseControlNumber,
		-- CCEWNCFLCourseTopCode,
		-- CCEWNCFLCourseTitle,
		-- CCEWNCFLCourseMarkLetter,
		-- CCEWNCFLCourseMarkPoints,
		-- CCEWNCFLCourseLevelCode,
		-- CCEWNC01CollegeCode,
		-- CCEWNC01YearTermCode,
		-- CCEWNC01CourseId,
		-- CCEWNC01CourseControlNumber,
		-- CCEWNC01CourseTopCode,
		-- CCEWNC01CourseTitle,
		-- CCEWNC01CourseMarkLetter,
		-- CCEWNC01CourseMarkPoints,
		-- CCEWNC01CourseLevelCode,
		-- CCEWNC02CollegeCode,
		-- CCEWNC02YearTermCode,
		-- CCEWNC02CourseId,
		-- CCEWNC02CourseControlNumber,
		-- CCEWNC02CourseTopCode,
		-- CCEWNC02CourseTitle,
		-- CCEWNC02CourseMarkLetter,
		-- CCEWNC02CourseMarkPoints,
		-- CCEWNC02CourseLevelCode,
		-- CCEWNC03CollegeCode,
		-- CCEWNC03YearTermCode,
		-- CCEWNC03CourseId,
		-- CCEWNC03CourseControlNumber,
		-- CCEWNC03CourseTopCode,
		-- CCEWNC03CourseTitle,
		-- CCEWNC03CourseMarkLetter,
		-- CCEWNC03CourseMarkPoints,
		-- CCEWNC03CourseLevelCode,
		-- CCEWNC04CollegeCode,
		-- CCEWNC04YearTermCode,
		-- CCEWNC04CourseId,
		-- CCEWNC04CourseControlNumber,
		-- CCEWNC04CourseTopCode,
		-- CCEWNC04CourseTitle,
		-- CCEWNC04CourseMarkLetter,
		-- CCEWNC04CourseMarkPoints,
		-- CCEWNC04CourseLevelCode,
		-- CCEWNC05CollegeCode,
		-- CCEWNC05YearTermCode,
		-- CCEWNC05CourseId,
		-- CCEWNC05CourseControlNumber,
		-- CCEWNC05CourseTopCode,
		-- CCEWNC05CourseTitle,
		-- CCEWNC05CourseMarkLetter,
		-- CCEWNC05CourseMarkPoints,
		-- CCEWNC05CourseLevelCode,
		-- CCEWNC06CollegeCode,
		-- CCEWNC06YearTermCode,
		-- CCEWNC06CourseId,
		-- CCEWNC06CourseControlNumber,
		-- CCEWNC06CourseTopCode,
		-- CCEWNC06CourseTitle,
		-- CCEWNC06CourseMarkLetter,
		-- CCEWNC06CourseMarkPoints,
		-- CCEWNC06CourseLevelCode,
		-- CCERNCFLCollegeCode,
		-- CCERNCFLYearTermCode,
		-- CCERNCFLCourseId,
		-- CCERNCFLCourseControlNumber,
		-- CCERNCFLCourseTopCode,
		-- CCERNCFLCourseTitle,
		-- CCERNCFLCourseMarkLetter,
		-- CCERNCFLCourseMarkPoints,
		-- CCERNCFLCourseLevelCode,
		-- CCERNC01CollegeCode,
		-- CCERNC01YearTermCode,
		-- CCERNC01CourseId,
		-- CCERNC01CourseControlNumber,
		-- CCERNC01CourseTopCode,
		-- CCERNC01CourseTitle,
		-- CCERNC01CourseMarkLetter,
		-- CCERNC01CourseMarkPoints,
		-- CCERNC01CourseLevelCode,
		-- CCERNC02CollegeCode,
		-- CCERNC02YearTermCode,
		-- CCERNC02CourseId,
		-- CCERNC02CourseControlNumber,
		-- CCERNC02CourseTopCode,
		-- CCERNC02CourseTitle,
		-- CCERNC02CourseMarkLetter,
		-- CCERNC02CourseMarkPoints,
		-- CCERNC02CourseLevelCode,
		-- CCERNC03CollegeCode,
		-- CCERNC03YearTermCode,
		-- CCERNC03CourseId,
		-- CCERNC03CourseControlNumber,
		-- CCERNC03CourseTopCode,
		-- CCERNC03CourseTitle,
		-- CCERNC03CourseMarkLetter,
		-- CCERNC03CourseMarkPoints,
		-- CCERNC03CourseLevelCode,
		-- CCERNC04CollegeCode,
		-- CCERNC04YearTermCode,
		-- CCERNC04CourseId,
		-- CCERNC04CourseControlNumber,
		-- CCERNC04CourseTopCode,
		-- CCERNC04CourseTitle,
		-- CCERNC04CourseMarkLetter,
		-- CCERNC04CourseMarkPoints,
		-- CCERNC04CourseLevelCode,
		-- CCERNC05CollegeCode,
		-- CCERNC05YearTermCode,
		-- CCERNC05CourseId,
		-- CCERNC05CourseControlNumber,
		-- CCERNC05CourseTopCode,
		-- CCERNC05CourseTitle,
		-- CCERNC05CourseMarkLetter,
		-- CCERNC05CourseMarkPoints,
		-- CCERNC05CourseLevelCode,
		-- CCERNC06CollegeCode,
		-- CCERNC06YearTermCode,
		-- CCERNC06CourseId,
		-- CCERNC06CourseControlNumber,
		-- CCERNC06CourseTopCode,
		-- CCERNC06CourseTitle,
		-- CCERNC06CourseMarkLetter,
		-- CCERNC06CourseMarkPoints,
		-- CCERNC06CourseLevelCode,
		-- CCESNCFLCollegeCode,
		-- CCESNCFLYearTermCode,
		-- CCESNCFLCourseId,
		-- CCESNCFLCourseControlNumber,
		-- CCESNCFLCourseTopCode,
		-- CCESNCFLCourseTitle,
		-- CCESNCFLCourseMarkLetter,
		-- CCESNCFLCourseMarkPoints,
		-- CCESNCFLCourseLevelCode,
		-- CCESNC01CollegeCode,
		-- CCESNC01YearTermCode,
		-- CCESNC01CourseId,
		-- CCESNC01CourseControlNumber,
		-- CCESNC01CourseTopCode,
		-- CCESNC01CourseTitle,
		-- CCESNC01CourseMarkLetter,
		-- CCESNC01CourseMarkPoints,
		-- CCESNC01CourseLevelCode,
		-- CCESNC02CollegeCode,
		-- CCESNC02YearTermCode,
		-- CCESNC02CourseId,
		-- CCESNC02CourseControlNumber,
		-- CCESNC02CourseTopCode,
		-- CCESNC02CourseTitle,
		-- CCESNC02CourseMarkLetter,
		-- CCESNC02CourseMarkPoints,
		-- CCESNC02CourseLevelCode,
		-- CCESNC03CollegeCode,
		-- CCESNC03YearTermCode,
		-- CCESNC03CourseId,
		-- CCESNC03CourseControlNumber,
		-- CCESNC03CourseTopCode,
		-- CCESNC03CourseTitle,
		-- CCESNC03CourseMarkLetter,
		-- CCESNC03CourseMarkPoints,
		-- CCESNC03CourseLevelCode,
		-- CCESNC04CollegeCode,
		-- CCESNC04YearTermCode,
		-- CCESNC04CourseId,
		-- CCESNC04CourseControlNumber,
		-- CCESNC04CourseTopCode,
		-- CCESNC04CourseTitle,
		-- CCESNC04CourseMarkLetter,
		-- CCESNC04CourseMarkPoints,
		-- CCESNC04CourseLevelCode,
		-- CCESNC05CollegeCode,
		-- CCESNC05YearTermCode,
		-- CCESNC05CourseId,
		-- CCESNC05CourseControlNumber,
		-- CCESNC05CourseTopCode,
		-- CCESNC05CourseTitle,
		-- CCESNC05CourseMarkLetter,
		-- CCESNC05CourseMarkPoints,
		-- CCESNC05CourseLevelCode,
		-- CCESNC06CollegeCode,
		-- CCESNC06YearTermCode,
		-- CCESNC06CourseId,
		-- CCESNC06CourseControlNumber,
		-- CCESNC06CourseTopCode,
		-- CCESNC06CourseTitle,
		-- CCESNC06CourseMarkLetter,
		-- CCESNC06CourseMarkPoints,
		-- CCESNC06CourseLevelCode,
		-- CCEINCFLCollegeCode,
		-- CCEINCFLYearTermCode,
		-- CCEINCFLCourseId,
		-- CCEINCFLCourseControlNumber,
		-- CCEINCFLCourseTopCode,
		-- CCEINCFLCourseTitle,
		-- CCEINCFLCourseMarkLetter,
		-- CCEINCFLCourseMarkPoints,
		-- CCEINCFLCourseLevelCode,
		-- CCEINC01CollegeCode,
		-- CCEINC01YearTermCode,
		-- CCEINC01CourseId,
		-- CCEINC01CourseControlNumber,
		-- CCEINC01CourseTopCode,
		-- CCEINC01CourseTitle,
		-- CCEINC01CourseMarkLetter,
		-- CCEINC01CourseMarkPoints,
		-- CCEINC01CourseLevelCode,
		-- CCEINC02CollegeCode,
		-- CCEINC02YearTermCode,
		-- CCEINC02CourseId,
		-- CCEINC02CourseControlNumber,
		-- CCEINC02CourseTopCode,
		-- CCEINC02CourseTitle,
		-- CCEINC02CourseMarkLetter,
		-- CCEINC02CourseMarkPoints,
		-- CCEINC02CourseLevelCode,
		-- CCEINC03CollegeCode,
		-- CCEINC03YearTermCode,
		-- CCEINC03CourseId,
		-- CCEINC03CourseControlNumber,
		-- CCEINC03CourseTopCode,
		-- CCEINC03CourseTitle,
		-- CCEINC03CourseMarkLetter,
		-- CCEINC03CourseMarkPoints,
		-- CCEINC03CourseLevelCode,
		-- CCEINC04CollegeCode,
		-- CCEINC04YearTermCode,
		-- CCEINC04CourseId,
		-- CCEINC04CourseControlNumber,
		-- CCEINC04CourseTopCode,
		-- CCEINC04CourseTitle,
		-- CCEINC04CourseMarkLetter,
		-- CCEINC04CourseMarkPoints,
		-- CCEINC04CourseLevelCode,
		-- CCEINC05CollegeCode,
		-- CCEINC05YearTermCode,
		-- CCEINC05CourseId,
		-- CCEINC05CourseControlNumber,
		-- CCEINC05CourseTopCode,
		-- CCEINC05CourseTitle,
		-- CCEINC05CourseMarkLetter,
		-- CCEINC05CourseMarkPoints,
		-- CCEINC05CourseLevelCode,
		-- CCEINC06CollegeCode,
		-- CCEINC06YearTermCode,
		-- CCEINC06CourseId,
		-- CCEINC06CourseControlNumber,
		-- CCEINC06CourseTopCode,
		-- CCEINC06CourseTitle,
		-- CCEINC06CourseMarkLetter,
		-- CCEINC06CourseMarkPoints,
		-- CCEINC06CourseLevelCode,
		-- CCEINC07CollegeCode,
		-- CCEINC07YearTermCode,
		-- CCEINC07CourseId,
		-- CCEINC07CourseControlNumber,
		-- CCEINC07CourseTopCode,
		-- CCEINC07CourseTitle,
		-- CCEINC07CourseMarkLetter,
		-- CCEINC07CourseMarkPoints,
		-- CCEINC07CourseLevelCode,
		-- CCEINC08CollegeCode,
		-- CCEINC08YearTermCode,
		-- CCEINC08CourseId,
		-- CCEINC08CourseControlNumber,
		-- CCEINC08CourseTopCode,
		-- CCEINC08CourseTitle,
		-- CCEINC08CourseMarkLetter,
		-- CCEINC08CourseMarkPoints,
		-- CCEINC08CourseLevelCode,
		-- CCECNCFLCollegeCode,
		-- CCECNCFLYearTermCode,
		-- CCECNCFLCourseId,
		-- CCECNCFLCourseControlNumber,
		-- CCECNCFLCourseTopCode,
		-- CCECNCFLCourseTitle,
		-- CCECNCFLCourseMarkLetter,
		-- CCECNCFLCourseMarkPoints,
		-- CCECNCFLCourseLevelCode,
		-- CCECNC00CollegeCode,
		-- CCECNC00YearTermCode,
		-- CCECNC00CourseId,
		-- CCECNC00CourseControlNumber,
		-- CCECNC00CourseTopCode,
		-- CCECNC00CourseTitle,
		-- CCECNC00CourseMarkLetter,
		-- CCECNC00CourseMarkPoints,
		-- CCECNC00CourseLevelCode,
		-- CCECNC01CollegeCode,
		-- CCECNC01YearTermCode,
		-- CCECNC01CourseId,
		-- CCECNC01CourseControlNumber,
		-- CCECNC01CourseTopCode,
		-- CCECNC01CourseTitle,
		-- CCECNC01CourseMarkLetter,
		-- CCECNC01CourseMarkPoints,
		-- CCECNC01CourseLevelCode,
		-- CCECNC02CollegeCode,
		-- CCECNC02YearTermCode,
		-- CCECNC02CourseId,
		-- CCECNC02CourseControlNumber,
		-- CCECNC02CourseTopCode,
		-- CCECNC02CourseTitle,
		-- CCECNC02CourseMarkLetter,
		-- CCECNC02CourseMarkPoints,
		-- CCECNC02CourseLevelCode,
		-- CCECNC03CollegeCode,
		-- CCECNC03YearTermCode,
		-- CCECNC03CourseId,
		-- CCECNC03CourseControlNumber,
		-- CCECNC03CourseTopCode,
		-- CCECNC03CourseTitle,
		-- CCECNC03CourseMarkLetter,
		-- CCECNC03CourseMarkPoints,
		-- CCECNC03CourseLevelCode,
		-- CCECNC04CollegeCode,
		-- CCECNC04YearTermCode,
		-- CCECNC04CourseId,
		-- CCECNC04CourseControlNumber,
		-- CCECNC04CourseTopCode,
		-- CCECNC04CourseTitle,
		-- CCECNC04CourseMarkLetter,
		-- CCECNC04CourseMarkPoints,
		-- CCECNC04CourseLevelCode,
		-- CCECNC05CollegeCode,
		-- CCECNC05YearTermCode,
		-- CCECNC05CourseId,
		-- CCECNC05CourseControlNumber,
		-- CCECNC05CourseTopCode,
		-- CCECNC05CourseTitle,
		-- CCECNC05CourseMarkLetter,
		-- CCECNC05CourseMarkPoints,
		-- CCECNC05CourseLevelCode,
		-- CCECNC06CollegeCode,
		-- CCECNC06YearTermCode,
		-- CCECNC06CourseId,
		-- CCECNC06CourseControlNumber,
		-- CCECNC06CourseTopCode,
		-- CCECNC06CourseTitle,
		-- CCECNC06CourseMarkLetter,
		-- CCECNC06CourseMarkPoints,
		-- CCECNC06CourseLevelCode,
		-- CCECNC07CollegeCode,
		-- CCECNC07YearTermCode,
		-- CCECNC07CourseId,
		-- CCECNC07CourseControlNumber,
		-- CCECNC07CourseTopCode,
		-- CCECNC07CourseTitle,
		-- CCECNC07CourseMarkLetter,
		-- CCECNC07CourseMarkPoints,
		-- CCECNC07CourseLevelCode,
		-- CCECNC08CollegeCode,
		-- CCECNC08YearTermCode,
		-- CCECNC08CourseId,
		-- CCECNC08CourseControlNumber,
		-- CCECNC08CourseTopCode,
		-- CCECNC08CourseTitle,
		-- CCECNC08CourseMarkLetter,
		-- CCECNC08CourseMarkPoints,
		-- CCECNC08CourseLevelCode,
		-- CCEVNCFLCollegeCode,
		-- CCEVNCFLYearTermCode,
		-- CCEVNCFLCourseId,
		-- CCEVNCFLCourseControlNumber,
		-- CCEVNCFLCourseTopCode,
		-- CCEVNCFLCourseTitle,
		-- CCEVNCFLCourseMarkLetter,
		-- CCEVNCFLCourseMarkPoints,
		-- CCEVNCFLCourseLevelCode,
		-- CCEVNC00CollegeCode,
		-- CCEVNC00YearTermCode,
		-- CCEVNC00CourseId,
		-- CCEVNC00CourseControlNumber,
		-- CCEVNC00CourseTopCode,
		-- CCEVNC00CourseTitle,
		-- CCEVNC00CourseMarkLetter,
		-- CCEVNC00CourseMarkPoints,
		-- CCEVNC00CourseLevelCode,
		-- CCEVNC01CollegeCode,
		-- CCEVNC01YearTermCode,
		-- CCEVNC01CourseId,
		-- CCEVNC01CourseControlNumber,
		-- CCEVNC01CourseTopCode,
		-- CCEVNC01CourseTitle,
		-- CCEVNC01CourseMarkLetter,
		-- CCEVNC01CourseMarkPoints,
		-- CCEVNC01CourseLevelCode,
		-- CCEVNC02CollegeCode,
		-- CCEVNC02YearTermCode,
		-- CCEVNC02CourseId,
		-- CCEVNC02CourseControlNumber,
		-- CCEVNC02CourseTopCode,
		-- CCEVNC02CourseTitle,
		-- CCEVNC02CourseMarkLetter,
		-- CCEVNC02CourseMarkPoints,
		-- CCEVNC02CourseLevelCode,
		-- CCEVNC03CollegeCode,
		-- CCEVNC03YearTermCode,
		-- CCEVNC03CourseId,
		-- CCEVNC03CourseControlNumber,
		-- CCEVNC03CourseTopCode,
		-- CCEVNC03CourseTitle,
		-- CCEVNC03CourseMarkLetter,
		-- CCEVNC03CourseMarkPoints,
		-- CCEVNC03CourseLevelCode,
		-- CCEVNC04CollegeCode,
		-- CCEVNC04YearTermCode,
		-- CCEVNC04CourseId,
		-- CCEVNC04CourseControlNumber,
		-- CCEVNC04CourseTopCode,
		-- CCEVNC04CourseTitle,
		-- CCEVNC04CourseMarkLetter,
		-- CCEVNC04CourseMarkPoints,
		-- CCEVNC04CourseLevelCode,
		-- CCEVNC05CollegeCode,
		-- CCEVNC05YearTermCode,
		-- CCEVNC05CourseId,
		-- CCEVNC05CourseControlNumber,
		-- CCEVNC05CourseTopCode,
		-- CCEVNC05CourseTitle,
		-- CCEVNC05CourseMarkLetter,
		-- CCEVNC05CourseMarkPoints,
		-- CCEVNC05CourseLevelCode,
		-- CCEVNC06CollegeCode,
		-- CCEVNC06YearTermCode,
		-- CCEVNC06CourseId,
		-- CCEVNC06CourseControlNumber,
		-- CCEVNC06CourseTopCode,
		-- CCEVNC06CourseTitle,
		-- CCEVNC06CourseMarkLetter,
		-- CCEVNC06CourseMarkPoints,
		-- CCEVNC06CourseLevelCode,

		CCENCRFLCollegeCode,
		CCENCRFLYearTermCode,
		CCENCRFLCourseId,
		CCENCRFLCourseControlNumber,
		CCENCRFLCourseTopCode,
		CCENCRFLCourseTitle,
		CCENCRFLCourseMarkLetter,
		CCENCRFLCourseMarkPoints,
		CCENCRFLCourseLevelCode,
		CCENCR00CollegeCode,
		CCENCR00YearTermCode,
		CCENCR00CourseId,
		CCENCR00CourseControlNumber,
		CCENCR00CourseTopCode,
		CCENCR00CourseTitle,
		CCENCR00CourseMarkLetter,
		CCENCR00CourseMarkPoints,
		CCENCR00CourseLevelCode,
	CCENCR002NCollegeCode,
	CCENCR002NYearTermCode,
	CCENCR002NCourseId,
	CCENCR002NCourseControlNumber,
	CCENCR002NCourseTopCode,
	CCENCR002NCourseTitle,
	CCENCR002NCourseMarkLetter,
	CCENCR002NCourseMarkPoints,
	CCENCR002NCourseLevelCode,
		CCENCR01CollegeCode,
		CCENCR01YearTermCode,
		CCENCR01CourseId,
		CCENCR01CourseControlNumber,
		CCENCR01CourseTopCode,
		CCENCR01CourseTitle,
		CCENCR01CourseMarkLetter,
		CCENCR01CourseMarkPoints,
		CCENCR01CourseLevelCode,
		CCENCR02CollegeCode,
		CCENCR02YearTermCode,
		CCENCR02CourseId,
		CCENCR02CourseControlNumber,
		CCENCR02CourseTopCode,
		CCENCR02CourseTitle,
		CCENCR02CourseMarkLetter,
		CCENCR02CourseMarkPoints,
		CCENCR02CourseLevelCode,
		CCENCR03CollegeCode,
		CCENCR03YearTermCode,
		CCENCR03CourseId,
		CCENCR03CourseControlNumber,
		CCENCR03CourseTopCode,
		CCENCR03CourseTitle,
		CCENCR03CourseMarkLetter,
		CCENCR03CourseMarkPoints,
		CCENCR03CourseLevelCode,
		CCENCR04CollegeCode,
		CCENCR04YearTermCode,
		CCENCR04CourseId,
		CCENCR04CourseControlNumber,
		CCENCR04CourseTopCode,
		CCENCR04CourseTitle,
		CCENCR04CourseMarkLetter,
		CCENCR04CourseMarkPoints,
		CCENCR04CourseLevelCode,
		CCRDCRFLCollegeCode,
		CCRDCRFLYearTermCode,
		CCRDCRFLCourseId,
		CCRDCRFLCourseControlNumber,
		CCRDCRFLCourseTopCode,
		CCRDCRFLCourseTitle,
		CCRDCRFLCourseMarkLetter,
		CCRDCRFLCourseMarkPoints,
		CCRDCRFLCourseLevelCode,
		CCRDCR00CollegeCode,
		CCRDCR00YearTermCode,
		CCRDCR00CourseId,
		CCRDCR00CourseControlNumber,
		CCRDCR00CourseTopCode,
		CCRDCR00CourseTitle,
		CCRDCR00CourseMarkLetter,
		CCRDCR00CourseMarkPoints,
		CCRDCR00CourseLevelCode,
		CCRDCR01CollegeCode,
		CCRDCR01YearTermCode,
		CCRDCR01CourseId,
		CCRDCR01CourseControlNumber,
		CCRDCR01CourseTopCode,
		CCRDCR01CourseTitle,
		CCRDCR01CourseMarkLetter,
		CCRDCR01CourseMarkPoints,
		CCRDCR01CourseLevelCode,
		CCRDCR02CollegeCode,
		CCRDCR02YearTermCode,
		CCRDCR02CourseId,
		CCRDCR02CourseControlNumber,
		CCRDCR02CourseTopCode,
		CCRDCR02CourseTitle,
		CCRDCR02CourseMarkLetter,
		CCRDCR02CourseMarkPoints,
		CCRDCR02CourseLevelCode,
		CCRDCR03CollegeCode,
		CCRDCR03YearTermCode,
		CCRDCR03CourseId,
		CCRDCR03CourseControlNumber,
		CCRDCR03CourseTopCode,
		CCRDCR03CourseTitle,
		CCRDCR03CourseMarkLetter,
		CCRDCR03CourseMarkPoints,
		CCRDCR03CourseLevelCode,
		CCRDCR04CollegeCode,
		CCRDCR04YearTermCode,
		CCRDCR04CourseId,
		CCRDCR04CourseControlNumber,
		CCRDCR04CourseTopCode,
		CCRDCR04CourseTitle,
		CCRDCR04CourseMarkLetter,
		CCRDCR04CourseMarkPoints,
		CCRDCR04CourseLevelCode
	)
SELECT
	InterSegmentKey             = t.InterSegmentKey,
	CCEACNFLCollegeCode         = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CollegeCode         end),
	CCEACNFLYearTermCode        = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then YearTermCode        end),
	CCEACNFLCourseId            = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CourseId            end),
	CCEACNFLCourseControlNumber = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CourseControlNumber end),
	CCEACNFLCourseTopCode       = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CourseTopCode       end),
	CCEACNFLCourseTitle         = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CourseTitle         end),
	CCEACNFLCourseMarkLetter    = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CourseMarkLetter    end),
	CCEACNFLCourseMarkPoints    = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CourseMarkPoints    end),
	CCEACNFLCourseLevelCode     = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CourseLevelCode     end),
	CCEACNFLCourseCreditCode    = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then c.Type              end),
	CCEACN00CollegeCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CollegeCode         end),
	CCEACN00YearTermCode        = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then YearTermCode        end),
	CCEACN00CourseId            = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CourseId            end),
	CCEACN00CourseControlNumber = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CourseControlNumber end),
	CCEACN00CourseTopCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CourseTopCode       end),
	CCEACN00CourseTitle         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CourseTitle         end),
	CCEACN00CourseMarkLetter    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CourseMarkLetter    end),
	CCEACN00CourseMarkPoints    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CourseMarkPoints    end),
	CCEACN00CourseLevelCode     = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CourseLevelCode     end),
	CCEACN00CourseCreditCode    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then c.Type              end),
	CCEACN01CollegeCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CollegeCode         end),
	CCEACN01YearTermCode        = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then YearTermCode        end),
	CCEACN01CourseId            = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CourseId            end),
	CCEACN01CourseControlNumber = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CourseControlNumber end),
	CCEACN01CourseTopCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CourseTopCode       end),
	CCEACN01CourseTitle         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CourseTitle         end),
	CCEACN01CourseMarkLetter    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CourseMarkLetter    end),
	CCEACN01CourseMarkPoints    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CourseMarkPoints    end),
	CCEACN01CourseLevelCode     = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CourseLevelCode     end),
	CCEACN01CourseCreditCode    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then c.Type              end),
	CCEACN02CollegeCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CollegeCode         end),
	CCEACN02YearTermCode        = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then YearTermCode        end),
	CCEACN02CourseId            = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CourseId            end),
	CCEACN02CourseControlNumber = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CourseControlNumber end),
	CCEACN02CourseTopCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CourseTopCode       end),
	CCEACN02CourseTitle         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CourseTitle         end),
	CCEACN02CourseMarkLetter    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CourseMarkLetter    end),
	CCEACN02CourseMarkPoints    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CourseMarkPoints    end),
	CCEACN02CourseLevelCode     = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CourseLevelCode     end),
	CCEACN02CourseCreditCode    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then c.Type              end),
	CCEACN03CollegeCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CollegeCode         end),
	CCEACN03YearTermCode        = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then YearTermCode        end),
	CCEACN03CourseId            = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CourseId            end),
	CCEACN03CourseControlNumber = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CourseControlNumber end),
	CCEACN03CourseTopCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CourseTopCode       end),
	CCEACN03CourseTitle         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CourseTitle         end),
	CCEACN03CourseMarkLetter    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CourseMarkLetter    end),
	CCEACN03CourseMarkPoints    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CourseMarkPoints    end),
	CCEACN03CourseLevelCode     = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CourseLevelCode     end),
	CCEACN03CourseCreditCode    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then c.Type              end),
	CCEACN04CollegeCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CollegeCode         end),
	CCEACN04YearTermCode        = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then YearTermCode        end),
	CCEACN04CourseId            = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CourseId            end),
	CCEACN04CourseControlNumber = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CourseControlNumber end),
	CCEACN04CourseTopCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CourseTopCode       end),
	CCEACN04CourseTitle         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CourseTitle         end),
	CCEACN04CourseMarkLetter    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CourseMarkLetter    end),
	CCEACN04CourseMarkPoints    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CourseMarkPoints    end),
	CCEACN04CourseLevelCode     = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CourseLevelCode     end),
	CCEACN04CourseCreditCode    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then c.Type              end),
	CCEACN05CollegeCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CollegeCode         end),
	CCEACN05YearTermCode        = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then YearTermCode        end),
	CCEACN05CourseId            = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CourseId            end),
	CCEACN05CourseControlNumber = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CourseControlNumber end),
	CCEACN05CourseTopCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CourseTopCode       end),
	CCEACN05CourseTitle         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CourseTitle         end),
	CCEACN05CourseMarkLetter    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CourseMarkLetter    end),
	CCEACN05CourseMarkPoints    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CourseMarkPoints    end),
	CCEACN05CourseLevelCode     = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CourseLevelCode     end),
	CCEACN05CourseCreditCode    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then c.Type              end),
	CCEACN06CollegeCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CollegeCode         end),
	CCEACN06YearTermCode        = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then YearTermCode        end),
	CCEACN06CourseId            = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CourseId            end),
	CCEACN06CourseControlNumber = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CourseControlNumber end),
	CCEACN06CourseTopCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CourseTopCode       end),
	CCEACN06CourseTitle         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CourseTitle         end),
	CCEACN06CourseMarkLetter    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CourseMarkLetter    end),
	CCEACN06CourseMarkPoints    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CourseMarkPoints    end),
	CCEACN06CourseLevelCode     = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CourseLevelCode     end),
	CCEACN06CourseCreditCode    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then c.Type              end),
	CCEACN07CollegeCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CollegeCode         end),
	CCEACN07YearTermCode        = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then YearTermCode        end),
	CCEACN07CourseId            = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CourseId            end),
	CCEACN07CourseControlNumber = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CourseControlNumber end),
	CCEACN07CourseTopCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CourseTopCode       end),
	CCEACN07CourseTitle         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CourseTitle         end),
	CCEACN07CourseMarkLetter    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CourseMarkLetter    end),
	CCEACN07CourseMarkPoints    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CourseMarkPoints    end),
	CCEACN07CourseLevelCode     = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CourseLevelCode     end),
	CCEACN07CourseCreditCode    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then c.Type              end),
	CCEACN08CollegeCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CollegeCode         end),
	CCEACN08YearTermCode        = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then YearTermCode        end),
	CCEACN08CourseId            = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CourseId            end),
	CCEACN08CourseControlNumber = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CourseControlNumber end),
	CCEACN08CourseTopCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CourseTopCode       end),
	CCEACN08CourseTitle         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CourseTitle         end),
	CCEACN08CourseMarkLetter    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CourseMarkLetter    end),
	CCEACN08CourseMarkPoints    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CourseMarkPoints    end),
	CCEACN08CourseLevelCode     = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CourseLevelCode     end),
	CCEACN08CourseCreditCode    = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then c.Type              end),
	-- CCEWCRFLCollegeCode         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.SelectorFL = 1 then CollegeCode         end),
	-- CCEWCRFLYearTermCode        = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.SelectorFL = 1 then YearTermCode        end),
	-- CCEWCRFLCourseId            = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.SelectorFL = 1 then CourseId            end),
	-- CCEWCRFLCourseControlNumber = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.SelectorFL = 1 then CourseControlNumber end),
	-- CCEWCRFLCourseTopCode       = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTopCode       end),
	-- CCEWCRFLCourseTitle         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTitle         end),
	-- CCEWCRFLCourseMarkLetter    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkLetter    end),
	-- CCEWCRFLCourseMarkPoints    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkPoints    end),
	-- CCEWCRFLCourseLevelCode     = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.SelectorFL = 1 then CourseLevelCode     end),
	-- CCEWCR00CollegeCode         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CollegeCode         end),
	-- CCEWCR00YearTermCode        = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then YearTermCode        end),
	-- CCEWCR00CourseId            = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseId            end),
	-- CCEWCR00CourseControlNumber = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEWCR00CourseTopCode       = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEWCR00CourseTitle         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTitle         end),
	-- CCEWCR00CourseMarkLetter    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEWCR00CourseMarkPoints    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEWCR00CourseLevelCode     = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEWCR01CollegeCode         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CollegeCode         end),
	-- CCEWCR01YearTermCode        = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then YearTermCode        end),
	-- CCEWCR01CourseId            = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseId            end),
	-- CCEWCR01CourseControlNumber = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEWCR01CourseTopCode       = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEWCR01CourseTitle         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTitle         end),
	-- CCEWCR01CourseMarkLetter    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEWCR01CourseMarkPoints    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEWCR01CourseLevelCode     = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEWCR02CollegeCode         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CollegeCode         end),
	-- CCEWCR02YearTermCode        = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then YearTermCode        end),
	-- CCEWCR02CourseId            = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseId            end),
	-- CCEWCR02CourseControlNumber = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEWCR02CourseTopCode       = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEWCR02CourseTitle         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTitle         end),
	-- CCEWCR02CourseMarkLetter    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEWCR02CourseMarkPoints    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEWCR02CourseLevelCode     = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEWCR03CollegeCode         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CollegeCode         end),
	-- CCEWCR03YearTermCode        = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then YearTermCode        end),
	-- CCEWCR03CourseId            = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseId            end),
	-- CCEWCR03CourseControlNumber = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEWCR03CourseTopCode       = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEWCR03CourseTitle         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTitle         end),
	-- CCEWCR03CourseMarkLetter    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEWCR03CourseMarkPoints    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEWCR03CourseLevelCode     = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEWCR04CollegeCode         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CollegeCode         end),
	-- CCEWCR04YearTermCode        = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then YearTermCode        end),
	-- CCEWCR04CourseId            = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseId            end),
	-- CCEWCR04CourseControlNumber = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEWCR04CourseTopCode       = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEWCR04CourseTitle         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTitle         end),
	-- CCEWCR04CourseMarkLetter    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEWCR04CourseMarkPoints    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEWCR04CourseLevelCode     = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEWCR05CollegeCode         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CollegeCode         end),
	-- CCEWCR05YearTermCode        = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then YearTermCode        end),
	-- CCEWCR05CourseId            = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseId            end),
	-- CCEWCR05CourseControlNumber = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEWCR05CourseTopCode       = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEWCR05CourseTitle         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTitle         end),
	-- CCEWCR05CourseMarkLetter    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEWCR05CourseMarkPoints    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEWCR05CourseLevelCode     = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEWCR06CollegeCode         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CollegeCode         end),
	-- CCEWCR06YearTermCode        = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then YearTermCode        end),
	-- CCEWCR06CourseId            = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseId            end),
	-- CCEWCR06CourseControlNumber = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEWCR06CourseTopCode       = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEWCR06CourseTitle         = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTitle         end),
	-- CCEWCR06CourseMarkLetter    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEWCR06CourseMarkPoints    = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEWCR06CourseLevelCode     = max(case when t.CourseTopCode = '493084' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCERCRFLCollegeCode         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.SelectorFL = 1 then CollegeCode         end),
	-- CCERCRFLYearTermCode        = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.SelectorFL = 1 then YearTermCode        end),
	-- CCERCRFLCourseId            = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.SelectorFL = 1 then CourseId            end),
	-- CCERCRFLCourseControlNumber = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.SelectorFL = 1 then CourseControlNumber end),
	-- CCERCRFLCourseTopCode       = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTopCode       end),
	-- CCERCRFLCourseTitle         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTitle         end),
	-- CCERCRFLCourseMarkLetter    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkLetter    end),
	-- CCERCRFLCourseMarkPoints    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkPoints    end),
	-- CCERCRFLCourseLevelCode     = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.SelectorFL = 1 then CourseLevelCode     end),
	-- CCERCR00CollegeCode         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CollegeCode         end),
	-- CCERCR00YearTermCode        = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then YearTermCode        end),
	-- CCERCR00CourseId            = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseId            end),
	-- CCERCR00CourseControlNumber = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseControlNumber end),
	-- CCERCR00CourseTopCode       = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTopCode       end),
	-- CCERCR00CourseTitle         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTitle         end),
	-- CCERCR00CourseMarkLetter    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCERCR00CourseMarkPoints    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCERCR00CourseLevelCode     = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCERCR01CollegeCode         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CollegeCode         end),
	-- CCERCR01YearTermCode        = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then YearTermCode        end),
	-- CCERCR01CourseId            = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseId            end),
	-- CCERCR01CourseControlNumber = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseControlNumber end),
	-- CCERCR01CourseTopCode       = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTopCode       end),
	-- CCERCR01CourseTitle         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTitle         end),
	-- CCERCR01CourseMarkLetter    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCERCR01CourseMarkPoints    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCERCR01CourseLevelCode     = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCERCR02CollegeCode         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CollegeCode         end),
	-- CCERCR02YearTermCode        = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then YearTermCode        end),
	-- CCERCR02CourseId            = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseId            end),
	-- CCERCR02CourseControlNumber = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseControlNumber end),
	-- CCERCR02CourseTopCode       = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTopCode       end),
	-- CCERCR02CourseTitle         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTitle         end),
	-- CCERCR02CourseMarkLetter    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCERCR02CourseMarkPoints    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCERCR02CourseLevelCode     = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCERCR03CollegeCode         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CollegeCode         end),
	-- CCERCR03YearTermCode        = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then YearTermCode        end),
	-- CCERCR03CourseId            = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseId            end),
	-- CCERCR03CourseControlNumber = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseControlNumber end),
	-- CCERCR03CourseTopCode       = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTopCode       end),
	-- CCERCR03CourseTitle         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTitle         end),
	-- CCERCR03CourseMarkLetter    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCERCR03CourseMarkPoints    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCERCR03CourseLevelCode     = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCERCR04CollegeCode         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CollegeCode         end),
	-- CCERCR04YearTermCode        = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then YearTermCode        end),
	-- CCERCR04CourseId            = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseId            end),
	-- CCERCR04CourseControlNumber = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseControlNumber end),
	-- CCERCR04CourseTopCode       = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTopCode       end),
	-- CCERCR04CourseTitle         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTitle         end),
	-- CCERCR04CourseMarkLetter    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCERCR04CourseMarkPoints    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCERCR04CourseLevelCode     = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCERCR05CollegeCode         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CollegeCode         end),
	-- CCERCR05YearTermCode        = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then YearTermCode        end),
	-- CCERCR05CourseId            = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseId            end),
	-- CCERCR05CourseControlNumber = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseControlNumber end),
	-- CCERCR05CourseTopCode       = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTopCode       end),
	-- CCERCR05CourseTitle         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTitle         end),
	-- CCERCR05CourseMarkLetter    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCERCR05CourseMarkPoints    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCERCR05CourseLevelCode     = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCERCR06CollegeCode         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CollegeCode         end),
	-- CCERCR06YearTermCode        = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then YearTermCode        end),
	-- CCERCR06CourseId            = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseId            end),
	-- CCERCR06CourseControlNumber = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseControlNumber end),
	-- CCERCR06CourseTopCode       = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTopCode       end),
	-- CCERCR06CourseTitle         = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTitle         end),
	-- CCERCR06CourseMarkLetter    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCERCR06CourseMarkPoints    = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCERCR06CourseLevelCode     = max(case when t.CourseTopCode = '493085' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCESCRFLCollegeCode         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.SelectorFL = 1 then CollegeCode         end),
	-- CCESCRFLYearTermCode        = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.SelectorFL = 1 then YearTermCode        end),
	-- CCESCRFLCourseId            = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.SelectorFL = 1 then CourseId            end),
	-- CCESCRFLCourseControlNumber = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.SelectorFL = 1 then CourseControlNumber end),
	-- CCESCRFLCourseTopCode       = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTopCode       end),
	-- CCESCRFLCourseTitle         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTitle         end),
	-- CCESCRFLCourseMarkLetter    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkLetter    end),
	-- CCESCRFLCourseMarkPoints    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkPoints    end),
	-- CCESCRFLCourseLevelCode     = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.SelectorFL = 1 then CourseLevelCode     end),
	-- CCESCR00CollegeCode         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CollegeCode         end),
	-- CCESCR00YearTermCode        = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then YearTermCode        end),
	-- CCESCR00CourseId            = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseId            end),
	-- CCESCR00CourseControlNumber = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseControlNumber end),
	-- CCESCR00CourseTopCode       = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTopCode       end),
	-- CCESCR00CourseTitle         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTitle         end),
	-- CCESCR00CourseMarkLetter    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCESCR00CourseMarkPoints    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCESCR00CourseLevelCode     = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCESCR01CollegeCode         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CollegeCode         end),
	-- CCESCR01YearTermCode        = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then YearTermCode        end),
	-- CCESCR01CourseId            = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseId            end),
	-- CCESCR01CourseControlNumber = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseControlNumber end),
	-- CCESCR01CourseTopCode       = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTopCode       end),
	-- CCESCR01CourseTitle         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTitle         end),
	-- CCESCR01CourseMarkLetter    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCESCR01CourseMarkPoints    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCESCR01CourseLevelCode     = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCESCR02CollegeCode         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CollegeCode         end),
	-- CCESCR02YearTermCode        = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then YearTermCode        end),
	-- CCESCR02CourseId            = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseId            end),
	-- CCESCR02CourseControlNumber = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseControlNumber end),
	-- CCESCR02CourseTopCode       = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTopCode       end),
	-- CCESCR02CourseTitle         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTitle         end),
	-- CCESCR02CourseMarkLetter    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCESCR02CourseMarkPoints    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCESCR02CourseLevelCode     = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCESCR03CollegeCode         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CollegeCode         end),
	-- CCESCR03YearTermCode        = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then YearTermCode        end),
	-- CCESCR03CourseId            = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseId            end),
	-- CCESCR03CourseControlNumber = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseControlNumber end),
	-- CCESCR03CourseTopCode       = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTopCode       end),
	-- CCESCR03CourseTitle         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTitle         end),
	-- CCESCR03CourseMarkLetter    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCESCR03CourseMarkPoints    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCESCR03CourseLevelCode     = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCESCR04CollegeCode         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CollegeCode         end),
	-- CCESCR04YearTermCode        = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then YearTermCode        end),
	-- CCESCR04CourseId            = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseId            end),
	-- CCESCR04CourseControlNumber = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseControlNumber end),
	-- CCESCR04CourseTopCode       = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTopCode       end),
	-- CCESCR04CourseTitle         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTitle         end),
	-- CCESCR04CourseMarkLetter    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCESCR04CourseMarkPoints    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCESCR04CourseLevelCode     = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCESCR05CollegeCode         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CollegeCode         end),
	-- CCESCR05YearTermCode        = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then YearTermCode        end),
	-- CCESCR05CourseId            = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseId            end),
	-- CCESCR05CourseControlNumber = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseControlNumber end),
	-- CCESCR05CourseTopCode       = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTopCode       end),
	-- CCESCR05CourseTitle         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTitle         end),
	-- CCESCR05CourseMarkLetter    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCESCR05CourseMarkPoints    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCESCR05CourseLevelCode     = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCESCR06CollegeCode         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CollegeCode         end),
	-- CCESCR06YearTermCode        = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then YearTermCode        end),
	-- CCESCR06CourseId            = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseId            end),
	-- CCESCR06CourseControlNumber = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseControlNumber end),
	-- CCESCR06CourseTopCode       = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTopCode       end),
	-- CCESCR06CourseTitle         = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTitle         end),
	-- CCESCR06CourseMarkLetter    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCESCR06CourseMarkPoints    = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCESCR06CourseLevelCode     = max(case when t.CourseTopCode = '493086' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEICRFLCollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.SelectorFL = 1 then CollegeCode         end),
	-- CCEICRFLYearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.SelectorFL = 1 then YearTermCode        end),
	-- CCEICRFLCourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.SelectorFL = 1 then CourseId            end),
	-- CCEICRFLCourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.SelectorFL = 1 then CourseControlNumber end),
	-- CCEICRFLCourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTopCode       end),
	-- CCEICRFLCourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTitle         end),
	-- CCEICRFLCourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkLetter    end),
	-- CCEICRFLCourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkPoints    end),
	-- CCEICRFLCourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.SelectorFL = 1 then CourseLevelCode     end),
	-- CCEICR00CollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CollegeCode         end),
	-- CCEICR00YearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then YearTermCode        end),
	-- CCEICR00CourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseId            end),
	-- CCEICR00CourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEICR00CourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEICR00CourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTitle         end),
	-- CCEICR00CourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEICR00CourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEICR00CourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEICR01CollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CollegeCode         end),
	-- CCEICR01YearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then YearTermCode        end),
	-- CCEICR01CourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseId            end),
	-- CCEICR01CourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEICR01CourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEICR01CourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTitle         end),
	-- CCEICR01CourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEICR01CourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEICR01CourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEICR02CollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CollegeCode         end),
	-- CCEICR02YearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then YearTermCode        end),
	-- CCEICR02CourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseId            end),
	-- CCEICR02CourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEICR02CourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEICR02CourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTitle         end),
	-- CCEICR02CourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEICR02CourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEICR02CourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEICR03CollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CollegeCode         end),
	-- CCEICR03YearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then YearTermCode        end),
	-- CCEICR03CourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseId            end),
	-- CCEICR03CourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEICR03CourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEICR03CourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTitle         end),
	-- CCEICR03CourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEICR03CourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEICR03CourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEICR04CollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CollegeCode         end),
	-- CCEICR04YearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then YearTermCode        end),
	-- CCEICR04CourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseId            end),
	-- CCEICR04CourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEICR04CourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEICR04CourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTitle         end),
	-- CCEICR04CourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEICR04CourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEICR04CourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEICR05CollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CollegeCode         end),
	-- CCEICR05YearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then YearTermCode        end),
	-- CCEICR05CourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseId            end),
	-- CCEICR05CourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEICR05CourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEICR05CourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTitle         end),
	-- CCEICR05CourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEICR05CourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEICR05CourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEICR06CollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CollegeCode         end),
	-- CCEICR06YearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then YearTermCode        end),
	-- CCEICR06CourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseId            end),
	-- CCEICR06CourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEICR06CourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEICR06CourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTitle         end),
	-- CCEICR06CourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEICR06CourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEICR06CourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'CR' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEWNCFLCollegeCode         = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.SelectorFL = 1 then CollegeCode         end),
	-- CCEWNCFLYearTermCode        = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.SelectorFL = 1 then YearTermCode        end),
	-- CCEWNCFLCourseId            = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.SelectorFL = 1 then CourseId            end),
	-- CCEWNCFLCourseControlNumber = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.SelectorFL = 1 then CourseControlNumber end),
	-- CCEWNCFLCourseTopCode       = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.SelectorFL = 1 then CourseTopCode       end),
	-- CCEWNCFLCourseTitle         = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.SelectorFL = 1 then CourseTitle         end),
	-- CCEWNCFLCourseMarkLetter    = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.SelectorFL = 1 then CourseMarkLetter    end),
	-- CCEWNCFLCourseMarkPoints    = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.SelectorFL = 1 then CourseMarkPoints    end),
	-- CCEWNCFLCourseLevelCode     = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.SelectorFL = 1 then CourseLevelCode     end),
	-- CCEWNC01CollegeCode         = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CollegeCode         end),
	-- CCEWNC01YearTermCode        = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then YearTermCode        end),
	-- CCEWNC01CourseId            = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseId            end),
	-- CCEWNC01CourseControlNumber = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEWNC01CourseTopCode       = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEWNC01CourseTitle         = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTitle         end),
	-- CCEWNC01CourseMarkLetter    = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEWNC01CourseMarkPoints    = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEWNC01CourseLevelCode     = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEWNC02CollegeCode         = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CollegeCode         end),
	-- CCEWNC02YearTermCode        = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then YearTermCode        end),
	-- CCEWNC02CourseId            = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseId            end),
	-- CCEWNC02CourseControlNumber = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEWNC02CourseTopCode       = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEWNC02CourseTitle         = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTitle         end),
	-- CCEWNC02CourseMarkLetter    = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEWNC02CourseMarkPoints    = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEWNC02CourseLevelCode     = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEWNC03CollegeCode         = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CollegeCode         end),
	-- CCEWNC03YearTermCode        = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then YearTermCode        end),
	-- CCEWNC03CourseId            = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseId            end),
	-- CCEWNC03CourseControlNumber = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEWNC03CourseTopCode       = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEWNC03CourseTitle         = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTitle         end),
	-- CCEWNC03CourseMarkLetter    = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEWNC03CourseMarkPoints    = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEWNC03CourseLevelCode     = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEWNC04CollegeCode         = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CollegeCode         end),
	-- CCEWNC04YearTermCode        = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then YearTermCode        end),
	-- CCEWNC04CourseId            = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseId            end),
	-- CCEWNC04CourseControlNumber = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEWNC04CourseTopCode       = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEWNC04CourseTitle         = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTitle         end),
	-- CCEWNC04CourseMarkLetter    = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEWNC04CourseMarkPoints    = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEWNC04CourseLevelCode     = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEWNC05CollegeCode         = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CollegeCode         end),
	-- CCEWNC05YearTermCode        = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then YearTermCode        end),
	-- CCEWNC05CourseId            = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseId            end),
	-- CCEWNC05CourseControlNumber = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEWNC05CourseTopCode       = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEWNC05CourseTitle         = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTitle         end),
	-- CCEWNC05CourseMarkLetter    = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEWNC05CourseMarkPoints    = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEWNC05CourseLevelCode     = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEWNC06CollegeCode         = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CollegeCode         end),
	-- CCEWNC06YearTermCode        = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then YearTermCode        end),
	-- CCEWNC06CourseId            = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseId            end),
	-- CCEWNC06CourseControlNumber = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEWNC06CourseTopCode       = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEWNC06CourseTitle         = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTitle         end),
	-- CCEWNC06CourseMarkLetter    = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEWNC06CourseMarkPoints    = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEWNC06CourseLevelCode     = max(case when t.CourseTopCode = '493084' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCERNCFLCollegeCode         = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.SelectorFL = 1 then CollegeCode         end),
	-- CCERNCFLYearTermCode        = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.SelectorFL = 1 then YearTermCode        end),
	-- CCERNCFLCourseId            = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.SelectorFL = 1 then CourseId            end),
	-- CCERNCFLCourseControlNumber = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.SelectorFL = 1 then CourseControlNumber end),
	-- CCERNCFLCourseTopCode       = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.SelectorFL = 1 then CourseTopCode       end),
	-- CCERNCFLCourseTitle         = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.SelectorFL = 1 then CourseTitle         end),
	-- CCERNCFLCourseMarkLetter    = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.SelectorFL = 1 then CourseMarkLetter    end),
	-- CCERNCFLCourseMarkPoints    = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.SelectorFL = 1 then CourseMarkPoints    end),
	-- CCERNCFLCourseLevelCode     = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.SelectorFL = 1 then CourseLevelCode     end),
	-- CCERNC01CollegeCode         = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CollegeCode         end),
	-- CCERNC01YearTermCode        = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then YearTermCode        end),
	-- CCERNC01CourseId            = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseId            end),
	-- CCERNC01CourseControlNumber = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseControlNumber end),
	-- CCERNC01CourseTopCode       = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTopCode       end),
	-- CCERNC01CourseTitle         = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTitle         end),
	-- CCERNC01CourseMarkLetter    = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCERNC01CourseMarkPoints    = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCERNC01CourseLevelCode     = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCERNC02CollegeCode         = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CollegeCode         end),
	-- CCERNC02YearTermCode        = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then YearTermCode        end),
	-- CCERNC02CourseId            = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseId            end),
	-- CCERNC02CourseControlNumber = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseControlNumber end),
	-- CCERNC02CourseTopCode       = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTopCode       end),
	-- CCERNC02CourseTitle         = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTitle         end),
	-- CCERNC02CourseMarkLetter    = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCERNC02CourseMarkPoints    = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCERNC02CourseLevelCode     = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCERNC03CollegeCode         = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CollegeCode         end),
	-- CCERNC03YearTermCode        = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then YearTermCode        end),
	-- CCERNC03CourseId            = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseId            end),
	-- CCERNC03CourseControlNumber = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseControlNumber end),
	-- CCERNC03CourseTopCode       = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTopCode       end),
	-- CCERNC03CourseTitle         = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTitle         end),
	-- CCERNC03CourseMarkLetter    = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCERNC03CourseMarkPoints    = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCERNC03CourseLevelCode     = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCERNC04CollegeCode         = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CollegeCode         end),
	-- CCERNC04YearTermCode        = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then YearTermCode        end),
	-- CCERNC04CourseId            = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseId            end),
	-- CCERNC04CourseControlNumber = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseControlNumber end),
	-- CCERNC04CourseTopCode       = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTopCode       end),
	-- CCERNC04CourseTitle         = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTitle         end),
	-- CCERNC04CourseMarkLetter    = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCERNC04CourseMarkPoints    = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCERNC04CourseLevelCode     = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCERNC05CollegeCode         = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CollegeCode         end),
	-- CCERNC05YearTermCode        = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then YearTermCode        end),
	-- CCERNC05CourseId            = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseId            end),
	-- CCERNC05CourseControlNumber = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseControlNumber end),
	-- CCERNC05CourseTopCode       = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTopCode       end),
	-- CCERNC05CourseTitle         = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTitle         end),
	-- CCERNC05CourseMarkLetter    = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCERNC05CourseMarkPoints    = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCERNC05CourseLevelCode     = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCERNC06CollegeCode         = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CollegeCode         end),
	-- CCERNC06YearTermCode        = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then YearTermCode        end),
	-- CCERNC06CourseId            = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseId            end),
	-- CCERNC06CourseControlNumber = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseControlNumber end),
	-- CCERNC06CourseTopCode       = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTopCode       end),
	-- CCERNC06CourseTitle         = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTitle         end),
	-- CCERNC06CourseMarkLetter    = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCERNC06CourseMarkPoints    = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCERNC06CourseLevelCode     = max(case when t.CourseTopCode = '493085' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCESNCFLCollegeCode         = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.SelectorFL = 1 then CollegeCode         end),
	-- CCESNCFLYearTermCode        = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.SelectorFL = 1 then YearTermCode        end),
	-- CCESNCFLCourseId            = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.SelectorFL = 1 then CourseId            end),
	-- CCESNCFLCourseControlNumber = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.SelectorFL = 1 then CourseControlNumber end),
	-- CCESNCFLCourseTopCode       = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.SelectorFL = 1 then CourseTopCode       end),
	-- CCESNCFLCourseTitle         = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.SelectorFL = 1 then CourseTitle         end),
	-- CCESNCFLCourseMarkLetter    = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.SelectorFL = 1 then CourseMarkLetter    end),
	-- CCESNCFLCourseMarkPoints    = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.SelectorFL = 1 then CourseMarkPoints    end),
	-- CCESNCFLCourseLevelCode     = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.SelectorFL = 1 then CourseLevelCode     end),
	-- CCESNC01CollegeCode         = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CollegeCode         end),
	-- CCESNC01YearTermCode        = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then YearTermCode        end),
	-- CCESNC01CourseId            = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseId            end),
	-- CCESNC01CourseControlNumber = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseControlNumber end),
	-- CCESNC01CourseTopCode       = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTopCode       end),
	-- CCESNC01CourseTitle         = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTitle         end),
	-- CCESNC01CourseMarkLetter    = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCESNC01CourseMarkPoints    = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCESNC01CourseLevelCode     = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCESNC02CollegeCode         = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CollegeCode         end),
	-- CCESNC02YearTermCode        = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then YearTermCode        end),
	-- CCESNC02CourseId            = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseId            end),
	-- CCESNC02CourseControlNumber = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseControlNumber end),
	-- CCESNC02CourseTopCode       = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTopCode       end),
	-- CCESNC02CourseTitle         = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTitle         end),
	-- CCESNC02CourseMarkLetter    = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCESNC02CourseMarkPoints    = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCESNC02CourseLevelCode     = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCESNC03CollegeCode         = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CollegeCode         end),
	-- CCESNC03YearTermCode        = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then YearTermCode        end),
	-- CCESNC03CourseId            = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseId            end),
	-- CCESNC03CourseControlNumber = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseControlNumber end),
	-- CCESNC03CourseTopCode       = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTopCode       end),
	-- CCESNC03CourseTitle         = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTitle         end),
	-- CCESNC03CourseMarkLetter    = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCESNC03CourseMarkPoints    = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCESNC03CourseLevelCode     = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCESNC04CollegeCode         = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CollegeCode         end),
	-- CCESNC04YearTermCode        = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then YearTermCode        end),
	-- CCESNC04CourseId            = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseId            end),
	-- CCESNC04CourseControlNumber = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseControlNumber end),
	-- CCESNC04CourseTopCode       = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTopCode       end),
	-- CCESNC04CourseTitle         = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTitle         end),
	-- CCESNC04CourseMarkLetter    = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCESNC04CourseMarkPoints    = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCESNC04CourseLevelCode     = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCESNC05CollegeCode         = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CollegeCode         end),
	-- CCESNC05YearTermCode        = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then YearTermCode        end),
	-- CCESNC05CourseId            = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseId            end),
	-- CCESNC05CourseControlNumber = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseControlNumber end),
	-- CCESNC05CourseTopCode       = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTopCode       end),
	-- CCESNC05CourseTitle         = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTitle         end),
	-- CCESNC05CourseMarkLetter    = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCESNC05CourseMarkPoints    = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCESNC05CourseLevelCode     = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCESNC06CollegeCode         = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CollegeCode         end),
	-- CCESNC06YearTermCode        = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then YearTermCode        end),
	-- CCESNC06CourseId            = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseId            end),
	-- CCESNC06CourseControlNumber = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseControlNumber end),
	-- CCESNC06CourseTopCode       = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTopCode       end),
	-- CCESNC06CourseTitle         = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTitle         end),
	-- CCESNC06CourseMarkLetter    = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCESNC06CourseMarkPoints    = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCESNC06CourseLevelCode     = max(case when t.CourseTopCode = '493086' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEINCFLCollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.SelectorFL = 1 then CollegeCode         end),
	-- CCEINCFLYearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.SelectorFL = 1 then YearTermCode        end),
	-- CCEINCFLCourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.SelectorFL = 1 then CourseId            end),
	-- CCEINCFLCourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.SelectorFL = 1 then CourseControlNumber end),
	-- CCEINCFLCourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.SelectorFL = 1 then CourseTopCode       end),
	-- CCEINCFLCourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.SelectorFL = 1 then CourseTitle         end),
	-- CCEINCFLCourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.SelectorFL = 1 then CourseMarkLetter    end),
	-- CCEINCFLCourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.SelectorFL = 1 then CourseMarkPoints    end),
	-- CCEINCFLCourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.SelectorFL = 1 then CourseLevelCode     end),
	-- CCEINC01CollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CollegeCode         end),
	-- CCEINC01YearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then YearTermCode        end),
	-- CCEINC01CourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseId            end),
	-- CCEINC01CourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEINC01CourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEINC01CourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTitle         end),
	-- CCEINC01CourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEINC01CourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEINC01CourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEINC02CollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CollegeCode         end),
	-- CCEINC02YearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then YearTermCode        end),
	-- CCEINC02CourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseId            end),
	-- CCEINC02CourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEINC02CourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEINC02CourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTitle         end),
	-- CCEINC02CourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEINC02CourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEINC02CourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEINC03CollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CollegeCode         end),
	-- CCEINC03YearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then YearTermCode        end),
	-- CCEINC03CourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseId            end),
	-- CCEINC03CourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEINC03CourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEINC03CourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTitle         end),
	-- CCEINC03CourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEINC03CourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEINC03CourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEINC04CollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CollegeCode         end),
	-- CCEINC04YearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then YearTermCode        end),
	-- CCEINC04CourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseId            end),
	-- CCEINC04CourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEINC04CourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEINC04CourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTitle         end),
	-- CCEINC04CourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEINC04CourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEINC04CourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEINC05CollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CollegeCode         end),
	-- CCEINC05YearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then YearTermCode        end),
	-- CCEINC05CourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseId            end),
	-- CCEINC05CourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEINC05CourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEINC05CourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTitle         end),
	-- CCEINC05CourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEINC05CourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEINC05CourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEINC06CollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CollegeCode         end),
	-- CCEINC06YearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then YearTermCode        end),
	-- CCEINC06CourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseId            end),
	-- CCEINC06CourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEINC06CourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEINC06CourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTitle         end),
	-- CCEINC06CourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEINC06CourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEINC06CourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEINC07CollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CollegeCode         end),
	-- CCEINC07YearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then YearTermCode        end),
	-- CCEINC07CourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CourseId            end),
	-- CCEINC07CourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEINC07CourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEINC07CourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CourseTitle         end),
	-- CCEINC07CourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEINC07CourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEINC07CourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEINC08CollegeCode         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CollegeCode         end),
	-- CCEINC08YearTermCode        = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then YearTermCode        end),
	-- CCEINC08CourseId            = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CourseId            end),
	-- CCEINC08CourseControlNumber = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEINC08CourseTopCode       = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEINC08CourseTitle         = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CourseTitle         end),
	-- CCEINC08CourseMarkLetter    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEINC08CourseMarkPoints    = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEINC08CourseLevelCode     = max(case when t.CourseTopCode = '493087' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCECNCFLCollegeCode         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.SelectorFL = 1 then CollegeCode         end),
	-- CCECNCFLYearTermCode        = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.SelectorFL = 1 then YearTermCode        end),
	-- CCECNCFLCourseId            = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.SelectorFL = 1 then CourseId            end),
	-- CCECNCFLCourseControlNumber = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.SelectorFL = 1 then CourseControlNumber end),
	-- CCECNCFLCourseTopCode       = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.SelectorFL = 1 then CourseTopCode       end),
	-- CCECNCFLCourseTitle         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.SelectorFL = 1 then CourseTitle         end),
	-- CCECNCFLCourseMarkLetter    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.SelectorFL = 1 then CourseMarkLetter    end),
	-- CCECNCFLCourseMarkPoints    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.SelectorFL = 1 then CourseMarkPoints    end),
	-- CCECNCFLCourseLevelCode     = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.SelectorFL = 1 then CourseLevelCode     end),
	-- CCECNC00CollegeCode         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CollegeCode         end),
	-- CCECNC00YearTermCode        = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then YearTermCode        end),
	-- CCECNC00CourseId            = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseId            end),
	-- CCECNC00CourseControlNumber = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseControlNumber end),
	-- CCECNC00CourseTopCode       = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTopCode       end),
	-- CCECNC00CourseTitle         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTitle         end),
	-- CCECNC00CourseMarkLetter    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCECNC00CourseMarkPoints    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCECNC00CourseLevelCode     = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCECNC01CollegeCode         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CollegeCode         end),
	-- CCECNC01YearTermCode        = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then YearTermCode        end),
	-- CCECNC01CourseId            = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseId            end),
	-- CCECNC01CourseControlNumber = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseControlNumber end),
	-- CCECNC01CourseTopCode       = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTopCode       end),
	-- CCECNC01CourseTitle         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTitle         end),
	-- CCECNC01CourseMarkLetter    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCECNC01CourseMarkPoints    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCECNC01CourseLevelCode     = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCECNC02CollegeCode         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CollegeCode         end),
	-- CCECNC02YearTermCode        = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then YearTermCode        end),
	-- CCECNC02CourseId            = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseId            end),
	-- CCECNC02CourseControlNumber = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseControlNumber end),
	-- CCECNC02CourseTopCode       = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTopCode       end),
	-- CCECNC02CourseTitle         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTitle         end),
	-- CCECNC02CourseMarkLetter    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCECNC02CourseMarkPoints    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCECNC02CourseLevelCode     = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCECNC03CollegeCode         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CollegeCode         end),
	-- CCECNC03YearTermCode        = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then YearTermCode        end),
	-- CCECNC03CourseId            = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseId            end),
	-- CCECNC03CourseControlNumber = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseControlNumber end),
	-- CCECNC03CourseTopCode       = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTopCode       end),
	-- CCECNC03CourseTitle         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTitle         end),
	-- CCECNC03CourseMarkLetter    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCECNC03CourseMarkPoints    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCECNC03CourseLevelCode     = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCECNC04CollegeCode         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CollegeCode         end),
	-- CCECNC04YearTermCode        = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then YearTermCode        end),
	-- CCECNC04CourseId            = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseId            end),
	-- CCECNC04CourseControlNumber = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseControlNumber end),
	-- CCECNC04CourseTopCode       = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTopCode       end),
	-- CCECNC04CourseTitle         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTitle         end),
	-- CCECNC04CourseMarkLetter    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCECNC04CourseMarkPoints    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCECNC04CourseLevelCode     = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCECNC05CollegeCode         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CollegeCode         end),
	-- CCECNC05YearTermCode        = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then YearTermCode        end),
	-- CCECNC05CourseId            = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseId            end),
	-- CCECNC05CourseControlNumber = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseControlNumber end),
	-- CCECNC05CourseTopCode       = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTopCode       end),
	-- CCECNC05CourseTitle         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTitle         end),
	-- CCECNC05CourseMarkLetter    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCECNC05CourseMarkPoints    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCECNC05CourseLevelCode     = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCECNC06CollegeCode         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CollegeCode         end),
	-- CCECNC06YearTermCode        = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then YearTermCode        end),
	-- CCECNC06CourseId            = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseId            end),
	-- CCECNC06CourseControlNumber = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseControlNumber end),
	-- CCECNC06CourseTopCode       = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTopCode       end),
	-- CCECNC06CourseTitle         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTitle         end),
	-- CCECNC06CourseMarkLetter    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCECNC06CourseMarkPoints    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCECNC06CourseLevelCode     = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCECNC07CollegeCode         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CollegeCode         end),
	-- CCECNC07YearTermCode        = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then YearTermCode        end),
	-- CCECNC07CourseId            = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CourseId            end),
	-- CCECNC07CourseControlNumber = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CourseControlNumber end),
	-- CCECNC07CourseTopCode       = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CourseTopCode       end),
	-- CCECNC07CourseTitle         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CourseTitle         end),
	-- CCECNC07CourseMarkLetter    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCECNC07CourseMarkPoints    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCECNC07CourseLevelCode     = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'G' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCECNC08CollegeCode         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CollegeCode         end),
	-- CCECNC08YearTermCode        = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then YearTermCode        end),
	-- CCECNC08CourseId            = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CourseId            end),
	-- CCECNC08CourseControlNumber = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CourseControlNumber end),
	-- CCECNC08CourseTopCode       = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CourseTopCode       end),
	-- CCECNC08CourseTitle         = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CourseTitle         end),
	-- CCECNC08CourseMarkLetter    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCECNC08CourseMarkPoints    = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCECNC08CourseLevelCode     = max(case when t.CourseTopCode = '493090' and c.Type = 'NC' and t.CourseLevelCode = 'H' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEVNCFLCollegeCode         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.SelectorFL = 1 then CollegeCode         end),
	-- CCEVNCFLYearTermCode        = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.SelectorFL = 1 then YearTermCode        end),
	-- CCEVNCFLCourseId            = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.SelectorFL = 1 then CourseId            end),
	-- CCEVNCFLCourseControlNumber = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.SelectorFL = 1 then CourseControlNumber end),
	-- CCEVNCFLCourseTopCode       = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.SelectorFL = 1 then CourseTopCode       end),
	-- CCEVNCFLCourseTitle         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.SelectorFL = 1 then CourseTitle         end),
	-- CCEVNCFLCourseMarkLetter    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.SelectorFL = 1 then CourseMarkLetter    end),
	-- CCEVNCFLCourseMarkPoints    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.SelectorFL = 1 then CourseMarkPoints    end),
	-- CCEVNCFLCourseLevelCode     = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.SelectorFL = 1 then CourseLevelCode     end),
	-- CCEVNC00CollegeCode         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CollegeCode         end),
	-- CCEVNC00YearTermCode        = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then YearTermCode        end),
	-- CCEVNC00CourseId            = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseId            end),
	-- CCEVNC00CourseControlNumber = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEVNC00CourseTopCode       = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEVNC00CourseTitle         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTitle         end),
	-- CCEVNC00CourseMarkLetter    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEVNC00CourseMarkPoints    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEVNC00CourseLevelCode     = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEVNC01CollegeCode         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CollegeCode         end),
	-- CCEVNC01YearTermCode        = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then YearTermCode        end),
	-- CCEVNC01CourseId            = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseId            end),
	-- CCEVNC01CourseControlNumber = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEVNC01CourseTopCode       = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEVNC01CourseTitle         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTitle         end),
	-- CCEVNC01CourseMarkLetter    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEVNC01CourseMarkPoints    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEVNC01CourseLevelCode     = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEVNC02CollegeCode         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CollegeCode         end),
	-- CCEVNC02YearTermCode        = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then YearTermCode        end),
	-- CCEVNC02CourseId            = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseId            end),
	-- CCEVNC02CourseControlNumber = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEVNC02CourseTopCode       = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEVNC02CourseTitle         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTitle         end),
	-- CCEVNC02CourseMarkLetter    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEVNC02CourseMarkPoints    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEVNC02CourseLevelCode     = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEVNC03CollegeCode         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CollegeCode         end),
	-- CCEVNC03YearTermCode        = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then YearTermCode        end),
	-- CCEVNC03CourseId            = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseId            end),
	-- CCEVNC03CourseControlNumber = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEVNC03CourseTopCode       = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEVNC03CourseTitle         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTitle         end),
	-- CCEVNC03CourseMarkLetter    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEVNC03CourseMarkPoints    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEVNC03CourseLevelCode     = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEVNC04CollegeCode         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CollegeCode         end),
	-- CCEVNC04YearTermCode        = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then YearTermCode        end),
	-- CCEVNC04CourseId            = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseId            end),
	-- CCEVNC04CourseControlNumber = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEVNC04CourseTopCode       = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEVNC04CourseTitle         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTitle         end),
	-- CCEVNC04CourseMarkLetter    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEVNC04CourseMarkPoints    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEVNC04CourseLevelCode     = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEVNC05CollegeCode         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CollegeCode         end),
	-- CCEVNC05YearTermCode        = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then YearTermCode        end),
	-- CCEVNC05CourseId            = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseId            end),
	-- CCEVNC05CourseControlNumber = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEVNC05CourseTopCode       = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEVNC05CourseTitle         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseTitle         end),
	-- CCEVNC05CourseMarkLetter    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEVNC05CourseMarkPoints    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEVNC05CourseLevelCode     = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'E' and t.Selector   = 1 then CourseLevelCode     end),
	-- CCEVNC06CollegeCode         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CollegeCode         end),
	-- CCEVNC06YearTermCode        = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then YearTermCode        end),
	-- CCEVNC06CourseId            = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseId            end),
	-- CCEVNC06CourseControlNumber = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseControlNumber end),
	-- CCEVNC06CourseTopCode       = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTopCode       end),
	-- CCEVNC06CourseTitle         = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseTitle         end),
	-- CCEVNC06CourseMarkLetter    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkLetter    end),
	-- CCEVNC06CourseMarkPoints    = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseMarkPoints    end),
	-- CCEVNC06CourseLevelCode     = max(case when t.CourseTopCode = '493100' and c.Type = 'NC' and t.CourseLevelCode = 'F' and t.Selector   = 1 then CourseLevelCode     end),

	CCENCRFLCollegeCode         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CollegeCode         end),
	CCENCRFLYearTermCode        = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then YearTermCode        end),
	CCENCRFLCourseId            = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CourseId            end),
	CCENCRFLCourseControlNumber = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CourseControlNumber end),
	CCENCRFLCourseTopCode       = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTopCode       end),
	CCENCRFLCourseTitle         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTitle         end),
	CCENCRFLCourseMarkLetter    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkLetter    end),
	CCENCRFLCourseMarkPoints    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkPoints    end),
	CCENCRFLCourseLevelCode     = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CourseLevelCode     end),
	CCENCR00CollegeCode         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CollegeCode         end),
	CCENCR00YearTermCode        = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then YearTermCode        end),
	CCENCR00CourseId            = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseId            end),
	CCENCR00CourseControlNumber = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseControlNumber end),
	CCENCR00CourseTopCode       = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTopCode       end),
	CCENCR00CourseTitle         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTitle         end),
	CCENCR00CourseMarkLetter    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkLetter    end),
	CCENCR00CourseMarkPoints    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkPoints    end),
	CCENCR00CourseLevelCode     = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseLevelCode     end),
CCENCR002NCollegeCode         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2 then CollegeCode         end),
CCENCR002NYearTermCode        = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2 then YearTermCode        end),
CCENCR002NCourseId            = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2 then CourseId            end),
CCENCR002NCourseControlNumber = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2 then CourseControlNumber end),
CCENCR002NCourseTopCode       = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2 then CourseTopCode       end),
CCENCR002NCourseTitle         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2 then CourseTitle         end),
CCENCR002NCourseMarkLetter    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2 then CourseMarkLetter    end),
CCENCR002NCourseMarkPoints    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2 then CourseMarkPoints    end),
CCENCR002NCourseLevelCode     = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2 then CourseLevelCode     end),
	CCENCR01CollegeCode         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CollegeCode         end),
	CCENCR01YearTermCode        = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then YearTermCode        end),
	CCENCR01CourseId            = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseId            end),
	CCENCR01CourseControlNumber = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseControlNumber end),
	CCENCR01CourseTopCode       = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTopCode       end),
	CCENCR01CourseTitle         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTitle         end),
	CCENCR01CourseMarkLetter    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkLetter    end),
	CCENCR01CourseMarkPoints    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkPoints    end),
	CCENCR01CourseLevelCode     = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseLevelCode     end),
	CCENCR02CollegeCode         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CollegeCode         end),
	CCENCR02YearTermCode        = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then YearTermCode        end),
	CCENCR02CourseId            = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseId            end),
	CCENCR02CourseControlNumber = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseControlNumber end),
	CCENCR02CourseTopCode       = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTopCode       end),
	CCENCR02CourseTitle         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTitle         end),
	CCENCR02CourseMarkLetter    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkLetter    end),
	CCENCR02CourseMarkPoints    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkPoints    end),
	CCENCR02CourseLevelCode     = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseLevelCode     end),
	CCENCR03CollegeCode         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CollegeCode         end),
	CCENCR03YearTermCode        = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then YearTermCode        end),
	CCENCR03CourseId            = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseId            end),
	CCENCR03CourseControlNumber = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseControlNumber end),
	CCENCR03CourseTopCode       = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTopCode       end),
	CCENCR03CourseTitle         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTitle         end),
	CCENCR03CourseMarkLetter    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkLetter    end),
	CCENCR03CourseMarkPoints    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkPoints    end),
	CCENCR03CourseLevelCode     = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseLevelCode     end),
	CCENCR04CollegeCode         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CollegeCode         end),
	CCENCR04YearTermCode        = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then YearTermCode        end),
	CCENCR04CourseId            = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseId            end),
	CCENCR04CourseControlNumber = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseControlNumber end),
	CCENCR04CourseTopCode       = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTopCode       end),
	CCENCR04CourseTitle         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTitle         end),
	CCENCR04CourseMarkLetter    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkLetter    end),
	CCENCR04CourseMarkPoints    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkPoints    end),
	CCENCR04CourseLevelCode     = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseLevelCode     end),
	CCRDCRFLCollegeCode         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CollegeCode         end),
	CCRDCRFLYearTermCode        = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then YearTermCode        end),
	CCRDCRFLCourseId            = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CourseId            end),
	CCRDCRFLCourseControlNumber = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CourseControlNumber end),
	CCRDCRFLCourseTopCode       = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTopCode       end),
	CCRDCRFLCourseTitle         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTitle         end),
	CCRDCRFLCourseMarkLetter    = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkLetter    end),
	CCRDCRFLCourseMarkPoints    = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkPoints    end),
	CCRDCRFLCourseLevelCode     = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CourseLevelCode     end),
	CCRDCR00CollegeCode         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CollegeCode         end),
	CCRDCR00YearTermCode        = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then YearTermCode        end),
	CCRDCR00CourseId            = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseId            end),
	CCRDCR00CourseControlNumber = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseControlNumber end),
	CCRDCR00CourseTopCode       = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTopCode       end),
	CCRDCR00CourseTitle         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTitle         end),
	CCRDCR00CourseMarkLetter    = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkLetter    end),
	CCRDCR00CourseMarkPoints    = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkPoints    end),
	CCRDCR00CourseLevelCode     = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseLevelCode     end),
	CCRDCR01CollegeCode         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CollegeCode         end),
	CCRDCR01YearTermCode        = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then YearTermCode        end),
	CCRDCR01CourseId            = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseId            end),
	CCRDCR01CourseControlNumber = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseControlNumber end),
	CCRDCR01CourseTopCode       = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTopCode       end),
	CCRDCR01CourseTitle         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTitle         end),
	CCRDCR01CourseMarkLetter    = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkLetter    end),
	CCRDCR01CourseMarkPoints    = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkPoints    end),
	CCRDCR01CourseLevelCode     = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseLevelCode     end),
	CCRDCR02CollegeCode         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CollegeCode         end),
	CCRDCR02YearTermCode        = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then YearTermCode        end),
	CCRDCR02CourseId            = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseId            end),
	CCRDCR02CourseControlNumber = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseControlNumber end),
	CCRDCR02CourseTopCode       = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTopCode       end),
	CCRDCR02CourseTitle         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTitle         end),
	CCRDCR02CourseMarkLetter    = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkLetter    end),
	CCRDCR02CourseMarkPoints    = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkPoints    end),
	CCRDCR02CourseLevelCode     = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseLevelCode     end),
	CCRDCR03CollegeCode         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CollegeCode         end),
	CCRDCR03YearTermCode        = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then YearTermCode        end),
	CCRDCR03CourseId            = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseId            end),
	CCRDCR03CourseControlNumber = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseControlNumber end),
	CCRDCR03CourseTopCode       = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTopCode       end),
	CCRDCR03CourseTitle         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTitle         end),
	CCRDCR03CourseMarkLetter    = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkLetter    end),
	CCRDCR03CourseMarkPoints    = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkPoints    end),
	CCRDCR03CourseLevelCode     = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseLevelCode     end),
	CCRDCR04CollegeCode         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CollegeCode         end),
	CCRDCR04YearTermCode        = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then YearTermCode        end),
	CCRDCR04CourseId            = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseId            end),
	CCRDCR04CourseControlNumber = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseControlNumber end),
	CCRDCR04CourseTopCode       = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTopCode       end),
	CCRDCR04CourseTitle         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTitle         end),
	CCRDCR04CourseMarkLetter    = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkLetter    end),
	CCRDCR04CourseMarkPoints    = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkPoints    end),
	CCRDCR04CourseLevelCode     = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseLevelCode     end)
FROM
	Mmap.CCTranscript t
	inner join
	Mmap.Credits c
		on c.Code = t.CourseCreditCode
	inner join
	Mmap.Programs p
		on p.Code = t.CourseTopCode
WHERE
	(
		t.CourseTopCode = '150100'
		and
		t.CourseLevelCode in ('Y','A','B','C','D')
		and
		c.Type = 'CR'
	)
	or
	(
		t.CourseTopCode = '152000'
		and
		t.CourseLevelCode in ('Y','A','B','C','D')
		and
		c.Type = 'CR'
	)
	or
	t.CourseTopCode in ('493087','493084','493085','493086','493100','493090')
GROUP BY
	t.InterSegmentKey;

UPDATE
	t
SET
	t.HS09GradeCode                                = s.HS09GradeCode,
	t.HS09SchoolCode                               = s.HS09SchoolCode,
	t.HS09YearTermCode                             = s.HS09YearTermCode,
	t.HS09CourseCode                               = s.HS09CourseCode,
	t.HS09CourseContentRank                        = s.HS09CourseContentRank,
	t.HS09CourseTitle                              = s.HS09CourseTitle,
	t.HS09CourseMarkLetter                         = s.HS09CourseMarkLetter,
	t.HS09CourseMarkPoints                         = s.HS09CourseMarkPoints,
	t.HS09CourseUniversityAdmissionRequirementCode = s.HS09CourseUniversityAdmissionRequirementCode,
	t.HS09CourseLevelCode                          = s.HS09CourseLevelCode,
	t.HS09CourseTypeCode                           = s.HS09CourseTypeCode,
	t.HS10GradeCode                                = s.HS10GradeCode,
	t.HS10SchoolCode                               = s.HS10SchoolCode,
	t.HS10YearTermCode                             = s.HS10YearTermCode,
	t.HS10CourseCode                               = s.HS10CourseCode,
	t.HS10CourseContentRank                        = s.HS10CourseContentRank,
	t.HS10CourseTitle                              = s.HS10CourseTitle,
	t.HS10CourseMarkLetter                         = s.HS10CourseMarkLetter,
	t.HS10CourseMarkPoints                         = s.HS10CourseMarkPoints,
	t.HS10CourseUniversityAdmissionRequirementCode = s.HS10CourseUniversityAdmissionRequirementCode,
	t.HS10CourseLevelCode                          = s.HS10CourseLevelCode,
	t.HS10CourseTypeCode                           = s.HS10CourseTypeCode,
	t.HS11GradeCode                                = s.HS11GradeCode,
	t.HS11SchoolCode                               = s.HS11SchoolCode,
	t.HS11YearTermCode                             = s.HS11YearTermCode,
	t.HS11CourseCode                               = s.HS11CourseCode,
	t.HS11CourseContentRank                        = s.HS11CourseContentRank,
	t.HS11CourseTitle                              = s.HS11CourseTitle,
	t.HS11CourseMarkLetter                         = s.HS11CourseMarkLetter,
	t.HS11CourseMarkPoints                         = s.HS11CourseMarkPoints,
	t.HS11CourseUniversityAdmissionRequirementCode = s.HS11CourseUniversityAdmissionRequirementCode,
	t.HS11CourseLevelCode                          = s.HS11CourseLevelCode,
	t.HS11CourseTypeCode                           = s.HS11CourseTypeCode,
	t.HS12GradeCode                                = s.HS12GradeCode,
	t.HS12SchoolCode                               = s.HS12SchoolCode,
	t.HS12YearTermCode                             = s.HS12YearTermCode,
	t.HS12CourseCode                               = s.HS12CourseCode,
	t.HS12CourseContentRank                        = s.HS12CourseContentRank,
	t.HS12CourseTitle                              = s.HS12CourseTitle,
	t.HS12CourseMarkLetter                         = s.HS12CourseMarkLetter,
	t.HS12CourseMarkPoints                         = s.HS12CourseMarkPoints,
	t.HS12CourseUniversityAdmissionRequirementCode = s.HS12CourseUniversityAdmissionRequirementCode,
	t.HS12CourseLevelCode                          = s.HS12CourseLevelCode,
	t.HS12CourseTypeCode                           = s.HS12CourseTypeCode,
	t.HSLGGradeCode                                = s.HSLGGradeCode,
	t.HSLGSchoolCode                               = s.HSLGSchoolCode,
	t.HSLGYearTermCode                             = s.HSLGYearTermCode,
	t.HSLGCourseCode                               = s.HSLGCourseCode,
	t.HSLGCourseContentRank                        = s.HSLGCourseContentRank,
	t.HSLGCourseTitle                              = s.HSLGCourseTitle,
	t.HSLGCourseMarkLetter                         = s.HSLGCourseMarkLetter,
	t.HSLGCourseMarkPoints                         = s.HSLGCourseMarkPoints,
	t.HSLGCourseUniversityAdmissionRequirementCode = s.HSLGCourseUniversityAdmissionRequirementCode,
	t.HSLGCourseLevelCode                          = s.HSLGCourseLevelCode,
	t.HSLGCourseTypeCode                           = s.HSLGCourseTypeCode
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey                              = rcc.InterSegmentKey,
			HS09GradeCode                                = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.GradeCode end),
			HS09SchoolCode                               = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
			HS09YearTermCode                             = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
			HS09CourseCode                               = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseCode end),
			HS09CourseContentRank                        = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.ContentRank end),
			HS09CourseTitle                              = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
			HS09CourseMarkLetter                         = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.SectionMark end),
			HS09CourseMarkPoints                         = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
			HS09CourseUniversityAdmissionRequirementCode = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
			HS09CourseLevelCode                          = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
			HS09CourseTypeCode                           = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
			HS10GradeCode                                = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.GradeCode end),
			HS10SchoolCode                               = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
			HS10YearTermCode                             = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
			HS10CourseCode                               = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseCode end),
			HS10CourseContentRank                        = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.ContentRank end),
			HS10CourseTitle                              = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
			HS10CourseMarkLetter                         = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.SectionMark end),
			HS10CourseMarkPoints                         = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
			HS10CourseUniversityAdmissionRequirementCode = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
			HS10CourseLevelCode                          = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
			HS10CourseTypeCode                           = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
			HS11GradeCode                                = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.GradeCode end),
			HS11SchoolCode                               = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
			HS11YearTermCode                             = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
			HS11CourseCode                               = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseCode end),
			HS11CourseContentRank                        = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.ContentRank end),
			HS11CourseTitle                              = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
			HS11CourseMarkLetter                         = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.SectionMark end),
			HS11CourseMarkPoints                         = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
			HS11CourseUniversityAdmissionRequirementCode = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
			HS11CourseLevelCode                          = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
			HS11CourseTypeCode                           = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
			HS12GradeCode                                = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.GradeCode end),
			HS12SchoolCode                               = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
			HS12YearTermCode                             = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
			HS12CourseCode                               = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseCode end),
			HS12CourseContentRank                        = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.ContentRank end),
			HS12CourseTitle                              = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
			HS12CourseMarkLetter                         = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.SectionMark end),
			HS12CourseMarkPoints                         = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
			HS12CourseUniversityAdmissionRequirementCode = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
			HS12CourseLevelCode                          = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
			HS12CourseTypeCode                           = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
			HSLGGradeCode                                = max(case when rcc.RecencySelector = 1 then rcc.GradeCode end),
			HSLGSchoolCode                               = max(case when rcc.RecencySelector = 1 then rcc.SchoolCode end),
			HSLGYearTermCode                             = max(case when rcc.RecencySelector = 1 then rcc.YearTermCode end),
			HSLGCourseCode                               = max(case when rcc.RecencySelector = 1 then rcc.CourseCode end),
			HSLGCourseContentRank                        = max(case when rcc.RecencySelector = 1 then rcc.ContentRank end),
			HSLGCourseTitle                              = max(case when rcc.RecencySelector = 1 then rcc.CourseTitle end),
			HSLGCourseMarkLetter                         = max(case when rcc.RecencySelector = 1 then rcc.SectionMark end),
			HSLGCourseMarkPoints                         = max(case when rcc.RecencySelector = 1 then rcc.MarkPoints end),
			HSLGCourseUniversityAdmissionRequirementCode = max(case when rcc.RecencySelector = 1 then rcc.CourseAGCode end),
			HSLGCourseLevelCode                          = max(case when rcc.RecencySelector = 1 then rcc.CourseLevelCode end),
			HSLGCourseTypeCode                           = max(case when rcc.RecencySelector = 1 then rcc.CourseTypeCode end)
		FROM
			Mmap.RetrospectiveCourseContent rcc
		WHERE
			rcc.DepartmentCode = 14
			and exists (
				SELECT
					null
				FROM
					Mmap.Student s
				WHERE
					s.InterSegmentKey = rcc.InterSegmentKey
			)
		GROUP BY
			rcc.InterSegmentKey
	) s
		on s.InterSegmentKey = t.InterSegmentKey;

UPDATE
	t
SET
	t.HS09OverallGradePointAverage           = s.HS09OverallGradePointAverage,
	t.HS09OverallCumulativeGradePointAverage = s.HS09OverallCumulativeGradePointAverage,
	t.HS09EnglishGradePointAverage           = s.HS09EnglishGradePointAverage,
	t.HS09EnglishCumulativeGradePointAverage = s.HS09EnglishCumulativeGradePointAverage,
	t.HS10OverallGradePointAverage           = s.HS10OverallGradePointAverage,
	t.HS10OverallCumulativeGradePointAverage = s.HS10OverallCumulativeGradePointAverage,
	t.HS10EnglishGradePointAverage           = s.HS10EnglishGradePointAverage,
	t.HS10EnglishCumulativeGradePointAverage = s.HS10EnglishCumulativeGradePointAverage,
	t.HS11OverallGradePointAverage           = s.HS11OverallGradePointAverage,
	t.HS11OverallCumulativeGradePointAverage = s.HS11OverallCumulativeGradePointAverage,
	t.HS11EnglishGradePointAverage           = s.HS11EnglishGradePointAverage,
	t.HS11EnglishCumulativeGradePointAverage = s.HS11EnglishCumulativeGradePointAverage,
	t.HS12OverallGradePointAverage           = s.HS12OverallGradePointAverage,
	t.HS12OverallCumulativeGradePointAverage = s.HS12OverallCumulativeGradePointAverage,
	t.HS12EnglishGradePointAverage           = s.HS12EnglishGradePointAverage,
	t.HS12EnglishCumulativeGradePointAverage = s.HS12EnglishCumulativeGradePointAverage,
	t.HSLGOverallGradePointAverage           = s.HSLGOverallGradePointAverage,
	t.HSLGOverallCumulativeGradePointAverage = s.HSLGOverallCumulativeGradePointAverage,
	t.HSLGEnglishGradePointAverage           = s.HSLGEnglishGradePointAverage,
	t.HSLGEnglishCumulativeGradePointAverage = s.HSLGEnglishCumulativeGradePointAverage
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey                        = rp.InterSegmentKey,
			HS09OverallGradePointAverage           = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '09' then rp.GradePointAverage end),
			HS09OverallCumulativeGradePointAverage = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '09' then rp.CumulativeGradePointAverage end),
			HS09EnglishGradePointAverage           = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '09' then rp.GradePointAverage end),
			HS09EnglishCumulativeGradePointAverage = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '09' then rp.CumulativeGradePointAverage end),
			HS10OverallGradePointAverage           = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '10' then rp.GradePointAverage end),
			HS10OverallCumulativeGradePointAverage = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '10' then rp.CumulativeGradePointAverage end),
			HS10EnglishGradePointAverage           = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '10' then rp.GradePointAverage end),
			HS10EnglishCumulativeGradePointAverage = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '10' then rp.CumulativeGradePointAverage end),
			HS11OverallGradePointAverage           = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '11' then rp.GradePointAverage end),
			HS11OverallCumulativeGradePointAverage = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '11' then rp.CumulativeGradePointAverage end),
			HS11EnglishGradePointAverage           = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '11' then rp.GradePointAverage end),
			HS11EnglishCumulativeGradePointAverage = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '11' then rp.CumulativeGradePointAverage end),
			HS12OverallGradePointAverage           = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '12' then rp.GradePointAverage end),
			HS12OverallCumulativeGradePointAverage = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '12' then rp.CumulativeGradePointAverage end),
			HS12EnglishGradePointAverage           = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '12' then rp.GradePointAverage end),
			HS12EnglishCumulativeGradePointAverage = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '12' then rp.CumulativeGradePointAverage end),
			HSLGOverallGradePointAverage           = max(case when rp.DepartmentCode = 0  and rp.IsLast    = 1    then rp.GradePointAverage end),
			HSLGOverallCumulativeGradePointAverage = max(case when rp.DepartmentCode = 0  and rp.IsLast    = 1    then rp.CumulativeGradePointAverage end),
			HSLGEnglishGradePointAverage           = max(case when rp.DepartmentCode = 14 and rp.IsLast    = 1    then rp.GradePointAverage end),
			HSLGEnglishCumulativeGradePointAverage = max(case when rp.DepartmentCode = 14 and rp.IsLast    = 1    then rp.CumulativeGradePointAverage end)
		FROM
			Mmap.RetrospectivePerformance rp
			inner join
			Mmap.Student s
				on s.InterSegmentKey = rp.InterSegmentKey
		GROUP BY
			rp.InterSegmentKey
	) s
		ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
	t
SET
	t.CCEACNFLSummary   = s.CCEACNFLSummary,
	t.CCEACNFLEslUnits  = s.CCEACNFLEslUnits,
	t.CCEACNFLEnglUnits = s.CCEACNFLEnglUnits
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey   = t.InterSegmentKey,
			CCEACNFLSummary   = 
				max(case when p.Code = '493087' then '1' else '0' end) +
				max(case when p.Code = '493084' then '1' else '0' end) +
				max(case when p.Code = '493085' then '1' else '0' end) +
				max(case when p.Code = '493086' then '1' else '0' end) +
				max(case when p.Code = '493100' then '1' else '0' end) +
				max(case when p.Code = '493090' then '1' else '0' end),
			CCEACNFLEslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
			CCEACNFLEnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
		FROM
			Mmap.CCTranscript t
			inner join
			Mmap.Programs p
				on p.Code = t.CourseTopCode
			inner join
			Mmap.RetroEsl r
				on  r.InterSegmentKey      = t.InterSegmentKey
				and r.CCEACNFLYearTermCode = t.YearTermCode
		GROUP BY
			t.InterSegmentKey
	) s
		ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
	t
SET
	t.CCEACN00Summary   = s.CCEACN00Summary,
	t.CCEACN00EslUnits  = s.CCEACN00EslUnits,
	t.CCEACN00EnglUnits = s.CCEACN00EnglUnits
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey   = t.InterSegmentKey,
			CCEACN00Summary   = 
				max(case when p.Code = '493087' then '1' else '0' end) +
				max(case when p.Code = '493084' then '1' else '0' end) +
				max(case when p.Code = '493085' then '1' else '0' end) +
				max(case when p.Code = '493086' then '1' else '0' end) +
				max(case when p.Code = '493100' then '1' else '0' end) +
				max(case when p.Code = '493090' then '1' else '0' end),
			CCEACN00EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
			CCEACN00EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
		FROM
			Mmap.CCTranscript t
			inner join
			Mmap.Programs p
				on p.Code = t.CourseTopCode
			inner join
			Mmap.RetroEsl r
				on  r.InterSegmentKey      = t.InterSegmentKey
				and r.CCEACN00YearTermCode = t.YearTermCode
		GROUP BY
			t.InterSegmentKey
	) s
		ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
	t
SET
	t.CCEACN01Summary   = s.CCEACN01Summary,
	t.CCEACN01EslUnits  = s.CCEACN01EslUnits,
	t.CCEACN01EnglUnits = s.CCEACN01EnglUnits
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey   = t.InterSegmentKey,
			CCEACN01Summary   = 
				max(case when p.Code = '493087' then '1' else '0' end) +
				max(case when p.Code = '493084' then '1' else '0' end) +
				max(case when p.Code = '493085' then '1' else '0' end) +
				max(case when p.Code = '493086' then '1' else '0' end) +
				max(case when p.Code = '493100' then '1' else '0' end) +
				max(case when p.Code = '493090' then '1' else '0' end),
			CCEACN01EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
			CCEACN01EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
		FROM
			Mmap.CCTranscript t
			inner join
			Mmap.Programs p
				on p.Code = t.CourseTopCode
			inner join
			Mmap.RetroEsl r
				on  r.InterSegmentKey      = t.InterSegmentKey
				and r.CCEACN01YearTermCode = t.YearTermCode
		GROUP BY
			t.InterSegmentKey
	) s
		ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
	t
SET
	t.CCEACN02Summary   = s.CCEACN02Summary,
	t.CCEACN02EslUnits  = s.CCEACN02EslUnits,
	t.CCEACN02EnglUnits = s.CCEACN02EnglUnits
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey   = t.InterSegmentKey,
			CCEACN02Summary   = 
				max(case when p.Code = '493087' then '1' else '0' end) +
				max(case when p.Code = '493084' then '1' else '0' end) +
				max(case when p.Code = '493085' then '1' else '0' end) +
				max(case when p.Code = '493086' then '1' else '0' end) +
				max(case when p.Code = '493100' then '1' else '0' end) +
				max(case when p.Code = '493090' then '1' else '0' end),
			CCEACN02EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
			CCEACN02EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
		FROM
			Mmap.CCTranscript t
			inner join
			Mmap.Programs p
				on p.Code = t.CourseTopCode
			inner join
			Mmap.RetroEsl r
				on  r.InterSegmentKey      = t.InterSegmentKey
				and r.CCEACN02YearTermCode = t.YearTermCode
		GROUP BY
			t.InterSegmentKey
	) s
		ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
	t
SET
	t.CCEACN03Summary   = s.CCEACN03Summary,
	t.CCEACN03EslUnits  = s.CCEACN03EslUnits,
	t.CCEACN03EnglUnits = s.CCEACN03EnglUnits
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey   = t.InterSegmentKey,
			CCEACN03Summary   = 
				max(case when p.Code = '493087' then '1' else '0' end) +
				max(case when p.Code = '493084' then '1' else '0' end) +
				max(case when p.Code = '493085' then '1' else '0' end) +
				max(case when p.Code = '493086' then '1' else '0' end) +
				max(case when p.Code = '493100' then '1' else '0' end) +
				max(case when p.Code = '493090' then '1' else '0' end),
			CCEACN03EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
			CCEACN03EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
		FROM
			Mmap.CCTranscript t
			inner join
			Mmap.Programs p
				on p.Code = t.CourseTopCode
			inner join
			Mmap.RetroEsl r
				on  r.InterSegmentKey      = t.InterSegmentKey
				and r.CCEACN03YearTermCode = t.YearTermCode
		GROUP BY
			t.InterSegmentKey
	) s
		ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
	t
SET
	t.CCEACN04Summary   = s.CCEACN04Summary,
	t.CCEACN04EslUnits  = s.CCEACN04EslUnits,
	t.CCEACN04EnglUnits = s.CCEACN04EnglUnits
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey   = t.InterSegmentKey,
			CCEACN04Summary   = 
				max(case when p.Code = '493087' then '1' else '0' end) +
				max(case when p.Code = '493084' then '1' else '0' end) +
				max(case when p.Code = '493085' then '1' else '0' end) +
				max(case when p.Code = '493086' then '1' else '0' end) +
				max(case when p.Code = '493100' then '1' else '0' end) +
				max(case when p.Code = '493090' then '1' else '0' end),
			CCEACN04EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
			CCEACN04EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
		FROM
			Mmap.CCTranscript t
			inner join
			Mmap.Programs p
				on p.Code = t.CourseTopCode
			inner join
			Mmap.RetroEsl r
				on  r.InterSegmentKey      = t.InterSegmentKey
				and r.CCEACN04YearTermCode = t.YearTermCode
		GROUP BY
			t.InterSegmentKey
	) s
		ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
	t
SET
	t.CCEACN05Summary   = s.CCEACN05Summary,
	t.CCEACN05EslUnits  = s.CCEACN05EslUnits,
	t.CCEACN05EnglUnits = s.CCEACN05EnglUnits
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey   = t.InterSegmentKey,
			CCEACN05Summary   = 
				max(case when p.Code = '493087' then '1' else '0' end) +
				max(case when p.Code = '493084' then '1' else '0' end) +
				max(case when p.Code = '493085' then '1' else '0' end) +
				max(case when p.Code = '493086' then '1' else '0' end) +
				max(case when p.Code = '493100' then '1' else '0' end) +
				max(case when p.Code = '493090' then '1' else '0' end),
			CCEACN05EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
			CCEACN05EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
		FROM
			Mmap.CCTranscript t
			inner join
			Mmap.Programs p
				on p.Code = t.CourseTopCode
			inner join
			Mmap.RetroEsl r
				on  r.InterSegmentKey      = t.InterSegmentKey
				and r.CCEACN05YearTermCode = t.YearTermCode
		GROUP BY
			t.InterSegmentKey
	) s
		ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
	t
SET
	t.CCEACN06Summary   = s.CCEACN06Summary,
	t.CCEACN06EslUnits  = s.CCEACN06EslUnits,
	t.CCEACN06EnglUnits = s.CCEACN06EnglUnits
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey   = t.InterSegmentKey,
			CCEACN06Summary   = 
				max(case when p.Code = '493087' then '1' else '0' end) +
				max(case when p.Code = '493084' then '1' else '0' end) +
				max(case when p.Code = '493085' then '1' else '0' end) +
				max(case when p.Code = '493086' then '1' else '0' end) +
				max(case when p.Code = '493100' then '1' else '0' end) +
				max(case when p.Code = '493090' then '1' else '0' end),
			CCEACN06EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
			CCEACN06EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
		FROM
			Mmap.CCTranscript t
			inner join
			Mmap.Programs p
				on p.Code = t.CourseTopCode
			inner join
			Mmap.RetroEsl r
				on  r.InterSegmentKey      = t.InterSegmentKey
				and r.CCEACN06YearTermCode = t.YearTermCode
		GROUP BY
			t.InterSegmentKey
	) s
		ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
	t
SET
	t.CCEACN07Summary   = s.CCEACN07Summary,
	t.CCEACN07EslUnits  = s.CCEACN07EslUnits,
	t.CCEACN07EnglUnits = s.CCEACN07EnglUnits
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey   = t.InterSegmentKey,
			CCEACN07Summary   = 
				max(case when p.Code = '493087' then '1' else '0' end) +
				max(case when p.Code = '493084' then '1' else '0' end) +
				max(case when p.Code = '493085' then '1' else '0' end) +
				max(case when p.Code = '493086' then '1' else '0' end) +
				max(case when p.Code = '493100' then '1' else '0' end) +
				max(case when p.Code = '493090' then '1' else '0' end),
			CCEACN07EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
			CCEACN07EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
		FROM
			Mmap.CCTranscript t
			inner join
			Mmap.Programs p
				on p.Code = t.CourseTopCode
			inner join
			Mmap.RetroEsl r
				on  r.InterSegmentKey      = t.InterSegmentKey
				and r.CCEACN07YearTermCode = t.YearTermCode
		GROUP BY
			t.InterSegmentKey
	) s
		ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
	t
SET
	t.CCEACN08Summary   = s.CCEACN08Summary,
	t.CCEACN08EslUnits  = s.CCEACN08EslUnits,
	t.CCEACN08EnglUnits = s.CCEACN08EnglUnits
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey   = t.InterSegmentKey,
			CCEACN08Summary   = 
				max(case when p.Code = '493087' then '1' else '0' end) +
				max(case when p.Code = '493084' then '1' else '0' end) +
				max(case when p.Code = '493085' then '1' else '0' end) +
				max(case when p.Code = '493086' then '1' else '0' end) +
				max(case when p.Code = '493100' then '1' else '0' end) +
				max(case when p.Code = '493090' then '1' else '0' end),
			CCEACN08EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
			CCEACN08EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
		FROM
			Mmap.CCTranscript t
			inner join
			Mmap.Programs p
				on p.Code = t.CourseTopCode
			inner join
			Mmap.RetroEsl r
				on  r.InterSegmentKey      = t.InterSegmentKey
				and r.CCEACN08YearTermCode = t.YearTermCode
		GROUP BY
			t.InterSegmentKey
	) s
		ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
	t
SET
	t.STIsEnglishEap           = ss.engl_eap_ind,
	t.STEnglishScaledScore     = ss.engl_scaled_score,
	t.STEnglishCluster1        = ss.engl_cluster_1,
	t.STEnglishCluster2        = ss.engl_cluster_2,
	t.STEnglishCluster3        = ss.engl_cluster_3,
	t.STEnglishCluster4        = ss.engl_cluster_4,
	t.STEnglishCluster5        = ss.engl_cluster_5,
	t.STEnglishCluster6        = ss.engl_cluster_6,
	t.STMathematicsSubject     = ss.math_subject,
	t.STIsMathematicsEap       = ss.math_eap_ind,
	t.STMathematicsScaledScore = ss.math_scaled_score,
	t.STMathematicsCluster1    = ss.math_cluster_1,
	t.STMathematicsCluster2    = ss.math_cluster_2,
	t.STMathematicsCluster3    = ss.math_cluster_3,
	t.STMathematicsCluster4    = ss.math_cluster_4,
	t.STMathematicsCluster5    = ss.math_cluster_5,
	t.STMathematicsCluster6    = ss.math_cluster_6,
	t.STIsEnglishOnly          = ss.esl_eo_ind,
	t.STIsEnglishLearner       = ss.esl_el_ind,
	t.STIsFluentInitially      = ss.esl_ifep_ind,
	t.STIsFluentReclassified   = ss.esl_rfep_ind
FROM
	Mmap.RetroEsl t
	inner join
	dbo.StudentStar ss
		on ss.derkey1 = t.InterSegmentKey;

UPDATE
	t
SET
	t.HSIsRemedial = 1
FROM
	Mmap.RetroEsl t
WHERE
	exists (
		SELECT
			null
		FROM
			Mmap.RetrospectiveCourseContent rcc
		WHERE
			rcc.InterSegmentKey = t.InterSegmentKey
			and rcc.DepartmentCode = 14
			and rcc.CourseCode = '2100'
	);

UPDATE
	t
SET
	t.HSGraduationDate = s.HSGraduationDate
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey  = s.InterSegmentKey,
			HSGraduationDate = max(a.AwardDate)
		FROM
			Mmap.Student s
			inner join
			K12AwardProd a
				on a.Derkey1 = s.InterSegmentKey
		GROUP BY
			s.InterSegmentKey
	) s
		on s.InterSegmentKey = t.InterSegmentKey;

UPDATE
	t
SET
	t.APHSCountry                       = s.APHSCountry,
	t.APUN01Country                     = s.APUN01Country,
	t.APUN01Award                       = s.APUN01Award,
	t.APUN02Country                     = s.APUN02Country,
	t.APUN02Award                       = s.APUN02Award,
	t.APUN03Country                     = s.APUN03Country,
	t.APUN03Award                       = s.APUN03Award,
	t.APUN04Country                     = s.APUN04Country,
	t.APUN04Award                       = s.APUN04Award,
	t.APVisaType                        = s.APVisaType,
	t.APAB540Waiver                     = s.APAB540Waiver,
	t.APComfortableEnglish              = s.APComfortableEnglish,
	t.APApplicationLanguage             = s.APApplicationLanguage,
	t.APEsl                             = s.APEsl,
	t.APBasicSkills                     = s.APBasicSkills,
	t.APEmploymentAssistance            = s.APEmploymentAssistance,
	t.APSeasonalAG                      = s.APSeasonalAG,
	t.APCountry                         = s.APCountry,
	t.APPermanentCountry                = s.APPermanentCountry,
	t.APPermanentCountryInternational   = s.APPermanentCountryInternational,
	t.APCompletedEleventhGrade          = s.APCompletedEleventhGrade,
	t.APCumulativeGradePointAverage     = s.APCumulativeGradePointAverage,
	t.APEnglishCompletedCourseId        = s.APEnglishCompletedCourseId,
	t.APEnglishCompletedCourseGrade     = s.APEnglishCompletedCourseGrade,
	t.APMathematicsCompletedCourseId    = s.APMathematicsCompletedCourseId,
	t.APMathematicsCompletedCourseGrade = s.APMathematicsCompletedCourseGrade,
	t.APMathematicsPassedCourseId       = s.APMathematicsPassedCourseId,
	t.APMathematicsPassedCourseGrade    = s.APMathematicsPassedCourseGrade
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey                   = s.InterSegmentKey,
			APHSCountry                       = CONVERT(CHAR(2)      , a.hs_country),
			APUN01Country                     = CONVERT(CHAR(2)      , a.col1_country),
			APUN01Award                       = CONVERT(CHAR(1)      , a.col1_degree_obtained),
			APUN02Country                     = CONVERT(CHAR(2)      , a.col2_country),
			APUN02Award                       = CONVERT(CHAR(1)      , a.col2_degree_obtained),
			APUN03Country                     = CONVERT(CHAR(2)      , a.col3_country),
			APUN03Award                       = CONVERT(CHAR(1)      , a.col3_degree_obtained),
			APUN04Country                     = CONVERT(CHAR(2)      , a.col4_country),
			APUN04Award                       = CONVERT(CHAR(1)      , a.col4_degree_obtained),
			APVisaType                        = null,
			APAB540Waiver                     = null,
			APComfortableEnglish              = CONVERT(BIT          , case when a.comfortable_english = 't' then 1 else 0 end),
			APApplicationLanguage             = null,
			APEsl                             = CONVERT(BIT          , case when a.esl = 't' then 1 else 0 end),
			APBasicSkills                     = CONVERT(BIT          , case when a.basic_skills = 't' then 1 else 0 end),
			APEmploymentAssistance            = CONVERT(BIT          , case when a.employment_assistance = 't' then 1 else 0 end),
			APSeasonalAG                      = CONVERT(BIT          , case when a.ca_seasonal_ag = 't' then 1 else 0 end),
			APCountry                         = null,
			APPermanentCountry                = null,
			APPermanentCountryInternational   = null,
			APCompletedEleventhGrade          = CONVERT(BIT          , case when a.completed_eleventh_grade = 't' then 1 else 0 end),
			APCumulativeGradePointAverage     = CONVERT(DECIMAL(3,2) , case when IsNumeric(a.grade_point_average) = 0 then null else a.grade_point_average end),
			APEnglishCompletedCourseId        = CONVERT(TINYINT      , case when IsNumeric(a.highest_english_course) = 0 or a.highest_english_course like '%.%' then null else a.highest_english_course end),
			APEnglishCompletedCourseGrade     = CONVERT(VARCHAR(3)   , a.highest_english_grade),
			APMathematicsCompletedCourseId    = CONVERT(TINYINT      , case when IsNumeric(a.highest_math_course_taken) = 0 or a.highest_math_course_taken like '%.%' then null else a.highest_math_course_taken end),
			APMathematicsCompletedCourseGrade = CONVERT(VARCHAR(3)   , a.highest_math_taken_grade),
			APMathematicsPassedCourseId       = CONVERT(TINYINT      , case when IsNumeric(a.highest_math_course_passed) = 0 or a.highest_math_course_passed like '%.%' then null else a.highest_math_course_passed end),
			APMathematicsPassedCourseGrade    = CONVERT(VARCHAR(3)   , a.highest_math_passed_grade),
			Selector                          = row_number() over(partition by a.InterSegmentKey order by (select 1))
		FROM
			Mmap.Student s
			inner join
			comis.ApplicationNew a
				on a.InterSegmentKey = s.InterSegmentKey
	) s
		on s.InterSegmentKey = t.InterSegmentKey
WHERE
	s.Selector = 1;

UPDATE
	t
SET
	t.CCAge                   = s.CCAge,
	t.CCGender                = s.CCGender,
	t.CCCitzenship            = s.CCCitzenship,
	t.CCResidency             = s.CCResidency,
	t.CCEducationStatus       = s.CCEducationStatus,
	t.CCEnrollmentStatus      = s.CCEnrollmentStatus,
	t.CCEthnicities           = s.CCEthnicities,
	t.CCEthnicity             = s.CCEthnicity,
	t.MilitaryStatus          = s.MilitaryStatus,
	t.MilitaryDependentStatus = s.MilitaryDependentStatus,
	t.FosterYouthStatus       = s.FosterYouthStatus,
	t.IncarceratedStatus      = s.IncarceratedStatus,
	t.MESAASEMStatus          = s.MESAASEMStatus,
	t.PuenteStatus            = s.PuenteStatus,
	t.MCHSECHSStatus          = s.MCHSECHSStatus,
	t.UMOJAStatus             = s.UMOJAStatus,
	t.CAAStatus               = s.CAAStatus,
	t.CCIsMultiCollege        = s.CCIsMultiCollege,
	t.CCPrimaryDisability     = s.CCPrimaryDisability,
	t.CCEducationalGoal       = s.CCEducationalGoal,
    t.CCInitialGoal           = s.CCInitialGoal,
	t.CCMajor                 = s.CCMajor,
	t.CCAssessmentStatus      = s.CCAssessmentStatus,
	t.CCAdvisementStatus      = s.CCAdvisementStatus,
	t.CCEnrollmentLastTerm    = s.CCEnrollmentLastTerm,
    t.CCEopsCareStatus        = s.CCEopsCareStatus
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey               = s.InterSegmentKey,
			CCAge                         = st.age_at_term,
			CCGender                      = st.gender,
			CCCitzenship                  = st.citizenship,
			CCResidency                   = st.residency,
			CCEducationStatus             = st.education,
			CCEnrollmentStatus            = st.enrollment,
			CCEthnicities                 = st.multi_race,
			CCEthnicity                   = st.ipeds_race,
			MilitaryStatus                = sg.military,
			MilitaryDependentStatus       = sg.military_dependent,
			FosterYouthStatus             = sg.foster_care,
			IncarceratedStatus            = sg.incarcerated,
			MESAASEMStatus                = sg.mesa,
			PuenteStatus                  = sg.puente,
			MCHSECHSStatus                = sg.mchs,
			UMOJAStatus                   = sg.umoja,
			CAAStatus                     = sg.caa,
			CCIsMultiCollege              = s.IsMultiCollege,
			CCPrimaryDisability           = sd.primary_disability,
			CCEducationalGoal             = coalesce(substring(sm.goals, 1, 1), ss.goals),
            CCInitialGoal                 = st.goal,
			CCMajor                       = coalesce(sm.major, ss.major),
			CCAssessmentStatus            = coalesce(substring(sm.assessment, 1, 2), ss.assessment_status),
			CCAdvisementStatus            = coalesce(substring(sm.advisement, 1, 2), ss.educationplan_status),
			CCEnrollmentLastTerm          = substring(s.TermCodeLast, 3, 3),
            CCEopsCareStatus              = se.eops_care_status
		FROM
			(
				SELECT
					InterSegmentKey = id.InterSegmentKey,
					CollegeId       = st.college_id,
					StudentId       = st.student_id,
					TermCodeFirst   = st.term_id,
					IsMultiCollege  = ss.IsMultiCollege,
					Selector        = row_number() over(partition by id.InterSegmentKey order by t.YearTermCode asc),
					TermCodeLast    = max(t.YearTermCode) over(partition by id.InterSegmentKey)
				FROM
					comis.STUDNTID id
					inner join
					comis.STTERM st
						on  st.college_id = id.college_id
						and st.student_id = id.student_id
					inner join
					comis.Term t
						on t.TermCode = st.term_id
					inner join
					comis.Student ss
						on ss.InterSegmentKey = id.InterSegmentKey
			) s
			inner join
			comis.STTERM st
				on  s.CollegeId      = st.college_id
				and s.StudentId      = st.student_id
				and s.TermCodeFirst  = st.term_id
			left outer join
			comis.SGPOPS sg
				on  sg.college_id = st.college_id
				and sg.student_id = st.student_id
				and sg.term_id    = st.term_id
			left outer join
			comis.SDDSPS sd
				on  sd.college_id = st.college_id
				and sd.student_id = st.student_id
				and sd.term_id    = st.term_id
			left outer join
			comis.SMMATRIC sm
				on  sm.college_id = st.college_id
				and sm.student_id = st.student_id
				and sm.term_id    = st.term_id
			left outer join
			comis.SSSUCCESS ss
				on  ss.college_id = st.college_id
				and ss.student_id = st.student_id
				and ss.term_id    = st.term_id
            left outer join
            comis.SEEOPS se
                on  se.college_id = st.college_id
                and se.student_id = st.student_id
                and se.term_id    = st.term_id
		WHERE
			Selector = 1
	) s
		on s.InterSegmentKey = t.InterSegmentKey;

UPDATE
	t
SET
	t.CCTransferableUnitsAttempted = s.CCTransferableUnitsAttempted,
	t.CCTransferableUnitsEarned    = s.CCTransferableUnitsEarned
FROM
	Mmap.RetroEsl t
	inner join
	(
		SELECT
			InterSegmentKey              = id.InterSegmentKey,
			CCTransferableUnitsAttempted = sum(sx.units_attempted),
			CCTransferableUnitsEarned    = sum(sx.units)
		FROM
			comis.CBCRSINV cb
			inner join
			comis.SXENRLM SX
				on  sx.college_id     = cb.college_id
				and sx.term_id        = cb.term_id
				and sx.course_id      = cb.course_id
				and sx.control_number = cb.control_number
			inner join
			comis.STUDNTID id
				on  id.college_id = sx.college_id
				and id.student_id = sx.student_id
		WHERE
			cb.transfer_status in ('A', 'B')
			and sx.units_attempted <> 88.88
			and sx.units <> 88.88
		GROUP BY
			id.InterSegmentKey
	) s
		on t.InterSegmentKey = s.InterSegmentKey;

UPDATE
    a
SET
    a.CCEslAge  = st.age_at_term
FROM
    comis.studntid id
    inner join
    comis.stterm st on
        st.college_id = id.college_id and
        st.student_id = id.student_id
    inner join
    comis.term t on
        t.TermCode = st.term_id
    inner join
    mmap.RetroEsl a on
        a.InterSegmentKey      = id.InterSegmentKey and
        a.CCEACNFLCollegeCode  = st.college_id and
        a.CCEACNFLYearTermCode = t.YearTermCode;

UPDATE
    a
SET
    a.CCEnglAge  = st.age_at_term
FROM
    comis.studntid id
    inner join
    comis.stterm st on
        st.college_id = id.college_id and
        st.student_id = id.student_id
    inner join
    comis.term t on
        t.TermCode = st.term_id
    inner join
    mmap.RetroEsl a on
        a.InterSegmentKey      = id.InterSegmentKey and
        a.CCENCRFLCollegeCode  = st.college_id and
        a.CCENCRFLYearTermCode = t.YearTermCode;

GO

UPDATE
    t
SET
    t.CCENCR00IsEngl   = isnull(tc1.IsEngl, 0),
    t.CCENCR002NIsEngl = isnull(tc2.IsEngl, 0)
FROM
    Mmap.RetroEsl t
    left outer join
    mmap.TransferCourses tc1 on
        tc1.ControlNumber = t.CCENCR00CourseControlNumber
    left outer join
    mmap.TransferCourses tc2 on
        tc2.ControlNumber = t.CCENCR002NCourseControlNumber;

UPDATE
    t
SET
    t.CCENCR00CoreqType   = isnull(c1.CoreqType, 0),
    t.CCENCR002NCoreqType = isnull(c2.CoreqType, 0)
FROM
    Mmap.RetroEsl t
    left outer join
    mmap.Coreqs c1 on
        c1.ControlNumber = t.CCENCR00CourseControlNumber
    left outer join
    mmap.Coreqs c2 on
        c2.ControlNumber = t.CCENCR002NCourseControlNumber;

GO

UPDATE
    a
SET
    a.XferFice       = b.fice,
    a.XferDate       = b.date_of_xfer,
    a.XfersSurce     = b.source,
    a.XferSchoolName = b.tfr_in_school_name,
    a.XferSchoolType = b.tfr_in_inst_type,
    a.XferSector     = b.sector,
    a.XferState      = b.state,
    a.XferSegment    = b.segment
FROM
    mmap.RetroEsl a
    inner join
    (
        SELECT
            a.InterSegmentKey,
            x.fice,
            x.date_of_xfer,
            x.source,
            x.tfr_in_school_name,
            x.tfr_in_inst_type,
            x.sector,
            x.state,
            x.segment,
            selector = row_number() over(partition by a.InterSegmentKey order by x.date_of_xfer asc)
        FROM
            mmap.RetroEsl a
            inner join
            comis.studntid id on
                id.InterSegmentKey = a.InterSegmentKey
            inner join
            comis.xfer_bucket x on
                x.ssn = id.ssn
            inner join
            comis.Term t on
                t.YearTermCode = a.CCENCR00YearTermCode
            cross apply
            GetAcademicYear(x.date_of_xfer) y
        WHERE
            y.AcademicYear >= t.AcademicYear and
            x.Segment      <> 'CCC'
    ) b on
        a.InterSegmentKey = b.InterSegmentKey
WHERE
    b.selector = 1;

GO

UPDATE
    t
SET
    t.ElaStatusRecent =
        case e.ElaStatusCode
            when 'EL' then 0
            when 'EO' then 1
            when 'IFEP' then 2
            when 'RFEP' then 3
        end
FROM
    mmap.RetroEsl t
    inner join
    calpads.student id on
        id.InterSegmentKey = t.InterSegmentKey
    inner join
    calpads.sela e on
        e.StudentStateId = id.StudentStateId
WHERE
    e.ElaStatusStartDate = (
        SELECT
            max(e1.ElaStatusStartDate)
        FROM
            calpads.sela e1
        WHERE
            e1.StudentStateId = e.StudentStateId
    );

GO

-- DECLARE
--     @ExportId tinyint = 0,
--     @FileId   tinyint = 1;

-- INSERT INTO
--     mmap.Exports
--     (
--         FileId,
--         Description
--     )
-- VALUES
--     (@FileId, 'Retrospective ESL Round 5');

-- SELECT
--     @ExportId = max(Id)
-- FROM
--     mmap.Exports
-- WHERE
--     FileId = @FileId;

-- INSERT INTO
--     mmap.Identifiers
--     (
--         ExportId,
--         InterSegmentKey,
--         StudentId
--     )
-- SELECT
--     ExportId = @ExportId,
--     InterSegmentKey,
--     StudentId = 100000000 + row_number() over(order by (newid()))
-- FROM
--     mmap.RetroEsl;