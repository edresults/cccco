WITH
	cte
	(
		Total,
		Lvl0Curr,
		Lvl0Mmap,
		Lvl0Cap,
		Lvl1Curr,
		Lvl1Mmap,
		Lvl1Cap,
		Lvl2Curr,
		Lvl2Mmap,
		Lvl2Cap
	)
AS
	(
		SELECT
			Total    = count(*),
			Lvl0Curr = sum(Lvl0Curr),
			Lvl0Mmap = sum(case when Lvl0Mmap = 1 then 1 else 0 end),
			Lvl0Cap  = sum(case when Lvl0Cap  = 1 then 1 else 0 end),
			Lvl1Curr = sum(Lvl1Curr),
			Lvl1Mmap = sum(case when Lvl0Mmap = 0 and Lvl1Mmap = 1 then 1 else 0 end),
			Lvl1Cap  = sum(case when Lvl0Cap  = 0 and Lvl1Cap  = 1 then 1 else 0 end),
			Lvl2Curr = sum(Lvl2Curr),
			Lvl2Mmap = sum(case when Lvl0Mmap = 0 and Lvl1Mmap = 0 and Lvl2Mmap = 1 then 1 else 0 end),
			Lvl2Cap  = sum(case when Lvl0Cap  = 0 and Lvl1Cap  = 0 and Lvl2Cap  = 1 then 1 else 0 end)
		FROM
			(
				SELECT
					InterSegmentKey,
					Lvl0Curr = 
						max(
							case
								when EnrollCalc    = 1 then 1
								when EnrollColAlg  = 1 then 1
								when EnrollGeMath  = 1 then 1
								when EnrollPreCalc = 1 then 1
								when EnrollStats   = 1 then 1
								when EnrollTrig    = 1 then 1
								else 0
							end
						),
					Lvl0Mmap = 
						max(
							case
								when EnrollCalc    = 1 then 1
								when EnrollColAlg  = 1 then 1
								when EnrollGeMath  = 1 then 1
								when EnrollPreCalc = 1 then 1
								when EnrollStats   = 1 then 1
								when EnrollTrig    = 1 then 1
								when MmapCalc      = 1 then 1
								when MmapColAlg    = 1 then 1
								when MmapGeMath    = 1 then 1
								when MmapPreCalc   = 1 then 1
								when MmapStats     = 1 then 1
								when MmapTrig      = 1 then 1
								else 0
							end
						),
					Lvl0Cap = 
						max(
							case
								when EnrollCalc    = 1 then 1
								when EnrollColAlg  = 1 then 1
								when EnrollGeMath  = 1 then 1
								when EnrollPreCalc = 1 then 1
								when EnrollStats   = 1 then 1
								when EnrollTrig    = 1 then 1
								when CapCalc       = 1 then 1
								when CapColAlg     = 1 then 1
								when CapGeMath     = 1 then 1
								when CapPreCalc    = 1 then 1
								when CapStats      = 1 then 1
								when CapTrig       = 1 then 1
								else 0
							end
						),
					Lvl1Curr = 
						max(
							case
								when EnrollIntAlg  = 1 then 1
								else 0
							end
						),
					Lvl1Mmap = 
						max(
							case
								when EnrollIntAlg  = 1 then 1
								when MmapIntAlg    = 1 then 1
								else 0
							end
						),
					Lvl1Cap = 
						max(
							case
								when EnrollIntAlg  = 1 then 1
								when CapIntAlg     = 1 then 1
								else 0
							end
						),
					Lvl2Curr = 
						max(
							case
								when EnrollElmAlg  = 1 then 1
								else 0
							end
						),
					Lvl2Mmap = 
						max(
							case
								when EnrollElmAlg  = 1 then 1
								when MmapElmAlg    = 1 then 1
								else 0
							end
						),
					Lvl2Cap = 
						max(
							case
								when EnrollElmAlg  = 1 then 1
								when CapElmAlg     = 1 then 1
								else 0
							end
						)
				FROM
					(
						SELECT
							InterSegmentKey,
							/* Calculus */
							EnrollCalc    = case when CC_CALC_I = 1 or CC_CALC_II = 1 or CC_DIFF_EQ = 1 then 1 else 0 end,
							MmapCalc      = IsMmapCalc,
							CapCalc       = IsCapCalc,
							/* College Algebra */
							EnrollColAlg  = CC_COLL_ALG,
							MmapColAlg    = IsMmapColAlg,
							CapColAlg     = IsCapColAlg,
							/* General Ed */
							EnrollGeMath  = CC_GE_MATH,
							MmapGeMath    = IsMmapGeMath,
							CapGeMath     = IsCapGeMath,
							/* PreCalculus */
							EnrollPreCalc = CC_PRE_CALC,
							MmapPreCalc   = IsMmapPreCalc,
							CapPreCalc    = IsCapPreCalc,
							/* Statistics */
							EnrollStats   = CC_STATISTICS,
							MmapStats     = IsMmapStats,
							CapStats      = IsCapStats,
							/* Trigonometry */
							EnrollTrig    = case when cc_primacy_course_title like '%Trig%' then 1 else 0 end,
							MmapTrig      = IsMmapTrig,
							CapTrig       = IsCapTrig,
							/* Intermediate Algebra */
							EnrollIntAlg  = case when cc_primacy_course_level = 'A' then 1 else 0 end,
							MmapIntAlg    = IsMmapIntAlg,
							CapIntAlg     = IsCapIntAlg,
							/* Elementary Algebra */
							EnrollElmAlg  = case when cc_primacy_course_level = 'B' then 1 else 0 end,
							MmapElmAlg    = IsMmapElmAlg,
							CapElmAlg     = IsCapElmAlg
						FROM
							Mmap.CapMmapMathematicsGrade
					) a
				GROUP BY
					InterSegmentKey
			) b
	)
SELECT
	Method  = 'Current',
	Total,
	Lvl0Num = Lvl0Curr,
	Lvl0Pct = convert(decimal(5,2), 100.00 * Lvl0Curr / Total),
	Lvl1Num = Lvl1Curr,
	Lvl1Pct = convert(decimal(5,2), 100.00 * Lvl1Curr / Total),
	Lvl2Num = Lvl2Curr,
	Lvl2Pct = convert(decimal(5,2), 100.00 * Lvl2Curr / Total)
FROM
	cte
UNION
SELECT
	Method  = 'MMAP',
	Total,
	Lvl0Num = Lvl0Mmap,
	Lvl0Pct = convert(decimal(5,2), 100.00 * Lvl0Mmap / Total),
	Lvl1Num = Lvl1Mmap,
	Lvl1Pct = convert(decimal(5,2), 100.00 * Lvl1Mmap / Total),
	Lvl2Num = Lvl2Mmap,
	Lvl2Pct = convert(decimal(5,2), 100.00 * Lvl2Mmap / Total)
FROM
	cte
UNION
SELECT
	Method  = 'CAP',
	Total,
	Lvl0Num = Lvl0Cap,
	Lvl0Pct = convert(decimal(5,2), 100.00 * Lvl0Cap / Total),
	Lvl1Num = Lvl1Cap,
	Lvl1Pct = convert(decimal(5,2), 100.00 * Lvl1Cap / Total),
	Lvl2Num = Lvl2Cap,
	Lvl2Pct = convert(decimal(5,2), 100.00 * Lvl2Cap / Total)
FROM
	cte;

SELECT
	Type,
	Total,
	Enroll,
	PctEnroll = convert(decimal(5,2), 100.00 * Enroll / Total),
	Mmap,
	PctMmap   = convert(decimal(5,2), 100.00 * Mmap / Total),
	Cap,
	PctCap    = convert(decimal(5,2), 100.00 * Cap / Total)
FROM
	(
		SELECT
			Type = 'Stem',
			Total = count(*),
			Enroll = sum(convert(tinyint, IsTransferStem)),
			Mmap = 
				sum(convert(tinyint, case
					when IsTransferStem     = 1 then 1
					when IsMmapCalc         = 1 then 1
					when IsMmapColAlg       = 1 then 1
					when IsMmapPreCalc      = 1 then 1
					when IsMmapTrig         = 1 then 1
					else 0
				end)),
			Cap = 
				sum(convert(tinyint, case
					when IsTransferStem     = 1 then 1
					when IsCapCalc          = 1 then 1
					when IsCapColAlg        = 1 then 1
					when IsCapPreCalc       = 1 then 1
					when IsCapTrig          = 1 then 1
					else 0
				end))
		FROM
			Mmap.CapMmapMathematicsGrade
		UNION
		SELECT
			Type = 'NonStem',
			Total = count(*),
			Enroll = sum(convert(tinyint, IsTransferNonStem)),
			Mmap = 
				sum(convert(tinyint, case
					when IsTransferNonStem  = 1 then 1
					when IsMmapStats        = 1 then 1
					when IsMmapGeMath       = 1 then 1
					else 0
				end)),
			CapNonStem = 
				sum(convert(tinyint, case
					when IsTransferNonStem  = 1 then 1
					when IsCapStats         = 1 then 1
					when IsCapGeMath        = 1 then 1
					else 0
				end))
		FROM
			Mmap.CapMmapMathematicsGrade
	) a