WITH
	cte
	(
		Total,
		Lvl0Curr,
		Lvl0Mmap,
		Lvl0Cap,
		Lvl1Curr,
		Lvl1Mmap,
		Lvl1Cap,
		Lvl2Curr,
		Lvl2Mmap,
		Lvl2Cap
	)
AS
	(
		SELECT
			Total    = count(*),
			Lvl0Curr = sum(Lvl0Curr),
			Lvl0Mmap = sum(case when Lvl0Mmap = 1 then 1 else 0 end),
			Lvl0Cap  = sum(case when Lvl0Cap  = 1 then 1 else 0 end),
			Lvl1Curr = sum(Lvl1Curr),
			Lvl1Mmap = sum(case when Lvl0Mmap = 0 and Lvl1Mmap = 1 then 1 else 0 end),
			Lvl1Cap  = sum(case when Lvl0Cap  = 0 and Lvl1Cap  = 1 then 1 else 0 end),
			Lvl2Curr = sum(Lvl2Curr),
			Lvl2Mmap = sum(case when Lvl0Mmap = 0 and Lvl1Mmap = 0 and Lvl2Mmap = 1 then 1 else 0 end),
			Lvl2Cap  = sum(case when Lvl0Cap  = 0 and Lvl1Cap  = 0 and Lvl2Cap  = 1 then 1 else 0 end)
		FROM
			(
				SELECT
					InterSegmentKey,
					Lvl0Curr =
						max(
							case
								when cc_primacy_course_level = 'Y' then 1
								else 0
							end
						),
					Lvl0Mmap = 
						max(
							case
								when cc_primacy_course_level = 'Y' then 1
								when OverallCumulativeGradePointAverage >= 2.6 then 1 
								else 0
							end
						),
					Lvl0Cap = 
						max(
							case
								when cc_primacy_course_level = 'Y' then 1
								when OverallCumulativeGradePointAverage >= 1.9 then 1
								else 0
							end
						),
					Lvl1Curr =
						max(
							case
								when cc_primacy_course_level = 'A' then 1
								else 0
							end
						),
					Lvl1Mmap = 
						max(
							case
								when cc_primacy_course_level = 'A' then 1
								when OverallCumulativeGradePointAverage < 2.6 and OverallCumulativeGradePointAverage >= 2.3 then 1
								else 0
							end
						),
					Lvl1Cap = 
						max(
							case
								when cc_primacy_course_level = 'A' then 1
								when OverallCumulativeGradePointAverage < 1.9 and OverallCumulativeGradePointAverage >= 1.4 then 1
								else 0
							end
						),
					Lvl2Curr =
						max(
							case
								when cc_primacy_course_level = 'B' then 1
								else 0
							end
						),
					Lvl2Mmap = 
						max(
							case
								when cc_primacy_course_level = 'B' then 1
								when OverallCumulativeGradePointAverage < 2.3 and OverallCumulativeGradePointAverage >= 2.0 then 2
								else 0
							end
						),
					Lvl2Cap = 
						max(
							case
								when cc_primacy_course_level = 'B' then 1
								else 0
							end
						)
				FROM
					mmap.CapMmapEnglishGrade
				GROUP BY
					InterSegmentKey
			) a
	)
SELECT
	Method  = 'Current',
	Total,
	Lvl0Num = Lvl0Curr,
	Lvl0Pct = convert(decimal(5,2), 100.00 * Lvl0Curr / Total),
	Lvl1Num = Lvl1Curr,
	Lvl1Pct = convert(decimal(5,2), 100.00 * Lvl1Curr / Total),
	Lvl2Num = Lvl2Curr,
	Lvl2Pct = convert(decimal(5,2), 100.00 * Lvl2Curr / Total)
FROM
	cte
UNION
SELECT
	Method  = 'MMAP',
	Total,
	Lvl0Num = Lvl0Mmap,
	Lvl0Pct = convert(decimal(5,2), 100.00 * Lvl0Mmap / Total),
	Lvl1Num = Lvl1Mmap,
	Lvl1Pct = convert(decimal(5,2), 100.00 * Lvl1Mmap / Total),
	Lvl2Num = Lvl2Mmap,
	Lvl2Pct = convert(decimal(5,2), 100.00 * Lvl2Mmap / Total)
FROM
	cte
UNION
SELECT
	Method  = 'CAP',
	Total,
	Lvl0Num = Lvl0Cap,
	Lvl0Pct = convert(decimal(5,2), 100.00 * Lvl0Cap / Total),
	Lvl1Num = Lvl1Cap,
	Lvl1Pct = convert(decimal(5,2), 100.00 * Lvl1Cap / Total),
	Lvl2Num = Lvl2Cap,
	Lvl2Pct = convert(decimal(5,2), 100.00 * Lvl2Cap / Total)
FROM
	cte;