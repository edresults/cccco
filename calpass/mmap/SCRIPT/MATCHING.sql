SELECT
	count(distinct LocStudentId)
FROM
	dbo.K12StudentProd
WHERE
	AcYear = '1617'
	and left(School, 7) = '1964733';

SELECT
	count(distinct LocStudentId)
FROM
	dbo.K12CourseProd
WHERE
	AcYear = '1617'
	and left(School, 7) = '1964733';


SELECT
	count(distinct StudentStateId)
FROM
	calpads.senr
WHERE
	YearCode = '2016-2017'
	and DistrictCode = '1964733';

SELECT
	count(distinct StudentStateId)
FROM
	calpads.scsc
WHERE
	YearCode = '2016-2017'
	and DistrictCode = '1964733';

SELECT
	count(*)
FROM
	dbo.K12StudentProd
WHERE
	CSISNum is null
	or CSISNum = '';

SELECT
	OrgId = isnull(d.OrganizationId, o.OrganizationId),
	OrgName = isnull(d.OrganizationName, o.OrganizationName),
	OrgCode = isnull(d.OrganizationCode, o.OrganizationCode),
	AcYear,
	Records = sum(Records)
FROM
	(
		SELECT
			School,
			AcYear,
			Records = count(*)
		FROM
			dbo.K12StudentProd s
		WHERE
			AcYear in ('1718', '1617', '1516')
			and not exists (
				SELECT
					1
				FROM
					dbo.K12CourseProd c
				WHERE
					c.School = s.School
					and c.LocStudentId = s.LocStudentId
					and c.AcYear = s.AcYear
			)
		GROUP BY
			School,
			AcYear
	) a
	left outer join
	[PRO-DAT-SQL-01].calpass.dbo.Organization o
		on o.OrganizationCode = a.School
	left outer join
	[PRO-DAT-SQL-01].calpass.dbo.Organization d
		on d.OrganizationId = o.ParentId
GROUP BY
	isnull(d.OrganizationId, o.OrganizationId),
	isnull(d.OrganizationName, o.OrganizationName),
	isnull(d.OrganizationCode, o.OrganizationCode),
	AcYear
ORDER BY
	sum(Records) desc;

SELECT
	[09] = sum(case when g.g09 = 1 then 1 else 0 end),
	[10] = sum(case when g.g10 = 1 then 1 else 0 end),
	[11] = sum(case when g.g10 = 1 then 1 else 0 end),
	[0910] = sum(case when g.g09 = 1 and g.g10 = 1 then 1 else 0 end),
	[09_10] = sum(case when g.g09 = 1 or g.g10 = 1 then 1 else 0 end),
	[0911] = sum(case when g.g09 = 1 and g.g11 = 1 then 1 else 0 end),
	[09_11] = sum(case when g.g09 = 1 or g.g11 = 1 then 1 else 0 end),
	[1011] = sum(case when g.g10 = 1 and g.g11 = 1 then 1 else 0 end),
	[10_11] = sum(case when g.g10 = 1 or g.g11 = 1 then 1 else 0 end),
	[091011] = sum(case when g.g09 = 1 and g.g10 = 1 and g.g11 = 1 then 1 else 0 end),
	[09_10_11] = sum(case when g.g09 = 1 or g.g10 = 1 or g.g11 = 1 then 1 else 0 end),
	[09101112] = sum(case when g.g09 = 1 and g.g10 = 1 and g.g11 = 1 and g.g12 = 1 then 1 else 0 end),
	[09_10_11_12] = sum(case when g.g09 = 1 or g.g10 = 1 or g.g11 = 1 or g.g12 = 1 then 1 else 0 end)
FROM
	(
		SELECT
			a.Derkey1,
			g09 = max(case when a.GradeLevel = '09' then 1 else 0 end),
			g10 = max(case when a.GradeLevel = '10' then 1 else 0 end),
			g11 = max(case when a.GradeLevel = '11' then 1 else 0 end),
			g12 = max(case when a.GradeLevel = '12' then 1 else 0 end)	
		FROM
			dbo.K12StudentProd a
		WHERE
			exists (
				SELECT
					1
				FROM
					mmap.CollegeCourseContent c
				WHERE
					c.InterSegmentKey = a.Derkey1
					and c.FirstSelector = 1
				HAVING
					min(YearTermCode) >= '201605'
			)
			and exists (
				SELECT
					1
				FROM
					dbo.Student s
				WHERE
					s.InterSegmentKey = a.Derkey1
					and s.IsHighSchoolCollision = 0
			)
			and exists (
				SELECT
					1
				FROM
					dbo.K12CourseProd c
				WHERE
					c.School = a.School
					and c.LocStudentId = a.LocStudentId
					and c.AcYear = a.AcYear
			)
		GROUP BY
			a.Derkey1
	) g

IF (object_id('tempdb.dbo.#Cohort') is not null)
	BEGIN
		DROP TABLE tempdb.dbo.#Cohort;
	END;

GO

CREATE TABLE
	tempdb.dbo.#Cohort
	(
		InterSegmentKey binary(64) not null primary key
	);

GO

--INSERT INTO
--	tempdb.dbo.#Cohort
--	(
--		InterSegmentKey
--	)
--SELECT DISTINCT
--	u.InterSegmentKey
SELECT
	origincode,
	count(*),
	count(distinct InterSegmentKey)
FROM
	(
		SELECT DISTINCT
			u.InterSegmentKey,
			Age = st.age_at_term,
			Gender = st.gender,
			Origin = left(st.high_school, 1),
			School = i.cdscode
		INTO
			#CohortTest
		FROM
			(
				SELECT
					InterSegmentKey,
					YearTermCode = min(YearTermCode)
				FROM
					mmap.CollegeCourseContent c
				WHERE
					c.FirstSelector = 1
				GROUP BY
					InterSegmentKey
				HAVING
					min(YearTermCode) >= '20165'
			) u
			inner join
			comis.studntid id
				on id.InterSegmentKey = u.InterSegmentKey
			inner join
			comis.stterm st
				on st.college_id = id.college_id
				and st.student_id = id.student_id
			inner join
			comis.Term t
				on t.TermCode = st.term_id
				and t.YearTermCode = u.YearTermCode
			inner join
			comis.Student s
				on s.InterSegmentKey = u.InterSegmentKey
			left outer join
			calpads.Institution i
				on substring(i.CDSCode, 8, 6) = st.high_school
		WHERE -- 653494
			s.IsCollision = 0 -- 640750
			--and st.age_at_term <= 20 -- 463354
			--and st.gender <> 'X' -- 457080
			--and substring(st.high_school, 1, 1) not in ('6', '8', 'Y', 'X') -- 347343
			--and i.cdscode is not null -- 324226
			--and substring(st.high_school, 1, 1) in ('6', '8', 'Y', 'X')
	) a
WHERE
	selector = 1
GROUP BY
	origincode

SELECT
	InterSegmentKey,
	AgeSelector = row_number() over(partition by U.intersegmentkey order by st.age_at_term asc),
	GenderSelector = row_number() over(partition by U.intersegmentkey order by case when st.gender = 'X' then 1 else 0 end asc),
	OriginSelector = row_number() over(partition by U.intersegmentkey order by 
			case
				when left(st.high_school, 1) = '6' then 0
				when left(st.high_school, 1) = '8' then 1
				when left(st.high_school, 1) = 'Y' then 2
				when left(st.high_school, 1) = 'X' then 3
				else 4
			end asc
		)
FROM
	#CohortTest
WHERE
	AgeSelector = 1
	and Age < 20
	and GenderSelector = 1
	and Gender <> 'X'
	and OriginSelector = 1
	and Origin not in ('6', '8', 'Y', 'X')

SELECT
	[09] = sum(case when g.g09 = 1 then 1 else 0 end),
	[10] = sum(case when g.g10 = 1 then 1 else 0 end),
	[11] = sum(case when g.g10 = 1 then 1 else 0 end),
	[0910] = sum(case when g.g09 = 1 and g.g10 = 1 then 1 else 0 end),
	[09_10] = sum(case when g.g09 = 1 or g.g10 = 1 then 1 else 0 end),
	[0911] = sum(case when g.g09 = 1 and g.g11 = 1 then 1 else 0 end),
	[09_11] = sum(case when g.g09 = 1 or g.g11 = 1 then 1 else 0 end),
	[1011] = sum(case when g.g10 = 1 and g.g11 = 1 then 1 else 0 end),
	[10_11] = sum(case when g.g10 = 1 or g.g11 = 1 then 1 else 0 end),
	[091011] = sum(case when g.g09 = 1 and g.g10 = 1 and g.g11 = 1 then 1 else 0 end),
	[09_10_11] = sum(case when g.g09 = 1 or g.g10 = 1 or g.g11 = 1 then 1 else 0 end),
	[09101112] = sum(case when g.g09 = 1 and g.g10 = 1 and g.g11 = 1 and g.g12 = 1 then 1 else 0 end),
	[09_10_11_12] = sum(case when g.g09 = 1 or g.g10 = 1 or g.g11 = 1 or g.g12 = 1 then 1 else 0 end),
	null,
	[101112] = sum(case when g.g10 = 1 and g.g11 = 1 and g.g12 = 1 then 1 else 0 end),
	[cohort] = count(*)
FROM
	(
		SELECT
			a.Derkey1,
			g09 = max(case when a.GradeLevel = '09' then 1 else 0 end),
			g10 = max(case when a.GradeLevel = '10' then 1 else 0 end),
			g11 = max(case when a.GradeLevel = '11' then 1 else 0 end),
			g12 = max(case when a.GradeLevel = '12' then 1 else 0 end)	
		FROM
			dbo.K12StudentProd a
		WHERE
			exists (
				SELECT
					1
				FROM
					#Cohort c
				WHERE
					c.InterSegmentKey = a.Derkey1
			)
			and exists (
				SELECT
					1
				FROM
					dbo.K12CourseProd c
				WHERE
					c.School = a.School
					and c.LocStudentId = a.LocStudentId
					and c.AcYear = a.AcYear
			)
		GROUP BY
			a.Derkey1
	) g