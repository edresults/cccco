WITH
	App
AS
	(
		SELECT DISTINCT
			InterSegmentKey = InterSegmentKeyBug
		FROM
			comis.Application a
		WHERE
			a.college_id = '963'
	),
	SelfReport
AS
	(
		SELECT
			InterSegmentKey,
			SelfReportCgpa
		FROM
			(
				SELECT
					InterSegmentKey,
					SelfReportCgpa = grade_point_average,
					RowSelector = row_number() over(partition by sr.InterSegmentKey order by (select 1) desc)
				FROM
					comis.SelfReport sr
				WHERE
					sr.completed_eleventh_grade = 't'
					and exists (
						SELECT
							1
						FROM
							App a
						WHERE
							sr.InterSegmentKey = a.InterSegmentKey
					)
			) a
		WHERE
			RowSelector = 1
	)
SELECT TOP 1
	sr.InterSegmentKey,
	SelfReportCgpa = sr.SelfReportCgpa
	--TranscriptCgpa = p.CGPA
FROM
	SelfReport sr
	inner join
	calpads.Student ss
		on ss.InterSegmentKeyBug = sr.InterSegmentKey
	--inner join
	--dbo.Student ss
	--	on ss.InterSegmentKey = sr.InterSegmentKey
	--inner join
	--mmap.RetrospectivePerformance p
	--	on p.InterSegmentKey = ss.InterSegmentKey
WHERE
	ss.IsHSGrade11Inclusive = 1
	and ss.IsK12Collision = 0;