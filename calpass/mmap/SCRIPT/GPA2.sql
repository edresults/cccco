-- (1) 1662 total self-report records
SELECT
	count(*) -- 1662
FROM
	mmap.SelfReport;

-- (2) of (1), 1087 with self-reported gpa
SELECT
	count(*) -- 1087
FROM
	mmap.ValidateSelfReport;

-- Group  Transcript   Self-Report
-- Mean   2.87452      2.63325
-- SD     0.53777      0.65451
-- SEM    0.01631      0.01985
-- N      1087         1087

-- p < 0.0001

> shapiro.test(t$cd)

Shapiro-Wilk normality test

data:  t$cd
W = 0.98544, p-value = 6.036e-09

> shapiro.test(t$sr)

Shapiro-Wilk normality test

data:  t$sr
W = 0.98408, p-value = 1.6e-09