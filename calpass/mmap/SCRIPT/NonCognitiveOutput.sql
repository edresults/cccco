USE calpass;

GO

IF (object_id('mmap.NonCognitiveGritOutput') is not null)
	BEGIN
		DROP TABLE mmap.NonCognitiveGritOutput;
	END;

GO

CREATE TABLE
	mmap.NonCognitiveGritOutput
	(
		-- <ixc>
		CollegeCode char(3),
		InterSegmentKey binary(64),
		SubjectCode varchar(4),
		-- </ixc>
		-- <NonCognitiveGrit>
		Discourage varchar(18),
		Distract varchar(18),
		Diligent varchar(18),
		Change varchar(18),
		Worker varchar(18),
		Maintain varchar(18),
		Pursue varchar(18),
		Finish varchar(18),
		-- </NonCognitiveGrit>
		-- <transcript>
		TermCodeFull char(5),
		CourseId varchar(12),
		CourseTitle varchar(68),
		CourseLevelCode char(1),
		CourseLevelRank tinyint,
		SectionId varchar(6),
		SectionUnitsAttempted decimal(4,2),
		SectionGradeCode varchar(3),
		SectionGradeRank tinyint,
		-- </transcript>
		-- <demographic>
		gender char(1),
		ipeds_race char(1),
		goal char(1),
		eops char(1),
		dsps char(1),
		-- </demographic>
		-- <additional>
		MultiCourseEnrollmentInd bit
		-- </additional>
	);

GO

CREATE CLUSTERED INDEX
	ixc_NonCognitiveGritOutput
ON
	mmap.NonCognitiveGritOutput
	(
		CollegeCode,
		InterSegmentKey,
		SubjectCode,
		TermCodeFull,
		CourseLevelRank,
		SectionUnitsAttempted,
		SectionGradeRank
	);

GO

OPEN SYMMETRIC KEY
	SecPii
DECRYPTION BY CERTIFICATE
	SecPii;

INSERT INTO
	mmap.NonCognitiveGritOutput
	(
		-- <ixc>
		CollegeCode,
		InterSegmentKey,
		SubjectCode,
		-- </ixc>
		-- <NonCognitiveGrit>
		Discourage,
		Distract,
		Diligent,
		Change,
		Worker,
		Maintain,
		Pursue,
		Finish,
		-- </NonCognitiveGrit>
		-- <transcript>
		TermCodeFull,
		CourseId,
		CourseTitle,
		CourseLevelCode,
		CourseLevelRank,
		SectionId,
		SectionUnitsAttempted,
		SectionGradeCode,
		SectionGradeRank,
		-- </transcript>
		-- <demographic>
		gender,
		ipeds_race,
		goal,
		eops,
		dsps
		-- </demographic>
	)
SELECT
	ncg.CollegeCode,
	InterSegmentKey = 
		HASHBYTES(
			'SHA2_512',
			convert(char(3), ncg.NameFirst) + 
			convert(char(3), ncg.NameLast) + 
			ncg.Gender + 
			ncg.Birthdate
		),
	p.SubjectCode,
	ncg.Discourage,
	ncg.Distract,
	ncg.Diligent,
	ncg.Change,
	ncg.Worker,
	ncg.Maintain,
	ncg.Pursue,
	ncg.Finish,
	t.TermCodeFull,
	CourseId = sx.course_id,
	CourseTitle = cb.title,
	CourseLevelCode = cb.prior_to_college,
	CourseLevelRank = cl.Rank,
	SectionId = sx.section_id,
	SectionUnitsAttempted = sx.units_attempted,
	SectionGradeCode = sx.grade,
	SectionGradeRank = g.Rank,
	gender = convert(char(1), DecryptByKey(st.gender_enc)),
	ipeds_race = convert(char(1), DecryptByKey(st.ipeds_race_enc)),
	st.goal,
	sd.primary_disability,
	se.eops_care_status
FROM
	mmap.NonCognitiveGrit ncg
	inner join
	comis.studntid id
		on id.college_id = ncg.CollegeCode
		and id.ssn = ncg.StudentId
		and id.student_id_status = ncg.IdStatus
	inner join
	comis.stterm st
		on st.college_id = id.college_id
		and st.student_id = id.student_id
	inner join
	comis.sxenrlm sx
		on sx.college_id = st.college_id
		and sx.student_id = st.student_id
		and sx.term_id = st.term_id
	inner join
	comis.cbcrsinv cb
		on cb.college_id = sx.college_id
		and cb.course_id = sx.course_id
		and cb.control_number = sx.control_number
		and cb.term_id = sx.term_id
	inner join
	comis.Program p
		on p.ProgramCode = cb.top_code
	inner join
	comis.Term t
		on t.TermCode = sx.term_id
	inner join
	comis.CourseLevel cl
		on cl.CourseLevelCode = cb.prior_to_college
	inner join
	comis.Grade g
		on g.GradeCode = sx.grade
	left outer join
	comis.sddsps sd
		on sd.college_id = st.college_id
		and sd.student_id = st.student_id
		and sd.term_id = st.term_id
	left outer join
	comis.seeops se
		on se.college_id = st.college_id
		and se.student_id = st.student_id
		and se.term_id = st.term_id;

CLOSE SYMMETRIC KEY	SecPii;

-- Time :: Lowest YearTerm Value
DELETE
	a
FROM
	mmap.NonCognitiveGritOutput a
WHERE
	exists (
		SELECT
			1
		FROM
			mmap.NonCognitiveGritOutput a1
		WHERE
			a1.CollegeCode = a.CollegeCode
			and a1.InterSegmentKey = a.InterSegmentKey
			and a1.SubjectCode = a.SubjectCode
		HAVING
			min(a1.TermCodeFull) != a.TermCodeFull
	);

UPDATE
	a
SET
	a.MultiCourseEnrollmentInd = 1
FROM
	mmap.NonCognitiveGritOutput a
WHERE
	exists (
		SELECT
			1
		FROM
			mmap.NonCognitiveGritOutput a1
		WHERE
			a1.CollegeCode = a.CollegeCode
			and a1.InterSegmentKey = a.InterSegmentKey
			and a1.SubjectCode = a.SubjectCode
			and a1.TermCodeFull = a.TermCodeFull
		HAVING
			count(*) > 1
	);

GO

IF (object_id('mmap.p_NonCognitiveGritOutput') is not null)
	BEGIN
		DROP PROCEDURE mmap.p_NonCognitiveGritOutput;
	END;

GO

CREATE PROCEDURE
	mmap.p_NonCognitiveGritOutput
AS
	
BEGIN
	SELECT
		-- <ixc>
		CollegeCode,
		InterSegmentKey,
		SubjectCode,
		-- </ixc>
		-- <NonCognitiveGrit>
		Discourage,
		Distract,
		Diligent,
		Change,
		Worker,
		Maintain,
		Pursue,
		Finish,
		-- </NonCognitiveGrit>
		-- <transcript>
		TermCodeFull,
		CourseId,
		CourseTitle,
		CourseLevelCode,
		CourseLevelRank,
		SectionId,
		SectionUnitsAttempted,
		SectionGradeCode,
		SectionGradeRank,
		-- </transcript>
		-- <demographic>
		gender,
		ipeds_race,
		goal,
		eops,
		dsps
		-- </demographic>
		MultiCourseEnrollmentInd
	FROM
		mmap.NonCognitiveGritOutput;
END;

GO

-- extract
EXECUTE xp_cmdshell
	'bcp "EXECUTE calpass.mmap.p_NonCognitiveGritOutput" QUERYOUT "C:\Users\dlamoree\Desktop\NonCognitiveGritOutput.txt" -T -c -q';
-- extract
EXECUTE dbo.sp_spss_output
	'calpass',
	'mmap',
	'NonCognitiveGritOutput',
	'C:\Users\dlamoree\desktop\',  -- '
	'1';

GO

-- upload
DECLARE
	@user nvarchar(255) = 'dlamoree',
	@password nvarchar(255) = '',
	@server nvarchar(255) = '174.127.112.130',
	@port nvarchar(255) = '22',
	@ssh_rsa nvarchar(255) = 'f7:0c:30:21:0d:d4:bf:5c:c2:79:f4:86:6f:21:3c:75';

EXECUTE sp_sftp_winscp_upload
	@user = @user,
	@password = @password,
	@server = @server,
	@port = @port,
	@ssh_rsa = @ssh_rsa,
	@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
	@source_file_name = 'NonCognitiveGritOutput.txt',
	@target_file_dir = '/home/steps2014/NonCognitive/';

EXECUTE sp_sftp_winscp_upload
	@user = @user,
	@password = @password,
	@server = @server,
	@port = @port,
	@ssh_rsa = @ssh_rsa,
	@source_file_dir = 'C:\Users\dlamoree\desktop\', --'
	@source_file_name = 'NonCognitiveGritOutput_spss_import.sps',
	@target_file_dir = '/home/steps2014/NonCognitive/';

GO

-- delete
EXECUTE master.dbo.clr_file_delete
	@file_path = 'C:\Users\dlamoree\desktop\NonCognitiveGritOutput.txt';

EXECUTE master.dbo.clr_file_delete
	@file_path = 'C:\Users\dlamoree\desktop\NonCognitiveGritOutput_spss_import.sps';

GO

-- clean up
DROP PROCEDURE mmap.p_NonCognitiveGritOutput;
DROP TABLE mmap.NonCognitiveGritOutput;