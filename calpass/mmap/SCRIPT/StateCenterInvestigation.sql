SELECT
	College,
	Submit,
	FileName,
	Origin,
	Records
FROM
	(
		SELECT
			College  = o.OrganizationName,
			Submit   = SubmissionDateTime,
			FileName = substring(OriginalFileName, 38, len(OriginalFileName)),
			Origin   = isnull(o2.OrganizationName, '-={File Total}=-'),
			Records  = count(*),
			Ordinal  = row_number() over(partition by SubmissionDateTime ORDER BY count(*) desc, o2.OrganizationName),
			Category = grouping(o2.OrganizationName)
		FROM
			calpads.Institution i
			inner join
			mmap.ProspectiveCohort p
				on p.HighSchool = substring(i.CdsCode, 8, 6)
			inner join
			[PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedrop f
				on f.SubmissionFileId = p.SubmissionFileId
			inner join 
			[PRO-DAT-SQL-01].calpass.dbo.organization o
				on o.OrganizationId = f.OrganizationId
			inner join
			lbswp.Age a
				on a.LowerBound <= ((convert(integer, convert(char(8), f.SubmissionDateTime,112)) - convert(char(8), p.Birthdate,112)) / 10000)
				and a.UpperBound >= ((convert(integer, convert(char(8), f.SubmissionDateTime,112)) - convert(char(8), p.Birthdate,112)) / 10000)
			inner join
			calpass.dbo.K12StudentProd s
				on s.Derkey1 = p.Derkey1
			inner join
			calpass.dbo.Organization o1
				on o1.OrganizationCode = s.School
			inner join
			calpass.dbo.Organization o2
				on o2.OrganizationId = o1.ParentId
			inner join
			calpass.dbo.Student t
				on t.InterSegmentKey = p.Derkey1
		WHERE
			o.OrganizationName in ('Reedley College', 'Clovis Community College', 'Fresno City College', 'State Center Community College District')
			and f.SubmissionFileDescriptionId = 7
			and f.Status <> 'Failed'
			and i.District = 'Kings Canyon Joint Unified'
			and i.StatusType <> 'Merged'
			and i.School is not null
			and p.gpa_cum is not null
			and a.AgeId = 0
			and s.GradeLevel in ('09', '10', '11', '12')
			and t.IsHighSchoolCollision = 0
		GROUP BY
			o.OrganizationName,
			f.SubmissionDateTime,
			f.OriginalFileName,
			rollup(
				o2.OrganizationName
			)
	) a
WHERE
	Ordinal <= 10
ORDER BY
	Submit desc,
	Category asc,
	Records desc