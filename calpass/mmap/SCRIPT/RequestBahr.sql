IF (object_id('mmap.RequestBahr') is not null)
	BEGIN
		DROP TABLE mmap.RequestBahr;
	END;

GO

CREATE TABLE
	mmap.RequestBahr
	(
		CollegeCode           char(3)    not null,
		StudentId             char(9)    not null,
		FirstYearTermCode     char(5)        null,
		GenderCode            char(1)        null,
		AgeAtTerm             smallint       null,
		RaceCode              char(1)        null,
		GoalCode              char(1)        null,
		IsBog                 tinyint        null,
		IsPell                tinyint        null,
		IsDsps                tinyint        null,
		IsEops                tinyint        null,
		FirstMathYearTermCode char(5)        null,
		FirstMathRank         tinyint        null,
		FirstEnglYearTermCode char(5)        null,
		FirstEnglRank         tinyint        null
	);

GO


ALTER TABLE
	mmap.RequestBahr
ADD CONSTRAINT
	PK_RequestBahr
PRIMARY KEY CLUSTERED
	(
		CollegeCode,
		StudentId
	);

GO

INSERT INTO
	mmap.RequestBahr
	(
		CollegeCode,
		StudentId,
		FirstYearTermCode,
		GenderCode,
		AgeAtTerm,
		RaceCode,
		GoalCode,
		IsBog,
		IsPell,
		IsDsps,
		IsEops,
		FirstMathYearTermCode,
		FirstEnglYearTermCode
	)
SELECT
	CollegeCode,
	StudentId,
	FirstYearTermCode,
	GenderCode,
	AgeAtTerm,
	RaceCode,
	GoalCode,
	IsBog,
	IsPell,
	IsDsps,
	IsEops,
	FirstMathYearTermCode,
	FirstEnglYearTermCode
FROM
	(
		SELECT
			CollegeCode = id.college_id,
			StudentId = id.student_id,
			gr.FirstCollegeCode,
			gr.FirstYearTermCode,
			gr.GenderCode,
			AgeAtTerm = st.age_at_term,
			RaceCode = gr.EthnicityCode,
			gr.GoalCode,
			IsBog = 
				isnull((
					SELECT DISTINCT
						1
					FROM
						comis.sfawards sf1
					WHERE
						sf1.college_id = st.college_id
						and sf1.student_id = st.student_id
						and sf1.type_id in ('BA','B1','B2','B3','BB','BC','BD','F1','F2','F3','F4','F5')
				), 0),
			IsPell = 
				isnull((
					SELECT DISTINCT
						1
					FROM
						comis.sfawards sf2
					WHERE
						sf2.college_id = st.college_id
						and sf2.student_id = st.student_id
						and sf2.type_id = 'GP'
				), 0),
			IsDsps = 
				isnull((
					SELECT DISTINCT
						1
					FROM
						comis.sddsps sd
					WHERE
						sd.college_id = st.college_id
						and sd.student_id = st.student_id
				), 0),
			IsEops = 
				isnull((
					SELECT DISTINCT
						1
					FROM
						comis.seeops se
					WHERE
						se.college_id = st.college_id
						and se.student_id = st.student_id
				), 0),
			FirstMathYearTermCode = gr.MathematicsFirstYearTermCode,
			FirstEnglYearTermCode = gr.EnglishFirstYearTermCode,
			Selector = row_number() over(partition by id.college_id, id.student_id order by id.student_id_status desc)
		FROM
			dbo.GoldenRecord gr
			inner join
			comis.studntid id
				on id.InterSegmentKey = gr.InterSegmentKey
				and id.college_id = gr.FirstCollegeCode
			inner join
			comis.Term t
				on t.YearTermCode = gr.FirstYearTermCode
			inner join
			comis.stterm st
				on st.college_id = id.college_id
				and st.student_id = id.student_id
				and st.term_id = t.TermCode
		WHERE
			(
				EnglishFirstYearTermcode <= '20156'
				or
				MathematicsFirstYearTermCode <= '20156'
			)
			and FirstCollegeCode is not null
			-- and gr.InterSegmentKey = 0x06a00f58725ab7a70ebe48b6adaf91791fa56f4c33982fb71cc8ff9736b49a8316d880a4ec328cc157eb969e8063700e281a9a5873ef7d2b7c97471045834280
	) a
WHERE
	Selector = 1;
GO

UPDATE
	t
SET
	t.FirstMathRank = s.FirstMathRank
FROM
	mmap.RequestBahr t
	inner join
	(
		SELECT
			CollegeCode,
			StudentId,
			FirstMathRank = min(cl.LevelsBelow)
		FROM
			mmap.RequestBahr rb
			inner join
			comis.Term t
				on t.YearTermCode = rb.FirstMathYearTermCode
			inner join
			comis.sxenrlm sx with(index=pk_sxenrlm__college_id__student_id__term_id__course_id__section_id)
				on sx.college_id = rb.CollegeCode
				and sx.student_id = rb.StudentId
				and sx.term_id = t.TermCode
			inner join
			comis.cbcrsinv cb
				on cb.college_id = sx.college_id
				and cb.term_id = sx.term_id
				and cb.course_id = sx.course_id
				and cb.control_number = sx.control_number
			inner join
			comis.Mark m
				on m.MarkCode = sx.grade
			inner join
			lbswp.Credit c
				on c.CreditCode = cb.credit_status
			inner join
			comis.CourseLevel cl
				on cl.CourseLevelCode = cb.prior_to_college
		WHERE
			m.IsEnroll = 1
			and c.IsCredit = 1
			and cb.top_code = '170100'
		GROUP BY
			CollegeCode,
			StudentId
	) s
		on s.CollegeCode = t.CollegeCode
		and s.StudentId = s.StudentId;

UPDATE
	t
SET
	t.FirstEnglRank = s.FirstEnglRank
FROM
	mmap.RequestBahr t
	inner join
	(
		SELECT
			CollegeCode,
			StudentId,
			FirstEnglRank = min(cl.LevelsBelow)
		FROM
			mmap.RequestBahr rb
			inner join
			comis.Term t
				on t.YearTermCode = rb.FirstEnglYearTermCode
			inner join
			comis.sxenrlm sx with(index=pk_sxenrlm__college_id__student_id__term_id__course_id__section_id)
				on sx.college_id = rb.CollegeCode
				and sx.student_id = rb.StudentId
				and sx.term_id = t.TermCode
			inner join
			comis.cbcrsinv cb
				on cb.college_id = sx.college_id
				and cb.term_id = sx.term_id
				and cb.course_id = sx.course_id
				and cb.control_number = sx.control_number
			inner join
			comis.Mark m
				on m.MarkCode = sx.grade
			inner join
			lbswp.Credit c
				on c.CreditCode = cb.credit_status
			inner join
			comis.CourseLevel cl
				on cl.CourseLevelCode = cb.prior_to_college
		WHERE
			m.IsEnroll = 1
			and c.IsCredit = 1
			and cb.top_code = '150100'
		GROUP BY
			CollegeCode,
			StudentId
	) s
		on s.CollegeCode = t.CollegeCode
		and s.StudentId = s.StudentId;

-- bcp "SELECT FirstCollegeCode,StudentId,FirstYearTermCode,GenderCode,AgeAtTerm,RaceCode,GoalCode,IsBog,IsPell,IsDsps,IsEops,FirstMathYearTermCode,FirstMathRank,FirstEnglYearTermCode,FirstEnglRank FROM calpass.mmap.RequestBahr" QUERYOUT "C:\Data\CC\demographic.txt" -T -c -t\t