CREATE PROCEDURE
	mmap.RequestKristen
AS

BEGIN
	SELECT
		t.InterSegmentKey,
		college_id,
		term_id,
		English,
		PreAlgebra,
		AlgebraI,
		Geometry,
		AlgebraII,
		Trigonometry,
		PreCalculus,
		[Statistics],
		Calculus,
		completed_eleventh_grade,
		grade_point_average,
		highest_english_course,
		highest_english_grade,
		highest_math_course_taken,
		highest_math_taken_grade,
		highest_math_course_passed,
		highest_math_passed_grade
	FROM
		mmap.Transcript t
		inner join
		comis.ApplicationNew a
			on t.InterSegmentKey = a.InterSegmentKey
		inner join
		dbo.Student s
			on s.InterSegmentKey = t.InterSegmentKey
	WHERE
		s.IsHighSchoolCollision = 0;
END;

GO

bcp "EXEC calpass.mmap.RequestKristen" QUERYOUT "C:\Data\CC\SelfReport.txt" -T -c -t\t

GO

DROP PROCEDURE mmap.RequestKristen;