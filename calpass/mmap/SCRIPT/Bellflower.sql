DECLARE
	@SubmissionFileId int = 16479,
	@DistrictCode char(7) = '1964303',
	@YearCode char(9);

SELECT
	s.Derkey1,
	s.GradeLevel,
	s.AcYear,
	max(c.AcYear)
FROM
	K12StudentProd s
	inner join
	K12CourseProd c
		on c.School = s.School
		and c.LocStudentId = s.LocStudentId
WHERE
	left(s.school, 7) = @DistrictCode
	and s.AcYear = (
		SELECT
			max(s1.AcYear)
		FROM
			K12StudentProd s1
		WHERE
			s1.School = s.School
			and s1.LocStudentId = s.LocStudentId
	)
	and exists (
		SELECT
			1
		FROM
			mmap.ProspectiveCohort c
		WHERE
			c.SubmissionFileId = @SubmissionFileId
			and c.derkey1 = s.Derkey1
	)
GROUP BY
	s.Derkey1,
	s.GradeLevel,
	s.AcYear;

SELECT
	grade_level_code,
	count(*)
FROM
	calpads.senr s
WHERE
	district_code = @DistrictCode
	and s.year_code = (
		SELECT
			max(s1.year_code)
		FROM
			calpads.senr s1
		WHERE
			s1.district_code = s.district_code
			and s1.ssid = s.ssid
	)
	and exists (
		SELECT
			1
		FROM
			mmap.ProspectiveCohort c
		WHERE
			c.SubmissionFileId = @SubmissionFileId
			and c.derkey1 = dbo.Derkey1(
				s.name_first,
				s.name_last,
				s.gender,
				s.birthdate
			)
	)
GROUP BY
	grade_level_code;