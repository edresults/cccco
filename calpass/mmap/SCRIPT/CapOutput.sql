SELECT
	TopCode,
	FirstTime,
	HighSchool,
	PctLoss     = convert(decimal(5,2), 100.00 * (1.00 - (1.00 * HighSchool / FirstTime))),
	Junior,
	PctLoss     = convert(decimal(5,2), 100.00 * (1.00 - (1.00 * Junior / HighSchool))),
	[Unique],
	PctLoss     = convert(decimal(5,2), 100.00 * (1.00 - (1.00 * [Unique] / Junior)))
FROM
	(
		SELECT
			TopCode,
			FirstTime  = count(*),
			HighSchool = count(case when s.IsHighSchoolSection = 1 then s.InterSegmentKey end),
			Junior     = count(case when s.IsGrade11SectionInclusive = 1 then s.InterSegmentKey end),
			[Unique]   = count(case when s.IsHighSchoolCollision = 0 and s.IsGrade11SectionInclusive = 1 then s.InterSegmentKey end)
		FROM
			(
				SELECT DISTINCT
					InterSegmentKey,
					TopCode
				FROM
					Mmap.CapCohort cc
				WHERE
					cc.TopCode = '150100'
					-- students without having K12 Math coursework
					and not exists (
						SELECT
							1
						FROM
							(
								SELECT DISTINCT
									InterSegmentKey
								FROM
									Mmap.CapCohort
								WHERE
									TopCode = '150100'
							) c
							inner join
							dbo.Student s
								on s.InterSegmentKey = c.InterSegmentKey
							left outer join
							Mmap.CapMmapEnglishGrade e
								on e.InterSegmentKey = c.InterSegmentKey
						WHERE
							s.IsGrade11SectionInclusive = 1
							and s.IsHighSchoolCollision = 0
							and e.InterSegmentKey is null
							and cc.InterSegmentKey = c.InterSegmentKey
					)
			) c
			left outer join
			dbo.Student s
				on s.InterSegmentKey = c.InterSegmentKey
			left outer join
			Mmap.CapMmapEnglishGrade e
				on e.InterSegmentKey = c.InterSegmentKey
		GROUP BY
			TopCode
		UNION
		SELECT
			TopCode,
			FirstTime  = count(*),
			HighSchool = count(case when s.IsHighSchoolSection = 1 then s.InterSegmentKey end),
			Junior     = count(case when s.IsGrade11SectionInclusive = 1 then s.InterSegmentKey end),
			[Unique]   = count(case when s.IsHighSchoolCollision = 0 and s.IsGrade11SectionInclusive = 1 then s.InterSegmentKey end)
		FROM
			(
				SELECT DISTINCT
					InterSegmentKey,
					TopCode
				FROM
					Mmap.CapCohort cc
				WHERE
					cc.TopCode = '170100'
					-- students without having K12 Math coursework
					and not exists (
						SELECT
							1
						FROM
							(
								SELECT DISTINCT
									InterSegmentKey
								FROM
									Mmap.CapCohort
								WHERE
									TopCode = '170100'
							) c
							inner join
							dbo.Student s
								on s.InterSegmentKey = c.InterSegmentKey
							left outer join
							Mmap.CapMmapMathematicsGrade e
								on e.InterSegmentKey = c.InterSegmentKey
						WHERE
							s.IsGrade11SectionInclusive = 1
							and s.IsHighSchoolCollision = 0
							and e.InterSegmentKey is null
							and cc.InterSegmentKey = c.InterSegmentKey
					)
			) c
			left outer join
			dbo.Student s
				on s.InterSegmentKey = c.InterSegmentKey
			left outer join
			Mmap.CapMmapMathematicsGrade e
				on e.InterSegmentKey = c.InterSegmentKey
		GROUP BY
			TopCode
	) a;