USE calpass;

GO

IF (object_id('mmap.SelfReportOutput') is not null)
	BEGIN
		DROP TABLE mmap.SelfReportOutput;
	END;

GO

SELECT
	InterSegmentKey,
	GradePointAverage,
	English,
	PreAlgebra,
	AlgebraI,
	Geometry,
	AlgebraII,
	Trigonometry,
	PreCalculus,
	[Statistics],
	Calculus,
	completed_eleventh_grade,
	grade_point_average,
	highest_english_course,
	highest_english_grade,
	highest_math_course_taken,
	highest_math_taken_grade,
	highest_math_course_passed,
	highest_math_passed_grade
INTO
	mmap.SelfReportOutput
FROM
	(
		SELECT
			InterSegmentKey            = a.InterSegmentKey,
			-- Transcript
			GradePointAverage          = t.CumulativeGradePointAverage,
			English                    = t.English,
			PreAlgebra                 = t.PreAlgebra,
			AlgebraI                   = t.AlgebraI,
			Geometry                   = t.Geometry,
			AlgebraII                  = t.AlgebraII,
			Trigonometry               = t.Trigonometry,
			PreCalculus                = t.PreCalculus,
			[Statistics]               = t.[Statistics],
			Calculus                   = t.Calculus,
			-- SelfReport
			completed_eleventh_grade   = convert(char(1),      ltrim(rtrim(replace(replace(a.completed_eleventh_grade, char(10), ''), char(13), '')))),
			grade_point_average        = convert(decimal(5,2), ltrim(rtrim(replace(replace(a.grade_point_average, char(10), ''), char(13), '')))),
			highest_english_course     = convert(varchar(255), ltrim(rtrim(replace(replace(sre.Description, char(10), ''), char(13), '')))),
			highest_english_grade      = convert(varchar(255), ltrim(rtrim(replace(replace(a.highest_english_grade, char(10), ''), char(13), '')))),
			highest_math_course_taken  = convert(varchar(255), ltrim(rtrim(replace(replace(srm1.Description, char(10), ''), char(13), '')))),
			highest_math_taken_grade   = convert(varchar(255), ltrim(rtrim(replace(replace(a.highest_math_taken_grade, char(10), ''), char(13), '')))),
			highest_math_course_passed = convert(varchar(255), ltrim(rtrim(replace(replace(srm2.Description, char(10), ''), char(13), '')))),
			highest_math_passed_grade  = convert(varchar(255), ltrim(rtrim(replace(replace(a.highest_math_passed_grade, char(10), ''), char(13), '')))),
			ApplicationSelector        = row_number() over(partition by a.ccc_id order by a.term_id asc)
		FROM
			comis.ApplicationNew a
			inner join
			dbo.Student s
				on s.InterSegmentKey = a.InterSegmentKey
			inner join
			mmap.Transcript t
				on t.InterSegmentKey = s.InterSegmentKey
			left outer join
			comis.SelfReportEngl sre
				on sre.EnglId = a.highest_english_course
			left outer join
			comis.SelfReportMath srm1
				on srm1.MathId = a.highest_math_course_taken
			left outer join
			comis.SelfReportMath srm2
				on srm2.MathId = a.highest_math_course_passed
		WHERE
			s.IsHighSchoolCollision = 0
	) a
WHERE
	ApplicationSelector = 1;

GO

-- extract
EXECUTE xp_cmdshell
	'bcp "SELECT * FROM calpass.mmap.SelfReportOutput" QUERYOUT "C:\Users\dlamoree.ERP\desktop\SelfReportOutput.txt" -T -c -q';
-- extract
EXECUTE dbo.sp_spss_output
	'calpass',
	'mmap',
	'SelfReportOutput',
	'C:\Users\dlamoree.ERP\desktop\',  -- '
	'1';

GO

-- clean up
DROP TABLE mmap.SelfReportOutput;