CREATE TABLE
	#Student
	(
		i int identity(0,1) primary key,
		InterSegmentKey binary(64)
	);

INSERT
	#Student
	(
		InterSegmentKey
	)
SELECT
	InterSegmentKey
FROM
	dbo.Student
WHERE
	IsHS = 1
	and IsK12Collision = 0;

SET NOCOUNT ON;

DECLARE
	@Severity tinyint = 0,
	@State tinyint = 1,
	@Error varchar(2048),
	@Counter int = 0,
	@Increment int = 100000,
	@Count int,
	@InterSegmentKey binary(64),
	@StudentCohort dbo.Student;

SELECT
	@Count = count(*)
FROM
	#Student;

WHILE (1 = 1)
BEGIN

	SET @Error = convert(varchar, @Counter) + ' of ' + convert(varchar, ceiling(convert(decimal, @count) / @increment));

	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

	DELETE @StudentCohort;

	INSERT INTO
		@StudentCohort
		(
			InterSegmentKey
		)
	SELECT
		InterSegmentKey
	FROM
		#Student
	WHERE
		i >= @Increment * @Counter
		and i < @Increment * (@Counter + 1);

	EXECUTE mmap.StudentTranscriptProcess
		@Student = @StudentCohort;

	IF (not exists (SELECT 1 FROM @StudentCohort))
		BEGIN
			BREAK;
		END;

	SET @Counter += 1;
END;