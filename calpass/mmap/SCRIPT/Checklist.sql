-- Checklist
-- [_] Execute mmap.p_prospectivecohort_eval
-- [_] Check MemberList for most recent data submitted by high school in question
-- [_] Check file uploaded for errors
-- [_] Check HSTranscriptSummaryHash
-- [_] Check K12StudentProd
-- [_] Check K12CourseProd

Execute mmap.p_prospectivecohort_eval 18381

SELECT
	*
FROM
	mmap.ProspectiveCohort c
	inner join
	dbo.HSTranscriptSummaryHash t
		on c.Derkey1 = t.InterSegmentKey
WHERE
	SubmissionFileId = 18381

SELECT
	*
FROM
	dbo.K12StudentProd
WHERE
	Derkey1 = 0x6D8BE783457B7CD2C83E40120460425E6595490E7D87DCE2EB5396356EA78799A1347D5ADF20836EDE63580F0D436781527D7AF8E9D0239B16515900EBAB5D4D

SELECT
	*
FROM
	dbo.K12CourseProd
WHERE
	Derkey1 = 0x6D8BE783457B7CD2C83E40120460425E6595490E7D87DCE2EB5396356EA78799A1347D5ADF20836EDE63580F0D436781527D7AF8E9D0239B16515900EBAB5D4D

SELECT
	*
FROM
	dbo.HSTranscriptSummaryHashGet(0x6D8BE783457B7CD2C83E40120460425E6595490E7D87DCE2EB5396356EA78799A1347D5ADF20836EDE63580F0D436781527D7AF8E9D0239B16515900EBAB5D4D)