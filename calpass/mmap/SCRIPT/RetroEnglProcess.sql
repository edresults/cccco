
/*

-- RetroEng Reviion Documentation
---------------------------

      This script bypasses the existing [mmap].[CCTranscript] table, which the legacy script was using. The table
      contains various selector fields that are used to identify the courses to be presented for each student. The 
      selector fields needed to be re-wired in order to meet the new requirements provided by the RP Group. Since I 
      was not able to determine whether [mmap].[CCTranscript] and its selector fields were being used by any other 
      processes, I opted to create a new separate version of that table in this script. The new code is adapted from 
      [mmap].[CCTranscriptProcess], which is the stored procedure that populates [mmap].[CCTranscript].

     
     
     Rebuilding Retrospective Course Content
     ---------------------------------------
      The highschool course data was previously derived from two tables; RetrospectiveCourseContent and
      RetrospectivePerformance. The legacy code for this section needed to be updated to meet the new 
      requirements from the RP Group. I decided to create new verions of these tables for the same reason
      as with the [CCTranscript] table. 


      * [RetroMathRetrospectiveCourseContent]
         - Original table: [RetrospectiveCourseContent]
             Dependancies: [RetrospectiveCourseContentMerge] (SPROC)
                           [RetrospectiveCourseContentGet] (TVF)
          
         This script re-implements the contents of [RetrospectiveCourseContentGet] with the following noteworthy
         changes:
           - The inner join to [calpads].[CourseContent] was changed to a left join


  
      * [RetroMathRetrospectivePerformance]
         - Original table: [RetrospectivePerformance]
             Dependancies: [RetrospectivePerformanceMerge] (SPROC)
                           [RetrospectivePerformanceGet] (TVF)
          
         This script re-implements the contents of [RetrospectivePerformanceGet] with the following noteworthy
         changes:
           - 

        
      I've included a @Rebuild_RetroMathRetrospectiveCourseContent flag to allow the script to be re-run multiple
      times without incurring the extra processing time for this leading section every time the script is run. 
      The flag should be set to 1 for the initial run. For each subsequent run (for testing, or re-running for 
      a full file) the value can be set to 0. It should only need to be set back to 1 in the event that new data 
      in {{{the source tables}}} needs to be processed.

*/


  
  /*
      Indexes
      -------

      CREATE NONCLUSTERED INDEX [IX_CCTranscript_CourseLevelCode]
          ON [mmap].[CCTranscript] ([CourseLevelCode])
     INCLUDE ([CollegeCode],[StudentId],[YearTermCode],[CourseId],[CourseControlNumber],[CourseSectionId],[CourseTopCode],[CourseTitle],[CourseMarkLetter],[CourseMarkPoints],[CourseCreditCode],[CourseUnitsTry],[CourseUnitsGet],[InterSegmentKey])


      CREATE NONCLUSTERED INDEX [IX_CCTranscript_CollegeCode]
          ON [mmap].[CCTranscript] ([CollegeCode],[CourseId],[CourseControlNumber],[CourseSectionId],[CourseLevelCode])
     INCLUDE ([StudentId],[YearTermCode],[CourseTopCode],[CourseTitle],[CourseMarkLetter],[CourseMarkPoints],[CourseCreditCode],[CourseUnitsTry],[CourseUnitsGet],[InterSegmentKey])

     
      CREATE NONCLUSTERED INDEX [IX_xbsecton_collegeid]
          ON [comis].[xbsecton] ([college_id],[control_number])
     INCLUDE ([term_id],[course_id],[section_id],[first_census_count])



  */
  
  
           DECLARE @CalendarYear char(4) = '2020' --<-- FILTER USED FOR PROVIDING PARTIAL SAMPLE DATA

           DECLARE @Rebuild_RetroEnglishRetrospectiveCourseContent bit = 0

  
   IF @Rebuild_RetroEnglishRetrospectiveCourseContent <> 0
BEGIN

              DROP TABLE IF EXISTS [mmap].[RetroEnglishRetrospectiveCourseContent]
              DROP TABLE IF EXISTS [mmap].[RetroEnglishRetrospectivePerformance]


            CREATE TABLE [mmap].[RetroEnglishRetrospectiveCourseContent]
                       (
                         [InterSegmentKey]  binary(64)   NOT NULL
                       , [DepartmentCode]  tinyint       NOT NULL
                       , [GradeCode]          char(2)    NOT NULL
                       , [GradeSelector]       int       NOT NULL
                       , [SchoolCode]         char(14)   NOT NULL
                       , [YearTermCode]       char(5)    NOT NULL
                       , [CourseCode]         char(4)    NOT NULL
                       , [ContentRank]     tinyint       NOT NULL
                       , [CourseTitle]     varchar(40)   NULL
                       , [SectionMark]     varchar(3)    NOT NULL
                       , [MarkPoints]      decimal(2, 1) NOT NULL
                       , [CourseAGCode]       char(2)    NULL
                       , [CourseLevelCode]    char(2)    NULL
                       , [CourseTypeCode]     char(2)    NULL
                       , [RecencySelector]     int       NOT NULL
                       )

            CREATE TABLE [mmap].[RetroEnglishRetrospectivePerformance]
                       (
                         [InterSegmentKey]              binary(64)   NOT NULL
                       , [DepartmentCode]              tinyint NOT   NULL
                       , [GradeCode]                      char(2)    NOT NULL
                       , [IsLast]                          bit       NULL
                       , [GradePointAverage]           decimal(9, 3) NULL
                       , [CumulativeGradePointAverage] decimal(9, 3) NULL
                       )
                   
            INSERT 
              INTO [mmap].[RetroEnglishRetrospectiveCourseContent]
                 (
                   [InterSegmentKey] 
                 , [DepartmentCode]  
                 , [GradeCode]       
                 , [GradeSelector]   
                 , [SchoolCode]      
                 , [YearTermCode]    
                 , [CourseCode]      
                 , [ContentRank]     
                 , [CourseTitle]     
                 , [SectionMark]     
                 , [MarkPoints]      
                 , [CourseAGCode]    
                 , [CourseLevelCode] 
                 , [CourseTypeCode]  
                 , [RecencySelector] 
                 )
            SELECT [InterSegmentKey] =   S.[Derkey1] 
                 , [DepartmentCode]  = CRS.[DepartmentCode]
                 , [GradeCode]       =   S.[GradeLevel]
                 , [GradeSelector]   = ROW_NUMBER() OVER(
                                                         PARTITION BY  S.[DerKey1] 
                                                                      -- Recency
                                                                    , CRS.[DepartmentCode]
                                                                    ,   S.[GradeLevel]
                                                             ORDER BY   Y.[Rank]             DESC
                                                                    ,   T.[IntraTermOrdinal] DESC
                                                                      -- Rigor
                                                                    ,  CC.[Rank]             DESC
                                                                    ,   C.[CreditAttempted]  DESC
                                                                      -- Performance
                                                                    ,   M.[Rank] asc
                                                        )
                 , [SchoolCode]      =   S.[School]
                 , [YearTermCode]    =  YT.[YearTermCode]
                 , [CourseCode]      =   C.[CourseId]
                 , [ContentRank]     =  ISNULL(CC.[Rank], 99)
                 , [CourseTitle]     =   C.[CourseTitle]
                 , [SectionMark]     =   M.[MarkCode]
                 , [MarkPoints]      =   M.[Points]
                 , [CourseAGCode]    =   C.[AGstatus]
                 , [CourseLevelCode] =   C.[CourseLevel]
                 , [CourseTypeCode]  =   C.[CourseType]
                 , [RecencySelector] = ROW_NUMBER() OVER(
                                                        PARTITION BY   S.[Derkey1]
                                                                   , CRS.[DepartmentCode]
                                                            ORDER BY -- Recency
                                                                       Y.[Rank]             DESC
                                                                   ,   T.[IntraTermOrdinal] DESC
                                                                     -- Rigor
                                                                   ,  CC.[Rank]             DESC
                                                                   ,   C.[CreditAttempted]  DESC
                                                                     -- Performance
                                                                   ,   M.[Rank]             ASC
                                                       )
               FROM [dbo].[K12StudentProd]      S
               JOIN [dbo].[K12CourseProd]       C  ON   S.[School]       =   C.[School]
                                                  AND   S.[LocStudentId] =   C.[LocStudentId]
                                                  AND   S.[AcYear]       =   C.[AcYear]
               JOIN [calpads].[Mark]            M  ON   M.[MarkCode]     =   C.[Grade]
               JOIN [calpads].[Grade]           G  ON   G.[GradeCode]    =   S.[GradeLevel]
               JOIN [calpads].[Year]            Y  ON   Y.[YearCodeAbbr] =   S.[AcYear]
               JOIN [calpads].[Term]            T  ON   T.[TermCode]     =   C.[CourseTerm]
               JOIN [calpads].[YearTerm]       YT  ON  YT.[YearCode]     =   Y.[YearCode]
                                                  AND  YT.[TermCode]     =   T.[TermCode]
               JOIN [calpads].[Course]        CRS  ON CRS.[Code]         =   C.[CourseId]
          LEFT JOIN [calpads].[CourseContent]  CC  ON  CC.[CourseCode]   = CRS.[Code]
              WHERE G.[IsHS] = 1
                AND C.[CreditEarned]    <= C.[CreditAttempted] 
                AND C.[CreditAttempted] <> 99.99
                AND C.[CreditEarned]    <> 99.99

                    

            INSERT 
              INTO [mmap].[RetroEnglishRetrospectivePerformance]
                 (
                   [InterSegmentKey]            
                 , [DepartmentCode]             
                 , [GradeCode]                  
                 , [IsLast]                     
                 , [GradePointAverage]          
                 , [CumulativeGradePointAverage]
                 )
            SELECT [InterSegmentKey]           = [InterSegmentKey]
                 , DepartmentCode
                 , GradeCode
                 , IsLast                      = case 
                                                   when row_number() over( 
                                                                          partition by [InterSegmentKey]
                                                                                     , DepartmentCode 
                                                                              order by LastYearTermCode DESC
                                                                                     , convert(tinyint, GradeCode) DESC
                                                                         ) = 1 
                                                   then 1 
                                                   else 0 
                                                 end
                 , GradePointAverage           = CONVERT(decimal(9,3), isnull(QualityPoints / nullif(CreditAttempted, 0), 0))
                 , CumulativeGradePointAverage = CONVERT(
                                                          decimal(9,3)
                                                        , isnull(
                                                                  sum(QualityPoints) over(
                                                                                          partition by [InterSegmentKey]
                                                                                                     , DepartmentCode 
                                                                                              order by GradeCode
                                                                                         ) 
                                                                / nullif(
                                                                          sum(CreditAttempted) over(
                                                                                                    partition by [InterSegmentKey]
                                                                                                               , DepartmentCode 
                                                                                                        order by GradeCode
                                                                                                   )
                                                                        , 0
                                                                        )
                                                               , 0
                                                               )
                                                        )
              FROM (
                    SELECT [InterSegmentKey]   = S.[Derkey1]
                         , DepartmentCode      = isnull(cc.DepartmentCode, 0)
                         , GradeCode           = g.GradeCode
                         , QualityPoints       = sum(c.CreditEarned * m.Points)
                         , CreditAttempted     = sum(c.CreditAttempted)        
                         , LastYearTermCode    = max(yt.YearTermCode)          
                      FROM dbo.K12StudentProd S
                      JOIN dbo.K12CourseProd  C  ON s.School       = c.School
                                                AND s.LocStudentId = c.LocStudentId
                                                AND s.AcYear       = c.AcYear
                      JOIN calpads.Mark       M  ON m.MarkCode = c.Grade
                      JOIN calpads.Grade      G  ON g.GradeCode = s.GradeLevel
                      JOIN calpads.Course    CC  ON cc.Code = c.CourseId
                      JOIN calpads.Year       Y  ON y.YearCodeAbbr = s.AcYear
                      JOIN calpads.YearTerm  YT  ON yt.YearCode = y.YearCode
                                                AND yt.TermCode = c.CourseTerm
                     WHERE G.IsHS             = 1
                       AND M.IsGpa            = 1
                       AND C.CreditEarned    <= c.CreditAttempted
                       AND C.CreditAttempted <> 99.99
                       AND C.CreditEarned    <> 99.99
                     GROUP BY ROLLUP (s.Derkey1, g.GradeCode, cc.DepartmentCode)
                    HAVING (
                               (
                                    cc.DepartmentCode IS NULL
                                AND grouping_id(cc.DepartmentCode) = 1
                               ) 
                            OR cc.DepartmentCode IN (14, 18)
                           ) 
                       AND NOT (
                                    grouping_id(g.GradeCode)       = 1 
                                AND grouping_id(cc.DepartmentCode) = 1
                               )
                   ) X

END

          


               SET NOCOUNT ON
               
              DROP TABLE IF EXISTS [#CCTranscriptWithNewSelectors]
              DROP TABLE IF EXISTS [#EnglishCourses]
              DROP TABLE IF EXISTS [#StudentCourse]


            CREATE TABLE [#CCTranscriptWithNewSelectors]
                       (
                         [CollegeCode]             char(3)    NOT NULL
                       , [StudentId]               char(9)    NOT NULL
                       , [YearTermCode]             int NOT   NULL
                       , [CourseId]             varchar(12)   NOT NULL
                       , [CourseControlNumber]     char(12)   NOT NULL
                       , [CourseSectionId]      varchar(12)   NOT NULL
                       , [CourseTopCode]           char(6)    NOT NULL
                       , [CourseTitle]          varchar(68)   NOT NULL
                       , [CourseMarkLetter]     varchar(3)    NOT NULL
                       , [CourseMarkPoints]     decimal(4, 2) NOT NULL
                       , [CourseLevelCode]         char(1)    NOT NULL
                       , [CourseCreditCode]        char(1)    NOT NULL
                       , [CourseUnitsTry]       decimal(4, 2) NOT NULL
                       , [CourseUnitsGet]       decimal(4, 2) NOT NULL
                       , [InterSegmentKey]       binary(64)   NOT NULL
                       , [NewSelector]         smallint NULL
                       , [NewSelectorFL]       smallint NULL
                       )



            CREATE TABLE [#EnglishCourses]
                         (
                           [CourseControlNumber]  char(12)
                         , [YearTermCodeMin]      char(5)
                         , [YearTermCodeMax]      char(5) 
                         , [EnglGateway]          int
                         , [EnglCriticalThinking] int
                         , [EnglTLEE]             int
                         , [EnglELL]              int
                         , [EnglEnhanced]         int
                         )

            CREATE INDEX IX_EnglishCourses_MathCourse  ON [#EnglishCourses] ([CourseControlNumber], [YearTermCodeMin], [YearTermCodeMax])    



            CREATE TABLE [#StudentCourse]
                         (
                           [InterSegmentKey]        binary(64)
                         , [CourseLevel]           varchar(12)
                         , [IsFirstLevel]              bit DEFAULT 0
                         , [CollegeCode]              char(3)
                         , [YearTermCode]              int 
                         , [CourseId]              varchar(12) 
                         , [CourseSectionId]       varchar(12)
                         , [CourseControlNumber]      char(12)
                         , [CourseTopCode]            char(6) 
                         , [CourseTitle]           varchar(68)
                         , [CourseMarkLetter]      varchar(3) 
                         , [CourseMarkPoints]      decimal(4,2) 
                         , [CourseLevelCode]          char(1)
                         , [CourseUnitsTry]        decimal(4,2) 
                         , [CourseUnitsGet]        decimal(4,2) 
                         , [EnglGateway]               int
                         , [EnglCriticalThinking]      int
                         , [EnglTLEE]                  int
                         , [EnglELL]                   int
                         , [EnglEnhanced]              int
                         , [SectionEnrollmentCount]    int
                         , [GradeRank]              decimal(4,2)
                         , [IsCoreqCourse]             bit DEFAULT 0
                         )
                         
            CREATE INDEX IX_StudentCourse_CourseLevelTerm  ON [#StudentCourse] ([InterSegmentKey], [CourseLevel], [CollegeCode], [YearTermCode]) INCLUDE ([IsCoreqCourse])    
            CREATE INDEX IX_StudentCourse_CourseLevelRank  ON [#StudentCourse] ([InterSegmentKey], [CourseLevel], [EnglGateway], [GradeRank], [CourseUnitsGet])    
 


            INSERT 
              INTO [#EnglishCourses]
                 (
                   [CourseControlNumber]
                 , [YearTermCodeMin]    
                 , [YearTermCodeMax]     
                 , [EnglGateway]          
                 , [EnglCriticalThinking] 
                 , [EnglTLEE]             
                 , [EnglELL]              
                 , [EnglEnhanced]         
                 )
            SELECT [CourseControlNumber] = [CCENCRCourseControlNumber]
                 , [YearTermCodeMin]     = CASE LTRIM(RTRIM([CCENCRYearTermCode_min]))
                                                  WHEN 'NULL' THEN 0
                                                  ELSE [CCENCRYearTermCode_min]
                                           END
                    -- {latest term will be treated as open-ended} --
                 , [YearTermCodeMax]     = CASE LTRIM(RTRIM([CCENCRYearTermCode_max]))
                                                  WHEN 'NULL'                   THEN '99999'
                                                  WHEN [CourseYearTermCode_Max] THEN '99999'
                                                  ELSE [CCENCRYearTermCode_max]
                                           END
                 , [EnglGateway]          
                 , [EnglCriticalThinking] 
                 , [EnglTLEE]             
                 , [EnglELL]              
                 , [EnglEnhanced]         
              FROM (
                    SELECT *
                         , [CourseYearTermCode_Max] = MAX([CCENCRYearTermCode_max]) OVER (PARTITION BY [CCENCRCourseControlNumber]) 
                     FROM [dbo].[English_Courses] 
                   ) X
                   
     

             ;WITH [cte_CCTranscriptWithNewSelectors]
                AS (
                       SELECT A.*
                            , [NewSelector]   = ROW_NUMBER() OVER (
                                                                   PARTITION BY A.[InterSegmentKey]
                                                                              , CASE 
                                                                                  WHEN A.[CourseTopCode] NOT IN (
                                                                                                                  '150100'  -- English                                                     
                                                                                                                , '152000'  -- Reading                                                     
                                                                                                                , '493087'  -- English as a Second Language - Integrated                   
                                                                                                                , '493084'  -- English as a Second Language - Writing                      
                                                                                                                , '493085'  -- English as a Second Language - Reading                      
                                                                                                                , '493086'  -- English as a Second Language - Listening and Speaking       
                                                                                                                , '493100'  -- Vocational ESL                                              
                                                                                                                , '493090'  -- Citizenship / ESL Civics                                    
                                                                                                                ) 
                                                                                   THEN '170100' -- Mathematics, General                                                       
                                                                                   ELSE A.[CourseTopCode] 
                                                                                END    
                                                                              , B.[Type]
                                                                              , A.[CourseLevelCode]
                                                                       ORDER BY A.[YearTermCode]      ASC 
                                                                              , A.[CourseUnitsTry]   DESC
                                                                              , A.[CourseMarkPoints] DESC
                                                                  )
                            , [NewSelectorFL] = ROW_NUMBER() OVER (
                                                                   PARTITION BY A.[InterSegmentKey]
                                                                              , CASE 
                                                                                  WHEN A.[CourseTopCode] NOT IN (
                                                                                                                  '150100'  -- English                                                     
                                                                                                                , '152000'  -- Reading                                                     
                                                                                                                , '493087'  -- English as a Second Language - Integrated                   
                                                                                                                , '493084'  -- English as a Second Language - Writing                      
                                                                                                                , '493085'  -- English as a Second Language - Reading                      
                                                                                                                , '493086'  -- English as a Second Language - Listening and Speaking       
                                                                                                                , '493100'  -- Vocational ESL                                              
                                                                                                                , '493090'  -- Citizenship / ESL Civics                                    
                                                                                                                ) 
                                                                                   THEN '170100' -- Mathematics, General                                                       
                                                                                   ELSE A.[CourseTopCode] 
                                                                                END    
                                                                              , B.[Type]
                                                                       ORDER BY A.[YearTermCode]      ASC 
                                                                              , C.[Ordinal]           ASC
                                                                              , A.[CourseUnitsTry]   DESC
                                                                              , A.[CourseMarkPoints] DESC
                                                                  )
                         FROM [mmap].[CCTranscript] A
                         JOIN [mmap].[Credits]      B  ON A.[CourseCreditCode] = B.[Code]
                         JOIN [mmap].[Levels]       C  ON A.[CourseLevelCode]  = C.[Code]
                    LEFT JOIN [mmap].[Programs]     D  ON A.[CourseTopCode]    = D.[Code]
                         JOIN [comis].[Term]        E  ON A.[YearTermCode]        = E.[YearTermCode]
                         JOIN [comis].[xbsecton]    F  ON A.[CollegeCode]         = F.[college_id]
                                                      AND E.[TermCode]            = F.[term_id]
                                                      AND A.[CourseId]            = F.[course_id]                                                --<-- SHOULD I REMOVE THE JOIN ON THIS FIELD?
                                                      AND A.[CourseControlNumber] = F.[control_number]
                                                      AND A.[CourseSectionId]     = F.[section_id] 
                        WHERE (
                                  A.[CourseTopCode] = '150100' -- English
                              )
                          AND A.[CourseLevelCode] IN ('Y','A','B','C','D')
                          AND B.[Type] = 'CR'
                          AND F.[units_min] >= 3
                          AND NOT EXISTS ( --{COREQUISITES ARE EXCLUDED HERE AND WILL BE JOINED LATER}--
                                          SELECT * 
                                            FROM [calpass].[dbo].[English_Corequisite_Support_Courses]
                                           WHERE [CollegeCode]         = A.[CollegeCode]
                                             AND [CourseControlNumber] = A.[CourseControlNumber]
                                         )
                   )   

            INSERT
              INTO [#CCTranscriptWithNewSelectors]
                 (
                   [CollegeCode]        
                 , [StudentId]          
                 , [YearTermCode]       
                 , [CourseId]           
                 , [CourseControlNumber]
                 , [CourseSectionId]    
                 , [CourseTopCode]      
                 , [CourseTitle]        
                 , [CourseMarkLetter]   
                 , [CourseMarkPoints]   
                 , [CourseLevelCode]    
                 , [CourseCreditCode]   
                 , [CourseUnitsTry]     
                 , [CourseUnitsGet]     
                 , [InterSegmentKey]    
                 , [NewSelector]        
                 , [NewSelectorFL]
                 )
            SELECT 
                   [CollegeCode]        
                 , [StudentId]          
                 , [YearTermCode]       
                 , [CourseId]           
                 , [CourseControlNumber]
                 , [CourseSectionId]    
                 , [CourseTopCode]      
                 , [CourseTitle]        
                 , [CourseMarkLetter]   
                 , [CourseMarkPoints]   
                 , [CourseLevelCode]    
                 , [CourseCreditCode]   
                 , [CourseUnitsTry]     
                 , [CourseUnitsGet]     
                 , [InterSegmentKey]    
                 , [NewSelector]        
                 , [NewSelectorFL]
              FROM [cte_CCTranscriptWithNewSelectors]



            --SELECT * FROM [#CCTranscriptWithNewSelectors] WHERE [InterSegmentKey] = 0x00F2ADA89E0676DBB0D37C7A65367E084EE147D80C884E94A95D5520B7F7AD685A5AFE348A06516C472D0DA80CCF1845A62E134DD8E688052EAD0E20A1D990AD

                        
CREATE NONCLUSTERED INDEX [IX_CCTranscriptTemp_NewSelectorFL]
ON [#CCTranscriptWithNewSelectors] ([NewSelectorFL])
INCLUDE ([YearTermCode],[InterSegmentKey])


CREATE NONCLUSTERED INDEX [IX_CCTranscriptTemp_YearTermCode]
ON [#CCTranscriptWithNewSelectors] ([YearTermCode],[NewSelectorFL])
INCLUDE ([InterSegmentKey])


               SET NOCOUNT OFF


             ;WITH [cteStudentCourse]
                AS (
                    SELECT [InterSegmentKey]        = A.[InterSegmentKey]
                         , [CourseLevel]            = CASE 
                                                        WHEN A.[CourseLevelCode] = 'Y' AND A.[NewSelector] = 1 THEN 'CCENCR00'
                                                        WHEN A.[CourseLevelCode] = 'Y' AND A.[NewSelector] = 2 THEN 'CCENCR002N'
                                                        WHEN A.[CourseLevelCode] = 'A' AND A.[NewSelector] = 1 THEN 'CCENCR01'
                                                        WHEN A.[CourseLevelCode] = 'B' AND A.[NewSelector] = 1 THEN 'CCENCR02'
                                                        WHEN A.[CourseLevelCode] = 'C' AND A.[NewSelector] = 1 THEN 'CCENCR03'
                                                        WHEN A.[CourseLevelCode] = 'D' AND A.[NewSelector] = 1 THEN 'CCENCR04'
                                                      END
                         , [IsFirstLevel]           = CASE WHEN A.[NewSelectorFL] = 1 THEN 1 ELSE 0 END
                         , [CollegeCode]            = A.[CollegeCode]
                         , [YearTermCode]           = A.[YearTermCode]
                         , [CourseId]               = A.[CourseId]
                         , [CourseSectionId]        = A.[CourseSectionId]
                         , [CourseControlNumber]    = A.[CourseControlNumber]
                         , [CourseTopCode]          = A.[CourseTopCode]
                         , [CourseTitle]            = A.[CourseTitle]
                         , [CourseMarkLetter]       = A.[CourseMarkLetter]
                         , [CourseMarkPoints]       = A.[CourseMarkPoints]
                         , [CourseLevelCode]        = A.[CourseLevelCode]   
                         , [CourseUnitsTry]         = A.[CourseUnitsTry]   
                         , [CourseUnitsGet]         = A.[CourseUnitsGet]
                         , [EnglGateway]            = D.[EnglGateway]         
                         , [EnglCriticalThinking]   = D.[EnglCriticalThinking]
                         , [EnglTLEE]               = D.[EnglTLEE]            
                         , [EnglELL]                = D.[EnglELL]             
                         , [EnglEnhanced]           = D.[EnglEnhanced]        
                         , [SectionEnrollmentCount] = C.[first_census_count]
                         , [GradeRank]              = E.[rank]
                      FROM [#CCTranscriptWithNewSelectors] A
                      JOIN [mmap].[Credits]                B  ON A.[CourseCreditCode]    = B.[Code]
                      JOIN [comis].[Term]                  F  ON A.[YearTermCode]        = F.[YearTermCode]
                      JOIN [comis].[xbsecton]              C  ON A.[CollegeCode]         = C.[college_id]
                                                             AND F.[TermCode]            = C.[term_id]
                                                             AND A.[CourseId]            = C.[course_id]                                                --<-- SHOULD I REMOVE THE JOIN ON THIS FIELD?
                                                             AND A.[CourseControlNumber] = C.[control_number]
                                                             AND A.[CourseSectionId]     = C.[section_id]    
                 LEFT JOIN [#EnglishCourses]               D  ON A.[CourseControlNumber] = D.[CourseControlNumber]
                                                             AND A.[YearTermCode] BETWEEN D.[YearTermCodeMin] AND D.[YearTermCodeMax]  
                 LEFT JOIN [comis].[Grade]                 E  ON A.[CourseMarkLetter]    = E.[GradeCode]
                 LEFT JOIN [comis].[cbcrsinv]              G  ON A.[CollegeCode]             = G.[college_id]
                                                             AND F.[TermCode]                = G.[term_id]
                                                             AND A.[CourseId]                = G.[course_id]
                                                             AND A.[CourseControlNumber]     = G.[control_number]
                     WHERE EXISTS ( -- only include students who took a first-level course in the filter year
                                   SELECT *
                                     FROM [#CCTranscriptWithNewSelectors] AA
                                     JOIN [comis].[Term]                  BB ON AA.[YearTermCode] = BB.[YearTermCode]
                                    WHERE [InterSegmentKey] = A.[InterSegmentKey]
                                      AND [CalendarYear] = ISNULL(@CalendarYear, [CalendarYear])   --<-- FILTER USED FOR PROVIDING PARTIAL SAMPLE DATA
                                      AND [NewSelectorFL] = 1                                      --<-- First-level course should be within the specified date filter
                                  )
                  )

  --       SELECT * FROM [StudentCourse] WHERE [InterSegmentKey] = 0x00F2ADA89E0676DBB0D37C7A65367E084EE147D80C884E94A95D5520B7F7AD685A5AFE348A06516C472D0DA80CCF1845A62E134DD8E688052EAD0E20A1D990AD

                              
            INSERT 
              INTO [#StudentCourse]
                 (
                   [InterSegmentKey]    
                 , [CourseLevel]        
                 , [IsFirstLevel]
                 , [CollegeCode]        
                 , [YearTermCode]       
                 , [CourseId]           
                 , [CourseSectionId]    
                 , [CourseControlNumber]
                 , [CourseTopCode]      
                 , [CourseTitle]        
                 , [CourseMarkLetter]   
                 , [CourseMarkPoints]    
                 , [CourseLevelCode]    
                 , [CourseUnitsTry]      
                 , [CourseUnitsGet]     
                 , [EnglGateway]          
                 , [EnglCriticalThinking] 
                 , [EnglTLEE]             
                 , [EnglELL]              
                 , [EnglEnhanced]         
                 , [SectionEnrollmentCount]   
                 , [GradeRank]
                 )
            SELECT [InterSegmentKey]    
                 , [CourseLevel]        
                 , [IsFirstLevel]
                 , [CollegeCode]        
                 , [YearTermCode]       
                 , [CourseId]           
                 , [CourseSectionId]    
                 , [CourseControlNumber]
                 , [CourseTopCode]      
                 , [CourseTitle]        
                 , [CourseMarkLetter]   
                 , [CourseMarkPoints]    
                 , [CourseLevelCode]    
                 , [CourseUnitsTry]      
                 , [CourseUnitsGet]     
                 , [EnglGateway]          
                 , [EnglCriticalThinking] 
                 , [EnglTLEE]             
                 , [EnglELL]              
                 , [EnglEnhanced]         
                 , [SectionEnrollmentCount]   
                 , [GradeRank]
              FROM (
                    SELECT * -- 0x08E37F63C06F02F5F981A3C2C76040F08F3E75A5C957AF5B86A3C12A08F5CA04E060DF0CAC25C57DEE02A07BB25E1F8C56FB3302A15D3656A98E5462860353A5
                         , [CourseTermPriority] = ROW_NUMBER() OVER (PARTITION BY [InterSegmentKey]
                                                                                , [CourseLevel]
                                                                         ORDER BY ISNULL([EnglGateway],0.5) DESC  --<-- If MathCourses match was not found, set priority between 0 and 1
                                                                                , [GradeRank]
                                                                                , [CourseUnitsGet] DESC 
                                                                                )
                    
                      FROM [cteStudentCourse]
                   ) X
             WHERE [CourseTermPriority] = 1
               AND [CourseLevel] IS NOT NULL 


            -- SELECT * FROM [#StudentCourse] WHERE [InterSegmentKey] = 0x00F2ADA89E0676DBB0D37C7A65367E084EE147D80C884E94A95D5520B7F7AD685A5AFE348A06516C472D0DA80CCF1845A62E134DD8E688052EAD0E20A1D990AD

             ;WITH [StudentCourseCoreq]
                AS (
                    SELECT [InterSegmentKey]        = A.[InterSegmentKey]
                         , [CollegeCode]            = A.[CollegeCode]
                         , [YearTermCode]           = A.[YearTermCode]
                         , [CourseId]               = A.[CourseId]
                         , [CourseSectionId]        = A.[CourseSectionId]
                         , [CourseControlNumber]    = A.[CourseControlNumber]
                         , [CourseTopCode]          = A.[CourseTopCode]
                         , [CourseTitle]            = A.[CourseTitle]
                         , [CourseMarkLetter]       = A.[CourseMarkLetter]
                         , [CourseMarkPoints]       = A.[CourseMarkPoints]
                         , [CourseLevelCode]        = A.[CourseLevelCode]   
                         , [CourseUnitsTry]         = A.[CourseUnitsTry]   
                         , [CourseUnitsGet]         = A.[CourseUnitsGet]
                         , [EnglGateway]            = D.[EnglGateway]         
                         , [EnglCriticalThinking]   = D.[EnglCriticalThinking]
                         , [EnglTLEE]               = D.[EnglTLEE]            
                         , [EnglELL]                = D.[EnglELL]             
                         , [EnglEnhanced]           = D.[EnglEnhanced]        
                         , [SectionEnrollmentCount] = C.[first_census_count]
                         , [GradeRank]              = E.[rank]
                      FROM [mmap].[CCTranscript] A 
                      JOIN [mmap].[Credits]      B  ON A.[CourseCreditCode]    = B.[Code]
                      JOIN [comis].[Term]        F  ON A.[YearTermCode]        = F.[YearTermCode]
                      JOIN [comis].[xbsecton]    C  ON A.[CollegeCode]         = C.[college_id]
                                                   AND F.[TermCode]            = C.[term_id]
                                                   AND A.[CourseId]            = C.[course_id]                                                --<-- SHOULD I REMOVE THE JOIN ON THIS FIELD?
                                                   AND A.[CourseControlNumber] = C.[control_number]
                                                   AND A.[CourseSectionId]     = C.[section_id]     
                 LEFT JOIN [#EnglishCourses]     D  ON A.[CourseControlNumber] = D.[CourseControlNumber]
                                                   AND A.[YearTermCode] BETWEEN D.[YearTermCodeMin] AND D.[YearTermCodeMax]  
                 LEFT JOIN [comis].[Grade]       E  ON A.[CourseMarkLetter]    = E.[GradeCode]
                     WHERE A.[CourseTopCode] NOT IN (
                                                      '150100' -- English                                                
                                                    , '152000' -- Reading                                                
                                                    , '493087' -- English as a Second Language - Integrated              
                                                    , '493084' -- English as a Second Language - Writing                 
                                                    , '493085' -- English as a Second Language - Reading                 
                                                    , '493086' -- English as a Second Language - Listening and Speaking  
                                                    , '493100' -- Vocational ESL                                         
                                                    , '493090' -- Citizenship / ESL Civics                               
                                                    )
                       AND A.[CourseLevelCode] IN ('Y','A','B','C','D')
                       AND EXISTS ( --{COREQUISITES}--
                                   SELECT * 
                                     FROM [calpass].[dbo].[English_Corequisite_Support_Courses]
                                    WHERE [CollegeCode]         = A.[CollegeCode]
                                      AND [CourseControlNumber] = A.[CourseControlNumber]
                                  )
                   )


                              
            INSERT 
              INTO [#StudentCourse]
                 (
                   [InterSegmentKey]    
                 , [CollegeCode]        
                 , [YearTermCode]       
                 , [CourseId]           
                 , [CourseSectionId]    
                 , [CourseControlNumber]
                 , [CourseTopCode]      
                 , [CourseTitle]        
                 , [CourseMarkLetter]   
                 , [CourseMarkPoints]    
                 , [CourseLevelCode]    
                 , [CourseUnitsTry]      
                 , [CourseUnitsGet]     
                 , [EnglGateway]          
                 , [EnglCriticalThinking] 
                 , [EnglTLEE]             
                 , [EnglELL]              
                 , [EnglEnhanced]         
                 , [SectionEnrollmentCount]   
                 , [GradeRank]
                 , [IsCoreqCourse]
                 )
            SELECT [InterSegmentKey]    
                 , [CollegeCode]        
                 , [YearTermCode]       
                 , [CourseId]           
                 , [CourseSectionId]    
                 , [CourseControlNumber]
                 , [CourseTopCode]      
                 , [CourseTitle]        
                 , [CourseMarkLetter]   
                 , [CourseMarkPoints]    
                 , [CourseLevelCode]    
                 , [CourseUnitsTry]      
                 , [CourseUnitsGet]      
                 , [EnglGateway]          
                 , [EnglCriticalThinking] 
                 , [EnglTLEE]             
                 , [EnglELL]              
                 , [EnglEnhanced]         
                 , [SectionEnrollmentCount]   
                 , [GradeRank]
                 , [IsCoreqCourse] = 1
              FROM (
                    SELECT * --0x08E37F63C06F02F5F981A3C2C76040F08F3E75A5C957AF5B86A3C12A08F5CA04E060DF0CAC25C57DEE02A07BB25E1F8C56FB3302A15D3656A98E5462860353A5
                         , [CourseTermPriority] = ROW_NUMBER() OVER (PARTITION BY [InterSegmentKey]
                                                                              , [YearTermCode]                  --<-- Additional partitioning for corequisite courses 
                                                                         ORDER BY ISNULL([EnglGateway],0.5) DESC  --<-- If MathCourses match was not found, set priority between 0 and 1
                                                                              , [GradeRank]
                                                                              , [CourseUnitsGet] DESC 
                                                                              )
                    
                      FROM [StudentCourseCoreq]
                   ) X
             WHERE [CourseTermPriority] = 1

/*
            SELECT *
              INTO [dbo].[StudentCourse]
              FROM [#StudentCourse]
*/

--/*
-------------------------------------------------------------------
         TRUNCATE TABLE [mmap].[RetroEnglUpdated]
-------------------------------------------------------------------
 

             ;WITH [cteStudents]
                AS (
                    SELECT DISTINCT [InterSegmentKey]
                      FROM [#StudentCourse]
                     WHERE [IsFirstLevel] = 1   --<-- only include students with a first-level course within the specified date filter
                   )
                 , [Courses]
                AS (
                       SELECT [InterSegmentKey]             = A.[InterSegmentKey]  
                            , [CourseLevel]                 = A.[CourseLevel]
                            , [IsFirstLevel]                = A.[IsFirstLevel]
                            , [MainCollegeCode]             = A.[CollegeCode]            
                            , [MainYearTermCode]            = A.[YearTermCode]       
                            , [MainCourseId]                = A.[CourseId]           
                            , [MainCourseSectionId]         = A.[CourseSectionId]    
                            , [MainCourseControlNumber]     = A.[CourseControlNumber]
                            , [MainCourseTopCode]           = A.[CourseTopCode]      
                            , [MainCourseTitle]             = A.[CourseTitle]        
                            , [MainCourseMarkLetter]        = A.[CourseMarkLetter]   
                            , [MainCourseMarkPoints]        = A.[CourseMarkPoints]    
                            , [MainCourseLevelCode]         = A.[CourseLevelCode]    
                            , [MainCourseUnitsTry]          = A.[CourseUnitsTry]      
                            , [MainCourseUnitsGet]          = A.[CourseUnitsGet]     
                            , [MainEnglGateway]             = A.[EnglGateway]         
                            , [MainEnglCriticalThinking]    = A.[EnglCriticalThinking]
                            , [MainEnglTLEE]                = A.[EnglTLEE]            
                            , [MainEnglELL]                 = A.[EnglELL]             
                            , [MainEnglEnhanced]            = A.[EnglEnhanced]                         
                            , [MainSectionEnrollmentCount]  = A.[SectionEnrollmentCount]
                            , [CoreqCollegeCode]            = B.[CollegeCode]            
                            , [CoreqYearTermCode]           = B.[YearTermCode]       
                            , [CoreqCourseId]               = B.[CourseId]           
                            , [CoreqCourseSectionId]        = B.[CourseSectionId]    
                            , [CoreqCourseControlNumber]    = B.[CourseControlNumber]
                            , [CoreqCourseTopCode]          = B.[CourseTopCode]      
                            , [CoreqCourseTitle]            = B.[CourseTitle]        
                            , [CoreqCourseMarkLetter]       = B.[CourseMarkLetter]   
                            , [CoreqCourseMarkPoints]       = B.[CourseMarkPoints]    
                            , [CoreqCourseLevelCode]        = B.[CourseLevelCode]    
                            , [CoreqCourseUnitsTry]         = B.[CourseUnitsTry]      
                            , [CoreqCourseUnitsGet]         = B.[CourseUnitsGet]
                            , [CoreqEnglGateway]            = B.[EnglGateway]         
                            , [CoreqEnglCriticalThinking]   = B.[EnglCriticalThinking]
                            , [CoreqEnglTLEE]               = B.[EnglTLEE]            
                            , [CoreqEnglELL]                = B.[EnglELL]             
                            , [CoreqEnglEnhanced]           = B.[EnglEnhanced]                   
                            , [CoreqSectionEnrollmentCount] = B.[SectionEnrollmentCount]
                         FROM [#StudentCourse] A
                    LEFT JOIN [#StudentCourse] B  ON A.[InterSegmentKey] = B.[InterSegmentKey]
                                                 AND A.[CollegeCode]     = B.[CollegeCode]
                                                 AND A.[YearTermCode]    = B.[YearTermCode]
                                                 AND B.[IsCoreqCourse]   = 1
                        WHERE A.[IsCoreqCourse] = 0
                          AND EXISTS (SELECT * FROM [cteStudents] WHERE [InterSegmentKey] = A.[InterSegmentKey])
                   )       
                   
                 --SELECT COUNT (*) FROM [Courses]

                 --SELECT * FROM [Courses] WHERE [InterSegmentKey] = 0x00F2ADA89E0676DBB0D37C7A65367E084EE147D80C884E94A95D5520B7F7AD685A5AFE348A06516C472D0DA80CCF1845A62E134DD8E688052EAD0E20A1D990AD
                 --SELECT * FROM [#StudentCourse] WHERE [InterSegmentKey] = 0x00F2ADA89E0676DBB0D37C7A65367E084EE147D80C884E94A95D5520B7F7AD685A5AFE348A06516C472D0DA80CCF1845A62E134DD8E688052EAD0E20A1D990AD
                   

            INSERT 
              INTO [mmap].[RetroEnglUpdated]
                 (
                   [InterSegmentKey]                     
                 , [RequestStudentId]                     
                 , [CCENCRFLCollegeCode]                 
                 , [CCENCRFLYearTermCode]                
                 , [CCENCRFLCourseId]                    
                 , [CCENCRFLSectionId]                   
                 , [CCENCRFLCourseControlNumber]         
                 , [CCENCRFLCourseTopCode]               
                 , [CCENCRFLCourseTitle]                 
                 , [CCENCRFLCourseMarkLetter]            
                 , [CCENCRFLCourseMarkPoints]            
                 , [CCENCRFLCourseLevelCode]             
                 , [CCENCRFLUnitsAttempt]                
                 , [CCENCRFLUnitsSuccess]       
                 , [CCENCRFLEnglGateway]          
                 , [CCENCRFLEnglCriticalThinking] 
                 , [CCENCRFLEnglTLEE]             
                 , [CCENCRFLEnglELL]              
                 , [CCENCRFLEnglEnhanced]                   
                 , [CCENCRFLSectionEnrollmentCount]       
                 , [CCENCR00CollegeCode]                 
                 , [CCENCR00YearTermCode]                
                 , [CCENCR00CourseId]                    
                 , [CCENCR00SectionId]                   
                 , [CCENCR00CourseControlNumber]         
                 , [CCENCR00CourseTopCode]               
                 , [CCENCR00CourseTitle]                 
                 , [CCENCR00CourseMarkLetter]            
                 , [CCENCR00CourseMarkPoints]            
                 , [CCENCR00CourseLevelCode]             
                 , [CCENCR00UnitsAttempt]                
                 , [CCENCR00UnitsSuccess]      
                 , [CCENCR00EnglGateway]          
                 , [CCENCR00EnglCriticalThinking] 
                 , [CCENCR00EnglTLEE]             
                 , [CCENCR00EnglELL]              
                 , [CCENCR00EnglEnhanced]                   
                 , [CCENCR00SectionEnrollmentCount]      
                 , [CCENCR002NCollegeCode]               
                 , [CCENCR002NYearTermCode]              
                 , [CCENCR002NCourseId]                  
                 , [CCENCR002NSectionId]                 
                 , [CCENCR002NCourseControlNumber]       
                 , [CCENCR002NCourseTopCode]             
                 , [CCENCR002NCourseTitle]               
                 , [CCENCR002NCourseMarkLetter]          
                 , [CCENCR002NCourseMarkPoints]          
                 , [CCENCR002NCourseLevelCode]           
                 , [CCENCR002NUnitsAttempt]              
                 , [CCENCR002NUnitsSuccess]      
                 , [CCENCR002NEnglGateway]          
                 , [CCENCR002NEnglCriticalThinking] 
                 , [CCENCR002NEnglTLEE]             
                 , [CCENCR002NEnglELL]              
                 , [CCENCR002NEnglEnhanced]                 
                 , [CCENCR002NSectionEnrollmentCount]    
                 , [CCENCR01CollegeCode]                 
                 , [CCENCR01YearTermCode]                
                 , [CCENCR01CourseId]                    
                 , [CCENCR01SectionId]                   
                 , [CCENCR01CourseControlNumber]         
                 , [CCENCR01CourseTopCode]               
                 , [CCENCR01CourseTitle]                 
                 , [CCENCR01CourseMarkLetter]            
                 , [CCENCR01CourseMarkPoints]            
                 , [CCENCR01CourseLevelCode]             
                 , [CCENCR01UnitsAttempt]                
                 , [CCENCR01UnitsSuccess]
                 , [CCENCR01EnglGateway]          
                 , [CCENCR01EnglCriticalThinking] 
                 , [CCENCR01EnglTLEE]             
                 , [CCENCR01EnglELL]              
                 , [CCENCR01EnglEnhanced]                         
                 , [CCENCR01SectionEnrollmentCount]      
                 , [CCENCR02CollegeCode]                 
                 , [CCENCR02YearTermCode]                
                 , [CCENCR02CourseId]                    
                 , [CCENCR02SectionId]                   
                 , [CCENCR02CourseControlNumber]         
                 , [CCENCR02CourseTopCode]               
                 , [CCENCR02CourseTitle]                 
                 , [CCENCR02CourseMarkLetter]            
                 , [CCENCR02CourseMarkPoints]            
                 , [CCENCR02CourseLevelCode]             
                 , [CCENCR02UnitsAttempt]                
                 , [CCENCR02UnitsSuccess]
                 , [CCENCR02EnglGateway]          
                 , [CCENCR02EnglCriticalThinking] 
                 , [CCENCR02EnglTLEE]             
                 , [CCENCR02EnglELL]              
                 , [CCENCR02EnglEnhanced]                         
                 , [CCENCR02SectionEnrollmentCount]      
                 , [CCENCR03CollegeCode]                 
                 , [CCENCR03YearTermCode]                
                 , [CCENCR03CourseId]                    
                 , [CCENCR03SectionId]                   
                 , [CCENCR03CourseControlNumber]         
                 , [CCENCR03CourseTopCode]               
                 , [CCENCR03CourseTitle]                 
                 , [CCENCR03CourseMarkLetter]            
                 , [CCENCR03CourseMarkPoints]            
                 , [CCENCR03CourseLevelCode]             
                 , [CCENCR03UnitsAttempt]                
                 , [CCENCR03UnitsSuccess]
                 , [CCENCR03EnglGateway]          
                 , [CCENCR03EnglCriticalThinking] 
                 , [CCENCR03EnglTLEE]             
                 , [CCENCR03EnglELL]              
                 , [CCENCR03EnglEnhanced]                         
                 , [CCENCR03SectionEnrollmentCount]      
                 , [CCENCR04CollegeCode]                 
                 , [CCENCR04YearTermCode]                
                 , [CCENCR04CourseId]                    
                 , [CCENCR04SectionId]                   
                 , [CCENCR04CourseControlNumber]         
                 , [CCENCR04CourseTopCode]               
                 , [CCENCR04CourseTitle]                 
                 , [CCENCR04CourseMarkLetter]            
                 , [CCENCR04CourseMarkPoints]            
                 , [CCENCR04CourseLevelCode]             
                 , [CCENCR04UnitsAttempt]                
                 , [CCENCR04UnitsSuccess]
                 , [CCENCR04EnglGateway]          
                 , [CCENCR04EnglCriticalThinking] 
                 , [CCENCR04EnglTLEE]             
                 , [CCENCR04EnglELL]              
                 , [CCENCR04EnglEnhanced]                         
                 , [CCENCR04SectionEnrollmentCount]      
                 , [CCENCR00RQCollegeCode]               
                 , [CCENCR00RQYearTermCode]              
                 , [CCENCR00RQCourseId]                  
                 , [CCENCR00RQSectionId]                 
                 , [CCENCR00RQCourseControlNumber]       
                 , [CCENCR00RQCourseTopCode]             
                 , [CCENCR00RQCourseTitle]               
                 , [CCENCR00RQCourseMarkLetter]          
                 , [CCENCR00RQCourseMarkPoints]          
                 , [CCENCR00RQCourseLevelCode]           
                 , [CCENCR00RQUnitsAttempt]              
                 , [CCENCR00RQUnitsSuccess]      
                 , [CCENCR00RQSectionEnrollmentCount]    
                 , [CCENCR002NRQCollegeCode]             
                 , [CCENCR002NRQYearTermCode]            
                 , [CCENCR002NRQCourseId]                
                 , [CCENCR002NRQSectionId]               
                 , [CCENCR002NRQCourseControlNumber]     
                 , [CCENCR002NRQCourseTopCode]           
                 , [CCENCR002NRQCourseTitle]             
                 , [CCENCR002NRQCourseMarkLetter]        
                 , [CCENCR002NRQCourseMarkPoints]        
                 , [CCENCR002NRQCourseLevelCode]         
                 , [CCENCR002NRQUnitsAttempt]            
                 , [CCENCR002NRQUnitsSuccess]            
                 , [CCENCR002NRQSectionEnrollmentCount]  
                 , [CCENCR01RQCollegeCode]               
                 , [CCENCR01RQYearTermCode]              
                 , [CCENCR01RQCourseId]                  
                 , [CCENCR01RQSectionId]                 
                 , [CCENCR01RQCourseControlNumber]       
                 , [CCENCR01RQCourseTopCode]             
                 , [CCENCR01RQCourseTitle]               
                 , [CCENCR01RQCourseMarkLetter]          
                 , [CCENCR01RQCourseMarkPoints]          
                 , [CCENCR01RQCourseLevelCode]           
                 , [CCENCR01RQUnitsAttempt]              
                 , [CCENCR01RQUnitsSuccess]              
                 , [CCENCR01RQSectionEnrollmentCount]    
                 , [CCENCR02RQCollegeCode]               
                 , [CCENCR02RQYearTermCode]              
                 , [CCENCR02RQCourseId]                  
                 , [CCENCR02RQSectionId]                 
                 , [CCENCR02RQCourseControlNumber]       
                 , [CCENCR02RQCourseTopCode]             
                 , [CCENCR02RQCourseTitle]               
                 , [CCENCR02RQCourseMarkLetter]          
                 , [CCENCR02RQCourseMarkPoints]          
                 , [CCENCR02RQCourseLevelCode]           
                 , [CCENCR02RQUnitsAttempt]              
                 , [CCENCR02RQUnitsSuccess]              
                 , [CCENCR02RQSectionEnrollmentCount]    
                 , [CCENCR03RQCollegeCode]               
                 , [CCENCR03RQYearTermCode]              
                 , [CCENCR03RQCourseId]                  
                 , [CCENCR03RQSectionId]                 
                 , [CCENCR03RQCourseControlNumber]       
                 , [CCENCR03RQCourseTopCode]             
                 , [CCENCR03RQCourseTitle]               
                 , [CCENCR03RQCourseMarkLetter]          
                 , [CCENCR03RQCourseMarkPoints]          
                 , [CCENCR03RQCourseLevelCode]           
                 , [CCENCR03RQUnitsAttempt]              
                 , [CCENCR03RQUnitsSuccess]              
                 , [CCENCR03RQSectionEnrollmentCount]    
                 , [CCENCR04RQCollegeCode]               
                 , [CCENCR04RQYearTermCode]              
                 , [CCENCR04RQCourseId]                  
                 , [CCENCR04RQSectionId]                 
                 , [CCENCR04RQCourseControlNumber]       
                 , [CCENCR04RQCourseTopCode]             
                 , [CCENCR04RQCourseTitle]               
                 , [CCENCR04RQCourseMarkLetter]          
                 , [CCENCR04RQCourseMarkPoints]          
                 , [CCENCR04RQCourseLevelCode]           
                 , [CCENCR04RQUnitsAttempt]              
                 , [CCENCR04RQUnitsSuccess]              
                 , [CCENCR04RQSectionEnrollmentCount]    
                 )
            SELECT [InterSegmentKey]                     = A.[InterSegmentKey]
                 , [RequestStudentId]                    = ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) + 100000000 
                 , [CCENCRFLCollegeCode]                 = B.[MainCollegeCode]
                 , [CCENCRFLYearTermCode]                = B.[MainYearTermCode]
                 , [CCENCRFLCourseId]                    = B.[MainCourseId]
                 , [CCENCRFLSectionId]                   = B.[MainCourseSectionId]
                 , [CCENCRFLCourseControlNumber]         = B.[MainCourseControlNumber]
                 , [CCENCRFLCourseTopCode]               = B.[MainCourseTopCode]
                 , [CCENCRFLCourseTitle]                 = B.[MainCourseTitle]
                 , [CCENCRFLCourseMarkLetter]            = B.[MainCourseMarkLetter]  
                 , [CCENCRFLCourseMarkPoints]            = B.[MainCourseMarkPoints]   
                 , [CCENCRFLCourseLevelCode]             = B.[MainCourseLevelCode]   
                 , [CCENCRFLUnitsAttempt]                = B.[MainCourseUnitsTry]   
                 , [CCENCRFLUnitsSuccess]                = B.[MainCourseUnitsGet]
                 , [CCENCRFLEnglGateway]                 = B.[MainEnglGateway]          
                 , [CCENCRFLEnglCriticalThinking]        = B.[MainEnglCriticalThinking] 
                 , [CCENCRFLEnglTLEE]                    = B.[MainEnglTLEE]             
                 , [CCENCRFLEnglELL]                     = B.[MainEnglELL]              
                 , [CCENCRFLEnglEnhanced]                = B.[MainEnglEnhanced]       
                 , [CCENCRFLSectionEnrollmentCount]      = B.[MainSectionEnrollmentCount]
                 , [CCENCR00CollegeCode]                 = C.[MainCollegeCode]
                 , [CCENCR00YearTermCode]                = C.[MainYearTermCode]
                 , [CCENCR00CourseId]                    = C.[MainCourseId]
                 , [CCENCR00SectionId]                   = C.[MainCourseSectionId]
                 , [CCENCR00CourseControlNumber]         = C.[MainCourseControlNumber]
                 , [CCENCR00CourseTopCode]               = C.[MainCourseTopCode]
                 , [CCENCR00CourseTitle]                 = C.[MainCourseTitle]
                 , [CCENCR00CourseMarkLetter]            = C.[MainCourseMarkLetter]
                 , [CCENCR00CourseMarkPoints]            = C.[MainCourseMarkPoints]
                 , [CCENCR00CourseLevelCode]             = C.[MainCourseLevelCode]   
                 , [CCENCR00UnitsAttempt]                = C.[MainCourseUnitsTry]   
                 , [CCENCR00UnitsSuccess]                = C.[MainCourseUnitsGet]
                 , [CCENCR00EnglGateway]                 = C.[MainEnglGateway]          
                 , [CCENCR00EnglCriticalThinking]        = C.[MainEnglCriticalThinking] 
                 , [CCENCR00EnglTLEE]                    = C.[MainEnglTLEE]             
                 , [CCENCR00EnglELL]                     = C.[MainEnglELL]              
                 , [CCENCR00EnglEnhanced]                = C.[MainEnglEnhanced]       
                 , [CCENCR00SectionEnrollmentCount]      = C.[MainSectionEnrollmentCount]
                 , [CCENCR002NCollegeCode]               = D.[MainCollegeCode]
                 , [CCENCR002NYearTermCode]              = D.[MainYearTermCode]
                 , [CCENCR002NCourseId]                  = D.[MainCourseId]
                 , [CCENCR002NSectionId]                 = D.[MainCourseSectionId]
                 , [CCENCR002NCourseControlNumber]       = D.[MainCourseControlNumber]
                 , [CCENCR002NCourseTopCode]             = D.[MainCourseTopCode]
                 , [CCENCR002NCourseTitle]               = D.[MainCourseTitle]
                 , [CCENCR002NCourseMarkLetter]          = D.[MainCourseMarkLetter]
                 , [CCENCR002NCourseMarkPoints]          = D.[MainCourseMarkPoints]
                 , [CCENCR002NCourseLevelCode]           = D.[MainCourseLevelCode]   
                 , [CCENCR002NUnitsAttempt]              = D.[MainCourseUnitsTry]   
                 , [CCENCR002NUnitsSuccess]              = D.[MainCourseUnitsGet]
                 , [CCENCR002NEnglGateway]               = D.[MainEnglGateway]          
                 , [CCENCR002NEnglCriticalThinking]      = D.[MainEnglCriticalThinking] 
                 , [CCENCR002NEnglTLEE]                  = D.[MainEnglTLEE]             
                 , [CCENCR002NEnglELL]                   = D.[MainEnglELL]              
                 , [CCENCR002NEnglEnhanced]              = D.[MainEnglEnhanced]       
                 , [CCENCR002NSectionEnrollmentCount]    = D.[MainSectionEnrollmentCount]
                 , [CCENCR01CollegeCode]                 = E.[MainCollegeCode]
                 , [CCENCR01YearTermCode]                = E.[MainYearTermCode]
                 , [CCENCR01CourseId]                    = E.[MainCourseId]
                 , [CCENCR01SectionId]                   = E.[MainCourseSectionId]
                 , [CCENCR01CourseControlNumber]         = E.[MainCourseControlNumber]
                 , [CCENCR01CourseTopCode]               = E.[MainCourseTopCode]
                 , [CCENCR01CourseTitle]                 = E.[MainCourseTitle]
                 , [CCENCR01CourseMarkLetter]            = E.[MainCourseMarkLetter]
                 , [CCENCR01CourseMarkPoints]            = E.[MainCourseMarkPoints]
                 , [CCENCR01CourseLevelCode]             = E.[MainCourseLevelCode]   
                 , [CCENCR01UnitsAttempt]                = E.[MainCourseUnitsTry]   
                 , [CCENCR01UnitsSuccess]                = E.[MainCourseUnitsGet]
                 , [CCENCR01EnglGateway]                 = E.[MainEnglGateway]          
                 , [CCENCR01EnglCriticalThinking]        = E.[MainEnglCriticalThinking] 
                 , [CCENCR01EnglTLEE]                    = E.[MainEnglTLEE]             
                 , [CCENCR01EnglELL]                     = E.[MainEnglELL]              
                 , [CCENCR01EnglEnhanced]                = E.[MainEnglEnhanced]       
                 , [CCENCR01SectionEnrollmentCount]      = E.[MainSectionEnrollmentCount]
                 , [CCENCR02CollegeCode]                 = F.[MainCollegeCode]
                 , [CCENCR02YearTermCode]                = F.[MainYearTermCode]
                 , [CCENCR02CourseId]                    = F.[MainCourseId]
                 , [CCENCR02SectionId]                   = F.[MainCourseSectionId]
                 , [CCENCR02CourseControlNumber]         = F.[MainCourseControlNumber]
                 , [CCENCR02CourseTopCode]               = F.[MainCourseTopCode]
                 , [CCENCR02CourseTitle]                 = F.[MainCourseTitle]
                 , [CCENCR02CourseMarkLetter]            = F.[MainCourseMarkLetter]
                 , [CCENCR02CourseMarkPoints]            = F.[MainCourseMarkPoints]
                 , [CCENCR02CourseLevelCode]             = F.[MainCourseLevelCode]   
                 , [CCENCR02UnitsAttempt]                = F.[MainCourseUnitsTry]   
                 , [CCENCR02UnitsSuccess]                = F.[MainCourseUnitsGet]
                 , [CCENCR02EnglGateway]                 = F.[MainEnglGateway]          
                 , [CCENCR02EnglCriticalThinking]        = F.[MainEnglCriticalThinking] 
                 , [CCENCR02EnglTLEE]                    = F.[MainEnglTLEE]             
                 , [CCENCR02EnglELL]                     = F.[MainEnglELL]              
                 , [CCENCR02EnglEnhanced]                = F.[MainEnglEnhanced]       
                 , [CCENCR02SectionEnrollmentCount]      = F.[MainSectionEnrollmentCount]
                 , [CCENCR03CollegeCode]                 = G.[MainCollegeCode]
                 , [CCENCR03YearTermCode]                = G.[MainYearTermCode]
                 , [CCENCR03CourseId]                    = G.[MainCourseId]
                 , [CCENCR03SectionId]                   = G.[MainCourseSectionId]
                 , [CCENCR03CourseControlNumber]         = G.[MainCourseControlNumber]
                 , [CCENCR03CourseTopCode]               = G.[MainCourseTopCode]
                 , [CCENCR03CourseTitle]                 = G.[MainCourseTitle]
                 , [CCENCR03CourseMarkLetter]            = G.[MainCourseMarkLetter]
                 , [CCENCR03CourseMarkPoints]            = G.[MainCourseMarkPoints]
                 , [CCENCR03CourseLevelCode]             = G.[MainCourseLevelCode]   
                 , [CCENCR03UnitsAttempt]                = G.[MainCourseUnitsTry]   
                 , [CCENCR03UnitsSuccess]                = G.[MainCourseUnitsGet]
                 , [CCENCR03EnglGateway]                 = G.[MainEnglGateway]          
                 , [CCENCR03EnglCriticalThinking]        = G.[MainEnglCriticalThinking] 
                 , [CCENCR03EnglTLEE]                    = G.[MainEnglTLEE]             
                 , [CCENCR03EnglELL]                     = G.[MainEnglELL]              
                 , [CCENCR03EnglEnhanced]                = G.[MainEnglEnhanced]       
                 , [CCENCR03SectionEnrollmentCount]      = G.[MainSectionEnrollmentCount]
                 , [CCENCR04CollegeCode]                 = H.[MainCollegeCode]
                 , [CCENCR04YearTermCode]                = H.[MainYearTermCode]
                 , [CCENCR04CourseId]                    = H.[MainCourseId]
                 , [CCENCR04SectionId]                   = H.[MainCourseSectionId]
                 , [CCENCR04CourseControlNumber]         = H.[MainCourseControlNumber]
                 , [CCENCR04CourseTopCode]               = H.[MainCourseTopCode]
                 , [CCENCR04CourseTitle]                 = H.[MainCourseTitle]
                 , [CCENCR04CourseMarkLetter]            = H.[MainCourseMarkLetter]
                 , [CCENCR04CourseMarkPoints]            = H.[MainCourseMarkPoints]
                 , [CCENCR04CourseLevelCode]             = H.[MainCourseLevelCode]   
                 , [CCENCR04UnitsAttempt]                = H.[MainCourseUnitsTry]   
                 , [CCENCR04UnitsSuccess]                = H.[MainCourseUnitsGet]
                 , [CCENCR04EnglGateway]                 = H.[MainEnglGateway]          
                 , [CCENCR04EnglCriticalThinking]        = H.[MainEnglCriticalThinking] 
                 , [CCENCR04EnglTLEE]                    = H.[MainEnglTLEE]             
                 , [CCENCR04EnglELL]                     = H.[MainEnglELL]              
                 , [CCENCR04EnglEnhanced]                = H.[MainEnglEnhanced]       
                 , [CCENCR04SectionEnrollmentCount]      = H.[MainSectionEnrollmentCount]
                 , [CCENCR00RQCollegeCode]               = C.[CoreqCollegeCode]
                 , [CCENCR00RQYearTermCode]              = C.[CoreqYearTermCode]
                 , [CCENCR00RQCourseId]                  = C.[CoreqCourseId]
                 , [CCENCR00RQSectionId]                 = C.[CoreqCourseSectionId]
                 , [CCENCR00RQCourseControlNumber]       = C.[CoreqCourseControlNumber]
                 , [CCENCR00RQCourseTopCode]             = C.[CoreqCourseTopCode]
                 , [CCENCR00RQCourseTitle]               = C.[CoreqCourseTitle]
                 , [CCENCR00RQCourseMarkLetter]          = C.[CoreqCourseMarkLetter]
                 , [CCENCR00RQCourseMarkPoints]          = C.[CoreqCourseMarkPoints]
                 , [CCENCR00RQCourseLevelCode]           = C.[CoreqCourseLevelCode]   
                 , [CCENCR00RQUnitsAttempt]              = C.[CoreqCourseUnitsTry]   
                 , [CCENCR00RQUnitsSuccess]              = C.[CoreqCourseUnitsGet]     
                 , [CCENCR00RQSectionEnrollmentCount]    = C.[CoreqSectionEnrollmentCount]
                 , [CCENCR002NRQCollegeCode]             = D.[CoreqCollegeCode]
                 , [CCENCR002NRQYearTermCode]            = D.[CoreqYearTermCode]
                 , [CCENCR002NRQCourseId]                = D.[CoreqCourseId]
                 , [CCENCR002NRQSectionId]               = D.[CoreqCourseSectionId]
                 , [CCENCR002NRQCourseControlNumber]     = D.[CoreqCourseControlNumber]
                 , [CCENCR002NRQCourseTopCode]           = D.[CoreqCourseTopCode]
                 , [CCENCR002NRQCourseTitle]             = D.[CoreqCourseTitle]
                 , [CCENCR002NRQCourseMarkLetter]        = D.[CoreqCourseMarkLetter]
                 , [CCENCR002NRQCourseMarkPoints]        = D.[CoreqCourseMarkPoints]
                 , [CCENCR002NRQCourseLevelCode]         = D.[CoreqCourseLevelCode]   
                 , [CCENCR002NRQUnitsAttempt]            = D.[CoreqCourseUnitsTry]   
                 , [CCENCR002NRQUnitsSuccess]            = D.[CoreqCourseUnitsGet]     
                 , [CCENCR002NRQSectionEnrollmentCount]  = D.[CoreqSectionEnrollmentCount]
                 , [CCENCR01RQCollegeCode]               = E.[CoreqCollegeCode]
                 , [CCENCR01RQYearTermCode]              = E.[CoreqYearTermCode]
                 , [CCENCR01RQCourseId]                  = E.[CoreqCourseId]
                 , [CCENCR01RQSectionId]                 = E.[CoreqCourseSectionId]
                 , [CCENCR01RQCourseControlNumber]       = E.[CoreqCourseControlNumber]
                 , [CCENCR01RQCourseTopCode]             = E.[CoreqCourseTopCode]
                 , [CCENCR01RQCourseTitle]               = E.[CoreqCourseTitle]
                 , [CCENCR01RQCourseMarkLetter]          = E.[CoreqCourseMarkLetter]
                 , [CCENCR01RQCourseMarkPoints]          = E.[CoreqCourseMarkPoints]
                 , [CCENCR01RQCourseLevelCode]           = E.[CoreqCourseLevelCode]   
                 , [CCENCR01RQUnitsAttempt]              = E.[CoreqCourseUnitsTry]   
                 , [CCENCR01RQUnitsSuccess]              = E.[CoreqCourseUnitsGet]     
                 , [CCENCR01RQSectionEnrollmentCount]    = E.[CoreqSectionEnrollmentCount]
                 , [CCENCR02RQCollegeCode]               = F.[CoreqCollegeCode]
                 , [CCENCR02RQYearTermCode]              = F.[CoreqYearTermCode]
                 , [CCENCR02RQCourseId]                  = F.[CoreqCourseId]
                 , [CCENCR02RQSectionId]                 = F.[CoreqCourseSectionId]
                 , [CCENCR02RQCourseControlNumber]       = F.[CoreqCourseControlNumber]
                 , [CCENCR02RQCourseTopCode]             = F.[CoreqCourseTopCode]
                 , [CCENCR02RQCourseTitle]               = F.[CoreqCourseTitle]
                 , [CCENCR02RQCourseMarkLetter]          = F.[CoreqCourseMarkLetter]
                 , [CCENCR02RQCourseMarkPoints]          = F.[CoreqCourseMarkPoints]
                 , [CCENCR02RQCourseLevelCode]           = F.[CoreqCourseLevelCode]   
                 , [CCENCR02RQUnitsAttempt]              = F.[CoreqCourseUnitsTry]   
                 , [CCENCR02RQUnitsSuccess]              = F.[CoreqCourseUnitsGet]     
                 , [CCENCR02RQSectionEnrollmentCount]    = F.[CoreqSectionEnrollmentCount]
                 , [CCENCR03RQCollegeCode]               = G.[CoreqCollegeCode]
                 , [CCENCR03RQYearTermCode]              = G.[CoreqYearTermCode]
                 , [CCENCR03RQCourseId]                  = G.[CoreqCourseId]
                 , [CCENCR03RQSectionId]                 = G.[CoreqCourseSectionId]
                 , [CCENCR03RQCourseControlNumber]       = G.[CoreqCourseControlNumber]
                 , [CCENCR03RQCourseTopCode]             = G.[CoreqCourseTopCode]
                 , [CCENCR03RQCourseTitle]               = G.[CoreqCourseTitle]
                 , [CCENCR03RQCourseMarkLetter]          = G.[CoreqCourseMarkLetter]
                 , [CCENCR03RQCourseMarkPoints]          = G.[CoreqCourseMarkPoints]
                 , [CCENCR03RQCourseLevelCode]           = G.[CoreqCourseLevelCode]   
                 , [CCENCR03RQUnitsAttempt]              = G.[CoreqCourseUnitsTry]   
                 , [CCENCR03RQUnitsSuccess]              = G.[CoreqCourseUnitsGet] 
                 , [CCENCR03RQSectionEnrollmentCount]    = G.[CoreqSectionEnrollmentCount]
                 , [CCENCR04RQCollegeCode]               = H.[CoreqCollegeCode]
                 , [CCENCR04RQYearTermCode]              = H.[CoreqYearTermCode]
                 , [CCENCR04RQCourseId]                  = H.[CoreqCourseId]
                 , [CCENCR04RQSectionId]                 = H.[CoreqCourseSectionId]
                 , [CCENCR04RQCourseControlNumber]       = H.[CoreqCourseControlNumber]
                 , [CCENCR04RQCourseTopCode]             = H.[CoreqCourseTopCode]
                 , [CCENCR04RQCourseTitle]               = H.[CoreqCourseTitle]
                 , [CCENCR04RQCourseMarkLetter]          = H.[CoreqCourseMarkLetter]
                 , [CCENCR04RQCourseMarkPoints]          = H.[CoreqCourseMarkPoints]
                 , [CCENCR04RQCourseLevelCode]           = H.[CoreqCourseLevelCode]   
                 , [CCENCR04RQUnitsAttempt]              = H.[CoreqCourseUnitsTry]   
                 , [CCENCR04RQUnitsSuccess]              = H.[CoreqCourseUnitsGet]    
                 , [CCENCR04RQSectionEnrollmentCount]    = H.[CoreqSectionEnrollmentCount]
              FROM [cteStudents] A
         LEFT JOIN [Courses]  B ON A.[InterSegmentKey] = B.[InterSegmentKey] AND B.[IsFirstLevel] = 1
         LEFT JOIN [Courses]  C ON A.[InterSegmentKey] = C.[InterSegmentKey] AND C.[CourseLevel]  = 'CCENCR00'
         LEFT JOIN [Courses]  D ON A.[InterSegmentKey] = D.[InterSegmentKey] AND D.[CourseLevel]  = 'CCENCR002N'
         LEFT JOIN [Courses]  E ON A.[InterSegmentKey] = E.[InterSegmentKey] AND E.[CourseLevel]  = 'CCENCR01'
         LEFT JOIN [Courses]  F ON A.[InterSegmentKey] = F.[InterSegmentKey] AND F.[CourseLevel]  = 'CCENCR02'
         LEFT JOIN [Courses]  G ON A.[InterSegmentKey] = G.[InterSegmentKey] AND G.[CourseLevel]  = 'CCENCR03'
         LEFT JOIN [Courses]  H ON A.[InterSegmentKey] = H.[InterSegmentKey] AND H.[CourseLevel]  = 'CCENCR04'

         

    ;WITH [cte_cbcrsinv]
       AS (
           SELECT A.*
                , B.[YearTermCode]
             FROM [comis].[cbcrsinv] A
             JOIN [comis].[Term]     B ON A.[term_id] = B.[TermCode]
          )

   UPDATE A
      SET [CCENCRFLCB25]               = B.[general_education_status]
        , [CCENCRFLCB26]               = B.[support_course_status]
        , [CCENCR00TransferStatus]     = C.[transfer_status]
        , [CCENCR00CB25]               = C.[general_education_status]
        , [CCENCR00CB26]               = C.[support_course_status]
        , [CCENCR00RQTransferStatus]   = D.[transfer_status]
        , [CCENCR00RQCB25]             = D.[general_education_status]
        , [CCENCR00RQCB26]             = D.[support_course_status]
        , [CCENCR002NTransferStatus]   = E.[transfer_status]
        , [CCENCR002NCB25]             = E.[general_education_status]
        , [CCENCR002NCB26]             = E.[support_course_status]
        , [CCENCR002NRQTransferStatus] = F.[transfer_status]
        , [CCENCR002NRQCB25]           = F.[general_education_status]
        , [CCENCR002NRQCB26]           = F.[support_course_status]
        , [CCENCR01TransferStatus]     = G.[transfer_status]
        , [CCENCR01CB25]               = G.[general_education_status]
        , [CCENCR01CB26]               = G.[support_course_status]
        , [CCENCR02TransferStatus]     = H.[transfer_status]
        , [CCENCR02CB25]               = H.[general_education_status]
        , [CCENCR02CB26]               = H.[support_course_status]
        , [CCENCR03TransferStatus]     = I.[transfer_status]
        , [CCENCR03CB25]               = I.[general_education_status]
        , [CCENCR03CB26]               = I.[support_course_status]
        , [CCENCR04TransferStatus]     = J.[transfer_status]
        , [CCENCR04CB25]               = J.[general_education_status]
        , [CCENCR04CB26]               = J.[support_course_status]
     FROM [mmap].[RetroEnglUpdated] A
LEFT JOIN [cte_cbcrsinv]            B  ON A.[CCENCRFLCollegeCode]             = B.[college_id]
                                      AND A.[CCENCRFLYearTermCode]            = B.[YearTermCode]
                                      AND A.[CCENCRFLCourseId]                = B.[course_id]
                                      AND A.[CCENCRFLCourseControlNumber]     = B.[control_number]
LEFT JOIN [cte_cbcrsinv]            C  ON A.[CCENCR00CollegeCode]             = C.[college_id]
                                      AND A.[CCENCR00YearTermCode]            = C.[YearTermCode]
                                      AND A.[CCENCR00CourseId]                = C.[course_id]
                                      AND A.[CCENCR00CourseControlNumber]     = C.[control_number]
LEFT JOIN [cte_cbcrsinv]            D  ON A.[CCENCR00RQCollegeCode]           = D.[college_id]
                                      AND A.[CCENCR00RQYearTermCode]          = D.[YearTermCode]
                                      AND A.[CCENCR00RQCourseId]              = D.[course_id]
                                      AND A.[CCENCR00RQCourseControlNumber]   = D.[control_number]
LEFT JOIN [cte_cbcrsinv]            E  ON A.[CCENCR002NCollegeCode]           = E.[college_id]
                                      AND A.[CCENCR002NYearTermCode]          = E.[YearTermCode]
                                      AND A.[CCENCR002NCourseId]              = E.[course_id]
                                      AND A.[CCENCR002NCourseControlNumber]   = E.[control_number]
LEFT JOIN [cte_cbcrsinv]            F  ON A.[CCENCR002NRQCollegeCode]         = F.[college_id]
                                      AND A.[CCENCR002NRQYearTermCode]        = F.[YearTermCode]
                                      AND A.[CCENCR002NRQCourseId]            = F.[course_id]
                                      AND A.[CCENCR002NRQCourseControlNumber] = F.[control_number]
LEFT JOIN [cte_cbcrsinv]            G  ON A.[CCENCR01CollegeCode]             = G.[college_id]
                                      AND A.[CCENCR01YearTermCode]            = G.[YearTermCode]
                                      AND A.[CCENCR01CourseId]                = G.[course_id]
                                      AND A.[CCENCR01CourseControlNumber]     = G.[control_number]
LEFT JOIN [cte_cbcrsinv]            H  ON A.[CCENCR02CollegeCode]             = H.[college_id]
                                      AND A.[CCENCR02YearTermCode]            = H.[YearTermCode]
                                      AND A.[CCENCR02CourseId]                = H.[course_id]
                                      AND A.[CCENCR02CourseControlNumber]     = H.[control_number]
LEFT JOIN [cte_cbcrsinv]            I  ON A.[CCENCR03CollegeCode]             = I.[college_id]
                                      AND A.[CCENCR03YearTermCode]            = I.[YearTermCode]
                                      AND A.[CCENCR03CourseId]                = I.[course_id]
                                      AND A.[CCENCR03CourseControlNumber]     = I.[control_number]
LEFT JOIN [cte_cbcrsinv]            J  ON A.[CCENCR04CollegeCode]             = J.[college_id]
                                      AND A.[CCENCR04YearTermCode]            = J.[YearTermCode]
                                      AND A.[CCENCR04CourseId]                = J.[course_id]
                                      AND A.[CCENCR04CourseControlNumber]     = J.[control_number]




                                      
              ;WITH [cte_RetrospectiveCourseContent]
                 AS (
                     SELECT [InterSegmentKey]                          = A.[IntersegmentKey]
                          , [DepartmentCode]                           = A.[DepartmentCode]
                          , [GradeCode]                                = A.[GradeCode]    
                          , [GradeSelector]                            = A.[GradeSelector]
                          , [SchoolCode]                               = A.[SchoolCode]                              
                          , [YearTermCode]                             = A.[YearTermCode]                            
                          , [CourseCode]                               = A.[CourseCode]                              
                          , [CourseContentRank]                        = A.[ContentRank]                       
                          , [CourseTitle]                              = A.[CourseTitle]                             
                          , [CourseMarkLetter]                         = A.[SectionMark]                        
                          , [CourseMarkPoints]                         = A.[MarkPoints]                        
                          , [CourseUniversityAdmissionRequirementCode] = A.[CourseAGCode]
                          , [CourseLevelCode]                          = A.[CourseLevelCode]                         
                          , [CourseTypeCode]                           = A.[CourseTypeCode]                          
                          , [RecencySelector]                          = A.[RecencySelector]                
                       FROM [mmap].[RetroEnglishRetrospectiveCourseContent] A
                    )
                  , [cte_RetrospectivePerformance]
                 AS (
                     SELECT [InterSegmentKey]                        
                          , [DepartmentCode]                         
                          , [GradeCode]                           
                          , [IsLast]
                          , [GradePointAverage]               
                          , [CumulativeGradePointAverage] 
                       FROM [mmap].[RetroEnglishRetrospectivePerformance]  
                    )
          
              UPDATE A
                 SET [HS09GradeCode]                                = B.[GradeCode]                                 
                   , [HS09SchoolCode]                               = B.[SchoolCode]                                
                   , [HS09YearTermCode]                             = B.[YearTermCode]                              
                   , [HS09CourseCode]                               = B.[CourseCode]                                
                   , [HS09CourseContentRank]                        = B.[CourseContentRank]                         
                   , [HS09CourseTitle]                              = B.[CourseTitle]                               
                   , [HS09CourseMarkLetter]                         = B.[CourseMarkLetter]                          
                   , [HS09CourseMarkPoints]                         = B.[CourseMarkPoints]                          
                   , [HS09CourseUniversityAdmissionRequirementCode] = B.[CourseUniversityAdmissionRequirementCode]  
                   , [HS09CourseLevelCode]                          = B.[CourseLevelCode]                           
                   , [HS09CourseTypeCode]                           = B.[CourseTypeCode]                            
                   , [HS09OverallGradePointAverage]                 = C.[GradePointAverage]        
                   , [HS09OverallCumulativeGradePointAverage]       = C.[CumulativeGradePointAverage]
                   , [HS09EnglishGradePointAverage]                 = D.[GradePointAverage]        
                   , [HS09EnglishCumulativeGradePointAverage]       = D.[CumulativeGradePointAverage]      
                   , [HS10GradeCode]                                = E.[GradeCode]                                 
                   , [HS10SchoolCode]                               = E.[SchoolCode]                                
                   , [HS10YearTermCode]                             = E.[YearTermCode]                              
                   , [HS10CourseCode]                               = E.[CourseCode]                                
                   , [HS10CourseContentRank]                        = E.[CourseContentRank]                         
                   , [HS10CourseTitle]                              = E.[CourseTitle]                               
                   , [HS10CourseMarkLetter]                         = E.[CourseMarkLetter]                          
                   , [HS10CourseMarkPoints]                         = E.[CourseMarkPoints]                          
                   , [HS10CourseUniversityAdmissionRequirementCode] = E.[CourseUniversityAdmissionRequirementCode]  
                   , [HS10CourseLevelCode]                          = E.[CourseLevelCode]                           
                   , [HS10CourseTypeCode]                           = E.[CourseTypeCode]                            
                   , [HS10OverallGradePointAverage]                 = F.[GradePointAverage]        
                   , [HS10OverallCumulativeGradePointAverage]       = F.[CumulativeGradePointAverage]
                   , [HS10EnglishGradePointAverage]                 = G.[GradePointAverage]        
                   , [HS10EnglishCumulativeGradePointAverage]       = G.[CumulativeGradePointAverage]      
                   , [HS11GradeCode]                                = H.[GradeCode]                                 
                   , [HS11SchoolCode]                               = H.[SchoolCode]                                
                   , [HS11YearTermCode]                             = H.[YearTermCode]                              
                   , [HS11CourseCode]                               = H.[CourseCode]                                
                   , [HS11CourseContentRank]                        = H.[CourseContentRank]                         
                   , [HS11CourseTitle]                              = H.[CourseTitle]                               
                   , [HS11CourseMarkLetter]                         = H.[CourseMarkLetter]                          
                   , [HS11CourseMarkPoints]                         = H.[CourseMarkPoints]                          
                   , [HS11CourseUniversityAdmissionRequirementCode] = H.[CourseUniversityAdmissionRequirementCode]  
                   , [HS11CourseLevelCode]                          = H.[CourseLevelCode]                           
                   , [HS11CourseTypeCode]                           = H.[CourseTypeCode]                            
                   , [HS11OverallGradePointAverage]                 = I.[GradePointAverage]        
                   , [HS11OverallCumulativeGradePointAverage]       = I.[CumulativeGradePointAverage]
                   , [HS11EnglishGradePointAverage]                 = J.[GradePointAverage]        
                   , [HS11EnglishCumulativeGradePointAverage]       = J.[CumulativeGradePointAverage]      
                   , [HS12GradeCode]                                = K.[GradeCode]                                 
                   , [HS12SchoolCode]                               = K.[SchoolCode]                                
                   , [HS12YearTermCode]                             = K.[YearTermCode]                              
                   , [HS12CourseCode]                               = K.[CourseCode]                                
                   , [HS12CourseContentRank]                        = K.[CourseContentRank]                         
                   , [HS12CourseTitle]                              = K.[CourseTitle]                               
                   , [HS12CourseMarkLetter]                         = K.[CourseMarkLetter]                          
                   , [HS12CourseMarkPoints]                         = K.[CourseMarkPoints]                          
                   , [HS12CourseUniversityAdmissionRequirementCode] = K.[CourseUniversityAdmissionRequirementCode]  
                   , [HS12CourseLevelCode]                          = K.[CourseLevelCode]                           
                   , [HS12CourseTypeCode]                           = K.[CourseTypeCode]                            
                   , [HS12OverallGradePointAverage]                 = L.[GradePointAverage]        
                   , [HS12OverallCumulativeGradePointAverage]       = L.[CumulativeGradePointAverage]
                   , [HS12EnglishGradePointAverage]                 = M.[GradePointAverage]        
                   , [HS12EnglishCumulativeGradePointAverage]       = M.[CumulativeGradePointAverage]      
                   , [HSLGGradeCode]                                = N.[GradeCode]                                 
                   , [HSLGSchoolCode]                               = N.[SchoolCode]                                
                   , [HSLGYearTermCode]                             = N.[YearTermCode]                              
                   , [HSLGCourseCode]                               = N.[CourseCode]                                
                   , [HSLGCourseContentRank]                        = N.[CourseContentRank]                         
                   , [HSLGCourseTitle]                              = N.[CourseTitle]                               
                   , [HSLGCourseMarkLetter]                         = N.[CourseMarkLetter]                          
                   , [HSLGCourseMarkPoints]                         = N.[CourseMarkPoints]                          
                   , [HSLGCourseUniversityAdmissionRequirementCode] = N.[CourseUniversityAdmissionRequirementCode]  
                   , [HSLGCourseLevelCode]                          = N.[CourseLevelCode]                           
                   , [HSLGCourseTypeCode]                           = N.[CourseTypeCode]                            
                   , [HSLGOverallGradePointAverage]                 = O.[GradePointAverage]        
                   , [HSLGOverallCumulativeGradePointAverage]       = O.[CumulativeGradePointAverage]
                   , [HSLGEnglishGradePointAverage]                 = P.[GradePointAverage]        
                   , [HSLGEnglishCumulativeGradePointAverage]       = P.[CumulativeGradePointAverage]
                FROM [mmap].[RetroEnglUpdated]        A
                     -- { GRADE 9 } --
           LEFT JOIN [cte_RetrospectiveCourseContent] B ON A.[InterSegmentKey] = B.[InterSegmentKey] AND B.[DepartmentCode] = 14 AND B.[GradeCode] = '09' AND B.[GradeSelector] = 1
           LEFT JOIN [cte_RetrospectivePerformance]   C ON A.[InterSegmentKey] = C.[InterSegmentKey] AND C.[DepartmentCode] = 0  AND C.[GradeCode] = '09'
           LEFT JOIN [cte_RetrospectivePerformance]   D ON A.[InterSegmentKey] = D.[InterSegmentKey] AND D.[DepartmentCode] = 14 AND D.[GradeCode] = '09'
                     -- { GRADE 10 } --
           LEFT JOIN [cte_RetrospectiveCourseContent] E ON A.[InterSegmentKey] = E.[InterSegmentKey] AND E.[DepartmentCode] = 14 AND E.[GradeCode] = '10' AND E.[GradeSelector] = 1
           LEFT JOIN [cte_RetrospectivePerformance]   F ON A.[InterSegmentKey] = F.[InterSegmentKey] AND F.[DepartmentCode] = 0  AND F.[GradeCode] = '10'
           LEFT JOIN [cte_RetrospectivePerformance]   G ON A.[InterSegmentKey] = G.[InterSegmentKey] AND G.[DepartmentCode] = 14 AND G.[GradeCode] = '10'
                     -- { GRADE 11 } --
           LEFT JOIN [cte_RetrospectiveCourseContent] H ON A.[InterSegmentKey] = H.[InterSegmentKey] AND H.[DepartmentCode] = 14 AND H.[GradeCode] = '11' AND H.[GradeSelector] = 1
           LEFT JOIN [cte_RetrospectivePerformance]   I ON A.[InterSegmentKey] = I.[InterSegmentKey] AND I.[DepartmentCode] = 0  AND I.[GradeCode] = '11'
           LEFT JOIN [cte_RetrospectivePerformance]   J ON A.[InterSegmentKey] = J.[InterSegmentKey] AND J.[DepartmentCode] = 14 AND J.[GradeCode] = '11'
                     -- { GRADE 12 } --
           LEFT JOIN [cte_RetrospectiveCourseContent] K ON A.[InterSegmentKey] = K.[InterSegmentKey] AND K.[DepartmentCode] = 14 AND K.[GradeCode] = '12' AND K.[GradeSelector] = 1
           LEFT JOIN [cte_RetrospectivePerformance]   L ON A.[InterSegmentKey] = L.[InterSegmentKey] AND L.[DepartmentCode] = 0  AND L.[GradeCode] = '12'
           LEFT JOIN [cte_RetrospectivePerformance]   M ON A.[InterSegmentKey] = M.[InterSegmentKey] AND M.[DepartmentCode] = 14 AND M.[GradeCode] = '12'
                     -- { LG } --
           LEFT JOIN [cte_RetrospectiveCourseContent] N ON A.[InterSegmentKey] = N.[InterSegmentKey] AND N.[DepartmentCode] = 14 AND N.[RecencySelector] = 1
           LEFT JOIN [cte_RetrospectivePerformance]   O ON A.[InterSegmentKey] = O.[InterSegmentKey] AND O.[DepartmentCode] = 0  AND O.[IsLast] = 1
           LEFT JOIN [cte_RetrospectivePerformance]   P ON A.[InterSegmentKey] = P.[InterSegmentKey] AND P.[DepartmentCode] = 14 AND P.[IsLast] = 1




UPDATE
    t
SET
    t.STIsEnglishEap           = ss.engl_eap_ind,       
    t.STEnglishScaledScore     = ss.engl_scaled_score,
    t.STEnglishCluster1        = ss.engl_cluster_1,
    t.STEnglishCluster2        = ss.engl_cluster_2,
    t.STEnglishCluster3        = ss.engl_cluster_3,
    t.STEnglishCluster4        = ss.engl_cluster_4,
    t.STEnglishCluster5        = ss.engl_cluster_5,
    t.STEnglishCluster6        = ss.engl_cluster_6,
    t.STMathematicsSubject     = ss.math_subject,
    t.STIsMathematicsEap       = ss.math_eap_ind,
    t.STMathematicsScaledScore = ss.math_scaled_score,
    t.STMathematicsCluster1    = ss.math_cluster_1,
    t.STMathematicsCluster2    = ss.math_cluster_2,
    t.STMathematicsCluster3    = ss.math_cluster_3,
    t.STMathematicsCluster4    = ss.math_cluster_4,
    t.STMathematicsCluster5    = ss.math_cluster_5,
    t.STMathematicsCluster6    = ss.math_cluster_6,
    t.STIsEnglishOnly          = ss.esl_eo_ind,
    t.STIsEnglishLearner       = ss.esl_el_ind,
    t.STIsFluentInitially      = ss.esl_ifep_ind,
    t.STIsFluentReclassified   = ss.esl_rfep_ind
FROM
    mmap.RetroEnglUpdated t
    inner join
    dbo.StudentStar ss
        on ss.derkey1 = t.InterSegmentKey;




UPDATE
    t
SET
    t.HSIsRemedial = 1
FROM
    mmap.RetroEnglUpdated t
WHERE
    exists (
        SELECT
            null
        FROM
            Mmap.RetrospectiveCourseContent rcc
        WHERE
            rcc.InterSegmentKey = t.InterSegmentKey
            and rcc.DepartmentCode = 14
            and rcc.CourseCode = '2100'
    );





UPDATE
    t
SET
    t.HSGraduationDate = s.HSGraduationDate
FROM
    mmap.RetroEnglUpdated t
    inner join
    (
        SELECT
            InterSegmentKey  = s.InterSegmentKey,
            HSGraduationDate = max(a.AwardDate)
        FROM
            mmap.RetroEnglUpdated s
            inner join
            K12AwardProd a
                on a.Derkey1 = s.InterSegmentKey
        GROUP BY
            s.InterSegmentKey
    ) s
        on s.InterSegmentKey = t.InterSegmentKey;

  



           ;WITH [cte_Application]
              AS (
                  SELECT [InterSegmentKey]                   = A.[InterSegmentKey]
                       , [APHSCountry]                       = CONVERT(char(2)      , B.[hs_country])
                       , [APUN01Country]                     = CONVERT(char(2)      , B.[col1_country])
                       , [APUN01Award]                       = CONVERT(char(1)      , B.[col1_degree_obtained])
                       , [APUN02Country]                     = CONVERT(char(2)      , B.[col2_country])
                       , [APUN02Award]                       = CONVERT(char(1)      , B.[col2_degree_obtained])
                       , [APUN03Country]                     = CONVERT(char(2)      , B.[col3_country])
                       , [APUN03Award]                       = CONVERT(char(1)      , B.[col3_degree_obtained])
                       , [APUN04Country]                     = CONVERT(char(2)      , B.[col4_country])
                       , [APUN04Award]                       = CONVERT(char(1)      , B.[col4_degree_obtained])
                       , [APVisaType]                        = NULL
                       , [APAB540Waiver]                     = NULL
                       , [APComfortableEnglish]              = CONVERT(bit          , CASE WHEN B.[comfortable_english] = 't' THEN 1 ELSE 0 END)
                       , [APApplicationLanguage]             = NULL
                       , [APEsl]                             = CONVERT(bit          , CASE WHEN B.[esl] = 't' THEN 1 ELSE 0 END)
                       , [APBasicSkills]                     = CONVERT(bit          , CASE WHEN B.[basic_skills] = 't' THEN 1 ELSE 0 END)
                       , [APEmploymentAssistance]            = CONVERT(bit          , CASE WHEN B.[employment_assistance] = 't' THEN 1 ELSE 0 END)
                       , [APSeasonalAG]                      = CONVERT(bit          , CASE WHEN B.[ca_seasonal_ag] = 't' THEN 1 ELSE 0 END)
                       , [APCountry]                         = NULL
                       , [APPermanentCountry]                = NULL
                       , [APPermanentCountryInternational]   = NULL
                       , [APCompletedEleventhGrade]          = CONVERT(bit          , CASE WHEN B.[completed_eleventh_grade] = 't' THEN 1 ELSE 0 END)
                       , [APCumulativeGradePointAverage]     = CONVERT(decimal(3,2) , CASE WHEN ISNUMERIC(B.[grade_point_average]) = 0 THEN NULL ELSE B.[grade_point_average] END)
                       , [APEnglishCompletedCourseId]        = CONVERT(tinyint      , CASE WHEN ISNUMERIC(B.[highest_english_course]) = 0 OR B.[highest_english_course] LIKE '%.%' THEN NULL ELSE B.[highest_english_course] END)
                       , [APEnglishCompletedCourseGrade]     = CONVERT(varchar(3)   , B.[highest_english_grade])
                       , [APMathematicsCompletedCourseId]    = CONVERT(tinyint      , CASE WHEN ISNUMERIC(B.[highest_math_course_taken]) = 0 OR B.[highest_math_course_taken] LIKE '%.%' THEN NULL ELSE B.[highest_math_course_taken] END)
                       , [APMathematicsCompletedCourseGrade] = CONVERT(varchar(3)   , B.[highest_math_taken_grade])
                       , [APMathematicsPassedCourseId]       = CONVERT(tinyint      , CASE WHEN ISNUMERIC(B.[highest_math_course_passed]) = 0 OR B.highest_math_course_passed LIKE '%.%' THEN NULL ELSE B.highest_math_course_passed END)
                       , [APMathematicsPassedCourseGrade]    = CONVERT(varchar(3)   , B.[highest_math_passed_grade])
                    FROM [mmap].[RetroEnglUpdated] A
                    JOIN [comis].[ApplicationNew]  B ON A.[InterSegmentKey] = B.[InterSegmentKey]
                 )
               , [cte_ApplicationValues]
              AS (
                  SELECT *
                       , [APEnglishCompletedCourseId]      = (SELECT MAX([APEnglishCompletedCourseId])     FROM [cte_Application] WHERE [InterSegmentKey] = X.[InterSegmentKey] AND [APEnglishCompletedCourseGrade]     = X.[APEnglishCompletedCourseGrade]    )     
                       , [APMathematicsCompletedCourseId]  = (SELECT MAX([APMathematicsCompletedCourseId]) FROM [cte_Application] WHERE [InterSegmentKey] = X.[InterSegmentKey] AND [APMathematicsCompletedCourseGrade] = X.[APMathematicsCompletedCourseGrade])
                       , [APMathematicsPassedCourseId]     = (SELECT MAX([APMathematicsPassedCourseId])    FROM [cte_Application] WHERE [InterSegmentKey] = X.[InterSegmentKey] AND [APMathematicsPassedCourseGrade]    = X.[APMathematicsPassedCourseGrade]   )  
                    FROM (
                          SELECT [InterSegmentKey]                   = A.[InterSegmentKey]
                               , [APHSCountry]                       = MAX(A.[APHSCountry]                          )  
                               , [APUN01Country]                     = MAX(A.[APUN01Country]                        )  
                               , [APUN01Award]                       = MAX(A.[APUN01Award]                          )  
                               , [APUN02Country]                     = MAX(A.[APUN02Country]                        )  
                               , [APUN02Award]                       = MAX(A.[APUN02Award]                          )  
                               , [APUN03Country]                     = MAX(A.[APUN03Country]                        )  
                               , [APUN03Award]                       = MAX(A.[APUN03Award]                          )  
                               , [APUN04Country]                     = MAX(A.[APUN04Country]                        )  
                               , [APUN04Award]                       = MAX(A.[APUN04Award]                          )  
                             --, [APVisaType]                        = MIN(A.[APVisaType]                           )  
                             --, [APAB540Waiver]                     = MAX(A.[APAB540Waiver]                        ) 
                               , [APComfortableEnglish]              = MAX(CAST(A.[APComfortableEnglish] AS int)    ) 
                             --, [APApplicationLanguage]             = MAX(A.[APApplicationLanguage]                ) 
                               , [APEsl]                             = MAX(CAST(A.[APEsl]                  AS int)  ) 
                               , [APBasicSkills]                     = MAX(CAST(A.[APBasicSkills]          AS int)  ) 
                               , [APEmploymentAssistance]            = MAX(CAST(A.[APEmploymentAssistance] AS int)  ) 
                               , [APSeasonalAG]                      = MAX(CAST(A.[APSeasonalAG]           AS int)  ) 
                             --, [APCountry]                         = MAX(A.[APCountry]                            ) 
                             --, [APPermanentCountry]                = MAX(A.[APPermanentCountry]                   ) 
                             --, [APPermanentCountryInternational]   = MAX(A.[APPermanentCountryInternational]      ) 
                               , [APCompletedEleventhGrade]          = MAX(CAST(A.[APCompletedEleventhGrade] AS int)) 
                               , [APCumulativeGradePointAverage]     = MAX(A.[APCumulativeGradePointAverage]        )
                               , [APEnglishCompletedCourseGrade]     = MIN(A.[APEnglishCompletedCourseGrade]        )
                               , [APMathematicsCompletedCourseGrade] = MIN(A.[APMathematicsCompletedCourseGrade]    )
                               , [APMathematicsPassedCourseGrade]    = MIN(A.[APMathematicsPassedCourseGrade]       )
                            FROM [cte_Application] A
                           GROUP BY A.[InterSegmentKey]
                         ) X
                 )
           
          UPDATE A
             SET [APHSCountry]                       = B.[APHSCountry]                        
               , [APUN01Country]                     = B.[APUN01Country]                      
               , [APUN01Award]                       = B.[APUN01Award]                        
               , [APUN02Country]                     = B.[APUN02Country]                      
               , [APUN02Award]                       = B.[APUN02Award]                        
               , [APUN03Country]                     = B.[APUN03Country]                      
               , [APUN03Award]                       = B.[APUN03Award]                        
               , [APUN04Country]                     = B.[APUN04Country]                      
               , [APUN04Award]                       = B.[APUN04Award]                        
             --, [APVisaType]                        = B.[APVisaType]                         
             --, [APAB540Waiver]                     = B.[APAB540Waiver]                     
               , [APComfortableEnglish]              = B.[APComfortableEnglish]              
             --, [APApplicationLanguage]             = B.[APApplicationLanguage]             
               , [APEsl]                             = B.[APEsl]                             
               , [APBasicSkills]                     = B.[APBasicSkills]                     
               , [APEmploymentAssistance]            = B.[APEmploymentAssistance]            
               , [APSeasonalAG]                      = B.[APSeasonalAG]                      
             --, [APCountry]                         = B.[APCountry]                         
             --, [APPermanentCountry]                = B.[APPermanentCountry]                
             --, [APPermanentCountryInternational]   = B.[APPermanentCountryInternational]   
               , [APCompletedEleventhGrade]          = B.[APCompletedEleventhGrade]          
               , [APCumulativeGradePointAverage]     = B.[APCumulativeGradePointAverage]    
               , [APEnglishCompletedCourseGrade]     = B.[APEnglishCompletedCourseGrade]    
               , [APMathematicsCompletedCourseGrade] = B.[APMathematicsCompletedCourseGrade]
               , [APMathematicsPassedCourseGrade]    = B.[APMathematicsPassedCourseGrade]   
               , [APEnglishCompletedCourseId]        = B.[APEnglishCompletedCourseId]
               , [APMathematicsCompletedCourseId]    = B.[APMathematicsCompletedCourseId]
               , [APMathematicsPassedCourseId]       = B.[APMathematicsPassedCourseId]
            FROM [mmap].[RetroEnglUpdated]  A
            JOIN [cte_ApplicationValues]    B ON A.[InterSegmentKey] = B.[InterSegmentKey]



        ;WITH [cte_stterm]
           AS (
               SELECT [InterSegmentKey]              = A.[InterSegmentKey]
                    , [CCSubjAge]                    = MAX(D.[age_at_term])
                    , [CCGender]                     = MAX(D.[gender]     )     
                    , [CCCitzenship]                 = MAX(D.[citizenship])
                    , [CCResidency]                  = MAX(D.[residency]  ) 
                    , [CCEducationStatus]            = MAX(D.[education]  ) 
                    , [CCEnrollmentStatus]           = MAX(D.[enrollment] ) 
                    , [CCEthnicities]                = MAX(D.[multi_race] ) 
                    , [CCEthnicity]                  = MAX(D.[ipeds_race] ) 
                    , [CCInitialGoal]                = MAX(D.[goal]       )
                    , [CCIsMultiCollege]             = MAX(CAST(B.[IsMultiCollege] AS int))
                    , [CCEnrollmentLastYearTermCode] = MAX(E.[YearTermCode])
                 FROM [mmap].[RetroEnglUpdated]           A
                 JOIN [comis].[Student]                   B  ON A.[InterSegmentKey]      = B.[InterSegmentKey]   
                 JOIN [comis].[studntid]                  C  ON A.[InterSegmentKey]      = C.[InterSegmentKey]
                 JOIN [comis].[stterm]                    D  ON C.[college_id]           = D.[college_id] 
                                                            AND C.[student_id]           = D.[student_id]
                 JOIN [comis].[term]                      E  ON D.[term_id]              = E.[TermCode]
                                                            AND A.[CCENCRFLYearTermCode] = E.[YearTermCode]
                GROUP BY A.[InterSegmentKey]
              )

       UPDATE A
          SET [CCSubjAge]                    = B.[CCSubjAge]          
            , [CCGender]                     = B.[CCGender]           
            , [CCCitzenship]                 = B.[CCCitzenship]       
            , [CCResidency]                  = B.[CCResidency]        
            , [CCEducationStatus]            = B.[CCEducationStatus]  
            , [CCEnrollmentStatus]           = B.[CCEnrollmentStatus] 
            , [CCEthnicities]                = B.[CCEthnicities]      
            , [CCEthnicity]                  = B.[CCEthnicity]        
            , [CCInitialGoal]                = B.[CCInitialGoal]      
            , [CCIsMultiCollege]             = B.[CCIsMultiCollege]      
            , [CCEnrollmentLastYearTermCode] = B.[CCEnrollmentLastYearTermCode]        
         FROM [mmap].[RetroEnglUpdated] A
         JOIN [cte_stterm]              B ON A.[InterSegmentKey] = B.[InterSegmentKey]


        ;WITH [cte_SGPOPS]
           AS (
               SELECT [InterSegmentKey]          = A.[InterSegmentKey]
                    , [MilitaryStatus]           = MAX(CASE WHEN SUBSTRING(C.[military], 1, 1) = '1' 
                                                          OR SUBSTRING(C.[military], 2, 1) = '1' 
                                                          OR SUBSTRING(C.[military], 3, 1) = '1' 
                                                          OR SUBSTRING(C.[military], 4, 1) = '1' 
                                                      THEN 1 
                                                   END)
                    , [MilitaryDependentStatus]  = MAX(CASE WHEN SUBSTRING(C.[military_dependent], 1, 1) = '1' 
                                                          OR SUBSTRING(C.[military_dependent], 2, 1) = '1' 
                                                          OR SUBSTRING(C.[military_dependent], 3, 1) = '1' 
                                                          OR SUBSTRING(C.[military_dependent], 4, 1) = '1' 
                                                      THEN 1 
                                                   END)
                    , [FosterYouthStatus]        = MAX(CASE WHEN C.[foster_care] = '1'                              THEN 1 END                          )
                    , [IncarceratedStatus]       = MAX(CASE WHEN C.[incarcerated] IN ('1', '2', '3', '4', '5', '6') THEN 1 END                          )
                    , [MESAASEMStatus]           = MAX(CASE WHEN C.[mesa] IN ('1', '2')                             THEN 1 END                          )
                    , [PuenteStatus]             = MAX(CASE WHEN C.[puente] = '1'                                   THEN 1 END                          )
                    , [MCHSECHSStatus]           = MAX(CASE WHEN C.[mchs] IN ('1', '4')                             THEN 1 END                          )
                    , [UMOJAStatus]              = MAX(CASE WHEN C.[umoja] = '1'                                    THEN 1 END                          )
                    , [CAAStatus]                = MAX(CASE WHEN C.[caa] IN ('1', '2')                              THEN 1 END                          )
                    , [BaccalaureateProgram]     = MAX(C.[ba]                                                                                           )
                    , [CCAPStatus]               = MAX(CASE WHEN C.[ccap] = '1'                                                               THEN 1 END)
                    , [EconomicallyDisadvStatus] = MAX(CASE WHEN LEFT(C.[economically_disadv], 1) IN ('1', '2', '3', '4', '5', '6', '7', '8') THEN 1 END)
                    , [ExOffenderStatus]         = MAX(CASE WHEN C.[ex_offender] = '1'                                                        THEN 1 END)
                    , [HomelessStatus]           = MAX(CASE WHEN C.[homeless] = '1'                                                           THEN 1 END)
                    , [LongtermUnemployStatus]   = MAX(CASE WHEN C.[longterm_unemploy] = '1'                                                  THEN 1 END)
                    , [CulturalBarrierStatus]    = MAX(CASE WHEN C.[cultural_barrier] = '1'                                                   THEN 1 END)
                    , [SeasonalFarmWorkStatus]   = MAX(CASE WHEN C.[seasonal_farm_work] = '1'                                                 THEN 1 END)
                    , [LiteracyStatus]           = MAX(CASE WHEN C.[literacy] = '1'                                                           THEN 1 END)
                    , [WorkBasedLearningStatus]  = MAX(CASE WHEN C.[work_based_learning] IN ('A', 'B', 'C')                                   THEN 1 END)
                 FROM [mmap].[RetroEnglUpdated] A
                 JOIN [comis].[STUDNTID]        B  ON A.[InterSegmentKey] = B.[InterSegmentKey]
                 JOIN [comis].[SGPOPS]          C  ON B.[college_id]      = C.[college_id]
                                                  AND B.[student_id]      = C.[student_id] 
                                                  AND A.[CCENCRFLCollegeCode] = C.[college_id]         --<-- New Requirement
                 JOIN [comis].[term]            D  ON C.[term_id]              = D.[TermCode]
                                                  AND A.[CCENCRFLYearTermCode] = D.[YearTermCode]      --<-- New Requirement
                GROUP BY A.[InterSegmentKey]
              )
       
       UPDATE A
          SET [MilitaryStatus]           = ISNULL(B.[MilitaryStatus]          , 0)
            , [MilitaryDependentStatus]  = ISNULL(B.[MilitaryDependentStatus] , 0)
            , [FosterYouthStatus]        = ISNULL(B.[FosterYouthStatus]       , 0)
            , [IncarceratedStatus]       = ISNULL(B.[IncarceratedStatus]      , 0)
            , [MESAASEMStatus]           = ISNULL(B.[MESAASEMStatus]          , 0)
            , [PuenteStatus]             = ISNULL(B.[PuenteStatus]            , 0)
            , [MCHSECHSStatus]           = ISNULL(B.[MCHSECHSStatus]          , 0)
            , [UMOJAStatus]              = ISNULL(B.[UMOJAStatus]             , 0)
            , [CAAStatus]                = ISNULL(B.[CAAStatus]               , 0)
            , [BaccalaureateProgram]     = ISNULL(B.[BaccalaureateProgram]    , 0)
            , [CCAPStatus]               = ISNULL(B.[CCAPStatus]              , 0)
            , [EconomicallyDisadvStatus] = ISNULL(B.[EconomicallyDisadvStatus], 0)
            , [ExOffenderStatus]         = ISNULL(B.[ExOffenderStatus]        , 0)
            , [HomelessStatus]           = ISNULL(B.[HomelessStatus]          , 0)
            , [LongtermUnemployStatus]   = ISNULL(B.[LongtermUnemployStatus]  , 0)
            , [CulturalBarrierStatus]    = ISNULL(B.[CulturalBarrierStatus]   , 0)
            , [SeasonalFarmWorkStatus]   = ISNULL(B.[SeasonalFarmWorkStatus]  , 0)
            , [LiteracyStatus]           = ISNULL(B.[LiteracyStatus]          , 0)
            , [WorkBasedLearningStatus]  = ISNULL(B.[WorkBasedLearningStatus] , 0) 
         FROM [mmap].[RetroEnglUpdated] A
         JOIN [cte_sgpops]              B ON A.[InterSegmentKey] = B.[InterSegmentKey]



        ;WITH [cte_SMMATRICandSSSUCCESS]
           AS (
                   SELECT [InterSegmentKey]    = A.[InterSegmentKey]
                        , [CCEducationalGoal]  = MIN(COALESCE(SUBSTRING(C.[goals], 1, 1), E.[goals])                    )
                        , [CCMajor]            = MIN(COALESCE(C.[Major], E.[Major])                                     )
                        , [CCAssessmentStatus] = MIN(COALESCE(SUBSTRING(C.[assessment], 1, 2), E.[assessment_status])   )       
                        , [CCAdvisementStatus] = MIN(COALESCE(SUBSTRING(C.[advisement], 1, 2), E.[educationplan_status]))               
                     FROM [mmap].[RetroEnglUpdated] A
                     JOIN [comis].[studntid]        B  ON A.[InterSegmentKey]     = B.[InterSegmentKey]
                LEFT JOIN [comis].[SMMATRIC]        C  ON B.[college_id]          = C.[college_id]
                                                      AND B.[student_id]          = C.[student_id]
                                                      AND A.[CCENCRFLCollegeCode] = C.[college_id]         --<-- New Requirement
                LEFT JOIN [comis].[term]            D  ON C.[term_id]             = D.[TermCode]
                                                      AND D.[YearTermCode]        = A.[CCENCRFLYearTermCode]
                LEFT JOIN [comis].[SSSUCCESS]       E  ON B.[college_id]          = E.[college_id]
                                                      AND B.[student_id]          = E.[student_id]
                                                      AND A.[CCENCRFLCollegeCode] = E.[college_id]         --<-- New Requirement
                LEFT JOIN [comis].[term]            F  ON E.[term_id]             = F.[TermCode]
                                                      AND F.[YearTermCode]        = A.[CCENCRFLYearTermCode]
                    GROUP BY A.[InterSegmentKey]
              )

       UPDATE A
          SET [CCEducationalGoal]  = B.[CCEducationalGoal]  
            , [CCMajor]            = B.[CCMajor]            
            , [CCAssessmentStatus] = B.[CCAssessmentStatus] 
            , [CCAdvisementStatus] = B.[CCAdvisementStatus] 
         FROM [mmap].[RetroEnglUpdated]  A
         JOIN [cte_SMMATRICandSSSUCCESS] B ON A.[InterSegmentKey] = B.[InterSegmentKey]


         
        ;WITH [cte_SEEOPS]
           AS (
           
                SELECT DISTINCT
                       [InterSegmentKey]         = A.[InterSegmentKey]
                     , [CCEopsEligibilityFactor] = MIN(C.[elig_factor]) OVER (PARTITION BY A.[InterSegmentKey])
                     , [CCEopsTermOfAcceptance]  = MIN(C.[term_accept]) OVER (PARTITION BY A.[InterSegmentKey])
                     , [CCEopsCareStatus]        = MAX(CASE WHEN C.[eops_care_status] IS NOT NULL THEN 1 END) OVER (PARTITION BY A.[InterSegmentKey])
                  FROM [mmap].[RetroEnglUpdated] A
                  JOIN [comis].[studntid]        B  ON A.[InterSegmentKey]      = B.[InterSegmentKey]
                  JOIN [comis].[SEEOPS]          C  ON B.[college_id]           = C.[college_id]
                                                   AND B.[student_id]           = C.[student_id]
               -- JOIN [comis].[term]            D  ON A.[CCENCRFLYearTermCode] = D.[YearTermCode]     --<-- requirement changed
               --                                  AND C.[term_id]              = D.[TermCode]         --<-- requirement changed
              )
         
       UPDATE A
          SET [CCEopsEligibilityFactor] = B.[CCEopsEligibilityFactor]
            , [CCEopsTermOfAcceptance]  = B.[CCEopsTermOfAcceptance] 
            , [CCEopsCareStatus]        = B.[CCEopsCareStatus]        
         FROM [mmap].[RetroEnglUpdated] A
         JOIN [cte_SEEOPS]              B ON A.[InterSegmentKey] = B.[InterSegmentKey]



        ;WITH [cte_SDDSPS]
           AS (
               SELECT DISTINCT
                      [InterSegmentKey]     = A.[InterSegmentKey]
                    , [CCPrimaryDisability] = MAX(CASE WHEN C.[primary_disability] IS NOT NULL THEN 1 END) OVER (PARTITION BY A.[InterSegmentKey])
                 FROM [mmap].[RetroEnglUpdated] A
                 JOIN [comis].[STUDNTID]        B  ON A.[InterSegmentKey] = B.[InterSegmentKey]
                 JOIN [comis].[SDDSPS]          C  ON B.[college_id]      = C.[college_id]
                                                  AND B.[student_id]      = C.[student_id]                                   
              )

       UPDATE A 
          SET [CCPrimaryDisability]  = B.[CCPrimaryDisability]
         FROM [mmap].[RetroEnglUpdated] A
         JOIN [cte_SDDSPS]              B ON A.[InterSegmentKey] = B.[InterSegmentKey]



UPDATE
    t
SET
    t.CCTransferableUnitsAttempted = s.CCTransferableUnitsAttempted,
    t.CCTransferableUnitsEarned    = s.CCTransferableUnitsEarned
FROM
    mmap.RetroEnglUpdated t
    inner join
    (
        SELECT
            InterSegmentKey              = id.InterSegmentKey,
            CCTransferableUnitsAttempted = sum(sx.units_attempted),
            CCTransferableUnitsEarned    = sum(sx.units)
        FROM
            comis.CBCRSINV cb
            inner join
            comis.SXENRLM SX
                on  sx.college_id     = cb.college_id
                and sx.term_id        = cb.term_id
                and sx.course_id      = cb.course_id
                and sx.control_number = cb.control_number
            inner join
            comis.STUDNTID id
                on  id.college_id = sx.college_id
                and id.student_id = sx.student_id
            inner join
            mmap.RetroEnglUpdated s
                on s.InterSegmentKey = id.InterSegmentKey
        WHERE
            cb.transfer_status in ('A', 'B')
            and sx.units_attempted <> 88.88
            and sx.units <> 88.88
        GROUP BY
            id.InterSegmentKey
    ) s
        on t.InterSegmentKey = s.InterSegmentKey;

GO



           ;WITH [StudentMaxAcademicYear]
              AS (             
                  SELECT [InterSegmentKey] = A.[InterSegmentKey]
                       , [AcademicYear]    = MAX(B.[AcademicYear])
                    FROM [#StudentCourse] A
                    JOIN [comis].[Term]        B ON A.[YearTermCode] = B.[YearTermCode]
                    GROUP BY [InterSegmentKey]
                 )
               , [Xfer] 
            AS (
                SELECT *
                  FROM (
                        SELECT [InterSegmentKey] = A.[InterSegmentKey]
                             , [XferFice]        = C.[fice]
                             , [XferDate]        = C.[date_of_xfer]
                             , [XfersSource]     = C.[source]
                             , [XferSchoolName]  = C.[tfr_in_school_name]
                             , [XferSchoolType]  = C.[tfr_in_inst_type]
                             , [XferSector]      = C.[sector]
                             , [XferState]       = C.[state]
                             , [XferSegment]     = C.[segment]
                             , [Selector]        = ROW_NUMBER() OVER(PARTITION BY A.[InterSegmentKey] ORDER BY C.[date_of_xfer] ASC)
                          FROM [StudentMaxAcademicYear]          A
                          JOIN [comis].[studntid]                B on A.[InterSegmentKey] = B.[InterSegmentKey]
                          JOIN [comis].[xfer_bucket]             C on B.[ssn]             = C.[ssn]
                   CROSS APPLY GetAcademicYear(C.[date_of_xfer]) E
                         WHERE C.[segment] <> 'CCC'
                           AND E.[AcademicYear] >= A.[AcademicYear] 
                       ) X
                 WHERE [Selector] = 1
               )

        UPDATE A
           SET [XferFice]       = B.[XferFice]         
             , [XferDate]       = B.[XferDate]         
             , [XfersSource]    = B.[XfersSource]      
             , [XferSchoolName] = B.[XferSchoolName]   
             , [XferSchoolType] = B.[XferSchoolType]   
             , [XferSector]     = B.[XferSector]       
             , [XferState]      = B.[XferState]        
             , [XferSegment]    = B.[XferSegment]              
          FROM [mmap].[RetroEnglUpdated] A
          JOIN [Xfer]                    B ON A.[InterSegmentKey] = B.[InterSegmentKey]





UPDATE
    t
SET
    t.ElaStatusRecent =
        case e.ElaStatusCode
            when 'EL' then 0
            when 'EO' then 1
            when 'IFEP' then 2
            when 'RFEP' then 3
        end
FROM
    mmap.RetroEnglUpdated t
    inner join
    calpads.student id on
        id.InterSegmentKey = t.InterSegmentKey
    inner join
    calpads.sela e on
        e.StudentStateId = id.StudentStateId
WHERE
    e.ElaStatusStartDate = (
        SELECT
            max(e1.ElaStatusStartDate)
        FROM
            calpads.sela e1
        WHERE
            e1.StudentStateId = e.StudentStateId
    );

GO



UPDATE
    t
SET
    t.CCSP0101 = s.CCSP0101,
    t.CCSP0102 = s.CCSP0102,
    t.CCSP0103 = s.CCSP0103,
    t.CCSP0104 = s.CCSP0104,
    t.CCSP0105 = s.CCSP0105,
    t.CCSP0201 = s.CCSP0201,
    t.CCSP0202 = s.CCSP0202,
    t.CCSP0203 = s.CCSP0203,
    t.CCSP0204 = s.CCSP0204,
    t.CCSP0205 = s.CCSP0205,
    t.CCSP0301 = s.CCSP0301,
    t.CCSP0302 = s.CCSP0302,
    t.CCSP0303 = s.CCSP0303,
    t.CCSP0304 = s.CCSP0304,
    t.CCSP0305 = s.CCSP0305
FROM
    mmap.RetroEnglUpdated t
    inner join
    (
        SELECT
            InterSegmentKey,
            CCSP0101 = max(case when a.first_ind = 1  then a.top_code     end),
            CCSP0102 = max(case when a.first_ind = 1  then a.award        end),
            CCSP0103 = max(case when a.first_ind = 1  then a.award_earned end),
            CCSP0104 = max(case when a.first_ind = 1  then a.program_code end),
            CCSP0105 = max(case when a.first_ind = 1  then a.college_id end),
            CCSP0201 = max(case when a.recent_ind = 1 then a.top_code     end),
            CCSP0202 = max(case when a.recent_ind = 1 then a.award        end),
            CCSP0203 = max(case when a.recent_ind = 1 then a.award_earned end),
            CCSP0204 = max(case when a.recent_ind = 1 then a.program_code end),
            CCSP0205 = max(case when a.recent_ind = 1 then a.college_id end),
            CCSP0301 = max(case when a.recent_ind = 2 then a.top_code     end),
            CCSP0302 = max(case when a.recent_ind = 2 then a.award        end),
            CCSP0303 = max(case when a.recent_ind = 2 then a.award_earned end),
            CCSP0304 = max(case when a.recent_ind = 2 then a.program_code end),
            CCSP0305 = max(case when a.recent_ind = 2 then a.college_id end)
        FROM
            (
                SELECT
                    id.InterSegmentKey,
                    sp.top_code,
                    sp.award,
                    award_earned = convert(char(8), sp.date, 112),
                    sp.program_code,
                    sp.college_id,
                    first_ind = 
                        row_number() over(
                            partition by
                                id.InterSegmentKey
                            order by
                                t.YearTermCode asc,
                                case award
                                    when 'Z' then 1
                                    when 'Y' then 2
                                    when 'S' then 3
                                    when 'A' then 4
                                    when 'F' then 5
                                    when 'T' then 6
                                    when 'L' then 7
                                    when 'B' then 8
                                    when 'E' then 9
                                    when 'O' then 10
                                    when 'R' then 11
                                    when 'Q' then 12
                                    when 'P' then 13
                                    when 'K' then 14
                                    when 'J' then 15
                                    when 'I' then 16
                                    when 'H' then 17
                                    when 'G' then 18
                                    else 99
                                end asc
                        ),
                    recent_ind = 
                        row_number() over(
                            partition by
                                id.InterSegmentKey
                            order by
                                t.YearTermCode desc,
                                case award
                                    when 'Z' then 1
                                    when 'Y' then 2
                                    when 'S' then 3
                                    when 'A' then 4
                                    when 'F' then 5
                                    when 'T' then 6
                                    when 'L' then 7
                                    when 'B' then 8
                                    when 'E' then 9
                                    when 'O' then 10
                                    when 'R' then 11
                                    when 'Q' then 12
                                    when 'P' then 13
                                    when 'K' then 14
                                    when 'J' then 15
                                    when 'I' then 16
                                    when 'H' then 17
                                    when 'G' then 18
                                    else 99
                                end asc
                        )
                FROM
                    comis.spawards sp
                    inner join
                    comis.studntid id on
                        id.college_id = sp.college_id and
                        id.student_id = sp.student_id
                    inner join
                    comis.term t on
                        t.TermCode = sp.term_id
            ) a
        GROUP BY
            a.InterSegmentKey
    ) s on
        s.InterSegmentKey = t.InterSegmentKey;

GO






UPDATE
    t
SET
    t.CCENCR00RQAttendHours = sx.attend_hours
FROM
    mmap.RetroEnglUpdated t
    inner join
    comis.student s on
        s.InterSegmentKey = t.InterSegmentKey
    inner join
    comis.studntid id on
        id.InterSegmentKey = s.InterSegmentKey
    inner join
    comis.sxenrlm sx on
        sx.college_id = id.college_id and
        sx.student_id = id.student_id
    inner join
    comis.Term z on
        z.TermCode     = sx.term_id
WHERE
    z.YearTermCode = t.CCENCR00RQYearTermCode and
    sx.course_id   = t.CCENCR00RQCourseId and
    sx.section_id  = t.CCENCR00RQSectionId;





UPDATE
    t
SET
    t.CCENCR002NRQAttendHours = sx.attend_hours
FROM
    mmap.RetroEnglUpdated t
    inner join
    comis.student s on
        s.InterSegmentKey = t.InterSegmentKey
    inner join
    comis.studntid id on
        id.InterSegmentKey = s.InterSegmentKey
    inner join
    comis.sxenrlm sx on
        sx.college_id = id.college_id and
        sx.student_id = id.student_id
    inner join
    comis.Term z on
        z.TermCode     = sx.term_id
WHERE
    z.YearTermCode = t.CCENCR002NRQYearTermCode and
    sx.course_id   = t.CCENCR002NRQCourseId and
    sx.section_id  = t.CCENCR002NRQSectionId;

GO



UPDATE
    t
SET
    t.CCENCR00IsEngl   = isnull(tc1.IsEngl, 0),
    t.CCENCR002NIsEngl = isnull(tc2.IsEngl, 0)
FROM
    Mmap.RetroEngl t
    left outer join
    mmap.TransferCourses tc1 on
        tc1.ControlNumber = t.CCENCR00CourseControlNumber
    left outer join
    mmap.TransferCourses tc2 on
        tc2.ControlNumber = t.CCENCR002NCourseControlNumber;


UPDATE
    mmap.RetroEngl
SET
    CCENCR00TleComposition   = mmap.tle(CCENCR00CourseTitle,   CCENCR00CollegeCode,   CCENCR00CourseControlNumber),
    CCENCR002NTleComposition = mmap.tle(CCENCR002NCourseTitle, CCENCR002NCollegeCode, CCENCR002NCourseControlNumber);



--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--   THE REMAINDER OF THIS CODE HAS NOT BEEN UPDATED. EXPORTING MANUALLY.
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


/*

DROP PROC IF EXISTS mmap.RetroMathExport_CB;

GO

CREATE PROC
    mmap.RetroMathExport_CB
AS
BEGIN
    SELECT
        RequestStudentId = convert(int, row_number() over(order by newid()) + 100000000),
        HS09GradeCode,
        HS09OverallGradePointAverage,
        HS09OverallCumulativeGradePointAverage,
        HS09MathematicsGradePointAverage,
        HS09MathematicsCumulativeGradePointAverage,
        HS09SchoolCode,
        HS09YearTermCode,
        HS09CourseCode,
        HS09CourseContentRank,
        HS09CourseTitle,
        HS09CourseMarkLetter,
        HS09CourseMarkPoints,
        HS09CourseUniversityAdmissionRequirementCode,
        HS09CourseLevelCode,
        HS09CourseTypeCode,
        HS10GradeCode,
        HS10OverallGradePointAverage,
        HS10OverallCumulativeGradePointAverage,
        HS10MathematicsGradePointAverage,
        HS10MathematicsCumulativeGradePointAverage,
        HS10SchoolCode,
        HS10YearTermCode,
        HS10CourseCode,
        HS10CourseContentRank,
        HS10CourseTitle,
        HS10CourseMarkLetter,
        HS10CourseMarkPoints,
        HS10CourseUniversityAdmissionRequirementCode,
        HS10CourseLevelCode,
        HS10CourseTypeCode,
        HS11GradeCode,
        HS11OverallGradePointAverage,
        HS11OverallCumulativeGradePointAverage,
        HS11MathematicsGradePointAverage,
        HS11MathematicsCumulativeGradePointAverage,
        HS11SchoolCode,
        HS11YearTermCode,
        HS11CourseCode,
        HS11CourseContentRank,
        HS11CourseTitle,
        HS11CourseMarkLetter,
        HS11CourseMarkPoints,
        HS11CourseUniversityAdmissionRequirementCode,
        HS11CourseLevelCode,
        HS11CourseTypeCode,
        HS12GradeCode,
        HS12OverallGradePointAverage,
        HS12OverallCumulativeGradePointAverage,
        HS12MathematicsGradePointAverage,
        HS12MathematicsCumulativeGradePointAverage,
        HS12SchoolCode,
        HS12YearTermCode,
        HS12CourseCode,
        HS12CourseContentRank,
        HS12CourseTitle,
        HS12CourseMarkLetter,
        HS12CourseMarkPoints,
        HS12CourseUniversityAdmissionRequirementCode,
        HS12CourseLevelCode,
        HS12CourseTypeCode,
        HSLGGradeCode,
        HSLGOverallGradePointAverage,
        HSLGOverallCumulativeGradePointAverage,
        HSLGMathematicsGradePointAverage,
        HSLGMathematicsCumulativeGradePointAverage,
        HSLGSchoolCode,
        HSLGYearTermCode,
        HSLGCourseCode,
        HSLGCourseContentRank,
        HSLGCourseTitle,
        HSLGCourseMarkLetter,
        HSLGCourseMarkPoints,
        HSLGCourseUniversityAdmissionRequirementCode,
        HSLGCourseLevelCode,
        HSLGCourseTypeCode,
        CCENCRFLCollegeCode,
        CCENCRFLYearTermCode,
        CCENCRFLCourseId,
        CCENCRFLSectionId,
        CCENCRFLCourseControlNumber,
        CCENCRFLCourseTopCode,
        CCENCRFLCourseTitle,
        CCENCRFLCourseMarkLetter,
        CCENCRFLCourseMarkPoints,
        CCENCRFLCourseLevelCode,
        CCENCRFLUnitsAttempt,
        CCENCRFLUnitsSuccess,
        CCENCR00CollegeCode,
        CCENCR00YearTermCode,
        CCENCR00CourseId,
        CCENCR00SectionId,
        CCENCR00CourseControlNumber,
        CCENCR00CourseTopCode,
        CCENCR00CourseTitle,
        CCENCR00CourseMarkLetter,
        CCENCR00CourseMarkPoints,
        CCENCR00CourseLevelCode,
        CCENCR00CB25,
        CCENCR00CB26,
        CCENCR002NCollegeCode,
        CCENCR002NYearTermCode,
        CCENCR002NCourseId,
        CCENCR002NSectionId,
        CCENCR002NCourseControlNumber,
        CCENCR002NCourseTopCode,
        CCENCR002NCourseTitle,
        CCENCR002NCourseMarkLetter,
        CCENCR002NCourseMarkPoints,
        CCENCR002NCourseLevelCode,
        CCENCR002NCB25,
        CCENCR002NCB26,
        CCENCR01CollegeCode,
        CCENCR01YearTermCode,
        CCENCR01CourseId,
        CCENCR01SectionId,
        CCENCR01CourseControlNumber,
        CCENCR01CourseTopCode,
        CCENCR01CourseTitle,
        CCENCR01CourseMarkLetter,
        CCENCR01CourseMarkPoints,
        CCENCR01CourseLevelCode,
        CCENCR01CB25,
        CCENCR01CB26,
        CCENCR02CollegeCode,
        CCENCR02YearTermCode,
        CCENCR02CourseId,
        CCENCR02SectionId,
        CCENCR02CourseControlNumber,
        CCENCR02CourseTopCode,
        CCENCR02CourseTitle,
        CCENCR02CourseMarkLetter,
        CCENCR02CourseMarkPoints,
        CCENCR02CourseLevelCode,
        CCENCR02CB25,
        CCENCR02CB26,
        CCENCR03CollegeCode,
        CCENCR03YearTermCode,
        CCENCR03CourseId,
        CCENCR03SectionId,
        CCENCR03CourseControlNumber,
        CCENCR03CourseTopCode,
        CCENCR03CourseTitle,
        CCENCR03CourseMarkLetter,
        CCENCR03CourseMarkPoints,
        CCENCR03CourseLevelCode,
        CCENCR03CB25,
        CCENCR03CB26,
        CCENCR04CollegeCode,
        CCENCR04YearTermCode,
        CCENCR04CourseId,
        CCENCR04SectionId,
        CCENCR04CourseControlNumber,
        CCENCR04CourseTopCode,
        CCENCR04CourseTitle,
        CCENCR04CourseMarkLetter,
        CCENCR04CourseMarkPoints,
        CCENCR04CourseLevelCode,
        CCENCR04CB25,
        CCENCR04CB26,
        CCENCR00RQCollegeCode,
        CCENCR00RQYearTermCode,
        CCENCR00RQCourseId,
        CCENCR00RQSectionId,
        CCENCR00RQCourseControlNumber,
        CCENCR00RQCourseTopCode,
        CCENCR00RQCourseTitle,
        CCENCR00RQCourseMarkLetter,
        CCENCR00RQCourseMarkPoints,
        CCENCR00RQCourseLevelCode,
        CCENCR00RQCB25,
        CCENCR00RQCB26,
        CCENCR00RQCreditType,
        CCENCR00RQAttendHours,
        CCENCR002NRQCollegeCode,
        CCENCR002NRQYearTermCode,
        CCENCR002NRQCourseId,
        CCENCR002NRQSectionId,
        CCENCR002NRQCourseControlNumber,
        CCENCR002NRQCourseTopCode,
        CCENCR002NRQCourseTitle,
        CCENCR002NRQCourseMarkLetter,
        CCENCR002NRQCourseMarkPoints,
        CCENCR002NRQCourseLevelCode,
        CCENCR002NRQCB25,
        CCENCR002NRQCB26,
        CCENCR002NRQCreditType,
        CCENCR002NRQAttendHours,
        STIsEnglishEap,
        STEnglishScaledScore,
        STEnglishCluster1,
        STEnglishCluster2,
        STEnglishCluster3,
        STEnglishCluster4,
        STEnglishCluster5,
        STEnglishCluster6,
        STMathematicsSubject,
        STIsMathematicsEap,
        STMathematicsScaledScore,
        STMathematicsCluster1,
        STMathematicsCluster2,
        STMathematicsCluster3,
        STMathematicsCluster4,
        STMathematicsCluster5,
        STMathematicsCluster6,
        STIsEnglishOnly,
        STIsEnglishLearner,
        STIsFluentInitially,
        STIsFluentReclassified,
        HSIsRemedial,
        HSGraduationDate,
        CCSubjAge,
        CCGender,
        CCCitzenship,
        CCResidency,
        CCEducationStatus,
        CCEnrollmentStatus,
        CCEthnicities,
        CCParentEducationLevel,
        CCEthnicity,
        MilitaryStatus,
        MilitaryDependentStatus,
        FosterYouthStatus,
        IncarceratedStatus,
        MESAASEMStatus,
        PuenteStatus,
        MCHSECHSStatus,
        UMOJAStatus,
        CAAStatus,
        CAFYESStatus,
        BaccalaureateProgram,
        CCAPStatus,
        EconomicallyDisadvStatus,
        ExOffenderStatus,
        HomelessStatus,
        LongtermUnemployStatus,
        CulturalBarrierStatus,
        SeasonalFarmWorkStatus,
        LiteracyStatus,
        WorkBasedLearningStatus,
        CCIsMultiCollege,
        CCPrimaryDisability,
        CCEducationalGoal,
        CCInitialGoal,
        CCMajor,
        CCAssessmentStatus,
        CCAdvisementStatus,
        CCEnrollmentLastYearTermCode,
        CCTransferableUnitsAttempted,
        CCTransferableUnitsEarned,
        CCEopsCareStatus,
        CCEopsEligibilityFactor,
        CCEopsTermOfAcceptance,
        APHSCountry,
        APUN01Country,
        APUN01Award,
        APUN02Country,
        APUN02Award,
        APUN03Country,
        APUN03Award,
        APUN04Country,
        APUN04Award,
        APVisaType,
        APAB540Waiver,
        APComfortableEnglish,
        APApplicationLanguage,
        APEsl,
        APBasicSkills,
        APEmploymentAssistance,
        APSeasonalAG,
        APCountry,
        APPermanentCountry,
        APPermanentCountryInternational,
        APCompletedEleventhGrade,
        APCumulativeGradePointAverage,
        APEnglishCompletedCourseId,
        APEnglishCompletedCourseGrade,
        APMathematicsCompletedCourseId,
        APMathematicsCompletedCourseGrade,
        APMathematicsPassedCourseId,
        APMathematicsPassedCourseGrade,
        XferFice,
        XferDate,
        XfersSource,
        XferSchoolName,
        XferSchoolType,
        XferSector,
        XferState,
        XferSegment,
        ElaStatusRecent,
        CCSP0101,
        CCSP0102,
        CCSP0103,
        CCSP0104,
        CCSP0201,
        CCSP0202,
        CCSP0203,
        CCSP0204,
        CCSP0301,
        CCSP0302,
        CCSP0303,
        CCSP0304                               
    FROM
        mmap.RetroEnglUpdated
END;

GO

--EXEC xp_cmdshell 'bcp "DECLARE @Header VARCHAR(MAX) = ''''; SELECT @Header += case when column_ordinal = min(column_ordinal) over () then '''' else char(9) end + c.name FROM sys.dm_exec_describe_first_result_set_for_object(object_id(''mmap.RetroEnglUpdatedExport''), 0) c; SELECT @Header;" QUERYOUT "C:\Users\dlamoree.erp\desktop\RetroEnglishHeader.txt" -c -T -dcalpass';
--EXEC xp_cmdshell 'bcp "calpass.mmap.RetroEnglUpdatedExport" QUERYOUT "C:\Users\dlamoree.erp\desktop\RetroEnglishOutput.txt" -c -T -dcalpass';
--EXEC xp_cmdshell 'copy C:\Users\dlamoree.erp\desktop\RetroEnglishHeader.txt + C:\Users\dlamoree.erp\desktop\RetroEnglishOutput.txt C:\Users\dlamoree.erp\desktop\RetroEnglish.txt';
--EXEC xp_cmdshell 'del C:\Users\dlamoree.erp\desktop\RetroEnglishHeader.txt C:\Users\dlamoree.erp\desktop\RetroEnglishOutput.txt';

GO

--DROP PROCEDURE mmap.RetroEnglishExport_CB;
--DROP TABLE mmap.RetroEnglUpdated;


EXEC mmap.RetroEnglishExport_CB

-- */ -- */ -- */ -- */ -- */ -- */ -- */ -- */ -- */ -- */ -- */ -- */ -- */ 