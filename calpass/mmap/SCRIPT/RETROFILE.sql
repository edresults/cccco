IF (object_id('mmap.RetroEngl') is not null)
	BEGIN
		DROP TABLE mmap.RetroEngl;
	END;

IF (object_id('mmap.RetroMath') is not null)
	BEGIN
		DROP TABLE mmap.RetroMath;
	END;

GO

DECLARE
	@OrganizationCode char(3)      = '361',
	@exestr           varchar(255) = '',
	@TopCode          char(6)      = '',
	@DepartmentCode   tinyint      = '',
	@IsCollege        bit          = 0,
	@IsDistrict       bit          = 0;

SELECT
	@IsCollege  = case when @OrganizationCode = CollegeCode  then 1 else 0 end,
	@IsDistrict = case when @OrganizationCode = DistrictCode then 1 else 0 end
FROM
	comis.College
WHERE
	CollegeCode = @OrganizationCode
	or DistrictCode = @OrganizationCode;

IF (object_id('tempdb.dbo.#College') is not null)
	BEGIN
		DROP TABLE #College;
	END;

SELECT
	CollegeCode
INTO
	#College
FROM
	comis.College
WHERE
	CollegeCode = @OrganizationCode
	or DistrictCode = @OrganizationCode;

/* ENGLISH */

SET @TopCode = '150100';
SET @DepartmentCode = 14;

SELECT
	organization_code              = @OrganizationCode,
	derkey1                        = s.InterSegmentKey,
	hs_09_grade_level              = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.GradeCode end),
	hs_09_gpa                      = max(case when rp.GradeCode = '09' then rp.CumulativeGradePointAverage end),
	hs_09_school_code              = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
	hs_09_year_term_code           = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
	hs_09_course_id                = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseCode end),
	hs_09_course_rank              = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.ContentRank end),
	hs_09_course_title             = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
	hs_09_course_grade             = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.SectionMark end),
	hs_09_course_grade_points      = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
	hs_09_course_success_ind       = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then convert(tinyint, rcc.IsSuccess) end),
	hs_09_course_ag_code           = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
	hs_09_course_level_code        = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
	hs_09_course_type_code         = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
	hs_10_grade_level              = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.GradeCode end),
	hs_10_gpa                      = max(case when rp.GradeCode = '10' then rp.CumulativeGradePointAverage end),
	hs_10_school_code              = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
	hs_10_year_term_code           = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
	hs_10_course_id                = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseCode end),
	hs_10_course_rank              = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.ContentRank end),
	hs_10_course_title             = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
	hs_10_course_grade             = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.SectionMark end),
	hs_10_course_grade_points      = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
	hs_10_course_success_ind       = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then convert(tinyint, rcc.IsSuccess) end),
	hs_10_course_ag_code           = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
	hs_10_course_level_code        = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
	hs_10_course_type_code         = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
	hs_11_grade_level              = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.GradeCode end),
	hs_11_gpa                      = max(case when rp.GradeCode = '11' then rp.CumulativeGradePointAverage end),
	hs_11_school_code              = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
	hs_11_year_term_code           = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
	hs_11_course_id                = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseCode end),
	hs_11_course_rank              = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.ContentRank end),
	hs_11_course_title             = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
	hs_11_course_grade             = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.SectionMark end),
	hs_11_course_grade_points      = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
	hs_11_course_success_ind       = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then convert(tinyint, rcc.IsSuccess) end),
	hs_11_course_ag_code           = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
	hs_11_course_level_code        = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
	hs_11_course_type_code         = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
	hs_12_grade_level              = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.GradeCode end),
	hs_12_gpa                      = max(case when rp.GradeCode = '12' then rp.CumulativeGradePointAverage end),
	hs_12_school_code              = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
	hs_12_year_term_code           = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
	hs_12_course_id                = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseCode end),
	hs_12_course_rank              = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.ContentRank end),
	hs_12_course_title             = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
	hs_12_course_grade             = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.SectionMark end),
	hs_12_course_grade_points      = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
	hs_12_course_success_ind       = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then convert(tinyint, rcc.IsSuccess) end),
	hs_12_course_ag_code           = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
	hs_12_course_level_code        = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
	hs_12_course_type_code         = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
	hs_last_grade_level            = max(case when rcc.GradeSelector = 1 then rcc.GradeCode end),
	hs_last_gpa                    = max(case when rp.IsLast = 1 then rp.CumulativeGradePointAverage end),
	hs_last_school_code            = max(case when rcc.RecencySelector= 1 then rcc.SchoolCode end),
	hs_last_year_term_code         = max(case when rcc.RecencySelector= 1 then rcc.YearTermCode end),
	hs_last_course_id              = max(case when rcc.RecencySelector= 1 then rcc.CourseCode end),
	hs_last_course_rank            = max(case when rcc.RecencySelector= 1 then rcc.ContentRank end),
	hs_last_course_title           = max(case when rcc.RecencySelector= 1 then rcc.CourseTitle end),
	hs_last_course_grade           = max(case when rcc.RecencySelector= 1 then rcc.SectionMark end),
	hs_last_course_grade_points    = max(case when rcc.RecencySelector= 1 then rcc.MarkPoints end),
	hs_last_course_success_ind     = max(case when rcc.RecencySelector= 1 then convert(tinyint, rcc.IsSuccess) end),
	hs_last_course_ag_code         = max(case when rcc.RecencySelector= 1 then rcc.CourseAGCode end),
	hs_last_course_level_code      = max(case when rcc.RecencySelector= 1 then rcc.CourseLevelCode end),
	hs_last_course_type_code       = max(case when rcc.RecencySelector= 1 then rcc.CourseTypeCode end),
	engl_eap_ind                   = ss.engl_eap_ind,
	engl_scaled_score              = ss.engl_scaled_score,
	engl_cluster_1                 = ss.engl_cluster_1,
	engl_cluster_2                 = ss.engl_cluster_2,
	engl_cluster_3                 = ss.engl_cluster_3,
	engl_cluster_4                 = ss.engl_cluster_4,
	engl_cluster_5                 = ss.engl_cluster_5,
	engl_cluster_6                 = ss.engl_cluster_6,
	math_subject                   = ss.math_subject,
	math_eap_ind                   = ss.math_eap_ind,
	math_scaled_score              = ss.math_scaled_score,
	math_cluster_1                 = ss.math_cluster_1,
	math_cluster_2                 = ss.math_cluster_2,
	math_cluster_3                 = ss.math_cluster_3,
	math_cluster_4                 = ss.math_cluster_4,
	math_cluster_5                 = ss.math_cluster_5,
	math_cluster_6                 = ss.math_cluster_6,
	esl_eo_ind                     = ss.esl_eo_ind,
	esl_ifep_ind                   = ss.esl_ifep_ind,
	esl_el_ind                     = ss.esl_el_ind,
	esl_rfep_ind                   = ss.esl_rfep_ind,
	-- college: First
	cc_first_college_id            = max(case when ccc.FirstSelector = 1 then ccc.CollegeId end),
	cc_first_course_level          = max(case when ccc.FirstSelector = 1 then ccc.CourseLevel end),
	cc_first_year_term_code        = max(case when ccc.FirstSelector = 1 then ccc.YearTermCode end),
	cc_first_course_id             = max(case when ccc.FirstSelector = 1 then ccc.CourseId end),
	cc_first_course_title          = max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end),
	cc_first_course_grade          = max(case when ccc.FirstSelector = 1 then ccc.SectionMark end),
	cc_first_course_grade_points   = max(case when ccc.FirstSelector = 1 then ccc.MarkPoints end),
	cc_first_course_grade_category = max(case when ccc.FirstSelector = 1 then ccc.MarkCategory end),
	cc_first_course_success_ind    = max(case when ccc.FirstSelector = 1 then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: Y
	cc_00_college_id               = max(case when ccc.CourseLevel = 'Y' then ccc.CollegeId end),
	cc_00_course_level             = max(case when ccc.CourseLevel = 'Y' then ccc.CourseLevel end),
	cc_00_year_term_code           = max(case when ccc.CourseLevel = 'Y' then ccc.YearTermCode end),
	cc_00_course_id                = max(case when ccc.CourseLevel = 'Y' then ccc.CourseId end),
	cc_00_course_title             = max(case when ccc.CourseLevel = 'Y' then ccc.CourseTitle end),
	cc_00_course_grade             = max(case when ccc.CourseLevel = 'Y' then ccc.SectionMark end),
	cc_00_course_grade_points      = max(case when ccc.CourseLevel = 'Y' then ccc.MarkPoints end),
	cc_00_course_grade_category    = max(case when ccc.CourseLevel = 'Y' then ccc.MarkCategory end),
	cc_00_course_success_ind       = max(case when ccc.CourseLevel = 'Y' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: A
	cc_01_college_id               = max(case when ccc.CourseLevel = 'A' then ccc.CollegeId end),
	cc_01_course_level             = max(case when ccc.CourseLevel = 'A' then ccc.CourseLevel end),
	cc_01_year_term_code           = max(case when ccc.CourseLevel = 'A' then ccc.YearTermCode end),
	cc_01_course_id                = max(case when ccc.CourseLevel = 'A' then ccc.CourseId end),
	cc_01_course_title             = max(case when ccc.CourseLevel = 'A' then ccc.CourseTitle end),
	cc_01_course_grade             = max(case when ccc.CourseLevel = 'A' then ccc.SectionMark end),
	cc_01_course_grade_points      = max(case when ccc.CourseLevel = 'A' then ccc.MarkPoints end),
	cc_01_course_grade_category    = max(case when ccc.CourseLevel = 'A' then ccc.MarkCategory end),
	cc_01_course_success_ind       = max(case when ccc.CourseLevel = 'A' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: B
	cc_02_college_id               = max(case when ccc.CourseLevel = 'B' then ccc.CollegeId end),
	cc_02_course_level             = max(case when ccc.CourseLevel = 'B' then ccc.CourseLevel end),
	cc_02_year_term_code           = max(case when ccc.CourseLevel = 'B' then ccc.YearTermCode end),
	cc_02_course_id                = max(case when ccc.CourseLevel = 'B' then ccc.CourseId end),
	cc_02_course_title             = max(case when ccc.CourseLevel = 'B' then ccc.CourseTitle end),
	cc_02_course_grade             = max(case when ccc.CourseLevel = 'B' then ccc.SectionMark end),
	cc_02_course_grade_points      = max(case when ccc.CourseLevel = 'B' then ccc.MarkPoints end),
	cc_02_course_grade_category    = max(case when ccc.CourseLevel = 'B' then ccc.MarkCategory end),
	cc_02_course_success_ind       = max(case when ccc.CourseLevel = 'B' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: C
	cc_03_college_id               = max(case when ccc.CourseLevel = 'C' then ccc.CollegeId end),
	cc_03_course_level             = max(case when ccc.CourseLevel = 'C' then ccc.CourseLevel end),
	cc_03_year_term_code           = max(case when ccc.CourseLevel = 'C' then ccc.YearTermCode end),
	cc_03_course_id                = max(case when ccc.CourseLevel = 'C' then ccc.CourseId end),
	cc_03_course_title             = max(case when ccc.CourseLevel = 'C' then ccc.CourseTitle end),
	cc_03_course_grade             = max(case when ccc.CourseLevel = 'C' then ccc.SectionMark end),
	cc_03_course_grade_points      = max(case when ccc.CourseLevel = 'C' then ccc.MarkPoints end),
	cc_03_course_grade_category    = max(case when ccc.CourseLevel = 'C' then ccc.MarkCategory end),
	cc_03_course_success_ind       = max(case when ccc.CourseLevel = 'C' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: D
	cc_04_college_id               = max(case when ccc.CourseLevel = 'D' then ccc.CollegeId end),
	cc_04_course_level             = max(case when ccc.CourseLevel = 'D' then ccc.CourseLevel end),
	cc_04_year_term_code           = max(case when ccc.CourseLevel = 'D' then ccc.YearTermCode end),
	cc_04_course_id                = max(case when ccc.CourseLevel = 'D' then ccc.CourseId end),
	cc_04_course_title             = max(case when ccc.CourseLevel = 'D' then ccc.CourseTitle end),
	cc_04_course_grade             = max(case when ccc.CourseLevel = 'D' then ccc.SectionMark end),
	cc_04_course_grade_points      = max(case when ccc.CourseLevel = 'D' then ccc.MarkPoints end),
	cc_04_course_grade_category    = max(case when ccc.CourseLevel = 'D' then ccc.MarkCategory end),
	cc_04_course_success_ind       = max(case when ccc.CourseLevel = 'D' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- -- college: Transfer
	-- cc_tran_college_id             = null,
	-- cc_tran_course_level           = null,
	-- cc_tran_year_term_code         = null,
	-- cc_tran_course_id              = null,
	-- cc_tran_course_title           = null,
	-- cc_tran_course_grade           = null,
	-- cc_tran_course_grade_points    = null,
	-- cc_tran_course_grade_category  = null,
	-- cc_tran_course_success_ind     = null,
	-- -- college: Support
	-- cc_supp_college_id             = null,
	-- cc_supp_course_level           = null,
	-- cc_supp_year_term_code         = null,
	-- cc_supp_course_id              = null,
	-- cc_supp_course_title           = null,
	-- cc_supp_course_grade           = null,
	-- cc_supp_course_grade_points    = null,
	-- cc_supp_course_grade_category  = null,
	-- cc_supp_course_success_ind     = null,
	-- binary variables
	EXPOSITORY_UP11                = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_UP11_C              = max(case when rcc.MarkPoints >= 2 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_UP11_Cplus          = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_UP11_Bminus         = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_UP11_B              = max(case when rcc.MarkPoints >= 3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_ANY                 = max(case when rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_ANY_C               = max(case when rcc.MarkPoints >= 2 and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_ANY_Cplus           = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_ANY_Bminus          = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	EXPOSITORY_ANY_B               = max(case when rcc.MarkPoints >= 3 and rcc.CourseCode in ('2113', '2114', '2118') then 1 else 0 end),
	REMEDIAL_UP11                  = max(case when rcc.GradeCode <= '11' and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_UP11_C                = max(case when rcc.MarkPoints >= 2 and rcc.GradeCode <= '11' and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_UP11_Cplus            = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_UP11_Bminus           = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_UP11_B                = max(case when rcc.MarkPoints >= 3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_ANY                   = max(case when rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_ANY_C                 = max(case when rcc.MarkPoints >= 2 and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_ANY_Cplus             = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_ANY_Bminus            = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode = '2100' then 1 else 0 end),
	REMEDIAL_ANY_B                 = max(case when rcc.MarkPoints >= 3 and rcc.CourseCode = '2100' then 1 else 0 end)
INTO
	mmap.RetroEngl
FROM
	dbo.Student s
	inner join
	mmap.RetrospectivePerformance rp
		on s.InterSegmentKey = rp.InterSegmentKey
	inner join
	mmap.RetrospectiveCourseContent rcc
		on rp.InterSegmentKey = rcc.InterSegmentKey
	inner join
	mmap.CollegeCourseContent ccc
		on ccc.InterSegmentKey = s.InterSegmentKey
	inner join
	#College c
		on c.CollegeCode = ccc.CollegeId
	left outer join
	dbo.StudentStar ss
		on ss.derkey1 = s.InterSegmentKey
WHERE
	ccc.TopCode = @TopCode
	and rp.DepartmentCode = 0
	and rcc.DepartmentCode = @DepartmentCode
	and s.IsHighSchoolCollision = 0
	and s.IsGrade11SectionInclusive = 1
	and ccc.LevelSelector = 1
	and (
		(
			@IsCollege = 1
			and
			ccc.CollegeCounter = 1
		)
		or
		(
			@IsDistrict = 1
			and
			ccc.DistrictCounter = 1
		)
		or
		(
			@IsDistrict = 0
			and
			@IsCollege = 0
		)
	)
GROUP BY
	s.InterSegmentKey,
	ss.engl_eap_ind,
	ss.engl_scaled_score,
	ss.engl_cluster_1,
	ss.engl_cluster_2,
	ss.engl_cluster_3,
	ss.engl_cluster_4,
	ss.engl_cluster_5,
	ss.engl_cluster_6,
	ss.math_subject,
	ss.math_eap_ind,
	ss.math_scaled_score,
	ss.math_cluster_1,
	ss.math_cluster_2,
	ss.math_cluster_3,
	ss.math_cluster_4,
	ss.math_cluster_5,
	ss.math_cluster_6,
	ss.esl_eo_ind,
	ss.esl_ifep_ind,
	ss.esl_el_ind,
	ss.esl_rfep_ind;

-- UPDATE
-- 	t
-- SET
-- 	re.cc_tran_college_id            = t.CollegeId,
-- 	re.cc_tran_course_level          = t.CourseLevel,
-- 	re.cc_tran_year_term_code        = t.YearTermCode,
-- 	re.cc_tran_course_id             = t.CourseId,
-- 	re.cc_tran_course_title          = t.CourseTitle,
-- 	re.cc_tran_course_grade          = t.SectionMark,
-- 	re.cc_tran_course_grade_points   = t.MarkPoints,
-- 	re.cc_tran_course_grade_category = t.MarkCategory,
-- 	re.cc_tran_course_success_ind    = convert(tinyint, t.MarkIsSuccess)
-- FROM
-- 	mmap.RetroEngl re
-- 	inner join
-- 	mmap.CollegeCourseContent t
-- 		on t.InterSegmentKey = re.InterSegmentKey
-- WHERE
-- 	t.TopCode = @TopCode
-- 	and t.CoRequisiteTranscriptSelector = (
-- 		SELECT
-- 			min(t1.CoRequisiteTranscriptSelector)
-- 		FROM
-- 			mmap.CollegeCourseContent t1
-- 		WHERE
-- 			t1.InterSegmentKey = t.InterSegmentKey
-- 			and t1.CollegeId = t.CollegeId
-- 			and t1.TopCode = t.TopCode
-- 	);

-- UPDATE
-- 	t
-- SET
-- 	re.cc_supp_college_id            = t.CollegeId,
-- 	re.cc_supp_course_level          = t.CourseLevel,
-- 	re.cc_supp_year_term_code        = t.YearTermCode,
-- 	re.cc_supp_course_id             = t.CourseId,
-- 	re.cc_supp_course_title          = t.CourseTitle,
-- 	re.cc_supp_course_grade          = t.SectionMark,
-- 	re.cc_supp_course_grade_points   = t.MarkPoints,
-- 	re.cc_supp_course_grade_category = t.MarkCategory,
-- 	re.cc_supp_course_success_ind    = convert(tinyint, t.MarkIsSuccess)
-- FROM
-- 	mmap.RetroEngl re
-- 	inner join
-- 	mmap.CollegeCourseContent t
-- 		on t.InterSegmentKey = re.InterSegmentKey
-- WHERE
-- 	t.TopCode = @TopCode
-- 	and t.CoRequisiteSupportSelector = (
-- 		SELECT
-- 			min(t1.CoRequisiteSupportSelector)
-- 		FROM
-- 			mmap.CollegeCourseContent t1
-- 		WHERE
-- 			t1.InterSegmentKey = t.InterSegmentKey
-- 			and t1.CollegeId = t.CollegeId
-- 			and t1.TopCode = t.TopCode
-- 	);

DELETE
FROM
	mmap.RetroEngl
WHERE
	hs_09_gpa > 4
	or hs_10_gpa > 4
	or hs_11_gpa > 4
	or hs_12_gpa > 4;

SET @exestr = 'bcp "SELECT * FROM calpass.mmap.RetroEngl" QUERYOUT "C:\Users\dlamoree.ERP\desktop\' + @OrganizationCode + '_Retrospective_English.txt" -T -c -q';

-- extract
EXECUTE xp_cmdshell
	@exestr;

-- extract
EXECUTE sp_spss_output
	'calpass',
	'mmap',
	'RetroEngl',
	'C:\Users\dlamoree.ERP\desktop\',
	'1';

/* MATHEMATICS */

SET @TopCode = '170100';
SET @DepartmentCode = 18;

SELECT
	organization_code              = @OrganizationCode,
	derkey1                        = s.InterSegmentKey,
	hs_09_grade_level              = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.GradeCode end),
	hs_09_gpa                      = max(case when rp.GradeCode = '09' then rp.CumulativeGradePointAverage end),
	hs_09_school_code              = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
	hs_09_year_term_code           = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
	hs_09_course_id                = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseCode end),
	hs_09_course_rank              = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.ContentRank end),
	hs_09_course_title             = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
	hs_09_course_grade             = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.SectionMark end),
	hs_09_course_grade_points      = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
	hs_09_course_success_ind       = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then convert(tinyint, rcc.IsSuccess) end),
	hs_09_course_ag_code           = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
	hs_09_course_level_code        = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
	hs_09_course_type_code         = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
	hs_10_grade_level              = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.GradeCode end),
	hs_10_gpa                      = max(case when rp.GradeCode = '10' then rp.CumulativeGradePointAverage end),
	hs_10_school_code              = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
	hs_10_year_term_code           = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
	hs_10_course_id                = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseCode end),
	hs_10_course_rank              = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.ContentRank end),
	hs_10_course_title             = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
	hs_10_course_grade             = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.SectionMark end),
	hs_10_course_grade_points      = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
	hs_10_course_success_ind       = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then convert(tinyint, rcc.IsSuccess) end),
	hs_10_course_ag_code           = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
	hs_10_course_level_code        = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
	hs_10_course_type_code         = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
	hs_11_grade_level              = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.GradeCode end),
	hs_11_gpa                      = max(case when rp.GradeCode = '11' then rp.CumulativeGradePointAverage end),
	hs_11_school_code              = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
	hs_11_year_term_code           = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
	hs_11_course_id                = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseCode end),
	hs_11_course_rank              = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.ContentRank end),
	hs_11_course_title             = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
	hs_11_course_grade             = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.SectionMark end),
	hs_11_course_grade_points      = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
	hs_11_course_success_ind       = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then convert(tinyint, rcc.IsSuccess) end),
	hs_11_course_ag_code           = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
	hs_11_course_level_code        = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
	hs_11_course_type_code         = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
	hs_12_grade_level              = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.GradeCode end),
	hs_12_gpa                      = max(case when rp.GradeCode = '12' then rp.CumulativeGradePointAverage end),
	hs_12_school_code              = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
	hs_12_year_term_code           = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
	hs_12_course_id                = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseCode end),
	hs_12_course_rank              = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.ContentRank end),
	hs_12_course_title             = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
	hs_12_course_grade             = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.SectionMark end),
	hs_12_course_grade_points      = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
	hs_12_course_success_ind       = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then convert(tinyint, rcc.IsSuccess) end),
	hs_12_course_ag_code           = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
	hs_12_course_level_code        = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
	hs_12_course_type_code         = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
	hs_last_grade_level            = max(case when rcc.GradeSelector = 1 then rcc.GradeCode end),
	hs_last_gpa                    = max(case when rp.IsLast = 1 then rp.CumulativeGradePointAverage end),
	hs_last_school_code            = max(case when rcc.RecencySelector= 1 then rcc.SchoolCode end),
	hs_last_year_term_code         = max(case when rcc.RecencySelector= 1 then rcc.YearTermCode end),
	hs_last_course_id              = max(case when rcc.RecencySelector= 1 then rcc.CourseCode end),
	hs_last_course_rank            = max(case when rcc.RecencySelector= 1 then rcc.ContentRank end),
	hs_last_course_title           = max(case when rcc.RecencySelector= 1 then rcc.CourseTitle end),
	hs_last_course_grade           = max(case when rcc.RecencySelector= 1 then rcc.SectionMark end),
	hs_last_course_grade_points    = max(case when rcc.RecencySelector= 1 then rcc.MarkPoints end),
	hs_last_course_success_ind     = max(case when rcc.RecencySelector= 1 then convert(tinyint, rcc.IsSuccess) end),
	hs_last_course_ag_code         = max(case when rcc.RecencySelector= 1 then rcc.CourseAGCode end),
	hs_last_course_level_code      = max(case when rcc.RecencySelector= 1 then rcc.CourseLevelCode end),
	hs_last_course_type_code       = max(case when rcc.RecencySelector= 1 then rcc.CourseTypeCode end),
	engl_eap_ind                   = ss.engl_eap_ind,
	engl_scaled_score              = ss.engl_scaled_score,
	engl_cluster_1                 = ss.engl_cluster_1,
	engl_cluster_2                 = ss.engl_cluster_2,
	engl_cluster_3                 = ss.engl_cluster_3,
	engl_cluster_4                 = ss.engl_cluster_4,
	engl_cluster_5                 = ss.engl_cluster_5,
	engl_cluster_6                 = ss.engl_cluster_6,
	math_subject                   = ss.math_subject,
	math_eap_ind                   = ss.math_eap_ind,
	math_scaled_score              = ss.math_scaled_score,
	math_cluster_1                 = ss.math_cluster_1,
	math_cluster_2                 = ss.math_cluster_2,
	math_cluster_3                 = ss.math_cluster_3,
	math_cluster_4                 = ss.math_cluster_4,
	math_cluster_5                 = ss.math_cluster_5,
	math_cluster_6                 = ss.math_cluster_6,
	esl_eo_ind                     = ss.esl_eo_ind,
	esl_ifep_ind                   = ss.esl_ifep_ind,
	esl_el_ind                     = ss.esl_el_ind,
	esl_rfep_ind                   = ss.esl_rfep_ind,
	-- college: First
	cc_first_college_id            = max(case when ccc.FirstSelector = 1 then ccc.CollegeId end),
	cc_first_course_level          = max(case when ccc.FirstSelector = 1 then ccc.CourseLevel end),
	cc_first_year_term_code        = max(case when ccc.FirstSelector = 1 then ccc.YearTermCode end),
	cc_first_course_id             = max(case when ccc.FirstSelector = 1 then ccc.CourseId end),
	cc_first_course_title          = max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end),
	cc_first_course_grade          = max(case when ccc.FirstSelector = 1 then ccc.SectionMark end),
	cc_first_course_grade_points   = max(case when ccc.FirstSelector = 1 then ccc.MarkPoints end),
	cc_first_course_grade_category = max(case when ccc.FirstSelector = 1 then ccc.MarkCategory end),
	cc_first_course_success_ind    = max(case when ccc.FirstSelector = 1 then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: Y
	cc_00_college_id               = max(case when ccc.CourseLevel = 'Y' then ccc.CollegeId end),
	cc_00_course_level             = max(case when ccc.CourseLevel = 'Y' then ccc.CourseLevel end),
	cc_00_year_term_code           = max(case when ccc.CourseLevel = 'Y' then ccc.YearTermCode end),
	cc_00_course_id                = max(case when ccc.CourseLevel = 'Y' then ccc.CourseId end),
	cc_00_course_title             = max(case when ccc.CourseLevel = 'Y' then ccc.CourseTitle end),
	cc_00_course_grade             = max(case when ccc.CourseLevel = 'Y' then ccc.SectionMark end),
	cc_00_course_grade_points      = max(case when ccc.CourseLevel = 'Y' then ccc.MarkPoints end),
	cc_00_course_grade_category    = max(case when ccc.CourseLevel = 'Y' then ccc.MarkCategory end),
	cc_00_course_success_ind       = max(case when ccc.CourseLevel = 'Y' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: A
	cc_01_college_id               = max(case when ccc.CourseLevel = 'A' then ccc.CollegeId end),
	cc_01_course_level             = max(case when ccc.CourseLevel = 'A' then ccc.CourseLevel end),
	cc_01_year_term_code           = max(case when ccc.CourseLevel = 'A' then ccc.YearTermCode end),
	cc_01_course_id                = max(case when ccc.CourseLevel = 'A' then ccc.CourseId end),
	cc_01_course_title             = max(case when ccc.CourseLevel = 'A' then ccc.CourseTitle end),
	cc_01_course_grade             = max(case when ccc.CourseLevel = 'A' then ccc.SectionMark end),
	cc_01_course_grade_points      = max(case when ccc.CourseLevel = 'A' then ccc.MarkPoints end),
	cc_01_course_grade_category    = max(case when ccc.CourseLevel = 'A' then ccc.MarkCategory end),
	cc_01_course_success_ind       = max(case when ccc.CourseLevel = 'A' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: B
	cc_02_college_id               = max(case when ccc.CourseLevel = 'B' then ccc.CollegeId end),
	cc_02_course_level             = max(case when ccc.CourseLevel = 'B' then ccc.CourseLevel end),
	cc_02_year_term_code           = max(case when ccc.CourseLevel = 'B' then ccc.YearTermCode end),
	cc_02_course_id                = max(case when ccc.CourseLevel = 'B' then ccc.CourseId end),
	cc_02_course_title             = max(case when ccc.CourseLevel = 'B' then ccc.CourseTitle end),
	cc_02_course_grade             = max(case when ccc.CourseLevel = 'B' then ccc.SectionMark end),
	cc_02_course_grade_points      = max(case when ccc.CourseLevel = 'B' then ccc.MarkPoints end),
	cc_02_course_grade_category    = max(case when ccc.CourseLevel = 'B' then ccc.MarkCategory end),
	cc_02_course_success_ind       = max(case when ccc.CourseLevel = 'B' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: C
	cc_03_college_id               = max(case when ccc.CourseLevel = 'C' then ccc.CollegeId end),
	cc_03_course_level             = max(case when ccc.CourseLevel = 'C' then ccc.CourseLevel end),
	cc_03_year_term_code           = max(case when ccc.CourseLevel = 'C' then ccc.YearTermCode end),
	cc_03_course_id                = max(case when ccc.CourseLevel = 'C' then ccc.CourseId end),
	cc_03_course_title             = max(case when ccc.CourseLevel = 'C' then ccc.CourseTitle end),
	cc_03_course_grade             = max(case when ccc.CourseLevel = 'C' then ccc.SectionMark end),
	cc_03_course_grade_points      = max(case when ccc.CourseLevel = 'C' then ccc.MarkPoints end),
	cc_03_course_grade_category    = max(case when ccc.CourseLevel = 'C' then ccc.MarkCategory end),
	cc_03_course_success_ind       = max(case when ccc.CourseLevel = 'C' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- college: D
	cc_04_college_id               = max(case when ccc.CourseLevel = 'D' then ccc.CollegeId end),
	cc_04_course_level             = max(case when ccc.CourseLevel = 'D' then ccc.CourseLevel end),
	cc_04_year_term_code           = max(case when ccc.CourseLevel = 'D' then ccc.YearTermCode end),
	cc_04_course_id                = max(case when ccc.CourseLevel = 'D' then ccc.CourseId end),
	cc_04_course_title             = max(case when ccc.CourseLevel = 'D' then ccc.CourseTitle end),
	cc_04_course_grade             = max(case when ccc.CourseLevel = 'D' then ccc.SectionMark end),
	cc_04_course_grade_points      = max(case when ccc.CourseLevel = 'D' then ccc.MarkPoints end),
	cc_04_course_grade_category    = max(case when ccc.CourseLevel = 'D' then ccc.MarkCategory end),
	cc_04_course_success_ind       = max(case when ccc.CourseLevel = 'D' then convert(tinyint, ccc.MarkIsSuccess) end),
	-- binary variables
	/* Pre Algebra: 09, 10, 11 */
	hs_pre_alg_UP11                = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_F              = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_Dminus         = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_D              = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_Dplus          = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_Cminus         = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_C              = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_Cplus          = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_Bminus         = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_B              = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_Bplus          = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_Aminus         = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_UP11_A              = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	/* Pre Algebra: 09, 10, 11, 12 */
	hs_pre_alg_ANY                 = max(case when rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_F               = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_Dminus          = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_D               = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_Dplus           = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_Cminus          = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_C               = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_Cplus           = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_Bminus          = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_B               = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_Bplus           = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_Aminus          = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	hs_pre_alg_ANY_A               = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode in ('2433','2424') then 1 else 0 end),
	/* Algebra I: 09, 10, 11 */
	hs_alg_i_UP11                  = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_F                = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_Dminus           = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_D                = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_Dplus            = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_Cminus           = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_C                = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_Cplus            = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_Bminus           = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_B                = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_Bplus            = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_Aminus           = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_UP11_A                = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	/* Algebra I: 09, 10, 11, 12 */
	hs_alg_i_ANY                   = max(case when rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_F                 = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_Dminus            = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_D                 = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_Dplus             = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_Cminus            = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_C                 = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_Cplus             = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_Bminus            = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_B                 = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_Bplus             = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_Aminus            = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	hs_alg_i_ANY_A                 = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode in ('2403','2428','2429','2467') then 1 else 0 end),
	/* Algebra II: 09, 10, 11 */
	hs_alg_ii_UP11                 = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_F               = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_Dminus          = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_D               = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_Dplus           = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_Cminus          = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_C               = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_Cplus           = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_Bminus          = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_B               = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_Bplus           = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_Aminus          = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_UP11_A               = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	/* Algebra II: 09, 10, 11, 12 */
	hs_alg_ii_ANY                  = max(case when rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_F                = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_Dminus           = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_D                = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_Dplus            = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_Cminus           = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_C                = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_Cplus            = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_Bminus           = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_B                = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_Bplus            = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_Aminus           = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	hs_alg_ii_ANY_A                = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode in ('2404','2408','2417','2411','2412','2467') then 1 else 0 end),
	/* Geometry: 09, 10, 11 */
	hs_geo_UP11                    = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_F                  = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_Dminus             = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_D                  = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_Dplus              = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_Cminus             = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_C                  = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_Cplus              = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_Bminus             = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_B                  = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_Bplus              = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_Aminus             = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_UP11_A                  = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	/* Geometry: 09, 10, 11, 12 */
	hs_geo_ANY                     = max(case when rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_F                   = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_Dminus              = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_D                   = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_Dplus               = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_Cminus              = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_C                   = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_Cplus               = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_Bminus              = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_B                   = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_Bplus               = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_Aminus              = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	hs_geo_ANY_A                   = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode in ('2405','2406','2409','2413') then 1 else 0 end),
	/* Trigonometry: 09, 10, 11 */
	hs_trig_UP11                   = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_F                 = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_Dminus            = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_D                 = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_Dplus             = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_Cminus            = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_C                 = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_Cplus             = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_Bminus            = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_B                 = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_Bplus             = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_Aminus            = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_UP11_A                 = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	/* Trigonometry: 09, 10, 11, 12 */
	hs_trig_ANY                    = max(case when rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_F                  = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_Dminus             = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_D                  = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_Dplus              = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_Cminus             = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_C                  = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_Cplus              = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_Bminus             = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_B                  = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_Bplus              = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_Aminus             = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	hs_trig_ANY_A                  = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode in ('2407','2408','2409','2468') then 1 else 0 end),
	/* PreCalculus: 09, 10, 11 */
	hs_pre_calc_UP11               = max(case when rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_F             = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_Dminus        = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_D             = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_Dplus         = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_Cminus        = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_C             = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_Cplus         = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_Bminus        = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_B             = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_Bplus         = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_Aminus        = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_UP11_A             = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2414' then 1 else 0 end),
	/* PreCalculus: 09, 10, 11, 12 */
	hs_pre_calc_ANY                = max(case when rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_F              = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_Dminus         = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_D              = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_Dplus          = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_Cminus         = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_C              = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_Cplus          = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_Bminus         = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_B              = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_Bplus          = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_Aminus         = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode = '2414' then 1 else 0 end),
	hs_pre_calc_ANY_A              = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode = '2414' then 1 else 0 end),
	/* Calculus: 09, 10, 11 */
	hs_calc_UP11                   = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_F                 = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_Dminus            = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_D                 = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_Dplus             = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_Cminus            = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_C                 = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_Cplus             = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_Bminus            = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_B                 = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_Bplus             = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_Aminus            = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_UP11_A                 = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	/* Calculus: 09, 10, 11, 12 */
	hs_calc_ANY                    = max(case when rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_F                  = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_Dminus             = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_D                  = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_Dplus              = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_Cminus             = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_C                  = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_Cplus              = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_Bminus             = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_B                  = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_Bplus              = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_Aminus             = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	hs_calc_ANY_A                  = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode in ('2415','2480','2481') then 1 else 0 end),
	/* Calculus AB: 09, 10, 11 */
	hs_calc_ab_UP11                = max(case when rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_F              = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_Dminus         = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_D              = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_Dplus          = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_Cminus         = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_C              = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_Cplus          = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_Bminus         = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_B              = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_Bplus          = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_Aminus         = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_UP11_A              = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2480' then 1 else 0 end),
	/* Calculus AB: 09, 10, 11, 12 */
	hs_calc_ab_ANY                 = max(case when rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_F               = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_Dminus          = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_D               = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_Dplus           = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_Cminus          = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_C               = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_Cplus           = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_Bminus          = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_B               = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_Bplus           = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_Aminus          = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode = '2480' then 1 else 0 end),
	hs_calc_ab_ANY_A               = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode = '2480' then 1 else 0 end),
	/* Calculus BC: 09, 10, 11 */
	hs_calc_bc_UP11                = max(case when rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_F              = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_Dminus         = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_D              = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_Dplus          = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_Cminus         = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_C              = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_Cplus          = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_Bminus         = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_B              = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_Bplus          = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_Aminus         = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_UP11_A              = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2481' then 1 else 0 end),
	/* Calculus BC: 09, 10, 11, 12 */
	hs_calc_bc_ANY                 = max(case when rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_F               = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_Dminus          = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_D               = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_Dplus           = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_Cminus          = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_C               = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_Cplus           = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_Bminus          = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_B               = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_Bplus           = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_Aminus          = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode = '2481' then 1 else 0 end),
	hs_calc_bc_ANY_A               = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode = '2481' then 1 else 0 end),
	/* Statistics: 09, 10, 11 */
	hs_stat_UP11                   = max(case when rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_F                 = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_Dminus            = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_D                 = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_Dplus             = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_Cminus            = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_C                 = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_Cplus             = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_Bminus            = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_B                 = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_Bplus             = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_Aminus            = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_UP11_A                 = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	/* Statistics: 09, 10, 11, 12 */
	hs_stat_ANY                    = max(case when rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_F                  = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_Dminus             = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_D                  = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_Dplus              = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_Cminus             = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_C                  = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_Cplus              = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_Bminus             = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_B                  = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_Bplus              = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_Aminus             = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	hs_stat_ANY_A                  = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode in ('2410','2483') then 1 else 0 end),
	/* Statistics AP: 09, 10, 11 */
	hs_stat_ap_UP11                = max(case when rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_F              = max(case when rcc.MarkPoints  = 0.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_Dminus         = max(case when rcc.MarkPoints >= 0.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_D              = max(case when rcc.MarkPoints >= 1.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_Dplus          = max(case when rcc.MarkPoints >= 1.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_Cminus         = max(case when rcc.MarkPoints >= 1.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_C              = max(case when rcc.MarkPoints >= 2.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_Cplus          = max(case when rcc.MarkPoints >= 2.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_Bminus         = max(case when rcc.MarkPoints >= 2.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_B              = max(case when rcc.MarkPoints >= 3.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_Bplus          = max(case when rcc.MarkPoints >= 3.3 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_Aminus         = max(case when rcc.MarkPoints >= 3.7 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_UP11_A              = max(case when rcc.MarkPoints  = 4.0 and rcc.GradeCode <= '11' and rcc.CourseCode = '2483' then 1 else 0 end),
	/* Statistics AP: 09, 10, 11, 12 */
	hs_stat_ap_ANY                 = max(case when rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_F               = max(case when rcc.MarkPoints  = 0.0 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_Dminus          = max(case when rcc.MarkPoints >= 0.3 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_D               = max(case when rcc.MarkPoints >= 1.0 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_Dplus           = max(case when rcc.MarkPoints >= 1.3 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_Cminus          = max(case when rcc.MarkPoints >= 1.7 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_C               = max(case when rcc.MarkPoints >= 2.0 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_Cplus           = max(case when rcc.MarkPoints >= 2.3 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_Bminus          = max(case when rcc.MarkPoints >= 2.7 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_B               = max(case when rcc.MarkPoints >= 3.0 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_Bplus           = max(case when rcc.MarkPoints >= 3.3 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_Aminus          = max(case when rcc.MarkPoints >= 3.7 and rcc.CourseCode = '2483' then 1 else 0 end),
	hs_stat_ap_ANY_A               = max(case when rcc.MarkPoints  = 4.0 and rcc.CourseCode = '2483' then 1 else 0 end),
	CC_GE_MATH = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH FOR%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH%LIB%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH%ELEM%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH TOPICS%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATHEMATICAL%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATHEMATICS%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%THE IDEAS%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%LIBERAL%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%SURVEY%' and max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) not like '%CALC%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%CONCEPTS%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%IDEAS%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%TEACH%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%LIFE%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BUS%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BRIEF%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%SOC%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BIO%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%SHORT%' then 1
			else 0
		end,
	CC_STATISTICS = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseId    end) like '%STA%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%STA%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO STA%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEM PROB%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEM. PROB%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEM STA%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEMENTARY STA%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEMNTRY STA%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO PROB%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO TO PROB%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO APPLIED STATS%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%HONORS STA%' then 1
			else 0
		end,
	CC_COLL_ALG =
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%COLL%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ADVANCED ALG%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ALGEBRA FOR CALC%' then 1
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%PRECALC%' then 0
			else 0
		end,
	CC_PRE_CALC = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%PRE%CALC%' then 1
			else 0
		end,
	CC_CALC_I = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%CALC%' then
				case
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%PRE%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%II%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%2%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%3%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%4A%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%DIFF%' then 0
					else 1
				end
			else 0
		end,
	CC_CALC_II = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%CALC%'
				and max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) not like '%PRE%' then
				case
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%II%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%2%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%3%' then 1
					else 0
				end
			else 0
		end,
	CC_BUS_MATH = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BUS%'
				and max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) not like '%FINITE%'
				and max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) not like '%DISCRETE%' then 1
			else 0
		end,
	CC_DIFF_EQ = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%DIFF%' then 1
			else 0
		end,
	CC_TRIG = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%Trig%' then 1
			else 0
		end,
	CC_TRANSFER_MATH_TYPE = 
		case
			when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) = 'Y' then
				case
					-- CC_GE_MATH
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH FOR%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH/LIB%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH/ELEM%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH:ELEM%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATH TOPICS%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATHEMATICAL%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%MATHEMATICS%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%THE IDEAS%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%LIBERAL%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%SURVEY%'
						and max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) not like '%CALC%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%CONCEPTS%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%IDEAS%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%TEACH%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%LIFE%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BUS%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BRIEF%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%SOC%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BIO%' then 0
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%SHORT%' then 0
					-- CC_STATISTICS
					when max(case when ccc.FirstSelector = 1 then ccc.CourseId    end) like '%STA%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%STA%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO STA%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEM PROB%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEM. PROB%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEM STA%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEMENTARY STA%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ELEMNTRY STA%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO PROB%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO TO PROB%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%INTRO APPLIED STATS%' then 1
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%HONORS STA%' then 1
					-- CC_COLL_ALG
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%COLL%' then 2
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ADVANCED ALG%' then 2
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%ALGEBRA FOR CALC%' then 2
					-- CC_PRE_CALC & CC_CALC_I & CC_CALC_II
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%CALC%' then
						case
							when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%PRE%' then 3
							when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%II%' then 5
							when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%2%' then 5
							when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%3%' then 5
							when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%4A%' then 5
							when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%DIFF%' then 7
							else 4
						end
					-- CC_BUS_MATH
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%BUS%'
						and max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) not like '%FINITE%'
						and max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) not like '%DISCRETE%' then 1
					-- CC_DIFF_EQ
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%DIFF%' then 7
					-- CC_LINEAR_ALGEBRA not in variables
					when max(case when ccc.FirstSelector = 1 then ccc.CourseTitle end) like '%LINEAR%' then 8
					else -9
				end
			else -9
		end
INTO
	mmap.RetroMath
FROM
	dbo.Student s
	inner join
	mmap.RetrospectivePerformance rp
		on s.InterSegmentKey = rp.InterSegmentKey
	inner join
	mmap.RetrospectiveCourseContent rcc
		on rp.InterSegmentKey = rcc.InterSegmentKey
	inner join
	mmap.CollegeCourseContent ccc
		on ccc.InterSegmentKey = s.InterSegmentKey
	inner join
	#College c
		on c.CollegeCode = ccc.CollegeId
	left outer join
	dbo.StudentStar ss
		on ss.derkey1 = s.InterSegmentKey
WHERE
	ccc.TopCode = @TopCode
	and rp.DepartmentCode = 0
	and rcc.DepartmentCode = @DepartmentCode
	and s.IsHighSchoolCollision = 0
	and s.IsGrade11SectionInclusive = 1
	and ccc.LevelSelector = 1
	and (
		(
			@IsCollege = 1
			and
			ccc.CollegeCounter = 1
		)
		or
		(
			@IsDistrict = 1
			and
			ccc.DistrictCounter = 1
		)
		or
		(
			@IsDistrict = 0
			and
			@IsCollege = 0
		)
	)
GROUP BY
	s.InterSegmentKey,
	ss.engl_eap_ind,
	ss.engl_scaled_score,
	ss.engl_cluster_1,
	ss.engl_cluster_2,
	ss.engl_cluster_3,
	ss.engl_cluster_4,
	ss.engl_cluster_5,
	ss.engl_cluster_6,
	ss.math_subject,
	ss.math_eap_ind,
	ss.math_scaled_score,
	ss.math_cluster_1,
	ss.math_cluster_2,
	ss.math_cluster_3,
	ss.math_cluster_4,
	ss.math_cluster_5,
	ss.math_cluster_6,
	ss.esl_eo_ind,
	ss.esl_ifep_ind,
	ss.esl_el_ind,
	ss.esl_rfep_ind;

-- UPDATE
-- 	t
-- SET
-- 	rm.cc_tran_college_id            = t.CollegeId,
-- 	rm.cc_tran_course_level          = t.CourseLevel,
-- 	rm.cc_tran_year_term_code        = t.YearTermCode,
-- 	rm.cc_tran_course_id             = t.CourseId,
-- 	rm.cc_tran_course_title          = t.CourseTitle,
-- 	rm.cc_tran_course_grade          = t.SectionMark,
-- 	rm.cc_tran_course_grade_points   = t.MarkPoints,
-- 	rm.cc_tran_course_grade_category = t.MarkCategory,
-- 	rm.cc_tran_course_success_ind    = convert(tinyint, t.MarkIsSuccess)
-- FROM
-- 	RetroMath rm
-- 	inner join
-- 	mmap.CollegeCourseContent t
-- 		on t.InterSegmentKey = re.InterSegmentKey
-- WHERE
-- 	t.TopCode = @TopCode
-- 	and t.CoRequisiteTranscriptSelector = (
-- 		SELECT
-- 			min(t1.CoRequisiteTranscriptSelector)
-- 		FROM
-- 			mmap.CollegeCourseContent t1
-- 		WHERE
-- 			t1.InterSegmentKey = t.InterSegmentKey
-- 			and t1.CollegeId = t.CollegeId
-- 			and t1.TopCode = t.TopCode
-- 	);

-- UPDATE
-- 	t
-- SET
-- 	rm.cc_supp_college_id            = t.CollegeId,
-- 	rm.cc_supp_course_level          = t.CourseLevel,
-- 	rm.cc_supp_year_term_code        = t.YearTermCode,
-- 	rm.cc_supp_course_id             = t.CourseId,
-- 	rm.cc_supp_course_title          = t.CourseTitle,
-- 	rm.cc_supp_course_grade          = t.SectionMark,
-- 	rm.cc_supp_course_grade_points   = t.MarkPoints,
-- 	rm.cc_supp_course_grade_category = t.MarkCategory,
-- 	rm.cc_supp_course_success_ind    = convert(tinyint, t.MarkIsSuccess)
-- FROM
-- 	RetroMath rm
-- 	inner join
-- 	mmap.CollegeCourseContent t
-- 		on t.InterSegmentKey = re.InterSegmentKey
-- WHERE
-- 	t.TopCode = @TopCode
-- 	and t.CoRequisiteSupportSelector = (
-- 		SELECT
-- 			min(t1.CoRequisiteSupportSelector)
-- 		FROM
-- 			mmap.CollegeCourseContent t1
-- 		WHERE
-- 			t1.InterSegmentKey = t.InterSegmentKey
-- 			and t1.CollegeId = t.CollegeId
-- 			and t1.TopCode = t.TopCode
-- 	);

DELETE
FROM
	mmap.RetroMath
WHERE
	hs_09_gpa > 4
	or hs_10_gpa > 4
	or hs_11_gpa > 4
	or hs_12_gpa > 4;

SET @exestr = 'bcp "SELECT * FROM calpass.mmap.RetroMath" QUERYOUT "C:\Users\dlamoree.ERP\desktop\' + @OrganizationCode + '_Retrospective_Mathematics.txt" -T -c -q';

-- extract
EXECUTE xp_cmdshell
	@exestr;

-- extract
EXECUTE sp_spss_output
	'calpass',
	'mmap',
	'RetroMath',
	'C:\Users\dlamoree.ERP\desktop\',
	'1';