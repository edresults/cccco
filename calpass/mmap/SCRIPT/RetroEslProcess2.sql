TRUNCATE TABLE Mmap.RetroEsl;

INSERT INTO
    Mmap.RetroEsl
    (
        InterSegmentKey,
        CCEACNFLCollegeCode,
        CCEACNFLYearTermCode,
        CCEACNFLCourseId,
        CCEACNFLSectionId,
        CCEACNFLCourseControlNumber,
        CCEACNFLCourseTopCode,
        CCEACNFLCourseTitle,
        CCEACNFLCourseMarkLetter,
        CCEACNFLCourseMarkPoints,
        CCEACNFLCourseLevelCode,
        CCEACNFLCourseCreditCode,
        CCEACN00CollegeCode,
        CCEACN00YearTermCode,
        CCEACN00CourseId,
        CCEACN00SectionId,
        CCEACN00CourseControlNumber,
        CCEACN00CourseTopCode,
        CCEACN00CourseTitle,
        CCEACN00CourseMarkLetter,
        CCEACN00CourseMarkPoints,
        CCEACN00CourseLevelCode,
        CCEACN00CourseCreditCode,
        CCEACN01CollegeCode,
        CCEACN01YearTermCode,
        CCEACN01CourseId,
        CCEACN01SectionId,
        CCEACN01CourseControlNumber,
        CCEACN01CourseTopCode,
        CCEACN01CourseTitle,
        CCEACN01CourseMarkLetter,
        CCEACN01CourseMarkPoints,
        CCEACN01CourseLevelCode,
        CCEACN01CourseCreditCode,
        CCEACN02CollegeCode,
        CCEACN02YearTermCode,
        CCEACN02CourseId,
        CCEACN02SectionId,
        CCEACN02CourseControlNumber,
        CCEACN02CourseTopCode,
        CCEACN02CourseTitle,
        CCEACN02CourseMarkLetter,
        CCEACN02CourseMarkPoints,
        CCEACN02CourseLevelCode,
        CCEACN02CourseCreditCode,
        CCEACN03CollegeCode,
        CCEACN03YearTermCode,
        CCEACN03CourseId,
        CCEACN03SectionId,
        CCEACN03CourseControlNumber,
        CCEACN03CourseTopCode,
        CCEACN03CourseTitle,
        CCEACN03CourseMarkLetter,
        CCEACN03CourseMarkPoints,
        CCEACN03CourseLevelCode,
        CCEACN03CourseCreditCode,
        CCEACN04CollegeCode,
        CCEACN04YearTermCode,
        CCEACN04CourseId,
        CCEACN04SectionId,
        CCEACN04CourseControlNumber,
        CCEACN04CourseTopCode,
        CCEACN04CourseTitle,
        CCEACN04CourseMarkLetter,
        CCEACN04CourseMarkPoints,
        CCEACN04CourseLevelCode,
        CCEACN04CourseCreditCode,
        CCEACN05CollegeCode,
        CCEACN05YearTermCode,
        CCEACN05CourseId,
        CCEACN05SectionId,
        CCEACN05CourseControlNumber,
        CCEACN05CourseTopCode,
        CCEACN05CourseTitle,
        CCEACN05CourseMarkLetter,
        CCEACN05CourseMarkPoints,
        CCEACN05CourseLevelCode,
        CCEACN05CourseCreditCode,
        CCEACN06CollegeCode,
        CCEACN06YearTermCode,
        CCEACN06CourseId,
        CCEACN06SectionId,
        CCEACN06CourseControlNumber,
        CCEACN06CourseTopCode,
        CCEACN06CourseTitle,
        CCEACN06CourseMarkLetter,
        CCEACN06CourseMarkPoints,
        CCEACN06CourseLevelCode,
        CCEACN06CourseCreditCode,
        CCEACN07CollegeCode,
        CCEACN07YearTermCode,
        CCEACN07CourseId,
        CCEACN07SectionId,
        CCEACN07CourseControlNumber,
        CCEACN07CourseTopCode,
        CCEACN07CourseTitle,
        CCEACN07CourseMarkLetter,
        CCEACN07CourseMarkPoints,
        CCEACN07CourseLevelCode,
        CCEACN07CourseCreditCode,
        CCEACN08CollegeCode,
        CCEACN08YearTermCode,
        CCEACN08CourseId,
        CCEACN08SectionId,
        CCEACN08CourseControlNumber,
        CCEACN08CourseTopCode,
        CCEACN08CourseTitle,
        CCEACN08CourseMarkLetter,
        CCEACN08CourseMarkPoints,
        CCEACN08CourseLevelCode,
        CCEACN08CourseCreditCode,
        CCENCRFLCollegeCode,
        CCENCRFLYearTermCode,
        CCENCRFLCourseId,
        CCENCNFLSectionId,
        CCENCRFLCourseControlNumber,
        CCENCRFLCourseTopCode,
        CCENCRFLCourseTitle,
        CCENCRFLCourseMarkLetter,
        CCENCRFLCourseMarkPoints,
        CCENCRFLCourseLevelCode,
        CCENCR00CollegeCode,
        CCENCR00YearTermCode,
        CCENCR00CourseId,
        CCENCR00SectionId,
        CCENCR00CourseControlNumber,
        CCENCR00CourseTopCode,
        CCENCR00CourseTitle,
        CCENCR00CourseMarkLetter,
        CCENCR00CourseMarkPoints,
        CCENCR00CourseLevelCode,
        CCENCR002NCollegeCode,
        CCENCR002NYearTermCode,
        CCENCR002NCourseId,
        CCENCR002NSectionId,
        CCENCR002NCourseControlNumber,
        CCENCR002NCourseTopCode,
        CCENCR002NCourseTitle,
        CCENCR002NCourseMarkLetter,
        CCENCR002NCourseMarkPoints,
        CCENCR002NCourseLevelCode,
        CCENCR01CollegeCode,
        CCENCR01YearTermCode,
        CCENCR01CourseId,
        CCENCR01SectionId,
        CCENCR01CourseControlNumber,
        CCENCR01CourseTopCode,
        CCENCR01CourseTitle,
        CCENCR01CourseMarkLetter,
        CCENCR01CourseMarkPoints,
        CCENCR01CourseLevelCode,
        CCENCR02CollegeCode,
        CCENCR02YearTermCode,
        CCENCR02CourseId,
        CCENCR02SectionId,
        CCENCR02CourseControlNumber,
        CCENCR02CourseTopCode,
        CCENCR02CourseTitle,
        CCENCR02CourseMarkLetter,
        CCENCR02CourseMarkPoints,
        CCENCR02CourseLevelCode,
        CCENCR03CollegeCode,
        CCENCR03YearTermCode,
        CCENCR03CourseId,
        CCENCR03SectionId,
        CCENCR03CourseControlNumber,
        CCENCR03CourseTopCode,
        CCENCR03CourseTitle,
        CCENCR03CourseMarkLetter,
        CCENCR03CourseMarkPoints,
        CCENCR03CourseLevelCode,
        CCENCR04CollegeCode,
        CCENCR04YearTermCode,
        CCENCR04CourseId,
        CCENCR04SectionId,
        CCENCR04CourseControlNumber,
        CCENCR04CourseTopCode,
        CCENCR04CourseTitle,
        CCENCR04CourseMarkLetter,
        CCENCR04CourseMarkPoints,
        CCENCR04CourseLevelCode,
        CCRDCRFLCollegeCode,
        CCRDCRFLYearTermCode,
        CCRDCRFLCourseId,
        CCRDCRFLSectionId,
        CCRDCRFLCourseControlNumber,
        CCRDCRFLCourseTopCode,
        CCRDCRFLCourseTitle,
        CCRDCRFLCourseMarkLetter,
        CCRDCRFLCourseMarkPoints,
        CCRDCRFLCourseLevelCode,
        CCRDCR00CollegeCode,
        CCRDCR00YearTermCode,
        CCRDCR00CourseId,
        CCRDCR00SectionId,
        CCRDCR00CourseControlNumber,
        CCRDCR00CourseTopCode,
        CCRDCR00CourseTitle,
        CCRDCR00CourseMarkLetter,
        CCRDCR00CourseMarkPoints,
        CCRDCR00CourseLevelCode,
        CCRDCR01CollegeCode,
        CCRDCR01YearTermCode,
        CCRDCR01CourseId,
        CCRDCR01SectionId,
        CCRDCR01CourseControlNumber,
        CCRDCR01CourseTopCode,
        CCRDCR01CourseTitle,
        CCRDCR01CourseMarkLetter,
        CCRDCR01CourseMarkPoints,
        CCRDCR01CourseLevelCode,
        CCRDCR02CollegeCode,
        CCRDCR02YearTermCode,
        CCRDCR02CourseId,
        CCRDCR02SectionId,
        CCRDCR02CourseControlNumber,
        CCRDCR02CourseTopCode,
        CCRDCR02CourseTitle,
        CCRDCR02CourseMarkLetter,
        CCRDCR02CourseMarkPoints,
        CCRDCR02CourseLevelCode,
        CCRDCR03CollegeCode,
        CCRDCR03YearTermCode,
        CCRDCR03CourseId,
        CCRDCR03SectionId,
        CCRDCR03CourseControlNumber,
        CCRDCR03CourseTopCode,
        CCRDCR03CourseTitle,
        CCRDCR03CourseMarkLetter,
        CCRDCR03CourseMarkPoints,
        CCRDCR03CourseLevelCode,
        CCRDCR04CollegeCode,
        CCRDCR04YearTermCode,
        CCRDCR04CourseId,
        CCRDCR04SectionId,
        CCRDCR04CourseControlNumber,
        CCRDCR04CourseTopCode,
        CCRDCR04CourseTitle,
        CCRDCR04CourseMarkLetter,
        CCRDCR04CourseMarkPoints,
        CCRDCR04CourseLevelCode
    )
SELECT
    InterSegmentKey               = t.InterSegmentKey,
    CCEACNFLCollegeCode           = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CollegeCode         end),
    CCEACNFLYearTermCode          = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then YearTermCode        end),
    CCEACNFLCourseId              = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CourseId            end),
    CCEACNFLSectionId             = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CourseSectionId     end),
    CCEACNFLCourseControlNumber   = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CourseControlNumber end),
    CCEACNFLCourseTopCode         = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CourseTopCode       end),
    CCEACNFLCourseTitle           = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CourseTitle         end),
    CCEACNFLCourseMarkLetter      = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CourseMarkLetter    end),
    CCEACNFLCourseMarkPoints      = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CourseMarkPoints    end),
    CCEACNFLCourseLevelCode       = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then CourseLevelCode     end),
    CCEACNFLCourseCreditCode      = max(case when p.Content = 'ESL' and t.SelectorCNFL = 1 then c.Type              end),
    CCEACN00CollegeCode           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CollegeCode         end),
    CCEACN00YearTermCode          = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then YearTermCode        end),
    CCEACN00CourseId              = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CourseId            end),
    CCEACN00SectionId             = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CourseSectionId     end),
    CCEACN00CourseControlNumber   = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CourseControlNumber end),
    CCEACN00CourseTopCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CourseTopCode       end),
    CCEACN00CourseTitle           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CourseTitle         end),
    CCEACN00CourseMarkLetter      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CourseMarkLetter    end),
    CCEACN00CourseMarkPoints      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CourseMarkPoints    end),
    CCEACN00CourseLevelCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then CourseLevelCode     end),
    CCEACN00CourseCreditCode      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'Y' and t.SelectorCN = 1 then c.Type              end),
    CCEACN01CollegeCode           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CollegeCode         end),
    CCEACN01YearTermCode          = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then YearTermCode        end),
    CCEACN01CourseId              = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CourseId            end),
    CCEACN01SectionId             = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CourseSectionId     end),
    CCEACN01CourseControlNumber   = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CourseControlNumber end),
    CCEACN01CourseTopCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CourseTopCode       end),
    CCEACN01CourseTitle           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CourseTitle         end),
    CCEACN01CourseMarkLetter      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CourseMarkLetter    end),
    CCEACN01CourseMarkPoints      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CourseMarkPoints    end),
    CCEACN01CourseLevelCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then CourseLevelCode     end),
    CCEACN01CourseCreditCode      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'A' and t.SelectorCN = 1 then c.Type              end),
    CCEACN02CollegeCode           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CollegeCode         end),
    CCEACN02YearTermCode          = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then YearTermCode        end),
    CCEACN02CourseId              = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CourseId            end),
    CCEACN02SectionId             = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CourseSectionId     end),
    CCEACN02CourseControlNumber   = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CourseControlNumber end),
    CCEACN02CourseTopCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CourseTopCode       end),
    CCEACN02CourseTitle           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CourseTitle         end),
    CCEACN02CourseMarkLetter      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CourseMarkLetter    end),
    CCEACN02CourseMarkPoints      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CourseMarkPoints    end),
    CCEACN02CourseLevelCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then CourseLevelCode     end),
    CCEACN02CourseCreditCode      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'B' and t.SelectorCN = 1 then c.Type              end),
    CCEACN03CollegeCode           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CollegeCode         end),
    CCEACN03YearTermCode          = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then YearTermCode        end),
    CCEACN03CourseId              = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CourseId            end),
    CCEACN03SectionId             = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CourseSectionId     end),
    CCEACN03CourseControlNumber   = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CourseControlNumber end),
    CCEACN03CourseTopCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CourseTopCode       end),
    CCEACN03CourseTitle           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CourseTitle         end),
    CCEACN03CourseMarkLetter      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CourseMarkLetter    end),
    CCEACN03CourseMarkPoints      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CourseMarkPoints    end),
    CCEACN03CourseLevelCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then CourseLevelCode     end),
    CCEACN03CourseCreditCode      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'C' and t.SelectorCN = 1 then c.Type              end),
    CCEACN04CollegeCode           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CollegeCode         end),
    CCEACN04YearTermCode          = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then YearTermCode        end),
    CCEACN04CourseId              = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CourseId            end),
    CCEACN04SectionId             = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CourseSectionId     end),
    CCEACN04CourseControlNumber   = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CourseControlNumber end),
    CCEACN04CourseTopCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CourseTopCode       end),
    CCEACN04CourseTitle           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CourseTitle         end),
    CCEACN04CourseMarkLetter      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CourseMarkLetter    end),
    CCEACN04CourseMarkPoints      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CourseMarkPoints    end),
    CCEACN04CourseLevelCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then CourseLevelCode     end),
    CCEACN04CourseCreditCode      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'D' and t.SelectorCN = 1 then c.Type              end),
    CCEACN05CollegeCode           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CollegeCode         end),
    CCEACN05YearTermCode          = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then YearTermCode        end),
    CCEACN05CourseId              = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CourseId            end),
    CCEACN05SectionId             = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CourseSectionId     end),
    CCEACN05CourseControlNumber   = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CourseControlNumber end),
    CCEACN05CourseTopCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CourseTopCode       end),
    CCEACN05CourseTitle           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CourseTitle         end),
    CCEACN05CourseMarkLetter      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CourseMarkLetter    end),
    CCEACN05CourseMarkPoints      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CourseMarkPoints    end),
    CCEACN05CourseLevelCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then CourseLevelCode     end),
    CCEACN05CourseCreditCode      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'E' and t.SelectorCN = 1 then c.Type              end),
    CCEACN06CollegeCode           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CollegeCode         end),
    CCEACN06YearTermCode          = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then YearTermCode        end),
    CCEACN06CourseId              = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CourseId            end),
    CCEACN06SectionId             = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CourseSectionId     end),
    CCEACN06CourseControlNumber   = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CourseControlNumber end),
    CCEACN06CourseTopCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CourseTopCode       end),
    CCEACN06CourseTitle           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CourseTitle         end),
    CCEACN06CourseMarkLetter      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CourseMarkLetter    end),
    CCEACN06CourseMarkPoints      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CourseMarkPoints    end),
    CCEACN06CourseLevelCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then CourseLevelCode     end),
    CCEACN06CourseCreditCode      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'F' and t.SelectorCN = 1 then c.Type              end),
    CCEACN07CollegeCode           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CollegeCode         end),
    CCEACN07YearTermCode          = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then YearTermCode        end),
    CCEACN07CourseId              = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CourseId            end),
    CCEACN07SectionId             = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CourseSectionId     end),
    CCEACN07CourseControlNumber   = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CourseControlNumber end),
    CCEACN07CourseTopCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CourseTopCode       end),
    CCEACN07CourseTitle           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CourseTitle         end),
    CCEACN07CourseMarkLetter      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CourseMarkLetter    end),
    CCEACN07CourseMarkPoints      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CourseMarkPoints    end),
    CCEACN07CourseLevelCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then CourseLevelCode     end),
    CCEACN07CourseCreditCode      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'G' and t.SelectorCN = 1 then c.Type              end),
    CCEACN08CollegeCode           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CollegeCode         end),
    CCEACN08YearTermCode          = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then YearTermCode        end),
    CCEACN08CourseId              = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CourseId            end),
    CCEACN08SectionId             = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CourseSectionId     end),
    CCEACN08CourseControlNumber   = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CourseControlNumber end),
    CCEACN08CourseTopCode         = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CourseTopCode       end),
    CCEACN08CourseTitle           = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CourseTitle         end),
    CCEACN08CourseMarkLetter      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CourseMarkLetter    end),
    CCEACN08CourseMarkPoints      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CourseMarkPoints    end),
    CCEACN08CourseLevelCode       = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then CourseLevelCode     end),
    CCEACN08CourseCreditCode      = max(case when p.Content = 'ESL' and t.CourseLevelCode = 'H' and t.SelectorCN = 1 then c.Type              end),
    CCENCRFLCollegeCode           = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CollegeCode         end),
    CCENCRFLYearTermCode          = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then YearTermCode        end),
    CCENCRFLCourseId              = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CourseId            end),
    CCENCRFLSectionId             = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CourseSectionId     end),
    CCENCRFLCourseControlNumber   = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CourseControlNumber end),
    CCENCRFLCourseTopCode         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTopCode       end),
    CCENCRFLCourseTitle           = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTitle         end),
    CCENCRFLCourseMarkLetter      = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkLetter    end),
    CCENCRFLCourseMarkPoints      = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkPoints    end),
    CCENCRFLCourseLevelCode       = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.SelectorFL = 1 then CourseLevelCode     end),
    CCENCR00CollegeCode           = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CollegeCode         end),
    CCENCR00YearTermCode          = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then YearTermCode        end),
    CCENCR00CourseId              = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseId            end),
    CCENCR00SectionId             = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseSectionId     end),
    CCENCR00CourseControlNumber   = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseControlNumber end),
    CCENCR00CourseTopCode         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTopCode       end),
    CCENCR00CourseTitle           = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTitle         end),
    CCENCR00CourseMarkLetter      = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkLetter    end),
    CCENCR00CourseMarkPoints      = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkPoints    end),
    CCENCR00CourseLevelCode       = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseLevelCode     end),
    CCENCR002NCollegeCode         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2   then CollegeCode         end),
    CCENCR002NYearTermCode        = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2   then YearTermCode        end),
    CCENCR002NCourseId            = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2   then CourseId            end),
    CCENCR002NSectionId           = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2   then CourseSectionId     end),
    CCENCR002NCourseControlNumber = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2   then CourseControlNumber end),
    CCENCR002NCourseTopCode       = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2   then CourseTopCode       end),
    CCENCR002NCourseTitle         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2   then CourseTitle         end),
    CCENCR002NCourseMarkLetter    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2   then CourseMarkLetter    end),
    CCENCR002NCourseMarkPoints    = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2   then CourseMarkPoints    end),
    CCENCR002NCourseLevelCode     = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector = 2   then CourseLevelCode     end),
    CCENCR01CollegeCode           = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CollegeCode         end),
    CCENCR01YearTermCode          = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then YearTermCode        end),
    CCENCR01CourseId              = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseId            end),
    CCENCR01SectionId             = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseSectionId     end),
    CCENCR01CourseControlNumber   = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseControlNumber end),
    CCENCR01CourseTopCode         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTopCode       end),
    CCENCR01CourseTitle           = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTitle         end),
    CCENCR01CourseMarkLetter      = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkLetter    end),
    CCENCR01CourseMarkPoints      = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkPoints    end),
    CCENCR01CourseLevelCode       = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseLevelCode     end),
    CCENCR02CollegeCode           = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CollegeCode         end),
    CCENCR02YearTermCode          = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then YearTermCode        end),
    CCENCR02CourseId              = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseId            end),
    CCENCR02SectionId             = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseSectionId     end),
    CCENCR02CourseControlNumber   = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseControlNumber end),
    CCENCR02CourseTopCode         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTopCode       end),
    CCENCR02CourseTitle           = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTitle         end),
    CCENCR02CourseMarkLetter      = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkLetter    end),
    CCENCR02CourseMarkPoints      = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkPoints    end),
    CCENCR02CourseLevelCode       = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseLevelCode     end),
    CCENCR03CollegeCode           = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CollegeCode         end),
    CCENCR03YearTermCode          = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then YearTermCode        end),
    CCENCR03CourseId              = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseId            end),
    CCENCR03SectionId             = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseSectionId     end),
    CCENCR03CourseControlNumber   = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseControlNumber end),
    CCENCR03CourseTopCode         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTopCode       end),
    CCENCR03CourseTitle           = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTitle         end),
    CCENCR03CourseMarkLetter      = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkLetter    end),
    CCENCR03CourseMarkPoints      = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkPoints    end),
    CCENCR03CourseLevelCode       = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseLevelCode     end),
    CCENCR04CollegeCode           = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CollegeCode         end),
    CCENCR04YearTermCode          = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then YearTermCode        end),
    CCENCR04CourseId              = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseId            end),
    CCENCR04SectionId             = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseSectionId     end),
    CCENCR04CourseControlNumber   = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseControlNumber end),
    CCENCR04CourseTopCode         = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTopCode       end),
    CCENCR04CourseTitle           = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTitle         end),
    CCENCR04CourseMarkLetter      = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkLetter    end),
    CCENCR04CourseMarkPoints      = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkPoints    end),
    CCENCR04CourseLevelCode       = max(case when t.CourseTopCode = '150100' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseLevelCode     end),
    CCRDCRFLCollegeCode           = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CollegeCode         end),
    CCRDCRFLYearTermCode          = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then YearTermCode        end),
    CCRDCRFLCourseId              = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CourseId            end),
    CCRDCRFLSectionId             = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CourseSectionId     end),
    CCRDCRFLCourseControlNumber   = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CourseControlNumber end),
    CCRDCRFLCourseTopCode         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTopCode       end),
    CCRDCRFLCourseTitle           = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CourseTitle         end),
    CCRDCRFLCourseMarkLetter      = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkLetter    end),
    CCRDCRFLCourseMarkPoints      = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CourseMarkPoints    end),
    CCRDCRFLCourseLevelCode       = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.SelectorFL = 1 then CourseLevelCode     end),
    CCRDCR00CollegeCode           = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CollegeCode         end),
    CCRDCR00YearTermCode          = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then YearTermCode        end),
    CCRDCR00CourseId              = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseId            end),
    CCRDCR00SectionId             = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseSectionId     end),
    CCRDCR00CourseControlNumber   = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseControlNumber end),
    CCRDCR00CourseTopCode         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTopCode       end),
    CCRDCR00CourseTitle           = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseTitle         end),
    CCRDCR00CourseMarkLetter      = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkLetter    end),
    CCRDCR00CourseMarkPoints      = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseMarkPoints    end),
    CCRDCR00CourseLevelCode       = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'Y' and t.Selector   = 1 then CourseLevelCode     end),
    CCRDCR01CollegeCode           = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CollegeCode         end),
    CCRDCR01YearTermCode          = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then YearTermCode        end),
    CCRDCR01CourseId              = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseId            end),
    CCRDCR01SectionId             = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseSectionId     end),
    CCRDCR01CourseControlNumber   = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseControlNumber end),
    CCRDCR01CourseTopCode         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTopCode       end),
    CCRDCR01CourseTitle           = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseTitle         end),
    CCRDCR01CourseMarkLetter      = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkLetter    end),
    CCRDCR01CourseMarkPoints      = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseMarkPoints    end),
    CCRDCR01CourseLevelCode       = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'A' and t.Selector   = 1 then CourseLevelCode     end),
    CCRDCR02CollegeCode           = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CollegeCode         end),
    CCRDCR02YearTermCode          = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then YearTermCode        end),
    CCRDCR02CourseId              = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseId            end),
    CCRDCR02SectionId             = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseSectionId     end),
    CCRDCR02CourseControlNumber   = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseControlNumber end),
    CCRDCR02CourseTopCode         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTopCode       end),
    CCRDCR02CourseTitle           = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseTitle         end),
    CCRDCR02CourseMarkLetter      = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkLetter    end),
    CCRDCR02CourseMarkPoints      = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseMarkPoints    end),
    CCRDCR02CourseLevelCode       = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'B' and t.Selector   = 1 then CourseLevelCode     end),
    CCRDCR03CollegeCode           = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CollegeCode         end),
    CCRDCR03YearTermCode          = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then YearTermCode        end),
    CCRDCR03CourseId              = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseId            end),
    CCRDCR03SectionId             = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseSectionId     end),
    CCRDCR03CourseControlNumber   = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseControlNumber end),
    CCRDCR03CourseTopCode         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTopCode       end),
    CCRDCR03CourseTitle           = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseTitle         end),
    CCRDCR03CourseMarkLetter      = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkLetter    end),
    CCRDCR03CourseMarkPoints      = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseMarkPoints    end),
    CCRDCR03CourseLevelCode       = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'C' and t.Selector   = 1 then CourseLevelCode     end),
    CCRDCR04CollegeCode           = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CollegeCode         end),
    CCRDCR04YearTermCode          = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then YearTermCode        end),
    CCRDCR04CourseId              = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseId            end),
    CCRDCR04SectionId             = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseSectionId     end),
    CCRDCR04CourseControlNumber   = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseControlNumber end),
    CCRDCR04CourseTopCode         = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTopCode       end),
    CCRDCR04CourseTitle           = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseTitle         end),
    CCRDCR04CourseMarkLetter      = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkLetter    end),
    CCRDCR04CourseMarkPoints      = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseMarkPoints    end),
    CCRDCR04CourseLevelCode       = max(case when t.CourseTopCode = '152000' and c.Type = 'CR' and t.CourseLevelCode = 'D' and t.Selector   = 1 then CourseLevelCode     end)
FROM
    Mmap.CCTranscript t
    inner join
    Mmap.Credits c
        on c.Code = t.CourseCreditCode
    inner join
    Mmap.Programs p
        on p.Code = t.CourseTopCode
WHERE
    (
        t.CourseTopCode = '150100'
        and
        t.CourseLevelCode in ('Y','A','B','C','D')
        and
        c.Type = 'CR'
    )
    or
    (
        t.CourseTopCode = '152000'
        and
        t.CourseLevelCode in ('Y','A','B','C','D')
        and
        c.Type = 'CR'
    )
    or
    t.CourseTopCode in ('493087','493084','493085','493086','493100','493090')
GROUP BY
    t.InterSegmentKey;

UPDATE
    t
SET
    t.HS09GradeCode                                = s.HS09GradeCode,
    t.HS09SchoolCode                               = s.HS09SchoolCode,
    t.HS09YearTermCode                             = s.HS09YearTermCode,
    t.HS09CourseCode                               = s.HS09CourseCode,
    t.HS09CourseContentRank                        = s.HS09CourseContentRank,
    t.HS09CourseTitle                              = s.HS09CourseTitle,
    t.HS09CourseMarkLetter                         = s.HS09CourseMarkLetter,
    t.HS09CourseMarkPoints                         = s.HS09CourseMarkPoints,
    t.HS09CourseUniversityAdmissionRequirementCode = s.HS09CourseUniversityAdmissionRequirementCode,
    t.HS09CourseLevelCode                          = s.HS09CourseLevelCode,
    t.HS09CourseTypeCode                           = s.HS09CourseTypeCode,
    t.HS10GradeCode                                = s.HS10GradeCode,
    t.HS10SchoolCode                               = s.HS10SchoolCode,
    t.HS10YearTermCode                             = s.HS10YearTermCode,
    t.HS10CourseCode                               = s.HS10CourseCode,
    t.HS10CourseContentRank                        = s.HS10CourseContentRank,
    t.HS10CourseTitle                              = s.HS10CourseTitle,
    t.HS10CourseMarkLetter                         = s.HS10CourseMarkLetter,
    t.HS10CourseMarkPoints                         = s.HS10CourseMarkPoints,
    t.HS10CourseUniversityAdmissionRequirementCode = s.HS10CourseUniversityAdmissionRequirementCode,
    t.HS10CourseLevelCode                          = s.HS10CourseLevelCode,
    t.HS10CourseTypeCode                           = s.HS10CourseTypeCode,
    t.HS11GradeCode                                = s.HS11GradeCode,
    t.HS11SchoolCode                               = s.HS11SchoolCode,
    t.HS11YearTermCode                             = s.HS11YearTermCode,
    t.HS11CourseCode                               = s.HS11CourseCode,
    t.HS11CourseContentRank                        = s.HS11CourseContentRank,
    t.HS11CourseTitle                              = s.HS11CourseTitle,
    t.HS11CourseMarkLetter                         = s.HS11CourseMarkLetter,
    t.HS11CourseMarkPoints                         = s.HS11CourseMarkPoints,
    t.HS11CourseUniversityAdmissionRequirementCode = s.HS11CourseUniversityAdmissionRequirementCode,
    t.HS11CourseLevelCode                          = s.HS11CourseLevelCode,
    t.HS11CourseTypeCode                           = s.HS11CourseTypeCode,
    t.HS12GradeCode                                = s.HS12GradeCode,
    t.HS12SchoolCode                               = s.HS12SchoolCode,
    t.HS12YearTermCode                             = s.HS12YearTermCode,
    t.HS12CourseCode                               = s.HS12CourseCode,
    t.HS12CourseContentRank                        = s.HS12CourseContentRank,
    t.HS12CourseTitle                              = s.HS12CourseTitle,
    t.HS12CourseMarkLetter                         = s.HS12CourseMarkLetter,
    t.HS12CourseMarkPoints                         = s.HS12CourseMarkPoints,
    t.HS12CourseUniversityAdmissionRequirementCode = s.HS12CourseUniversityAdmissionRequirementCode,
    t.HS12CourseLevelCode                          = s.HS12CourseLevelCode,
    t.HS12CourseTypeCode                           = s.HS12CourseTypeCode,
    t.HSLGGradeCode                                = s.HSLGGradeCode,
    t.HSLGSchoolCode                               = s.HSLGSchoolCode,
    t.HSLGYearTermCode                             = s.HSLGYearTermCode,
    t.HSLGCourseCode                               = s.HSLGCourseCode,
    t.HSLGCourseContentRank                        = s.HSLGCourseContentRank,
    t.HSLGCourseTitle                              = s.HSLGCourseTitle,
    t.HSLGCourseMarkLetter                         = s.HSLGCourseMarkLetter,
    t.HSLGCourseMarkPoints                         = s.HSLGCourseMarkPoints,
    t.HSLGCourseUniversityAdmissionRequirementCode = s.HSLGCourseUniversityAdmissionRequirementCode,
    t.HSLGCourseLevelCode                          = s.HSLGCourseLevelCode,
    t.HSLGCourseTypeCode                           = s.HSLGCourseTypeCode
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey                              = rcc.InterSegmentKey,
            HS09GradeCode                                = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.GradeCode end),
            HS09SchoolCode                               = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
            HS09YearTermCode                             = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
            HS09CourseCode                               = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseCode end),
            HS09CourseContentRank                        = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.ContentRank end),
            HS09CourseTitle                              = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
            HS09CourseMarkLetter                         = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.SectionMark end),
            HS09CourseMarkPoints                         = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
            HS09CourseUniversityAdmissionRequirementCode = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
            HS09CourseLevelCode                          = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
            HS09CourseTypeCode                           = max(case when rcc.GradeCode = '09' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
            HS10GradeCode                                = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.GradeCode end),
            HS10SchoolCode                               = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
            HS10YearTermCode                             = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
            HS10CourseCode                               = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseCode end),
            HS10CourseContentRank                        = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.ContentRank end),
            HS10CourseTitle                              = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
            HS10CourseMarkLetter                         = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.SectionMark end),
            HS10CourseMarkPoints                         = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
            HS10CourseUniversityAdmissionRequirementCode = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
            HS10CourseLevelCode                          = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
            HS10CourseTypeCode                           = max(case when rcc.GradeCode = '10' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
            HS11GradeCode                                = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.GradeCode end),
            HS11SchoolCode                               = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
            HS11YearTermCode                             = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
            HS11CourseCode                               = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseCode end),
            HS11CourseContentRank                        = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.ContentRank end),
            HS11CourseTitle                              = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
            HS11CourseMarkLetter                         = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.SectionMark end),
            HS11CourseMarkPoints                         = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
            HS11CourseUniversityAdmissionRequirementCode = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
            HS11CourseLevelCode                          = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
            HS11CourseTypeCode                           = max(case when rcc.GradeCode = '11' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
            HS12GradeCode                                = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.GradeCode end),
            HS12SchoolCode                               = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.SchoolCode end),
            HS12YearTermCode                             = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.YearTermCode end),
            HS12CourseCode                               = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseCode end),
            HS12CourseContentRank                        = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.ContentRank end),
            HS12CourseTitle                              = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseTitle end),
            HS12CourseMarkLetter                         = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.SectionMark end),
            HS12CourseMarkPoints                         = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.MarkPoints end),
            HS12CourseUniversityAdmissionRequirementCode = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseAGCode end),
            HS12CourseLevelCode                          = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseLevelCode end),
            HS12CourseTypeCode                           = max(case when rcc.GradeCode = '12' and rcc.GradeSelector = 1 then rcc.CourseTypeCode end),
            HSLGGradeCode                                = max(case when rcc.RecencySelector = 1 then rcc.GradeCode end),
            HSLGSchoolCode                               = max(case when rcc.RecencySelector = 1 then rcc.SchoolCode end),
            HSLGYearTermCode                             = max(case when rcc.RecencySelector = 1 then rcc.YearTermCode end),
            HSLGCourseCode                               = max(case when rcc.RecencySelector = 1 then rcc.CourseCode end),
            HSLGCourseContentRank                        = max(case when rcc.RecencySelector = 1 then rcc.ContentRank end),
            HSLGCourseTitle                              = max(case when rcc.RecencySelector = 1 then rcc.CourseTitle end),
            HSLGCourseMarkLetter                         = max(case when rcc.RecencySelector = 1 then rcc.SectionMark end),
            HSLGCourseMarkPoints                         = max(case when rcc.RecencySelector = 1 then rcc.MarkPoints end),
            HSLGCourseUniversityAdmissionRequirementCode = max(case when rcc.RecencySelector = 1 then rcc.CourseAGCode end),
            HSLGCourseLevelCode                          = max(case when rcc.RecencySelector = 1 then rcc.CourseLevelCode end),
            HSLGCourseTypeCode                           = max(case when rcc.RecencySelector = 1 then rcc.CourseTypeCode end)
        FROM
            Mmap.RetrospectiveCourseContent rcc
        WHERE
            rcc.DepartmentCode = 14
            and exists (
                SELECT
                    null
                FROM
                    Mmap.Student s
                WHERE
                    s.InterSegmentKey = rcc.InterSegmentKey
            )
        GROUP BY
            rcc.InterSegmentKey
    ) s
        on s.InterSegmentKey = t.InterSegmentKey;

UPDATE
    t
SET
    t.HS09OverallGradePointAverage           = s.HS09OverallGradePointAverage,
    t.HS09OverallCumulativeGradePointAverage = s.HS09OverallCumulativeGradePointAverage,
    t.HS09EnglishGradePointAverage           = s.HS09EnglishGradePointAverage,
    t.HS09EnglishCumulativeGradePointAverage = s.HS09EnglishCumulativeGradePointAverage,
    t.HS10OverallGradePointAverage           = s.HS10OverallGradePointAverage,
    t.HS10OverallCumulativeGradePointAverage = s.HS10OverallCumulativeGradePointAverage,
    t.HS10EnglishGradePointAverage           = s.HS10EnglishGradePointAverage,
    t.HS10EnglishCumulativeGradePointAverage = s.HS10EnglishCumulativeGradePointAverage,
    t.HS11OverallGradePointAverage           = s.HS11OverallGradePointAverage,
    t.HS11OverallCumulativeGradePointAverage = s.HS11OverallCumulativeGradePointAverage,
    t.HS11EnglishGradePointAverage           = s.HS11EnglishGradePointAverage,
    t.HS11EnglishCumulativeGradePointAverage = s.HS11EnglishCumulativeGradePointAverage,
    t.HS12OverallGradePointAverage           = s.HS12OverallGradePointAverage,
    t.HS12OverallCumulativeGradePointAverage = s.HS12OverallCumulativeGradePointAverage,
    t.HS12EnglishGradePointAverage           = s.HS12EnglishGradePointAverage,
    t.HS12EnglishCumulativeGradePointAverage = s.HS12EnglishCumulativeGradePointAverage,
    t.HSLGOverallGradePointAverage           = s.HSLGOverallGradePointAverage,
    t.HSLGOverallCumulativeGradePointAverage = s.HSLGOverallCumulativeGradePointAverage,
    t.HSLGEnglishGradePointAverage           = s.HSLGEnglishGradePointAverage,
    t.HSLGEnglishCumulativeGradePointAverage = s.HSLGEnglishCumulativeGradePointAverage
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey                        = rp.InterSegmentKey,
            HS09OverallGradePointAverage           = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '09' then rp.GradePointAverage end),
            HS09OverallCumulativeGradePointAverage = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '09' then rp.CumulativeGradePointAverage end),
            HS09EnglishGradePointAverage           = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '09' then rp.GradePointAverage end),
            HS09EnglishCumulativeGradePointAverage = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '09' then rp.CumulativeGradePointAverage end),
            HS10OverallGradePointAverage           = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '10' then rp.GradePointAverage end),
            HS10OverallCumulativeGradePointAverage = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '10' then rp.CumulativeGradePointAverage end),
            HS10EnglishGradePointAverage           = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '10' then rp.GradePointAverage end),
            HS10EnglishCumulativeGradePointAverage = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '10' then rp.CumulativeGradePointAverage end),
            HS11OverallGradePointAverage           = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '11' then rp.GradePointAverage end),
            HS11OverallCumulativeGradePointAverage = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '11' then rp.CumulativeGradePointAverage end),
            HS11EnglishGradePointAverage           = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '11' then rp.GradePointAverage end),
            HS11EnglishCumulativeGradePointAverage = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '11' then rp.CumulativeGradePointAverage end),
            HS12OverallGradePointAverage           = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '12' then rp.GradePointAverage end),
            HS12OverallCumulativeGradePointAverage = max(case when rp.DepartmentCode = 0  and rp.GradeCode = '12' then rp.CumulativeGradePointAverage end),
            HS12EnglishGradePointAverage           = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '12' then rp.GradePointAverage end),
            HS12EnglishCumulativeGradePointAverage = max(case when rp.DepartmentCode = 14 and rp.GradeCode = '12' then rp.CumulativeGradePointAverage end),
            HSLGOverallGradePointAverage           = max(case when rp.DepartmentCode = 0  and rp.IsLast    = 1    then rp.GradePointAverage end),
            HSLGOverallCumulativeGradePointAverage = max(case when rp.DepartmentCode = 0  and rp.IsLast    = 1    then rp.CumulativeGradePointAverage end),
            HSLGEnglishGradePointAverage           = max(case when rp.DepartmentCode = 14 and rp.IsLast    = 1    then rp.GradePointAverage end),
            HSLGEnglishCumulativeGradePointAverage = max(case when rp.DepartmentCode = 14 and rp.IsLast    = 1    then rp.CumulativeGradePointAverage end)
        FROM
            Mmap.RetrospectivePerformance rp
            inner join
            Mmap.Student s
                on s.InterSegmentKey = rp.InterSegmentKey
        GROUP BY
            rp.InterSegmentKey
    ) s
        ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
    t
SET
    t.CCEACNFLSummary   = s.CCEACNFLSummary,
    t.CCEACNFLEslUnits  = s.CCEACNFLEslUnits,
    t.CCEACNFLEnglUnits = s.CCEACNFLEnglUnits
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey   = t.InterSegmentKey,
            CCEACNFLSummary   = 
                max(case when p.Code = '493087' then '1' else '0' end) +
                max(case when p.Code = '493084' then '1' else '0' end) +
                max(case when p.Code = '493085' then '1' else '0' end) +
                max(case when p.Code = '493086' then '1' else '0' end) +
                max(case when p.Code = '493100' then '1' else '0' end) +
                max(case when p.Code = '493090' then '1' else '0' end),
            CCEACNFLEslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
            CCEACNFLEnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
        FROM
            Mmap.CCTranscript t
            inner join
            Mmap.Programs p
                on p.Code = t.CourseTopCode
            inner join
            Mmap.RetroEsl r
                on  r.InterSegmentKey      = t.InterSegmentKey
                and r.CCEACNFLYearTermCode = t.YearTermCode
        GROUP BY
            t.InterSegmentKey
    ) s
        ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
    t
SET
    t.CCEACN00Summary   = s.CCEACN00Summary,
    t.CCEACN00EslUnits  = s.CCEACN00EslUnits,
    t.CCEACN00EnglUnits = s.CCEACN00EnglUnits
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey   = t.InterSegmentKey,
            CCEACN00Summary   = 
                max(case when p.Code = '493087' then '1' else '0' end) +
                max(case when p.Code = '493084' then '1' else '0' end) +
                max(case when p.Code = '493085' then '1' else '0' end) +
                max(case when p.Code = '493086' then '1' else '0' end) +
                max(case when p.Code = '493100' then '1' else '0' end) +
                max(case when p.Code = '493090' then '1' else '0' end),
            CCEACN00EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
            CCEACN00EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
        FROM
            Mmap.CCTranscript t
            inner join
            Mmap.Programs p
                on p.Code = t.CourseTopCode
            inner join
            Mmap.RetroEsl r
                on  r.InterSegmentKey      = t.InterSegmentKey
                and r.CCEACN00YearTermCode = t.YearTermCode
        GROUP BY
            t.InterSegmentKey
    ) s
        ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
    t
SET
    t.CCEACN01Summary   = s.CCEACN01Summary,
    t.CCEACN01EslUnits  = s.CCEACN01EslUnits,
    t.CCEACN01EnglUnits = s.CCEACN01EnglUnits
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey   = t.InterSegmentKey,
            CCEACN01Summary   = 
                max(case when p.Code = '493087' then '1' else '0' end) +
                max(case when p.Code = '493084' then '1' else '0' end) +
                max(case when p.Code = '493085' then '1' else '0' end) +
                max(case when p.Code = '493086' then '1' else '0' end) +
                max(case when p.Code = '493100' then '1' else '0' end) +
                max(case when p.Code = '493090' then '1' else '0' end),
            CCEACN01EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
            CCEACN01EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
        FROM
            Mmap.CCTranscript t
            inner join
            Mmap.Programs p
                on p.Code = t.CourseTopCode
            inner join
            Mmap.RetroEsl r
                on  r.InterSegmentKey      = t.InterSegmentKey
                and r.CCEACN01YearTermCode = t.YearTermCode
        GROUP BY
            t.InterSegmentKey
    ) s
        ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
    t
SET
    t.CCEACN02Summary   = s.CCEACN02Summary,
    t.CCEACN02EslUnits  = s.CCEACN02EslUnits,
    t.CCEACN02EnglUnits = s.CCEACN02EnglUnits
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey   = t.InterSegmentKey,
            CCEACN02Summary   = 
                max(case when p.Code = '493087' then '1' else '0' end) +
                max(case when p.Code = '493084' then '1' else '0' end) +
                max(case when p.Code = '493085' then '1' else '0' end) +
                max(case when p.Code = '493086' then '1' else '0' end) +
                max(case when p.Code = '493100' then '1' else '0' end) +
                max(case when p.Code = '493090' then '1' else '0' end),
            CCEACN02EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
            CCEACN02EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
        FROM
            Mmap.CCTranscript t
            inner join
            Mmap.Programs p
                on p.Code = t.CourseTopCode
            inner join
            Mmap.RetroEsl r
                on  r.InterSegmentKey      = t.InterSegmentKey
                and r.CCEACN02YearTermCode = t.YearTermCode
        GROUP BY
            t.InterSegmentKey
    ) s
        ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
    t
SET
    t.CCEACN03Summary   = s.CCEACN03Summary,
    t.CCEACN03EslUnits  = s.CCEACN03EslUnits,
    t.CCEACN03EnglUnits = s.CCEACN03EnglUnits
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey   = t.InterSegmentKey,
            CCEACN03Summary   = 
                max(case when p.Code = '493087' then '1' else '0' end) +
                max(case when p.Code = '493084' then '1' else '0' end) +
                max(case when p.Code = '493085' then '1' else '0' end) +
                max(case when p.Code = '493086' then '1' else '0' end) +
                max(case when p.Code = '493100' then '1' else '0' end) +
                max(case when p.Code = '493090' then '1' else '0' end),
            CCEACN03EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
            CCEACN03EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
        FROM
            Mmap.CCTranscript t
            inner join
            Mmap.Programs p
                on p.Code = t.CourseTopCode
            inner join
            Mmap.RetroEsl r
                on  r.InterSegmentKey      = t.InterSegmentKey
                and r.CCEACN03YearTermCode = t.YearTermCode
        GROUP BY
            t.InterSegmentKey
    ) s
        ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
    t
SET
    t.CCEACN04Summary   = s.CCEACN04Summary,
    t.CCEACN04EslUnits  = s.CCEACN04EslUnits,
    t.CCEACN04EnglUnits = s.CCEACN04EnglUnits
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey   = t.InterSegmentKey,
            CCEACN04Summary   = 
                max(case when p.Code = '493087' then '1' else '0' end) +
                max(case when p.Code = '493084' then '1' else '0' end) +
                max(case when p.Code = '493085' then '1' else '0' end) +
                max(case when p.Code = '493086' then '1' else '0' end) +
                max(case when p.Code = '493100' then '1' else '0' end) +
                max(case when p.Code = '493090' then '1' else '0' end),
            CCEACN04EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
            CCEACN04EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
        FROM
            Mmap.CCTranscript t
            inner join
            Mmap.Programs p
                on p.Code = t.CourseTopCode
            inner join
            Mmap.RetroEsl r
                on  r.InterSegmentKey      = t.InterSegmentKey
                and r.CCEACN04YearTermCode = t.YearTermCode
        GROUP BY
            t.InterSegmentKey
    ) s
        ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
    t
SET
    t.CCEACN05Summary   = s.CCEACN05Summary,
    t.CCEACN05EslUnits  = s.CCEACN05EslUnits,
    t.CCEACN05EnglUnits = s.CCEACN05EnglUnits
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey   = t.InterSegmentKey,
            CCEACN05Summary   = 
                max(case when p.Code = '493087' then '1' else '0' end) +
                max(case when p.Code = '493084' then '1' else '0' end) +
                max(case when p.Code = '493085' then '1' else '0' end) +
                max(case when p.Code = '493086' then '1' else '0' end) +
                max(case when p.Code = '493100' then '1' else '0' end) +
                max(case when p.Code = '493090' then '1' else '0' end),
            CCEACN05EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
            CCEACN05EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
        FROM
            Mmap.CCTranscript t
            inner join
            Mmap.Programs p
                on p.Code = t.CourseTopCode
            inner join
            Mmap.RetroEsl r
                on  r.InterSegmentKey      = t.InterSegmentKey
                and r.CCEACN05YearTermCode = t.YearTermCode
        GROUP BY
            t.InterSegmentKey
    ) s
        ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
    t
SET
    t.CCEACN06Summary   = s.CCEACN06Summary,
    t.CCEACN06EslUnits  = s.CCEACN06EslUnits,
    t.CCEACN06EnglUnits = s.CCEACN06EnglUnits
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey   = t.InterSegmentKey,
            CCEACN06Summary   = 
                max(case when p.Code = '493087' then '1' else '0' end) +
                max(case when p.Code = '493084' then '1' else '0' end) +
                max(case when p.Code = '493085' then '1' else '0' end) +
                max(case when p.Code = '493086' then '1' else '0' end) +
                max(case when p.Code = '493100' then '1' else '0' end) +
                max(case when p.Code = '493090' then '1' else '0' end),
            CCEACN06EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
            CCEACN06EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
        FROM
            Mmap.CCTranscript t
            inner join
            Mmap.Programs p
                on p.Code = t.CourseTopCode
            inner join
            Mmap.RetroEsl r
                on  r.InterSegmentKey      = t.InterSegmentKey
                and r.CCEACN06YearTermCode = t.YearTermCode
        GROUP BY
            t.InterSegmentKey
    ) s
        ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
    t
SET
    t.CCEACN07Summary   = s.CCEACN07Summary,
    t.CCEACN07EslUnits  = s.CCEACN07EslUnits,
    t.CCEACN07EnglUnits = s.CCEACN07EnglUnits
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey   = t.InterSegmentKey,
            CCEACN07Summary   = 
                max(case when p.Code = '493087' then '1' else '0' end) +
                max(case when p.Code = '493084' then '1' else '0' end) +
                max(case when p.Code = '493085' then '1' else '0' end) +
                max(case when p.Code = '493086' then '1' else '0' end) +
                max(case when p.Code = '493100' then '1' else '0' end) +
                max(case when p.Code = '493090' then '1' else '0' end),
            CCEACN07EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
            CCEACN07EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
        FROM
            Mmap.CCTranscript t
            inner join
            Mmap.Programs p
                on p.Code = t.CourseTopCode
            inner join
            Mmap.RetroEsl r
                on  r.InterSegmentKey      = t.InterSegmentKey
                and r.CCEACN07YearTermCode = t.YearTermCode
        GROUP BY
            t.InterSegmentKey
    ) s
        ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
    t
SET
    t.CCEACN08Summary   = s.CCEACN08Summary,
    t.CCEACN08EslUnits  = s.CCEACN08EslUnits,
    t.CCEACN08EnglUnits = s.CCEACN08EnglUnits
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey   = t.InterSegmentKey,
            CCEACN08Summary   = 
                max(case when p.Code = '493087' then '1' else '0' end) +
                max(case when p.Code = '493084' then '1' else '0' end) +
                max(case when p.Code = '493085' then '1' else '0' end) +
                max(case when p.Code = '493086' then '1' else '0' end) +
                max(case when p.Code = '493100' then '1' else '0' end) +
                max(case when p.Code = '493090' then '1' else '0' end),
            CCEACN08EslUnits  = sum(case when p.Content = 'ESL'  then t.CourseUnitsTry end),
            CCEACN08EnglUnits = sum(case when p.Content = 'ENGL' then t.CourseUnitsTry end)
        FROM
            Mmap.CCTranscript t
            inner join
            Mmap.Programs p
                on p.Code = t.CourseTopCode
            inner join
            Mmap.RetroEsl r
                on  r.InterSegmentKey      = t.InterSegmentKey
                and r.CCEACN08YearTermCode = t.YearTermCode
        GROUP BY
            t.InterSegmentKey
    ) s
        ON s.InterSegmentKey = t.InterSegmentKey;

UPDATE
    t
SET
    t.STIsEnglishEap           = ss.engl_eap_ind,
    t.STEnglishScaledScore     = ss.engl_scaled_score,
    t.STEnglishCluster1        = ss.engl_cluster_1,
    t.STEnglishCluster2        = ss.engl_cluster_2,
    t.STEnglishCluster3        = ss.engl_cluster_3,
    t.STEnglishCluster4        = ss.engl_cluster_4,
    t.STEnglishCluster5        = ss.engl_cluster_5,
    t.STEnglishCluster6        = ss.engl_cluster_6,
    t.STMathematicsSubject     = ss.math_subject,
    t.STIsMathematicsEap       = ss.math_eap_ind,
    t.STMathematicsScaledScore = ss.math_scaled_score,
    t.STMathematicsCluster1    = ss.math_cluster_1,
    t.STMathematicsCluster2    = ss.math_cluster_2,
    t.STMathematicsCluster3    = ss.math_cluster_3,
    t.STMathematicsCluster4    = ss.math_cluster_4,
    t.STMathematicsCluster5    = ss.math_cluster_5,
    t.STMathematicsCluster6    = ss.math_cluster_6,
    t.STIsEnglishOnly          = ss.esl_eo_ind,
    t.STIsEnglishLearner       = ss.esl_el_ind,
    t.STIsFluentInitially      = ss.esl_ifep_ind,
    t.STIsFluentReclassified   = ss.esl_rfep_ind
FROM
    Mmap.RetroEsl t
    inner join
    dbo.StudentStar ss
        on ss.derkey1 = t.InterSegmentKey;

UPDATE
    t
SET
    t.HSIsRemedial = 1
FROM
    Mmap.RetroEsl t
WHERE
    exists (
        SELECT
            null
        FROM
            Mmap.RetrospectiveCourseContent rcc
        WHERE
            rcc.InterSegmentKey = t.InterSegmentKey
            and rcc.DepartmentCode = 14
            and rcc.CourseCode = '2100'
    );

UPDATE
    t
SET
    t.HSGraduationDate = s.HSGraduationDate
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey  = s.InterSegmentKey,
            HSGraduationDate = max(a.AwardDate)
        FROM
            Mmap.Student s
            inner join
            K12AwardProd a
                on a.Derkey1 = s.InterSegmentKey
        GROUP BY
            s.InterSegmentKey
    ) s
        on s.InterSegmentKey = t.InterSegmentKey;

UPDATE
    t
SET
    t.APHSCountry                       = s.APHSCountry,
    t.APUN01Country                     = s.APUN01Country,
    t.APUN01Award                       = s.APUN01Award,
    t.APUN02Country                     = s.APUN02Country,
    t.APUN02Award                       = s.APUN02Award,
    t.APUN03Country                     = s.APUN03Country,
    t.APUN03Award                       = s.APUN03Award,
    t.APUN04Country                     = s.APUN04Country,
    t.APUN04Award                       = s.APUN04Award,
    t.APVisaType                        = s.APVisaType,
    t.APAB540Waiver                     = s.APAB540Waiver,
    t.APComfortableEnglish              = s.APComfortableEnglish,
    t.APApplicationLanguage             = s.APApplicationLanguage,
    t.APEsl                             = s.APEsl,
    t.APBasicSkills                     = s.APBasicSkills,
    t.APEmploymentAssistance            = s.APEmploymentAssistance,
    t.APSeasonalAG                      = s.APSeasonalAG,
    t.APCountry                         = s.APCountry,
    t.APPermanentCountry                = s.APPermanentCountry,
    t.APPermanentCountryInternational   = s.APPermanentCountryInternational,
    t.APCompletedEleventhGrade          = s.APCompletedEleventhGrade,
    t.APCumulativeGradePointAverage     = s.APCumulativeGradePointAverage,
    t.APEnglishCompletedCourseId        = s.APEnglishCompletedCourseId,
    t.APEnglishCompletedCourseGrade     = s.APEnglishCompletedCourseGrade,
    t.APMathematicsCompletedCourseId    = s.APMathematicsCompletedCourseId,
    t.APMathematicsCompletedCourseGrade = s.APMathematicsCompletedCourseGrade,
    t.APMathematicsPassedCourseId       = s.APMathematicsPassedCourseId,
    t.APMathematicsPassedCourseGrade    = s.APMathematicsPassedCourseGrade
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey                   = s.InterSegmentKey,
            APHSCountry                       = CONVERT(CHAR(2)      , a.hs_country),
            APUN01Country                     = CONVERT(CHAR(2)      , a.col1_country),
            APUN01Award                       = CONVERT(CHAR(1)      , a.col1_degree_obtained),
            APUN02Country                     = CONVERT(CHAR(2)      , a.col2_country),
            APUN02Award                       = CONVERT(CHAR(1)      , a.col2_degree_obtained),
            APUN03Country                     = CONVERT(CHAR(2)      , a.col3_country),
            APUN03Award                       = CONVERT(CHAR(1)      , a.col3_degree_obtained),
            APUN04Country                     = CONVERT(CHAR(2)      , a.col4_country),
            APUN04Award                       = CONVERT(CHAR(1)      , a.col4_degree_obtained),
            APVisaType                        = null,
            APAB540Waiver                     = null,
            APComfortableEnglish              = CONVERT(BIT          , case when a.comfortable_english = 't' then 1 else 0 end),
            APApplicationLanguage             = null,
            APEsl                             = CONVERT(BIT          , case when a.esl = 't' then 1 else 0 end),
            APBasicSkills                     = CONVERT(BIT          , case when a.basic_skills = 't' then 1 else 0 end),
            APEmploymentAssistance            = CONVERT(BIT          , case when a.employment_assistance = 't' then 1 else 0 end),
            APSeasonalAG                      = CONVERT(BIT          , case when a.ca_seasonal_ag = 't' then 1 else 0 end),
            APCountry                         = null,
            APPermanentCountry                = null,
            APPermanentCountryInternational   = null,
            APCompletedEleventhGrade          = CONVERT(BIT          , case when a.completed_eleventh_grade = 't' then 1 else 0 end),
            APCumulativeGradePointAverage     = CONVERT(DECIMAL(3,2) , case when IsNumeric(a.grade_point_average) = 0 then null else a.grade_point_average end),
            APEnglishCompletedCourseId        = CONVERT(TINYINT      , case when IsNumeric(a.highest_english_course) = 0 or a.highest_english_course like '%.%' then null else a.highest_english_course end),
            APEnglishCompletedCourseGrade     = CONVERT(VARCHAR(3)   , a.highest_english_grade),
            APMathematicsCompletedCourseId    = CONVERT(TINYINT      , case when IsNumeric(a.highest_math_course_taken) = 0 or a.highest_math_course_taken like '%.%' then null else a.highest_math_course_taken end),
            APMathematicsCompletedCourseGrade = CONVERT(VARCHAR(3)   , a.highest_math_taken_grade),
            APMathematicsPassedCourseId       = CONVERT(TINYINT      , case when IsNumeric(a.highest_math_course_passed) = 0 or a.highest_math_course_passed like '%.%' then null else a.highest_math_course_passed end),
            APMathematicsPassedCourseGrade    = CONVERT(VARCHAR(3)   , a.highest_math_passed_grade),
            Selector                          = row_number() over(partition by a.InterSegmentKey order by (select 1))
        FROM
            Mmap.Student s
            inner join
            comis.ApplicationNew a
                on a.InterSegmentKey = s.InterSegmentKey
    ) s
        on s.InterSegmentKey = t.InterSegmentKey
WHERE
    s.Selector = 1;

UPDATE
    t
SET
    t.CCSubjAge                    = s.CCSubjAge,
    t.CCGender                     = s.CCGender,
    t.CCCitzenship                 = s.CCCitzenship,
    t.CCResidency                  = s.CCResidency,
    t.CCEducationStatus            = s.CCEducationStatus,
    t.CCEnrollmentStatus           = s.CCEnrollmentStatus,
    t.CCEthnicities                = s.CCEthnicities,
    t.CCEthnicity                  = s.CCEthnicity,
    t.MilitaryStatus               = s.MilitaryStatus,
    t.MilitaryDependentStatus      = s.MilitaryDependentStatus,
    t.FosterYouthStatus            = s.FosterYouthStatus,
    t.IncarceratedStatus           = s.IncarceratedStatus,
    t.MESAASEMStatus               = s.MESAASEMStatus,
    t.PuenteStatus                 = s.PuenteStatus,
    t.MCHSECHSStatus               = s.MCHSECHSStatus,
    t.UMOJAStatus                  = s.UMOJAStatus,
    t.CAAStatus                    = s.CAAStatus,
    t.BaccalaureateProgram         = s.BaccalaureateProgram,
    t.CCAPStatus                   = s.CCAPStatus,
    t.EconomicallyDisadvStatus     = s.EconomicallyDisadvStatus,
    t.ExOffenderStatus             = s.ExOffenderStatus,
    t.HomelessStatus               = s.HomelessStatus,
    t.LongtermUnemployStatus       = s.LongtermUnemployStatus,
    t.CulturalBarrierStatus        = s.CulturalBarrierStatus,
    t.SeasonalFarmWorkStatus       = s.SeasonalFarmWorkStatus,
    t.LiteracyStatus               = s.LiteracyStatus,
    t.WorkBasedLearningStatus      = s.WorkBasedLearningStatus,
    t.CCIsMultiCollege             = s.CCIsMultiCollege,
    t.CCPrimaryDisability          = s.CCPrimaryDisability,
    t.CCEducationalGoal            = s.CCEducationalGoal,
    t.CCInitialGoal                = s.CCInitialGoal,
    t.CCMajor                      = s.CCMajor,
    t.CCAssessmentStatus           = s.CCAssessmentStatus,
    t.CCAdvisementStatus           = s.CCAdvisementStatus,
    t.CCEnrollmentLastYearTermCode = s.CCEnrollmentLastYearTermCode,
    t.CCEopsCareStatus             = s.CCEopsCareStatus,
    t.CCEopsEligibilityFactor      = s.CCEopsEligibilityFactor,
    t.CCEopsTermOfAcceptance       = s.CCEopsTermOfAcceptance
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey              = r.InterSegmentKey,
            CCSubjAge                    = max(case when t.YearTermCode = r.CCEACNFLYearTermCode then st.age_at_term end),
            CCGender                     = max(case when t.YearTermCode = r.CCEACNFLYearTermCode then st.gender end),
            CCCitzenship                 = max(case when t.YearTermCode = r.CCEACNFLYearTermCode then st.citizenship end),
            CCResidency                  = max(case when t.YearTermCode = r.CCEACNFLYearTermCode then st.residency end),
            CCEducationStatus            = max(case when t.YearTermCode = r.CCEACNFLYearTermCode then st.education end),
            CCEnrollmentStatus           = max(case when t.YearTermCode = r.CCEACNFLYearTermCode then st.enrollment end),
            CCEthnicities                = max(case when t.YearTermCode = r.CCEACNFLYearTermCode then st.multi_race end),
            CCEthnicity                  = max(case when t.YearTermCode = r.CCEACNFLYearTermCode then st.ipeds_race end),
            MilitaryStatus               = max(case when substring(sg.military, 1, 1) = '1' or substring(sg.military, 2, 1) = '1' or substring(sg.military, 3, 1) = '1' or substring(sg.military, 4, 1) = '1' then 1 end),
            MilitaryDependentStatus      = max(case when substring(sg.military_dependent, 1, 1) = '1' or substring(sg.military_dependent, 2, 1) = '1' or substring(sg.military_dependent, 3, 1) = '1' or substring(sg.military_dependent, 4, 1) = '1' then 1 end),
            FosterYouthStatus            = max(case when sg.foster_care = '1' then 1 end),
            IncarceratedStatus           = max(case when sg.incarcerated in ('1', '2', '3', '4', '5', '6') then 1 end),
            MESAASEMStatus               = max(case when sg.mesa in ('1', '2') then 1 end),
            PuenteStatus                 = max(case when sg.puente = '1' then 1 end),
            MCHSECHSStatus               = max(case when sg.mchs in ('1', '4') then 1 end),
            UMOJAStatus                  = max(case when sg.umoja = '1' then 1 end),
            CAAStatus                    = max(case when sg.caa in ('1', '2') then 1 end),
            BaccalaureateProgram         = max(sg.ba),
            CCAPStatus                   = max(case when sg.ccap = '1' then 1 end),
            EconomicallyDisadvStatus     = max(case when left(sg.economically_disadv, 1) in ('1', '2', '3', '4', '5', '6', '7', '8') then 1 end),
            ExOffenderStatus             = max(case when sg.ex_offender = '1' then 1 end),
            HomelessStatus               = max(case when sg.homeless = '1' then 1 end),
            LongtermUnemployStatus       = max(case when sg.longterm_unemploy = '1' then 1 end),
            CulturalBarrierStatus        = max(case when sg.cultural_barrier = '1' then 1 end),
            SeasonalFarmWorkStatus       = max(case when sg.seasonal_farm_work = '1' then 1 end),
            LiteracyStatus               = max(case when sg.literacy = '1' then 1 end),
            WorkBasedLearningStatus      = max(case when sg.work_based_learning in ('A', 'B', 'C') then 1 end),
            CCPrimaryDisability          = max(case when sd.primary_disability is not null then 1 end),
            CCEducationalGoal            = max(case when t.YearTermCode = r.CCEACNFLYearTermCode then coalesce(substring(sm.goals, 1, 1), ss.goals) end),
            CCInitialGoal                = max(case when t.YearTermCode = r.CCEACNFLYearTermCode then st.goal end),
            CCMajor                      = max(case when t.YearTermCode = r.CCEACNFLYearTermCode then coalesce(sm.major, ss.major) end),
            CCAssessmentStatus           = max(case when t.YearTermCode = r.CCEACNFLYearTermCode then coalesce(substring(sm.assessment, 1, 2), ss.assessment_status) end),
            CCAdvisementStatus           = max(case when t.YearTermCode = r.CCEACNFLYearTermCode then coalesce(substring(sm.advisement, 1, 2), ss.educationplan_status) end),
            CCEnrollmentLastYearTermCode = max(t.YearTermCode),
            CCEopsCareStatus             = max(case when se.eops_care_status is not null then 1 end),
            CCEopsEligibilityFactor      = max(case when t.YearTermCode = r.CCEACNFLYearTermCode then se.elig_factor end),
            CCEopsTermOfAcceptance       = max(case when t.YearTermCode = r.CCEACNFLYearTermCode then se.term_accept end),
            CCIsMultiCollege             = s.IsMultiCollege
        FROM
            Mmap.RetroEsl r
            inner join
            comis.Student s on
                s.InterSegmentKey = r.InterSegmentKey
            inner join
            comis.studntid id on
                id.InterSegmentKey = r.InterSegmentKey
            inner join
            comis.stterm st on
                st.college_id = id.college_id and
                st.student_id = id.student_id
            inner join
            comis.term t on
                t.TermCode = st.term_id
            left outer join
            comis.sgpops sg on
                sg.college_id = st.college_id and
                sg.student_id = st.student_id and
                sg.term_id    = st.term_id
            left outer join
            comis.sddsps sd on
                sd.college_id = st.college_id and
                sd.student_id = st.student_id and
                sd.term_id    = st.term_id
            left outer join
            comis.smmatric sm on
                sm.college_id = st.college_id and
                sm.student_id = st.student_id and
                sm.term_id    = st.term_id
            left outer join
            comis.sssuccess ss on
                ss.college_id = st.college_id and
                ss.student_id = st.student_id and
                ss.term_id    = st.term_id
            left outer join
            comis.seeops se on
                se.college_id = st.college_id and
                se.student_id = st.student_id and
                se.term_id    = st.term_id
        GROUP BY
            r.InterSegmentKey,
            s.IsMultiCollege
    ) s
        on s.InterSegmentKey = t.InterSegmentKey;

UPDATE
    t
SET
    t.CCTransferableUnitsAttempted = s.CCTransferableUnitsAttempted,
    t.CCTransferableUnitsEarned    = s.CCTransferableUnitsEarned
FROM
    Mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey              = id.InterSegmentKey,
            CCTransferableUnitsAttempted = sum(sx.units_attempted),
            CCTransferableUnitsEarned    = sum(sx.units)
        FROM
            comis.CBCRSINV cb
            inner join
            comis.SXENRLM SX
                on  sx.college_id     = cb.college_id
                and sx.term_id        = cb.term_id
                and sx.course_id      = cb.course_id
                and sx.control_number = cb.control_number
            inner join
            comis.STUDNTID id
                on  id.college_id = sx.college_id
                and id.student_id = sx.student_id
        WHERE
            cb.transfer_status in ('A', 'B')
            and sx.units_attempted <> 88.88
            and sx.units <> 88.88
        GROUP BY
            id.InterSegmentKey
    ) s
        on t.InterSegmentKey = s.InterSegmentKey;

GO

UPDATE
    t
SET
    t.CCENCR00IsEngl   = isnull(tc1.IsEngl, 0),
    t.CCENCR002NIsEngl = isnull(tc2.IsEngl, 0)
FROM
    Mmap.RetroEsl t
    left outer join
    mmap.TransferCourses tc1 on
        tc1.ControlNumber = t.CCENCR00CourseControlNumber
    left outer join
    mmap.TransferCourses tc2 on
        tc2.ControlNumber = t.CCENCR002NCourseControlNumber;

GO

UPDATE
    a
SET
    a.XferFice       = b.fice,
    a.XferDate       = b.date_of_xfer,
    a.XfersSource    = b.source,
    a.XferSchoolName = b.tfr_in_school_name,
    a.XferSchoolType = b.tfr_in_inst_type,
    a.XferSector     = b.sector,
    a.XferState      = b.state,
    a.XferSegment    = b.segment
FROM
    mmap.RetroEsl a
    inner join
    (
        SELECT
            a.InterSegmentKey,
            x.fice,
            x.date_of_xfer,
            x.source,
            x.tfr_in_school_name,
            x.tfr_in_inst_type,
            x.sector,
            x.state,
            x.segment,
            selector = row_number() over(partition by a.InterSegmentKey order by x.date_of_xfer asc)
        FROM
            mmap.RetroEsl a
            inner join
            comis.studntid id on
                id.InterSegmentKey = a.InterSegmentKey
            inner join
            comis.xfer_bucket x on
                x.ssn = id.ssn
            inner join
            comis.Term t on
                t.YearTermCode = a.CCENCR00YearTermCode
            cross apply
            GetAcademicYear(x.date_of_xfer) y
        WHERE
            y.AcademicYear >= t.AcademicYear and
            x.Segment      <> 'CCC'
    ) b on
        a.InterSegmentKey = b.InterSegmentKey
WHERE
    b.selector = 1;

GO

UPDATE
    mmap.RetroEsl
SET
    CCENCR00TleComposition   = mmap.tle(CCENCR00CourseTitle,   CCENCR00CollegeCode,   CCENCR00CourseControlNumber),
    CCENCR002NTleComposition = mmap.tle(CCENCR002NCourseTitle, CCENCR002NCollegeCode, CCENCR002NCourseControlNumber);

GO

UPDATE
    t
SET
    t.ElaStatusRecent =
        case e.ElaStatusCode
            when 'EL' then 0
            when 'EO' then 1
            when 'IFEP' then 2
            when 'RFEP' then 3
        end
FROM
    mmap.RetroEsl t
    inner join
    calpads.student id on
        id.InterSegmentKey = t.InterSegmentKey
    inner join
    calpads.sela e on
        e.StudentStateId = id.StudentStateId
WHERE
    e.ElaStatusStartDate = (
        SELECT
            max(e1.ElaStatusStartDate)
        FROM
            calpads.sela e1
        WHERE
            e1.StudentStateId = e.StudentStateId
    );

GO

UPDATE
    t
SET
    t.CCSP0101 = s.CCSP0101,
    t.CCSP0102 = s.CCSP0102,
    t.CCSP0103 = s.CCSP0103,
    t.CCSP0104 = s.CCSP0104,
    t.CCSP0201 = s.CCSP0201,
    t.CCSP0202 = s.CCSP0202,
    t.CCSP0203 = s.CCSP0203,
    t.CCSP0204 = s.CCSP0204,
    t.CCSP0301 = s.CCSP0301,
    t.CCSP0302 = s.CCSP0302,
    t.CCSP0303 = s.CCSP0303,
    t.CCSP0304 = s.CCSP0304
FROM
    mmap.RetroEsl t
    inner join
    (
        SELECT
            InterSegmentKey,
            CCSP0101 = max(case when a.first_ind = 1  then a.top_code     end),
            CCSP0102 = max(case when a.first_ind = 1  then a.award        end),
            CCSP0103 = max(case when a.first_ind = 1  then a.award_earned end),
            CCSP0104 = max(case when a.first_ind = 1  then a.program_code end),
            CCSP0201 = max(case when a.recent_ind = 1 then a.top_code     end),
            CCSP0202 = max(case when a.recent_ind = 1 then a.award        end),
            CCSP0203 = max(case when a.recent_ind = 1 then a.award_earned end),
            CCSP0204 = max(case when a.recent_ind = 1 then a.program_code end),
            CCSP0301 = max(case when a.recent_ind = 2 then a.top_code     end),
            CCSP0302 = max(case when a.recent_ind = 2 then a.award        end),
            CCSP0303 = max(case when a.recent_ind = 2 then a.award_earned end),
            CCSP0304 = max(case when a.recent_ind = 2 then a.program_code end)
        FROM
            (
                SELECT
                    id.InterSegmentKey,
                    sp.top_code,
                    sp.award,
                    award_earned = convert(char(8), sp.date, 112),
                    sp.program_code,
                    first_ind = 
                        row_number() over(
                            partition by
                                id.InterSegmentKey
                            order by
                                t.YearTermCode asc,
                                case award
                                    when 'Z' then 1
                                    when 'Y' then 2
                                    when 'S' then 3
                                    when 'A' then 4
                                    when 'F' then 5
                                    when 'T' then 6
                                    when 'L' then 7
                                    when 'B' then 8
                                    when 'E' then 9
                                    when 'O' then 10
                                    when 'R' then 11
                                    when 'Q' then 12
                                    when 'P' then 13
                                    when 'K' then 14
                                    when 'J' then 15
                                    when 'I' then 16
                                    when 'H' then 17
                                    when 'G' then 18
                                    else 99
                                end asc
                        ),
                    recent_ind = 
                        row_number() over(
                            partition by
                                id.InterSegmentKey
                            order by
                                t.YearTermCode desc,
                                case award
                                    when 'Z' then 1
                                    when 'Y' then 2
                                    when 'S' then 3
                                    when 'A' then 4
                                    when 'F' then 5
                                    when 'T' then 6
                                    when 'L' then 7
                                    when 'B' then 8
                                    when 'E' then 9
                                    when 'O' then 10
                                    when 'R' then 11
                                    when 'Q' then 12
                                    when 'P' then 13
                                    when 'K' then 14
                                    when 'J' then 15
                                    when 'I' then 16
                                    when 'H' then 17
                                    when 'G' then 18
                                    else 99
                                end asc
                        )
                FROM
                    comis.spawards sp
                    inner join
                    comis.studntid id on
                        id.college_id = sp.college_id and
                        id.student_id = sp.student_id
                    inner join
                    comis.term t on
                        t.TermCode = sp.term_id
            ) a
        GROUP BY
            a.InterSegmentKey
    ) s on
        s.InterSegmentKey = t.InterSegmentKey;

GO

-- REMOVE SUPORT COURSES AS PRIMARY COURSES
UPDATE
    a
SET
    a.CCENCR00CollegeCode           = null,
    a.CCENCR00YearTermCode          = null,
    a.CCENCR00CourseId              = null,
    a.CCENCR00SectionId             = null,
    a.CCENCR00CourseControlNumber   = null,
    a.CCENCR00CourseTopCode         = null,
    a.CCENCR00CourseTitle           = null,
    a.CCENCR00CourseMarkLetter      = null,
    a.CCENCR00CourseMarkPoints      = null,
    a.CCENCR00CourseLevelCode       = null
FROM
    mmap.RetroEsl a
    inner join
    mmap.CoReqEngl b on
        b.CollegeCode   = a.CCENCR00CollegeCode and
        b.ControlNumber = a.CCENCR00CourseControlNumber;

UPDATE
    a
SET
    a.CCENCR002NCollegeCode         = null,
    a.CCENCR002NYearTermCode        = null,
    a.CCENCR002NCourseId            = null,
    a.CCENCR002NSectionId           = null,
    a.CCENCR002NCourseControlNumber = null,
    a.CCENCR002NCourseTopCode       = null,
    a.CCENCR002NCourseTitle         = null,
    a.CCENCR002NCourseMarkLetter    = null,
    a.CCENCR002NCourseMarkPoints    = null,
    a.CCENCR002NCourseLevelCode     = null
FROM
    mmap.RetroEsl a
    inner join
    mmap.CoReqEngl b on
        b.CollegeCode   = a.CCENCR002NCollegeCode and
        b.ControlNumber = a.CCENCR002NCourseControlNumber;

-- UPDATE SUPPORT COURSES
UPDATE
    t
SET
    t.CCENCR00RQCollegeCode           = s.CollegeCode,
    t.CCENCR00RQYearTermCode          = s.YearTermCode,
    t.CCENCR00RQCourseId              = s.CourseId,
    t.CCENCR00RQSectionId             = s.CourseSectionId,
    t.CCENCR00RQCourseControlNumber   = s.CourseControlNumber,
    t.CCENCR00RQCourseTopCode         = s.CourseTopCode,
    t.CCENCR00RQCourseTitle           = s.CourseTitle,
    t.CCENCR00RQCourseMarkLetter      = s.CourseMarkLetter,
    t.CCENCR00RQCourseMarkPoints      = s.CourseMarkPoints,
    t.CCENCR00RQCourseLevelCode       = s.CourseLevelCode,
    t.CCENCR00RQCreditType            = s.CourseCreditCode
FROM
    mmap.RetroEsl t
    inner join
    mmap.CCTranscript s on
        s.CollegeCode     = t.CCENCR00CollegeCode  and
        s.YearTermCode    = t.CCENCR00YearTermCode and
        s.InterSegmentKey = t.InterSegmentKey
    inner join
    mmap.CoReqEngl c on
        c.CollegeCode   = s.CollegeCode and
        c.ControlNumber = s.CourseControlNumber;

-- UPDATE SUPPORT COURSES

UPDATE
    t
SET
    t.CCENCR002NRQCollegeCode           = s.CollegeCode,
    t.CCENCR002NRQYearTermCode          = s.YearTermCode,
    t.CCENCR002NRQCourseId              = s.CourseId,
    t.CCENCR002NRQSectionId             = s.CourseSectionId,
    t.CCENCR002NRQCourseControlNumber   = s.CourseControlNumber,
    t.CCENCR002NRQCourseTopCode         = s.CourseTopCode,
    t.CCENCR002NRQCourseTitle           = s.CourseTitle,
    t.CCENCR002NRQCourseMarkLetter      = s.CourseMarkLetter,
    t.CCENCR002NRQCourseMarkPoints      = s.CourseMarkPoints,
    t.CCENCR002NRQCourseLevelCode       = s.CourseLevelCode,
    t.CCENCR002NRQCreditType            = s.CourseCreditCode
FROM
    mmap.RetroEsl t
    inner join
    mmap.CCTranscript s on
        s.CollegeCode     = t.CCENCR002NCollegeCode  and
        s.YearTermCode    = t.CCENCR002NYearTermCode and
        s.InterSegmentKey = t.InterSegmentKey
    inner join
    mmap.CoReqEngl c on
        c.CollegeCode   = s.CollegeCode and
        c.ControlNumber = s.CourseControlNumber;

UPDATE
    t
SET
    t.CCENCR00RQAttendHours = sx.attend_hours
FROM
    mmap.retroesl t
    inner join
    comis.student s on
        s.InterSegmentKey = t.InterSegmentKey
    inner join
    comis.studntid id on
        id.InterSegmentKey = s.InterSegmentKey
    inner join
    comis.sxenrlm sx on
        sx.college_id = id.college_id and
        sx.student_id = id.student_id
    inner join
    comis.Term z on
        z.TermCode     = sx.term_id
WHERE
    z.YearTermCode = t.CCENCR00RQYearTermCode and
    sx.course_id   = t.CCENCR00RQCourseId and
    sx.section_id  = t.CCENCR00RQSectionId;

UPDATE
    t
SET
    t.CCENCR002NRQAttendHours = sx.attend_hours
FROM
    mmap.retroesl t
    inner join
    comis.student s on
        s.InterSegmentKey = t.InterSegmentKey
    inner join
    comis.studntid id on
        id.InterSegmentKey = s.InterSegmentKey
    inner join
    comis.sxenrlm sx on
        sx.college_id = id.college_id and
        sx.student_id = id.student_id
    inner join
    comis.Term z on
        z.TermCode     = sx.term_id
WHERE
    z.YearTermCode = t.CCENCR002NRQYearTermCode and
    sx.course_id   = t.CCENCR002NRQCourseId and
    sx.section_id  = t.CCENCR002NRQSectionId;

GO

DROP PROC IF EXISTS mmap.RetroEslExport;

GO

CREATE PROC
    mmap.RetroEslExport
AS
BEGIN
    SELECT
        RequestStudentId = convert(int, row_number() over(order by newid()) + 100000000),
        HS09GradeCode,
        HS09OverallGradePointAverage,
        HS09OverallCumulativeGradePointAverage,
        HS09EnglishGradePointAverage,
        HS09EnglishCumulativeGradePointAverage,
        HS09SchoolCode,
        HS09YearTermCode,
        HS09CourseCode,
        HS09CourseContentRank,
        HS09CourseTitle,
        HS09CourseMarkLetter,
        HS09CourseMarkPoints,
        HS09CourseUniversityAdmissionRequirementCode,
        HS09CourseLevelCode,
        HS09CourseTypeCode,
        HS10GradeCode,
        HS10OverallGradePointAverage,
        HS10OverallCumulativeGradePointAverage,
        HS10EnglishGradePointAverage,
        HS10EnglishCumulativeGradePointAverage,
        HS10SchoolCode,
        HS10YearTermCode,
        HS10CourseCode,
        HS10CourseContentRank,
        HS10CourseTitle,
        HS10CourseMarkLetter,
        HS10CourseMarkPoints,
        HS10CourseUniversityAdmissionRequirementCode,
        HS10CourseLevelCode,
        HS10CourseTypeCode,
        HS11GradeCode,
        HS11OverallGradePointAverage,
        HS11OverallCumulativeGradePointAverage,
        HS11EnglishGradePointAverage,
        HS11EnglishCumulativeGradePointAverage,
        HS11SchoolCode,
        HS11YearTermCode,
        HS11CourseCode,
        HS11CourseContentRank,
        HS11CourseTitle,
        HS11CourseMarkLetter,
        HS11CourseMarkPoints,
        HS11CourseUniversityAdmissionRequirementCode,
        HS11CourseLevelCode,
        HS11CourseTypeCode,
        HS12GradeCode,
        HS12OverallGradePointAverage,
        HS12OverallCumulativeGradePointAverage,
        HS12EnglishGradePointAverage,
        HS12EnglishCumulativeGradePointAverage,
        HS12SchoolCode,
        HS12YearTermCode,
        HS12CourseCode,
        HS12CourseContentRank,
        HS12CourseTitle,
        HS12CourseMarkLetter,
        HS12CourseMarkPoints,
        HS12CourseUniversityAdmissionRequirementCode,
        HS12CourseLevelCode,
        HS12CourseTypeCode,
        HSLGGradeCode,
        HSLGOverallGradePointAverage,
        HSLGOverallCumulativeGradePointAverage,
        HSLGEnglishGradePointAverage,
        HSLGEnglishCumulativeGradePointAverage,
        HSLGSchoolCode,
        HSLGYearTermCode,
        HSLGCourseCode,
        HSLGCourseContentRank,
        HSLGCourseTitle,
        HSLGCourseMarkLetter,
        HSLGCourseMarkPoints,
        HSLGCourseUniversityAdmissionRequirementCode,
        HSLGCourseLevelCode,
        HSLGCourseTypeCode,
        CCEACNFLCollegeCode,
        CCEACNFLYearTermCode,
        CCEACNFLCourseId,
        CCEACNFLSectionId,
        CCEACNFLCourseControlNumber,
        CCEACNFLCourseTopCode,
        CCEACNFLCourseTitle,
        CCEACNFLCourseMarkLetter,
        CCEACNFLCourseMarkPoints,
        CCEACNFLCourseLevelCode,
        CCEACNFLCourseCreditCode,
        CCEACNFLSummary,
        CCEACNFLEslUnits,
        CCEACNFLEnglUnits,
        CCEACN00CollegeCode,
        CCEACN00YearTermCode,
        CCEACN00CourseId,
        CCEACN00SectionId,
        CCEACN00CourseControlNumber,
        CCEACN00CourseTopCode,
        CCEACN00CourseTitle,
        CCEACN00CourseMarkLetter,
        CCEACN00CourseMarkPoints,
        CCEACN00CourseLevelCode,
        CCEACN00CourseCreditCode,
        CCEACN00Summary,
        CCEACN00EslUnits,
        CCEACN00EnglUnits,
        CCEACN01CollegeCode,
        CCEACN01YearTermCode,
        CCEACN01CourseId,
        CCEACN01SectionId,
        CCEACN01CourseControlNumber,
        CCEACN01CourseTopCode,
        CCEACN01CourseTitle,
        CCEACN01CourseMarkLetter,
        CCEACN01CourseMarkPoints,
        CCEACN01CourseLevelCode,
        CCEACN01CourseCreditCode,
        CCEACN01Summary,
        CCEACN01EslUnits,
        CCEACN01EnglUnits,
        CCEACN02CollegeCode,
        CCEACN02YearTermCode,
        CCEACN02CourseId,
        CCEACN02SectionId,
        CCEACN02CourseControlNumber,
        CCEACN02CourseTopCode,
        CCEACN02CourseTitle,
        CCEACN02CourseMarkLetter,
        CCEACN02CourseMarkPoints,
        CCEACN02CourseLevelCode,
        CCEACN02CourseCreditCode,
        CCEACN02Summary,
        CCEACN02EslUnits,
        CCEACN02EnglUnits,
        CCEACN03CollegeCode,
        CCEACN03YearTermCode,
        CCEACN03CourseId,
        CCEACN03SectionId,
        CCEACN03CourseControlNumber,
        CCEACN03CourseTopCode,
        CCEACN03CourseTitle,
        CCEACN03CourseMarkLetter,
        CCEACN03CourseMarkPoints,
        CCEACN03CourseLevelCode,
        CCEACN03CourseCreditCode,
        CCEACN03Summary,
        CCEACN03EslUnits,
        CCEACN03EnglUnits,
        CCEACN04CollegeCode,
        CCEACN04YearTermCode,
        CCEACN04CourseId,
        CCEACN04SectionId,
        CCEACN04CourseControlNumber,
        CCEACN04CourseTopCode,
        CCEACN04CourseTitle,
        CCEACN04CourseMarkLetter,
        CCEACN04CourseMarkPoints,
        CCEACN04CourseLevelCode,
        CCEACN04CourseCreditCode,
        CCEACN04Summary,
        CCEACN04EslUnits,
        CCEACN04EnglUnits,
        CCEACN05CollegeCode,
        CCEACN05YearTermCode,
        CCEACN05CourseId,
        CCEACN05SectionId,
        CCEACN05CourseControlNumber,
        CCEACN05CourseTopCode,
        CCEACN05CourseTitle,
        CCEACN05CourseMarkLetter,
        CCEACN05CourseMarkPoints,
        CCEACN05CourseLevelCode,
        CCEACN05CourseCreditCode,
        CCEACN05Summary,
        CCEACN05EslUnits,
        CCEACN05EnglUnits,
        CCEACN06CollegeCode,
        CCEACN06YearTermCode,
        CCEACN06CourseId,
        CCEACN06SectionId,
        CCEACN06CourseControlNumber,
        CCEACN06CourseTopCode,
        CCEACN06CourseTitle,
        CCEACN06CourseMarkLetter,
        CCEACN06CourseMarkPoints,
        CCEACN06CourseLevelCode,
        CCEACN06CourseCreditCode,
        CCEACN06Summary,
        CCEACN06EslUnits,
        CCEACN06EnglUnits,
        CCEACN07CollegeCode,
        CCEACN07YearTermCode,
        CCEACN07CourseId,
        CCEACN07SectionId,
        CCEACN07CourseControlNumber,
        CCEACN07CourseTopCode,
        CCEACN07CourseTitle,
        CCEACN07CourseMarkLetter,
        CCEACN07CourseMarkPoints,
        CCEACN07CourseLevelCode,
        CCEACN07CourseCreditCode,
        CCEACN07Summary,
        CCEACN07EslUnits,
        CCEACN07EnglUnits,
        CCEACN08CollegeCode,
        CCEACN08YearTermCode,
        CCEACN08CourseId,
        CCEACN08SectionId,
        CCEACN08CourseControlNumber,
        CCEACN08CourseTopCode,
        CCEACN08CourseTitle,
        CCEACN08CourseMarkLetter,
        CCEACN08CourseMarkPoints,
        CCEACN08CourseLevelCode,
        CCEACN08CourseCreditCode,
        CCEACN08Summary,
        CCEACN08EslUnits,
        CCEACN08EnglUnits,
        CCENCRFLCollegeCode,
        CCENCRFLYearTermCode,
        CCENCRFLCourseId,
        CCENCNFLSectionId,
        CCENCRFLCourseControlNumber,
        CCENCRFLCourseTopCode,
        CCENCRFLCourseTitle,
        CCENCRFLCourseMarkLetter,
        CCENCRFLCourseMarkPoints,
        CCENCRFLCourseLevelCode,
        CCENCRFLUnitsAttempt,
        CCENCRFLUnitsSuccess,
        CCENCR00CollegeCode,
        CCENCR00YearTermCode,
        CCENCR00CourseId,
        CCENCR00SectionId,
        CCENCR00CourseControlNumber,
        CCENCR00CourseTopCode,
        CCENCR00CourseTitle,
        CCENCR00CourseMarkLetter,
        CCENCR00CourseMarkPoints,
        CCENCR00CourseLevelCode,
        CCENCR00CB25,
        CCENCR00CB26,
        CCENCR00TleComposition,
        CCENCR00IsEngl,
        CCENCR002NCollegeCode,
        CCENCR002NYearTermCode,
        CCENCR002NCourseId,
        CCENCR002NSectionId,
        CCENCR002NCourseControlNumber,
        CCENCR002NCourseTopCode,
        CCENCR002NCourseTitle,
        CCENCR002NCourseMarkLetter,
        CCENCR002NCourseMarkPoints,
        CCENCR002NCourseLevelCode,
        CCENCR002NCB25,
        CCENCR002NCB26,
        CCENCR002NTleComposition,
        CCENCR002NIsEngl,
        CCENCR00RQCollegeCode,
        CCENCR00RQYearTermCode,
        CCENCR00RQCourseId,
        CCENCR00RQSectionId,
        CCENCR00RQCourseControlNumber,
        CCENCR00RQCourseTopCode,
        CCENCR00RQCourseTitle,
        CCENCR00RQCourseMarkLetter,
        CCENCR00RQCourseMarkPoints,
        CCENCR00RQCourseLevelCode,
        CCENCR00RQCB25,
        CCENCR00RQCB26,
        CCENCR00RQCreditType,
        CCENCR00RQAttendHours,
        CCENCR002NRQCollegeCode,
        CCENCR002NRQYearTermCode,
        CCENCR002NRQCourseId,
        CCENCR002NRQSectionId,
        CCENCR002NRQCourseControlNumber,
        CCENCR002NRQCourseTopCode,
        CCENCR002NRQCourseTitle,
        CCENCR002NRQCourseMarkLetter,
        CCENCR002NRQCourseMarkPoints,
        CCENCR002NRQCourseLevelCode,
        CCENCR002NRQCB25,
        CCENCR002NRQCB26,
        CCENCR002NRQCreditType,
        CCENCR002NRQAttendHours,
        CCENCR01CollegeCode,
        CCENCR01YearTermCode,
        CCENCR01CourseId,
        CCENCR01SectionId,
        CCENCR01CourseControlNumber,
        CCENCR01CourseTopCode,
        CCENCR01CourseTitle,
        CCENCR01CourseMarkLetter,
        CCENCR01CourseMarkPoints,
        CCENCR01CourseLevelCode,
        CCENCR01CB25,
        CCENCR01CB26,
        CCENCR02CollegeCode,
        CCENCR02YearTermCode,
        CCENCR02CourseId,
        CCENCR02SectionId,
        CCENCR02CourseControlNumber,
        CCENCR02CourseTopCode,
        CCENCR02CourseTitle,
        CCENCR02CourseMarkLetter,
        CCENCR02CourseMarkPoints,
        CCENCR02CourseLevelCode,
        CCENCR02CB25,
        CCENCR02CB26,
        CCENCR03CollegeCode,
        CCENCR03YearTermCode,
        CCENCR03CourseId,
        CCENCR03SectionId,
        CCENCR03CourseControlNumber,
        CCENCR03CourseTopCode,
        CCENCR03CourseTitle,
        CCENCR03CourseMarkLetter,
        CCENCR03CourseMarkPoints,
        CCENCR03CourseLevelCode,
        CCENCR03CB25,
        CCENCR03CB26,
        CCENCR04CollegeCode,
        CCENCR04YearTermCode,
        CCENCR04CourseId,
        CCENCR04SectionId,
        CCENCR04CourseControlNumber,
        CCENCR04CourseTopCode,
        CCENCR04CourseTitle,
        CCENCR04CourseMarkLetter,
        CCENCR04CourseMarkPoints,
        CCENCR04CourseLevelCode,
        CCENCR04CB25,
        CCENCR04CB26,
        CCRDCRFLCollegeCode,
        CCRDCRFLYearTermCode,
        CCRDCRFLCourseId,
        CCRDCRFLSectionId,
        CCRDCRFLCourseControlNumber,
        CCRDCRFLCourseTopCode,
        CCRDCRFLCourseTitle,
        CCRDCRFLCourseMarkLetter,
        CCRDCRFLCourseMarkPoints,
        CCRDCRFLCourseLevelCode,
        CCRDCR00CollegeCode,
        CCRDCR00YearTermCode,
        CCRDCR00CourseId,
        CCRDCR00SectionId,
        CCRDCR00CourseControlNumber,
        CCRDCR00CourseTopCode,
        CCRDCR00CourseTitle,
        CCRDCR00CourseMarkLetter,
        CCRDCR00CourseMarkPoints,
        CCRDCR00CourseLevelCode,
        CCRDCR01CollegeCode,
        CCRDCR01YearTermCode,
        CCRDCR01CourseId,
        CCRDCR01SectionId,
        CCRDCR01CourseControlNumber,
        CCRDCR01CourseTopCode,
        CCRDCR01CourseTitle,
        CCRDCR01CourseMarkLetter,
        CCRDCR01CourseMarkPoints,
        CCRDCR01CourseLevelCode,
        CCRDCR02CollegeCode,
        CCRDCR02YearTermCode,
        CCRDCR02CourseId,
        CCRDCR02SectionId,
        CCRDCR02CourseControlNumber,
        CCRDCR02CourseTopCode,
        CCRDCR02CourseTitle,
        CCRDCR02CourseMarkLetter,
        CCRDCR02CourseMarkPoints,
        CCRDCR02CourseLevelCode,
        CCRDCR03CollegeCode,
        CCRDCR03YearTermCode,
        CCRDCR03CourseId,
        CCRDCR03SectionId,
        CCRDCR03CourseControlNumber,
        CCRDCR03CourseTopCode,
        CCRDCR03CourseTitle,
        CCRDCR03CourseMarkLetter,
        CCRDCR03CourseMarkPoints,
        CCRDCR03CourseLevelCode,
        CCRDCR04CollegeCode,
        CCRDCR04YearTermCode,
        CCRDCR04CourseId,
        CCRDCR04SectionId,
        CCRDCR04CourseControlNumber,
        CCRDCR04CourseTopCode,
        CCRDCR04CourseTitle,
        CCRDCR04CourseMarkLetter,
        CCRDCR04CourseMarkPoints,
        CCRDCR04CourseLevelCode,
        STIsEnglishEap,
        STEnglishScaledScore,
        STEnglishCluster1,
        STEnglishCluster2,
        STEnglishCluster3,
        STEnglishCluster4,
        STEnglishCluster5,
        STEnglishCluster6,
        STMathematicsSubject,
        STIsMathematicsEap,
        STMathematicsScaledScore,
        STMathematicsCluster1,
        STMathematicsCluster2,
        STMathematicsCluster3,
        STMathematicsCluster4,
        STMathematicsCluster5,
        STMathematicsCluster6,
        STIsEnglishOnly,
        STIsEnglishLearner,
        STIsFluentInitially,
        STIsFluentReclassified,
        HSIsRemedial,
        HSGraduationDate,
        CCSubjAge,
        CCGender,
        CCCitzenship,
        CCResidency,
        CCEducationStatus,
        CCEnrollmentStatus,
        CCEthnicities,
        CCParentEducationLevel,
        CCEthnicity,
        MilitaryStatus,
        MilitaryDependentStatus,
        FosterYouthStatus,
        IncarceratedStatus,
        MESAASEMStatus,
        PuenteStatus,
        MCHSECHSStatus,
        UMOJAStatus,
        CAAStatus,
        CAFYESStatus,
        BaccalaureateProgram,
        CCAPStatus,
        EconomicallyDisadvStatus,
        ExOffenderStatus,
        HomelessStatus,
        LongtermUnemployStatus,
        CulturalBarrierStatus,
        SeasonalFarmWorkStatus,
        LiteracyStatus,
        WorkBasedLearningStatus,
        CCIsMultiCollege,
        CCPrimaryDisability,
        CCEducationalGoal,
        CCInitialGoal,
        CCMajor,
        CCAssessmentStatus,
        CCAdvisementStatus,
        CCEnrollmentLastYearTermCode,
        CCTransferableUnitsAttempted,
        CCTransferableUnitsEarned,
        CCEopsCareStatus,
        CCEopsEligibilityFactor,
        CCEopsTermOfAcceptance,
        APHSCountry,
        APUN01Country,
        APUN01Award,
        APUN02Country,
        APUN02Award,
        APUN03Country,
        APUN03Award,
        APUN04Country,
        APUN04Award,
        APVisaType,
        APAB540Waiver,
        APComfortableEnglish,
        APApplicationLanguage,
        APEsl,
        APBasicSkills,
        APEmploymentAssistance,
        APSeasonalAG,
        APCountry,
        APPermanentCountry,
        APPermanentCountryInternational,
        APCompletedEleventhGrade,
        APCumulativeGradePointAverage,
        APEnglishCompletedCourseId,
        APEnglishCompletedCourseGrade,
        APMathematicsCompletedCourseId,
        APMathematicsCompletedCourseGrade,
        APMathematicsPassedCourseId,
        APMathematicsPassedCourseGrade,
        XferFice,
        XferDate,
        XfersSource,
        XferSchoolName,
        XferSchoolType,
        XferSector,
        XferState,
        XferSegment,
        ElaStatusRecent,
        CCSP0101,
        CCSP0102,
        CCSP0103,
        CCSP0104,
        CCSP0201,
        CCSP0202,
        CCSP0203,
        CCSP0204,
        CCSP0301,
        CCSP0302,
        CCSP0303,
        CCSP0304
    FROM
        mmap.RetroEsl
END;

GO

EXEC xp_cmdshell 'bcp "DECLARE @Header VARCHAR(MAX) = ''''; SELECT @Header += case when column_ordinal = min(column_ordinal) over () then '''' else char(9) end + c.name FROM sys.dm_exec_describe_first_result_set_for_object(object_id(''mmap.RetroEslExport''), 0) c; SELECT @Header;" QUERYOUT "C:\Users\dlamoree.erp\desktop\RetroEslHeader.txt" -c -T -dcalpass';
EXEC xp_cmdshell 'bcp "calpass.mmap.RetroEslExport" QUERYOUT "C:\Users\dlamoree.erp\desktop\RetroEslOutput.txt" -c -T -dcalpass';
EXEC xp_cmdshell 'copy C:\Users\dlamoree.erp\desktop\RetroEslHeader.txt + C:\Users\dlamoree.erp\desktop\RetroEslOutput.txt C:\Users\dlamoree.erp\desktop\RetroEsl.txt';
EXEC xp_cmdshell 'del C:\Users\dlamoree.erp\desktop\RetroEslHeader.txt C:\Users\dlamoree.erp\desktop\RetroEslOutput.txt';

GO

DROP PROCEDURE mmap.RetroEslExport;
DROP TABLE mmap.RetroEsl;