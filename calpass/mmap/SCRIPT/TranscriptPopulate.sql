SET NOCOUNT ON;

DECLARE
	@Time datetime,
	@Seconds int,
	@Severity tinyint = 0,
	@State tinyint = 1,
	@Error varchar(2048),
	@Counter int = 0,
	@Increment int = 100000,
	@Cycles int;

IF (object_id('tempdb.dbo.#InterSegment') is not null)
	BEGIN
		DROP TABLE #InterSegment;
	END;

CREATE TABLE #InterSegment (Iterator int identity(0,1) primary key clustered, InterSegmentKey binary(64));

IF (object_id('tempdb.dbo.#InterSegmentCohort') is not null)
	BEGIN
		DROP TABLE #InterSegmentCohort;
	END;

CREATE TABLE #InterSegmentCohort (InterSegmentKey binary(64) primary key clustered);

INSERT
	#InterSegment
	(
		InterSegmentKey
	)
SELECT
	InterSegmentKey
FROM
	dbo.Student
WHERE
	IsHighSchoolCollision = 0
	and IsHighSchoolSection = 1
ORDER BY
	InterSegmentKey;

SELECT
	@Cycles = convert(varchar, ceiling(convert(decimal, count(*)) / @Increment))
FROM
	#InterSegment;

WHILE (@Counter < @Cycles)
BEGIN

	SET @Error = convert(varchar, @Counter) + ' of ' + convert(varchar, @Cycles - 1);

	RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

	-- reset time
	SET @Time = getdate();

	TRUNCATE TABLE #InterSegmentCohort;

	INSERT
		#InterSegmentCohort
		(
			InterSegmentKey
		)
	SELECT
		InterSegmentKey
	FROM
		#InterSegment i
	WHERE
		Iterator >= @Increment * @Counter
		and Iterator < @Increment * (@Counter + 1);

	INSERT INTO
		mmap.Transcript
	SELECT
		InterSegmentKey,
		IsHighSchoolGrade09,
		IsHighSchoolGrade10,
		IsHighSchoolGrade11,
		IsHighSchoolGrade12,
		CumulativeGradePointAverage,
		English,
		PreAlgebra,
		AlgebraI,
		Geometry,
		AlgebraII,
		Trigonometry,
		PreCalculus,
		[Statistics],
		Calculus
	FROM
		#InterSegmentCohort i
		cross apply
		mmap.TranscriptGet(i.InterSegmentKey)
	WHERE
		CumulativeGradePointAverage is not null;

	-- get time elapsed
	SET @Seconds = datediff(second, @Time, getdate());
	-- set msg
	SET @Error = 'INSERT: ' + 
		convert(nvarchar, @Seconds / 86400) + ':' +
		convert(nvarchar, dateadd(ms, (@Seconds % 86400) * 1000, 0), 114);
	-- output to user
	RAISERROR(@Error, 0, 1) WITH NOWAIT;

	SET @Counter += 1;
END;