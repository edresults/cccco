USE calpass;

GO

IF (object_id('mmap.K12CourseContentGet') is not null)
	BEGIN
		DROP FUNCTION mmap.K12CourseContentGet;
	END;

GO

CREATE FUNCTION
	mmap.K12CourseContentGet
	(
		@CSISNum char(10)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			DepartmentCode  = d.Code,
			GradeCode       = s.GradeLevel,
			SchoolCode      = s.School,
			YearTermCode    = yt.YearTermCode,
			TermCode        = c.CourseTerm,
			ContentCode     = coalesce(e.ContentCode, ma.ContentCode),
			ContentRank     = coalesce(e.Rank, ma.Rank),
			CourseCode      = c.CourseId,
			CourseUnits     = c.CreditAttempted,
			CourseTitle     = c.CourseTitle,
			CourseAGCode    = c.AGstatus,
			CourseLevelCode = c.CourseLevel,
			CourseTypeCode  = c.CourseType,
			SectionMark     = m.MarkCode,
			MarkPoints      = m.Points,
			MarkCategory    = m.Category,
			IsSuccess       = m.IsSuccess,
			IsPromoted      = m.IsPromoted,
			ContentSelector = 
				row_number() over(
					partition by
						d.Code,
						coalesce(e.ContentCode, ma.ContentCode)
					order by
						-- Recency
						y.Rank desc,
						t.IntraTermOrdinal desc,
						-- Rigor
						coalesce(e.Rank, ma.Rank),
						c.CreditAttempted,
						-- Performance
						m.Rank
				),
			GradeSelector   = 
				row_number() over(
					partition by
						d.Code,
						s.GradeLevel
					order by
						y.Rank desc,
						t.IntraTermOrdinal desc,
						-- Rigor
						coalesce(e.Rank, ma.Rank),
						c.CreditAttempted,
						-- Performance
						m.Rank
				),
			RecencySelector = 
				row_number() over(
					partition by
						d.Code
					order by
						y.Rank desc,
						t.IntraTermOrdinal desc,
						-- Rigor
						coalesce(e.Rank, ma.Rank),
						c.CreditAttempted,
						-- Performance
						m.Rank
				)
		FROM
			dbo.K12StudentProd s
			inner join
			dbo.K12CourseProd c
				on s.School = c.School
				and s.LocStudentId = c.LocStudentId
				and s.AcYear = c.AcYear
			inner join
			calpads.Mark m
				on m.MarkCode = c.Grade
			inner join
			calpads.Grade g
				on g.GradeCode = s.GradeLevel
			inner join
			calpads.Year y
				on y.YearCodeAbbr = s.AcYear
			inner join
			calpads.Term t
				on t.TermCode = c.CourseTerm
			inner join
			calpads.YearTerm yt
				on yt.YearCode = y.YearCode
				and yt.TermCode = t.TermCode
			inner join
			calpads.Course crs
				on crs.Code = c.CourseId
			inner join
			calpads.Department d
				on d.Code = crs.DepartmentCode
			left outer join
			mmap.English e
				on e.CourseCode = c.CourseId
			left outer join
			mmap.Mathematics ma
				on ma.CourseCode = c.CourseId
		WHERE
			s.CSISNum = @CSISNum
			and g.IsHS = 1
			and c.CreditEarned <= c.CreditAttempted
			and c.CreditAttempted != 99.99
			and c.CreditEarned != 99.99
			and d.Code in (14,18) -- english, mathematics
			and (
				e.ContentCode is not null
				or
				ma.ContentCode is not null
			)
	);