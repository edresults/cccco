USE calpass;

GO

IF (object_id('mmap.K12TranscriptGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION mmap.K12TranscriptGet;
	END;

GO

CREATE FUNCTION
	mmap.K12TranscriptGet
	(
		@StudentStateId char(10)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			IsHighSchoolGrade09         = max(IsHighSchoolGrade09),
			IsHighSchoolGrade10         = max(IsHighSchoolGrade10),
			IsHighSchoolGrade11         = max(IsHighSchoolGrade11),
			IsHighSchoolGrade12         = max(IsHighSchoolGrade12),
			CumulativeGradePointAverage = max(CumulativeGradePointAverage),
			English                     = max(English),
			PreAlgebra                  = max(PreAlgebra),
			AlgebraI                    = max(AlgebraI),
			Geometry                    = max(Geometry),
			AlgebraII                   = max(AlgebraII),
			Trigonometry                = max(Trigonometry),
			PreCalculus                 = max(PreCalculus),
			[Statistics]                = max([Statistics]),
			Calculus                    = max(Calculus)
		FROM
			(
				SELECT
					IsHighSchoolGrade09         = max(case when GradeCode = '09' then 1 else 0 end),
					IsHighSchoolGrade10         = max(case when GradeCode = '10' then 1 else 0 end),
					IsHighSchoolGrade11         = max(case when GradeCode = '11' then 1 else 0 end),
					IsHighSchoolGrade12         = max(case when GradeCode = '12' then 1 else 0 end),
					CumulativeGradePointAverage = max(case when DepartmentCode = 0 and IsLast = 1 then CumulativeGradePointAverage end),
					English                     = null,
					PreAlgebra                  = null,
					AlgebraI                    = null,
					Geometry                    = null,
					AlgebraII                   = null,
					Trigonometry                = null,
					PreCalculus                 = null,
					[Statistics]                = null,
					Calculus                    = null
				FROM
					mmap.K12Performance
				WHERE
					StudentStateId = @StudentStateId
				GROUP BY
					StudentStateId
				UNION
				SELECT
					IsHighSchoolGrade09         = null,
					IsHighSchoolGrade10         = null,
					IsHighSchoolGrade11         = null,
					IsHighSchoolGrade12         = null,
					CumulativeGradePointAverage = null,
					English                     = max(case when DepartmentCode = 14 and RecencySelector = 1 then MarkPoints end),
					PreAlgebra                  = max(case when ContentCode = 'PreAlgebra'                  then MarkPoints end),
					AlgebraI                    = max(case when ContentCode = 'AlgebraI'                    then MarkPoints end),
					Geometry                    = max(case when ContentCode = 'Geometry'                    then MarkPoints end),
					AlgebraII                   = max(case when ContentCode = 'AlgebraII'                   then MarkPoints end),
					Trigonometry                = max(case when ContentCode = 'Trigonometry'                then MarkPoints end),
					PreCalculus                 = max(case when ContentCode = 'PreCalculus'                 then MarkPoints end),
					[Statistics]                = max(case when ContentCode = 'Statistics'                  then MarkPoints end),
					Calculus                    = max(case when ContentCode = 'Calculus'                    then MarkPoints end)
				FROM
					mmap.K12CourseContent
				WHERE
					StudentStateId = @StudentStateId
					and ContentSelector = 1
				GROUP BY
					StudentStateId
			) a
		HAVING
			count(*) > 1
	);