USE calpass;

GO

IF (object_id('mmap.K12PlacementGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION mmap.K12PlacementGet;
	END;

GO

CREATE FUNCTION
	mmap.K12PlacementGet
	(
		@StudentStateId char(10)
	)
RETURNS
	TABLE
AS
	RETURN (
	SELECT
		EnglishY         = isnull(EnglishY, 0),
		EnglishA         = isnull(EnglishA, 0),
		EnglishB         = isnull(EnglishB, 0),
		EnglishC         = isnull(EnglishC, 0),
		PreAlgebra       = isnull(PreAlgebra, 0),
		AlgebraI         = isnull(AlgebraI, 0),
		AlgebraII        = isnull(AlgebraII, 0),
		MathGE           = isnull(MathGE, 0),
		[Statistics]     = isnull([Statistics], 0),
		CollegeAlgebra   = isnull(CollegeAlgebra, 0),
		Trigonometry     = isnull(Trigonometry, 0),
		PreCalculus      = isnull(PreCalculus, 0),
		CalculusI        = isnull(CalculusI, 0),
		ReadingM_UboundY = isnull(ReadingM_UboundY, 0),
		ReadingY_UboundY = isnull(ReadingY_UboundY, 0),
		ReadingA_UboundY = isnull(ReadingA_UboundY, 0),
		ReadingB_UboundY = isnull(ReadingB_UboundY, 0),
		ReadingC_UboundY = isnull(ReadingC_UboundY, 0),
		ReadingM_UboundA = isnull(ReadingM_UboundA, 0),
		ReadingA_UboundA = isnull(ReadingA_UboundA, 0),
		ReadingB_UboundA = isnull(ReadingB_UboundA, 0),
		ReadingC_UboundA = isnull(ReadingC_UboundA, 0),
		ReadingM_UboundB = isnull(ReadingM_UboundB, 0),
		ReadingB_UboundB = isnull(ReadingB_UboundB, 0),
		ReadingC_UboundB = isnull(ReadingC_UboundB, 0),
		EslY_UboundY     = isnull(EslY_UboundY, 0),
		EslA_UboundY     = isnull(EslA_UboundY, 0),
		EslB_UboundY     = isnull(EslB_UboundY, 0),
		EslA_UboundA     = isnull(EslA_UboundA, 0),
		EslB_UboundA     = isnull(EslB_UboundA, 0),
		EslC_UboundA     = isnull(EslC_UboundA, 0),
		EslB_UboundB     = isnull(EslB_UboundB, 0),
		EslC_UboundB     = isnull(EslC_UboundB, 0),
		EslD_UboundB     = isnull(EslD_UboundB, 0)
	FROM
		(
			SELECT
				EnglishY         = max(case when o.Name = 'EnglishY'         then 1 else 0 end),
				EnglishA         = max(case when o.Name = 'EnglishA'         then 1 else 0 end),
				EnglishB         = max(case when o.Name = 'EnglishB'         then 1 else 0 end),
				EnglishC         = max(case when o.Name = 'EnglishC'         then 1 else 0 end),
				PreAlgebra       = max(case when o.Name = 'PreAlgebra'       then 1 else 0 end),
				AlgebraI         = max(case when o.Name = 'AlgebraI'         then 1 else 0 end),
				AlgebraII        = max(case when o.Name = 'AlgebraII'        then 1 else 0 end),
				MathGE           = max(case when o.Name = 'MathGE'           then 1 else 0 end),
				[Statistics]     = max(case when o.Name = 'Statistics'       then 1 else 0 end),
				CollegeAlgebra   = max(case when o.Name = 'CollegeAlgebra'   then 1 else 0 end),
				Trigonometry     = max(case when o.Name = 'Trigonometry'     then 1 else 0 end),
				PreCalculus      = max(case when o.Name = 'PreCalculus'      then 1 else 0 end),
				CalculusI        = max(case when o.Name = 'Calculus'         then 1 else 0 end),
				ReadingM_UboundY = max(case when o.Name = 'ReadingM_UboundY' then 1 else 0 end),
				ReadingY_UboundY = max(case when o.Name = 'ReadingY_UboundY' then 1 else 0 end),
				ReadingA_UboundY = max(case when o.Name = 'ReadingA_UboundY' then 1 else 0 end),
				ReadingB_UboundY = max(case when o.Name = 'ReadingB_UboundY' then 1 else 0 end),
				ReadingC_UboundY = max(case when o.Name = 'ReadingC_UboundY' then 1 else 0 end),
				ReadingM_UboundA = max(case when o.Name = 'ReadingM_UboundA' then 1 else 0 end),
				ReadingA_UboundA = max(case when o.Name = 'ReadingA_UboundA' then 1 else 0 end),
				ReadingB_UboundA = max(case when o.Name = 'ReadingB_UboundA' then 1 else 0 end),
				ReadingC_UboundA = max(case when o.Name = 'ReadingC_UboundA' then 1 else 0 end),
				ReadingM_UboundB = max(case when o.Name = 'ReadingM_UboundB' then 1 else 0 end),
				ReadingB_UboundB = max(case when o.Name = 'ReadingB_UboundB' then 1 else 0 end),
				ReadingC_UboundB = max(case when o.Name = 'ReadingC_UboundB' then 1 else 0 end),
				EslY_UboundY     = max(case when o.Name = 'EslY_UboundY'     then 1 else 0 end),
				EslA_UboundY     = max(case when o.Name = 'EslA_UboundY'     then 1 else 0 end),
				EslB_UboundY     = max(case when o.Name = 'EslB_UboundY'     then 1 else 0 end),
				EslA_UboundA     = max(case when o.Name = 'EslA_UboundA'     then 1 else 0 end),
				EslB_UboundA     = max(case when o.Name = 'EslB_UboundA'     then 1 else 0 end),
				EslC_UboundA     = max(case when o.Name = 'EslC_UboundA'     then 1 else 0 end),
				EslB_UboundB     = max(case when o.Name = 'EslB_UboundB'     then 1 else 0 end),
				EslC_UboundB     = max(case when o.Name = 'EslC_UboundB'     then 1 else 0 end),
				EslD_UboundB     = max(case when o.Name = 'EslD_UboundB'     then 1 else 0 end)
			FROM
				(
					SELECT DISTINCT
						r.Name
					FROM
						mmap.RulesetGet(default) r
						cross apply
						mmap.K12TranscriptHashGet(@StudentStateId) th
					WHERE
						r.Entity = th.Entity
						and r.Attribute = th.Attribute
					GROUP BY
						r.Name,
						r.BranchId,
						r.LeafCount
					HAVING
						sum(case when th.Value >= r.Value then 1 else 0 end) = r.LeafCount
				) o
			HAVING
				count(*) > 1
		) a
		cross apply
		dbo.RandomAssignment(default) ra
	);