USE calpass;

GO

IF (object_id('mmap.RetrospectiveCourseContentGet') is not null)
    BEGIN
        DROP FUNCTION mmap.RetrospectiveCourseContentGet;
    END;

GO

CREATE FUNCTION
    mmap.RetrospectiveCourseContentGet
    (
        @InterSegmentKey binary(64)
    )
RETURNS
    TABLE
AS
    RETURN (
        SELECT
            DepartmentCode  = crs.DepartmentCode,
            GradeCode       = s.GradeLevel,
            SchoolCode      = s.School,
            YearTermCode    = yt.YearTermCode,
            TermCode        = c.CourseTerm,
            ContentCode     = cc.ContentCode,
            ContentRank     = cc.Rank,
            CourseCode      = c.CourseId,
            CourseUnits     = c.CreditAttempted,
            CourseTitle     = c.CourseTitle,
            CourseAGCode    = c.AGstatus,
            CourseLevelCode = c.CourseLevel,
            CourseTypeCode  = c.CourseType,
            SectionMark     = m.MarkCode,
            MarkPoints      = m.Points,
            MarkCategory    = m.Category,
            IsSuccess       = m.IsSuccess,
            IsPromoted      = m.IsPromoted,
            ContentSelector = 
                row_number() over(
                    partition by
                        crs.DepartmentCode,
                        cc.ContentCode
                    order by
                        -- Recency
                        y.Rank desc,
                        t.IntraTermOrdinal desc,
                        -- Rigor
                        cc.Rank desc,
                        c.CreditAttempted desc,
                        -- Performance
                        m.Rank asc
                ),
            GradeSelector   = 
                row_number() over(
                    partition by
                        -- Recency
                        crs.DepartmentCode,
                        s.GradeLevel
                    order by
                        y.Rank desc,
                        t.IntraTermOrdinal desc,
                        -- Rigor
                        cc.Rank desc,
                        c.CreditAttempted desc,
                        -- Performance
                        m.Rank asc
                ),
            RecencySelector = 
                row_number() over(
                    partition by
                        crs.DepartmentCode
                    order by
                        -- Recency
                        y.Rank desc,
                        t.IntraTermOrdinal desc,
                        -- Rigor
                        cc.Rank desc,
                        c.CreditAttempted desc,
                        -- Performance
                        m.Rank asc
                )
        FROM
            dbo.K12StudentProd s
            inner join
            dbo.K12CourseProd c on
                s.School       = c.School and
                s.LocStudentId = c.LocStudentId and
                s.AcYear       = c.AcYear
            inner join
            calpads.Mark m on
                m.MarkCode = c.Grade
            inner join
            calpads.Grade g on
                g.GradeCode = s.GradeLevel
            inner join
            calpads.Year y on
                y.YearCodeAbbr = s.AcYear
            inner join
            calpads.Term t on
                t.TermCode = c.CourseTerm
            inner join
            calpads.YearTerm yt on
                yt.YearCode = y.YearCode and
                yt.TermCode = t.TermCode
            inner join
            calpads.Course crs on
                crs.Code = c.CourseId
            inner join
            calpads.CourseContent cc on
                cc.CourseCode = crs.Code
        WHERE
            s.Derkey1          = @InterSegmentKey and 
            g.IsHS             = 1 and 
            c.CreditEarned    <= c.CreditAttempted and 
            c.CreditAttempted <> 99.99 and 
            c.CreditEarned    <> 99.99
    );