IF (object_id('Mmap.RetrospectivePerformanceGet') is not null)
    BEGIN
        DROP FUNCTION Mmap.RetrospectivePerformanceGet;
    END;

GO

CREATE FUNCTION
    Mmap.RetrospectivePerformanceGet
    (
        @InterSegmentKey binary(64)
    )
RETURNS
    TABLE
AS
    RETURN (
        SELECT
            DepartmentCode,
            GradeCode,
            QualityPoints                   = convert(decimal(9,3), QualityPoints),
            CreditAttempted                 = convert(decimal(9,3), CreditAttempted),
            GradePointAverage               = convert(decimal(9,3), isnull(QualityPoints / nullif(CreditAttempted, 0), 0)),
            GradePointAverageSans           = 
                convert(
                    decimal(9,3),
                    isnull(
                        (max(case when DepartmentCode = 0 then QualityPoints end) over(partition by GradeCode) - QualityPoints)
                        /
                        nullif(max(case when DepartmentCode = 0 then CreditAttempted end) over(partition by GradeCode) - CreditAttempted, 0),
                        0
                    )
                ),
            CumulativeQualityPoints         = convert(decimal(9,3), isnull(sum(QualityPoints)   over(partition by DepartmentCode order by GradeCode), 0)),
            CumulativeCreditAttempted       = convert(decimal(9,3), isnull(sum(CreditAttempted) over(partition by DepartmentCode order by GradeCode), 0)),
            CumulativeGradePointAverage     = convert(decimal(9,3), isnull(sum(QualityPoints)   over(partition by DepartmentCode order by GradeCode) / nullif(sum(CreditAttempted) over(partition by DepartmentCode order by GradeCode), 0), 0)),
            CumulativeGradePointAverageSans = 
                convert(
                    decimal(9,3),
                    isnull(
                        (sum(case when DepartmentCode = 0 then QualityPoints else 0 end) over(order by GradeCode) - sum(QualityPoints) over(partition by DepartmentCode order by GradeCode))
                        /
                        nullif(sum(case when DepartmentCode = 0 then CreditAttempted else 0 end) over(order by GradeCode) - sum(CreditAttempted) over(partition by DepartmentCode order by GradeCode), 0),
                        0
                    )
                ),
            FirstYearTermCode,
            LastYearTermCode,
            IsFirst = case when row_number() over(partition by DepartmentCode order by FirstYearTermCode ASC, convert(tinyint, GradeCode) ASC ) = 1 then 1 else 0 end,
            IsLast  = case when row_number() over(partition by DepartmentCode order by LastYearTermCode DESC, convert(tinyint, GradeCode) DESC) = 1 then 1 else 0 end
        FROM
            (
                SELECT
                    DepartmentCode          = isnull(cc.DepartmentCode, 0),
                    GradeCode               = g.GradeCode,
                    QualityPoints           = sum(c.CreditEarned * m.Points),
                    CreditAttempted         = sum(c.CreditAttempted),
                    FirstYearTermCode       = min(yt.YearTermCode),
                    LastYearTermCode        = max(yt.YearTermCode)
                FROM
                    dbo.K12StudentProd s
                    inner join
                    dbo.K12CourseProd c on
                        s.School       = c.School and
                        s.LocStudentId = c.LocStudentId and
                        s.AcYear       = c.AcYear
                    inner join
                    calpads.Mark m on
                        m.MarkCode = c.Grade
                    inner join
                    calpads.Grade g on
                        g.GradeCode = s.GradeLevel
                    inner join
                    calpads.Course cc on
                        cc.Code = c.CourseId
                    inner join
                    calpads.Year y on
                        y.YearCodeAbbr = s.AcYear
                    inner join
                    calpads.YearTerm yt on
                        yt.YearCode = y.YearCode and
                        yt.TermCode = c.CourseTerm
                WHERE
                    s.Derkey1          = @InterSegmentKey and
                    g.IsHS             = 1 and
                    m.IsGpa            = 1 and
                    c.CreditEarned    <= c.CreditAttempted and
                    c.CreditAttempted <> 99.99 and
                    c.CreditEarned    <> 99.99
                GROUP BY
                    s.Derkey1,
                    rollup(
                        g.GradeCode,
                        cc.DepartmentCode
                    )
                HAVING
                    (
                        (
                            cc.DepartmentCode is null and
                            grouping_id(cc.DepartmentCode) = 1
                        ) or
                        cc.DepartmentCode in (14, 18)
                    ) and
                    not (
                        grouping_id(g.GradeCode) = 1 and
                        grouping_id(cc.DepartmentCode) = 1
                    )
            ) dt
    );