USE calpass;

GO

IF (object_id('mmap.RulesetGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION mmap.RulesetGet;
	END;

GO

CREATE FUNCTION
	mmap.RulesetGet
	(
		@PlacementModelId int = null
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT
			Name      = c.Name,
			Entity    = pl.Entity,
			Attribute = pl.Attribute,
			Value     = pl.Value,
			BranchId  = pb.Id,
			LeafCount = pb.LeafCount
		FROM
			mmap.ProspectiveModel pm
			inner join
			mmap.ProspectiveCourse pc
				on pc.ProspectiveModelId = pm.Id
			inner join
			mmap.Course c
				on c.Id = pc.CourseId
			inner join
			mmap.ProspectiveBranch pb
				on pb.ProspectiveCourseId = pc.Id
			inner join
			mmap.ProspectiveLeaf pl
				on pl.ProspectiveBranchId = pb.Id
		WHERE
			(
				@PlacementModelId is null
				or
				pm.Id = @PlacementModelId
			)
	);