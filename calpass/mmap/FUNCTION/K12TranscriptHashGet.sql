USE calpass;

GO

IF (object_id('mmap.K12TranscriptHashGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION mmap.K12TranscriptHashGet;
	END;

GO

CREATE FUNCTION
	mmap.K12TranscriptHashGet
	(
		@StudentStateId char(10)
	)
RETURNS
	TABLE
AS
	RETURN (
		-- Performance Hash
		SELECT
			Entity    = 'Cgpa',
			Attribute = rp.GradeCode,
			Value     = rp.CumulativeGradePointAverage
		FROM
			mmap.K12Performance rp
		WHERE
			rp.StudentStateId = @StudentStateId
			and rp.DepartmentCode = 0
			and rp.IsLast = 1
		UNION
		-- Mathematics Hash
		SELECT
			Entity = 
				case
					when grouping(rcc.ContentCode) = 1 then 'Mathematics'
					when grouping(rcc.ContentCode) = 0 then rcc.ContentCode
				end,
			Attribute = 
				case
					when grouping(rcc.ContentCode) = 1 then 'PromotedRank'
					when grouping(rcc.ContentCode) = 0 then 'MarkPoints'
				end,
			Value = 
				case
					when grouping(rcc.ContentCode) = 1 then max(case when rcc.IsPromoted = 1 then rcc.ContentRank else 0 end)
					when grouping(rcc.ContentCode) = 0 then max(rcc.MarkPoints)
				end
		FROM
			mmap.K12CourseContent rcc
		WHERE
			rcc.StudentStateId = @StudentStateId
			and rcc.ContentSelector = 1
			and rcc.DepartmentCode = 18
		GROUP BY
			rcc.DepartmentCode,
			rollup(
				rcc.ContentCode
			)
		UNION
		-- English Hash
		SELECT
			Entity = 
				case
					when grouping(rcc.GradeCode) = 1 then 'English'
					when grouping(rcc.GradeCode) = 0 then 'English' + rcc.GradeCode
				end,
			Attribute = 
				case
					when grouping(rcc.GradeCode) = 1 then 'PromotedRank'
					when grouping(rcc.GradeCode) = 0 then 'MarkPoints'
				end,
			Value = 
				case
					when grouping(rcc.GradeCode) = 1 then max(case when rcc.IsPromoted = 1 then rcc.ContentRank end)
					when grouping(rcc.GradeCode) = 0 then max(rcc.MarkPoints)
				end
		FROM
			mmap.K12CourseContent rcc
		WHERE
			rcc.StudentStateId = @StudentStateId
			and rcc.GradeSelector = 1
			and rcc.DepartmentCode = 14
		GROUP BY
			rcc.DepartmentCode,
			rollup(
				rcc.GradeCode
			)
	);