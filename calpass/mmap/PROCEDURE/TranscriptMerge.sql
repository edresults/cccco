USE calpass;

GO

IF (object_id('mmap.TranscriptMerge', 'P') is not null)
	BEGIN
		DROP PROCEDURE mmap.TranscriptMerge;
	END;

GO

CREATE PROCEDURE
	mmap.TranscriptMerge
	(
		@InterSegment dbo.InterSegment READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Message nvarchar(2048),
	@ErrorCode int = 70099,
	@ErrorSeverity int = 1,
	@True bit = 1;

BEGIN
	BEGIN TRY
		-- Remove Collisions
		DELETE
			t
		FROM
			mmap.Transcript t
			inner join
			dbo.Student s
				on s.InterSegmentKey = t.InterSegmentKey
		WHERE
			s.IsHighSchoolCollision = 1;
		-- Merge new students
		MERGE
			mmap.Transcript
		WITH
			(
				TABLOCKX
			) t
		USING
			(
				SELECT
					InterSegmentKey,
					IsHighSchoolGrade09,
					IsHighSchoolGrade10,
					IsHighSchoolGrade11,
					IsHighSchoolGrade12,
					CumulativeGradePointAverage,
					English,
					PreAlgebra,
					AlgebraI,
					Geometry,
					AlgebraII,
					Trigonometry,
					PreCalculus,
					[Statistics],
					Calculus,
					IsEscrow = @True
				FROM
					@InterSegment i
					cross apply
					mmap.TranscriptGet(i.InterSegmentKey) stu
			) s
		ON
			(
				s.InterSegmentKey = t.InterSegmentKey
			)
		WHEN MATCHED THEN
			UPDATE SET
				t.InterSegmentKey             = s.InterSegmentKey,
				t.IsHighSchoolGrade09         = s.IsHighSchoolGrade09,
				t.IsHighSchoolGrade10         = s.IsHighSchoolGrade10,
				t.IsHighSchoolGrade11         = s.IsHighSchoolGrade11,
				t.IsHighSchoolGrade12         = s.IsHighSchoolGrade12,
				t.CumulativeGradePointAverage = s.CumulativeGradePointAverage,
				t.English                     = s.English,
				t.PreAlgebra                  = s.PreAlgebra,
				t.AlgebraI                    = s.AlgebraI,
				t.Geometry                    = s.Geometry,
				t.AlgebraII                   = s.AlgebraII,
				t.Trigonometry                = s.Trigonometry,
				t.PreCalculus                 = s.PreCalculus,
				t.[Statistics]                = s.[Statistics],
				t.Calculus                    = s.Calculus,
				t.IsEscrow                    = s.IsEscrow
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					InterSegmentKey,
					IsHighSchoolGrade09,
					IsHighSchoolGrade10,
					IsHighSchoolGrade11,
					IsHighSchoolGrade12,
					CumulativeGradePointAverage,
					English,
					PreAlgebra,
					AlgebraI,
					Geometry,
					AlgebraII,
					Trigonometry,
					PreCalculus,
					[Statistics],
					Calculus,
					IsEscrow
				)
			VALUES
				(
					s.InterSegmentKey,
					s.IsHighSchoolGrade09,
					s.IsHighSchoolGrade10,
					s.IsHighSchoolGrade11,
					s.IsHighSchoolGrade12,
					s.CumulativeGradePointAverage,
					s.English,
					s.PreAlgebra,
					s.AlgebraI,
					s.Geometry,
					s.AlgebraII,
					s.Trigonometry,
					s.PreCalculus,
					s.[Statistics],
					s.Calculus,
					s.IsEscrow
				);
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message =
			nchar(9) + N'Error Procedure: ' + upper(object_name(@@PROCID)) + char(13) + char(10) +
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		-- put error message
		THROW @ErrorCode, @Message, @ErrorSeverity;
	END CATCH;
END;