USE calpass;

GO

IF (object_id('mmap.ProspectiveRulesetSelect') is not null)
	BEGIN
		DROP PROCEDURE mmap.ProspectiveRulesetSelect;
	END;

GO

CREATE PROCEDURE
	mmap.ProspectiveRulesetSelect
	(
		@OrganizationId int
	)
AS

	SET NOCOUNT ON;

DECLARE
	@RPGroup int = 175721;

BEGIN

	WITH
		Ruleset
		(
			Entity,
			Attribute,
			Value,
			BranchId,
			Name
		)
	AS
		(
			SELECT
				pl.Entity,
				pl.Attribute,
				pl.Value,
				pb.Id,
				c.Name
			FROM
				mmap.ProspectiveModel pm
				inner join
				mmap.ProspectiveCourse pc
					on pc.ProspectiveModelId = pm.Id
				inner join
				mmap.Course c
					on c.Id = pc.CourseId
				inner join
				mmap.ProspectiveBranch pb
					on pb.ProspectiveCourseId = pc.Id
				inner join
				mmap.ProspectiveLeaf pl
					on pl.ProspectiveBranchId = pb.Id
		)
	SELECT
		Entity,
		Attribute,
		Value,
		BranchId,
		CourseCode,
		LeafCount = count(*) over(partition by BranchId)
	FROM
		RuleSet rs
	WHERE
		not exists (
			SELECT
				1
			FROM
				Remedial r
			WHERE
				r.BranchId = rs.BranchId
		)
		-- For the remaining ESL and Reading Branches, remove the leaves that required the user-input level
		-- because user-input leaves are not part of the summary transcript hash.
		and not exists (
			SELECT
				1
			FROM
				mmap.RemedialUpperBound rub
			WHERE
				rub.OrganizationId = @OrganizationId
				and rub.Entity = rs.Entity
				and rub.Attribute = rs.Attribute
		);
	
END;