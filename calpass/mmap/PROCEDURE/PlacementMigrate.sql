USE calpass;

GO

IF (object_id('mmap.PlacementMigrate') is not null)
	BEGIN
		DROP PROCEDURE mmap.PlacementMigrate;
	END;

GO

CREATE PROCEDURE
	mmap.PlacementMigrate
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Placement mmap.Placement,
	@False     bit = 0,
	@True      bit = 1;

BEGIN
	INSERT INTO
		@Placement
		(
			InterSegmentKey,
			EnglishY,
			EnglishA,
			EnglishB,
			EnglishC,
			PreAlgebra,
			AlgebraI,
			AlgebraII,
			MathGE,
			[Statistics],
			CollegeAlgebra,
			Trigonometry,
			PreCalculus,
			CalculusI,
			ReadingM_UboundY,
			ReadingY_UboundY,
			ReadingA_UboundY,
			ReadingB_UboundY,
			ReadingC_UboundY,
			ReadingM_UboundA,
			ReadingA_UboundA,
			ReadingB_UboundA,
			ReadingC_UboundA,
			ReadingM_UboundB,
			ReadingB_UboundB,
			ReadingC_UboundB,
			EslY_UboundY,
			EslA_UboundY,
			EslB_UboundY,
			EslA_UboundA,
			EslB_UboundA,
			EslC_UboundA,
			EslB_UboundB,
			EslC_UboundB,
			EslD_UboundB,
			IsTreatment
		)
	SELECT
		InterSegmentKey,
		EnglishY,
		EnglishA,
		EnglishB,
		EnglishC,
		PreAlgebra,
		AlgebraI,
		AlgebraII,
		MathGE,
		[Statistics],
		CollegeAlgebra,
		Trigonometry,
		PreCalculus,
		CalculusI,
		ReadingM_UboundY,
		ReadingY_UboundY,
		ReadingA_UboundY,
		ReadingB_UboundY,
		ReadingC_UboundY,
		ReadingM_UboundA,
		ReadingA_UboundA,
		ReadingB_UboundA,
		ReadingC_UboundA,
		ReadingM_UboundB,
		ReadingB_UboundB,
		ReadingC_UboundB,
		EslY_UboundY,
		EslA_UboundY,
		EslB_UboundY,
		EslA_UboundA,
		EslB_UboundA,
		EslC_UboundA,
		EslB_UboundB,
		EslC_UboundB,
		EslD_UboundB,
		IsTreatment
	FROM
		mmap.Placement
	WHERE
		IsEscrow = @True;

	UPDATE
		a
	SET
		a.EnglishY         = p.EnglishY,
		a.EnglishA         = p.EnglishA,
		a.EnglishB         = p.EnglishB,
		a.EnglishC         = p.EnglishC,
		a.PreAlgebra       = p.PreAlgebra,
		a.AlgebraI         = p.AlgebraI,
		a.AlgebraII        = p.AlgebraII,
		a.MathGE           = p.MathGE,
		a.[Statistics]     = p.[Statistics],
		a.CollegeAlgebra   = p.CollegeAlgebra,
		a.Trigonometry     = p.Trigonometry,
		a.PreCalculus      = p.PreCalculus,
		a.CalculusI        = p.CalculusI,
		a.ReadingM_UboundY = p.ReadingM_UboundY,
		a.ReadingY_UboundY = p.ReadingY_UboundY,
		a.ReadingA_UboundY = p.ReadingA_UboundY,
		a.ReadingB_UboundY = p.ReadingB_UboundY,
		a.ReadingC_UboundY = p.ReadingC_UboundY,
		a.ReadingM_UboundA = p.ReadingM_UboundA,
		a.ReadingA_UboundA = p.ReadingA_UboundA,
		a.ReadingB_UboundA = p.ReadingB_UboundA,
		a.ReadingC_UboundA = p.ReadingC_UboundA,
		a.ReadingM_UboundB = p.ReadingM_UboundB,
		a.ReadingB_UboundB = p.ReadingB_UboundB,
		a.ReadingC_UboundB = p.ReadingC_UboundB,
		a.EslY_UboundY     = p.EslY_UboundY,
		a.EslA_UboundY     = p.EslA_UboundY,
		a.EslB_UboundY     = p.EslB_UboundY,
		a.EslA_UboundA     = p.EslA_UboundA,
		a.EslB_UboundA     = p.EslB_UboundA,
		a.EslC_UboundA     = p.EslC_UboundA,
		a.EslB_UboundB     = p.EslB_UboundB,
		a.EslC_UboundB     = p.EslC_UboundB,
		a.EslD_UboundB     = p.EslD_UboundB,
		a.IsTreatment      = p.IsTreatment
	FROM
		CPPDELSQLP01.CPP_API.dbo.Placement a
		inner join
		@Placement p
			on p.InterSegmentKey = a.InterSegmentKey;

	INSERT
		CPPDELSQLP01.CPP_API.dbo.Placement
		(
			InterSegmentKey,
			EnglishY,
			EnglishA,
			EnglishB,
			EnglishC,
			PreAlgebra,
			AlgebraI,
			AlgebraII,
			MathGE,
			[Statistics],
			CollegeAlgebra,
			Trigonometry,
			PreCalculus,
			CalculusI,
			ReadingM_UboundY,
			ReadingY_UboundY,
			ReadingA_UboundY,
			ReadingB_UboundY,
			ReadingC_UboundY,
			ReadingM_UboundA,
			ReadingA_UboundA,
			ReadingB_UboundA,
			ReadingC_UboundA,
			ReadingM_UboundB,
			ReadingB_UboundB,
			ReadingC_UboundB,
			EslY_UboundY,
			EslA_UboundY,
			EslB_UboundY,
			EslA_UboundA,
			EslB_UboundA,
			EslC_UboundA,
			EslB_UboundB,
			EslC_UboundB,
			EslD_UboundB,
			IsTreatment
		)
	SELECT
		InterSegmentKey,
		EnglishY,
		EnglishA,
		EnglishB,
		EnglishC,
		PreAlgebra,
		AlgebraI,
		AlgebraII,
		MathGE,
		[Statistics],
		CollegeAlgebra,
		Trigonometry,
		PreCalculus,
		CalculusI,
		ReadingM_UboundY,
		ReadingY_UboundY,
		ReadingA_UboundY,
		ReadingB_UboundY,
		ReadingC_UboundY,
		ReadingM_UboundA,
		ReadingA_UboundA,
		ReadingB_UboundA,
		ReadingC_UboundA,
		ReadingM_UboundB,
		ReadingB_UboundB,
		ReadingC_UboundB,
		EslY_UboundY,
		EslA_UboundY,
		EslB_UboundY,
		EslA_UboundA,
		EslB_UboundA,
		EslC_UboundA,
		EslB_UboundB,
		EslC_UboundB,
		EslD_UboundB,
		IsTreatment
	FROM
		@Placement p
	WHERE
		not exists (
			SELECT
				0
			FROM
				CPPDELSQLP01.CPP_API.dbo.Placement a
			WHERE
				a.InterSegmentKey = p.InterSegmentKey
		);

	UPDATE
		a
	SET
		a.EnglishY         = p.EnglishY,
		a.EnglishA         = p.EnglishA,
		a.EnglishB         = p.EnglishB,
		a.EnglishC         = p.EnglishC,
		a.PreAlgebra       = p.PreAlgebra,
		a.AlgebraI         = p.AlgebraI,
		a.AlgebraII        = p.AlgebraII,
		a.MathGE           = p.MathGE,
		a.[Statistics]     = p.[Statistics],
		a.CollegeAlgebra   = p.CollegeAlgebra,
		a.Trigonometry     = p.Trigonometry,
		a.PreCalculus      = p.PreCalculus,
		a.CalculusI        = p.CalculusI,
		a.ReadingM_UboundY = p.ReadingM_UboundY,
		a.ReadingY_UboundY = p.ReadingY_UboundY,
		a.ReadingA_UboundY = p.ReadingA_UboundY,
		a.ReadingB_UboundY = p.ReadingB_UboundY,
		a.ReadingC_UboundY = p.ReadingC_UboundY,
		a.ReadingM_UboundA = p.ReadingM_UboundA,
		a.ReadingA_UboundA = p.ReadingA_UboundA,
		a.ReadingB_UboundA = p.ReadingB_UboundA,
		a.ReadingC_UboundA = p.ReadingC_UboundA,
		a.ReadingM_UboundB = p.ReadingM_UboundB,
		a.ReadingB_UboundB = p.ReadingB_UboundB,
		a.ReadingC_UboundB = p.ReadingC_UboundB,
		a.EslY_UboundY     = p.EslY_UboundY,
		a.EslA_UboundY     = p.EslA_UboundY,
		a.EslB_UboundY     = p.EslB_UboundY,
		a.EslA_UboundA     = p.EslA_UboundA,
		a.EslB_UboundA     = p.EslB_UboundA,
		a.EslC_UboundA     = p.EslC_UboundA,
		a.EslB_UboundB     = p.EslB_UboundB,
		a.EslC_UboundB     = p.EslC_UboundB,
		a.EslD_UboundB     = p.EslD_UboundB
	FROM
		CPPDELSQLP01.CPP_API.mmap.Placement a
		inner join
		@Placement p
			on p.InterSegmentKey = a.StudentId;

	INSERT
		CPPDELSQLP01.CPP_API.mmap.Placement
		(
			StudentId,
			DataSource,
			EnglishY,
			EnglishA,
			EnglishB,
			EnglishC,
			PreAlgebra,
			AlgebraI,
			AlgebraII,
			MathGE,
			[Statistics],
			CollegeAlgebra,
			Trigonometry,
			PreCalculus,
			CalculusI,
			ReadingM_UboundY,
			ReadingY_UboundY,
			ReadingA_UboundY,
			ReadingB_UboundY,
			ReadingC_UboundY,
			ReadingM_UboundA,
			ReadingA_UboundA,
			ReadingB_UboundA,
			ReadingC_UboundA,
			ReadingM_UboundB,
			ReadingB_UboundB,
			ReadingC_UboundB,
			EslY_UboundY,
			EslA_UboundY,
			EslB_UboundY,
			EslA_UboundA,
			EslB_UboundA,
			EslC_UboundA,
			EslB_UboundB,
			EslC_UboundB,
			EslD_UboundB
		)
	SELECT
		InterSegmentKey,
		DataSource = 2,
		EnglishY,
		EnglishA,
		EnglishB,
		EnglishC,
		PreAlgebra,
		AlgebraI,
		AlgebraII,
		MathGE,
		[Statistics],
		CollegeAlgebra,
		Trigonometry,
		PreCalculus,
		CalculusI,
		ReadingM_UboundY,
		ReadingY_UboundY,
		ReadingA_UboundY,
		ReadingB_UboundY,
		ReadingC_UboundY,
		ReadingM_UboundA,
		ReadingA_UboundA,
		ReadingB_UboundA,
		ReadingC_UboundA,
		ReadingM_UboundB,
		ReadingB_UboundB,
		ReadingC_UboundB,
		EslY_UboundY,
		EslA_UboundY,
		EslB_UboundY,
		EslA_UboundA,
		EslB_UboundA,
		EslC_UboundA,
		EslB_UboundB,
		EslC_UboundB,
		EslD_UboundB
	FROM
		@Placement p
	WHERE
		not exists (
			SELECT
				0
			FROM
				CPPDELSQLP01.CPP_API.mmap.Placement a
			WHERE
				a.StudentId = p.InterSegmentKey
		);

	UPDATE
		p
	SET
		IsEscrow = @False
	FROM
		mmap.Placement p
		inner join
		@Placement pp
			on pp.InterSegmentKey = p.InterSegmentKey;
END;