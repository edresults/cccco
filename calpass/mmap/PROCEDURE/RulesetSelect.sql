USE calpass;

GO

IF (object_id('mmap.RulesetSelect') is not null)
	BEGIN
		DROP PROCEDURE mmap.RulesetSelect;
	END;

GO

CREATE PROCEDURE
	mmap.RulesetSelect
	(
		@OrganizationId int
	)
AS

	SET NOCOUNT ON;

DECLARE
	@RPGroup int = 175721;

BEGIN

	WITH
		Ruleset
		(
			Entity,
			Attribute,
			Value,
			BranchId,
			CourseCode
		)
	AS
		(
			SELECT
				l.Entity,
				l.Attribute,
				l.Value,
				l.BranchId,
				t.CourseCode
			FROM
				mmap.Model m
				inner join
				mmap.Tree t
					on t.ModelId = m.ModelId
				inner join
				mmap.Branch b
					on b.TreeId = t.TreeId
				inner join
				mmap.Leaf l
					on l.BranchId = b.BranchId
			WHERE
				m.OrganizationId = @RPGroup
		),
		Remedial
		(
			BranchId
		)
	AS
		(
			-- For Esl and Reading only, get all leaves of a branch for which a leaf in the branch 
			-- that requies the user-input remedial level condition and is not met
			SELECT DISTINCT
				BranchId
			FROM
				Ruleset rs
			WHERE
				rs.Entity in ('Esl', 'Reading')
				and not exists (
					SELECT
						1
					FROM
						mmap.RemedialUpperBound rub
					WHERE
						rub.OrganizationId = @OrganizationId
						and rub.Entity = rs.Entity
						and rub.Attribute = rs.Attribute
						and rub.Value >= rs.Value
				)
		)
	SELECT
		Entity,
		Attribute,
		Value,
		BranchId,
		CourseCode,
		LeafCount = count(*) over(partition by BranchId)
	FROM
		RuleSet rs
	WHERE
		not exists (
			SELECT
				1
			FROM
				Remedial r
			WHERE
				r.BranchId = rs.BranchId
		)
		-- For the remaining ESL and Reading Branches, remove the leaves that required the user-input level
		-- because user-input leaves are not part of the summary transcript hash.
		and not exists (
			SELECT
				1
			FROM
				mmap.RemedialUpperBound rub
			WHERE
				rub.OrganizationId = @OrganizationId
				and rub.Entity = rs.Entity
				and rub.Attribute = rs.Attribute
		);
	
END;