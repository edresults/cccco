CREATE PROCEDURE
    mmap.CCTranscriptProcess
AS

BEGIN
    DROP TABLE IF EXISTS #Colleges;

    TRUNCATE TABLE Mmap.CCTranscript;

    CREATE TABLE
        #Colleges
        (
            Iterator    TINYINT IDENTITY,
            CollegeCode CHAR(3) NOT NULL
        );

    SET NOCOUNT ON;

    INSERT
        #Colleges
        (
            CollegeCode
        )
    SELECT
        CollegeCode
    FROM
        comis.College
    ORDER BY
        CollegeCode;

    DECLARE
        @Severity     TINYINT = 0,
        @State        TINYINT = 1,
        @Iterator     TINYINT = 1,
        @UBound       INTEGER = 0,
        @CollegeCode  CHAR(3) = '';

    SELECT
        @UBound = COUNT(*)
    FROM
        #Colleges;

    WHILE (@Iterator <= @UBound)
    BEGIN

        SELECT
            @CollegeCode = CollegeCode
        FROM
            #Colleges
        WHERE
            Iterator = @Iterator;

        RAISERROR(@CollegeCode, @Severity, @State) WITH NOWAIT;

        INSERT INTO
            Mmap.CCTranscript
            (
                CollegeCode,
                StudentId,
                YearTermCode,
                CourseId,
                CourseControlNumber,
                CourseSectionId,
                CourseTopCode,
                CourseTitle,
                CourseMarkLetter,
                CourseMarkPoints,
                CourseLevelCode,
                CourseCreditCode,
                CourseUnitsTry,
                CourseUnitsGet,
                InterSegmentKey
            )
        SELECT
            CollegeCode         = cb.college_id,
            StudentId           = sx.student_id,
            YearTermCode        = CONVERT(INTEGER, t.YearTermCode),
            CourseId            = cb.course_id,
            CourseControlNumber = cb.control_number,
            CourseSectionId     = sx.section_id,
            CourseTopCode       = cb.top_code,
            CourseTitle         = cb.title,
            CourseMarkLetter    = sx.grade,
            CourseMarkPoints    = m.Points,
            CourseLevelCode     = cb.prior_to_college,
            CourseCreditCode    = cb.credit_status,
            CourseUnitsTry      = case when sx.units_attempted in (88.88, 99.99) then 0 else sx.units_attempted end,
            CourseUnitsGet      = case when sx.units           in (88.88, 99.99) then 0 else sx.units           end,
            InterSegmentKey     = id.InterSegmentKey
        FROM
            comis.CBCRSINV cb
            inner join
            comis.SXENRLM SX
                on  sx.college_id     = cb.college_id
                and sx.term_id        = cb.term_id
                and sx.course_id      = cb.course_id
                and sx.control_number = cb.control_number
            inner join
            comis.STUDNTID id
                on  id.college_id = sx.college_id
                and id.student_id = sx.student_id
            inner join
            comis.Term t
                on t.TermCode = cb.term_id
            inner join
            comis.Mark m
                on m.MarkCode = sx.grade
            inner join
            comis.Student s
                on s.InterSegmentKey = id.InterSegmentKey
        WHERE
            cb.college_id = @CollegeCode
            and cb.prior_to_college is not null
            and id.InterSegmentKey is not null
            and s.IsCollision = 0
            and (
                exists (
                    SELECT
                        1
                    FROM
                        Mmap.Programs p
                    WHERE
                        p.Code = cb.top_code
                )
                or exists (
                    SELECT
                        1
                    FROM
                        Mmap.NonMath n
                    WHERE
                        n.ControlNumber = cb.control_number
                )
            );

        SET @Iterator += 1;
    END;

    UPDATE
        t
    SET
        t.Selector     = s.Selector,
        t.SelectorFL   = s.SelectorFL,
        t.SelectorCN   = s.SelectorCN,
        t.SelectorCNFL = s.SelectorCNFL
    FROM
        Mmap.CCTranscript t
        inner join
        (
            SELECT
                t.CollegeCode,
                t.StudentId,
                t.YearTermCode,
                t.CourseId,
                t.CourseControlNumber,
                t.CourseSectionId,
                Selector = 
                    row_number() over(
                    partition by
                        t.InterSegmentKey,
                        case when t.CourseTopCode not in ('150100','152000','493087','493084','493085','493086','493100','493090') then '170100' else t.CourseTopCode end,
                        c.Type,
                        t.CourseLevelCode
                    order by
                        t.YearTermCode asc,
                        t.CourseUnitsTry desc,
                        t.CourseMarkPoints desc
                    ),
                SelectorFL = 
                    row_number() over(
                    partition by
                        t.InterSegmentKey,
                        case when t.CourseTopCode not in ('150100','152000','493087','493084','493085','493086','493100','493090') then '170100' else t.CourseTopCode end,
                        c.Type
                    order by
                        t.YearTermCode asc,
                        l.Ordinal asc,
                        t.CourseUnitsTry desc,
                        t.CourseMarkPoints desc
                    ),
                SelectorCN = 
                    row_number() over(
                    partition by
                        t.InterSegmentKey,
                        ISNULL(p.Content, 'MATH'),
                        t.CourseLevelCode
                    order by
                        t.YearTermCode asc,
                        ISNULL(p.Rank, 0) asc,
                        t.CourseUnitsTry desc,
                        t.CourseMarkPoints desc
                    ),
                SelectorCNFL = 
                    row_number() over(
                    partition by
                        t.InterSegmentKey,
                        ISNULL(p.Content, 'MATH')
                    order by
                        t.YearTermCode asc,
                        ISNULL(p.Rank, 0) asc,
                        l.Ordinal asc,
                        t.CourseUnitsTry desc,
                        t.CourseMarkPoints desc
                    )
            FROM
                Mmap.CCTranscript t
                inner join
                Mmap.Credits c
                    on c.Code = t.CourseCreditCode
                inner join
                Mmap.Levels l
                    on l.Code = t.CourseLevelCode
                left outer join
                Mmap.Programs p
                    on p.Code = t.CourseTopCode
        ) s
            on  s.CollegeCode         = t.CollegeCode
            and s.StudentId           = t.StudentId
            and s.YearTermCode        = t.YearTermCode
            and s.CourseId            = t.CourseId
            and s.CourseControlNumber = t.CourseControlNumber
            and s.CourseSectionId     = t.CourseSectionId;
END;