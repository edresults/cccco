USE calpass;

GO

IF (object_id('mmap.PlacementMerge') is not null)
	BEGIN
		DROP PROCEDURE mmap.PlacementMerge;
	END;

GO

CREATE PROCEDURE
	mmap.PlacementMerge
	(
		@InterSegment dbo.InterSegment READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Message nvarchar(2048),
	@ErrorCode int = 70099,
	@ErrorSeverity int = 1,
	@True bit = 1;

BEGIN
	BEGIN TRY
		-- Remove Collisions
		DELETE
			p
		FROM
			mmap.Placement p
			inner join
			dbo.Student s
				on s.InterSegmentKey = p.InterSegmentKey
		WHERE
			s.IsHighSchoolCollision = 1;
		-- Merge new students
		MERGE
			mmap.Placement
		WITH
			(
				TABLOCKX
			) t
		USING
			(
				SELECT
					i.InterSegmentKey,
					p.EnglishY,
					p.EnglishA,
					p.EnglishB,
					p.EnglishC,
					p.PreAlgebra,
					p.AlgebraI,
					p.AlgebraII,
					p.MathGE,
					p.[Statistics],
					p.CollegeAlgebra,
					p.Trigonometry,
					p.PreCalculus,
					p.CalculusI,
					p.ReadingM_UboundY,
					p.ReadingY_UboundY,
					p.ReadingA_UboundY,
					p.ReadingB_UboundY,
					p.ReadingC_UboundY,
					p.ReadingM_UboundA,
					p.ReadingA_UboundA,
					p.ReadingB_UboundA,
					p.ReadingC_UboundA,
					p.ReadingM_UboundB,
					p.ReadingB_UboundB,
					p.ReadingC_UboundB,
					p.EslY_UboundY,
					p.EslA_UboundY,
					p.EslB_UboundY,
					p.EslA_UboundA,
					p.EslB_UboundA,
					p.EslC_UboundA,
					p.EslB_UboundB,
					p.EslC_UboundB,
					p.EslD_UboundB,
					p.IsTreatment
				FROM
					@InterSegment i
					inner join
					dbo.Student s
						on s.InterSegmentKey = i.InterSegmentKey
					cross apply
					Mmap.PlacementGet(i.InterSegmentKey) p
				WHERE
					s.IsHighSchoolSection = 1
					and s.IsHighSchoolCollision = 0
			) s
		ON
			s.InterSegmentKey = t.InterSegmentKey
		WHEN MATCHED THEN
			UPDATE SET
				t.EnglishY         = s.EnglishY,
				t.EnglishA         = s.EnglishA,
				t.EnglishB         = s.EnglishB,
				t.EnglishC         = s.EnglishC,
				t.PreAlgebra       = s.PreAlgebra,
				t.AlgebraI         = s.AlgebraI,
				t.AlgebraII        = s.AlgebraII,
				t.MathGE           = s.MathGE,
				t.[Statistics]     = s.[Statistics],
				t.CollegeAlgebra   = s.CollegeAlgebra,
				t.Trigonometry     = s.Trigonometry,
				t.PreCalculus      = s.PreCalculus,
				t.CalculusI        = s.CalculusI,
				t.ReadingM_UboundY = s.ReadingM_UboundY,
				t.ReadingY_UboundY = s.ReadingY_UboundY,
				t.ReadingA_UboundY = s.ReadingA_UboundY,
				t.ReadingB_UboundY = s.ReadingB_UboundY,
				t.ReadingC_UboundY = s.ReadingC_UboundY,
				t.ReadingM_UboundA = s.ReadingM_UboundA,
				t.ReadingA_UboundA = s.ReadingA_UboundA,
				t.ReadingB_UboundA = s.ReadingB_UboundA,
				t.ReadingC_UboundA = s.ReadingC_UboundA,
				t.ReadingM_UboundB = s.ReadingM_UboundB,
				t.ReadingB_UboundB = s.ReadingB_UboundB,
				t.ReadingC_UboundB = s.ReadingC_UboundB,
				t.EslY_UboundY     = s.EslY_UboundY,
				t.EslA_UboundY     = s.EslA_UboundY,
				t.EslB_UboundY     = s.EslB_UboundY,
				t.EslA_UboundA     = s.EslA_UboundA,
				t.EslB_UboundA     = s.EslB_UboundA,
				t.EslC_UboundA     = s.EslC_UboundA,
				t.EslB_UboundB     = s.EslB_UboundB,
				t.EslC_UboundB     = s.EslC_UboundB,
				t.EslD_UboundB     = s.EslD_UboundB,
				t.IsTreatment      = s.IsTreatment,
				t.IsEscrow         = @True
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					InterSegmentKey,
					EnglishY,
					EnglishA,
					EnglishB,
					EnglishC,
					PreAlgebra,
					AlgebraI,
					AlgebraII,
					MathGE,
					[Statistics],
					CollegeAlgebra,
					Trigonometry,
					PreCalculus,
					CalculusI,
					ReadingM_UboundY,
					ReadingY_UboundY,
					ReadingA_UboundY,
					ReadingB_UboundY,
					ReadingC_UboundY,
					ReadingM_UboundA,
					ReadingA_UboundA,
					ReadingB_UboundA,
					ReadingC_UboundA,
					ReadingM_UboundB,
					ReadingB_UboundB,
					ReadingC_UboundB,
					EslY_UboundY,
					EslA_UboundY,
					EslB_UboundY,
					EslA_UboundA,
					EslB_UboundA,
					EslC_UboundA,
					EslB_UboundB,
					EslC_UboundB,
					EslD_UboundB,
					IsTreatment,
					IsEscrow
				)
			VALUES
				(
					s.InterSegmentKey,
					s.EnglishY,
					s.EnglishA,
					s.EnglishB,
					s.EnglishC,
					s.PreAlgebra,
					s.AlgebraI,
					s.AlgebraII,
					s.MathGE,
					s.[Statistics],
					s.CollegeAlgebra,
					s.Trigonometry,
					s.PreCalculus,
					s.CalculusI,
					s.ReadingM_UboundY,
					s.ReadingY_UboundY,
					s.ReadingA_UboundY,
					s.ReadingB_UboundY,
					s.ReadingC_UboundY,
					s.ReadingM_UboundA,
					s.ReadingA_UboundA,
					s.ReadingB_UboundA,
					s.ReadingC_UboundA,
					s.ReadingM_UboundB,
					s.ReadingB_UboundB,
					s.ReadingC_UboundB,
					s.EslY_UboundY,
					s.EslA_UboundY,
					s.EslB_UboundY,
					s.EslA_UboundA,
					s.EslB_UboundA,
					s.EslC_UboundA,
					s.EslB_UboundB,
					s.EslC_UboundB,
					s.EslD_UboundB,
					s.IsTreatment,
					@True
				);
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message =
			nchar(9) + N'Error Procedure: ' + upper(object_name(@@PROCID)) + char(13) + char(10) +
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		-- put error message
		THROW @ErrorCode, @Message, @ErrorSeverity;
	END CATCH;
END;