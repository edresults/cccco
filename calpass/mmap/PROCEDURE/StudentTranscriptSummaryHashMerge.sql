USE calpass;

GO

IF (object_id('mmap.StudentTranscriptSummaryHashMerge') is not null)
	BEGIN
		DROP PROCEDURE mmap.StudentTranscriptSummaryHashMerge;
	END;

GO

CREATE PROCEDURE
	mmap.StudentTranscriptSummaryHashMerge
	(
		@StudentTranscriptSummaryHash mmap.StudentTranscriptSummaryHash READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

BEGIN
	MERGE
		mmap.StudentTranscriptSummaryHash t
	USING
		@StudentTranscriptSummaryHash s
	ON
		t.InterSegmentKey = s.InterSegmentKey
		and t.Entity = s.Entity
		and t.Attribute = s.Attribute
	WHEN MATCHED THEN
		UPDATE SET
			t.Value = s.Value
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				InterSegmentKey,
				Entity,
				Attribute,
				Value
			)
		VALUES
			(
				s.InterSegmentKey,
				s.Entity,
				s.Attribute,
				s.Value
			);
END;