USE calpass;

GO

IF (object_id('mmap.K12PlacementMigrate') is not null)
	BEGIN
		DROP PROCEDURE mmap.K12PlacementMigrate;
	END;

GO

CREATE PROCEDURE
	mmap.K12PlacementMigrate
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@K12Placement mmap.K12Placement,
	@False        bit = 0,
	@True         bit = 1;

BEGIN
	INSERT INTO
		@K12Placement
		(
			StudentStateId,
			EnglishY,
			EnglishA,
			EnglishB,
			EnglishC,
			PreAlgebra,
			AlgebraI,
			AlgebraII,
			MathGE,
			[Statistics],
			CollegeAlgebra,
			Trigonometry,
			PreCalculus,
			CalculusI,
			ReadingM_UboundY,
			ReadingY_UboundY,
			ReadingA_UboundY,
			ReadingB_UboundY,
			ReadingC_UboundY,
			ReadingM_UboundA,
			ReadingA_UboundA,
			ReadingB_UboundA,
			ReadingC_UboundA,
			ReadingM_UboundB,
			ReadingB_UboundB,
			ReadingC_UboundB,
			EslY_UboundY,
			EslA_UboundY,
			EslB_UboundY,
			EslA_UboundA,
			EslB_UboundA,
			EslC_UboundA,
			EslB_UboundB,
			EslC_UboundB,
			EslD_UboundB
		)
	SELECT
		StudentStateId,
		EnglishY,
		EnglishA,
		EnglishB,
		EnglishC,
		PreAlgebra,
		AlgebraI,
		AlgebraII,
		MathGE,
		[Statistics],
		CollegeAlgebra,
		Trigonometry,
		PreCalculus,
		CalculusI,
		ReadingM_UboundY,
		ReadingY_UboundY,
		ReadingA_UboundY,
		ReadingB_UboundY,
		ReadingC_UboundY,
		ReadingM_UboundA,
		ReadingA_UboundA,
		ReadingB_UboundA,
		ReadingC_UboundA,
		ReadingM_UboundB,
		ReadingB_UboundB,
		ReadingC_UboundB,
		EslY_UboundY,
		EslA_UboundY,
		EslB_UboundY,
		EslA_UboundA,
		EslB_UboundA,
		EslC_UboundA,
		EslB_UboundB,
		EslC_UboundB,
		EslD_UboundB
	FROM
		mmap.K12Placement
	WHERE
		IsEscrow = @True;

	UPDATE
		a
	SET
		a.EnglishY         = p.EnglishY,
		a.EnglishA         = p.EnglishA,
		a.EnglishB         = p.EnglishB,
		a.EnglishC         = p.EnglishC,
		a.PreAlgebra       = p.PreAlgebra,
		a.AlgebraI         = p.AlgebraI,
		a.AlgebraII        = p.AlgebraII,
		a.MathGE           = p.MathGE,
		a.[Statistics]     = p.[Statistics],
		a.CollegeAlgebra   = p.CollegeAlgebra,
		a.Trigonometry     = p.Trigonometry,
		a.PreCalculus      = p.PreCalculus,
		a.CalculusI        = p.CalculusI,
		a.ReadingM_UboundY = p.ReadingM_UboundY,
		a.ReadingY_UboundY = p.ReadingY_UboundY,
		a.ReadingA_UboundY = p.ReadingA_UboundY,
		a.ReadingB_UboundY = p.ReadingB_UboundY,
		a.ReadingC_UboundY = p.ReadingC_UboundY,
		a.ReadingM_UboundA = p.ReadingM_UboundA,
		a.ReadingA_UboundA = p.ReadingA_UboundA,
		a.ReadingB_UboundA = p.ReadingB_UboundA,
		a.ReadingC_UboundA = p.ReadingC_UboundA,
		a.ReadingM_UboundB = p.ReadingM_UboundB,
		a.ReadingB_UboundB = p.ReadingB_UboundB,
		a.ReadingC_UboundB = p.ReadingC_UboundB,
		a.EslY_UboundY     = p.EslY_UboundY,
		a.EslA_UboundY     = p.EslA_UboundY,
		a.EslB_UboundY     = p.EslB_UboundY,
		a.EslA_UboundA     = p.EslA_UboundA,
		a.EslB_UboundA     = p.EslB_UboundA,
		a.EslC_UboundA     = p.EslC_UboundA,
		a.EslB_UboundB     = p.EslB_UboundB,
		a.EslC_UboundB     = p.EslC_UboundB,
		a.EslD_UboundB     = p.EslD_UboundB
	FROM
		CPPDELSQLP01.CPP_API.dbo.K12Placement a
		inner join
		@K12Placement p
			on p.StudentStateId = a.StudentStateId;

	INSERT
		CPPDELSQLP01.CPP_API.dbo.K12Placement
		(
			StudentStateId,
			EnglishY,
			EnglishA,
			EnglishB,
			EnglishC,
			PreAlgebra,
			AlgebraI,
			AlgebraII,
			MathGE,
			[Statistics],
			CollegeAlgebra,
			Trigonometry,
			PreCalculus,
			CalculusI,
			ReadingM_UboundY,
			ReadingY_UboundY,
			ReadingA_UboundY,
			ReadingB_UboundY,
			ReadingC_UboundY,
			ReadingM_UboundA,
			ReadingA_UboundA,
			ReadingB_UboundA,
			ReadingC_UboundA,
			ReadingM_UboundB,
			ReadingB_UboundB,
			ReadingC_UboundB,
			EslY_UboundY,
			EslA_UboundY,
			EslB_UboundY,
			EslA_UboundA,
			EslB_UboundA,
			EslC_UboundA,
			EslB_UboundB,
			EslC_UboundB,
			EslD_UboundB
		)
	SELECT
		StudentStateId,
		EnglishY,
		EnglishA,
		EnglishB,
		EnglishC,
		PreAlgebra,
		AlgebraI,
		AlgebraII,
		MathGE,
		[Statistics],
		CollegeAlgebra,
		Trigonometry,
		PreCalculus,
		CalculusI,
		ReadingM_UboundY,
		ReadingY_UboundY,
		ReadingA_UboundY,
		ReadingB_UboundY,
		ReadingC_UboundY,
		ReadingM_UboundA,
		ReadingA_UboundA,
		ReadingB_UboundA,
		ReadingC_UboundA,
		ReadingM_UboundB,
		ReadingB_UboundB,
		ReadingC_UboundB,
		EslY_UboundY,
		EslA_UboundY,
		EslB_UboundY,
		EslA_UboundA,
		EslB_UboundA,
		EslC_UboundA,
		EslB_UboundB,
		EslC_UboundB,
		EslD_UboundB
	FROM
		@K12Placement p
	WHERE
		not exists (
			SELECT
				0
			FROM
				CPPDELSQLP01.CPP_API.dbo.K12Placement a
			WHERE
				a.StudentStateId = p.StudentStateId
		);

	UPDATE
		p
	SET
		IsEscrow = @False
	FROM
		mmap.K12Placement p
		inner join
		@K12Placement pp
			on pp.StudentStateId = p.StudentStateId;
END;