USE calpass;

GO

IF (object_id('mmap.p_ProspectiveCohort_Delsert') is not null)
	BEGIN
		DROP PROCEDURE mmap.p_ProspectiveCohort_Delsert;
	END;

GO

CREATE PROCEDURE
	mmap.p_ProspectiveCohort_Delsert
	(
		@SubmissionFileId int,
		@StageSchemaTableName nvarchar(517)
	)
AS

DECLARE
	@Message nvarchar(2048),
	@Validate bit = 0,
	@Sql nvarchar(max),
	@OrganizationId int,
	@InterSegment dbo.InterSegment,
	@Ruleset mmap.Ruleset,
	@Hash dbo.HSTranscriptSummaryHash;

	DECLARE
		@StudentRuleset
	AS TABLE
		(
			InterSegmentKey binary(64),
			CourseCode varchar(30),
			PRIMARY KEY
				(
					InterSegmentKey,
					CourseCode
				)
		);

	DECLARE
		@Placement
	AS TABLE
		(
			InterSegmentKey binary(64) primary key,
			engl_cb21 char (1),
			read_cb21 char (1),
			esl_cb21 char (1),
			math_cb21 char (1),
			math_geo char (1),
			math_alg char (1),
			math_ge char (1),
			math_stat char (1),
			math_pre_calc char (1),
			math_trig char (1),
			math_col_alg char (1),
			math_calc_i char (1)
		);

	DECLARE
		@Star
	AS TABLE
		(
			InterSegmentKey binary(64) primary key,
			engl_eap_ind tinyint,
			engl_scaled_score int,
			math_subject tinyint,
			math_eap_ind tinyint,
			math_scaled_score int,
			esl_ind tinyint
		);

	DECLARE
		@StudentInputs
	AS TABLE
		(
			InterSegmentKey binary(64) primary key,
			grade_level char(2),
			gpa_cum decimal(4,3),
			english decimal(3,2),
			english_ap decimal(3,2),
			pre_alg decimal(3,2),
			pre_alg_ap decimal(3,2),
			alg_i decimal(3,2),
			alg_i_ap decimal(3,2),
			alg_ii decimal(3,2),
			alg_ii_ap decimal(3,2),
			geo decimal(3,2),
			geo_ap decimal(3,2),
			trig decimal(3,2),
			trig_ap decimal(3,2),
			pre_calc decimal(3,2),
			pre_calc_ap decimal(3,2),
			calc decimal(3,2),
			calc_ap decimal(3,2),
			stat decimal(3,2),
			stat_ap decimal(3,2),
			esl_ind tinyint
		);

BEGIN
	-- validate inputs
	SELECT
		@Validate = 1
	FROM
		sys.tables
	WHERE
		object_id = object_id(@StageSchemaTableName);
	-- process validation
	IF (@Validate = 0)
		BEGIN
			SET @Message = N'Table not found';
			THROW 70099, @Message, 1;
		END;
	-- get OrganizationId
	SELECT
		@OrganizationId = OrganizationId
	FROM
		[PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedrop
	WHERE
		SubmissionFileId = @SubmissionFileId;
	-- set dynamic sql
	DELETE
	FROM
		mmap.ProspectiveCohort with (tablockx)
	WHERE
		SubmissionFileId = @SubmissionFileId;
	-- set dynamic sql
	SET @Sql = N'
		INSERT INTO
			mmap.ProspectiveCohort with (tablockx)
			(
				CollegeId,
				StudentId,
				IdStatus,
				Birthdate,
				Gender,
				HighSchool,
				NameFirst,
				NameLast,
				derkey1,
				SubmissionFileId,
				SubmissionFileRecordNumber
			)
		SELECT
			CollegeId,
			StudentId = case
					when IdStatus = ''S'' then calpass.dbo.Get1289Encryption(StudentId, ''X'')
					else StudentId
				end,
			IdStatus,
			Birthdate,
			Gender,
			HighSchool,
			NameFirst,
			NameLast,
			derkey1 = 
				HASHBYTES(
					''SHA2_512'',
					upper(convert(nchar(3), NameFirst)) + 
					upper(convert(nchar(3), NameLast)) + 
					upper(convert(nchar(1), Gender)) + 
					convert(nchar(8), Birthdate)
				),
			@SubmissionFileId,
			SubmissionFileRecordNumber
		FROM
			' + @StageSchemaTableName + ';';
	-- exe dynamic sql
	EXECUTE sp_executesql
		@Sql,
		N'@SubmissionFileId int',
		@SubmissionFileId = @SubmissionFileId;

	INSERT
		@InterSegment
		(
			InterSegmentKey
		)
	SELECT
		pc.Derkey1
	FROM
		mmap.ProspectiveCohort pc
		inner join
		dbo.Student s
			on s.InterSegmentKey = pc.Derkey1
	WHERE
		pc.SubmissionFileId = @SubmissionFileId
		and s.IsHighSchoolCollision = 0
		and not exists (
			SELECT
				1
			FROM
				mmap.ProspectiveCohort pc1
			WHERE
				pc1.SubmissionFileId = pc.SubmissionFileId
				and pc1.Derkey1 = pc.Derkey1
			HAVING
				count(*) > 1
		);

	-- set ruleset
	INSERT
		@Ruleset
		(
			Entity,
			Attribute,
			Value,
			BranchId,
			CourseCode,
			LeafCount
		)
	EXECUTE mmap.RulesetSelect
		@OrganizationId = @OrganizationId;

	-- set student hash
	INSERT
		@Hash
		(
			Entity,
			Attribute,
			Value,
			InterSegmentKey
		)
	SELECT
		h.Entity,
		h.Attribute,
		h.Value,
		h.InterSegmentKey
	FROM
		dbo.HSTranscriptSummaryHash h
	WHERE
		exists (
			SELECT
				1
			FROM
				@InterSegment s
			WHERE
				s.InterSegmentKey = h.InterSegmentKey
		);

	INSERT INTO
		@StudentRuleset
		(
			InterSegmentKey,
			CourseCode
		)
	SELECT DISTINCT
		h.InterSegmentKey,
		CourseCode
	FROM
		@Ruleset r
		inner join
		@Hash h
			on r.Entity = h.Entity
			and r.Attribute = h.Attribute
	GROUP BY
		h.InterSegmentKey,
		CourseCode,
		r.BranchId,
		r.LeafCount
	HAVING
		sum(case when h.Value >= r.Value then 1 else 0 end) = r.LeafCount;

	INSERT INTO
		@Placement
		(
			InterSegmentKey,
			engl_cb21,
			read_cb21,
			esl_cb21,
			math_cb21,
			math_geo,
			math_alg,
			math_ge,
			math_stat,
			math_pre_calc,
			math_trig,
			math_col_alg,
			math_calc_i
		)
	SELECT
		InterSegmentKey,
		engl_cb21 = 
			coalesce(
				max(case when CourseCode = 'EnglishCollege' then 'Y' end), 
				max(case when CourseCode = 'EnglishLevelOne' then 'A' end),
				max(case when CourseCode = 'EnglishLevelTwo' then 'B' end),
				max(case when CourseCode = 'EnglishLevelThree' then 'C' end)
			),
		read_cb21 = 
			coalesce(
				max(case when CourseCode = 'ReadingTestOut' then 'M' end), 
				max(case when CourseCode = 'ReadingCollege' then 'Y' end),
				max(case when CourseCode = 'ReadingLevelOne' then 'A' end),
				max(case when CourseCode = 'ReadingLevelTwo' then 'B' end),
				max(case when CourseCode = 'ReadingLevelThree' then 'C' end)
			),
		esl_cb21 = 
			coalesce(
				max(case when CourseCode = 'EslCollege' then 'Y' end),
				max(case when CourseCode = 'EslLevelOne' then 'A' end),
				max(case when CourseCode = 'EslLevelTwo' then 'B' end),
				max(case when CourseCode = 'EslLevelThree' then 'C' end)
			),
		math_cb21 = 
			coalesce(
				max(case when CourseCode = 'CalculusI' then 'Y' end),
				max(case when CourseCode = 'PreCalculus' then 'Y' end),
				max(case when CourseCode = 'Trigonometry' then 'Y' end),
				max(case when CourseCode = 'CollegeAlgebra' then 'Y' end),
				max(case when CourseCode = 'Statistics' then 'Y' end),
				max(case when CourseCode = 'MathGE' then 'Y' end),
				max(case when CourseCode = 'AlgebraII' then 'A' end),
				max(case when CourseCode = 'AlgebraI' then 'B' end),
				max(case when CourseCode = 'PreAlgebra' then 'C' end)
			),
		math_geo = null,
		math_alg = max(case when CourseCode = 'AlgebraII' then 'A' end),
		math_ge = max(case when CourseCode = 'MathGE' then 'Y' end),
		math_stat = max(case when CourseCode = 'Statistics' then 'Y' end),
		math_pre_calc = max(case when CourseCode = 'PreCalculus' then 'Y' end),
		math_trig = max(case when CourseCode = 'Trigonometry' then 'Y' end),
		math_col_alg = max(case when CourseCode = 'CollegeAlgebra' then 'Y' end),
		math_calc_i = max(case when CourseCode = 'CalculusI' then 'Y' end)
	FROM
		@StudentRuleset
	GROUP BY
		InterSegmentKey;

	INSERT INTO
		@StudentInputs
		(
			InterSegmentKey,
			grade_level,
			gpa_cum,
			english,
			english_ap,
			pre_alg,
			pre_alg_ap,
			alg_i,
			alg_i_ap,
			alg_ii,
			alg_ii_ap,
			geo,
			geo_ap,
			trig,
			trig_ap,
			pre_calc,
			pre_calc_ap,
			calc,
			calc_ap,
			stat,
			stat_ap,
			esl_ind
		)
	SELECT
		InterSegmentKey,
		grade_level = max(case when Entity = 'Cgpa' then Attribute end),
		gpa_cum = max(case when Entity = 'Cgpa' then Value end),
		english = 
			coalesce(
				max(case when Entity = 'LiteratureAP' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'LanguageAP' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'Expository' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'English12' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'English11' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'English10' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'English09' and Attribute = 'MarkPoints' then Value end)
			),
		english_ap = 
			coalesce(
				max(case when Entity = 'LiteratureAP' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'LanguageAP' and Attribute = 'MarkPoints' then Value end)
			),
		pre_alg = max(case when Entity = 'PreAlgebra' and Attribute = 'MarkPoints' then Value end),
		pre_alg_ap = null,
		alg_i = max(case when Entity = 'AlgebraI' and Attribute = 'MarkPoints' then Value end),
		alg_i_ap = null,
		alg_ii = max(case when Entity = 'AlgebraII' and Attribute = 'MarkPoints' then Value end),
		alg_ii_ap = null,
		geo = max(case when Entity = 'Geometry' and Attribute = 'MarkPoints' then Value end),
		geo_ap = null,
		trig = max(case when Entity = 'Trigonometry' and Attribute = 'MarkPoints' then Value end),
		trig_ap = null,
		pre_calc = max(case when Entity = 'PreCalculus' and Attribute = 'MarkPoints' then Value end),
		pre_calc_ap = null,
		calc = 
			coalesce(
				max(case when Entity = 'CalculusIIAP' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'CalculusIAP' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'CalculusII' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'CalculusI' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'Calculus' and Attribute = 'MarkPoints' then Value end)
			),
		calc_ap = 
			coalesce(
				max(case when Entity = 'CalculusIIAP' and Attribute = 'MarkPoints' then Value end),
				max(case when Entity = 'CalculusIAP' and Attribute = 'MarkPoints' then Value end)
			),
		stat = max(case when Entity = 'Statistics' and Attribute = 'MarkPoints' then Value end),
		stat_ap = max(case when Entity = 'StatisticsAP' and Attribute = 'MarkPoints' then Value end),
		esl_ind = max(case when Entity = 'Remedial' then 1 else 0 end)
	FROM
		@Hash
	GROUP BY
		InterSegmentKey;

	INSERT INTO
		@Star
		(
			InterSegmentKey,
			engl_eap_ind,
			engl_scaled_score,
			math_subject,
			math_eap_ind,
			math_scaled_score,
			esl_ind
		)
	SELECT
		d.InterSegmentKey,
		engl_eap_ind = max(d.engl_eap_ind),
		engl_scaled_score = max(case when d.EnglishSelector = 1 then d.engl_scaled_score end),
		math_subject = max(case when d.MathSelector = 1 then d.math_subject end),
		math_eap_ind = max(d.math_eap_ind),
		math_scaled_score = max(case when d.MathSelector = 1 then d.math_scaled_score end),
		esl_ind = max(d.esl_ind)
	FROM
		(
			SELECT
				InterSegmentKey = cst.derkey1,
				EnglishSelector = 
					row_number() OVER(
							partition by
								cst.derkey1
							order by
								case when cst.cstengss is null then 0 else 1 end, -- get non-null scores
								y.YearTrailing DESC, -- most recent year that has a non-null score
								convert(int, cst.cstengss) DESC -- highest score
						),
				engl_eap_ind = convert(tinyint, case when dem.EAPELAStatus = '1' then 1 else 0 end),
				engl_scaled_score = convert(int, cst.cstengss),
				MathSelector = 
					row_number() OVER(
						partition by
							cst.derkey1
						ORDER BY
							-- b.rank DESC, [1]
							case when cst.cstmathsubj in ('0', '2', '6', '8') then 1 else 0 end DESC,
							y.YearTrailing DESC,
							convert(int, cst.cstmathss) DESC
					),
				math_subject = convert(tinyint, cst.cstmathsubj),
				math_eap_ind = convert(tinyint, case when dem.EAPMathStatus = '1' then 1 else 0 end),
				math_scaled_score = convert(int, cst.cstmathss),
				esl_ind = convert(tinyint, case when dem.englprof in ('3', 'EL') then 1 else 0 end)
			FROM
				dbo.K12StarDEMProd dem
				inner join
				dbo.K12StarCSTProd cst
					on cst.School = dem.School
					and cst.LocStudentId = dem.LocStudentId
					and cst.AcYear = dem.AcYear
				left outer join
				dbo.StarMath sm
					on cst.cstmathsubj = sm.Code
				left outer join
				calpads.Year y
					on dem.AcYear = y.YearCodeAbbr
			WHERE
				cst.derkey1 is not null
				and dem.derkey1 is not null
				and cst.cstmathsubj in ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
				and exists (
						SELECT
							1
						FROM
							@InterSegment s
						WHERE
							s.InterSegmentKey = cst.derkey1
				)
		) d
	GROUP BY
		d.InterSegmentKey;

	UPDATE
		t
	SET
		t.match_type = coalesce(s.MatchType, 'No Match'),
		t.engl_cb21 = s.engl_cb21,
		t.read_cb21 = s.read_cb21,
		t.esl_cb21 = s.esl_cb21,
		t.math_cb21 = s.math_cb21,
		t.math_geo = s.math_geo,
		t.math_alg = s.math_alg,
		t.math_ge = s.math_ge,
		t.math_stat = s.math_stat,
		t.math_trig = s.math_trig,
		t.math_pre_calc = s.math_pre_calc,
		t.math_col_alg = s.math_col_alg,
		t.math_calc_i = s.math_calc_i,
		t.grade_level = s.grade_level,
		t.gpa_cum = s.gpa_cum,
		t.ap_any = s.ap_any,
		t.english = s.english,
		t.english_ap = s.english_ap,
		t.pre_alg = s.pre_alg,
		t.pre_alg_ap = s.pre_alg_ap,
		t.alg_i = s.alg_i,
		t.alg_i_ap = s.alg_i_ap,
		t.alg_ii = s.alg_ii,
		t.alg_ii_ap = s.alg_ii_ap,
		t.geo = s.geo,
		t.geo_ap = s.geo_ap,
		t.trig = s.trig,
		t.trig_ap = s.trig_ap,
		t.pre_calc = s.pre_calc,
		t.pre_calc_ap = s.pre_calc_ap,
		t.calc = s.calc,
		t.calc_ap = s.calc_ap,
		t.stat = s.stat,
		t.stat_ap = s.stat_ap,
		t.engl_eap_ind = s.engl_eap_ind,
		t.engl_scaled_score = s.engl_scaled_score,
		t.math_subject = s.math_subject,
		t.math_eap_ind = s.math_eap_ind,
		t.math_scaled_score = s.math_scaled_score,
		t.esl_ind = s.esl_ind
	FROM
		calpass.mmap.ProspectiveCohort t
		left outer join
		(
			SELECT
				si.InterSegmentKey,
				MatchType = 'Match' + 
					case
						when coalesce(
								math_calc_i,
								math_pre_calc,
								math_stat,
								math_col_alg,
								math_ge,
								math_alg,
								math_geo,
								math_cb21
							) is not null then '; Mathematics Placement'
						else '; No Mathematics Placement'
					end + 
					case
						when engl_cb21 is not null then '; English Placement'
						else '; No English Placement'
					end + 
					case
						when p.read_cb21 is not null then '; Reading Placement'
						else '; No Reading Placement'
					end +
					case
						when p.esl_cb21 is not null then '; ESL Placement'
						else '; No ESL Placement'
					end + 
					case
						when convert(tinyint, grade_level) < 11 then '; No 11th or 12th Grade Data'
						else '' + 
							case
								when english is null then '; No 11th or 12th Grade English Data'
								else ''
							end + 
							case
								when (pre_alg is null
									and alg_i is null
									and alg_ii is null
									and geo is null
									and trig is null
									and pre_calc is null
									and calc is null
									and calc_ap is null
									and stat is null
									and stat_ap is null) then '; No Course Specific Mathematics Data'
								else ''
							end + 
							case
								when s.InterSegmentKey is null then '; No CST Data'
								else ''
							end
					end,
				p.engl_cb21,
				p.read_cb21,
				p.esl_cb21,
				p.math_cb21,
				p.math_geo,
				p.math_alg,
				p.math_ge,
				p.math_stat,
				math_pre_calc = case when p.math_calc_i = 'Y'  then p.math_pre_calc 'Y' else p.math_calc_i end,
				p.math_trig,
				p.math_col_alg,
				p.math_calc_i,
				si.grade_level,
				si.gpa_cum,
				ap_any =
					coalesce(
						si.english_ap,
						si.stat_ap,
						si.calc_ap
					),
				si.english,
				si.english_ap,
				si.pre_alg,
				si.pre_alg_ap,
				si.alg_i,
				si.alg_i_ap,
				si.alg_ii,
				si.alg_ii_ap,
				si.geo,
				si.geo_ap,
				si.trig,
				si.trig_ap,
				si.pre_calc,
				si.pre_calc_ap,
				si.calc,
				si.calc_ap,
				si.stat,
				si.stat_ap,
				s.engl_eap_ind,
				s.engl_scaled_score,
				s.math_subject,
				s.math_eap_ind,
				s.math_scaled_score,
				esl_ind =
					case
						when si.esl_ind = 1 or s.esl_ind = 1 then 1
						else 0
					end
			FROM
				@StudentInputs si
				left outer join
				@Placement p
					on p.InterSegmentKey = si.InterSegmentKey
				left outer join
				@Star s
					on s.InterSegmentKey = p.InterSegmentKey
		) s
			on t.Derkey1 = s.InterSegmentKey
	WHERE
		t.SubmissionFileId = @SubmissionFileId;
	
	-- ROADMMAP PROJECT
	IF (@OrganizationId = 170739)
		BEGIN
			UPDATE
				t
			SET
				t.IsTreatmentRoadmmap = s.IsTreatmentRoadmmap,
				t.match_type          = s.match_type,
				t.engl_cb21           = s.engl_cb21,
				t.read_cb21           = s.read_cb21,
				t.esl_cb21            = s.esl_cb21,
				t.math_cb21           = s.math_cb21,
				t.math_geo            = s.math_geo,
				t.math_alg            = s.math_alg,
				t.math_ge             = s.math_ge,
				t.math_stat           = s.math_stat,
				t.math_pre_calc       = s.math_pre_calc,
				t.math_trig           = s.math_trig,
				t.math_col_alg        = s.math_col_alg,
				t.math_calc_i         = s.math_calc_i,
				t.grade_level         = s.grade_level,
				t.gpa_cum             = s.gpa_cum,
				t.AP_ANY              = s.AP_ANY,
				t.english             = s.english,
				t.english_ap          = s.english_ap,
				t.pre_alg             = s.pre_alg,
				t.pre_alg_ap          = s.pre_alg_ap,
				t.alg_i               = s.alg_i,
				t.alg_i_ap            = s.alg_i_ap,
				t.alg_ii              = s.alg_ii,
				t.alg_ii_ap           = s.alg_ii_ap,
				t.geo                 = s.geo,
				t.geo_ap              = s.geo_ap,
				t.trig                = s.trig,
				t.trig_ap             = s.trig_ap,
				t.pre_calc            = s.pre_calc,
				t.pre_calc_ap         = s.pre_calc_ap,
				t.calc                = s.calc,
				t.calc_ap             = s.calc_ap,
				t.stat                = s.stat,
				t.stat_ap             = s.stat_ap,
				t.engl_eap_ind        = s.engl_eap_ind,
				t.engl_scaled_score   = s.engl_scaled_score,
				t.math_subject        = s.math_subject,
				t.math_eap_ind        = s.math_eap_ind,
				t.math_scaled_score   = s.math_scaled_score,
				t.esl_ind             = s.esl_ind
			FROM
				mmap.ProspectiveCohort t
				inner join
				(
					SELECT
						CollegeId,
						StudentId,
						IdStatus,
						SubmissionFileId,
						IsTreatmentRoadmmap = ra.Assignment,
						match_type          = case when ra.Assignment = 1 then pc.match_type end,
						engl_cb21           = case when ra.Assignment = 1 then pc.engl_cb21 end,
						read_cb21           = case when ra.Assignment = 1 then pc.read_cb21 end,
						esl_cb21            = case when ra.Assignment = 1 then pc.esl_cb21 end,
						math_cb21           = case when ra.Assignment = 1 then pc.math_cb21 end,
						math_geo            = case when ra.Assignment = 1 then pc.math_geo end,
						math_alg            = case when ra.Assignment = 1 then pc.math_alg end,
						math_ge             = case when ra.Assignment = 1 then pc.math_ge end,
						math_stat           = case when ra.Assignment = 1 then pc.math_stat end,
						math_pre_calc       = case when ra.Assignment = 1 then pc.math_pre_calc end,
						math_trig           = case when ra.Assignment = 1 then pc.math_trig end,
						math_col_alg        = case when ra.Assignment = 1 then pc.math_col_alg end,
						math_calc_i         = case when ra.Assignment = 1 then pc.math_calc_i end,
						grade_level         = case when ra.Assignment = 1 then pc.grade_level end,
						gpa_cum             = case when ra.Assignment = 1 then pc.gpa_cum end,
						AP_ANY              = case when ra.Assignment = 1 then pc.AP_ANY end,
						english             = case when ra.Assignment = 1 then pc.english end,
						english_ap          = case when ra.Assignment = 1 then pc.english_ap end,
						pre_alg             = case when ra.Assignment = 1 then pc.pre_alg end,
						pre_alg_ap          = case when ra.Assignment = 1 then pc.pre_alg_ap end,
						alg_i               = case when ra.Assignment = 1 then pc.alg_i end,
						alg_i_ap            = case when ra.Assignment = 1 then pc.alg_i_ap end,
						alg_ii              = case when ra.Assignment = 1 then pc.alg_ii end,
						alg_ii_ap           = case when ra.Assignment = 1 then pc.alg_ii_ap end,
						geo                 = case when ra.Assignment = 1 then pc.geo end,
						geo_ap              = case when ra.Assignment = 1 then pc.geo_ap end,
						trig                = case when ra.Assignment = 1 then pc.trig end,
						trig_ap             = case when ra.Assignment = 1 then pc.trig_ap end,
						pre_calc            = case when ra.Assignment = 1 then pc.pre_calc end,
						pre_calc_ap         = case when ra.Assignment = 1 then pc.pre_calc_ap end,
						calc                = case when ra.Assignment = 1 then pc.calc end,
						calc_ap             = case when ra.Assignment = 1 then pc.calc_ap end,
						stat                = case when ra.Assignment = 1 then pc.stat end,
						stat_ap             = case when ra.Assignment = 1 then pc.stat_ap end,
						engl_eap_ind        = case when ra.Assignment = 1 then pc.engl_eap_ind end,
						engl_scaled_score   = case when ra.Assignment = 1 then pc.engl_scaled_score end,
						math_subject        = case when ra.Assignment = 1 then pc.math_subject end,
						math_eap_ind        = case when ra.Assignment = 1 then pc.math_eap_ind end,
						math_scaled_score   = case when ra.Assignment = 1 then pc.math_scaled_score end,
						esl_ind             = case when ra.Assignment = 1 then pc.esl_ind end
					FROM
						calpass.mmap.ProspectiveCohort pc
						cross apply
						dbo.RandomAssignment(default) ra
					WHERE
						SubmissionFileId = @SubmissionFileId
			) s
				on t.CollegeId = s.CollegeId
				and t.StudentId = s.StudentId
				and t.IdStatus = s.IdStatus
				and t.SubmissionFileId = s.SubmissionFileId
		END;
END;