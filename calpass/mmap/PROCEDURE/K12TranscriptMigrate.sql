USE calpass;

GO

IF (object_id('mmap.K12TranscriptMigrate') is not null)
	BEGIN
		DROP PROCEDURE mmap.K12TranscriptMigrate;
	END;

GO

CREATE PROCEDURE
	mmap.K12TranscriptMigrate
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@K12Transcript mmap.K12Transcript,
	@False         bit = 0,
	@True          bit = 1;

BEGIN
	INSERT INTO
		@K12Transcript
		(
			StudentStateId,
			IsHighSchoolGrade09,
			IsHighSchoolGrade10,
			IsHighSchoolGrade11,
			IsHighSchoolGrade12,
			CumulativeGradePointAverage,
			English,
			PreAlgebra,
			AlgebraI,
			Geometry,
			AlgebraII,
			Trigonometry,
			PreCalculus,
			[Statistics],
			Calculus
		)
	SELECT
		StudentStateId,
		IsHighSchoolGrade09,
		IsHighSchoolGrade10,
		IsHighSchoolGrade11,
		IsHighSchoolGrade12,
		CumulativeGradePointAverage,
		English,
		PreAlgebra,
		AlgebraI,
		Geometry,
		AlgebraII,
		Trigonometry,
		PreCalculus,
		[Statistics],
		Calculus
	FROM
		mmap.K12Transcript
	WHERE
		IsEscrow = @True;

	UPDATE
		t
	SET
		t.IsHighSchoolGrade09         = tt.IsHighSchoolGrade09,
		t.IsHighSchoolGrade10         = tt.IsHighSchoolGrade10,
		t.IsHighSchoolGrade11         = tt.IsHighSchoolGrade11,
		t.IsHighSchoolGrade12         = tt.IsHighSchoolGrade12,
		t.CumulativeGradePointAverage = tt.CumulativeGradePointAverage,
		t.English                     = tt.English,
		t.PreAlgebra                  = tt.PreAlgebra,
		t.AlgebraI                    = tt.AlgebraI,
		t.Geometry                    = tt.Geometry,
		t.AlgebraII                   = tt.AlgebraII,
		t.Trigonometry                = tt.Trigonometry,
		t.PreCalculus                 = tt.PreCalculus,
		t.[Statistics]                = tt.[Statistics],
		t.Calculus                    = tt.Calculus
	FROM
		CPPDELSQLP01.CPP_API.dbo.K12Transcript t
		inner join
		@K12Transcript tt
			on tt.StudentStateId = t.StudentStateId;

	INSERT
		CPPDELSQLP01.CPP_API.dbo.K12Transcript
		(
			StudentStateId,
			IsHighSchoolGrade09,
			IsHighSchoolGrade10,
			IsHighSchoolGrade11,
			IsHighSchoolGrade12,
			CumulativeGradePointAverage,
			English,
			PreAlgebra,
			AlgebraI,
			Geometry,
			AlgebraII,
			Trigonometry,
			PreCalculus,
			[Statistics],
			Calculus
		)
	SELECT
		tt.StudentStateId,
		tt.IsHighSchoolGrade09,
		tt.IsHighSchoolGrade10,
		tt.IsHighSchoolGrade11,
		tt.IsHighSchoolGrade12,
		tt.CumulativeGradePointAverage,
		tt.English,
		tt.PreAlgebra,
		tt.AlgebraI,
		tt.Geometry,
		tt.AlgebraII,
		tt.Trigonometry,
		tt.PreCalculus,
		tt.[Statistics],
		tt.Calculus
	FROM
		@K12Transcript tt
	WHERE
		not exists (
			SELECT
				0
			FROM
				CPPDELSQLP01.CPP_API.dbo.K12Transcript t
			WHERE
				tt.StudentStateId = t.StudentStateId
		);

	UPDATE
		t
	SET
		t.IsEscrow = @False
	FROM
		mmap.K12Transcript t
		inner join
		@K12Transcript tt
			on tt.StudentStateId = t.StudentStateId;
END;