USE calpass;

GO

IF (object_id('mmap.p_ProspectiveCohort_Eval') is not null)
	BEGIN
		DROP PROCEDURE mmap.p_ProspectiveCohort_Eval;
	END;

GO

CREATE PROCEDURE
	mmap.p_ProspectiveCohort_Eval
	(
		@SubmissionFileId integer
	)
AS

DECLARE
	-- constants
	@ReplicateStatic   integer      = 16,
	@ReplicateChar     char(1)      = '-',
	@Grade09           char(2)      = '09',
	@Grade10           char(2)      = '10',
	@Grade11           char(2)      = '11',
	@Grade12           char(2)      = '12',
	@MatchType         char(8)      = 'No Match',
	@StatusType        char(6)      = 'Active',
	@ProfileName       sysname      = 'Cal-PASS SQL Support Profile',
	@Recipients        varchar(max) = 'dlamoree@edresults.org;jhetts@edresults.org;dmoynihan@edresults.org',
	@Subject           varchar(255) = '',
	@Body              varchar(max) = '',
	@Space             char(5)      = '&nbsp',
	@CRLF              char(4)      = '<br>',
	@Threshold         tinyint      = 75,
	-- variables
	@Spacer            varchar(8000),
	@OrgName           varchar(255),
	@FileName          varchar(255),
	@FileSubmitDate    datetime,
	@FileProcessDate   datetime,
	@CohortCount       int,
	@OriginCount       int,
	@CourseYearCode    char(4),
	@PlacementRateEngl decimal(5,2),
	@PlacementRateMath decimal(5,2);

	SET NOCOUNT ON;
	
BEGIN
	SELECT
		@OrgName = o.OrganizationName
	FROM
		[PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedrop f
		inner join
		[PRO-DAT-SQL-01].calpass.dbo.Organization o
			on o.OrganizationId = f.OrganizationId
	WHERE
		f.SubmissionFileId = @SubmissionFileId;

	SELECT
		@FileName = substring(OriginalFileName, charindex(char(46), OriginalFileName) + 1, datalength(OriginalFileName)),
		@FileSubmitDate = SubmissionDateTime,
		@FileProcessDate = isnull(ProcessedDateTime, current_timestamp)
	FROM
		[PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedrop
	WHERE
		SubmissionFileId = @SubmissionFileId;

	CREATE TABLE
		#ProspectiveCohortEval
		(
			DistrictCode char(7) not null,
			DistrictName varchar(90) not null,
			DistrictDenominator int not null,
			NoGradeNumerator int not null,
			Grade09Numerator int not null,
			Grade10Numerator int not null,
			Grade11Numerator int not null,
			Grade12Numerator int not null,
			MatchNumerator int not null,
			PlacementRateEngl int not null,
			PlacementRateMath int not null
		);

	INSERT INTO
		#ProspectiveCohortEval
		(
			DistrictCode,
			DistrictName,
			DistrictDenominator,
			NoGradeNumerator,
			Grade09Numerator,
			Grade10Numerator,
			Grade11Numerator,
			Grade12Numerator,
			MatchNumerator,
			PlacementRateEngl,
			PlacementRateMath
		)
	SELECT
		DistrictCode        = substring(i.CdsCode, 1, 7),
		DistrictName        = i.District,
		DistrictDenominator = sum(case when i.CdsCode is not null then 1 else 0 end),
		NoGradeNumerator    = sum(case when c.grade_level is null then 1 else 0 end),
		Grade09Numerator    = sum(case when c.grade_level = @Grade09 then 1 else 0 end),
		Grade10Numerator    = sum(case when c.grade_level = @Grade10 then 1 else 0 end),
		Grade11Numerator    = sum(case when c.grade_level = @Grade11 then 1 else 0 end),
		Grade12Numerator    = sum(case when c.grade_level = @Grade12 then 1 else 0 end),
		MatchNumerator      = sum(case when c.match_type != @MatchType then 1 else 0 end),
		PlacementRateEngl   = sum(case when c.engl_cb21 is not null then 1 else 0 end),
		PlacementRateMath   = sum(case when c.math_cb21 is not null then 1 else 0 end)
	FROM
		mmap.ProspectiveCohort c
		inner join
		calpads.Institution i
			on substring(i.CdsCode, 8, 6) = c.HighSchool
	WHERE
		c.SubmissionFileId = @SubmissionFileId
		and i.School is not null
		and i.StatusType <> 'Merged'
	GROUP BY
		substring(i.CdsCode, 1, 7),
		i.District;

	SELECT
		@CohortCount = count(*)
	FROM
		mmap.ProspectiveCohort
	WHERE
		SubmissionFileId = @SubmissionFileId;

	SELECT
		@OriginCount = isnull(sum(DistrictDenominator), 0)
	FROM
		#ProspectiveCohortEval;

	SELECT
		@Spacer = Replicate(@ReplicateChar, @ReplicateStatic + max(datalength(c)))
	FROM
		(
			SELECT
				c
			FROM
				(
					VALUES
					(@OrgName),
					(@FileName),
					(convert(varchar, @FileSubmitDate, 121)),
					(convert(varchar, @FileProcessDate, 121)),
					(convert(varchar, @CohortCount)),
					(convert(varchar, @OriginCount))
				) t (c)
			UNION ALL
			SELECT TOP 10
				DistrictName
			FROM
				#ProspectiveCohortEval
			ORDER BY
				DistrictDenominator desc
		) a;

	SET @Body = 
		'College Name:   ' + @OrgName + char(13) + char(10) +
		'File Name:      ' + @FileName + char(13) + char(10) +
		'Submit Date:    ' + convert(varchar, @FileSubmitDate, 121) + char(13) + char(10) +
		'Process Date:   ' + convert(varchar, @FileProcessDate, 121) + char(13) + char(10) +
		'Record Count:   ' + convert(varchar, @CohortCount) + char(13) + char(10) +
		'Origin Count:   ' + convert(varchar, @OriginCount) + char(13) + char(10);

	-- overall
	SELECT
		@Body += char(13) + char(10) +
		'File Summary' + char(13) + char(10) + 
		@Spacer + char(13) + char(10) +
		'NoGrade Count:  ' + convert(varchar, sum(NoGradeNumerator)) + char(13) + char(10) +
		'Grade09 Count:  ' + convert(varchar, sum(Grade09Numerator)) + char(13) + char(10) +
		'Grade10 Count:  ' + convert(varchar, sum(Grade10Numerator)) + char(13) + char(10) +
		'Grade11 Count:  ' + convert(varchar, sum(Grade11Numerator)) + char(13) + char(10) +
		'Grade12 Count:  ' + convert(varchar, sum(Grade12Numerator)) + char(13) + char(10) +
		'File Count:     ' + convert(varchar, sum(FileDenominator)) + char(13) + char(10) +
		'HS Match:       ' + 
			convert(varchar, sum(MatchNumerator)) + replicate(' ', 1 + len(sum(FileDenominator)) - len(sum(MatchNumerator))) + 
			' (' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * sum(MatchNumerator) / sum(FileDenominator)), 2)) + '%)' + char(13) + char(10) +
		'Engl Placement: ' + 
			convert(varchar, sum(PlacementRateEngl)) + replicate(' ', 1 + len(sum(FileDenominator)) - len(sum(PlacementRateEngl))) + 
			' (' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * sum(PlacementRateEngl) / sum(FileDenominator)), 2)) + '%)' + char(13) + char(10) +
		'Math Placement: ' + 
			convert(varchar, sum(PlacementRateMath)) + replicate(' ', 1 + len(sum(FileDenominator)) - len(sum(PlacementRateMath))) + 
			' (' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * sum(PlacementRateMath) / sum(FileDenominator)), 2)) + '%)' + char(13) + char(10)
	FROM
		(
			SELECT
				FileDenominator     = count(*),
				NoGradeNumerator    = sum(case when c.grade_level is null then 1 else 0 end),
				Grade09Numerator    = sum(case when c.grade_level = @Grade09 then 1 else 0 end),
				Grade10Numerator    = sum(case when c.grade_level = @Grade10 then 1 else 0 end),
				Grade11Numerator    = sum(case when c.grade_level = @Grade11 then 1 else 0 end),
				Grade12Numerator    = sum(case when c.grade_level = @Grade12 then 1 else 0 end),
				MatchNumerator      = sum(case when c.grade_level is not null then 1 else 0 end),
				PlacementRateEngl   = sum(case when c.engl_cb21 is not null then 1 else 0 end),
				PlacementRateMath   = sum(case when c.math_cb21 is not null then 1 else 0 end)
			FROM
				mmap.ProspectiveCohort c
			WHERE
				c.SubmissionFileId = @SubmissionFileId
		) a;

	-- districts
	SELECT
		@Body += char(13) + char(10) +
		'District Summary' + char(13) + char(10) + 
		@Spacer + char(13) + char(10) +
		'NoGrade Count:  ' + convert(varchar, sum(NoGradeNumerator)) + char(13) + char(10) +
		'Grade09 Count:  ' + convert(varchar, sum(Grade09Numerator)) + char(13) + char(10) +
		'Grade10 Count:  ' + convert(varchar, sum(Grade10Numerator)) + char(13) + char(10) +
		'Grade11 Count:  ' + convert(varchar, sum(Grade11Numerator)) + char(13) + char(10) +
		'Grade12 Count:  ' + convert(varchar, sum(Grade12Numerator)) + char(13) + char(10) +
		'District Count: ' + convert(varchar, sum(DistrictDenominator)) + char(13) + char(10) +
		'HS Match:       ' + 
			convert(varchar, sum(MatchNumerator)) + replicate(' ', 1 + len(sum(DistrictDenominator)) - len(sum(MatchNumerator))) + 
			' (' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * sum(MatchNumerator) / sum(DistrictDenominator)), 2)) + '%)' + char(13) + char(10) +
		'Engl Placement: ' + 
			convert(varchar, sum(PlacementRateEngl)) + replicate(' ', 1 + len(sum(DistrictDenominator)) - len(sum(PlacementRateEngl))) + 
			' (' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * sum(PlacementRateEngl) / sum(DistrictDenominator)), 2)) + '%)' + char(13) + char(10) +
		'Math Placement: ' + 
			convert(varchar, sum(PlacementRateMath)) + replicate(' ', 1 + len(sum(DistrictDenominator)) - len(sum(PlacementRateMath))) + 
			' (' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * sum(PlacementRateMath) / sum(DistrictDenominator)), 2)) + '%)' + char(13) + char(10),
		@PlacementRateEngl = round(convert(decimal(5,2), 100.00 * sum(PlacementRateEngl) / sum(DistrictDenominator)), 2),
		@PlacementRateMath = round(convert(decimal(5,2), 100.00 * sum(PlacementRateMath) / sum(DistrictDenominator)), 2)
	FROM
		#ProspectiveCohortEval;

	-- Top 10 districts
	SELECT TOP 10
		@Body += char(13) + char(10) +
		'Top 10 Districts (#' + convert(varchar, row_number() over(order by DistrictDenominator desc)) + ')' + char(13) + char(10) + 
		@Spacer + char(13) + char(10) +
		'District Name:  ' + DistrictName + char(13) + char(10) +
		'Course Year:    ' + isnull(StudentYearCode, '') + char(13) + char(10) +
		'Student Year:   ' + isnull(CourseYearCode, '') + char(13) + char(10) +
		'NoGrade Count:  ' + convert(varchar, NoGradeNumerator) + char(13) + char(10) +
		'Grade09 Count:  ' + convert(varchar, Grade09Numerator) + char(13) + char(10) +
		'Grade10 Count:  ' + convert(varchar, Grade10Numerator) + char(13) + char(10) +
		'Grade11 Count:  ' + convert(varchar, Grade11Numerator) + char(13) + char(10) +
		'Grade12 Count:  ' + convert(varchar, Grade12Numerator) + char(13) + char(10) +
		'District Count: ' + convert(varchar, DistrictDenominator) + char(13) + char(10) +
		'HS Match:       ' + 
			convert(varchar, MatchNumerator) + replicate(' ', 1 + len(DistrictDenominator) - len(MatchNumerator)) + 
			'(' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * MatchNumerator / DistrictDenominator), 2)) + '%)' + char(13) + char(10) +
		'Engl Placement: ' + 
			convert(varchar, PlacementRateEngl) + replicate(' ', 1 + len(DistrictDenominator) - len(PlacementRateEngl)) + 
			'(' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * PlacementRateEngl / DistrictDenominator), 2)) + '%)' + char(13) + char(10) +
		'Math Placement: ' + 
			convert(varchar, PlacementRateMath) + replicate(' ', 1 + len(DistrictDenominator) - len(PlacementRateMath)) + 
			'(' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * PlacementRateMath / DistrictDenominator), 2)) + '%)' + char(13) + char(10)
	FROM
		#ProspectiveCohortEval
		left outer join
		(
			SELECT
				CdsCode = d.DistrictCode,
				CourseYearCode = max(y1.YearCode),
				StudentYearCode = max(y2.YearCode)
			FROM
				(
					SELECT TOP 10
						DistrictCode
					FROM
						#ProspectiveCohortEval
					ORDER BY
						DistrictDenominator desc
				) d
				left outer join
				[PRO-DAT-SQL-01].calpass.dbo.ad_CourseCount c
					on left(c.School, 7) = d.DistrictCode
				left outer join
				[PRO-DAT-SQL-01].calpass.dbo.ad_StudentCount s
					on left(s.School, 7) = d.DistrictCode
				left outer join
				calpads.Year y1
					on y1.YearCodeAbbr = c.AcYear
				left outer join
				calpads.Year y2
					on y2.YearCodeAbbr = s.AcYear
			GROUP BY
				d.DistrictCode
		) a
			on a.CdsCode = DistrictCode
	ORDER BY
		DistrictDenominator desc;

	SELECT
		@Body += 
			case
				when row_number() over(order by (select 1)) = 1 then char(13) + char(10)
				else N''
			end +
			AgeCategory + replicate(' ', (4 + max(len(AgeCategory)) over()) - len(AgeCategory)) + 
			replicate(' ', (max(len(Records)) over()) - len(Records)) + Records +
			case
				when row_number() over(order by (select 1)) <> count(*) over() then char(13) + char(10)
				else N''
			end
	FROM
		(
			SELECT
				AgeCategory = 'AgeCateogry',
				Records     = 'Records',
				SortOrder   = 0,
				Ordinal     = 0
			UNION ALL
			SELECT
				AgeCategory = convert(varchar(255), a.Label),
				Records     = convert(varchar(255), count(*)),
				SortOrder   = 1,
				Ordinal     = a.AgeId
			FROM
				[PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedrop f
				inner join
				mmap.ProspectiveCohort c
						on c.SubmissionFileId = f.SubmissionFileId
				inner join
				calpads.Institution i
					on substring(i.CdsCode, 8, 6) = c.HighSchool
				inner join
				lbswp.Age a
					on a.LowerBound <= ((convert(integer, convert(char(8), f.SubmissionDateTime,112)) - convert(char(8), c.Birthdate,112)) / 10000)
					and a.UpperBound >= ((convert(integer, convert(char(8), f.SubmissionDateTime,112)) - convert(char(8), c.Birthdate,112)) / 10000)
			WHERE
				i.School is not null
				and c.SubmissionFileId = @SubmissionFileId
			GROUP BY
				a.AgeId,
				a.Label
		) a
	ORDER BY
		SortOrder,
		Ordinal;

	IF (object_id('tempdb.dbo.#ProspectiveCohortEval') is not null)
		BEGIN
			DROP TABLE #ProspectiveCohortEval;
		END;

	IF (@Body is null)
		BEGIN
			-- alert
			EXEC msdb.dbo.sp_send_dbmail
				@profile_name = @ProfileName,
				@recipients   = 'dlamoree@edresults.org',
				@subject      = 'Error',
				@body         = 'Body is null',
				@body_format  = 'HTML';
			RETURN;
		END;

	-- Formatting
	SET @Body = '<tt>' + @Body + '</tt>';
	SET @Body = replace(@Body, char(13) + char(10), @CRLF);
	SET @Body = replace(@Body, '  ', @Space + @Space);

	IF (@PlacementRateEngl < @Threshold OR @PlacementRateMath < @Threshold)
		BEGIN
			SET @Subject = 'MMAP :: Student Cohort :: District Placement Rate Less Than ' + convert(varchar, @Threshold) + '%';
		END;
	ELSE
		BEGIN
			SET @Subject = 'MMAP :: Student Cohort :: District Placement Rate Greater Than Or Equal To ' + convert(varchar, @Threshold) + '%';
		END;

	-- alert
	EXEC msdb.dbo.sp_send_dbmail
		@profile_name = @ProfileName,
		@recipients   = @Recipients,
		@subject      = @Subject,
		@body         = @Body,
		@body_format  = 'HTML';
END;