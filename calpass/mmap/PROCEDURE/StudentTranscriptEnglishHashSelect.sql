USE calpass;

GO

IF (object_id('mmap.StudentTranscriptEnglishHashSelect') is not null)
	BEGIN
		DROP PROCEDURE mmap.StudentTranscriptEnglishHashSelect;
	END;

GO

CREATE PROCEDURE
	mmap.StudentTranscriptEnglishHashSelect
	(
		@Student dbo.Student READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

BEGIN
	SELECT
		s.InterSegmentKey,
		Entity = e.ContentCode,
		Attribute = 'MarkPoints',
		Value = isnull(convert(decimal(4,3), sum(t.QualityPoints) / nullif(sum(t.CreditTry), 0)), 0)
	FROM
		@Student s
		cross apply
		dbo.HSTranscriptGet(s.InterSegmentKey) t
		inner join
		mmap.English e with(index=PK_English)
			on e.CourseCode = t.CourseCode
			and e.GradeCode = t.GradeCode
	GROUP BY
		s.InterSegmentKey,
		e.ContentCode
	HAVING
		isnull(convert(decimal(9,3), sum(t.QualityPoints) / nullif(sum(t.CreditTry), 0)), 0) <= 4;
END;
