USE calpass;

GO

IF (object_id('mmap.StudentTranscriptCgpaHashSelect') is not null)
	BEGIN
		DROP PROCEDURE mmap.StudentTranscriptCgpaHashSelect;
	END;

GO

CREATE PROCEDURE
	mmap.StudentTranscriptCgpaHashSelect
	(
		@Student dbo.Student READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

BEGIN
	SELECT
		s.InterSegmentKey,
		Entity = 'Cgpa',
		Attribute = max(t.GradeCode),
		Value = isnull(convert(decimal(4,3), sum(t.QualityPoints) / nullif(sum(t.CreditTry), 0)), 0)
	FROM
		@Student s
		cross apply
		dbo.HSTranscriptGet(s.InterSegmentKey) t
	GROUP BY
		s.InterSegmentKey
	HAVING
		isnull(convert(decimal(9,3), sum(t.QualityPoints) / nullif(sum(t.CreditTry), 0)), 0) <= 4;
END;