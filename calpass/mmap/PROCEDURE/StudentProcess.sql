CREATE PROCEDURE
	Mmap.StudentProcess
AS

BEGIN
	TRUNCATE TABLE Mmap.Student;

	INSERT INTO
		Mmap.Student
		(
			InterSegmentKey
		)
	SELECT
		s1.InterSegmentKey
	FROM
		comis.Student s1
		inner join
		dbo.Student s2
			on s2.InterSegmentKey = s1.InterSegmentKey
	WHERE
		s1.IsCollision = 0
		and s2.IsHighSchoolCollision = 0
		and s2.IsHighSchoolSection = 1;
END;