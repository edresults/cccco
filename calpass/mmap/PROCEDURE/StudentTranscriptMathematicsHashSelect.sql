USE calpass;

GO

IF (object_id('mmap.StudentTranscriptMathematicsHashSelect') is not null)
	BEGIN
		DROP PROCEDURE mmap.StudentTranscriptMathematicsHashSelect;
	END;

GO

CREATE PROCEDURE
	mmap.StudentTranscriptMathematicsHashSelect
	(
		@Student dbo.Student READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

BEGIN
	SELECT
		s.InterSegmentKey,
		Entity = 
			case
				when grouping(m.ContentCode) = 0 then m.ContentCode
				when grouping(m.ContentCode) = 1 then 'Mathematics'
			end,
		Attribute = 
			case
				when grouping(m.ContentCode) = 0 then 'MarkPoints'
				when grouping(m.ContentCode) = 1 then 'PromotedRank'
			end,
		Value = 
			case
				when grouping(m.ContentCode) = 0 then isnull(convert(decimal(4,3), sum(t.QualityPoints) / nullif(sum(t.CreditTry), 0)), 0)
				when grouping(m.ContentCode) = 1 then convert(decimal(4,3), max(case when t.IsPromoted = 1 then m.Rank else 0 end))
			end
	FROM
		@Student s
		cross apply
		dbo.HSTranscriptGet(s.InterSegmentKey) t
		inner join
		mmap.Mathematics m with(index=PK_Mathematics)
			on m.CourseCode = t.CourseCode
	GROUP BY
		s.InterSegmentKey,
		rollup(m.ContentCode)
	HAVING
		isnull(convert(decimal(9,3), sum(t.QualityPoints) / nullif(sum(t.CreditTry), 0)), 0) <= 4
END;
