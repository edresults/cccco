USE calpass;

GO

IF (object_id('mmap.RetrospectivePerformanceMerge') is not null)
	BEGIN
		DROP PROCEDURE mmap.RetrospectivePerformanceMerge;
	END;

GO

CREATE PROCEDURE
	mmap.RetrospectivePerformanceMerge
	(
		@InterSegment dbo.InterSegment READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Message nvarchar(2048),
	@ErrorCode int = 70099,
	@ErrorSeverity int = 1;

BEGIN
	BEGIN TRY
		SELECT
			i.InterSegmentKey,
			rp.DepartmentCode,
			rp.GradeCode,
			rp.QualityPoints,
			rp.CreditAttempted,
			rp.GradePointAverage,
			rp.GradePointAverageSans,
			rp.CumulativeQualityPoints,
			rp.CumulativeCreditAttempted,
			rp.CumulativeGradePointAverage,
			rp.CumulativeGradePointAverageSans,
			rp.FirstYearTermCode,
			rp.LastYearTermCode,
			rp.IsFirst,
			rp.IsLast
		INTO
			#RetrospectivePerformance
		FROM
			@InterSegment i
			cross apply
			Mmap.RetrospectivePerformanceGet(i.InterSegmentKey) rp;

		DELETE
			t
		FROM
			Mmap.RetrospectivePerformance t
			inner join
			@InterSegment i
				on i.InterSegmentKey = t.InterSegmentKey
			left outer join
			#RetrospectivePerformance s
				on  s.InterSegmentKey = t.InterSegmentKey
				and s.DepartmentCode  = t.DepartmentCode
				and s.GradeCode       = t.GradeCode
		WHERE
			s.InterSegmentKey is null;

		UPDATE
			t
		SET
			t.QualityPoints                   = s.QualityPoints,
			t.CreditAttempted                 = s.CreditAttempted,
			t.GradePointAverage               = s.GradePointAverage,
			t.GradePointAverageSans           = s.GradePointAverageSans,
			t.CumulativeQualityPoints         = s.CumulativeQualityPoints,
			t.CumulativeCreditAttempted       = s.CumulativeCreditAttempted,
			t.CumulativeGradePointAverage     = s.CumulativeGradePointAverage,
			t.CumulativeGradePointAverageSans = s.CumulativeGradePointAverageSans,
			t.FirstYearTermCode               = s.FirstYearTermCode,
			t.LastYearTermCode                = s.LastYearTermCode,
			t.IsFirst                         = s.IsFirst,
			t.IsLast                          = s.IsLast
		FROM
			Mmap.RetrospectivePerformance t
			inner join
			#RetrospectivePerformance s
				on  s.InterSegmentKey = t.InterSegmentKey
				and s.DepartmentCode  = t.DepartmentCode
				and s.GradeCode       = t.GradeCode
		WHERE
			t.QualityPoints                      <> s.QualityPoints
			or t.CreditAttempted                 <> s.CreditAttempted
			or t.GradePointAverage               <> s.GradePointAverage
			or t.GradePointAverageSans           <> s.GradePointAverageSans
			or t.CumulativeQualityPoints         <> s.CumulativeQualityPoints
			or t.CumulativeCreditAttempted       <> s.CumulativeCreditAttempted
			or t.CumulativeGradePointAverage     <> s.CumulativeGradePointAverage
			or t.CumulativeGradePointAverageSans <> s.CumulativeGradePointAverageSans
			or t.FirstYearTermCode               <> s.FirstYearTermCode
			or t.LastYearTermCode                <> s.LastYearTermCode
			or t.IsFirst                         <> s.IsFirst
			or t.IsLast                          <> s.IsLast;

		INSERT INTO
			Mmap.RetrospectivePerformance
			(
				InterSegmentKey,
				DepartmentCode,
				GradeCode,
				QualityPoints,
				CreditAttempted,
				GradePointAverage,
				GradePointAverageSans,
				CumulativeQualityPoints,
				CumulativeCreditAttempted,
				CumulativeGradePointAverage,
				CumulativeGradePointAverageSans,
				FirstYearTermCode,
				LastYearTermCode,
				IsFirst,
				IsLast
			)
		SELECT
			s.InterSegmentKey,
			s.DepartmentCode,
			s.GradeCode,
			s.QualityPoints,
			s.CreditAttempted,
			s.GradePointAverage,
			s.GradePointAverageSans,
			s.CumulativeQualityPoints,
			s.CumulativeCreditAttempted,
			s.CumulativeGradePointAverage,
			s.CumulativeGradePointAverageSans,
			s.FirstYearTermCode,
			s.LastYearTermCode,
			s.IsFirst,
			s.IsLast
		FROM
			#RetrospectivePerformance s
		WHERE
			not exists (
				SELECT
					NULL
				FROM
					Mmap.RetrospectivePerformance t
				WHERE
					t.InterSegmentKey    = s.InterSegmentKey
					and t.DepartmentCode = s.DepartmentCode
					and t.GradeCode      = s.GradeCode
			);
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message =
			nchar(9) + N'Error Procedure: ' + upper(object_name(@@PROCID)) + char(13) + char(10) +
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		-- put error message
		THROW @ErrorCode, @Message, @ErrorSeverity;
	END CATCH;
END;