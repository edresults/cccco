USE calpass;

GO

IF (object_id('mmap.StudentTranscriptProcess') is not null)
	BEGIN
		DROP PROCEDURE mmap.StudentTranscriptProcess;
	END;

GO

CREATE PROCEDURE
	mmap.StudentTranscriptProcess
	(
		@Student dbo.Student READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@StudentTranscriptSummaryHash mmap.StudentTranscriptSummaryHash;

BEGIN
	-- English
	INSERT
		@StudentTranscriptSummaryHash
		(
			InterSegmentKey,
			Entity,
			Attribute,
			Value
		)
	EXECUTE mmap.StudentTranscriptEnglishHashSelect
		@Student = @Student;

	-- Mathematics
	INSERT
		@StudentTranscriptSummaryHash
		(
			InterSegmentKey,
			Entity,
			Attribute,
			Value
		)
	EXECUTE mmap.StudentTranscriptMathematicsHashSelect
		@Student = @Student;

	-- Cgpa
	INSERT
		@StudentTranscriptSummaryHash
		(
			InterSegmentKey,
			Entity,
			Attribute,
			Value
		)
	EXECUTE mmap.StudentTranscriptCgpaHashSelect
		@Student = @Student;

	-- Merge Hash
	EXECUTE mmap.StudentTranscriptSummaryHashMerge
		@StudentTranscriptSummaryHash = @StudentTranscriptSummaryHash;

	-- Merge Flat
	EXECUTE mmap.StudentTranscriptSummaryMerge
		@StudentTranscriptSummaryHash = @StudentTranscriptSummaryHash;
	PRINT GETDATE();
	-- Merge Placement
	EXECUTE mmap.ProspectivePlacementMerge
		@StudentTranscriptSummaryHash = @StudentTranscriptSummaryHash;
	PRINT GETDATE();
END;