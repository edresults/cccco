USE calpass;

GO

IF (object_id('mmap.p_ValidationAssessment_Delsert') is not null)
	BEGIN
		DROP PROCEDURE mmap.p_ValidationAssessment_Delsert;
	END;

GO

CREATE PROCEDURE
	mmap.p_ValidationAssessment_Delsert
	(
		@SubmissionFileId int,
		@StageSchemaTableName nvarchar(517)
	)
AS

DECLARE
	@Message nvarchar(2048),
	@Validate bit = 0,
	@Sql nvarchar(max),
	@Needle varchar(255) = char(13) + char(10) + char(9) + char(9),
	@Replace varchar(255) = char(13) + char(10);

BEGIN
	-- Validate inputs
	SELECT
		@Validate = 1
	FROM
		sys.tables
	WHERE
		object_id = object_id(@StageSchemaTableName);
	-- process validation
	IF (@Validate = 0)
		BEGIN
			SET @Message = N'Table not found';
			THROW 70099, @Message, 1;
		END;
	-- set dynamic sql
	SET @Sql = replace(N'
		DELETE
		FROM
			mmap.ValidationAssessment with (tablockx)
		WHERE
			SubmissionFileId = @SubmissionFileId', @Needle, @Replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@Sql,
		N'@SubmissionFileId int',
		@SubmissionFileId = @SubmissionFileId;
	-- set dynamic sql
	SET @Sql = replace(N'
		INSERT INTO
			mmap.ValidationAssessment with (tablockx)
			(
				CollegeId,
				StudentId,
				IdStatusCode,
				InterSegmentKey,
				Birthdate,
				Gender,
				NameFirst,
				NameLast,
				TermCode,
				PlacementTypeId,
				MMMethodId,
				MMEligibilityId,
				MMBasisId,
				TestSubjectCode,
				TestDescription,
				TestDate,
				TestScoreDescriptionId,
				TestScore,
				TestScoreCompensatory,
				TestPlacementLevelDescription,
				TestPlacementLevelCode,
				TestPlacementMathCourseTransferArray,
				MMPlacementLevelDescription,
				MMPlacementLevelCode,
				MMPlacementMathCourseTransferArray,
				FinalPlacementLevelDescription,
				FinalPlacementLevelCode,
				FinalPlacementMathCourseTransferArray,
				SubmissionFileId,
				SubmissionFileRecordNumber
			)
		SELECT
			CollegeId,
			StudentId = case
					when IdStatusCode = ''S'' then calpass.dbo.Get1289Encryption(StudentId, '''')
					else StudentId
				end,
			IdStatusCode,
			InterSegmentKey = hashbytes(
					''SHA2_512'', 
					upper(convert(nchar(3), NameFirst)) + 
					upper(convert(nchar(3), NameLast)) + 
					upper(convert(nchar(1), Gender)) + 
					convert(nchar(8), Birthdate)
				),
			Birthdate,
			Gender,
			NameFirst,
			NameLast,
			TermCode,
			PlacementTypeId,
			MMMethodId,
			MMEligibilityId,
			MMBasisId,
			TestSubjectCode,
			TestDescription,
			TestDate,
			TestScoreDescriptionId,
			TestScore,
			TestScoreCompensatory,
			TestPlacementLevelDescription,
			TestPlacementLevelCode,
			TestPlacementMathCourseTransferArray,
			MMPlacementLevelDescription,
			MMPlacementLevelCode,
			MMPlacementMathCourseTransferArray,
			FinalPlacementLevelDescription,
			FinalPlacementLevelCode,
			FinalPlacementMathCourseTransferArray,
			@SubmissionFileId,
			SubmissionFileRecordNumber
		FROM
			' + @StageSchemaTableName + ';', @Needle, @Replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@Sql,
		N'@SubmissionFileId int',
		@SubmissionFileId = @SubmissionFileId;
END;