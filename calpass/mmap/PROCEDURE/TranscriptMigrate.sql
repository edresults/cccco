USE calpass;

GO

IF (object_id('mmap.TranscriptMigrate') is not null)
	BEGIN
		DROP PROCEDURE mmap.TranscriptMigrate;
	END;

GO

CREATE PROCEDURE
	mmap.TranscriptMigrate
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

DECLARE
	@Transcript mmap.Transcript,
	@False      bit = 0,
	@True       bit = 1;

BEGIN
	INSERT INTO
		@Transcript
		(
			InterSegmentKey,
			IsHighSchoolGrade09,
			IsHighSchoolGrade10,
			IsHighSchoolGrade11,
			IsHighSchoolGrade12,
			CumulativeGradePointAverage,
			English,
			PreAlgebra,
			AlgebraI,
			Geometry,
			AlgebraII,
			Trigonometry,
			PreCalculus,
			[Statistics],
			Calculus
		)
	SELECT
		InterSegmentKey,
		IsHighSchoolGrade09,
		IsHighSchoolGrade10,
		IsHighSchoolGrade11,
		IsHighSchoolGrade12,
		CumulativeGradePointAverage,
		English,
		PreAlgebra,
		AlgebraI,
		Geometry,
		AlgebraII,
		Trigonometry,
		PreCalculus,
		[Statistics],
		Calculus
	FROM
		mmap.Transcript
	WHERE
		IsEscrow = @True;

	UPDATE
		t
	SET
		t.IsHighSchoolGrade09         = tt.IsHighSchoolGrade09,
		t.IsHighSchoolGrade10         = tt.IsHighSchoolGrade10,
		t.IsHighSchoolGrade11         = tt.IsHighSchoolGrade11,
		t.IsHighSchoolGrade12         = tt.IsHighSchoolGrade12,
		t.CumulativeGradePointAverage = tt.CumulativeGradePointAverage,
		t.English                     = tt.English,
		t.PreAlgebra                  = tt.PreAlgebra,
		t.AlgebraI                    = tt.AlgebraI,
		t.Geometry                    = tt.Geometry,
		t.AlgebraII                   = tt.AlgebraII,
		t.Trigonometry                = tt.Trigonometry,
		t.PreCalculus                 = tt.PreCalculus,
		t.[Statistics]                = tt.[Statistics],
		t.Calculus                    = tt.Calculus
	FROM
		CPPDELSQLP01.CPP_API.dbo.Transcript t
		inner join
		@Transcript tt
			on tt.InterSegmentKey = t.InterSegmentKey;

	INSERT
		CPPDELSQLP01.CPP_API.dbo.Transcript
		(
			InterSegmentKey,
			IsHighSchoolGrade09,
			IsHighSchoolGrade10,
			IsHighSchoolGrade11,
			IsHighSchoolGrade12,
			CumulativeGradePointAverage,
			English,
			PreAlgebra,
			AlgebraI,
			Geometry,
			AlgebraII,
			Trigonometry,
			PreCalculus,
			[Statistics],
			Calculus
		)
	SELECT
		tt.InterSegmentKey,
		tt.IsHighSchoolGrade09,
		tt.IsHighSchoolGrade10,
		tt.IsHighSchoolGrade11,
		tt.IsHighSchoolGrade12,
		tt.CumulativeGradePointAverage,
		tt.English,
		tt.PreAlgebra,
		tt.AlgebraI,
		tt.Geometry,
		tt.AlgebraII,
		tt.Trigonometry,
		tt.PreCalculus,
		tt.[Statistics],
		tt.Calculus
	FROM
		@Transcript tt
	WHERE
		not exists (
			SELECT
				0
			FROM
				CPPDELSQLP01.CPP_API.dbo.Transcript t
			WHERE
				tt.InterSegmentKey = t.InterSegmentKey
		);

	UPDATE
		t
	SET
		t.IsHighSchoolGrade09         = tt.IsHighSchoolGrade09,
		t.IsHighSchoolGrade10         = tt.IsHighSchoolGrade10,
		t.IsHighSchoolGrade11         = tt.IsHighSchoolGrade11,
		t.IsHighSchoolGrade12         = tt.IsHighSchoolGrade12,
		t.CumulativeGradePointAverage = tt.CumulativeGradePointAverage,
		t.English                     = tt.English,
		t.PreAlgebra                  = tt.PreAlgebra,
		t.AlgebraI                    = tt.AlgebraI,
		t.Geometry                    = tt.Geometry,
		t.AlgebraII                   = tt.AlgebraII,
		t.Trigonometry                = tt.Trigonometry,
		t.PreCalculus                 = tt.PreCalculus,
		t.[Statistics]                = tt.[Statistics],
		t.Calculus                    = tt.Calculus
	FROM
		CPPDELSQLP01.CPP_API.mmap.Transcript t
		inner join
		@Transcript tt
			on tt.InterSegmentKey = t.StudentId;

	INSERT
		CPPDELSQLP01.CPP_API.mmap.Transcript
		(
			StudentId,
			DataSource,
			IsHighSchoolGrade09,
			IsHighSchoolGrade10,
			IsHighSchoolGrade11,
			IsHighSchoolGrade12,
			CumulativeGradePointAverage,
			English,
			PreAlgebra,
			AlgebraI,
			Geometry,
			AlgebraII,
			Trigonometry,
			PreCalculus,
			[Statistics],
			Calculus
		)
	SELECT
		tt.InterSegmentKey,
		DataSource = 2,
		tt.IsHighSchoolGrade09,
		tt.IsHighSchoolGrade10,
		tt.IsHighSchoolGrade11,
		tt.IsHighSchoolGrade12,
		tt.CumulativeGradePointAverage,
		tt.English,
		tt.PreAlgebra,
		tt.AlgebraI,
		tt.Geometry,
		tt.AlgebraII,
		tt.Trigonometry,
		tt.PreCalculus,
		tt.[Statistics],
		tt.Calculus
	FROM
		@Transcript tt
	WHERE
		not exists (
			SELECT
				0
			FROM
				CPPDELSQLP01.CPP_API.mmap.Transcript t
			WHERE
				tt.InterSegmentKey = t.StudentId
		);

	UPDATE
		t
	SET
		t.IsEscrow = @False
	FROM
		mmap.Transcript t
		inner join
		@Transcript tt
			on tt.InterSegmentKey = t.InterSegmentKey;
END;