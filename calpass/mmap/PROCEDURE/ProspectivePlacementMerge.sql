USE calpass;

GO

IF (object_id('mmap.ProspectivePlacementMerge') is not null)
	BEGIN
		DROP PROCEDURE mmap.ProspectivePlacementMerge;
	END;

GO

CREATE PROCEDURE
	mmap.ProspectivePlacementMerge
	(
		@StudentTranscriptSummaryHash mmap.StudentTranscriptSummaryHash READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

BEGIN
	MERGE
		mmap.ProspectivePlacement t
	USING
		(
			SELECT
				InterSegmentKey,
				EnglishY = isnull(EnglishY, 0),
				EnglishA = isnull(EnglishA, 0),
				EnglishB = isnull(EnglishB, 0),
				EnglishC = isnull(EnglishC, 0),
				PreAlgebra = isnull(PreAlgebra, 0),
				AlgebraI = isnull(AlgebraI, 0),
				AlgebraII = isnull(AlgebraII, 0),
				MathGE = isnull(MathGE, 0),
				[Statistics] = isnull([Statistics], 0),
				CollegeAlgebra = isnull(CollegeAlgebra, 0),
				Trigonometry = isnull(Trigonometry, 0),
				PreCalculus = isnull(PreCalculus, 0),
				CalculusI = isnull(CalculusI, 0),
				ReadingT_UboundY = isnull(ReadingT_UboundY, 0),
				ReadingY_UboundY = isnull(ReadingY_UboundY, 0),
				ReadingA_UboundY = isnull(ReadingA_UboundY, 0),
				ReadingB_UboundY = isnull(ReadingB_UboundY, 0),
				ReadingC_UboundY = isnull(ReadingC_UboundY, 0),
				ReadingT_UboundA = isnull(ReadingT_UboundA, 0),
				ReadingA_UboundA = isnull(ReadingA_UboundA, 0),
				ReadingB_UboundA = isnull(ReadingB_UboundA, 0),
				ReadingC_UboundA = isnull(ReadingC_UboundA, 0),
				ReadingT_UboundB = isnull(ReadingT_UboundB, 0),
				ReadingB_UboundB = isnull(ReadingB_UboundB, 0),
				ReadingC_UboundB = isnull(ReadingC_UboundB, 0),
				EslY_UboundY = isnull(EslY_UboundY, 0),
				EslA_UboundY = isnull(EslA_UboundY, 0),
				EslB_UboundY = isnull(EslB_UboundY, 0),
				EslA_UboundA = isnull(EslA_UboundA, 0),
				EslB_UboundA = isnull(EslB_UboundA, 0),
				EslC_UboundA = isnull(EslC_UboundA, 0),
				EslB_UboundB = isnull(EslB_UboundB, 0),
				EslC_UboundB = isnull(EslC_UboundB, 0),
				EslD_UboundB = isnull(EslD_UboundB, 0)
			FROM
				(
					SELECT DISTINCT
						s.InterSegmentKey,
						c.Name,
						True = 1
					FROM
						mmap.Course c
						inner join
						mmap.ProspectiveCourse pc
							on pc.CourseId = c.Id
						inner join
						mmap.ProspectiveBranch pb
							on pb.ProspectiveCourseId = pc.Id
						inner join
						mmap.ProspectiveLeaf pl
							on pl.ProspectiveBranchId = pb.Id
						inner join
						@StudentTranscriptSummaryHash s
							on s.Entity = pl.Entity
							and s.Attribute = pl.Attribute
					GROUP BY
						s.InterSegmentKey,
						c.Name,
						pb.Id,
						pb.LeafCount
					HAVING
						sum(case when s.Value >= pl.Value then 1 else 0 end) = pb.LeafCount
				) p
			PIVOT
				(
					max(True) FOR Name IN (
						EnglishY,
						EnglishA,
						EnglishB,
						EnglishC,
						PreAlgebra,
						AlgebraI,
						AlgebraII,
						MathGE,
						[Statistics],
						CollegeAlgebra,
						Trigonometry,
						PreCalculus,
						CalculusI,
						ReadingT_UboundY,
						ReadingY_UboundY,
						ReadingA_UboundY,
						ReadingB_UboundY,
						ReadingC_UboundY,
						ReadingT_UboundA,
						ReadingA_UboundA,
						ReadingB_UboundA,
						ReadingC_UboundA,
						ReadingT_UboundB,
						ReadingB_UboundB,
						ReadingC_UboundB,
						EslY_UboundY,
						EslA_UboundY,
						EslB_UboundY,
						EslA_UboundA,
						EslB_UboundA,
						EslC_UboundA,
						EslB_UboundB,
						EslC_UboundB,
						EslD_UboundB
					)
				) u
		) s
	ON
		s.InterSegmentKey = t.InterSegmentKey
	WHEN MATCHED THEN
		UPDATE SET
			t.EnglishY = s.EnglishY,
			t.EnglishA = s.EnglishA,
			t.EnglishB = s.EnglishB,
			t.EnglishC = s.EnglishC,
			t.PreAlgebra = s.PreAlgebra,
			t.AlgebraI = s.AlgebraI,
			t.AlgebraII = s.AlgebraII,
			t.MathGE = s.MathGE,
			t.[Statistics] = s.[Statistics],
			t.CollegeAlgebra = s.CollegeAlgebra,
			t.Trigonometry = s.Trigonometry,
			t.PreCalculus = s.PreCalculus,
			t.CalculusI = s.CalculusI,
			t.ReadingT_UboundY = s.ReadingT_UboundY,
			t.ReadingY_UboundY = s.ReadingY_UboundY,
			t.ReadingA_UboundY = s.ReadingA_UboundY,
			t.ReadingB_UboundY = s.ReadingB_UboundY,
			t.ReadingC_UboundY = s.ReadingC_UboundY,
			t.ReadingT_UboundA = s.ReadingT_UboundA,
			t.ReadingA_UboundA = s.ReadingA_UboundA,
			t.ReadingB_UboundA = s.ReadingB_UboundA,
			t.ReadingC_UboundA = s.ReadingC_UboundA,
			t.ReadingT_UboundB = s.ReadingT_UboundB,
			t.ReadingB_UboundB = s.ReadingB_UboundB,
			t.ReadingC_UboundB = s.ReadingC_UboundB,
			t.EslY_UboundY = s.EslY_UboundY,
			t.EslA_UboundY = s.EslA_UboundY,
			t.EslB_UboundY = s.EslB_UboundY,
			t.EslA_UboundA = s.EslA_UboundA,
			t.EslB_UboundA = s.EslB_UboundA,
			t.EslC_UboundA = s.EslC_UboundA,
			t.EslB_UboundB = s.EslB_UboundB,
			t.EslC_UboundB = s.EslC_UboundB,
			t.EslD_UboundB = s.EslD_UboundB
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				InterSegmentKey,
				EnglishY,
				EnglishA,
				EnglishB,
				EnglishC,
				PreAlgebra,
				AlgebraI,
				AlgebraII,
				MathGE,
				[Statistics],
				CollegeAlgebra,
				Trigonometry,
				PreCalculus,
				CalculusI,
				ReadingT_UboundY,
				ReadingY_UboundY,
				ReadingA_UboundY,
				ReadingB_UboundY,
				ReadingC_UboundY,
				ReadingT_UboundA,
				ReadingA_UboundA,
				ReadingB_UboundA,
				ReadingC_UboundA,
				ReadingT_UboundB,
				ReadingB_UboundB,
				ReadingC_UboundB,
				EslY_UboundY,
				EslA_UboundY,
				EslB_UboundY,
				EslA_UboundA,
				EslB_UboundA,
				EslC_UboundA,
				EslB_UboundB,
				EslC_UboundB,
				EslD_UboundB
			)
		VALUES
			(
				s.InterSegmentKey,
				s.EnglishY,
				s.EnglishA,
				s.EnglishB,
				s.EnglishC,
				s.PreAlgebra,
				s.AlgebraI,
				s.AlgebraII,
				s.MathGE,
				s.[Statistics],
				s.CollegeAlgebra,
				s.Trigonometry,
				s.PreCalculus,
				s.CalculusI,
				s.ReadingT_UboundY,
				s.ReadingY_UboundY,
				s.ReadingA_UboundY,
				s.ReadingB_UboundY,
				s.ReadingC_UboundY,
				s.ReadingT_UboundA,
				s.ReadingA_UboundA,
				s.ReadingB_UboundA,
				s.ReadingC_UboundA,
				s.ReadingT_UboundB,
				s.ReadingB_UboundB,
				s.ReadingC_UboundB,
				s.EslY_UboundY,
				s.EslA_UboundY,
				s.EslB_UboundY,
				s.EslA_UboundA,
				s.EslB_UboundA,
				s.EslC_UboundA,
				s.EslB_UboundB,
				s.EslC_UboundB,
				s.EslD_UboundB
			);
END;