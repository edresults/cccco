USE calpass;

GO

IF (object_id('mmap.ProspectiveCohortNewEval') is not null)
	BEGIN
		DROP PROCEDURE mmap.ProspectiveCohortNewEval;
	END;

GO

CREATE PROCEDURE
	mmap.ProspectiveCohortNewEval
	(
		@SubmissionFileId int,
		@Output nvarchar(max) = null out
	)
AS

DECLARE
	-- constants
	@ReplicateStatic int = 16,
	@ReplicateChar char(1) = '-',
	@Grade09 char(2) = '09',
	@Grade10 char(2) = '10',
	@Grade11 char(2) = '11',
	@Grade12 char(2) = '12',
	@MatchType char(8) = 'No Match',
	@StatusType char(6) = 'Active',
	-- variables
	@Spacer varchar(8000),
	@OrgName varchar(255),
	@FileName varchar(255),
	@FileSubmitDate datetime,
	@FileProcessDate datetime,
	@CohortCount int,
	@OriginCount int,
	@OutputStatus bit;

	SET NOCOUNT ON;
	
BEGIN
	
	IF (@Output is null)
		BEGIN
			SET @OutputStatus = 0;
			SET @Output = N'';
		END
	ELSE
		BEGIN
			SET @OutputStatus = 1;
			SET @Output = N'';
		END;
	
	SELECT
		@OrgName = o.OrganizationName
	FROM
		[PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedrop f
		inner join
		Organization o
			on o.OrganizationId = f.OrganizationId
	WHERE
		f.SubmissionFileId = @SubmissionFileId;

	SELECT
		@FileName = substring(OriginalFileName, charindex(char(46), OriginalFileName) + 1, datalength(OriginalFileName)),
		@FileSubmitDate = SubmissionDateTime,
		@FileProcessDate = ProcessedDateTime
	FROM
		[PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedrop
	WHERE
		SubmissionFileId = @SubmissionFileId;

	CREATE TABLE
		#ProspectiveCohortEval
		(
			DistrictCode char(7) not null,
			DistrictName varchar(90) not null,
			DistrictDenominator int not null,
			NoGradeNumerator int not null,
			Grade09Numerator int not null,
			Grade10Numerator int not null,
			Grade11Numerator int not null,
			Grade12Numerator int not null,
			MatchNumerator int not null,
			PlacementRateEngl int not null,
			PlacementRateMath int not null
		);

	INSERT INTO
		#ProspectiveCohortEval
		(
			DistrictCode,
			DistrictName,
			DistrictDenominator,
			NoGradeNumerator,
			Grade09Numerator,
			Grade10Numerator,
			Grade11Numerator,
			Grade12Numerator,
			MatchNumerator,
			PlacementRateEngl,
			PlacementRateMath
		)
	SELECT
		DistrictCode        = substring(i.CdsCode, 1, 7),
		DistrictName        = i.District,
		DistrictDenominator = sum(case when i.CdsCode is not null then 1 else 0 end),
		NoGradeNumerator    = sum(case when c.grade_level is null then 1 else 0 end),
		Grade09Numerator    = sum(case when c.grade_level = @Grade09 then 1 else 0 end),
		Grade10Numerator    = sum(case when c.grade_level = @Grade10 then 1 else 0 end),
		Grade11Numerator    = sum(case when c.grade_level = @Grade11 then 1 else 0 end),
		Grade12Numerator    = sum(case when c.grade_level = @Grade12 then 1 else 0 end),
		MatchNumerator      = sum(case when c.match_type != @MatchType then 1 else 0 end),
		PlacementRateEngl   = sum(case when c.engl_cb21 is not null then 1 else 0 end),
		PlacementRateMath   = sum(case when c.math_cb21 is not null then 1 else 0 end)
	FROM
		mmap.ProspectiveCohort c
		inner join
		calpads.Institution i
			on substring(i.CdsCode, 8, 6) = c.HighSchool
	WHERE
		c.SubmissionFileId = @SubmissionFileId
		and i.School is not null
		and i.StatusType = @StatusType
		and not exists (
			SELECT
				1
			FROM
				comis.studntid id
			WHERE
				id.InterSegmentKey = c.Derkey1
				and id.college_id = c.CollegeId
		)
	GROUP BY
		substring(i.CdsCode, 1, 7),
		i.District;

	SELECT
		@CohortCount = count(*)
	FROM
		mmap.ProspectiveCohort c
	WHERE
		c.SubmissionFileId = @SubmissionFileId
		and not exists (
			SELECT
				1
			FROM
				comis.studntid id
			WHERE
				id.InterSegmentKey = c.Derkey1
				and id.college_id = c.CollegeId
		);

	SELECT
		@OriginCount = sum(DistrictDenominator)
	FROM
		#ProspectiveCohortEval;

	SELECT
		@Spacer = Replicate(@ReplicateChar, @ReplicateStatic + max(datalength(c)))
	FROM
		(
			SELECT
				c
			FROM
				(
					VALUES
					(@OrgName),
					(@FileName),
					(convert(varchar, @FileSubmitDate, 121)),
					(convert(varchar, @FileProcessDate, 121)),
					(convert(varchar, @CohortCount)),
					(convert(varchar, @OriginCount))
				) t (c)
			UNION ALL
			SELECT TOP 10
				DistrictName
			FROM
				#ProspectiveCohortEval
			ORDER BY
				DistrictDenominator desc
		) a;

	SET @Output = 
		'College Name:   ' + @OrgName + char(13) + char(10) +
		'File Name:      ' + @FileName + char(13) + char(10) +
		'Submit Date:    ' + convert(varchar, @FileSubmitDate, 121) + char(13) + char(10) +
		'Process Date:   ' + convert(varchar, @FileProcessDate, 121) + char(13) + char(10) +
		'Record Count:   ' + convert(varchar, @CohortCount) + char(13) + char(10) +
		'Origin Count:   ' + convert(varchar, @OriginCount) + char(13) + char(10);

	-- overall
	SELECT
		@Output += char(13) + char(10) +
		'File Summary' + char(13) + char(10) + 
		@Spacer + char(13) + char(10) +
		'NoGrade Count:  ' + convert(varchar, sum(NoGradeNumerator)) + char(13) + char(10) +
		'Grade09 Count:  ' + convert(varchar, sum(Grade09Numerator)) + char(13) + char(10) +
		'Grade10 Count:  ' + convert(varchar, sum(Grade10Numerator)) + char(13) + char(10) +
		'Grade11 Count:  ' + convert(varchar, sum(Grade11Numerator)) + char(13) + char(10) +
		'Grade12 Count:  ' + convert(varchar, sum(Grade12Numerator)) + char(13) + char(10) +
		'File Count:     ' + convert(varchar, sum(FileDenominator)) + char(13) + char(10) +
		'Match Rate:     ' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * sum(MatchNumerator) / sum(FileDenominator)), 2)) + '%' + char(13) + char(10) +
		'English Rate:   ' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * sum(PlacementRateEngl) / sum(FileDenominator)), 2)) + '%' + char(13) + char(10) +
		'Math Rate:      ' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * sum(PlacementRateMath) / sum(FileDenominator)), 2)) + '%' + char(13) + char(10)
	FROM
		(
			SELECT
				FileDenominator     = count(*),
				NoGradeNumerator    = sum(case when c.grade_level is null then 1 else 0 end),
				Grade09Numerator    = sum(case when c.grade_level = @Grade09 then 1 else 0 end),
				Grade10Numerator    = sum(case when c.grade_level = @Grade10 then 1 else 0 end),
				Grade11Numerator    = sum(case when c.grade_level = @Grade11 then 1 else 0 end),
				Grade12Numerator    = sum(case when c.grade_level = @Grade12 then 1 else 0 end),
				MatchNumerator      = sum(case when c.grade_level is not null then 1 else 0 end),
				PlacementRateEngl   = sum(case when c.engl_cb21 is not null then 1 else 0 end),
				PlacementRateMath   = sum(case when c.math_cb21 is not null then 1 else 0 end)
			FROM
				mmap.ProspectiveCohort c
			WHERE
				c.SubmissionFileId = @SubmissionFileId
				and not exists (
					SELECT
						1
					FROM
						comis.studntid id
					WHERE
						id.InterSegmentKey = c.Derkey1
						and id.college_id = c.CollegeId
				)
		) a;

	-- districts
	SELECT
		@Output += char(13) + char(10) +
		'District Summary' + char(13) + char(10) + 
		@Spacer + char(13) + char(10) +
		'NoGrade Count:  ' + convert(varchar, sum(NoGradeNumerator)) + char(13) + char(10) +
		'Grade09 Count:  ' + convert(varchar, sum(Grade09Numerator)) + char(13) + char(10) +
		'Grade10 Count:  ' + convert(varchar, sum(Grade10Numerator)) + char(13) + char(10) +
		'Grade11 Count:  ' + convert(varchar, sum(Grade11Numerator)) + char(13) + char(10) +
		'Grade12 Count:  ' + convert(varchar, sum(Grade12Numerator)) + char(13) + char(10) +
		'District Count: ' + convert(varchar, sum(DistrictDenominator)) + char(13) + char(10) +
		'Match Rate:     ' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * sum(MatchNumerator) / sum(DistrictDenominator)), 2)) + '%' + char(13) + char(10) +
		'English Rate:   ' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * sum(PlacementRateEngl) / sum(DistrictDenominator)), 2)) + '%' + char(13) + char(10) +
		'Math Rate:      ' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * sum(PlacementRateMath) / sum(DistrictDenominator)), 2)) + '%' + char(13) + char(10)
	FROM
		#ProspectiveCohortEval;

	-- Top 10 districts
	SELECT TOP 10
		@Output += char(13) + char(10) +
		'Top 10 Districts (#' + convert(varchar, row_number() over(order by DistrictDenominator desc)) + ')' + char(13) + char(10) + 
		@Spacer + char(13) + char(10) +
		'District Name:  ' + DistrictName + char(13) + char(10) +
		'NoGrade Count:  ' + convert(varchar, NoGradeNumerator) + char(13) + char(10) +
		'Grade09 Count:  ' + convert(varchar, Grade09Numerator) + char(13) + char(10) +
		'Grade10 Count:  ' + convert(varchar, Grade10Numerator) + char(13) + char(10) +
		'Grade11 Count:  ' + convert(varchar, Grade11Numerator) + char(13) + char(10) +
		'Grade12 Count:  ' + convert(varchar, Grade12Numerator) + char(13) + char(10) +
		'District Count: ' + convert(varchar, DistrictDenominator) + char(13) + char(10) +
		'Match Rate:     ' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * MatchNumerator / DistrictDenominator), 2)) + '%' + char(13) + char(10) +
		'English Rate:   ' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * PlacementRateEngl / DistrictDenominator), 2)) + '%' + char(13) + char(10) +
		'Math Rate:      ' + convert(varchar(6), round(convert(decimal(5,2), 100.00 * PlacementRateMath / DistrictDenominator), 2)) + '%' + char(13) + char(10)
	FROM
		#ProspectiveCohortEval
	ORDER BY
		DistrictDenominator desc;

	IF (@OutputStatus = 0)
		BEGIN
			PRINT @Output;
		END;

	IF (object_id('tempdb.dbo.#ProspectiveCohortEval') is not null)
		BEGIN
			DROP TABLE #ProspectiveCohortEval;
		END;
END;