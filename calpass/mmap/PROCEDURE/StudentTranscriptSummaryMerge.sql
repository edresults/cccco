USE calpass;

GO

IF (object_id('mmap.StudentTranscriptSummaryMerge') is not null)
	BEGIN
		DROP PROCEDURE mmap.StudentTranscriptSummaryMerge;
	END;

GO

CREATE PROCEDURE
	mmap.StudentTranscriptSummaryMerge
	(
		@StudentTranscriptSummaryHash mmap.StudentTranscriptSummaryHash READONLY
	)
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

BEGIN
	MERGE
		mmap.StudentTranscriptSummary t
	USING
		(
			SELECT
				InterSegmentKey,
				Remedial = max(case when h.Entity = 'Remedial' and h.Attribute = 'MarkPoints' then h.Value end),
				English09 = max(case when h.Entity = 'English09' and h.Attribute = 'MarkPoints' then h.Value end),
				English10 = max(case when h.Entity = 'English10' and h.Attribute = 'MarkPoints' then h.Value end),
				English11 = max(case when h.Entity = 'English11' and h.Attribute = 'MarkPoints' then h.Value end),
				English12 = max(case when h.Entity = 'English12' and h.Attribute = 'MarkPoints' then h.Value end),
				EnglishAP = max(case when h.Entity = 'EnglishAP' and h.Attribute = 'MarkPoints' then h.Value end),
				Literature = max(case when h.Entity = 'Literature' and h.Attribute = 'MarkPoints' then h.Value end),
				Rhetoric = max(case when h.Entity = 'Rhetoric' and h.Attribute = 'MarkPoints' then h.Value end),
				Composition = max(case when h.Entity = 'Composition' and h.Attribute = 'MarkPoints' then h.Value end),
				DualEnrollment = max(case when h.Entity = 'DualEnrollment' and h.Attribute = 'MarkPoints' then h.Value end),
				PromotedRank = max(case when h.Entity = 'Mathematics' and h.Attribute = 'PromotedRank' then h.Value end),
				Arithmetic = max(case when h.Entity = 'Arithmetic' and h.Attribute = 'MarkPoints' then h.Value end),
				PreAlgebra = max(case when h.Entity = 'PreAlgebra' and h.Attribute = 'MarkPoints' then h.Value end),
				AlgebraI = max(case when h.Entity = 'AlgebraI' and h.Attribute = 'MarkPoints' then h.Value end),
				Geometry = max(case when h.Entity = 'Geometry' and h.Attribute = 'MarkPoints' then h.Value end),
				AlgebraII = max(case when h.Entity = 'AlgebraII' and h.Attribute = 'MarkPoints' then h.Value end),
				[Statistics] = max(case when h.Entity = 'Statistics' and h.Attribute = 'MarkPoints' then h.Value end),
				PreCalculus = max(case when h.Entity = 'PreCalculus' and h.Attribute = 'MarkPoints' then h.Value end),
				Trigonometry = max(case when h.Entity = 'Trigonometry' and h.Attribute = 'MarkPoints' then h.Value end),
				Calculus = max(case when h.Entity = 'Calculus' and h.Attribute = 'MarkPoints' then h.Value end),
				Cgpa = max(case when h.Entity = 'Cgpa' then h.Value end),
				Grade = max(case when h.Entity = 'Cgpa' then h.Attribute end)
			FROM
				@StudentTranscriptSummaryHash h
			GROUP BY
				InterSegmentKey
		) s
	ON
		t.InterSegmentKey = s.InterSegmentKey
	WHEN MATCHED THEN
		UPDATE SET
			t.Cgpa = s.Cgpa,
			t.English00 = s.English00,
			t.English09 = s.English09,
			t.English10 = s.English10,
			t.English11 = s.English11,
			t.English12 = s.English12,
			t.EnglishAP = s.EnglishAP,
			t.Arithmetic = s.Arithmetic,
			t.PreAlgebra = s.PreAlgebra,
			t.AlgebraI = s.AlgebraI,
			t.Geometry = s.Geometry,
			t.AlgebraII = s.AlgebraII,
			t.[Statistics] = s.[Statistics],
			t.PreCalculus = s.PreCalculus,
			t.Trigonometry = s.Trigonometry,
			t.Calculus = s.Calculus
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				InterSegmentKey,
				Cgpa,
				English00,
				English09,
				English10,
				English11,
				English12,
				EnglishAP,
				Arithmetic,
				PreAlgebra,
				AlgebraI,
				Geometry,
				AlgebraII,
				[Statistics],
				PreCalculus,
				Trigonometry,
				Calculus
			)
		VALUES
			(
				s.InterSegmentKey,
				s.Cgpa,
				s.English00,
				s.English09,
				s.English10,
				s.English11,
				s.English12,
				s.EnglishAP,
				s.Arithmetic,
				s.PreAlgebra,
				s.AlgebraI,
				s.Geometry,
				s.AlgebraII,
				s.[Statistics],
				s.PreCalculus,
				s.Trigonometry,
				s.Calculus
			);
END;
