USE calpass;

GO

IF (object_id('mmap.CollegeCourseContentPopulate') is not null)
	BEGIN
		DROP PROCEDURE mmap.CollegeCourseContentPopulate;
	END;

GO

CREATE PROCEDURE
	mmap.CollegeCourseContentPopulate
AS

BEGIN

	IF (object_id('tempdb..#Student') is not null)
		BEGIN
			DROP TABLE #Student;
		END;

	IF (object_id('tempdb..#StudentCohort') is not null)
		BEGIN
			DROP TABLE #StudentCohort;
		END;

	CREATE TABLE
		#Student
		(
			Iterator        integer   identity(0,1) primary key,
			InterSegmentKey binary(64)
		);

	CREATE TABLE
		#StudentCohort
		(
			InterSegmentKey binary(64) primary key
		);

	SET NOCOUNT ON;

	INSERT
		#Student
		(
			InterSegmentKey
		)
	SELECT DISTINCT
		id.InterSegmentKey
	FROM
		comis.studntid id
	WHERE
		id.InterSegmentKey is not null;

	DECLARE
		-- looping variables
		@Severity      tinyint       = 0,
		@State         tinyint       = 1,
		@Error         varchar(2048) = '',
		@UBound        integer       = 0,
		@Counter       integer       = 0,
		@Increment     integer       = 100000,
		@Count         integer       = 0;

	DECLARE
		-- inner script variables
		@TopCode          char(6)    = '',
		@DepartmentCode   tinyint    = null;

	SELECT
		@Count = count(*)
	FROM
		#Student;
	
	SET @UBound = ceiling(convert(decimal, @count) / @increment);


	WHILE (@Counter <= @UBound)
	BEGIN

		SET @Error = convert(varchar, @Counter) + ' of ' + convert(varchar, ceiling(convert(decimal, @count) / @increment));

		RAISERROR(@Error, @Severity, @State) WITH NOWAIT;

		TRUNCATE TABLE #StudentCohort;

		INSERT INTO
			#StudentCohort
			(
				InterSegmentKey
			)
		SELECT
			InterSegmentKey
		FROM
			#Student
		WHERE
			Iterator >= @Increment * @Counter
			and Iterator < @Increment * (@Counter + 1);

		MERGE
			Mmap.CollegeCourseContent
		WITH
			(
				TABLOCKX
			) t
		USING
			(
				SELECT
					id.InterSegmentKey,
					TopCode              = cb.top_code,
					CourseLevel          = cb.prior_to_college,
					CollegeId            = id.college_id,
					YearTermCode         = t.YearTermCode,
					CourseId             = sx.course_id,
					ControlNumber        = cb.control_number,
					CourseTitle          = cb.title,
					CourseUnitsAttempted = sx.units_attempted,
					SectionMark          = sx.grade,
					MarkPoints           = m.points,
					MarkCategory         = m.category,
					MarkIsSuccess        = m.IsSuccess,
					FirstSelector = 
						row_number() OVER(
							PARTITION BY
								id.InterSegmentKey,
								cb.top_code
							ORDER BY
								t.YearTermCode ASC,
								case when crt.course_id is not null then 1 when crs.course_id is not null then 2 else 0 end ASC,
								l.rank ASC,
								sx.units_attempted DESC, --rigor
								m.rank ASC, -- performance
								cb.course_id ASC
						),
					LevelSelector = 
						row_number() OVER(
							PARTITION BY
								id.InterSegmentKey,
								cb.top_code,
								l.rank
							ORDER BY
								t.YearTermCode ASC,
								sx.units_attempted DESC, --rigor
								m.rank ASC, -- performance
								cb.course_id ASC
						)
				FROM
					#StudentCohort sc
					inner join
					comis.studntid id
						on id.InterSegmentKey = sc.InterSegmentKey
					inner join
					comis.stterm st
						on st.college_id = id.college_id
						and st.student_id = id.student_id
					inner join
					comis.sxenrlm sx
						on sx.college_id = st.college_id
						and sx.student_id = st.student_id
						and sx.term_id = st.term_id
					inner join
					comis.cbcrsinv cb
						on cb.college_id = sx.college_id
						and cb.course_id = sx.course_id
						and cb.control_number = sx.control_number
						and cb.term_id = sx.term_id
					inner join
					comis.Mark m
						on m.MarkCode = sx.grade
					inner join
					comis.term t
						on st.term_id = t.TermCode
					inner join
					dbo.CourseLevel l
						on l.code = cb.prior_to_college
					left outer join
					mmap.CoRequisiteSupport crs
						on crs.college_id = cb.college_id
						and crs.control_number = cb.control_number
					left outer join
					mmap.CoRequisiteTransfer crt
						on crt.college_id = cb.college_id
						and crt.control_number = cb.control_number
				WHERE
					sx.units_attempted > 1
					and sx.credit_flag in ('T','D','C','S')
					and id.college_id != '841'
					and (
						(
							cb.top_code = '170100'
							and
							(
								cb.prior_to_college <> 'Y'
								or
								(
									cb.prior_to_college = 'Y'
									and
									cb.transfer_status in ('A', 'B') -- must be A and B; trig courses can be code CSU only
								)
							)
						)
						or
						(
							cb.top_code = '150100'
							and
							(
								cb.Title not like '%LIT%'
								and
								(
									(
										case
											when cb.college_id = '651' and cb.course_id = 'ENG100' then 'A' -- SBCC MOD
											else cb.prior_to_college
										end <> 'Y'
									)
									or
									(
										case
											when cb.college_id = '651' and cb.course_id = 'ENG100' then 'A' -- SBCC MOD
											else cb.prior_to_college
										end = 'Y'
										and
										cb.transfer_status = 'A'
									)
								)
							)
						)
					)
			) s
				on  t.InterSegmentKey = s.InterSegmentKey
				and t.TopCode = s.TopCode
				and t.FirstSelector = s.FirstSelector
		WHEN MATCHED THEN
			UPDATE SET
				t.InterSegmentKey = s.InterSegmentKey,
				t.TopCode = s.TopCode,
				t.CourseLevel = s.CourseLevel,
				t.CollegeId = s.CollegeId,
				t.YearTermCode = s.YearTermCode,
				t.CourseId = s.CourseId,
				t.ControlNumber = s.ControlNumber,
				t.CourseTitle = s.CourseTitle,
				t.CourseUnitsAttempted = s.CourseUnitsAttempted,
				t.SectionMark = s.SectionMark,
				t.MarkPoints = s.MarkPoints,
				t.MarkCategory = s.MarkCategory,
				t.MarkIsSuccess = s.MarkIsSuccess,
				t.FirstSelector = s.FirstSelector,
				t.LevelSelector = s.LevelSelector
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					InterSegmentKey,
					TopCode,
					CourseLevel,
					CollegeId,
					YearTermCode,
					CourseId,
					ControlNumber,
					CourseTitle,
					CourseUnitsAttempted,
					SectionMark,
					MarkPoints,
					MarkCategory,
					MarkIsSuccess,
					FirstSelector,
					LevelSelector
				)
			VALUES
				(
					s.InterSegmentKey,
					s.TopCode,
					s.CourseLevel,
					s.CollegeId,
					s.YearTermCode,
					s.CourseId,
					s.ControlNumber,
					s.CourseTitle,
					s.CourseUnitsAttempted,
					s.SectionMark,
					s.MarkPoints,
					s.MarkCategory,
					s.MarkIsSuccess,
					s.FirstSelector,
					s.LevelSelector
				);

		UPDATE
			ccc1
		SET
			ccc1.CoRequisiteSupportSelector = ccc2.FirstSelector
		FROM
			#StudentCohort sc
			inner join
			mmap.CollegeCourseContent ccc1
				on ccc1.InterSegmentKey = sc.InterSegmentKey
			inner join
			mmap.CollegeCourseContent ccc2
				on ccc1.InterSegmentKey = ccc2.InterSegmentKey 
				and ccc1.CollegeId      = ccc2.CollegeId
				and ccc1.YearTermCode   = ccc2.YearTermCode
				and ccc1.TopCode        = ccc2.TopCode
			inner join
			mmap.CoRequisite crs
				on crs.college_id = ccc1.CollegeId
				and crs.support_control_number = ccc1.ControlNumber
				and crs.transfer_control_number = ccc2.ControlNumber;

		UPDATE
			ccc1
		SET
			ccc1.CoRequisiteTransferSelector = ccc2.FirstSelector
		FROM
			#StudentCohort sc
			inner join
			mmap.CollegeCourseContent ccc1
				on ccc1.InterSegmentKey = sc.InterSegmentKey
			inner join
			mmap.CollegeCourseContent ccc2
				on  ccc1.InterSegmentKey = ccc2.InterSegmentKey 
				and ccc1.CollegeId       = ccc2.CollegeId
				and ccc1.YearTermCode    = ccc2.YearTermCode
				and ccc1.TopCode         = ccc2.TopCode
			inner join
			mmap.CoRequisite crs
				on  crs.college_id              = ccc1.CollegeId
				and crs.transfer_control_number = ccc1.ControlNumber
				and crs.support_control_number  = ccc2.ControlNumber;

		SET @Counter += 1;
	END;

	UPDATE
		t
	SET
		t.CollegeCounter = s.CollegeCounter
	FROM
		Mmap.CollegeCourseContent t
		inner join
		(
			SELECT
				a.InterSegmentKey,
				a.TopCode,
				CollegeCounter = count(distinct a.CollegeId)
			FROM
				Mmap.CollegeCourseContent a
			GROUP BY
				a.InterSegmentKey,
				a.TopCode
		) s
			on t.InterSegmentKey = s.InterSegmentKey
			and t.TopCode = s.TopCode;

	UPDATE
		t
	SET
		t.DistrictCounter = s.DistrictCounter
	FROM
		Mmap.CollegeCourseContent t
		inner join
		(
			SELECT
				a.InterSegmentKey,
				a.TopCode,
				DistrictCounter = count(distinct c.DistrictCode)
			FROM
				Mmap.CollegeCourseContent a
				inner join
				comis.College c
					on c.CollegeCode = a.CollegeId
			GROUP BY
				a.InterSegmentKey,
				a.TopCode
		) s
			on t.InterSegmentKey = s.InterSegmentKey
			and t.TopCode = s.TopCode;
END;