CREATE PROCEDURE
    mmap.TranscriptStageMigrate
AS

BEGIN
    INSERT INTO
        CPPDELSQLP01.CPP_API.AB705.TranscriptStage
        (
            StatewideStudentId,
            InterSegmentKey,
            CaliforniaCommunityCollegeId,
            DataSource,
            IsCollision,
            CompletedEleventhGrade,
            CumulativeGradePointAverage,
            EnglishCompletedCourseId,
            EnglishCompletedCourseGrade,
            MathematicsCompletedCourseId,
            MathematicsCompletedCourseGrade,
            MathematicsPassedCourseId,
            MathematicsPassedCourseGrade,
            CreateDate,
            ModifyDate
        )
    SELECT
        StatewideStudentId              = s.StudentStateId,
        InterSegmentKey                 = s.InterSegmentKey,
        CaliforniaCommunityCollegeId    = null,
        DataSource                      = 2,
        IsCollision                     = s.IsCollision,
        CompletedEleventhGrade          = case when rp.GradeCode in ('11', '12') then 1 else 0 end,
        CumulativeGradePointAverage     = rp.CumulativeGradePointAverage,
        EnglishCompletedCourseId        = 
            case
                when e.GradeCode = '12' then
                    case
                        when e.CourseLevelCode = '30' then 1
                        when e.CourseLevelCode = '39' then 2
                        else 3
                    end
                when e.GradeCode = '11' then
                    case
                        when e.CourseLevelCode = '30' then 4
                        when e.CourseLevelCode = '39' then 5
                        else 6
                    end
                when e.GradeCode = '10' then 7
                else 0
            end,
        EnglishCompletedCourseGrade     = ISNULL(eg.GradeCode, 'X'),
        MathematicsCompletedCourseId    = ISNULL(tmc.TranscriptMathematicsId, 0),
        MathematicsCompletedCourseGrade = ISNULL(mg.GradeCode, 'X'),
        MathematicsPassedCourseId       = ISNULL(tmc1.TranscriptMathematicsId, 0),
        MathematicsPassedCourseGrade    = ISNULL(mg1.GradeCode, 'X'),
        CreateDate                      = SYSDATETIME(),
        ModifyDate                      = SYSDATETIME()
    FROM
        calpads.Students s
        inner join
        Mmap.RetrospectivePerformance rp
            on rp.InterSegmentKey = s.InterSegmentKey
        left outer join
        Mmap.RetrospectiveCourseContent e
            on  e.InterSegmentKey = rp.InterSegmentKey
            and e.DepartmentCode  = 14
            and e.RecencySelector = 1
        left outer join
        AB705.TranscriptGrade eg
            on eg.GradeCode = e.SectionMark
        left outer join
        Mmap.RetrospectiveCourseContent m
            on  m.InterSegmentKey = rp.InterSegmentKey
            and m.DepartmentCode  = 18
            and m.RecencySelector = 1
        left outer join
        AB705.TranscriptMathematicsContent tmc
            on tmc.Code = m.CourseCode
        left outer join
        AB705.TranscriptGrade mg
            on mg.GradeCode = m.SectionMark
        left outer join
        Mmap.RetrospectiveCourseContent m1
            on  m1.InterSegmentKey = rp.InterSegmentKey
            and m1.DepartmentCode  = 18
            and m1.RecencySelector = (
                SELECT
                    min(m2.RecencySelector)
                FROM
                    Mmap.RetrospectiveCourseContent m2
                WHERE
                    m2.InterSegmentKey    = m1.InterSegmentKey
                    and m2.DepartmentCode = 18
                    and (
                        m.IsSuccess = 1
                        or
                        (
                            m.IsSuccess = 0
                            and
                            m2.IsSuccess = 1
                            and
                            m2.CourseCode <> m.CourseCode
                        )
                    )
            )
        left outer join
        AB705.TranscriptMathematicsContent tmc1
            on tmc1.Code = m1.CourseCode
        left outer join
        AB705.TranscriptGrade mg1
            on mg1.GradeCode = m1.SectionMark
    WHERE
        rp.DepartmentCode = 0
        and rp.GradeCode in ('11', '12')
        and rp.IsLast = 1
        and s.IsEscrow = 1;
END;