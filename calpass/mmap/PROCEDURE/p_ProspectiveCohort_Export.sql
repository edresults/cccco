USE calpass;

GO

IF (object_id('mmap.p_ProspectiveCohort_Export') is not null)
	BEGIN
		DROP PROCEDURE mmap.p_ProspectiveCohort_Export;
	END;

GO

CREATE PROCEDURE
	mmap.p_ProspectiveCohort_Export
	(
		@SubmissionFileId int
	)
AS

DECLARE
	@OrganizationId int;

BEGIN
	-- get OrganizationId
	SELECT
		@OrganizationId = OrganizationId
	FROM
		[PRO-DAT-SQL-01].cms_production_calpass.dbo.calpassplus_filedrop
	WHERE
		SubmissionFileId = @SubmissionFileId;
	-- ROADMMAP PROJECT
	-- IF (@OrganizationId = 170739)
	-- 	BEGIN
	-- 		SELECT
	-- 			college_id = CollegeId,
	-- 			student_id = 
	-- 				case
	-- 					when IdStatus = 'S' then right('000000000' + calpass.dbo.get9812Encryption(StudentId, ''), 9)
	-- 					else StudentId
	-- 				end,
	-- 			id_status = IdStatus,
	-- 			name_first = rtrim(NameFirst),
	-- 			name_last = rtrim(NameLast),
	-- 			birthdate = Birthdate,
	-- 			gender = Gender,
	-- 			match_type,
	-- 			engl_cb21,
	-- 			read_cb21,
	-- 			esl_cb21,
	-- 			math_cb21,
	-- 			math_geo,
	-- 			math_alg,
	-- 			math_ge,
	-- 			math_stat,
	-- 			math_pre_calc,
	-- 			math_trig,
	-- 			math_col_alg,
	-- 			math_calc_i,
	-- 			grade_level,
	-- 			gpa_cum,
	-- 			AP_ANY,
	-- 			english,
	-- 			english_ap,
	-- 			pre_alg,
	-- 			pre_alg_ap,
	-- 			alg_i,
	-- 			alg_i_ap,
	-- 			alg_ii,
	-- 			alg_ii_ap,
	-- 			geo,
	-- 			geo_ap,
	-- 			trig,
	-- 			trig_ap,
	-- 			pre_calc,
	-- 			pre_calc_ap,
	-- 			calc,
	-- 			calc_ap,
	-- 			stat,
	-- 			stat_ap,
	-- 			engl_eap_ind,
	-- 			engl_scaled_score,
	-- 			math_subject,
	-- 			math_eap_ind,
	-- 			math_scaled_score,
	-- 			esl_ind,
	-- 			IsTreatmentRoadmmap
	-- 		FROM
	-- 			calpass.mmap.ProspectiveCohort
	-- 		WHERE
	-- 			SubmissionFileId = @SubmissionFileId
	-- 		ORDER BY
	-- 			IsTreatmentRoadmmap DESC,
	-- 			CollegeId ASC,
	-- 			StudentId ASC;
	-- 	END;
	-- ELSE
	-- 	BEGIN
			SELECT
				college_id = CollegeId,
				student_id = 
					case
						when IdStatus = 'S' then right('000000000' + calpass.dbo.get9812Encryption(StudentId, ''), 9)
						else StudentId
					end,
				id_status = IdStatus,
				name_first = rtrim(NameFirst),
				name_last = rtrim(NameLast),
				birthdate = Birthdate,
				gender = Gender,
				match_type,
				engl_cb21,
				read_cb21,
				esl_cb21,
				math_cb21,
				math_geo,
				math_alg,
				math_ge,
				math_stat,
				math_pre_calc,
				math_trig,
				math_col_alg,
				math_calc_i,
				grade_level,
				gpa_cum,
				AP_ANY,
				english,
				english_ap,
				pre_alg,
				pre_alg_ap,
				alg_i,
				alg_i_ap,
				alg_ii,
				alg_ii_ap,
				geo,
				geo_ap,
				trig,
				trig_ap,
				pre_calc,
				pre_calc_ap,
				calc,
				calc_ap,
				stat,
				stat_ap,
				engl_eap_ind,
				engl_scaled_score,
				math_subject,
				math_eap_ind,
				math_scaled_score,
				esl_ind
			FROM
				calpass.mmap.ProspectiveCohort
			WHERE
				SubmissionFileId = @SubmissionFileId
			ORDER BY
				SubmissionFileRecordNumber;
		-- END;
END;