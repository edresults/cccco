CREATE TABLE
	[dbo].[UnivStudentProd]
	(
		[school] [char](6) NOT NULL,
		[YrTerm] [char](5) NOT NULL,
		[StudentId] [char](10) NOT NULL,
		[IdStatus] [char](1) NULL,
		[CSISNum] [char](10) NULL,
		[Fname] [char](30) NULL,
		[Lname] [char](40) NULL,
		[Gender] [char](1) NULL,
		[Ethnicity] [char](3) NULL,
		[Birthdate] [char](8) NULL,
		[InstOrigin] [char](6) NULL,
		[AdmitBasis] [char](2) NULL,
		[Major] [char](6) NULL,
		[EnrollStatus] [char](1) NULL,
		[StudentLevel] [char](1) NULL,
		[UnitsEarnedTerm] [decimal](6, 2) NULL,
		[TermGPA] [decimal](4, 2) NULL,
		[UnitsEarnedLocal] [decimal](6, 2) NULL,
		[CumeGPA] [decimal](4, 2) NULL,
		[TotalUnitsEarned] [decimal](6, 2) NULL,
		[TotalUnitsEnroll] [decimal](6, 2) NULL,
		[XferGPA] [decimal](4, 2) NULL,
		[XferUnitsEarned] [decimal](6, 2) NULL,
		[HispanicEthnicity] [char](1) NULL,
		[MultipleEthnicity] [varchar](10) NULL,
		[FosterYouth] [char](3) NULL,
		[FinancialAid] [char](1) NULL,
		[EthnicityHispanic] [char](1) NULL,
		[EthnicityBlack] [char](1) NULL,
		[EthnicityNativeAmerican] [char](1) NULL,
		[EthnicityAsian] [char](1) NULL,
		[EthnicityPacificIslander] [char](1) NULL,
		[EthnicityWhite] [char](1) NULL,
		[DateAdded] [datetime] NULL,
		[Derkey1] [binary](64) NULL
	);

ALTER TABLE
	[dbo].[UnivStudentProd]
ADD CONSTRAINT
	[pk_UnivStudentProd]
PRIMARY KEY CLUSTERED 
	(
		[school] ASC,
		[StudentId] ASC,
		[YrTerm] ASC
	);