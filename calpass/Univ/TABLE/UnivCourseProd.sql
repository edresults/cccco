CREATE TABLE
	[dbo].[UnivCourseProd]
	(
		[School] [char](6) NOT NULL,
		[YrTerm] [char](5) NOT NULL,
		[StudentId] [char](10) NOT NULL,
		[CourseTitle] [varchar](40) NOT NULL,
		[CourseAbbr] [varchar](8) NOT NULL,
		[CourseNum] [varchar](5) NOT NULL,
		[CourseNumSuffix] [varchar](2) NOT NULL,
		[Units] [decimal](5, 2) NULL,
		[Grade] [varchar](3) NULL,
		[DateAdded] [datetime] NULL,
		[Remedial] [char](1) NULL
	);

ALTER TABLE
	[dbo].[UnivCourseProd]
ADD CONSTRAINT
	[pk_UnivCourseProd]
PRIMARY KEY CLUSTERED 
	(
		[School] ASC,
		[StudentId] ASC,
		[YrTerm] ASC,
		[CourseAbbr] ASC,
		[CourseNum] ASC,
		[CourseNumSuffix] ASC,
		[CourseTitle] ASC
	)