CREATE TABLE
	[dbo].[UnivAwardProd]
	(
		[school] [char](6) NOT NULL,
		[YrTerm] [char](5) NOT NULL,
		[StudentId] [char](10) NOT NULL,
		[DegreeType] [char](1) NULL,
		[Major1] [char](6) NULL,
		[Major2] [char](6) NULL,
		[Major3] [char](6) NULL,
		[AwardDate] [char](8) NULL,
		[DateAdded] [datetime] NULL,
		[Derkey1] [binary](64) NULL
	);