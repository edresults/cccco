USE calpass;

GO

IF (object_id('dbo.UnivCourseProdv2Merge') is not null)
	BEGIN
		DROP PROCEDURE dbo.UnivCourseProdv2Merge;
	END;

GO

CREATE PROCEDURE
	dbo.UnivCourseProdv2Merge
	(
		@StageSchemaTableName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Sql nvarchar(max) = N'',
	@RowCount int = 0,
	@Message nvarchar(2048);

BEGIN
	-- validate stage table
	IF (object_id(@StageSchemaTableName) is null)
		BEGIN
			SET @Message = N'Stage table not found';
			THROW 70099, @Message, 1;
		END;
	-- merge
	SET @Sql = N'
		MERGE
			dbo.UnivCourseProd t
		USING
			(
				SELECT
					School,
					YrTerm,
					StudentId,
					CourseTitle,
					CourseAbbr,
					CourseNum,
					CourseNumSuffix,
					Units,
					Grade,
					Remedial,
					DateAdded
				FROM
					(
						SELECT
							School,
							YrTerm,
							StudentId,
							CourseTitle,
							CourseAbbr,
							CourseNum,
							CourseNumSuffix = isnull(CourseNumSuffix, ''''),
							Units = convert(decimal(5,2), case when Units = ''xx.xx'' then null else Units end),
							Grade,
							Remedial,
							DateAdded = GetDate(),
							Selector = 
								row_number()
								over(
									partition by
										School,
										StudentId,
										YrTerm,
										CourseAbbr,
										CourseNum,
										isnull(CourseNumSuffix, ''''),
										CourseTitle
									order by
										SubmissionFileRecordNumber desc
								)
						FROM
							' + convert(nvarchar(max), @StageSchemaTableName) + '
					) u
				WHERE
					Selector = 1
			) s
		ON
			t.School = s.School
			and t.StudentId = s.StudentId
			and t.YrTerm = s.YrTerm
			and t.CourseAbbr = s.CourseAbbr
			and t.CourseNum = s.CourseNum
			and t.CourseNumSuffix = s.CourseNumSuffix
			and t.CourseTitle = s.CourseTitle
		WHEN MATCHED THEN
			UPDATE SET
				t.Units = s.Units,
				t.Grade = s.Grade,
				t.Remedial = s.Remedial,
				t.DateAdded = s.DateAdded
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					School,
					YrTerm,
					StudentId,
					CourseTitle,
					CourseAbbr,
					CourseNum,
					CourseNumSuffix,
					Units,
					Grade,
					Remedial,
					DateAdded
				)
			VALUES
				(
					s.School,
					s.YrTerm,
					s.StudentId,
					s.CourseTitle,
					s.CourseAbbr,
					s.CourseNum,
					s.CourseNumSuffix,
					s.Units,
					s.Grade,
					s.Remedial,
					s.DateAdded
				);
				
		SET @RowCount = @@ROWCOUNT;';

	-- merge stage records into production table
	EXECUTE sp_executesql
		@Sql,
		N'@RowCount int out',
		@RowCount = @RowCount out;
	-- update output message
	SET @Message += convert(nvarchar, @RowCount) + N' Stage Records Merged to Production ' + nchar(13) + nchar(10);
END;