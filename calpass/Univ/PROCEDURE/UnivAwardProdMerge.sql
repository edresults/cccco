USE calpass;

GO

IF (object_id('dbo.UnivAwardProdMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.UnivAwardProdMerge;
	END;

GO

CREATE PROCEDURE
	dbo.UnivAwardProdMerge
	(
		@StageSchemaTableName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Sql nvarchar(max) = N'',
	@RowCount int = 0,
	@Message nvarchar(2048);

BEGIN
	-- validate stage table
	IF (object_id(@StageSchemaTableName) is null)
		BEGIN
			SET @Message = N'Stage table not found';
			THROW 70099, @Message, 1;
		END;
	-- merge
	SET @Sql = N'
		MERGE
			dbo.UnivAwardProd t
		USING
			(
				SELECT
					School,
					YrTerm,
					StudentId,
					DegreeType,
					Major1,
					Major2,
					Major3,
					AwardDate,
					DateAdded
				FROM
					(
						SELECT DISTINCT
							School,
							YrTerm,
							StudentId,
							DegreeType = isnull(DegreeType, ''''),
							Major1,
							Major2,
							Major3,
							AwardDate,
							DateAdded = GetDate(),
							Selector = 
								row_number()
								over(
									partition by
										School,
										StudentId,
										YrTerm,
										isnull(DegreeType, ''''),
										Major1
									order by
										SubmissionFileRecordNumber desc
								)
						FROM
							' + convert(nvarchar(max), @StageSchemaTableName) + '
					) s
				WHERE
					Selector = 1
			) s
		ON
			t.School = s.School
			and t.YrTerm = s.YrTerm
			and t.StudentId = s.StudentId
			and t.DegreeType = s.DegreeType
			and t.Major1 = s.Major1
		WHEN MATCHED THEN
			UPDATE SET
				t.Major2 = s.Major2,
				t.Major3 = s.Major3,
				t.AwardDate = s.AwardDate,
				t.DateAdded = s.DateAdded
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					School,
					YrTerm,
					StudentId,
					DegreeType,
					Major1,
					Major2,
					Major3,
					AwardDate,
					DateAdded
				)
			VALUES
				(
					s.School,
					s.YrTerm,
					s.StudentId,
					s.DegreeType,
					s.Major1,
					s.Major2,
					s.Major3,
					s.AwardDate,
					s.DateAdded
				);
				
		SET @RowCount = @@ROWCOUNT;';

	-- merge stage records into production table
	EXECUTE sp_executesql
		@Sql,
		N'@RowCount int out',
		@RowCount = @RowCount out;
	-- update output message
	SET @Message += convert(nvarchar, @RowCount) + N' Stage Records Merged to Production ' + nchar(13) + nchar(10);
END;