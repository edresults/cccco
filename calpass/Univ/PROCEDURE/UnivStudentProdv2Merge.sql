USE calpass;

GO

IF (object_id('dbo.UnivStudentProdv2Merge') is not null)
	BEGIN
		DROP PROCEDURE dbo.UnivStudentProdv2Merge;
	END;

GO

CREATE PROCEDURE
	dbo.UnivStudentProdv2Merge
	(
		@StageSchemaTableName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Sql nvarchar(max) = N'',
	@RowCount int = 0,
	@Message nvarchar(2048);

BEGIN
	-- validate stage table
	IF (object_id(@StageSchemaTableName) is null)
		BEGIN
			SET @Message = N'Stage table not found';
			THROW 70099, @Message, 1;
		END;
	-- merge
	SET @Sql = N'
		MERGE
			dbo.UnivStudentProd t
		USING
			(
				SELECT
					School,
					YrTerm,
					StudentId,
					IdStatus,
					CSISNum,
					Fname,
					Lname,
					Gender,
					Ethnicity,
					Birthdate,
					InstOrigin,
					AdmitBasis,
					Major,
					EnrollStatus,
					StudentLevel,
					UnitsEarnedTerm = convert(decimal(6,2), case when isnumeric(UnitsEarnedTerm + ''e0'') = 1 then UnitsEarnedTerm end),
					TermGPA = convert(decimal(4,2), case when isnumeric(TermGPA + ''e0'') = 1 then TermGPA end),
					UnitsEarnedLocal = convert(decimal(6,2), case when isnumeric(UnitsEarnedLocal + ''e0'') = 1 then UnitsEarnedLocal end),
					CumeGPA = convert(decimal(4,2), case when isnumeric(CumeGPA + ''e0'') = 1 then CumeGPA end),
					TotalUnitsEarned = convert(decimal(6,2), case when isnumeric(TotalUnitsEarned + ''e0'') = 1 then TotalUnitsEarned end),
					TotalUnitsEnroll = convert(decimal(6,2), case when isnumeric(TotalUnitsEnroll + ''e0'') = 1 then TotalUnitsEnroll end),
					XferGPA = convert(decimal(4,2), case when isnumeric(XferGPA + ''e0'') = 1 then XferGPA end),
					XferUnitsEarned = convert(decimal(6,2), case when isnumeric(XferUnitsEarned + ''e0'') = 1 then XferUnitsEarned end),
					HispanicEthnicity,
					MultipleEthnicity,
					FosterYouth,
					FinancialAid,
					EthnicityHispanic,
					EthnicityBlack,
					EthnicityNativeAmerican,
					EthnicityAsian,
					EthnicityPacificIslander,
					EthnicityWhite,
					DateAdded,
					Derkey1
				FROM
					(
						SELECT
							School,
							YrTerm,
							StudentId,
							IdStatus,
							CSISNum = 
								case
									when try_parse(CSISNum as int) is not null then calpass.dbo.Get9812Encryption(CSISNum, '''')
									else ''''
								end,
							Fname,
							Lname,
							Gender,
							Ethnicity,
							Birthdate,
							InstOrigin,
							AdmitBasis,
							Major,
							EnrollStatus,
							StudentLevel,
							UnitsEarnedTerm,
							TermGPA,
							UnitsEarnedLocal,
							CumeGPA,
							TotalUnitsEarned,
							TotalUnitsEnroll,
							XferGPA,
							XferUnitsEarned,
							HispanicEthnicity,
							MultipleEthnicity,
							FosterYouth,
							FinancialAid,
							EthnicityHispanic = 
								case
									when substring(MultipleEthnicity, 1, 1) = ''Y'' then ''Y''
									else HispanicEthnicity
								end,
							EthnicityBlack = 
								case
									when charindex(''02'', MultipleEthnicity) > 0 then ''Y''
									when substring(MultipleEthnicity, 2, 1) = ''Y'' then ''Y''
									else ''N''
								end,
							EthnicityNativeAmerican = 
								case
									when charindex(''03'', MultipleEthnicity) > 0 then ''Y''
									when substring(MultipleEthnicity, 3, 1) = ''Y'' then ''Y''
									else ''N''
								end,
							EthnicityAsian = 
								case
									when charindex(''04'', MultipleEthnicity) > 0 then ''Y''
									when substring(MultipleEthnicity, 4, 1) = ''Y'' then ''Y''
									else ''N''
								end,
							EthnicityPacificIslander = 
								case
									when charindex(''05'', MultipleEthnicity) > 0 then ''Y''
									when substring(MultipleEthnicity, 5, 1) = ''Y'' then ''Y''
									else ''N''
								end,
							EthnicityWhite = 
								case
									when charindex(''01'', MultipleEthnicity) > 0 then ''Y''
									when substring(MultipleEthnicity, 6, 1) = ''Y'' then ''Y''
									else ''N''
								end,
							DateAdded = GetDate(),
							Derkey1 = 
								HASHBYTES(
									''SHA2_512'',
									upper(convert(nchar(3), Fname)) + 
									upper(convert(nchar(3), LName)) + 
									upper(convert(nchar(1), Gender)) + 
									convert(nchar(8), Birthdate)
								),
							Selector = 
								row_number()
								over(
									partition by
										School,
										StudentId,
										YrTerm
									order by
										SubmissionFileRecordNumber desc
								)
						FROM
							' + convert(nvarchar(max), @StageSchemaTableName) + '
					) u
				WHERE
					Selector = 1
			) s
		ON
			t.School = s.School
			and t.StudentId = s.StudentId
			and t.YrTerm = s.YrTerm
		WHEN MATCHED THEN
			UPDATE SET
				t.IdStatus = s.IdStatus,
				t.CSISNum = s.CSISNum,
				t.Fname = s.Fname,
				t.Lname = s.Lname,
				t.Gender = s.Gender,
				t.Ethnicity = s.Ethnicity,
				t.Birthdate = s.Birthdate,
				t.InstOrigin = s.InstOrigin,
				t.AdmitBasis = s.AdmitBasis,
				t.Major = s.Major,
				t.EnrollStatus = s.EnrollStatus,
				t.StudentLevel = s.StudentLevel,
				t.UnitsEarnedTerm = s.UnitsEarnedTerm,
				t.TermGPA = s.TermGPA,
				t.UnitsEarnedLocal = s.UnitsEarnedLocal,
				t.CumeGPA = s.CumeGPA,
				t.TotalUnitsEarned = s.TotalUnitsEarned,
				t.TotalUnitsEnroll = s.TotalUnitsEnroll,
				t.XferGPA = s.XferGPA,
				t.XferUnitsEarned = s.XferUnitsEarned,
				t.HispanicEthnicity = s.HispanicEthnicity,
				t.MultipleEthnicity = s.MultipleEthnicity,
				t.FosterYouth = s.FosterYouth,
				t.FinancialAid = s.FinancialAid,
				t.EthnicityHispanic = s.EthnicityHispanic,
				t.EthnicityBlack = s.EthnicityBlack,
				t.EthnicityNativeAmerican = s.EthnicityNativeAmerican,
				t.EthnicityAsian = s.EthnicityAsian,
				t.EthnicityPacificIslander = s.EthnicityPacificIslander,
				t.EthnicityWhite = s.EthnicityWhite,
				t.DateAdded = s.DateAdded,
				t.Derkey1 = s.Derkey1
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					School,
					YrTerm,
					StudentId,
					IdStatus,
					CSISNum,
					Fname,
					Lname,
					Gender,
					Ethnicity,
					Birthdate,
					InstOrigin,
					AdmitBasis,
					Major,
					EnrollStatus,
					StudentLevel,
					UnitsEarnedTerm,
					TermGPA,
					UnitsEarnedLocal,
					CumeGPA,
					TotalUnitsEarned,
					TotalUnitsEnroll,
					XferGPA,
					XferUnitsEarned,
					HispanicEthnicity,
					MultipleEthnicity,
					FosterYouth,
					FinancialAid,
					EthnicityHispanic,
					EthnicityBlack,
					EthnicityNativeAmerican,
					EthnicityAsian,
					EthnicityPacificIslander,
					EthnicityWhite,
					DateAdded,
					Derkey1
				)
			VALUES
				(
					s.School,
					s.YrTerm,
					s.StudentId,
					s.IdStatus,
					s.CSISNum,
					s.Fname,
					s.Lname,
					s.Gender,
					s.Ethnicity,
					s.Birthdate,
					s.InstOrigin,
					s.AdmitBasis,
					s.Major,
					s.EnrollStatus,
					s.StudentLevel,
					s.UnitsEarnedTerm,
					s.TermGPA,
					s.UnitsEarnedLocal,
					s.CumeGPA,
					s.TotalUnitsEarned,
					s.TotalUnitsEnroll,
					s.XferGPA,
					s.XferUnitsEarned,
					s.HispanicEthnicity,
					s.MultipleEthnicity,
					s.FosterYouth,
					s.FinancialAid,
					s.EthnicityHispanic,
					s.EthnicityBlack,
					s.EthnicityNativeAmerican,
					s.EthnicityAsian,
					s.EthnicityPacificIslander,
					s.EthnicityWhite,
					s.DateAdded,
					s.Derkey1
				);
				
		SET @RowCount = @@ROWCOUNT;';

	-- merge stage records into production table
	EXECUTE sp_executesql
		@Sql,
		N'@RowCount int out',
		@RowCount = @RowCount out;
	-- update output message
	SET @Message += convert(nvarchar, @RowCount) + N' Stage Records Merged to Production ' + nchar(13) + nchar(10);
END;