USE calpass;

GO

IF (object_id('dbo.UnivPostProcessing') is not null)
	BEGIN
		DROP PROCEDURE dbo.UnivPostProcessing;
	END;

GO

CREATE PROCEDURE
	dbo.UnivPostProcessing
AS

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

BEGIN
	UPDATE
		a
	SET
		a.Derkey1 = s1.Derkey1
	FROM
		dbo.UnivAwardProd a
		inner join
		dbo.UnivStudentProd s1
			on a.School = s1.School
			and a.StudentId = s1.StudentId
		inner join
		dbo.UnivTerm t1
			on t1.TermCode = s1.YrTerm
	WHERE
		a.Derkey1 is null
		and t1.Ordinal = (
			SELECT
				min(t2.Ordinal)
			FROM
				dbo.UnivStudentProd s2
				inner join
				dbo.UnivTerm t2
					on t2.TermCode = s2.YrTerm
			WHERE
				s2.School = s1.School
				and s2.StudentId = s1.StudentId
		);

	UPDATE
		c
	SET
		c.Derkey1 = s1.Derkey1
	FROM
		dbo.UnivCourseProd c
		inner join
		dbo.UnivStudentProd s1
			on c.School = s1.School
			and c.StudentId = s1.StudentId
		inner join
		dbo.UnivTerm t1
			on t1.TermCode = s1.YrTerm
	WHERE
		c.Derkey1 is null
		and t1.Ordinal = (
			SELECT
				min(t2.Ordinal)
			FROM
				dbo.UnivStudentProd s2
				inner join
				dbo.UnivTerm t2
					on t2.TermCode = s2.YrTerm
			WHERE
				s2.School = s1.School
				and s2.StudentId = s1.StudentId
		);
END;