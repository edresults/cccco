USE calpass;

GO

IF (object_id('dbo.UnivCharProdMerge') is not null)
	BEGIN
		DROP PROCEDURE dbo.UnivCharProdMerge;
	END;

GO

CREATE PROCEDURE
	dbo.UnivCharProdMerge
	(
		@StageSchemaTableName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Sql nvarchar(max) = N'',
	@RowCount int = 0,
	@Message nvarchar(2048);

BEGIN
	-- validate stage table
	IF (object_id(@StageSchemaTableName) is null)
		BEGIN
			SET @Message = N'Stage table not found';
			THROW 70099, @Message, 1;
		END;
	-- merge
	SET @Sql = N'
		MERGE
			dbo.UnivCharProd t
		USING
			(
				SELECT
					School,
					YrTerm,
					CourseTitle,
					Discipline,
					Subdiscipline,
					RemedialCode,
					GatewayCode,
					CourseLevel
				FROM
					(
						SELECT DISTINCT
							School,
							YrTerm,
							CourseTitle,
							Discipline,
							Subdiscipline,
							RemedialCode,
							GatewayCode,
							CourseLevel,
							Selector = 
								row_number()
								over(
									partition by
										School,
										YrTerm,
										CourseTitle
									order by
										SubmissionFileRecordNumber desc
								)
						FROM
							' + convert(nvarchar(max), @StageSchemaTableName) + '
					) s
				WHERE
					Selector = 1
			) s
		ON
			t.School = s.School
			and t.YrTerm = s.YrTerm
			and t.CourseTitle = s.CourseTitle
		WHEN MATCHED THEN
			UPDATE SET
				t.Discipline    = s.Discipline,
				t.Subdiscipline = s.Subdiscipline,
				t.RemedialCode  = s.RemedialCode,
				t.GatewayCode   = s.GatewayCode,
				t.CourseLevel   = s.CourseLevel
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					School,
					YrTerm,
					CourseTitle,
					Discipline,
					Subdiscipline,
					RemedialCode,
					GatewayCode,
					CourseLevel
				)
			VALUES
				(
					s.School,
					s.YrTerm,
					s.CourseTitle,
					s.Discipline,
					s.Subdiscipline,
					s.RemedialCode,
					s.GatewayCode,
					s.CourseLevel
				);
				
		SET @RowCount = @@ROWCOUNT;';

	-- merge stage records into production table
	EXECUTE sp_executesql
		@Sql,
		N'@RowCount int out',
		@RowCount = @RowCount out;
	-- update output message
	SET @Message += convert(nvarchar, @RowCount) + N' Stage Records Merged to Production ' + nchar(13) + nchar(10);
END;