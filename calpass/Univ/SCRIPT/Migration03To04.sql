USE calpass;

-- Drop
DROP TABLE dbo.UnivAwardProd;
DROP TABLE dbo.UnivCourseProd;
DROP TABLE dbo.UnivStudentProd;

GO

-- Migrate
SELECT * INTO dbo.UnivAwardProd   FROM [PRO-DAT-SQL-03].calpass.dbo.UnivAwardProd;
SELECT * INTO dbo.UnivCourseProd  FROM [PRO-DAT-SQL-03].calpass.dbo.UnivCourseProd;
SELECT * INTO dbo.UnivStudentProd FROM [PRO-DAT-SQL-03].calpass.dbo.UnivStudentProd;

GO

-- Constaint
ALTER TABLE dbo.UnivAwardProd   ADD CONSTRAINT PK_UnivAwardProd   PRIMARY KEY CLUSTERED (School, StudentId, YrTerm, DegreeType, Major1);
ALTER TABLE dbo.UnivCourseProd  ADD CONSTRAINT PK_UnivCourseProd  PRIMARY KEY CLUSTERED (School, StudentId, YrTerm, CourseAbbr, CourseNum, CourseNumSuffix, CourseTitle);
ALTER TABLE dbo.UnivStudentProd ADD CONSTRAINT PK_UnivStudentProd PRIMARY KEY CLUSTERED (School, StudentId, YrTerm);

GO

-- Index
CREATE NONCLUSTERED INDEX IX_UnivAwardProd__DateAdded         ON dbo.UnivAwardProd (DateAdded);
CREATE NONCLUSTERED INDEX IX_UnivAwardProd__Derkey1           ON dbo.UnivAwardProd (Derkey1);
CREATE NONCLUSTERED INDEX IX_UnivAwardProd__School__YrTerm    ON dbo.UnivAwardProd (School, YrTerm);

CREATE NONCLUSTERED INDEX IX_UnivCourseProd__DateAdded        ON dbo.UnivCourseProd (DateAdded);
CREATE NONCLUSTERED INDEX IX_UnivCourseProd__School__YrTerm   ON dbo.UnivCourseProd (School, YrTerm);

CREATE NONCLUSTERED INDEX IX_UnivStudentProd__DateAdded       ON dbo.UnivStudentProd (DateAdded);
CREATE NONCLUSTERED INDEX IX_UnivStudentProd__Derkey1         ON dbo.UnivStudentProd (Derkey1);
CREATE NONCLUSTERED INDEX IX_UnivStudentProd__School__YrTerm  ON dbo.UnivStudentProd (School, YrTerm);