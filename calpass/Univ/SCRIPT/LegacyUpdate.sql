-- SELECT * INTO UnivStudentProdBackup FROM [DEV-DAT-SQL-02].calpass.dbo.UnivStudentProd
-- SELECT * INTO UnivCourseProdBackup FROM [DEV-DAT-SQL-02].calpass.dbo.UnivCourseProdBackup
-- SELECT * INTO UnivAwardProdBackup FROM [DEV-DAT-SQL-02].calpass.dbo.UnivAwardProdBackup

-- GO

-- CREATE CLUSTERED INDEX
-- 	IC_UnivCourseProdBackup
-- ON
-- 	dbo.UnivCourseProdBackup
-- 	(
-- 		School,
-- 		StudentId,
-- 		YrTerm
-- 	);

-- CREATE CLUSTERED INDEX
-- 	IC_UnivStudentProdBackup
-- ON
-- 	dbo.UnivStudentProdBackup
-- 	(
-- 		School,
-- 		StudentId,
-- 		YrTerm
-- 	);

-- CREATE CLUSTERED INDEX
-- 	IC_UnivAwardProdBackup
-- ON
-- 	dbo.UnivAwardProdBackup
-- 	(
-- 		School,
-- 		StudentId,
-- 		YrTerm
-- 	);

-- GO

-- DELETE
-- FROM
-- 	dbo.UnivStudentProdBackup
-- WHERE
-- 	StudentId is null;
-- DELETE
-- FROM
-- 	dbo.UnivCourseProdBackup
-- WHERE
-- 	StudentId is null;
-- DELETE
-- FROM
-- 	dbo.UnivAwardProdBackup
-- WHERE
-- 	StudentId is null;

--GO

--ALTER TABLE
--	dbo.UnivCourseProdBackup
--ADD
--	StudentIdEnc binary(64);
--ALTER TABLE
--	dbo.UnivStudentProdBackup
--ADD
--	StudentIdEnc binary(64);
--ALTER TABLE
--	dbo.UnivAwardProdBackup
--ADD
--	StudentIdEnc binary(64);

--GO

-- UPDATE
-- 	c
-- SET
-- 	c.StudentIdEnc = 
-- 		HASHBYTES(
-- 			'SHA2_512',
-- 			case
-- 				when s.IdStatus = 'S' then convert(varchar(10), calpass.dbo.Get9812Encryption(s.StudentId, ''))
-- 				else  convert(varchar(10), s.StudentId)
-- 			end
-- 		)
-- FROM
-- 	dbo.UnivCourseProdBackup c
-- 	inner join
-- 	dbo.UnivStudentProdBackup s
-- 		on s.School     = c.School
-- 		and s.StudentId = c.StudentId
-- 		and s.YrTerm    = c.YrTerm;

-- UPDATE
-- 	a
-- SET
-- 	a.StudentIdEnc = 
-- 		HASHBYTES(
-- 			'SHA2_512',
-- 			case
-- 				when s.IdStatus = 'S' then convert(varchar(10), calpass.dbo.Get9812Encryption(s.StudentId, ''))
-- 				else  convert(varchar(10), s.StudentId)
-- 			end
-- 		)
-- FROM
-- 	dbo.UnivAwardProdBackup a
-- 	inner join
-- 	dbo.UnivStudentProdBackup s
-- 		on s.School     = a.School
-- 		and s.StudentId = a.StudentId
-- 		and s.YrTerm    = a.YrTerm;

-- UPDATE
-- 	s
-- SET
-- 	s.StudentIdEnc = 
-- 		HASHBYTES(
-- 			'SHA2_512',
-- 			case
-- 				when s.IdStatus = 'S' then convert(varchar(10), calpass.dbo.Get9812Encryption(s.StudentId, ''))
-- 				else convert(varchar(10), s.StudentId)
-- 			end
-- 		)
-- FROM
-- 	dbo.UnivStudentProdBackup s

-- GO

-- DELETE
-- FROM
-- 	dbo.UnivStudentProdBackup
-- WHERE
-- 	StudentIdEnc is null;
-- DELETE
-- FROM
-- 	dbo.UnivCourseProdBackup
-- WHERE
-- 	StudentIdEnc is null;
-- DELETE
-- FROM
-- 	dbo.UnivAwardProdBackup
-- WHERE
-- 	StudentIdEnc is null;

-- GO

-- ALTER TABLE
-- 	dbo.UnivCourseProdBackup
-- ALTER COLUMN
-- 	StudentIdEnc binary(64) not null;

-- ALTER TABLE
-- 	dbo.UnivStudentProdBackup
-- ALTER COLUMN
-- 	StudentIdEnc binary(64) not null;

-- ALTER TABLE
-- 	dbo.UnivAwardProdBackup
-- ALTER COLUMN
-- 	StudentIdEnc binary(64) not null;

-- GO

-- DROP INDEX IC_UnivStudentProdBackup ON UnivStudentProdBackup;
-- DROP INDEX IC_UnivCourseProdBackup ON UnivCourseProdBackup;
-- DROP INDEX IC_UnivAwardProdBackup ON UnivAwardProdBackup;

-- GO

-- ALTER TABLE
-- 	dbo.UnivAwardProdBackup
-- DROP COLUMN
-- 	StudentId;

-- ALTER TABLE
-- 	dbo.UnivCourseProdBackup
-- DROP COLUMN
-- 	StudentId;

-- ALTER TABLE
-- 	dbo.UnivStudentProdBackup
-- DROP COLUMN
-- 	StudentId;

-- GO

-- EXEC sp_RENAME 'UnivAwardProdBackup.StudentIdEnc' , 'StudentId', 'COLUMN';
-- EXEC sp_RENAME 'UnivCourseProdBackup.StudentIdEnc' , 'StudentId', 'COLUMN';
-- EXEC sp_RENAME 'UnivStudentProdBackup.StudentIdEnc' , 'StudentId', 'COLUMN';

-- GO

-- CREATE CLUSTERED INDEX
-- 	IC_UnivCourseProdBackup
-- ON
-- 	dbo.UnivCourseProdBackup
-- 	(
-- 		School,
-- 		StudentId,
-- 		YrTerm
-- 	);

-- CREATE CLUSTERED INDEX
-- 	IC_UnivStudentProdBackup
-- ON
-- 	dbo.UnivStudentProdBackup
-- 	(
-- 		School,
-- 		StudentId,
-- 		YrTerm
-- 	);

-- CREATE CLUSTERED INDEX
-- 	IC_UnivAwardProdBackup
-- ON
-- 	dbo.UnivAwardProdBackup
-- 	(
-- 		School,
-- 		StudentId,
-- 		YrTerm
-- 	);

-- GO

-- UPDATE
-- 	dbo.UnivAwardProdBackup
-- SET
-- 	DegreeType = ''
-- WHERE
-- 	DegreeType is null;

-- GO

-- DELETE
-- 	s
-- FROM
-- 	(
-- 		SELECT
-- 			Selector = 
-- 				row_number()
-- 				over(
-- 					partition by
-- 						School,
-- 						StudentId,
-- 						YrTerm,
-- 						isnull(DegreeType, ''''),
-- 						Major1
-- 					order by
-- 						(select 1)
-- 				)
-- 		FROM
-- 			dbo.UnivAwardProdBackup
-- 	) s
-- WHERE
-- 	s.Selector > 1;

-- DROP INDEX
-- 	IC_UnivAwardProdBackup
-- ON
-- 	dbo.UnivAwardProdBackup;

-- GO

-- ALTER TABLE
-- 	dbo.UnivAwardProdBackup
-- ALTER COLUMN
-- 	School char(6) not null;

-- ALTER TABLE
-- 	dbo.UnivAwardProdBackup
-- ALTER COLUMN
-- 	YrTerm char(5) not null;

-- ALTER TABLE
-- 	dbo.UnivAwardProdBackup
-- ALTER COLUMN
-- 	DegreeType char(1) not null;

-- ALTER TABLE
-- 	dbo.UnivAwardProdBackup
-- ALTER COLUMN
-- 	Major1 char(6) not null;

-- ALTER TABLE
-- 	dbo.UnivAwardProdBackup
-- ADD CONSTRAINT
-- 	PK_UnivAwardProdBackup
-- PRIMARY KEY CLUSTERED
-- 	(
-- 		School,
-- 		StudentId,
-- 		YrTerm,
-- 		DegreeType,
-- 		Major1
-- 	);

-- CREATE NONCLUSTERED INDEX
-- 	IX_UnivAwardProdBackup__School__YrTerm
-- ON
-- 	dbo.UnivAwardProdBackup
-- 	(
-- 		School,
-- 		YrTerm
-- 	);

-- CREATE NONCLUSTERED INDEX
-- 	IX_UnivAwardProdBackup__Derkey1
-- ON
-- 	dbo.UnivAwardProdBackup
-- 	(
-- 		Derkey1
-- 	);

-- CREATE NONCLUSTERED INDEX
-- 	IX_UnivAwardProdBackup__DateAdded
-- ON
-- 	dbo.UnivAwardProdBackup
-- 	(
-- 		DateAdded
-- 	);

-- GO

-- DELETE
-- 	s
-- FROM
-- 	(
-- 		SELECT
-- 			Selector = 
-- 				row_number()
-- 				over(
-- 					partition by
-- 						School,
-- 						StudentId,
-- 						YrTerm
-- 					order by
-- 						(select 1)
-- 				)
-- 		FROM
-- 			dbo.UnivStudentProdBackup
-- 	) s
-- WHERE
-- 	s.Selector > 1;

-- DROP INDEX
-- 	IC_UnivStudentProdBackup
-- ON
-- 	dbo.UnivStudentProdBackup;

-- ALTER TABLE
-- 	dbo.UnivStudentProdBackup
-- ALTER COLUMN
-- 	School char(6) not null;

-- ALTER TABLE
-- 	dbo.UnivStudentProdBackup
-- ALTER COLUMN
-- 	YrTerm char(5) not null;

-- ALTER TABLE
-- 	dbo.UnivStudentProdBackup
-- ADD CONSTRAINT
-- 	PK_UnivStudentProdBackup
-- PRIMARY KEY CLUSTERED
-- 	(
-- 		School,
-- 		StudentId,
-- 		YrTerm
-- 	);

-- CREATE NONCLUSTERED INDEX
-- 	IX_UnivStudentProdBackup__School__YrTerm
-- ON
-- 	dbo.UnivStudentProdBackup
-- 	(
-- 		School,
-- 		YrTerm
-- 	);

-- CREATE NONCLUSTERED INDEX
-- 	IX_UnivStudentProdBackup__Derkey1
-- ON
-- 	dbo.UnivStudentProdBackup
-- 	(
-- 		Derkey1
-- 	);

-- CREATE NONCLUSTERED INDEX
-- 	IX_UnivStudentProdBackup__DateAdded
-- ON
-- 	dbo.UnivStudentProdBackup
-- 	(
-- 		DateAdded
-- 	);

-- GO

-- DELETE
-- 	s
-- FROM
-- 	(
-- 		SELECT
-- 			Selector = 
-- 				row_number()
-- 				over(
-- 					partition by
-- 						School,
-- 						StudentId,
-- 						YrTerm,
-- 						CourseAbbr,
-- 						CourseNum,
-- 						isnull(CourseNumSuffix, ''''),
-- 						CourseTitle
-- 					order by
-- 						(select 1)
-- 				)
-- 		FROM
-- 			dbo.UnivCourseProdBackup
-- 	) s
-- WHERE
-- 	s.Selector > 1;

-- DROP INDEX
-- 	IC_UnivCourseProdBackup
-- ON
-- 	dbo.UnivCourseProdBackup;

-- ALTER TABLE
-- 	dbo.UnivCourseProdBackup
-- ALTER COLUMN
-- 	School char(6) not null;

-- ALTER TABLE
-- 	dbo.UnivCourseProdBackup
-- ALTER COLUMN
-- 	YrTerm char(5) not null;

-- ALTER TABLE
-- 	dbo.UnivCourseProdBackup
-- ALTER COLUMN
-- 	CourseAbbr varchar(8) not null;

-- ALTER TABLE
-- 	dbo.UnivCourseProdBackup
-- ALTER COLUMN
-- 	CourseNum varchar(5) not null;

-- ALTER TABLE
-- 	dbo.UnivCourseProdBackup
-- ALTER COLUMN
-- 	CourseNumSuffix varchar(2) not null;

-- ALTER TABLE
-- 	dbo.UnivCourseProdBackup
-- ALTER COLUMN
-- 	CourseTitle varchar(40) not null;

-- ALTER TABLE
-- 	dbo.UnivCourseProdBackup
-- ADD CONSTRAINT
-- 	PK_UnivCourseProdBackup
-- PRIMARY KEY CLUSTERED
-- 	(
-- 		School,
-- 		StudentId,
-- 		YrTerm,
-- 		CourseAbbr,
-- 		CourseNum,
-- 		CourseNumSuffix,
-- 		CourseTitle
-- 	);

-- CREATE NONCLUSTERED INDEX
-- 	IX_UnivCourseProdBackup__School__YrTerm
-- ON
-- 	dbo.UnivCourseProdBackup
-- 	(
-- 		School,
-- 		YrTerm
-- 	);

-- CREATE NONCLUSTERED INDEX
-- 	IX_UnivCourseProdBackup__DateAdded
-- ON
-- 	dbo.UnivCourseProdBackup
-- 	(
-- 		DateAdded
-- 	);

-- EXEC sp_RENAME 'UnivAwardProd', 'UnivAwardProdBackup2';
-- EXEC sp_RENAME 'UnivCourseProd' , 'UnivCourseProdBackup2';
-- EXEC sp_RENAME 'UnivStudentProd' , 'UnivStudentProdBackup2';

-- EXEC sp_RENAME 'UnivAwardProdBackup', 'UnivAwardProd';
-- EXEC sp_RENAME 'UnivCourseProdBackup' , 'UnivCourseProd';
-- EXEC sp_RENAME 'UnivStudentProdBackup' , 'UnivStudentProd';