CREATE TYPE
	PescTranscriptType
AS TABLE
	(
		SchoolAssignedPersonID        varchar(20)  not null, -- Core v1.9: SchoolAssignedPersonIDType @ 5262
		RecipientAssignedID           varchar(20)  not null, -- Core v1.9: RecipientAssignedIDType @ 5271
		BirthDate                     date         not null, -- Core v1.9: BirthDateType @ 4906
		FirstName                     varchar(35)  not null, -- Core v1.9: FirstNameType @ 4647
		LastName                      varchar(35)  not null, -- Core v1.9: LastNameType @ 4656
		CEEBACT                       char(6)      not null, -- Core v1.9: CEEBACTType @ 6671
		LocalOrganizationIDCode       varchar(35)  not null, -- Core v1.9: LocalOrganizationIDCodeType @ 6734
		StudentLevelCode              varchar(40)  not null, -- Core v1.9: StudentLevelCodeType @ 4549
		OrganizationName              varchar(60)  not null, -- Core v1.9: OrganizationNameType @ 2486
		AcademicCompletionDate        date         not null, -- Core v1.9: AcademicCompletionDateType @ 1130
		GradePointAverage             decimal(6,5) not null, -- Core v1.9: GradePointAverageType @ 1663
		AcademicSummaryType           varchar(19)  not null, -- Core v1.9: AcademicSummary @ 940
		SessionSchoolYear             char(9)      not null, -- Core v1.9: SessionSchoolYearType @ 2651
		SessionTermCode               varchar(7)   not null, -- Custom
		OriginalCourseID              varchar(30)  not null, -- Core v1.9: OriginalCourseIDType @ 2301
		AgencyCourseID                varchar(30)  not null, -- Core v1.9: AgencyCourseIDType @ 2310
		CourseSectionNumber           varchar(30)      null, -- Core v1.9: CourseSectionNumberType @ 2292
		CourseTitle                   varchar(60)  not null, -- Core v1.9: CourseTitleType @ 2334
		CourseCreditBasis             varchar(30)  not null, -- Core v1.9: CourseCreditBasisType @ 1696
		CourseCreditUnits             varchar(25)  not null, -- Core v1.9: CourseCreditUnitsType @ 1791
		CourseCreditValue             decimal(7,5) not null, -- Core v1.9: CourseCreditValueType @ 2319
		CourseCreditEarned            decimal(7,5) not null, -- Core v1.9: CourseCreditEarnedType @ 2286
		CourseAcademicGrade           varchar(10)  not null, -- Core v1.9: CourseAcademicGradeType @ 1830
		CourseAcademicGradeStatusCode varchar(25)      null, -- Core v1.9: CourseAcademicGradeStatusCodeType @ 1848
		CourseHonorsCodeType          varchar(12)      null, -- Core v1.9: CourseHonorsCodeType @ 2015
		PRIMARY KEY CLUSTERED
		(
			SchoolAssignedPersonID,
			LocalOrganizationIDCode,
			SessionSchoolYear,
			SessionTermCode,
			OriginalCourseId
		)
	);