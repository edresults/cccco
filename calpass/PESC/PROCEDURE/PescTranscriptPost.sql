CREATE PROCEDURE
	dbo.PescTranscriptPost
	(
		@Records dbo.PescTranscriptType READONLY
	)
AS

BEGIN
	MERGE
		dbo.PescTranscript t
	USING
		@Records s
	ON
		(
			s.SchoolAssignedPersonID      = t.SchoolAssignedPersonID
			and s.LocalOrganizationIDCode = t.LocalOrganizationIDCode
			and s.SessionSchoolYear       = t.SessionSchoolYear
			and s.SessionTermCode         = t.SessionTermCode
			and s.OriginalCourseId        = t.OriginalCourseId
		)
	WHEN MATCHED THEN
		UPDATE SET
			t.RecipientAssignedID         = s.RecipientAssignedID,
			t.BirthDate                   = s.BirthDate,
			t.FirstName                   = s.FirstName,
			t.LastName                    = s.LastName,
			t.CEEBACT                     = s.CEEBACT,
			t.StudentLevelCode            = s.StudentLevelCode,
			t.OrganizationName            = s.OrganizationName,
			t.AcademicCompletionDate      = s.AcademicCompletionDate,
			t.GradePointAverage           = s.GradePointAverage,
			t.AcademicSummaryType         = s.AcademicSummaryType,
			t.AgencyCourseID              = s.AgencyCourseID,
			t.CourseSectionNumber         = s.CourseSectionNumber,
			t.CourseTitle                 = s.CourseTitle,
			t.CourseCreditBasis           = s.CourseCreditBasis,
			t.CourseCreditUnits           = s.CourseCreditUnits,
			t.CourseCreditValue           = s.CourseCreditValue,
			t.CourseCreditEarned          = s.CourseCreditEarned,
			t.CourseAcademicGrade         = s.CourseAcademicGrade,
			t.ModifyDate                  = sysdatetime()
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				SchoolAssignedPersonID,
				RecipientAssignedID,
				BirthDate,
				FirstName,
				LastName,
				CEEBACT,
				LocalOrganizationIDCode,
				StudentLevelCode,
				OrganizationName,
				AcademicCompletionDate,
				GradePointAverage,
				AcademicSummaryType,
				SessionSchoolYear,
				SessionTermCode,
				OriginalCourseID,
				AgencyCourseID,
				CourseSectionNumber,
				CourseTitle,
				CourseCreditBasis,
				CourseCreditUnits,
				CourseCreditValue,
				CourseCreditEarned,
				CourseAcademicGrade,
				CreateDate,
				ModifyDate
			)
		VALUES
			(
				s.SchoolAssignedPersonID,
				s.RecipientAssignedID,
				s.BirthDate,
				s.FirstName,
				s.LastName,
				s.CEEBACT,
				s.LocalOrganizationIDCode,
				s.StudentLevelCode,
				s.OrganizationName,
				s.AcademicCompletionDate,
				s.GradePointAverage,
				s.AcademicSummaryType,
				s.SessionSchoolYear,
				s.SessionTermCode,
				s.OriginalCourseID,
				s.AgencyCourseID,
				s.CourseSectionNumber,
				s.CourseTitle,
				s.CourseCreditBasis,
				s.CourseCreditUnits,
				s.CourseCreditValue,
				s.CourseCreditEarned,
				s.CourseAcademicGrade,
				sysdatetime(),
				sysdatetime()
			);
END;