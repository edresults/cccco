USE calpass;

GO

IF (object_id('calpads.HSTranscriptGet', 'IF') is not null)
	BEGIN
		DROP FUNCTION calpads.HSTranscriptGet;
	END;

GO

CREATE FUNCTION
	calpads.HSTranscriptGet
	(
		@StudentStateId char(10)
	)
RETURNS
	TABLE
AS
	RETURN (
		SELECT DISTINCT
			/* school */
			e.DistrictCode,
			e.SchoolCode,
			e.GradeCode,
			e.YearCode,
			TermCode = coalesce(s.MarkingPeriodCode, s.TermCode),
			/* course */
			c.CourseName,
			CourseId = c.CourseId,
			c.CourseCode,
			UniversityCategory = s.UniversityCode,
			IsAG = case c.IsUniversityApproved when 'Y' then 1 else 0 end,
			IsCte = case c.IsCte when 'Y' then 1 else 0 end,
			/* section */
			m.MarkCode,
			m.IsSuccess,
			MarkCategory = m.Category,
			MarkPoints = m.Points,
			CreditTry = s.CreditTry * IsGpa,
			CreditGet = s.CreditGet * IsGpa,
			/* extras */
			CreditPoints = convert(decimal(6,3), s.CreditGet * IsGpa * m.Points)
		FROM
			calpads.Senr e with(index(pk_Senr))
			inner join
			calpads.Scsc s with(index(pk_Scsc))
				on s.StudentStateId = e.StudentStateId
				and s.SchoolCode = e.SchoolCode
				and s.YearCode = e.YearCode
			inner join
			calpads.Crsc c with(index(pk_Crsc))
				on c.SchoolCode = s.SchoolCode
				and c.YearCode = s.YearCode
				and c.TermCode = s.TermCode
				and c.CourseId = s.CourseId
				and c.SectionId = s.SectionId
			inner join
			calpads.Mark m
				on m.MarkCode = s.MarkCode
			inner join
			calpads.Grade g
				on g.GradeCode = e.GradeCode
		WHERE
			s.StudentStateId = @StudentStateId
			and g.IsHS = 1
	);