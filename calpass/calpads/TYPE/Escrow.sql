USE calpass;

GO

IF (type_id('calpads.Escrow') is not null)
	BEGIN
		DROP TYPE calpads.Escrow;
	END;

GO

CREATE TYPE
	calpads.Escrow
AS TABLE
	(
		StudentStateId char(10),
		SchoolCode char(7),
		YearCode char(9),
		EscrowName sysname,
		InterSegmentKey binary(64),
		SinfYearCode char(9),
		SinfEffectiveStartDate char(8),
		PRIMARY KEY CLUSTERED
			(
				StudentStateId,
				SchoolCode,
				YearCode,
				EscrowName
			),
		INDEX
			ix_Escrow__EscrowName
		NONCLUSTERED
			(
				EscrowName
			)
	);