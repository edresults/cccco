CREATE TABLE
    calpads.SIRS
    (
        ReportingLEA                   char(7)      not null,
        SchoolofAttendance             char(7)      not null,
        AcademicYearID                 char(9)      not null,
        SSID                           char(10)     not null,
        IncidentIDLocal                varchar(20)  not null,
        IncidentResultCode             char(3)      not null,
        IncidentResultAuthorityCode    char(2)          null,
        IncidentResultDurationDays     integer          null,
        IncidentResultModificationCode decimal(5,2)     null,
        CONSTRAINT PK_SIRS PRIMARY KEY ( SchoolofAttendance, AcademicYearID, SSID, IncidentIDLocal, IncidentResultCode )
    );