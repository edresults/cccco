USE calpass;

GO

IF (object_id('calpads.Term') is not null)
	BEGIN
		DROP TABLE calpads.Term;
	END;

GO

CREATE TABLE
	calpads.Term
	(
		TermCode            CHAR(2)      NOT NULL,
		Name                VARCHAR(255) NOT NULL,
		Description         VARCHAR(255) NOT NULL,
		Type                VARCHAR(255) NOT NULL,
		Season              VARCHAR(255) NOT NULL,
		TermRank            TINYINT      NOT NULL,
		IntraTermOrdinal    TINYINT      NOT NULL,
		CalendarYearOrdinal CHAR(1)      NOT NULL
	);

GO

ALTER TABLE
	calpads.Term
ADD CONSTRAINT
	pk_Term__TermCode
PRIMARY KEY CLUSTERED
	(
		TermCode
	);

GO

INSERT INTO
	calpads.Term
	(
		TermRank,
		TermCode,
		Name,
		Description,
		Type,
		Season,
		IntraTermOrdinal,
		CalendarYearOrdinal
	)
SELECT
	TermRank = row_number() over(order by LBound + UBound),
	TermCode,
	Name,
	Description,
	Type,
	Season,
	IntraTermOrdinal = rank() over(partition by Type order by TermCode),
	CalendarYearOrdinal = 
		case
			when Season = 'Annual' then '7'
			when Season = 'Summer' then '5'
			when Season = 'Fall' then '7'
			when Season = 'Winter' then '1'
			when Season = 'Spring' then '3'
		end
FROM
	(
		VALUES
		(0,100,'FY','Full Year','A session that lasts the full academic year.','Annual','Annual'),
		(0,16,'H1','First Hexmester','The first of six hexmesters in an Academic Year.','Hexmester','Fall'),
		(17,32,'H2','Second Hexmester','The second of six hexmesters in an Academic Year.','Hexmester','Fall'),
		(33,49,'H3','Third Hexmester','The third of six hexmesters in an Academic Year.','Hexmester','Winter'),
		(50,66,'H4','Fourth Hexmester','The fourth of six hexmesters in an Academic Year.','Hexmester','Spring'),
		(67,83,'H5','Fifth Hexmester','The fifth of six hexmesters in an Academic Year.','Hexmester','Spring'),
		(84,100,'H6','Sixth Hexmester','The sixth of six hexmesters in an Academic Year.','Hexmester','Summer'),
		(50,51,'IS','Intersession','An academic session that occurs during a short break during the academic year (not necessarily a longer, summer break), typical of year-round schools.','Intersession','Winter'),
		(0,25,'Q1','First Quarter','The first of four quarters of an Academic Year.','Quarter','Fall'),
		(26,50,'Q2','Second Quarter','The second of four quarters of an Academic Year.','Quarter','Winter'),
		(51,75,'Q3','Third Quarter','The third of four quarters of an Academic Year.','Quarter','Spring'),
		(76,100,'Q4','Fourth Quarter','The fourth and final quarter of an Academic Year.','Quarter','Summer'),
		(0,50,'S1','First Semester','The first of two semesters in an Academic Year.','Semester','Fall'),
		(51,100,'S2','Second Semester','The Second of two semesters in an Academic Year.','Semester','Spring'),
		(0,100,'SP','Supplemental Session','A session that occurs on evenings, after school, or weekends.','Supplemental','Annual'),
		(101,125,'SS','Summer Session','An academic session that occurs during the summer break.','Summer','Summer'),
		(0,33,'T1','First Trimester','The first of three trimesters in an Academic Year.','Trimester','Fall'),
		(34,66,'T2','Second Trimester','The second of three trimesters in an Academic Year.','Trimester','Winter'),
		(67,100,'T3','Third Trimester','The third of three trimesters in an Academic Year.','Trimester','Spring'),
		(0,14,'Z1','Other First Term','The first term in a set of terms not otherwise defined in this code set.','Other','Fall'),
		(15,28,'Z2','Other Second Term','The second term in a set of terms not otherwise defined in this code set.','Other','Fall'),
		(29,42,'Z3','Other Third Term','The third term in a set of terms not otherwise defined in this code set.','Other','Winter'),
		(43,56,'Z4','Other Fourth Term','The fourth term in a set of terms not otherwise defined in this code set.','Other','Winter'),
		(57,70,'Z5','Other Fifth Term','The fifth term in a set of terms not otherwise defined in this code set.','Other','Spring'),
		(71,84,'Z6','Other Sixth Term','The sixth term in a set of terms not otherwise defined in this code set.','Other','Spring'),
		(85,98,'Z7','Other Seventh Term','The seventh term in a set of terms not otherwise defined in this code set.','Other','Summer'),
		(99,112,'Z8','Other Eighth Term','The eighth term in a set of terms not otherwise defined in this code set.','Other','Summer'),
		(113,125,'Z9','Other Ninth Term','The ninth term in a set of terms not otherwise defined in this code set.','Other','Summer')
	) t
	(
		LBound,
		UBound,
		TermCode,
		Name,
		Description,
		Type,
		Season
	)