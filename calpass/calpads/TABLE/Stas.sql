USE calpass;

GO

IF (object_id('calpads.Stas') is not null)
	BEGIN
		DROP TABLE calpads.Stas;
	END;

GO

CREATE TABLE
	calpads.Stas
	(
		DistrictCode          char(7)      not null,
		SchoolCode            char(7)      not null,
		YearCode              char(9)      not null,
		StudentStateId        varchar(10)  not null,
		IsExempt              char(1)          null,
		IsHourly              char(1)          null,
		ExpectedAttenanceDays decimal(5,2)     null,
		AttendedDays          decimal(5,2)     null,
		SuspendedDays         decimal(5,2)     null,
		ExclusionDays         decimal(5,2)     null,
		ExcusedDays           decimal(5,2)     null,
		UnexcusedDays         decimal(5,2)     null,
		IncompleteDays        decimal(5,2)     null,
		IsEscrow              bit          not null
	);

GO

ALTER TABLE
	calpads.Stas
ADD CONSTRAINT
	PK_Stas
PRIMARY KEY CLUSTERED
	(
		StudentStateId,
		SchoolCode,
		YearCode
	);

ALTER TABLE
	calpads.Stas
ADD CONSTRAINT
	DF_Stas__YearCode
DEFAULT
	(
		'####-####'
	)
FOR
	YearCode;

GO

CREATE NONCLUSTERED INDEX
	IX_Stas__Operational
ON
	calpads.Stas
	(
		SchoolCode,
		YearCode
	);

CREATE NONCLUSTERED INDEX
	IX_Stas__IsEscrow
ON
	calpads.Stas
	(
		IsEscrow
	);