CREATE TABLE
	calpads.Person
	(
		PersonId int identity(0,1),
		NameFirst varchar(30),
		NameMiddle varchar(30),
		NameLast varchar(50),
		NameSuffix varchar(3),
		NameFirstAlias varchar(30),
		NameMiddleAlias varchar(30),
		NameLastAlias varchar(50),
		Gender char(1),
		BirthDate date,
		BirthCityName varchar(30),
		BirthStateCode varchar(6),
		BirthCountryCode char(2),
		PrimaryLanguageCode char(2),
		ModifyDate date
	)