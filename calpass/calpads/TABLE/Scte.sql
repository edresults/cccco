USE calpass;

GO

IF (object_id('calpads.Scte') is not null)
	BEGIN
		DROP TABLE calpads.Scte;
	END;

GO

CREATE TABLE
	calpads.Scte
	(
		RecordTypeCode varchar(6), -- valid: 4,5,6,7
		TransactionTypeCode char(1), -- valid: 4,5,6,7
		LocalRecordId varchar(255), -- valid: 4,5,6,7
		DistrictCode char(7), -- valid: 4,5,6,7
		SchoolCode char(7), -- valid: 4,5,6,7
		YearCode char(9)
			CONSTRAINT
				df_Scte__YearCode
			DEFAULT
				(
					'####-####'
				), -- valid: 4,5,6,7
		StudentIdState varchar(10), -- valid: 4,5,6,7
		CtePathwayCode char(3)
			CONSTRAINT
				df_Scte__CtePathwayCode
			DEFAULT
				(
					'XXX'
				), -- valid: 4,5,6,7
		CtePathwayCompletionYearCode char(9), -- valid: 4,5,6,7
		IsEscrow bit not null
	);

GO

CREATE CLUSTERED INDEX
	ixc_Scte__District
ON
	calpads.Scte
	(
		DistrictCode,
		SchoolCode,
		YearCode,
		StudentIdState,
		CtePathwayCode
	);

CREATE NONCLUSTERED INDEX
	ix_Scte__IsEscrow
ON
	calpads.Scte
	(
		IsEscrow
	);