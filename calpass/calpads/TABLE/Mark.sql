USE calpass;

GO

IF (object_id('calpads.Mark') is not null)
	BEGIN
		DROP TABLE calpads.Mark;
	END;

GO

CREATE TABLE
	calpads.Mark
	(
		MarkCode varchar(5) not null,
		Points decimal(3,2) not null,
		Category smallint not null,
		IsEnroll bit not null,
		IsComplete bit not null,
		IsCompleteDenominator bit not null,
		IsSuccess bit not null,
		IsSuccessDenominator bit not null,
		IsPromoted bit not null,
		IsPromotedDenominator bit not null,
		IsGpa bit not null,
		Rank bit not null,
		Abbreviation varchar(6) not null,
		Description varchar(255) not null
	);

GO

ALTER TABLE
	calpads.Mark
ADD CONSTRAINT
	PK_Mark
PRIMARY KEY CLUSTERED
	(
		MarkCode
	);

GO

INSERT INTO
	calpads.Mark
	(
		MarkCode,
		Points,
		Category,
		IsEnroll,
		IsComplete,
		IsCompleteDenominator,
		IsSuccess,
		IsSuccessDenominator,
		IsPromoted,
		IsPromotedDenominator,
		IsGpa,
		Rank,
		Abbreviation,
		Description
	)
SELECT
	MarkCode,
	Points,
	Category,
	IsEnroll,
	IsComplete,
	IsCompleteDenominator,
	IsSuccess,
	IsSuccessDenominator,
	IsPromoted = case when MarkCode = 'C-' then 0 else IsSuccess end,
	IsPromotedDenominator = IsSuccessDenominator,
	IsGpa,
	Rank = row_number() over(order by (select 1)),
	Abbreviation,
	Description
FROM
	(
		VALUES
		('A+',4,4,1,1,1,1,1,1,'A','Highest level of academic achievement where students demonstrate mastery of a subject'),
		('A',4,4,1,1,1,1,1,1,'A','Highest level of academic achievement where students demonstrate mastery of a subject'),
		('A-',3.7,4,1,1,1,1,1,1,'A','Highest level of academic achievement where students demonstrate mastery of a subject'),
		('B+',3.3,3,1,1,1,1,1,1,'B','Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
		('B',3,3,1,1,1,1,1,1,'B','Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
		('B-',2.7,3,1,1,1,1,1,1,'B','Second highest level of academic achievement where students demonstrate a good understanding of a subject'),
		('C+',2.3,2,1,1,1,1,1,1,'C','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
		('C',2,2,1,1,1,1,1,1,'C','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
		('C-',1.7,2,1,1,1,1,1,1,'C','Third highest level of academic achievement where students demonstrate an adequate understanding of a subject'),
		('CR',0,1,1,1,1,1,1,0,'CR','Credit'),
		('P',0,1,1,1,1,1,1,0,'CR','Pass'),
		('S',0,1,1,1,1,1,1,0,'S','Satisfactory Progress'),
		('SP',0,1,1,1,1,1,1,0,'S','Satisfactory Progress'),
		('D+',1.3,1,1,1,1,0,1,1,'D','Fourth highest level of academic achievement where students demonstrate an inadequate understanding of a subject'),
		('D',1,1,1,1,1,0,1,1,'D','Fourth highest level of academic achievement where students demonstrate an inadequate understanding of a subject'),
		('D-',0.7,1,1,1,1,0,1,1,'D','Fourth highest level of academic achievement where students demonstrate an inadequate understanding of a subject'),
		('F+',0.3,0,1,1,1,0,1,1,'F','Lowest level of academic achievement where students demonstrate a substandard understanding of a subject'),
		('F',0,0,1,1,1,0,1,1,'F','Lowest level of academic achievement where students demonstrate a substandard understanding of a subject'),
		('NC',0,0,1,1,1,0,1,0,'NC','No Credit'),
		('NP',0,0,1,1,1,0,1,0,'NC','No Pass'),
		('NS',0,0,1,1,1,0,1,0,'S','No Satisfactory Progress'),
		('IA+',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IA',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IA-',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IB+',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IB',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IB-',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IC+',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IC',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IC-',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('ID+',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('ID',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('ID-',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IF+',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IF',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('I',0,-99,1,1,1,0,1,0,'I','Incomplete'),
		('IP',0,-99,1,0,0,0,0,0,'I','Incomplete'),
		('ICR',0,-99,1,1,1,0,1,0,'I','Incomplete Credit'),
		('IPP',0,-99,1,1,1,0,1,0,'I','Incomplete Pass'),
		('INC',0,-99,1,1,1,0,1,0,'I','Incomplete Pass'),
		('INP',0,-99,1,1,1,0,1,0,'I','Incomplete No Pass'),
		('W',0,-99,1,0,1,0,1,0,'W','Withdrawl'),
		('FW',0,-99,1,0,1,0,1,0,'W','Withdrawl without permission and without having achieved a final passing Mark'),
		('MW',0,-99,1,0,0,0,0,0,'Other','Military withdraw'),
		('RD',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD0',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD1',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD2',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD3',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD4',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD5',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD6',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD7',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD8',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('RD9',0,-99,1,1,1,0,0,0,'Other','Report delayed'),
		('NG',0,-99,0,0,0,0,0,0,'Other','Not Graded'),
		('UG',0,-99,0,0,0,0,0,0,'Other','UnMarked'),
		('UD',0,-99,0,0,0,0,0,0,'Other','UnMarked Dependent'),
		('DR',0,-99,0,0,0,0,0,0,'Drop','Dropped section ON OR AFTER first census date and before withdraw period'),
		('XX',0,-99,0,0,0,0,0,0,'Other','None of the above or unknown')
	) t
	(
		MarkCode,
		Points,
		Category,
		IsEnroll,
		IsComplete,
		IsCompleteDenominator,
		IsSuccess,
		IsSuccessDenominator,
		IsGpa,
		Abbreviation,
		Description
	)