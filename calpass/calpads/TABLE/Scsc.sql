USE calpass;

GO

IF (object_id('calpads.Scsc') is not null)
	BEGIN
		DROP TABLE calpads.Scsc;
	END;

GO

CREATE TABLE
	calpads.Scsc
	(
		RecordTypeCode varchar(6), -- valid: 3,4,5,6,7
		TransactionTypeCode char(1), -- valid: 3,4,5,6,7
		LocalRecordId varchar(255), -- valid: 3,4,5,6,7
		DistrictCode char(7), -- valid: 3,4,5,6,7
		SchoolCode char(7), -- valid: 3,4,5,6,7
		YearCode char(9), -- valid: 3,4,5,6,7
		StudentIdState varchar(10), -- valid: 3,4,5,6,7
		CourseId varchar(10), -- valid: 3,4,5,6,7
		SectionId varchar(10), -- valid: 3,4,5,6,7
		TermCode char(2), -- valid: 3,4,5,6,7
		CreditTry decimal(4,2), -- valid: 3,4,5,6,7
		CreditGet decimal(4,2), -- valid: 3,4,5,6,7
		MarkCode varchar(3), -- valid: 3,4,5,6,7
		UniversityCode varchar(2), -- valid: 3,4,5,6,7
		MarkingPeriodCode char(2), -- valid: 4,5,6,7
		IsEscrow bit not null
	);

GO

ALTER TABLE
	calpads.Scsc
ADD CONSTRAINT
	pk_Scsc
PRIMARY KEY CLUSTERED
	(
		StudentIdState,
		SchoolCode,
		YearCode,
		TermCode,
		CourseId,
		SectionId,
		MarkingPeriodCode
	);

CREATE NONCLUSTERED INDEX
	ix_Scsc__Section
ON
	calpads.Scsc
	(
		SectionId,
		CourseId,
		TermCode,
		YearCode,
		SchoolCode
	);

CREATE NONCLUSTERED INDEX
	ix_scsc__IsEscrow
ON
	calpads.scsc
	(
		IsEscrow
	);