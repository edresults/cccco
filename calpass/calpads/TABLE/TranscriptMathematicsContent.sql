CREATE TABLE
	calpads.TranscriptMathematicsContent
	(
		Code                    CHAR(4) NOT NULL,
		TranscriptMathematicsId TINYINT NOT NULL
	);

ALTER TABLE
	calpads.TranscriptMathematicsContent
ADD CONSTRAINT
	PK_TranscriptMathematicsContent
PRIMARY KEY CLUSTERED
	(
		Code
	);

ALTER TABLE
	calpads.TranscriptMathematicsContent
ADD CONSTRAINT
	FK_TranscriptMathematicsContent_TranscriptMathematicsId
FOREIGN KEY
	(
		TranscriptMathematicsId
	)
REFERENCES
	calpads.TranscriptMathematics
	(
		TranscriptMathematicsId
	);

GO

INSERT INTO
	calpads.TranscriptMathematicsContent
	(
		Code,
		TranscriptMathematicsId
	)
VALUES
	('2400',1),
	('2401',1),
	('2402',1),
	('2403',2),
	('2404',6),
	('2407',10),
	('2408',6),
	('2409',5),
	('2410',8),
	('2411',6),
	('2412',6),
	('2413',5),
	('2414',11),
	('2415',12),
	('2417',6),
	('2420',1),
	('2421',1),
	('2422',13),
	('2424',1),
	('2425',3),
	('2426',4),
	('2427',7),
	('2428',2),
	('2429',2),
	('2430',9),
	('2433',1),
	('2437',2),
	('2438',6),
	('2439',5),
	('2440',3),
	('2441',4),
	('2442',7),
	('2443',9),
	('2444',11),
	('2445',8),
	('2446',2),
	('2447',2),
	('2460',11),
	('2461',11),
	('2462',12),
	('2463',11),
	('2464',5),
	('2467',2),
	('2468',5),
	('2469',5),
	('2473',6),
	('2480',12),
	('2481',12),
	('2483',8),
	('2485',2);