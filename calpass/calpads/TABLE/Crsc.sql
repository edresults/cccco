USE calpass;

GO

IF (object_id('calpads.Crsc') is not null)
	BEGIN
		DROP TABLE calpads.Crsc;
	END;

GO

CREATE TABLE
	calpads.Crsc
	(
		DistrictCode char(7),
		SchoolCode char(7),
		YearCode char(9),
		CourseCode char(4),
		CourseId varchar(10),
		CourseName varchar(50),
		CourseContentCode char(3),
		CourseLevelCode char(1),
		IsCte char(1),
		IsUniversityApproved char(1),
		SectionId varchar(10),
		TermCode char(2),
		EmployeeIdState char(10),
		EmployeeIdLocal varchar(10),
		ClassId varchar(20),
		SectionLeveLCode char(2),
		ServiceCode char(1),
		InstructionLanguage char(2),
		StrategyCode char(3),
		IsIndependentStudy char(1),
		IsDistanceLearning char(1),
		MultipleTeacherCode char(1),
		ProgramFundingSource char(3),
		CteCourseSectionProviderCode char(1),
		CompetencyCode char(1),
		IsEscrow bit not null
	);

GO

ALTER TABLE
	calpads.Crsc
ADD CONSTRAINT
	pk_Crsc
PRIMARY KEY CLUSTERED
	calpads.Crsc
	(
		SchoolCode,
		YearCode,
		TermCode,
		CourseId,
		SectionId,
		EmployeeIdState
	);

CREATE NONCLUSTERED INDEX
	ix_Crsc__IsEscrow
ON
	calpads.Crsc
	(
		IsEscrow
	);


ALTER TABLE calpads.CRSC ADD CourseContentAreaSubcategoryCode                   varchar(7) null;
ALTER TABLE calpads.CRSC ADD DepartmentalizedCourseStandardsGradeLevelRangeCode varchar(3) null;
ALTER TABLE calpads.CRSC ADD ContentStandardsAlignmentCode                      char(1)    null;
ALTER TABLE calpads.CRSC ADD CharterNonCoreNonCollegePrepCourseIndicator        char(1)    null;
ALTER TABLE calpads.CRSC ADD ApIbCourseCodeCrossReference                       char(4)    null;
ALTER TABLE calpads.CRSC ADD OnlineCourseInstructionTypeCode                    char(1)    null;
ALTER TABLE calpads.CRSC ADD MiddleSchoolCoreCourseIndicator                    char(1)    null;
ALTER TABLE calpads.CRSC ADD LocalAssignmentOptionCode                          varchar(2) null;
ALTER TABLE calpads.CRSC ADD HighQualityCteCourseIndicator                      char(1)    null;