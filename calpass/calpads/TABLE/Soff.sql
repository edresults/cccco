CREATE TABLE
    calpads.SOFF
    (
        ReportingLEA          char(7)     not null,
        SchoolofAttendance    char(7)     not null,
        AcademicYearID        char(9)     not null,
        SSID                  char(10)    not null,
        IncidentIDLocal       varchar(20) not null,
        StudentOffenseCode    char(3)     not null,
        WeaponCategoryCode    char(2)         null,
        CONSTRAINT PK_SOFF PRIMARY KEY ( SchoolofAttendance, AcademicYearID, SSID, IncidentIDLocal, StudentOffenseCode )
    );