USE calpass;

GO

IF (object_id('calpads.TermLegacy') is not null)
	BEGIN
		DROP TABLE calpads.TermLegacy;
	END;

GO

CREATE TABLE
	calpads.TermLegacy
	(
		TermLegacyCode varchar(3) not null,
		TermCode char(2) not null
	);

GO

ALTER TABLE
	calpads.TermLegacy
ADD CONSTRAINT
	PK_TermLegacy
PRIMARY KEY CLUSTERED
	(
		TermLegacyCode
	);

GO

INSERT
	calpads.TermLegacy
	(
		TermLegacyCode,
		TermCode
	)
VALUES
	('CQ1','Q1'),
	('CQ2','Q2'),
	('CQ3','Q3'),
	('CQ4','Q4'),
	('CS1','S1'),
	('CS2','S2'),
	('FLS','FY'),
	('FY','FY'),
	('FS1','SS'),
	('FS2','SS'),
	('FS3','SS'),
	('HX1','H1'),
	('HX2','H2'),
	('HX3','H3'),
	('HX4','H4'),
	('HX5','H5'),
	('HX6','H6'),
	('H1','H1'),
	('H2','H2'),
	('H3','H3'),
	('H4','H4'),
	('H5','H5'),
	('H6','H6'),
	('IS1','IS'),
	('IS2','IS'),
	('IS3','IS'),
	('QQ1','SS'),
	('QQ2','SS'),
	('QS1','SS'),
	('QS2','SS'),
	('QS3','SS'),
	('QT1','Q1'),
	('QT2','Q2'),
	('QT3','Q3'),
	('QT4','Q4'),
	('Q1','Q1'),
	('Q2','Q2'),
	('Q3','Q3'),
	('Q4','Q4'),
	('SC1','SS'),
	('SC2','SS'),
	('SM1','S1'),
	('SM2','S2'),
	('S1','S1'),
	('S2','S2'),
	('SPL','SP'),
	('SS1','SS'),
	('SS','SS'),
	('SS2','SS'),
	('SS3','SS'),
	('TS1','T1'),
	('TS2','T2'),
	('TS3','T3'),
	('T1','T1'),
	('T2','T2'),
	('T3','T3'),
	('TR1','T1'),
	('TR2','T2'),
	('TR3','T3'),
	('ZO1','Z1'),
	('ZO2','Z2'),
	('ZO3','Z3'),
	('ZO4','Z4'),
	('ZO5','Z5'),
	('ZO6','Z6'),
	('ZO7','Z7'),
	('ZO8','Z8'),
	('ZO9','Z9'),
	('Z1','Z1'),
	('Z2','Z2'),
	('Z3','Z3'),
	('Z4','Z4'),
	('Z5','Z5'),
	('Z6','Z6'),
	('Z7','Z7'),
	('Z8','Z8'),
	('Z9','Z9');