USE calpass;

GO

IF (object_id('calpads.Student') is not null)
	BEGIN
		DROP TABLE calpads.Student;
	END;

GO

CREATE TABLE
	calpads.Student
	(
		StudentStateId char(10) not null,
		InterSegmentKey binary(64) not null,
		InterSegmentKeyBug binary(64) not null
	);

GO

ALTER TABLE
	calpads.Student
ADD CONSTRAINT
	pk_Student
PRIMARY KEY CLUSTERED
	(
		StudentStateId
	);

CREATE NONCLUSTERED INDEX
	ix_Student__InterSegmentKey
ON
	calpads.Student
	(
		InterSegmentKey
	);

CREATE NONCLUSTERED INDEX
	ix_Student__InterSegmentKeyBug
ON
	calpads.Student
	(
		InterSegmentKeyBug
	);