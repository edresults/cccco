DROP TABLE IF EXISTS calpads.course_catalog;

GO

CREATE TABLE
    calpads.course_catalog
    (
        school_code              char(7)     not null,
        academic_year            char(9)     not null,
        course_id                varchar(10) not null,
        section_id               varchar(10) not null,
        academic_term            char(2)     not null,
        district_code            char(7)     not null,
        course_code              char(4)     not null,
        university_approved      char(1)     not null,
        instructional_level_code char(2)         null,
        ap_ib_code_cross_ref     char(4)         null
    );

GO

BULK INSERT
    calpads.course_catalog
FROM
    N'\\10.11.4.21\Backups\Source Files\Data\calpads_course_catalog.txt'
WITH
    (
        FORMATFILE = N'\\10.11.4.21\Backups\Source Files\Formats\course_catalog.fmt',
        FIRSTROW = 2,
        TABLOCK
    );

GO

DELETE
    a
FROM
    (
        SELECT
            row_num = row_number() over(partition by school_code, academic_year, course_id, section_id, academic_term order by (select 1) )
        FROM
            calpads.course_catalog
    ) a
WHERE
    row_num > 1;

GO

ALTER TABLE
    calpads.course_catalog
ADD CONSTRAINT
    pk_course_catalog
PRIMARY KEY
    (
        school_code,
        academic_year,
        course_id,
        section_id,
        academic_term
    );