USE calpass;

GO

IF (object_id('calpads.Grade') is not null)
	BEGIN
		DROP TABLE calpads.Grade;
	END;

GO

CREATE TABLE
	calpads.Grade
	(
		GradeCode char(2) not null,
		Category char(2) not null,
		IsHS bit not null,
		Rank tinyint not null,
		Label varchar(255) not null,
		Description varchar(255) not null
	);

GO

ALTER TABLE
	calpads.Grade
ADD CONSTRAINT
	PK_Grade
PRIMARY KEY CLUSTERED
	(
		GradeCode
	);

GO

CREATE NONCLUSTERED INDEX
	IX_Grade__IsHS
ON
	calpads.Grade
	(
		IsHS
	)
INCLUDE
	(
		Rank
	);

GO

INSERT INTO
	calpads.Grade
	(
		GradeCode,
		Category,
		IsHS,
		Rank,
		Label,
		Description
	)
VALUES
	('01','GR',0,5,'First Grade','Grade One'),
	('02','GR',0,6,'Second Grade','Grade Two'),
	('03','GR',0,7,'Third Grade','Grade Three'),
	('04','GR',0,8,'Fourth Grade','Grade Four'),
	('05','GR',0,9,'Fifth Grade','Grade Five'),
	('06','JR',0,10,'Sixth Grade','Grade Six'),
	('07','JR',0,11,'Seventh Grade','Grade Seven'),
	('08','JR',0,12,'Eighth Grade','Grade Eight'),
	('09','HS',1,13,'Ninth Grade','Grade Nine'),
	('10','HS',1,14,'Tenth Grade','Grade Ten'),
	('11','HS',1,15,'Eleventh Grade','Grade Eleven'),
	('12','HS',1,16,'Twelfth Grade','Grade Twelve'),
	('AD','AD',0,17,'Adult','A category for students who are primarily enrolled in an adult education center. These students would not have an affiliation with a K-12 institution.'),
	('IN','PR',0,1,'Infant','A grade level for individuals, ages 0 through 18 months.'),
	('KN','PR',0,4,'Kindergarten','Kindergarten'),
	('PS','PR',0,3,'Prekindergarten','A grade level for children formally enrolled in a preschool program in preparation for entrance to kindergarten.'),
	('TD','PR',0,2,'Toddlers','A grade level for individuals, ages 19 months through 35 months, not yet enrolled in a public educational institution or formal preschool program.'),
	('UE','XX',0,0,'Ungraded Elementary','A grade level for students enrolled in elementary classes (kindergarten through eighth grades) that are not assigned to a specific grade level. This grade level is specifically for students in a special education program.'),
	('US','XX',0,0,'Ungraded Secondary','A grade level for students enrolled in secondary classes (ninth through twelfth grades) that are not assigned to a specific grade level. This grade level is specifically for students in a special education program.'),
	('UU','XX',0,0,'Unknown','The grade level cannot be determined. For example, there were multiple responses or no response at all. You would not see this option on a form, but rather this category is used for storage of data anomalies.');