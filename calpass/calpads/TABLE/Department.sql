USE calpass;

GO

IF (object_id('calpads.Department') is not null)
	BEGIN
		DROP TABLE calpads.Department;
	END;

GO

CREATE TABLE
	calpads.Department
	(
		Code tinyint not null,
		Name varchar(255) not null
	);

GO

ALTER TABLE
	calpads.Department
ADD CONSTRAINT
	PK_Department
PRIMARY KEY CLUSTERED
	(
		Code
	);

GO

INSERT
	calpads.Department
	(
		Code,
		Name
	)
VALUES
	(0, 'All'),
	(10,'Art'),
	(11,'Computer Education'),
	(12,'Dance'),
	(13,'Drama or Theater'),
	(14,'English Language Arts'),
	(15,'Foreign Languages'),
	(16,'Health Education'),
	(18,'Mathematics'),
	(19,'Physical Education'),
	(20,'Special Designated Subjects'),
	(21,'Science'),
	(22,'History or Social Science'),
	(40,'Career Technical Education'),
	(41,'Self-Contained Class'),
	(42,'Music'),
	(43,'Other Instruction-Related Assignments');

GO

UPDATE
    t
SET
    t.Name = s.Name
FROM
    calpads.Department t
    inner join
    (
        VALUES
        (10, 'Art'),
        (11, 'Computer Education'),
        (12, 'Dance'),
        (13, 'Drama or Theater'),
        (14, 'English'),
        (15, 'World Languages'),
        (16, 'Health Education'),
        (18, 'Mathematics'),
        (19, 'Physical Education'),
        (20, 'Special Designated Subjects'),
        (21, 'Science'),
        (22, 'History or Social Science'),
        (23, 'Business'),
        (24, 'Media Arts'),
        (25, 'Agriculture'),
        (40, 'Career Technical Education'),
        (41, 'Self-Contained Class'),
        (42, 'Music'),
        (43, 'Interdisciplinary Content'),
        (43, 'Other Instruction-Related Assignments'),
        (44, 'Work Experience Education')
    ) s (Code, Name) on
        s.Code = t.Code;

INSERT INTO
    calpads.Department
    (
        Code,
        Name
    )
SELECT
    Code,
    Name
FROM
    (
        VALUES
        (10, 'Art'),
        (11, 'Computer Education'),
        (12, 'Dance'),
        (13, 'Drama or Theater'),
        (14, 'English'),
        (15, 'World Languages'),
        (16, 'Health Education'),
        (18, 'Mathematics'),
        (19, 'Physical Education'),
        (20, 'Special Designated Subjects'),
        (21, 'Science'),
        (22, 'History or Social Science'),
        (23, 'Business'),
        (24, 'Media Arts'),
        (25, 'Agriculture'),
        (40, 'Career Technical Education'),
        (41, 'Self-Contained Class'),
        (42, 'Music'),
        (43, 'Interdisciplinary Content'),
        (43, 'Other Instruction-Related Assignments'),
        (44, 'Work Experience Education')
    ) s (Code, Name)
WHERE
    not exists (
        SELECT
            1
        FROM
            calpads.Department t
        WHERE
            t.Code = s.Code
    );