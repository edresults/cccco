CREATE TABLE
    calpads.SINC
    (
        ReportingLEA                                 char(7)     not null,
        SchoolofAttendance                           char(7)     not null,
        AcademicYearID                               char(9)     not null,
        SSID                                         char(10)    not null,
        IncidentIDLocal                              varchar(20) not null,
        IncidentOccurrenceDate                       char(8)     not null,
        StatutoryOffenseIndicator                    char(1)     not null,
        StudentInstructionalSupportIndicator         char(1)         null,
        RemovaltoInterimAlternativeSettingReasonCode char(1)         null,
        CONSTRAINT PK_SINC PRIMARY KEY CLUSTERED ( SchoolofAttendance, AcademicYearID, SSID, IncidentIDLocal )
    );