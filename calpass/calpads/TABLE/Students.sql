CREATE TABLE
	calpads.Students
	(
		InterSegmentKey BINARY(64) NOT NULL,
		StudentStateId  CHAR(10)   NOT NULL,
		IsCollision     BIT            NULL,
		IsEscrow        BIT            NULL,
		INDEX
			IC_Students
		CLUSTERED
			(
				InterSegmentKey,
				StudentStateId
			)
	);