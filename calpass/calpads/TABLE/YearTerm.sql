USE calpass;

GO

IF (object_id('calpads.YearTerm') is not null)
	BEGIN
		DROP TABLE calpads.YearTerm;
	END;

GO

CREATE TABLE
	calpads.YearTerm
	(
		YearCode char(9) not null,
		TermCode char(2) not null,
		YearTermCode char(5) not null
	);

GO

ALTER TABLE
	calpads.YearTerm
ADD CONSTRAINT
	PK_YearTerm
PRIMARY KEY CLUSTERED
	(
		YearCode,
		TermCode
	);

GO

INSERT
	calpads.YearTerm
	(
		YearCode,
		TermCode,
		YearTermCode
	)
SELECT
	YearCode,
	TermCode,
	YearTermCode = 
		convert(
			char(4),
			case
				when Season = 'Annual' then YearTrailing
				when Season = 'Summer' then YearLeading
				when Season = 'Fall' then YearTrailing
				when Season = 'Winter' then YearLeading
				when Season = 'Spring' then YearLeading
			end
		) + CalendarYearOrdinal
FROM
	calpads.Year y
	cross join
	calpads.Term t