CREATE TABLE
    calpads.CourseContent
    (
        CourseCode  char(4)     not null,
        ContentCode varchar(15) not null,
        Rank        tinyint     not null,
        CONSTRAINT PK_CourseContent PRIMARY KEY CLUSTERED ( CourseCode, ContentCode ),
        CONSTRAINT FK_CourseContent_Course FOREIGN KEY ( CourseCode ) REFERENCES calpads.Course ( Code ),
        CONSTRAINT FK_CourseContent__Content FOREIGN KEY ( ContentCode ) REFERENCES calpads.Content ( Code )
    );

GO

INSERT INTO
    calpads.CourseContent
    (
        CourseCode,
        Rank,
        ContentCode
    )
VALUES
    -- ESL
    ('2110', 1, 'ESL'           ),
    ('9104', 1, 'ESL'           ),
    -- English
    ('2100', 2, 'English'       ),
    ('2101', 2, 'English'       ),
    ('2102', 2, 'Remedial'      ),
    ('2105', 2, 'English'       ),
    ('2106', 2, 'English'       ),
    ('2107', 2, 'English'       ),
    ('2108', 2, 'English'       ),
    ('2109', 2, 'English'       ),
    ('2111', 2, 'English'       ),
    ('2112', 2, 'English'       ),
    ('2115', 2, 'English'       ),
    ('2116', 2, 'English'       ),
    ('2117', 2, 'English'       ),
    ('2120', 2, 'English'       ),
    ('2161', 2, 'English'       ),
    ('2173', 2, 'English'       ),
    ('2174', 2, 'English'       ),
    ('2175', 2, 'English'       ),
    ('2190', 2, 'English'       ),
    ('2198', 2, 'English'       ),
    ('9101', 2, 'English'       ),
    ('9110', 2, 'English'       ),
    ('9121', 2, 'English'       ),
    ('2130', 3, 'English'       ),
    ('9105', 3, 'English'       ),
    ('2131', 4, 'English'       ),
    ('9106', 4, 'English'       ),
    ('2132', 5, 'English'       ),
    ('9107', 5, 'English'       ),
    ('2133', 6, 'English'       ),
    ('2160', 6, 'English'       ),
    ('9108', 6, 'English'       ),
    ('2113', 7, 'Expository'    ),
    ('2114', 7, 'Expository'    ),
    ('2118', 7, 'Expository'    ),
    ('9102', 7, 'Expository'    ),
    ('9118', 7, 'Expository'    ),
    ('9119', 7, 'Expository'    ),
    ('2171', 8, 'LiteratureAP'  ),
    ('2170', 9, 'LanguageAP'    ),
    ('9100', 2, 'English'       ),
    ('9103', 2, 'English'       ),
    ('9109', 2, 'English'       ),
    ('9111', 7, 'English'       ),
    ('9112', 2, 'English'       ),
    ('9113', 4, 'English'       ),
    ('9114', 4, 'English'       ),
    ('9115', 7, 'English'       ),
    ('9116', 7, 'English'       ),
    ('9117', 7, 'English'       ),
    ('9120', 7, 'English'       ),
    -- Mathematics
    ('2400', 1, 'Arithmetic'    ),
    ('2401', 1, 'Arithmetic'    ),
    ('2402', 1, 'Arithmetic'    ),
    ('2403', 3, 'AlgebraI'      ),
    ('2404', 5, 'AlgebraII'     ),
    ('2407', 7, 'Trigonometry'  ),
    ('2408', 5, 'AlgebraII'     ),
    ('2409', 4, 'Geometry'      ),
    ('2410', 6, 'Statistics'    ),
    ('2411', 5, 'AlgebraII'     ),
    ('2412', 5, 'AlgebraII'     ),
    ('2413', 4, 'Geometry'      ),
    ('2414', 7, 'PreCalculus'   ),
    ('2415', 8, 'CalculusI'     ),
    ('2417', 5, 'AlgebraII'     ),
    ('2420', 2, 'PreAlgebra'    ),
    ('2421', 2, 'PreAlgebra'    ),
    ('2422', 7, 'PreCalculus'   ),
    ('2424', 2, 'PreAlgebra'    ),
    ('2425', 3, 'AlgebraI'      ),
    ('2426', 4, 'Geometry'      ),
    ('2427', 5, 'AlgebraII'     ),
    ('2428', 3, 'AlgebraI'      ),
    ('2429', 3, 'AlgebraI'      ),
    ('2430', 6, 'Statistics'    ),
    ('2433', 2, 'PreAlgebra'    ),
    ('2437', 3, 'AlgebraI'      ),
    ('2438', 5, 'AlgebraII'     ),
    ('2439', 4, 'Geometry'      ),
    ('2440', 3, 'AlgebraI'      ),
    ('2441', 4, 'Geometry'      ),
    ('2442', 5, 'AlgebraII'     ),
    ('2443', 6, 'Statistics'    ),
    ('2444', 7, 'PreCalculus'   ),
    ('2445', 6, 'Statistics'    ),
    ('2446', 3, 'AlgebraI'      ),
    ('2447', 3, 'AlgebraI'      ),
    ('2460', 7, 'PreCalculus'   ),
    ('2461', 7, 'PreCalculus'   ),
    ('2462', 8, 'CalculusI'     ),
    ('2463', 7, 'PreCalculus'   ),
    ('2464', 4, 'Geometry'      ),
    ('2467', 3, 'AlgebraI'      ),
    ('2468', 4, 'Geometry'      ),
    ('2469', 4, 'Geometry'      ),
    ('2473', 5, 'AlgebraII'     ),
    ('2480', 8, 'CalculusIAP'   ),
    ('2481', 9, 'CalculusIIAP'  ),
    ('2483', 8, 'StatisticsAP'  ),
    ('2485', 3, 'AlgebraI'      ),
    ('9240', 2, 'PreAlgebra'    ),
    ('9241', 3, 'AlgebraI'      ),
    ('9242', 4, 'Geometry'      ),
    ('9243', 5, 'AlgebraII'     ),
    ('9244', 6, 'Statistics'    ),
    ('9245', 1, 'Arithmetic'    ),
    ('9246', 5, 'AlgebraII'     ),
    ('9247', 7, 'PreCalculus'   ),
    ('9248', 2, 'PreAlgebra'    ),
    ('9249', 3, 'AlgebraI'      ),
    ('9250', 3, 'AlgebraI'      ),
    ('9251', 3, 'AlgebraI'      ),
    ('9252', 5, 'AlgebraII'     ),
    ('9253', 5, 'AlgebraII'     ),
    ('9254', 3, 'AlgebraI'      ),
    ('9255', 4, 'Geometry'      ),
    ('9256', 7, 'Trigonometry'  ),
    ('9257', 7, 'PreCalculus'   ),
    ('9258', 8, 'CalculusI'     ),
    ('9259', 6, 'Statistics'    ),
    ('9260', 1, 'Arithmetic'    ),
    ('9261', 5, 'AlgebraII'     ),
    ('9262', 7, 'PreCalculus'   ),
    ('9263', 7, 'PreCalculus'   ),
    ('9264', 3, 'AlgebraI'      ),
    ('9265', 7, 'PreCalculus'   ),
    ('9266', 8, 'CalculusIAP'   ),
    ('9267', 8, 'CalculusIAP'   ),
    ('9268', 8, 'CalculusIAP'   ),
    ('9269', 7, 'PreCalculus'   ),
    ('9270', 7, 'PreCalculus'   ),
    ('9271', 7, 'PreCalculus'   ),
    ('9272', 7, 'PreCalculus'   ),
    ('9273', 5, 'AlgebraII'     ),
    ('9274', 7, 'PreCalculus'   ),
    ('9275', 3, 'AlgebraI'      ),
    ('9276', 3, 'AlgebraI'      ),
    ('9277', 3, 'AlgebraI'      ),
    ('9278', 5, 'AlgebraII'     ),
    ('9279', 2, 'PreAlgebra'    ),
    ('9280', 5, 'AlgebraII'     );