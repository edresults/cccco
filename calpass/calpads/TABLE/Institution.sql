IF (object_id('calpads.Institution') is not null)
    BEGIN
        DROP TABLE calpads.Institution;
    END;

GO

CREATE TABLE
    calpads.Institution
    (
        CDSCode              CHAR(14)     NOT NULL,
        NCESDist             CHAR(7)          NULL,
        NCESSchool           CHAR(5)          NULL,
        StatusType           VARCHAR(50)  NOT NULL,
        County               VARCHAR(15)  NOT NULL,
        District             VARCHAR(90)  NOT NULL,
        School               VARCHAR(90)      NULL,
        Street               VARCHAR(211)     NULL,
        StreetAbr            VARCHAR(201)     NULL,
        City                 VARCHAR(25)      NULL,
        Zip                  VARCHAR(10)      NULL,
        State                CHAR(2)          NULL,
        MailStreet           VARCHAR(211)     NULL,
        MailStrAbr           VARCHAR(201)     NULL,
        MailCity             VARCHAR(25)      NULL,
        MailZip              VARCHAR(10)      NULL,
        MailState            CHAR(2)          NULL,
        Phone                CHAR(14)         NULL,
        Ext                  VARCHAR(6)       NULL,
        FaxNumber            VARCHAR(14)      NULL,
        Email                VARCHAR(50)      NULL,
        Website              VARCHAR(100)     NULL,
        OpenDate             DATE             NULL,
        ClosedDate           DATE             NULL,
        Charter              CHAR(1)          NULL,
        CharterNum           CHAR(4)          NULL,
        FundingType          VARCHAR(25)      NULL,
        DOC                  CHAR(2)      NOT NULL,
        DOCType              VARCHAR(50)  NOT NULL,
        SOC                  CHAR(2)          NULL,
        SOCType              VARCHAR(50)      NULL,
        EdOpsCode            VARCHAR(20)      NULL,
        EdOpsName            VARCHAR(100)     NULL,
        EILCode              VARCHAR(50)      NULL,
        EILName              VARCHAR(50)      NULL,
        GSoffered            VARCHAR(101)     NULL,
        GSserved             VARCHAR(101)     NULL,
        Virtual              CHAR(1)          NULL,
        Magnet               CHAR(1)          NULL,
        YearRoundYN          CHAR(1)          NULL,
        FederalDFCDistrictID CHAR(7)          NULL,
        Latitude             DECIMAL(10,7)    NULL,
        Longitude            DECIMAL(10,7)    NULL,
        AdmFName             VARCHAR(20)      NULL,
        AdmLName             VARCHAR(40)      NULL,
        AdmEmail             VARCHAR(50)      NULL,
        LastUpdate           DATE         NOT NULL
    );

GO

CREATE CLUSTERED INDEX
    ixc_Institution__CDSCode
ON
    calpads.Institution
    (
        CDSCode
    );

GO

BULK INSERT
    calpads.Institution
FROM
    N'\\PRO-DAT-SQL-03\C$\Data\K12\pubschls.txt'
WITH
    (
        FORMATFILE = N'\\PRO-DAT-SQL-03\C$\Data\K12\pubschls.fmt',
        FIRSTROW = 2
    );