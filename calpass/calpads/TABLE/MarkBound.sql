USE calpass;

GO

IF (object_id('calpads.MarkBound') is not null)
	BEGIN
		DROP TABLE calpads.MarkBound;
	END;

GO

CREATE TABLE
	calpads.MarkBound
	(
		BoundLower decimal(3,2) not null,
		BoundUpper decimal(3,2) not null,
		Category smallint not null
	);

GO

ALTER TABLE
	calpads.MarkBound
ADD CONSTRAINT
	pk_MarkBound
PRIMARY KEY CLUSTERED
	(
		BoundLower,
		BoundUpper
	);

GO

INSERT INTO
	calpads.MarkBound
	(
		BoundLower,
		BoundUpper,
		Category
	)
VALUES
	(3.50,4.00,4),
	(2.50,3.49,3),
	(1.50,2.49,2),
	(0.50,1.49,1),
	(0.00,0.49,0);