USE calpass;

GO

IF (object_id('calpads.Sinf') is not null)
	BEGIN
		DROP TABLE calpads.Sinf;
	END;

GO

CREATE TABLE
	calpads.Sinf
	(
		RecordTypeCode varchar(6),
		TransactionTypeCode char(1),
		LocalRecordId varchar(255),
		EffectiveStartDate char(8),
		EffectiveEndDate char(8),
		DistrictCode char(7),
		SchoolCode char(7),
		YearCode char(9)
			CONSTRAINT
				df_Sinf__YearCode
			DEFAULT
				(
					'####-####'
				),
		StudentIdState varchar(10),
		StudentIdLocal varchar(15),
		NameFirst varchar(30),
		NameMiddle varchar(30),
		NameLast varchar(50),
		NameSuffix varchar(3),
		NameFirstAlias varchar(30),
		NameMiddleAlias varchar(30),
		NameLastAlias varchar(50),
		Birthdate char(8),
		Gender char(1),
		Birthcity varchar(30),
		Birthstate varchar(6),
		Birthcountry char(2),
		PrimaryLanguageCode char(2),
		IsHispanicEthnicity char(1),
		IsMissingEthnicity char(1),
		Race01 char(3),
		Race02 char(3),
		Race03 char(3),
		Race04 char(3),
		Race05 char(3),
		IsMissingRace char(1),
		GradeCode char(2),
		PrimaryResidenceCategoryCode char(3),
		TransferCode char(1),
		ResidenceDistrictCode char(7),
		AddressLine01 varchar(60),
		AddressLine02 varchar(60),
		AddressCityName varchar(30),
		AddressStateCode varchar(6),
		AddressZipCode varchar(10),
		ElaStatusCode varchar(4),
		ElaStatusStartDate char(8),
		IsElaProficient char(1),
		IsBirthCountrySpecial char(1),
		InitialSchoolEnrollDate char(8),
		IsNotCumulativeThreeYearEnroll char(1),
		InitialNinthGradeEntryYear char(9),
		CtePathwayCode char(3),
		IsCtePathwayComplete char(1),
		ParentHighestEducationLevelCode char(2),
		Parent01NameFirst varchar(30),
		Parent01NameLast varchar(50),
		Parent02NameFirst varchar(30),
		Parent02NameLast varchar(50),
		IsEscrow bit not null
	);

GO

CREATE CLUSTERED INDEX
	ixc_Sinf__District
ON
	calpads.Sinf
	(
		DistrictCode,
		SchoolCode,
		YearCode,
		StudentIdState,
		EffectiveStartDate
	);

CREATE NONCLUSTERED INDEX
	ix_Sinf__IsEscrow
ON
	calpads.Sinf
	(
		IsEscrow
	);

CREATE NONCLUSTERED INDEX
	ix_Sinf__Student
ON
	calpads.Sinf
	(
		StudentIdState,
		DistrictCode,
		SchoolCode,
		YearCode,
		GradeCode
	)
INCLUDE
	(
		NameFirst,
		NameLast,
		Gender,
		Birthdate
	);