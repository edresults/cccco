USE calpass;

GO

IF (object_id('calpads.Year') is not null)
	BEGIN
		DROP TABLE calpads.Year;
	END;

GO

CREATE TABLE
	calpads.Year
	(
		YearCode char(9) not null,
		YearCodeAbbr char(4) not null,
		YearTrailing smallint not null,
		YearLeading smallint not null,
		DateStart date not null,
		DateEnd date not null,
		IsoDateStart char(8) not null,
		IsoDateEnd char(8) not null,
		Rank tinyint
	);

GO

ALTER TABLE
	calpads.Year
ADD CONSTRAINT
	PK_Year
PRIMARY KEY CLUSTERED
	(
		YearCode
	);

GO

CREATE INDEX
	ix_Year__YearCodeAbbr
ON
	calpads.Year
	(
		YearCodeAbbr
	)
INCLUDE
	(
		YearCode,
		YearTrailing
	);

GO

CREATE INDEX
	ix_Year__DateStart__DateEnd
ON
	calpads.Year
	(
		DateStart,
		DateEnd
	)
INCLUDE
	(
		YearCode,
		YearTrailing
	);

GO

CREATE INDEX
	ix_Year__IsoDateStart__IsoDateEnd
ON
	calpads.Year
	(
		IsoDateStart,
		IsoDateEnd
	)
INCLUDE
	(
		YearCode,
		YearTrailing
	);

GO

INSERT INTO
	calpads.Year
	(
		YearCode,
		YearCodeAbbr,
		YearTrailing,
		YearLeading,
		DateStart,
		DateEnd,
		IsoDateStart,
		IsoDateEnd,
		Rank
	)
SELECT
	YearCode,
	YearCodeAbbr = substring(YearCode, 3,2) + substring(YearCode, 8, 2),
	YearTrailing = convert(smallint, substring(YearCode, 1, 4)),
	YearLeading = convert(smallint, substring(YearCode, 6, 4)),
	DateStart = convert(date, '07/01/' + substring(YearCode, 1, 4)),
	DateEnd = convert(date, '06/30/' + substring(YearCode, 6, 4)),
	IsoDateStart = substring(YearCode, 1, 4) + '0701',
	IsoDateEnd = substring(YearCode, 6, 4) + '0630',
	Rank = rank() over(order by YearCode)
FROM
	(
		VALUES
			('1993-1994'),
			('1994-1995'),
			('1995-1996'),
			('1996-1997'),
			('1997-1998'),
			('1998-1999'),
			('1999-2000'),
			('2000-2001'),
			('2001-2002'),
			('2002-2003'),
			('2003-2004'),
			('2004-2005'),
			('2005-2006'),
			('2006-2007'),
			('2007-2008'),
			('2008-2009'),
			('2009-2010'),
			('2010-2011'),
			('2011-2012'),
			('2012-2013'),
			('2013-2014'),
			('2014-2015'),
			('2015-2016'),
			('2016-2017'),
			('2017-2018'),
			('2018-2019'),
			('2019-2020'),
			('2020-2021'),
			('2021-2022'),
			('2022-2023'),
			('2023-2024'),
			('2024-2025'),
			('2025-2026'),
			('2026-2027'),
			('2027-2028'),
			('2028-2029'),
			('2029-2030'),
			('2030-2031'),
			('2031-2032'),
			('2032-2033'),
			('2033-2034'),
			('2034-2035'),
			('2035-2036'),
			('2036-2037'),
			('2037-2038'),
			('2038-2039'),
			('2039-2040'),
			('2040-2041')
	) t (YearCode)