USE calpass;

GO

IF (object_id('calpads.Senr') is not null)
	BEGIN
		DROP TABLE calpads.Senr;
	END;

GO

CREATE TABLE
	calpads.Senr
	(
		RecordTypeCode varchar(6),
		TransactionTypeCode char(1),
		LocalRecordId varchar(255),
		DistrictCode char(7),
		SchoolCode char(7),
		SchoolNpsCode char(7),
		YearCode char(9)
			CONSTRAINT
				df_Senr__YearCode
			DEFAULT
				(
					'####-####'
				),
		StudentIdState varchar(10),
		PrimaryLanguageCode char(2),
		EnrollStartDate char(8)
			CONSTRAINT
				df_Senr__EnrollStartDate
			DEFAULT
				(
					'########'
				),
		EnrollStatusCode char(2),
		GradeCode char(2),
		EnrollExitDate char(8),
		EnrollExitCode char(4),
		CompletionStatusCode char(3),
		ExpectedReciver char(7),
		IsUniversityReady char(1),
		TransferCode char(1),
		ResidenceDistrictCode char(7),
		IsDiploma char(1),
		IsBiliteracy char(1),
		IsEscrow bit not null
	);

GO

CREATE CLUSTERED INDEX
	ixc_Senr__District
ON
	calpads.Senr
	(
		DistrictCode,
		SchoolCode,
		YearCode,
		StudentIdState,
		EnrollStartDate
	);

CREATE NONCLUSTERED INDEX
	ix_Senr__IsEscrow
ON
	calpads.Senr
	(
		IsEscrow
	);

CREATE NONCLUSTERED INDEX
	ix_Senr__Student
ON
	calpads.Senr
	(
		StudentIdState,
		DistrictCode,
		SchoolCode,
		YearCode,
		GradeCode
	);

alter table calpads.senr add IsTransition                    char(1) null;
alter table calpads.senr add IsWorkforceReady                char(1) null;
alter table calpads.senr add IsFoodHandler                   char(1) null;
alter table calpads.senr add IsPreApprecticeshipCertificate  char(1) null;
alter table calpads.senr add IsPreApprecticeshipProgram      char(1) null;
alter table calpads.senr add IsGovtProgram                   char(1) null;