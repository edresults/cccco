IF (object_id('calpads.TranscriptMathematics') is not null)
	BEGIN
		DROP TABLE calpads.TranscriptMathematics;
	END;

GO

CREATE TABLE
	calpads.TranscriptMathematics
	(
		TranscriptMathematicsId tinyint       not null CONSTRAINT PK_TranscriptMathematics PRIMARY KEY CLUSTERED IDENTITY(0,1),
		Label                   varchar(20)   not null CONSTRAINT UQ_TranscriptMathematics_Label UNIQUE,
		Topics                  varchar(8000) not null,
		IsAlgI                  bit           not null,
		IsAlgII                 bit           not null,
		ContentRank             tinyint       not null
	);

GO

INSERT INTO
	calpads.TranscriptMathematics
	(
		ContentRank,
		IsAlgI,
		IsAlgII,
		Label,
		Topics
	)
VALUES
	(0,0,0,'Unknown',              'Unknown'),
	(2,0,0,'Pre-algebra or lower', 'Exponents and radicals (e.g. square roots), the coordinate system, sets, logic formulas, and solving linear, first-degree equations and inequalities. These classes often cover the use of equations to solve word problems is taught along with solving quations in one and two variables.'),
	(3,1,0,'Algebra 1',            'Addition, subtraction, multiplication and division of real numbers; inequalities and absolute value in equations; slope and x and y intercepts, graphing of linear equations; inequalities and quadratic equations; systems of two linear equations; polynomials; rational expressions and functions; the quadratic formula.'),
	(3,1,0,'Integrated Math 1',    'Functions; algebra; geometry; statistics; probability; discrete mathematics; measurement; number; logic; and language.'),
	(4,1,0,'Integrated Math 2',    'Quadratic expressions, equations, and functions; comparing their characteristics and behavior to those of linear and exponential relationships'),
	(4,1,0,'Geometry',             'Graphing lines, areas and volumes of plane figures and solids; congruence and similarity; the Pythagorean theorem; properties of angles, parallel and perpendicular lines, triangles, special right triangles, quadrilaterals, and circles; and basic trigonometric function.'),
	(5,1,1,'Algebra 2',            'Polynomials of higher order, logarithms and logarithmic functions, absolute value, systems of linear equations and inequalities, matrices, operations on polynomials, rational expressions, quadratic equations and functions, conic sections, inverse functions, sequences and series, the basic ideas of probability and statistics'),
	(5,1,1,'Integrated Math 3',    'Applying methods from probability and statistics to draw inferences and conclusions from data; expanding understanding of functions to include polynomial, rational, and radical functions; trigonometry of general triangles and trigonometric functions.'),
	(6,1,1,'Statistics',           'Independent events, conditional probability, discrete random variables; examinations of chance-based phenomena; standard distributions; mean, median, and mode; variance and standard deviation; probability theory; and data organization.'),
	(6,1,1,'Integrated Math 4',    'Advanced geometry, advanced algebra, and probability and statistics.'),
	(7,1,1,'Trigonometry',         'Radian measure; unit circle; trigonometric identities; simplifying trigonometric expressions; graphs of trigonometric functions and their inverse; polar coordinates; analytic geometry; and graphing circular functions.'),
	(7,1,1,'Pre-calculus',         'Coordinate geometry with analytical methods and proofs; equations and graphs of conic sections; rectangular and polar coordinates; parametric equations; vectors; the study of polynomial, logarithmic, exponential, and rational functions and their graphs; induction; limits and rate change; continuity; and problem analysis.'),
	(8,1,1,'Calculus or higher',   'The study of derivatives and differentiation; limits; instantaneous rates of change; areas under graphs of functions of first and second derivatives; integration; the definite and indefinite integral; and applications of calculus.'),
	(7,1,1,'Math Analysis',        'Polar coordinates, vectors, complex numbers, limits, mathematical induction, fundamental theorem of algebra, conic sections, rational functions, and functions and equations defined parametrically.');