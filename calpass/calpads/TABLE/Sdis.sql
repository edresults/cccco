USE calpass;

GO

IF (object_id('calpads.Sdis') is not null)
	BEGIN
		DROP TABLE calpads.Sdis;
	END;

GO

CREATE TABLE
	calpads.Sdis
	(
		RecordTypeCode varchar(6),
		TransactionTypeCode char(1),
		LocalRecordId varchar(255),
		DistrictCode char(7),
		SchoolCode char(7),
		YearCode char(9)
			CONSTRAINT
				df_Sdis__YearCode
			DEFAULT
				(
					'####-####'
				),
		StudentIdState varchar(10),
		IncidentId varchar(20)
			CONSTRAINT
				df_Sdis__IncidentId
			DEFAULT
				(
					'XXXXXXXXXXXXXXXXXXXX'
				),
		IncidentDate char(8),
		OffenseCode char(3),
		SevereOffenseCode char(3),
		WeaponCode char(2),
		ActionTakenCode char(3),
		ActionAuthorityCode char(2),
		ActionDurationDays decimal(5,2),
		InstructionalSupport char(1),
		ModificationCateogryCode char(3),
		RemovalCode char(1),
		IsEscrow bit not null
	);

GO

CREATE CLUSTERED INDEX
	ixc_Sdis__DistrictCode
ON
	calpads.Sdis
	(
		DistrictCode,
		SchoolCode,
		YearCode,
		StudentIdState,
		IncidentId,
		OffenseCode
	);

CREATE NONCLUSTERED INDEX
	ix_Sdis__IsEscrow
ON
	calpads.Sdis
	(
		IsEscrow
	);