CREATE TABLE
    calpads.Content
    (
        Code                varchar(15) not null,
        DepartmentCode      tinyint     not null,
        CONSTRAINT PK_Content PRIMARY KEY CLUSTERED ( Code ),
        CONSTRAINT FK_Content_Department FOREIGN KEY ( DepartmentCode ) REFERENCES calpads.Department ( Code)
    );

GO

INSERT
    calpads.Content
    (
        DepartmentCode,
        Code
    )
VALUES
    (14,'Esl'         ),
    (14,'Remedial'    ),
    (14,'English'     ),
    (14,'Expository'  ),
    (14,'LiteratureAP'),
    (14,'LanguageAP'  ),
    (18,'Arithmetic'  ),
    (18,'PreAlgebra'  ),
    (18,'AlgebraI'    ),
    (18,'Geometry'    ),
    (18,'AlgebraII'   ),
    (18,'Statistics'  ),
    (18,'StatisticsAP'),
    (18,'PreCalculus' ),
    (18,'Trigonometry'),
    (18,'CalculusI'   ),
    (18,'CalculusII'  ),
    (18,'CalculusIAP' ),
    (18,'CalculusIIAP');