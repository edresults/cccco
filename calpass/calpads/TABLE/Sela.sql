USE calpass;

GO

IF (object_id('calpads.Sela') is not null)
	BEGIN
		DROP TABLE calpads.Sela;
	END;

GO

CREATE TABLE
	calpads.Sela
	(
		RecordTypeCode varchar(6), -- valid: 6,7
		TransactionTypeCode char(1), -- valid: 6,7
		LocalRecordId varchar(255), -- valid: 6,7
		DistrictCode char(7), -- valid: 6,7
		SchoolCode char(7), -- valid: 6,7
		YearCode char(9)
			CONSTRAINT
				df_Sela__YearCode
			DEFAULT
				(
					'####-####'
				), -- valid: 6,7
		StudentIdState varchar(10), -- valid: 6,7
		ElaStatusCode varchar(4), -- valid: 6,7
		ElaStatusStartDate char(8)
			CONSTRAINT
				df_Sela__ElaStatusStartDate
			DEFAULT
				(
					'########'
				), -- valid: 6,7
		PrimaryLanguageCode char(2), -- valid: 6,7
		IsEscrow bit not null
	);

GO

ALTER TABLE
	calpads.Sela
ADD CONSTRAINT
	pk_Sela
PRIMARY KEY CLUSTERED
	(
		StudentStateId,
		SchoolCode,
		YearCode,
		ElaStatusStartDate
	);

CREATE NONCLUSTERED INDEX
	ix_Sela__IsEscrow
ON
	calpads.Sela
	(
		IsEscrow
	);