USE calpass;

GO

IF (object_id('calpads.Sprg') is not null)
	BEGIN
		DROP TABLE calpads.Sprg;
	END;

GO

CREATE TABLE
	calpads.Sprg
	(
		RecordTypeCode varchar(6), -- valid: 3,4,5,6,7
		TransactionTypeCode char(1), -- valid: 3,4,5,6,7
		LocalRecordId varchar(255), -- valid: 3,4,5,6,7
		DistrictCode char(7), -- valid: 3,4,5,6,7
		SchoolCode char(7), -- valid: 3,4,5,6,7
		YearCode char(9)
			CONSTRAINT
				df_Sprg__YearCode
			DEFAULT
				(
					'####-####'
				), -- valid: 3,4,5,6,7
		StudentIdState varchar(10), -- valid: 3,4,5,6,7
		ProgramCode char(3)
			CONSTRAINT
				df_Sprg__ProgramCode
			DEFAULT
				(
					'XXX'
				), -- valid: 3,4,5,6,7
		MembershipCode char(1), -- valid: 3,4,5,6,7
		MembershipStartDate char(8)
			CONSTRAINT
				df_Sprg__MembershipStartDate
			DEFAULT
				(
					'########'
				), -- valid: 3,4,5,6,7
		MembershipEndDate char(8), -- valid: 3,4,5,6,7
		EducationServiceYearCode char(9)
			CONSTRAINT
				df_Sprg__EducationServiceYearCode
			DEFAULT
				(
					'####-####'
				), -- valid: 3,4,5,6,7
		EducationServiceCode char(2)
			CONSTRAINT
				df_Sprg__EducationServiceCode
			DEFAULT
				(
					'XX'
				), -- valid: 3,4,5,6,7
		CaliforniaPartnershipAcademyId varchar(5), -- valid: 3,4,5,6,7
		MigrantStudentId varchar(12), -- valid: 3,4,5,6,7
		PrimaryDisabilityCode char(3), -- valid: 3,4,5,6,7
		AccountabilityDistrictCode char(7), -- valid: 3,4,5,6,7
		HomlessDwellingCode char(3), -- valid: 6,7
		IsUnaccompaniedYouth char(1), -- valid: 6,7
		IsRunawayYouth char(1), -- valid: 6,7
		StudentFosterId	varchar(10), -- valid: 6, filler
		IsEscrow bit not null
	);

/*
	20160308: cannot use education_service_year or education_service_code as part of clustered index values are almost always null
	20161210: inclusion of SchoolCode slows delete process by hundreds of thousands of orders of magnitude
*/

GO

CREATE CLUSTERED INDEX
	ixc_Sprg
ON
	calpads.Sprg
	(
		DistrictCode,
		SchoolCode,
		YearCode,
		StudentIdState,
		program_code,
		education_service_code,
		program_membership_start_date,
		education_service_year
	);

CREATE NONCLUSTERED INDEX
	ix_Sprg__IsEscrow
ON
	calpads.Sprg
	(
		IsEscrow
	);