USE calpass;

GO

IF (object_id('calpads.p_SenrRealignment') is not null)
	BEGIN
		DROP PROCEDURE calpads.p_SenrRealignment;
	END;

GO

CREATE PROCEDURE
	calpads.p_SenrRealignment
	(
		@file_year_code char(9),
		@table_name nvarchar(255)
	)
AS

DECLARE
	@msg nvarchar(2048),
	@sql nvarchar(max) = N'',
	@year_trailing_threshold smallint = 2014,
	@year_trailing_file smallint;

BEGIN
	-- validate table
	IF (0 = (SELECT count(*) FROM calpass.sys.tables WHERE name = @table_name))
		BEGIN
			SET @msg = 'Invalid object name ''' + @table_name + '''.';
			THROW 70099, @msg, 1;
		END;
	-- set var
	SET @year_trailing_file = convert(smallint, left(@file_year_code, 4));
	-- branch based on year compared to threshold
	IF (@year_trailing_file < 2014)
		-- year must be updated and grades must be aligned
		BEGIN
			-- realign grade_level_code for students without good data
			-- assume valid grade_level_code for invalid year_code i.e., increase grade_level_code
			SET @sql = '
				UPDATE
					b
				WITH
					(
						TabLockX
					)
				SET
					b.grade_level_code = f.code
				FROM
					calpass.calpads.[' + @table_name + '] b -- [b]ad
					inner join
					calpass.calpads.Year yb
						on yb.YearCode = b.year_code
					inner join
					calpass.calpads.senr_grade_level gb
						on gb.code = b.grade_level_code
					inner join
					calpass.calpads.senr_grade_level f -- [f]ix
						on f.rank = gb.rank + (@year_trailing_file - yb.YearTrailing)
				WHERE
					-- only records without a good data
					not exists (
						SELECT
							1
						FROM
							calpass.calpads.senr g -- [g]ood
							inner join
							calpass.calpads.Year yg
								on yg.YearCode = g.year_code
						WHERE
							g.ssid = b.ssid
							and yg.YearTrailing >= @year_trailing_threshold
					)
					-- include years prior to good anchor
					and yb.YearTrailing < @year_trailing_threshold
					-- include fix grade rank lower than or equal to 12th grade
					and f.rank <= 15
					-- include unexpected values
					and f.code != b.grade_level_code;';
			-- exe grade fix
			EXECUTE sp_executesql
				@sql,
				N'@file_year_code char(9),
					@year_trailing_file smallint,
					@year_trailing_threshold smallint',
				@file_year_code = @file_year_code,
				@year_trailing_file = @year_trailing_file,
				@year_trailing_threshold = @year_trailing_threshold;
			-- update invalid year_code
			SET @sql = '
				UPDATE
					calpass.calpads.[' + @table_name + ']
				SET
					year_code = @file_year_code';
			-- exe update
			EXECUTE sp_executesql
				@sql,
				N'@file_year_code char(9)',
				@file_year_code = @file_year_code;
			-- realign grade_level_code for now-valid year_code using good data
			SET @sql = '
				UPDATE
					b
				WITH
					(
						TabLockX
					)
				SET
					b.grade_level_code = f.code
				FROM
					calpass.calpads.[' + @table_name + '] b -- [b]ad
					inner join
					calpass.calpads.senr_grade_level gb
						on gb.code = b.grade_level_code
					inner join
					calpass.calpads.senr g -- [g]ood
						on b.ssid = g.ssid
					inner join
					calpass.calpads.Year yg
						on yg.YearCode = g.year_code
					inner join
					calpass.calpads.senr_grade_level gg
						on gg.code = g.grade_level_code
					inner join
					calpass.calpads.senr_grade_level f -- [f]ix
						on f.rank = gg.rank - (yg.YearTrailing - @year_trailing_file)
				WHERE
					-- nearest past good year threshold
					g.year_code = (
						SELECT
							min(a.year_code)
						FROM
							calpass.calpads.senr a
							inner join
							calpass.calpads.Year ya
								on ya.YearCode = a.year_code
						WHERE
							a.ssid = b.ssid
							and ya.YearTrailing >= @year_trailing_threshold
					)
					-- exclude anchor grade ranks lower than kindergarten
					and f.rank >= 4
					-- exclude fix grade rank greater than anchor rank
					and f.rank <= gg.rank
					-- include unexpected
					and f.code != g.grade_level_code;';
			-- exe grade fix
			EXECUTE sp_executesql
				@sql,
				N'@file_year_code char(9),
					@year_trailing_file smallint,
					@year_trailing_threshold smallint',
				@file_year_code = @file_year_code,
				@year_trailing_file = @year_trailing_file,
				@year_trailing_threshold = @year_trailing_threshold;
		END;
 	ELSE
		-- look to prior years to update grades
		BEGIN
			-- exe grade fix
			SET @sql = N'
				UPDATE
					b
				WITH
					(
						TabLockX
					)
				SET
					b.grade_level_code = f.code
				FROM
					calpass.calpads.[' + @table_name + '] g -- [g]ood
					inner join
					calpass.calpads.Year yg
						on yg.YearCode = g.year_code
					inner join
					calpass.calpads.senr_grade_level gg
						on gg.code = g.grade_level_code
					inner join
					calpass.calpads.senr b -- [b]ad
						on b.ssid = g.ssid
					inner join
					calpass.calpads.Year yb
						on yb.YearCode = b.year_code
					inner join
					calpass.calpads.senr_grade_level gb
						on gb.code = b.grade_level_code
					inner join
					calpass.calpads.senr_grade_level f -- [f]ix
						on f.rank = gg.rank - (yg.YearTrailing - yb.YearTrailing)
				WHERE
					-- exclude realigned students
					not exists (
						SELECT
							1
						FROM
							calpass.calpads.senr a -- [a]nchor
							inner join
							calpass.calpads.Year ya
								on ya.YearCode = a.year_code
						WHERE
							a.ssid = g.ssid
							and ya.YearTrailing >= @year_trailing_threshold
							and ya.YearTrailing < @year_trailing_file
					)
					-- exclude expected results
					and gb.code != f.code
					-- exclude anchor grade ranks lower than kindergarten
					and f.rank >= 4;';
			-- exe grade fix
			EXECUTE sp_executesql
				@sql,
				N'@file_year_code char(9),
					@year_trailing_file smallint,
					@year_trailing_threshold smallint',
				@file_year_code = @file_year_code,
				@year_trailing_file = @year_trailing_file,
				@year_trailing_threshold = @year_trailing_threshold;
		END;
END;