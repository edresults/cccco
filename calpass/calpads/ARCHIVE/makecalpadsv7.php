<?php
ini_set('max_execution_time', 60*60*24*2);
$erp = mysql_connect("localhost", "calpassuser", "calpasspass")
	or die ("no connect");
mysql_select_db("calpass")
	or die ("no db");

// Command Line Options
$path = $argv[1];
if ($path <> '') chdir($path);

if (isset($argv[2])) {
    $year = $argv[2];
}
else
{
    $year='1213';
}

if (isset($argv[3]) && $argv[3] == "new") {
    $newlayout = 1;
}
else
{
    $newlayout = 0;
}

$tableparam = isset($argv[4]) ? $argv[4] : '';
if ($tableparam == 'notemp') {    // Allowed me to retain the tables when things didn't work
    $tabletype = '';
}
else
{
    $tabletype = 'TEMPORARY';
}    

$delim = isset($argv[5]) ? $argv[5] : '^';
$debug = 0;

// Find CALPADS Files
$files = scandir(".");

//print_r($files);
$senrindex = $sinfindex = $crscindex = $scscindex = $selaindex = $sprgindex = 
        $scteindex = -99;
$count = 0;
foreach($files as $file) {
    if (stripos($file, 'SSID') !== FALSE) 
        $senrindex = $count;
    if (stripos($file, 'SENR') !== FALSE) 
        $senrindex = $count;
    if (stripos($file, 'SINF') !== FALSE) 
        $sinfindex = $count;
    if (stripos($file, 'CRSC') !== FALSE) 
        $crscindex = $count;
    if (stripos($file, 'SCSC') !== FALSE) 
        $scscindex = $count;
    if (stripos($file, 'SELA') !== FALSE) 
        $selaindex = $count;
    if (stripos($file, 'SPRG') !== FALSE) 
        $sprgindex = $count;
    if (stripos($file, 'SCTE') !== FALSE) 
        $scteindex = $count;
     $count++;
}

// Create Temporary Tables to load raw CALPADS records
$createq = "CREATE " . $tabletype . " TABLE IF NOT EXISTS senr (
districtcode char(7),
schoolcode char(7),
schoolyear char(10),
studentid char(15),
ssid char(15),
fname char(30),
lname char(40),
gender char(1),
birthdate char(8),
gradelevel char(2),
primarylanguage char(2),
exitcode char(4),
exitdate char(8),
index (studentid),
index (ssid),
index (schoolyear))";
$creater = mysql_query($createq);

$createq = "CREATE " . $tabletype . " TABLE IF NOT EXISTS sinf (
districtcode char(7),
schoolcode char(7),
schoolyear char(10),
studentid char(15),
ssid char(15),
fname char(30),
lname char(40),
gender char(1),
birthdate char(8),
hispanic char(1),
ethnicity char(15),
gradelevel char(2),
primarylanguage char(2),
enddate char(8),
index (studentid),
index (ssid),
index (schoolyear))";
$creater = mysql_query($createq);

$createq = "CREATE " . $tabletype . " TABLE IF NOT EXISTS crsc (
districtcode char(7),
schoolcode char(7),
schoolyear char(10),
courseid char(4),
localcourseid char(10),
coursesection char(10),
coursetitle char(40),
agstatus char(2),
funding char(4),
courselevel char(2),
coursetype char(2),
courseterm char(4),
index (localcourseid),
index (schoolyear),
index (schoolcode))";
$creater = mysql_query($createq);

$createq = "CREATE " . $tabletype . " TABLE IF NOT EXISTS scsc (
districtcode char(7),
schoolcode char(7),
schoolyear char(10),
studentid char(15),
ssid char(15),
localcourseid char(10),
coursesection char(10),
courseterm char(4),
agstatus char(2),
mark char(3),
creditearned decimal(5,2),
creditattempted decimal(5,2),
markingperiod char(2),
index (studentid),
index (ssid),
index (localcourseid),
index (schoolyear),
index (schoolcode))";
$creater = mysql_query($createq);

$createq = "CREATE " . $tabletype . " TABLE IF NOT EXISTS sela (
districtcode char(7),
schoolcode char(7),
schoolyear char(10),
studentid char(15),
ssid char(15),
fname char(30),
lname char(40),
gender char(1),
birthdate char(8),
elascode char(4),
elasdate char(8),
primarylanguage char(2),
index (studentid),
index (ssid),
index (schoolyear),
index (schoolcode))";
$creater = mysql_query($createq);

// SPRG
if ($sprgindex <> 99) {
    $createq = "CREATE " . $tabletype . " TABLE IF NOT EXISTS sprg (
    cds char(14),
    schoolyear char(10),
    studentid char(10),
    ssid char(25),
    fname char(30),
    lname char(40),
    gender char(1),
    birthdate char(8),
    edprogramcode char(3),
    edprogrammembershipcode char(1),
    edpmstart char(8),
    edpmend char(8),
    edserviceyear char(9),
    edservicecode char(2),
    capartacademyid char(5),
    migrantstudentid char(11),
    primarydisabilitycode char(3),
    speced char(7),
    homelessdwelling char(3),
    unaccyouth char(1),
    runaway char(1))";
    $creater = mysql_query($createq);
}

// SCTE
if ($scteindex <> 99) {
    $createq = "CREATE " . $tabletype . " TABLE IF NOT EXISTS scte (
    cds char(14),
    schoolyear char(10),
    studentid char(10),
    ssid char(25),
    fname char(30),
    lname char(40),
    gender char(1),
    birthdate char(8),
    ctepathwaycode char(3),
    ctepathwaycompacyear char(9))";
    $creater = mysql_query($createq);
}

//$newlayout=1;

// Load SENR
printf("Loading Records...");
printf("\n SENR... ");
$distcode = '';
$good = 0;

$firsttime = 1;
printf("Processing: " . $files[$senrindex] . "\n");
if (($handle = fopen($files[$senrindex], "r")) !== FALSE) {               // Open the file
    while (($data = fgetcsv($handle, 3000, $delim)) !== FALSE) {      // Grab a row
         // Determine if new or old files
        if ($firsttime) {
            $firsttime = 0;
            $dcode = str_pad($data[3], 7, "0", STR_PAD_LEFT); 
            if (strlen($data[21]) > 2) { 
                $newlayout = 1;
                print("Using New File Layout.\n");
            }
            if (substr($data[0],-2) == "-R") {
                $filetype = 'Extract';
            }
            else
            {
                $filetype = 'Original';
            }
            print("Processing $filetype files.\n");
        }
        $fname = str_replace("'", "", $data[9]);                  // Clean up data
        $fname = str_replace(".", "", $fname);
        $fname = str_replace(" ", "", $fname);
        $fname = strtoupper(substr($fname, 0, 3));
        $lname = str_replace("'", "", $data[11]);
        $lname = str_replace(".", "", $lname);
        $lname = str_replace(" ", "", $lname);
        $lname = strtoupper(substr($lname, 0, 3));
        $distcode = substr(str_pad($data[3], 7, "0", STR_PAD_LEFT),-5);
      // Load the SENR records
        if ($newlayout) {
            $insq = "INSERT INTO senr (districtcode, schoolcode, schoolyear, studentid, ssid, "  .
            "fname, lname, gender, birthdate, gradelevel, exitcode, exitdate) " . 
            "VALUES ('" . str_pad($data[3], 7, "0", STR_PAD_LEFT) . "', '" . 
            str_pad($data[4], 7, "0", STR_PAD_LEFT) . "', '" . $data[6] . "', '" . $data[8] . "', '" . 
            $data[7] . "', '" . $fname . "', '" . $lname . "', '" . $data[17] . "', '" . 
            $data[16] . "', '" . 
            str_pad($data[23], 2, "0", STR_PAD_LEFT) . "', '" . 
            $data[26] . "', '" . $data[24] . "')";   
        }
        else
        {
             $insq = "INSERT INTO senr (districtcode, schoolcode, schoolyear, studentid, ssid, "  .
            "fname, lname, gender, birthdate, gradelevel, primarylanguage, exitcode, exitdate) " . 
            "VALUES ('" . str_pad($data[3], 7, "0", STR_PAD_LEFT) . "', '" . 
            str_pad($data[4], 7, "0", STR_PAD_LEFT) . "', '" . $data[6] . "', '" . $data[8] . "', '" . 
            $data[7] . "', '" . $fname . "', '" . $lname . "', '" . $data[17] . "', '" . 
            $data[16] . "', '" . str_pad($data[24], 2, "0", STR_PAD_LEFT) . "', '" . 
            str_pad($data[21], 2, "0", STR_PAD_LEFT) . "', '" . 
            $data[27] . "', '" . $data[25] . "')";
        }
        $insr = mysql_query($insq);                                 // Write the record
        $good++;
   }
}
else
{
   logupdate($dcode, $year, "Missing SENR. $path Aborted.");
   die("Couldn't open SENR file.\n");
}
printf("Loaded: %d   ", $good);

// SINF
printf("\n SINF... ");
printf("Processing %s\n", $files[$sinfindex]);
$good = 0;
//$newlayout = 0; // Patch for mixed file types
if (($handle = fopen($files[$sinfindex], "r")) !== FALSE) {            // Open the file
	while (($data = fgetcsv($handle, 3000, $delim)) !== FALSE) {      // Grab a row
            $fname = str_replace("'", "", $data[10]);
            $fname = str_replace(".", "", $fname);
            $fname = str_replace(" ", "", $fname);
            $fname = strtoupper(substr($fname, 0, 3));
            $lname = str_replace("'", "", $data[12]);
            $lname = str_replace(".", "", $lname);
            $lname = str_replace(" ", "", $lname);
            $lname = strtoupper(substr($lname, 0, 3));

            if ($newlayout) {
                $eth = trim($data[24] . $data[25] . $data[26] .             // Clean up data
                $data[27] . $data[28]);

                $insq = "INSERT INTO sinf (districtcode, schoolcode, schoolyear, studentid, ssid, 
                fname, lname, gender, birthdate, 
                hispanic, ethnicity, enddate) 
                VALUES ('" . str_pad($data[5], 7, "0", STR_PAD_LEFT) . "', '" . 
                str_pad($data[6], 7, "0", STR_PAD_LEFT) . "', '" . $data[7] . "', '" . 
                $data[9] . "', '" . $data[8] . "', '" . 
                $fname . "', '" . $lname . "', '" . $data[18] . "', '" . $data[17] . "', '" .
                $data[22] . "', 
                '" . $eth . "', '" . 
                $data[4] . "')";
            }
            else
            {
                $eth = trim($data[25] . $data[26] . $data[27] .             // Clean up data
                $data[28] . $data[29]);

                $insq = "INSERT INTO sinf (districtcode, schoolcode, schoolyear, studentid, ssid, 
                fname, lname, gender, birthdate, 
                hispanic, ethnicity, gradelevel, primarylanguage, enddate) 
                VALUES ('" . str_pad($data[5], 7, "0", STR_PAD_LEFT) . "', '" . 
                str_pad($data[6], 7, "0", STR_PAD_LEFT) . "', '" . $data[7] . "', '" . 
                $data[9] . "', '" . $data[8] . "', '" . 
                $fname . "', '" . $lname . "', '" . $data[18] . "', '" . $data[17] . "', '" .
                $data[23] . "', 
                '" . $eth . "', '" . str_pad($data[31], 2, "0", STR_PAD_LEFT) . "', '" . 
                str_pad($data[22], 2, "0", STR_PAD_LEFT) . "', '" . $data[3] . "')";
            }
            if (substr($eth,0,1) == 'N' OR substr($eth,0,1) == 'Y') {
                // Invalid file
                print("Ethnicity invalid. Probably wrong file layout for SINF.\n");
                logupdate($dcode, $year, "Ethnicity $eth invalid. Probably wrong file layout for SINF. Aborted.");
                die();
            }
            $insr = mysql_query($insq);                                 // Write the record
            $good++;
	}
} 
else
{
    logupdate($dcode, $year, "Missing SINF $path. Aborted.");
    die("Couldn't open SINF file.\n");
}
printf("Loaded: %d   ", $good);

// CRSC
printf("\n CSRC... ");
printf("Processing: %s\n", $files[$crscindex]);
$good = 0;
if (($handle = fopen($files[$crscindex], "r")) !== FALSE) {               // Open the file
    while (($data = fgetcsv($handle, 3000, $delim)) !== FALSE) {      // Get a record
       $insq = "INSERT INTO crsc (districtcode, schoolcode, schoolyear, courseid, " . 
       "localcourseid, coursesection, coursetitle, agstatus, funding, courselevel, " . 
       "coursetype, courseterm) VALUES ('" . str_pad($data[3], 7, "0", STR_PAD_LEFT) . "', '" . 
       str_pad($data[4], 7, "0", STR_PAD_LEFT) . "', '" . $data[5]. "', '" . 
       $data[6] . "', '" . $data[7] . "', '" . $data[13] . "', '" . str_replace(',', '-', $data[8]) . "', '" .
       $data[12] . "', '" . $data[25] . "', '" . $data[18] . "', '" . $data[11] . "', '" . 
       $data[14] . "')";
       $insr = mysql_query($insq);                                // Write the record
       $good++;
    }
} 
else
{
   logupdate($dcode, $year, "Missing CRSC $path.");
   print("Couldn't open CRSC file.\n");
   //die("Couldn't open CRSC file.\n");
}
printf("Loaded: %d   ", $good);

// SCSC
printf("\n SCSC... ");
printf("Processing: %s\n", $files[$scscindex]);
$good = 0;
if (($handle = fopen($files[$scscindex], "r")) !== FALSE) {               // Open the file
    while (($data = fgetcsv($handle, 3000, $delim)) !== FALSE) {      // Grab a record
       $insq = "INSERT INTO scsc (districtcode, schoolcode, schoolyear, studentid, ssid, " . 
        "localcourseid, coursesection, courseterm, agstatus, mark, creditearned, creditattempted, 
        markingperiod) " .  
        "VALUES ('" . str_pad($data[3], 7, "0", STR_PAD_LEFT) . "', '" .  
        str_pad($data[4], 7, "0", STR_PAD_LEFT) . "', '" . $data[5] . "', '" . 
        $data[7] . "', '" . $data[6] . "', '" . $data[12] . "', '" . $data[13] . "', '" . 
        $data[14] . "', '" . $data[18] . "', '" . $data[17] . "', '" . $data[16] . "', '" . 
        $data[15] . "', '" . $data[19] . "')";
        $insr = mysql_query($insq);                                 // Write the record
        $good++;
    }
} 
else
{
   logupdate($dcode, $year, "Missing SCSC $path.");
   //die("Couldn't open SCSC file.\n");
   printf("Couldn't open SCSC file.\n");
}
printf("Loaded: %d   ", $good);

// SELA
if ($selaindex <> -99) {
    printf("\n SELA... ");
    printf("Processing: %s\n", $files[$selaindex]);
    $good = 0;

    if (($handle = fopen($files[$selaindex], "r")) !== FALSE) {               // Open the file
        while (($data = fgetcsv($handle, 3000, $delim)) !== FALSE) {      // Grab a row
            $fname = str_replace("'", "", $data[7]);
            $fname = str_replace(".", "", $fname);
            $fname = str_replace(" ", "", $fname);
            $fname = strtoupper(substr($fname, 0, 3));
            $lname = str_replace("'", "", $data[8]);
            $lname = str_replace(".", "", $lname);
            $lname = str_replace(" ", "", $lname);
            $lname = strtoupper(substr($lname, 0, 3));

            $insq = "INSERT INTO sela (districtcode, schoolcode, schoolyear, studentid, ssid, 
            fname, lname, gender, birthdate, 
            elascode, elasdate, primarylanguage) 
            VALUES ('" . str_pad($data[3], 7, "0", STR_PAD_LEFT) . "', '" . 
            str_pad($data[4], 7, "0", STR_PAD_LEFT) . "', '" . $data[5] . "', '" . 
            $data[11] . "', '" . $data[6] . "', '" . 
            $fname . "', '" . $lname . "', '" . $data[10] . "', '" . $data[9] . "', '" .
            $data[12] . "', '" . $data[13] . "', '" . $data[14] . "')";
            $insr = mysql_query($insq);                                 // Write the record
            $good++;
        }
    } 
    else
    {
            logupdate($dcode, $year, "Couldn't open SELA. Aborted.");
            die("Couldn't open SELA file\n");
    }
    printf("Loaded: %d   ", $good);
}
else
{
    if ($newlayout) {
        logupdate($dcode, $year, 'Missing SELA. Ignored.');
    }
}
// SPRG
if ($sprgindex <> -99) {
    printf("\n SPRG... ");
    printf("Processing: %s\n", $files[$sprgindex]);
    $good = 0;

    if (($handle = fopen($files[$sprgindex], "r")) !== FALSE) {        // Open the file
        while (($data = fgetcsv($handle, 3000, $delim)) !== FALSE) {      // Grab a row
            $fname = str_replace("'", "", $data[8]);
            $fname = str_replace(".", "", $fname);
            $fname = str_replace(" ", "", $fname);
            $fname = strtoupper(substr($fname, 0, 3));
            $lname = str_replace("'", "", $data[9]);
            $lname = str_replace(".", "", $lname);
            $lname = str_replace(" ", "", $lname);
            $lname = strtoupper(substr($lname, 0, 3));
            
            // Adjust for missing academic year
            if ($data[5] == '') {
                $acyear = '20' . substr($year,0,2) . '-' . '20' . substr($year,2,2);
            }
            else
            {
                $acyear = $data[5];
                
            }
            $data22 = isset($data[22]) ? $data[22] : ''; 
            $data23 = isset($data[23]) ? $data[23] : ''; 
            $data24 = isset($data[24]) ? $data[24] : ''; 
            $insq = "INSERT INTO sprg (cds, schoolyear, studentid, ssid, 
            fname, lname, gender, birthdate, edprogramcode,
            edprogrammembershipcode, edpmstart, edpmend, edserviceyear, edservicecode, 
            capartacademyid, migrantstudentid, primarydisabilitycode, speced,
            homelessdwelling, unaccyouth, runaway) 
            VALUES ('" . str_pad($data[3], 7, "0", STR_PAD_LEFT) .  
            str_pad($data[4], 7, "0", STR_PAD_LEFT) . "', '" . $acyear . "', '" . 
            $data[7] . "', '" . $data[6] . "', '" . 
            $fname . "', '" . $lname . "', '" . $data[11] . "', '" . $data[10] . "', '" .
            $data[12] . "', '" . $data[13] . "', '" . $data[14] . "', '" . $data[15] . "', '" . 
            $data[16] . "', '" . $data[17] . "', '" . $data[18] . "', '" . $data[19] . "', '" . 
            $data[20] . "', '" . $data[21] . "', '" . $data22  . "', '" . 
            $data23 . "', '" . 
            $data24 . "')";
            $insr = mysql_query($insq);                                 // Write the record

            $good++;
        }
    } 
    else
    {
       logupdate($dcode, $year, "Couldn't open SPRG. Aborted.");
       die("Couldn't open SPRG file.\n");
    }
    printf("Loaded: %d   ", $good);
}
else
{
    logupdate($dcode, $year, 'Missing SPRG. Ignored.');
}

// SCTE
if ($scteindex <> -99) {
    printf("\n SCTE... ");
    printf("Processing: %s\n", $files[$scteindex]);
    $good = 0;

    if (($handle = fopen($files[$scteindex], "r")) !== FALSE) {               // Open the file
        while (($data = fgetcsv($handle, 3000, $delim)) !== FALSE) {      // Grab a row
            $fname = str_replace("'", "", $data[8]);
            $fname = str_replace(".", "", $fname);
            $fname = str_replace(" ", "", $fname);
            $fname = strtoupper(substr($fname, 0, 3));
            $lname = str_replace("'", "", $data[9]);
            $lname = str_replace(".", "", $lname);
            $lname = str_replace(" ", "", $lname);
            $lname = strtoupper(substr($lname, 0, 3));

            $insq = "INSERT INTO scte (cds, schoolyear, studentid, ssid, 
            fname, lname, gender, birthdate, 
            ctepathwaycode, ctepathwaycompacyear) 
            VALUES ('" . str_pad($data[3], 7, "0", STR_PAD_LEFT) . 
            str_pad($data[4], 7, "0", STR_PAD_LEFT) . "', '" . $data[5] . "', '" . 
            $data[7] . "', '" . $data[6] . "', '" . 
            $fname . "', '" . $lname . "', '" . $data[11] . "', '" . $data[10] . "', '" .
            $data[12] . "', '" . $data[13] . "')";

            $insr = mysql_query($insq);                                 // Write the record
            $good++;
        }
    } 
    else
    {
        logupdate($dcode, $year, "Couldn't open SCTE. Aborted.");
        die("Couldn't open SCTE file.\n");
    }
    printf("Loaded: %d   ", $good);
    printf("\n done.\n");
}
else
{
    logupdate($dcode, $year, 'Missing SCTE file. Ignored.');

}

// Open the Output files
printf("\nOpening Output Files... ");
$stufile = fopen('S' . $distcode . "K.TXT", "w");
if (!$stufile) die ("Unable to open Student file S" . $distcode . "K.TXT");
$awardfile = fopen('A' . $distcode . "K.TXT", "w");
if (!$awardfile) die ("Unable to open Course file A" . $distcode . "K.TXT");
$coursefile = fopen('C' . $distcode . "K.TXT", "w");
if (!$coursefile) die ("Unable to open Course file C" . $distcode . "K.TXT");
if ($sprgindex <> -99) {
    $programfile = fopen('P' . $distcode . "K.TXT", "w");
    if (!$programfile) die ("Unable to open Course file C" . $distcode . "K.TXT");
}
if ($scteindex <> -99) {
    $ctefile = fopen('V' . $distcode . "K.TXT", "w");
    if (!$ctefile) die ("Unable to open Course file C" . $distcode . "K.TXT");
}
printf(" done.\n");
   
// Student File 
$badstu = $badcourse = $badaward = $badprog = $badcte = 0;
$senrq = "SELECT DISTINCT studentid, ssid FROM senr";             // Get each student from SENR
$senrr = mysql_query($senrq);
$seen = array();
while ($senr = mysql_fetch_assoc($senrr)) {
/*
 *  $stuq = "SELECT DISTINCT a.studentid, a.ssid, a.lname, a.fname, a.primarylanguage, 
    c.primarylanguage as primarylanguagesela,
    a.districtcode, a.schoolcode, a.gender, a.birthdate, a.exitcode, a.exitdate, 
    a.gradelevel as gradelevelsenr, a.schoolyear as schoolyearsenr, 
    b.hispanic, b.ethnicity, b.schoolyear as schoolyearsinf, b.gradelevel as gradelevelsinf
    FROM senr as a 
    LEFT JOIN sinf as b 
    ON a.ssid = b.ssid
    LEFT JOIN sela as c
    ON a.ssid = c.ssid
    WHERE a.ssid = '" . $senr['ssid'] . "' AND 
    (a.schoolyear = '20" . substr($year,0,2) . "-20" . substr($year,-2)  . "' OR 
    b.schoolyear = '20" . substr($year,0,2) . "-20" . substr($year,-2)  . "')
    ORDER BY a.schoolyear DESC, a.exitdate DESC LIMIT 1";

 */
    $stuq = "SELECT DISTINCT a.studentid, a.ssid, a.lname, a.fname, a.primarylanguage, 
    c.primarylanguage as primarylanguagesela,
    a.districtcode, a.schoolcode, a.gender, a.birthdate, a.exitcode, a.exitdate, 
    a.gradelevel as gradelevelsenr, a.schoolyear as schoolyearsenr, 
    b.hispanic, b.ethnicity, b.schoolyear as schoolyearsinf, b.gradelevel as gradelevelsinf
    FROM senr as a 
    LEFT JOIN sinf as b 
    ON a.studentid = b.studentid
    LEFT JOIN sela as c
    ON a.studentid = c.studentid
    WHERE a.studentid = '" . $senr['studentid'] . "' AND 
    (a.schoolyear = '20" . substr($year,0,2) . "-20" . substr($year,-2)  . "' OR 
    b.schoolyear = '20" . substr($year,0,2) . "-20" . substr($year,-2)  . "')
    ORDER BY a.schoolyear DESC, a.exitdate DESC LIMIT 1";
    if ($debug) print("stuq=$stuq\n");
    $stur = mysql_query($stuq);
    $stu = mysql_fetch_assoc($stur);
    if (!in_array($stu['studentid'], $seen)) $seen[] = $stu['studentid'];   // building array of student ids

    // I've found many examples where the SENR and SINF have different grades and years

    if (substr($stu['schoolyearsenr'],-4) >= substr($stu['schoolyearsinf'],-4)) {
        $acyear = substr($stu['schoolyearsenr'],2,2) . substr($stu['schoolyearsenr'],7,2);
    }
    else
    {
        $acyear = substr($stu['schoolyearsinf'],2,2) . substr($stu['schoolyearsinf'],7,2);
    }
 
    
    if ($stu['gradelevelsinf'] <> '' && !$newlayout) {
        $gradelevel = $stu['gradelevelsinf'];
    }
    else
    {
        if ($filetype == 'Original') {
            $gradelevel = $stu['gradelevelsenr'];
        }
        else
        {
            // Adjusting Grade Level in SENR in new 
            $gradelevel = $stu['gradelevelsenr'] + (substr($acyear,-2) - substr($stu['schoolyearsenr'],-2));
            if ($gradelevel > 12) $gradelevel = 12;
        }
    }
    
    if ($debug) printf("Processing Student/Award %s-%s-%s-%s\n", $stu['studentid'], $stu['ssid'],
    $stu['lname'] . "," . $stu['fname'], $acyear);

    // Set Hispanic/Ethnicity
    if ($stu['hispanic'] == '') {
        $hisp = 'U';
        if ($stu['ethnicity'] <> '') {
            $eth = $stu['ethnicity'];  
        }
        else                 
        {
            $hisp = 'U';
            $eth = '';   
        }
    }
    else
    {
        $hisp = $stu['hispanic'];
        $eth = $stu['ethnicity'];     
    }   
      
    // Set Language
    $language = $stu['primarylanguage'];
    if ($language == '') $language = $stu['primarylanguagesela'];
    if ($language == 'UU') $language = 99;

    $fields = array($stu['districtcode'] . $stu['schoolcode'], $acyear, $stu['studentid'], '', 
    $stu['ssid'], $stu['fname'], $stu['lname'], $stu['gender'], '', $stu['birthdate'], $gradelevel, 
    $language, $hisp, $eth);

    // Check for empty fields
    if ($stu['districtcode'] <> '' AND $stu['schoolcode'] <> '' AND 
            $stu['studentid'] <> '' AND $stu['fname'] <> '' AND $stu['lname'] <> '' AND 
            $stu['gender'] <> '' AND $stu['birthdate'] <> '') {
        if ($acyear == $year) {
            $stustatus = fputcsv($stufile, $fields);                       // Write S record
            // If can't write CSV, crash and burn
            if ($stustatus <= 0) {
                printf("stustatus=%s\n", $stustatus);
                print_r($fields);
                die("Student Record could not be written.");
            }
        }
    }
    else
    {
        $badstu++;
        if ($debug) print_r($fields);
    }
     
    // Set Award info
    // Exit Code 360/480 is non-grads so no award record
    if ($stu['exitcode'] <> '' AND substr($stu['exitcode'],-3) <> '360' AND 
        substr($stu['exitcode'],-3) <> '480' AND  // Added the follow filter to remove dupe award recs
        $stu['exitdate'] >=  '20' . substr($year,0,2) . "0901" AND 
        $stu['exitdate'] < '20' . substr($year,2,2) . "0901") {
        
       if ($stu['exitdate'] <> '') {
            $awarddate = $stu['exitdate'];
        }
        else
        {
            $awarddate = '99999999';
        }

        $fields = array($stu['districtcode'] . $stu['schoolcode'], $acyear, $stu['studentid'],
        substr($stu['exitcode'],-3), $awarddate);
        if ($stu['districtcode'] <> '' AND $stu['schoolcode'] <> '' AND 
                $stu['studentid'] <> '' AND $stu['fname'] <> '' AND $stu['lname'] <> '' AND 
                $stu['gender'] <> '' AND $stu['birthdate'] <> '') {
            if ($acyear == $year) {
                $awardstatus = fputcsv($awardfile, $fields);                // Write A record
                // If can't write CSV, crash and burn
                if ($awardstatus <= 0) {
                    printf("awardstatus=%s\n", $awardstatus);
                    print_r($fields);
                    die("Award Record could not be written.");
                }
            }
        }
        else
        {
            $badaward++;
            if ($debug) print_r($fields);
        }
    }
}
// Create Student Records for Students who are in SINF but not SENR -- only works in old layout
if (!$newlayout) {
    $stuq = "SELECT DISTINCT a.studentid, a.ssid, a.lname, a.fname, a.primarylanguage, 
    a.districtcode, a.schoolcode, a.gender, a.birthdate, a.gradelevel, a.schoolyear, 
    a.hispanic, a.ethnicity 
    FROM sinf as a 
    WHERE a.schoolyear = '20" . substr($year,0,2) . "-20" . substr($year,-2)  . "'
    ORDER BY a.schoolyear DESC, a.enddate DESC";
    //printf("stuq=$stuq\n");
    $stur = mysql_query($stuq);
    while ($stu = mysql_fetch_assoc($stur)) {
        if (!in_array($stu['studentid'], $seen)) {
            $seen[] = $stu['studentid'];
            $acyear = substr($stu['schoolyear'],2,2) . substr($stu['schoolyear'],7,2);
            $gradelevel = $stu['gradelevel'];

            if ($debug) printf("Processing Student from INF %s-%s-%s-%s\n", $stu['studentid'], $stu['ssid'],
            $stu['lname'] . "," . $stu['fname'], $acyear);

       // Set Hispanic/Ethnicity
            if ($stu['hispanic'] == '') {
                $hisp = 'U';
                if ($stu['ethnicity'] <> '') {
                    $eth = $stu['ethnicity'];  
                }
                else
                {
                    $hisp = 'U';
                    $eth = '';   
                }
            }
            else
            {
                $hisp = $stu['hispanic'];
                $eth = $stu['ethnicity'];     
            }   

            // Set Primary Language
            $language = $stu['primarylanguage'];
            if ($language == 'UU') $language = 99;

            $fields = array($stu['districtcode'] . $stu['schoolcode'], $acyear, $stu['studentid'], '', 
            $stu['ssid'], $stu['fname'], $stu['lname'], $stu['gender'], '', $stu['birthdate'], $gradelevel, 
            $language, $hisp, $eth);

            if ($stu['districtcode'] <> '' AND $stu['schoolcode'] <> '' AND 
            $stu['studentid'] <> '' AND $stu['fname'] <> '' AND $stu['lname'] <> '' AND 
            $stu['gender'] <> '' AND $stu['birthdate'] <> '') {
                if ($acyear == $year) {
                    $stustatus = fputcsv($stufile, $fields);                    // Write S record
                    // If can't write CSV, crash and burn
                    if ($stustatus <= 0) {
                        printf("stustatus=%s\n", $stustatus);
                        print_r($fields);
                        die("Record could not be written.");
                    }
                }
            }
            else
            {
                $badstu++;
                if ($debug) print_r($fields);
            }
        }
    }
}

// It is typical to find records with an SSID but no Local Student ID in the SCSC file on ODS Extracts 
// It is apparently a known glitch in CALPADS
// So we look up the Local Student ID from the SENR data or the SINF data
// Finding Missing Student IDs
$idq = "SELECT DISTINCT studentid, ssid FROM scsc WHERE studentid = ''";
$idr = mysql_query($idq);
if (mysql_num_rows($idr) > 0) {
   printf("\n\nFound Student Course Records with SSID, but no Local Student ID.  Updating...\n");
    while ($id = mysql_fetch_assoc($idr)) {
        $stuq = "SELECT studentid, ssid FROM senr WHERE ssid = '" . $id['ssid'] . "' AND 
        studentid <> '' LIMIT 1";
        $stur = mysql_query($stuq);
        if (mysql_num_rows($stur) == 1) {
                $stu = mysql_fetch_assoc($stur);
                $updq = "UPDATE scsc SET studentid = '" . $stu['studentid'] . "' 
                WHERE ssid = '" . $id['ssid'] . "'";
                $updr = mysql_query($updq);
                printf("Matched: %s\n", $id['ssid']);
        }
        else
        {
         // If we can't find them in SENR, looking in SINF
            $stuq = "SELECT studentid, ssid FROM sinf WHERE ssid = '" . $id['ssid'] . "' AND 
            studentid <> '' LIMIT 1";
            $stur = mysql_query($stuq);
            if (mysql_num_rows($stur) == 1) {
                $stu = mysql_fetch_assoc($stur);
                $updq = "UPDATE scsc SET studentid = '" . $stu['studentid'] . "' 
                WHERE ssid = '" . $id['ssid'] . "'";
                //                      printf("updq=$updq\n");
                $updr = mysql_query($updq);
                printf("Matched: %s\n", $id['ssid']);
            }
            else
            {
            // IF we can't find it in either file set it to the SSID as a last resort to try to match
                $updq = "UPDATE scsc SET studentid = '" . $id['ssid'] . "' 
                WHERE ssid = '" . $id['ssid'] . "'";
                $updr = mysql_query($updq);
                printf("No Match: %s\n", $id['ssid']);             
            }
        }
    }
}

// Creating the Course file from the combination of the CRSC and the SCSC files
printf("\n\nCreating Course File...\n");
$courseq = "SELECT DISTINCT a.*, b.courseid, b.coursetitle, b.courselevel, b.courseterm, 
b.coursetype, b.funding 
FROM scsc as a INNER JOIN crsc as b 
ON a.localcourseid = b.localcourseid AND a.schoolyear = b.schoolyear AND 
a.schoolcode = b.schoolcode";
//printf("courseq=" . $courseq);

$courser = mysql_query($courseq);
while ($course = mysql_fetch_assoc($courser)) {

    $acyear = substr($course['schoolyear'],2,2) . substr($course['schoolyear'],7,2);

    if ($course['courseid'] != NULL) {
        $courseid = $course['courseid'];
        $coursetitle = $course['coursetitle'];
        $localcourseid = $course['localcourseid'];
        $funding = $course['funding'];
        $courselevel = convcourselevel($course['courselevel'], $courseid);
        $coursetype = convcoursetype($course['coursetype'], $funding);

        // Some districts mark term as FY, but use the marking period to designate semesters
        if ($course['markingperiod'] <> '') {
            $courseterm = convcourseterm($course['markingperiod']);
        }
        else
        {
            $courseterm = convcourseterm($course['courseterm']);
        }
    }
    else
    {
        $courseid = '';
        $coursetitle = '';
        $courselevel = 'XX';
        $coursetype = 'XX';
        if ($course['markingperiod'] <> '') {
            $courseterm = convcourseterm($course['markingperiod']);
        }
        else
        {
            $courseterm = convcourseterm($course['courseterm']);
        }
    }

    if ($debug) printf("Processing Course %s-%s-%s-%s\n", $course['studentid'], $course['ssid'], 
    $course['courseterm'], $acyear);
    $fields = array($course['districtcode'] . $course['schoolcode'], $acyear, 
    $course['studentid'], $course['courseid'], $course['localcourseid'], 
    $course['coursesection'], $course['coursetitle'], $course['agstatus'], $course['mark'], 
    $course['creditearned'], $course['creditattempted'], $courselevel, $coursetype, 
    $courseterm);
    if ($course['districtcode'] <> '' AND $course['schoolcode'] <> '' AND 
        $course['studentid'] <> '') {
        if ($acyear == $year) {
            $coursestatus = fputcsv($coursefile, $fields);                 // Write Course Record
            if ($coursestatus <= 0) {
                    printf("coursestatus=%s\n", $coursestatus);
                    print_r($fields);
                    die("Course Record could not be written.");
            }
        }
    }
            
    else
    {
        $badcourse++;
        if ($debug) print_r($fields);
    }
} 

// Program File
$progq = "SELECT DISTINCT cds, schoolyear, studentid, ssid, 
fname, lname, gender, birthdate, edprogramcode,
edprogrammembershipcode, edpmstart, edpmend, edserviceyear, edservicecode, 
capartacademyid, migrantstudentid, primarydisabilitycode, speced,
homelessdwelling, unaccyouth,runaway
FROM sprg
WHERE schoolyear = '20" . substr($year,0,2) . "-20" . substr($year,-2)  . "'";
$progr = mysql_query($progq);
printf("Found %d Program Records\n", mysql_num_rows($progr));
while ($prog = mysql_fetch_assoc($progr)) {
    $acyear = substr($prog['schoolyear'],2,2) . substr($prog['schoolyear'],7,2);

    $fields = $prog;
    $acyear = substr($prog['schoolyear'],2,2) . substr($prog['schoolyear'],7,2);
    $fields['schoolyear'] = $acyear;

    if ($debug) printf("Processing %s-%s-%s-%s-%s\n",
    $prog['cds'], $fields['schoolyear'], $prog['studentid'], $prog['ssid'],
    $prog['edprogramcode'], $prog['edprogrammembershipcode']);
    if ($prog['cds'] <> '' AND $fields['schoolyear'] <> '' AND $prog['studentid'] <> '') {
        if ($acyear == $year) {
            $progstatus = fputcsv($programfile, $fields);
            if ($progstatus <= 0) {
                printf("programstatus=%s\n", $progstatus);
                print_r($fields);
                die("Program Record could not be written");
            }
        }
    }
    else
    {
        $badprog++;
        if ($debug) print_r($fields);
    }
}
 
// CTE File
$cteq = "SELECT DISTINCT cds, schoolyear, studentid, ssid, fname, lname, gender, 
birthdate, ctepathwaycode, ctepathwaycompacyear
FROM scte
WHERE schoolyear = '20" . substr($year,0,2) . "-20" . substr($year,-2)  . "'";
$cter = mysql_query($cteq);
printf("Found %d CTE Records\n", mysql_num_rows($cter));
while ($cte = mysql_fetch_assoc($cter)) {
    $fields = $cte;
    $acyear = substr($cte['schoolyear'],2,2) . substr($cte['schoolyear'],7,2);
    $compyear = substr($cte['ctepathwaycompacyear'],2,2) . substr($cte['ctepathwaycompacyear'],7,2);
    $fields['schoolyear'] = $acyear;
    $fields['ctepathwaycompacyear'] = $compyear;
    if ($debug) printf("Processing %s-%s-%s-%s-%s-%s\n",
    $cte['cds'], $fields['schoolyear'], $cte['studentid'], $cte['ssid'],
    $cte['ctepathwaycode'], $fields['ctepathwaycompacyear']);
    if ($cte['cds'] <> '' AND $fields['schoolyear'] <> '' AND $cte['studentid'] <> '') {
        if ($acyear == $year) {
            $ctestatus = fputcsv($ctefile, $fields);
            if ($ctestatus <= 0) {
                printf("programstatus=%s\n", $ctestatus);
                print_r($fields);
                die("CTE Record could not be written");
            }
        }    
    }
    else
    {
        $badcte++;
        if ($debug) print_r($fields);
    }
}

if ($newlayout) {
    $layoutname = "New";
}
 else {
    $layoutname = "Old";
}
logupdate($dcode, $year, "Created Classic CALPASS Files from $layoutname style CALPADS $filetype files.");
if ($badstu > 0 OR $badcourse > 0 OR $badaward > 0 OR $badprog > 0 OR $badcte > 0)
logupdate($dcode, $year, "Dropped S:$badstu C:$badcourse A:$badaward P:$badprog V:$badcte");
// Functions to perform conversions from CALPADS to Cal-PASS data 

function convcourselevel($courselevel, $statecourseid) {
// Try to determine hte course level based on the course level field or the state course id field
/*
Course Non Standard Instructional Level        10      Remedial
Course Non Standard Instructional Level        12      Gifted and Talented
Course Non Standard Instructional Level        14      Honors - UC Certified
Course Non Standard Instructional Level        15      Honors Non-UC certified
Course Non Standard Instructional Level        16      College*/

   switch($courselevel) {
      case '10':
         $courselevel2 = 35;
      break;
      
      case '12':
         $courselevel2 = 33;
      break;
      
      case '14':
       $courselevel2 = 39;
      break;
      
      case '15':
         $courselevel2 = 34;
      break;
      
      case '16':
         $courselevel2 = 40;
      break;
      
      default:
         $courselevel2 = 'XX';
      break;
   }
   if ($courselevel2 == 'XX') {
		switch($statecourseid) {
			// IB Higher
			case '2462':
				$courselevel2 = 37;
			break;

			// IB Standard
			case '2160':
			case '2260':
			case '2261':
			case '2262':
			case '2263':
			case '2264':
			case '2360':
			case '2460':
			case '2461':
			case '2463':
			case '2465':
			case '2466':
			case '2660':
			case '2661':
			case '2662':
			case '2664':
			case '2666':
			case '2760':
			case '2761':
			case '2762':
			case '2763':
			case '2764':
			case '2765':
			case '2766':
			case '2767':
			case '2768':
			case '2860':
			case '2960':
				$courselevel2 = 38;
			break;

			// AP 
			case '2170':
			case '2171':
			case '2270':
			case '2271':
			case '2272':
			case '2273':
			case '2274':
			case '2275':
			case '2276':
			case '2277':
			case '2278':
			case '2279':
			case '2370':
			case '2470':
			case '2471':
			case '2480':
			case '2481':
			case '2483':
			case '2670':
			case '2671':
			case '2672':
			case '2673':
			case '2674':
			case '2770':
			case '2771':
			case '2772':
			case '2773':
			case '2774':
			case '2775':
			case '2776':
			case '2777':
			case '2778':
			case '2870':
			case '2874':
			case '2875':
			case '2876':
				$courselevel2 = 30;
			break;

			// Remedial
			case '2402':
				$courselevel2 = 35;
			break;

			default:
			break;      
		}
   }   
   return($courselevel2);
}

// Determine the course type field based on course type and the funding source field
function convcoursetype($coursetype, $funding) {
   if ($coursetype == 'Y') {
      if ($funding == 1) {
         $coursetype2 = 33;
      }
      else
      {
			$coursetype2 = 32;
      }
   }
   else
   {
      if ($funding == 1) {
         $coursetype2 = 30;
      }
      else
      {
			$coursetype2 = 'XX';
      }
   }
return($coursetype2);
}   

// Convert the course term data
function convcourseterm($courseterm) {
/*
Academic Term  FY      Full Year
Academic Term  H1      First Hexmester
Academic Term  H2      Second Hexmester
Academic Term  H3      Third Hexmester
Academic Term  H4      Fourth Hexmester
Academic Term  H5      Fifth Hexmester
Academic Term  H6      Sixth Hexmester
Academic Term  IS      Intersession
Academic Term  Q1      First Quarter
Academic Term  Q2      Second Quarter
Academic Term  Q3      Third Quarter
Academic Term  Q4      Fourth Quarter
Academic Term  S1      First Semester
Academic Term  S2      Second Semester
Academic Term  SP      Supplemental Session
Academic Term  SS      Summer Session
Academic Term  T1      First Trimester
Academic Term  T2      Second Trimester
Academic Term  T3      Third Trimester
Academic Term  Z1      Other First Term
Academic Term  Z2      Other Second Term
Academic Term  Z3      Other Third Term
Academic Term  Z4      Other Fourth Term
Academic Term  Z5      Other Fifth Term
Academic Term  Z6      Other Sixth Term
Academic Term  Z7      Other Seventh Term
Academic Term  Z8      Other Eighth Term
Academic Term  Z9      Other Ninth Term
*/
   switch($courseterm) {
      case 'SS':
         $courseterm2 = 'FS1';
      break;
      
      case 'IS':
         $courseterm2 = 'IS1';
      break;
      
      case 'SP':
         $courseterm2 = 'SPL';
      break;
      
      default:
         $courseterm2 = $courseterm;
      break;
   }
	return($courseterm2);
}

function logupdate($district, $year, $msg) {
    try {
	$dsna = 'dblib:dbname=calpass;host=prodatsql03';
//	$dsna = 'dblib:dbname=calpass;host=prodatsql01';
	$user = 'CALPASS\\zz_gua_d1_admin';
	$password = 'd2001O!#%';
	$dbh = new PDO($dsna, $user, $password);
    }
    catch(PDOException $e) {  
             echo $e->getMessage();  
    }
    $date = date('Y/m/d H:i:s');
    $processid = getmypid();
    
    switch(strlen($district)) {
        case 5:
            $distq = "SELECT OrganizationId, OrganizationCode, OrganizationName "
                . "FROM Organization "
                . "WHERE SUBSTRING(OrganizationCode,3,5) = '" . $district . "' AND "
                . "RIGHT(OrganizationCode,7)='0000000'";
            break;
        
        case 6:
            $distq = "SELECT OrganizationId, OrganizationCode, OrganizationName "
                . "FROM Organization "
                . "WHERE OrganizationId = '" . $district . "'";
            break;
        
        case 7:
            $distq = "SELECT OrganizationId, OrganizationCode, OrganizationName "
                . "FROM Organization "
                . "WHERE LEFT(OrganizationCode,7) = '" . $district . "' AND "
                . "RIGHT(OrganizationCode,7)='0000000'";
            break;
    }
    //print("distq=$distq\n");
    $distr = $dbh->query($distq);
    $distr->setFetchMode(PDO::FETCH_ASSOC);
    $dist = $distr->fetch();
    
    $insq = "INSERT INTO LoaderLog"
    . "(orgid, orgcode, orgname, acyear, processid, date, message) "
    . "VALUES "
    . "('" . $dist['OrganizationId'] . "', '" . 
    substr($dist['OrganizationCode'],0,7) . "', '" . 
    $dist['OrganizationName'] . "', '" . $year . "', '" . $processid . "', '" . 
    $date . "', '" . 
    $msg . "')";
    $insr = $dbh->query($insq);
//    printf("insq=$insq\n");
    return;
}
?>
