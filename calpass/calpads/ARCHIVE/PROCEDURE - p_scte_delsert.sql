USE calpass;

GO

IF (object_id('calpads.p_scte_delsert') is not null)
	BEGIN
		DROP PROCEDURE calpads.p_scte_delsert;
	END;

GO

CREATE PROCEDURE
	calpads.p_scte_delsert
	(
		@SubmissionFileId int,
		@StageSchemaTableName nvarchar(517)
	)
AS

DECLARE
	@Message nvarchar(2048),
	@Validate bit = 0,
	@Sql nvarchar(max),
	@Needle varchar(255) = char(13) + char(10) + char(9) + char(9),
	@Replace varchar(255) = char(13) + char(10),
	@Query nvarchar(4000) = N'SELECT * FROM ' + @StageSchemaTableName + ';',
	@ColumnArray nvarchar(max) = N'';

BEGIN
	-- validate inputs
	SELECT
		@Validate = 1
	FROM
		sys.tables
	WHERE
		object_id = object_id(@StageSchemaTableName);
	-- process validation
	IF (@Validate = 0)
		BEGIN
			SET @Message = N'Table not found';
			THROW 70099, @Message, 1;
		END;
	-- delete data from production using staging
	SET @Sql = replace(N'
		DELETE
			t
		FROM
			calpads.scte t with (tablockx)
			inner join
			' + @StageSchemaTableName + ' s
				on t.district_code = s.district_code
				and t.school_code = s.school_code
				and t.year_code = s.year_code
				and t.ssid = s.ssid;', @Needle, @Replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@Sql;
	-- set columns
	SET @Sql = replace(N'
		SELECT
			@ColumnArray += name + case when count(*) over() = row_number() over(order by (select 1)) then '''' else '','' end
		FROM
			sys.dm_exec_describe_first_result_set(@query, null, 0);', @Needle, @Replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@Sql,
		N'@query nvarchar(max),
			@ColumnArray nvarchar(max) out',
		@query = @query,
		@ColumnArray = @ColumnArray out;
	-- set dynamic sql
	SET @Sql = replace(N'
		INSERT INTO
			calpads.scte with (tablockx)
			(
				SubmissionFileId,IsEscrow,' + @ColumnArray + '
			)
		SELECT
			@SubmissionFileId,1,' + @ColumnArray + '
		FROM
			' + @StageSchemaTableName + ';', @Needle, @Replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@Sql,
		N'@SubmissionFileId int',
		@SubmissionFileId = @SubmissionFileId;
END;