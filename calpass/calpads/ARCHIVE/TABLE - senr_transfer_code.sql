USE calpass;

GO

IF (object_id('calpads.senr_transfer_code') is not null)
	BEGIN
		DROP TABLE calpads.senr_transfer_code;
	END;

GO

CREATE TABLE
	calpads.senr_transfer_code
	(
		code char(1) NOT NULL,
		name nvarchar(255) NULL,
		description nvarchar(4000) NULL
	);

GO

ALTER TABLE
	calpads.senr_transfer_code
ADD CONSTRAINT
	pk_senr_transfer_code__code
PRIMARY KEY CLUSTERED 
	(
		code
	);

/* DML */

GO

INSERT INTO
	calpads.senr_transfer_code
	(
		code,
		name,
		description
	)
VALUES
('1','Formal Interdistrict Transfer Agreement','This is a formal agreement between two districts that approves a transfer from one district to another pursuant to Education Code Section 46600. This category does not include transfers that occur as a result of establishing residency in one of the following ways: 1) EC 48204b a parent or guardian requests that their child be transferred to a school district where the parent or legal guardian is physically employed; or 2) enrollment in a charter school outside the district of residence.'),
('2','NCLB Public School Choice  - Program Improvement','This a transfer for students who were enrolled in a school identified for program improvement, corrective action or restructuring; were planning to enter the school for the first time; who moved into the school''s attendance area; or were matriculating to the school, and who exercised their right to request enrollment in a different school.'),
('3','NCLB Public School Choice Transfer - Persistently Dangerous','This a transfer where the student exercised the option to transfer from a school which the state has identified as persistently dangerous, or in which the student was a victim of violent crime on school property.'),
('4','District of Choice Transfer','This is a transfer where the student transferred into the district pursuant to the district having deemed itself a District of Choice, pursuant to Education Code Section 48313.'),
('5','Disciplinary COE School Transfer','A student who transferred to a county office of education school (not a juvenile court school), including charter schools operating county programs,  was transferred to that  school for one or more of the following reasons:
	• Probation referral pursuant to Sections 300, 601, 602, and 654 of the Welfare and Institutions Code.
	• On probation or parole and not in attendance in a school other than the county office of education school.
	• Expelled for any of the reasons specified in subdivision (a) or (c) of Education Code Section 48915.'),
('6','Other Transfer','This is any other type of student transfer that may occur including, but not limited to:
	• Students voluntarily choosing to transfer to a county office of education school, who were not transferred due to any of the reasons specified in the Disciplinary COE School Transfer code.
	• Students who have established residency because their parents work in the area.
	• Special education students who are transferring to another school because it written into their Individualized Education Plan (IEP).
	• Students who are transferring to a charter school that is outside their district of residence.');