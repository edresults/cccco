SELECT
	s.district_code,
	s.school_code,
	s.year_code,
	(s.rx3 - 3*s.rx2*s.av + 3*s.rx*s.av*s.av - s.rn*s.av*s.av*s.av) / nullif((s.stdevp*s.stdevp*s.stdevp), 0) * rn / nullif((rn-1), 0) / nullif((rn-2), 0)
FROM
	(
		SELECT
			s.district_code,
			s.school_code,
			s.year_code,
			sum(s.cnt) as rx,
			sum(s.cnt * s.cnt) as rx2,
			sum(s.cnt * s.cnt * s.cnt) as rx3,
			count(s.cnt) as rn,
			stdevp(s.cnt) as stdevp,
			avg(s.cnt) as av
		FROM
			(
				SELECT
					s.district_code,
					s.school_code,
					s.year_code,
					s.grade_level_code,
					cast(count(s.ssid) as bigint) as cnt
				FROM
					calpads.dbo.senr s
				WHERE
					s.district_code = '3768296' -- Poway
					and s.grade_level_code like '[0-9][0-9]' -- 1-12
				GROUP BY
					s.district_code,
					s.school_code,
					s.year_code,
					s.grade_level_code
			) s
		GROUP BY
			s.district_code,
			s.school_code,
			s.year_code
-- ORDER BY
	-- s.district_code,
	-- s.school_code,
	-- s.year_code
	) s
ORDER BY
	s.district_code,
	s.school_code,
	s.year_code