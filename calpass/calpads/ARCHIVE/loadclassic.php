<?php
 ini_set('memory_limit', '512M');
/*
Load Classic CalPass files and STAR/CAHSEE to production on SQL03
*/
//$debug = 1;
$debug = 0;
$district = $argv[1];
if ($district == '') die("No district selected.");
$path = $argv[2];
try {
	$dsna = 'dblib:dbname=calpass;host=prodatsql03';
//	$dsna = 'dblib:dbname=calpass;host=prodatsql01';
	$user = 'CALPASS\\zz_gua_d1_admin';
	$password = 'd2001O!#%';
	$dbh = new PDO($dsna, $user, $password);
}
catch(PDOException $e) {  
	 echo $e->getMessage();  
}
$year = substr($path,-4);

// Find Files
printf("Finding Files.\n");
$files = scandir($path);
$sindex = $cindex = $aindex = $pindex = $vindex = -99;
$cahseefiles = $starfiles = array();

$count = 0;
foreach($files as $file) {
    if (stripos($file, 'S' . $district . "K.TXT") !== FALSE) 
        $sindex = $count;
    if (stripos($file, 'C' . $district . "K.TXT") !== FALSE) 
        $cindex = $count;
    if (stripos($file, 'A' . $district . "K.TXT") !== FALSE) 
        $aindex = $count;
    if (stripos($file, 'P' . $district . "K.TXT") !== FALSE) 
        $pindex = $count;
    if (stripos($file, 'V' . $district . "K.TXT") !== FALSE) 
        $vindex = $count;
    if (stripos($file, 'CAHSEE') !== FALSE) 
        $cahseefiles[] = $file;
    if (stripos($file, 'REC1') !== FALSE) 
        $starfiles[] = $file;
     $count++;
}
if ($debug) print_r($files);
if ($sindex <> -99) {    
    printf("Found Classic Files.\n");

    // Check for existing records 
    printf("Checking for existing records...\n");
    $checkq = "SELECT TOP 1 derkey1 
    FROM K12StudentProd 
    WHERE substring(School,3,5) = '" . $district . "' AND "
            . "AcYear = '" . substr($path,-4) . "'";
    $checkr = $dbh->query($checkq);
    $checkr->setFetchMode(PDO::FETCH_ASSOC);
    $check = $checkr->fetch();
    if ($check['derkey1'] <> '') {
        logupdate($district, substr($path,-4), "Found existing records. Skipping upload of Student, Course and Award files.\n");

        printf("Found existing records. Skipping upload of Student, Course and Award files.\n");
        $skipbasic = 1;
    }
    else
    {
        printf("None found. Moving on.\n");
        $skipbasic = 0;
    }

    if (!$skipbasic) {

        printf("Loading Students.\n");
        $stu = array();
        $stucount = $coursecount = $awardcount = $progcount = $ctecount = 0;
        // Student
        if ($files[$sindex] <> -99) {
            if (($handle = fopen($path . "/" . $files[$sindex], "r")) !== FALSE) {   // Open the file
               while (($data = fgetcsv($handle, 3000, ",")) !== FALSE) {      // Grab a row
                   $school = $data[0];
                   $acyear = $data[1];
                   $locstudentid = $data[2];
                   $studentid = $data[3];
                   $csisnum = $data[4];
                   $fname = cleanname($data[5]);
                   $lname = cleanname($data[6]);
                   $gender = $data[7];
                   $birthdate = $data[9];
                   $gradelevel = $data[10];
                   $homelanguage = $data[11];
                   $hispanicethnicity = $data[12];
                   $ethnicity = $data[13];

                   // Split Ethnicity Codes
                   $ethnicitycode1 = $ethnicitycode2 = $ethnicitycode3 = $ethnicitycode4 = 
                   $ethnicitycode5 = '';       

                    if (strlen($ethnicity) > 0) {
                        $ethnicitycode1 = substr($ethnicity,0,3);
                        if (strlen($ethnicity) > 3) {
                            $ethnicitycode2 = substr($ethnicity,3,3);
                            if (strlen($ethnicity) > 6) {
                                $ethnicitycode3 = substr($ethnicity,6,3);
                                if (strlen($ethnicity) > 9) {
                                    $ethnicitycode4 = substr($ethnicity,9,3);
                                    if (strlen($ethnicity) > 12) {
                                        $ethnicitycode5 = substr($ethnicity,12,3);
                                    }
                                }
                            }
                        }
                    }

                    $dateadded = date("Y-m-d H:i:s");
                    $schoolcode = substr($school,7,6);
                    $origlocstudentid = $locstudentid;

                    $fname = cleanname($fname);
                    $lname = cleanname($lname);
                    if (substr($csisnum,0,3) == 'XXX') $csisnum = '';
                    // Derkey
                    $keyq = "SELECT dbo.Derkey1('" . strtoupper($fname) . "', '" . strtoupper($lname) . "', 
                    '" . $gender . "', '" . $birthdate . "') as derkey1, 
                    dbo.Get1289Encryption('"  . $locstudentid . "', '') as locstudentid, 
                    dbo.Get1289Encryption('"  . $studentid . "', '') as studentid, 
                    dbo.Get9812Encryption('"  . $csisnum . "', '') as csisnum";
                    if ($debug) printf("keyq=$keyq\n");
                    $keyr = $dbh->query($keyq);
                    $key = $keyr->fetch();
                    $derkey1 = $key['derkey1'];
                    $locstudentid = $key['locstudentid'];
                    $studentid = $key['studentid'];
                    if (strlen($studentid) > 9) $studentid = 'XXXXXXXXX';
                    $csisnum = $key['csisnum'];
                    $stu[$origlocstudentid][$acyear]['derkey'] = $derkey1;
                    $stu[$origlocstudentid][$acyear]['locstudentid'] = $locstudentid;
                    $stu[$origlocstudentid][$acyear]['studentid'] = $studentid;
                    $stu[$origlocstudentid][$acyear]['csisnum'] = $csisnum;

                    // Write Record
                    $insq = "INSERT INTO K12StudentProd
                    (derkey1, derkey2, School, AcYear, LocStudentId, StudentId, CSISNum, FName, LName,
                    Gender, Birthdate, GradeLevel, HomeLanguage, HispanicEthnicity, EthnicityCode1,
                    EthnicityCode2, EthnicityCode3, EthnicityCode4, EthnicityCode5, DateAdded)
                    VALUES 
                    ('" . $derkey1. "', '" . $derkey1. $schoolcode.  "', '" . $school . "', 
                    '" . $acyear . "', '" . $locstudentid . "', '" . $studentid . "', 
                    '" . $csisnum . "', '" . $fname . "', '" . $lname . "',
                    '" . $gender . "', '" . $birthdate . "', '" . $gradelevel . "',
                    '" . $homelanguage . "', '" . $hispanicethnicity . "', 
                    '" . $ethnicitycode1 . "', '" . $ethnicitycode2 . "',
                    '" . $ethnicitycode3 . "', '" . $ethnicitycode4 . "',
                    '" . $ethnicitycode5 . "', '" . $dateadded . "')";
                    if ($debug) printf("insq=$insq\n");        
                    $sth = $dbh->query($insq);
                    $stucount++;
                }
            }   
        }

        // Course
        printf("Loading Courses.\n");
        $dropcourse = 0;
        if ($files[$cindex] <> -99) {
            if (($handle = fopen($path . "/" . $files[$cindex], "r")) !== FALSE) {   // Open the file
                while (($data = fgetcsv($handle, 3000, ",")) !== FALSE) {      // Grab a row

                    $school = $data[0];
                    $acyear = $data[1];
                    $origlocstudentid = $data[2];
                    $courseid = $data[3];
                    $locallyassignedcoursenumber = $data[4];
                    $coursesectionclassnumber = $data[5];
                    $coursetitle = $data[6];
                    $agstatus = $data[7];
                    $grade = $data[8];
                    $creditearned = $data[9];
                    $creditattempted = $data[10];
                    $courselevel = $data[11];
                    $coursetype = $data[12];
                    $courseterm = $data[13];

                    $dateadded = date("Y-m-d H:i:s");
                    $schoolcode = substr($school,7,6);
        //            $derkey1 = $stu[$origlocstudentid][$acyear]['derkey'];
        //            $derkey2 = $derkey1 . $schoolcode;
        //            $locstudentid = $stu[$origlocstudentid][$acyear]['locstudentid'];
                    $derkey1 = isset($stu[$origlocstudentid][$acyear]['derkey']) ? $stu[$origlocstudentid][$acyear]['derkey']: '';
                    $derkey2 = $derkey1 . $schoolcode;
                    $locstudentid = isset($stu[$origlocstudentid][$acyear]['locstudentid']) ? $stu[$origlocstudentid][$acyear]['locstudentid'] : '';


                    // Write Record 
                    if ($derkey1 <> '') {
                        $insq = "INSERT INTO K12CourseProd 	
                        (derkey1, derkey2, School, AcYear, LocStudentId, CourseId, 
                        LocallyAssignedCourseNumber, CourseSectionClassNumber, CourseTitle,
                        AGstatus, Grade, CreditEarned, CreditAttempted,
                        CourseLevel, CourseType, CourseTerm, DateAdded)
                        VALUES 
                        ('" . $derkey1. "', '" . $derkey2 . "', '" . $school . "', 
                        '" . $acyear . "', '" . $locstudentid . "', '" . $courseid . "', 
                        '" . $locallyassignedcoursenumber . "', '" . $coursesectionclassnumber . "', 
                        '" . $coursetitle . "', '" . $agstatus . "', '" . $grade . "', '" . $creditearned . "', 
                        '" . $creditattempted . "', '" . $courselevel . "', '" . $coursetype . "', 
                        '" . $courseterm . "', '" . $dateadded . "')";
                        if ($debug) printf("insq=$insq\n");
                        $sth = $dbh->query($insq);
                        $coursecount++;
                    }
                    else
                    {
                        //die("Couldn't locate derkey for course record.");
                        if ($debug) printf("Dropping Course Record: %d\n", $dropcourse++);
                    }
                }
            }
        }

        // Award
        printf("Loading Awards.\n");
        $dropaward = 0;
        if ($files[$aindex] <> -99) {
            if (($handle = fopen($path . "/" . $files[$aindex], "r")) !== FALSE) {   // Open the file
                while (($data = fgetcsv($handle, 3000, ",")) !== FALSE) {      // Grab a row

                    $school = $data[0];
                    $acyear = $data[1];
                    $origlocstudentid = $data[2];
                    $awardtype = $data[3];
                    $awarddate = $data[4];

                    $dateadded = date("Y-m-d H:i:s");
                    $schoolcode = substr($school,7,6);
                    $derkey1 = $stu[$origlocstudentid][$acyear]['derkey'];
                    $derkey2 = $derkey1 . $schoolcode; 	

                    $locstudentid = $stu[$origlocstudentid][$acyear]['locstudentid'];

                    if ($derkey1 <> '') {
                        $insq = "INSERT INTO K12AwardProd
                        (derkey1, derkey2, School, AcYear, LocStudentId, AwardType,
                        AwardDate, DateAdded)
                        VALUES
                        ('" . $derkey1. "', '" . $derkey2 . "', '" . $school . "', 
                        '" . $acyear . "', '" . $locstudentid . "', '" . $awardtype . "', 
                        '" . $awarddate . "', '" . $dateadded . "')";
                        if ($debug) printf("insq=$insq\n");
                        $sth = $dbh->query($insq);
                        $awardcount++;
                    }
                    else
                    {
                     //  die("Couldn't locate derkey for award record.");
                         if ($debug) printf("Dropping Award Record: %d\n", $dropaward++);
                    }
                }
            }
        }

        // Prog
        printf("Loading Programs.\n");
        $dropprog = 0;
        if ($files[$pindex] <> -99) {
            if (($handle = fopen($path . "/" . $files[$pindex], "r")) !== FALSE) {   // Open the file
                while (($data = fgetcsv($handle, 3000, ",")) !== FALSE) {      // Grab a row

                    $acyear = $data[1];
                    $origlocstudentid = $data[2];

                    $dateadded = date("Y-m-d H:i:s");
                    $derkey1 = isset($stu[$origlocstudentid][$acyear]['derkey']) ? $stu[$origlocstudentid][$acyear]['derkey']: '';
                    $derkey2 = $derkey1 . $schoolcode;
                    $locstudentid = isset($stu[$origlocstudentid][$acyear]['locstudentid']) ? $stu[$origlocstudentid][$acyear]['locstudentid'] : '';        
                    $csisnum = isset($stu[$origlocstudentid][$acyear]['csisnum']) ? $stu[$origlocstudentid][$acyear]['csisnum']: '';

                    if ($derkey1 <> '') {
                        $insq = "INSERT INTO K12PRGProd
                        (derkey1, derkey2, School, AcYear, LocStudentId, CSISnum,
                        Fname, Lname, Birthdate, Gender, EdProgramCode,
                        EdProgramMembershipCode, EdPMStart, EdPMEnd, EdServiceYear,
                        EdServiceCode, CaPartAcademyID, MigrantStudentID,
                        PrimaryDisabilityCode, SpecEd, HomelessDwelling, 
                        UnaccYouth, Runaway, DateAdded)
                        VALUES
                        ('" . $derkey1. "', '" . $derkey2 . "', '" . $data[0] . "',
                        '" . $acyear . "',    
                        '" . $locstudentid . "', '" . $csisnum . "', '" . 
                        $data[4] . "',   '" . $data[5] . "', '" . $data[7] . "', '" .
                        $data[6] . "', '" . $data[8] . "', '" . $data[9] . "', '" . 
                        $data[10] . "', '" . $data[11] . "', '" . $data[12] . "', '" .
                        $data[13] . "', '" . $data[14] . "', '" . $data[15] . "', '" .      
                        $data[16] . "', '" . $data[17] . "', '" . $data[18] . "', '" .
                        $data[19] . "', '" . $data[20] . "', '" . $dateadded . "')";
                        if ($debug) printf("insq=$insq\n");
                        $sth = $dbh->query($insq);
                        $progcount++;
                    }
                    else
                    {
                     //  die("Couldn't locate derkey for Program record.");
                         if ($debug) printf("Dropping Program Record: %d\n", $dropprog++);
                    }
                }
            }
        }

        // CTE
        printf("Loading CTE Records.\n");
        $dropcte = 0;
        if ($files[$vindex] <> -99) {
            if (($handle = fopen($path . "/" . $files[$vindex], "r")) !== FALSE) {   // Open the file
                while (($data = fgetcsv($handle, 3000, ",")) !== FALSE) {      // Grab a row

                    $acyear = $data[1];
                    $origlocstudentid = $data[2];

                    $dateadded = date("Y-m-d H:i:s");
                    $derkey1 = $stu[$origlocstudentid][$acyear]['derkey'];
                    $derkey2 = $derkey1 . $schoolcode; 	

                    $locstudentid = $stu[$origlocstudentid][$acyear]['locstudentid'];
                    $csisnum = $stu[$origlocstudentid][$acyear]['csisnum'];

                    if ($derkey1 <> '') {
                        $insq = "INSERT INTO K12CTEProd
                        (derkey1, derkey2, School, AcYear, LocStudentId, CSISnum,
                        Fname, Lname, Birthdate, Gender, CTEPathwayCode, CTEPathwayCompAcYear, 
                        DateAdded)
                        VALUES
                        ('" . $derkey1. "', '" . $derkey2 . "', '" . $data[0] . "', '" .
                        $acyear . "', 
                        '" . $locstudentid . "', '" . $csisnum . "', '" . 
                        $data[4] . "',   '" . $data[5] . "', '" . $data[7] . "', '" .
                        $data[6] . "', '" . $data[8] . "', '" . $data[9] . "', '" . 
                        $dateadded . "')";
                        if ($debug) printf("insq=$insq\n");
                        $sth = $dbh->query($insq);
                        $ctecount++;
                    }
                    else
                    {
                     //  die("Couldn't locate derkey for CTE record.");
                         if ($debug) printf("Dropping CTE Record: %d\n", $dropcte++);
                    }
                }
            }
        }
        if ($stucount > 0) {
        printf("Loaded %d students, %d courses, %d awards, %d programs and %d ctes.\n", 
        $stucount, $coursecount, $awardcount, $progcount, $ctecount);
        printf("Dropped %d courses, %d awards, %d progs, %d ctes.\n", $dropcourse, $dropaward, 
                $dropprog, $dropcte);		

        logupdate($district, $year, "Loaded Data: S:$stucount C:$coursecount A:$awardcount P:$progcount V:$ctecount");
        }    
    }
}
// Find CAHSEE/STAR/CAASPP Files
//$debug=1;
$acyear = $year;
if ($debug) print_r($cahseefiles);

if (isset($cahseefiles[0])) {
    logupdate($district, $year, "Found CAHSEE files to process.");
    printf("Found CAHSEE files to process.\n");
    printf("Checking for existing CAHSEE records...\n");
    $checkq = "SELECT TOP 1 derkey1, LocalStudentId
    FROM CAHSEEDemographics 
    WHERE substring(SchoolIdentifier,3,5) = '" . $district . "' AND "
            . "SchoolYear = '" . substr($path,-4) . "'";
    if ($debug) printf("checkq=$checkq\n");
    $checkr = $dbh->query($checkq);
    $checkr->setFetchMode(PDO::FETCH_ASSOC);
    $check = $checkr->fetch();
    if ($check['derkey1'] <> '' OR $check['LocalStudentId'] <> '') {
        logupdate($district, substr($path,-4), "Found existing CAHSEE student records. Aborting.");
        printf("Found existing CAHSEE records. Aborting.\n");
    }
    else
    {
        $cahseecount = $cahseedropped = 0;
        printf("No Existing CAHSEE data found. Moving on.\n");
        foreach($cahseefiles as $cahseefile) {
            printf("Loading %s\n", $cahseefile);
            if (($handle = fopen($path . "/" . $cahseefile, "r")) !== FALSE) {   // Open the file
//                $stu = array();
                // Get Headers
                $headers = array_flip(fgetcsv($handle, 3000, "\t"));
                if ($debug) print_r($headers);
                while (($data = fgetcsv($handle, 3000, "\t")) !== FALSE) {      // Grab a row
    //                print_r($data);

                    // Get Fields to generate derived key
                    $fname = cleanname($data[$headers['FirstName']]);
                    $lname = cleanname($data[$headers['LastName']]);
                    $gender = $data[$headers['Gender']];
                    if ($data[$headers['BirthDate']] == 'U' OR 
                            $data[$headers['BirthDate']] == '' OR 
                            $data[$headers['Gender']] == 'U' OR 
                            $data[$headers['Gender']] == '') {
                        // Invalid Record
                        $cahseedropped++;
                        continue; // Skip record
                    }
                    else
                    {
                        $birthdate = fixdate($data[$headers['BirthDate']]);
                        $cahseecount++;
                    }
                    $locstudentid = $data[$headers['LocalStudentID']];
                    $csisnum = $data[$headers['SSIDStudentNumber']];
                    if ($csisnum == 'U') $csisnum = '';
                    $origlocstudentid = $data[$headers['LocalStudentID']];
                    $ETSStudentNbr = $data[$headers['ETSStudentNbr']];
                   
                    $keyq = "SELECT dbo.Derkey1('" . strtoupper($fname) . "', '" . strtoupper($lname) . "', 
                    '" . $gender . "', '" . $birthdate . "') as derkey1, 
                    dbo.Get1289Encryption('"  . $locstudentid . "', '') as locstudentid, 
                    dbo.Get9812Encryption('"  . $csisnum . "', '') as csisnum,
                    dbo.Get1289Encryption('"  . $ETSStudentNbr . "', '') as etsstudentnbr";
                    if ($debug) printf("keyq=$keyq\n");
                    $keyr = $dbh->query($keyq);
                    $key = $keyr->fetch();
                    $derkey1 = $key['derkey1'];
                    $derkey2 = $derkey1 .= substr(sprintf("%07s", $data[$headers['SchoolCode']]),0,6);
                    $locstudentid = $key['locstudentid'];
                    $csisnum = $key['csisnum'];
                    $etsstudentnbr = $key['etsstudentnbr'];
                    $dateadded = date("Y-m-d H:i:s");

                    // Deal with some deprecated fields
                    $ProgPartTitleI = isset($headers['ProgPartTitleI']) ? $data[$headers['ProgPartTitleI']] : '';
                    $ProgPartIndian = isset($headers['ProgPartIndian']) ? $data[$headers['ProgPartIndian']] : '';
                    $ProgPartELD = isset($headers['ProgPartELD']) ? $data[$headers['ProgPartELD']] : '';
                    $ASAMSchool = isset($headers['ASAMSchool']) ? $data[$headers['ASAMSchool']] : '';

                    // Demographics
                    $demoq = "INSERT INTO CAHSEEDemographics (derkey1, derkey2, 
                    SchoolYear, SchoolIdentifier, LocalStudentID, StatewideStudentIdentifier, 
                    Fname, Lname, Grade, CharterNumber, BirthDate, Gender, EthnicityPrimary,
                    EthnSecAmerIndian, EthnSecFilipino, EthnSecHisp, EthnSecAfrAmer, EthnSecWhite,
                    AsianChinese, AsianJapanese, AsianKorean, AsianVietnamese, AsianIndian, AsianLaotian,
                    AsianCambodian, AsianOther, PacIslanderHawaiian, PacIslanderGuamanian, PacIslanderSamoan, 
                    PacIslanderTahitian, PacificIslanderOther, StudentsPrimaryLanguage, ParentEducationLevel,
                    StudentsEnglishProficiency, ProgPartTitleI, ProgPartMigrant, ProgPartIndian,
                    ProgPartGifted, ProgPartELD, NSLP, SpecialEdDabltyCde, SpEDNPS, SEC504, IEP, 
                    ParentCityStateZip, RFEPDateReclassified, RFEPStudent, ASAMSchool, CAPAAssessed,
                    ResidenceCountyClientCode, Grade12Plus, HispanicLatinoInd, EthnHmong, ETSStudentNbr,
                    DateAdded)"
                    . "VALUES ('" . $derkey1 . "', '" . $derkey2 . "', 
                    '" . $acyear . "', '" . sprintf("%02s", $data[$headers['CountyCode']]) . 
                    sprintf("%05s", $data[$headers['DistrictCode']]) .
                    sprintf("%07s", $data[$headers['SchoolCode']]) . "' , '" . $locstudentid . "', 
                    '" . $csisnum . "', 
                    '" . cleanname(substr($data[$headers['FirstName']],0,3)) . "','" . 
                    cleanname(substr($data[$headers['LastName']],0,3)) . "','" . $data[$headers['Grade']] . "', 
                    '" . $data[$headers['CharterNumber']] . "', '" . $birthdate . "', 
                    '" . $gender . "','" . $data[$headers['Ethnicity']] . "', 
                    '" . $data[$headers['AmerIndian']] . "','" . $data[$headers['Filipino']] . "', 
                    '" . $data[$headers['HSPLT']] . "','" . $data[$headers['AfrAmer']] . "', 
                    '" . $data[$headers['White']] . "','" . $data[$headers['AsianChinese']] . "', 
                    '" . $data[$headers['AsianJapanese']] . "','" . $data[$headers['AsianKorean']] . "',
                    '" . $data[$headers['AsianVietnamese']] . "','" . $data[$headers['AsianIndian']] . "', 
                    '" . $data[$headers['AsianLaotian']] . "','" . $data[$headers['AsianCambodian']] . "', 
                    '" . $data[$headers['AsianOther']] . "','" . $data[$headers['PacIslanderHawaiian']] . "', 
                    '" . $data[$headers['PacIslanderGuamanian']] . "','" . $data[$headers['PacIslanderSamoan']] . "',  
                    '" . $data[$headers['PacIslanderTahitian']] . "','" . $data[$headers['PacificIslanderOther']] . "', 
                    '" . sprintf("%02d", $data[$headers['StudentsPrimaryLanguage']]) . "','" . $data[$headers['ParentEducationLevel']] . "', 
                    '" . $data[$headers['StudentsEnglishProficiency']] . "',
                    '" . $ProgPartTitleI . "',  
                    '" . $data[$headers['ProgPartMigrant']] . "','" . $ProgPartIndian . "', 
                    '" . $data[$headers['ProgPartGifted']] . "','" . $ProgPartELD . "', 
                    '" . $data[$headers['NSLP']] . "','" . $data[$headers['SpecialEdDabltyCde']] . "',
                    '" . $data[$headers['SpEDNPS']] . "','" . $data[$headers['SEC504']] . "', 
                    '" . $data[$headers['IEP']] . "','" . $data[$headers['ParentCityStateZip']] . "', 
                    '" . fixdate($data[$headers['RFEPDateReclassified']]) . "','" . $data[$headers['RFEPStudent']] . "', 
                    '" . $ASAMSchool . "','" . $data[$headers['CAPAAssessed']] . "', 
                    '" . $data[$headers['ResidenceCountyClientCode']] . "','" . $data[$headers['Grade12+']] . "', 
                    '" . $data[$headers['HSPLT']] . "','" . $data[$headers['Hmong']] . "', 
                    '" . $etsstudentnbr . "', '" . $dateadded . "')";
                    if ($debug) printf("demoq=$demoq\n");
                    $sth = $dbh->query($demoq);


                    // English
                    $elaq = "INSERT INTO CAHSEEEnglish 
                    (derkey1, derkey2, SchoolYear, SchoolIdentifier, LocalStudentID, ELAAccomTransfer,
                    ELAAccomDictatedMC, ELAccomWP, ELAAccomDictatedEssay, ELAAccomAssistive, ELAAccomBraille,
                    ELAAccomLargePrint, ELAAccomExtratime, ELAAccomBreaks, ELAAccomBesttime, ELAAccomHome,
                    ELAModDictionary, ELAModSign, ELAModOralPresentation, ELAModWPwithchecks, ELAModEssayDictated, 
                    ELAModAssistivedevice, ELATestForm, ELATestDate, ELAPassed, ELAScaleScore, 
                    ELAReadingWANumberCorrect,ELAReadingWAPercentCorrect, ELAReadingRCNumberCorrect, 
                    ELAReadingRCPercentCorrect, ELAReadingLANumberCorrect, ELAReadingLAPercentCorrect, 
                    ELAWritingWSNumberCorrect, ELAWritingWSPercentCorrect, ELAWritingWCNumberCorrect, 
                    ELAWritingWCPercentCorrect, ELAWritingApplicEssay1, ELASpecVersionAudioCD,
                    ELASpecVersionLargePrint, ELASpecVersionBraille, ELDateEnrolled, TotalELAResponses,
                    ELADirPrimeLang, ELASupBreaks, ELASepTest, ELAGlossWL, ELAScoreCode, ELAPrfrmncLvl,
                    ELANumStuResponses, DateAdded) VALUES 
                    ('" . $derkey1 . "', '" . $derkey2  . "', 
                    '" . $acyear . "', '" . sprintf("%02s", $data[$headers['CountyCode']]) . 
                    sprintf("%05s", $data[$headers['DistrictCode']]) .
                    sprintf("%07s", $data[$headers['SchoolCode']]) . "' , '" . $locstudentid . "', 
                    '" . $data[$headers['ELAAccomTransfer']] . "', 
                    '" . $data[$headers['ELAAccomDictatedMC']] . "', '" . $data[$headers['ELAAccomWP']] . "',  
                    '" . $data[$headers['ELAAccomDictatedEssay']] . "', '" . $data[$headers['ELAAccomAssistive']] . "', 
                    '" . $data[$headers['ELAAccomBraille']] . "', 
                    '" . $data[$headers['ELAAccomLargePrint']] . "', '" . $data[$headers['ELAAccomExtraTime']] . "',  
                    '" . $data[$headers['ELAAccomBreaks']] . "', '" . $data[$headers['ELAAccomBestTime']] . "', 
                    '" . $data[$headers['ELAAccomHome']] . "', 
                    '" . $data[$headers['ELAModDictionary']] . "', '" . $data[$headers['ELAModSign']] . "', 
                    '" . $data[$headers['ELAModOralPresentation']] . "', '" . $data[$headers['ELAModWPWithChecks']] . "', 
                    '" . $data[$headers['ELAModEssayDictated']] . "', 
                    '" . $data[$headers['ELAModAssistiveDevice']] . "', '" . $data[$headers['ELATestForm']] . "', 
                    '" . fixdate($data[$headers['ELATestDate']]) . "', '" . $data[$headers['ELAPassed']] . "', 
                    '" . $data[$headers['ELAScaleScore']] . "', 
                    '" . $data[$headers['ELAReadingWANumberCorrect']] . "', '" . $data[$headers['ELAReadingWAPercentCorrect']] . "', 
                    '" . $data[$headers['ELAReadingRCNumberCorrect']] . "', 
                    '" . $data[$headers['ELAReadingRCPercentCorrect']] . "', '" . $data[$headers['ELAReadingLANumberCorrect']] . "', 
                    '" . $data[$headers['ELAReadingLAPercentCorrect']] . "', 
                    '" . $data[$headers['ELAWritingWSNumberCorrect']] . "', '" . $data[$headers['ELAWritingWSPercentCorrect']] . "', 
                    '" . $data[$headers['ELAWritingWCNumberCorrect']] . "', 
                    '" . $data[$headers['ELAWritingWCPercentCorrect']] . "', '" . $data[$headers['ELAWritingApplicEssay1']] . "', 
                    '" . $data[$headers['ELASpecVersionAudioCD']] . "', 
                    '" . $data[$headers['ELASpecVersionLargePrint']] . "', '" . $data[$headers['ELASpecVersionBraille']] . "', 
                    '" . fixdate($data[$headers['ELDateEnrolled']]) . "', '" . $data[$headers['TotalELAResponses']] . "', 
                    '" . $data[$headers['ELADirPrimeLang']] . "', '" . $data[$headers['ELASupBreaks']] . "', 
                    '" . $data[$headers['ELASepTest']] . "', '" . $data[$headers['ELAGloss/WL']] . "', 
                    '" . $data[$headers['ELAScoreCode']] . "', '" . $data[$headers['ELAPrfrmncLvl']] . "', 
                    '" . $data[$headers['ScoredELAResponses']] . "', '" . $dateadded . "')";
                    if ($debug) printf("elaq=$elaq\n");
                    $sth = $dbh->query($elaq);

                    // Math
                    $mathq = "INSERT INTO CAHSEEMath (
                    derkey1, derkey2, SchoolYear, SchoolIdentifier, LocalStudentID, MathAccomTransfer,
                    MathAccomDictatedMC, MathAccomBraille, MathAccomLargePrint, MathAccomExtratime,
                    MathAccomBreaks, MathAccomBesttime, MathAccomHome, MathModDictionary, MathAccomSign,
                    MathAccommOralPresentation, MathModCalculator, MathModTables,MathModManipulatives,
                    MathModAssistivedevice, MathClassGeneral, MathClassPreAlgebra, MathClassAlgebra1,
                    MathClassGeometry, MathClassAlgebra2, MathClassIntegMath1, MathClassIntegMath2,
                    MathClassIntegMath3, MathClassAdvanced, MathTestForm, MathTestDate, MathPassed,
                    MathScaleScore, MathPSNumberCorrect, MathPSPercentCorrect, MathNSNumberCorrect,
                    MathNSPercentCorrect, MathAFNumberCorrect, MathAFPercentCorrect, MathMGNumberCorrect,
                    MathMGPercentCorrect, MathA1NumberCorrect, MathA1PercentCorrect, MathSpecVersionAudioCD,
                    MathSpecVersionLargePrint, MathSpecVersionBraille, TotalMathResponses, MathDirPrimeLang,
                    MathSupBreaks, MathSepTest, MathGlossWL, MathScoreCode, MathPrfrmncLvl, MathNumStuResponses,
                    DateAdded)
                    VALUES ('" . $derkey1 . "', '" . $derkey2 . "', 
                    '" . $acyear . "', '" . sprintf("%02s", $data[$headers['CountyCode']]) . 
                    sprintf("%05s", $data[$headers['DistrictCode']]) .
                    sprintf("%07s", $data[$headers['SchoolCode']]) . "' , '" . $locstudentid . "', 
                    '" . $data[$headers['MathAccomTransfer']] . "', '" . $data[$headers['MathAccomDictatedMC']] . "', 
                    '" . $data[$headers['MathAccomBraille']] . "', '" . $data[$headers['MathAccomLargePrint']] . "', 
                    '" . $data[$headers['MathAccomExtraTime']] . "', '" . $data[$headers['MathAccomBreaks']] . "', 
                    '" . $data[$headers['MathAccomBestTime']] . "', '" . $data[$headers['MathAccomHome']] . "', 
                    '" . $data[$headers['MathModDictionary']] . "', '" . $data[$headers['MathAccomSign']] . "', 
                    '" . $data[$headers['MathAccomOralPresentation']] . "', '" . $data[$headers['MathModCalculator']] . "', 
                    '" . $data[$headers['MathModTables']] . "','" . $data[$headers['MathModManipulatives']] . "', 
                    '" . $data[$headers['MathModAssistiveDevice']] . "', 
                    '" . $data[$headers['MathClassGeneral']] . "', '" . $data[$headers['MathClassPreAlgebra']] . "', 
                    '" . $data[$headers['MathClassAlgebra1']] . "', '" . $data[$headers['MathClassGeometry']] . "', 
                    '" . $data[$headers['MathClassAlgebra2']] . "', '" . $data[$headers['MathClassIntegMath1']] . "', 
                    '" . $data[$headers['MathClassIntegMath2']] . "', '" . $data[$headers['MathClassIntegMath3']] . "', 
                    '" . $data[$headers['MathClassAdvanced']] . "', '" . $data[$headers['MathTestForm']] . "', 
                    '" . fixdate($data[$headers['MathTestDate']]) . "', '" . $data[$headers['MathPassed']] . "', 
                    '" . $data[$headers['MathScaleScore']] . "', '" . $data[$headers['MathPSNumberCorrect']] . "', 
                    '" . $data[$headers['MathPSPercentCorrect']] . "', '" . $data[$headers['MathNSNumberCorrect']] . "', 
                    '" . $data[$headers['MathNSPercentCorrect']] . "', '" . $data[$headers['MathAFNumberCorrect']] . "', 
                    '" . $data[$headers['MathAFPercentCorrect']] . "', '" . $data[$headers['MathMGNumberCorrect']] . "', 
                    '" . $data[$headers['MathMGPercentCorrect']] . "', '" . $data[$headers['MathA1NumberCorrect']] . "', 
                    '" . $data[$headers['MathA1PercentCorrect']] . "', '" . $data[$headers['MathSpecVersionAudioCD']] . "', 
                    '" . $data[$headers['MathSpecVersionLargePrint']] . "', '" . $data[$headers['MathSpecVersionBraille']] . "', 
                    '" . $data[$headers['TotalMathResponses']] . "', '" . $data[$headers['MathDirPrimeLang']] . "', 
                    '" . $data[$headers['MathSupBreaks']] . "', '" . $data[$headers['MathSepTest']] . "', 
                    '" . $data[$headers['MathGloss/WL']] . "', '" . $data[$headers['MathScoreCode']] . "', 
                    '" . $data[$headers['MathPrfrmncLvl']] . "', '" . $data[$headers['ScoredMathResponses']] . "', 
                    '" . $dateadded . "')";
                    if ($debug) printf("mathq=$mathq\n");
                    $sth = $dbh->query($mathq);
                }
            }
        }
        echo "Loaded " . $cahseecount . " CAHSEE records, droppped " . 
                $cahseedropped . " records.\n";
        logupdate($district, substr($path,-4), "Loaded " . $cahseecount . " CAHSEE records, droppped " . 
                $cahseedropped . " records.");
    }
}

// STAR/CAASPP
//$debug = 1;
$starcount = 0;
if (isset($starfiles[0])) {
    logupdate($district, substr($path,-4), "Found STAR/CAASPP files to process.");
    printf("Found STAR/CAASPP Files.\n");

    printf("Checking for existing STAR/CAASPP records...\n");
    if ($acyear < '1415') {
        $checkq = "SELECT TOP 1 derkey1, LocStudentId 
        FROM K12StarDemProd 
        WHERE substring(School,3,5) = '" . $district . "' AND "
                . "AcYear = '" . substr($path,-4) . "'";
    }
    else
    {
        $checkq = "SELECT TOP 1 derkey1, LocStudentId 
        FROM K12CAASPPDemProd 
        WHERE substring(School,3,5) = '" . $district . "' AND "
                . "AcYear = '" . substr($path,-4) . "'";        
    }
    if ($debug) printf("checkq=$checkq\n");
    $checkr = $dbh->query($checkq);
    $checkr->setFetchMode(PDO::FETCH_ASSOC);
    $check = $checkr->fetch();
    if ($check['derkey1'] <> '' OR $check['LocStudentId'] <> '') {
        printf("Found existing STAR/CAASP student records. Aborting.\n");
        logupdate($district, substr($path,-4), "Found existing STAR/CASSPP student records. Aborting.");
    }
    else
    {
        foreach($starfiles as $starfile) {

            $cahseecount = $cahseedropped = 0;
            printf("No Existing STAR/CAASPP data found. Moving on.\n");

            $fieldsq = "SELECT * FROM K12TestFileLayout WHERE AcYear = '" . $acyear . "'";
            $fieldsr = $dbh->query($fieldsq); 
            $fieldsr->setFetchMode(PDO::FETCH_ASSOC);
            $fields = array();
            while ($fieldrow = $fieldsr->fetch()) {
                $fieldname = trim($fieldrow['fieldname']);
                $fields[$fieldname]['position'] = trim($fieldrow['position']);
                $fields[$fieldname]['length'] = trim($fieldrow['length']);
                $fields[$fieldname]['include'] = trim($fieldrow['include']);
            }
            printf("Loading %s\n", $starfile);
            $first = 1;
            if (($handle = fopen($path . "/" . $starfile, "r")) !== FALSE) {   // Open the file
                while (($data = fgets($handle, 3000)) !== FALSE) {      // Grab a row
                    if ($first) {
                        if (strlen($data) <> linelength($acyear)) {
                            logupdate($district, substr($path,-4), "STAR Line Length of " . 
                                    strlen($data) ." does not match " . $acyear);
                            die("Line Length of " . strlen($data) . " indicates file is not $acyear\n");
                        }
                        $first = 0;
                    }
                    if ($acyear < '1415') {
                        $insq = "INSERT INTO K12STARDemProd (";
                    }
                    else
                    {
                        $insq = "INSERT INTO K12CAASPPDemProd (";
                    }
                    $ins2q = ") VALUES (";
                    $ins3q = ")";
                    $fieldlist = $valuelist = '';
                    foreach($fields as $fieldname => $rest) {
                        if ($rest['include'] == 1 OR $rest['include'] == 3 OR 
                                $rest['include'] == 4 OR $rest['include'] == 6) {
                            $fieldlist .= $fieldname . ", ";
                            $valuelist .= "'" . trim(substr($data, $fields[$fieldname]['position'], 
                                $fields[$fieldname]['length'])) . "',";
                        }
                    }

                    // Get Fields for Keys
                    $fname = cleanname(substr(trim(substr($data, $fields['Fname']['position'], 
                                $fields['Fname']['length'])),0,3));
                    $lname = cleanname(substr(trim(substr($data, $fields['Lname']['position'], 
                                $fields['Lname']['length'])),0,3));
                    $gender = substr(trim(substr($data, $fields['Gender']['position'], 
                                $fields['Gender']['length'])),0,3);
                    $birthdate = trim(substr($data, $fields['doby']['position'], 
                                $fields['doby']['length'])) .
                                trim(substr($data, $fields['dobm']['position'], 
                                $fields['dobm']['length'])) .
                                trim(substr($data, $fields['dobd']['position'], 
                                $fields['dobd']['length']));
                    $locstudentid = trim(substr($data, $fields['LocStudentId']['position'], 
                                $fields['LocStudentId']['length']));
                    $csisnum = trim(substr($data, $fields['CSISNum']['position'], 
                                $fields['CSISNum']['length']));

                    $keyq = "SELECT dbo.Derkey1('" . strtoupper($fname) . "', '" . strtoupper($lname) . "', 
                    '" . $gender . "', '" . $birthdate . "') as derkey1, 
                    dbo.Get1289Encryption('"  . $locstudentid . "', '') as locstudentid, 
                    dbo.Get9812Encryption('"  . $csisnum . "', '') as csisnum";
            //printf("keyq=$keyq\n");
                    $keyr = $dbh->query($keyq);
                    $key = $keyr->fetch();
                    $derkey1 = $key['derkey1'];
                    if ($acyear < '1415') {
                        $derkey2 = $derkey1 . substr(trim(substr($data, $fields['schoolcode']['position'], 
                                    $fields['schoolcode']['length'])),0,6);
                    }
                    else
                    {
                        $derkey2 = $derkey1 . substr(trim(substr($data, $fields['elaschoolcode']['position'], 
                                    $fields['elaschoolcode']['length'])),0,6);

                    }
                    // Suppress Incomplete Derkeys
                    if (strlen($derkey1) < 15) {
                        $derkey1 = '';
                        $derkey2 = '';
                    }
                    $locstudentid = $key['locstudentid'];
                    $csisnum = $key['csisnum'];

                    // Create Demo Record
                    if ($acyear < '1415') {
                        $fieldlist .= " School, Birthdate, AcYear, derkey1, derkey2, 
                                LocStudentId, CSISNum, Fname, Lname, dateadded";
                        $valuelist .= "'" . trim(substr($data, $fields['districtcode']['position'], 
                                $fields['districtcode']['length'])) . 
                                trim(substr($data, $fields['schoolcode']['position'], 
                                $fields['schoolcode']['length'])) .
                                "','" . trim(substr($data, $fields['doby']['position'], 
                                $fields['doby']['length'])) .
                                trim(substr($data, $fields['dobm']['position'], 
                                $fields['dobm']['length'])) .
                                trim(substr($data, $fields['dobd']['position'], 
                                $fields['dobd']['length'])) .
                                "','" . $acyear .
                                "','" . $derkey1 .
                                "','" . $derkey2 . 
                                "','" . $locstudentid .
                                "','" . $csisnum .
                                "','" . $fname .
                                "','" . $lname .
                                "','" . date('Y/m/d H:i:s') . "'";
                    }
                    else
                    {
                        $fieldlist .= " ELASchool, MathSchool, SciSchool, RLASchool, "
                                . "Birthdate, AcYear, derkey1, derkey2, 
                                LocStudentId, CSISNum, Fname, Lname, acdateadded";
                        $valuelist .= "'" . trim(substr($data, $fields['eladistrictcode']['position'], 
                                $fields['eladistrictcode']['length'])) . 
                                trim(substr($data, $fields['elaschoolcode']['position'], 
                                $fields['elaschoolcode']['length'])) .
                                "','" . trim(substr($data, $fields['mathdistrictcode']['position'], 
                                $fields['mathdistrictcode']['length'])) . 
                                trim(substr($data, $fields['mathschoolcode']['position'], 
                                $fields['mathschoolcode']['length'])) .
                                "','" . trim(substr($data, $fields['scidistrictcode']['position'], 
                                $fields['scidistrictcode']['length'])) . 
                                trim(substr($data, $fields['scischoolcode']['position'], 
                                $fields['scischoolcode']['length'])) .
                                "','" . trim(substr($data, $fields['rladistrictcode']['position'], 
                                $fields['rladistrictcode']['length'])) . 
                                trim(substr($data, $fields['rlaschoolcode']['position'], 
                                $fields['rlaschoolcode']['length'])) .
                                "','" . trim(substr($data, $fields['doby']['position'], 
                                $fields['doby']['length'])) .
                                trim(substr($data, $fields['dobm']['position'], 
                                $fields['dobm']['length'])) .
                                trim(substr($data, $fields['dobd']['position'], 
                                $fields['dobd']['length'])) .
                                "','" . $acyear .                
                                "','" . $derkey1 .
                                "','" . $derkey2 .
                                "','" . $locstudentid . 
                                "','" . $csisnum .
                                "','" . $fname .
                                "','" . $lname .
                                "','" . date('Y/m/d H:i:s') . "'";               
                    }
                    $insq .= $fieldlist . $ins2q . $valuelist . $ins3q;
                    $insr = $dbh->query($insq); 
                    $starcount++;
                    if ($debug) printf("insq=$insq\n");

                    // Test Record
                    if ($acyear < '1415') {
                        $insq = "INSERT INTO K12STARCSTProd (";
                    }
                    else
                    {
                        $insq = "INSERT INTO K12CAASPPTestProd (";
                    }
                    $ins2q = ") VALUES (";
                    $ins3q = ")";
                    $fieldlist = $valuelist = '';
                    foreach($fields as $fieldname => $rest) {
                        if ($rest['include'] == 2 OR $rest['include'] == 3 OR 
                                $rest['include'] == 5 OR $rest['include'] == 6) {
                            $fieldlist .= $fieldname . ", ";
                            $valuelist .= "'" . trim(substr($data, $fields[$fieldname]['position'], 
                                $fields[$fieldname]['length'])) . "',";
                        }
                    }

                    if ($acyear < '1415') {
                        $fieldlist .= " School, AcYear, derkey1, derkey2, 
                                LocStudentId, CSISNum, dateadded";
                        $valuelist .= "'" . trim(substr($data, $fields['districtcode']['position'], 
                                $fields['districtcode']['length'])) . 
                                trim(substr($data, $fields['schoolcode']['position'], 
                                $fields['schoolcode']['length'])) .
                                "','" . $acyear .
                                "','" . $derkey1 .
                                "','" . $derkey2 .  
                                "','" . $locstudentid .
                                "','" . $csisnum .
                                "','" . date('Y/m/d H:i:s') . "'";
                    }
                    else
                    {
                        $fieldlist .= " ELASchool, MathSchool, SciSchool, RLASchool, "
                                . "Birthdate, AcYear, derkey1, derkey2, 
                                LocStudentId, CSISNum, Facdateadded";
                        $valuelist .= "'" . trim(substr($data, $fields['eladistrictcode']['position'], 
                                $fields['eladistrictcode']['length'])) . 
                                trim(substr($data, $fields['elaschoolcode']['position'], 
                                $fields['elaschoolcode']['length'])) .
                                "','" . trim(substr($data, $fields['mathdistrictcode']['position'], 
                                $fields['mathdistrictcode']['length'])) . 
                                trim(substr($data, $fields['mathschoolcode']['position'], 
                                $fields['mathschoolcode']['length'])) .
                                "','" . trim(substr($data, $fields['scidistrictcode']['position'], 
                                $fields['scidistrictcode']['length'])) . 
                                trim(substr($data, $fields['scischoolcode']['position'], 
                                $fields['scischoolcode']['length'])) .
                                "','" . trim(substr($data, $fields['rladistrictcode']['position'], 
                                $fields['rladistrictcode']['length'])) . 
                                trim(substr($data, $fields['rlaschoolcode']['position'], 
                                $fields['rlaschoolcode']['length'])) .
                                "','" . trim(substr($data, $fields['doby']['position'], 
                                $fields['doby']['length'])) .
                                trim(substr($data, $fields['dobm']['position'], 
                                $fields['dobm']['length'])) .
                                trim(substr($data, $fields['dobd']['position'], 
                                $fields['dobd']['length'])) .
                                "','" . $acyear .                
                                "','" . $derkey1 .
                                "','" . $derkey2 . 
                                "','" . $locstudentid . 
                                "','" . $csisnum .
                                "','" . date('Y/m/d H:i:s') . "'";               
                    }
                    $insq .= $fieldlist . $ins2q . $valuelist . $ins3q;
                    if ($debug) printf("insq=$insq\n");
                    $insr = $dbh->query($insq); 
                }
            }
        }
    }
    printf("Loaded %d STAR/CASSP records.\n", $starcount);
    logupdate($district, $year, "Loaded " . $starcount . " STAR/CAASP records.\n");
}
if (isset($handle)) {
    if (get_resource_type($handle) == 'stream')
    fclose($handle);
}

printf("Archiving Directory\n");
$pathparts = split("/", $path);
if ($debug) print_r($pathparts);
if ($pathparts[3] == 'FileDrop') {
    // Running From /mnt/nas/FileDrop
    chdir("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3]);
    if (!file_exists("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
        $pathparts[4] . "/processed")) {
        mkdir("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
        $pathparts[4] . "/processed");
        printf("Created folder:". "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . 
        $pathparts[3] . "/" .
        $pathparts[4] . "/processed\n");
    }
    if (!file_exists("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
        $pathparts[4] . "/processed/" . $pathparts[6])) {
        // No existing directory
        rename($path, "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
            $pathparts[4] . "/processed/" . $pathparts[6]);
        printf("Renaming $path to " . "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . 
            $pathparts[3] . "/" . 
            $pathparts[4] . "/processed/" . $pathparts[6] . "\n");  
    }
    else
    {
        // Existing directory
/*
        rename($path . "/*", "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
            $pathparts[4] . "/processed/" . $pathparts[6] . "/");
*/
        printf("Renaming $path/*  to " . "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . 
            $pathparts[3] . "/" . 
            $pathparts[4] . "/processed/" . $pathparts[6] . "/\n");   
               
          $files = scandir($path . "/");
          foreach($files as $fname) {
              if($fname != '.' && $fname != '..') {
                  rename($path . "/" . $fname, "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
            $pathparts[4] . "/processed/" . $pathparts[6] . "/" . $fname);
              }
          }
        rmdir("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
            $pathparts[4] . "/toprocess/" . $pathparts[6] . "/");
    }
}
else
{
    chdir("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
        $pathparts[4]);
    if (!file_exists("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
        $pathparts[4] . "/" .  $pathparts[5] . "/processed")) {
        mkdir("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
        $pathparts[4] . "/" . $pathparts[5] . "/processed");
        printf("Created folder:". "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
        $pathparts[4] . "/" .  $pathparts[5] . "/processed\n");
    }
    
    if (!file_exists("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
        $pathparts[4] . "/" . $pathparts[5] . "/processed/" . $pathparts[7])) {
        // No existing directory
        rename($path, "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
            $pathparts[4] . "/" . $pathparts[5] . "/processed/" . $pathparts[7] . "/");
        printf("Renaming $path to " . "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
            $pathparts[4] . "/" . $pathparts[5] . "/processed/" . $pathparts[7] . "/\n");
    }
    else
    {
        // Existing directory
        $files = scandir($path . "/");
        foreach($files as $fname) {
            if($fname != '.' && $fname != '..') {
                rename($path . "/" . $fname, "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
                $pathparts[4] . "/" . $pathparts[5] . "/processed/" . $pathparts[7] . "/" . $fname);
            }
        }
        rmdir("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
            $pathparts[4] . "/" . $pathparts[5] . "/toprocess/" . $pathparts[7] . "/");
        
 //       rename($path . "/*", "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
 //           $pathparts[4] . "/" . $pathparts[5] . "/processed/" . $pathparts[7] . "/");
        printf("Renaming $path to " . "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
            $pathparts[4] . "/" . $pathparts[5] . "/processed/" . $pathparts[7] . "\n");
    }
}
logupdate($district, $year, "Archived folder.\n");


function logupdate($district, $year, $msg) {
    try {
	$dsna = 'dblib:dbname=calpass;host=prodatsql03';
//	$dsna = 'dblib:dbname=calpass;host=prodatsql01';
	$user = 'CALPASS\\zz_gua_d1_admin';
	$password = 'd2001O!#%';
	$dbh = new PDO($dsna, $user, $password);
    }
    catch(PDOException $e) {  
             echo $e->getMessage();  
    }
    $date = date('Y/m/d H:i:s');
    $processid = getmypid();
    
    switch(strlen($district)) {
        case 5:
            $distq = "SELECT OrganizationId, OrganizationCode, OrganizationName "
                . "FROM Organization "
                . "WHERE SUBSTRING(OrganizationCode,3,5) = '" . $district . "' AND "
                . "RIGHT(OrganizationCode,7)='0000000'";
            break;
        
        case 6:
            $distq = "SELECT OrganizationId, OrganizationCode, OrganizationName "
                . "FROM Organization "
                . "WHERE OrganizationId = '" . $district . "'";
            break;
        
        case 7:
            $distq = "SELECT OrganizationId, OrganizationCode, OrganizationName "
                . "FROM Organization "
                . "WHERE LEFT(OrganizationCode,7) = '" . $district . "' AND "
                . "RIGHT(OrganizationCode,7)='0000000'";
            break;
    }
    $distr = $dbh->query($distq);
    $distr->setFetchMode(PDO::FETCH_ASSOC);
    $dist = $distr->fetch();
    
    $insq = "INSERT INTO LoaderLog"
    . "(orgid, orgcode, orgname, acyear, processid, date, message) "
    . "VALUES "
    . "('" . $dist['OrganizationId'] . "', '" . 
    substr($dist['OrganizationCode'],0,7) . "', '" . 
    $dist['OrganizationName'] . "', '" . $year . "', '" . $processid . "', '" . 
    $date . "', '" . 
    $msg . "')";
    $insr = $dbh->query($insq);
//    printf("insq=$insq\n");
    return;
}
function fixdate($date) {
    if ($date <> 'U' AND $date <> '') {
        $datework = sprintf("%08s", $date);
        $year = substr($datework,-4);
        $monthday = substr($datework,0,4);
        $date = $year . $monthday;
    }
    return($date);
}

function linelength($year) {
    switch($year) {
        case '1314':
        case '1213':
        case '0910':
            $length = 412;
        break;
    
        case '1112':
            $length = 413; // one file had a problem with this
//            $length = 412;
        break;
    
        case '1011':
        case '0809':
            $length = 419;
        break;
    }
    return($length);
}

/*
 * Removes special characters from first and last name
 */
function cleanname($name) {
    $unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
    'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
    'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
    'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
    'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ñ' => 'n');

//        $lname = strtr( $lname, $unwanted_array );
    $enye = chr(0xf1);
    $name = strtr( $name, $enye, "n" );
    $eyye = chr(0xfd);
    $name = strtr( $name, $eyye, "y" );
    $eaccent = chr(0xe9);
    $name = strtr( $name, $eaccent, "e" );
    $iaccent = chr(0xed);
    $name = strtr( $name, $iaccent, "i" );
    $weirdone = chr(0xb1);
    $name = strtr( $name, $weirdone, "n" );
    $gujarati = chr(0xae1);
    $name = strtr($name, $gujarati, "a" );
    $gujarati2 = chr(0xafc);
    $name = strtr($name, $gujarati2, "a" );
    $name = str_replace("'", "", $name);
    $dha = chr(0xd27);
    $name = strtr($name, $dha, "a" );
    $apos = chr(0x27);
    $name = str_replace($apos, " ",$name);
     
    return($name);
}
?>
