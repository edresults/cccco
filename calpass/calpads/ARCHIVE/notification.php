<?php

/* 
 * Send notification to the Outreach team with data loaded
 */

try {
	$dsna = 'dblib:dbname=calpass;host=prodatsql03';
	$user = 'CALPASS\\zz_gua_d1_admin';
	$password = 'd2001O!#%';
	$dbh = new PDO($dsna, $user, $password);
}
catch(PDOException $e) {  
	 echo $e->getMessage();  
}
$dbh->exec('SET QUOTED_IDENTIFIER ON');
$dbh->exec('SET ANSI_WARNINGS ON');
$dbh->exec('SET ANSI_PADDING ON');
$dbh->exec('SET ANSI_NULLS ON');
$dbh->exec('SET CONCAT_NULL_YIELDS_NULL ON');

$today = date('Y/m/d');
//$today = '2015-10-08';
$msg = array();

$email = "\nGreetings,\n\n"
        . "Here is the list of data loaded for " . $today 
        . ". If you have any questions, contact the help desk.\n";

$rptq = "SELECT * FROM LoaderLog WHERE date >= '" . $today . "'  AND orgcode <> '' "
        . "ORDER BY orgname, AcYear, date";
$rptr = $dbh->query($rptq);
$rptr->setFetchMode(PDO::FETCH_ASSOC);
while ($rpt = $rptr->fetch()) {
    $orgname = $rpt['orgname'];
    $acyear = $rpt['acyear'];
    $msg[$orgname][$acyear][] = $rpt['message'];
}
//print_r($msg);
foreach ($msg as $orgname => $rest) {
//    print("\n" . $orgname . ":\n");
    $email .= "\n" . $orgname . ":\n";
    foreach($rest as $acyear => $messages) {
//        print($acyear . ": ");
        $email .= $acyear . ": ";
        //print_r($messages);
        foreach($messages as $message) {  
//            printf("message=" . substr(chop($message),-19));
            if ($message == 'Aborted: Could not identify ethnicity type.')
                $status[0] = $message . "\n";
            if (substr($message,0,12) == 'Missing CRSC')
                $status[0] = "Missing CRSC file, no courses loaded.\n";
            if (substr($message,0,12) == 'Missing SCSC')
                $status[0] = "Missing SCSC file, no courses loaded.\n";
            if (substr($message,0,12) == 'Missing SELA')
                $status[0] = "Missing SELA file, ignored.\n";
            if (substr($message,0,12) == 'Missing SPRG')
                $status[0] = "Missing SPRG file, ignored.\n";
            if (substr($message,0,12) == 'Missing SCTE')
                $status[0] = "Missing SCTE file, ignored.\n";
            if (substr($message,0,15) == 'Loaded Data: S:')
                $status[0] = $message . "\n";
            if (substr($message,0,12) == 'Missing SINF')
                $status[0] = "Missing SINF file, aborted.\n";
            if ($message == 'Found existing records. Skipping upload of Student, Course and Award files.')
                $status[0] = "Basic data already present, skipped.\n";
            if (substr(chop($message),-19) == 'STAR/CAASP records.')
                $status[1] = $message;
            if ($message == "Found existing STAR/CASSPP student records. Aborting.\n") 
                $tatus[1] = 'STAR/CASSP already present, skipped.';
            $pos = stripos(" CAHSEE records, droppped", $message);  
            if ($pos !== false) {
                $pos2 = explode(',',$message);
                $status[2] = $pos2[0] . ".";
            }
            if ($message == 'Found existing CAHSEE student records. Aborting.')
                $status[2] = "Found existing CAHSEE records, skipped.\n";
        }
//        if (isset($status[0]) ? $status[0] : '' <> '') print($status[0]);
//        if (isset($status[1]) ? $status[1] : '' <> '') print($status[1]);
//        if (isset($status[2]) ? $status[2] : '' <> '') print($status[2]);
        if (isset($status[0]) ? $status[0] : '' <> '') $email .= $status[0];
        if (isset($status[1]) ? $status[1] : '' <> '') $email .= $status[1];
        if (isset($status[2]) ? $status[2] : '' <> '') $email .= $status[2];
        $status = array();
    }
}
$email .= "\nThanks,\nYour Server";
//echo $email;
//$to      = 'dave@friendsofdave.org, dave@edresults.org';
$to      = 'outreach@edresults.org,dave@edresults.org,angie@edresults.org';
$subject = 'Data Loading Report for ' . $today;
$headers = 'From: help@calpassplus.org' . "\r\n" .
    'Reply-To: help@calpassplus.org' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

$lines = substr_count( $email, "\n" );
if ($lines > 6) 
    mail($to, $subject, $email, $headers);
?>