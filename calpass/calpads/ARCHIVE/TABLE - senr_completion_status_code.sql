USE calpass;

GO

IF (object_id('calpads.senr_completion_status_code') is not null)
	BEGIN
		DROP TABLE calpads.senr_completion_status_code;
	END;

GO

CREATE TABLE
	calpads.senr_completion_status_code
	(
		code char(3) NOT NULL,
		name nvarchar(255) NULL,
		description nvarchar(4000) NULL
	);

GO

ALTER TABLE
	calpads.senr_completion_status_code
ADD CONSTRAINT
	pk_senr_completion_status_code__code
PRIMARY KEY CLUSTERED 
	(
		code
	);

/* DML */

GO

INSERT INTO
	calpads.senr_completion_status_code
	(
		code,
		name,
		description
	)
VALUES
	('100','Graduated, standard HS diploma','The student left school after meeting all state and local high school graduation requirements and received a standard high school diploma, or a student in foster care who met the state and local graduation requirements of a district they previously attended, as specified in Education Code Section 51225.3.'),
	('104','Completed all local and state graduation requirements, failed CAHSEE','Student who was required to take the California High school Exit Exam (CAHSEE) left school after meeting all other state and local high school graduation requirements, but without passing the California High School Exit Exam (CAHSEE). The student did not receive a standard high school diploma and there is no evidence that the student is in an academic program leading toward a high school diploma or its equivalent.'),
	('106','Grad, CAHSEE mods & waiver','The student met all state and local graduation requirements, passing the California High School Exit Exam with a modified passing score and obtained a waiver under Education Code 60851(c)(1). Education Code 60851(c)(1) waives the requirement to successfully pass the exit examination as a condition of receiving a diploma of graduation or a condition of graduation from high school special education students.'),
	('108','Grad, CAHSEE exempt','108 - Grad, CAHSEE Exemption: The student left school after meeting all state and local high school graduation requirements by obtaining an an exemption from passing the California High School Exit Exam per Education Code 60852.3(a). Education Code60852.3(a) states: Notwithstanding any other provision of law, commencing with the 2009-10 school year, an eligible pupil with a disability is not required to pass the high school exit examination established pursuant to Section 60850 as a condition of receiving a diploma of graduation or as a condition of graduation from high school.'),
	('120','Special Education certificate of completion','A student with exceptional needs (having an individualized education program [IEP]) left school after receiving a certificate or document of educational achievement or completion meeting the requirements of Education Code Section 56390.'),
	('250','Adult Ed High School Diploma','The student left school and the district has acceptable documentation of the student having received a high school diploma through an adult basic education program.'),
	('320','Received a High School Equivalency Certificate (and no standard HS diploma)','Student left school and the district has acceptable documentation that the student received a High School Equivalency Certificate by passing one or more of the following exams: the General Educational Development (GED) exam, the Test Assessing Secondary Completion (TASC) exam, or the High School Equivalency Test (HiSet).'),
	('330','Passed CHSPE (and no standard HS diploma)','Student left after passing the California High School Proficiency Exam (CHSPE), and the district has acceptable documentation.'),
	('360','Completed grade 12 without completing graduation requirements, not grad','Student completed grade 12 or exceeded the maximum age for high school attendance but did not meet the state and/or local high school graduation requirements, and there is no evidence that the student is in an academic program leading toward a high school diploma or its equivalent. This does not include students who did not graduate because of failure to pass the California High School Exit Exam.'),
	('480','Promoted (matriculated)','The student completed the highest grade level offered  at a school (excluding high school completion), left the school, and was expected to attend another California public school.');