USE calpass;

GO

IF (object_id('calpads.senr_birthstate') is not null)
	BEGIN
		DROP TABLE calpads.senr_birthstate;
	END;

GO

CREATE TABLE
	calpads.senr_birthstate
	(
		code varchar(6) NOT NULL,
		name nvarchar(255) NULL,
		description nvarchar(255) NULL
	);

GO

ALTER TABLE
	calpads.senr_birthstate
ADD CONSTRAINT
	pk_senr_birthstate__code
PRIMARY KEY CLUSTERED 
	(
		code
	);

/* DML */

GO

INSERT INTO
	calpads.senr_birthstate
	(
		code,
		name,
		description
	)
VALUES
	('CA-AB','Alberta','The Canadian province of Alberta.'),
	('CA-BC','British Columbia','The Canadian province of British Columbia.'),
	('CA-MB','Manitoba','The Canadian province of Manitoba.'),
	('CA-NB','New Brunswick','The Canadian province of New Brunswick.'),
	('CA-NL','Newfoundland and Labrador','The Canadian province of Newfoundland and Labrador.'),
	('CA-NS','Nova Scotia','The Canadian province of Nova Scotia.'),
	('CA-NT','Northwest Territories','The Canadian province of Northwest Territories.'),
	('CA-NU','Nunavut','The Canadian province of Nunavut.'),
	('CA-ON','Ontario','The Canadian province of Ontario.'),
	('CA-PE','Prince Edward Island','The Canadian province of Prince Edward Island.'),
	('CA-QC','Quebec','The Canadian province of Quebec.'),
	('CA-SK','Saskatchewan','The Canadian province of Saskatchewan.'),
	('CA-YT','Yukon Territory','The Canadian province of Yukon Territory.'),
	('MX-AGU','Aguascalientes','The Mexican state of Aguascalientes'),
	('MX-BCN','Baja California','The Mexican state of Baja California'),
	('MX-BCS','Baja California Sur','The Mexican state of Baja California Sur'),
	('MX-CAM','Campeche','The Mexican state of Campeche'),
	('MX-CHH','Chihuahua','The Mexican state of Chihuahua'),
	('MX-CHP','Chiapas','The Mexican state of Chiapas'),
	('MX-COA','Coahuila','The Mexican state of Coahuila'),
	('MX-COL','Colima','The Mexican state of Colima'),
	('MX-DIF','Distrito Federal','The Mexican state of Distrito Federal'),
	('MX-DUR','Durango','The Mexican state of Durango'),
	('MX-GRO','Guerrero','The Mexican state of Guerrero'),
	('MX-GUA','Guanajuato','The Mexican state of Guanajuato'),
	('MX-HID','Hidalgo','The Mexican state of Hidalgo'),
	('MX-JAL','Jalisco','The Mexican state of Jalisco'),
	('MX-MEX','Mexico','The Mexican state of Mexico'),
	('MX-MIC','Michoacan','The Mexican state of Michoacan'),
	('MX-MOR','Morelos','The Mexican state of Morelos'),
	('MX-NAY','Nayarit','The Mexican state of Nayarit'),
	('MX-NLE','Nuevo Leon','The Mexican state of Nuevo Leon'),
	('MX-OAX','Oaxaca','The Mexican state of Oaxaca'),
	('MX-PUE','Puebla','The Mexican state of Puebla'),
	('MX-QUE','QuerTtaro','The Mexican state of Queretaro'),
	('MX-ROO','Quintana Roo','The Mexican state of Quintana Roo'),
	('MX-SIN','Sinaloa','The Mexican state of Sinaloa'),
	('MX-SLP','San Luis Potosi','The Mexican state of San Luis Potosf'),
	('MX-SON','Sonora','The Mexican state of Sonora'),
	('MX-TAB','Tabasco','The Mexican state of Tabasco'),
	('MX-TAM','Tamaulipas','The Mexican state of Tamaulipas'),
	('MX-TLA','Tlaxcala','The Mexican state of Tlaxcala'),
	('MX-VER','Veracruz','The Mexican state of Veracruz'),
	('MX-YUC','Yucatan','The Mexican state of Yucatan'),
	('MX-ZAC','Zacatecas','The Mexican state of Zacatecas'),
	('US-AA','Armed Forces Americas','The American state or territory of Armed Forces Americas.'),
	('US-AE','Armed Forces Europe, Middle East, & Canada','The American state or territory of Armed Forces Europe, Middle East, & Canada.'),
	('US-AK','Alaska','The American state or territory of Alaska.'),
	('US-AL','Alabama','The American state or territory of Alabama.'),
	('US-AP','Armed Forces Pacific','The American state or territory of Armed Forces Pacific.'),
	('US-AR','Arkansas','The American state or territory of Arkansas.'),
	('US-AS','American Samoa','The American state or territory of American Samoa.'),
	('US-AZ','Arizona','The American state or territory of Arizona.'),
	('US-CA','California','The American state or territory of California.'),
	('US-CO','Colorado','The American state or territory of Colorado.'),
	('US-CT','Connecticut','The American state or territory of Connecticut.'),
	('US-DC','District of Columbia','The American state or territory of District of Columbia.'),
	('US-DE','Delaware','The American state or territory of Delaware.'),
	('US-FL','Florida','The American state or territory of Florida.'),
	('US-FM','Federated States of Micronesia','The American state or territory of Federated States of Micronesia.'),
	('US-GA','Georgia','The American state or territory of Georgia.'),
	('US-GU','Guam','The American state or territory of Guam.'),
	('US-HI','Hawaii','The American state or territory of Hawaii.'),
	('US-IA','Iowa','The American state or territory of Iowa.'),
	('US-ID','Idaho','The American state or territory of Idaho.'),
	('US-IL','Illinois','The American state or territory of Illinois.'),
	('US-IN','Indiana','The American state or territory of Indiana.'),
	('US-KS','Kansas','The American state or territory of Kansas.'),
	('US-KY','Kentucky','The American state or territory of Kentucky.'),
	('US-LA','Louisiana','The American state or territory of Louisiana.'),
	('US-MA','Massachusetts','The American state or territory of Massachusetts.'),
	('US-MD','Maryland','The American state or territory of Maryland.'),
	('US-ME','Maine','The American state or territory of Maine.'),
	('US-MH','Marshall Islands','The American state or territory of Marshall Islands.'),
	('US-MI','Michigan','The American state or territory of Michigan.'),
	('US-MN','Minnesota','The American state or territory of Minnesota.'),
	('US-MO','Missouri','The American state or territory of Missouri.'),
	('US-MP','Northern Mariana Islands','The American state or territory of Northern Mariana Islands.'),
	('US-MS','Mississippi','The American state or territory of Mississippi.'),
	('US-MT','Montana','The American state or territory of Montana.'),
	('US-NC','North Carolina','The American state or territory of North Carolina.'),
	('US-ND','North Dakota','The American state or territory of North Dakota.'),
	('US-NE','Nebraska','The American state or territory of Nebraska.'),
	('US-NH','New Hampshire','The American state or territory of New Hampshire.'),
	('US-NJ','New Jersey','The American state or territory of New Jersey.'),
	('US-NM','New Mexico','The American state or territory of New Mexico.'),
	('US-NV','Nevada','The American state or territory of Nevada.'),
	('US-NY','New York','The American state or territory of New York.'),
	('US-OH','Ohio','The American state or territory of Ohio.'),
	('US-OK','Oklahoma','The American state or territory of Oklahoma.'),
	('US-OR','Oregon','The American state or territory of Oregon.'),
	('US-PA','Pennsylvania','The American state or territory of Pennsylvania.'),
	('US-PR','Puerto Rico','The American state or territory of Puerto Rico.'),
	('US-PW','Palau','The American state or territory of Palau.'),
	('US-RI','Rhode Island','The American state or territory of Rhode Island.'),
	('US-SC','South Carolina','The American state or territory of South Carolina.'),
	('US-SD','South Dakota','The American state or territory of South Dakota.'),
	('US-TN','Tennessee','The American state or territory of Tennessee.'),
	('US-TX','Texas','The American state or territory of Texas.'),
	('US-UT','Utah','The American state or territory of Utah.'),
	('US-VA','Virginia','The American state or territory of Virginia.'),
	('US-VI','Virgin Islands','The American state or territory of Virgin Islands.'),
	('US-VT','Vermont','The American state or territory of Vermont.'),
	('US-WA','Washington','The American state or territory of Washington.'),
	('US-WI','Wisconsin','The American state or territory of Wisconsin.'),
	('US-WV','West Virginia','The American state or territory of West Virginia.'),
	('US-WY','Wyoming','The American state or territory of Wyoming.');