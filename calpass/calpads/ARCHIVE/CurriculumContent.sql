USE calpass;

GO

IF (object_id('calpads.CurriculumContent') is not null)
	BEGIN
		DROP TABLE calpads.CurriculumContent;
	END;

GO

CREATE TABLE
	calpads.CurriculumContent
	(
		CurriculumCode char(4) not null,
		ContentCode varchar(25) not null,
		IsPrimary bit not null
	);

GO

ALTER TABLE
	calpads.CurriculumContent
ADD CONSTRAINT
	pk_CurriculumContent
PRIMARY KEY CLUSTERED
	(
		CurriculumCode,
		ContentCode
	);

ALTER TABLE
	calpads.CurriculumContent
ADD CONSTRAINT
	fk_CurriculumContent__Curriculum
FOREIGN KEY
	(
		CurriculumCode
	)
REFERENCES
	calpads.Curriculum
	(
		CurriculumCode
	);

ALTER TABLE
	calpads.CurriculumContent
ADD CONSTRAINT
	fk_CurriculumContent__Content
FOREIGN KEY
	(
		ContentCode
	)
REFERENCES
	calpads.Content
	(
		Code
	);

GO

INSERT INTO
	calpads.CurriculumContent
	(
		CurriculumCode,
		ContentCode,
		IsPrimary
	)
VALUES
	/* Esl */
	('2110','Esl',1),
	/* English */
	('2100','English',1),
	('2101','English',1),
	('2102','Remedial',1),
	('2105','English',1),
	('2106','English',1),
	('2107','English',1),
	('2108','English',1),
	('2109','English',1),
	('2110','English',1),
	('2111','English',1),
	('2112','English',1),
	('2113','Expository',1),
	('2114','Expository',1),
	('2115','English',1),
	('2116','English',1),
	('2117','English',1),
	('2118','Expository',1),
	('2120','English',1),
	('2130','English09',1),
	('2131','English10',1),
	('2132','English11',1),
	('2133','English12',1),
	('2160','English12',1),
	('2161','English',1),
	('2170','LanguageAP',1),
	('2171','LiteratureAP',1),
	('2173','English',1),
	('2174','English',1),
	('2175','English',1),
	('2190','English',1),
	('2198','English',1),
	/* Mathematics */
	('2400','Arithmetic',1),
	('2401','Arithmetic',1),
	('2402','Arithmetic',1),
	('2403','AlgebraI',1),
	('2404','AlgebraII',1),
	('2407','Trigonometry',1),
	('2408','AlgebraII',1),
	('2409','Geometry',1),
	('2410','Statistics',1),
	('2411','AlgebraII',1),
	('2412','AlgebraII',1),
	('2413','Geometry',1),
	('2414','PreCalculus',1),
	('2415','CalculusI',1),
	('2417','AlgebraII',1),
	('2420','PreAlgebra',1),
	('2421','PreAlgebra',1),
	('2422','PreCalculus',1),
	('2424','PreAlgebra',1),
	('2425','AlgebraI',1),
	('2426','Geometry',1),
	('2427','AlgebraII',1),
	('2428','AlgebraI',1),
	('2429','AlgebraI',1),
	('2430','AlgebraII',1),
	('2430','Statistics',0),
	('2433','PreAlgebra',1),
	('2437','AlgebraI',1),
	('2438','AlgebraII',1),
	('2439','Geometry',1),
	('2440','AlgebraI',1),
	('2441','Geometry',1),
	('2442','AlgebraII',1),
	('2443','AlgebraII',1),
	('2443','Statistics',0),
	('2444','PreCalculus',1),
	('2445','Statistics',1),
	('2446','AlgebraI',1),
	('2447','AlgebraI',1),
	('2460','PreCalculus',1),
	('2461','PreCalculus',1),
	('2462','CalculusI',1),
	('2463','PreCalculus',1),
	('2464','Geometry',1),
	('2467','AlgebraI',1),
	('2468','Geometry',1),
	('2469','Geometry',1),
	('2473','AlgebraII',1),
	('2480','CalculusIAP',1),
	('2481','CalculusIIAP',1),
	('2483','StatisticsAP',1),
	('2485','AlgebraI',1);