USE calpass;

GO

IF (object_id('calpads.senr_enroll_status_code') is not null)
	BEGIN
		DROP TABLE calpads.senr_enroll_status_code;
	END;

GO

CREATE TABLE
	calpads.senr_enroll_status_code
	(
		code char(2) NOT NULL,
		name nvarchar(255) NULL,
		description nvarchar(4000) NULL
	);

GO

ALTER TABLE
	calpads.senr_enroll_status_code
ADD CONSTRAINT
	pk_senr_enroll_status_code__code
PRIMARY KEY CLUSTERED 
	(
		code
	);

/* DML */

GO

INSERT INTO
	calpads.senr_enroll_status_code
	(
		code,
		name,
		description
	)
VALUES
	('10','Primary enrollment','The student’s name appears on a register, roll, or list, the student is currently attending (or intends to attend) the educational service institution (ESI), or is responsible for the students instruction (students attending NPS schools).'),
	('20','Secondary enrollment','The student’s name appears on a register, roll, or list and the student is currently attending the educational service institution concurrently with their PRIMARY educational service institution'),
	('30','Short term enrollment','The student’s name appears on a register, roll, or list, the student is currently attending the educational service institution, and receives or will receive the majority of their instruction at the institution for less than 30 days. (Use allowed only by specific ESIs)'),
	('40','Receiving specialized services only','The student’s name appears on a register, roll, or list solely for the purposes of receiving some sort of specialized instruction, e.g., CAHSEE Intensive Instruction. (Use for non-ADA students only).'),
	('50','Summer or Intersession','The student’s name appears on a register, roll, or list for a summer or intersession term but is not primarily enrolled within the reporting local educational agency.');