USE calpass;

GO

IF (object_id('calpads.p_CalpadsYearCodeEval') is not null)
	BEGIN
		DROP PROCEDURE calpads.p_CalpadsYearCodeEval;
	END;

GO

CREATE PROCEDURE
	calpads.p_CalpadsYearCodeEval
	(
		@TableName nvarchar(255),
		@YearCode char(9) out
	)
AS

	SET NOCOUNT ON;

DECLARE
	@msg nvarchar(2048),
	@sql nvarchar(max) = N'';

BEGIN
	-- validate table
	IF (0 = (SELECT count(*) FROM calpass.sys.tables WHERE name = @TableName))
		BEGIN
			SET @msg = 'Invalid object name ''' + @TableName + '''.';
			THROW 70099, @msg, 1;
		END;
	-- populate year counts
	SET @sql = N'
		DECLARE @Threshold int = 50;
		DECLARE
			@tab
		AS TABLE
			(
				year_code char(9),
				numerator int,
				denominator int,
				pct decimal(5,2)
			);
	
		INSERT INTO
			@tab
			(
				year_code,
				numerator,
				denominator,
				pct
			)
		SELECT
			year_code,
			numerator = count(*),
			denominator = sum(count(*)) over(),
			pct = convert(decimal(5,2), round(100.00 * count(*) / sum(count(*)) over(), 2))
		FROM
			calpass.calpads.[' + @TableName + ']
		WHERE
			year_code is not null
		GROUP BY
			year_code;

		WHILE (@YearCode is null)
		BEGIN
			WITH
				a
			AS
				(
					SELECT TOP 2
						year_code
					FROM
						@tab
					WHERE
						pct > @Threshold
					ORDER BY
						numerator desc
				)
			SELECT
				@YearCode = max(year_code)
			FROM
				a
			HAVING
				(
					@Threshold <= 0
					or
					count(*) >= 2
				);

			IF (@YearCode is null)
				BEGIN
					SET @Threshold -= 2;
				END;
		END;';
	-- set 
	EXECUTE sp_executesql
		@sql,
		N'@YearCode char(9) out',
		@YearCode = @YearCode out;
END;