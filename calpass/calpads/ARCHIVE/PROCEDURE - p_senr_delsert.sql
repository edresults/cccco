USE calpass;

GO

IF (object_id('calpads.p_Senr_delsert') is not null)
	BEGIN
		DROP PROCEDURE calpads.p_Senr_delsert;
	END;

GO

CREATE PROCEDURE
	calpads.p_Senr_delsert
	(
		@SubmissionFileId int,
		@StageSchemaTableName nvarchar(517)
	)
AS

DECLARE
	@Message nvarchar(2048),
	@Validate bit = 0,
	@Sql nvarchar(max),
	@Needle varchar(255) = char(13) + char(10) + char(9) + char(9),
	@Replace varchar(255) = char(13) + char(10),
	@Query nvarchar(4000) = N'SELECT * FROM ' + @StageSchemaTableName + ';',
	@ColumnArray nvarchar(max) = N'',
	@YearCode char(9),
	@YearTrailingFile smallint,
	@YearTrailingThreshold smallint = 2014;

BEGIN
	-- validate inputs
	SELECT
		@Validate = 1
	FROM
		sys.tables
	WHERE
		object_id = object_id(@StageSchemaTableName);
	-- process validation
	IF (@Validate = 0)
		BEGIN
			SET @Message = N'Table not found';
			THROW 70099, @Message, 1;
		END;
	-- get year of file
	SET @Sql = replace(N'
		DECLARE @Threshold int = 50;
		DECLARE
			@tab
		AS TABLE
			(
				year_code char(9),
				numerator int,
				denominator int,
				pct decimal(5,2)
			);
	
		INSERT INTO
			@tab
			(
				year_code,
				numerator,
				denominator,
				pct
			)
		SELECT
			year_code,
			numerator = count(*),
			denominator = sum(count(*)) over(),
			pct = convert(decimal(5,2), round(100.00 * count(*) / sum(count(*)) over(), 2))
		FROM
			' + @StageSchemaTableName + '
		WHERE
			year_code is not null
		GROUP BY
			year_code;

		WHILE (@YearCode is null)
		BEGIN
			WITH
				a
			AS
				(
					SELECT TOP 2
						year_code
					FROM
						@tab
					WHERE
						pct > @Threshold
					ORDER BY
						numerator desc
				)
			SELECT
				@YearCode = max(year_code)
			FROM
				a
			HAVING
				(
					@Threshold <= 0
					or
					count(*) >= 2
				);

			IF (@YearCode is null)
				BEGIN
					SET @Threshold -= 2;
				END;
		END;', @Needle, @Replace);
	-- set YearCode
	EXECUTE sp_executesql
		@Sql,
		N'@YearCode char(9) out',
		@YearCode = @YearCode out;
	-- convert YearCode
	SELECT
		@YearTrailingFile = YearTrailing
	FROM
		calpads.Year
	WHERE
		YearCode = @YearCode;
	-- process YearCode
	IF (@YearTrailingFile < @YearTrailingThreshold)
		-- year must be updated and grades must be aligned
		BEGIN
			-- realign grade_level_code for students without good data
			-- assume valid grade_level_code for invalid year_code
			-- i.e., increase grade_level_code then update year_code
			SET @Sql = replace(N'
				UPDATE
					b
				WITH
					(
						TabLockX
					)
				SET
					b.grade_level_code = f.code,
					b.year_code = @YearCode
				FROM
					' + @StageSchemaTableName + ' b
					inner join
					calpads.Year y
						on b.enroll_exit_date >= y.IsoDateStart
						and b.enroll_exit_date <= y.IsoDateEnd
					inner join
					calpads.senr_grade_level g
						on g.code = b.grade_level_code
					inner join
					calpads.senr_grade_level f
						on f.rank = g.rank - (y.YearTrailing - @YearTrailingFile);', @Needle, @Replace);
			-- exe grade fix
			EXECUTE sp_executesql
				@Sql,
				N'@YearCode char(9),
					@YearTrailingFile smallint',
				@YearCode = @YearCode,
				@YearTrailingFile = @YearTrailingFile;
		END;
	-- delete data from production using staging
	SET @Sql = replace(N'
		DELETE
			t
		FROM
			calpads.senr t with (tablockx)
			inner join
			' + @StageSchemaTableName + ' s
				on t.district_code = s.district_code
				and t.school_code = s.school_code
				and t.year_code = s.year_code
				and t.ssid = s.ssid', @Needle, @Replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@Sql;
	-- set columns
	SET @Sql = replace(N'
		SELECT
			@ColumnArray += name + case when count(*) over() = row_number() over(order by (select 1)) then '''' else '','' end
		FROM
			sys.dm_exec_describe_first_result_set(@query, null, 0);', @Needle, @Replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@Sql,
		N'@query nvarchar(max),
			@ColumnArray nvarchar(max) out',
		@query = @query,
		@ColumnArray = @ColumnArray out;
	-- set dynamic sql
	SET @Sql = replace(N'
		INSERT INTO
			calpads.senr with (tablockx)
			(
				SubmissionFileId,IsEscrow,' + @ColumnArray + '
			)
		SELECT
			@SubmissionFileId,1,' + @ColumnArray + '
		FROM
			' + @StageSchemaTableName + ';', @Needle, @Replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@Sql,
		N'@SubmissionFileId int',
		@SubmissionFileId = @SubmissionFileId;
END;