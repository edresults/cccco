USE calpass;

GO

IF (object_id('calpads.senr_gender') is not null)
	BEGIN
		DROP TABLE calpads.senr_gender;
	END;

GO

CREATE TABLE
	calpads.senr_gender
	(
		code char(2) NOT NULL,
		name nvarchar(255) NULL,
		description nvarchar(255) NULL
	);

GO

ALTER TABLE
	calpads.senr_gender
ADD CONSTRAINT
	pk_senr_gender__code
PRIMARY KEY CLUSTERED 
	(
		code
	);

/* DML */

GO

INSERT INTO
	calpads.senr_gender
	(
		code,
		name,
		description
	)
VALUES
	('F','Female','Female'),
	('M','Male','Male');