USE calpass;

GO

IF (object_id('calpads.senr_primary_language') is not null)
	BEGIN
		DROP TABLE calpads.senr_primary_language;
	END;

GO

CREATE TABLE
	calpads.senr_primary_language
	(
		code char(2) NOT NULL,
		name nvarchar(255) NULL,
		description nvarchar(255) NULL
	);

GO

ALTER TABLE
	calpads.senr_primary_language
ADD CONSTRAINT
	pk_senr_primary_language__code
PRIMARY KEY CLUSTERED 
	(
		code
	);

/* DML */

GO

INSERT INTO
	calpads.senr_primary_language
	(
		code,
		name,
		description
	)
VALUES
	('00','English','English'),
	('01','Spanish','Spanish'),
	('02','Vietnamese','Vietnamese'),
	('03','Cantonese','Cantonese'),
	('04','Korean','Korean'),
	('05','Filipino (Pilipino or Tagalog)','Pilipino (Tagalog)'),
	('06','Portuguese','Portuguese'),
	('07','Mandarin (Putonghua)','Mandarin (Putonghua)'),
	('08','Japanese','Japanese'),
	('09','Khmer (Cambodian)','Khmer (Cambodian)'),
	('10','Lao','Lao'),
	('11','Arabic','Arabic'),
	('12','Armenian','Armenian'),
	('13','Burmese','Burmese'),
	('15','Dutch','Dutch'),
	('16','Farsi (Persian)','Farsi (Persian)'),
	('17','French','French'),
	('18','German','German'),
	('19','Greek','Greek'),
	('20','Chamorro (Guamanian)','Chamorro (Guamanian)'),
	('21','Hebrew','Hebrew'),
	('22','Hindi','Hindi'),
	('23','Hmong','Hmong'),
	('24','Hungarian','Hungarian'),
	('25','Ilocano','Ilocano'),
	('26','Indonesian','Indonesian'),
	('27','Italian','Italian'),
	('28','Punjabi','Punjabi'),
	('29','Russian','Russian'),
	('30','Samoan','Samoan'),
	('32','Thai','Thai'),
	('33','Turkish','Turkish'),
	('34','Tongan','Tongan'),
	('35','Urdu','Urdu'),
	('36','Cebuano (Visayan)','Cebuano (Visayan)'),
	('37','Sign Language','Sign Language'),
	('38','Ukrainian','Ukrainian'),
	('39','Chaozhou (Chiuchow)','Chaozhou (Chiuchow)'),
	('40','Pashto','Pashto'),
	('41','Polish','Polish'),
	('42','Assyrian','Assyrian'),
	('43','Gujarati','Gujarati'),
	('44','Mien (Yao)','Mien (Yao)'),
	('45','Rumanian','Rumanian'),
	('46','Taiwanese','Taiwanese'),
	('47','Lahu','Lahu'),
	('48','Marshallese','Marshallese'),
	('49','Mixteco','Mixteco'),
	('50','Khmu','Khmu'),
	('51','Kurdish (Kurdi, Kurmanji)','Kurdish (Kurdi, Kurmanji)'),
	('52','Serbo-Croatian (Bosnian, Croatian, Serbian)','Serbo-Croatian (Bosnian, Croatian, Serbian)'),
	('53','Toishanese','Toishanese'),
	('54','Chaldean','Chaldean'),
	('56','Albanian','Albanian'),
	('57','Tigrinya','Tigrinya'),
	('60','Somali','Somali'),
	('61','Bengali','Bengali'),
	('62','Telugu','Telugu'),
	('63','Tamil','Tamil'),
	('64','Marathi','Marathi'),
	('65','Kannada','Kannada'),
	('66','Amharic','Amharic'),
	('67','Bulgarian ','Bulgarian '),
	('68','Kikuyu (Gikuyu)','Kikuyu (Gikuyu)'),
	('69','Kashmiri','Kashmiri'),
	('70','Swedish ','Swedish '),
	('71','Zapoteco ','Zapoteco '),
	('72','Uzbek','Uzbek'),
	('99','Other non-English languages','Other non-English languages'),
	('UU','Unknown','Unknown');