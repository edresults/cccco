<?php
 ini_set('memory_limit', '512M');
/*
Load University CalPass files to production on SQL03
*/
$debug = 0;
$collegeid = isset($argv[2]) ? $argv[2] : '';
if ($collegeid == '') die("No collegeid selected.");
$path = isset($argv[1]) ? $argv[1] : '';
//$type = isset($argv[3]) ? $argv[3] : 'csu';
$acyear = substr($path,-4);
try {
	$dsna = 'dblib:dbname=calpass;host=prodatsql03';
//	$dsna = 'dblib:dbname=calpass;host=prodatsql01';
	$user = 'CALPASS\\zz_gua_d1_admin';
	$password = 'd2001O!#%';
	$dbh = new PDO($dsna, $user, $password);
}
catch(PDOException $e) {  
	 echo $e->getMessage();  
}
$dbh->exec('SET QUOTED_IDENTIFIER ON');
$dbh->exec('SET ANSI_WARNINGS ON');
$dbh->exec('SET ANSI_PADDING ON');
$dbh->exec('SET ANSI_NULLS ON');
$dbh->exec('SET CONCAT_NULL_YIELDS_NULL ON');
$year = substr($path,-4);

// Check for existing records 
printf("Checking for existing records...\n");
$checkq = "SELECT TOP 1 derkey1 
FROM UnivStudentProd 
WHERE school = '" . $collegeid . "' AND "
        . "YrTerm IN ('" . '20' . substr($path,-2) . "2'," . 
        "'20" . substr($path,-2) . "3', " .
        "'20" . substr($path,-2) . "4', " .
        "'20" . (substr($path,-2)-1) . "1')";
if ($debug) printf("checkq=$checkq\n");
$checkr = $dbh->query($checkq);
$checkr->setFetchMode(PDO::FETCH_ASSOC);
$check = $checkr->fetch();
if ($check['derkey1'] <> '') {
    logupdate($collegeid, substr($path,-4), "Found existing student records. Aborting.");
    printf("Renaming Directory\n");
    $pathparts = split("/", $path);
    if ($pathparts[3] == 'FileDrop') {
        // Running From /mnt/nas/FileDrop
        chdir("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3]);
        if (!file_exists("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
            $pathparts[4] . "/processed")) {
            mkdir("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
            $pathparts[4] . "/processed");
            printf("Created folder:". "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
            $pathparts[4] . "/processed\n");
        }
        rename($path, "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
            $pathparts[4] . "/processed/" . $pathparts[6]);
        printf("Renaming $path to " . "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
            $pathparts[4] . "/processed/" . $pathparts[6] . "\n");   
    }
    else
    {
        // Running From /mnt/nas/Raw/New
        chdir("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
            $pathparts[4]);
        if (!file_exists("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
            $pathparts[4] . "/" .  $pathparts[5] . "/processed")) {
            mkdir("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
            $pathparts[4] . "/" . $pathparts[5] . "/processed");
            printf("Created folder:". "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
            $pathparts[4] . "/" .  $pathparts[5] . "/processed\n");
        }
        rename($path, "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
            $pathparts[4] . "/" . $pathparts[5] . "/processed/" . $pathparts[7]);
        printf("Renaming $path to " . "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
            $pathparts[4] . "/" . $pathparts[5] . "/processed/" . $pathparts[7] . "\n");
    }
    logupdate($collegeid, $year, "Archived folder.\n");

    die("Found existing records. Aborting.");
}
else
{
    printf("None found. Moving on.\n");
}

// Find Files
printf("Finding Classic Files.\n");
$files = scandir($path);
$sindex = $cindex = $aindex = $pindex = $vindex = -99;
$count = 0;
foreach($files as $file) {
    if (stripos($file, 'student') !== FALSE) 
        $sindex = $count;
    if (stripos($file, 's' . $collegeid) !== FALSE) 
        $sindex = $count;
    
    if (stripos($file, 'course') !== FALSE) 
        $cindex = $count;
    if (stripos($file, 'c' . $collegeid) !== FALSE) 
        $cindex = $count;
    
    if (stripos($file, 'award') !== FALSE) 
        $aindex = $count;
    if (stripos($file, 'a' . $collegeid) !== FALSE) 
        $aindex = $count;
    if (stripos($file, 'degree') !== FALSE) 
        $aindex = $count;
     $count++;
}
if ($debug) print_r($files);
printf("Loading Students.\n");
//printf("Processing %s file.\n", $type);
$stu = array();
$stucount = $coursecount = $awardcount = $progcount = $ctecount = 0;
$firsttime = TRUE;
// Student
if (($handle = fopen($path . "/" . $files[$sindex], "r")) !== FALSE) {   // Open the file
   while (($data = fgetcsv($handle, 3000, ",")) !== FALSE) {      // Grab a row
        // skip header if present
        if (stripos($data[8], 'blank') !== FALSE) {
            print("Skipping Headers.\n");
        }
        else
        {
            $school = substr($data[0],-6);
            $yrterm = $data[1];
            $studentid = $data[2];
            $idstatus = $data[3];
            $csisnum = $data[4];
            if ($csisnum == '**********') $csisnum='';
//            $fname = substr(strtoupper($data[5]),0,3);
//            $lname = substr(strtoupper($data[6]),0,3);
            $gender = $data[7];
            $birthdate = $data[9];
            $instorigin = $data[10];
            $admissionbasis = $data[11];
            $major = $data[12];
            $enrollmentstatus = $data[13];
            $stulevel = $data[14];
            $termearned = $data[15] > 0 ? $data[15] : 0;
            $termgpa = $data[16] > 0 ? $data[16]: 0;
            if ($termgpa > 10) $termgpa /= 100;
            $unitsearnedlocal = $data[17] > 0 ? $data[17]: 0;
            $cumegpa = $data[18] > 0 ? $data[18]: 0;
            if ($cumegpa > 10) $cumegpa /= 100;
            $totalunitsearned = $data[19] > 0 ? $data[19]: 0;
            $totalunitsenrolled = $data[20] > 0 ? $data[20]: 0;
            $transfergpa = $data[21] > 0 ? $data[21]: 0;
            if ($transfergpa > 10) $transfergpa /= 100;
            $transferunitsearned = $data[22] > 0 ? $data[22]: 0;
            $hispanic = $data[23];
            $ethnicity = $data[24];
            $fosteryouth = isset($data[25]) ? $data[25]: '';
            $financialaid = isset($data[26]) ? $data[26] : '';

            $fname = str_replace("'", "", $data[5]);                  // Clean up data
            $fname = str_replace(".", "", $fname);
            $fname = str_replace(" ", "", $fname);
            $fname = strtoupper(substr($fname, 0, 3));
            $lname = str_replace("'", "", $data[6]);
            $lname = str_replace(".", "", $lname);
            $lname = str_replace(" ", "", $lname);
            $lname = strtoupper(substr($lname, 0, 3));

/*            
            // Plug for UC Davis - non-standard layout
            $school = $data[0];
            $yrterm = $data[1];
            $studentid = $data[3];
            $idstatus = $data[4];
            $csisnum = $data[5];
            $fname = $data[6];
            $lname = $data[7];
            $gender = $data[8];
            $birthdate = $data[10];
            $instorigin = $data[11];
            $admissionbasis = $data[12];
            $major = $data[14];
            $enrollmentstatus = $data[15];
            $stulevel = $data[16];
            $termearned = $data[17];
            $termgpa = $data[18];
            if ($termgpa > 10) $termgpa /= 100;
            $unitsearnedlocal = $data[19];
            $cumegpa = $data[20];
            if ($cumegpa > 10) $cumegpa /= 100;
            $totalunitsearned = $data[21];
            $totalunitsenrolled = $data[22];
            $transfergpa = $data[23];
            if ($transfergpa > 10) $transfergpa /= 100;
            $transferunitsearned = $data[24];
            $hispanic = $data[25];
            $ethnicity = $data[26];
            $fosteryouth = isset($data[27]) ? $data[27]: '';
            $financialaid = isset($data[28]) ? $data[28] : '';
*/             
            if ($debug) print_r($data);

            if ($firsttime) {
                $firsttime = FALSE;
                if (substr($ethnicity,0,1) == '0' AND $ethnicity <> '') {
                    $type = 'csu';
                    print("Processing CSU file.\n");
                    logupdate($collegeid, $year, "Found CSU file.");

                }
                else
                {
                    if (substr($ethnicity,0,1) == 'Y' OR substr($ethnicity,0,1) == 'N') {
                        $type = 'uc';
                        print("Processing UC file.\n");
                        logupdate($collegeid, $year, "Found UC file.");
                    }
                    else
                    {
                        logupdate($collegeid, $year, "Aborted: Could not identify ethnicity type.");
                        die("Couldn't identify ethnicity type.\n");
                    }
                }
            }
/*
            // Plug for Fresno Loading
            if ($termearned <> 999.99) $termearned /= 10;
            if ($unitsearnedlocal <> 999.99) $unitsearnedlocal /= 10;
            if ($totalunitsearned <> 999.99) $totalunitsearned /= 10;
            if ($totalunitsenrolled <> 999.99) $totalunitsenrolled /= 10;
            if ($transferunitsearned <> 999.99) $transferunitsearned /= 10;
*/                    
            
            if ($debug) print_r($data);
            // Split Ethnicity Codes
            $ethnicitycode = array();

            if (strlen($ethnicity) > 0) {
                if ($type == 'csu') {
                    $ethnicitycode[0] = substr($ethnicity,0,2);
                    if (strlen($ethnicity) > 2) {
                        $ethnicitycode[1] = substr($ethnicity,2,2);
                        if (strlen($ethnicity) > 4 ) {
                            $ethnicitycode[2] = substr($ethnicity,4,2);
                            if (strlen($ethnicity) > 6) {
                                $ethnicitycode[3] = substr($ethnicity,6,2);
                                if (strlen($ethnicity) > 8) {
                                    $ethnicitycode[4] = substr($ethnicity,8,2);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if ($type == 'uc') {
                        $enthnicitycode[0] = substr($ethnicity, 0, 1); // hispanic
                        $enthnicitycode[1] = substr($ethnicity, 1, 1); // afri am
                        $enthnicitycode[2] = substr($ethnicity, 2, 1); // nat am
                        $enthnicitycode[3] = substr($ethnicity, 3, 1); // asian
                        $enthnicitycode[4] = substr($ethnicity, 4, 1); // pac isld
                        $enthnicitycode[5] = substr($ethnicity, 5, 1); // white                    
                    }
                }
            }
            
            // Analyze Ethnicity Data
            $ethhi = $ethaa = $ethai = $ethas = $ethpi = $ethwh = 'N';
            if ($debug) print_r($ethnicitycode);

            foreach($ethnicitycode as $p => $ethcode) {
                switch($ethcode) {
                    case '01':
                        $ethwh = 'Y';
                        break;

                    case '02':
                        $ethaa = 'Y';
                        break;

                    case '03':
                        $ethai = 'Y';
                        break;

                    case '04':
                        $ethas = 'Y';
                        break;

                    case '05':
                        $ethpi = 'Y';
                        break;
                    
                    case '':
                        break;
                    
                    case 'Y':
                    case 'N':
                        switch($p) {
                            case 0:
                                $ethhi = $ethnicitycode[$p];
                                break;

                            case 1:
                                $ethaa = $ethnicitycode[$p];
                                break;

                            case 2:
                                $ethai = $ethnicitycode[$p];
                                break;

                            case '3':
                                $ethas = $ethnicitycode[$p];
                                break;

                            case '4':
                                $ethpi = $ethnicitycode[$p];
                                break;

                            case '5':
                                $ethwh = $ethnicitycode[$p];
                                break;

                            default:
                                logupdate($collegeid, $year, "Aborted: Invalid UC Ethnicity Code " . $p . " " . $ethnicitycode[$p]);
                                die("Invalid UC Ethnicity Code " . $p . " " . $ethnicitycode[$p] . ".\n");
                            }
                       break;

                    default:
                        logupdate($collegeid, $year, "Aborted: Invalid CSU Ethnicity Code " . $p . " " . $ethnicitycode[$p]);
                        die("Invalid CSU Ethnicity Code " . $p . " " . $ethnicitycode[$p] . ".\n");
                        break;
                }   
            } 
            
            $dateadded = date("Y-m-d H:i:s");
            $origstudentid = $studentid;

            $unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ñ' => 'n');

    //        $lname = strtr( $lname, $unwanted_array );
            $enye = chr(0xf1);
            $fname = strtr( $fname, $enye, "n" );
            $lname = strtr( $lname, $enye, "n" );
            $eyye = chr(0xfd);
            $fname = strtr( $fname, $eyye, "y" );
            $lname = strtr( $lname, $eyye, "y" );
            $eaccent = chr(0xe9);
            $fname = strtr( $fname, $eaccent, "e" );
            $lname = strtr( $lname, $eaccent, "e" );
            $iaccent = chr(0xed);
            $fname = strtr( $fname, $iaccent, "i" );
            $lname = strtr( $lname, $iaccent, "i" );
            $weirdone = chr(0xb1);
            $fname = strtr( $fname, $weirdone, "n" );
            $lname = strtr( $lname, $weirdone, "n" );
            $apost = "'";
            $fname = strtr($fname, $apost, "$");
            $lname = strtr($lname, $apost, "$");
            $fname = str_pad($fname, 3, "$", STR_PAD_RIGHT);
            $lname = str_pad($lname, 3, "$", STR_PAD_RIGHT);

            // Derkey
            if (($csisnum) == 'XXXXXXXXXX' OR $csisnum == '' OR $csisnum == '**********') {
                $keyq = "SELECT dbo.Derkey1('" . strtoupper(substr($fname,0,3)) . "', '" . 
                        strtoupper(substr($lname,0,3)) . "', 
                '" . $gender . "', '" . $birthdate . "') as derkey1, 
                dbo.Get1289Encryption('"  . $studentid . "', '') as studentid";                
                $keyr = $dbh->query($keyq);
                $key = $keyr->fetch();
                $derkey1 = $key['derkey1'];
                $studentid = $key['studentid'];
                $stu[$origstudentid][$acyear]['derkey'] = $derkey1;
                $stu[$origstudentid][$acyear]['studentid'] = $studentid;
                $stu[$origstudentid][$acyear]['csisnum'] = $csisnum;
            }
            else
            {
                $keyq = "SELECT dbo.Derkey1('" . strtoupper(substr($fname,0,3)) . "', '" . 
                        strtoupper(substr($lname,0,3)) . "', 
                '" . $gender . "', '" . $birthdate . "') as derkey1, 
                dbo.Get1289Encryption('"  . $studentid . "', '') as studentid, 
                dbo.Get9812Encryption('"  . $csisnum . "', '') as csisnum";
                
                if ($debug) printf("keyq=$keyq\n");
                $keyr = $dbh->query($keyq);
                $key = $keyr->fetch();
                $derkey1 = $key['derkey1'];
                $studentid = $key['studentid'];
                $csisnum = $key['csisnum'];
                $stu[$origstudentid][$acyear]['derkey'] = $derkey1;
                $stu[$origstudentid][$acyear]['studentid'] = $studentid;
                $stu[$origstudentid][$acyear]['csisnum'] = $csisnum;
            }
            
            if ($derkey1 == '') {
                print_r($data);
                die("Empty Derkey1\n");
            }

            if (substr($derkey1,0,5) <> 'RRRRR' AND $fname <> '' AND $lname <> '' AND 
                    $gender <> '' AND $birthdate <> '') {
                // Write Record
                $insq = "INSERT INTO calpass.dbo.UnivStudentProd
                (Derkey1, Derkey2, school, YrTerm, StudentId, IdStatus, CSISNum, Fname, 
                Lname, Gender, Birthdate, InstOrigin, AdmitBasis, Major, 
                EnrollStatus, StudentLevel, UnitsEarnedTerm, TermGPA, UnitsEarnedLocal, 
                CumeGPA, TotalUnitsEarned, TotalUnitsEnroll, XferGPA, XferUnitsEarned, 
                HispanicEthnicity, EthnicityHispanic, EthnicityBlack, EthnicityNativeAmerican, 
                EthnicityAsian, EthnicityPacificIslander, EthnictyWhite, FosterYouth,
                FinancialAid, DateAdded, batch_id)
                VALUES 
                ('" . $derkey1 . "', '" . $derkey1 . "XXXXXX', '" . $school . "', 
                '" . $yrterm . "', '" . $studentid . "', '" . $idstatus . "', 
                '" . $csisnum . "', '" . $fname . "', '" . $lname . "', '" . $gender . "', 
                '" . $birthdate . "', '" . $instorigin . "', '" . $admissionbasis . "', 
                '" . str_replace('.','',$major) . "', '" . $enrollmentstatus . "', '" . $stulevel . "', 
                " . $termearned . ", " . $termgpa . ", " . $unitsearnedlocal . ", 
                " . $cumegpa . ", " . $totalunitsearned . ", " . $totalunitsenrolled . ", 
                " . $transfergpa . ", " . $transferunitsearned . ", '" . $hispanic . "',
                '" . $ethhi . "', '" . $ethaa . "', '" . $ethai . "', '" . $ethas . "', 
                '" . $ethpi . "', '" . $ethwh . "', '" . $fosteryouth . "', 
                '" . $financialaid . "', 
                '" . $dateadded . "', " . "'')";
                if ($debug) printf("insq=$insq\n"); 
                $sth = $dbh->query($insq);
                if ($debug) {
                    echo "\nPDO::errorInfo():\n";
                    print_r($dbh->errorInfo());                
                }
                $stucount++;
            }
            else
            {
                print("Dumping a record.\n");
                print_r($data);
                die();
            }
        }
    }
}   
// Course
printf("Loading Courses.\n");
$dropcourse = 0;
if (($handle = fopen($path . "/" . $files[$cindex], "r")) !== FALSE) {   // Open the file
    while (($data = fgetcsv($handle, 3000, ",")) !== FALSE) {      // Grab a row

        // skip header if present
        if (stripos($data[2], 'student') !== FALSE) {
            print("Skipping Headers.\n");
        }
        else
        {
            $school = $data[0];
            $yrterm = $data[1];
            $origstudentid = $data[2];
            $coursetitle = $data[3];
            $courseabbrev = $data[4];
            $coursenumber = $data[5];
            $coursesuffix = $data[6];
            $unitsattempted = $data[7];
            $grade = $data[8];
            $remedial = isset($data[9]) ? $data[9] : '';

            $dateadded = date("Y-m-d H:i:s");
            $derkey1 = isset($stu[$origstudentid][$acyear]['derkey']) ? $stu[$origstudentid][$acyear]['derkey']: '';
            $derkey2 = $derkey1 . 'XXXXXX';
            $studentid = isset($stu[$origstudentid][$acyear]['studentid']) ? $stu[$origstudentid][$acyear]['studentid'] : '';
         	
            // Write Record 
            if ($derkey1 <> '' AND substr($derkey1,0,5) <> 'RRRRR') {
                $insq = "INSERT INTO UnivCourseProd 	
                (Derkey1, Derkey2, school, YrTerm, StudentId, CourseTitle, 
                CourseAbbr, CourseNum, CourseNumSuffix,
                Units, Grade, Remedial, DateAdded)
                VALUES 
                ('" . $derkey1. "', '" . $derkey2 . "', '" . $school . "', 
                '" . $yrterm . "', '" . $studentid . "', '" . $coursetitle . "', 
                '" . $courseabbrev . "', '" . $coursenumber . "', 
                '" . $coursesuffix . "', '" . $unitsattempted . "', '" . $grade . "', 
                '" . $remedial . "', '" . $dateadded . "')";
                if ($debug) printf("insq=$insq\n");
                $sth = $dbh->query($insq);
                $coursecount++;
            }
            else
            {
                //die("Couldn't locate derkey for course record.");
                if ($debug) printf("Dropping Course Record %s: %d\n", $origstudentid, 
                    $dropcourse++);
            }
        }
    }
}

// Award
printf("Loading Awards.\n");
$dropaward = 0;
if (($handle = fopen($path . "/" . $files[$aindex], "r")) !== FALSE) {   // Open the file
    while (($data = fgetcsv($handle, 3000, ",")) !== FALSE) {      // Grab a row

        $school = $data[0];
        $yrterm = $data[1];
        $origstudentid = $data[2];
        $awardtype = $data[3];
        $major1 = str_replace('.', '', $data[4]);
        $major2 = isset($data[5]) ? str_replace('.', '', $data[5]) : 'XXXXXX';
        $major3 = isset($data[6]) ? str_replace('.', '', $data[6]) : 'XXXXXX';
        $awarddate = isset($data[7]) ? $data[7] : substr($yrterm,0,4) . "XXXX";

        $dateadded = date("Y-m-d H:i:s");
        $derkey1 = isset($stu[$origstudentid][$acyear]['derkey']) ? $stu[$origstudentid][$acyear]['derkey'] : '';
        $derkey2 = $derkey1 . 'XXXXXX'; 	
        
        $studentid = isset($stu[$origstudentid][$acyear]['studentid']) ? $stu[$origstudentid][$acyear]['studentid'] : '';
 
        if ($derkey1 <> '' AND substr($derkey1,0,5) <> 'RRRRR') {
            $insq = "INSERT INTO UnivAwardProd
            (derkey1, derkey2, school, YrTerm, StudentId, DegreeType,
            Major1, Major2, Major3, AwardDate, DateAdded)
            VALUES
            ('" . $derkey1. "', '" . $derkey2 . "', '" . $school . "', 
            '" . $yrterm . "', '" . $studentid . "', '" . $awardtype . "', 
            '" . $major1 . "', '" . $major2 . "', '" . $major3 . "',
            '" . $awarddate . "', '" . $dateadded . "')";
            if ($debug) printf("insq=$insq\n");
            $sth = $dbh->query($insq);
            $awardcount++;
        }
        else
        {
         //  die("Couldn't locate derkey for award record.");
             if ($debug) printf("Dropping Award Record $origstudentid: %d\n", $dropaward++);
        }
    }
}

printf("Loaded %d students, %d courses, and %d awards.\n", 
$stucount, $coursecount, $awardcount);
printf("Dropped %d courses and %d awards.\n", $dropcourse, $dropaward);		

logupdate($collegeid, $year, "Loaded Data: S:$stucount C:$coursecount A:$awardcount");

fclose($handle);
printf("Renaming Directory\n");
$pathparts = split("/", $path);
print_r($pathparts);
if ($pathparts[3] == 'FileDrop') {
    // Running From /mnt/nas/FileDrop
    chdir("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3]);
    if (!file_exists("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
        $pathparts[4] . "/processed")) {
        mkdir("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
        $pathparts[4] . "/processed");
        printf("Created folder:". "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . 
        $pathparts[3] . "/" .
        $pathparts[4] . "/processed\n");
    }
    rename($path, "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
        $pathparts[4] . "/processed/" . $pathparts[6]);
    printf("Renaming $path to " . "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . 
        $pathparts[3] . "/" . 
        $pathparts[4] . "/processed/" . $pathparts[6] . "\n");   
}
else
{
    chdir("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
        $pathparts[4]);
    if (!file_exists("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
        $pathparts[4] . "/" .  $pathparts[5] . "/processed")) {
        mkdir("/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
        $pathparts[4] . "/" . $pathparts[5] . "/processed");
        printf("Created folder:". "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" .
        $pathparts[4] . "/" .  $pathparts[5] . "/processed\n");
    }
    rename($path, "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
        $pathparts[4] . "/" . $pathparts[5] . "/processed/" . $pathparts[7]);
    printf("Renaming $path to " . "/" . $pathparts[1] . "/" . $pathparts[2] . "/" . $pathparts[3] . "/" . 
        $pathparts[4] . "/" . $pathparts[5] . "/processed/" . $pathparts[7] . "\n");
}
logupdate($collegeid, $year, "Archived folder.\n");

function logupdate($district, $year, $msg) {
    try {
	$dsna = 'dblib:dbname=calpass;host=prodatsql03';
//	$dsna = 'dblib:dbname=calpass;host=prodatsql01';
	$user = 'CALPASS\\zz_gua_d1_admin';
	$password = 'd2001O!#%';
	$dbh = new PDO($dsna, $user, $password);
    }
    catch(PDOException $e) {  
             echo $e->getMessage();  
    }
    $date = date('Y/m/d H:i:s');
    $processid = getmypid();
    
    switch(strlen($district)) {
        case 5:
            $distq = "SELECT OrganizationId, OrganizationCode, OrganizationName "
                . "FROM Organization "
                . "WHERE SUBSTRING(OrganizationCode,3,5) = '" . $district . "' AND "
                . "RIGHT(OrganizationCode,7)='0000000'";
            break;
        
        case 6:
            $distq = "SELECT OrganizationId, OrganizationCode, OrganizationName "
                . "FROM Organization "
                . "WHERE OrganizationId = '" . $district . "' OR OrganizationCode = '" . $district . "'";
            break;
        
        case 7:
            $distq = "SELECT OrganizationId, OrganizationCode, OrganizationName "
                . "FROM Organization "
                . "WHERE LEFT(OrganizationCode,7) = '" . $district . "' AND "
                . "RIGHT(OrganizationCode,7)='0000000'";
            break;
    }
    $distr = $dbh->query($distq);
    $distr->setFetchMode(PDO::FETCH_ASSOC);
    $dist = $distr->fetch();
    
    $insq = "INSERT INTO LoaderLog"
    . "(orgid, orgcode, orgname, acyear, processid, date, message) "
    . "VALUES "
    . "('" . $dist['OrganizationId'] . "', '" . 
    substr($dist['OrganizationCode'],0,7) . "', '" . 
    $dist['OrganizationName'] . "', '" . $year . "', '" . $processid . "', '" . 
    $date . "', '" . 
    $msg . "')";
    $insr = $dbh->query($insq);
//    printf("insq=$insq\n");
    return;
}
?>
