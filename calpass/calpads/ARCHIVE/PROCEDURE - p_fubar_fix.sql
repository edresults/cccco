USE calpads;

GO

IF (object_id('dbo.p_fubar_fix', 'P') IS NOT NULL)
	BEGIN
		DROP PROCEDURE dbo.p_fubar_fix;
	END;

GO

CREATE PROCEDURE
	dbo.p_fubar_fix
AS

	SET NOCOUNT ON;

DECLARE
	-- variables
	@grade_level_code_superior char(2),
	@grade_level_code_inferior char(2),
	@year_code char(9),
	@ubound int,
	@year_code_anchor_alpha smallint = 2014,
	@year_code_anchor_omega smallint = 2015,
	@iterator int = 1;
	-- table types
	DECLARE
		@tt_parameters
	AS TABLE
		(
			grade_level_code char(2),
			ordinal int
		);

	DECLARE
		@tt_loop
	AS TABLE
		(
			id int identity(1,1),
			grade_level_code_superior char(2),
			grade_level_code_inferior char(2),
			year_code char(9)
		);

BEGIN
	-- create dummy data
	IF (object_id('calpads.dbo.senr_1314') IS NOT NULL)
		BEGIN
			DROP TABLE calpads.dbo.senr_1314;
		END;

	SELECT
		*
	INTO
		calpads.dbo.senr_1314
	FROM
		calpads.dbo.senr_73
	WHERE
		ssid = '6040051334';

	IF (object_id('calpads.dbo.senr_1415') IS NOT NULL)
		BEGIN
			DROP TABLE calpads.dbo.senr_1415;
		END;

	SELECT
		*
	INTO
		calpads.dbo.senr_1415
	FROM
		calpads.dbo.senr_75
	WHERE
		ssid = '6040051334';

	-- sample data
	SELECT
		*
	FROM
		calpads.dbo.senr_1314 b
		inner join
		calpads.dbo.senr_1415 g
			on b.ssid = g.ssid;

	-- populate parameters table
	INSERT INTO
		@tt_parameters
		(
			grade_level_code,
			ordinal
		)
	VALUES
		('PS',1),
		('KN',2),
		('1',3),
		('2',4),
		('3',5),
		('4',6),
		('5',7),
		('6',8),
		('7',9),
		('8',10),
		('9',11),
		('10',12),
		('11',13),
		('12',14);

	-- populate looping table
	INSERT INTO
		@tt_loop
		(
			
			grade_level_code_superior,
			grade_level_code_inferior,
			year_code
		)
	SELECT
		t1.grade_level_code,
		t2.grade_level_code,
		cast(@year_code_anchor_alpha - row_number() over(partition by t1.grade_level_code order by t2.ordinal desc) as char(4)) + '-' + 
			cast(@year_code_anchor_omega - row_number() over(partition by t1.grade_level_code order by t2.ordinal desc) as char(4)) as year_code
	FROM
		@tt_parameters t1
		inner join
		@tt_parameters t2
			on t1.ordinal > t2.ordinal
	ORDER BY
		t1.ordinal desc,
		t2.ordinal desc;

	-- set ubound for loop abort
	SELECT
		@ubound = max(id)
	FROM
		@tt_loop;

	-- set looping variables
	SELECT
		@grade_level_code_superior = grade_level_code_superior,
		@grade_level_code_inferior = grade_level_code_inferior,
		@year_code = year_code
	FROM
		@tt_loop
	WHERE
		id = @iterator;

	-- loop
	WHILE (@iterator <= @ubound)
	BEGIN

		-- update bad values with good values
		UPDATE
			b
		SET
			b.grade_level_code = @grade_level_code_inferior
		FROM
			calpads.dbo.senr_1314 b
			inner join
			calpads.dbo.senr_1415 g
				on g.ssid = b.ssid
		WHERE
			b.year_code = @year_code
			and g.grade_level_code = @grade_level_code_superior
			and b.grade_level_code <> @grade_level_code_inferior;

		-- iterate
		SET @iterator += 1;

		-- next iteration
		SELECT
			@grade_level_code_superior = grade_level_code_superior,
			@grade_level_code_inferior = grade_level_code_inferior,
			@year_code = year_code
		FROM
			@tt_loop
		WHERE
			id = @iterator;

	END;

	-- sample fix
	SELECT
		*
	FROM
		calpads.dbo.senr_1314 b
		inner join
		calpads.dbo.senr_1415 g
			on b.ssid = g.ssid;
END;