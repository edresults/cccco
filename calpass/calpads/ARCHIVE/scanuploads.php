<?php
 ini_set('memory_limit', '512M');

/* 
 * Scan NAS for files to load
 */

//$path = "/mnt/nas/RawFiles/New/";
$path = "/mnt/nas/FileDrop/";
$folders = scandir($path);

try {
    $dsna = 'dblib:dbname=calpass;host=prodatsql03';
//	$dsna = 'dblib:dbname=calpass;host=prodatsql01';
    $user = 'CALPASS\\zz_gua_d1_admin';
    $password = 'd2001O!#%';
    $dbh = new PDO($dsna, $user, $password);
}
catch(PDOException $e) {  
         echo $e->getMessage();  
}

// Start Run
$processid = getmypid();
$msg = 'Start Data Loading Run';
$insq = "INSERT INTO LoaderLog"
. "(processid, date, message) "
. "VALUES ('"
. $processid . "', '" . 
date('Y/m/d H:i:s') . "', '" . 
$msg . "')";
$insr = $dbh->query($insq);

$count = 0;
foreach($folders as $folder) {
    if (is_dir($path . $folder)) {
        // Look for toprocess folder
        //        printf("path=%s\n", $path . $folder . "/toprocess");
              
        if (file_exists($path . $folder . "/toprocess") && 
                is_dir($path . $folder . "/toprocess")) {
            // Found one
            $years = scandir($path . $folder . "/toprocess/");
            foreach($years as $year) {
                if (strlen($year) == 4) {
                    printf("Working on %s\n", $folder);
                    $subdirs = scandir($path . $folder);
                    
                    logupdate($folder, $year, 'Found Files to Process');
                    $distcode = getDistrict(substr($folder,0,6));
                    if ($count >= 25) die("Stopping for data testing...\n");
                    $count++;
 
                    // Are there classic files to upload?
                    $files = scandir($path . $folder . "/toprocess/" . $year);
                    $sindex = -99;
                    $count2 = 0;
                    foreach($files as $file) {
                        if (strpos($file, 'S' . $distcode . "K.TXT") !== FALSE) 
                            $sindex = $count2;
                        $count2++;
                    }

                    // Check to determine K-12 or University
                    $chkq = "SELECT OrganizationTypeId "
                            . "FROM Organization "
                            . "WHERE OrganizationCode = '" . substr($folder,0,6) . "' OR 
                                OrganizationId = '" . substr($folder,0,6) . "'";
        
                    $chkr = $dbh->query($chkq);
                    $chkr->setFetchMode(PDO::FETCH_ASSOC);
                    $chk = $chkr->fetch();
                    if ($chk['OrganizationTypeId'] == 3) {
                        // University
                        logupdate($folder, $year, 'Processing University Data');
                        printf("Processing University Data\n");
                        passthru('php univload.php ' . $path . $folder . "/toprocess/" . $year . ' ' . getCollegeId(substr($folder,0,6)));
                    }
                    else
                    {
                        // K-12
                        // Spawn the makecalpads script 
                        if ($sindex == -99)
                            passthru('php makecalpadsv7.php ' . $path . $folder . "/toprocess/" . $year . " " . $year);
                        logupdate($folder, $year, 'Processing K12 Data');
                        printf("Processing K12 Data\n");
                        passthru('php loadclassic.php ' . $distcode . " " . $path . $folder . "/toprocess/" . $year);
                    }
                }
            }
        }
    }
}
passthru('php notification.php');

// Ending Run
$msg = 'End Data Loading Run';
$insq = "INSERT INTO LoaderLog"
. "(processid, date, message) "
. "VALUES ('"
. $processid . "', '" . 
date('Y/m/d H:i:s') . "', '" . 
$msg . "')";
$insr = $dbh->query($insq);

function getDistrict($orgid) {
    try {
	$dsna = 'dblib:dbname=calpass;host=prodatsql03';
//	$dsna = 'dblib:dbname=calpass;host=prodatsql01';
	$user = 'CALPASS\\zz_gua_d1_admin';
	$password = 'd2001O!#%';
	$dbh = new PDO($dsna, $user, $password);
    }
    catch(PDOException $e) {  
             echo $e->getMessage();  
    }

    $distq = "SELECT SUBSTRING(OrganizationCode,3,5) as district "
            . "FROM Organization "
            . "WHERE OrganizationId = '" . $orgid . "'";
    $distr = $dbh->query($distq);
    $distr->setFetchMode(PDO::FETCH_ASSOC);
    $dist = $distr->fetch();
    return($dist['district']);
}
function getCollegeId($orgid) {
    try {
	$dsna = 'dblib:dbname=calpass;host=prodatsql03';
//	$dsna = 'dblib:dbname=calpass;host=prodatsql01';
	$user = 'CALPASS\\zz_gua_d1_admin';
	$password = 'd2001O!#%';
	$dbh = new PDO($dsna, $user, $password);
    }
    catch(PDOException $e) {  
             echo $e->getMessage();  
    }

    $distq = "SELECT OrganizationCode as district "
            . "FROM Organization "
            . "WHERE OrganizationId = '" . $orgid . "'";
    $distr = $dbh->query($distq);
    $distr->setFetchMode(PDO::FETCH_ASSOC);
    $dist = $distr->fetch();
    return($dist['district']);
}


function logupdate($district, $year, $msg) {
    try {
	$dsna = 'dblib:dbname=calpass;host=prodatsql03';
//	$dsna = 'dblib:dbname=calpass;host=prodatsql01';
	$user = 'CALPASS\\zz_gua_d1_admin';
	$password = 'd2001O!#%';
	$dbh = new PDO($dsna, $user, $password);
    }
    catch(PDOException $e) {  
             echo $e->getMessage();  
    }
    $date = date('Y/m/d H:i:s');
    $processid = getmypid();
    
    switch(strlen($district)) {
        case 5:
            $distq = "SELECT OrganizationId, OrganizationCode, OrganizationName "
                . "FROM Organization "
                . "WHERE SUBSTRING(OrganizationCode,3,5) = '" . $district . "' AND "
                . "RIGHT(OrganizationCode,7)='0000000'";
            break;
        
        case 6:
            $distq = "SELECT OrganizationId, OrganizationCode, OrganizationName "
                . "FROM Organization "
                . "WHERE OrganizationId = '" . $district . "' OR OrganizationCode = '" . $district . "'";
            break;
        
        case 7:
            $distq = "SELECT OrganizationId, OrganizationCode, OrganizationName "
                . "FROM Organization "
                . "WHERE LEFT(OrganizationCode,7) = '" . $district . "' AND "
                . "RIGHT(OrganizationCode,7)='0000000'";
            break;
        
        default:
            die("Invalid District Code:" . $district);
        break;
    }
    $distr = $dbh->query($distq);
    $distr->setFetchMode(PDO::FETCH_ASSOC);
    $dist = $distr->fetch();
    
    $insq = "INSERT INTO LoaderLog"
    . "(orgid, orgcode, orgname, acyear, processid, date, message) "
    . "VALUES "
    . "('" . $dist['OrganizationId'] . "', '" . 
    substr($dist['OrganizationCode'],0,7) . "', '" . 
    $dist['OrganizationName'] . "', '" . $year . "', '" . $processid . "', '" . 
    $date . "', '" . 
    $msg . "')";
    $insr = $dbh->query($insq);
//    printf("insq=$insq\n");
    return;
}
?>