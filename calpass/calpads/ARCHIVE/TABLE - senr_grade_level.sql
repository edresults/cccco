USE calpass;

GO

IF (object_id('calpads.senr_grade_level') is not null)
	BEGIN
		DROP TABLE calpads.senr_grade_level;
	END;

GO

CREATE TABLE
	calpads.senr_grade_level
	(
		code char(2) not null,
		name nvarchar(255) not null,
		description nvarchar(4000) not null,
		rank int not null,
		CodePubSchls varchar(101)
	);

GO

ALTER TABLE
	calpads.senr_grade_level
ADD CONSTRAINT
	pk_senr_grade_level__code
PRIMARY KEY CLUSTERED 
	(
		code
	);

/* DML */

GO

INSERT INTO
	calpads.senr_grade_level
	(
		CodePubSchls,
		rank,
		code,
		name,
		description
	)
VALUES
	('1',4,'01','First Grade','Grade One'),
	('2',5,'02','Second Grade','Grade Two'),
	('3',6,'03','Third Grade','Grade Three'),
	('4',7,'04','Fourth Grade','Grade Four'),
	('5',8,'05','Fifth Grade','Grade Five'),
	('6',9,'06','Sixth Grade','Grade Six'),
	('7',10,'07','Seventh Grade','Grade Seven'),
	('8',11,'08','Eighth Grade','Grade Eight'),
	('9',12,'09','Ninth Grade','Grade Nine'),
	('10',13,'10','Tenth Grade','Grade Ten'),
	('11',14,'11','Eleventh Grade','Grade Eleven'),
	('12',15,'12','Twelfth Grade','Grade Twelve'),
	('A',16,'AD','Adult','A category for students who are primarily enrolled in an adult education center. These students would not have an affiliation with a K-12 institution.'),
	('I',0,'IN','Infant','A grade level for individuals, ages 0 through 18 months.'),
	('K',3,'KN','Kindergarten','Kindergarten'),
	('P',2,'PS','Prekindergarten','A grade level for children formally enrolled in a preschool program in preparation for entrance to kindergarten.'),
	('T',1,'TD','Toddlers','A grade level for individuals, ages 19 months through 35 months, not yet enrolled in a public educational institution or formal preschool program.'),
	(null,'UE','Ungraded Elementary','A grade level for students enrolled in elementary classes (kindergarten through eighth grades) that are not assigned to a specific grade level. This grade level is specifically for students in a special education program.'),
	(null,'US','Ungraded Secondary','A grade level for students enrolled in secondary classes (ninth through twelfth grades) that are not assigned to a specific grade level. This grade level is specifically for students in a special education program.'),
	(null,'UU','Unknown','The grade level cannot be determined. For example, there were multiple responses or no response at all. You would not see this option on a form, but rather this category is used for storage of data anomalies.');