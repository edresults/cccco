DECLARE @FileYearTrailing smallint = 2011;

SELECT
  grade_level_code,
  count(code)
FROM
  [calpads].[senr201704251812030876837]
  inner join
  calpads.senr_grade_level
    on code = grade_level_code
GROUP BY
  grade_level_code,
  rank
ORDER BY
  rank;

SELECT
  f.code,
  count(f.code)
FROM
  [calpads].[senr201704251812030876837] b -- [b]ad 2011-2012
  inner join
  calpads.Year y -- [y]ear
    on b.enroll_exit_date >= y.IsoDateStart
    and b.enroll_exit_date <= y.IsoDateEnd
  inner join
  calpads.senr_grade_level g -- [g]rade
    on g.code = b.grade_level_code
  inner join
  calpads.senr_grade_level f -- [f]ix
    on f.rank = g.rank - (y.YearTrailing - @FileYearTrailing)
GROUP BY
  f.code,
  f.rank
ORDER BY
  f.rank;

SELECT
  b.ssid,
  b.school_code,
  grade = b.grade_level_code,
  b.year_code,
  b.enroll_start_date,
  enroll_start_year_code = by1.YearCode,
  b.enroll_exit_date,
  enroll_exit_year_code = by2.YearCode,
  fileDiff = by2.YearTrailing - @FileYearTrailing,
  intraDiff = by2.YearTrailing - by1.YearTrailing,
  interDiff = gy2.YearTrailing - by2.YearTrailing,
  FIXED_GRADE_LEVEL_CODE = f.code,
  '<= 2011-2012 : 2014-2015 =>' as partition,
  g.school_code,
  grade = g.grade_level_code,
  g.year_code,
  g.enroll_start_date,
  enroll_start_year_code = gy1.YearCode,
  g.enroll_exit_date,
  enroll_exit_year_code = gy2.YearCode
FROM
  [calpads].[senr201704251812030876837] b -- [b]ad 2011-2012
  inner join
  [calpads].[senr201704260806060085842] g -- [g]ood 2014-2015
    on b.ssid = g.ssid
  inner join
  calpads.Year by1
    on b.enroll_start_date >= by1.IsoDateStart
    and b.enroll_start_date <= by1.IsoDateEnd
  inner join
  calpads.Year by2
    on b.enroll_exit_date >= by2.IsoDateStart
    and b.enroll_exit_date <= by2.IsoDateEnd    
  inner join
  calpads.Year gy1
    on g.enroll_start_date >= gy1.IsoDateStart
    and g.enroll_start_date <= gy1.IsoDateEnd
  inner join
  calpads.Year gy2
    on g.enroll_exit_date >= gy2.IsoDateStart
    and g.enroll_exit_date <= gy2.IsoDateEnd
  inner join
  calpads.senr_grade_level bg
    on bg.code = b.grade_level_code
  inner join
  calpads.senr_grade_level f
    on f.rank = bg.rank - (by2.YearTrailing - @FileYearTrailing)
ORDER BY
  ssid;