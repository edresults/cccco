USE calpass;

GO

IF (object_id('calpads.senr_enroll_exit_code') is not null)
	BEGIN
		DROP TABLE calpads.senr_enroll_exit_code;
	END;

GO

CREATE TABLE
	calpads.senr_enroll_exit_code
	(
		code char(4) NOT NULL,
		name nvarchar(255) NULL,
		description nvarchar(4000) NULL
	);

GO

ALTER TABLE
	calpads.senr_enroll_exit_code
ADD CONSTRAINT
	pk_senr_enroll_exit_code__code
PRIMARY KEY CLUSTERED 
	(
		code
	);

/* DML */

GO

INSERT INTO
	calpads.senr_enroll_exit_code
	(
		code,
		name,
		description
	)
VALUES
('E125','PriorComplSpecEd','Student exited a special education transition program and was previously reported as receiving a special education certificate of completion, passing the California High School Proficiency Examination (CHSPE), or passing the General Educational Development (GED) test.'),
('E130','Died','Student died while enrolled in school or student completed the school year, was expected to return, and died during the summer break.'),
('E140','NoKnownEnrollTruant','Student withdrew from/left school, and the local educational agency is certifying that after taking the following truancy intervention steps, there is no evidence the student is in an academic program toward a diploma or its equivalent:1) When a pupil between the ages of 6 and 18 (inclusive) accumulates three absences of more than 30 minutes each without a valid excuse in one school year, the school district shall notify the pupil''s parent or guardian by the notice required in Education Code Section 48260.5.2. After continued absences without a valid excuse following the notice, an appropriate district officer or employee shall make a conscientious effort to hold at least one conference with the parent or guardian and pupil as required in Education Code Section 48262.3. If it appears upon investigation that any parent, guardian, or other person having control or charge of the child is violating any of the provisions of California''s compulsory education law, then the case should be referred to a school attendance review board (SARB) pursuant to Education Code Section 48291.'),
('E150','MidYearEnrollmentUpdate','The student is not exiting the school but the one or more of the following pieces of information about the student is being updated:
	• Grade level, greater than 14 days before the end of the school year;
	• Student School Transfer Code; or
	• District of Geographic Residence

If the student exited and left the school use the appropriate exit code, for example:
	• T160-Transfer to another CA school; or
	• E155 [YearEndGradeLevelExit] if a grade level exit took place during the last 14 days of the academic year because of summer break or year-end intersession.
	• E230 [Completer Exit] and School Completion Status=480 [Promoted/ Matriculated] and not E155 if the student completed the grade that is being exited and that grade-level is the last grade offered at the school.'),
('E155','YearEndEnrlmntExitSameSchl','The student exited a grade level (excluding high school completion) during the last 14 days of the current academic year because of summer break or year-end intersession . This exit code is to be used for students thought to be returning to the school. If the student is completing the school year, but known to be transfering to another school or some other type of exit, use the appropriate exit code (for example T160 Transfer to another California school).

	The grade level exit at the end of the year indicates only that the student exited the grade level at the end of the year. The student''s enrollment for the next academic year may be either a grade-level promotion, grade retention, or grade demotion.

	Note: If the student completed the last grade offered at the school, use Student Exit Category=E230 [Completer Exit] and School Completion Status=480 [Promoted/ Matriculated] instead of this code.'),
('E230','CompleterExit','Student left school after completing their academic program at this school, whether or not the completion resulted in high school graduation. This includes students who fail CAHSEE, who graduate with CAHSEE waivers, or who reach the maximum age for high school but do not have enough credits to graduate.'),
('E300','ExpellNoKnownEnroll','Student left school after being expelled, was subsequently referred to another educational service institution, but never showed up, and attempts to locate the student were unsuccessful. Do NOT use this code if the district took the appropriate steps to refer the student to the Student Attendance Review Board (SARB); use E140 (TruantNoKnownEnroll). Only use this code if the student was exited without first being referred to the SARB.'),
('E400','OtherOrUnknown','The student withdrew from/left school for reasons that cannot be determined or for reasons other that those described in the Student Exit Category codes. Do NOT use this code for students who were referred for truancy intervention, as outlined in E140 (NoKnownEnrollTruant).'),
('E410','MedicalRsns','Student withdrew from/left school due to medical reasons.'),
('E450','PreK-6Exit','Infant or student in pre-kindergarten through grade six, or ungraded elementary, exited/withdrew from school; or the student exited school during a temporary break such as summer vacation or year-round intersession, but was expected to return to the same school after the break.'),
('E490','Summer or Intersession Exit','The student exited school during a temporary break such as summer vacation or year-round intersession, but was expected to return to the same school after the break.'),
('N420','NoShowSameSchl','Student completed an academic year at a school and did not return to the same school the following year when the student was expected to return and no other exit code is appropriate.'),
('N470','NoShow','The student’s enrollment was exited because the student was pre-enrolled in a school but did not show up as expected to attend the school. This exit represents a nullification of the pre-enrollment.'),
('T160','TransCASchlRegular','The student withdrew from/left school and transferred (not referred by a school or district) to another California public school (within or outside the district). Transfers that are a result of referrals made by a school or district are to be coded as T165 or T167 as appropriate. The district has acceptable documentation of this transfer.'),
('T165','TransSpecDiscRsnsOrJudg','The student was withdrawn from one school due to specified disciplinary reasons, and transferred to another California public school (within or outside the district). The district has acceptable documentation of this transfer. The specified disciplinary reasons include: referral by a juvenile court judge or other correctional or judicial official; or expulsions pursuant to Education Code Section 48915 (a), (b), or (c) and where the student is known to be enrolled in another institution.Do NOT use this code if: - student is expelled and referred to an alternative education program for disciplinary reasons other than those specified in Education Code Section 48915 (a), (b), or (c). (use T167 [TransAltSchlPgm]) - an expelled student is NOT known to be enrolled in another institution (use E300 [ExpelledNoKnownEnroll]) - a truant student has not been attending and has been referred for truancy intervention (use E140 [NoKnownEnrollTruant]) - a student is referred by the school or district to an alternative education program for non-disciplinary reasons. (use T167 [TransAltSchlPrgml]) - a student is transferred voluntarily, without a referral and for non-disciplinary reasons. (use T160 [TransCASchlRegular])'),
('T167','TransAltSchlPrgm','The student was referred by a school and/or school district to enroll in an alternative education school or voluntarily transferred to an independent study program in another California public school in the same district or in a different district for one of the following reasons: The student was referred or voluntarily transferred to an independent study program for non-disciplinary reasons.ò The student was referred by school and/or school district to withdraw from/leave school and transfer to an alternative education school or to a non-alternative education school independent study program for any disciplinary reason except those specified in T165. Do NOT use this code for: - interdistrict transfers (i.e., formal agreement pursuant to Education Code Section 46600, NCLB public school choice-program improvement, NCLB public school choice-persistently dangerous, district of choice transfers pursuant to Education Code Section 48313) - students who are referred to an alternative education school or independent study program for disciplinary reasons by a juvenile court judge or other correctional or judicial officer or who are expelled pursuant to Education Code Section 48915 (a), (b), or (c). (Use T165)'),
('T180','TransPrivate','Student withdrew from/left school and the district has received acceptable documentation of enrollment in a private school in California.'),
('T200','TransUS','Student withdrew from/left school and the district has received acceptable documentation of enrollment in another public or private U.S. school outside California.'),
('T240','TransOutUS','Student withdrew from/left school to move to another country.'),
('T260','TransInAdult','Student withdrew from/left school to enroll in an adult education program and there is evidence that the student is in attendance and is working toward the completion of a GED certificate, California High School Exit Exam preparation courses, or high school diploma through an adult education program.'),
('T270','TransDropAdult','Student withdrew from/left school to enroll in an adult education program in order to obtain a GED certificate or high school diploma, but subsequently dropped out of the adult education program. Note: This code is to be used by the last secondary (non-adult education) school attended.'),
('T280','TransCollege','Student withdrew from/left school and the district has received acceptable documentation that the student is enrolled in college, working toward an Associate or Bachelor''s degree or taking California High School Exit Exam preparation courses.'),
('T310','TransHealthFacil','Student withdrew from/left school and entered a health care facility.'),
('T370','TransInstHSDipl','Student withdrew from/left school and entered an institution that is not primarily academic (military, job corps, justice system, etc.) and is in a secondary program leading toward a high school diploma.'),
('T380','TransInstNoHSDip','Student withdrew from/left school and entered an institution that is not primarily academic (military, job corps, justice system, etc.) and is not in a secondary program leading toward a high school diploma'),
('T460','TransHomeSchl','Student withdrew from/left school for a home school setting not affiliated with a private school or independent study program at a public school.');