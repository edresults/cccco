USE calpads;

GO

IF (
	EXISTS (
		SELECT
			1
		FROM
			sys.schemas s
			inner join
			sys.procedures p
				on s.schema_id = p.schema_id
		WHERE
			s.name = 'dbo'
			and p.name = 'p_year_grade_distribution'
		)
	)
	BEGIN
		DROP PROCEDURE dbo.p_year_grade_distribution;
	END;

GO

CREATE PROCEDURE
	dbo.p_year_grade_distribution
	(
		@district_code CHAR(7)
	)
AS

	DECLARE
		@sql NVARCHAR(MAX),
		@table_name SYSNAME;
		-- @district_code CHAR(7) = '3868478'; -- 'SFUSD'
		-- @district_code CHAR(7) = '3768296'; -- 'Poway'

	DECLARE
		@tbl
	AS TABLE
		(
			table_name SYSNAME
		);

	INSERT INTO
		@tbl
	VALUES
		('senr'),
		('sinf');

	DECLARE
		cur_tbl
	CURSOR FOR
		SELECT
			table_name
		FROM
			@tbl;

BEGIN
	-- open cursor
	OPEN cur_tbl;
	-- fetch first
	FETCH NEXT FROM
		cur_tbl
	INTO
		@table_name;
	-- iterate over cursor
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		
		SET @sql =
			'SELECT' + char(13) + char(10) +
			char(9) + 'year_code as Year,' + char(13) + char(10) +
			char(9) + 'grade_level_code as Grade,' + char(13) + char(10) +
			char(9) + 'count(*) as Count' + char(13) + char(10) +
			'FROM' + char(13) + char(10) +
			char(9) + 'calpads.dbo.' + @table_name + char(13) + char(10) +
			'WHERE' + char(13) + char(10) +
			char(9) + 'district_code = @district_code' + char(13) + char(10) +
			'GROUP BY' + char(13) + char(10) +
			char(9) + 'year_code,' + char(13) + char(10) +
			char(9) + 'grade_level_code' + char(13) + char(10) +
			'ORDER BY' + char(13) + char(10) +
			char(9) + 'year_code,' + char(13) + char(10) +
			char(9) + 'grade_level_code;';

		EXECUTE master.dbo.sp_executesql
			@sql,
			N'@district_code CHAR(7)',
			@district_code = @district_code;

		-- fetch first
		FETCH NEXT FROM
			cur_tbl
		INTO
			@table_name;
	END;

	CLOSE cur_tbl;
	DEALLOCATE cur_tbl;

END;