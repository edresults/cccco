USE calpass;

GO

IF (object_id('calpads.CurriculumDetail') is not null)
	BEGIN
		DROP TABLE calpads.CurriculumDetail;
	END;

GO

CREATE TABLE
	calpads.CurriculumDetail
	(
		CurriculumCode char(4) not null,
		Name varchar(255),
		CteSectorCode char(3) not null,
		CteSectorName varchar(255),
		CtePathwayCode char(3) not null,
		CteNontraditionalGender varchar(6),
		IsCteCapstone bit,
		IsCteAcademic bit,
		ContentAreaCode char(2),
		ContentAreaName varchar(255),
		IsAP bit,
		IsIB bit,
		GradeLevelCodeHigh varchar(2),
		GradeLevelCodeLow varchar(2),
		IsAG bit,
		IsNclb bit,
		AcademicYearStart char(9) not null,
		AcademicYearEnd char(9) not null,
		CurriculumType varchar(255),
		ContentAreaIntraRank tinyint
	);

GO

ALTER TABLE
	calpads.CurriculumDetail
ADD CONSTRAINT
	pk_CurriculumDetail
PRIMARY KEY CLUSTERED
	(
		CurriculumCode,
		AcademicYearStart,
		AcademicYearEnd,
		CteSectorCode,
		CtePathwayCode
	);