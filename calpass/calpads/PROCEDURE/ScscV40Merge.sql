USE calpass;

GO

IF (object_id('calpads.ScscV40Merge') is not null)
	BEGIN
		DROP PROCEDURE calpads.ScscV40Merge;
	END;

GO

CREATE PROCEDURE
	calpads.ScscV40Merge
	(
		@StageSchemaTableName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
	-- constants
	@SourceSchemaTableName nvarchar(517) = '[calpads].[Scsc]',
	-- variables
	@Message nvarchar(2048);

BEGIN
	-- process table
	IF (object_id(@StageSchemaTableName) is null)
		BEGIN
			SET @Message = N'Table not found';
			THROW 70099, @Message, 1;
		END;

	-- Delete production records with TransactionCode = 'D'
	-- Delete stage records with TransactionCode = 'D'
	EXECUTE calpads.TransactionCodeDelete
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;

	-- force stage table to adhere to production primary key
	EXECUTE dbo.TableAssimilate
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;

	-- merge stage table to production table
	EXECUTE dbo.TableMerge
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;
END;