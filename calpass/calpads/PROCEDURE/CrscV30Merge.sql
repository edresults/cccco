USE calpass;

GO

IF (object_id('calpads.CrscV30Merge') is not null)
	BEGIN
		DROP PROCEDURE calpads.CrscV30Merge;
	END;

GO

CREATE PROCEDURE
	calpads.CrscV30Merge
	(
		@StageSchemaTableName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
	-- constants
	@DefaultEmployeeStateId char(10) = '9999999999',
	@SourceSchemaTableName nvarchar(517) = '[calpads].[Crsc]',
	-- variables
	@Message nvarchar(2048),
	@RowCount int,
	@Sql nvarchar(max);

BEGIN
	-- process table
	IF (object_id(@StageSchemaTableName) is null)
		BEGIN
			SET @Message = N'Table not found';
			THROW 70099, @Message, 1;
		END;

	-- update defaults
	SET @Sql = N'
		UPDATE
			' + @StageSchemaTableName + '
		SET
			EmployeeStateId = @DefaultEmployeeStateId
		WHERE
			EmployeeStateId is null;
		
		SET @RowCount = @@ROWCOUNT;';

	-- update defaults
	EXECUTE sp_executesql
		@Sql,
		N'@DefaultEmployeeStateId char(10),
			@RowCount int out',
		@DefaultEmployeeStateId = @DefaultEmployeeStateId,
		@RowCount = @RowCount out;

	-- update message
	SET @Message = convert(nvarchar, @RowCount) + ' Records updated with EmployeeStateId = ' + @DefaultEmployeeStateId;
	-- output message
	PRINT @Message;
	
	-- Delete production records with TransactionCode = 'D'
	-- Delete stage records with TransactionCode = 'D'
	EXECUTE calpads.TransactionCodeDelete
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;

	-- force stage table to adhere to production primary key
	EXECUTE dbo.TableAssimilate
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;

	-- merge stage table to production table
	EXECUTE dbo.TableMerge
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;
END;