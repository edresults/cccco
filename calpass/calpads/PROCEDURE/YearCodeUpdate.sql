USE calpass;

GO

IF (object_id('calpads.YearCodeUpdate') is not null)
	BEGIN
		DROP PROCEDURE calpads.YearCodeUpdate;
	END;

GO

CREATE PROCEDURE
	calpads.YearCodeUpdate
	(
		@StageName nvarchar(517),
		@IsoDateColumnName sysname = N''
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Message nvarchar(2048),
	@DefaultYearCode char(9),
	@RowCount int,
	@Sql nvarchar(max);

BEGIN
	-- validate stage table
	IF (object_id(@StageName) is null)
		BEGIN
			SET @Message = N'Table not found';
			THROW 70099, @Message, 1;
		END;

	-- validate date column
	IF (
		@IsoDateColumnName != N''
		and 
		not exists(SELECT 1 FROM sys.columns WHERE object_id = object_id(@StageName) and name = @IsoDateColumnName)
		)
		BEGIN
			SET @Message = N'IsoDateColumn not found';
			THROW 70099, @Message, 1;
		END;
	ELSE IF (@IsoDateColumnName = N'')
		BEGIN
			SET @Sql = N'
				UPDATE
					t
				SET
					t.YearCode = @DefaultYearCode
				FROM
					' + @StageName + ' t
				WHERE
					t.YearCode is null;
					
				SET @RowCount = @@ROWCOUNT;';
		END;
	ELSE
		BEGIN
			SET @Sql = N'
				UPDATE
					t
				SET
					t.YearCode = coalesce(y.YearCode, @DefaultYearCode)
				FROM
					' + @StageName + ' t
					left outer join
					calpads.Year y
						on t.' + @IsoDateColumnName + ' >= y.IsoDateStart
						and t.' + @IsoDateColumnName + ' <= y.IsoDateEnd
				WHERE
					t.YearCode is null;
					
				SET @RowCount = @@ROWCOUNT;';
		END;

	-- get default YearCode
	SELECT
		@DefaultYearCode = y.YearCode
	FROM
		calpads.Year y
	WHERE
		y.YearTrailing = (
			SELECT
				min(y1.YearTrailing)
			FROM
				calpads.Year y1
		);

	-- update YearCode default
	EXECUTE sp_executesql
		@Sql,
		N'@DefaultYearCode char(9),
			@RowCount int out',
		@DefaultYearCode = @DefaultYearCode,
		@RowCount = @RowCount out;

	-- update output message
	SET @Message = convert(nvarchar, @RowCount) + ' Records updated with YearCode using ' + 
		isnull(@IsoDateColumnName, 'no date column') + ' or ' + @DefaultYearCode;
	-- output message
	PRINT @Message;
END;