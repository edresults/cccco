USE calpass;

GO

IF (object_id('calpads.EscrowGet') is not null)
	BEGIN
		DROP PROCEDURE calpads.EscrowGet;
	END;

GO

CREATE PROCEDURE
	calpads.EscrowGet
AS

	SET NOCOUNT ON;

DECLARE
	@Message nvarchar(2048),
	@IsEscrow bit = 1,
	@Algorithm char(8) = 'SHA2_512',
	@EscrowNameStudent sysname = 'K12StudentProd',
	@EscrowNameAward sysname = 'K12AwardProd',
	@EscrowNameCourse sysname = 'K12CourseProd',
	@EscrowNameProgram sysname = 'K12PrgProd',
	@EscrowNameCareer sysname = 'K12CteProd';

BEGIN
	BEGIN TRY
		WITH
			Escrow
			(
				StudentStateId,
				SchoolCode,
				YearCode,
				EscrowName
			)
		AS
			(
				-- Information
				SELECT
					f.StudentStateId,
					f.SchoolCode,
					f.YearCode,
					EscrowName
				FROM
					calpads.Sinf f with(tablockx)
					cross join
					(
						VALUES
						(@EscrowNameStudent),
						(@EscrowNameAward),
						(@EscrowNameCourse),
						(@EscrowNameProgram),
						(@EscrowNameCareer)
					) e (EscrowName)
				WHERE
					f.IsEscrow = @IsEscrow
				UNION
				-- Enrollment
				SELECT
					r.StudentStateId,
					r.SchoolCode,
					r.YearCode,
					EscrowName
				FROM
					calpads.Senr r with(tablockx)
					cross join
					(
						VALUES
						(@EscrowNameStudent),
						(@EscrowNameAward)
					) e (EscrowName)
				WHERE
					r.IsEscrow = @IsEscrow
				UNION
				-- Language
				SELECT
					l.StudentStateId,
					l.SchoolCode,
					l.YearCode,
					@EscrowNameStudent
				FROM
					calpads.Sela l
				WHERE
					l.IsEscrow = @IsEscrow
				UNION
				-- Section
				SELECT
					s.StudentStateId,
					s.SchoolCode,
					s.YearCode,
					@EscrowNameCourse
				FROM
					calpads.Scsc s with(tablockx, index(ix_Scsc__IsEscrow))
				WHERE
					s.IsEscrow = @IsEscrow
				UNION
				-- Course
				SELECT
					s.StudentStateId,
					s.SchoolCode,
					s.YearCode,
					@EscrowNameCourse
				FROM
					calpads.Scsc s with(tablockx, index(ix_Scsc__Section))
				WHERE
					exists (
						SELECT
							1
						FROM
							calpads.Crsc c with(tablockx, index(ix_Crsc__IsEscrow))
						WHERE
							c.IsEscrow = @IsEscrow
							and c.SectionId = s.SectionId
							and c.CourseId = s.CourseId
							and c.TermCode = s.TermCode
							and c.YearCode = s.YearCode
							and c.SchoolCode = s.SchoolCode
					)
				UNION
				-- Program
				SELECT
					p.StudentStateId,
					p.SchoolCode,
					p.YearCode,
					@EscrowNameProgram
				FROM
					calpads.Sprg p with(tablockx)
				WHERE
					p.IsEscrow = @IsEscrow
				UNION
				-- Career
				SELECT
					t.StudentStateId,
					t.SchoolCode,
					t.YearCode,
					@EscrowNameCareer
				FROM
					calpads.Scte t with(tablockx)
				WHERE
					t.IsEscrow = @IsEscrow
			)
		SELECT
			StudentStateId = e.StudentStateId,
			SchoolCode = e.SchoolCode,
			YearCode = e.YearCode,
			EscrowName = e.EscrowName,
			InterSegmentKey = 
				HASHBYTES(
					@Algorithm,
					upper(convert(nchar(3), f.NameFirst)) + 
					upper(convert(nchar(3), f.NameLast)) + 
					upper(convert(nchar(1), f.Gender)) + 
					convert(nchar(8), f.Birthdate)
				),
			SinfYearCode = f.YearCode,
			SinfEffectiveStartDate = f.EffectiveStartDate
		FROM
			Escrow e
			inner join
			calpads.Sinf f with(index(pk_Sinf))
				on f.StudentStateId = e.StudentStateId
				and f.SchoolCode = e.SchoolCode
		WHERE
			f.EffectiveStartDate = (
				SELECT
					max(f1.EffectiveStartDate)
				FROM
					calpads.Sinf f1 with(index(pk_Sinf))
				WHERE
					f1.StudentStateId = f.StudentStateId
					and f1.SchoolCode = f.SchoolCode
					and f1.YearCode = f.YearCode
					and f1.YearCode = (
						SELECT
							max(f2.YearCode)
						FROM
							calpads.Sinf f2 with(index(pk_Sinf))
						WHERE
							f2.StudentStateId = f1.StudentStateId
							and f2.SchoolCode = f1.SchoolCode
							and f2.YearCode <= e.YearCode
					)
			)
            and HASHBYTES(
                @Algorithm,
                upper(convert(nchar(3), f.NameFirst)) + 
                upper(convert(nchar(3), f.NameLast)) + 
                upper(convert(nchar(1), f.Gender)) + 
                convert(nchar(8), f.Birthdate)
            ) is not null;
	END TRY
	BEGIN CATCH
		-- set error message
		SET @Message =
			nchar(9) + N'Error Procedure: ' + upper(object_name(@@PROCID)) + char(13) + char(10) +
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
	END CATCH;
END;