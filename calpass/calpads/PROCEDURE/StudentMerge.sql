USE calpass;

GO

IF (object_id('calpads.StudentMerge') is not null)
	BEGIN
		DROP PROCEDURE calpads.StudentMerge;
	END;

GO

CREATE PROCEDURE
	calpads.StudentMerge
AS

DECLARE
	@HashAlgorithm char(8) = 'SHA2_512',
	@EffectiveEndDate char(8) = '99999999';

BEGIN
	
	MERGE
		calpads.Student t
	USING
		(
			-- non-deterministic; unavoidable
			SELECT
				StudentStateId,
				InterSegmentKey = 
					HASHBYTES(
						@HashAlgorithm,
						upper(convert(nchar(3), NameFirst)) + 
						upper(convert(nchar(3), NameLast)) + 
						upper(convert(nchar(1), Gender)) + 
						convert(nchar(8), Birthdate)
					),
				InterSegmentKeyBug = 
					HASHBYTES(
						@HashAlgorithm,
						upper(convert(char(5), substring(NameFirst, 1, 1) + '\000')) + 
						upper(convert(char(5), substring(NameFirst, 2, 1) + '\000')) + 
						upper(convert(char(5), substring(NameFirst, 3, 1) + '\000')) + 
						upper(convert(char(5), substring(NameLast, 1, 1) + '\000')) + 
						upper(convert(char(5), substring(NameLast, 2, 1) + '\000')) + 
						upper(convert(char(5), substring(NameLast, 3, 1) + '\000')) + 
						upper(convert(char(5), Gender + '\000')) + 
						convert(char(5), substring(Birthdate, 1, 1) + '\000') + 
						convert(char(5), substring(Birthdate, 2, 1) + '\000') + 
						convert(char(5), substring(Birthdate, 3, 1) + '\000') + 
						convert(char(5), substring(Birthdate, 4, 1) + '\000') + 
						convert(char(5), substring(Birthdate, 5, 1) + '\000') + 
						convert(char(5), substring(Birthdate, 6, 1) + '\000') + 
						convert(char(5), substring(Birthdate, 7, 1) + '\000') + 
						convert(char(5), substring(Birthdate, 8, 1) + '\000')
					)
			FROM
				(
					SELECT
						StudentStateId,
						NameFirst,
						NameLast,
						Gender,
						Birthdate,
						NonDeterministicSelector = 
							row_number()
							over(
								partition by
									StudentStateId
								order by
									-- most recent year
									YearCode desc,
									-- most recent effective record
									EffectiveStartDate desc,
									-- null indicates record is still active
									-- not null indicates record is no longer active
									-- not null used for historical updates
									isnull(EffectiveEndDate, @EffectiveEndDate) desc
							)
					FROM
						calpads.sinf s
					WHERE
						not exists (
							SELECT
								1
							FROM
								calpads.Student s1
							WHERE
								s1.StudentStateId = s.StudentStateId
						)
				) s
			WHERE
				NonDeterministicSelector = 1
		) s
	ON
		t.StudentStateId = s.StudentStateId
	WHEN MATCHED THEN
		UPDATE SET
			t.InterSegmentKey = s.InterSegmentKey,
			t.InterSegmentKeyBug = s.InterSegmentKeyBug
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				StudentStateId,
				InterSegmentKey,
				InterSegmentKeyBug
			)
		VALUES
			(
				s.StudentStateId,
				s.InterSegmentKey,
				s.InterSegmentKeyBug
			);
END;