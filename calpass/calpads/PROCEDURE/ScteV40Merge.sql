USE calpass;

GO

IF (object_id('calpads.ScteV40Merge') is not null)
	BEGIN
		DROP PROCEDURE calpads.ScteV40Merge;
	END;

GO

CREATE PROCEDURE
	calpads.ScteV40Merge
	(
		@StageSchemaTableName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
	-- constants
	@SourceSchemaTableName nvarchar(517) = '[calpads].[Scte]',
	-- variables
	@Message nvarchar(2048),
	@DefaultYearCode char(9),
	@RowCount int,
	@Sql nvarchar(max);

BEGIN
	-- process table
	IF (object_id(@StageSchemaTableName) is null)
		BEGIN
			SET @Message = N'Table not found';
			THROW 70099, @Message, 1;
		END;

	-- get default YearCode
	SELECT
		@DefaultYearCode = y.YearCode
	FROM
		calpads.Year y
	WHERE
		y.YearTrailing = (
			SELECT
				min(y1.YearTrailing)
			FROM
				calpads.Year y1
		);
	
	-- update YearCode default
	SET @Sql = N'
		UPDATE
			e
		SET
			e.YearCode = coalesce(e.CtePathwayCompletionYearCode, @DefaultYearCode)
		FROM
			' + @StageSchemaTableName + ' e
		WHERE
			e.YearCode is null;
			
		SET @RowCount = @@ROWCOUNT;';

	-- update YearCode default
	EXECUTE sp_executesql
		@Sql,
		N'@DefaultYearCode char(9),
			@RowCount int out',
		@DefaultYearCode = @DefaultYearCode,
		@RowCount = @RowCount out;

	-- update output message
	SET @Message += convert(nvarchar, @RowCount) + ' Records updated with YearCode using CtePathwayCompletionYearCode or ' + @DefaultYearCode;
	-- output message
	PRINT @Message;

	-- force stage table to adhere to production primary key
	EXECUTE dbo.TableAssimilate
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;

	-- merge stage table to production table
	EXECUTE dbo.TableMerge
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;
END;