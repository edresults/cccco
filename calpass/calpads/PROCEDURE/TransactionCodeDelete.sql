USE calpass;

GO

IF (object_id('calpads.TransactionCodeDelete') is not null)
	BEGIN
		DROP PROCEDURE calpads.TransactionCodeDelete;
	END;

GO

CREATE PROCEDURE
	calpads.TransactionCodeDelete
	(
		@ProductionName nvarchar(517),
		@StageName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

	-- Delete - This transaction type requires a “D” (Delete) be submitted in the transaction type code field.
	-- Records with this transaction are processed one at a time.
	-- The system uses the operational key of each record to look for a record with the same operational key.
	-- If a record with the same operational key already exists in the Operational Data Store (ODS) then the record is deleted.
	-- If no record exists with the same operational key then a fatal error for the record is generated and the file processing continues.

DECLARE
	@Message nvarchar(2048),
	@TransactionCodeDelete char(1) = 'D',
	@Join nvarchar(4000) = N'',
	@RowCount int,
	@Sql nvarchar(max);

BEGIN
	-- compare names
	IF (@ProductionName = @StageName)
		BEGIN
			SET @Message = N'Production Table Name and Stage Table Name must not be the same';
			THROW 70099, @Message, 1;
		END;

	-- validate production table
	IF (object_id(@ProductionName) is null)
		BEGIN
			SET @Message = N'Table not found';
			THROW 70099, @Message, 1;
		END;

	-- validate stage table
	IF (object_id(@StageName) is null)
		BEGIN
			SET @Message = N'Table not found';
			THROW 70099, @Message, 1;
		END;

	-- collect key columns
	SELECT
		@Join +=
			case
				when ic.key_ordinal = min(ic.key_ordinal) over() then N''
				else N' and '
			end
			+ N's.' + c.Name + ' = t.' + c.Name
	FROM
		sys.indexes i
		inner join
		sys.index_columns ic
			on i.object_id = ic.object_id
			and i.index_id = ic.index_id
		inner join
		sys.columns c
			on ic.object_id = c.object_id
			and c.column_id = ic.column_id
		inner join
		sys.columns sc
			on sc.name = c.name
	WHERE
		i.object_id = object_id(@ProductionName)
		and i.is_primary_key = 1
		and sc.object_id = object_id(@StageName)
		and c.is_identity = 0
	ORDER BY
		ic.key_ordinal;

	SET @Sql = N'
		DELETE
			t
		FROM
			' + @ProductionName + ' t
			inner join
			' + @StageName + ' s
				on ' + @Join + '
		WHERE
			s.TransactionTypeCode = @TransactionCodeDelete;
		
		SET @RowCount = @@ROWCOUNT;
		
		DELETE
		FROM
			' + @StageName + '
		WHERE
			TransactionTypeCode = @TransactionCodeDelete;';

	-- delete deleted transactions
	EXECUTE sp_executesql
		@Sql,
		N'@TransactionCodeDelete char(1),
			@RowCount int out',
		@TransactionCodeDelete = @TransactionCodeDelete,
		@RowCount = @RowCount out;

	-- update message
	SET @Message = convert(nvarchar, @RowCount) + ' Records deleted with TransactionTypeCode = ' + @TransactionCodeDelete;
	-- output message
	PRINT @Message;
END;