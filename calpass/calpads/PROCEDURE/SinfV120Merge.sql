USE calpass;

GO

IF (object_id('calpads.SinfV120Merge') is not null)
	BEGIN
		DROP PROCEDURE calpads.SinfV120Merge;
	END;

GO

CREATE PROCEDURE
	calpads.SinfV120Merge
	(
		@StageSchemaTableName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
	-- constants
	@SourceSchemaTableName nvarchar(517) = '[calpads].[Sinf]',
	@IsoDateColumnName sysname = 'EffectiveStartDate',
	-- variables
	@Message nvarchar(2048);

BEGIN
	-- validate table
	IF (object_id(@StageSchemaTableName) is null)
		BEGIN
			SET @Message = N'Table not found';
			THROW 70099, @Message, 1;
		END;

	-- update null YearCode values
	EXECUTE calpads.YearCodeUpdate
		@StageName = @StageSchemaTableName,
		@IsoDateColumnName = @IsoDateColumnName;

	-- Delete production records with TransactionCode = 'D'
	-- Delete stage records with TransactionCode = 'D'
	EXECUTE calpads.TransactionCodeDelete
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;

	-- force stage table to adhere to production primary key
	EXECUTE dbo.TableAssimilate
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;

	-- mrege stage table to production table
	EXECUTE dbo.TableMerge
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;
END;