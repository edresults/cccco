USE calpass;

GO

IF (object_id('calpads.PstsV110Merge') is not null)
	BEGIN
		DROP PROCEDURE calpads.PstsV110Merge;
	END;

GO

CREATE PROCEDURE
	calpads.PstsV110Merge
	(
		@StageSchemaTableName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
    @sql nvarchar(max) = N'';

BEGIN

    SET @sql = N'
INSERT INTO
    calpads.PSTS
    (
        ReportingLEA,
        SchoolofAttendance,
        SchoolofAttendanceNPS,
        AcademicYearID,
        SSID,
        ReportingSELPA,
        EducationProgramParticipationTypeCode,
        PostsecondaryStatusCode
    )
SELECT
    ReportingLEA,
    SchoolofAttendance,
    SchoolofAttendanceNPS,
    AcademicYearID,
    SSID,
    ReportingSELPA,
    EducationProgramParticipationTypeCode,
    PostsecondaryStatusCode
FROM
    ' + @StageSchemaTableName + ';'

    EXECUTE sp_executesql
        @Sql;

END;