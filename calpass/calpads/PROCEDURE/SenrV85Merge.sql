USE calpass;

GO

IF (object_id('calpads.SenrV85Merge') is not null)
	BEGIN
		DROP PROCEDURE calpads.SenrV85Merge;
	END;

GO

CREATE PROCEDURE
	calpads.SenrV85Merge
	(
		@StageSchemaTableName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
	-- constants
	@YearTrailingThreshold smallint = 2014,
	@SourceSchemaTableName nvarchar(517) = '[calpads].[Senr]',
	-- variables
	@Message nvarchar(2048),
	@RowCount int,
	@Sql nvarchar(max),
	@YearCode char(9),
	@YearTrailingFile smallint,
	@Threshold tinyint = '15';

BEGIN
	-- process table
	IF (object_id(@StageSchemaTableName) is null)
		BEGIN
			SET @Message = N'Table not found';
			THROW 70099, @Message, 1;
		END;

	-- Delete production records with TransactionCode = 'D'
	-- Delete stage records with TransactionCode = 'D'
	EXECUTE calpads.TransactionCodeDelete
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;

	-- -- get year of file
	SET @Sql = N'
		WITH
			Baseline
			(
				YearCode,
				Records
			)
		AS
			(
				SELECT
					YearCode,
					Records = count(*)
				FROM
					' + @StageSchemaTableName + '
				GROUP BY
					YearCode
			),
			Aggregates
		AS
			(
				SELECT
					Groups = count(YearCode),
					Total = sum(Records)
				FROM
					Baseline

			),
			Threshold
		AS
			(
				SELECT
					YearCode,
					Records,
					Total,
					Pct = convert(decimal(5,2), 100.00 * Records / Total)
				FROM
					Aggregates a
					cross join
					Baseline b
				WHERE
					convert(decimal(5,2), 100.00 * Records / Total) >= @Threshold
			)
		SELECT
			@YearCode = max(YearCode)
		FROM
			Threshold;'
	-- set YearCode
	EXECUTE sp_executesql
		@Sql,
		N'@Threshold decimal(3,0),
			@YearCode char(9) out',
		@Threshold = @Threshold,
		@YearCode = @YearCode out;

	-- YearCode
	IF (@YearCode is null)
		BEGIN
			SET @Message = N'YearCode could not be determined';
			THROW 70099, @Message, 1;
		END;

	-- Update YearCode in SENR table
	SET @Sql = N'
		UPDATE
			' + @StageSchemaTableName + '
		SET
			YearCode = @YearCode;';

	-- set YearCode
	EXECUTE sp_executesql
		@Sql,
		N'@YearCode char(9)',
		@YearCode = @YearCode;

	-- set year trailing 
	SELECT
		@YearTrailingFile = YearTrailing
	FROM
		calpads.Year
	WHERE
		YearCode = @YearCode;

	-- process YearCode
	IF (@YearTrailingFile < @YearTrailingThreshold)
		BEGIN
			-- realign grade_level_code for students without good data
			-- assume valid grade_level_code for invalid year_code
			-- i.e., increase grade_level_code then update year_code
			SET @Sql = N'
				UPDATE
					b
				SET
					b.GradeCode = f.GradeCode,
					b.YearCode = @YearCode
				FROM
					' + @StageSchemaTableName + ' b
					inner join
					calpads.Year y
						on b.EnrollExitDate >= y.IsoDateStart
						and b.EnrollExitDate <= y.IsoDateEnd
					inner join
					calpads.Grade g
						on g.GradeCode = b.GradeCode
					inner join
					calpads.Grade f
						on f.Rank = g.Rank - (y.YearTrailing - @YearTrailingFile);
				
				SET @RowCount = @@ROWCOUNT;';
			-- exe grade fix
			EXECUTE sp_executesql
				@Sql,
				N'@YearCode char(9),
					@YearTrailingFile smallint,
					@RowCount int out',
				@YearCode = @YearCode,
				@YearTrailingFile = @YearTrailingFile,
				@RowCount = @RowCount out;
			-- update message
			SET @Message = convert(nvarchar, @RowCount) + ' Stage Records updated fixing GradeCode and YearCode';
			-- output message
			PRINT @message
		END;

	-- force stage table to adhere to production primary key
	EXECUTE dbo.TableAssimilate
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;

	-- merge stage table to production table
	EXECUTE dbo.TableMerge
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;
END;