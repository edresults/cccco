USE calpass;

GO

IF (object_id('calpads.SprgV30Merge') is not null)
	BEGIN
		DROP PROCEDURE calpads.SprgV30Merge;
	END;

GO

CREATE PROCEDURE
	calpads.SprgV30Merge
	(
		@StageSchemaTableName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
	-- constants
	@SourceSchemaTableName nvarchar(517) = '[calpads].[Sprg]',
	@IsoDateColumnName sysname = 'MembershipStartDate',
	@DefaultServiceCode char(2) = 'XX',
	-- variables
	@Message nvarchar(2048),
	@DefaultYearCode char(9),
	@RowCount int,
	@Sql nvarchar(max);

BEGIN
	-- process table
	IF (object_id(@StageSchemaTableName) is null)
		BEGIN
			SET @Message = N'Table not found';
			THROW 70099, @Message, 1;
		END;

	-- update null YearCode values
	EXECUTE calpads.YearCodeUpdate
		@StageName = @StageSchemaTableName,
		@IsoDateColumnName = @IsoDateColumnName;

	-- update DefaultServiceCode
	SET @Sql = N'
		UPDATE
			' + @StageSchemaTableName + '
		SET
			ServiceCode = @DefaultServiceCode
		WHERE
			ServiceCode is null;
			
		SET @RowCount = @@ROWCOUNT;';
		
	-- update DefaultServiceCode
	EXECUTE sp_executesql
		@Sql,
		N'@DefaultServiceCode char(2),
			@RowCount int out',
		@DefaultServiceCode = @DefaultServiceCode,
		@RowCount = @RowCount out;

		-- output message
	SET @Message = convert(nvarchar, @RowCount) + ' Records updated with ServiceCode = ' + @DefaultServiceCode;
	-- output
	PRINT @Message;
	
	-- Delete production records with TransactionCode = 'D'
	-- Delete stage records with TransactionCode = 'D'
	EXECUTE calpads.TransactionCodeDelete
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;

	-- force stage table to adhere to production primary key
	EXECUTE dbo.TableAssimilate
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;

	-- mrege stage table to production table
	EXECUTE dbo.TableMerge
		@ProductionName = @SourceSchemaTableName,
		@StageName = @StageSchemaTableName;
END;