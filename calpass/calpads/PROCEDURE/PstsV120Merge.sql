USE calpass;

GO

IF (object_id('calpads.PstsV120Merge') is not null)
	BEGIN
		DROP PROCEDURE calpads.PstsV120Merge;
	END;

GO

CREATE PROCEDURE
	calpads.PstsV120Merge
	(
		@StageSchemaTableName nvarchar(517)
	)
AS

	SET NOCOUNT ON;

DECLARE
    @sql nvarchar(max) = N'';

BEGIN

    SET @sql = N'
INSERT INTO
    calpads.PSTS
    (
        ReportingLEA,
        SchoolofAttendance,
        SchoolofAttendanceNPS,
        AcademicYearID,
        SSID,
        ReportingSELPA,
        EducationProgramParticipationTypeCode,
        PostsecondaryStatusCode,
        EducationalInstitutionType,
        IndustryField,
        PostHighSchoolCredential
    )
SELECT
    ReportingLEA,
    SchoolofAttendance,
    SchoolofAttendanceNPS,
    AcademicYearID,
    SSID,
    ReportingSELPA,
    EducationProgramParticipationTypeCode,
    PostsecondaryStatusCode,
    EducationalInstitutionType,
    IndustryField,
    PostHighSchoolCredential
FROM
    ' + @StageSchemaTableName + ';'

    EXECUTE sp_executesql
        @Sql;

END;