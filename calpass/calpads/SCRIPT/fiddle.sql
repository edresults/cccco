using System;
using System.Security.Cryptography;
using System.Text;

public class Program
{
	public static void Main()
	{
		string nameFirst = "";
		string nameLast = "";
		string gender = "";
		string birthdate = "";
		
		nameFirst = nameFirst.Substring(0, 3).ToUpper();
		Console.WriteLine("First Name:  " + nameFirst);
		
		nameLast = nameLast.Substring(0, 3).ToUpper();
		Console.WriteLine("Last Name:   " + nameLast);
		
		gender = gender.Substring(0, 1).ToUpper();
		Console.WriteLine("Gender:      " + gender);
		
		Console.WriteLine("Birthdate:   " + birthdate);
		
		string payload = nameFirst + nameLast + gender + birthdate;
		Console.WriteLine("Payload:     " + payload);

		SHA512 shaM = new SHA512Managed();
		
		Console.WriteLine(Environment.NewLine + "Encoding:    " + Encoding.Unicode.WebName);
		byte[] encoding = Encoding.Unicode.GetBytes(payload);
		Console.WriteLine("Byte Array:  " + ByteArrayToHexViaLookup32(encoding));
		byte[] output = shaM.ComputeHash(encoding);
		Console.WriteLine("Hash Output: " + ByteArrayToHexViaLookup32(output));
		
		Console.WriteLine(Environment.NewLine + "Encoding:    " + Encoding.UTF8.WebName);
		encoding = Encoding.UTF8.GetBytes(payload);
		Console.WriteLine("Byte Array:  " + ByteArrayToHexViaLookup32(encoding));
		output = shaM.ComputeHash(encoding);
		Console.WriteLine("Hash Output: " + ByteArrayToHexViaLookup32(output));
		
		Console.WriteLine(Environment.NewLine + "Encoding:    " + Encoding.Default.WebName);		
		encoding = Encoding.Default.GetBytes(payload);
		Console.WriteLine("Byte Array:  " + ByteArrayToHexViaLookup32(encoding));
		output = shaM.ComputeHash(encoding);
		Console.WriteLine("Hash Output: " + ByteArrayToHexViaLookup32(output));
		
		Console.WriteLine(Environment.NewLine + "Encoding:    " + Encoding.UTF7.WebName);
		encoding = Encoding.UTF7.GetBytes(payload);
		Console.WriteLine("Byte Array:  " + ByteArrayToHexViaLookup32(encoding));
		output = shaM.ComputeHash(encoding);
		Console.WriteLine("Hash Output: " + ByteArrayToHexViaLookup32(output));
		
		Console.WriteLine(Environment.NewLine + "Encoding:    " + Encoding.UTF32.WebName);
		encoding = Encoding.UTF32.GetBytes(payload);
		Console.WriteLine("Byte Array:  " + ByteArrayToHexViaLookup32(encoding));
		output = shaM.ComputeHash(encoding);
		Console.WriteLine("Hash Output: " + ByteArrayToHexViaLookup32(output));
		
		Console.WriteLine(Environment.NewLine + "Encoding:    " + Encoding.BigEndianUnicode.WebName);
		encoding = Encoding.BigEndianUnicode.GetBytes(payload);
		Console.WriteLine("Byte Array:  " + ByteArrayToHexViaLookup32(encoding));
		output = shaM.ComputeHash(encoding);
		Console.WriteLine("Hash Output: " + ByteArrayToHexViaLookup32(output));
		
		Console.WriteLine(Environment.NewLine + "Encoding:    " + Encoding.ASCII.WebName);
		encoding = Encoding.ASCII.GetBytes(payload);
		Console.WriteLine("Byte Array:  " + ByteArrayToHexViaLookup32(encoding));
		output = shaM.ComputeHash(encoding);
		Console.WriteLine("Hash Output: " + ByteArrayToHexViaLookup32(output));
	}
	
	private static readonly uint[] _lookup32 = CreateLookup32();
	
	private static uint[] CreateLookup32()
	{
		var result = new uint[256];
		for (int i = 0; i < 256; i++)
		{
			string s = i.ToString("X2");
			result[i] = ((uint)s[0]) + ((uint)s[1] << 16);
		}
		return result;
	}
	
	public static string ByteArrayToHexViaLookup32(byte[] bytes)
	{
		var lookup32 = _lookup32;
		var result = new char[bytes.Length * 2];
		for (int i = 0; i < bytes.Length; i++)
		{
			var val = lookup32[bytes[i]];
			result[2 * i] = (char)val;
			result[2 * i + 1] = (char)(val >> 16);
		}
		return new string(result);
	}
}