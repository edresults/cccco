DECLARE
    @UserId                      integer          = 5552,
    -- @FileFolder                  varchar(255)     = '\\10.11.4.21\WebFileUploads\FileDrop\',
    -- @OrganizationId              integer          = 161617,
    @FileFolder                  varchar(255)     = '\\10.11.6.43\c$\Data\CC\COMIS\Processing\',
    @OrganizationId              integer          = 175722,
    -- @BatchId                     uniqueidentifier = newid();
    @BatchId                     uniqueidentifier = 'BFBA7A9C-1720-4CE3-93E6-2D3A0DC02AC2';

INSERT
    dbo.calpassplus_filedrop
    (
        OrganizationId,
        BatchId,
        SubmissionDateTime,
        ProcessedDateTime,
        Status,
        FileLocation,
        OriginalFileName,
        TempFileName,
        ErrorMessage,
        UserId,
        SubmissionFileSource,
        Removed,
        SubmissionFileDetails,
        SubmissionFileDescriptionId
    )
SELECT
    OrganizationId              = @OrganizationId,
    BatchId                     = @BatchId,
    SubmissionDateTime          = getdate(),
    ProcessedDateTime           = null,
    Status                      = 'Escrow',
    FileLocation                = @FileFolder,
    OriginalFileName            = convert(char(36), @BatchId) + '.' + FileName,
    TempFileName                = @FileFolder + 
                                    case
                                        when @OrganizationId = 175722 then convert(char(36), @BatchId) + '.' + FileName
                                        else convert(char(6), @OrganizationId) + char(92) + convert(char(36), @BatchId) + '.' + FileName
                                    end,
    ErrorMessage                = null,
    UserId                      = @UserId,
    SubmissionFileSource        = 'Manual',
    Removed                     = null,
    SubmissionFileDetails       = null,
    SubmissionFileDescriptionId = SubmissionFileDescriptionId
FROM
    (
        VALUES
        (11, 'CCCCO-DAS_MOUdata-20210129.csv')
    ) t
    (
        SubmissionFileDescriptionId,
        FileName
    );

SELECT * FROM calpassplus_filedrop WHERE BatchId = @BatchId;