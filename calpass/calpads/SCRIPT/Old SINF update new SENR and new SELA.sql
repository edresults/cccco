	-- update Sela with old SINF records
	SET @Sql = replace(N'
		MERGE
			calpads.Sela t
		USING
			(
				SELECT
					DistrictCode,
					SchoolCode,
					YearCode,
					StudentStateId,
					ElaStatusCode,
					ElaStatusStartDate,
					PrimaryLanguageCode
				FROM
					(
						SELECT
							DistrictCode,
							SchoolCode,
							YearCode,
							StudentStateId,
							ElaStatusCode,
							ElaStatusStartDate,
							PrimaryLanguageCode,
							RecordNumber = 
								row_number() over(
									partition by
										StudentStateId,
										SchoolCode,
										YearCode,
										ElaStatusCode
									order by
										(select 1)
								)
						FROM
							(
								SELECT DISTINCT
									DistrictCode,
									SchoolCode,
									YearCode,
									StudentStateId,
									ElaStatusCode,
									ElaStatusStartDate,
									PrimaryLanguageCode
								FROM
									' + @StageSchemaTableName + '
								WHERE
									ElaStatusCode is not null
							) a
					) b
				WHERE
					b.RecordNumber = 1
			) s
		ON
			t.StudentStateId = s.StudentStateId
			and t.SchoolCode = s.SchoolCode
			and t.YearCode = s.YearCode
			and t.ElaStatusCode = s.ElaStatusCode
		WHEN MATCHED THEN
			UPDATE SET
				t.ElaStatusStartDate = s.ElaStatusStartDate,
				t.PrimaryLanguageCode = s.PrimaryLanguageCode,
				t.IsEscrow = @IsEscrow
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					DistrictCode,
					SchoolCode,
					YearCode,
					StudentStateId,
					ElaStatusCode,
					ElaStatusStartDate,
					PrimaryLanguageCode,
					IsEscrow
				)			
			VALUES
				(
					s.DistrictCode,
					s.SchoolCode,
					s.YearCode,
					s.StudentStateId,
					s.ElaStatusCode,
					s.ElaStatusStartDate,
					s.PrimaryLanguageCode,
					@IsEscrow
				);
				
			SET @RowCount = @@ROWCOUNT;', @Needle, @Replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@Sql,
		N'@IsEscrow bit,
			@RowCount int out',
		@IsEscrow = @IsEscrow,
		@RowCount = @RowCount out;
	-- update output message
	SET @Message += convert(nvarchar, @RowCount) + ' Records updated in Sela using Sinf' + nchar(13) + nchar(10);
	
	-- update Senr with old SINF records; only if Senr record exists
	SET @Sql = replace(N'
		MERGE
			calpads.Senr t
		USING
			(
				SELECT
					DistrictCode,
					SchoolCode,
					YearCode,
					StudentStateId,
					EffectiveStartDate,
					TransferCode,
					ResidenceDistrictCode
				FROM
					(
						SELECT
							DistrictCode,
							SchoolCode,
							YearCode,
							StudentStateId,
							EffectiveStartDate,
							TransferCode,
							ResidenceDistrictCode,
							RecordNumber = 
								row_number() over(
									partition by
										StudentStateId,
										SchoolCode,
										YearCode,
										EffectiveStartDate
									order by
										(select 1)
								)
						FROM
							(
								SELECT DISTINCT
									DistrictCode,
									SchoolCode,
									YearCode,
									StudentStateId,
									EffectiveStartDate,
									TransferCode,
									ResidenceDistrictCode
								FROM
									' + @StageSchemaTableName + '
								WHERE
									TransferCode is not null
									or ResidenceDistrictCode is not null
							) a
					) b
				WHERE
					b.RecordNumber = 1
			) s
		ON
			t.StudentStateId = s.StudentStateId
			and t.SchoolCode = s.SchoolCode
			and t.YearCode = s.YearCode
			and t.EnrollStartDate = s.EffectiveStartDate
		WHEN MATCHED THEN
			UPDATE SET
				t.TransferCode = s.TransferCode,
				t.ResidenceDistrictCode = s.ResidenceDistrictCode;
				
			SET @RowCount = @@ROWCOUNT;', @Needle, @Replace);
	-- exe dynamic sql
	EXECUTE sp_executesql
		@Sql,
		N'@IsEscrow bit,
			@RowCount int out',
		@IsEscrow = @IsEscrow,
		@RowCount = @RowCount out;
	-- update output message
	SET @Message += convert(nvarchar, @RowCount) + ' Records updated in Senr using Sinf' + nchar(13) + nchar(10);