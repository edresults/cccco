-- K12StudentProd audit
(1) How many students have invalid StudentStateId values? 2889699

-- SQL
SELECT
	YearCode,
	DistrictCode = left(school, 7),
	RecordCount = count(*)
FROM
	K12StudentProd
	inner join
	calpads.Year
		on YearCodeAbbr = AcYear
WHERE
	CSISNum like '%X%'
GROUP BY
	YearCode,
	left(school, 7)
ORDER BY
	YearCode descending,
	RecordCount descending;

(2) Of (1), How many can be updated with correct StudentStateId with past or future data using LocStudentId from the same School? 1340744

-- SQL
SELECT
	UpdateCount = count(*)
FROM
	K12StudentProd s
	inner join
	K12StudentProd s1
		on s.School = s1.School
		and s.LocStudentId = s1.LocStudentId
WHERE
	s.CSISNum like '%X%'
	and s1.CSISNum not like '%X%';

(3) Of (2), How many have Derkey1 Collision (i.e., A Derkey1 value shared by two or more unique students)? -- 0

-- SQL
SELECT
	DerkeyCollisionRate = convert(decimal(8,5), round(100.0 * sum(case when s.derkey1 != s.derkey1 then 1 else 0 end) / count(*), 5)),
	UpdateCount = count(*)
FROM
	K12StudentProd s
	inner join
	K12StudentProd s1
		on s.School = s1.School
		and s.LocStudentId = s1.LocStudentId
WHERE
	s.CSISNum like '%X%'
	and s1.CSISNum not like '%X%';

(4) UPDATE (3)

UPDATE
	s
SET
	s.CSISNum = s1.CSISNum
FROM
	K12StudentProd s
	inner join
	K12StudentProd s1
		on s.School = s1.School
		and s.LocStudentId = s1.LocStudentId
		and s.Derkey1 = s1.Derkey1
WHERE
	s.CSISNum like '%X%'
	and s1.CSISNum not like '%X%';

(5) After executing an UPDATE using (2), how many can be updated with correct StudentStateId with past or future data using LocStudentId from the same District and same Derkey1? 1148217

-- SQL
SELECT
	count(*)
FROM
	K12StudentProd s
	inner join
	K12StudentProd s1
		on left(s.School, 7) = left(s1.School, 7)
		and s.LocStudentId = s1.LocStudentId
		and s.Derkey1 = s1.Derkey1
WHERE
	s.CSISNum like '%X%'
	and s1.CSISNum not like '%X%';

(6) UPDATE (5)

UPDATE
	s
SET
	s.CSISNum = s1.CSISNum
FROM
	K12StudentProd s
	inner join
	K12StudentProd s1
		on left(s.School, 7) = left(s1.School, 7)
		and s.LocStudentId = s1.LocStudentId
		and s.Derkey1 = s1.Derkey1
WHERE
	s.CSISNum like '%X%'
	and s1.CSISNum not like '%X%';

(7) UPDATE using School and Derkey1

UPDATE
	s
SET
	s.CSISNum = s1.CSISNum
FROM
	K12StudentProd s
	inner join
	K12StudentProd s1
		on s.School = s1.School
		and s.Derkey1 = s1.Derkey1
WHERE
	s.CSISNum like '%X%'
	and s1.CSISNum not like '%X%';

(8) UPDATE using District and Derkey1

UPDATE
	s
SET
	s.CSISNum = s1.CSISNum
FROM
	K12StudentProd s
	inner join
	K12StudentProd s1
		on left(s.School, 7) = left(s1.School, 7)
		and s.Derkey1 = s1.Derkey1
WHERE
	s.CSISNum like '%X%'
	and s1.CSISNum not like '%X%';

SELECT
	YearCode,
	DistrictCode = left(school, 7),
	RecordCount = count(*),
	YearCount = sum(count(*)) over(partition by YearCode),
	TotalCount = sum(count(*)) over()
FROM
	K12StudentProd
	inner join
	calpads.Year
		on YearCodeAbbr = AcYear
WHERE
	CSISNum like '%X%'
GROUP BY
	YearCode,
	left(school, 7)
ORDER BY
	YearCode desc,
	RecordCount desc;
