USE calpass;

GO

IF (object_id('lbccpt.SchoolPathway_SchoolPathwayCohortUpload') is not null)
	BEGIN
		DROP TABLE lbccpt.SchoolPathway_SchoolPathwayCohortUpload;
	END;

GO

CREATE TABLE
	lbccpt.SchoolPathway_SchoolPathwayCohortUpload
	(
		ConsortiumId int,
		CdsCode nchar(14),
		ConsortiumSchoolId int,
		AcademicYear nchar(9),
		AcademicYearId int,
		SchoolPathwayCode nchar(3),
		SchoolPathwayId int,
		School_SchoolPathwayId int, -- confluence of consortium, site, year, pathway
		StudentIdState char(10),
		StudentIdLocal varchar(15),
		CohortYear int,
		IntersegmentalKey binary(64),
		CONSTRAINT
			pk_SchoolPathway_SchoolPathwayCohortUpload
		PRIMARY KEY CLUSTERED
			(
				ConsortiumId,
				CdsCode,
				AcademicYear,
				SchoolPathwayCode,
				StudentIdState
			),
		INDEX
			ix_SchoolPathway_SchoolPathwayCohortUpload__IntersegmentalKey
		NONCLUSTERED
			(
				IntersegmentalKey
			)
	);