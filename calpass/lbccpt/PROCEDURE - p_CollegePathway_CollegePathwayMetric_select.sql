USE calpass;

GO

IF (object_id('lbccpt.p_CollegePathway_CollegePathwayMetric_select') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_CollegePathway_CollegePathwayMetric_select;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_CollegePathway_CollegePathwayMetric_select
	(
		@ConsortiumName nvarchar(255),
		@AcademicYear char(9),
		@MetricId int
	)
AS

BEGIN
	SELECT
		College = o.Label,
		MetricId = cpm.Id,
		MetricName = cpm.Name,
		MetricLabel = cpm.Label,
		CollegePathwayMetricId = cpcpm.Id,
		CollegePathwayMetricValue = cpcpm.MetricValue
	FROM
		lbccpt.CollegePathwayMetric cpm
		left outer join
		(
			lbccpt.Consortium c
			inner join
			lbccpt.Consortium_College cs
				on c.Id = cs.ConsortiumId
				and c.Name = @ConsortiumName
			inner join
			lbccpt.College_CollegePathway ccp
				on cs.Id = ccp.ConsortiumCollegeId
			inner join
			lbccpt.AcademicYear y
				on y.Id = ccp.AcademicYearId
				and y.CodeCalpads = @AcademicYear
			inner join
			lbccpt.College o
				on o.Id = ccp.ConsortiumCollegeId
			inner join
			lbccpt.CollegePathway_CollegePathwayMetric cpcpm
				on cpcpm.CollegePathwayId = ccp.Id
		)
			on cpm.Id = cpcpm.CollegePathwayMetricId
	WHERE
		cpm.IsActive = 1
		and cpm.Id = @MetricId
	ORDER BY
		cpm.Ordinal,
		o.Id;
END;