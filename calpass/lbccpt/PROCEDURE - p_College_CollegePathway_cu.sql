USE calpass;

IF (object_id('lbccpt.p_College_CollegePathway_cu') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_College_CollegePathway_cu;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_College_CollegePathway_cu
	(
		@json_in nvarchar(max),
		@message nvarchar(2048) = N'' OUT
	)
AS

	SET NOCOUNT ON;

DECLARE
	@College_CollegePathway lbccpt.College_CollegePathway;

BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			INSERT INTO
				@College_CollegePathway
				(
					Id,
					ConsortiumCollegeId,
					Name,
					AcademicYearId
				)
			SELECT
				o.Id,
				o.ConsortiumCollegeId,
				o.Name,
				o.AcademicYearId
			FROM
				(
					-- merge data into consortium pathway for k12 organizations
					MERGE
						calpass.lbccpt.College_CollegePathway with(
							holdlock
						) t
					USING
						(
							SELECT
								Id = json_value(j.value, '$.Id'),
								ConsortiumCollegeId = json_value(j.value, '$.ConsortiumCollegeId'),
								Name = json_value(j.value, '$.Name'),
								AcademicYearId = json_value(j.value, '$.AcademicYearId')
							FROM
								openjson(@json_in, '$.CollegePathway') j
						) s
					ON
						t.Id = s.Id
					WHEN NOT MATCHED BY TARGET THEN
						INSERT
							(
								ConsortiumCollegeId,
								Name,
								AcademicYearId
							)
						VALUES
							(
								s.ConsortiumCollegeId,
								s.Name,
								s.AcademicYearId
							)
					WHEN MATCHED THEN
						UPDATE SET
							t.ConsortiumCollegeId = s.ConsortiumCollegeId,
							t.Name = s.Name,
							t.AcademicYearId = s.AcademicYearId
					OUTPUT
						inserted.Id,
						inserted.ConsortiumCollegeId,
						inserted.Name,
						inserted.AcademicYearId,
						$action
				) o (
					Id,
					ConsortiumCollegeId,
					Name,
					AcademicYearId,
					Action
				);
		END TRY
		BEGIN CATCH
			IF (@@TRANCOUNT > 0)
				BEGIN
					ROLLBACK TRANSACTION;
				END;
			-- SET @message = N'Error occurred when ' + @verb_present_participle + ' pathway';
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			THROW 70099, @message, 1;
		END CATCH;

		-- ToP codes
		BEGIN TRY
			MERGE
				calpass.lbccpt.CollegePathway_CollegePathwayTaxonomy with(
					holdlock
				) t
			USING
				(
					SELECT DISTINCT
						Id = json_value(c.value, '$.Id'),
						CollegePathwayId = tt.Id,
						CollegePathwayTaxonomyId = json_value(c.value, '$.CollegePathwayTaxonomyId')
					FROM
						@College_CollegePathway tt
						cross apply
						openjson(@json_in, '$.CollegePathway') j
						cross apply
						openjson(j.value, '$.CollegePathwayTaxonomies') c
				) s
			ON
				t.Id = s.Id
			WHEN NOT MATCHED BY TARGET THEN
				INSERT
					(
						CollegePathwayId,
						CollegePathwayTaxonomyId
					)
				VALUES
					(
						s.CollegePathwayId,
						s.CollegePathwayTaxonomyId
					)
			WHEN NOT MATCHED BY SOURCE
				and (exists (SELECT 1 FROM @College_CollegePathway tt WHERE t.CollegePathwayId = tt.Id)) THEN
				DELETE;
		END TRY
		BEGIN CATCH
			IF (@@TRANCOUNT > 0)
				BEGIN
					ROLLBACK TRANSACTION;
				END;
			-- SET @message = N'Error occurred when attempting to ' + @verb + N' Consortium Colleges';
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			THROW 70099, @message, 1;
		END CATCH;

		-- Courses
		BEGIN TRY
			MERGE
				calpass.lbccpt.CollegePathway_CollegePathwayCourse with(
					holdlock
				) t
			USING
				(
					SELECT DISTINCT
						Id = json_value(c.value, '$.Id'),
						CollegePathwayId = tt.Id,
						CollegePathwayCourseId = json_value(c.value, '$.CollegePathwayCourseId')
					FROM
						@College_CollegePathway tt
						cross apply
						openjson(@json_in, '$.CollegePathway') j
						cross apply
						openjson(j.value, '$.CollegePathwayCourses') c
				) s
			ON
				t.Id = s.Id
			WHEN NOT MATCHED BY TARGET THEN
				INSERT
					(
						CollegePathwayId,
						CollegePathwayCourseId
					)
				VALUES
					(
						s.CollegePathwayId,
						s.CollegePathwayCourseId
					)
			WHEN NOT MATCHED BY SOURCE
				and (exists (SELECT 1 FROM @College_CollegePathway tt WHERE t.CollegePathwayId = tt.Id)) THEN
				DELETE;
		END TRY
		BEGIN CATCH
			IF (@@TRANCOUNT > 0)
				BEGIN
					ROLLBACK TRANSACTION;
				END;
			-- SET @message = N'Error occurred when attempting to ' + @verb + N' Consortium Colleges';
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			THROW 70099, @message, 1;
		END CATCH;

		-- Students
		BEGIN TRY
			MERGE
				calpass.lbccpt.CollegePathway_CollegePathwayStudent with(
					holdlock
				) t
			USING
				(
					SELECT DISTINCT
						Id = json_value(c.value, '$.Id'),
						CollegePathwayId = tt.Id,
						CollegePathwayStudentId = json_value(c.value, '$.CollegePathwayStudentId')
					FROM
						@College_CollegePathway tt
						cross apply
						openjson(@json_in, '$.CollegePathway') j
						cross apply
						openjson(j.value, '$.CollegePathwayStudents') c
				) s
			ON
				t.Id = s.Id
			WHEN NOT MATCHED BY TARGET THEN
				INSERT
					(
						CollegePathwayId,
						CollegePathwayStudentId
					)
				VALUES
					(
						s.CollegePathwayId,
						s.CollegePathwayStudentId
					)
			WHEN NOT MATCHED BY SOURCE
				and (exists (SELECT 1 FROM @College_CollegePathway tt WHERE t.CollegePathwayId = tt.Id)) THEN
				DELETE;
		END TRY
		BEGIN CATCH
			IF (@@TRANCOUNT > 0)
				BEGIN
					ROLLBACK TRANSACTION;
				END;
			-- SET @message = N'Error occurred when attempting to ' + @verb + N' Consortium Colleges';
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			THROW 70099, @message, 1;
		END CATCH;

		-- Instructors
		BEGIN TRY
			MERGE
				calpass.lbccpt.CollegePathway_CollegePathwayInstructor with(
					holdlock
				) t
			USING
				(
					SELECT DISTINCT
						Id = json_value(c.value, '$.Id'),
						CollegePathwayId = tt.Id,
						CollegePathwayInstructorId = json_value(c.value, '$.CollegePathwayInstructorId')
					FROM
						@College_CollegePathway tt
						cross apply
						openjson(@json_in, '$.CollegePathway') j
						cross apply
						openjson(j.value, '$.CollegePathwayInstructors') c
				) s
			ON
				t.Id = s.Id
			WHEN NOT MATCHED BY TARGET THEN
				INSERT
					(
						CollegePathwayId,
						CollegePathwayInstructorId
					)
				VALUES
					(
						s.CollegePathwayId,
						s.CollegePathwayInstructorId
					)
			WHEN NOT MATCHED BY SOURCE
				and (exists (SELECT 1 FROM @College_CollegePathway tt WHERE t.CollegePathwayId = tt.Id)) THEN
				DELETE;
		END TRY
		BEGIN CATCH
			IF (@@TRANCOUNT > 0)
				BEGIN
					ROLLBACK TRANSACTION;
				END;
			-- SET @message = N'Error occurred when attempting to ' + @verb + N' Consortium Colleges';
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			THROW 70099, @message, 1;
		END CATCH;

	COMMIT TRANSACTION;
END;