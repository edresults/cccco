USE calpass;

GO

IF (object_id('lbccpt.p_SchoolPathway_SchoolPathwayMetric_select') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_SchoolPathway_SchoolPathwayMetric_select;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_SchoolPathway_SchoolPathwayMetric_select
	(
		@ConsortiumName nvarchar(255),
		@AcademicYear char(9),
		@SchoolPathwayCode char(3),
		@MetricId int
	)
AS

BEGIN
	SELECT
		School = s.Label,
		MetricId = spm.Id,
		MetricName = spm.Name,
		MetricLabel = spm.Label,
		SchoolPathwayMetricId = spspm.Id,
		SchoolPathwayMetricValue = spspm.MetricValue
	FROM
		lbccpt.SchoolPathwayMetric spm
		left outer join
		(
			lbccpt.Consortium c
			inner join
			lbccpt.Consortium_School cs
				on c.Id = cs.ConsortiumId
				and c.Name = @ConsortiumName
			inner join
			lbccpt.School_SchoolPathway ssp
				on cs.Id = ssp.ConsortiumSchoolId
			inner join
			lbccpt.AcademicYear y
				on y.Id = ssp.AcademicYearId
				and y.CodeCalpads = @AcademicYear
			inner join
			lbccpt.SchoolPathway sp
				on sp.Id = ssp.SchoolPathwayId
				and sp.Code = @SchoolPathwayCode
			inner join
			lbccpt.School s
				on s.Id = ssp.ConsortiumSchoolId
			left outer join
			lbccpt.SchoolPathway_SchoolPathwayMetric spspm
				on spspm.SchoolPathwayId = ssp.Id
		)
			on spm.Id = spspm.SchoolPathwayMetricId
	WHERE
		spm.IsActive = 1
		and spm.Id = @MetricId
	ORDER BY
		spm.Ordinal,
		s.Id;
END;