USE calpass;

GO

IF (object_id('lbccpt.CollegePathway_CollegePathwayStudent') is not null)
	BEGIN
		DROP TABLE lbccpt.CollegePathway_CollegePathwayStudent;
	END;

GO

CREATE TABLE
	lbccpt.CollegePathway_CollegePathwayStudent
	(
		Id int identity(1,1) not null,
		CollegePathwayId int not null,
		CollegePathwayStudentId int not null,
		CONSTRAINT
			pk_CollegePathway_CollegePathwayStudent__Id
		PRIMARY KEY
			(
				Id
			),
		CONSTRAINT
			uq_CollegePathway_CollegePathwayStudent__CollegePathwayId__CollegePathwayStudentId
		UNIQUE
			(
				CollegePathwayId,
				CollegePathwayStudentId
			),
		CONSTRAINT
			fk_CollegePathway_CollegePathwayStudent__CollegePathwayId
		FOREIGN KEY
			(
				CollegePathwayId
			)
		REFERENCES
			lbccpt.College_CollegePathway
			(
				Id
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_CollegePathway_CollegePathwayStudent_CollegePathwayStudentId
		FOREIGN KEY
			(
				CollegePathwayStudentId
			)
		REFERENCES
			lbccpt.CollegePathwayStudent
			(
				Id
			)
		ON DELETE CASCADE
	);