USE calpass;

GO

IF (object_id('lbccpt.CollegePathway_CollegePathwayCourse') is not null)
	BEGIN
		DROP TABLE lbccpt.CollegePathway_CollegePathwayCourse;
	END;

GO

CREATE TABLE
	lbccpt.CollegePathway_CollegePathwayCourse
	(
		Id int identity(1,1) not null,
		CollegePathwayId int not null,
		CollegePathwayCourseId int not null,
		CONSTRAINT
			pk_CollegePathway_CollegePathwayCourse__Id
		PRIMARY KEY
			(
				Id
			),
		CONSTRAINT
			uq_CollegePathway_CollegePathwayCourse__CollegePathwayId__CollegePathwayCourseId
		UNIQUE
			(
				CollegePathwayId,
				CollegePathwayCourseId
			),
		CONSTRAINT
			fk_CollegePathway_CollegePathwayCourse__CollegePathwayId
		FOREIGN KEY
			(
				CollegePathwayId
			)
		REFERENCES
			lbccpt.College_CollegePathway
			(
				Id
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_CollegePathway_CollegePathwayCourse_CollegePathwayCourseId
		FOREIGN KEY
			(
				CollegePathwayCourseId
			)
		REFERENCES
			lbccpt.CollegePathwayCourse
			(
				Id
			)
		ON DELETE CASCADE
	);