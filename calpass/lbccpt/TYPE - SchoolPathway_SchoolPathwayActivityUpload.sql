USE calpass;

GO

IF (type_id('lbccpt.SchoolPathway_SchoolPathwayActivityUpload') is not null)
	BEGIN
		DROP TYPE lbccpt.SchoolPathway_SchoolPathwayActivityUpload;
	END;

GO

CREATE TYPE
	lbccpt.SchoolPathway_SchoolPathwayActivityUpload
AS TABLE
	(
		ConsortiumId int,
		CdsCode char(14),
		ConsortiumSchoolId int,
		AcademicYear char(9),
		AcademicYearId int,
		SchoolPathwayCode char(3),
		SchoolPathwayId int,
		School_SchoolPathwayId int,
		course_00 int,
		course_01 int,
		course_02 int,
		course_03 int,
		course_04 int,
		course_05 int,
		student_00 int,
		student_01 int,
		student_02 int,
		student_03 int,
		instructor_00 int,
		instructor_01 int,
		instructor_02 int,
		instructor_03 int
	);