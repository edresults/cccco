USE calpass;

GO

IF (object_id('lbccpt.SchoolPathwayStudent') is not null)
	BEGIN
		DROP TABLE lbccpt.SchoolPathwayStudent;
	END;

GO

CREATE TABLE
	lbccpt.SchoolPathwayStudent
	(
		Id int identity(0,1) not null,
		Name nvarchar(255) not null,
		Label nvarchar(255) not null,
		IsActive bit not null,
		Ordinal int not null,
		CONSTRAINT
			pk_SchoolPathwayStudent__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		INDEX
			ix_SchoolPathwayStudent__IsActive
		NONCLUSTERED
			(
				IsActive
			)
	);

GO

INSERT INTO
	lbccpt.SchoolPathwayStudent
	(
		Name,
		Label,
		Ordinal,
		IsActive
	)
SELECT
	Name,
	Label,
	Ordinal = row_number() over(order by Name),
	IsActive
FROM
	(
		VALUES
		('student_00','Career guidance/counseling (e.g., career navigation, job-seeking, resumewriting, career fairs)',1),
		('student_01','Postsecondary transition (e.g., college tours, bridge programs)',1),
		('student_02','Successful course completion (e.g., tutoring, personal counseling)',1),
		('student_03','Preparation for postsecondary credit accrual (e.g., college assessments)',1)
	) t (Name,Label,IsActive)
ORDER BY
	Name;