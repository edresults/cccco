USE calpass;

GO

IF (object_id('lbccpt.School') is not null)
	BEGIN
		DROP TABLE lbccpt.School;
	END;

GO

CREATE TABLE
	lbccpt.School
	(
		Id int identity(0,1) not null,
		CodeCalpass int not null,
		CodeCalpads char(14) not null,
		Name nvarchar(255) not null,
		Label nvarchar(255) not null,
		Type int not null,
		IsActive bit not null,
		Ordinal int not null,
		CONSTRAINT
			pk_School__Id
		PRIMARY KEY CLUSTERED
			(
				id
			),
		CONSTRAINT
			uq_School__CodeCalpass
		UNIQUE
			(
				CodeCalpass
			),
		CONSTRAINT
			uq_School__CodeCalpads
		UNIQUE
			(
				CodeCalpads
			),
		INDEX
			ix_School__IsActive
		NONCLUSTERED
			(
				IsActive
			)
	);

GO

-- High School Districts
INSERT INTO
	lbccpt.School
	(
		CodeCalpass,
		CodeCalpads,
		Name,
		Label,
		Type,
		IsActive,
		Ordinal
	)
SELECT
	CodeCalpass = OrganizationId,
	CodeCalpads = OrganizationCode,
	Name = rtrim(ltrim(OrganizationName)),
	Label = 'District - ' + rtrim(ltrim(OrganizationName)),
	Type = 1,
	IsActive = 1,
	Ordinal = row_number() over(order by rtrim(ltrim(OrganizationName)))
FROM
	[pro-dat-sql-01].calpass.dbo.Organization
WHERE
	OrganizationTypeId = 1
	and ParentId is null
	and (
		OrganizationName like '%Department Of Education%'
		or OrganizationName like '%High School%'
		or OrganizationName like '%Unified%'
		or OrganizationName like '%Union High%'
	)
	and OrganizationName not like '%Charter%'
	and OrganizationName not like '% ROP%'
ORDER BY
	OrganizationName;

-- High Schools
INSERT INTO
	lbccpt.School
	(
		CodeCalpass,
		CodeCalpads,
		Name,
		Label,
		Type,
		IsActive,
		Ordinal
	)
SELECT DISTINCT
	CodeCalpass = a.OrganizationId,
	CodeCalpads = a.OrganizationCode,
	Name = rtrim(ltrim(a.OrganizationName)),
	Label = 'School - ' + rtrim(ltrim(a.OrganizationName)) + ' (' + rtrim(ltrim(b.OrganizationName)) + ')',
	Type = 2,
	IsActive = 1,
	Ordinal = (SELECT max(Ordinal) FROM lbccpt.School) + 1 + row_number() over(order by rtrim(ltrim(a.OrganizationName)))
FROM
	[pro-dat-sql-01].calpass.dbo.Organization a
	inner join
	[pro-dat-sql-01].calpass.dbo.Organization b
		on substring(a.organizationcode,1,7) + '0000000' = b.organizationcode
WHERE
	a.ParentId in (
		SELECT
			s.CodeCalpass
		FROM
			lbccpt.School s
		WHERE 
			Type = 1
	) 
	and a.OrganizationName not like '%Middle%' 
	and a.OrganizationName not like '%Elementary%' 
	and a.OrganizationName not like '%Adult%'
	and a.OrganizationName not like '%Kindergarten%'
	and a.OrganizationName like '%High%'
ORDER BY
	rtrim(ltrim(a.OrganizationName));

-- County Office Of Ed
INSERT INTO
	lbccpt.School
	(
		CodeCalpass,
		CodeCalpads,
		Name,
		Label,
		Type,
		IsActive,
		Ordinal
	)
SELECT
	CodeCalpass = OrganizationId,
	CodeCalpads = OrganizationCode,
	Name = rtrim(ltrim(OrganizationName)),
	Label = 'COE - ' + rtrim(ltrim(OrganizationName)),
	Type = 3,
	IsActive = 1,
	Ordinal = (SELECT max(Ordinal) FROM lbccpt.School) + 1 + row_number() over(order by rtrim(ltrim(OrganizationName)))
FROM
	[pro-dat-sql-01].calpass.dbo.Organization
WHERE
	OrganizationName like '%County Office%'
	and OrganizationName not like '%Charter%'
	and OrganizationName not like '% ROP%'
ORDER BY
	rtrim(ltrim(OrganizationName));

-- ROP
INSERT INTO
	lbccpt.School
	(
		CodeCalpass,
		CodeCalpads,
		Name,
		Label,
		Type,
		IsActive,
		Ordinal
	)
SELECT
	CodeCalpass = OrganizationId,
	CodeCalpads = OrganizationCode,
	Name = rtrim(ltrim(OrganizationName)),
	Label = 'ROP - ' + rtrim(ltrim(OrganizationName)),
	Type = 4,
	IsActive = 1,
	Ordinal = (SELECT max(Ordinal) FROM lbccpt.School) + 1 + row_number() over(order by rtrim(ltrim(OrganizationName)))
FROM
	[pro-dat-sql-01].calpass.dbo.Organization
WHERE
	OrganizationName like '% ROP%'
ORDER BY
	rtrim(ltrim(OrganizationName));

-- ROP Schools
INSERT INTO
	lbccpt.School
	(
		CodeCalpass,
		CodeCalpads,
		Name,
		Label,
		Type,
		IsActive,
		Ordinal
	)
SELECT
	CodeCalpass = o.OrganizationId,
	CodeCalpads = o.OrganizationCode,
	Name = rtrim(ltrim(o.OrganizationName)),
	Label = 'ROP School - ' + rtrim(ltrim(o.OrganizationName)),
	Type = 5,
	IsActive = 1,
	Ordinal = (SELECT max(Ordinal) FROM lbccpt.School) + 1 + row_number() over(order by rtrim(ltrim(o.OrganizationName)))
FROM
	[pro-dat-sql-01].calpass.dbo.Organization o
WHERE
	ParentId in (
		SELECT
			s1.CodeCalpass
		FROM
			lbccpt.School s1
		WHERE
			s1.Type = 4
	)
	and o.OrganizationName not like '%District School%'
	and not exists (
		SELECT
			1
		FROM
			lbccpt.School s2
		WHERE
			s2.CodeCalpass = o.organizationId
	)
ORDER BY
	rtrim(ltrim(o.OrganizationName));

-- Charter Districts
INSERT INTO
	lbccpt.School
	(
		CodeCalpass,
		CodeCalpads,
		Name,
		Label,
		Type,
		IsActive,
		Ordinal
	)
SELECT
	CodeCalpass = OrganizationId,
	CodeCalpads = OrganizationCode,
	Name = rtrim(ltrim(OrganizationName)),
	Label = 'Charter District - ' + replace(rtrim(ltrim(OrganizationName)),'(District)',''),
	Type = 6,
	IsActive = 1,
	Ordinal = (SELECT max(Ordinal) FROM lbccpt.School) + 1 + row_number() over(order by rtrim(ltrim(OrganizationName)))
FROM
	[pro-dat-sql-01].calpass.dbo.Organization
WHERE
	OrganizationTypeId = 1
	and ParentId is null
	and (
		OrganizationName like '%Charter%'
		or
		OrganizationName like '%the academy%'
	)
	and OrganizationName not like '% ROP%'
ORDER BY
	rtrim(ltrim(OrganizationName));

-- Charter Schools
INSERT INTO
	lbccpt.School
	(
		CodeCalpass,
		CodeCalpads,
		Name,
		Label,
		Type,
		IsActive,
		Ordinal
	)
SELECT
	CodeCalpass = o.OrganizationId,
	CodeCalpads = o.OrganizationCode,
	Name = rtrim(ltrim(o.OrganizationName)),
	Label = 'Charter School - ' + rtrim(ltrim(o.OrganizationName)),
	Type = 7,
	IsActive = 1,
	Ordinal = (SELECT max(Ordinal) FROM lbccpt.School) + 1 + row_number() over(order by rtrim(ltrim(o.OrganizationName)))
FROM
	[pro-dat-sql-01].calpass.dbo.Organization o
WHERE
	(
		(
			ParentId in (
				SELECT
					s.CodeCalpass
				FROM
					lbccpt.School s
				WHERE
					s.Type = 6
			)
			and o.OrganizationName not like '%Middle%' 
			and o.OrganizationName not like '%Elementary%' 
			and o.OrganizationName not like '%Adult%'
			and o.OrganizationName not like '%Kindergarten%'
			and o.OrganizationName like '%High%'
		)
		or o.organizationId = '160901'
	)
ORDER BY
	rtrim(ltrim(o.OrganizationName));