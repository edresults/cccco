USE calpass;

GO

IF (object_id('lbccpt.School_AcademicYear') is not null)
	BEGIN
		DROP TABLE lbccpt.School_AcademicYear;
	END;

GO

CREATE TABLE
	lbccpt.School_AcademicYear
	(
		Id int identity(1,1) not null,
		ConsortiumId_SchoolId int not null,
		AcademicYearId int not null,
		CONSTRAINT
			pk_School_AcademicYear__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		CONSTRAINT
			uk_School_AcademicYear__ConsortiumId_SchoolId__AcademicYearId
		UNIQUE
			(
				ConsortiumId_SchoolId,
				AcademicYearId
			),
		CONSTRAINT
			fk_School_AcademicYear__ConsortiumId_SchoolId
		FOREIGN KEY
			(
				ConsortiumId_SchoolId
			)
		REFERENCES
			lbccpt.Consortium_School
			(
				Id
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_School_AcademicYear__AcademicYearId
		FOREIGN KEY
			(
				AcademicYearId
			)
		REFERENCES
			lbccpt.AcademicYear
			(
				Id
			)
		ON DELETE CASCADE
	);