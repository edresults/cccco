USE calpass;

GO

IF (object_id('lbccpt.SchoolPathwayMetric') is not null)
	BEGIN
		DROP TABLE lbccpt.SchoolPathwayMetric;
	END;

GO

CREATE TABLE
	lbccpt.SchoolPathwayMetric
	(
		Id int identity(0,1) not null,
		Name nvarchar(255) not null,
		Label nvarchar(255) not null,
		IsActive bit not null,
		Ordinal int not null,
		NameLegacy nvarchar(255),
		CONSTRAINT
			pk_SchoolPathwayMetric__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		INDEX
			ix_SchoolPathwayMetric__IsActive
		NONCLUSTERED
			(
				IsActive
			)
	);

GO

INSERT INTO
	lbccpt.SchoolPathwayMetric
	(
		Name,
		Label,
		Ordinal,
		IsActive,
		NameLegacy
	)
SELECT
	Name,
	Label,
	Ordinal = row_number() over(order by Name),
	IsActive,
	NameLegacy
FROM
	(
		VALUES
		('metric_00','Number of first-year students enrolled in the career pathway program.',1,null),
		('metric_01','Number of second-year students enrolled in the career pathway program.',1,null),
		('metric_02','Number of third-year students enrolled in the career pathway program.',1,null),
		('metric_03','Number of fourth-year students enrolled in the career pathway program.',1,null),
		('metric_04','Student academic performance indicators of students enrolled in the career pathway program.',1,'v01'),
		('metric_05','Number of students who have successfully completed a career pathway program with a “C” grade or better in all English courses.',1,'v02'),
		('metric_06','Number of students who have successfully completed a career pathway program with a “C” grade or better in all Mathematics courses.',1,'v24'),
		('metric_07','Number of students who have successfully completed a career pathway program with a “C” grade or better in all Science courses.',1,'v25'),
		('metric_08','Number of students who have successfully completed a career pathway program with a “C” grade or better in all History courses.',1,'v26'),
		('metric_09','Number of students who have successfully completed a career pathway program with a “C” grade or better in all Career pathway courses related to the funded CCPT pathway.',1,'v27'),
		('metric_10','Number of students participating in job shadowing opportunities aligned with the career pathway program in which the studentsare participating',1,'v03'),
		('metric_11','Number of students participating in mentoring opportunities aligned with the career pathway program in which the studentsare participating.',1,'v05'),
		('metric_12','Number of students participating in internships aligned with the career pathway program in which the studentsare participating.',1,'v06'),
		('metric_13','Number of students participating in work experience opportunities aligned with the career pathway programinwhich the studentsare participating.',1,'v07'),
		('metric_14','Number of students participating in a preapprenticeship program aligned with the career pathway program in which studentsare participatingand aligned with a state-approved apprenticeship.',1,'v08'),
		('metric_15','Number of students participating in an apprenticeship program aligned with the career pathway program in which thestudentsare participating.',1,'v09'),
		('metric_16','Number of students participating in a student leadership organization as part of the career pathway program.',1,'v10'),
		('metric_17','Number of students in the career pathway program who received a high school diploma.',1,'v28'),
		('metric_18','Number of students in the career pathway program who received a California High School Equivalency Certificate.',1,'v29'),
		('metric_19','Number of students in the career pathway program who received a nationally recognized industry-valuedcertificateand/or state license.',1,'v11'),
		('metric_20','Number of students in the career pathway program who received a state-approvedCareer and Technical Education (CTE)certificate.',1,'v12')
	) t (Name,Label,IsActive)
ORDER BY
	Name;