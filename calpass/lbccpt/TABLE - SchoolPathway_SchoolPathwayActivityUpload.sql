USE calpass;

GO

IF (object_id('lbccpt.SchoolPathway_SchoolPathwayActivityUpload') is not null)
	BEGIN
		DROP TABLE lbccpt.SchoolPathway_SchoolPathwayActivityUpload;
	END;

GO

CREATE TABLE
	lbccpt.SchoolPathway_SchoolPathwayActivityUpload
	(
		ConsortiumId int,
		CdsCode nchar(14),
		ConsortiumSchoolId int,
		AcademicYear nchar(9),
		AcademicYearId int,
		SchoolPathwayCode nchar(3),
		SchoolPathwayId int,
		School_SchoolPathwayId int,
		course_00 bit,
		course_01 bit,
		course_02 bit,
		course_03 bit,
		course_04 bit,
		course_05 bit,
		student_00 bit,
		student_01 bit,
		student_02 bit,
		student_03 bit,
		instructor_00 bit,
		instructor_01 bit,
		instructor_02 bit,
		instructor_03 bit,
		CONSTRAINT
			pk_SchoolPathway_SchoolPathwayActivityUpload
		PRIMARY KEY CLUSTERED
			(
				ConsortiumId,
				CdsCode,
				AcademicYear,
				SchoolPathwayCode
			),
		CONSTRAINT
			uq_SchoolPathway_SchoolPathwayActivityUpload__School_SchoolPathwayId
		UNIQUE
			(
				School_SchoolPathwayId
			)
	);

GO

CREATE TRIGGER
	lbccpt.tr_SchoolPathway_SchoolPathwayActivityUpload__ins_upd
ON
	lbccpt.SchoolPathway_SchoolPathwayActivityUpload
AFTER
	INSERT, UPDATE
AS

BEGIN
	-- Merge into SchoolPathway_SchoolPathwayCourse
	MERGE
		lbccpt.SchoolPathway_SchoolPathwayCourse
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				SchoolPathwayId = u.School_SchoolPathwayId,
				SchoolPathwayCourseId = spc.Id
			FROM
				inserted i
			UNPIVOT
				(
					val for Name in (
						course_00,
						course_01,
						course_02,
						course_03,
						course_04,
						course_05
					)
				) u
				inner join
				lbccpt.SchoolPathwayCourse spc
					on spc.Name = u.Name
			WHERE
				u.val = 1
				and u.School_SchoolPathwayId is not null
		) s
	ON
		t.SchoolPathwayId = s.SchoolPathwayId
		and t.SchoolPathwayCourseId = s.SchoolPathwayCourseId
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				SchoolPathwayId,
				SchoolPathwayCourseId
			)
		VALUES
			(
				s.SchoolPathwayId,
				s.SchoolPathwayCourseId
			)
	WHEN NOT MATCHED BY SOURCE
		and (exists (SELECT 1 FROM inserted tt WHERE tt.School_SchoolPathwayId = t.SchoolPathwayId)) THEN
		DELETE;
	-- Merge into SchoolPathway_SchoolPathwayStudent
	MERGE
		lbccpt.SchoolPathway_SchoolPathwayStudent
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				SchoolPathwayId = u.School_SchoolPathwayId,
				SchoolPathwayStudentId = sps.Id
			FROM
				inserted i
			UNPIVOT
				(
					val for Name in (
						student_00,
						student_01,
						student_02,
						student_03
					)
				) u
				inner join
				lbccpt.SchoolPathwayStudent sps
					on sps.Name = u.Name
			WHERE
				u.val = 1
				and u.School_SchoolPathwayId is not null
		) s
	ON
		t.SchoolPathwayId = s.SchoolPathwayId
		and t.SchoolPathwayStudentId = s.SchoolPathwayStudentId
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				SchoolPathwayId,
				SchoolPathwayStudentId
			)
		VALUES
			(
				s.SchoolPathwayId,
				s.SchoolPathwayStudentId
			)
	WHEN NOT MATCHED BY SOURCE
		and (exists (SELECT 1 FROM inserted tt WHERE tt.School_SchoolPathwayId = t.SchoolPathwayId)) THEN
		DELETE;
	-- Merge into SchoolPathway_SchoolPathwayInstructor
	MERGE
		lbccpt.SchoolPathway_SchoolPathwayInstructor
	WITH
		(
			TABLOCKX
		) t
	USING
		(
			SELECT
				SchoolPathwayId = u.School_SchoolPathwayId,
				SchoolPathwayInstructorId = spi.Id
			FROM
				inserted i
			UNPIVOT
				(
					val for Name in (
						instructor_00,
						instructor_01,
						instructor_02,
						instructor_03
					)
				) u
				inner join
				lbccpt.SchoolPathwayInstructor spi
					on spi.Name = u.Name
			WHERE
				u.val = 1
				and u.School_SchoolPathwayId is not null
		) s
	ON
		t.SchoolPathwayId = s.SchoolPathwayId
		and t.SchoolPathwayInstructorId = s.SchoolPathwayInstructorId
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				SchoolPathwayId,
				SchoolPathwayInstructorId
			)
		VALUES
			(
				s.SchoolPathwayId,
				s.SchoolPathwayInstructorId
			)
	WHEN NOT MATCHED BY SOURCE
		and (exists (SELECT 1 FROM inserted tt WHERE tt.School_SchoolPathwayId = t.SchoolPathwayId)) THEN
		DELETE;
END;