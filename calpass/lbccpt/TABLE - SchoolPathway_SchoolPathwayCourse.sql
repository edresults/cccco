USE calpass;

GO

IF (object_id('lbccpt.SchoolPathway_SchoolPathwayCourse') is not null)
	BEGIN
		DROP TABLE lbccpt.SchoolPathway_SchoolPathwayCourse;
	END;

GO

CREATE TABLE
	lbccpt.SchoolPathway_SchoolPathwayCourse
	(
		Id int identity(1,1) not null,
		SchoolPathwayId int not null,
		SchoolPathwayCourseId int not null,
		CONSTRAINT
			pk_SchoolPathway_SchoolPathwayCourse__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		CONSTRAINT
			uq_SchoolPathway_SchoolPathwayCourse__SchoolPathwayId__SchoolPathwayCourseId
		UNIQUE
			(
				SchoolPathwayId,
				SchoolPathwayCourseId
			),
		CONSTRAINT
			fk_SchoolPathway_SchoolPathwayCourse__SchoolPathwayId
		FOREIGN KEY
			(
				SchoolPathwayId
			)
		REFERENCES
			lbccpt.School_SchoolPathway
			(
				Id
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_SchoolPathway_SchoolPathwayCourse_SchoolPathwayCourseId
		FOREIGN KEY
			(
				SchoolPathwayCourseId
			)
		REFERENCES
			lbccpt.SchoolPathwayCourse
			(
				Id
			)
		ON DELETE CASCADE
	);