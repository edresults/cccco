DECLARE
	@UserId int = 5552,
	@ConsortiumId int = 1;

-- dropdown: Please select the consortium; on page: Create Pathway Program for K12 Site
-- key|val pair of consortia for user
SELECT
	[val] = c.Name,
	[key] = c.Id
FROM
	calpass.lbccpt.Consortium c
WHERE
	c.UserId = @UserId;

-- dropdown: Please select the site; on page: Create Pathway Program for K12 Site
-- key|val pair of Schools for specific Consortium; cascades from [val] above
SELECT
	[val] = s.Label,
	[key] = cs.Id
FROM
	calpass.lbccpt.Consortium c
	inner join
	calpass.lbccpt.Consortium_School cs
		on c.Id = cs.ConsortiumId
	inner join
	calpass.lbccpt.School s
		on s.Id = cs.SchoolId
WHERE
	c.UserId = @UserId
ORDER BY
	s.Ordinal;

-- dropdown: Please select the grant year for the following program characteristics; on page: Create Pathway Program for K12 Site
-- key|val pair of Schools for specific Consortium; cascades from [val] above
SELECT
	[val] = CodeCalpads,
	[key] = Id
FROM
	calpass.lbccpt.AcademicYear
ORDER BY
	Ordinal;

-- dropdown: Please select the pathway...; on page: Create Pathway Program for K12 Site
-- key|val pair of Schools for specific Consortium; cascades from [val] above
SELECT
	[val] = Label,
	[key] = Id
FROM
	calpass.lbccpt.SchoolPathway
ORDER BY
	Ordinal;

-- EDIT PATHWAY
SELECT
	[val] = s.Label + ' | ' + ay.CodeCalpads + ' | ' + sp.Label,
	[key] = ssp.Id
FROM
	calpass.lbccpt.Consortium c
	inner join
	calpass.lbccpt.Consortium_School cs
		on c.Id = cs.ConsortiumId
	inner join
	calpass.lbccpt.School s
		on s.Id = cs.SchoolId
	inner join
	calpass.lbccpt.School_SchoolPathway ssp
		on ssp.ConsortiumSchoolId = cs.Id
	inner join
	calpass.lbccpt.AcademicYear ay
		on ay.Id = ssp.AcademicYearId
	inner join
	calpass.lbccpt.SchoolPathway sp
		on sp.Id = ssp.SchoolPathwayId
WHERE
	c.UserId = @UserId
	and c.Id = @ConsortiumId
ORDER BY
	s.Ordinal;