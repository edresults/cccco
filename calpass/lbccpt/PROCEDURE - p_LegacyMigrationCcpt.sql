USE calpass;

IF (object_id('lbccpt.p_LegacyMigrationCcpt') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_LegacyMigrationCcpt;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_LegacyMigrationCcpt
AS

DECLARE
	@IdentityUbound int;

	DECLARE
		@Consortium
	AS TABLE
		(
			Id int,
			Name nvarchar(255),
			UserId int
		);
	DECLARE
		@Consortium_School
	AS TABLE
		(
			Id int,
			ConsortiumId int,
			SchoolId int
		);

	DECLARE
		@School_SchoolPathway 
	AS TABLE
		(
			Id int,
			ConsortiumSchoolId int, -- must be tied to consortium_school which is tied to consortrium
			AcademicYearId int,
			SchoolPathwayId int
		);

	DECLARE
		@SchoolPathway_SchoolPathwayActivity
	AS TABLE
		(
			LegacyConsortiumId int,
			ConsortiumName nvarchar(255),
			UserId int,
			LegacyPathwayProgramId int,
			SchoolId int,
			AcademicYearCode char(9),
			AcademicYearId int,
			SchoolPathwayCode char(3),
			SchoolPathwayId int,
			Course varchar(255),
			Student varchar(255),
			Instructor varchar(255)
		);
		
	DECLARE
		@Consortium_College
	AS TABLE
		(
			Id int,
			ConsortiumId int,
			CollegeId int
		);

	DECLARE
		@College_CollegePathway 
	AS TABLE
		(
			Id int,
			ConsortiumCollegeId int, -- must be tied to consortium_school which is tied to consortrium
			CollegePathwayName nvarchar(255),
			AcademicYearId int
		);

	DECLARE
		@CollegePathway_CollegePathwayActivity
	AS TABLE
		(
			LegacyConsortiumId int,
			ConsortiumName nvarchar(255),
			UserId int,
			LegacyPathwayProgramId int,
			CollegeId int,
			AcademicYearCode char(9),
			AcademicYearId int,
			CollegePathwayName nvarchar(255),
			Taxonomy varchar(255),
			Course varchar(255),
			Student varchar(255),
			Instructor varchar(255)
		);

BEGIN
	-- cleanse everything
	DELETE
	FROM
		lbccpt.Consortium;
	-- reset identity values
	DBCC CHECKIDENT ('lbccpt.Consortium', RESEED, 0);
	DBCC CHECKIDENT ('lbccpt.Consortium_School', RESEED, 0);
	DBCC CHECKIDENT ('lbccpt.School_SchoolPathway', RESEED, 0);
	DBCC CHECKIDENT ('lbccpt.SchoolPathway_SchoolPathwayCourse', RESEED, 0);
	DBCC CHECKIDENT ('lbccpt.SchoolPathway_SchoolPathwayStudent', RESEED, 0);
	DBCC CHECKIDENT ('lbccpt.SchoolPathway_SchoolPathwayInstructor', RESEED, 0);
	DBCC CHECKIDENT ('lbccpt.SchoolPathway_SchoolPathwayMetric', RESEED, 0);
	DBCC CHECKIDENT ('lbccpt.Consortium_College', RESEED, 0);
	DBCC CHECKIDENT ('lbccpt.College_CollegePathway', RESEED, 0);
	DBCC CHECKIDENT ('lbccpt.CollegePathway_CollegePathwayTaxonomy', RESEED, 0);
	DBCC CHECKIDENT ('lbccpt.CollegePathway_CollegePathwayCourse', RESEED, 0);
	DBCC CHECKIDENT ('lbccpt.CollegePathway_CollegePathwayStudent', RESEED, 0);
	DBCC CHECKIDENT ('lbccpt.CollegePathway_CollegePathwayInstructor', RESEED, 0);
	DBCC CHECKIDENT ('lbccpt.CollegePathway_CollegePathwayMetric', RESEED, 0);
	/*
		K12
	*/
	INSERT INTO
		@SchoolPathway_SchoolPathwayActivity
		(
			LegacyConsortiumId,
			ConsortiumName,
			UserId,
			LegacyPathwayProgramId,
			SchoolId,
			AcademicYearCode,
			AcademicYearId,
			SchoolPathwayCode,
			SchoolPathwayId,
			Course,
			Student,
			Instructor
		)
	SELECT
		LegacyConsortiumId = convert(int, substring(p.District, 1, charindex('|', p.District) - 1)),
		ConsortiumName = c.ConsortiumName,
		UserId = c.FormCreatedBy,
		LegacyPathwayProgramId = p.LaunchboardCCPTK12PathwayId,
		SchoolId = o.Id,
		AcademicYearCode = p.AcademicYear,
		AcademicYearId = ay.Id,
		SchoolPathwayCode = p.PathwayCode,
		SchoolPathwayId = sp.Id,
		Course = p.ProgramCharacteristics_Courses,
		Student = p.ProgramCharacteristics_StudentSupport,
		Instructor = p.ProgramCharacteristics_Teachers
	FROM
		[PRO-DAT-SQL-01].cms_production_calpass.dbo.Form_calpassplus_LaunchboardCCPTConsortium c
		inner join
		[PRO-DAT-SQL-01].cms_production_calpass.dbo.Form_calpassplus_LaunchboardCCPTK12Pathway p
			on c.LaunchboardCCPTConsortiumId = substring(p.District, 1, charindex('|', p.District) - 1)
		inner join
		lbccpt.SchoolPathway sp
			on sp.Code = p.PathwayCode
		inner join
		lbccpt.AcademicYear ay
			on ay.CodeCalpads = AcademicYear
		inner join
		lbccpt.School o
			on o.CodeCalpass = substring(p.District, charindex('|', p.District) + 1, len(p.District))
	WHERE
		isnull(p.Deleted, 0) = 0;
	/*
		CC
	*/
	INSERT INTO
		@CollegePathway_CollegePathwayActivity
		(
			LegacyConsortiumId,
			ConsortiumName,
			UserId,
			LegacyPathwayProgramId,
			CollegeId,
			AcademicYearCode,
			AcademicYearId,
			CollegePathwayName,
			Taxonomy,
			Course,
			Student,
			Instructor
		)
	SELECT
		LegacyConsortiumId = convert(int, substring(p.College, 1, charindex('|', p.College) - 1)),
		ConsortiumName = c.ConsortiumName,
		UserId = c.FormCreatedBy,
		LegacyPathwayProgramId = p.LaunchboardCCPTCCPathwayId,
		CollegeId = o.Id,
		AcademicYearCode = p.AcademicYear,
		AcademicYearId = ay.Id,
		CollegePathwayName = p.PathwayProgramName,
		Taxonomy = p.Top4Codes,
		Course = p.ProgramCharacteristics_Courses,
		Student = p.ProgramCharacteristics_StudentSupport,
		Instructor = p.ProgramCharacteristics_Teachers
	FROM
		[PRO-DAT-SQL-01].cms_production_calpass.dbo.Form_calpassplus_LaunchboardCCPTConsortium c
		inner join
		[PRO-DAT-SQL-01].cms_production_calpass.dbo.Form_calpassplus_LaunchboardCCPTCCPathway p
			on c.LaunchboardCCPTConsortiumId = substring(p.College, 1, charindex('|', p.College) - 1)
		inner join
		lbccpt.AcademicYear ay
			on ay.CodeCalpads = AcademicYear
		inner join
		lbccpt.College o
			on o.CodeCalpass = substring(p.College, charindex('|', p.College) + 1, len(p.College))
	WHERE
		isnull(p.Deleted, 0) = 0;
	/*
		Consortium
	*/
	-- enable identity inserts
	SET IDENTITY_INSERT lbccpt.Consortium ON;
	-- populate
	INSERT INTO
		lbccpt.Consortium
		(
			Id,
			Name,
			UserId
		)
	OUTPUT
		Inserted.Id,
		Inserted.Name,
		Inserted.UserId
	INTO
		@Consortium
		(
			Id,
			Name,
			UserId
		)
	SELECT
		LegacyConsortiumId,
		ConsortiumName,
		UserId
	FROM
		@SchoolPathway_SchoolPathwayActivity
	UNION
	SELECT
		LegacyConsortiumId,
		ConsortiumName,
		UserId
	FROM
		@CollegePathway_CollegePathwayActivity;
	-- disable identity inserts
	SET IDENTITY_INSERT lbccpt.Consortium OFF;
	-- get identity ubound
	SELECT
		@IdentityUbound = max(Id)
	FROM
		@Consortium;
	-- reset identity value;
	DBCC CHECKIDENT ('lbccpt.Consortium', RESEED, @IdentityUbound);
	/*
		Consortium_School
	*/
	INSERT INTO
		lbccpt.Consortium_School
		(
			ConsortiumId,
			SchoolId
		)
	OUTPUT
		Inserted.Id,
		Inserted.ConsortiumId,
		Inserted.SchoolId
	INTO
		@Consortium_School
		(
			Id,
			ConsortiumId,
			SchoolId
		)
	SELECT DISTINCT
		LegacyConsortiumId,
		SchoolId
	FROM
		@SchoolPathway_SchoolPathwayActivity;
	/*
		School_SchoolPathway
	*/
	INSERT INTO
		lbccpt.School_SchoolPathway
		(
			ConsortiumSchoolId, -- Consortium_School.Id
			AcademicYearId, -- AcademicYear.Id
			SchoolPathwayId -- SchoolPathway.Id
		)
	OUTPUT
		Inserted.Id,
		Inserted.ConsortiumSchoolId,
		Inserted.AcademicYearId,
		Inserted.SchoolPathwayId
	INTO
		@School_SchoolPathway
		(
			Id,
			ConsortiumSchoolId,
			AcademicYearId,
			SchoolPathwayId
		)
	SELECT
		cs.Id,
		spspa.AcademicYearId,
		spspa.SchoolPathwayId
	FROM
		@SchoolPathway_SchoolPathwayActivity spspa
		inner join
		@Consortium_School cs
			on cs.ConsortiumId = spspa.LegacyConsortiumId
			and cs.SchoolId = spspa.SchoolId;
	/*
		SchoolPathway_SchoolPathwayCourse
	*/
	INSERT INTO
		lbccpt.SchoolPathway_SchoolPathwayCourse
		(
			SchoolPathwayId,
			SchoolPathwayCourseId
		)
	SELECT
		SchoolPathwayId = ssp.Id,
		SchoolPathwayCourseId = spc.Id
	FROM
		@SchoolPathway_SchoolPathwayActivity spspa
		inner join
		@Consortium_School cs
			on cs.ConsortiumId = spspa.LegacyConsortiumId
			and cs.SchoolId = spspa.SchoolId
		inner join
		@School_SchoolPathway ssp
			on cs.Id = ssp.ConsortiumSchoolId
			and ssp.AcademicYearId = spspa.AcademicYearId
			and ssp.SchoolPathwayId = spspa.SchoolPathwayId
		cross apply
		string_split(Course, '|') ssc
		inner join
		lbccpt.SchoolPathwayCourse spc
			on spc.Name = case
				when ssc.value = '10' then 'course_00'
				when ssc.value = '20' then 'course_01'
				when ssc.value = '30' then 'course_02'
				when ssc.value = '40' then 'course_03'
				when ssc.value = '50' then 'course_04'
				when ssc.value = '60' then 'course_05'
			end;
	/*
		SchoolPathway_SchoolPathwayStudent
	*/	
	INSERT INTO
		lbccpt.SchoolPathway_SchoolPathwayStudent
		(
			SchoolPathwayId,
			SchoolPathwayStudentId
		)
	SELECT
		SchoolPathwayId = ssp.Id,
		SchoolPathwayStudentId = sps.Id
	FROM
		@SchoolPathway_SchoolPathwayActivity spspa
		inner join
		@Consortium_School cs
			on cs.ConsortiumId = spspa.LegacyConsortiumId
			and cs.SchoolId = spspa.SchoolId
		inner join
		@School_SchoolPathway ssp
			on cs.Id = ssp.ConsortiumSchoolId
			and ssp.AcademicYearId = spspa.AcademicYearId
			and ssp.SchoolPathwayId = spspa.SchoolPathwayId
		cross apply
		string_split(Student, '|') sss
		inner join
		lbccpt.SchoolPathwayStudent sps
			on sps.Name = case
				when sss.value = '200' then 'student_00'
				when sss.value = '210' then 'student_01'
				when sss.value = '220' then 'student_02'
				when sss.value = '230' then 'student_03'
			end;
	/*
		SchoolPathway_SchoolPathwayInstructor
	*/
	INSERT INTO
		lbccpt.SchoolPathway_SchoolPathwayInstructor
		(
			SchoolPathwayId,
			SchoolPathwayInstructorId
		)
	SELECT
		SchoolPathwayId = ssp.Id,
		SchoolPathwayInstructorId = spi.Id
	FROM
		@SchoolPathway_SchoolPathwayActivity spspa
		inner join
		@Consortium_School cs
			on cs.ConsortiumId = spspa.LegacyConsortiumId
			and cs.SchoolId = spspa.SchoolId
		inner join
		@School_SchoolPathway ssp
			on cs.Id = ssp.ConsortiumSchoolId
			and ssp.AcademicYearId = spspa.AcademicYearId
			and ssp.SchoolPathwayId = spspa.SchoolPathwayId
		cross apply
		string_split(Instructor, '|') ssi
		inner join
		lbccpt.SchoolPathwayInstructor spi
			on spi.Name = case
				when ssi.value = '300' then 'instructor_00'
				when ssi.value = '310' then 'instructor_01'
				when ssi.value = '320' then 'instructor_02'
				when ssi.value = '330' then 'instructor_03'
			end;
	/*
		SchoolPathway_SchoolPathwayMetric
	*/
	INSERT INTO
		lbccpt.SchoolPathway_SchoolPathwayMetric
		(
			SchoolPathwayId,
			SchoolPathwayMetricId,
			MetricValue
		)
	SELECT
		SchoolPathwayId,
		SchoolPathwayMetricId = spm.Id,
		MetricValue = isnull(u.MetricValue, 0)
	FROM
		(
			SELECT
				SchoolPathwayId = ssp.Id,
				m.*
			FROM
				@SchoolPathway_SchoolPathwayActivity spspa
				inner join
				@Consortium_School cs
					on cs.ConsortiumId = spspa.LegacyConsortiumId
					and cs.SchoolId = spspa.SchoolId
				inner join
				@School_SchoolPathway ssp
					on cs.Id = ssp.ConsortiumSchoolId
					and ssp.AcademicYearId = spspa.AcademicYearId
					and ssp.SchoolPathwayId = spspa.SchoolPathwayId
				inner join
				[PRO-DAT-SQL-01].cms_production_calpass.dbo.Form_calpassplus_LaunchboardCCPTK12Metrics m
					on spspa.LegacyPathwayProgramId = substring(m.PathwayProgramCohortId, charindex('|', m.PathwayProgramCohortId) + 1, len(m.PathwayProgramCohortId))
			WHERE
				isnull(m.Deleted, 0) = 0
		) p
	UNPIVOT
		(
			MetricValue FOR MetricName IN (v02,v24,v25,v26,v27,v03,v05,v06,v07,v08,v09,v10,v28,v29,v11,v12)
		) u
		left outer join
		lbccpt.SchoolPathwayMetric spm
			on spm.NameLegacy = MetricName;
	/*
		Consortium_College
	*/
	INSERT INTO
		lbccpt.Consortium_College
		(
			ConsortiumId,
			CollegeId
		)
	OUTPUT
		Inserted.Id,
		Inserted.ConsortiumId,
		Inserted.CollegeId
	INTO
		@Consortium_College
		(
			Id,
			ConsortiumId,
			CollegeId
		)
	SELECT DISTINCT
		LegacyConsortiumId,
		CollegeId
	FROM
		@CollegePathway_CollegePathwayActivity;
	/*
		College_CollegePathway
	*/
	INSERT INTO
		lbccpt.College_CollegePathway
		(
			ConsortiumCollegeId, -- Consortium_College.Id
			Name, -- CollegePathway.Name
			AcademicYearId -- AcademicYear.Id
		)
	OUTPUT
		Inserted.Id,
		Inserted.ConsortiumCollegeId,
		Inserted.Name,
		Inserted.AcademicYearId
	INTO
		@College_CollegePathway
		(
			Id,
			ConsortiumCollegeId,
			CollegePathwayName,
			AcademicYearId
		)
	SELECT
		cc.Id,
		cpcpa.CollegePathwayName,
		cpcpa.AcademicYearId
	FROM
		@CollegePathway_CollegePathwayActivity cpcpa
		inner join
		@Consortium_College cc
			on cc.ConsortiumId = cpcpa.LegacyConsortiumId
			and cc.CollegeId = cpcpa.CollegeId;
	/*
		CollegePathway_CollegePathwayTaxonomy
	*/
	INSERT INTO
		lbccpt.CollegePathway_CollegePathwayTaxonomy
		(
			CollegePathwayId,
			CollegePathwayTaxonomyId
		)
	SELECT
		CollegePathwayId = ccp.Id,
		CollegePathwayTaxonomyId = cpt.Id
	FROM
		@CollegePathway_CollegePathwayActivity cpcpa
		inner join
		@Consortium_College cc
			on cc.ConsortiumId = cpcpa.LegacyConsortiumId
			and cc.CollegeId = cpcpa.CollegeId
		inner join
		@College_CollegePathway ccp
			on cc.Id = ccp.ConsortiumCollegeId
			and ccp.AcademicYearId = cpcpa.AcademicYearId
			and ccp.CollegePathwayName = cpcpa.CollegePathwayName
		cross apply
		string_split(Taxonomy, '|') cct
		inner join
		lbccpt.CollegePathwayTaxonomy cpt
			on cpt.Code = cct.value;
	/*
		CollegePathway_CollegePathwayCourse
	*/
	INSERT INTO
		lbccpt.CollegePathway_CollegePathwayCourse
		(
			CollegePathwayId,
			CollegePathwayCourseId
		)
	SELECT
		CollegePathwayId = ccp.Id,
		CollegePathwayCourseId = cpc.Id
	FROM
		@CollegePathway_CollegePathwayActivity cpcpa
		inner join
		@Consortium_College cc
			on cc.ConsortiumId = cpcpa.LegacyConsortiumId
			and cc.CollegeId = cpcpa.CollegeId
		inner join
		@College_CollegePathway ccp
			on cc.Id = ccp.ConsortiumCollegeId
			and ccp.AcademicYearId = cpcpa.AcademicYearId
			and ccp.CollegePathwayName = cpcpa.CollegePathwayName
		cross apply
		string_split(Course, '|') ccc
		inner join
		lbccpt.CollegePathwayCourse cpc
			on cpc.Name = case
				when ccc.value = '1010' then 'course_00'
				when ccc.value = '1030' then 'course_01'
			end;
	/*
		CollegePathway_CollegePathwayStudent
	*/	
	INSERT INTO
		lbccpt.CollegePathway_CollegePathwayStudent
		(
			CollegePathwayId,
			CollegePathwayStudentId
		)
	SELECT
		CollegePathwayId = ccp.Id,
		CollegePathwayStudentId = cps.Id
	FROM
		@CollegePathway_CollegePathwayActivity cpcpa
		inner join
		@Consortium_College cc
			on cc.ConsortiumId = cpcpa.LegacyConsortiumId
			and cc.CollegeId = cpcpa.CollegeId
		inner join
		@College_CollegePathway ccp
			on cc.Id = ccp.ConsortiumCollegeId
			and ccp.AcademicYearId = cpcpa.AcademicYearId
			and ccp.CollegePathwayName = cpcpa.CollegePathwayName
		cross apply
		string_split(Student, '|') ccs
		inner join
		lbccpt.CollegePathwayStudent cps
			on cps.Name = case
				when ccs.value = '1210' then 'student_00'
				when ccs.value = '1220' then 'student_01'
			end;
	/*
		CollegePathway_CollegePathwayInstructor
	*/
	INSERT INTO
		lbccpt.CollegePathway_CollegePathwayInstructor
		(
			CollegePathwayId,
			CollegePathwayInstructorId
		)
	SELECT
		CollegePathwayId = ccp.Id,
		CollegePathwayInstructorId = cpi.Id
	FROM
		@CollegePathway_CollegePathwayActivity cpcpa
		inner join
		@Consortium_College cc
			on cc.ConsortiumId = cpcpa.LegacyConsortiumId
			and cc.CollegeId = cpcpa.CollegeId
		inner join
		@College_CollegePathway ccp
			on cc.Id = ccp.ConsortiumCollegeId
			and ccp.AcademicYearId = cpcpa.AcademicYearId
			and ccp.CollegePathwayName = cpcpa.CollegePathwayName
		cross apply
		string_split(Instructor, '|') cci
		inner join
		lbccpt.CollegePathwayInstructor cpi
			on cpi.Name = case
				when cci.value = '1320' then 'instructor_00'
				when cci.value = '1330' then 'instructor_01'
				when cci.value = '1340' then 'instructor_02'
			end;
	/*
		CollegePathway_CollegePathwayMetric
	*/
	INSERT INTO
		lbccpt.CollegePathway_CollegePathwayMetric
		(
			CollegePathwayId,
			CollegePathwayMetricId,
			MetricValue
		)
	SELECT
		CollegePathwayId,
		CollegePathwayMetricId = spm.Id,
		MetricValue = isnull(u.MetricValue, 0)
	FROM
		(
			SELECT
				CollegePathwayId = ccp.Id,
				v01 = convert(int, v01),
				v02 = convert(int, v02),
				v03 = convert(int, v03),
				v04 = convert(int, v04),
				v05 = convert(int, v05),
				v06 = convert(int, v06),
				v07 = convert(int, v07),
				v08 = convert(int, v08),
				v09 = convert(int, v09),
				v10 = convert(int, v10),
				v11 = convert(int, v11),
				v12 = convert(int, v12),
				v13 = convert(int, v13),
				v14 = convert(int, v14),
				v15 = convert(int, v15),
				v16 = convert(int, v16),
				v17 = convert(int, v17),
				v18 = convert(int, v18),
				v19 = convert(int, v19),
				v20 = convert(int, v20),
				v21 = convert(int, v21),
				v01b = convert(int, v01b),
				v02b = convert(int, v02b),
				v22 = convert(int, v22),
				v23 = convert(int, v23),
				v24 = convert(int, v24),
				v25 = convert(int, v25),
				v26 = convert(int, v26),
				v27 = convert(int, v27),
				v28 = convert(int, v28),
				v29 = convert(int, v29),
				v30 = convert(int, v30),
				v31 = convert(int, v31),
				v32 = convert(int, v32)
			FROM
				@CollegePathway_CollegePathwayActivity cpcpa
				inner join
				@Consortium_College cc
					on cc.ConsortiumId = cpcpa.LegacyConsortiumId
					and cc.CollegeId = cpcpa.CollegeId
				inner join
				@College_CollegePathway ccp
					on cc.Id = ccp.ConsortiumCollegeId
					and ccp.AcademicYearId = cpcpa.AcademicYearId
					and ccp.CollegePathwayName = cpcpa.CollegePathwayName
				inner join
				[PRO-DAT-SQL-01].cms_production_calpass.dbo.Form_calpassplus_LaunchboardCCPTCCMetrics m
					on cpcpa.LegacyPathwayProgramId = substring(m.PathwayProgramCohortId, charindex('|', m.PathwayProgramCohortId) + 1, len(m.PathwayProgramCohortId))
			WHERE
				isnull(m.Deleted, 0) = 0
		) p
	UNPIVOT
		(
			MetricValue FOR MetricName IN (v31,v01,v01b,v02,v02b,v22,v23,v24,v25,v26,v03,v05,v06,v07,v08,v09,v10,v27,v28,v29,v30,v11,v12,v13,v32)
		) u
		left outer join
		lbccpt.CollegePathwayMetric spm
			on spm.NameLegacy = MetricName;
END;