IF (object_id('tempdb..#SloData') is not null)
	BEGIN
		DROP TABLE #SloData;
	END;

GO

DECLARE
	@School_SchoolPathway lbccpt.School_SchoolPathway;

SELECT
	ConsortiumId = c.Id,
	ConsortiumName,
	SchoolId = s.Id,
	CdsCode,
	CdsName,
	ConsortiumSchoolId = cs.Id,
	AcademicYearId = y.Id,
	AcademicYear,
	SchoolPathwayId = sp.Id,
	PathwayCode,
	PathwayName,
	School_SchoolPathwayId = null,
	course_00 = case when course_00 is null then 0 else 1 end,
	course_01 = case when course_01 is null then 0 else 1 end,
	course_02 = case when course_02 is null then 0 else 1 end,
	course_03 = case when course_03 is null then 0 else 1 end,
	course_04 = case when course_04 is null then 0 else 1 end,
	course_05 = case when course_05 is null then 0 else 1 end,
	student_00 = case when student_00 is null then 0 else 1 end,
	student_01 = case when student_01 is null then 0 else 1 end,
	student_02 = case when student_02 is null then 0 else 1 end,
	student_03 = case when student_03 is null then 0 else 1 end,
	instructor_00 = case when instructor_00 is null then 0 else 1 end,
	instructor_01 = case when instructor_01 is null then 0 else 1 end,
	instructor_02 = case when instructor_02 is null then 0 else 1 end,
	instructor_03 = case when instructor_03 is null then 0 else 1 end
INTO
	#SloData
FROM
	(
		VALUES
		('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','101','Agricultural Mechanics','Yes','Yes',null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','103','Animal Science','Yes','Yes',null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','105','Ornamental Horticulture','Yes','Yes',null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','111','Design, Visual, and Media Arts','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','113','Production and Managerial Arts','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','130','Child Development','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','141','Environmental Resources','Yes','Yes',null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','150','Architectural Design','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','198','Patient Care','Yes','Yes',null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','202','Hospitality, Tourism, and Recreation','Yes',null,null,null,'Yes','','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','221','Systems Diagnostics, Services, and Repair','Yes',null,null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687000000000','Atascadero High','2016-2017','101','Agricultural Mechanics','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687000000000','Atascadero High','2016-2017','103','Animal Science','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40687000000000','Atascadero High','2016-2017','111','Design, Visual, and Media Arts','Yes','Yes',null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687000000000','Atascadero High','2016-2017','221','Systems Diagnostics, Services, and Repair','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687000000000','Atascadero Unified','2016-2017','130','Child Development','Yes',null,null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687000000000','Atascadero Unified','2016-2017','174','Software and Systems Development','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687000000000','Atascadero Unified','2016-2017','201','Food Service and Hospitality','Yes',null,null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687600000000','Central Coast New Tech High','2016-2017','111','Design, Visual, and Media Arts','Yes','Yes','Yes','Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687600000000','Central Coast New Tech High','2016-2017','174','Software and Systems Development','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes'),
		('San Luis Obispo Partners In Education*','40754700000000','Coast Unified','2016-2017','103','Animal Science','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40754700000000','Coast Unified','2016-2017','111','Design, Visual, and Media Arts','Yes','Yes',null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40104100000000','Grizzly Youth Academy','2016-2017',null,null,null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,null,null),
		('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','100','Agricultural Business',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','102','Agriscience',null,null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','103','Animal Science',null,null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','111','Design, Visual, and Media Arts',null,'Yes',null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','113','Production and Managerial Arts',null,null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','174','Software and Systems Development',null,null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','196','Biotechnology',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','198','Patient Care',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','216','Product Innovation and Design',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','221','Systems Diagnostics, Services, and Repair',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','231','Legal Practices',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687600000000','Nipomo High','2016-2017','101','Agricultural Mechanics','Yes','Yes',null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687600000000','Nipomo High','2016-2017','103','Animal Science','Yes','Yes',null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40687600000000','Nipomo High','2016-2017','105','Ornamental Horticulture','Yes','Yes',null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40687600000000','Nipomo High','2016-2017','198','Patient Care','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40687600000000','Nipomo High','2016-2017','232','Public Safety','Yes','Yes',null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','101','Agricultural Mechanics','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','102','Agriscience','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','103','Animal Science','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','105','Ornamental Horticulture','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','111','Design, Visual, and Media Arts','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','113','Production and Managerial Arts','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','123','Residential and Commercial Construction','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','130','Child Development','Yes',null,null,'Yes','Yes','yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','132','Education','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','174','Software and Systems Development','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','198','Patient Care','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','201','Food Service and Hospitality','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','221','Systems Diagnostics, Services, and Repair','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40104100000000','San Luis Obispo County Office of Education','2016-2017','111','Design, Visual, and Media Arts',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40104100000000','San Luis Obispo County Office of Education','2016-2017','202','Hospitality, Tourism, and Recreation',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','100','Agricultural Business','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','102','Agriscience','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','103','Animal Science','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','105','Ornamental Horticulture','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','111','Design, Visual, and Media Arts','Yes',null,null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','120','Cabinetmaking, Millwork, and Woodworking','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','123','Residential and Commercial Construction','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','130','Child Development','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','152','Engineering Design','Yes','Yes',null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','160','Fashion Design and Merchandising','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','161','Interior Design','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','170','Information Support and Services','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','172','Networking','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','174','Software and Systems Development','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','202','Hospitality, Tourism, and Recreation',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','216','Product Innovation and Design','Yes',null,null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','221','Systems Diagnostics, Services, and Repair',null,null,null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40104100000000','Shandon High','2016-2017','101','Agricultural Mechanics','Yes','Yes',null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','40104100000000','Shandon High','2016-2017','102','Agriscience','Yes','Yes',null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40104100000000','Shandon High','2016-2017','105','Ornamental Horticulture','Yes','Yes',null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','40104100000000','Shandon High','2016-2017','174','Software and Systems Development',null,'Yes',null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','46688414037701','Templeton High','2016-2017','101','Agricultural Mechanics','Yes',null,null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','47688414037701','Templeton High','2016-2017','103','Animal Science','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','48688414037701','Templeton High','2016-2017','105','Ornamental Horticulture','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','49688414037701','Templeton High','2016-2017','112','Performing Arts','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','50688414037701','Templeton High','2016-2017','113','Production and Managerial Arts','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
		('San Luis Obispo Partners In Education*','51688414037701','Templeton High','2016-2017','143','Energy and Power Technology','Yes','Yes',null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
		('San Luis Obispo Partners In Education*','52688414037701','Templeton High','2016-2017','198','Patient Care','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes')
	) t (ConsortiumName,CdsCode,CdsName,AcademicYear,PathwayCode,PathwayName,course_00,course_01,course_02,course_03,course_04,course_05,student_00,student_01,student_02,student_03,instructor_00,instructor_01,instructor_02,instructor_03)
	inner join
	lbccpt.Consortium c
		on c.Name = t.ConsortiumName
	inner join
	lbccpt.School s
		on s.Name = t.CdsName
	inner join
	lbccpt.Consortium_School cs
		on cs.ConsortiumId = c.Id
		and cs.SchoolId = s.Id
	inner join
	lbccpt.AcademicYear y
		on y.CodeCalpads = t.AcademicYear
	inner join
	lbccpt.SchoolPathway sp
		on sp.Code = PathwayCode;

MERGE
	lbccpt.School_SchoolPathway t
USING
	(
		SELECT DISTINCT
			ConsortiumSchoolId,
			AcademicYearId,
			SchoolPathwayId
		FROM
			#SloData d
	) s
		on t.ConsortiumSchoolId = s.ConsortiumSchoolId
		and t.AcademicYearId = s.AcademicYearId
		and t.SchoolPathwayId = s.SchoolPathwayId
WHEN MATCHED THEN
	UPDATE SET
		t.ConsortiumSchoolId = s.ConsortiumSchoolId,
		t.AcademicYearId = s.AcademicYearId,
		t.SchoolPathwayId = s.SchoolPathwayId
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			ConsortiumSchoolId,
			AcademicYearId,
			SchoolPathwayId
		)
	VALUES
		(
			s.ConsortiumSchoolId,
			s.AcademicYearId,
			s.SchoolPathwayId
		)
-- Collect School_SchoolPathway.Id
OUTPUT
	inserted.Id,
	inserted.ConsortiumSchoolId,
	inserted.AcademicYearId,
	inserted.SchoolPathwayId
INTO
	@School_SchoolPathway
	(
		Id,
		ConsortiumSchoolId,
		AcademicYearId,
		SchoolPathwayId
	);

-- Update Temp Table
UPDATE
	d
SET
	d.School_SchoolPathwayId = ssp.Id
FROM
	#SloData d
	inner join
	@School_SchoolPathway ssp
		on ssp.ConsortiumSchoolId = d.ConsortiumSchoolId
		and ssp.AcademicYearId = d.AcademicYearId
		and ssp.SchoolPathwayId = d.SchoolPathwayId;

-- Populate Pathway Course
MERGE
	lbccpt.SchoolPathway_SchoolPathwayCourse
WITH
	(
		TABLOCK
	) t
USING
	(
		SELECT
			SchoolPathwayId = u.School_SchoolPathwayId,
			SchoolPathwayCourseId = spc.Id
		FROM
			#SloData
		UNPIVOT
			(
				val for Name in (
					course_00,
					course_01,
					course_02,
					course_03,
					course_04,
					course_05
				)
			) u
			inner join
			lbccpt.SchoolPathwayCourse spc
				on spc.Name = u.Name
		WHERE
			u.val = 1
	) s
ON
	t.SchoolPathwayId = s.SchoolPathwayId
	and t.SchoolPathwayCourseId = s.SchoolPathwayCourseId
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			SchoolPathwayId,
			SchoolPathwayCourseId
		)
	VALUES
		(
			s.SchoolPathwayId,
			s.SchoolPathwayCourseId
		)
WHEN NOT MATCHED BY SOURCE
	and (exists (SELECT 1 FROM #SloData tt WHERE tt.School_SchoolPathwayId = t.SchoolPathwayId)) THEN
	DELETE;

-- Populate Pathway Student
MERGE
	lbccpt.SchoolPathway_SchoolPathwayStudent
WITH
	(
		TABLOCK
	) t
USING
	(
		SELECT
			SchoolPathwayId = u.School_SchoolPathwayId,
			SchoolPathwayStudentId = sps.Id
		FROM
			#SloData
		UNPIVOT
			(
				val for Name in (
					student_00,
					student_01,
					student_02,
					student_03
				)
			) u
			inner join
			lbccpt.SchoolPathwayStudent sps
				on sps.Name = u.Name
		WHERE
			u.val = 1
	) s
ON
	t.SchoolPathwayId = s.SchoolPathwayId
	and t.SchoolPathwayStudentId = s.SchoolPathwayStudentId
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			SchoolPathwayId,
			SchoolPathwayStudentId
		)
	VALUES
		(
			s.SchoolPathwayId,
			s.SchoolPathwayStudentId
		)
WHEN NOT MATCHED BY SOURCE
	and (exists (SELECT 1 FROM #SloData tt WHERE tt.School_SchoolPathwayId = t.SchoolPathwayId)) THEN
	DELETE;

-- Populate Pathway Instructor
MERGE
	lbccpt.SchoolPathway_SchoolPathwayInstructor
WITH
	(
		TABLOCK
	) t
USING
	(
		SELECT
			SchoolPathwayId = u.School_SchoolPathwayId,
			SchoolPathwayInstructorId = spi.Id
		FROM
			#SloData
		UNPIVOT
			(
				val for Name in (
					instructor_00,
					instructor_01,
					instructor_02,
					instructor_03
				)
			) u
			inner join
			lbccpt.SchoolPathwayInstructor spi
				on spi.Name = u.Name
		WHERE
			u.val = 1
	) s
ON
	t.SchoolPathwayId = s.SchoolPathwayId
	and t.SchoolPathwayInstructorId = s.SchoolPathwayInstructorId
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			SchoolPathwayId,
			SchoolPathwayInstructorId
		)
	VALUES
		(
			s.SchoolPathwayId,
			s.SchoolPathwayInstructorId
		)
WHEN NOT MATCHED BY SOURCE
	and (exists (SELECT 1 FROM #SloData tt WHERE tt.School_SchoolPathwayId = t.SchoolPathwayId)) THEN
	DELETE;