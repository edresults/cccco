IF (object_id('tempdb..#SloData') is not null)
	BEGIN
		DROP TABLE #SloData;
	END;

GO

SELECT
	ConsortiumId = c.Id,
	ConsortiumName,
	SchoolId = fix.SchoolId,
	CdsCode = t.CdsCode,
	ConsortiumSchoolId = cs.Id,
	AcademicYearId = y.Id,
	AcademicYear,
	SchoolPathwayId = sp.Id,
	PathwayCode,
	School_SchoolPathwayId = ssp.Id,
	metric_00,
	metric_01,
	metric_02,
	metric_03,
	metric_04,
	metric_05,
	metric_06,
	metric_07,
	metric_08,
	metric_09,
	metric_10,
	metric_11,
	metric_12,
	metric_13,
	metric_14,
	metric_15,
	metric_16,
	metric_17,
	metric_18,
	metric_19,
	metric_20
INTO
	#SloData
FROM
	(
		VALUES
		('San Luis Obispo Partners In Education*','40754600000000','2016-2017','101',84,27,7,0,22,5,3,5,4,0,7,118,4,2,1,3,118,19,0,0,2),
		('San Luis Obispo Partners In Education*','40754600000000','2016-2017','102',104,28,0,0,2,0,0,0,0,0,0,0,0,0,0,0,132,2,0,0,13),
		('San Luis Obispo Partners In Education*','40754600000000','2016-2017','103',54,33,12,0,18,9,4,8,7,0,45,6,0,0,6,2,99,12,0,0,48),
		('San Luis Obispo Partners In Education*','40754600000000','2016-2017','105',45,49,10,0,25,1,0,1,1,0,5,5,4,0,22,4,99,24,0,0,46),
		('San Luis Obispo Partners In Education*','40754600000000','2016-2017','111',101,18,20,0,36,8,7,7,10,20,38,5,4,2,3,4,69,25,0,0,0),
		('San Luis Obispo Partners In Education*','40754600000000','2016-2017','113',0,29,27,0,27,8,7,10,5,0,27,5,5,0,9,0,4,13,0,0,0),
		('San Luis Obispo Partners In Education*','40754600000000','2016-2017','123',59,7,9,0,21,2,3,2,5,0,5,0,5,5,2,2,75,15,0,0,0),
		('San Luis Obispo Partners In Education*','40754600000000','2016-2017','130',89,24,21,0,49,7,7,6,8,0,18,12,4,4,2,2,134,36,0,0,0),
		('San Luis Obispo Partners In Education*','40754600000000','2016-2017','132',87,32,28,0,58,9,9,9,11,0,75,20,28,3,0,1,147,42,0,0,0),
		('San Luis Obispo Partners In Education*','40754600000000','2016-2017','174',86,54,25,0,19,2,0,1,1,0,25,5,0,4,2,0,24,18,0,0,0),
		('San Luis Obispo Partners In Education*','40754600000000','2016-2017','198',90,19,14,0,43,14,7,12,11,14,14,7,7,3,0,0,34,23,0,0,0),
		('San Luis Obispo Partners In Education*','40754600000000','2016-2017','201',96,12,12,0,51,2,1,2,3,12,24,4,0,12,6,0,12,51,0,6,0),
		('San Luis Obispo Partners In Education*','40754600000000','2016-2017','221',85,33,18,0,39,2,2,3,3,18,10,8,6,5,0,2,18,28,0,0,0),
		('San Luis Obispo Partners In Education*','40688300000000','2016-2017','101',15,12,16,2,0,6,8,13,12,1,1,0,0,2,0,0,14,5,0,0,0),
		('San Luis Obispo Partners In Education*','40688300000000','2016-2017','105',0,0,8,0,0,3,5,4,4,1,3,0,0,0,0,0,8,3,0,0,0),
		('San Luis Obispo Partners In Education*','40688300000000','2016-2017','102',9,36,8,5,1,2,3,6,5,3,0,0,0,0,0,0,20,10,0,0,0),
		('San Luis Obispo Partners In Education*','40104100000000','2016-2017','174',14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0),
		('San Luis Obispo Partners In Education*','40688414037701','2015-2016','101',0,0,27,30,2,4,3,4,4,3,0,0,20,0,0,85,4,0,11,0,0),
		('San Luis Obispo Partners In Education*','41688414037701','2015-2016','103',0,40,46,46,8,9,8,8,10,4,0,1,16,0,0,52,10,0,0,0,0),
		('San Luis Obispo Partners In Education*','42688414037701','2015-2016','105',0,87,16,21,0,1,0,1,1,0,0,0,0,0,0,86,1,0,0,0,0),
		('San Luis Obispo Partners In Education*','43688414037701','2015-2016','112',0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,1,0,0,0,0),
		('San Luis Obispo Partners In Education*','44688414037701','2015-2016','113',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','45688414037701','2015-2016','143',0,52,20,14,13,13,13,12,14,0,0,0,0,0,0,0,14,0,0,0,0),
		('San Luis Obispo Partners In Education*','46688414037701','2015-2016','198',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40687000000000','2016-2017','101',33,48,6,0,2,4,2,4,2,0,3,3,3,3,0,3,87,12,0,87,0),
		('San Luis Obispo Partners In Education*','40687000000000','2016-2017','103',42,37,13,0,2,11,7,15,9,0,3,3,3,3,0,4,92,3,0,92,0),
		('San Luis Obispo Partners In Education*','40687000000000','2016-2017','111',61,187,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,61,0,0,0),
		('San Luis Obispo Partners In Education*','40687000000000','2016-2017','130',76,132,3,0,2,3,3,1,2,0,10,10,10,10,0,10,0,61,0,78,0),
		('San Luis Obispo Partners In Education*','40687000000000','2016-2017','174',48,57,1,0,0,1,1,1,0,0,57,0,0,0,0,0,0,14,0,0,0),
		('San Luis Obispo Partners In Education*','40687000000000','2016-2017','201',57,29,2,0,0,1,1,0,0,0,0,0,0,0,0,0,0,53,0,0,0),
		('San Luis Obispo Partners In Education*','40687000000000','2016-2017','221',0,127,13,0,1,5,6,8,6,0,1,1,1,1,0,0,13,16,0,13,0),
		('San Luis Obispo Partners In Education*','40754700000000','2016-2017','111',79,11,0,0,0,0,0,0,0,0,0,0,0,79,0,0,0,18,0,0,0),
		('San Luis Obispo Partners In Education*','40754700000000','2016-2017','103',72,5,0,0,0,0,0,0,0,0,0,0,0,72,0,0,136,22,0,0,0),
		('San Luis Obispo Partners In Education*','40104050101725','2016-2017','221',109,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40104050101725','2016-2017','111',42,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40104050101725','2016-2017','123',55,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40104050101725','2016-2017','182',46,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40104050101725','2016-2017','182',37,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40104050101725','2016-2017','174',13,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40104050101725','2016-2017','182',403,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40104100000000','2015-2016','111',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40104100000000','2015-2016','202',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','100',0,52,29,0,5,29,29,29,29,29,0,3,0,0,0,0,0,3,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','102',100,60,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','103',0,50,7,0,2,7,7,7,7,7,0,0,0,0,0,0,16,13,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','105',0,24,0,0,1,0,0,0,0,0,0,0,0,0,0,0,24,7,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','111',132,20,0,0,0,0,0,0,0,0,1,0,2,0,0,0,0,9,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','113',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','120',125,75,4,0,3,4,4,4,4,4,0,0,0,0,0,0,0,13,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','123',54,22,2,0,3,2,2,2,2,2,0,0,0,0,0,0,0,6,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','130',68,0,7,0,8,7,7,7,7,7,0,0,5,0,0,0,1,34,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','152',28,18,11,0,4,11,11,11,11,11,0,1,0,0,0,0,0,8,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','160',70,29,8,0,5,8,8,8,8,8,0,0,6,0,0,0,8,11,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','161',70,27,9,0,4,9,9,9,9,9,0,0,0,0,0,0,0,8,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','170',70,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,11,0,0,1,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','174',70,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','172',20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,13,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','196',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','198',0,35,26,0,8,26,26,26,26,26,8,5,0,0,0,0,0,10,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','202',139,29,2,0,4,2,2,2,2,2,0,0,0,3,0,0,3,0,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','216',80,36,21,0,11,21,21,21,21,21,0,4,0,0,0,0,0,26,0,0,0),
		('San Luis Obispo Partners In Education*','40688100000000','2015-2016','221',107,40,19,0,17,19,19,19,19,19,0,4,10,0,0,0,9,37,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030557','2016-2017','101',34,23,25,4,1,9,6,20,15,22,5,3,6,6,0,0,34,0,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030557','2016-2017','103',126,27,0,0,0,0,0,0,0,0,3,4,4,4,0,0,61,0,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030557','2016-2017','105',60,0,5,0,0,0,0,0,0,0,4,4,5,5,0,0,26,0,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030557','2016-2017','111',84,28,5,0,1,3,3,2,4,5,0,0,0,0,0,0,5,5,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030557','2016-2017','113',0,8,5,0,0,0,3,3,2,4,10,8,5,6,0,0,5,5,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030557','2016-2017','130',31,20,7,0,0,0,0,0,0,0,0,0,0,10,0,0,0,0,0,27,0),
		('San Luis Obispo Partners In Education*','40687594030557','2016-2017','141',26,28,19,0,2,5,8,4,12,19,20,50,20,20,0,0,0,0,0,19,0),
		('San Luis Obispo Partners In Education*','40687594030557','2016-2017','150',59,72,66,0,8,20,19,21,22,28,0,15,0,0,0,0,20,20,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030557','2016-2017','198',18,22,6,0,2,2,3,2,2,4,7,8,8,10,0,0,0,0,0,40,0),
		('San Luis Obispo Partners In Education*','40687594030557','2016-2017','202',64,23,14,0,1,5,4,4,5,9,0,0,0,0,0,0,10,28,0,37,0),
		('San Luis Obispo Partners In Education*','40687594030557','2016-2017','221',52,19,11,0,0,0,1,3,4,9,0,0,0,0,0,0,0,24,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2016-2017','101',29,17,10,0,0,4,2,7,2,10,3,5,6,3,0,0,50,29,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2016-2017','103',83,19,0,0,2,13,2,3,3,3,6,10,8,5,0,0,91,2,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2016-2017','105',92,14,37,0,1,2,3,3,2,6,4,7,8,6,0,0,128,25,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2016-2017','198',15,20,0,0,0,0,0,0,0,0,12,0,0,0,0,0,0,1,0,20,0),
		('San Luis Obispo Partners In Education*','40687594030268','2016-2017','232',44,31,23,0,2,7,3,6,7,11,0,0,0,0,0,0,5,18,0,23,0),
		('San Luis Obispo Partners In Education*','40687590125328','2016-2017','111',92,50,0,0,17,34,30,31,34,45,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40687590125328','2016-2017','174',54,19,0,0,20,25,19,25,28,37,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030557','2015-2016','212',28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030557','2015-2016','216',0,0,0,0,0,8,7,8,8,6,0,0,0,0,0,0,0,2,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030557','2015-2016','221',55,33,13,0,0,31,44,44,40,39,0,0,0,0,0,0,0,24,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2015-2016','100',0,0,48,0,9,39,42,42,41,33,3,4,4,5,0,0,30,26,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2015-2016','101',48,40,22,0,2,49,58,70,53,56,3,5,6,3,0,0,40,29,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2015-2016','102',130,0,0,0,0,14,14,16,11,15,2,8,7,4,0,0,10,0,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2015-2016','103',0,17,0,0,0,13,11,15,13,13,6,10,8,5,0,0,10,2,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2015-2016','105',51,15,0,0,0,33,37,40,36,27,4,7,8,6,0,0,30,25,0,31,0),
		('San Luis Obispo Partners In Education*','40687594030268','2015-2016','106',0,43,0,0,0,32,35,40,36,30,2,6,4,4,0,0,30,0,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2015-2016','130',7,0,0,0,0,3,3,4,2,2,0,0,0,0,0,0,0,2,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2015-2016','131',0,3,0,0,0,2,2,3,2,2,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2015-2016','180',0,71,0,0,0,47,67,66,51,44,0,0,0,0,0,0,0,37,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2015-2016','198',13,0,0,0,0,1,2,2,2,2,12,0,0,0,0,0,0,1,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2015-2016','200',2,8,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2015-2016','201',0,0,0,0,0,5,7,6,5,5,0,0,0,0,0,0,0,3,0,0,0),
		('San Luis Obispo Partners In Education*','40687594030268','2015-2016','232',52,33,39,0,1,50,48,61,56,55,0,0,0,0,0,0,0,18,0,0,0),
		('San Luis Obispo Partners In Education*','40687590125328','2015-2016','201',0,1,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0)
	) t (ConsortiumName, CdsCode, AcademicYear, PathwayCode, metric_00, metric_01, metric_02, metric_03, metric_04, metric_05, metric_06, metric_07, metric_08, metric_09, metric_10, metric_11, metric_12, metric_13, metric_14, metric_15, metric_16, metric_17, metric_18, metric_19, metric_20)
	inner join
	(
		SELECT DISTINCT
			CdsCode,
			CdsName,
			SchoolId = s.Id
		FROM
			(
				VALUES
				('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','101','Agricultural Mechanics','Yes','Yes',null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','103','Animal Science','Yes','Yes',null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','105','Ornamental Horticulture','Yes','Yes',null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','111','Design, Visual, and Media Arts','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','113','Production and Managerial Arts','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','130','Child Development','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','141','Environmental Resources','Yes','Yes',null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','150','Architectural Design','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','198','Patient Care','Yes','Yes',null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','202','Hospitality, Tourism, and Recreation','Yes',null,null,null,'Yes','','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687600000000','Arroyo Grande High','2016-2017','221','Systems Diagnostics, Services, and Repair','Yes',null,null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687000000000','Atascadero High','2016-2017','101','Agricultural Mechanics','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687000000000','Atascadero High','2016-2017','103','Animal Science','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40687000000000','Atascadero High','2016-2017','111','Design, Visual, and Media Arts','Yes','Yes',null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687000000000','Atascadero High','2016-2017','221','Systems Diagnostics, Services, and Repair','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687000000000','Atascadero Unified','2016-2017','130','Child Development','Yes',null,null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687000000000','Atascadero Unified','2016-2017','174','Software and Systems Development','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687000000000','Atascadero Unified','2016-2017','201','Food Service and Hospitality','Yes',null,null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687600000000','Central Coast New Tech High','2016-2017','111','Design, Visual, and Media Arts','Yes','Yes','Yes','Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687600000000','Central Coast New Tech High','2016-2017','174','Software and Systems Development','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes'),
				('San Luis Obispo Partners In Education*','40754700000000','Coast Unified','2016-2017','103','Animal Science','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40754700000000','Coast Unified','2016-2017','111','Design, Visual, and Media Arts','Yes','Yes',null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40104100000000','Grizzly Youth Academy','2016-2017',null,null,null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,null,null),
				('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','100','Agricultural Business',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','102','Agriscience',null,null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','103','Animal Science',null,null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','111','Design, Visual, and Media Arts',null,'Yes',null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','113','Production and Managerial Arts',null,null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','174','Software and Systems Development',null,null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','196','Biotechnology',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','198','Patient Care',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','216','Product Innovation and Design',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','221','Systems Diagnostics, Services, and Repair',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','Morro Bay High','2016-2017','231','Legal Practices',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687600000000','Nipomo High','2016-2017','101','Agricultural Mechanics','Yes','Yes',null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687600000000','Nipomo High','2016-2017','103','Animal Science','Yes','Yes',null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40687600000000','Nipomo High','2016-2017','105','Ornamental Horticulture','Yes','Yes',null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40687600000000','Nipomo High','2016-2017','198','Patient Care','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40687600000000','Nipomo High','2016-2017','232','Public Safety','Yes','Yes',null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','101','Agricultural Mechanics','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','102','Agriscience','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','103','Animal Science','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','105','Ornamental Horticulture','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','111','Design, Visual, and Media Arts','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','113','Production and Managerial Arts','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','123','Residential and Commercial Construction','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','130','Child Development','Yes',null,null,'Yes','Yes','yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','132','Education','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','174','Software and Systems Development','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','198','Patient Care','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','201','Food Service and Hospitality','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40754600000000','Paso Robles High','2016-2017','221','Systems Diagnostics, Services, and Repair','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40104100000000','San Luis Obispo County Office of Education','2016-2017','111','Design, Visual, and Media Arts',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40104100000000','San Luis Obispo County Office of Education','2016-2017','202','Hospitality, Tourism, and Recreation',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','100','Agricultural Business','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','102','Agriscience','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','103','Animal Science','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','105','Ornamental Horticulture','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','111','Design, Visual, and Media Arts','Yes',null,null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','120','Cabinetmaking, Millwork, and Woodworking','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','123','Residential and Commercial Construction','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','130','Child Development','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','152','Engineering Design','Yes','Yes',null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','160','Fashion Design and Merchandising','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','161','Interior Design','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','170','Information Support and Services','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','172','Networking','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','174','Software and Systems Development','Yes',null,null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','202','Hospitality, Tourism, and Recreation',null,null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','216','Product Innovation and Design','Yes',null,null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40688100000000','San Luis Obispo High','2016-2017','221','Systems Diagnostics, Services, and Repair',null,null,null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40104100000000','Shandon High','2016-2017','101','Agricultural Mechanics','Yes','Yes',null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','40104100000000','Shandon High','2016-2017','102','Agriscience','Yes','Yes',null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40104100000000','Shandon High','2016-2017','105','Ornamental Horticulture','Yes','Yes',null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','40104100000000','Shandon High','2016-2017','174','Software and Systems Development',null,'Yes',null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','46688414037701','Templeton High','2016-2017','101','Agricultural Mechanics','Yes',null,null,null,'Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','47688414037701','Templeton High','2016-2017','103','Animal Science','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','48688414037701','Templeton High','2016-2017','105','Ornamental Horticulture','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','49688414037701','Templeton High','2016-2017','112','Performing Arts','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','50688414037701','Templeton High','2016-2017','113','Production and Managerial Arts','Yes',null,null,'Yes','Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes',null),
				('San Luis Obispo Partners In Education*','51688414037701','Templeton High','2016-2017','143','Energy and Power Technology','Yes','Yes',null,'Yes','Yes','Yes','Yes','Yes',null,'Yes',null,null,'Yes','Yes'),
				('San Luis Obispo Partners In Education*','52688414037701','Templeton High','2016-2017','198','Patient Care','Yes',null,null,null,'Yes',null,'Yes','Yes',null,'Yes',null,null,'Yes','Yes')
			) f (ConsortiumName,CdsCode,CdsName,AcademicYear,PathwayCode,PathwayName,course_00,course_01,course_02,course_03,course_04,course_05,student_00,student_01,student_02,student_03,instructor_00,instructor_01,instructor_02,instructor_03)
			inner join
			lbccpt.School s
				on s.Name = f.CdsName
	) fix
		on t.CdsCode = fix.CdsCode
	inner join
	lbccpt.Consortium c
		on c.Name = t.ConsortiumName
	inner join
	lbccpt.Consortium_School cs
		on cs.ConsortiumId = c.Id
		and cs.SchoolId = fix.SchoolId
	inner join
	lbccpt.AcademicYear y
		on y.CodeCalpads = t.AcademicYear
	inner join
	lbccpt.SchoolPathway sp
		on sp.Code = PathwayCode
	inner join
	lbccpt.School_SchoolPathway ssp
		on ssp.ConsortiumSchoolId = cs.Id
		and ssp.AcademicYearId = y.Id
		and ssp.SchoolPathwayId = sp.Id;

MERGE
	lbccpt.SchoolPathway_SchoolPathwayMetric
WITH
	(
		TABLOCKX
	) t
USING
	(
		SELECT
			SchoolPathwayId = u.School_SchoolPathwayId,
			SchoolPathwayMetricId = spm.Id,
			MetricValue = u.MetricValue
		FROM
			#SloData i
		UNPIVOT
			(
				MetricValue for MetricName in (
					metric_00,
					metric_01,
					metric_02,
					metric_03,
					metric_04,
					metric_05,
					metric_06,
					metric_07,
					metric_08,
					metric_09,
					metric_10,
					metric_11,
					metric_12,
					metric_13,
					metric_14,
					metric_15,
					metric_16,
					metric_17,
					metric_18,
					metric_19,
					metric_20
				)
			) u
			inner join
			lbccpt.SchoolPathwayMetric spm
				on spm.Name = u.MetricName
	) s
ON
	t.SchoolPathwayId = s.SchoolPathwayId
	and t.SchoolPathwayMetricId = s.SchoolPathwayMetricId
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			SchoolPathwayId,
			SchoolPathwayMetricId,
			MetricValue
		)
	VALUES
		(
			s.SchoolPathwayId,
			s.SchoolPathwayMetricId,
			s.MetricValue
		)
WHEN MATCHED THEN
	UPDATE SET
		t.MetricValue = s.MetricValue;