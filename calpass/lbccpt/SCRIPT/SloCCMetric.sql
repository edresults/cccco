IF (object_id('tempdb..#SloData') is not null)
	BEGIN
		DROP TABLE #SloData;
	END;

GO

DECLARE
	@College_CollegePathway lbccpt.College_CollegePathway;

SELECT
	ConsortiumId = c.Id,
	ConsortiumName,
	CollegeId = s.Id,
	CollegeName,
	ConsortiumCollegeId = cs.Id,
	AcademicYearId = y.Id,
	AcademicYear,
	PathwayCode,
	PathwayName,
	CollegePathwayId = null,
	CollegePathwayTaxonomyId = tax.Id,
	metric_00,
	metric_01,
	metric_02,
	metric_03,
	metric_04,
	metric_05,
	metric_06,
	metric_07,
	metric_08,
	metric_09,
	metric_10,
	metric_11,
	metric_12,
	metric_13,
	metric_14,
	metric_15,
	metric_16,
	metric_17,
	metric_18,
	metric_19,
	metric_20,
	metric_21,
	metric_22,
	metric_23,
	metric_24
INTO
	#SloData
FROM
	(
		VALUES
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','Plant',       '010300','106',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','Ag Bus',      '011200','100',20,0,0,0,0,120,29,9,0,0,0,6,0,0,25,9,0,0,0,0,0,76,0,0,13),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','Ag Mech',     '011600','101',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','Arch',        '020100','150',61,0,0,0,0,240,102,5,0,0,0,1,0,0,0,12,0,25,0,0,0,22,0,0,0),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','Acct',        '050200','180',333,0,0,0,0,659,287,0,0,0,0,4,0,0,0,74,0,73,0,0,8,72,0,0,6),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','Bus General', '050100','182',0,1782,5346,0,0,786,35,107,0,0,0,24,0,0,3,0,0,603,0,0,60,15,0,0,0),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','Bus Mgmt',    '050500','182',0,40,160,0,0,1739,730,107,0,0,0,24,0,0,0,0,0,33,0,0,60,213,0,0,102),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','CAOA',        '051400','181',99,36,144,0,0,284,16,0,0,0,0,0,0,0,0,0,0,29,0,0,0,0,0,0,12),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','FTVE',        '060400','113',66,41,123,0,0,130,63,8,0,0,0,0,0,0,4,0,0,14,0,0,0,55,0,0,0),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','Graphics',    '061400','113',54,0,0,0,0,246,90,0,0,0,0,2,0,0,0,0,0,0,0,0,1,62,0,0,3),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','CIS',         '070200','174',285,103,309,0,0,540,248,10,0,0,0,0,0,0,0,0,0,64,0,0,3,28,0,0,0),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','CNET',        '070800','151',33,0,0,0,0,91,81,4,0,0,0,0,0,0,0,0,0,0,0,0,7,63,0,0,0),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','ECE',         '130500','130',650,0,0,1,3,1224,819,41,0,0,0,16,0,0,0,134,0,297,0,0,10,342,0,0,3),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','Engr',        '095300','150',204,176,528,0,0,634,292,9,0,0,0,0,0,0,0,72,0,62,0,0,1,42,0,0,1),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','ETCH',        '095220','143',189,0,0,0,0,57,7,4,0,0,0,1,0,0,0,2,0,36,0,0,6,0,0,0,28),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','AUTO',        '094800','221',99,285,855,0,0,370,201,4,0,0,0,3,0,0,4,1,0,29,0,0,2,42,0,0,0),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','Collison',    '094900','220',47,0,0,0,0,64,34,0,0,0,0,0,0,0,0,0,0,24,0,0,1,29,0,0,0),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','CTCH',        '095200','220',76,68,204,0,0,273,136,2,0,0,0,2,0,0,14,3,0,39,0,0,1,0,0,0,2),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','WELD',        '095600','213',123,67,201,0,0,374,148,3,0,0,0,1,0,0,6,15,0,30,0,0,5,57,0,0,39),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','C Mus',       '100500','112',48,0,0,0,0,75,27,0,0,0,0,0,0,0,0,1,0,0,0,0,8,56,0,0,160),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','Grp',         '103000','111',54,0,0,0,0,246,90,0,0,0,0,2,0,0,0,0,0,0,0,0,1,38,0,0,7),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','Health',      '120000','198',572,0,0,0,0,643,450,145,0,0,0,16,0,0,5,77,0,391,0,0,316,14,0,0,10),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','Culinary',    '130600','201',6,53,106,0,0,0,0,13,0,0,0,6,0,0,2,65,0,133,0,0,1,0,0,0,0),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','CJ',          '210500','232',194,0,0,0,0,460,258,31,0,0,0,2,0,0,1,23,0,34,0,0,0,65,0,0,48),
		('San Luis Obispo Partners In Education*','Cuesta College','2016-2017','Para',        '140200','231',53,0,0,0,0,117,113,3,0,0,0,0,0,0,0,1,0,7,0,0,7,50,0,0,1)
	) t (ConsortiumName,CollegeName,AcademicYear,PathwayName,TopCode,PathwayCode,metric_00,metric_01,metric_02,metric_03,metric_04,metric_05,metric_06,metric_07,metric_08,metric_09,metric_10,metric_11,metric_12,metric_13,metric_14,metric_15,metric_16,metric_17,metric_18,metric_19,metric_20,metric_21,metric_22,metric_23,metric_24)
	inner join
	lbccpt.Consortium c
		on c.Name = t.ConsortiumName
	inner join
	lbccpt.College s
		on s.Name = t.CollegeName
	inner join
	lbccpt.Consortium_College cs
		on cs.ConsortiumId = c.Id
		and cs.CollegeId = s.Id
	inner join
	lbccpt.AcademicYear y
		on y.CodeCalpads = t.AcademicYear
	inner join
	lbccpt.CollegePathwayTaxonomy tax
		on tax.Code = t.TopCode;

MERGE
	lbccpt.College_CollegePathway t
USING
	#SloData s
ON
	t.ConsortiumCollegeId = s.ConsortiumCollegeId
	and t.Name = s.PathwayName
	and t.AcademicYearId = s.AcademicYearId
WHEN MATCHED THEN
	UPDATE SET
		t.Name = s.PathwayName
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			ConsortiumCollegeId,
			Name,
			AcademicYearId
		)
	VALUES
		(
			s.ConsortiumCollegeId,
			s.PathwayName,
			s.AcademicYearId
		)
OUTPUT
	inserted.Id,
	inserted.ConsortiumCollegeId,
	inserted.Name,
	inserted.AcademicYearId
INTO
	@College_CollegePathway
	(
		Id,
		ConsortiumCollegeId,
		Name,
		AcademicYearId
	);

UPDATE
	t
SET
	t.CollegePathwayId = s.Id
FROM
	#SloData t
	inner join
	@College_CollegePathway s
		on t.ConsortiumCollegeId = s.ConsortiumCollegeId
		and t.PathwayName = s.Name
		and t.AcademicYearId = s.AcademicYearId;

MERGE
	lbccpt.CollegePathway_CollegePathwayTaxonomy t
USING
	#SloData s
ON
	t.CollegePathwayId = s.CollegePathwayId
	and t.CollegePathwayTaxonomyId = s.CollegePathwayTaxonomyId
WHEN MATCHED THEN
	UPDATE SET
		t.CollegePathwayTaxonomyId = s.CollegePathwayTaxonomyId
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			CollegePathwayId,
			CollegePathwayTaxonomyId
		)
	VALUES
		(
			s.CollegePathwayId,
			s.CollegePathwayTaxonomyId
		);

MERGE
	lbccpt.CollegePathway_CollegePathwayMetric
WITH
	(
		TABLOCKX
	) t
USING
	(
		SELECT
			CollegePathwayId = u.CollegePathwayId,
			CollegePathwayMetricId = spm.Id,
			MetricValue = u.MetricValue
		FROM
			#SloData i
		UNPIVOT
			(
				MetricValue for MetricName in (
					metric_00,
					metric_01,
					metric_02,
					metric_03,
					metric_04,
					metric_05,
					metric_06,
					metric_07,
					metric_08,
					metric_09,
					metric_10,
					metric_11,
					metric_12,
					metric_13,
					metric_14,
					metric_15,
					metric_16,
					metric_17,
					metric_18,
					metric_19,
					metric_20,
					metric_21,
					metric_22,
					metric_23,
					metric_24
				)
			) u
			inner join
			lbccpt.CollegePathwayMetric spm
				on spm.Name = u.MetricName
	) s
ON
	t.CollegePathwayId = s.CollegePathwayId
	and t.CollegePathwayMetricId = s.CollegePathwayMetricId
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			CollegePathwayId,
			CollegePathwayMetricId,
			MetricValue
		)
	VALUES
		(
			s.CollegePathwayId,
			s.CollegePathwayMetricId,
			s.MetricValue
		)
WHEN MATCHED THEN
	UPDATE SET
		t.MetricValue = s.MetricValue;