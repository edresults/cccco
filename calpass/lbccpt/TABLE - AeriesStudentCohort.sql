USE calpass;

GO

IF (object_id('lbccpt.AeriesStudentCohort') is not null)
	BEGIN
		DROP TABLE lbccpt.AeriesStudentCohort;
	END;

GO

CREATE TABLE
	lbccpt.AeriesStudentCohort
	(
		CdsCode nchar(14) not null,
		Ssid nvarchar(10) not null,
		StudentId nvarchar(15),
		NameLast nvarchar(50),
		NameFirst nvarchar(30),
		Gender nchar(1),
		Birthdate nchar(8),
		StudentYear int not null,
		ActivityId int
	);
