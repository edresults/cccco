DECLARE
	@UserId int = 5552,
	@ConsortiumId int = 1;

-- dropdown: Please select the consortium; on page: Create Pathway Program for College Site
-- key|val pair of consortia for user
SELECT
	[val] = c.Name,
	[key] = c.Id
FROM
	calpass.lbccpt.Consortium c
WHERE
	c.UserId = @UserId;

-- dropdown: Please select the site; on page: Create Pathway Program for College Site
-- key|val pair of Colleges for specific Consortium; cascades from [val] above
SELECT
	[val] = s.Label,
	[key] = cc.Id
FROM
	calpass.lbccpt.Consortium c
	inner join
	calpass.lbccpt.Consortium_College cc
		on c.Id = cc.ConsortiumId
	inner join
	calpass.lbccpt.College s
		on s.Id = cc.CollegeId
WHERE
	c.UserId = @UserId
ORDER BY
	s.Ordinal;

-- dropdown: Please select the grant year for the following program characteristics; on page: Create Pathway Program for College Site
-- key|val pair of Colleges for specific Consortium; cascades from [val] above
SELECT
	[val] = CodeCalpads,
	[key] = Id
FROM
	calpass.lbccpt.AcademicYear
ORDER BY
	Ordinal;

-- EDIT PATHWAY
SELECT
	[val] = s.Label + ' | ' + ay.CodeCalpads + ' | ' + ssp.Name,
	[key] = ssp.Id
FROM
	calpass.lbccpt.Consortium c
	inner join
	calpass.lbccpt.Consortium_College cc
		on c.Id = cc.ConsortiumId
	inner join
	calpass.lbccpt.College s
		on s.Id = cc.CollegeId
	inner join
	calpass.lbccpt.College_CollegePathway ssp
		on ssp.ConsortiumCollegeId = cc.Id
	inner join
	calpass.lbccpt.AcademicYear ay
		on ay.Id = ssp.AcademicYearId
WHERE
	c.UserId = @UserId
	and c.Id = @ConsortiumId
ORDER BY
	s.Ordinal;