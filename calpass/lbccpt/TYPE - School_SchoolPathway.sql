USE calpass;

GO

IF (type_id('lbccpt.School_SchoolPathway') is not null)
	BEGIN
		DROP TYPE lbccpt.School_SchoolPathway;
	END;

GO

CREATE TYPE
	lbccpt.School_SchoolPathway
AS TABLE
	(
		Id int,
		ConsortiumSchoolId int not null,
		AcademicYearId int not null,
		SchoolPathwayId int not null,
		Action nvarchar(10),
		INDEX
			ixc_School_SchoolPathway__Id
		CLUSTERED
			(
				Id
			)
	);