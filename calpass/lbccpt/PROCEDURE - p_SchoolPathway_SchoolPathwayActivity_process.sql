DECLARE
	@activity
AS TABLE
	(
		ConsortiumId int,
		ConsortiumSchoolId int,
		AcademicYearId int,
		SchoolPathwayId int,
		School_SchoolPathwayId int,
		course_00 bit,
		course_01 bit,
		course_02 bit,
		course_03 bit,
		course_04 bit,
		course_05 bit,
		student_00 bit,
		student_01 bit,
		student_02 bit,
		student_03 bit,
		instructor_00 bit,
		instructor_01 bit,
		instructor_02 bit,
		instructor_03 bit
	);

INSERT INTO
	@activity
	(
		ConsortiumId,
		ConsortiumSchoolId,
		AcademicYearId,
		SchoolPathwayId,
		School_SchoolPathwayId,
		course_00,
		course_01,
		course_02,
		course_03,
		course_04,
		course_05,
		student_00,
		student_01,
		student_02,
		student_03,
		instructor_00,
		instructor_01,
		instructor_02,
		instructor_03
	)
SELECT
	ConsortiumId = c.Id,
	ConsortiumSchoolId = s.Id,
	AcademicYearId = ay.Id,
	SchoolPathwayId = sp.Id,
	School_SchoolPathwayId = ssp.Id,
	course_00 = isnull(course_00, 0),
	course_01 = isnull(course_01, 0),
	course_02 = isnull(course_02, 0),
	course_03 = isnull(course_03, 0),
	course_04 = isnull(course_04, 0),
	course_05 = isnull(course_05, 0),
	student_00 = isnull(student_00, 0),
	student_01 = isnull(student_01, 0),
	student_02 = isnull(student_02, 0),
	student_03 = isnull(student_03, 0),
	instructor_00 = isnull(instructor_00, 0),
	instructor_01 = isnull(instructor_01, 0),
	instructor_02 = isnull(instructor_02, 0),
	instructor_03 = isnull(instructor_03, 0)
FROM
	lbccpt.SchoolPathway_SchoolPathwayActivityUpload u
	inner join
	lbccpt.Consortium c
		on c.Name = u.ConsortiumName
		and c.UserId = u.UserId
	inner join
	lbccpt.School s
		on s.CodeCalpads = a.CdsCode
	inner join
	lbccpt.AcademicYear ay
		on ay.CodeCalpads = a.AcademicYear
	inner join
	lbccpt.SchoolPathway sp
		on sp.Code = a.SchoolPathwayCode
	inner join
	lbccpt.School_SchoolPathway ssp
		on ssp.ConsortiumSchoolId = s.Id
		and ssp.AcademicYearId = ay.Id
		and ssp.SchoolPathwayId = sp.Id;

-- Course
MERGE
	lbccpt.SchoolPathway_SchoolPathwayCourse
WITH
	(
		TABLOCK
	) t
USING
	(
		SELECT
			SchoolPathwayId = u.School_SchoolPathwayId,
			SchoolPathwayCourseId = spc.Id
		FROM
			@activity
		UNPIVOT
			(
				val for Name in (
					course_00,
					course_01,
					course_02,
					course_03,
					course_04,
					course_05
				)
			) u
			inner join
			lbccpt.SchoolPathwayCourse spc
				on spc.Name = u.Name
		WHERE
			u.val = 1
	) s
ON
	t.SchoolPathwayId = s.SchoolPathwayId
	and t.SchoolPathwayCourseId = s.SchoolPathwayCourseId
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			SchoolPathwayId,
			SchoolPathwayCourseId
		)
	VALUES
		(
			s.SchoolPathwayId,
			s.SchoolPathwayCourseId
		)
WHEN NOT MATCHED BY SOURCE
	and (exists (SELECT 1 FROM @activity tt WHERE tt.School_SchoolPathwayId = t.SchoolPathwayId)) THEN
	DELETE;

-- Student
MERGE
	lbccpt.SchoolPathway_SchoolPathwayStudent
WITH
	(
		TABLOCK
	) t
USING
	(
		SELECT
			SchoolPathwayId = u.School_SchoolPathwayId,
			SchoolPathwayStudentId = sps.Id
		FROM
			@activity
		UNPIVOT
			(
				val for Name in (
					student_00,
					student_01,
					student_02,
					student_03
				)
			) u
			inner join
			lbccpt.SchoolPathwayStudent sps
				on sps.Name = u.Name
		WHERE
			u.val = 1
	) s
ON
	t.SchoolPathwayId = s.SchoolPathwayId
	and t.SchoolPathwayStudentId = s.SchoolPathwayStudentId
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			SchoolPathwayId,
			SchoolPathwayStudentId
		)
	VALUES
		(
			s.SchoolPathwayId,
			s.SchoolPathwayStudentId
		)
WHEN NOT MATCHED BY SOURCE
	and (exists (SELECT 1 FROM @activity tt WHERE tt.School_SchoolPathwayId = t.SchoolPathwayId)) THEN
	DELETE;

-- Instructor
MERGE
	lbccpt.SchoolPathway_SchoolPathwayInstructor
WITH
	(
		TABLOCK
	) t
USING
	(
		SELECT
			SchoolPathwayId = u.School_SchoolPathwayId,
			SchoolPathwayInstructorId = spi.Id
		FROM
			@activity
		UNPIVOT
			(
				val for Name in (
					instructor_00,
					instructor_01,
					instructor_02,
					instructor_03
				)
			) u
			inner join
			lbccpt.SchoolPathwayInstructor spi
				on spi.Name = u.Name
		WHERE
			u.val = 1
	) s
ON
	t.SchoolPathwayId = s.SchoolPathwayId
	and t.SchoolPathwayInstructorId = s.SchoolPathwayInstructorId
WHEN NOT MATCHED BY TARGET THEN
	INSERT
		(
			SchoolPathwayId,
			SchoolPathwayInstructorId
		)
	VALUES
		(
			s.SchoolPathwayId,
			s.SchoolPathwayInstructorId
		)
WHEN NOT MATCHED BY SOURCE
	and (exists (SELECT 1 FROM @activity tt WHERE tt.School_SchoolPathwayId = t.SchoolPathwayId)) THEN
	DELETE;