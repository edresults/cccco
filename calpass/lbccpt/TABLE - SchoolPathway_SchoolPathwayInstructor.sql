USE calpass;

GO

IF (object_id('lbccpt.SchoolPathway_SchoolPathwayInstructor') is not null)
	BEGIN
		DROP TABLE lbccpt.SchoolPathway_SchoolPathwayInstructor;
	END;

GO

CREATE TABLE
	lbccpt.SchoolPathway_SchoolPathwayInstructor
	(
		Id int identity(1,1) not null,
		SchoolPathwayId int not null,
		SchoolPathwayInstructorId int not null,
		CONSTRAINT
			pk_SchoolPathway_SchoolPathwayInstructor__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		CONSTRAINT
			uq_SchoolPathway_SchoolPathwayInstructor__SchoolPathwayId__SchoolPathwayInstructorId
		UNIQUE
			(
				SchoolPathwayId,
				SchoolPathwayInstructorId
			),
		CONSTRAINT
			fk_SchoolPathway_SchoolPathwayInstructor__SchoolPathwayId
		FOREIGN KEY
			(
				SchoolPathwayId
			)
		REFERENCES
			lbccpt.School_SchoolPathway
			(
				Id
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_SchoolPathway_SchoolPathwayInstructor_SchoolPathwayInstructorId
		FOREIGN KEY
			(
				SchoolPathwayInstructorId
			)
		REFERENCES
			lbccpt.SchoolPathwayInstructor
			(
				Id
			)
		ON DELETE CASCADE
	);