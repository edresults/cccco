DELETE FROM lbccpt.Consortium;
dbcc checkident('lbccpt.Consortium', reseed, 0);
dbcc checkident('lbccpt.Consortium_School', reseed, 0);
dbcc checkident('lbccpt.School_SchoolPathway', reseed, 0);
dbcc checkident('lbccpt.SchoolPathway_SchoolPathwayInstructor', reseed, 0);
dbcc checkident('lbccpt.SchoolPathway_SchoolPathwayCourse', reseed, 0);
dbcc checkident('lbccpt.SchoolPathway_SchoolPathwayStudent', reseed, 0);
dbcc checkident('lbccpt.Consortium_College', reseed, 0);
dbcc checkident('lbccpt.College_CollegePathway', reseed, 0);
dbcc checkident('lbccpt.CollegePathway_CollegePathwayInstructor', reseed, 0);
dbcc checkident('lbccpt.CollegePathway_CollegePathwayCourse', reseed, 0);
dbcc checkident('lbccpt.CollegePathway_CollegePathwayStudent', reseed, 0);

DECLARE
	@message nvarchar(2048),
	@json_out nvarchar(max),
	@json_in nvarchar(max) =
N'{
	"Consortium":[
		{
			"Id":null,
			"UserId":5552,
			"Name":"Burt Macklin FBI Training: Level I",
			"Schools":[
				{
					"Id":null,
					"SchoolId":459
				},
				{
					"Id":null,
					"SchoolId":1
				},
				{
					"Id":null,
					"SchoolId":1384
				},
				{
					"Id":null,
					"SchoolId":462
				},
				{
					"Id":null,
					"SchoolId":1752
				},
				{
					"Id":null,
					"SchoolId":468
				},
				{
					"Id":null,
					"SchoolId":2
				},
				{
					"Id":null,
					"SchoolId":2523
				},
				{
					"Id":null,
					"SchoolId":3
				},
				{
					"Id":null,
					"SchoolId":471
				}
			],
			"Colleges":[
				{
					"Id":null,
					"CollegeId":30
				}
			]
		}
	]
}';

EXECUTE lbccpt.p_Consortium_cu
	@json_in = @json_in,
	@message = @message out;

SET @json_in = 
N'{
	"Consortium":[
		{
			"Id":1
		}
	]
}';

EXECUTE lbccpt.p_Consortium_r
	@json_in = @json_in,
	@json_out = @json_out out,
	@message = @message out;

PRINT @json_out;

/*
{
  "Consortium":[
    {
      "Id":1,
      "UserId":5552,
      "Name":"Burt Macklin FBI Training: Level I",
      "Schools":[
        {
          "Id":1,
          "SchoolId":1
        },
        {
          "Id":2,
          "SchoolId":1384
        },
        {
          "Id":3,
          "SchoolId":1752
        },
        {
          "Id":4,
          "SchoolId":2
        },
        {
          "Id":5,
          "SchoolId":2523
        },
        {
          "Id":6,
          "SchoolId":3
        },
        {
          "Id":7,
          "SchoolId":459
        },
        {
          "Id":8,
          "SchoolId":462
        },
        {
          "Id":9,
          "SchoolId":468
        },
        {
          "Id":10,
          "SchoolId":471
        }
      ],
      "Colleges":[
        {
          "Id":1,
          "CollegeId":30
        }
      ]
    }
  ]
}
*/

SET @json_in = 
N'{
	"SchoolPathway":[
		{
			"Id":null,
			"ConsortiumSchoolId":1,
			"AcademicYearId":4,
			"SchoolPathwayId":1,
			"SchoolPathwayCourses":[
				{
					"Id":null,
					"SchoolPathwayCourseId":0
				},
				{
					"Id":null,
					"SchoolPathwayCourseId":2
				},
				{
					"Id":null,
					"SchoolPathwayCourseId":4
				}
			],
			"SchoolPathwayStudents":[
				{
					"Id":null,
					"SchoolPathwayStudentId":0
				},
				{
					"Id":null,
					"SchoolPathwayStudentId":1
				}
			],
			"SchoolPathwayInstructors":[
				{
					"Id":null,
					"SchoolPathwayInstructorId":0
				},
				{
					"Id":null,
					"SchoolPathwayInstructorId":3
				}
			]
    }
  ]
}';

EXECUTE lbccpt.p_School_SchoolPathway_cu
	@json_in = @json_in,
	@message = @message out;

SET @json_in = 
N'{
	"SchoolPathway":[
		{
			"Id":1
		}
	]
}';

EXECUTE lbccpt.p_School_SchoolPathway_r
	@json_in = @json_in,
	@json_out = @json_out out,
	@message = @message out;

PRINT @json_out;
/*

{
  "SchoolPathway":[
    {
      "Id":1,
      "ConsortiumSchoolId":11,
      "AcademicYearId":4,
      "SchoolPathwayId":1,
      "SchoolPathwayCourses":[
        {
          "Id":null,
          "SchoolPathwayCourseId":0,
          "Name":"course_00"
        },
        {
          "Id":null,
          "SchoolPathwayCourseId":2,
          "Name":"course_02"
        },
        {
          "Id":null,
          "SchoolPathwayCourseId":4,
          "Name":"course_04"
        }
      ],
      "SchoolPathwayStudents":[
        {
          "Id":null,
          "SchoolPathwayStudentId":0,
          "Name":"student_00"
        },
        {
          "Id":null,
          "SchoolPathwayStudentId":1,
          "Name":"student_01"
        }
      ],
      "SchoolPathwayInstructors":[
        {
          "Id":null,
          "SchoolPathwayInstructorId":0,
          "Name":"instructor_00"
        },
        {
          "Id":null,
          "SchoolPathwayInstructorId":3,
          "Name":"instructor_03"
        }
      ]
    }
  ]
}

*/

SET @json_in = 
N'{
	"SchoolPathway":[
		{
			"Id":null,
			"ConsortiumSchoolId":1,
			"AcademicYearId":4,
			"SchoolPathwayId":1,
			"SchoolPathwayMetrics":[
				{
					"Id":null,
					"SchoolPathwayMetricId":0,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":1,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":2,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":3,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":5,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":6,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":7,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":8,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":9,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":10,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":11,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":12,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":13,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":14,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":15,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":16,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":17,
					"MetricValue":86
				},
				{
					"Id":null,
					"SchoolPathwayMetricId":18,
					"MetricValue":86
				}
			]
    }
  ]
}';

EXECUTE lbccpt.p_School_SchoolPathwayMetric_cu
	@json_in = @json_in,
	@message = @message out;

SET @json_in = 
N'{
	"SchoolPathway":[
		{
			"Id":1
		}
	]
}';

EXECUTE lbccpt.p_School_SchoolPathwayMetric_r
	@json_in = @json_in,
	@json_out = @json_out out,
	@message = @message out;

PRINT @json_out;

/*

{
  "SchoolPathway":[
    {
      "Id":1,
      "ConsortiumSchoolId":1,
      "AcademicYearId":4,
      "SchoolPathwayId":1,
      "SchoolPathwayMetrics":[
        {
          "Id":1,
          "SchoolPathwayMetricId":0,
          "Name":"metric_00",
          "MetricValue":86
        },
        {
          "Id":2,
          "SchoolPathwayMetricId":1,
          "Name":"metric_01",
          "MetricValue":86
        },
        {
          "Id":12,
          "SchoolPathwayMetricId":2,
          "Name":"metric_02",
          "MetricValue":86
        },
        {
          "Id":13,
          "SchoolPathwayMetricId":3,
          "Name":"metric_03",
          "MetricValue":86
        },
        {
          "Id":14,
          "SchoolPathwayMetricId":5,
          "Name":"metric_05",
          "MetricValue":86
        },
        {
          "Id":15,
          "SchoolPathwayMetricId":6,
          "Name":"metric_06",
          "MetricValue":86
        },
        {
          "Id":16,
          "SchoolPathwayMetricId":7,
          "Name":"metric_07",
          "MetricValue":86
        },
        {
          "Id":17,
          "SchoolPathwayMetricId":8,
          "Name":"metric_08",
          "MetricValue":86
        },
        {
          "Id":18,
          "SchoolPathwayMetricId":9,
          "Name":"metric_09",
          "MetricValue":86
        },
        {
          "Id":3,
          "SchoolPathwayMetricId":10,
          "Name":"metric_10",
          "MetricValue":86
        },
        {
          "Id":4,
          "SchoolPathwayMetricId":11,
          "Name":"metric_11",
          "MetricValue":86
        },
        {
          "Id":5,
          "SchoolPathwayMetricId":12,
          "Name":"metric_12",
          "MetricValue":86
        },
        {
          "Id":6,
          "SchoolPathwayMetricId":13,
          "Name":"metric_13",
          "MetricValue":86
        },
        {
          "Id":7,
          "SchoolPathwayMetricId":14,
          "Name":"metric_14",
          "MetricValue":86
        },
        {
          "Id":8,
          "SchoolPathwayMetricId":15,
          "Name":"metric_15",
          "MetricValue":86
        },
        {
          "Id":9,
          "SchoolPathwayMetricId":16,
          "Name":"metric_16",
          "MetricValue":86
        },
        {
          "Id":10,
          "SchoolPathwayMetricId":17,
          "Name":"metric_17",
          "MetricValue":86
        },
        {
          "Id":11,
          "SchoolPathwayMetricId":18,
          "Name":"metric_18",
          "MetricValue":86
        }
      ]
    }
  ]
}

*/

SET @json_in = 
N'{
	"CollegePathway":[
		{
			"Id":null,
			"ConsortiumCollegeId":1,
			"AcademicYearId":4,
			"Name":"Burt Macklin FBI Training: Level II",
			"CollegePathwayTaxonomies":[
				{
					"Id":null,
					"CollegePathwayTaxonomyId":44
				}
			],
			"CollegePathwayCourses":[
				{
					"Id":null,
					"CollegePathwayCourseId":0
				}
			],
			"CollegePathwayStudents":[
				{
					"Id":null,
					"CollegePathwayStudentId":0
				},
				{
					"Id":null,
					"CollegePathwayStudentId":1
				}
			],
			"CollegePathwayInstructors":[
				{
					"Id":null,
					"CollegePathwayInstructorId":0
				}
			]
    }
  ]
}';

EXECUTE lbccpt.p_College_CollegePathway_cu
	@json_in = @json_in,
	@message = @message out;

SET @json_in = 
N'{
	"CollegePathway":[
		{
			"Id":1
		}
	]
}';

EXECUTE lbccpt.p_College_CollegePathway_r
	@json_in = @json_in,
	@json_out = @json_out out,
	@message = @message out;

PRINT @json_out;

/*

{
  "CollegePathway":[
    {
      "Id":1,
      "ConsortiumCollegeId":1,
      "Name":"Burt Macklin FBI Training: Level II",
      "AcademicYearId":4,
      "CollegePathwayTaxonomies":[
        {
          "Id":2,
          "CollegePathwayTaxonomyId":44,
          "Label":"Banking and Finance (050400)"
        }
      ],
      "CollegePathwayCourses":[
        {
          "Id":1,
          "CollegePathwayCourseId":0,
          "Name":"course_00"
        }
      ],
      "CollegePathwayStudents":[
        {
          "Id":1,
          "CollegePathwayStudentId":0,
          "Name":"student_00"
        },
        {
          "Id":2,
          "CollegePathwayStudentId":1,
          "Name":"student_01"
        }
      ],
      "CollegePathwayInstructors":[
        {
          "Id":1,
          "CollegePathwayInstructorId":0,
          "Name":"instructor_00"
        }
      ]
    }
  ]
}

*/



SET @json_in = 
N'{
	"CollegePathway":[
		{
			"Id":null,
			"ConsortiumCollegeId":1,
			"AcademicYearId":4,
			"Name":"Burt Macklin FBI Training: Level II",
			"CollegePathwayMetrics":[
				{
					"Id":null,
					"CollegePathwayMetricId":0,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":1,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":2,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":3,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":5,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":6,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":7,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":8,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":9,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":10,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":11,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":12,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":13,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":14,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":15,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":16,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":17,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":18,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":19,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":20,
					"MetricValue":86
				},
				{
					"Id":null,
					"CollegePathwayMetricId":21,
					"MetricValue":86
				}
			]
    }
  ]
}';


EXECUTE lbccpt.p_College_CollegePathwayMetric_cu
	@json_in = @json_in,
	@message = @message out;

SET @json_in = 
N'{
	"CollegePathway":[
		{
			"Id":1
		}
	]
}';

EXECUTE lbccpt.p_College_CollegePathwayMetric_r
	@json_in = @json_in,
	@json_out = @json_out out,
	@message = @message out;

PRINT @json_out;

/*

{
  "CollegePathway":[
    {
      "Id":1,
      "ConsortiumCollegeId":1,
      "Name":"Burt Macklin FBI Training: Level II",
      "AcademicYearId":4,
      "CollegePathwayMetrics":[
        {
          "Id":1,
          "CollegePathwayMetricId":0,
          "Name":"metric_00",
          "MetricValue":86
        },
        {
          "Id":2,
          "CollegePathwayMetricId":1,
          "Name":"metric_01",
          "MetricValue":86
        },
        {
          "Id":3,
          "CollegePathwayMetricId":10,
          "Name":"metric_10",
          "MetricValue":86
        },
        {
          "Id":4,
          "CollegePathwayMetricId":11,
          "Name":"metric_11",
          "MetricValue":86
        },
        {
          "Id":5,
          "CollegePathwayMetricId":12,
          "Name":"metric_12",
          "MetricValue":86
        },
        {
          "Id":6,
          "CollegePathwayMetricId":13,
          "Name":"metric_13",
          "MetricValue":86
        },
        {
          "Id":7,
          "CollegePathwayMetricId":14,
          "Name":"metric_14",
          "MetricValue":86
        },
        {
          "Id":8,
          "CollegePathwayMetricId":15,
          "Name":"metric_15",
          "MetricValue":86
        },
        {
          "Id":9,
          "CollegePathwayMetricId":16,
          "Name":"metric_16",
          "MetricValue":86
        },
        {
          "Id":10,
          "CollegePathwayMetricId":17,
          "Name":"metric_17",
          "MetricValue":86
        },
        {
          "Id":11,
          "CollegePathwayMetricId":18,
          "Name":"metric_18",
          "MetricValue":86
        },
        {
          "Id":12,
          "CollegePathwayMetricId":19,
          "Name":"metric_19",
          "MetricValue":86
        },
        {
          "Id":13,
          "CollegePathwayMetricId":2,
          "Name":"metric_02",
          "MetricValue":86
        },
        {
          "Id":14,
          "CollegePathwayMetricId":20,
          "Name":"metric_20",
          "MetricValue":86
        },
        {
          "Id":15,
          "CollegePathwayMetricId":21,
          "Name":"metric_21",
          "MetricValue":86
        },
        {
          "Id":16,
          "CollegePathwayMetricId":3,
          "Name":"metric_03",
          "MetricValue":86
        },
        {
          "Id":17,
          "CollegePathwayMetricId":5,
          "Name":"metric_05",
          "MetricValue":86
        },
        {
          "Id":18,
          "CollegePathwayMetricId":6,
          "Name":"metric_06",
          "MetricValue":86
        },
        {
          "Id":19,
          "CollegePathwayMetricId":7,
          "Name":"metric_07",
          "MetricValue":86
        },
        {
          "Id":20,
          "CollegePathwayMetricId":8,
          "Name":"metric_08",
          "MetricValue":86
        },
        {
          "Id":21,
          "CollegePathwayMetricId":9,
          "Name":"metric_09",
          "MetricValue":86
        }
      ]
    }
  ]
}

*/