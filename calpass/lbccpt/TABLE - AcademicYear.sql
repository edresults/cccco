USE calpass;

GO

IF (object_id('lbccpt.AcademicYear') is not null)
	BEGIN
		DROP TABLE lbccpt.AcademicYear;
	END;

GO

CREATE TABLE
	lbccpt.AcademicYear
	(
		Id int identity(1,1) not null,
		CodeCalpass char(4) not null,
		CodeCalpads char(9) not null,
		CodeComis char(3) not null,
		IsActive bit not null,
		Ordinal int not null,
		CONSTRAINT
			pk_AcademicYear__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		CONSTRAINT
			uq_AcademicYear__CodeCalpass
		UNIQUE
			(
				CodeCalpass
			),
		CONSTRAINT
			uq_AcademicYear__CodeCalpads
		UNIQUE
			(
				CodeCalpads
			),
		CONSTRAINT
			uq_AcademicYear__CodeComis
		UNIQUE
			(
				CodeComis
			),
		INDEX
			ix_AcademicYear__IsActive
		NONCLUSTERED
			(
				IsActive
			)
	);

GO

INSERT INTO
	lbccpt.AcademicYear
	(
		CodeCalpass,
		CodeCalpads,
		CodeComis,
		IsActive,
		Ordinal
	)
SELECT
	CodeCalpass,
	CodeCalpads,
	CodeComis,
	IsActive,
	Ordinal = row_number() over(order by CodeCalpass)
FROM
	(
		VALUES
		('1112','2011-2012','120',1),
		('1213','2012-2013','130',1),
		('1314','2013-2014','140',1),
		('1415','2014-2015','150',1),
		('1516','2015-2016','160',1),
		('1617','2016-2017','170',1)
	) t (CodeCalpass, CodeCalpads, CodeComis, IsActive)
ORDER BY
	CodeCalpass;
	
-- DECLARE
	-- @user_id int = 5552;

-- SELECT
	-- [val] = p.program_k12_name + ' (' + p.program_k12_code + ')',
	-- [key] = p.program_k12_id
-- FROM
	-- calpass.lbccpt.program_k12 p
-- WHERE
	-- not exists (
		-- SELECT
			-- 0
		-- FROM
			-- calpass.lbccpt.consortium c
			-- inner join
			-- calpass.lbccpt.consortium_pathway_k12 cp
				-- on c.consortium_id = cp.consortium_id
		-- WHERE
			-- c.user_id = @user_id
			-- and cp.consortium_pathway_k12 = p.pathway_
	-- );