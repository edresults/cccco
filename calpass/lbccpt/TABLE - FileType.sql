USE calpass;

GO

IF (object_id('lbccpt.FileType') is not null)
	BEGIN
	 DROP TABLE lbccpt.FileType;
	END;

GO

CREATE TABLE
	lbccpt.FileType
	(
		FileTypeId int identity(1,1) not null,
		Code varchar(255),
		Description varchar(8000),
		CONSTRAINT
			pk_FileType
		PRIMARY KEY CLUSTERED
			(
				FileTypeId
			),
		CONSTRAINT
			uq_FileType__Code
		UNIQUE
			(
				Code
			)
	);

GO

INSERT INTO
	lbccpt.FileType
	(
		Code,
		Description
	)
VALUES
	('Cohort','Cohort of students in the consortium, uploaded by users, to be used when matching to COMIS data'),
	('Activity','The activities identified for the K12 Career Technical Education pathway with respect to Student, Course, and Instructor.'),
	('Metric','Metrics for the K12 portion of the consortium');