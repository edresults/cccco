USE calpass;

GO

IF (object_id('lbccpt.SchoolPathway') is not null)
	BEGIN
		DROP TABLE lbccpt.SchoolPathway;
	END;

GO

CREATE TABLE
	lbccpt.SchoolPathway
	(
		Id int identity(1,1) not null,
		Code char(3) not null,
		Name nvarchar(255) not null,
		Label nvarchar(255) not null,
		IsActive bit not null,
		Ordinal int not null,
		CONSTRAINT
			pk_SchoolPathway__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		INDEX
			ix_SchoolPathway__IsActive
		NONCLUSTERED
			(
				IsActive
			)
	);

GO

INSERT INTO
	lbccpt.SchoolPathway
	(
		Code,
		Name,
		Label,
		IsActive,
		Ordinal
	)
SELECT
	Code,
	Name,
	Label = Name + ' (' + Code + ')',
	IsActive = 1,
	Ordinal = row_number() over(order by Name)
FROM
	(
		VALUES
		('100','Agricultural Business'),
		('101','Agricultural Mechanics'),
		('102','Agriscience'),
		('103','Animal Science'),
		('104','Forestry and Natural Resources'),
		('105','Ornamental Horticulture'),
		('106','Plant and Soil Science'),
		('111','Design, Visual, and Media Arts'),
		('112','Performing Arts'),
		('113','Production and Managerial Arts'),
		('114','Game Design and Integration'),
		('120','Cabinetmaking, Millwork, and Woodworking'),
		('121','Engineering and Heavy Construction'),
		('122','Mechanical Systems Installation and Repair'),
		('123','Residential and Commercial Construction'),
		('130','Child Development'),
		('131','Consumer Services'),
		('132','Education'),
		('133','Family and Human Services'),
		('140','Electromechanical Installation and Maintenance'),
		('141','Environmental Resources'),
		('142','Telecommunications'),
		('143','Energy and Power Technology'),
		('150','Architectural Design'),
		('151','Computer Hardware, Electrical, and Networking Engineering'),
		('152','Engineering Design'),
		('153','Engineering Technology'),
		('154','Environmental Engineering'),
		('160','Fashion Design and Merchandising'),
		('161','Interior Design'),
		('162','Personal Services'),
		('170','Information Support and Services'),
		('171','Media Support and Services'),
		('172','Networking'),
		('174','Software and Systems Development'),
		('175','Games and Simulations'),
		('180','Financial Services'),
		('181','International Business'),
		('182','Business Management'),
		('190','Biotechnology Research and Development'),
		('192','Health Informatics'),
		('193','Support Services'),
		('194','Therapeutic Services'),
		('195','Mental and Behavioral Health'),
		('196','Biotechnology'),
		('197','Healthcare Operational Support'),
		('197','Mental and Behavioral Health'),
		('198','Patient Care'),
		('199','Public and Community Health'),
		('200','Food Science, Dietetics, and Nutrition'),
		('201','Food Service and Hospitality'),
		('202','Hospitality, Tourism, and Recreation'),
		('210','Graphic Production Technologies'),
		('212','Machine and Forming Technologies'),
		('213','Welding and Materials Joining'),
		('214','Introductory/Core'),
		('215','Emerging Technologies in Manufacturing and Product Development'),
		('216','Product Innovation and Design'),
		('220','Structural Repair and Refinishing'),
		('221','Systems Diagnostics, Services, and Repair'),
		('222','Aviation and Aerospace Transportation Services'),
		('223','Operations'),
		('230','Human Services'),
		('231','Legal Practices'),
		('232','Public Safety'),
		('233','Emergency Response'),
		('241','Entrepreneurship'),
		('242','International Trade'),
		('243','Marketing'),
		('243','Professional Sales'),
		('244','Marketing'),
		('250','Healthcare Administrative Services')
	) t (Code, Name)
ORDER BY
	Name;