SET IDENTITY_INSERT calpass.lbccpt.consortium ON;

INSERT INTO
	lbccpt.consortium
	(
		id,
		name,
		organization_id
	)
SELECT
	consortiaId,
	consortiaName,
	organizationId
FROM
	[pro-dat-sql-01].pro_launchboard_ccpt.dbo.Lookup_External_LaunchboardCCPTConsortium;

SET IDENTITY_INSERT calpass.lbccpt.consortium OFF;