USE calpass;

GO

IF (object_id('lbccpt.SchoolPathwayCourse') is not null)
	BEGIN
		DROP TABLE lbccpt.SchoolPathwayCourse;
	END;

GO

CREATE TABLE
	lbccpt.SchoolPathwayCourse
	(
		Id int identity(0,1) not null,
		Name nvarchar(255) not null,
		Label nvarchar(255) not null,
		IsActive bit not null,
		Ordinal int not null,
		CONSTRAINT
			pk_SchoolPathwayCourse__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		INDEX
			ix_SchoolPathwayCourse__IsActive
		NONCLUSTERED
			(
				IsActive
			)
	);

GO

INSERT INTO
	lbccpt.SchoolPathwayCourse
	(
		Name,
		Label,
		Ordinal,
		IsActive
	)
SELECT
	Name,
	Label,
	Ordinal = row_number() over(order by Name),
	IsActive
FROM
	(
		VALUES
		('course_00','A sequence of 3 or more CTE classes in high school',1),
		('course_01','Cross-disciplinary projects or assignments linking academic and CTE classes',1),
		('course_02','Cohort scheduling that includes both CTE and academic classes',1),
		('course_03','At least one CTE course that qualifies for “A-G” credit',1),
		('course_04','Scheduling that enables in-depth extended projects and work-based learning',1),
		('course_05','Dual enrollment opportunities',1)
	) t (Name,Label,IsActive)
ORDER BY
	Name;