USE calpass;

GO

IF (object_id('lbccpt.SchoolPathway_SchoolPathwayMetric') is not null)
	BEGIN
		DROP TABLE lbccpt.SchoolPathway_SchoolPathwayMetric;
	END;

GO

CREATE TABLE
	lbccpt.SchoolPathway_SchoolPathwayMetric
	(
		Id int identity(1,1) not null,
		SchoolPathwayId int not null,
		SchoolPathwayMetricId int not null,
		MetricValue int not null,
		CONSTRAINT
			pk_SchoolPathway_SchoolPathwayMetric__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		CONSTRAINT
			uq_SchoolPathway_SchoolPathwayMetric__SchoolPathwayId__SchoolPathwayMetricId
		UNIQUE
			(
				SchoolPathwayId,
				SchoolPathwayMetricId
			),
		CONSTRAINT
			fk_SchoolPathway_SchoolPathwayMetric__SchoolPathwayId
		FOREIGN KEY
			(
				SchoolPathwayId
			)
		REFERENCES
			lbccpt.School_SchoolPathway
			(
				Id
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_SchoolPathway_SchoolPathwayMetric_SchoolPathwayMetricId
		FOREIGN KEY
			(
				SchoolPathwayMetricId
			)
		REFERENCES
			lbccpt.SchoolPathwayMetric
			(
				Id
			)
		ON DELETE CASCADE
	);