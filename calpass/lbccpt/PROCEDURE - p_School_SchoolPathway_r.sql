USE calpass;

IF (object_id('lbccpt.p_School_SchoolPathway_r') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_School_SchoolPathway_r;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_School_SchoolPathway_r
	(
		@json_in nvarchar(max),
		@json_out nvarchar(max) = N'' OUT,
		@message nvarchar(2048) = N'' OUT
	)
AS

	SET NOCOUNT ON;

DECLARE
	@IsActive bit = 1;

BEGIN
	BEGIN TRY
		SET @json_out = (
			SELECT
				Id,
				ConsortiumSchoolId,
				AcademicYearId,
				SchoolPathwayId,
				(
					SELECT
						spspc.Id,
						spspc.SchoolPathwayCourseId,
						spc.Name
					FROM
						calpass.lbccpt.SchoolPathway_SchoolPathwayCourse spspc
						inner join
						calpass.lbccpt.SchoolPathwayCourse spc
							on spspc.SchoolPathwayCourseId = spc.Id
					WHERE
						School_SchoolPathway.Id = spspc.SchoolPathwayId
						and spc.IsActive = @IsActive
					ORDER BY
						spc.Ordinal
					FOR JSON
						path
				) as SchoolPathwayCourses,
				(
					SELECT
						spsps.Id,
						spsps.SchoolPathwayStudentId,
						sps.Name
					FROM
						calpass.lbccpt.SchoolPathway_SchoolPathwayStudent spsps
						inner join
						calpass.lbccpt.SchoolPathwayStudent sps
							on spsps.SchoolPathwayStudentId = sps.Id
					WHERE
						School_SchoolPathway.Id = spsps.SchoolPathwayId
						and sps.IsActive = @IsActive
					ORDER BY
						sps.Ordinal
					FOR JSON
						path
				) as SchoolPathwayStudents,
				(
					SELECT
						spspi.Id,
						spspi.SchoolPathwayInstructorId,
						spi.Name
					FROM
						calpass.lbccpt.SchoolPathway_SchoolPathwayInstructor spspi
						inner join
						calpass.lbccpt.SchoolPathwayInstructor spi
							on spspi.SchoolPathwayInstructorId = spi.Id
					WHERE
						School_SchoolPathway.Id = spspi.SchoolPathwayId
						and spi.IsActive = @IsActive
					ORDER BY
						spi.Ordinal
					FOR JSON
						path
				) as SchoolPathwayInstructors
			FROM
				calpass.lbccpt.School_SchoolPathway
			WHERE
				School_SchoolPathway.Id = (
					SELECT
						j.Id
					FROM
						openjson(@json_in, '$.SchoolPathway') with (
							Id int '$.Id'
						) j
				)
			FOR JSON
				path,
				root('SchoolPathway')
		);
	END TRY
	BEGIN CATCH
		-- SET @message = N'Error occurred when reading consortium organizations';
		SET @message =
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		THROW 70099, @message, 1;
	END CATCH;
END;