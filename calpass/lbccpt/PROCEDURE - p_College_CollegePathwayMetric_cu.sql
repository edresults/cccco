USE calpass;

IF (object_id('lbccpt.p_College_CollegePathwayMetric_cu') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_College_CollegePathwayMetric_cu;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_College_CollegePathwayMetric_cu
	(
		@json_in nvarchar(max),
		@message nvarchar(2048) = N'' OUT
	)
AS

	SET NOCOUNT ON;

DECLARE
	@CollegePathwayId int;

BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			-- set CollegePathwayId
			SELECT
				@CollegePathwayId = json_value(j.value, '$.Id')
			FROM
				openjson(@json_in, '$.CollegePathway') j;
			-- Metrics
			MERGE
				calpass.lbccpt.CollegePathway_CollegePathwayMetric
			WITH
				(
					holdlock
				) t
			USING
				(
					SELECT DISTINCT
						Id = json_value(c.value, '$.Id'),
						CollegePathwayId = json_value(j.value, '$.Id'),
						CollegePathwayMetricId = json_value(c.value, '$.CollegePathwayMetricId'),
						MetricValue = json_value(c.value, '$.MetricValue')
					FROM
						openjson(@json_in, '$.CollegePathway') j
						cross apply
						openjson(j.value, '$.CollegePathwayMetrics') c
				) s
			ON
				t.Id = s.Id
			WHEN MATCHED THEN
				UPDATE SET
					t.MetricValue = s.MetricValue
			WHEN NOT MATCHED BY TARGET THEN
				INSERT
					(
						CollegePathwayId,
						CollegePathwayMetricId,
						MetricValue
					)
				VALUES
					(
						s.CollegePathwayId,
						s.CollegePathwayMetricId,
						s.MetricValue
					)
			WHEN NOT MATCHED BY SOURCE
				and t.CollegePathwayId = @CollegePathwayId THEN
				DELETE;
		END TRY
		BEGIN CATCH
			IF (@@TRANCOUNT > 0)
				BEGIN
					ROLLBACK TRANSACTION;
				END;
			-- SET @message = N'Error occurred when attempting to ' + @verb + N' Consortium Colleges';
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			THROW 70099, @message, 1;
		END CATCH;

	COMMIT TRANSACTION;
END;