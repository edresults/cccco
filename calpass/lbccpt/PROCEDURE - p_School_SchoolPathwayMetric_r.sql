USE calpass;

IF (object_id('lbccpt.p_School_SchoolPathwayMetric_r') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_School_SchoolPathwayMetric_r;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_School_SchoolPathwayMetric_r
	(
		@json_in nvarchar(max),
		@json_out nvarchar(max) = N'' OUT,
		@message nvarchar(2048) = N'' OUT
	)
AS

	SET NOCOUNT ON;

DECLARE
	@IsActive bit = 1;

BEGIN
	BEGIN TRY
		SET @json_out = (
			SELECT
				Id,
				ConsortiumSchoolId,
				AcademicYearId,
				SchoolPathwayId,
				(
					SELECT
						spspm.Id,
						spspm.SchoolPathwayMetricId,
						spm.Name,
						spspm.MetricValue
					FROM
						calpass.lbccpt.SchoolPathway_SchoolPathwayMetric spspm
						inner join
						calpass.lbccpt.SchoolPathwayMetric spm
							on spspm.SchoolPathwayMetricId = spm.Id
					WHERE
						School_SchoolPathway.Id = spspm.SchoolPathwayId
						and spm.IsActive = @IsActive
					ORDER BY
						spm.Ordinal
					FOR JSON
						path
				) as SchoolPathwayMetrics
			FROM
				calpass.lbccpt.School_SchoolPathway
			WHERE
				School_SchoolPathway.Id = (
					SELECT
						j.Id
					FROM
						openjson(@json_in, '$.SchoolPathway') with (
							Id int '$.Id'
						) j
				)
			FOR JSON
				path,
				root('SchoolPathway')
		);
	END TRY
	BEGIN CATCH
		-- SET @message = N'Error occurred when reading consortium organizations';
		SET @message =
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		THROW 70099, @message, 1;
	END CATCH;
END;