USE calpass;

IF (object_id('lbccpt.p_consortium_r') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_consortium_r;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_consortium_r
	(
		@json_in nvarchar(max),
		@json_out nvarchar(max) = N'' OUT,
		@message nvarchar(2048) = N'' OUT
	)
AS

	SET NOCOUNT ON;

BEGIN
	BEGIN TRY
		SET @json_out = (
			SELECT
				Consortium.Id,
				Consortium.UserId,
				Consortium.Name,
				(
					SELECT
						s.Id,
						s.SchoolId
					FROM
						calpass.lbccpt.Consortium_School s
					WHERE
						Consortium.Id = s.ConsortiumId
					ORDER BY
						s.Id
					FOR JSON
						path
				) as Schools,
				(
					SELECT
						c.Id,
						c.CollegeId
					FROM
						calpass.lbccpt.Consortium_College c
					WHERE
						Consortium.Id = c.ConsortiumId
					ORDER BY
						c.Id
					FOR JSON
						path
				) as Colleges
			FROM
				calpass.lbccpt.Consortium
			WHERE
				Consortium.Id = (
					SELECT
						j.Id
					FROM
						openjson(@json_in, '$.Consortium') with (
							Id int '$.Id'
						) j
				)
			FOR JSON
				path,
				root('Consortium')
		);
	END TRY
	BEGIN CATCH
		-- SET @message = N'Error occurred when reading consortium organizations';
		SET @message =
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		THROW 70099, @message, 1;
	END CATCH;
END;