USE calpass;

GO

IF (type_id('lbccpt.Consortium_School') is not null)
	BEGIN
		DROP TYPE lbccpt.Consortium_School;
	END;

GO

CREATE TYPE
	lbccpt.Consortium_School
AS TABLE
	(
		Id int,
		ConsortiumId int not null,
		SchoolId int not null,
		Action nvarchar(10),
		INDEX
			ixc_Consortium_School__Id
		 CLUSTERED
			(
				Id
			)
	);