USE calpass;

GO

IF (object_id('lbccpt.Consortium') is not null)
	BEGIN
		DROP TABLE lbccpt.Consortium;
	END;

GO

CREATE TABLE
	lbccpt.Consortium
	(
		Id int identity(1,1) not null,
		UserId int not null,
		Name nvarchar(255) not null,
		CONSTRAINT
			pk_Consortium__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		INDEX
			ix_Consortium__UserId
		NONCLUSTERED
			(
				UserId
			)
	);

GO

IF (object_id('lbccpt.tr_Consortium__ins_upd') is not null)
	BEGIN
		DROP TRIGGER lbccpt.tr_Consortium__ins_upd;
	END;

GO

CREATE TRIGGER
	lbccpt.tr_Consortium__ins_upd
ON
	lbccpt.Consortium
AFTER
	INSERT, UPDATE
AS

DECLARE
	@success bit,
	@message nvarchar(2048);

BEGIN

	IF (exists (SELECT 1 FROM inserted))
		BEGIN
			-- validate user
			SELECT
				@success = count(*)
			FROM
				[pro-dat-sql-01].cms_production_calpass.dbo.cms_user u
				inner join
				inserted i
					on u.UserId = i.userId;
			-- process user
			IF (@success = 0)
				BEGIN
					ROLLBACK;
					SET @message = N'User does not exist';
					THROW 70099, @message, 1;
				END;
		END;
END;