USE calpass;

IF (object_id('lbccpt.p_College_CollegePathwayMetric_r') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_College_CollegePathwayMetric_r;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_College_CollegePathwayMetric_r
	(
		@json_in nvarchar(max),
		@json_out nvarchar(max) = N'' OUT,
		@message nvarchar(2048) = N'' OUT
	)
AS

	SET NOCOUNT ON;

DECLARE
	@IsActive bit = 1;

BEGIN
	BEGIN TRY
		SET @json_out = (
			SELECT
				Id,
				ConsortiumCollegeId,
				Name,
				AcademicYearId,
				(
					SELECT
						cpcpm.Id,
						cpcpm.CollegePathwayMetricId,
						cpm.Name,
						cpcpm.MetricValue
					FROM
						calpass.lbccpt.CollegePathway_CollegePathwayMetric cpcpm
						inner join
						calpass.lbccpt.CollegePathwayMetric cpm
							on cpcpm.CollegePathwayMetricId = cpm.Id
					WHERE
						College_CollegePathway.Id = cpcpm.CollegePathwayId
						and cpm.IsActive = 1
					FOR JSON
						path
				) as CollegePathwayMetrics
			FROM
				calpass.lbccpt.College_CollegePathway
			WHERE
				College_CollegePathway.Id = (
					SELECT
						j.Id
					FROM
						openjson(@json_in, '$.CollegePathway') with (
							Id int '$.Id'
						) j
				)
			FOR JSON
				path,
				root('CollegePathway')
		);
	END TRY
	BEGIN CATCH
		-- SET @message = N'Error occurred when reading College organizations';
		SET @message =
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		THROW 70099, @message, 1;
	END CATCH;
END;