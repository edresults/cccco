USE calpass;

GO

IF (object_id('lbccpt.CollegePathway_CollegePathwayTaxonomy') is not null)
	BEGIN
		DROP TABLE lbccpt.CollegePathway_CollegePathwayTaxonomy;
	END;

GO

CREATE TABLE
	lbccpt.CollegePathway_CollegePathwayTaxonomy
	(
		Id int identity(1,1) not null,
		CollegePathwayId int not null,
		CollegePathwayTaxonomyId int not null,
		CONSTRAINT
			pk_CollegePathway_CollegePathwayTaxonomy__Id
		PRIMARY KEY
			(
				Id
			),
		CONSTRAINT
			uq_CollegePathway_CollegePathwayTaxonomy__CollegePathwayId__CollegePathwayTaxonomyId
		UNIQUE
			(
				CollegePathwayId,
				CollegePathwayTaxonomyId
			),
		CONSTRAINT
			fk_CollegePathway_CollegePathwayTaxonomy__CollegePathwayId
		FOREIGN KEY
			(
				CollegePathwayId
			)
		REFERENCES
			lbccpt.College_CollegePathway
			(
				Id
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_CollegePathway_CollegePathwayTaxonomy_CollegePathwayTaxonomyId
		FOREIGN KEY
			(
				CollegePathwayTaxonomyId
			)
		REFERENCES
			lbccpt.CollegePathwayTaxonomy
			(
				Id
			)
		ON DELETE CASCADE
	);