USE calpass;

GO

IF (object_id('lbccpt.FileUpload') is not null)
	BEGIN
	 DROP TABLE lbccpt.FileUpload;
	END;

GO

CREATE TABLE
	lbccpt.FileUpload
	(
		FileUploadId int identity(1,1) not null,
		UserId int not null,
		ConsortiumId int not null, -- consortium
		FileTypeId int not null,
		FilePath nvarchar(255) not null,
		UploadDatetime datetime not null,
		ProcessStatus varchar(255) not null,
		ProcessMessage nvarchar(4000) null,
		ProcessDatetime datetime null,
		CONSTRAINT
			pk_FileUpload
		PRIMARY KEY CLUSTERED
			(
				FileUploadId
			),
		CONSTRAINT
			pk_FileUpload__FileTypeId
		FOREIGN KEY
			(
				FileTypeId
			)
		REFERENCES
			lbccpt.FileType
			(
				FileTypeId
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_FileUpload__ConsortiumId
		FOREIGN KEY
			(
				ConsortiumId
			)
		REFERENCES
			lbccpt.Consortium
			(
				Id
			)
		ON DELETE CASCADE
	);