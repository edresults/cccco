USE calpass;

GO

IF (object_id('lbccpt.SchoolPathway_SchoolPathwayMetricUpload') is not null)
	BEGIN
		DROP TABLE lbccpt.SchoolPathway_SchoolPathwayMetricUpload;
	END;

GO

CREATE TABLE
	lbccpt.SchoolPathway_SchoolPathwayMetricUpload
	(
		ConsortiumId int,
		CdsCode nchar(14),
		SchoolId int,
		ConsortiumSchoolId int,
		AcademicYear nchar(9),
		AcademicYearId int,
		SchoolPathwayCode nchar(3),
		SchoolPathwayId int,
		School_SchoolPathwayId int,
		metric_00 int,
		metric_01 int,
		metric_02 int,
		metric_03 int,
		metric_04 int,
		metric_05 int,
		metric_06 int,
		metric_07 int,
		metric_08 int,
		metric_09 int,
		metric_10 int,
		metric_11 int,
		metric_12 int,
		metric_13 int,
		metric_14 int,
		metric_15 int,
		metric_16 int,
		metric_17 int,
		metric_18 int,
		metric_19 int,
		metric_20 int,
		CONSTRAINT
			pk_SchoolPathway_SchoolPathwayMetricUpload
		PRIMARY KEY CLUSTERED
			(
				ConsortiumId,
				CdsCode,
				AcademicYear,
				SchoolPathwayCode
			),
		CONSTRAINT
			uq_SchoolPathway_SchoolPathwayMetricUpload__School_SchoolPathwayId
		UNIQUE
			(
				School_SchoolPathwayId
			)
	);