USE calpass;

GO

IF (object_id('lbccpt.CollegePathwayStudent') is not null)
	BEGIN
		DROP TABLE lbccpt.CollegePathwayStudent;
	END;

GO

CREATE TABLE
	lbccpt.CollegePathwayStudent
	(
		Id int identity(0,1) not null,
		Name nvarchar(255) not null,
		Label nvarchar(255) not null,
		IsActive bit not null,
		Ordinal int not null,
		CONSTRAINT
			pk_CollegePathwayStudent__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		INDEX
			ix_CollegePathwayStudent__IsActive
		NONCLUSTERED
			(
				IsActive
			)
	);

GO

INSERT INTO
	lbccpt.CollegePathwayStudent
	(
		Name,
		Label,
		Ordinal,
		IsActive
	)
SELECT
	Name,
	Label,
	Ordinal = row_number() over(order by Name),
	IsActive
FROM
	(
		VALUES
		('student_00','Career guidance/counseling (e.g., career navigation, job-seeking, resume-writing, career fairs)',1),
		('student_01','Successful course completion (e.g., tutoring, personal counseling)',1)
	) t (Name,Label,IsActive)
ORDER BY
	Name;