USE calpass;

IF (object_id('lbccpt.p_consortium_d') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_consortium_d;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_consortium_d
	(
		@json_in nvarchar(max),
		@message nvarchar(2048) = N'' OUT
	)
AS

	SET NOCOUNT ON;

BEGIN
	BEGIN TRY
		DELETE
			t
		FROM
			calpass.lbccpt.Consortium t
		WHERE
			t.Id = (
				SELECT
					j.Id
				FROM
					openjson(@json_in, '$.Consortium') with (
						Id int '$.Id'
					) j
			);
	END TRY
	BEGIN CATCH
		-- SET @message = N'Error occurred when deleting consortium';
		SET @message =
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		THROW 70099, @message, 1;
	END CATCH;
END;