USE calpass;

IF (object_id('lbccpt.p_College_CollegePathway_r') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_College_CollegePathway_r;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_College_CollegePathway_r
	(
		@json_in nvarchar(max),
		@json_out nvarchar(max) = N'' OUT,
		@message nvarchar(2048) = N'' OUT
	)
AS

	SET NOCOUNT ON;

DECLARE
	@IsActive bit = 1;

BEGIN
	BEGIN TRY
		SET @json_out = (
			SELECT
				Id,
				ConsortiumCollegeId,
				Name,
				AcademicYearId,
				(
					SELECT
						cpcpt.Id,
						cpcpt.CollegePathwayTaxonomyId,
						cpt.Label
					FROM
						calpass.lbccpt.CollegePathway_CollegePathwayTaxonomy cpcpt
						inner join
						calpass.lbccpt.CollegePathwayTaxonomy cpt
							on cpcpt.CollegePathwayTaxonomyId = cpt.Id
					WHERE
						College_CollegePathway.Id = cpcpt.CollegePathwayId
						and cpt.IsActive = 1
					ORDER BY
						cpt.Ordinal
					FOR JSON
						path
				) as CollegePathwayTaxonomies,
				(
					SELECT
						cpcpc.Id,
						cpcpc.CollegePathwayCourseId,
						cpc.Name
					FROM
						calpass.lbccpt.CollegePathway_CollegePathwayCourse cpcpc
						inner join
						calpass.lbccpt.CollegePathwayCourse cpc
							on cpcpc.CollegePathwayCourseId = cpc.Id
					WHERE
						College_CollegePathway.Id = cpcpc.CollegePathwayId
						and cpc.IsActive = 1
					ORDER BY
						cpc.Ordinal
					FOR JSON
						path
				) as CollegePathwayCourses,
				(
					SELECT
						cpcps.Id,
						cpcps.CollegePathwayStudentId,
						cps.Name
					FROM
						calpass.lbccpt.CollegePathway_CollegePathwayStudent cpcps
						inner join
						calpass.lbccpt.CollegePathwayStudent cps
							on cpcps.CollegePathwayStudentId = cps.Id
					WHERE
						College_CollegePathway.Id = cpcps.CollegePathwayId
						and cps.IsActive = 1
					ORDER BY
						cps.Ordinal
					FOR JSON
						path
				) as CollegePathwayStudents,
				(
					SELECT
						cpcpi.Id,
						cpcpi.CollegePathwayInstructorId,
						cpi.Name
					FROM
						calpass.lbccpt.CollegePathway_CollegePathwayInstructor cpcpi
						inner join
						calpass.lbccpt.CollegePathwayInstructor cpi
							on cpcpi.CollegePathwayInstructorId = cpi.Id
					WHERE
						College_CollegePathway.Id = cpcpi.CollegePathwayId
						and cpi.IsActive = 1
					ORDER BY
						cpi.Ordinal
					FOR JSON
						path
				) as CollegePathwayInstructors
			FROM
				calpass.lbccpt.College_CollegePathway
			WHERE
				College_CollegePathway.Id = (
					SELECT
						j.Id
					FROM
						openjson(@json_in, '$.CollegePathway') with (
							Id int '$.Id'
						) j
				)
			FOR JSON
				path,
				root('CollegePathway')
		);
	END TRY
	BEGIN CATCH
		-- SET @message = N'Error occurred when reading College organizations';
		SET @message =
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		THROW 70099, @message, 1;
	END CATCH;
END;