USE calpass;

IF (object_id('lbccpt.p_Consortium_cu') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_Consortium_cu;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_Consortium_cu
	(
		@json_in nvarchar(max),
		@message nvarchar(2048) = N'' OUT
	)
AS

	SET NOCOUNT ON;

DECLARE
	@Consortium lbccpt.Consortium;

BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			-- merge data into Consortium
			INSERT INTO
				@Consortium
				(
					Id,
					UserId,
					Name,
					Action
				)
			SELECT
				o.Id,
				o.UserId,
				o.Name,
				o.Action
			FROM
				(
					MERGE
						calpass.lbccpt.Consortium with(
							holdlock
						) t
					USING
						(
							SELECT DISTINCT
								Id,
								Userid,
								Name
							FROM
								openjson(@json_in, '$.Consortium') with (
									Id int '$.Id',
									UserId int '$.UserId',
									Name nvarchar(255) '$.Name'
								)
						) s
					ON
						t.Id = s.Id
					WHEN NOT MATCHED BY TARGET THEN
						INSERT
							(
								UserId,
								Name
							)
						VALUES
							(
								s.UserId,
								s.Name
							)
					WHEN MATCHED THEN
						UPDATE SET
							t.Name = s.Name
					OUTPUT
						inserted.Id,
						inserted.UserId,
						inserted.Name,
						$action
				) o (
					Id,
					UserId,
					Name,
					Action
				);
		END TRY
		BEGIN CATCH
			IF (@@TRANCOUNT > 0)
				BEGIN
					ROLLBACK TRANSACTION;
				END;
			-- SET @message = N'Error occurred when attempting to modify Consortium';
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			THROW 70099, @message, 1;
		END CATCH;

		-- Schools
		BEGIN TRY
			MERGE
				calpass.lbccpt.Consortium_School with(
					holdlock
				) t
			USING
				(
					SELECT DISTINCT
						Id = json_value(s.value, '$.Id'),
						ConsortiumId = tt.Id,
						SchoolId = json_value(s.value, '$.SchoolId')
					FROM
						@Consortium tt
						cross apply
						openjson(@json_in, '$.Consortium') c
						cross apply
						openjson(c.value, '$.Schools') s
				) s
			ON
				t.Id = s.Id
			WHEN NOT MATCHED BY TARGET THEN
				INSERT
					(
						ConsortiumId,
						SchoolId
					)
				VALUES
					(
						s.ConsortiumId,
						s.SchoolId
					)
			WHEN NOT MATCHED BY SOURCE
				-- used for mult-record table type passed
				and (exists (SELECT 1 FROM @Consortium tt WHERE t.ConsortiumId = tt.Id)) THEN
				DELETE;
		END TRY
		BEGIN CATCH
			IF (@@TRANCOUNT > 0)
				BEGIN
					ROLLBACK TRANSACTION;
				END;
			-- SET @message = N'Error occurred when attempting to ' + @verb + N' Consortium schools';
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			THROW 70099, @message, 1;
		END CATCH;

		-- Colleges
		BEGIN TRY
			MERGE
				calpass.lbccpt.Consortium_College with(
					holdlock
				) t
			USING
				(
					SELECT DISTINCT
						Id = json_value(s.value, '$.Id'),
						ConsortiumId = tt.Id,
						CollegeId = json_value(s.value, '$.CollegeId')
					FROM
						@Consortium tt
						cross apply
						openjson(@json_in, '$.Consortium') c
						cross apply
						openjson(c.value, '$.Colleges') s
				) s
			ON
				t.Id = s.Id
			WHEN NOT MATCHED BY TARGET THEN
				INSERT
					(
						ConsortiumId,
						CollegeId
					)
				VALUES
					(
						s.ConsortiumId,
						s.CollegeId
					)
			WHEN NOT MATCHED BY SOURCE
				-- used for single-record table type passed
				-- and t.ConsortiumId = @ConsortiumId THEN
				-- used for mult-record table type passed
				and (exists (SELECT 1 FROM @Consortium tt WHERE t.ConsortiumId = tt.Id)) THEN
				DELETE;
		END TRY
		BEGIN CATCH
			IF (@@TRANCOUNT > 0)
				BEGIN
					ROLLBACK TRANSACTION;
				END;
			-- SET @message = N'Error occurred when attempting to ' + @verb + ' Consortium colleges';
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			THROW 70099, @message, 1;
		END CATCH;
	COMMIT TRANSACTION;
END;