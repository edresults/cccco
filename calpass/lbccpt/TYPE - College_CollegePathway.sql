USE calpass;

GO

IF (type_id('lbccpt.College_CollegePathway') is not null)
	BEGIN
		DROP TYPE lbccpt.College_CollegePathway;
	END;

GO

CREATE TYPE
	lbccpt.College_CollegePathway
AS TABLE
	(
		Id int,
		ConsortiumCollegeId int not null,
		Name nvarchar(255) not null,
		AcademicYearId int not null,
		Action nvarchar(10),
		INDEX
			ixc_College_CollegePathway__Id
		CLUSTERED
			(
				Id
			)
	);