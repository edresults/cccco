USE calpass;

GO

IF (object_id('lbccpt.CollegePathwayCourse') is not null)
	BEGIN
		DROP TABLE lbccpt.CollegePathwayCourse;
	END;

GO

CREATE TABLE
	lbccpt.CollegePathwayCourse
	(
		Id int identity(0,1) not null,
		Name nvarchar(255) not null,
		Label nvarchar(255) not null,
		IsActive bit not null,
		Ordinal int not null,
		CONSTRAINT
			pk_CollegePathwayCourse__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		INDEX
			ix_CollegePathwayCourse__IsActive
		NONCLUSTERED
			(
				IsActive
			)
	);

GO

INSERT INTO
	lbccpt.CollegePathwayCourse
	(
		Name,
		Label,
		Ordinal,
		IsActive
	)
SELECT
	Name,
	Label,
	Ordinal = row_number() over(order by Name),
	IsActive
FROM
	(
		VALUES
		('course_00','College courses aligned to the K-12 feeder pathway courses',1),
		('course_01','Stackable certificates',1)
	) t (Name,Label,IsActive)
ORDER BY
	Name;