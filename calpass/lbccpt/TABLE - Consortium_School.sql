USE calpass;

GO

IF (object_id('lbccpt.Consortium_School') is not null)
	BEGIN
		DROP TABLE lbccpt.Consortium_School;
	END;

GO

CREATE TABLE
	lbccpt.Consortium_School
	(
		Id int identity(1,1) not null,
		ConsortiumId int not null,
		SchoolId int not null,
		CONSTRAINT
			pk_Consortium_School__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		CONSTRAINT
			uk_Consortium_School__ConsortiumId__SchoolId
		UNIQUE
			(
				ConsortiumId,
				SchoolId
			),
		CONSTRAINT
			fk_Consortium_School__ConsortiumId
		FOREIGN KEY
			(
				ConsortiumId
			)
		REFERENCES
			lbccpt.Consortium
			(
				Id
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_Consortium_School__SchoolId
		FOREIGN KEY
			(
				SchoolId
			)
		REFERENCES
			lbccpt.School
			(
				Id
			)
		ON DELETE CASCADE
	);