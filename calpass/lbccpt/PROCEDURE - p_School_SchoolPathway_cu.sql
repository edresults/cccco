USE calpass;

IF (object_id('lbccpt.p_School_SchoolPathway_cu') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_School_SchoolPathway_cu;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_School_SchoolPathway_cu
	(
		@json_in nvarchar(max),
		@message nvarchar(2048) = N'' OUT
	)
AS

	SET NOCOUNT ON;

DECLARE
	@School_SchoolPathway lbccpt.School_SchoolPathway;

BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			INSERT INTO
				@School_SchoolPathway
				(
					Id,
					ConsortiumSchoolId,
					AcademicYearId,
					SchoolPathwayId
				)
			SELECT
				o.Id,
				o.ConsortiumSchoolId,
				o.AcademicYearId,
				o.SchoolPathwayId
			FROM
				(
					-- merge data into consortium pathway for k12 organizations
					MERGE
						calpass.lbccpt.School_SchoolPathway with(
							holdlock
						) t
					USING
						(
							SELECT
								Id = json_value(j.value, '$.Id'),
								ConsortiumSchoolId = json_value(j.value, '$.ConsortiumSchoolId'),
								AcademicYearId = json_value(j.value, '$.AcademicYearId'),
								SchoolPathwayId = json_value(j.value, '$.SchoolPathwayId')
							FROM
								openjson(@json_in, '$.SchoolPathway') j
						) s
					ON
						t.Id = s.Id
					WHEN NOT MATCHED BY TARGET THEN
						INSERT
							(
								ConsortiumSchoolId,
								AcademicYearId,
								SchoolPathwayId
							)
						VALUES
							(
								s.ConsortiumSchoolId,
								s.AcademicYearId,
								s.SchoolPathwayId
							)
					WHEN MATCHED THEN
						UPDATE SET
							t.ConsortiumSchoolId = s.ConsortiumSchoolId,
							t.AcademicYearId = s.AcademicYearId,
							t.SchoolPathwayId = s.SchoolPathwayId
					OUTPUT
						inserted.Id,
						inserted.ConsortiumSchoolId,
						inserted.AcademicYearId,
						inserted.SchoolPathwayId,
						$action
				) o (
					Id,
					ConsortiumSchoolId,
					AcademicYearId,
					SchoolPathwayId,
					Action
				);
		END TRY
		BEGIN CATCH
			IF (@@TRANCOUNT > 0)
				BEGIN
					ROLLBACK TRANSACTION;
				END;
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			THROW 70099, @message, 1;
		END CATCH;

		-- Courses
		BEGIN TRY
			MERGE
				calpass.lbccpt.SchoolPathway_SchoolPathwayCourse with(
					holdlock
				) t
			USING
				(
					SELECT DISTINCT
						Id = json_value(c.value, '$.Id'),
						SchoolPathwayId = tt.Id,
						SchoolPathwayCourseId = json_value(c.value, '$.SchoolPathwayCourseId')
					FROM
						@School_SchoolPathway tt
						cross apply
						openjson(@json_in, '$.SchoolPathway') j
						cross apply
						openjson(j.value, '$.SchoolPathwayCourses') c
				) s
			ON
				t.Id = s.Id
			WHEN NOT MATCHED BY TARGET THEN
				INSERT
					(
						SchoolPathwayId,
						SchoolPathwayCourseId
					)
				VALUES
					(
						s.SchoolPathwayId,
						s.SchoolPathwayCourseId
					)
			WHEN NOT MATCHED BY SOURCE
				and (exists (SELECT 1 FROM @School_SchoolPathway tt WHERE t.SchoolPathwayId = tt.Id)) THEN
				DELETE;
		END TRY
		BEGIN CATCH
			IF (@@TRANCOUNT > 0)
				BEGIN
					ROLLBACK TRANSACTION;
				END;
			-- SET @message = N'Error occurred when attempting to ' + @verb + N' Consortium schools';
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			THROW 70099, @message, 1;
		END CATCH;
		
		-- Students
		BEGIN TRY
			MERGE
				calpass.lbccpt.SchoolPathway_SchoolPathwayStudent with(
					holdlock
				) t
			USING
				(
					SELECT DISTINCT
						Id = json_value(c.value, '$.Id'),
						SchoolPathwayId = tt.Id,
						SchoolPathwayStudentId = json_value(c.value, '$.SchoolPathwayStudentId')
					FROM
						@School_SchoolPathway tt
						cross apply
						openjson(@json_in, '$.SchoolPathway') j
						cross apply
						openjson(j.value, '$.SchoolPathwayStudents') c
				) s
			ON
				t.Id = s.Id
			WHEN NOT MATCHED BY TARGET THEN
				INSERT
					(
						SchoolPathwayId,
						SchoolPathwayStudentId
					)
				VALUES
					(
						s.SchoolPathwayId,
						s.SchoolPathwayStudentId
					)
			WHEN NOT MATCHED BY SOURCE
				and (exists (SELECT 1 FROM @School_SchoolPathway tt WHERE t.SchoolPathwayId = tt.Id)) THEN
				DELETE;
		END TRY
		BEGIN CATCH
			IF (@@TRANCOUNT > 0)
				BEGIN
					ROLLBACK TRANSACTION;
				END;
			-- SET @message = N'Error occurred when attempting to ' + @verb + N' Consortium schools';
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			THROW 70099, @message, 1;
		END CATCH;
		
		-- Instructors
		BEGIN TRY
			MERGE
				calpass.lbccpt.SchoolPathway_SchoolPathwayInstructor with(
					holdlock
				) t
			USING
				(
					SELECT DISTINCT
						Id = json_value(c.value, '$.Id'),
						SchoolPathwayId = tt.Id,
						SchoolPathwayInstructorId = json_value(c.value, '$.SchoolPathwayInstructorId')
					FROM
						@School_SchoolPathway tt
						cross apply
						openjson(@json_in, '$.SchoolPathway') j
						cross apply
						openjson(j.value, '$.SchoolPathwayInstructors') c
				) s
			ON
				t.Id = s.Id
			WHEN NOT MATCHED BY TARGET THEN
				INSERT
					(
						SchoolPathwayId,
						SchoolPathwayInstructorId
					)
				VALUES
					(
						s.SchoolPathwayId,
						s.SchoolPathwayInstructorId
					)
			WHEN NOT MATCHED BY SOURCE
				and (exists (SELECT 1 FROM @School_SchoolPathway tt WHERE t.SchoolPathwayId = tt.Id)) THEN
				DELETE;
		END TRY
		BEGIN CATCH
			IF (@@TRANCOUNT > 0)
				BEGIN
					ROLLBACK TRANSACTION;
				END;
			-- SET @message = N'Error occurred when attempting to ' + @verb + N' Consortium schools';
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			THROW 70099, @message, 1;
		END CATCH;

	COMMIT TRANSACTION;
END;