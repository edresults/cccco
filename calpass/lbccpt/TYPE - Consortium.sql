USE calpass;

GO

IF (type_id('lbccpt.Consortium') is not null)
	BEGIN
		DROP TYPE lbccpt.Consortium;
	END;

GO

CREATE TYPE
	lbccpt.Consortium
AS TABLE
	(
		Id int not null,
		UserId int not null,
		Name nvarchar(255) not null,
		Action nvarchar(10),
		INDEX
			ixc_Consortium__Id
		CLUSTERED
			(
				Id
			)
	);