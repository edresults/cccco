USE calpass;

GO

IF (object_id('lbccpt.CollegePathwayInstructor') is not null)
	BEGIN
		DROP TABLE lbccpt.CollegePathwayInstructor;
	END;

GO

CREATE TABLE
	lbccpt.CollegePathwayInstructor
	(
		Id int identity(0,1) not null,
		Name nvarchar(255) not null,
		Label nvarchar(255) not null,
		IsActive bit not null,
		Ordinal int not null,
		CONSTRAINT
			pk_CollegePathwayInstructor__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		INDEX
			ix_CollegePathwayInstructor__IsActive
		NONCLUSTERED
			(
				IsActive
			)
	);

GO

INSERT INTO
	lbccpt.CollegePathwayInstructor
	(
		Name,
		Label,
		Ordinal,
		IsActive
	)
SELECT
	Name,
	Label,
	Ordinal = row_number() over(order by Name),
	IsActive
FROM
	(
		VALUES
		('instructor_00','Instructor externships',1),
		('instructor_01','Professional development related to the pathway',1),
		('instructor_02','Time for K–12 and community college staff to collaborate',1)
	) t (Name,Label,IsActive)
ORDER BY
	Name;