USE calpass;

GO

IF (object_id('lbccpt.School_SchoolPathway') is not null)
	BEGIN
		DROP TABLE lbccpt.School_SchoolPathway;
	END;

GO

CREATE TABLE
	lbccpt.School_SchoolPathway
	(
		Id int identity(1,1) not null,
		ConsortiumSchoolId int not null, -- must be tied to consortium_school which is tied to consortrium
		AcademicYearId int not null,
		SchoolPathwayId int not null,
		CONSTRAINT
			pk_School_SchoolPathway__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		CONSTRAINT
			uk_School_SchoolPathway__ConsortiumSchoolId__AcademicYearId__SchoolPathwayId
		UNIQUE
			(
				ConsortiumSchoolId,
				AcademicYearId,
				SchoolPathwayId
			),
		CONSTRAINT
			fk_School_SchoolPathway__ConsortiumSchoolId
		FOREIGN KEY
			(
				ConsortiumSchoolId
			)
		REFERENCES
			lbccpt.Consortium_School
			(
				Id
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_School_SchoolPathway__AcademicYearId
		FOREIGN KEY
			(
				AcademicYearId
			)
		REFERENCES
			lbccpt.AcademicYear
			(
				Id
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_School_SchoolPathway__SchoolPathwayId
		FOREIGN KEY
			(
				SchoolPathwayId
			)
		REFERENCES
			lbccpt.SchoolPathway
			(
				Id
			)
		ON DELETE CASCADE
	)