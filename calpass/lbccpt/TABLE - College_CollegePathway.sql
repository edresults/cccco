USE calpass;

GO

IF (object_id('lbccpt.College_CollegePathway') is not null)
	BEGIN
		DROP TABLE lbccpt.College_CollegePathway;
	END;

GO

CREATE TABLE
	lbccpt.College_CollegePathway
	(
		Id int identity(1,1) not null,
		ConsortiumCollegeId int not null,
		Name nvarchar(255) not null,
		AcademicYearId int not null,
		CONSTRAINT
			pk_College_CollegePathway__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		CONSTRAINT
			uk_College_CollegePathway__ConsortiumCollegeId__AcademicYearId__Name
		UNIQUE
			(
				ConsortiumCollegeId,
				AcademicYearId,
				Name
			),
		CONSTRAINT
			fk_College_CollegePathway__ConsortiumCollegeId
		FOREIGN KEY
			(
				ConsortiumCollegeId
			)
		REFERENCES
			lbccpt.Consortium_College
			(
				Id
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_College_CollegePathway__AcademicYearId
		FOREIGN KEY
			(
				AcademicYearId
			)
		REFERENCES
			lbccpt.AcademicYear
			(
				Id
			)
		ON DELETE CASCADE
	);