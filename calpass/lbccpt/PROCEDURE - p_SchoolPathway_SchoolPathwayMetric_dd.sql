USE calpass;

GO

IF (object_id('lbccpt.p_SchoolPathway_SchoolPathwayMetric_dd') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_SchoolPathway_SchoolPathwayMetric_dd;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_SchoolPathway_SchoolPathwayMetric_dd
	(
		@School_SchoolPathway__Id int
	)
AS

BEGIN
	SELECT
		MetricId = spm.Id,
		MetricName = spm.Name,
		MetricLabel = spm.Label,
		SchoolPathwayMetricId = spspm.Id,
		SchoolPathwayMetricValue = spspm.MetricValue
	FROM
		lbccpt.SchoolPathwayMetric spm
		left outer join
		lbccpt.SchoolPathway_SchoolPathwayMetric spspm
			on spm.Id = spspm.SchoolPathwayMetricId
			and spspm.SchoolPathwayId = @School_SchoolPathway__Id
	WHERE
		spm.IsActive = 1
	ORDER BY
		spm.Ordinal;
END;