USE calpass;

GO

IF (object_id('lbccpt.CollegePathwayMetric') is not null)
	BEGIN
		DROP TABLE lbccpt.CollegePathwayMetric;
	END;

GO

CREATE TABLE
	lbccpt.CollegePathwayMetric
	(
		Id int identity(0,1) not null,
		Name nvarchar(255) not null,
		Label nvarchar(255) not null,
		IsActive bit not null,
		Ordinal int not null,
		NameLegacy nvarchar(255) null,
		CONSTRAINT
			pk_CollegePathwayMetric__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		INDEX
			ix_CollegePathwayMetric__IsActive
		NONCLUSTERED
			(
				IsActive
			)
	);

GO

INSERT INTO
	lbccpt.CollegePathwayMetric
	(
		Name,
		Label,
		IsActive,
		Ordinal,
		NameLegacy
	)
SELECT
	Name,
	Label,
	IsActive,
	Ordinal = row_number() over(order by Name),
	NameLegacy
FROM
	(
		VALUES
		('metric_00','Number of students enrolled in the career pathway program.',1,'v31'),
		('metric_01','Number of students in dual enrollment courses aligned with the career pathway program (academic and CTE) in which the students are participating.',1,'v01'),
		('metric_02','Number of credits earned in dual enrollment courses aligned with the career pathway program (academic and CTE) in which the students are participating.',1,'v01b'),
		('metric_03','Number of students in credit by exam opportunities aligned with the career pathway program in which the students are participating.',1,'v02'),
		('metric_04','Number of credits earned in credit by exam opportunities aligned with the career pathway program in which the students are participating.',1,'v02b'),
		('metric_05','Number of students who completed one credit-bearing CTE course aligned with the career pathway program in which the students are participating.',1,'v22'),
		('metric_06','Number of students who completed two credit-bearing CTE courses aligned with the career pathway program in which the students are participating.',1,'v23'),
		('metric_07','Received an associate of arts degree in the targeted pathway.',1,'v24'),
		('metric_08','Received an associate of science degree in the targeted pathway.',1,'v25'),
		('metric_09','Received a transfer associate degree in the targeted pathway.',1,'v26'),
		('metric_10','Number of students participating in job shadowing opportunities aligned with the career pathway program in which they are participating.',1,'v03'),
		('metric_11','Number of students participating in mentoring opportunities aligned with the career pathwayprogram in which thestudentsare participating.',1,'v05'),
		('metric_12','Number of students participating in internships aligned with the career pathway program in which thestudentsare participating.',1,'v06'),
		('metric_13','Number of students participating in work experience opportunities aligned with the career pathway program in which they are participating.',1,'v07'),
		('metric_14','Number of students participating in a preapprenticeship program aligned with the career pathway program in which theyare participatingand aligned with a state-approved apprenticeship',1,'v08'),
		('metric_15','Number of students participating in a state-approved apprenticeship program aligned with the career pathway program in which they are participating.',1,'v09'),
		('metric_16','Number of students participating in student leadership organizations as part of the career pathway program.',1,'v10'),
		('metric_17','Number of students in the career pathway program who transitioned from community college to a four-year college or university.',1,'v27'),
		('metric_18','Received an associate of arts degree not aligned to the targeted pathway.',1,'v28'),
		('metric_19','Received an associate of science degree not aligned to the targeted pathway.',1,'v29'),
		('metric_20','Received a transfer associate degree not aligned to the targeted pathway.',1,'v30'),
		('metric_21','Number of students in the career pathway program who entered into employment aligned with the career pathway.',1,'v11'),
		('metric_22','Number of students in the career pathway program who entered into training aligned with the career pathway.',1,'v12'),
		('metric_23','Number of students in the career pathway program who received a nationally recognized industry-valued certificateand/or state license.',1,'v13'),
		('metric_24','Number of students in the career pathway program who received a Chancellor’s Office approved certificate of achievement or performance.',1,'v32')
	) t (Name,Label,IsActive,NameLegacy)
ORDER BY
	Name;