USE calpass;

IF (object_id('lbccpt.p_School_SchoolPathwayMetric_cu') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_School_SchoolPathwayMetric_cu;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_School_SchoolPathwayMetric_cu
	(
		@json_in nvarchar(max),
		@message nvarchar(2048) = N'' OUT
	)
AS

	SET NOCOUNT ON;

DECLARE
	@SchoolPathwayId int;

BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			-- set SchoolPathwayId
			SELECT
				@SchoolPathwayId = json_value(j.value, '$.Id')
			FROM
				openjson(@json_in, '$.SchoolPathway') j;
			-- Metrics
			MERGE
				calpass.lbccpt.SchoolPathway_SchoolPathwayMetric with(
					holdlock
				) t
			USING
				(
					SELECT DISTINCT
						Id = json_value(m.value, '$.Id'),
						SchoolPathwayId = json_value(j.value, '$.Id'),
						SchoolPathwayMetricId = json_value(m.value, '$.SchoolPathwayMetricId'),
						MetricValue = json_value(m.value, '$.MetricValue')
					FROM
						openjson(@json_in, '$.SchoolPathway') j
						cross apply
						openjson(j.value, '$.SchoolPathwayMetrics') m
				) s
			ON
				t.Id = s.Id
			WHEN NOT MATCHED BY TARGET THEN
				INSERT
					(
						SchoolPathwayId,
						SchoolPathwayMetricId,
						MetricValue
					)
				VALUES
					(
						s.SchoolPathwayId,
						s.SchoolPathwayMetricId,
						s.MetricValue
					)
			WHEN MATCHED THEN
				UPDATE SET
					MetricValue = s.MetricValue
			WHEN NOT MATCHED BY SOURCE
				and t.SchoolPathwayId = @SchoolPathwayId THEN
				DELETE;
		END TRY
		BEGIN CATCH
			IF (@@TRANCOUNT > 0)
				BEGIN
					ROLLBACK TRANSACTION;
				END;
			-- SET @message = N'Error occurred when attempting to ' + @verb + N' Consortium schools';
			SET @message =
				nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
				nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
				nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
			THROW 70099, @message, 1;
		END CATCH;

	COMMIT TRANSACTION;
END;