USE calpass;

IF (object_id('lbccpt.p_College_CollegePathway_d') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_College_CollegePathway_d;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_College_CollegePathway_d
	(
		@json_in nvarchar(max),
		@message nvarchar(2048) = N'' OUT
	)
AS

	SET NOCOUNT ON;

DECLARE
	@IsActive bit = 1;

BEGIN
	BEGIN TRY
		DELETE
			t
		FROM
			calpass.lbccpt.College_CollegePathway t
		WHERE
			t.Id = (
				SELECT
					j.Id
				FROM
					openjson(@json_in, '$.CollegePathway') with (
						Id int '$.Id'
					) j
			);
	END TRY
	BEGIN CATCH
		-- SET @message = N'Error occurred when deleting college pathway';
		SET @message =
			nchar(9) + N'Error Number:    ' + cast(isnull(ERROR_NUMBER(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Severity:  ' + cast(isnull(ERROR_SEVERITY(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error State:     ' + cast(isnull(ERROR_STATE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Procedure: ' + object_name(@@PROCID) + char(13) + char(10) +
			nchar(9) + N'Error Line:      ' + cast(isnull(ERROR_LINE(), N'') as nvarchar) + char(13) + char(10) +
			nchar(9) + N'Error Message:   ' + isnull(ERROR_MESSAGE(), N'');
		THROW 70099, @message, 1;
	END CATCH;
END;