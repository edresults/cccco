USE calpass;

GO

IF (object_id('lbccpt.CollegePathway_CollegePathwayInstructor') is not null)
	BEGIN
		DROP TABLE lbccpt.CollegePathway_CollegePathwayInstructor;
	END;

GO

CREATE TABLE
	lbccpt.CollegePathway_CollegePathwayInstructor
	(
		Id int identity(1,1) not null,
		CollegePathwayId int not null,
		CollegePathwayInstructorId int not null,
		CONSTRAINT
			pk_CollegePathway_CollegePathwayInstructor__Id
		PRIMARY KEY
			(
				Id
			),
		CONSTRAINT
			uq_CollegePathway_CollegePathwayInstructor__CollegePathwayId__CollegePathwayInstructorId
		UNIQUE
			(
				CollegePathwayId,
				CollegePathwayInstructorId
			),
		CONSTRAINT
			fk_CollegePathway_CollegePathwayInstructor__CollegePathwayId
		FOREIGN KEY
			(
				CollegePathwayId
			)
		REFERENCES
			lbccpt.College_CollegePathway
			(
				Id
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_CollegePathway_CollegePathwayInstructor_CollegePathwayInstructorId
		FOREIGN KEY
			(
				CollegePathwayInstructorId
			)
		REFERENCES
			lbccpt.CollegePathwayInstructor
			(
				Id
			)
		ON DELETE CASCADE
	);