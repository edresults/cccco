USE calpass;

GO

IF (object_id('lbccpt.CollegePathway_CollegePathwayMetric') is not null)
	BEGIN
		DROP TABLE lbccpt.CollegePathway_CollegePathwayMetric;
	END;

GO

CREATE TABLE
	lbccpt.CollegePathway_CollegePathwayMetric
	(
		Id int identity(1,1) not null,
		CollegePathwayId int not null,
		CollegePathwayMetricId int not null,
		MetricValue int not null,
		CONSTRAINT
			pk_CollegePathway_CollegePathwayMetric__Id
		PRIMARY KEY
			(
				Id
			),
		CONSTRAINT
			uq_CollegePathway_CollegePathwayMetric__CollegePathwayId__CollegePathwayMetricId
		UNIQUE
			(
				CollegePathwayId,
				CollegePathwayMetricId
			),
		CONSTRAINT
			fk_CollegePathway_CollegePathwayMetric__CollegePathwayId
		FOREIGN KEY
			(
				CollegePathwayId
			)
		REFERENCES
			lbccpt.College_CollegePathway
			(
				Id
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_CollegePathway_CollegePathwayMetric_CollegePathwayMetricId
		FOREIGN KEY
			(
				CollegePathwayMetricId
			)
		REFERENCES
			lbccpt.CollegePathwayMetric
			(
				Id
			)
		ON DELETE CASCADE
	);