USE calpass;

GO

IF (object_id('lbccpt.CollegePathwayTaxonomy') is not null)
	BEGIN
		DROP TABLE lbccpt.CollegePathwayTaxonomy;
	END;

GO

CREATE TABLE
	lbccpt.CollegePathwayTaxonomy
	(
		Id int identity(0,1) not null,
		Code char(6) not null,
		Name nvarchar(255) not null,
		Label nvarchar(255) not null,
		IsActive bit not null,
		Ordinal int not null,
		CONSTRAINT
			pk_CollegePathwayTaxonomy__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		INDEX
			ix_CollegePathwayTaxonomy__IsActive
		NONCLUSTERED
			(
				IsActive
			)
	);

GO

INSERT INTO
	lbccpt.CollegePathwayTaxonomy
	(
		Code,
		Name,
		Label,
		IsActive,
		Ordinal
	)
SELECT
	Code = ProgramCode,
	Name = Name,
	Lable = Name + ' (' + ProgramCode + ')',
	1,
	Ordinal = row_number() over(order by Name)
FROM
	[pro-dat-sql-03].calpass.comis.Program
ORDER BY
	Name;