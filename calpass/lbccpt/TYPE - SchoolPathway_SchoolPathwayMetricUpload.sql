USE calpass;

GO

IF (type_id('lbccpt.SchoolPathway_SchoolPathwayMetricUpload') is not null)
	BEGIN
		DROP TYPE lbccpt.SchoolPathway_SchoolPathwayMetricUpload;
	END;

GO

CREATE TYPE
	lbccpt.SchoolPathway_SchoolPathwayMetricUpload
AS TABLE
	(
		ConsortiumId int,
		CdsCode char(14),
		ConsortiumSchoolId int,
		AcademicYear char(9),
		AcademicYearId int,
		SchoolPathwayCode char(3),
		SchoolPathwayId int,
		School_SchoolPathwayId int,
		metric_00 int,
		metric_01 int,
		metric_02 int,
		metric_03 int,
		metric_04 int,
		metric_05 int,
		metric_06 int,
		metric_07 int,
		metric_08 int,
		metric_09 int,
		metric_10 int,
		metric_11 int,
		metric_12 int,
		metric_13 int,
		metric_14 int,
		metric_15 int,
		metric_16 int,
		metric_17 int,
		metric_18 int,
		metric_19 int,
		metric_20 int
	);