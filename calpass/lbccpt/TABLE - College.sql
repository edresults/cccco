USE calpass;

GO

IF (object_id('lbccpt.College') is not null)
	BEGIN
	 DROP TABLE lbccpt.College;
	END;

GO

CREATE TABLE
	lbccpt.College
	(
		Id int identity(0,1) not null,
		CodeCalpass int not null,
		CodeIpeds char(6) not null,
		CodeComis char(14) not null,
		Name nvarchar(255) not null,
		Label nvarchar(255) not null,
		IsActive bit not null,
		Ordinal int not null,
		CONSTRAINT
			pk_College
		PRIMARY KEY CLUSTERED
			(
				id
			),
		CONSTRAINT
			uq_College__CodeCalpass
		UNIQUE
			(
				CodeCalpass
			),
		CONSTRAINT
			uq_College__CodeIpeds
		UNIQUE
			(
				CodeIpeds
			),
		CONSTRAINT
			uq_College__CodeComis
		UNIQUE
			(
				CodeComis
			)
	);

GO

INSERT INTO
	lbccpt.College
	(
		CodeCalpass,
		CodeIpeds,
		CodeComis,
		Name,
		Label,
		IsActive,
		Ordinal
	)
SELECT
	CodeCalpass = o.OrganizationId,
	CodeIpeds = o.OrganizationCode,
	CodeComis = c.CollegeCode,
	Name = o.OrganizationName,
	Label = o.OrganizationName,
	IsActive = 1,
	Ordinal = row_number() over(order by o.OrganizationName)
FROM
	[pro-dat-sql-01].calpass.dbo.Organization o
	inner join
	[pro-dat-sql-03].calpass.comis.College c
		on o.organizationId = c.CodeLegacy
ORDER BY
	o.OrganizationName;