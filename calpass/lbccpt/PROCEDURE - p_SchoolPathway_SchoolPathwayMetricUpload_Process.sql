USE calpass;

GO

IF (object_id('lbccpt.p_SchoolPathway_SchoolPathwayMetricUpload_Process') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_SchoolPathway_SchoolPathwayMetricUpload_Process;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_SchoolPathway_SchoolPathwayMetricUpload_Process
	(
		@StageSchemaTableName nvarchar(517)
	)
AS

DECLARE
	@validate bit = 0,
	@sql nvarchar(max),
	@tab_needle varchar(255) = char(13) + char(10) + char(9) + char(9),
	@tab_replace varchar(255) = char(13) + char(10);

BEGIN
	-- validate inputs
	SELECT
		@validate = 1
	FROM
		sys.schemas s
		inner join
		sys.tables t
			on s.schema_id = t.schema_id
	WHERE
		s.name = parsename(@StageSchemaTableName, 2)
		and t.name = parsename(@StageSchemaTableName, 1);
	-- process validation
	IF (@validate = 0)
		BEGIN
			RETURN;
		END;
	/*
		step 01: update SchoolId
	*/
	SET @sql = replace(N'
		MERGE
			' + @StageSchemaTableName + '
		WITH
			(
				TABLOCKX
			) t
		USING
			lbccpt.School s
		ON
			s.CodeCalpads = t.CdsCode
		WHEN MATCHED THEN
			UPDATE SET
				t.SchoolId = s.Id
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;', @tab_needle, @tab_replace);
	
	EXECUTE sp_executesql
		@sql;

	/*
		step 02: update AcademicYearId
	*/
	SET @sql = replace(N'
		MERGE
			' + @StageSchemaTableName + '
		WITH
			(
				TABLOCKX
			) t
		USING
			lbccpt.AcademicYear s
		ON
			s.CodeCalpads = t.AcademicYear
		WHEN MATCHED THEN
			UPDATE SET
				t.AcademicYearId = s.Id
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;', @tab_needle, @tab_replace);
	
	EXECUTE sp_executesql
		@sql;

	/*
		step 03: update SchoolPathwayId
	*/
	SET @sql = replace(N'
		MERGE
			' + @StageSchemaTableName + '
		WITH
			(
				TABLOCKX
			) t
		USING
			lbccpt.SchoolPathway s
		ON
			s.Code = t.SchoolPathwayCode
		WHEN MATCHED THEN
			UPDATE SET
				t.SchoolPathwayId = s.Id
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;', @tab_needle, @tab_replace);
	
	EXECUTE sp_executesql
		@sql;

	/*
		step 04: update ConsortiumSchoolId
	*/
	SET @sql = replace(N'
		UPDATE
			t
		SET
			t.ConsortiumSchoolId = s.id
		FROM
			' + @StageSchemaTableName + ' t
			inner join
			lbccpt.Consortium_School s
				on t.ConsortiumId = s.ConsortiumId
				and t.SchoolId = s.SchoolId;

		DECLARE
			@Consortium_School
		AS TABLE
			(
				Id int,
				ConsortiumId int,
				SchoolId int
			);

		INSERT INTO
			lbccpt.Consortium_School
		WITH
			(
				TABLOCKX
			)
			(
				ConsortiumId,
				SchoolId
			)
		OUTPUT
			inserted.Id,
			inserted.ConsortiumId,
			inserted.SchoolId
		INTO
			@Consortium_School
			(
				Id,
				ConsortiumId,
				SchoolId
			)
		SELECT DISTINCT
			ConsortiumId,
			SchoolId
		FROM
			' + @StageSchemaTableName + ' s
		WHERE
			s.ConsortiumSchoolId is null;

		UPDATE
			t
		SET
			t.ConsortiumSchoolId = s.id
		FROM
			' + @StageSchemaTableName + ' t
			inner join
			@Consortium_School s
				on t.ConsortiumId = s.ConsortiumId
				and t.SchoolId = s.SchoolId;', @tab_needle, @tab_replace);
	
	EXECUTE sp_executesql
		@sql;

	/*
		step 04: update School_SchoolPathwayId
	*/
	SET @sql = replace(N'
		UPDATE
			t
		SET
			t.School_SchoolPathwayId = s.id
		FROM
			' + @StageSchemaTableName + ' t
			inner join
			lbccpt.School_SchoolPathway s
				on t.ConsortiumSchoolId = s.ConsortiumSchoolId
				and t.AcademicYearId = s.AcademicYearId
				and t.SchoolPathwayId = s.SchoolPathwayId;

		DECLARE
			@School_SchoolPathway
		AS TABLE
			(
				Id int,
				ConsortiumSchoolId int,
				AcademicYearId int,
				SchoolPathwayId int
			);

		INSERT INTO
			lbccpt.School_SchoolPathway
		WITH
			(
				TABLOCKX
			)
			(
				ConsortiumSchoolId,
				AcademicYearId,
				SchoolPathwayId
			)
		OUTPUT
			inserted.Id,
			inserted.ConsortiumSchoolId,
			inserted.AcademicYearId,
			inserted.SchoolPathwayId
		INTO
			@School_SchoolPathway
			(
				Id,
				ConsortiumSchoolId,
				AcademicYearId,
				SchoolPathwayId
			)
		SELECT
			ConsortiumSchoolId,
			AcademicYearId,
			SchoolPathwayId
		FROM
			' + @StageSchemaTableName + ' s
		WHERE
			s.School_SchoolPathwayId is null;

		UPDATE
			t
		SET
			t.School_SchoolPathwayId = s.id
		FROM
			' + @StageSchemaTableName + ' t
			inner join
			@School_SchoolPathway s
				on t.ConsortiumSchoolId = s.ConsortiumSchoolId
				and t.AcademicYearId = s.AcademicYearId
				and t.SchoolPathwayId = s.SchoolPathwayId;', @tab_needle, @tab_replace);
	
	EXECUTE sp_executesql
		@sql;

	/*
		step 05: merge into SchoolPathway_SchoolPathwayMetricUpload
	*/
	SET @sql = replace(N'
		UPDATE
			t
		SET
			t.School_SchoolPathwayId = s.id
		FROM
			' + @StageSchemaTableName + ' t
			inner join
			lbccpt.School_SchoolPathway s
				on t.ConsortiumSchoolId = s.ConsortiumSchoolId
				and t.AcademicYearId = s.AcademicYearId
				and t.SchoolPathwayId = s.SchoolPathwayId;

		DECLARE
			@School_SchoolPathway
		AS TABLE
			(
				Id int,
				ConsortiumSchoolId int,
				AcademicYearId int,
				SchoolPathwayId int
			);

		INSERT INTO
			lbccpt.School_SchoolPathway
		WITH
			(
				TABLOCKX
			)
			(
				ConsortiumSchoolId,
				AcademicYearId,
				SchoolPathwayId
			)
		OUTPUT
			inserted.Id,
			inserted.ConsortiumSchoolId,
			inserted.AcademicYearId,
			inserted.SchoolPathwayId
		INTO
			@School_SchoolPathway
			(
				Id,
				ConsortiumSchoolId,
				AcademicYearId,
				SchoolPathwayId
			)
		SELECT
			ConsortiumSchoolId,
			AcademicYearId,
			SchoolPathwayId
		FROM
			' + @StageSchemaTableName + ' s
		WHERE
			s.School_SchoolPathwayId is null;

		UPDATE
			t
		SET
			t.School_SchoolPathwayId = s.id
		FROM
			' + @StageSchemaTableName + ' t
			inner join
			@School_SchoolPathway s
				on t.ConsortiumSchoolId = s.ConsortiumSchoolId
				and t.AcademicYearId = s.AcademicYearId
				and t.SchoolPathwayId = s.SchoolPathwayId;', @tab_needle, @tab_replace);
	
	EXECUTE sp_executesql
		@sql;

	/*
		step 06: insert from stage to production
	*/
	SET @sql = replace(N'
		MERGE
			lbccpt.SchoolPathway_SchoolPathwayMetricUpload
		WITH
			(
				TABLOCKX
			) t
		USING
			' + @StageSchemaTableName + ' s
		ON
			t.School_SchoolPathwayId = s.School_SchoolPathwayId
		WHEN MATCHED THEN
			UPDATE SET
				t.metric_00 = s.metric_00,
				t.metric_01 = s.metric_01,
				t.metric_02 = s.metric_02,
				t.metric_03 = s.metric_03,
				t.metric_04 = s.metric_04,
				t.metric_05 = s.metric_05,
				t.metric_06 = s.metric_06,
				t.metric_07 = s.metric_07,
				t.metric_08 = s.metric_08,
				t.metric_09 = s.metric_09,
				t.metric_10 = s.metric_10,
				t.metric_11 = s.metric_11,
				t.metric_12 = s.metric_12,
				t.metric_13 = s.metric_13,
				t.metric_14 = s.metric_14,
				t.metric_15 = s.metric_15,
				t.metric_16 = s.metric_16,
				t.metric_17 = s.metric_17,
				t.metric_18 = s.metric_18,
				t.metric_19 = s.metric_19,
				t.metric_20 = s.metric_20
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					ConsortiumId,
					CdsCode,
					SchoolId,
					ConsortiumSchoolId,
					AcademicYear,
					AcademicYearId,
					SchoolPathwayCode,
					SchoolPathwayId,
					School_SchoolPathwayId,
					metric_00,
					metric_01,
					metric_02,
					metric_03,
					metric_04,
					metric_05,
					metric_06,
					metric_07,
					metric_08,
					metric_09,
					metric_10,
					metric_11,
					metric_12,
					metric_13,
					metric_14,
					metric_15,
					metric_16,
					metric_17,
					metric_18,
					metric_19,
					metric_20
				)
			VALUES
				(
					ConsortiumId,
					CdsCode,
					SchoolId,
					ConsortiumSchoolId,
					AcademicYear,
					AcademicYearId,
					SchoolPathwayCode,
					SchoolPathwayId,
					School_SchoolPathwayId,
					metric_00,
					metric_01,
					metric_02,
					metric_03,
					metric_04,
					metric_05,
					metric_06,
					metric_07,
					metric_08,
					metric_09,
					metric_10,
					metric_11,
					metric_12,
					metric_13,
					metric_14,
					metric_15,
					metric_16,
					metric_17,
					metric_18,
					metric_19,
					metric_20
				);', @tab_needle, @tab_replace);
	
	EXECUTE sp_executesql
		@sql;

	/*
		step 07: insert from stage to production
	*/
	SET @sql = replace(N'
		MERGE
			lbccpt.SchoolPathway_SchoolPathwayMetric
		WITH
			(
				TABLOCKX
			) t
		USING
			(
				SELECT
					SchoolPathwayId = u.School_SchoolPathwayId,
					SchoolPathwayMetricId = spm.Id,
					MetricValue = u.MetricValue
				FROM
					' + @StageSchemaTableName + ' i
				UNPIVOT
					(
						MetricValue for MetricName in (
							metric_00,
							metric_01,
							metric_02,
							metric_03,
							metric_04,
							metric_05,
							metric_06,
							metric_07,
							metric_08,
							metric_09,
							metric_10,
							metric_11,
							metric_12,
							metric_13,
							metric_14,
							metric_15,
							metric_16,
							metric_17,
							metric_18,
							metric_19,
							metric_20
						)
					) u
					inner join
					lbccpt.SchoolPathwayMetric spm
						on spm.Name = u.MetricName
			) s
		ON
			t.SchoolPathwayId = s.SchoolPathwayId
			and t.SchoolPathwayMetricId = s.SchoolPathwayMetricId
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
				(
					SchoolPathwayId,
					SchoolPathwayMetricId,
					MetricValue
				)
			VALUES
				(
					s.SchoolPathwayId,
					s.SchoolPathwayMetricId,
					s.MetricValue
				)
		WHEN MATCHED THEN
			UPDATE SET
				t.MetricValue = s.MetricValue;', @tab_needle, @tab_replace);
	
	EXECUTE sp_executesql
		@sql;
END;