USE calpass;

GO

IF (object_id('lbccpt.Consortium_College') is not null)
	BEGIN
		DROP TABLE lbccpt.Consortium_College;
	END;

GO

CREATE TABLE
	lbccpt.Consortium_College
	(
		Id int identity(1,1) not null,
		ConsortiumId int not null,
		CollegeId int not null,
		CONSTRAINT
			pk_Consortium_College__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		CONSTRAINT
			uq_Consortium_College__ConsortiumId__CollegeId
		UNIQUE
			(
				ConsortiumId,
				CollegeId
			),
		CONSTRAINT
			fk_Consortium_College__ConsortiumId
		FOREIGN KEY
			(
				ConsortiumId
			)
		REFERENCES
			lbccpt.Consortium
			(
				Id
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_Consortium_College__CollegeId
		FOREIGN KEY
			(
				CollegeId
			)
		REFERENCES
			lbccpt.College
			(
				Id
			)
		ON DELETE CASCADE
	);