USE calpass;

GO

IF (object_id('lbccpt.p_CollegePathway_CollegePathwayMetric_dd') is not null)
	BEGIN
		DROP PROCEDURE lbccpt.p_CollegePathway_CollegePathwayMetric_dd;
	END;

GO

CREATE PROCEDURE
	lbccpt.p_CollegePathway_CollegePathwayMetric_dd
	(
		@College_CollegePathway__Id int
	)
AS

BEGIN
	SELECT
		MetricId = spm.Id,
		MetricName = spm.Name,
		MetricLabel = spm.Label,
		CollegePathwayMetricId = spspm.Id,
		CollegePathwayMetricValue = spspm.MetricValue
	FROM
		lbccpt.CollegePathwayMetric spm
		left outer join
		lbccpt.CollegePathway_CollegePathwayMetric spspm
			on spm.Id = spspm.CollegePathwayMetricId
			and spspm.CollegePathwayId = @College_CollegePathway__Id
	WHERE
		spm.IsActive = 1
	ORDER BY
		spm.Ordinal;
END;