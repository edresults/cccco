USE calpass;

GO

IF (object_id('lbccpt.SchoolPathway_SchoolPathwayStudent') is not null)
	BEGIN
		DROP TABLE lbccpt.SchoolPathway_SchoolPathwayStudent;
	END;

GO

CREATE TABLE
	lbccpt.SchoolPathway_SchoolPathwayStudent
	(
		Id int identity(1,1) not null,
		SchoolPathwayId int not null,
		SchoolPathwayStudentId int not null,
		CONSTRAINT
			pk_SchoolPathway_SchoolPathwayStudent__Id
		PRIMARY KEY CLUSTERED
			(
				Id
			),
		CONSTRAINT
			uq_SchoolPathway_SchoolPathwayStudent__SchoolPathwayId__SchoolPathwayStudentId
		UNIQUE
			(
				SchoolPathwayId,
				SchoolPathwayStudentId
			),
		CONSTRAINT
			fk_SchoolPathway_SchoolPathwayStudent__SchoolPathwayId
		FOREIGN KEY
			(
				SchoolPathwayId
			)
		REFERENCES
			lbccpt.School_SchoolPathway
			(
				Id
			)
		ON DELETE CASCADE,
		CONSTRAINT
			fk_SchoolPathway_SchoolPathwayStudent_SchoolPathwayStudentId
		FOREIGN KEY
			(
				SchoolPathwayStudentId
			)
		REFERENCES
			lbccpt.SchoolPathwayStudent
			(
				Id
			)
		ON DELETE CASCADE
	);